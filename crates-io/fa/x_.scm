(define-module (crates-io fa x_) #:use-module (crates-io))

(define-public crate-fax_derive-0.1 (crate (name "fax_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fas9nvz2yqidlv2my7lvp0n1p4gkjlq2ln791ik3j1dkzy7y79w")))

