(define-module (crates-io fa hr) #:use-module (crates-io))

(define-public crate-fahrcel-1 (crate (name "fahrcel") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pkjpc7jr5rplm8fxdaywc8ngn0yvzc0j2k89v2nj2jgy96lbsb4")))

(define-public crate-fahrenheit-4 (crate (name "fahrenheit") (vers "4.5.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zx8p88cd6kjd6y176l5x722kx63ib389a3nk87qmx02s0abyari")))

(define-public crate-fahrenheit-4 (crate (name "fahrenheit") (vers "4.5.2") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rpwjrm38r232b83agj2a0dzsbj4fwypb97xxl6j8wzr5wmpf96z")))

(define-public crate-fahrenheit-4 (crate (name "fahrenheit") (vers "4.5.3") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bnrqaqczq49l245dsdxzfaiw73alxz3g4v8qq5c4ahkdbk1x371")))

(define-public crate-fahrenheit-4 (crate (name "fahrenheit") (vers "4.5.4") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b2vybn5wxff1cvj6ap8sk2ga2wwyvhcn5wa1lwb1kwb5kh9c24v")))

(define-public crate-fahrenheit_to_celsius-0.1 (crate (name "fahrenheit_to_celsius") (vers "0.1.0") (deps (list (crate-dep (name "macro_colors") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1zihxgs1crrpqnxc0jfgcgf2dgw5lhirsjvinli334xagwm5dv2w")))

