(define-module (crates-io fa va) #:use-module (crates-io))

(define-public crate-favannat-0.1 (crate (name "favannat") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)))) (hash "145vwnb4iadhwn21pzpnpyk9sddb46ybjfiwwaf00qnr448324yg")))

(define-public crate-favannat-0.1 (crate (name "favannat") (vers "0.1.2") (deps (list (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)))) (hash "0zzkj3ckqk17l5hffnzaqnkv531314f6365h24wr4av7x15cn4l4")))

(define-public crate-favannat-0.2 (crate (name "favannat") (vers "0.2.0") (deps (list (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)))) (hash "0wmgpfg0263zsn8y7ll911q06wi332598cz8x8jq5if2p5a1hd4k")))

(define-public crate-favannat-0.3 (crate (name "favannat") (vers "0.3.0") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)))) (hash "1znlw6r1chfn1pi4m28ff5p1qrrs9x4p3c96qfj4b3vlvk113ypx")))

(define-public crate-favannat-0.4 (crate (name "favannat") (vers "0.4.0") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)))) (hash "140wqban86d1ff1p7n3i75zb9jzkx3yc1vbyibskwyi8pcyjfgxp")))

(define-public crate-favannat-0.4 (crate (name "favannat") (vers "0.4.1") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)))) (hash "19s5380qmp38cgf2yy3jgh2asp3sv136ms55aqy4x3d6sh1cqy82")))

(define-public crate-favannat-0.5 (crate (name "favannat") (vers "0.5.0") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "1v1dx6xwgf69wnv8pbx1xpgm4p55q0iglc0ms4c16hrqgf1gk5ar")))

(define-public crate-favannat-0.5 (crate (name "favannat") (vers "0.5.1") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "04vspni32zrk2rkdvkk3z9qc9cb90vpbzr94iip9x0bdmkwnqx9r")))

(define-public crate-favannat-0.5 (crate (name "favannat") (vers "0.5.2") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "17d11kv6mxckm7nvw2v65m2gxyma2l4w8dnlh7nnqxh6lfdx7mnz")))

(define-public crate-favannat-0.6 (crate (name "favannat") (vers "0.6.0") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "10pr7bzgg3l0grs7ipypdks3nr3q95jsibbqadzm43h1kn0llnq7")))

(define-public crate-favannat-0.6 (crate (name "favannat") (vers "0.6.1") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "1fi8rld5xxczk1kqn2vmmrqs5bybkqwi5ispqa4p0xx6jqd0q8xy")))

(define-public crate-favannat-0.6 (crate (name "favannat") (vers "0.6.2") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "064qk4xirkhja8bfnikjdphxh7kp8ln09plm500mfl44j79qkfjr")))

(define-public crate-favannat-0.6 (crate (name "favannat") (vers "0.6.3") (deps (list (crate-dep (name "nalgebra") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "171fgmn8zh2iadbk90bv7ic0qwhy4c452vfj6ygqd1f8wl95dxlw")))

(define-public crate-favannat-0.6 (crate (name "favannat") (vers "0.6.4") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "0c21869anfb3gswlnz8hn2x6wfwidzh35hk69k2d3qa1sagrixfn")))

