(define-module (crates-io fa dv) #:use-module (crates-io))

(define-public crate-fadvise-0.1 (crate (name "fadvise") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^3.2.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1hywws7ax0c6zz26pvl1dparympgafkvfj9yvcik69n82gp36mji") (rust-version "1.63")))

