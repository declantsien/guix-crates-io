(define-module (crates-io fa tb) #:use-module (crates-io))

(define-public crate-fatbinary-0.1 (crate (name "fatbinary") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "binread") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0rd8vypny8xf68sbi7s5ay6l618g2hjfvri7g1wpd7i7a360s83x")))

(define-public crate-fatbinary-0.2 (crate (name "fatbinary") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "binread") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0azkrfcmdvy25396p31fmprc6j8f71imag9kpxicr1jcjjv044q1")))

