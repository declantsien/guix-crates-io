(define-module (crates-io fa bc) #:use-module (crates-io))

(define-public crate-fabcode-0.1 (crate (name "fabcode") (vers "0.1.0") (hash "0r35zg32j8cmyy6pnh0fn0fssm1lwc9b6yjwipv7vhplp27vrfmp")))

(define-public crate-fabcode-0.2 (crate (name "fabcode") (vers "0.2.0") (hash "14r7dnvxajbvvlkjbbnrs938aw8vp2fpahis98p5dx8jhkqwwqa5")))

