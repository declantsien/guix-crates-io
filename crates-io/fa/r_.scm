(define-module (crates-io fa r_) #:use-module (crates-io))

(define-public crate-far_macros-0.2 (crate (name "far_macros") (vers "0.2.0-beta.0") (deps (list (crate-dep (name "far_shared") (req "^0.2.0-beta.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-crate") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zbx02i0qgyyzlrliiahwlw41rg2mybw4n6l9s11c1w4bxp924c7")))

(define-public crate-far_macros-0.2 (crate (name "far_macros") (vers "0.2.0") (deps (list (crate-dep (name "far_shared") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-crate") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pb6v0593p09lyhjzj368ajm6bq65riqmj4y4zyi785v4an230fc")))

(define-public crate-far_macros-0.2 (crate (name "far_macros") (vers "0.2.1") (deps (list (crate-dep (name "far_shared") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-crate") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f6hih83qnb7azzk5jgc7k42jldj4971q1ndzdql9hk1b56vbg5m")))

(define-public crate-far_shared-0.2 (crate (name "far_shared") (vers "0.2.0-beta.0") (hash "0g0yfbvybn6syb2n84myvl4hmgf4idqfm4g6rbwz5ky2ksn9p694")))

(define-public crate-far_shared-0.2 (crate (name "far_shared") (vers "0.2.0") (hash "0dd7kz00qa1nva7wap6nxirbp9cii59dfc3cci4v76061w2jwxrn")))

