(define-module (crates-io fa tt) #:use-module (crates-io))

(define-public crate-fattureincloud-rs-2 (crate (name "fattureincloud-rs") (vers "2.0.29") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.188") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0jl96n9p4qrx2wz6a7pj6ql8cv5jpna98i4a50bijvq5rd7s1hq2")))

(define-public crate-fattureincloud-rs-2 (crate (name "fattureincloud-rs") (vers "2.0.32") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^3") (features (quote ("base64" "std" "macros"))) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1a2npc5xn71jaa54kgg4xw53qsr96s2rp4pg97af1yhl122yji8h")))

(define-public crate-fatty_scheduler-0.1 (crate (name "fatty_scheduler") (vers "0.1.1") (hash "1dmd5ddarihdhx61n066xypmvvjkl8w93v7h82g1w0803dzsi3a5")))

(define-public crate-fatty_scheduler-0.1 (crate (name "fatty_scheduler") (vers "0.1.2") (hash "0x0v1w4sw9y74awvq37sx39w5z8qvii0s0kjmr13x8v3dra8ljzi")))

(define-public crate-fatty_scheduler-0.1 (crate (name "fatty_scheduler") (vers "0.1.3") (hash "15hx73iayy5dfnafsykm5lv1snnlxkklg98kcqw33hgvkqyp5zcm")))

(define-public crate-fatty_scheduler-0.1 (crate (name "fatty_scheduler") (vers "0.1.4") (hash "02cwy27cwxfv5xsmjv398hqlfyc911w2m9v2gmmxljbmwn6v92jb")))

