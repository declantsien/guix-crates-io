(define-module (crates-io fa la) #:use-module (crates-io))

(define-public crate-falafel-0.0.0 (crate (name "falafel") (vers "0.0.0") (hash "1xyq3s0pfza40y2ralc2izdalim0xbnj5bxzj29ri070k7gqi9f0")))

(define-public crate-falafel-0.0.0 (crate (name "falafel") (vers "0.0.0+amqp-suppurt") (deps (list (crate-dep (name "amiquip") (req "^0.4") (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)))) (hash "0gnprl0sdvm96s1qhhn0hpmdwjnwzqr2f7cgirpljyz6yp76kdkp")))

(define-public crate-falafel-0.1 (crate (name "falafel") (vers "0.1.0+amqp-suppurt") (deps (list (crate-dep (name "amiquip") (req "^0.4") (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)))) (hash "1pzrkvlsaqd4rs9rbv7zfh4wf87i2i732nv7szymwvbhlxvk5nci")))

