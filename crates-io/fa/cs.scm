(define-module (crates-io fa cs) #:use-module (crates-io))

(define-public crate-facsimile-0.0.1 (crate (name "facsimile") (vers "0.0.1") (hash "0y92v2dhnc5wzyasp3vsj8mh7rghml1m19d1wvwz834nl2nr0rym")))

(define-public crate-facsimile-0.0.2 (crate (name "facsimile") (vers "0.0.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.2") (features (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (kind 0)))) (hash "193m74fxbk05q7akl6yhzxpwrkvh93ax689wa8jzi4kqn40gmx0h") (features (quote (("gen") ("det") ("default" "det" "gen" "crp") ("crp"))))))

(define-public crate-facsimile-0.0.3 (crate (name "facsimile") (vers "0.0.3") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.2") (features (quote ("gif" "jpeg" "ico" "png" "pnm" "tga" "tiff" "webp" "bmp" "hdr" "dxt" "dds" "farbfeld"))) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.80") (optional #t) (default-features #t) (kind 0)))) (hash "18kcx8n63k0rgbm2f3cy4rmr1cvx2529df8jbrx8948f541s814z") (features (quote (("no_wasm" "det" "gen" "crp") ("gen") ("det") ("default" "wasm") ("crp")))) (v 2) (features2 (quote (("wasm" "dep:wasm-bindgen" "det" "gen" "crp"))))))

