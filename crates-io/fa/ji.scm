(define-module (crates-io fa ji) #:use-module (crates-io))

(define-public crate-fajita-0.0.1 (crate (name "fajita") (vers "0.0.1") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0rlw7ikk52ip447hrrz9fdppv7p4i6jpz2nxcglbw14jff69gnh2")))

