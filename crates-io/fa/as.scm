(define-module (crates-io fa as) #:use-module (crates-io))

(define-public crate-faas-wasm-runtime-0.1 (crate (name "faas-wasm-runtime") (vers "0.1.0") (deps (list (crate-dep (name "cloudevents") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasmtime") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "03a1nvvc9qxlazr8341wrbz9710qvilwjny6b4n1i3a7sx1887ch")))

(define-public crate-faasm-sys-0.0.10 (crate (name "faasm-sys") (vers "0.0.10") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "default-tls"))) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "0cv52q95ng1rd2jhjlgfrszq36rlsvhvy6pxwb4zhhr7vc9nyaf4")))

(define-public crate-faasm-sys-0.0.12 (crate (name "faasm-sys") (vers "0.0.12") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "default-tls"))) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "1frx6q1ar8490cbr17qndd0i0bknnr2hx7r1852h0mv525ryr8v6")))

