(define-module (crates-io fa ve) #:use-module (crates-io))

(define-public crate-favent-0.1 (crate (name "favent") (vers "0.1.0") (hash "192zdyyalm4icv3ai4l5q7ymyibkqrr036ixa9aqi5g0z0pp9ikn") (yanked #t)))

(define-public crate-favero-0.1 (crate (name "favero") (vers "0.1.0") (hash "11fjp19b4d1lmh4fhqj745l0i3hxpv5lr4cga1dh86s0q768yvlw")))

