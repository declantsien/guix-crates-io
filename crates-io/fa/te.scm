(define-module (crates-io fa te) #:use-module (crates-io))

(define-public crate-fate-0.1 (crate (name "fate") (vers "0.1.0") (deps (list (crate-dep (name "dmc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04qwy6v30gpvzm2220qrnga44bzlndgy8q1y04w7dap823h82pv1")))

(define-public crate-fateful-0.1 (crate (name "fateful") (vers "0.1.0") (hash "1jjwfs3m32f12ag0zz7shbpbjf0vy9r0nw0zi42w437a51bfm23y")))

(define-public crate-fateful-0.1 (crate (name "fateful") (vers "0.1.1") (hash "1vrf8wg41x1jzn4bvn32jbf1hqj3ll0hrb2702i9ssc7i6srgrv0")))

(define-public crate-fateful-0.1 (crate (name "fateful") (vers "0.1.2") (hash "15mdwi9v6iicbwvwvq65gr9ykrr58k45zkl9cr9g50kjdhbcnp4f")))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.0") (deps (list (crate-dep (name "fates_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16vd5jhwvyxnhq7isn099806x1jzninx2m5050j7mla42nfd3g6g") (yanked #t)))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.1") (deps (list (crate-dep (name "fates_macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1mqyw1a5g4gc3yadkqm68010nvnphh6263px6rdfv38lqck74295") (yanked #t)))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.2") (deps (list (crate-dep (name "fates_macro") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "03h2wx03xr37k69jqwp0yjs6hmmibp9hdcy7l2ks4qpyhpr7pph6") (yanked #t)))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.3") (deps (list (crate-dep (name "fates_macro") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1xb423d8z4lmlb776xqykmni15fww3m8qalf7kyrpn888sh9d7p2") (yanked #t)))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.4") (deps (list (crate-dep (name "fates_macro") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0s3s7kwa6c72aidwbnp7lf4x1kbkl8i9pdsdprzna6hwladfrihs") (yanked #t)))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.5") (deps (list (crate-dep (name "fates_macro") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1qnxj7h4garw3kaxza9i63ldkn42cb816lbhixan9x946dl70z3c") (yanked #t)))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.6") (deps (list (crate-dep (name "fates_macro") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1kfl3midk5z6snkzpnl7adz3i5qzg4wnabp3fixpwb9jm68w4p8y") (yanked #t)))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.7") (deps (list (crate-dep (name "fates_macro") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "184q9n66cvm6blc27qpv3fnnb6h6xrmsja3i3i8ahw4qim0msa16")))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.8") (deps (list (crate-dep (name "fates_macro") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0y5nan2fc15zc7zlx4g96qapnf0g7xb8c9fqkd794vg8g9jnvcci")))

(define-public crate-fates-0.1 (crate (name "fates") (vers "0.1.9") (deps (list (crate-dep (name "fates_macro") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "05c1j9apl4c0ap2p2xh064bbmnrl8pbamzb29fm3ayag4d7095gw")))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16m1afl4k92m8537injik1afyjj6gs27hrs082vrr7ch8c426sq0") (yanked #t)))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full" "fold" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1i7qdiqv8zx8flcnp416ysj8yvfdq4vnn8gfqfl6hz9ga4zmz5i3") (yanked #t)))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full" "fold" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0babgmwa6z5zyxm4z6yrv3cd0d2yzxvknd7c2y883h270xm9jids") (yanked #t)))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full" "fold" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1i9kjd2byhbh21rzqn39zdz9wds81k6favycy5h5k3ynnxb1q9bq") (yanked #t)))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full" "fold" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "175q16hjwhmcvhncsxf0dfxyglyz6y34a2mqxhyvlnqcxrsbgyy1") (yanked #t)))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full" "fold" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "17hz1m6mg2ww25dfqxjhb6rybhag82psw1x7fcyvdcklm0srdzq6")))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full" "fold" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1ch76v6817ylavwqh6jh0k5ssi72426w0gk4nrl9ks6fzk868fsm")))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0h8v7xanvj8v8rs1xwhs1pal3zr2rdi3si39r1ps1awa9c8nndcg")))

(define-public crate-fates_macro-0.1 (crate (name "fates_macro") (vers "0.1.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1ib2lxwrmdp2m59lixy5xdi4pq9dw06ggi6y5402240bhwnx21zj")))

