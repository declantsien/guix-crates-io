(define-module (crates-io fa ex) #:use-module (crates-io))

(define-public crate-faex-0.1 (crate (name "faex") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "058177q3219bln08cz359dsxvqim4pmkcs1sv4dqky63cscsyvjc")))

(define-public crate-faex-0.1 (crate (name "faex") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "178xnbpl7bj8zjkww6lqlrzkmkf2ynbk44ww0z080r12f00404qj")))

(define-public crate-faex-0.1 (crate (name "faex") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xnbl3vivd9mgmq3yb7sc271sm4mjrz4k7zxf8vkxgkhxspwfqj8")))

