(define-module (crates-io fa mi) #:use-module (crates-io))

(define-public crate-family-0.1 (crate (name "family") (vers "0.1.0") (hash "0vafyhlmcmfp5prd8rzjdvfkzsmmjzbjbx65c241xm4f74s5wbzg") (yanked #t)))

(define-public crate-family-0.1 (crate (name "family") (vers "0.1.1") (hash "1ds53488x0s12m9qjr1vyi41ddmw9msrcsapql2kr0xw1hyqwba3")))

(define-public crate-family-0.1 (crate (name "family") (vers "0.1.2") (deps (list (crate-dep (name "family-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14rkfg9ylbf18v3dmm1yzy8nnks6iv0blx9p15437xdn3a2z93w7")))

(define-public crate-family-derive-0.1 (crate (name "family-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (default-features #t) (kind 0)))) (hash "1qzwqphwjfdqh8i4fwy2ncfm25rkrv17jhhc87vd2d84k5g672vy")))

(define-public crate-family-derive-0.1 (crate (name "family-derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (default-features #t) (kind 0)))) (hash "0jm1dbg1vpbv3q622mybyankvhiff0ymd29gxf8c6fbv7swsff81")))

