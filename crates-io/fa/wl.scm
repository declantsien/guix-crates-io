(define-module (crates-io fa wl) #:use-module (crates-io))

(define-public crate-fawlty-0.0.0 (crate (name "fawlty") (vers "0.0.0") (deps (list (crate-dep (name "dashmap") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "hash_hasher") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)))) (hash "0wvgmpsz41m52363nvqmjybcvvfr5j24b5g3dj66przbc2gbr6hs")))

