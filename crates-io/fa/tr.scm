(define-module (crates-io fa tr) #:use-module (crates-io))

(define-public crate-fatr-0.2 (crate (name "fatr") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "158l42gglhb147lhxfniyg804zrk98bbmhqh10158nhf1nl9666f")))

