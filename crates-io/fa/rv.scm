(define-module (crates-io fa rv) #:use-module (crates-io))

(define-public crate-farve-0.1 (crate (name "farve") (vers "0.1.0") (deps (list (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1qbnv5lmwfgxyrwxmjrbfx4dmfmjkk38cx7n3m2sa8ra65n5yg20")))

(define-public crate-farve-0.1 (crate (name "farve") (vers "0.1.1") (deps (list (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "09shcx7gm0yas8kn1zvbhc6pxsrij316jzmzn2bcl8wdkkqpabfh")))

(define-public crate-farve-0.1 (crate (name "farve") (vers "0.1.2") (deps (list (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0gz2cs3567zh2psmylv37ddv72kw7ajyx9jrg5bnldsb3i2k7mac")))

(define-public crate-farve-0.1 (crate (name "farve") (vers "0.1.3") (deps (list (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1cxxmi82s5g1ssdrp4xryx4q9r9nx90s755s32qflnikh8mll548")))

(define-public crate-farver-3 (crate (name "farver") (vers "3.1.0") (deps (list (crate-dep (name "bevy") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.8.1") (features (quote ("bevy_render"))) (default-features #t) (kind 2)) (crate-dep (name "palette") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)))) (hash "1saz88w005h1j2i7c1jbazz08ncfm9q0fdbkc5ijxcaak1frnpv4")))

