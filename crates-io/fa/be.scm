(define-module (crates-io fa be) #:use-module (crates-io))

(define-public crate-faber-0.1 (crate (name "faber") (vers "0.1.0") (deps (list (crate-dep (name "libic") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xmltojson") (req "^0") (default-features #t) (kind 0)))) (hash "0fzp6hcaygwy5a5glygn81fp44arnrp21zrlgqmyswsmdlb5xr9q")))

