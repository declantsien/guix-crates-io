(define-module (crates-io fa t-) #:use-module (crates-io))

(define-public crate-fat-date-time-0.1 (crate (name "fat-date-time") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)))) (hash "0sl7681n29ai3rbbdincjyvfsl0lblwq6f2lslmpalygdb1wxwgl")))

(define-public crate-fat-macho-0.1 (crate (name "fat-macho") (vers "0.1.0") (deps (list (crate-dep (name "goblin") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1k290226q66a5qj50nx0j6idq61fypkmgd9a6q1y6x2rkip9jjwa")))

(define-public crate-fat-macho-0.1 (crate (name "fat-macho") (vers "0.1.1") (deps (list (crate-dep (name "goblin") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "116642730wzz48gb4dxir7m77xyy3ax4s9cf71xhg8yidfc70624")))

(define-public crate-fat-macho-0.1 (crate (name "fat-macho") (vers "0.1.2") (deps (list (crate-dep (name "goblin") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0v1qdn9c23rlgy3qfhv196rjysi4hjjlncxrmflzb7n7mc18qan2")))

(define-public crate-fat-macho-0.2 (crate (name "fat-macho") (vers "0.2.0") (deps (list (crate-dep (name "goblin") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1v20w6jk5vr7jj61f386g8ii5rxb389cpn7xhrdmkh4ql995sxpj")))

(define-public crate-fat-macho-0.3 (crate (name "fat-macho") (vers "0.3.0") (deps (list (crate-dep (name "goblin") (req "^0.3") (default-features #t) (kind 0)))) (hash "0z2zsp1m69jaardrdsx7hml3f3pfmzq17ajlq7g99mdii0dxdrk0")))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.0") (deps (list (crate-dep (name "goblin") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ksac1g4ghgrvlj0g6l40nim95pps2yj7iyjyysz62aacjwvzc1p")))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.1") (deps (list (crate-dep (name "goblin") (req "^0.3") (default-features #t) (kind 0)))) (hash "0x8439m6np1hab6ig6906y6x5fr2jlm5p1g6rwm6w4nygsvrdfg9")))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.2") (deps (list (crate-dep (name "goblin") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "llvm-bitcode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dhr0fd43jwrkjwrd2gcpgr5zcyad7q4z67rjlm0583ssglg6hrm")))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.3") (deps (list (crate-dep (name "goblin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "llvm-bitcode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1yi4lhrmzff60w82g1y6c1icccil0dvg2y50p990rn63ygs1xskw")))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.4") (deps (list (crate-dep (name "goblin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "llvm-bitcode") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "14bvgdg8dlvksjyx6v4p3y2qkn4z5jav9bj8drb602a7ghv3i8vj") (features (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.5") (deps (list (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "llvm-bitcode") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1w5hd1ma3cpai2kcjrg1jh0ksbz23cy8bqlbmis1fwcjraw1wgls") (features (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.6") (deps (list (crate-dep (name "goblin") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "llvm-bitcode") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0jfib3z5vzs45f6lfm2lvw8rq17g01310n3a5fsc4i5rl8qp3w37") (features (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.7") (deps (list (crate-dep (name "goblin") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "llvm-bitcode") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ywr3xqi884i12d5mfidbma1hrd4rxj9f8jw7p4bignagmy13yk3") (features (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

(define-public crate-fat-macho-0.4 (crate (name "fat-macho") (vers "0.4.8") (deps (list (crate-dep (name "goblin") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "llvm-bitcode") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1pqsjf13pdbhki2sdh70575hwqd18gm3vp8hpir3vl5djgrr6k0d") (features (quote (("default" "bitcode") ("bitcode" "llvm-bitcode"))))))

