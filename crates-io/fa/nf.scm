(define-module (crates-io fa nf) #:use-module (crates-io))

(define-public crate-fanfic-0.0.0 (crate (name "fanfic") (vers "0.0.0") (hash "0da0hy3njf2zy0f9kd6vd0d96al2vwxx0pg66gbvi5p807lgl98j")))

(define-public crate-fanfic-macros-0.0.0 (crate (name "fanfic-macros") (vers "0.0.0") (hash "1hvxvn62w4krxq9zgqfm03drxngbcch23g8jdczhasmvhvg9q6n1")))

(define-public crate-fanfou-0.1 (crate (name "fanfou") (vers "0.1.0") (hash "09rrp5d188s8axfj1lsx6niwl4kcz28qg8vh17vzhzf2bgsjs1wp")))

(define-public crate-fanfou-types-0.1 (crate (name "fanfou-types") (vers "0.1.0") (hash "0k2m3gqr5xlqwlnmbn4sg1l5aj33idd8qwlm5aqcly3b595mcih9")))

