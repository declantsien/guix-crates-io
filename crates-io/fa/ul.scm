(define-module (crates-io fa ul) #:use-module (crates-io))

(define-public crate-fault-injection-0.1 (crate (name "fault-injection") (vers "0.1.0") (hash "1g6ik8cn6iqaz8mdncxdv524fkiqykwmx09z7r610n2m5c5knf49")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.0") (hash "1xx38vwh8mncbhxir5w2zyscbg55i9l42wpac6hx1nnkjzapzcdr")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.1") (hash "0ksawgiznbppc7llwzpkinmfrxclpz709j3wg5ib017vnsn0wgh5")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.2") (hash "1a1y8s4bb2zshm15ahdlrpsblqr34bmh1yf8d0mhg7bdakhcrlam")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.3") (hash "1aq6bsbphfxaan6pirx4d6rcrxp8nlb2hqkggn00w1sxl25sq9i2")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.4") (hash "1jdxbp8vash1k5020sv9wamwan6b45fz4dqd9m91p0aa3x2s98gj")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.5") (hash "0cq687mhpwq0pg7i32yc3hdrcx783r5i6k48gs56102m2701m3jw") (yanked #t)))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.6") (hash "0hqvcd4n7sj4aj4f1h6bbjpp0qlppmzy8psnj98w1bsp2dxsgv7j") (yanked #t)))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.7") (hash "12rc9mmfwhyxi183x3hg1584lsm6mk9yfra3aw8wlrdjhmapzjmf")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.8") (hash "178v3kq16709rf64r4v7dlixrxgpmnqfmsyvl1xkd84k1c43q5x9")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.9") (hash "19q9nfs6szis14wg5z5f4jsq0cd58mriy9nii6gydbc77gq32378")))

(define-public crate-fault-injection-1 (crate (name "fault-injection") (vers "1.0.10") (hash "1lzysyins92zrknfjcnhcb2y88xhrmxdbz5iygggvhyy8r91fgcy")))

(define-public crate-faulty-0.0.0 (crate (name "faulty") (vers "0.0.0") (hash "0pq7vvz1l4dggdgnv1bfysml1vsys6yqdsm5y856rgm4imjnhcj8")))

