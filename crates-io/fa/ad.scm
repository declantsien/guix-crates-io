(define-module (crates-io fa ad) #:use-module (crates-io))

(define-public crate-faad2-0.1 (crate (name "faad2") (vers "0.1.0") (deps (list (crate-dep (name "faad2-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qplcshx4ji81dlv3dddq8a89xkilqr7spjf2cymq4dpp7svdizk")))

(define-public crate-faad2-sys-0.1 (crate (name "faad2-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1p1fywshdaj0i58qz8czsy38n672dycdryjbh9yfnpjvh9rfdj34")))

