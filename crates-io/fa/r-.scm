(define-module (crates-io fa r-) #:use-module (crates-io))

(define-public crate-far-rs-0.1 (crate (name "far-rs") (vers "0.1.0") (deps (list (crate-dep (name "libfar") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1vp17rpf5vja9jdhxxvjgvv0cr84bmwwcncq2hhd28kl11h1a9s8")))

