(define-module (crates-io fa to) #:use-module (crates-io))

(define-public crate-fator-semiprime-0.1 (crate (name "fator-semiprime") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0k0l5qgql36wj658k95qlaxls53vkgmgirwx9a7mvifcf3gm8a9j") (yanked #t)))

