(define-module (crates-io fa po) #:use-module (crates-io))

(define-public crate-fapolicy-rules-0.4 (crate (name "fapolicy-rules") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y8dhzdhpq5f4p0bin3gbs3x3pgb4lprzspc5fcc8dw5g2h70gy8")))

(define-public crate-fapolicy-rules-0.4 (crate (name "fapolicy-rules") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0slm828il5b46438qhj1180s43vw25mvz991a7d9vb61ypgrhx7z")))

