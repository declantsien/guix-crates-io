(define-module (crates-io fa ny) #:use-module (crates-io))

(define-public crate-fanyi-0.1 (crate (name "fanyi") (vers "0.1.0") (hash "09izhr4xlraplfz5k58nlmplp3x50mljsbk1r0fywa54k546z2j1")))

(define-public crate-fanyi-rs-0.1 (crate (name "fanyi-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0m7vw13w5n4lkq39bl3kc0hn3zaazjkyp0gwfxv7wfxhl1rfdgks")))

