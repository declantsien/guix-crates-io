(define-module (crates-io fa t_) #:use-module (crates-io))

(define-public crate-fat_fs_types-0.1 (crate (name "fat_fs_types") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (features (quote ("min_const_generics" "derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.31") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1nxaqdvjk4s5dhkzbng64b3zrmsjc4i59sgpfqd788rbgbq2hmp2") (features (quote (("std" "alloc") ("alloc")))) (rust-version "1.73.0")))

(define-public crate-fat_fs_types-0.1 (crate (name "fat_fs_types") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (features (quote ("min_const_generics" "derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.31") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0rw0lifx0ffzs8417lphg486lsfqr31333n86qamhaaqb2gin2ml") (features (quote (("std" "alloc") ("alloc")))) (rust-version "1.73.0")))

(define-public crate-fat_fs_types-0.2 (crate (name "fat_fs_types") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (features (quote ("min_const_generics" "derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.31") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0c8rgnchcgjjh0s3bdxm4yr9kzxqz0flw1gk10n8pd3r1ybzmjj3") (features (quote (("std" "alloc") ("alloc")))) (yanked #t) (rust-version "1.73.0")))

(define-public crate-fat_fs_types-0.2 (crate (name "fat_fs_types") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (features (quote ("min_const_generics" "derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.31") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1l6agrk5rlvsqz070q4q4hwv4s3lqw3j88gh9s98w8zfark4ziyl") (features (quote (("std" "alloc") ("alloc")))) (rust-version "1.73.0")))

(define-public crate-fat_fs_types-0.3 (crate (name "fat_fs_types") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14.0") (features (quote ("min_const_generics" "derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.31") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1czjd6955k1jjvmk83pa8jrnbj3n8ni06r91sd268ih931dfy363") (features (quote (("std")))) (rust-version "1.73.0")))

(define-public crate-fat_type-0.1 (crate (name "fat_type") (vers "0.1.0") (hash "0pba2id42vsr7v9clj4npzsg6ffpvpg4f04x2qns1gsga8v8bqfg")))

(define-public crate-fat_type-0.2 (crate (name "fat_type") (vers "0.2.0") (hash "1469qg5j6drwc6zmzs4dh76zhm7g28v8j9rq8v0rxl0jaz5m4qff")))

(define-public crate-fat_type-0.3 (crate (name "fat_type") (vers "0.3.0") (hash "1rm4yly6yijzpp02ip62n7whf8fqfdymc6gipcq93w6mq9l24dkn")))

