(define-module (crates-io fa im) #:use-module (crates-io))

(define-public crate-faimm-0.1 (crate (name "faimm") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.6") (default-features #t) (kind 0)))) (hash "1sc73b547c5kim13ayfyv2bm58irhkhr21znw9a3srjf780aq326")))

(define-public crate-faimm-0.2 (crate (name "faimm") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0sw2sxwb4d2416iscl2rxr2nwqajj31g5yzkisqi4xg4mzcg146f")))

(define-public crate-faimm-0.2 (crate (name "faimm") (vers "0.2.1") (deps (list (crate-dep (name "indexmap") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0rzbg4m1kmhf10b4kb4z3cljsng12jm596hl5yfpampp6anqn5jz")))

(define-public crate-faimm-0.3 (crate (name "faimm") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kfq7zy9wvhri7vdlv7i2k4ghspznkfqy9w0lnqbc84d8gxjpbm3")))

(define-public crate-faimm-0.4 (crate (name "faimm") (vers "0.4.0") (deps (list (crate-dep (name "indexmap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.7") (default-features #t) (kind 0)))) (hash "0vzblbcyf28dmx4ihcx49j7y4nmdzzknb88bg4ny8wd70facx9ss")))

(define-public crate-faimm-0.5 (crate (name "faimm") (vers "0.5.0") (deps (list (crate-dep (name "indexmap") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "1idh4rvk62byf7m4k8zx36b6gf104wsvdww20fpp0ac14mfkjbii")))

