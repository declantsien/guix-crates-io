(define-module (crates-io fa cl) #:use-module (crates-io))

(define-public crate-faclair-0.1 (crate (name "faclair") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1gmxx9y8h1fynwsiwdh2wzdl8s93if4ibp08m0pjrfvbnkxv7prx")))

