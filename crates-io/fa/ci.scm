(define-module (crates-io fa ci) #:use-module (crates-io))

(define-public crate-facilitest-0.1 (crate (name "facilitest") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "06q9p6gcyj87cc3pz3z2srsbhvsawx3zw4n3vhqwg5iybhhx5vr1") (yanked #t)))

(define-public crate-facilitest-0.1 (crate (name "facilitest") (vers "0.1.1") (deps (list (crate-dep (name "facilitest-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18hq6gm50pkmk2zkppdlpfc49dd44axl2lnj6qwj9g5dfbbxmc0d") (yanked #t)))

(define-public crate-facilitest-0.1 (crate (name "facilitest") (vers "0.1.2") (deps (list (crate-dep (name "facilitest-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10lr3psdk2mlpcrkks11fyysj20v5d0flj2q63sbyyk7alxi15dn") (yanked #t)))

(define-public crate-facilitest-1 (crate (name "facilitest") (vers "1.0.0") (deps (list (crate-dep (name "facilitest-macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0l7svsijp8861z4448iqfyl0qizm4c4kn4jwcyc7zlwv1j9avyr1")))

(define-public crate-facilitest-macros-0.1 (crate (name "facilitest-macros") (vers "0.1.0") (hash "1qhpn5w821ggkbfq7zb40mqih36wns7967lf6fd68ygsjs0wzv6z") (yanked #t)))

(define-public crate-facilitest-macros-1 (crate (name "facilitest-macros") (vers "1.0.0") (hash "17dyp5pvaxjhcdvpszg8yxfpa3bdfanh1h34zxw86yhfjszmpcl0")))

(define-public crate-facio-0.1 (crate (name "facio") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "00mcb32jgv4gw8fl5fwv45hr5f3drf44xk9ivrh6bzxg2c78s8n4")))

