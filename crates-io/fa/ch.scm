(define-module (crates-io fa ch) #:use-module (crates-io))

(define-public crate-fache-0.1 (crate (name "fache") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)))) (hash "1z4pbkza9rjqxmzpf9jx0mhz59qyg3a618yvsm9b8r4lybh1s7zn")))

(define-public crate-fache-0.1 (crate (name "fache") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)))) (hash "1xnw65gm95f3vapf0i7ngmkhc9i41mk9f5cfl9xx3qfqifpx5m4p")))

(define-public crate-fache-0.1 (crate (name "fache") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)))) (hash "10is1cfx0bizlcihykj0bcqwlk2ljzv25nmgrp9lp992v0mvd3qq")))

(define-public crate-fache-0.1 (crate (name "fache") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)))) (hash "0g5c959b65dddn809a8k4fc99bw8nwrc1628qjp45z862fax1z56")))

(define-public crate-fache-0.1 (crate (name "fache") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "07qj4qcbal6r7q1a893sza8ip7n458501a1298fz7ci64a0rxslc")))

(define-public crate-fache-0.1 (crate (name "fache") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1a62czsg42ga9kc7hmsn5gs8ycybj670a4j2ykrki5z8cw04b0h5")))

(define-public crate-fache-0.1 (crate (name "fache") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.19.14") (default-features #t) (kind 0)))) (hash "0498y32pmjxi4vkg155x8lcpkkawhfkcsf6cc7r4878bmpyr6pa2")))

