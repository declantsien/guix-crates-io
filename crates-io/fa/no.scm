(define-module (crates-io fa no) #:use-module (crates-io))

(define-public crate-fano-0.1 (crate (name "fano") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "05ns8qa7999jdsrlqfylz6y4xim1is0xakczpm42fwv7wh1y7ps2")))

(define-public crate-fano-0.1 (crate (name "fano") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1cwk06ghp129imkpqm15kph52mriiqkvkd7bhdd0s4dzr4ckvxdq")))

(define-public crate-fanotify-0.1 (crate (name "fanotify") (vers "0.1.0") (hash "029wqiidbgjycpx0f4j9awiyn68w88fkb911hyaz18bqap3s26s6")))

(define-public crate-fanotify-rs-0.1 (crate (name "fanotify-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wndxpf7llv6i45zby6547scg89cgfbw3vq52351gfsnk517n0a7") (yanked #t)))

(define-public crate-fanotify-rs-0.1 (crate (name "fanotify-rs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12wslc4l4zzjf34w65bdf1bd6dyi3y7agjzrzhrn5zibqqwnsazz") (yanked #t)))

(define-public crate-fanotify-rs-0.1 (crate (name "fanotify-rs") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d79sn2xm2pjqqaxvk3y218f218nyg9zbl4a4y8ppi0wjrbp0nc0") (yanked #t)))

(define-public crate-fanotify-rs-0.1 (crate (name "fanotify-rs") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "09zawi1v72jf010z78v8dx81vryhxxq42xm81jl2k2h7acj9zzph") (yanked #t)))

(define-public crate-fanotify-rs-0.1 (crate (name "fanotify-rs") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0prbvv5n9579bw18h4sl4ldd3c5ncpzxjwb5547zkz1z1la3mr2p") (yanked #t)))

(define-public crate-fanotify-rs-0.1 (crate (name "fanotify-rs") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "10j0rm6in9g7v7jd70xd7571srhiqfsjl26w5p6qynjqyk3yq9a8") (yanked #t)))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sr1n58j1p6j9k0dxi8q813rakyng7kphy49f3j8za1li5p5m4m6") (yanked #t)))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bbmq2mv4xf29al595nrnzx0g7hjnzy94kw79d7k8whsl1xwhryk") (yanked #t)))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18cyqmklf22lsq85a7yfijajfcwnr8xf7k6zmgkjlvspgf0bq6lx") (yanked #t)))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "087ypbj2fxhxw40iq4bj9wkwsbabsbfdymi0qmk6l4bggqpmc1x6") (yanked #t)))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0js6k18g135ifhqwxacpv5ivadm8r5rr25kwpbwdwn7jiq6lifgx") (yanked #t)))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0di5fph4vx76c6m18jm3bgcfgq3hzgbldr931cxfmkmqlj8hgd9l") (yanked #t)))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "01qs711xwk51raqfcaydc7d3dlmsb28s6vpsn686as5kyby2rbm6")))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.8") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v586n1f3vcm20zzw5mbw5df7dk6i4ynr01r755djqji2kabzj69")))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.9") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v58yjrwcnw64fdr0pq490yx4mb109pxrzndixbwg1a8bkc3q5bs")))

(define-public crate-fanotify-rs-0.2 (crate (name "fanotify-rs") (vers "0.2.10") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qlk0mjkkfa568harhafyg0m1ny4n1n089zri02qxlqd2r4x3z0c")))

(define-public crate-fanotify-rs-0.3 (crate (name "fanotify-rs") (vers "0.3.1-rc1") (deps (list (crate-dep (name "enum-iterator") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j7vfbqxniysk7lvkp5ynrxj12p3wlqncbdgkfghmwmz8rsfh5w4")))

(define-public crate-fanotify-rs-0.3 (crate (name "fanotify-rs") (vers "0.3.1-rc2") (deps (list (crate-dep (name "enum-iterator") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r3mvc4i9ya01r39sh14yrfj8pjwyjjla3f7n087ww3j81yffkrg")))

(define-public crate-fanotify-rs-0.3 (crate (name "fanotify-rs") (vers "0.3.1-rc3") (deps (list (crate-dep (name "enum-iterator") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "10pxlkf7dkjllzcmwag5kjmgmr3897i9fmspjn8ikqks9pbax4bm")))

(define-public crate-fanotify-rs-0.3 (crate (name "fanotify-rs") (vers "0.3.1") (deps (list (crate-dep (name "enum-iterator") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s5zpdgc88iv601x569nhrg1bj57jcx6chhnvj7m3bpksqzmyln9")))

(define-public crate-fanova-0.1 (crate (name "fanova") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0n43axzqzsp2k019k8yfgavss2k16xq0smyh80pvvi3fkphg354c")))

(define-public crate-fanova-0.1 (crate (name "fanova") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "057fc7sggwj0pf6y1sz3rgnm6v9zbk7rdlkalwbrdnj8lyx27l1k")))

(define-public crate-fanova-0.2 (crate (name "fanova") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "02ym1lnnn2b1m8wkpr5kqcg4xs82mmwmhvxm3h76gki9s2c9fzy3")))

