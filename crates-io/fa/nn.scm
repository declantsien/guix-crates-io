(define-module (crates-io fa nn) #:use-module (crates-io))

(define-public crate-fann-0.0.1 (crate (name "fann") (vers "0.0.1") (deps (list (crate-dep (name "fann-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1kmh9fzxd261am1hh512d1sydwhklyrfdp50znknwainwli16yqx")))

(define-public crate-fann-0.0.2 (crate (name "fann") (vers "0.0.2") (deps (list (crate-dep (name "fann-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1h25zj0swslpwvr64id33y0nmjai8h7r768wcc4pg32bbpb0p9jw")))

(define-public crate-fann-0.0.3 (crate (name "fann") (vers "0.0.3") (deps (list (crate-dep (name "fann-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0z9l7zmj7ww8vwjywl29kqz8pwdgw5v7drbj19bvbmb73k4l35sq")))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.0") (deps (list (crate-dep (name "fann-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0irgp8gsifqd0mcmzil52588nagk7kfvh667lmiqi93r2l6l4rvc")))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.1") (deps (list (crate-dep (name "fann-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "003y0vhz16xlxlvpf6g8l7qlcjngamahk0b22ybfjh4wx1jwji4k")))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.2") (deps (list (crate-dep (name "fann-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0n8z7pg9wzj8vg5vm1wg2psn8d082a3fq5l5c4fzyp4mvqwkckpq")))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.3") (deps (list (crate-dep (name "fann-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "13kln8mj261gdgg769cng0f6amgw4kby61qc19mj5r9xaj3v4p97") (features (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.5") (deps (list (crate-dep (name "fann-sys") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1yx7iaf7hwdhiwmyryy5dw1zb0wnxqk9bm6l9mzivpqx1dx6bcbl") (features (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.6") (deps (list (crate-dep (name "fann-sys") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0imx4pbqnbhp7hnnk35wrp83b45v2q085gxdzn4ack8kjah262pi") (features (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.7") (deps (list (crate-dep (name "fann-sys") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.20") (default-features #t) (kind 0)))) (hash "1dhwql727n3wlcp7khwik77q4sgx2rir57q94n566gw0wcyjwk2h") (features (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-0.1 (crate (name "fann") (vers "0.1.8") (deps (list (crate-dep (name "fann-sys") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.20") (default-features #t) (kind 0)))) (hash "0nrkixmq10q8mypqliv3q1xpiqjmf9gilfpapjs0aa2xpgwgf9rh") (features (quote (("double" "fann-sys/double"))))))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0zb1ryzs2qj68vd8m6xf396374mrwwzz8l4d6a58bpch34rv4nbv")))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0dyi1xrz49z3lx856khkkryxb5fkkhp5zl2d0qjhl0glvjg4bm3g")))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "128fz1x4kkpqaad0rf968v819qwcl0fd9cd87fxq309whgf5k4x0")))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1x6fy2gi42gdmnqql9d3fdz9myi2cm6ixsl4hv7ckadvmiq4q7rh")))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0nm2ckd3r6iyg9irfgng07fn8smik8wy7kk693brxgd6va6r0s85") (features (quote (("double"))))))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)))) (hash "02ijp3jfbkhc5gcg4zgk67gi6baarlrqg0h3mbc1y2ccl5r9fynk") (features (quote (("double"))))))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0r3ifv3damjjbnrc0vmwyqsg77akg2k22ckp3gpwzfp3v6z2n4yg") (features (quote (("double"))))))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "~0.2.20") (default-features #t) (kind 0)))) (hash "1byj3g5iy7q3glffp2zfzwihazc3xppk1zfvsbp9zwl5y9642yab") (features (quote (("double"))))))

(define-public crate-fann-sys-0.1 (crate (name "fann-sys") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "~0.2.20") (default-features #t) (kind 0)))) (hash "1v9rvslssqbk7xzyi6zqy2b2xaxsrisjiiy9pjdm73dx58fhnxrz") (features (quote (("double"))))))

