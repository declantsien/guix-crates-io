(define-module (crates-io fa ra) #:use-module (crates-io))

(define-public crate-faraday-0.0.0 (crate (name "faraday") (vers "0.0.0-pre") (hash "0zg8avlhvnmsq9f4ijgkqnyqiyvhx7x0slfp09cq5w9k47kynhr9") (yanked #t)))

(define-public crate-farahfinn_guessing_game-0.1 (crate (name "farahfinn_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0k3bxia0sha80zxrg8wkhc4659w3cnq5z2qi476i9fzna7s64j48")))

