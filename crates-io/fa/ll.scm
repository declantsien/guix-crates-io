(define-module (crates-io fa ll) #:use-module (crates-io))

(define-public crate-fall-0.1 (crate (name "fall") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "frunk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.16.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1x9752k3zmvmsmxczykp8rwqcjd2hl9mcai2vjhjyi99xbygf1yx")))

(define-public crate-fall-0.1 (crate (name "fall") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "frunk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.16.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0mrzcfqavddiwcvr98fpacrwhx2gdzrjzwrsrh9ffj6ypv4my6xd")))

(define-public crate-fall-0.1 (crate (name "fall") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "frunk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.16.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1vha8j5v06wv5gyf9xkvb3yz09b4hljjwzzpzaqvz44fqp5caxjl")))

(define-public crate-fallacy-0.0.0 (crate (name "fallacy") (vers "0.0.0") (hash "0vk7an381i5dkalwml3k3cmlqxnj5qqgmkybqrayrydms3kwcjz7")))

(define-public crate-fallacy-alloc-0.1 (crate (name "fallacy-alloc") (vers "0.1.0") (deps (list (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 1)))) (hash "047acpb9g9rcy52s3xcg8hl4427m9wcinlz4laknf8f14k7va8ay") (rust-version "1.57")))

(define-public crate-fallacy-arc-0.1 (crate (name "fallacy-arc") (vers "0.1.0") (deps (list (crate-dep (name "fallacy-alloc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04w16b5wnn211ym8w7zv8h5gxv8lyb7n3snqhiv4730b6hpl950q") (rust-version "1.57")))

(define-public crate-fallacy-arc-0.1 (crate (name "fallacy-arc") (vers "0.1.1") (deps (list (crate-dep (name "fallacy-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (optional #t) (default-features #t) (kind 0)))) (hash "0rnwp4mj899n1kpbcibhsl45sz9qmn6bpk37kisapr419q9qh3vv") (rust-version "1.57")))

(define-public crate-fallacy-box-0.1 (crate (name "fallacy-box") (vers "0.1.0") (deps (list (crate-dep (name "fallacy-clone") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "00w1pysr08ljf9i6xyb04bdxbciliig8049qgp4rixll3f54ycqh") (rust-version "1.57")))

(define-public crate-fallacy-box-0.1 (crate (name "fallacy-box") (vers "0.1.1") (deps (list (crate-dep (name "fallacy-clone") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (optional #t) (default-features #t) (kind 0)))) (hash "1qxnwl7zlnwy0xxy7d5aj2ygqp0fi6n2i2h52prxd8qmj77hfxci") (rust-version "1.57")))

(define-public crate-fallacy-clone-0.1 (crate (name "fallacy-clone") (vers "0.1.0") (deps (list (crate-dep (name "fallacy-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fallacy-clone-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "10y8phpfq35dmyxq5cnyzj5bhdgpv93cxgd95x7flzzlplld1nvs") (features (quote (("derive" "fallacy-clone-derive")))) (rust-version "1.57")))

(define-public crate-fallacy-clone-0.1 (crate (name "fallacy-clone") (vers "0.1.1") (deps (list (crate-dep (name "fallacy-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fallacy-clone-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 1)))) (hash "1br0czch5srvnbygzzsa7vy4hrqpj32fvkh5x2iv78ss8apnd84m") (features (quote (("derive" "fallacy-clone-derive")))) (rust-version "1.57")))

(define-public crate-fallacy-clone-derive-0.1 (crate (name "fallacy-clone-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "1vx2cwylhvw3l2yyc0rljpfrsk2h3vzdaak52iwdb6kbxcqzarlr") (rust-version "1.57")))

(define-public crate-fallacy-hash-0.1 (crate (name "fallacy-hash") (vers "0.1.0") (deps (list (crate-dep (name "fallacy-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (optional #t) (default-features #t) (kind 0)))) (hash "1fx3n1y7qrs1sp62vcmc53csba4q49xqgaiks9swrcixalljnjqy") (rust-version "1.57")))

(define-public crate-fallback-0.1 (crate (name "fallback") (vers "0.1.0") (deps (list (crate-dep (name "fallback-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1993r36ca1vjr47ka780dsdwvpl2sx5xlgkr0gwm1mnh3j3i3c29")))

(define-public crate-fallback-0.1 (crate (name "fallback") (vers "0.1.1") (deps (list (crate-dep (name "fallback-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fi1wsjgc2ap9van8z9gy0p1jz7p3kmrinlmnd30qdmfna2sm72v")))

(define-public crate-fallback-0.1 (crate (name "fallback") (vers "0.1.2") (deps (list (crate-dep (name "fallback-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09vhjnwpxyxpam6zcynjjqx3krs2l7jh2llyyrj8dzahrzcj386p")))

(define-public crate-fallback-0.1 (crate (name "fallback") (vers "0.1.3") (deps (list (crate-dep (name "fallback-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1h8y7wrsgvm70r6n3m72ris11xyr1kkrrid6h9kgzypfh7my0gf7")))

(define-public crate-fallback-derive-0.1 (crate (name "fallback-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vrii71k9iwipl1csqizzlch92mx99r7cvhyvlrnh1rsvv9fivyk")))

(define-public crate-fallback-derive-0.1 (crate (name "fallback-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i4vqaq2hz9z8b49mbcl9y6xf3p60nqx0dga33mzahnhkcqcmnmc")))

(define-public crate-fallback-derive-0.1 (crate (name "fallback-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0p1ydzagp77wfg9vi53mj1vcxl11a8kmsbg6vsa4iss0x0cg4396")))

(define-public crate-fallback-if-1 (crate (name "fallback-if") (vers "1.0.0") (deps (list (crate-dep (name "yare") (req "^3") (default-features #t) (kind 2)))) (hash "1xpqb3qyhb77xra7zxvj23x832zk5jl0npwjf7jcvymyhsfy6375") (rust-version "1.60")))

(define-public crate-fallback-if-1 (crate (name "fallback-if") (vers "1.0.1") (deps (list (crate-dep (name "yare") (req "^3") (default-features #t) (kind 2)))) (hash "01sq9fqwrk8h57j70pp75m2laj8acp3vwniawdww2by4zpsd5zik") (rust-version "1.60")))

(define-public crate-fallbaq-0.1 (crate (name "fallbaq") (vers "0.1.0") (deps (list (crate-dep (name "actix-files") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "actix-http") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "actix-rt") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "path-dedot") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1ik59x2gsjl9imss8svhkjmd3ykadca7rwkdr407l98f5w8qwbqi")))

(define-public crate-fallible-0.1 (crate (name "fallible") (vers "0.1.0") (deps (list (crate-dep (name "void") (req "^1") (kind 0)))) (hash "0nnr23cfclw9y49cim1zlyjqf0k4d0jp4fvxb8a4myzsxz85xn9i")))

(define-public crate-fallible-0.1 (crate (name "fallible") (vers "0.1.1") (deps (list (crate-dep (name "void") (req "^1") (kind 0)))) (hash "1mr62xvjcy8rgih6w5sfgjz10rmzdpphlqcis2qc4w52sy9s0day")))

(define-public crate-fallible-0.1 (crate (name "fallible") (vers "0.1.2") (deps (list (crate-dep (name "void") (req "^1") (kind 0)))) (hash "074vjhv9v3v1phqipz4kchzxafqjqpn868a43dx50a4yc6ss829x")))

(define-public crate-fallible-0.1 (crate (name "fallible") (vers "0.1.3") (deps (list (crate-dep (name "void") (req "^1") (kind 0)))) (hash "10xjsv1slg0s1xn7vhibhbpsqfrq2axcaxb020f2vli61g9v5z7s")))

(define-public crate-fallible-iterator-0.1 (crate (name "fallible-iterator") (vers "0.1.0") (hash "036lzbrsaf32svh6a7k4p9bnnadv0c2vm41xsh53kqdylsx745m4")))

(define-public crate-fallible-iterator-0.1 (crate (name "fallible-iterator") (vers "0.1.1") (hash "188ndbdxqbmwcs230si0k5vibnryh9z55y41pmbiwyzz3mdnvppw")))

(define-public crate-fallible-iterator-0.1 (crate (name "fallible-iterator") (vers "0.1.2") (hash "0iwy4zla38vn24gs4lkyf9j2ps9z2rxhi3yzshh0d81q62q5wan6")))

(define-public crate-fallible-iterator-0.1 (crate (name "fallible-iterator") (vers "0.1.3") (hash "07gzk5lnn0ynf7xlhjswq158h2r0vk1c436cx0l6c20sq4dsnj2x")))

(define-public crate-fallible-iterator-0.1 (crate (name "fallible-iterator") (vers "0.1.4") (hash "0b84kff2a1gfw9v4wagysa68f1plxq1d4a01f73igr7wknf4l0yn") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.1 (crate (name "fallible-iterator") (vers "0.1.5") (hash "02gmh1rilwbbxah5wwrn5gy9bkc9f7zg2g3nkvqpf39yg9d2jyga") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.2 (crate (name "fallible-iterator") (vers "0.2.1") (hash "1x31skjsynn2h7sq3qzyv4zlyk2w8jmqcs3phsg4qxhz52yj16qx") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-fallible-iterator-0.1 (crate (name "fallible-iterator") (vers "0.1.6") (hash "0bpp2812lxm9fjv082dsy70ggsfg40nhqva7nxr5dp0j9091fwpb") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.2 (crate (name "fallible-iterator") (vers "0.2.0") (hash "1xq759lsr8gqss7hva42azn3whgrbrs2sd9xpn92c5ickxm1fhs4") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-fallible-iterator-0.3 (crate (name "fallible-iterator") (vers "0.3.0") (hash "0ja6l56yka5vn4y4pk6hn88z0bpny7a8k1919aqjzp0j1yhy9k1a") (features (quote (("std" "alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-fallible-option-0.1 (crate (name "fallible-option") (vers "0.1.0") (hash "01yiaqw40n2dx9gvzjpj88k9k5af4432sifqgabab5mm9z0zk94p")))

(define-public crate-fallible-option-0.1 (crate (name "fallible-option") (vers "0.1.1") (hash "04pqj289zna86jkd6rbq6rgvcc6xkg8pd15ahm2p0vkkjsds14fa")))

(define-public crate-fallible-option-0.1 (crate (name "fallible-option") (vers "0.1.2") (hash "0zsg88i4hz2by6s867h9cnq940hpwjl6zhlmffz0kziqc1i4d00w")))

(define-public crate-fallible-option-0.1 (crate (name "fallible-option") (vers "0.1.3") (hash "1gfm7lpnr3px8qkqkh3sdql2nd50fdqakjrvp9vwwrc1bb68fl40")))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.0") (hash "156g52n4b5svsa6m7qyvzc739s3zy7ng4dlq1dg71gsscapy79f3")))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.1") (hash "1jh3jkk477icph252wvl0q0zmbbyd7qmb0hr2nn7xbvi4jkqj5zj") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.2") (hash "02rmzd9ff5bfhx0zsj5v04agn4m94xsyhapm01r743ibdv4kwqfr") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.3") (hash "1mvnkdwla828q42p7hjfx3v5ip4ibx1y2q2lmm9vff32nbny2lfj") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.4") (hash "12s18mlgi5wn4gdf38xxy9qccval4zbnwsb3dmjsl4x3p7llrgfb") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.5") (hash "04zvan922pg4ckhvbmx2skjr6q7v3bqxirh2l1dw5wqplfmslwg0") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.6") (hash "16ys004saja9s1lf36wc9x4wrzyy31l4z9rvwhi5grr2b91bd7nb") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.7") (hash "0djrydqw94qgkrwhd1zhzfsfinwadbrjn27m6di20c8pnq3ws7gx") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.8") (hash "0xi94rg46idbq6psppjijn664z1rkyzwi880z54ak0j93wiiwr75") (features (quote (("std"))))))

(define-public crate-fallible-streaming-iterator-0.1 (crate (name "fallible-streaming-iterator") (vers "0.1.9") (hash "0nj6j26p71bjy8h42x6jahx1hn0ng6mc2miwpgwnp8vnwqf4jq3k") (features (quote (("std"))))))

(define-public crate-fallible_alloc-0.1 (crate (name "fallible_alloc") (vers "0.1.0") (hash "1d5q3qpchgipgfmhl82wdd4p1kvcl8isvwziyjpd973vnqly7gdx")))

(define-public crate-fallible_alloc-0.2 (crate (name "fallible_alloc") (vers "0.2.0") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0kymll4p71i0gx9170dgxds2mkjmilknvgnz163g9z00wpc9dm6h")))

(define-public crate-fallible_collections-0.1 (crate (name "fallible_collections") (vers "0.1.0") (hash "1vj145k492q7wj9y8vn2mvci2zbzppm39lbxb09vis8k7avvgsrf")))

(define-public crate-fallible_collections-0.1 (crate (name "fallible_collections") (vers "0.1.1") (hash "18fmg4fsx2slg8knjkzpfmypyzaymm2p6j0iz0207ywv2y94ms72")))

(define-public crate-fallible_collections-0.1 (crate (name "fallible_collections") (vers "0.1.2") (deps (list (crate-dep (name "hashbrown") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "01h30fx0ifbl0ssld7g6ks3q4fsfmdch752livwkag43bz0h5ayl") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.1 (crate (name "fallible_collections") (vers "0.1.3") (deps (list (crate-dep (name "hashbrown") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0rsa3wnplwi6ibqw7kdn9kwy23v3cbwlpzr6qqczrlslvsy02cxs") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.2 (crate (name "fallible_collections") (vers "0.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "0fwhvnk8f54cg23ksx1pgr33zpv400qfhbpzy0qj6km8ph24vniv") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.3 (crate (name "fallible_collections") (vers "0.3.0") (deps (list (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "05lfchnb9aib2vy84a55277mr718inypbri2ndkgqnhp98hzwvpl") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.3 (crate (name "fallible_collections") (vers "0.3.1") (deps (list (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "1shgcljh6pliv1b1qk6knk2hzig5ah76hx01f1icpgkiqp6fi6cm") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.0") (deps (list (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "11iijssvznmd5xfi5mgbji30ccs3fvqy6mf434b3r21fzq7bzgkl") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.1") (deps (list (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "1rn5dscajzm9f6dih2safzid0p60h4z0w5z6zjbyiwp1d38q0ydg") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.2") (deps (list (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)))) (hash "1sk6ckixvf0pax47qgs8lfd2zi2gmyg1xgk1k7z2qgalhaaidnaa") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.3") (deps (list (crate-dep (name "hashbrown") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "006cfs45jdsczk7a8rhmf2rsw4vz5m2x74q7dzqmim2i04cx9vza") (features (quote (("unstable") ("std_io"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.4") (deps (list (crate-dep (name "hashbrown") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1xwmi3fdag0fysr5s2nfx71gzgx14cfg8c4vy6x4g4m1nrrmknsj") (features (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.5") (deps (list (crate-dep (name "hashbrown") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "11ad7vdc5gn61afakyfhw6xjhmqddg2lsyw8xf9wklw5495wz5f1") (features (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.6") (deps (list (crate-dep (name "hashbrown") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0ma7lga3zqbpzrhl76raljc6y69f38mb6j5yhkk6ldkh531wqmrz") (features (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.7") (deps (list (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)))) (hash "0k87q5f4j4iz5gh4s5idii785ncs2l792jsam7nczwslalh7gkws") (features (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57") ("hashmap" "hashbrown") ("default" "hashmap"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.8") (deps (list (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)))) (hash "01c5nbncky1azi8c0h0brzh97zn3afgi90djwx89r9cjwqhg52v1") (features (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57") ("hashmap" "hashbrown") ("default" "hashmap"))))))

(define-public crate-fallible_collections-0.4 (crate (name "fallible_collections") (vers "0.4.9") (deps (list (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)))) (hash "1zf6ir26qbdwlywv9m266n19p6yzqqmi968qy8njc58aiiv6k358") (features (quote (("unstable") ("std_io" "std") ("std") ("rust_1_57") ("hashmap" "hashbrown") ("default" "hashmap"))))))

(define-public crate-fallible_vec-0.1 (crate (name "fallible_vec") (vers "0.1.0") (hash "1fic6qq0ay627ai221zj0rs6s1s8v2nim3xzlqvbhp31r7ygh47z")))

(define-public crate-fallible_vec-0.2 (crate (name "fallible_vec") (vers "0.2.0") (hash "1nchxv1251g2q3phg7z8vg6sx5pypybjn11gdhahylgkxz975jiw")))

(define-public crate-fallible_vec-0.3 (crate (name "fallible_vec") (vers "0.3.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)))) (hash "0pgxr83jvgyv2chapd6vl8pxb8ycwcd20k364lccc78vp4y29gk9") (features (quote (("use_unstable_apis") ("default" "allocator_api" "use_unstable_apis") ("allocator_api"))))))

(define-public crate-fallible_vec-0.3 (crate (name "fallible_vec") (vers "0.3.1") (deps (list (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)))) (hash "1wmcncnjjky178ja9fn7v5shc8r30l3nclwz4mfvid7m6v2zmr21") (features (quote (("use_unstable_apis") ("default" "allocator_api" "use_unstable_apis") ("allocator_api"))))))

(define-public crate-falloc-0.1 (crate (name "falloc") (vers "0.1.0") (hash "0659wjhxnynnq4b570pfvs7c0hz551npiv1f6i1cjip06wabah8g")))

(define-public crate-fallthrough-0.1 (crate (name "fallthrough") (vers "0.1.0") (hash "059lc44pk3gdy100jksz2c2np74ywfw1i0da266ya85gxc8n7ibd")))

(define-public crate-fallthrough-0.1 (crate (name "fallthrough") (vers "0.1.1") (hash "1f3ik7pdhc5sdddm1k6j5vi9fy8zhm3b8raqpmkavqdnj35mzfsr")))

(define-public crate-fallthrough-0.1 (crate (name "fallthrough") (vers "0.1.2") (hash "1jxqsws4a66r0ywcldzs5vls9nvxjq70pw44d0iv45b21qaplsc2")))

