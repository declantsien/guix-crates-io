(define-module (crates-io fa bi) #:use-module (crates-io))

(define-public crate-fabio-0.0.0 (crate (name "fabio") (vers "0.0.0") (hash "0spfwl0ifyfabyiqf4mqf807y413yhpvz5p6g57wz6p70r3h4d9r")))

(define-public crate-fabio_exp_doc-0.1 (crate (name "fabio_exp_doc") (vers "0.1.0") (hash "1vv9gaz0i6x5hsmzkrvgi543q9ivy9z4pjmmz42m4v9d1a5z7gyp")))

