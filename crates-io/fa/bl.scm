(define-module (crates-io fa bl) #:use-module (crates-io))

(define-public crate-fable-0.0.0 (crate (name "fable") (vers "0.0.0") (hash "0ppy14gd3vifsifvvcgcdzn53p0nk5zr6wf5c22lkpw99xinaimq")))

(define-public crate-fable_data-0.1 (crate (name "fable_data") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "00nlg3rsynfzb5xn4jmb4dly7q4arrd140q0qqxhhj7r8j0qa0mm")))

(define-public crate-fable_format-0.1 (crate (name "fable_format") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "18mp7r8k2zcarxjc7iwg4zfliyg78didqc02jqh6wfak807afz2k")))

(define-public crate-fable_format-0.1 (crate (name "fable_format") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "0ngn75hqz623svb0lych49j0lqv7m3hjm8lwand484i052axhq8z")))

