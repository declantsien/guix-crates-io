(define-module (crates-io fa t3) #:use-module (crates-io))

(define-public crate-fat32-0.1 (crate (name "fat32") (vers "0.1.0") (hash "1yfsk0dx609a17vz4lklww5yy8gcs7a89h9flxrbsrcvzpl43h6v") (features (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (yanked #t)))

(define-public crate-fat32-0.1 (crate (name "fat32") (vers "0.1.1") (hash "0zgvcrrx7ddxij6dmvcxwnl6f7qg3gvln99gm8q90i01z9fiq48j") (features (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (yanked #t)))

(define-public crate-fat32-0.1 (crate (name "fat32") (vers "0.1.2") (hash "1glym47kmq84sfh7kxd8xc40lwzjf1s3zhdfpva9j44mp5j2x19h") (features (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (yanked #t)))

(define-public crate-fat32-0.1 (crate (name "fat32") (vers "0.1.3") (deps (list (crate-dep (name "block_device") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xj6c7mwhfhwxws46lzp02wdj7ig332xalskjrz69kb9hf6zny2m") (features (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (yanked #t)))

(define-public crate-fat32-0.1 (crate (name "fat32") (vers "0.1.4") (deps (list (crate-dep (name "block_device") (req "^0.1") (default-features #t) (kind 0)))) (hash "12j8snzjpi1r0r0wgzgav7ayq8z7ixilvw5ry17nxs0cjdba7b75") (features (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (yanked #t)))

(define-public crate-fat32-0.1 (crate (name "fat32") (vers "0.1.5") (deps (list (crate-dep (name "block_device") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0wfk1aaa15ywlzdzpqm58fhvq4v92y27205vqwclaby8agfwm93y") (features (quote (("default" "512") ("512") ("4086") ("2048") ("1024")))) (yanked #t)))

(define-public crate-fat32-0.2 (crate (name "fat32") (vers "0.2.0") (deps (list (crate-dep (name "block_device") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fileapi" "winioctl" "ioapiset"))) (default-features #t) (target "cfg(windows)") (kind 2)))) (hash "05rcf1s424fpqlxbilm81nmxp2qvhllk67adgp03f035vk4f9b84") (features (quote (("default" "512") ("512") ("4096") ("2048") ("1024"))))))

