(define-module (crates-io mf -u) #:use-module (crates-io))

(define-public crate-mf-utils-0.0.1 (crate (name "mf-utils") (vers "0.0.1") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "01yjd8q52jpg136ck3j6rp3z9jhsaizgrghw788pd960j9jqdng9")))

