(define-module (crates-io mf ac) #:use-module (crates-io))

(define-public crate-mface-0.1 (crate (name "mface") (vers "0.1.0") (deps (list (crate-dep (name "divider") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "rustybuzz") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tessor") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tessor_viewer") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "zeno") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ygqxixpmk7y3jlczyx9xihvkngi1sc8l9lsn56ig6cs8bxrqnmy")))

(define-public crate-mface-0.2 (crate (name "mface") (vers "0.2.0") (deps (list (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "rustybuzz") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tessor") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tessor_viewer") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "zeno") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xrwwhhr5ndcsb4j3fvc1lni3zrn1zm5989b9563lchb5q96l04l")))

