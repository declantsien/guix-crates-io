(define-module (crates-io mf xs) #:use-module (crates-io))

(define-public crate-mfxstm32l152-0.0.1 (crate (name "mfxstm32l152") (vers "0.0.1") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "i2c-hal-tools") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0vbysnnaxpd3vp09dh51npjc44dkn93gbkhqck74nsl9zik3kpcm")))

