(define-module (crates-io mf _m) #:use-module (crates-io))

(define-public crate-mf_multihash-0.0.0 (crate (name "mf_multihash") (vers "0.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.3.22") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0r78h2ngkq7n4m7r7lsidirdnfcr0raxhk44djdqs42yh7c0x1p1") (yanked #t)))

