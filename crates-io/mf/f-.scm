(define-module (crates-io mf f-) #:use-module (crates-io))

(define-public crate-mff-extract-0.1 (crate (name "mff-extract") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "number_prefix") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "tabled") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "tauri-winres") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1cccjvsc0xcnsj9yd7w41njhzhjla4499jcb3c1x25i0nvb2c7hp")))

