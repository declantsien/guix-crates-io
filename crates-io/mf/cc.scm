(define-module (crates-io mf cc) #:use-module (crates-io))

(define-public crate-mfcc-0.0.1 (crate (name "mfcc") (vers "0.0.1") (deps (list (crate-dep (name "rustfft") (req "^3") (default-features #t) (kind 0)))) (hash "1ags8qn2qz2lk6hqqz49ikfk972msgc7pcxb3qqmf9ybshdc5ms2")))

(define-public crate-mfcc-0.1 (crate (name "mfcc") (vers "0.1.0") (deps (list (crate-dep (name "fftw") (req "^0.6") (features (quote ("system"))) (optional #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^3") (optional #t) (default-features #t) (kind 0)))) (hash "1i13mq0ffnkd26ldragawzbnlj0dhlp6dgfdn46jq1xy2gz3x8iy") (features (quote (("fftrust" "rustfft") ("fftextern" "fftw/system") ("default" "fftrust"))))))

