(define-module (crates-io mf ar) #:use-module (crates-io))

(define-public crate-mfarag_guessing_game-0.1 (crate (name "mfarag_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1n9brh398szvyw8fhssv5c6pr50mv1619yif5s00nimw75pxd0lr") (yanked #t)))

(define-public crate-mfarag_guessing_game-0.1 (crate (name "mfarag_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1kih2d6yxn0wbqmia85zd7jm8ryshddj29ikzdmk5k8skf432j4d")))

