(define-module (crates-io mf mt) #:use-module (crates-io))

(define-public crate-mfmt-0.1 (crate (name "mfmt") (vers "0.1.1") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "0izkdzim8czkn08d6r1xm201svshg1kch3xay3xfpr6h04dyg67s")))

(define-public crate-mfmt-0.1 (crate (name "mfmt") (vers "0.1.2") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "01qfqj2aicz99xjdffnw6lzmp08p8ijs5y2h3bdq8y5mq30l2m33")))

(define-public crate-mfmt-0.1 (crate (name "mfmt") (vers "0.1.3") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "047n8skb2c73gjr75pqfhkaky1hy4kzmcsx62wfxka1z9m8vkr80")))

(define-public crate-mfmt-0.1 (crate (name "mfmt") (vers "0.1.4") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "10y6hjrzlz1sxxnqvfaailykb7sx2aq6bdbqpzai4xfhnsg8fw2j")))

(define-public crate-mfmt-0.2 (crate (name "mfmt") (vers "0.2.0") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "1hl5adzk1d33mm47x9dzvl62vk3jgk2h72rcfx5zpcmcpwg4jqlk")))

(define-public crate-mfmt-0.2 (crate (name "mfmt") (vers "0.2.1") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "15va1kcd22wjrckmydimacn1xwzamirwmvs76wkg84jglr9by44n")))

(define-public crate-mfmt-0.2 (crate (name "mfmt") (vers "0.2.2") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "02i95bc34y4qhh6hwzfvps27g1x9g42q69ibdwzzq11s9fxsancq")))

(define-public crate-mfmt-0.2 (crate (name "mfmt") (vers "0.2.3") (deps (list (crate-dep (name "indoc") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "0ga3prqgksql6y3fnq752kp7h39avlkvbyxl65x9fnyk1kqsrpib")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.0") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)))) (hash "1idqnavhzp39jrm2c7fdp2974rgwgprwja42fdgrhki497b5c424")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.1") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)))) (hash "10abk1jya197jj1v1f926if898j6rb2268cixxvnmnhw6gjlb34g")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.2") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)))) (hash "04wv3r61ak7kd3hmgxypajhqmji6c3yicrr9dqkmpidj26d5ry9l")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.3") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)))) (hash "1mq368055bqba5hlqq6jfwcyi02chz3fg2l4vyz5y6zffgj1c426")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.4") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "15jdq5aj9kq7kh0chjvv1w8fq19n0rpv0p4njcl6klsafaijqw0v")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.5") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0g9a6h4rbpzah4fk6ngsd6b1jpyfps87zjc3ihcbmxl4z8l47z2q")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.6") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0ggyxf59byzjf63vq8z3gfb3cr6is5ca6i60c9wzsihdjng8bp3n")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.7") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0aq6izxck0yb3hb55l9nvkghx0ai6l6hasi8k1bcy9ki3di20ljn")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.8") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1f761cvk6i0fv10ljjd149pfk0mkjcz6smy3wb42qsmqyc7x4sd0")))

(define-public crate-mfmt-0.3 (crate (name "mfmt") (vers "0.3.9") (deps (list (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1l5rcs5jk0c40ny3bp7q8whfvha7gyn74zqxyfpfakdvz90d38py")))

