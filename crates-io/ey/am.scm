(define-module (crates-io ey am) #:use-module (crates-io))

(define-public crate-eyaml-rs-0.1 (crate (name "eyaml-rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml" "color" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.30") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req ">=0.4.1") (default-features #t) (kind 0)))) (hash "0zgbxjjp6z19kqvvaydn7l9mh1rvsbpv1qq74x9dbzvq5l96zm6i")))

