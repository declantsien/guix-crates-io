(define-module (crates-io ey el) #:use-module (crates-io))

(define-public crate-eyelid-match-ops-0.0.0 (crate (name "eyelid-match-ops") (vers "0.0.0") (deps (list (crate-dep (name "ark-ff") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "ark-poly") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "ark-std") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("cargo_bench_support" "rayon"))) (optional #t) (kind 0)) (crate-dep (name "eyelid-test") (req "^0.0.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0fkc197200i83vi9vdljjhcpy0f2r7qk9vqc9fafy5zrg22mp6g4") (features (quote (("benchmark" "criterion" "rand"))))))

(define-public crate-eyelid-matcher-0.0.0 (crate (name "eyelid-matcher") (vers "0.0.0") (deps (list (crate-dep (name "eyelid-match-ops") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "eyelid-test") (req "^0.0.0") (default-features #t) (kind 2)))) (hash "1gipv10bdb2d2qgagbmdkw853al7pm54igpnh4ilkh54xbcmgi3d")))

(define-public crate-eyelid-test-0.0.0 (crate (name "eyelid-test") (vers "0.0.0") (hash "0qv1pv220jqyy0pw22n60mfswh7nsaiwmlwg0cqbvxh17kca2djw")))

(define-public crate-eyeliner-0.1 (crate (name "eyeliner") (vers "0.1.0") (hash "0lnbil23h4ybx8cjw8inkc1f3jmfds320jpyzkm6kys1i9lrbib8")))

