(define-module (crates-io ey tz) #:use-module (crates-io))

(define-public crate-eytzinger-1 (crate (name "eytzinger") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "14dcwpbznbiyi8hy96hpcmf1b28sk1rz53vbxvl6vr2s00w8xclc")))

(define-public crate-eytzinger-1 (crate (name "eytzinger") (vers "1.0.1") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "06q52nm38596jaiv7nb6qhv1nyj1v61p52d5l82l14cbg0i5ij1z") (features (quote (("branchless"))))))

(define-public crate-eytzinger-1 (crate (name "eytzinger") (vers "1.1.0") (deps (list (crate-dep (name "nohash-hasher") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nonmax") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0n1lm15h7hrpi3793h7pfh3bkp3rlc9qlildiy1l7i5dacahxmpv") (features (quote (("heap-permutator-sparse" "nohash-hasher") ("heap-permutator" "nonmax") ("default" "heap-permutator" "heap-permutator-sparse") ("branchless"))))))

(define-public crate-eytzinger-1 (crate (name "eytzinger") (vers "1.1.1") (deps (list (crate-dep (name "nohash-hasher") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nonmax") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0jy3c98n98jshxh1vw39v8xq25wd9xgdfpbq4m5k28vb2p73bvlc") (features (quote (("heap-permutator-sparse" "nohash-hasher") ("heap-permutator" "nonmax") ("default" "heap-permutator" "heap-permutator-sparse") ("branchless"))))))

(define-public crate-eytzinger-map-0.1 (crate (name "eytzinger-map") (vers "0.1.0") (deps (list (crate-dep (name "eytzinger") (req "^1") (default-features #t) (kind 0)))) (hash "1sb5zxrnr06vphbgl865kgddkgdydq7a62r7rwn3v8crb26bbsbw")))

(define-public crate-eytzinger-map-0.1 (crate (name "eytzinger-map") (vers "0.1.1") (deps (list (crate-dep (name "eytzinger") (req "^1") (default-features #t) (kind 0)))) (hash "1jmvszd7ym52yqakdks1jlanjkjgrmwfhdk0dfqwz30f9y52b2rb")))

