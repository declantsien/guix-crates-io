(define-module (crates-io ey wa) #:use-module (crates-io))

(define-public crate-eywa-0.0.1 (crate (name "eywa") (vers "0.0.1") (hash "16h4rxj760wvmynz582yi5lwrkhxdlqyn3j0pvwi92jyz1gs857b")))

(define-public crate-eywa-0.0.2 (crate (name "eywa") (vers "0.0.2") (hash "1rww2n16if0alwjybcwrzj1q5ms8fwlw9gns99mhc64cjv25dfw9")))

(define-public crate-eywa-0.0.3 (crate (name "eywa") (vers "0.0.3") (hash "0ylnhvc2gm617176zvn3sl3n1yqdj3ngijaxgxmvf3mdqdrp5nf6")))

(define-public crate-eywa-0.0.4 (crate (name "eywa") (vers "0.0.4") (hash "084rbzbd83fsv7fyvn2skjvb49rnniwqbh57djpz6s24sp10hjb0")))

(define-public crate-eywa-0.0.5 (crate (name "eywa") (vers "0.0.5") (hash "0l0vyim3m481c6g1xzy8c2hykpr7xmn548pll1g1l1l85cxv1qd3")))

(define-public crate-eywa-0.0.6 (crate (name "eywa") (vers "0.0.6") (hash "0agdyiqfm8jws73b3591g11mgwwazwqx5b8bf9mmllmh6cpkjxij")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.0") (hash "1w9f6fd2apkszj0kinydmr86csd8iymwdc8mrg9drarzl72m9qmr")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.1") (hash "10zxh1k0pvwyf1b2fspjby60jq3hgnyp88kf2q9bfbw38fj2iz8q")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.2") (hash "1kan7slx0q5yq4sv9449mrx3w7d5n1735zacxqqi4dlawd5fzjv8")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.3") (hash "0k8rbwvpvhhg0mr087kvcl9q759anrfkk8yz4y0zi63f578mqyp9")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.4") (hash "015gch4gii3zkhr622lzx017ib32qcwqlxf5251cnq6ww1q95ly1")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.5") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "11xbxybi15yn5d1msd18lrivb0rga87g3ki0m3llapwzc4rwfr48")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.6") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0l8753xhv6ss9fqs3ciygkjqshpkn6bjcp1x01mcr9r9dgxnjkb8")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.7") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1snfix06nrsn04majfy3nh6nq2n7ygny66ryv21dqv6s2kcgsp25")))

(define-public crate-eywa-0.1 (crate (name "eywa") (vers "0.1.8") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "17hd95rdsif4hipddc909yb55538zai4p43b6y38gwhj09wci226")))

(define-public crate-eywa-0.2 (crate (name "eywa") (vers "0.2.0") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "printers") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "1g8ww054r3cr819r558fsza3slxzgv6nd6sk7chycccin5p54924")))

(define-public crate-eywa-0.2 (crate (name "eywa") (vers "0.2.1") (hash "16225mkjkcsqwnk3cimhkxnqdqbgd0klcsg52r1bl36hpfzy84yy")))

