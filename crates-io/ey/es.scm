(define-module (crates-io ey es) #:use-module (crates-io))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.0.0") (hash "1zi5xsbvc79nn1157xi99k0r6w0l949x9yz3ln8dk3izvyv0ps4p") (yanked #t)))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.0.1") (hash "1n70pn4zyfc40c34dyqinb8vv1vs3irzwp2iw6svvmbnf1blaxfc") (yanked #t)))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.1.0") (hash "0lb9idcq9n6j67jmyp0by9z5cwrl8jlp5ayklcwvdj5avgzfvz5n") (yanked #t)))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.1.1") (hash "0ip2czd4d2ysmxab9d3w5aj1w2gfsnby8nigm4l4ndv8aly7f1ac") (yanked #t)))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.1.2") (hash "1qmxphizcs8ny3adnym2lifjhsspbc9hqsg1i6iy8mx1j4vhc37r")))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.2.0") (hash "18q65j07va7ln82gk1vcgc8sb8hgdlrkz3vdp1kvccqy4xda8bbj")))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.2.1") (hash "02x6z6g0dj7n38r6s8y4a5g3x454x13c2rk71fszkvk8n916ki10")))

(define-public crate-eyes-1 (crate (name "eyes") (vers "1.3.0") (hash "11dr4p4pmb4ybp343w9vyxpxlsy708skfcrbkmp257qq770x57zs")))

