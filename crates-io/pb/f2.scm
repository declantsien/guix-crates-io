(define-module (crates-io pb f2) #:use-module (crates-io))

(define-public crate-pbf2graph-0.3 (crate (name "pbf2graph") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "osmpbf") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1rj5g0k4s7j8pkxykb2nz846y7f61yccmbsndg54d3kx7cwzlmwq")))

(define-public crate-pbf2graph-0.4 (crate (name "pbf2graph") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "osmpbf") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "131knm6dam3km1yv58h1sp535clwr8p01bsg7vvvmrxqxm9sqrqr")))

(define-public crate-pbf2txt-0.1 (crate (name "pbf2txt") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "osmpbfreader") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "18mcal8qm7xidmp2pqsg625qg0c4r8pwvxsppvy0fkh9lq79vsfd")))

