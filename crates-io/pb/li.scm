(define-module (crates-io pb li) #:use-module (crates-io))

(define-public crate-pblib-rs-0.1 (crate (name "pblib-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "splr") (req "^0.17.1") (features (quote ("incremental_solver"))) (default-features #t) (kind 2)))) (hash "0b8l3shmgkbwycwl29vw53qp8vgq18d5nra0dxp0a1pcnz8053f6") (links "libcpblib.a")))

