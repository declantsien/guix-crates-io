(define-module (crates-io pb ar) #:use-module (crates-io))

(define-public crate-pbar-0.1 (crate (name "pbar") (vers "0.1.1") (deps (list (crate-dep (name "aok") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "aok") (req "^0.1.11") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.17.8") (default-features #t) (kind 0)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "1clxqxvy0gpr3h7qkrxw36fk7zfdbf0prghrqzq1g72ah60pmy5q")))

(define-public crate-pbars-0.1 (crate (name "pbars") (vers "0.1.0") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "1mszwbmr3camk6fdmh5jlm1idvgy03k1q4ka6nw782n9c1144amk")))

(define-public crate-pbars-0.1 (crate (name "pbars") (vers "0.1.1") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "1mkdy7cpzkvrx9jr3ss5wpdqy2xyr9glz2fs2mjxy1qqz16sapl4") (features (quote (("default" "crossterm"))))))

(define-public crate-pbars-0.1 (crate (name "pbars") (vers "0.1.2") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "1gflc21sxgp3napnsfxy4l4mn9zv3aczfv12crrzsgb03wrzkkir") (features (quote (("default" "crossterm"))))))

