(define-module (crates-io pb to) #:use-module (crates-io))

(define-public crate-pbtools-0.1 (crate (name "pbtools") (vers "0.1.0") (hash "0za44nd7bfrm1nlqjhfvwkgfl23p9xdv1ig7qzp3gzkf2cp1g00v")))

(define-public crate-pbtools-0.2 (crate (name "pbtools") (vers "0.2.0") (hash "18iddv7qgg5y7gm894lcx6056897s0kbv2rm0bbh371mx1jmx73f")))

(define-public crate-pbtools-0.3 (crate (name "pbtools") (vers "0.3.0") (hash "155x64sa0c547y20hkwbccp1fzqkwxqarkcz8fmwlw095ynq9rcr")))

(define-public crate-pbtools-0.4 (crate (name "pbtools") (vers "0.4.0") (hash "08h42702nwdgm9chh41xrx0xp39r448rqav0ff552yy71mgrza7d")))

(define-public crate-pbtools-0.5 (crate (name "pbtools") (vers "0.5.0") (hash "1j97nmna0m8f4a3ccq4b0c67j8fzdaf2aj0428a00irsdlfnxfpr")))

(define-public crate-pbtools-0.6 (crate (name "pbtools") (vers "0.6.0") (hash "06f2j4hbxks3d8x3vbpnyamf8fisrr8las0k4qb3mxnpgf73dr3y")))

(define-public crate-pbtools-0.7 (crate (name "pbtools") (vers "0.7.0") (hash "0s371y0zrmkjbmj2gzsjwn53wac2vfbgf1wpq4qfd4wbi08w2fsf")))

(define-public crate-pbtools-0.8 (crate (name "pbtools") (vers "0.8.0") (hash "09awd6bg7hqcnkwmclcxr5n1cmabykw2mxjvwvr0rnsidngiibzh")))

(define-public crate-pbtools-0.9 (crate (name "pbtools") (vers "0.9.0") (hash "1s4yif49xsa0fczzxp2a5b72360509zq27gpbk11mwi0wcbsph3m")))

(define-public crate-pbtools-0.10 (crate (name "pbtools") (vers "0.10.0") (hash "1qcandhf6g8bk6m2a80g75ig22pyrzcyrsrws8jykk66ngcasy6c")))

(define-public crate-pbtools-0.11 (crate (name "pbtools") (vers "0.11.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0qa0mnicxglp7azgcjjd61kmhfrnzw6klqm47znbs4razq0b1g6f")))

(define-public crate-pbtools-0.12 (crate (name "pbtools") (vers "0.12.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "06rjb6j3svm0wcs4686jdjr4zbvq7g9a0c1kwlk1mcdalds9pkxq")))

(define-public crate-pbtools-0.13 (crate (name "pbtools") (vers "0.13.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1lggccvxfpyif3113d2z0xik5psrmh5l0rryrn1hy8xxik16xpgb")))

(define-public crate-pbtools-0.14 (crate (name "pbtools") (vers "0.14.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1qc8zn88jpsb7wh2cibj1hsmyp09jbbhadkvmw14cyciixamx1p7")))

(define-public crate-pbtools-0.15 (crate (name "pbtools") (vers "0.15.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "04hpgrpqy918jzvfz0m1aizk25zfp199yxyrv51qcl4vwfznmq9m")))

