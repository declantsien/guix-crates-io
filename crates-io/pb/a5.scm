(define-module (crates-io pb a5) #:use-module (crates-io))

(define-public crate-pba5-aes-modes-activity-group2-0.1 (crate (name "pba5-aes-modes-activity-group2") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "17676sh9m3xaxk6aivyq8956g59j92b1vy6385v8cmd1r7mbdzjd")))

(define-public crate-pba5-aes-modes-activity-group2-0.1 (crate (name "pba5-aes-modes-activity-group2") (vers "0.1.1") (deps (list (crate-dep (name "aes") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "14w1x2qfh9nyhm51lcp3swm2n54p03d028lv5b5ym6sglybggqig")))

