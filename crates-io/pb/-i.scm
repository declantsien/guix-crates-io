(define-module (crates-io pb #{-i}#) #:use-module (crates-io))

(define-public crate-pb-imgsize-0.2 (crate (name "pb-imgsize") (vers "0.2.4") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1j3vqvkrkjpqmki00wrd8b8x2k33jhpwx5vvbcn8hkkvgjvc9x6b")))

(define-public crate-pb-imgsize-0.2 (crate (name "pb-imgsize") (vers "0.2.5") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1pzk8f22v9r8xcf237ay7xa3zjgaay522cp3fghixi0bpva28gp6")))

