(define-module (crates-io pb b_) #:use-module (crates-io))

(define-public crate-pbb_add_two-0.1 (crate (name "pbb_add_two") (vers "0.1.0") (hash "156xf10r3pjw53c6dpfg62dn4g21bb445bw9qkzmcy506ccw2wz2")))

(define-public crate-pbb_minigrep-0.1 (crate (name "pbb_minigrep") (vers "0.1.0") (hash "1cqnbi0np0q1a9j3pdf00fqzs2v4qf5g93wr7zr0dirks1n4cm7h")))

