(define-module (crates-io pb -t) #:use-module (crates-io))

(define-public crate-pb-to-json-0.1 (crate (name "pb-to-json") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fq2s0m9nhqfl2pci00nvpyrpwg9d3hq0ii781ig54lcd88ilpgj")))

(define-public crate-pb-to-json-0.1 (crate (name "pb-to-json") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "16krnf8ahkqkiqjn5hlq9gcmcxwwhp5a55kw5yrc0fjn8k3qqxyq")))

