(define-module (crates-io pb mx) #:use-module (crates-io))

(define-public crate-pbmx-0.1 (crate (name "pbmx") (vers "0.1.0") (hash "1zckq1y0xizrfqiqk004h5v73jgci2w1rpdql81aa0gmd8s4n3as") (yanked #t)))

(define-public crate-pbmx-0.1 (crate (name "pbmx") (vers "0.1.1") (hash "1hw1sfhyasnxzsfbk298bq825nml4ipm4v3symys6cfk1q6w3qda") (yanked #t)))

(define-public crate-pbmx-0.1 (crate (name "pbmx") (vers "0.1.2") (hash "14r2hy129nmxw1p17vi239yf28857m8kjy27qh4y0ibl7grpcan7")))

