(define-module (crates-io pb f-) #:use-module (crates-io))

(define-public crate-pbf-reader-0.0.3 (crate (name "pbf-reader") (vers "0.0.3") (deps (list (crate-dep (name "flate2") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "18sqv509d06fxbbhy8ar49qfxkymvma696wlnnf94xjkd5hf0ggi")))

(define-public crate-pbf-reader-0.0.4 (crate (name "pbf-reader") (vers "0.0.4") (deps (list (crate-dep (name "flate2") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "1f0plb00rmwvhrwjkyhp472jybsdiv3xbaynrwqgvv98m6qn14ik")))

(define-public crate-pbf-reader-0.1 (crate (name "pbf-reader") (vers "0.1.1") (deps (list (crate-dep (name "flate2") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.49.0") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "14biz86m3496pdz174dk8drac0c4cy9lj042i4p07ymnx3vdcg98")))

(define-public crate-pbf-reader-0.1 (crate (name "pbf-reader") (vers "0.1.2") (deps (list (crate-dep (name "flate2") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.49.0") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "08cgma2lxrm34106mk1j5x76a96z8irhxdd3da2xsrb67m8s8pcp")))

(define-public crate-pbf-reader-0.1 (crate (name "pbf-reader") (vers "0.1.3") (deps (list (crate-dep (name "flate2") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.57.0") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "14sh4y4yyb4gjxz4ph4xqg8vgvaia68zsiw9g98y7map2j0af33a")))

(define-public crate-pbf-reader-0.1 (crate (name "pbf-reader") (vers "0.1.4") (deps (list (crate-dep (name "flate2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.74.0") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "06cla9jkqhpvkp43lp1nyf5xxdbmb7wlvcl8dkqblmr8032g7p6y")))

(define-public crate-pbf-reader-0.1 (crate (name "pbf-reader") (vers "0.1.7") (deps (list (crate-dep (name "flate2") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.82.0") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^2.1.1") (default-features #t) (kind 1)))) (hash "1ycp5g8fmq50g7sqkwzv9isqz26jdc6ricaqkmd5xzvf1k3dg1bg")))

(define-public crate-pbf-reader-0.1 (crate (name "pbf-reader") (vers "0.1.8") (deps (list (crate-dep (name "flate2") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.82.0") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen-pure") (req "^2.2.0") (default-features #t) (kind 1)))) (hash "1jcwyfyapikshb0glxy5f5fdy643w3lywlskcrbn06098f9ryw5b")))

(define-public crate-pbf-reader-0.1 (crate (name "pbf-reader") (vers "0.1.9") (deps (list (crate-dep (name "cbindgen") (req "^0.9.1") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.103.0") (default-features #t) (kind 2)) (crate-dep (name "protobuf") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen-pure") (req "^2.8.0") (default-features #t) (kind 1)))) (hash "1kxb0907y467z3n7fqmn3zp5imv4bvwghvbcxq8m9235d8pwm8vl")))

