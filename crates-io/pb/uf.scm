(define-module (crates-io pb uf) #:use-module (crates-io))

(define-public crate-pbuf-0.3 (crate (name "pbuf") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "1a2wbfsw00gxkiz5s1d2xl7pzb7y417v443b79r34l8177sf4m4c")))

(define-public crate-pbuf-0.3 (crate (name "pbuf") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "08i0dg7nifhd7s019j75cx60fs3wm30ls9hz66pxklxxjb75pg1h")))

(define-public crate-pbuf-0.3 (crate (name "pbuf") (vers "0.3.31") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "0i5b2mg6y8c8aklgh4gbrvz9n1xxpvqs2h6h1g8i7h3z36337yyp")))

