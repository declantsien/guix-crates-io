(define-module (crates-io pb s-) #:use-module (crates-io))

(define-public crate-pbs-sys-0.0.1 (crate (name "pbs-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.63") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.63") (optional #t) (default-features #t) (kind 1)))) (hash "1ya06b0xmpw0s78hasjv1ngw8z4053w0vxzpirdyq2ca0ch8sxzd") (features (quote (("static") ("default"))))))

(define-public crate-pbs-sys-0.0.2 (crate (name "pbs-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.63") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked_list_c") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.63") (optional #t) (default-features #t) (kind 1)))) (hash "0w9k15v400lg03xlsihh2qxpbycsils2qgar8x6ja9xcq4h7n41l") (features (quote (("static") ("default"))))))

