(define-module (crates-io pb si) #:use-module (crates-io))

(define-public crate-pbsim-rs-0.1 (crate (name "pbsim-rs") (vers "0.1.0") (hash "1ap6zf5krchh61cyvgwhcjn5w91i62dmsjwyzqrf0mgnb5ji7jj8") (yanked #t)))

(define-public crate-pbsim-rs-0.1 (crate (name "pbsim-rs") (vers "0.1.1") (hash "0knyn6fwaivf73pbys0j4aaj0cy293ww0ma9wy6p0lc36mp39hcl") (yanked #t)))

