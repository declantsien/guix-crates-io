(define-module (crates-io pb od) #:use-module (crates-io))

(define-public crate-pboddupalli-helloworld-0.1 (crate (name "pboddupalli-helloworld") (vers "0.1.0") (hash "1s4ybq871v4drbw1ms0fxkk94fig3psfnsd60l5459x4s660bxk2")))

(define-public crate-pboddupalli-helloworld-0.1 (crate (name "pboddupalli-helloworld") (vers "0.1.1") (hash "125z4pq7fqc9c1rvkvc5j41x1v3vcxrrc65kkfbbjz9rfygfd1zm")))

(define-public crate-pboddupalli-helloworld-0.1 (crate (name "pboddupalli-helloworld") (vers "0.1.2") (hash "12yx16hvvk454yk6cd0xkan7vmdmac9basxmfc08fkhnrpbn6k15")))

