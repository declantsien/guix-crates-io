(define-module (crates-io pb ix) #:use-module (crates-io))

(define-public crate-pbix-0.1 (crate (name "pbix") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (features (quote ("deflate"))) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0ya06rfxsw6m90i8llhr6kh1vc7vm90idrr7c9hkax0y9ys2p8n5") (v 2) (features2 (quote (("rayon" "dep:rayon"))))))

