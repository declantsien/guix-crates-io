(define-module (crates-io pb ut) #:use-module (crates-io))

(define-public crate-pbutil-0.1 (crate (name "pbutil") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0rkzylwl7msl0qvxlxcwcff5w4g88n86xgmrjb15rvp0mp016fvz")))

(define-public crate-pbutil-0.1 (crate (name "pbutil") (vers "0.1.1") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1iw0k4nqprjdx5wkbc4jsksimqg55z5wjm9x43ps24vard30vrk5")))

(define-public crate-pbutil-0.1 (crate (name "pbutil") (vers "0.1.2") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "06zblkv1if9nr530dxfibw74q3p4jrgw6lh828z9d190gsr4wh2l")))

