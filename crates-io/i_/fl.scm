(define-module (crates-io i_ fl) #:use-module (crates-io))

(define-public crate-i_float-0.1 (crate (name "i_float") (vers "0.1.0") (hash "1bz9r8vhjc3jclqlpbgnxkc2axx4gpaihdgwz65zif8s075rr2j1")))

(define-public crate-i_float-0.1 (crate (name "i_float") (vers "0.1.1") (hash "1l8plll5vplxvmcfkcim1wslfcpyinhpxf6kh6ccvxyn896a5np0")))

(define-public crate-i_float-0.2 (crate (name "i_float") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ydda53qnsf44rjjwg3jd4va9hll5yp7nv15li6n99248wr8vcl6")))

(define-public crate-i_float-0.3 (crate (name "i_float") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0x2723yw8y43js2cs8c7qnzl7m33dd6c44zfvd1xavxz86i5ba85")))

(define-public crate-i_float-0.4 (crate (name "i_float") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rr7gy3di837y9xa0bd536ly791vapmz9z7aqjmsddp06fy5mpla")))

(define-public crate-i_float-0.5 (crate (name "i_float") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wdz3wm07x3k86cmcjfnhiigd7myvr6pqg4yp6ffv0lnxkrrgwrn")))

(define-public crate-i_float-0.6 (crate (name "i_float") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1520idr7yvsml07gci4fgmd8bnx9x1bj1ywwh6cw40lq65jaqfib")))

(define-public crate-i_float-0.7 (crate (name "i_float") (vers "0.7.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wld6xzmyb4vfqa72jav8j52zj8fpg0n1w4k4lbp3vkspwxvhf7b")))

(define-public crate-i_float-0.8 (crate (name "i_float") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lrldlqfw3gxp3nbbpy94n6rk43xkl714qsj9glkali115ybzh85")))

(define-public crate-i_float-0.9 (crate (name "i_float") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0l2yxs267gwqwgjqaygybjgk011chj5hnivgman3vdks8rb0axg2")))

(define-public crate-i_float-0.10 (crate (name "i_float") (vers "0.10.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gx3rq9f9kyvhzy1rqwbhmn5ljahmmpbcym4lfafqbhyz8i83ay8")))

(define-public crate-i_float-1 (crate (name "i_float") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16a7sl4n1iqc1kvgxic36q24ry96wdxgklkyy5m0b1j8lxpfc7fr")))

(define-public crate-i_float-1 (crate (name "i_float") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1r7qanp7vjdjn2q8b1i5xsdlz5cnwjwm8a3g1c2f3xr5nga1660x")))

(define-public crate-i_float-1 (crate (name "i_float") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cg704lqb507r9d7azy5r0z6cjihsjmwm6c1yh9iyr9r052v10my")))

(define-public crate-i_float-1 (crate (name "i_float") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ja5x6d5zq7drwz1sx0ahhh2l5h88sriw65d28qkv2i16xzmizxn")))

