(define-module (crates-io wy br) #:use-module (crates-io))

(define-public crate-wybr-0.0.1 (crate (name "wybr") (vers "0.0.1") (hash "111hqnmqlyyf3qvl7gwz73w7d35ssqasilrrq8yy83jz8r3z82v6")))

(define-public crate-wybr-0.0.2 (crate (name "wybr") (vers "0.0.2") (hash "0cf1mbswymdr9h6i971jacgxxdhis4pkxg6x9258riv79nrwwqvv")))

(define-public crate-wybr-0.0.5 (crate (name "wybr") (vers "0.0.5") (hash "1wg7wpz4zvqfhgxaf73k9vdjlcxwr62rh557ma5bn4jbnsgiphp6")))

