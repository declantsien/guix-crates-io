(define-module (crates-io wy ha) #:use-module (crates-io))

(define-public crate-wyhash-0.1 (crate (name "wyhash") (vers "0.1.0") (hash "0a9h862p3agj7qpdpn0xypgaiqrd84d9r7snrkxrhd44yz8ha35z") (features (quote (("std") ("default" "std"))))))

(define-public crate-wyhash-0.2 (crate (name "wyhash") (vers "0.2.0") (hash "0xza1w48yxmkq5j7p3b5kpvx5jj9j4ssczs11cci3z8k42dwk8h4")))

(define-public crate-wyhash-0.2 (crate (name "wyhash") (vers "0.2.1") (deps (list (crate-dep (name "rand_core") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rd39k5lw1h77j1bi42dwd3v6hpis75k3yyk5ypfivqn094jfv8a")))

(define-public crate-wyhash-0.3 (crate (name "wyhash") (vers "0.3.0") (deps (list (crate-dep (name "rand_core") (req "^0.4") (default-features #t) (kind 0)))) (hash "005fad6cf4pb94dl41fp1a6z6hkv3k39klbw48b6jcy4ibs50akq")))

(define-public crate-wyhash-0.4 (crate (name "wyhash") (vers "0.4.0") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "1zhg5qm083g2ry23099agi9q8apd41nkz6zgj6dn084ilzj2svwc")))

(define-public crate-wyhash-0.4 (crate (name "wyhash") (vers "0.4.1") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "0754mjqx16kda0bhl5wz93dhxzpv0scfvv8cnd7mwmr7vchn3qhg")))

(define-public crate-wyhash-0.4 (crate (name "wyhash") (vers "0.4.2") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "0m7hjqpgywxrkz1nmmywpxni8sxb6xirbzmr3w6rlsds2kjfr1ln")))

(define-public crate-wyhash-0.5 (crate (name "wyhash") (vers "0.5.0") (deps (list (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "15f26hvx6nyp4d6iswha7rm3psidxa2k2iab1f1aqgsyq9iy3xms")))

(define-public crate-wyhash-final4-0.1 (crate (name "wyhash-final4") (vers "0.1.0") (hash "0nqm802s5rcq2xll3a4ds2zgqk7x8kvzds358iwg4v64abwnkx1v") (features (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std")))) (yanked #t)))

(define-public crate-wyhash-final4-0.1 (crate (name "wyhash-final4") (vers "0.1.1") (hash "19divz6xjsgb4yilyfrc6mz1fdysbwwwl0icnwg693mbaypnhxyn") (features (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std")))) (yanked #t)))

(define-public crate-wyhash-final4-0.1 (crate (name "wyhash-final4") (vers "0.1.2") (hash "0cwy75g67b6h63bhb3q7ps279la90wvrj0fznns1in0wq7a11px2") (features (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std")))) (yanked #t)))

(define-public crate-wyhash-final4-0.1 (crate (name "wyhash-final4") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1dqvz48cvch6isf9ir46wfzzna8nwdq08qjgj3dg5knkg08h3di5") (features (quote (("wyhash64condom") ("wyhash64") ("wyhash32condom") ("wyhash32") ("std") ("default" "wyhash32" "wyhash32condom" "wyhash64" "wyhash64condom" "std"))))))

(define-public crate-wyhash2-0.1 (crate (name "wyhash2") (vers "0.1.0") (hash "0vqjfzc8hlh6sha074bv7bzbyz23p2f93ps3infb40zx5h3pc27s") (features (quote (("nightly") ("default"))))))

(define-public crate-wyhash2-0.1 (crate (name "wyhash2") (vers "0.1.1") (hash "1fd2pw9z4adrz2zrkidqf9kjwqvl9s0ia8fxhfbpnklwmw44cx4x") (features (quote (("nightly") ("default"))))))

(define-public crate-wyhash2-0.2 (crate (name "wyhash2") (vers "0.2.0") (deps (list (crate-dep (name "no-std-compat") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xyhmmjxrqg50kpphwhzwmxisqbmpd0d72mvq4vrwpzcj22rkw08") (features (quote (("std" "no-std-compat/std") ("nightly") ("default" "std")))) (yanked #t)))

(define-public crate-wyhash2-0.2 (crate (name "wyhash2") (vers "0.2.1") (deps (list (crate-dep (name "no-std-compat") (req "^0.4") (default-features #t) (kind 0)))) (hash "06xgy5m270364rpdi1y0pp3zk0awdqgw77wc0fbq32ijdv4cfcwl") (features (quote (("std" "no-std-compat/std") ("nightly") ("default" "std"))))))

