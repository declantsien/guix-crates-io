(define-module (crates-io wy nd) #:use-module (crates-io))

(define-public crate-wynd-utils-0.4 (crate (name "wynd-utils") (vers "0.4.1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta7") (default-features #t) (kind 0)) (crate-dep (name "cw-storage-plus") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "prost") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "1akj7ysmwqziqikihdmhl13syns67d9dgw66mjczb3c0p8zpp8ys")))

