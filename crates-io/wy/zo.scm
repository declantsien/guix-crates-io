(define-module (crates-io wy zo) #:use-module (crates-io))

(define-public crate-wyzoid-0.1 (crate (name "wyzoid") (vers "0.1.0") (deps (list (crate-dep (name "ash") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0k06blmmfb7j7f46brjylh9myw8m00nf0d9vlk76nhnjcdciffh4")))

(define-public crate-wyzoid-0.1 (crate (name "wyzoid") (vers "0.1.1") (deps (list (crate-dep (name "ash") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1nbzvgrycxrgxs7z9w3s2k655hvp1wkpyh81lfz4jrfssgvp88pz")))

(define-public crate-wyzoid-0.1 (crate (name "wyzoid") (vers "0.1.2") (deps (list (crate-dep (name "ash") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0n1pirjmx39p1pn0ax420yxp9m9780525il2niww2r2q4ydx53pg")))

