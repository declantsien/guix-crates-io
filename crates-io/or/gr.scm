(define-module (crates-io or gr) #:use-module (crates-io))

(define-public crate-orgrender-0.1 (crate (name "orgrender") (vers "0.1.0") (deps (list (crate-dep (name "orgize") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "01rra0d2x9c8wjqczsp63zmmb6x14jln5b82ni2ddx6a64vhvmg2")))

(define-public crate-orgrender-1 (crate (name "orgrender") (vers "1.0.0") (deps (list (crate-dep (name "orgize") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0bjnd0j91aqfzi7xqq9iaa11mjgxz4lipz323d9qvl1ampil5c3r")))

