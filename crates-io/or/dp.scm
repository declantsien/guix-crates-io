(define-module (crates-io or dp) #:use-module (crates-io))

(define-public crate-ordpath-0.1 (crate (name "ordpath") (vers "0.1.0") (hash "0g6yby7qfg7mcv3ilvxm9pixvadsxc1ih01vk2m9k70xacifg3g2")))

(define-public crate-ordpath-0.2 (crate (name "ordpath") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1n3pf5dh5av4j9555d0gjgp2q5xsrwhgjz4hc0dpihc70x14kz7y")))

