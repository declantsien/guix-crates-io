(define-module (crates-io or -r) #:use-module (crates-io))

(define-public crate-or-rs-0.1 (crate (name "or-rs") (vers "0.1.0") (hash "1yan7b10aw7k3xrhc1plp6mh98cqmpkz1y1hax8z9kzilsw2qav0") (features (quote (("unstable_feature") ("default")))) (rust-version "1.60")))

(define-public crate-or-rs-0.1 (crate (name "or-rs") (vers "0.1.1") (hash "1a52igm2n55jgrgz03nrcng8if69i1mzgpa699pk39b1dkir18n0") (features (quote (("unstable_feature") ("default")))) (rust-version "1.60")))

(define-public crate-or-rs-0.1 (crate (name "or-rs") (vers "0.1.2") (hash "1xh0qvj7yyr5gij5zm6q3bih62629w4h0k08bzckh1rmlm18f1cz") (features (quote (("unstable_feature") ("default")))) (rust-version "1.60")))

(define-public crate-or-rs-macros-0.1 (crate (name "or-rs-macros") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qmsqwf9bxv366hgqsafqwh96fkbsk8rq05v2085gn393fpqwv3v") (features (quote (("macro_error_debugging" "colored" "proc-macro2/span-locations") ("default" "proc-macro2")))) (rust-version "1.60")))

(define-public crate-or-rs-macros-0.1 (crate (name "or-rs-macros") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pmv020srf25vsamh0xbay8dd46zjscakkz8y1x8piw8f2p82q32") (features (quote (("macro_error_debugging" "colored" "proc-macro2/span-locations") ("default" "proc-macro2")))) (rust-version "1.60")))

