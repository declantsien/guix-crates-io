(define-module (crates-io or dn) #:use-module (crates-io))

(define-public crate-ordnl-1 (crate (name "ordnl") (vers "1.0.0") (hash "1g1k8yj493mhhi1my9kbvd6wa9yndl7zvzwbqk5k9igq3zqi714z")))

(define-public crate-ordnl-1 (crate (name "ordnl") (vers "1.0.1") (hash "00cqh4q7avr3srmln6qmn25jyj53irm7y4h786c9pzd66clvyrvk")))

(define-public crate-ordnl-1 (crate (name "ordnl") (vers "1.0.2") (hash "1gdfn1j5jnrlhlrazrzbvwhc5mxgixqww6snlran4qvbi7f0z4ps")))

(define-public crate-ordnl-1 (crate (name "ordnl") (vers "1.0.3") (hash "17z05dlyaywcjp2431x661f94phv08lldc7cqlvnx474j2y3lmbh")))

(define-public crate-ordnung-0.0.0 (crate (name "ordnung") (vers "0.0.0") (deps (list (crate-dep (name "ahash") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.0") (default-features #t) (kind 0)))) (hash "0npqb07q3f3lyq8qclzlwcxy059lyk4r4p24kj21sd3bljzbyvcs")))

(define-public crate-ordnung-0.0.1 (crate (name "ordnung") (vers "0.0.1") (deps (list (crate-dep (name "ahash") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.0") (default-features #t) (kind 2)))) (hash "0lan4si2ah1w13g6rkpqq587saal5ypsi8clq4msg6h182i4352y")))

