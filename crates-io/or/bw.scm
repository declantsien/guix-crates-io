(define-module (crates-io or bw) #:use-module (crates-io))

(define-public crate-orbweaver-0.1 (crate (name "orbweaver") (vers "0.1.0") (hash "1wc7x9d4b08vbz5rhacs2vfrlp2np5x0ci5m466634jkpi5n5y75")))

(define-public crate-orbweaver-0.2 (crate (name "orbweaver") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "0l619knlqc5gj0sh3027z29nzp9sy00w38mqwwpbsz23ln46c541") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-orbweaver-0.2 (crate (name "orbweaver") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "04z2057xczdzc81gnm4dq1kn1xyjxlbyjkq5w5pi2r3d3zdwz2h4") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-orbweaver-0.3 (crate (name "orbweaver") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "0sq04a0cwz6a1r35l904ypp4wj7whfx5q9q41b6d35d5kb7zb6dn") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-orbweaver-0.3 (crate (name "orbweaver") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wgml17hyp703cw286wn6zn8h8j4ir40vfclfgn2f452w274zmc5") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-orbweaver-0.4 (crate (name "orbweaver") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "0s7afjdwlhldxkhy6b486wkb7jciqb32xj6ihxphk6v56q36b82s") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-orbweaver-0.4 (crate (name "orbweaver") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ycjwjdvm6wybxskxyy91m0yabgjlvccq4mb01wblnq3821n5sq9") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

