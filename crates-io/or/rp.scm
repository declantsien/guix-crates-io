(define-module (crates-io or rp) #:use-module (crates-io))

(define-public crate-orrp-0.1 (crate (name "orrp") (vers "0.1.0") (deps (list (crate-dep (name "open") (req "^1.2.2") (default-features #t) (kind 0)))) (hash "0mq9650jcy071x5px93r7s0w64f72qibmbyprynf0jjf1rbllk8d")))

