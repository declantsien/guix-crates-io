(define-module (crates-io or t_) #:use-module (crates-io))

(define-public crate-ort_batcher-0.1 (crate (name "ort_batcher") (vers "0.1.0") (deps (list (crate-dep (name "divan") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "flume") (req "^0.11.0") (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("approx"))) (default-features #t) (kind 2)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "ort") (req "^2.0.0-alpha.1") (features (quote ("ndarray"))) (kind 0)) (crate-dep (name "ort") (req "^2.0.0-alpha.1") (features (quote ("download-binaries" "fetch-models" "cuda"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "1ym3083i9kbyk90qd8mllvb8f84qi45sqd56lnwxxj9wkskjbjm8")))

(define-public crate-ort_batcher-0.1 (crate (name "ort_batcher") (vers "0.1.1") (deps (list (crate-dep (name "divan") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "flume") (req "^0.11.0") (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("approx"))) (default-features #t) (kind 2)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "ort") (req "^2.0.0-alpha.1") (features (quote ("ndarray"))) (kind 0)) (crate-dep (name "ort") (req "^2.0.0-alpha.1") (features (quote ("download-binaries" "fetch-models" "cuda"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "0yq0a2mp866xg8nrknkgzvla488j3id137kyhdxdnx6vqi7akkjk")))

(define-public crate-ort_custom_op-0.1 (crate (name "ort_custom_op") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "1la5hy89hyk3xglsl679qx7yay19bp6g09vp2ld2snxqlmbdx8d8")))

(define-public crate-ort_custom_op-0.1 (crate (name "ort_custom_op") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "071kyiwxwjp6y83mdrkizkw0nc8dd3rnzdjyzsf32sb2xh2fcxpw")))

(define-public crate-ort_custom_op-0.2 (crate (name "ort_custom_op") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0cq1y8z4610ivs42gjajwf5yzg5yxqf4xaxvcjihvgn5lc19h1k3")))

(define-public crate-ort_custom_op-0.3 (crate (name "ort_custom_op") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "1x8wr99s07yhrrqd9zh12813afp9vyvx8gw3d79sxpiw59aww7an")))

(define-public crate-ort_custom_op-0.4 (crate (name "ort_custom_op") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0b8kf2mgm5ijn1vbsvgnw41kcid9kkfgvxrxs7h793961hm675n4")))

(define-public crate-ort_custom_op-0.4 (crate (name "ort_custom_op") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "1a0d13b6vva22jz5pyhvsv1iargq9vm3ws9vvsj1zxpas2lzbrkb")))

(define-public crate-ort_custom_op-0.5 (crate (name "ort_custom_op") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "186fpj5wwlpypbcbknisdh4bd02wgpgysc3grychm63w2p5bvl3g")))

(define-public crate-ort_custom_op-0.5 (crate (name "ort_custom_op") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "11f1v72d25rganqvf8smzx61ip98p7yfiii38d6sb26ipkczkwvk")))

(define-public crate-ort_custom_op-0.6 (crate (name "ort_custom_op") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0f0p0sf2zlw2wdbsl7s4vyvgv2cqyq8sla91zkbby94g3kb3x0s3")))

(define-public crate-ort_custom_op-0.7 (crate (name "ort_custom_op") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "02czw7xvwfgxv4ycw4sfhhzc3fsylabnnwpqwl1kj5rbgf40m2pd")))

