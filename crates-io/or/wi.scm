(define-module (crates-io or wi) #:use-module (crates-io))

(define-public crate-orwith-0.1 (crate (name "orwith") (vers "0.1.0") (hash "0bk7hr2idm720jjaw8lj3cqwdgxikd76cdd17kbgw36191qpa693")))

(define-public crate-orwith-0.1 (crate (name "orwith") (vers "0.1.1") (hash "0pazwjbn60vmh4gmdcb5avl408gr3v8chsq3gf3dpkb5hpn6pk07")))

(define-public crate-orwith-0.1 (crate (name "orwith") (vers "0.1.2") (hash "0jahgbxq61h3ngbyq2dxpnh7pgvkpj72vdqpdh3a4071mwyrmhqz")))

(define-public crate-orwith-0.1 (crate (name "orwith") (vers "0.1.3") (hash "0b6f2khin5hbzal8ig5c9b90205viglrlapmzhc958lghvq2mika")))

