(define-module (crates-io or go) #:use-module (crates-io))

(define-public crate-orgora-0.1 (crate (name "orgora") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "107ql40r3axjw0dphqy8mlys09xbgq7asic6jvljjlgb71422h20")))

(define-public crate-orgora-0.1 (crate (name "orgora") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.6") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "0841pzysfs720sp0qkxxn6pw5cwzr72543gc3jx3qnzgdphqkbgv")))

