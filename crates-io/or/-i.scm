(define-module (crates-io or #{-i}#) #:use-module (crates-io))

(define-public crate-or-iterator-0.1 (crate (name "or-iterator") (vers "0.1.0") (hash "17wl81g9mqm8dggvgabr4hl8k11nfrdj40hcgx70ny70wf1sni94")))

(define-public crate-or-iterator-0.1 (crate (name "or-iterator") (vers "0.1.1") (hash "0hd1yd046gca9k1h6fvmincn1pp3g18ngr65rwwsvn0nfck5p781")))

