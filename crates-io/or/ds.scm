(define-module (crates-io or ds) #:use-module (crates-io))

(define-public crate-ordsearch-0.1 (crate (name "ordsearch") (vers "0.1.0") (deps (list (crate-dep (name "prefetch") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0qv934xbkjicvx6mfyd3q4afn6rlwg7az0vl0f7ci93nc51ar67d") (features (quote (("default") ("bench"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.0") (deps (list (crate-dep (name "prefetch") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "197m5pk50is6qz1jpqp7vzswfsma6h863l270qvpyaldcc3x8lwn") (features (quote (("nightly" "prefetch") ("default"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.1") (hash "19i0bwhrzmk8y4kppv8n0rsp9z9h1kjwczcz7mf7if42hcw4a7l1") (features (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.2") (hash "0dm0yxkd1n96abp2ppclglx33cx4z835mp2yi471dwa4v63g6g30") (features (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.3") (hash "0hxnddp5fndb3a0vpqwcqb2xbmn6q6gzjwfh484l2s82bzskrkqd") (features (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.4") (hash "1kf6ay5smc46k22igb0hdvazzfh9j2d516qa64lid420z0gj3amh") (features (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)))) (hash "1ribzzkfhk80bg36fv647x4dsgaxba6imfpkrn5vc40spgyswkhz") (features (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.6") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)))) (hash "0xkxmdfkj63nqycvyrchqq3vp84hg89aq7mhm76i476bgjg4achj") (features (quote (("nightly") ("default"))))))

(define-public crate-ordsearch-0.2 (crate (name "ordsearch") (vers "0.2.7") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6.0") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (optional #t) (default-features #t) (target "cfg(any())") (kind 0)))) (hash "1xks1n1w66m5m4316az6ls30yllfimw4rhsyhca4k5v90p8rdcpc") (features (quote (("nightly") ("default"))))))

(define-public crate-ordslice-0.1 (crate (name "ordslice") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mxm35nd4bjf27fmly1wqwn99my03vfil2gzhi34ypd8l1hd0lfj")))

(define-public crate-ordslice-0.2 (crate (name "ordslice") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0x02718apcr08h7cljcq5rk77pnv70xk6j89350jgliqa89ydr84")))

(define-public crate-ordslice-0.3 (crate (name "ordslice") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0950m13dnh1dzw1059fw65l7c2jyqimax03xrclnhdz4vg1yw86x")))

