(define-module (crates-io or kz) #:use-module (crates-io))

(define-public crate-orkz-0.1 (crate (name "orkz") (vers "0.1.0") (hash "16mqbj3pfibpxx4qllgsgcmf0yjflksw0dm9i1d5w8fv65fvlpb2")))

(define-public crate-orkz-0.1 (crate (name "orkz") (vers "0.1.1") (hash "03li5yzxk30q86ic1ylrjmpph4ms8qxs5ryrzjvyyn4zc2jkrphi")))

(define-public crate-orkz-0.1 (crate (name "orkz") (vers "0.1.2") (hash "1sj73p1xll3vn43cf5r4di0140vdqr4bk7ljsj0vdmn5yqyqlj1x")))

(define-public crate-orkz-0.2 (crate (name "orkz") (vers "0.2.0") (hash "0ryg02fg7r9i84qf5q00npch379jhj8gw1wfbxrxyld4kqw89sll")))

