(define-module (crates-io or tf) #:use-module (crates-io))

(define-public crate-ortfodb-0.3 (crate (name "ortfodb") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "0ya8xmjb30x6na6csgxzq07lqc855s2hsfmy4w95640dxn0r8n6n")))

(define-public crate-ortfodb-0.3 (crate (name "ortfodb") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "0nd99clkargn8gv6p3llwcjq1sfv28nv3ci5wfjqs1xxcz14an2b")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "158pqwq2a31yp0g1bwk2j1zzq1l82xrfp4pzva0fbkgsz5fy1l20")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "0jqfb4lwl1ik1zcfkjfdqjl9ymh7qj9v3k063mnn8r88a3mqvh78")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "1pn2xf18d09cknsyiiw62b4bc05y515hgmbi8ijkwh1xjpfdnmjb")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "07hymysc6qbvnvy7yhifpymmmcak9wczsd4i35jisgvhnsbxprpy")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "13wrsn01ir6820yrig4bmczq1yqwpvanf0f9w2djhy6ldrjzs675")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.4.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "1sbqw4318d3k5hf1d4095hm501ni69ra3xzjibpwya5570qxwjjx")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.5.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "0q5ybzb42vqk12fymc1yrdk07xl76f8jmy1f7znx1zaalg39bj35")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.6.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "16bz0hnhfh43zy1nw2jpmnc89sqap3dlx6b22njh09if36p8p6v1")))

(define-public crate-ortfodb-1 (crate (name "ortfodb") (vers "1.6.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "17r63hk0cgia8jbr0q08z77pv0sc50kb4z6ggbyh07dpkphl87f2")))

