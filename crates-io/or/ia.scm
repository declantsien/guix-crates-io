(define-module (crates-io or ia) #:use-module (crates-io))

(define-public crate-oriak-0.1 (crate (name "oriak") (vers "0.1.0") (deps (list (crate-dep (name "uderna") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p618b2az3sjvl8n5yz4kniqkvgn0c9wgbms9lqm6bgkjjn7win5")))

(define-public crate-orias-0.1 (crate (name "orias") (vers "0.1.0") (hash "12j0bws334hx26c1n4mb2790la2b6q9mjz2qdhvbc3nia9h63gfd")))

(define-public crate-orias-0.1 (crate (name "orias") (vers "0.1.1") (hash "1awswavv0psgr0b07kk4q3dv7igf4qhibcyqij6pkmkf645wnk67")))

