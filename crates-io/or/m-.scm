(define-module (crates-io or m-) #:use-module (crates-io))

(define-public crate-orm-rs-0.0.0 (crate (name "orm-rs") (vers "0.0.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1m5w417qvzar2yx5r2x0m6xdr9mlfbxs98kj1v0v46hr79ynxlpl")))

