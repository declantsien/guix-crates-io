(define-module (crates-io or fa) #:use-module (crates-io))

(define-public crate-orfail-0.1 (crate (name "orfail") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "18xf55i4gjbdy92fqnhq6dk5x0z6ki3pxwpaf3dl2pcls389516j")))

(define-public crate-orfail-0.1 (crate (name "orfail") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1r0aqx79c03k06zkih6lcavx59rj7vavmx5m0as084jvs45alc0a")))

(define-public crate-orfail-0.1 (crate (name "orfail") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1fc3nwdibynpdxks4ha6bqzqpv4mb0g0izvybc0gbm0871cplpmz")))

(define-public crate-orfail-0.1 (crate (name "orfail") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hylw60y9jdwcqlwwcaga18h6sswkz9wlwdi6k4n1vp8vlj3asb3")))

(define-public crate-orfail-1 (crate (name "orfail") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1raia366yha5ff7z4na8imn7yw6gvv5py1iy6k9zhb42n2xs5w6c")))

(define-public crate-orfail-1 (crate (name "orfail") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bj4ygq95ynp8pr72jn257g5whw26cwv1fcz43491lnx1wm238mb")))

