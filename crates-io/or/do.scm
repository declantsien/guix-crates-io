(define-module (crates-io or do) #:use-module (crates-io))

(define-public crate-ordo-0.1 (crate (name "ordo") (vers "0.1.0") (hash "1y2vdnc7bffy0caprkqd3g69nizacyjl0p0zy3n5503237kb5mm8")))

(define-public crate-ordo-0.1 (crate (name "ordo") (vers "0.1.1") (hash "0hspq06hx8ga4xzx41lz51ba4z5k567yb1jwkc5s62qnidyi4l00")))

(define-public crate-ordoo-0.1 (crate (name "ordoo") (vers "0.1.0") (hash "1m48464ff94n47dxsm1qz0mf0ach8jr221hm61a8f2pgrf78fbcb")))

(define-public crate-ordoo-0.1 (crate (name "ordoo") (vers "0.1.1") (hash "08dfqrq70mm3d618243m0w8g93bw04c68lqa6v42ica5bzl9cqcq")))

