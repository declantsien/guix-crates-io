(define-module (crates-io or tb) #:use-module (crates-io))

(define-public crate-ortb_v2_5-0.1 (crate (name "ortb_v2_5") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_path_to_error") (req "^0.1") (default-features #t) (kind 0)))) (hash "1565azq7hq8llbn414mlnzzmqajvhnfgs4mrf4f0702viv3s0bwx")))

