(define-module (crates-io or is) #:use-module (crates-io))

(define-public crate-oris-0.1 (crate (name "oris") (vers "0.1.0") (hash "0s7pkvdyfzv0liqv09a9nrfalbgn6safwijz320rzbhki0hpg8zw")))

(define-public crate-oris-0.2 (crate (name "oris") (vers "0.2.0") (hash "0skb8z7jq3a1bhgqr16z6d1bs44xlbngl5d620r3s7yms09xdj2x")))

(define-public crate-oris-0.2 (crate (name "oris") (vers "0.2.1") (hash "1yknk710qllf4hnivqz91jkf2bw45n1aixkww0pzbq9bhiq4q9rr")))

