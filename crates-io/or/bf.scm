(define-module (crates-io or bf) #:use-module (crates-io))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.0") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l2pn1b6vsxci45m5xcldywil08sx41w8l7hmh6iqh7f1ycl5cnv")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.1") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gim3v71xjw566fzxyz92wbfp24a1xmsa0461qlzi1rmwqdhqj5c")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.2") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sq7yrsnawbs8vja756ni46pvxa2q8fj899y51955kf5lq7qqp9v")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.3") (deps (list (crate-dep (name "orbclient") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "1h7icvqa7l1py45mvz2499rvnb555i9pkl5w548cjhlygzkar1a2")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.4") (deps (list (crate-dep (name "orbclient") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "1idbwmxvg33s4i3fki9rgla3iamv4f1y3jry0gmjdfdm8acr7ajq")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.5") (deps (list (crate-dep (name "orbclient") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y8361dqc2hckbmjr90wnx0yra9ag99yy2wcd3g9iygcqz31gphn")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.6") (deps (list (crate-dep (name "orbclient") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x967ya0q4msk6h729jp4kr1rd902jbayll0i4h22q44cryqchrz")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.7") (deps (list (crate-dep (name "orbclient") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bw91fxlfvc0p3yr89wyxjh4m5izpg6biy1fhc384gfzm3j1wy4s")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.8") (deps (list (crate-dep (name "orbclient") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2") (default-features #t) (kind 0)))) (hash "18k7j9kf47q5ac5ksjdps9i373w65wicavp1y4y4rhp5a74gnkfs")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.10") (deps (list (crate-dep (name "font-loader") (req "^0.7.0") (default-features #t) (target "cfg(not(target_os = \"redox\"))") (kind 0)) (crate-dep (name "orbclient") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8") (default-features #t) (kind 0)))) (hash "1d5q5kiy9khw2nbxnb7r18amjmxl7bpxmks7g50g0c9mjaw0zr4i")))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.11") (deps (list (crate-dep (name "font-loader") (req "^0.11.0") (optional #t) (default-features #t) (target "cfg(not(target_os = \"redox\"))") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "orbclient") (req "^0.3") (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (kind 0)))) (hash "0rrb3ayn651z7h32rq1y8mpg014cy43xvcr5px186vphcfddf1xb") (features (quote (("std" "rusttype/std" "font-loader" "orbclient/bundled") ("no-std" "rusttype/libm-math" "num-traits") ("default" "std"))))))

(define-public crate-orbfont-0.1 (crate (name "orbfont") (vers "0.1.12") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "orbclient") (req "^0.3") (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (kind 0)) (crate-dep (name "font-loader") (req "^0.11.0") (optional #t) (default-features #t) (target "cfg(not(target_os = \"redox\"))") (kind 0)))) (hash "18caf0zfri6xbp3sw1a6bkjsirax188z7pzmcbiypzlkywc38hzy") (features (quote (("std" "rusttype/std" "font-loader" "orbclient/bundled") ("no-std" "rusttype/libm-math" "num-traits") ("default" "std"))))))

