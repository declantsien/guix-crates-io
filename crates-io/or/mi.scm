(define-module (crates-io or mi) #:use-module (crates-io))

(define-public crate-ormio-0.1 (crate (name "ormio") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4.4") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "08jlrda221b1bfgxcyry1dy1niw0k6rv15qg94vww29pcrgjc6nc")))

