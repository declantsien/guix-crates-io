(define-module (crates-io or bu) #:use-module (crates-io))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.0") (deps (list (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sd6v39v867m1vysnlmx9kh9xl7i3kiqw7fyykc01kll9m2s49ws")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.1") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "088lbqfgyh02283zzfzpzf5kdwfailg1ifbdgr1rc7marax3np4x")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.2") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lkfvf289pnbb1g71q3wjcj8prkkmznljgh8njmjbhz22682i6iv")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.3") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jx5q8lr4pmzl3pnvyz3bpwf1r2zjnacqqq8v96mkhn7r3n2c1g9")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.4") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "0riycsaj7q2rljrjld927d8lz4r99wb1q71g4bqjcr1rf8bvj0xw")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.5") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "114lhxmn78yy5fxz72d690q73fhvpxyv9yz7w71f9plhsigv6g1k")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.6") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vvhdg3p9zj2is8fxypa7dgbdxy0qsyh3hz8fvp9pmiiiyiyxagw")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.7") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "0x7f9jqij14issjcny7rz2wv8g6862z178fmv838cpgrvb74wwn7")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.8") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wqg99j8jmwnfl6diwq1z9p4wzwzq9xj08xqy12m90z1028q3xz2")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.9") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ph3yip6pvzbfk4dfd00iv24610r84fjs2ssjd9bfg3k10liz8i3")))

(define-public crate-orbutils-0.1 (crate (name "orbutils") (vers "0.1.10") (deps (list (crate-dep (name "orbclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "orbtk") (req "^0.1") (default-features #t) (kind 0)))) (hash "0k4w507q4zr3sdqcx0gs3x7p6apa6f9rpaqwagmg5gazw7s7jglb")))

