(define-module (crates-io or na) #:use-module (crates-io))

(define-public crate-ornament-0.1 (crate (name "ornament") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "02a8602lmjmljvmlk6585gcq6g6r75k9rwir4aigdn4mqbnxbcsq") (features (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

(define-public crate-ornament-0.2 (crate (name "ornament") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1lmmf8jb6x2dfgafzgfvi14i82f60v4awpb87s2gd199vf9ji8c6") (features (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

(define-public crate-ornament-0.2 (crate (name "ornament") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zdmvv3hm4qmxi845b3a1awq971nf22v3m0vcmc3f6k9m75y750x") (features (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

(define-public crate-ornament-0.2 (crate (name "ornament") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "15am8zjkvry9vr40i6h2xas8sl03fpxbpjzxf80wv22rv3ndhncr") (features (quote (("serde_support" "serde") ("json" "serde_support" "serde_json"))))))

