(define-module (crates-io or se) #:use-module (crates-io))

(define-public crate-orset-0.1 (crate (name "orset") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "00ksk4gxwngc3chw17rvv0p0f79qgzmrqqpwnv0rz36pcfykn955")))

(define-public crate-orset-0.2 (crate (name "orset") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p32bxj1csj5fhqj5zxp1gyqayxidg461a4czfckrgmrz868cfj0")))

