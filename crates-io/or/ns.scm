(define-module (crates-io or ns) #:use-module (crates-io))

(define-public crate-ornstein-uhlenbeck-0.1 (crate (name "ornstein-uhlenbeck") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.11") (default-features #t) (kind 0)))) (hash "00zs5yxhbhkj81741imhsc57i48pf8swvvdd475s1kpk4cgc8n45")))

