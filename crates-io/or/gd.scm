(define-module (crates-io or gd) #:use-module (crates-io))

(define-public crate-orgdt-0.1 (crate (name "orgdt") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1zff731zxvc2a8ibzlnim3f0aqyc6631sah3h425pkndm4gynqv6")))

(define-public crate-orgdt-0.1 (crate (name "orgdt") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "00l9amkwjmdz0fcmds979myn1mgb9lhpijxyzsxf9bmx73qigif2")))

