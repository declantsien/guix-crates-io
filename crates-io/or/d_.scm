(define-module (crates-io or d_) #:use-module (crates-io))

(define-public crate-ord_by-0.1 (crate (name "ord_by") (vers "0.1.0") (hash "0f395cfjbh23i489ai1h2mivax45kn9hrhj13h0pb90va5kxgyw7")))

(define-public crate-ord_by-0.1 (crate (name "ord_by") (vers "0.1.1") (hash "0g9y91blqznql4izpm1idrwiyv89kqxl9nlc56xhh1gv90v9j3vb")))

(define-public crate-ord_by_key-0.1 (crate (name "ord_by_key") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18ixxrkyac657ddkhc319r5zqgrgpr12xxkw8sm1vpwg4pawykic") (yanked #t)))

(define-public crate-ord_by_key-0.1 (crate (name "ord_by_key") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01smw90rxiy4cdila0sglf2w1ay45varxnjggji5ag03nnwbng6l") (yanked #t)))

(define-public crate-ord_by_key-0.1 (crate (name "ord_by_key") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k6zqwp977j0k94rq5kmavy6iqgcarr8w260d319x8by7fkdxakb")))

(define-public crate-ord_by_key-0.1 (crate (name "ord_by_key") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "085xswdmhxypx71373z835zysyhbyjj3rgx4kdiy8gxl9svjdss2")))

(define-public crate-ord_subset-1 (crate (name "ord_subset") (vers "1.0.0") (hash "1w2mhsr8klv787p1xb44pbgka2hcz3gcwblpghcv2drwdcbryq5b")))

(define-public crate-ord_subset-1 (crate (name "ord_subset") (vers "1.1.0") (hash "0l5icd3myh71llz3029k07janj983jfwy4559lyny40krc8csmam")))

(define-public crate-ord_subset-1 (crate (name "ord_subset") (vers "1.2.0") (hash "13xbdj13mqwhbkby6r60gxr625wq7g8gxayv1i5kv33qnkxk2001")))

(define-public crate-ord_subset-2 (crate (name "ord_subset") (vers "2.0.0") (hash "1f7qy0d46lppbaap089bj8dwdvflyakfrh8gr1z0zixsn564x5bh")))

(define-public crate-ord_subset-2 (crate (name "ord_subset") (vers "2.1.0") (hash "1s050kgy8v3lvabcbkkin2hhin1pbnjj0kr9r04wcjw2fp1bif5x")))

(define-public crate-ord_subset-3 (crate (name "ord_subset") (vers "3.0.0") (hash "0qyh5b3yh3p9w3y2fs3l4s3p2f28awmkl1r7wwr0sbk60brvrprx") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-ord_subset-3 (crate (name "ord_subset") (vers "3.1.0") (hash "0h0w6a8wj4qinnddbqdak99jaygz32wn03skxnxabqbn8kifcrdw") (features (quote (("unstable") ("unchecked_ops" "ops") ("std") ("ops") ("default" "std"))))))

(define-public crate-ord_subset-3 (crate (name "ord_subset") (vers "3.1.1") (hash "1vvb6zmz279nb59dki7kbsvixbk8zpg2gxvgcpsjfnxg9ik19knp") (features (quote (("unstable") ("unchecked_ops" "ops") ("std") ("ops") ("default" "std"))))))

