(define-module (crates-io or dq) #:use-module (crates-io))

(define-public crate-ordq-0.1 (crate (name "ordq") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)))) (hash "1r1v76b4fm45ks404wsd1kvf61nfla483q8khfkdgbhlnq3680qd")))

(define-public crate-ordq-0.2 (crate (name "ordq") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1aigxwnd4g9aq1gcbx2mr9raxc75fdjnkgmz9an8qnb1p02mwlvf")))

