(define-module (crates-io or dl) #:use-module (crates-io))

(define-public crate-ordlebot-0.1 (crate (name "ordlebot") (vers "0.1.0") (deps (list (crate-dep (name "comfy-table") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mersenne-twister-m") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "roaring") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0amfw2ssw4jjlnlzffpy76knyq0b74rrs4vvd7gl270dag0115cy")))

