(define-module (crates-io or dc) #:use-module (crates-io))

(define-public crate-ordcode-0.2 (crate (name "ordcode") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "1.*") (optional #t) (kind 0)) (crate-dep (name "serde_bytes") (req "0.*") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "1.*") (default-features #t) (kind 2)))) (hash "1ag258diqqdhyvxy1vzcw58h0ndbk1rqwpfmd86allwvvqwckazl") (features (quote (("std" "serde/std") ("default" "std" "serde"))))))

