(define-module (crates-io gg ru) #:use-module (crates-io))

(define-public crate-ggrust-0.1 (crate (name "ggrust") (vers "0.1.0") (deps (list (crate-dep (name "csscolorparser") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "ggplot-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ggplot-error") (req "1.0.*") (features (quote ("csscolorparser"))) (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "plotters-svg") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1v839wzjdj8w6gdya76hhxqpmd6c1s3bbbiawdk6f7qpf1j5i9pv") (features (quote (("svg") ("default"))))))

