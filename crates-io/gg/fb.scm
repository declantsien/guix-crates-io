(define-module (crates-io gg fb) #:use-module (crates-io))

(define-public crate-ggfb-0.1 (crate (name "ggfb") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0351vdfx7x0xla9r8qdzd9la8pp7vpmzr4ksqidlqmdi0kdq5qmw")))

(define-public crate-ggfb-0.2 (crate (name "ggfb") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "08gqp2axzfk3iazksyy57qz9nr39xy3hc782cpcpivaapi3fp317")))

