(define-module (crates-io gg eg) #:use-module (crates-io))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.2") (deps (list (crate-dep (name "egui") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0w0n52in5s6ys2gzsd0psg64hrqcqqipwqd7xklla03wsllvympf")))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.3") (deps (list (crate-dep (name "egui") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0qim8gg8czlwj9c17s233j499whvv208hk8kkplyycvbcvxhlybj")))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.4") (deps (list (crate-dep (name "egui") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "160xalr3cxgi76hqq0dx0l659lz1ql81gqc6aqj25s2yvqzxgdli")))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.5") (deps (list (crate-dep (name "egui") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1v3w71lczzwf9xpjn1lrqi5vz2vd6d28l6wbxvx0n9rir14bqs5z")))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.6") (deps (list (crate-dep (name "egui") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "egui_demo_lib") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "ggez") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1v6ff3sq744k5gjxai2c7kf9wgcr94c04y1ixx9djvkzki7qwxjk")))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.7") (deps (list (crate-dep (name "egui") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "egui_demo_lib") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "ggez") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0hx0shvlg3a71vdfm5p7q5fjv4d0b7xpm5p54p5wyyy188597cyx")))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.8") (deps (list (crate-dep (name "egui") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "egui_demo_lib") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "ggez") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0bvj7k1s8j7vdbcdgxchy9sy4rapfgwa3mqgahm2hb8z7b27yakx")))

(define-public crate-ggegui-0.3 (crate (name "ggegui") (vers "0.3.9") (deps (list (crate-dep (name "egui") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "egui_demo_lib") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "ggez") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1gzgikvq0y8dz9fxkz77iw15xnknr394a3f9gkhsf01bnk7zmi4v")))

(define-public crate-ggegui-0.4 (crate (name "ggegui") (vers "0.4.0") (deps (list (crate-dep (name "egui") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "egui_demo_lib") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "ggez") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0h8fss62q2h5ghkgj0dx5zzmkpfgqa2a53111lyvmnax3ip665nr")))

