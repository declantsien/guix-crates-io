(define-module (crates-io gg up) #:use-module (crates-io))

(define-public crate-gguppy-0.1 (crate (name "gguppy") (vers "0.1.0") (deps (list (crate-dep (name "arwggplot") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gguppy_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gguppy_data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sg16srj58j8q43mryc34cv0bapjiwqg23jrhg9rw9v0kyb2ziwi") (features (quote (("save_file" "arwggplot/save_file"))))))

(define-public crate-gguppy_core-0.1 (crate (name "gguppy_core") (vers "0.1.0") (hash "1r9a2hc8zspwmvhyw5ar2z5zp70wygk27qgraigqbp6xwl0i5f0z")))

(define-public crate-gguppy_data-0.1 (crate (name "gguppy_data") (vers "0.1.0") (hash "06p7931n886j1cjh7v5cy81g6zf1npdaxk2fgabm2i3wzn5p66ky")))

