(define-module (crates-io gg ap) #:use-module (crates-io))

(define-public crate-ggapi-0.1 (crate (name "ggapi") (vers "0.1.0") (deps (list (crate-dep (name "gql_client") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.174") (default-features #t) (kind 0)))) (hash "0137dkybmfch1272sfpwyx6zkcrpjhbds6hi48kwkd64c2j7y2nk")))

(define-public crate-ggapi-0.2 (crate (name "ggapi") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "gql_client") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.174") (default-features #t) (kind 0)))) (hash "0rpx1qch7q39dzrnj73yxlwk5qahwaawpjhzf8zj870p2qk7hgi5")))

(define-public crate-ggapi-0.2 (crate (name "ggapi") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "gql_client") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.174") (default-features #t) (kind 0)))) (hash "1csna7b3fcrnsdjy243l1nz9mfy3sbj3ikvlvj7jxmr41243n52n")))

(define-public crate-ggapi-0.2 (crate (name "ggapi") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "gql_client") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.174") (default-features #t) (kind 0)))) (hash "0qhx4ghim4n8wy1dfckqh2rzpy9rrp9ljfwp34kc6bbyxgx1z4ww")))

(define-public crate-ggapi-0.2 (crate (name "ggapi") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "gql_client") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.174") (default-features #t) (kind 0)))) (hash "0s9r7pkxfdnkjwvfa0ijd4l94502kc5fx4mh3iwyd3n2f2mvxl7b")))

(define-public crate-ggapi-0.2 (crate (name "ggapi") (vers "0.2.4") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "gql_client") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.174") (default-features #t) (kind 0)))) (hash "1jnq3871wkla69ipw87ppcv5681012sxjisb9nd6mq7111g1vyzq")))

