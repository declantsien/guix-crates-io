(define-module (crates-io gg st) #:use-module (crates-io))

(define-public crate-ggstd-0.0.1 (crate (name "ggstd") (vers "0.0.1") (hash "1i71hbrqz5s9xczv8zqpk88krwd3dq4vgz26gn0fk02xgbgfw4jb")))

(define-public crate-ggstd-0.0.2 (crate (name "ggstd") (vers "0.0.2") (hash "0gwhmqpa5wg2iwv33mv8g987rbpd0wb7p81gv9284qhz64ycy35d")))

(define-public crate-ggstd-0.0.3 (crate (name "ggstd") (vers "0.0.3") (hash "0mq7axr22pj0im9nsisl1rxsnabk2i5il6cf3hpf9v8lc2nr5xjv")))

(define-public crate-ggstd-0.0.4 (crate (name "ggstd") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("everything"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "06wkzr2mxhds7vczkvny5xzssfipxl323sal53axzgnj7dv2fim6")))

(define-public crate-ggstd-0.0.5 (crate (name "ggstd") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("everything"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1yc9zvrcn52amnh8gr7w1qr2b2cm7hvjc0ggfj8dg3k43n5m16qp")))

(define-public crate-ggstd-0.0.6 (crate (name "ggstd") (vers "0.0.6") (hash "1g5drypvjnz9gq3lnwrzspi3ksjdlxgh2nj729902miyn8wr72bw")))

(define-public crate-ggstd-0.0.7 (crate (name "ggstd") (vers "0.0.7") (hash "0bjs3kcrm17r8mqc650kk3vdwzsm9dmngf2rabg1ky98347yk88s")))

(define-public crate-ggstd-0.0.8 (crate (name "ggstd") (vers "0.0.8") (hash "0b1q676iipzlgpqxv83a1bbvkkzwhf7n8j5dgcz50swkpyds0a3x")))

(define-public crate-ggstd-0.0.9 (crate (name "ggstd") (vers "0.0.9") (hash "1d0zc4jspy84aa8izcrrr4ziilcwq6yh93gvfqhj04qkzpamb8bm")))

(define-public crate-ggstd-0.1 (crate (name "ggstd") (vers "0.1.0") (hash "11v4zlbfds31rr82x9q9l5f7qii9hjz8lrdfgnnkbawdsdwl94lf")))

