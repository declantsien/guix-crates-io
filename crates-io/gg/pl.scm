(define-module (crates-io gg pl) #:use-module (crates-io))

(define-public crate-ggplot-0.1 (crate (name "ggplot") (vers "0.1.0") (deps (list (crate-dep (name "csscolorparser") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "18a3pby916zi7i2d5yggzk92194py15kpjc74w28k37agaymg29b") (features (quote (("default"))))))

(define-public crate-ggplot-0.1 (crate (name "ggplot") (vers "0.1.1") (deps (list (crate-dep (name "ggrust") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1hccjwrrk31lykwckjhpfj4d8raryxh0jgkn4sixzhw3s6qar82f") (features (quote (("default"))))))

(define-public crate-ggplot-derive-0.1 (crate (name "ggplot-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)))) (hash "1lmg0cymnmi3yid3cy7f37v2k3k7v1scq00ipr4n9kr2rrxyzz22") (features (quote (("default" "add") ("add"))))))

(define-public crate-ggplot-derive-0.1 (crate (name "ggplot-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)))) (hash "097qbspav0l8z66qy7alqxxpricmm2h3jknid38gyhipkrk2rrhv") (features (quote (("default" "add") ("add"))))))

(define-public crate-ggplot-error-1 (crate (name "ggplot-error") (vers "1.0.0") (deps (list (crate-dep (name "csscolorparser") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "0ayzrpnki9zjzc6nfi0rsfhsh8bqw9mjvmj3wrw53mxzskyzw7hy") (features (quote (("default"))))))

