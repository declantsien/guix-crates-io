(define-module (crates-io zn gu) #:use-module (crates-io))

(define-public crate-zngur-0.0.3 (crate (name "zngur") (vers "0.0.3") (deps (list (crate-dep (name "zngur-generator") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1v93k67ysjla9cqxp98pqx1z1g05dx0pb9w09w8xjy82b2fdzlgl")))

(define-public crate-zngur-0.2 (crate (name "zngur") (vers "0.2.1") (deps (list (crate-dep (name "zngur-generator") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "1srihg77s2nhf5hm0zv5ch37rqjhmhgxgiay7miwngp03hvfjaqh")))

(define-public crate-zngur-0.2 (crate (name "zngur") (vers "0.2.2") (deps (list (crate-dep (name "zngur-generator") (req "=0.2.2") (default-features #t) (kind 0)))) (hash "16h2zfj26nxkdgscvas6g42aiqzbk612jf5m9fia0rsw16n9nwmz")))

(define-public crate-zngur-0.3 (crate (name "zngur") (vers "0.3.0") (deps (list (crate-dep (name "zngur-generator") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "034paibw22bbg2qqrdaqh12qa43w58plyq1656s97kscnjajknb3")))

(define-public crate-zngur-0.4 (crate (name "zngur") (vers "0.4.0") (deps (list (crate-dep (name "zngur-generator") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "1g49gjqg12ishvlydk59lbcxfrf09zgzs2q4p9kxqz0grfj810lm")))

(define-public crate-zngur-cli-0.0.3 (crate (name "zngur-cli") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zngur") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1pprmwivks6mha98dqhdwmgyjcl1kz5ss0nrcwqkjj9wd66yh8qj")))

(define-public crate-zngur-cli-0.2 (crate (name "zngur-cli") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zngur") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "1xbvdqwa5mc7p1s1gcp9pk5i1sv3wq858lav3r3li7qmg0wwq8jf")))

(define-public crate-zngur-cli-0.2 (crate (name "zngur-cli") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.3.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zngur") (req "=0.2.2") (default-features #t) (kind 0)))) (hash "11fsnyfyv5wv9r1yrfxvlwjvwx9nb0n597r9mg3g74g0jn6fqjrn")))

(define-public crate-zngur-cli-0.3 (crate (name "zngur-cli") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zngur") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "0brmxlh9gi0vy2cw6v6wdm4hg24hq7g0i96icima3q9l44q54jab")))

(define-public crate-zngur-cli-0.4 (crate (name "zngur-cli") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.3.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zngur") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "0kcppsw5bsnnkyvxq7szzf7fa8da16caajvc8s4rdff4hzjgg4f9")))

(define-public crate-zngur-def-0.0.3 (crate (name "zngur-def") (vers "0.0.3") (deps (list (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1ic33pl7srprz2lwynf91v8h85bdj5kvich3wzjn44nz6djsrag4")))

(define-public crate-zngur-def-0.1 (crate (name "zngur-def") (vers "0.1.1") (deps (list (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1wsp85g76krih0grf48sym07zacwky4rzd66azdb95mqvgmny3mp")))

(define-public crate-zngur-def-0.2 (crate (name "zngur-def") (vers "0.2.1") (deps (list (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "02820hdqh8ldhg49q6sni0j2g4m0792qhd17fqiiqc5qddjxwc20")))

(define-public crate-zngur-def-0.2 (crate (name "zngur-def") (vers "0.2.2") (deps (list (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0zycqrpwv3scyhvdyr5cisp8fpj60b1hpmynf4crwb8f43cp0x90")))

(define-public crate-zngur-def-0.3 (crate (name "zngur-def") (vers "0.3.0") (deps (list (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "01ygsyqnjrv8ibx40is828z7i4yc0l4vwlp4xrndmsc0hjkm0528")))

(define-public crate-zngur-def-0.4 (crate (name "zngur-def") (vers "0.4.0") (deps (list (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1h8sazp0fs5x2hghz8gqnf9hmpl8hsjw7csr46flpf4vbsb2nw4k")))

(define-public crate-zngur-generator-0.0.3 (crate (name "zngur-generator") (vers "0.0.3") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "zngur-parser") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0iwnn2kq67m4yyk4ryvzr5k73r22528vnpxxy6wzljhfri7zvwsj")))

(define-public crate-zngur-generator-0.2 (crate (name "zngur-generator") (vers "0.2.1") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "zngur-parser") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "0gkqfx30r9fshm3jpmrz3hs74q6pi2snfpdlinspnj2gf5nhz9xv")))

(define-public crate-zngur-generator-0.2 (crate (name "zngur-generator") (vers "0.2.2") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "=0.2.2") (default-features #t) (kind 0)) (crate-dep (name "zngur-parser") (req "=0.2.2") (default-features #t) (kind 0)))) (hash "0grpw08rxbzf7lyncd18s0dx8dwhhwdqv0iiv976rj4jp3vkdd4r")))

(define-public crate-zngur-generator-0.3 (crate (name "zngur-generator") (vers "0.3.0") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "zngur-parser") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "0kjd6q3yir3qsjjj3x9nv4k2dvgmfn76idhfd342mi7icf6kfzz0")))

(define-public crate-zngur-generator-0.4 (crate (name "zngur-generator") (vers "0.4.0") (deps (list (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "zngur-parser") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "05vl0bkp4pap2qnhw4f005rx0vwsirpmm5fpp5pj8fw3xrm6bqds")))

(define-public crate-zngur-parser-0.0.3 (crate (name "zngur-parser") (vers "0.0.3") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0zcj6fl223k3yl3vv0a570fa2vx0vfk93qa24bfws548i3nsjs4x")))

(define-public crate-zngur-parser-0.2 (crate (name "zngur-parser") (vers "0.2.1") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "1gp88kb7jlypwvff1dzdjg1292ssf3p3r7hybp7srd688zy1jpzf")))

(define-public crate-zngur-parser-0.2 (crate (name "zngur-parser") (vers "0.2.2") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zngur-def") (req "=0.2.2") (default-features #t) (kind 0)))) (hash "1w3i6gczb2r6q5dj3a97qm2dn53ryrpjck6256qyn87ri5qc3v8h")))

(define-public crate-zngur-parser-0.3 (crate (name "zngur-parser") (vers "0.3.0") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.4.1") (default-features #t) (kind 2)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "strip-ansi-escapes") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "zngur-def") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "01gj1f27p6maghbkbrcal3jvmgq7nhkjcdn5ap8s13mf4hw0jn8s")))

(define-public crate-zngur-parser-0.4 (crate (name "zngur-parser") (vers "0.4.0") (deps (list (crate-dep (name "ariadne") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "=1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.4.1") (default-features #t) (kind 2)) (crate-dep (name "iter_tools") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "strip-ansi-escapes") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "zngur-def") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "0slvbsw9p9wq0l9jzwxzi4rj05f669fcsylf3gj1fharg8wlxyk8")))

