(define-module (crates-io bh ar) #:use-module (crates-io))

(define-public crate-bharat-cafe-0.1 (crate (name "bharat-cafe") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0r1r9c8rawif4dfqq2x0k0p3bjhxfmhc3rv8i6ad0v5fcka020nr")))

(define-public crate-bharat-cafe-0.1 (crate (name "bharat-cafe") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12fbnjjz6549vf0rhw1yp2lj8hs9dqnbgnxn90kqghimrag45awj")))

(define-public crate-bharat-cafe-0.1 (crate (name "bharat-cafe") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0waf6mdyyfym139hbvlx7paxf8hk19m1jc4fxiykfqgp86fnlp35")))

(define-public crate-bharat-cafe-0.2 (crate (name "bharat-cafe") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vs2dng0rydwdp9prgsh1bqf8xk58dwg5mali599xf5h2nm39qlx")))

(define-public crate-bharat-cafe-0.2 (crate (name "bharat-cafe") (vers "0.2.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xkz0n14pn8n2zrw2246d393fc7hri691764svfmbrzka7yn5qa8")))

