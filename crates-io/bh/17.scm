(define-module (crates-io bh #{17}#) #:use-module (crates-io))

(define-public crate-bh1730fvc-0.1 (crate (name "bh1730fvc") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (target "cfg(all(target_arch = \"arm\"))") (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "00nvrazbdfxjvvy73yx8x85n5cjxqrd6sy94ghkkj4qj0jllhpxy")))

(define-public crate-bh1730fvc-0.1 (crate (name "bh1730fvc") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (target "cfg(all(target_arch = \"arm\"))") (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "19djhbvglnq7vj4jdd5j2ny7xanfpw42jy6skxp9s8lxl9yqmxwi")))

(define-public crate-bh1730fvc-0.2 (crate (name "bh1730fvc") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (target "cfg(all(target_arch = \"arm\"))") (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "1ilsrn6zif2ga50iww283qfxxcwav8cnpdan8bhh6f4zyi5mrp33") (features (quote (("async")))) (v 2) (features2 (quote (("unittesting" "dep:embedded-hal-mock"))))))

(define-public crate-bh1750-ehal-0.0.1 (crate (name "bh1750-ehal") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13qlzsqs53bcjjn0lhyqjswf3cbjsh87lnjshjh4923rp45125bj")))

(define-public crate-bh1750-ehal-0.0.2 (crate (name "bh1750-ehal") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0adxr222qcpv0grwzpcabiv826src5pgzwjrf7g4khvpv1q6hrm5")))

