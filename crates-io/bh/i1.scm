(define-module (crates-io bh i1) #:use-module (crates-io))

(define-public crate-bhi160-0.1 (crate (name "bhi160") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11") (default-features #t) (kind 0)))) (hash "1w5kw47jnv11cn448z2vncnl2zns7yp41xmixk6qyxi3mvpqbgza") (v 2) (features2 (quote (("log" "dep:log"))))))

