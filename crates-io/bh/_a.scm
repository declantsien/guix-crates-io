(define-module (crates-io bh _a) #:use-module (crates-io))

(define-public crate-bh_alloc-0.1 (crate (name "bh_alloc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mb0m6wim8hmvqsdaw1j4h61g3rqi0iwzlhshl11mynnwijgvlhf")))

(define-public crate-bh_alloc-0.1 (crate (name "bh_alloc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x82q72jwrz5bxnv9j38pf3z16k56csn0pq9n1ikdxhlsdcsqydl")))

(define-public crate-bh_alloc-0.2 (crate (name "bh_alloc") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)))) (hash "0wrpvpqpx2nwxn4xffxrw76adaasmqvfjlw39s2ygqzkrh9z56dy")))

(define-public crate-bh_alloc-0.2 (crate (name "bh_alloc") (vers "0.2.2") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h2gj4fwc2nr4501qmzrsniiwrqj8i8lsqpn1mvj2i4j9xbzlpc6")))

(define-public crate-bh_alloc-0.2 (crate (name "bh_alloc") (vers "0.2.3") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1782dm29j56hjm65vb3b21b19wvzwyga98f2nrix8j8sxyh3mcqd")))

(define-public crate-bh_alloc-0.2 (crate (name "bh_alloc") (vers "0.2.4") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 2)))) (hash "0g9sc2a6prhxbxwfdi5p4hjql5i4g41699cz94wki70b47bwl223")))

