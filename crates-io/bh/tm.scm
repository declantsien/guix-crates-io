(define-module (crates-io bh tm) #:use-module (crates-io))

(define-public crate-bhtmp-0.1 (crate (name "bhtmp") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "053ny6k6fbxrjyaj7j7fx5cg10h0kqir6fw5k174zkyqixsw75b0") (yanked #t)))

(define-public crate-bhtmp-1 (crate (name "bhtmp") (vers "1.0.0") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "0fl4kz5l7hglby1cmv1lzp58ijxfi2cmsc1q7qnd20wqrbkdc88q")))

