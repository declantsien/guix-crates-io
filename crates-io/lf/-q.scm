(define-module (crates-io lf -q) #:use-module (crates-io))

(define-public crate-lf-queue-0.1 (crate (name "lf-queue") (vers "0.1.0") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "0s7d27aj5d9haphs6iwqri8iqb4d4avbh7dpxbcy917qf06lab2f")))

