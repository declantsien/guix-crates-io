(define-module (crates-io lf t-) #:use-module (crates-io))

(define-public crate-lft-rust-0.1 (crate (name "lft-rust") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "05s0wnqn11cd9fmc1f0jy82yhcb1qjqsyk1xw3jzdj60vj9rh1v3") (rust-version "1.57.0")))

