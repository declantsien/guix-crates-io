(define-module (crates-io lf sr) #:use-module (crates-io))

(define-public crate-lfsr-0.1 (crate (name "lfsr") (vers "0.1.0") (hash "1d652b9cizyyafh72fb0mcnb4q6wij877p0npqg8gmywihnqm9hc")))

(define-public crate-lfsr-0.2 (crate (name "lfsr") (vers "0.2.0") (deps (list (crate-dep (name "lfsr-base") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-instances") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-macro-generate") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-macro-lookup") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1l5vk7bhs528kscdfv7ih95z7ix5s3kzj6pb6plq7nfjh5d2x53v")))

(define-public crate-lfsr-0.3 (crate (name "lfsr") (vers "0.3.0") (deps (list (crate-dep (name "lfsr-base") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-instances") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-macro-generate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-macro-lookup") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0mvw0k85klmhwzcbgr34kvpbxg7m8xbbhimag0m547y9kxawa20l")))

(define-public crate-lfsr-base-0.2 (crate (name "lfsr-base") (vers "0.2.0") (hash "1b8ndrs4sj0yxbgcdv7hbm9wlqp6khvhgqy8ps3mrmairwx8m3pn")))

(define-public crate-lfsr-base-0.3 (crate (name "lfsr-base") (vers "0.3.0") (hash "0v4l5m5i5kcnbldvqk89zddaf9qhgcciciqm3zsb4s8cxzjghm8k")))

(define-public crate-lfsr-instances-0.2 (crate (name "lfsr-instances") (vers "0.2.0") (deps (list (crate-dep (name "lfsr-base") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-macro-generate") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0xjz4qp7kdhwh8nszvbgi04s2kd09ay15x0w5279hq7mpkq81dgj")))

(define-public crate-lfsr-instances-0.3 (crate (name "lfsr-instances") (vers "0.3.0") (deps (list (crate-dep (name "lfsr-base") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-macro-generate") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0xgvbfaayx9r0bjzsss66zx8pdj6yfqzz76qqsawki147d7gvhj0")))

(define-public crate-lfsr-macro-generate-0.2 (crate (name "lfsr-macro-generate") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-base") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1x3bs85axjvn79jjbf3vvskg96g7dl4fi39fvm5z194dxfx145bx")))

(define-public crate-lfsr-macro-generate-0.3 (crate (name "lfsr-macro-generate") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-base") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1xng1qq3m8lxvmayp306lm0qh8lc8r4l158q7qxh2zaxkrsbx5xp")))

(define-public crate-lfsr-macro-lookup-0.2 (crate (name "lfsr-macro-lookup") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-base") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-instances") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "10adxwa43p9011gbll93a52z0p310vj7y8bk4fl0062fd3g67rxg")))

(define-public crate-lfsr-macro-lookup-0.3 (crate (name "lfsr-macro-lookup") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-base") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lfsr-instances") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1y6dc8sdjkcyp030silizp4amsk5nm5c3bclkwfciam7hmm3lfmc")))

