(define-module (crates-io lf _l) #:use-module (crates-io))

(define-public crate-lf_lint-0.1 (crate (name "lf_lint") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "content_inspector") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)))) (hash "0xck8wvsvgk57jwwkxjapvxfizkp6f1h8anqy7w3y7dy1dn9wfhw")))

