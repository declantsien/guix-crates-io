(define-module (crates-io lf -s) #:use-module (crates-io))

(define-public crate-lf-sll-0.0.1 (crate (name "lf-sll") (vers "0.0.1") (hash "10z3lnvwwg0sjid1h6k2hpzmikhj3fjy7xk3hnpqn2fpb1zrksjr")))

(define-public crate-lf-sll-0.0.2 (crate (name "lf-sll") (vers "0.0.2") (hash "0hb7ddbndnszyjwj7cdywz6jswba654jff7bxrc9r3ds7xh2mcp5")))

(define-public crate-lf-sll-0.0.3 (crate (name "lf-sll") (vers "0.0.3") (hash "00z9a262bpa0z3c1027770xq4i5g8dw70blnd4iblfsrmifa2z5c")))

