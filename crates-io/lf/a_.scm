(define-module (crates-io lf a_) #:use-module (crates-io))

(define-public crate-lfa_derive-0.1 (crate (name "lfa_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0srzivw3irdxwyb29v9lasxrhdnycj066nn2l9v4mhaggwdnirad")))

(define-public crate-lfa_derive-0.12 (crate (name "lfa_derive") (vers "0.12.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "15gh3p4kp14s285vxig6fnb6k8v854qsaqg2mwfwc60m43bd9jf1")))

(define-public crate-lfa_derive-0.13 (crate (name "lfa_derive") (vers "0.13.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "07a831fc7xrnd499x5npf6i47hi8xbjdsahc571bk4axhs9nl2xj")))

