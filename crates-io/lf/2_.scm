(define-module (crates-io lf #{2_}#) #:use-module (crates-io))

(define-public crate-lf2_codec-0.1 (crate (name "lf2_codec") (vers "0.1.0") (hash "1n1k9jizsh79ivcy0r5ajljkj0madahf3sfni98ckmqsyi82ld62")))

(define-public crate-lf2_codec-0.2 (crate (name "lf2_codec") (vers "0.2.0") (hash "1j03q4qnw50zljcn12ca8bqw61rijw7mwrgzzfljznp0mm9wh13x")))

(define-public crate-lf2_codec-0.2 (crate (name "lf2_codec") (vers "0.2.1") (hash "0ys1ib1a0561wya4mpaq9dfq25zliabkjrbqphkpx6ig29a6hwjh")))

(define-public crate-lf2_parse-0.1 (crate (name "lf2_parse") (vers "0.1.0") (deps (list (crate-dep (name "lf2_codec") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.0.1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "0ylf58faldr04c2xzhj3pjhxng5zj0zc2vzj57mljgdvxr3r772a")))

