(define-module (crates-io lf bs) #:use-module (crates-io))

(define-public crate-lfbs-0.1 (crate (name "lfbs") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 2)))) (hash "1q46wy3gla43gw8rvhqwa5ngnnkyrcyb6j3i7w3crww726m5bxz1")))

