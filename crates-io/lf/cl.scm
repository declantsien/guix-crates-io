(define-module (crates-io lf cl) #:use-module (crates-io))

(define-public crate-lfcl-0.0.1 (crate (name "lfcl") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "~2.26") (kind 0)) (crate-dep (name "lfclib") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1adxpxhqq5nvwc81hacbxhfmaizik2sj1gl8vfnp12s3fhmwspzv")))

(define-public crate-lfcl-0.0.2 (crate (name "lfcl") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "~2.26") (kind 0)) (crate-dep (name "lfclib") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1vhjx65d6li811mnsnzhi1vp2ra4pjk0x1bhdg5855w8p574xlx7")))

(define-public crate-lfclib-0.0.1 (crate (name "lfclib") (vers "0.0.1") (deps (list (crate-dep (name "libusb") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bxc2j40r1m4m06m75wzqcq1q6sfwmm138rbha6rjrjlm4wzg15v")))

(define-public crate-lfclib-0.1 (crate (name "lfclib") (vers "0.1.1") (deps (list (crate-dep (name "hidapi") (req "~0.4") (default-features #t) (kind 0)))) (hash "0dip0i3xmkziqnkw5ycdf57clqrssj41qxw14g01bw2qi7hxiwkg")))

