(define-module (crates-io lf ml) #:use-module (crates-io))

(define-public crate-lfml-0.1 (crate (name "lfml") (vers "0.1.0") (deps (list (crate-dep (name "axum-core") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "http") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "lfml-escape") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lfml-html5") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lfml-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lc1axr5g3g16xgs5ha6hc647nbkbzgayv3x842jjx8l6fq5c9d4") (features (quote (("default" "axum") ("axum" "axum-core" "http"))))))

(define-public crate-lfml-escape-0.1 (crate (name "lfml-escape") (vers "0.1.0") (hash "1my7g2zw83flfsks8kj67vl6ibvin3kn4sgp645ny2f5zl1gcdgf")))

(define-public crate-lfml-html5-0.1 (crate (name "lfml-html5") (vers "0.1.0") (hash "0n5lsfnsd9rzxhqbav5ssanf5lp6syh2ry129yyiba1xd1f2mpmh")))

(define-public crate-lfml-macros-0.1 (crate (name "lfml-macros") (vers "0.1.0") (deps (list (crate-dep (name "lfml-escape") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lfml-html5") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "02wwnp9mg4mmrghdmjmxmfzj46rrrls6s233wzhfgw3ybfdkn7ws")))

