(define-module (crates-io z1 in) #:use-module (crates-io))

(define-public crate-z1info_rust-0.1 (crate (name "z1info_rust") (vers "0.1.0") (hash "0n4bcjy997fpxw42mg5l74cckf8cl0687iw0nzy74v14cx9yn6zi") (yanked #t)))

(define-public crate-z1info_rust-0.1 (crate (name "z1info_rust") (vers "0.1.1") (hash "1wn9pkkxcqa4pp5idprzf98rg4mikm2cvx6gd0nmvz98v2wcvb5z")))

(define-public crate-z1info_rust-0.1 (crate (name "z1info_rust") (vers "0.1.2") (hash "0zbypki726vqzda3a1w9y7m65rq595q84c5vnkqw2yyc7nfcd1bk")))

(define-public crate-z1info_rust-0.1 (crate (name "z1info_rust") (vers "0.1.3") (hash "1zzqyqgh7w0gpzx52016w4c1wvis8kxssc9xiv2yvx6bd7nppic4")))

(define-public crate-z1info_rust-0.1 (crate (name "z1info_rust") (vers "0.1.4") (hash "15hx8ffm1rza9dwlwlc39hnpv9hmnk5qlq7vsm6j4h1l6xfz4549")))

