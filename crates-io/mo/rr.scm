(define-module (crates-io mo rr) #:use-module (crates-io))

(define-public crate-morristown-0.1 (crate (name "morristown") (vers "0.1.0") (hash "175747b7nxylh74mjxg3b7dlq3aj90bml5lan7jkd0j51hbqa4w7")))

(define-public crate-morristown-0.1 (crate (name "morristown") (vers "0.1.1") (hash "09cg3cvy7x4kmx1xnphvkc03b8vhhfsjs6fk36kyfvj39png13lx")))

(define-public crate-morristown-0.1 (crate (name "morristown") (vers "0.1.2") (hash "13fwyr3nrf5r16r541q549pyph9dr3p7qv0jn22sw37xa3j42b20")))

(define-public crate-morristown-0.1 (crate (name "morristown") (vers "0.1.3") (hash "1szxw2zsgx9d5p2wvqdq3l7jzqbhss211fc7a6sa75xn0ihnhmz0")))

(define-public crate-morristown-0.1 (crate (name "morristown") (vers "0.1.4") (hash "1xi5yw5d4ksyh52j17nvnilhyyhs05s5f4i554fbjw33v4gijfzq")))

(define-public crate-morrsse-0.1 (crate (name "morrsse") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0wsxzl4ndcz537zmiq6807x8hlq6cxldkljnn2qp12kdwcfrzy0y")))

(define-public crate-morrsse-0.1 (crate (name "morrsse") (vers "0.1.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)))) (hash "1plg8h6nmpwvfdh8gbxhlmz1h1hwwic258jsjql3jylb726z9m03")))

