(define-module (crates-io mo ca) #:use-module (crates-io))

(define-public crate-mocats-0.1 (crate (name "mocats") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "04snip55a2m17s08pvzqikb2ylcfcbh4cmd30iwys0pv0ifl28w3")))

(define-public crate-mocats-0.2 (crate (name "mocats") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0fg9iqcw87l581nqhqg06rp9kbg6limg7qjg57rmp2lpq4b126rd")))

(define-public crate-mocats-0.2 (crate (name "mocats") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0b2zdfwwy458amhn8s8bikxpfqzh4dp5442v9l2kdymid8n5cbhz")))

