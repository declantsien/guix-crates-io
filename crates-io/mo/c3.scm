(define-module (crates-io mo c3) #:use-module (crates-io))

(define-public crate-moc3-0.1 (crate (name "moc3") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0pzxhnqcbnzqcczq3bldwsacwl8fnnwai4ngcxipqvcwv1izrmz1")))

