(define-module (crates-io mo c-) #:use-module (crates-io))

(define-public crate-moc-rich-presence-0.1 (crate (name "moc-rich-presence") (vers "0.1.0") (deps (list (crate-dep (name "discord-rich-presence") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "moc-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ylqw8accl1icdvh5vxh3dck75w0bvknc40yf00shbni4rn80r1h")))

(define-public crate-moc-rs-0.1 (crate (name "moc-rs") (vers "0.1.0") (hash "13a4v88k7iilcihkz94g0j25qzswja5xy82qkr40q074s0y59chx")))

(define-public crate-moc-rs-0.1 (crate (name "moc-rs") (vers "0.1.1") (hash "13kx1a1sf0rk547s875jlbhz8bz4zynhfbr2f8240mgcmc0zkkax")))

(define-public crate-moc-rs-0.1 (crate (name "moc-rs") (vers "0.1.2") (hash "0y5g4wgaiji6f90al1cikf7d3qn9n508sq8q3xvjr60zsxwa5rgy")))

(define-public crate-moc-rs-0.1 (crate (name "moc-rs") (vers "0.1.3") (hash "0i4q4yna3ahf6xbpvps16khfsk6ni5yrryd3ayp4f3ciqpbbx8jn")))

