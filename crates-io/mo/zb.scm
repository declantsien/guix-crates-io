(define-module (crates-io mo zb) #:use-module (crates-io))

(define-public crate-mozbuild-0.0.0 (crate (name "mozbuild") (vers "0.0.0") (hash "1c8aghbgrr7f4s19lhm6q9w9w3x1yqjp0agafbw8s49949lvfxq9")))

(define-public crate-mozbuild-0.1 (crate (name "mozbuild") (vers "0.1.0") (hash "05l6w86bcga9hl734ynqwfklw35sz23z7y2c45sp53945yp70fch")))

