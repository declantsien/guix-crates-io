(define-module (crates-io mo sc) #:use-module (crates-io))

(define-public crate-moscato-0.1 (crate (name "moscato") (vers "0.1.0") (deps (list (crate-dep (name "pinot") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1mahf9alydaxdfam2lpf26f8hpfnapgf2b439d42qs3n6cz69hf8")))

(define-public crate-moscato-0.1 (crate (name "moscato") (vers "0.1.1") (deps (list (crate-dep (name "pinot") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "13d30fpg26rnmacj32dnz6fjfccx8qfhi7daa4wjh6hgwfy0k12p")))

(define-public crate-moscato-0.1 (crate (name "moscato") (vers "0.1.2") (deps (list (crate-dep (name "pinot") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0r3w815434q4a01alaj7d0dfy4f5v74dnklw19sk3i5jr36zcwl3")))

