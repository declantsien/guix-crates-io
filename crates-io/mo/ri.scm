(define-module (crates-io mo ri) #:use-module (crates-io))

(define-public crate-mori-0.1 (crate (name "mori") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0bgv1f5plzy0xgnc5l6i3radjammnf79ca6baw9197glsakzl86v")))

(define-public crate-mori-0.1 (crate (name "mori") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0jgbyv6cvj90srhg3jlzcxac1b7nzysf0bcqlqj9ycjxapcjkr8x")))

(define-public crate-mori-0.1 (crate (name "mori") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0dacvm1iqwv9zsnazv4pi77nrj6z2qac0zarx2lqc7vd8xv607k7")))

(define-public crate-mori-0.2 (crate (name "mori") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.13.0") (features (quote ("approx"))) (default-features #t) (kind 0)))) (hash "0a33ysi6dvfppk5cqh0v7lpvxj54v730m8ryfnk8cyn3cjbj4y9d") (features (quote (("parallel" "ndarray/rayon"))))))

(define-public crate-mori_parallel-0.1 (crate (name "mori_parallel") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray-parallel") (req "^0.8") (default-features #t) (kind 0)))) (hash "14z95qdpmfc1jpqz9lfwg3dfi1ysdlp73gixhg4ckjqzqxxqzrsq")))

(define-public crate-mori_parallel-0.1 (crate (name "mori_parallel") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray-parallel") (req "^0.8") (default-features #t) (kind 0)))) (hash "0k0brwb9qygl644j41rdl6b22m9565pqsd3kywqcm64s7sgn63j5")))

(define-public crate-mori_parallel-0.1 (crate (name "mori_parallel") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray-parallel") (req "^0.8") (default-features #t) (kind 0)))) (hash "098kksxpgy2qsvm8k9i1i3mn5nkiyzgpx2sh054ivv2i1c87cxa6")))

(define-public crate-mori_parallel-0.1 (crate (name "mori_parallel") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray-parallel") (req "^0.8") (default-features #t) (kind 0)))) (hash "18943nrrgqmwws1f46ip9q93scimhb3x8yg42abwlrnl90b31nn9")))

(define-public crate-moria-0.0.0 (crate (name "moria") (vers "0.0.0") (hash "0x8ygh04lvbl0wfbw4gg2x54y8r5kb9i9fxic6gvs0s43wchkj5v")))

