(define-module (crates-io mo mm) #:use-module (crates-io))

(define-public crate-mommy-rs-0.1 (crate (name "mommy-rs") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1w4c5lsmwn666xgr3l76p3jnhw2hald2zgq0gjqp4xqi5qwclmlg")))

