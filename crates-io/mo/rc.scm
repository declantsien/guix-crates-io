(define-module (crates-io mo rc) #:use-module (crates-io))

(define-public crate-morc-0.0.1 (crate (name "morc") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "17qyq9hkwzyb5a4araf2lrmjkg5yd3pmdsd00f80bywmp5if2n8l") (rust-version "1.72")))

(define-public crate-morc-0.0.2 (crate (name "morc") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0jj015pifis4gspj07addcg4l19k19k47sh8r9b2b3wjyq32qr6s") (rust-version "1.72")))

