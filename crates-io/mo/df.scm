(define-module (crates-io mo df) #:use-module (crates-io))

(define-public crate-modfile-1 (crate (name "modfile") (vers "1.3.2") (hash "1z0zpqj12bfwbb7fykl90xvl3nv61iss42nh20g5rc16v1a1c9w6")))

(define-public crate-modfile-1 (crate (name "modfile") (vers "1.3.4") (hash "117p2ls7k7f0mlsji0fvqgpg4fvf5fd11bkfyx4wjjd1j4sw57ih")))

(define-public crate-modfile-1 (crate (name "modfile") (vers "1.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-big-array") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m2sfd6gi33c430z6skxn4c5g3l73hmphnb04palhnasr5i7yh9k") (yanked #t)))

(define-public crate-modfile-1 (crate (name "modfile") (vers "1.4.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-big-array") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vfzy9235rmbd7lcr6ivcnff1yc6pcd9j28pma6m2vrsprdfry1g") (yanked #t)))

(define-public crate-modfile-1 (crate (name "modfile") (vers "1.4.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-big-array") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0apnigay6v476zfc8fdbynsimwmbla7dpn3vb45w9313lncm8lz1")))

