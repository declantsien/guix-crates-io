(define-module (crates-io mo zv) #:use-module (crates-io))

(define-public crate-mozversion-0.1 (crate (name "mozversion") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 0)))) (hash "0sdiny4mp0bnygyp425xqr9npxncz3xwsar433ij8xavk0lrcidr")))

(define-public crate-mozversion-0.1 (crate (name "mozversion") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 0)))) (hash "1w3260s3pqggp0aqrggx46plcgjiwk2xvq1151hlmjdq9lhfmb3n")))

(define-public crate-mozversion-0.1 (crate (name "mozversion") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 0)))) (hash "09lgy3fyvc6570rp4gx9qn8hrbxyg6aa9d1y1mb12djm6l0s9cwz")))

(define-public crate-mozversion-0.1 (crate (name "mozversion") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 0)))) (hash "0qaq52z0xc6y1ybdxmb559pmj6fcs20amr5c4gl19yhw0g4i4x2r")))

(define-public crate-mozversion-0.2 (crate (name "mozversion") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 0)))) (hash "1zj8hx7fzy7022dnlliy64wlg7fisqzl7dvd4wa6mslbfa158a9g")))

(define-public crate-mozversion-0.2 (crate (name "mozversion") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 0)))) (hash "07kc30b5fhcawf6c7z006h4xwgvq01hdya1mw01qyhavfqclcz5w")))

(define-public crate-mozversion-0.3 (crate (name "mozversion") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)))) (hash "1rkc3w7h96f1kra6vi5412l7m716xv3ik68n48jscg13sw8y3nh6")))

(define-public crate-mozversion-0.4 (crate (name "mozversion") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)))) (hash "0adhaxlci5yd6x98a5nnqkiksljbv5xhhgqqwphk65ifvd3x1rh4")))

(define-public crate-mozversion-0.4 (crate (name "mozversion") (vers "0.4.1") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)))) (hash "1dqw346xc52qgd5ff6nc8x9bylhb0w23fs3wqrybx1vi4pi96g6k")))

(define-public crate-mozversion-0.4 (crate (name "mozversion") (vers "0.4.2") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)))) (hash "04hk4gpi5r7ba4f3hfzjc2y8d222i410phg11pwd7hci7fqhf1z4")))

(define-public crate-mozversion-0.4 (crate (name "mozversion") (vers "0.4.3") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)))) (hash "1k037ng3fj6jypgs1m93wwbgb41cv4iwd13vz67lqiz18b7zjvr3")))

(define-public crate-mozversion-0.5 (crate (name "mozversion") (vers "0.5.0") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wfiqif4bb40sc3h16jn3mh24spc22d3qy4qi06a9rwml331l8iy")))

(define-public crate-mozversion-0.5 (crate (name "mozversion") (vers "0.5.1") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mklqnvgmc6hg6hkadzlm6g2fv666ly6h6xsw8yzllnc6iprj72y")))

(define-public crate-mozversion-0.5 (crate (name "mozversion") (vers "0.5.2") (deps (list (crate-dep (name "regex") (req "^1") (features (quote ("perf" "std"))) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "13li5a7wcbc0862rqkadadj709fp56dkcav9v9p085ii86saiw5b")))

