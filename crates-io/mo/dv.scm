(define-module (crates-io mo dv) #:use-module (crates-io))

(define-public crate-modVM-0.2 (crate (name "modVM") (vers "0.2.0") (hash "1ygpvnhi6qa57vszk9b4k5m5049hmxhmr59w7ddqwb03nc2nrmmf")))

(define-public crate-modVM-0.2 (crate (name "modVM") (vers "0.2.1") (hash "1rl1if50c1drqb8qzy861mbmalwdhb4620535rhpz6slylg6q22d")))

(define-public crate-modVM-0.3 (crate (name "modVM") (vers "0.3.0") (hash "19pksf6ml3wb8ysayr7gng9c4ms4vgm4jasfdxz9spy0gqdrgpj3")))

