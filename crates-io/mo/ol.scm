(define-module (crates-io mo ol) #:use-module (crates-io))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.0") (deps (list (crate-dep (name "claims") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1dqpg16z2hq13w31cfjpgmmbhqw0ivqvgwilhahyzwkabqcyw4y1")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.1") (deps (list (crate-dep (name "claims") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1mn7xqsnpxfnvcbsxfgyjbsv3wi8nanwszw9ms4bz2xkqzc5i4in")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.2") (deps (list (crate-dep (name "claims") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0w2xhk5a9clnk7yylj0inycs1xfd3kfhnk7mm31wk6b6rddn9w0l")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.3") (deps (list (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0migd1bd1y6yy8sji6i33ldi3znmhhxxwsyidh9dq9nvgll68in6")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.4") (deps (list (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1wxr70pw1g49zyvxcrx4bkpk3cshh1vp0vms8kiwj0ycry0g4j9m")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.5") (deps (list (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18") (kind 2)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)))) (hash "12p9wj464zmxyrw300mhz3b0qnqbayygkhilndravvjynm9hqfm6")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.7") (deps (list (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18") (kind 2)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0y71nrzdzcd2vs5w7ba1hfzc1df0cyaqp03f6750arrb835kwabl")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.8") (deps (list (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18") (kind 2)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "01g4fbdfmac5asslcy4yfcziy7sdqafs0nj3708lzv4dypvi9bqs")))

(define-public crate-moolah-0.1 (crate (name "moolah") (vers "0.1.9") (deps (list (crate-dep (name "mudra") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18") (kind 2)) (crate-dep (name "rust_decimal") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0nhpxzz69ywkh05girpf8a5il166699ril88w8id1c7sxf9wcg9b")))

