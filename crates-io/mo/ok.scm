(define-module (crates-io mo ok) #:use-module (crates-io))

(define-public crate-mook-0.1 (crate (name "mook") (vers "0.1.0") (hash "1mh9x2yq3qp5n13nx5gpyfhjxmbsmwlbkqmw3wx2xdbl2j0iik7c")))

(define-public crate-mook-0.1 (crate (name "mook") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "tindercrypt") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0m0sgpc0p381hlhn1x45n3x9k9ifda47znpky2pmllqf5l4ij0c6")))

