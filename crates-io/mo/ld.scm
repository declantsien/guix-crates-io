(define-module (crates-io mo ld) #:use-module (crates-io))

(define-public crate-mold-0.0.1 (crate (name "mold") (vers "0.0.1") (deps (list (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1v0d4cqk2syl9m0camakhnml223ir9v3dq74yqm3jdj1g0ihpnfl")))

(define-public crate-mold2d-0.0.1 (crate (name "mold2d") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "sdl2_image") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sdl2_ttf") (req "^0.9") (default-features #t) (kind 0)))) (hash "030gqhwy2y95ixkihjfwwpyynqx8h2rdabwpm5hardppsi55lhyz")))

(define-public crate-mold2d-0.0.2 (crate (name "mold2d") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "sdl2_image") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sdl2_ttf") (req "^0.9") (default-features #t) (kind 0)))) (hash "0vndjn0jphkljqigd0pinyhlp7j8hsfsai0cc0wa4bw87q0flw5m")))

(define-public crate-moldenfile-0.1 (crate (name "moldenfile") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.7") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1918czxzdb2h3mq01qsr7r2jirakya210ml7ijrb6lfj8crqsqys")))

