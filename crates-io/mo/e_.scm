(define-module (crates-io mo e_) #:use-module (crates-io))

(define-public crate-moe_logger-0.1 (crate (name "moe_logger") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-uring") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q96s6xfg9ijxnhlnl04s0cqg8iqlh170xw7v682y59w96fqbaf1")))

(define-public crate-moe_logger-0.2 (crate (name "moe_logger") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-uring") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wjwwk1ld13x2n1pgj40d8kczihbjl081yawzx32qvx9yyy47zkg")))

