(define-module (crates-io mo fu) #:use-module (crates-io))

(define-public crate-mofurun-0.1 (crate (name "mofurun") (vers "0.1.0") (hash "07szgmad9266988137gx484lzgmay544a2bvq05y6csrsg67pyvf") (yanked #t)))

(define-public crate-mofurun-0.1 (crate (name "mofurun") (vers "0.1.1") (hash "0h7wg3yy1b6r06yxxn6hzxkmhiwy0cl5mhs147iyjh8v7x5f1crc")))

(define-public crate-mofurun-0.1 (crate (name "mofurun") (vers "0.1.2") (hash "1l76vn34va9iw10qd3l8mbjlw7bcni3bjmk8mr6x05a89haqx2ml")))

(define-public crate-mofurun-0.1 (crate (name "mofurun") (vers "0.1.3") (hash "151kll5sls61mhl1jpgnpwss88nh4rwzwijja9x88505vi7wajgg")))

(define-public crate-mofurun-0.3 (crate (name "mofurun") (vers "0.3.0") (hash "0c9qrl6lrs9gfgq6rq9cb820pbl3jazab6c91jr85x9rgcgm3ln5")))

(define-public crate-mofurun-0.4 (crate (name "mofurun") (vers "0.4.0") (hash "1mzikdkgvs10if6msvznz56n9sqdc6xr3cqschnhn9j084p3z9bf")))

