(define-module (crates-io mo sa) #:use-module (crates-io))

(define-public crate-mosaic-0.1 (crate (name "mosaic") (vers "0.1.0") (deps (list (crate-dep (name "ggez") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "137s260h9sf38ng6cnph9qwxndmidibb5s7f45flz94l7zsnv6nr") (yanked #t)))

(define-public crate-mosaic-tile-0.1 (crate (name "mosaic-tile") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k3cfypbw448r1iqnjg9ycyqnwqphyp5jlapq7q1pngj9isj2jnr")))

(define-public crate-mosaic-tile-0.1 (crate (name "mosaic-tile") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jzxkcxq6m8vkwikzpd7rxfv78c4fasnsx5z1hcqxj9nn370wz6x")))

(define-public crate-mosaic-tile-0.2 (crate (name "mosaic-tile") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kh62z5bwpa2gkgizjb8j1px5rdibmi5jpbxd1jaixgy3p5l4i1a")))

(define-public crate-mosaic-tile-0.2 (crate (name "mosaic-tile") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09phm6rf1lf7fpmh16w56425fyxrf9qw55gshc5gznmi0282i4b8")))

(define-public crate-mosaic-tile-0.3 (crate (name "mosaic-tile") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q9k1zk2ayk3jm45yylmcfg43wp5xpm9adyvbcav1j2bk10yvkfw")))

(define-public crate-mosaic-tile-0.3 (crate (name "mosaic-tile") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z3v719xrw0r2vwsnwr6irw6inw0c2g4rwca07bc69bj6vh129qc")))

(define-public crate-mosaic-tile-0.4 (crate (name "mosaic-tile") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bw4cxxzv9fxi6q14bggcw38zi472x2jj0l5bfd5482q2ai362k7")))

(define-public crate-mosaiq-app-api-0.1 (crate (name "mosaiq-app-api") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fsqkdp14arfxq1n42nmpyxr6jzd6wnwq8196bs1yblwplp8yk8y") (features (quote (("testing" "byteorder") ("default")))) (yanked #t)))

(define-public crate-mosaiq-app-api-0.1 (crate (name "mosaiq-app-api") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "0z1sbcb6l1g07lidzsskaqcfnnv398glpr9fplrklnwir23jjk6i") (features (quote (("testing" "byteorder") ("default")))) (yanked #t)))

(define-public crate-mosaiq-app-api-0.1 (crate (name "mosaiq-app-api") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jidbl1vj5v4j8266144ydja5qdi0qqrsnkxhq38vn68dq47p8xv") (features (quote (("testing" "byteorder") ("default")))) (yanked #t)))

(define-public crate-mosaiq-app-api-0.1 (crate (name "mosaiq-app-api") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1igm6gp5pjamyl41s81ndqph9i82lxlvkw2y1kc48c7bv46ndg3n") (features (quote (("testing" "byteorder") ("default")))) (yanked #t)))

(define-public crate-mosaiq-app-api-0.1 (crate (name "mosaiq-app-api") (vers "0.1.8") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1610rzai1krymdxbyn1yx9s7l11s4xg2a7h7nqmbhzsa1yj1bb1b") (features (quote (("testing" "byteorder") ("default")))) (yanked #t)))

(define-public crate-mosaiq-app-api-0.1 (crate (name "mosaiq-app-api") (vers "0.1.9") (deps (list (crate-dep (name "byteorder") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sybdyj11rbf924jchqakrdwf4hpnz55a413cqk01qm6pf82918d") (features (quote (("testing" "byteorder") ("default")))) (yanked #t)))

(define-public crate-mosaiq-app-c-bindings-0.2 (crate (name "mosaiq-app-c-bindings") (vers "0.2.7") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "09a4nir0d5xynjfdyvrf7ryx0pvm60a5cmyzpnyp0z1j8d41qyjf") (yanked #t)))

(define-public crate-mosaiq-app-c-bindings-0.2 (crate (name "mosaiq-app-c-bindings") (vers "0.2.8") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "1sbgqdvhwggskb4yds7gqrh8irwkp3g6akwf0c5ly50kk6861mb3") (yanked #t)))

(define-public crate-mosaiq-app-c-bindings-0.2 (crate (name "mosaiq-app-c-bindings") (vers "0.2.9") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "14dd8nnlx1h4dn9jf9aabp5n50ryk2crad06pa3cz64x1533pkf8") (yanked #t)))

(define-public crate-mosaiq-app-c-bindings-0.2 (crate (name "mosaiq-app-c-bindings") (vers "0.2.10") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0cvxmpcmywmplcw73viz704kdzc8kzxbx5qas642rciwys72c3vv") (yanked #t)))

(define-public crate-mosaiq-app-macros-0.1 (crate (name "mosaiq-app-macros") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "019912z96qqhg0rhi2s6x5qsbdr4fpav7z5110aj9cbha0xgkyxf") (yanked #t)))

(define-public crate-mosaiq-app-macros-0.1 (crate (name "mosaiq-app-macros") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nhx322c5pnlc0934n9iwrayi89diqb8avhmj0k2gj3fx5fm66n4") (yanked #t)))

(define-public crate-mosaiq-app-macros-0.1 (crate (name "mosaiq-app-macros") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dm35nzskvf261305xqzbdiyib8q1hn2j0vkkg6c9cixz3b41gmm") (yanked #t)))

(define-public crate-mosaiq-app-macros-0.1 (crate (name "mosaiq-app-macros") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jq4lp0hf4jhsn4rqi0li7lwxv8mr1vxk3g4gd4yqhwpa7v756fy") (yanked #t)))

(define-public crate-mosaiq-app-macros-0.1 (crate (name "mosaiq-app-macros") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b28rqvqcgpbvb90p9hrkrcwl6l5x52s8z7wy8892p2a837l13j9") (yanked #t)))

(define-public crate-mosaiq-app-macros-0.1 (crate (name "mosaiq-app-macros") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "062iib593n24zkl6sy9kc6fi85sdf7i6wdfnmi87cb5jjq2ybvil") (yanked #t)))

(define-public crate-mosaiq-app-macros-0.1 (crate (name "mosaiq-app-macros") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mosaiq-app-c-bindings") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cfsaazqy741bcmax7vr21rjnrpx88jpkpdj7p2zq2y5zz54fiih") (yanked #t)))

(define-public crate-mosaiq-c-bindings-0.2 (crate (name "mosaiq-c-bindings") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0qaybcvkxczsay5dc9wc1pz7wyvl3273qsg31mshjif55vlm153b") (yanked #t)))

(define-public crate-mosaiq-c-bindings-0.2 (crate (name "mosaiq-c-bindings") (vers "0.2.5") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "10ppq785dsy52ahqxzil2d4cjwd94lp4v4fbwpl48dwkcp93jqz0") (yanked #t)))

(define-public crate-mosaiq-c-bindings-0.2 (crate (name "mosaiq-c-bindings") (vers "0.2.6") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "02rmqiggnilbbny7l3y23rzh73mj9gdgkn8sb9zd4bhj03yavdqk") (yanked #t)))

