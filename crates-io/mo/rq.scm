(define-module (crates-io mo rq) #:use-module (crates-io))

(define-public crate-morq-0.1 (crate (name "morq") (vers "0.1.0") (hash "025h3i08z3v92mbjx1fsc6kgvsfwpbrjszyw9h4zhac5wdxyx1ay")))

(define-public crate-morq-0.2 (crate (name "morq") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0g8nal095bcxfdd1p5i1rr6020y0hrnvf3xrxckh5hnw2kyaisly")))

(define-public crate-morq-0.2 (crate (name "morq") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0n6p4j338ynf1p4k4khhrc11cdxcd2587r9zy2y3swxd9m2x88vv")))

(define-public crate-morq-0.2 (crate (name "morq") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0rvw3z53d74na5z7w957bd2mq1vgvcjnvz598jafg5x5rsa5jn8p")))

(define-public crate-morq-0.3 (crate (name "morq") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1x2kkj2wnl5b3siidmy2cw2j7q40bfrgjjcsshrhv8ns8hv8xy01")))

