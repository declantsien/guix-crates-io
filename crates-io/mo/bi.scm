(define-module (crates-io mo bi) #:use-module (crates-io))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0779al83v9hy7dlcmqa4wg07krj1rzz5q5zi75malkzgq2b4n3bm")))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1n81mq1ng21m7fpy4fymyy1dznld95l1z3zszzffi5adxyfsrhgv")))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "07pxx7ay06808g8w388hq0ypss1yhyycj4v5i8zkw63nkbi6dfk0")))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1aq946rs1q2dnvwv1mhzcsfr0vy6kz986gfr92mpa2fqv498hf72")))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0s49fzcvg10jpv8n3vmabfx697li0vgkl4da2wp9z9qsczrvncss")))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "111wcxnl1xflj28mc5l7mjqa5jnwl68daldy2aq4jfdav7r0ciky")))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0fglk5szk7dmrnhw2in4jkmgzn1aw6bshgyc1awxrsmj3x16npyj")))

(define-public crate-mobi-0.1 (crate (name "mobi") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1l4asy5pf6jnd75qy54l3y6imwmblyng1r16mn2jklzc3ywnvs8l")))

(define-public crate-mobi-0.2 (crate (name "mobi") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ya4iya2d5b9ba4j1lf2w4rw3j38lmrmcb495a5bkcpk7ckg1djv")))

(define-public crate-mobi-0.2 (crate (name "mobi") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0028j9is3ab2893gld1nqq1xpm36qjsd7pnxkr5pscvv9l77g6qi")))

(define-public crate-mobi-0.2 (crate (name "mobi") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "01amzpakypx3kiaicpwrz22g5b9pjwx3qmfqy6mv0x6nq1xwppvm")))

(define-public crate-mobi-0.2 (crate (name "mobi") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0vz2v34ibvy4brr42dz69jn1h78hhs1hlvckgdanxg7jcd5ygcpz")))

(define-public crate-mobi-0.2 (crate (name "mobi") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xnzbkc6dpcz2a1vmviypbg9nfqdl5c93xx6rcdbgzrl3rdgcf59")))

(define-public crate-mobi-0.2 (crate (name "mobi") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "00g0gqyh8xdx8i7qxv48cfz56vnv3qf305p2p3w6y3sa6rida7sw")))

(define-public crate-mobi-0.3 (crate (name "mobi") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1nwfb1gsqrkamdgpynmabmrhq80v7kx3swpbngv884gjnb73vhl1") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.3 (crate (name "mobi") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0h88a29yj52a2dwf5r6nhjlrbgyh5sfaqksk2qv607bjhmjkar15") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.3 (crate (name "mobi") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14fsz7ha6z330lya7y827hd7z9wpm43hwszz2zcl82nhj0nhd05g") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.4 (crate (name "mobi") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0z37cg59pw991lhavbknw9h7b8vx3r59pdkkv5g6if8kv4n7v5vv") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.4 (crate (name "mobi") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1xbp4fg5v4sm4hdq5d0kpyfxm0bp8zjr8jx2v5yaj1iczyxpad1x") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.4 (crate (name "mobi") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ifhqfnjgy64yxa2wldns4p8yc9b02l8yng7zpks4m7nd6g5wks6") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.4 (crate (name "mobi") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1sb4m6ha26ni2pssan8q2439qfbz6dhqd9p9i0i6qa4vqjvrsv39") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.5 (crate (name "mobi") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req ">=1.3.2, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req ">=0.4.0, <0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)))) (hash "1ljsc9f8nfqqacmwwkrn94pr1x7r7fdxgncyw0s7689zfqfpfh6h") (features (quote (("time" "chrono") ("fmt") ("default"))))))

(define-public crate-mobi-0.5 (crate (name "mobi") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1fain2yigpjpsv0vwvk6pppljb9z9g6r31cpym5asi20d9fh5sxz") (features (quote (("time" "chrono") ("default"))))))

(define-public crate-mobi-0.6 (crate (name "mobi") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0jmqqlrib7742929y5xfa4a1nchp87ys5jzwjqpz5kv1c4v3rgln") (features (quote (("time" "chrono") ("default"))))))

(define-public crate-mobi-0.7 (crate (name "mobi") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1v28q4zkl9432rgg844laja2qrlqxsding9bq2bjz5ifh9bkkpfy") (features (quote (("time" "chrono") ("default"))))))

(define-public crate-mobi-0.8 (crate (name "mobi") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1d5ia7q4mzwrddfy3m4wsm1ifbj64zd5n449l40bw9k144s8wgwg") (features (quote (("time" "chrono") ("default"))))))

(define-public crate-mobi-sys-0.1 (crate (name "mobi-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.44.0") (default-features #t) (kind 1)))) (hash "0dpzcilb2sndp3i3bpc22hfh8y3h4fsx87j97qcv4gafkzinx4g5") (links "libmobi")))

(define-public crate-mobi-sys-0.1 (crate (name "mobi-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.44.0") (default-features #t) (kind 1)))) (hash "0a8drh6xlv9hkg3vxbp3g4ncmw8jjsxjpva3j247mfv1fls2vcna") (links "libmobi")))

(define-public crate-mobi-sys-0.1 (crate (name "mobi-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.44.0") (default-features #t) (kind 1)))) (hash "13yndlk7ni4cjlg78pgllqckwr65y4zdin1hn3lq7ggz703fwpza") (links "libmobi")))

(define-public crate-mobile-device-0.1 (crate (name "mobile-device") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "core-foundation") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1wnj5j2vbsx10wq26ig3vmmj1lwr9ajwsf3v9w4i7k81p8b0szxy")))

(define-public crate-mobile-entry-point-0.1 (crate (name "mobile-entry-point") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.48") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "113mipiqc5lmmzq32ij6dd9nkhjvbacij9z5physyjinwy32j2ix")))

(define-public crate-mobile-entry-point-0.1 (crate (name "mobile-entry-point") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.48") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19m1nswwnhv54w1jzc4p0dvkwysd888cmk0w8y1nachq02lzbgl1")))

(define-public crate-mobile_code-0.0.1 (crate (name "mobile_code") (vers "0.0.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1s13396hzxvbhqn1l9gg9y1ch08dljxr7mwii5i0xjsx30yn46jw")))

(define-public crate-mobilenet_v2-0.1 (crate (name "mobilenet_v2") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tensorflow") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0yh0kgdpghfz7v1hjbhcab1kc616mmij0r8vhxn1yz5g3gan8zvj")))

(define-public crate-mobius-0.1 (crate (name "mobius") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1p1zg214cqysbkan2h8ksmqnwfdizc9gcb4k5h84da4q5jx1kvm9")))

