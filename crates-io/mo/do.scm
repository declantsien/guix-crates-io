(define-module (crates-io mo do) #:use-module (crates-io))

(define-public crate-modoomo-0.1 (crate (name "modoomo") (vers "0.1.0") (hash "0zl3afyb4n458any8l5hfgqcvzacbj7lwncf6say0hbjysndi4qs")))

(define-public crate-modor-0.0.1 (crate (name "modor") (vers "0.0.1") (hash "1gbcl39m14lghbk3fc0m5a5na8mqnx39fqqldvjcxadanmvb71xg")))

(define-public crate-modor_ai-0.0.1 (crate (name "modor_ai") (vers "0.0.1") (hash "1bhjr2pyy2g96fradyq308687j6yw0bzdq8y16k54mjz215xsszg")))

(define-public crate-modor_derive-0.0.1 (crate (name "modor_derive") (vers "0.0.1") (hash "188w7gqy26d2gahag8jck9bxkyz93izpjkwsind7iryh6gi1xyr5")))

(define-public crate-modor_editor-0.0.1 (crate (name "modor_editor") (vers "0.0.1") (hash "057i0amz5r306y297fz4qm272p8g3f3g4mrypcbjvm1ar3qksgsp")))

(define-public crate-modor_graphics-0.0.1 (crate (name "modor_graphics") (vers "0.0.1") (hash "1h7dam6as8yb32bychaay3nnf360v43m01hdhcg5bb1g4kk6jsbw")))

(define-public crate-modor_input-0.0.1 (crate (name "modor_input") (vers "0.0.1") (hash "1bq34v461d30hq6v84s0mj7mp30fzk5h8x7cp96vjkyg47xc0ih2")))

(define-public crate-modor_internal-0.0.1 (crate (name "modor_internal") (vers "0.0.1") (hash "0qvk60yky872z27q01w8vaz0m4vjs1c6lvddg9v1ppy264q3vfma")))

(define-public crate-modor_jobs-0.0.1 (crate (name "modor_jobs") (vers "0.0.1") (hash "0bgawggbq5r31j5srmhi6p8aaip2kygn0k0m22pr38xr5rhpcmys")))

(define-public crate-modor_math-0.0.1 (crate (name "modor_math") (vers "0.0.1") (hash "092wi8jrwawsvlqbadz41bsys1cg2yngi1nss8sps22qlnvx66a0")))

(define-public crate-modor_network-0.0.1 (crate (name "modor_network") (vers "0.0.1") (hash "0nnczcxn08zzbm3yak98xii8fm16s5hjk3nndyhrm8gqz6ribq8s")))

(define-public crate-modor_physics-0.0.1 (crate (name "modor_physics") (vers "0.0.1") (hash "01r2qbxhnh6xc8wsdky521ylsakw9hl8favf73n41wvlg3a861x9")))

(define-public crate-modor_picking-0.0.1 (crate (name "modor_picking") (vers "0.0.1") (hash "0xxgf8y8qgrk9qm1akjzlqbd4yqg13s61qb86mqzdnj16yrdqamq")))

(define-public crate-modor_resources-0.0.1 (crate (name "modor_resources") (vers "0.0.1") (hash "0iyn6ikkm0hm7qq5ki534xplxxm6cab9h1rgkb11nl5zfwprmk53")))

(define-public crate-modor_text-0.0.1 (crate (name "modor_text") (vers "0.0.1") (hash "061iywxgpfg1rda4dbgcv52w0lyzkk01zpql88r24bfjagf98xwm")))

(define-public crate-modor_ui-0.0.1 (crate (name "modor_ui") (vers "0.0.1") (hash "06jlh3v8l73b33ibwh5k768rq9s3w7afpns7wi5y2896d2qq229c")))

