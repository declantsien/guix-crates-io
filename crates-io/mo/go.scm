(define-module (crates-io mo go) #:use-module (crates-io))

(define-public crate-mogotip-0.1 (crate (name "mogotip") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0j0z2ryz88ladgzzdxdjam61iadpcm5s6817j1jgwzv85y8gn0qs")))

(define-public crate-mogotip-0.2 (crate (name "mogotip") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16a1n3i396jsz075hhhilmjz5br860g3pqh645svqmcah7ysjk0z")))

(define-public crate-mogotip-0.3 (crate (name "mogotip") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "01816agq379g6kw3rrr641cnp2k61ij1q79nbqvdvc3j4jcpy1yr")))

