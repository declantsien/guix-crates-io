(define-module (crates-io mo lt) #:use-module (crates-io))

(define-public crate-molt-0.1 (crate (name "molt") (vers "0.1.0") (hash "1kaaslcr3w5rd5zvpazv6yvvg257jmrlnskzzkv49fbw7m6yj6ra")))

(define-public crate-molt-0.1 (crate (name "molt") (vers "0.1.1") (hash "133cy2c7p2pfbjylckdz4nlwawyy305c6wfc9k10cr46vkvsrzbc")))

(define-public crate-molt-0.2 (crate (name "molt") (vers "0.2.0") (hash "036bkk0hyja652jpq8p8pldhqm9240klwgpkf0ln5xjrxbc516bx")))

(define-public crate-molt-0.2 (crate (name "molt") (vers "0.2.1") (hash "12jbbms5l58kc31wfnmzk6hhy97304grlka500msxz1xdxlyrbz6")))

(define-public crate-molt-0.2 (crate (name "molt") (vers "0.2.2") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "07a1f9nm9hdyxagacfbbh90xk3r770bf5wn7z1nbw1dni1m9s0yr")))

(define-public crate-molt-0.3 (crate (name "molt") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1jhj36i2mgczbx5sqidw70jl3n0hjwzw0xk686fb3r565rb0csa0")))

(define-public crate-molt-0.3 (crate (name "molt") (vers "0.3.1") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1g0nwm9wkdj5b1g9kkwmkxyr2a9x2qdxw6dqbbb786r6qqkhvzzn")))

(define-public crate-molt-app-0.1 (crate (name "molt-app") (vers "0.1.0") (deps (list (crate-dep (name "molt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "molt-shell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rv6rbr8j3w0y37n8pixc8yj88pmm75z0h4jj0nrjk310yswkpa9")))

(define-public crate-molt-app-0.2 (crate (name "molt-app") (vers "0.2.0") (deps (list (crate-dep (name "molt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "molt-shell") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1wj3vsxr799fvcdil24a6wdqbdx3s6cwh52sqb0zvzc3c544n2v7")))

(define-public crate-molt-app-0.2 (crate (name "molt-app") (vers "0.2.1") (deps (list (crate-dep (name "molt") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "molt-shell") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0zwixaj0qv4x5xa31dzp1nbzzbhz59l572r9d2yh8dflzzq9053q")))

(define-public crate-molt-app-0.2 (crate (name "molt-app") (vers "0.2.2") (deps (list (crate-dep (name "molt") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "molt-shell") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "09fnhcbciy1h011slb8mliib2jslxwlcdvrpz4b5mznamql1x3zr")))

(define-public crate-molt-app-0.3 (crate (name "molt-app") (vers "0.3.0") (deps (list (crate-dep (name "molt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "molt-shell") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n99bqi1h5wls1pkfcjad1j4k291garwnkr7iii9hyld3yv4ac62")))

(define-public crate-molt-app-0.3 (crate (name "molt-app") (vers "0.3.1") (deps (list (crate-dep (name "molt") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "molt-shell") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1lksqi8h737dd8h6m3kl272x6hsc95ryl1vr9w172i64sjs29zbg")))

(define-public crate-molt-argparse-0.1 (crate (name "molt-argparse") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "molt") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "molt-argparse-procmacro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09xh290mkfghlfc97xnnwh365qngr6jmwz79dpnq1qn3mqcvvprg")))

(define-public crate-molt-argparse-0.1 (crate (name "molt-argparse") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "molt-argparse-procmacro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "molt-ng") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1sdspyxsxbna4f72hyxx4fxcld3ji1hapwy8qh8q6w3yp545dag8")))

(define-public crate-molt-argparse-procmacro-0.1 (crate (name "molt-argparse-procmacro") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "10kjlppf5zr7sakhv1n4m0dpb33xw7rcjwaq2sq7s04h6d5hlgg4")))

(define-public crate-molt-argparse-procmacro-0.1 (crate (name "molt-argparse-procmacro") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0gdx5clymhiaznqr2x0x3iwad46wl43m93f0b9nzrvg0zliq3pw2")))

(define-public crate-molt-ng-0.3 (crate (name "molt-ng") (vers "0.3.2") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "03x6k5px3x090kniz1fh85ign29yiiixri611937svi1hqxw9fjb")))

(define-public crate-molt-shell-0.1 (crate (name "molt-shell") (vers "0.1.0") (deps (list (crate-dep (name "molt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "04d8dirhaqv1gildknhsiyqgrzy1s9rvn8x5bl6d739lncjfx4bn")))

(define-public crate-molt-shell-0.1 (crate (name "molt-shell") (vers "0.1.1") (deps (list (crate-dep (name "molt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "12y45211mdhdk8db0kbkali7gxh1sxrisg5l4lj97zli6dgr058d")))

(define-public crate-molt-shell-0.2 (crate (name "molt-shell") (vers "0.2.0") (deps (list (crate-dep (name "molt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "18l6k5p7k0dxlc2d2sfj57mn0sy96lpl7kg3nd09xn7ni2fa8jq5")))

(define-public crate-molt-shell-0.2 (crate (name "molt-shell") (vers "0.2.1") (deps (list (crate-dep (name "molt") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1d0shg77fhb0fzqp4pnni7llq0nigbawn437g1pmjhn8xj4mp12h")))

(define-public crate-molt-shell-0.2 (crate (name "molt-shell") (vers "0.2.2") (deps (list (crate-dep (name "molt") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "02ymrldv315ycn8dn2fr2hn0v3ar09psbcy47flrnv40s7kfgmz5")))

(define-public crate-molt-shell-0.3 (crate (name "molt-shell") (vers "0.3.0") (deps (list (crate-dep (name "molt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "0b22k2kqx0s7lfchk3k5sj7vp0qhb907ng3z5li2z03n9bsck9s5")))

(define-public crate-molt-shell-0.3 (crate (name "molt-shell") (vers "0.3.1") (deps (list (crate-dep (name "molt") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1311kfzqklwc7liyj1sf89xlf888dwhn82wc4si8byz1vgmji59a")))

(define-public crate-moltenvk-sys-0.1 (crate (name "moltenvk-sys") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 1)))) (hash "09km6xwbynfdbwl765kwryszv2n073d1ij3xqlrij2wy5hyngifj")))

(define-public crate-moltenvk-sys-0.1 (crate (name "moltenvk-sys") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 1)))) (hash "1vr3a3y2aifgakaq60cmvc711ms2g8jm7rasxqmfxkjk92ykabmc")))

(define-public crate-moltenvk-sys-0.1 (crate (name "moltenvk-sys") (vers "0.1.2") (deps (list (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 1)))) (hash "0l5fi2j6hh85i1gd297vdhxsb9nv8aazimfjhb9icw6j29g28y0i")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "1494m25bdsq0af699bc5x70f8jpn0b6chlrrdfiscfbd1xf9v05d")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.1") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "02vyclk7lrmr5ymv5z79zqcgjr116bslbk01bccnilbhsgf510r9")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.2") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0g2m4gci9x22m77dkwq4lcnn34i6nxh1fx1ga2mg9acsvvwlm021")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.3") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wd9lmxygpriq1wr8rmnpdfaa5nrgkf10m5k9rmj971x1a896g8x")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.4") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bj7svh8psq3p13m1h40j18w5d1lcgpsk68axqa04ccaxyyzrbjn") (yanked #t)))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.5") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "1l74ff1cqwvxl4wizf1ak6mryv6k3inlv21drwmgd9074wyvyzzz")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.6") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "13bc95f9q9pr5k6n5019qkdb99x5l00ccc7bm505l6b1ddcadr1f")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.7") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0m3wgn2i796dr2lsmmw4qgja5vzmjzd3h4n72dx23pk8gakkin0w")))

(define-public crate-moltenvk_deps-0.1 (crate (name "moltenvk_deps") (vers "0.1.8") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_perm") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "172z5zcnxkv80v2bsfb195n92lc3j5znkmdgx8vmwj1h34bdlma8")))

