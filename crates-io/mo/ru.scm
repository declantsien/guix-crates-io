(define-module (crates-io mo ru) #:use-module (crates-io))

(define-public crate-morus-0.1 (crate (name "morus") (vers "0.1.0") (deps (list (crate-dep (name "aes-gcm") (req "^0.9.4") (default-features #t) (kind 2)) (crate-dep (name "benchmark-simple") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "chacha20poly1305") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1507bqvz7zhrsaxi9nv3pnk18j890sngfjgvajkcy7m9p3rr1gli") (features (quote (("std") ("default" "std"))))))

(define-public crate-morus-0.1 (crate (name "morus") (vers "0.1.1") (deps (list (crate-dep (name "aes-gcm") (req "^0.9.4") (default-features #t) (kind 2)) (crate-dep (name "benchmark-simple") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "chacha20poly1305") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "096p7si5qd2v39rzhfl02jvljglhfiwia1pvj50986z5h84qyk9c") (features (quote (("std") ("default" "std"))))))

(define-public crate-morus-0.1 (crate (name "morus") (vers "0.1.2") (deps (list (crate-dep (name "aes-gcm") (req "^0.9.4") (default-features #t) (kind 2)) (crate-dep (name "benchmark-simple") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "chacha20poly1305") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "12vf0i2nb2mhz88f550zb390353z2ph45pnax54ffj2gz7vs45by") (features (quote (("std") ("default" "std"))))))

(define-public crate-morus-0.1 (crate (name "morus") (vers "0.1.3") (deps (list (crate-dep (name "aes-gcm") (req "^0.9.4") (default-features #t) (kind 2)) (crate-dep (name "benchmark-simple") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "chacha20poly1305") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0qr9ndylrkpyvbydz8rbjgrax483izim1q4pcb2370f5a38im9s1") (features (quote (("std") ("default" "std"))))))

