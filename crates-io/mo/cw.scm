(define-module (crates-io mo cw) #:use-module (crates-io))

(define-public crate-mocword-0.1 (crate (name "mocword") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "1impmlhlx4l0fpj9n8fani4spni4jd1c227vmqsg6svbals1iqa2")))

(define-public crate-mocword-0.2 (crate (name "mocword") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0fgm41n5xg10jcqm0sdd73d1q3lwcb4ijw5nvdcz3m1bx8vxbgiw")))

