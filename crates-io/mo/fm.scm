(define-module (crates-io mo fm) #:use-module (crates-io))

(define-public crate-mofmt-0.4 (crate (name "mofmt") (vers "0.4.0") (deps (list (crate-dep (name "moparse") (req "^0.1.4-rc4") (default-features #t) (kind 0)))) (hash "0ywhqnvq27mdz8qschf1gknv54dzbzfqh4jvpq14sj679lap7xrr")))

(define-public crate-mofmt-0.4 (crate (name "mofmt") (vers "0.4.1") (deps (list (crate-dep (name "moparse") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0mxr6mh2cmhcb7pnpgh77r20qpdz41vxq8sx52y92cz07vgsdil5")))

(define-public crate-mofmt-0.4 (crate (name "mofmt") (vers "0.4.2") (deps (list (crate-dep (name "moparse") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0373i60qm3am48mw0x094s8vzlpwfj779n5x5jl18nzj61gpbms2")))

(define-public crate-mofmt-0.5 (crate (name "mofmt") (vers "0.5.0") (deps (list (crate-dep (name "moparse") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0lwni2vh2np6lf76n01rn2n386v69f18np2afcxxxpglklk1j7lq")))

(define-public crate-mofmt-0.5 (crate (name "mofmt") (vers "0.5.1") (deps (list (crate-dep (name "moparse") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "168l5xv4s5sz6ksbxw2z0g67bl66hcc4n5giayw29cvipnfp9pfh")))

