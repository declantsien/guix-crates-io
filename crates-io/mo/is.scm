(define-module (crates-io mo is) #:use-module (crates-io))

(define-public crate-moist-0.0.0 (crate (name "moist") (vers "0.0.0-alpha.0") (hash "0x1a6fish4jxj3lwhlb73bpd0fhmnja0w19a06bn5b1sdjy1jq6h")))

(define-public crate-moisture-0.1 (crate (name "moisture") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.91") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1c6pd831kln472mg0m3jbmm31wfkmy1z6wz604picmgrjpn1476s")))

