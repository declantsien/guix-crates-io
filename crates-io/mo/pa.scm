(define-module (crates-io mo pa) #:use-module (crates-io))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.0") (hash "139m7c62kdkx6n5xclcjfxmlvkkbmql5v6d4jbc768ar335vafk1")))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.1") (hash "0xd7qvycca7mi186y3yd2ag97413zdmbnfxgnpw8yjz26yrz6m9v")))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.2") (hash "06cxfk7l6syw1mrknry96dp3y2k8sfxyjkaqf596wimkpqcrk22x")))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.3") (hash "07nxl9yrxbp7b8qv6x6nlhb0c7wl6k2j9p40xqqch8ccwgpm3lrc")))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.4") (hash "07c4gmhqmrm2wq9km2mjrjc3iz87wxds5r0499wgxd3nd9dzk7cb")))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.5") (hash "1gx0c09nw1y39sbwa6kzdwh7cjfz5az65d1r0lxmg85vil48q5b9")))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.6") (hash "0qm1xmmc27bg2524xnrf138n13a1pxp9m2w7n9zivzxpff7b3s9f")))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.7") (hash "0xb3rd8z3fnb33n4d5mxrwnyd5w9wiz6zfc2l3xfpy5ccg414l3y") (yanked #t)))

(define-public crate-mopa-0.1 (crate (name "mopa") (vers "0.1.8") (hash "085cn1ivc7z1fc6nknh5m4zjw40scis7mxcn0dd8s9vc5zq0hh4z")))

(define-public crate-mopa-0.2 (crate (name "mopa") (vers "0.2.0") (hash "1w3brvc7dgjd3zd5xmy182rvpdldq9nqz6sl1mzjny76c43dsdsd") (features (quote (("no_std"))))))

(define-public crate-mopa-0.2 (crate (name "mopa") (vers "0.2.1") (hash "0h5l90ks0aq3l6wyhd33j2bq96jjcvqk1pcw68j0j4c1r4cmczgp") (features (quote (("no_std"))))))

(define-public crate-mopa-0.2 (crate (name "mopa") (vers "0.2.2") (hash "05grm5s996fmjs7w2bq6lwrq969gwn1knba6aw7j6v15f41791d7") (features (quote (("no_std_examples" "no_std") ("no_std"))))))

(define-public crate-mopa-maintained-0.2 (crate (name "mopa-maintained") (vers "0.2.3") (hash "04dsfc0k32in1c0bz7551ps4nbn24vrsc8avr73jr1k747ig7dvr") (features (quote (("no_std_examples"))))))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.0") (hash "1lhgv6va7ndwq05bag8g1ipv5055l64adx5y5xay18d6frggvyqn") (yanked #t)))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.1-rc1") (hash "1aaddd4bdd5gxl7xih1dzd3y8da6ilgra2l35w2iv0wsm3xw7if1")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.1") (hash "0jqlrfi4xil48v8kwgvfsyw2scdnjapzyznkxx85acrcgfqcj7j7")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.2-rc1") (hash "15qd93p1zlx4b21c3mfvkszhm6z4bvd5xbcgz0zpsmp33kr9wwvi")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.2-rc2") (hash "0b274m71vlnmd5li85nfirx3f0dq4ak59yz0znhrzgnrzm2cd9ql")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.2-rc3") (hash "1gp936iixidlcg95cnzqjgdx91h6yc5q3qa49m25ygi1iqgckmlq")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.2") (hash "1ll03g6shnxc5vf0yax18bys005di66pi3gqwjkjb87bmflqjcf1")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.3") (hash "03c0f3n9q85wxcrfnwfymsa1yrnnkjm47lq0315hq914282nkmwc")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.4-rc1") (hash "0pk3i7cl7cf0sfvd5fdbfwr7pcnf6lcpvb6nzgl2hmz635nn1kab")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.4-rc2") (hash "1pvb1gzxvk7znzwfnih7afrl0xszc86bhnbmxv36lsyqsrj7j82a")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.4-rc3") (hash "12bf75g06is3rzaca05nr22b116qw4rn681m1wz1gb3na2p9bvn4")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.4-rc4") (hash "032qfchg06d6z5qkkikf91hdq6fbzv4122kn8ayg6lkx82ay98mk")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.4") (hash "09zw1l0dw2z30ykbs2wb60jbrbasyg5sz3abx2c4vbryg5fkmdvy")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.5-rc1") (hash "08ickkijgf8806xfjr6vx3c4m5iab586rr85jjpnm22raiv3k4yj")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.5-rc2") (hash "0crwcwr286k0mz4sssyc6lnab27najm9byqvvs0xs425kv683if3")))

(define-public crate-moparse-0.1 (crate (name "moparse") (vers "0.1.6") (hash "1iky2796is1hgxfxplgvz1qk1sfmyja5fajs97zjn9jhgwk8i19i")))

