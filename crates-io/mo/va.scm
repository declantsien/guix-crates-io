(define-module (crates-io mo va) #:use-module (crates-io))

(define-public crate-movable-0.1 (crate (name "movable") (vers "0.1.0") (hash "0p6jdpp3bwx7xmzrbhycci1kqhd1qi1rdw76461csmqw20jcf0rx")))

(define-public crate-movable-0.1 (crate (name "movable") (vers "0.1.1") (hash "0b7inl75mwxb45c2anc7lw9bmffbv669ms127ak4a3r5czm9zlig")))

(define-public crate-movavg-1 (crate (name "movavg") (vers "1.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1z9p0ga7sd0jwfk2wjq3a34vzr0y3vxm5fmg896xl463gbk3ba46")))

(define-public crate-movavg-1 (crate (name "movavg") (vers "1.1.0") (deps (list (crate-dep (name "autocfg") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1qk40aig2d1ysz0z7nnlc6k4qahgq88y25p7r3kzsk5xmjm2gci5")))

(define-public crate-movavg-2 (crate (name "movavg") (vers "2.0.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.0") (kind 0)))) (hash "0qpv5hg44fxkdwq0820aaf5qwygqfd6pc05iz90f6yz6bwm349qf") (features (quote (("std") ("default" "std"))))))

(define-public crate-movavg-2 (crate (name "movavg") (vers "2.1.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.0") (kind 0)))) (hash "1ds6gzm4dbj53v6yikxxc6gaik4wkbv496c9nkdara75jdp69zxh") (features (quote (("std") ("fastfloat") ("default" "std"))))))

(define-public crate-movavg-2 (crate (name "movavg") (vers "2.2.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.0") (kind 0)))) (hash "1frvf6pylqq6db915icc77q9y140xlqy5v3cd66dgajfvf58whv2") (features (quote (("std") ("fastfloat") ("default" "std"))))))

(define-public crate-movavg-2 (crate (name "movavg") (vers "2.3.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "10k9489va441dbd3vpdhp5bhbd6h0rz7vb2w563zqa4grnpk2q8h") (features (quote (("std") ("fastfloat") ("default" "std"))))))

