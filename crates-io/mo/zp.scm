(define-module (crates-io mo zp) #:use-module (crates-io))

(define-public crate-mozpdb-0.2 (crate (name "mozpdb") (vers "0.2.1") (deps (list (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "13b8gl9ssywsda7j8kr5j8lqsfljsfdzdkqigk6xs8zmw6zjs8il")))

(define-public crate-mozpdb-0.2 (crate (name "mozpdb") (vers "0.2.2") (deps (list (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1rvhrqx10nqhbs15348brqviifmkkgsg3bpv8pdjk4l99hsr08yg")))

(define-public crate-mozpdb-0.2 (crate (name "mozpdb") (vers "0.2.3") (deps (list (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0ppknhlcqs1hiqjibjy5imj61afrzh0q6gf3q8qhrlifyiqxz093")))

(define-public crate-mozpdb-0.2 (crate (name "mozpdb") (vers "0.2.4") (deps (list (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0yb0v34b7hwmcwnbx08wmpn46k2iaf3f1dggysjjf4zp301rmml7")))

(define-public crate-mozpdb-0.2 (crate (name "mozpdb") (vers "0.2.5") (deps (list (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1lb4pwfrf1ngz5kakig8imvsvjz3scmfc2jr097k7kqnhvvvy39c")))

(define-public crate-mozpdb-0.2 (crate (name "mozpdb") (vers "0.2.6") (deps (list (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0b18r5qx2g2w164h5yls56ynd8sx7aaf7ya90q4rg10qds722s0v")))

(define-public crate-mozpdb-0.2 (crate (name "mozpdb") (vers "0.2.7") (deps (list (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.9.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "17xngxp2li6hlmjynq1gdn05jkzpn80qwqzsd2722ksgcfagd94p")))

(define-public crate-mozprofile-0.1 (crate (name "mozprofile") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0dyaanbmw43mdjhalynif0q1brvagrmv8l457yv22j7m85wxj1w0")))

(define-public crate-mozprofile-0.2 (crate (name "mozprofile") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "109wr3fhyi1dnsj3847y2ndir53g32adkihxnbb0bad0m487q5q1")))

(define-public crate-mozprofile-0.2 (crate (name "mozprofile") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0ra1m0hl19and50l0q2ifvlcc1k9nki69xd2cna98zdskwidx0a9")))

(define-public crate-mozprofile-0.3 (crate (name "mozprofile") (vers "0.3.0") (deps (list (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0zndqyf5xpj72iycwghp6alvm2bgghnijwxhr3qzph0xvsxvh5qs")))

(define-public crate-mozprofile-0.4 (crate (name "mozprofile") (vers "0.4.0") (deps (list (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0b492arp8jkk31cijzxcddlzvkpzx4ira5yid4m0s2300zincak5")))

(define-public crate-mozprofile-0.5 (crate (name "mozprofile") (vers "0.5.0") (deps (list (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1q6bpkq3h2ri3w04rral4z7p23m3r4dq9ki0a3w9hjkaig0kqicz")))

(define-public crate-mozprofile-0.6 (crate (name "mozprofile") (vers "0.6.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "077a6931zr3zhnlhcpwv8f4l291n6q6z3f5gl9n898rg5s8gw5az")))

(define-public crate-mozprofile-0.7 (crate (name "mozprofile") (vers "0.7.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "13r2l4mv7zgd7pa0cp956199qjg547r3kdqbxiqzrn3f7680p5jq")))

(define-public crate-mozprofile-0.7 (crate (name "mozprofile") (vers "0.7.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "180gl0b7pg3gjcv6wnpkkq4nr91b0xvc9c2mp1fbz8d47j6hlfym")))

(define-public crate-mozprofile-0.7 (crate (name "mozprofile") (vers "0.7.2") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "18fldl5xqmmpxszpqyilpc8yhvqhfdr3kbbfxm2v26aksb31kl30")))

(define-public crate-mozprofile-0.7 (crate (name "mozprofile") (vers "0.7.3") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "175kx6dj3wcjnxs36fnbyhic051bvqs3isy4y6lh38m9sw7mriq9")))

(define-public crate-mozprofile-0.8 (crate (name "mozprofile") (vers "0.8.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0lkna46w0xh57lsc0ym6kvdjcyh4pmplvjnkvswjv83lvcf2vb65")))

(define-public crate-mozprofile-0.9 (crate (name "mozprofile") (vers "0.9.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1h919pjbdnvllh895jqgx01y0ixp57wwzyang9sz8p0n7k70scnx")))

(define-public crate-mozprofile-0.9 (crate (name "mozprofile") (vers "0.9.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0acmd1x57v364qw0zx5f4z99r08w0xdvpbr7cghwqcqxhshm88p2")))

(define-public crate-mozprofile-0.9 (crate (name "mozprofile") (vers "0.9.2") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0pavjkag5llvg4nckjgmr2v8hz3jpyvflxk5bz7fk65m7fahvxwg")))

