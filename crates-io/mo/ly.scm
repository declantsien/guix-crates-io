(define-module (crates-io mo ly) #:use-module (crates-io))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "19wnnha66m3975irdv1x3xs4i2mk2pmji94pw4lh0612nw8hxvjm")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "11q0i9wk3s06lg2mm1b4rgajnlj2xz7ml5b77w9nhyx26ais1vlw")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1wxcai9rqdc8w7k382gfkc22wyn46hazjbcj1c4z3midifka323p")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0grcnvsgs597v75b2l8f3s9aw5ag16li81hafl88gfl32mayaxqd")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "01ikgf8mr1y8ar1v2djh1y13bznr3iq82m1l90s1xl5cwx1n6d29")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.5") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1x812cmrgnp7li0qd8ggpbz66sjdclv2n9c3mw7ddyig1dla0fgp")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.6") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0s40gb7r6g0zyjpfi39jh47ih708zl2w60nxy3r0d1a35h0j6msa")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.7") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0r4ilzrdq0kg802ijdc08n76j8vvg1wafjx3c6g8kkpr943m5in0")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.8") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "037276yr7rhal4nvf87y0kny18iiibp4acmn9rxsq83q240kpbyc")))

(define-public crate-molybdenum-0.1 (crate (name "molybdenum") (vers "0.1.9") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0jhs3aizqxaa7vfgb10k7xsw5mylzmjcag5g41f2n7lyr8n6b5nk")))

