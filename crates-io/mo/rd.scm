(define-module (crates-io mo rd) #:use-module (crates-io))

(define-public crate-mordhau-loadout-parser-0.1 (crate (name "mordhau-loadout-parser") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.0-beta2") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0a8gqf154h4sy4za4pckl9ibvdrrw3yi4qw4xpxm2azyw8s0yhfr")))

