(define-module (crates-io mo dd) #:use-module (crates-io))

(define-public crate-moddef-0.1 (crate (name "moddef") (vers "0.1.0") (hash "19gw5d1ac76hx7ldblbjz6dqhzf15a1xd3qdrpkm0036vk44m2iy")))

(define-public crate-moddef-0.2 (crate (name "moddef") (vers "0.2.0") (hash "0wxzd7y6a0cb4dgc9hkdvxh6swwq35y5iysryshbrl1xdw6s9wm8")))

(define-public crate-moddef-0.2 (crate (name "moddef") (vers "0.2.1") (hash "0m5494kq3xmic1wbmh2qd7x0mnqkqbb8mi89nbgivr256abck742")))

(define-public crate-moddef-0.2 (crate (name "moddef") (vers "0.2.2") (hash "1hp5qajcp0dplzlw9mnr4iy1l8smjdhlhnshq7qn66q64bvhxljy")))

