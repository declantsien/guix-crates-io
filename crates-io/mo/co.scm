(define-module (crates-io mo co) #:use-module (crates-io))

(define-public crate-moco-0.0.0 (crate (name "moco") (vers "0.0.0") (hash "014wy1ih0v31waq62svhbhflp418xhcvlv9cvj62lmph5knfi36p")))

(define-public crate-moco_abm-0.1 (crate (name "moco_abm") (vers "0.1.0") (hash "01y5m7v0ap0kj5kpa4gijvabfw3wyfm349q28hbzl5csrpgm2z2z")))

(define-public crate-moco_abm-0.1 (crate (name "moco_abm") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)))) (hash "069rqdyyh227lv0y79mxf59nxykkqq4yg4l6qcnjmx110dy637n8")))

(define-public crate-moco_abm-0.1 (crate (name "moco_abm") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)))) (hash "1akm2nm01cvg2dfv7kzi801k99958g96g0hchxgawaq109k1w9nb")))

(define-public crate-moco_abm-0.2 (crate (name "moco_abm") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "15rx6w2kdh0d8nw7pp6hyzynpgdh6apmjzjclxmdvjgjmn0dawpw")))

(define-public crate-mocommons-0.1 (crate (name "mocommons") (vers "0.1.0") (hash "1flhm2jg2sp7k2ra6w0gxm82i3rzb5f2bm1c8p5namdh4xkij6ay")))

(define-public crate-mocopi_parser-0.1 (crate (name "mocopi_parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "06jd280cy6r10hjvcl8sgb92bqp6yl6mr9rhvms80wf1csgq5yk1")))

(define-public crate-mocopi_parser-0.2 (crate (name "mocopi_parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0gir6zdy3rf1ddwzcgq83g64kzldwchn0c5jnzsnxw3961p0clai")))

(define-public crate-mocopi_parser-0.3 (crate (name "mocopi_parser") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1nkqgrigzpslrlkxdiy8hkhndv7g3rw7b3fjly8jc0m6rbavld67")))

(define-public crate-mocopi_parser-0.3 (crate (name "mocopi_parser") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1q4jy3vj3lp0k8y1jm8b28rdiszncyfrshgz76gf04rpxqr5jfhn")))

