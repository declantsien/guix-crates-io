(define-module (crates-io mo wn) #:use-module (crates-io))

(define-public crate-mown-0.1 (crate (name "mown") (vers "0.1.0") (hash "11478i98hnbwggni0d7shmi3pcl963yw6z4qywa4dm7n4s75rwy2")))

(define-public crate-mown-0.1 (crate (name "mown") (vers "0.1.1") (hash "18hh6bsncjcwlfnavmky3cyhz6brn40xlihhbk53zprqcb9skxv1")))

(define-public crate-mown-0.2 (crate (name "mown") (vers "0.2.0") (hash "14hnnpmvvq2q66jnspg0fdh6805cahp859cr4lfhfrcrh3cfg97n")))

(define-public crate-mown-0.2 (crate (name "mown") (vers "0.2.1") (hash "1r2mqdqia5svhw7cnwp59l5bnmzg43yas7kxapywdin7sc9dj7cc")))

(define-public crate-mown-0.2 (crate (name "mown") (vers "0.2.2") (hash "0znrrgli003x46s175x81l281xjair422jzpqgqxnzmips5psqp7")))

(define-public crate-mown-1 (crate (name "mown") (vers "1.0.0") (hash "1fcda3k0adbpgmx2fvmkkms5wr03ak8zjdkmyf3xc6r4xs65aq0l")))

(define-public crate-mownstr-0.1 (crate (name "mownstr") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "083wy24yxcgmf8zz6rcs8ndc73pzqcn48grk0lyjydpsgkbcxglf")))

(define-public crate-mownstr-0.1 (crate (name "mownstr") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "112grspy6dqir2ldnmmssxg06bi32da8mzq87bqh1lnvh5w40ail")))

(define-public crate-mownstr-0.1 (crate (name "mownstr") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "05h59a2d0aha7pnya9xqvkjajpp172h4bmg53456ry5h855qym8m")))

(define-public crate-mownstr-0.1 (crate (name "mownstr") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1cx5rk564ikkxgxcpchfgmbvjabsq6zmmqrk9srrs2l5z7wcvakn")))

(define-public crate-mownstr-0.2 (crate (name "mownstr") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0wwws0rq8r4hfgr44ykq77p76gpw5lv6kczcid2l808a7xa0cxy9")))

(define-public crate-mownstr-0.2 (crate (name "mownstr") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "16258iyvvh1shh6il5vx78l1qhychp6hrcgz1jrdidcjc7lmri4b")))

