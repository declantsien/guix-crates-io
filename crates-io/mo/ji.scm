(define-module (crates-io mo ji) #:use-module (crates-io))

(define-public crate-moji-0.1 (crate (name "moji") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("default" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta-ext") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "emoji") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0f4i6wkl5m1lfzp8gj7xkg5sm97367f9l7139nshi8fd9pj15ldm")))

(define-public crate-mojibake-0.1 (crate (name "mojibake") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.11.1") (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "17305lspsbfh6vp1lxj1rnqljwv5h6x746s0qkipcsbkvhpwlkvm") (yanked #t)))

(define-public crate-mojibake-0.2 (crate (name "mojibake") (vers "0.2.0") (deps (list (crate-dep (name "phf") (req "^0.11.1") (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)) (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1dmx6wi57imynd4wzk0zkh5gcmc1qz1shcxvyzshw045wyjmwp8g")))

(define-public crate-mojibake-0.2 (crate (name "mojibake") (vers "0.2.1") (deps (list (crate-dep (name "phf") (req "^0.11.1") (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)) (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "14m38dfyn7v5wdnxazaphc4zfrxwbmcahvb5pbssqnspqjwska69")))

(define-public crate-mojimoji-rs-0.1 (crate (name "mojimoji-rs") (vers "0.1.0") (hash "189f4gbw6nz1m0if85q0k9dr468ih16qnl980nf8jzypl9z3and5")))

(define-public crate-mojimoji-rs-0.1 (crate (name "mojimoji-rs") (vers "0.1.1") (hash "0mnblcqvw2wg4n13v5xjq0yh63szqmg68phd98gjg4lj9flf3cav")))

