(define-module (crates-io mo bb) #:use-module (crates-io))

(define-public crate-mobbit-0.1 (crate (name "mobbit") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1jkzj6qkv3348xxnpdnppfnh0nlfk7rrcbf4n3xirrbsyzpqjbp2")))

(define-public crate-mobbit-0.1 (crate (name "mobbit") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1x5w1cbj3s3ksd3z1r9hbfgxm98xwls88k4pbx1m390pwrljxlm9")))

(define-public crate-mobbit-0.2 (crate (name "mobbit") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "03hzfrv76pygrq550qfh7jidr3jkncnxjdzsx5d4wnlr7qqpw9fw")))

