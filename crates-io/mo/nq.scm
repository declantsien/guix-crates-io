(define-module (crates-io mo nq) #:use-module (crates-io))

(define-public crate-monqyclouds_guessing_game-0.1 (crate (name "monqyclouds_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1as45s2cn6xizbiwjdcgw68k31mz83jwmrd2s130h685wh2grsym")))

