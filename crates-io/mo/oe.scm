(define-module (crates-io mo oe) #:use-module (crates-io))

(define-public crate-mooeye-0.2 (crate (name "mooeye") (vers "0.2.0") (deps (list (crate-dep (name "ggez") (req "^0.9.0-rc0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0k1gcysi8xsassfpji9dyshkmdjvazxba5j0kmcdldsxh1pm7h06")))

(define-public crate-mooeye-0.2 (crate (name "mooeye") (vers "0.2.1") (deps (list (crate-dep (name "ggez") (req "^0.9.0-rc0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0q8xca0k5sbbxcvcrspkyb6nl9q57bdcma637xbac8537znpi6jd")))

(define-public crate-mooeye-0.3 (crate (name "mooeye") (vers "0.3.0") (deps (list (crate-dep (name "ggez") (req "^0.9.0-rc0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1n1f0xncc6vjmi7hv32f2xaqjj5n44ydd0jfdnhrkb9vr9kxp91c")))

(define-public crate-mooeye-0.3 (crate (name "mooeye") (vers "0.3.1") (deps (list (crate-dep (name "ggez") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0n3s22rh4x0y5ljqp5s42pc5fc9bdgjmkr7167yvwm1r5s9lclks")))

(define-public crate-mooeye-0.3 (crate (name "mooeye") (vers "0.3.2") (deps (list (crate-dep (name "ggez") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0095v729x29qbhyyiin8qczgr2vjmnaw6y3sjdpgyik5ix530f2f")))

(define-public crate-mooeye-0.4 (crate (name "mooeye") (vers "0.4.0") (deps (list (crate-dep (name "ggez") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "15z9waqqgd0r0sifa6h53j2m4ac9lfvafppmziw2acs2xk67s6nd")))

(define-public crate-mooeye-0.4 (crate (name "mooeye") (vers "0.4.1") (deps (list (crate-dep (name "ggez") (req "=0.9.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0s85lhcv8b6nrcn8rdj2iaqw4zgbpg2lipzklvl536cj9i1vr8ds")))

