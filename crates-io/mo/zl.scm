(define-module (crates-io mo zl) #:use-module (crates-io))

(define-public crate-mozlz4-0.2 (crate (name "mozlz4") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mozlz4-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "15h5prq5ynqs3giry4yk6w2pdkn6pdvp3av5ly9876pg25akdwpj")))

(define-public crate-mozlz4-0.3 (crate (name "mozlz4") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mozlz4-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yzzhp5l0y30h7lha4s3k3izx1z4s8icasf6py6wxn7fq7vd88mm")))

(define-public crate-mozlz4-0.3 (crate (name "mozlz4") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mozlz4-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1p73x9cwing8h1drcw58f0csv9zwxfmgw16jn615v2kgmfq6hjkh")))

(define-public crate-mozlz4-sys-0.1 (crate (name "mozlz4-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "07ll3chz7ns4v3cy4j2vqhd7a1p95frlzx7cijky46vs5v6ghqms") (links "lz4")))

