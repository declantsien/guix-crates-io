(define-module (crates-io mo zs) #:use-module (crates-io))

(define-public crate-mozsvc-common-0.1 (crate (name "mozsvc-common") (vers "0.1.0") (deps (list (crate-dep (name "hostname") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)))) (hash "1yk7ynkmjsa1xy2nbi7mkdly8bijfdikvkzd3q0pw7dhfvaqc8vk")))

(define-public crate-mozsvc-common-0.1 (crate (name "mozsvc-common") (vers "0.1.1") (deps (list (crate-dep (name "hostname") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "0bpbmicjivg71s9h9847lyyw7pxc7xgplgbh5wpb3bfn5qcybpzg")))

(define-public crate-mozsvc-common-0.2 (crate (name "mozsvc-common") (vers "0.2.0") (deps (list (crate-dep (name "hostname") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1pzcc2p3rzc225s87qmmmlz0n5vps5vj5xq9lj4cl8zdgn6a7ysi")))

(define-public crate-mozsvc-common-wvp-0.1 (crate (name "mozsvc-common-wvp") (vers "0.1.0") (deps (list (crate-dep (name "hostname") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0szqgbiip0zslabr7x3730w0hq5z20sn9ymx5622282v6ncjlgk9")))

