(define-module (crates-io mo ec) #:use-module (crates-io))

(define-public crate-moecs-0.1 (crate (name "moecs") (vers "0.1.0") (deps (list (crate-dep (name "moecs_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0m1g0riz7b0v8hgq0zwmnbq8qpnwfvaygfpncnnsg331r1ic47g4")))

(define-public crate-moecs_macros-0.1 (crate (name "moecs_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1z1s045117kjwv2f67xbdfy8s599l9a3h38ycqy4h7i5nrnl2zb8")))

