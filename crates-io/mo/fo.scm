(define-module (crates-io mo fo) #:use-module (crates-io))

(define-public crate-mofo-0.1 (crate (name "mofo") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "15scpa3h7jvgzm7qz7a9s84pw8ry3a1r3qjclx5n3vgsfmfs4bxh")))

(define-public crate-mofo-0.1 (crate (name "mofo") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "0wpqijwh2rcqadjzmq1ah9nspwf6la6a90akn1ba1jqgcnx7lbcr")))

(define-public crate-mofo-0.1 (crate (name "mofo") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "127hkn04l56c24dr4xhx74h7x99r8nglsml77ymnanzd5hdfhai8")))

(define-public crate-mofo-0.1 (crate (name "mofo") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1jkwk18papwx399yzdnw35bj90vbrqx5ad97zizsvmn3klisyx6z")))

(define-public crate-mofo-0.1 (crate (name "mofo") (vers "0.1.4") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0pagbidsyp9pdf361l8wyva5bgdcdfb540j8w8napyjy0yzynqwv")))

(define-public crate-mofo-0.1 (crate (name "mofo") (vers "0.1.5") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "11f8n43sgzbip7y55msl4agpfg7hxnkav39fcgq6ffym9v4wn8zb")))

(define-public crate-mofo-0.2 (crate (name "mofo") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0aww21yd6j7rnjf24w8hrw2lhslbg588q3kfvi0rsy8q5kq4wklg")))

(define-public crate-mofo-0.2 (crate (name "mofo") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "12499a8kzfagizm6hi8f5p0aqxk7vjk8iikmch95dr5ra384zmck")))

(define-public crate-mofo-0.2 (crate (name "mofo") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "1r95zqp7ymw0d9gnx4lapgx8hsm5lxl615399l4rai9gdsy791i5")))

