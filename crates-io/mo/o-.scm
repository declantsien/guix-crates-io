(define-module (crates-io mo o-) #:use-module (crates-io))

(define-public crate-moo-math-0.1 (crate (name "moo-math") (vers "0.1.0") (hash "1irxr47pvi46mci1rc0qs5vz5zd791b35dl5j5z54x3x57lc2rxl")))

(define-public crate-moo-math-0.1 (crate (name "moo-math") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "11k66gg2kc82z76sgz2j6kps3aaxh3gymbfcxcb5afzf30j9kqdj")))

