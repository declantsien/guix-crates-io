(define-module (crates-io mo z_) #:use-module (crates-io))

(define-public crate-moz_cbor-0.1 (crate (name "moz_cbor") (vers "0.1.0") (hash "0f7jf0lnnr275f2rvvqwqc6xfiaxwrr6x67rwvcil1203rnwj3cz") (features (quote (("default"))))))

(define-public crate-moz_cbor-0.1 (crate (name "moz_cbor") (vers "0.1.1") (hash "0769w8av9ak5jynxksaq54yhrcn30wbgznr2f46rkmbz11bjmj10") (features (quote (("default"))))))

(define-public crate-moz_cbor-0.1 (crate (name "moz_cbor") (vers "0.1.2") (hash "1sd17yjb9p42nw4fps0cv0f45fhvg5mvjxw9gir7w6sr60ib2cr1") (features (quote (("default"))))))

(define-public crate-moz_genai-0.1 (crate (name "moz_genai") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "candle-core") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "candle-nn") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "candle-transformers") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "cbindgen") (req "^0.23") (default-features #t) (kind 1)) (crate-dep (name "hf-hub") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "tokenizers") (req "^0.15.0") (features (quote ("onig"))) (default-features #t) (kind 0)))) (hash "1j2zhalgzdm27ph7xlnpxkbhldf30h844jb9d1v5kgk092nw5vsp")))

(define-public crate-moz_genai-0.2 (crate (name "moz_genai") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "candle-core") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "candle-nn") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "candle-transformers") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "cbindgen") (req "^0.23") (default-features #t) (kind 1)) (crate-dep (name "hf-hub") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokenizers") (req "^0.15.0") (features (quote ("onig"))) (default-features #t) (kind 0)))) (hash "0pc653kyb80qzcf7rqbvdpi4xlkf59nxmymhbpcfj5513ggwgbmc")))

