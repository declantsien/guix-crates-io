(define-module (crates-io mo ss) #:use-module (crates-io))

(define-public crate-moss-0.0.1 (crate (name "moss") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wkja54f1pyxg321kxn3l3lb3rgg6hzzq58w783b86w54zhymg47")))

(define-public crate-moss-0.0.2 (crate (name "moss") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qb1g8a3avgb8qh63rrb7q13hc318yl8r52i20inh3n8lxdcj5j9")))

(define-public crate-moss-0.0.3 (crate (name "moss") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (kind 0)))) (hash "0skxqahkhnijas5nr3vcnyqzm56aym1fvq8k2ipk9rwypjv5c9y7") (features (quote (("math-la") ("long-gmp") ("default" "long-gmp" "math-la"))))))

(define-public crate-moss-0.0.4 (crate (name "moss") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0lgsz12vpzpfx0fz1z6v4smbhh2vsmslxhmc6flb6l3cx0hrsybk") (features (quote (("math-sf") ("math-la") ("long-none") ("long-gmp") ("la") ("graphics") ("default" "long-gmp" "la" "math-la" "math-sf" "graphics"))))))

(define-public crate-moss-fmt-0.1 (crate (name "moss-fmt") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "15ylann16byljzsqhzy1dmms34bbw0q5jgq9b6gsjhwsi4hyn36k")))

(define-public crate-moss-fmt-0.2 (crate (name "moss-fmt") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "19a577h0w29ll1916fn48zc7xf4y7j3crjp52blyi23bgh6kprw1")))

(define-public crate-moss-oxide-0.1 (crate (name "moss-oxide") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1d544z329a5m9sf3q565847pdmjl1yld8wqg6fjwcwfchr4knqa2")))

(define-public crate-moss-oxide-0.2 (crate (name "moss-oxide") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1ydfbh2z1p71z0hwrac83zywdagz6wnlyj8mrxas6bvdj19bhg5h")))

(define-public crate-moss-oxide-0.3 (crate (name "moss-oxide") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0nljf5r8x6lrfh17awlsfl6fps2iqdlffibl413s5gmx80zh1wx3")))

(define-public crate-moss-trimmer-0.1 (crate (name "moss-trimmer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lm6b9clyczqiikjfqfdq8i7r51gh0z2vvb5avp6m1nvxqfkkbbi")))

(define-public crate-moss-trimmer-1 (crate (name "moss-trimmer") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0m9aca0l0qcd9rjh0fra6mpfkg6sbrjdqmkffs6kby9bfgspap6b")))

(define-public crate-moss-trimmer-1 (crate (name "moss-trimmer") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1viv1rjsh4hkfx5m3773kxbyid9nwx30185qf2av8rakn9j0cqlh")))

(define-public crate-moss-trimmer-1 (crate (name "moss-trimmer") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19skzsqzimnqfclc5rza3db6394n6324ycp7sb8x7n4raw8ayz30")))

(define-public crate-moss-trimmer-1 (crate (name "moss-trimmer") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ylzhs00n3hmil1fgyawllky9i3i4g6hqawalf8ya51d17d1rgjh")))

(define-public crate-moss-trimmer-1 (crate (name "moss-trimmer") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "190nxr12rl310m4msbiw5b11qg6c1x59anwbhr9v427xaj288r44")))

(define-public crate-moss_glfw_windowing-0.1 (crate (name "moss_glfw_windowing") (vers "0.1.0") (deps (list (crate-dep (name "glfw") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "moss_windowing") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raw-window-handle") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0a71bnjsvjrm6akcfh99vimdg40rf3jyln688id7wkncrhd8mr6a") (yanked #t)))

(define-public crate-moss_input-0.1 (crate (name "moss_input") (vers "0.1.0") (hash "1i2hilnxb90ycm1fs4v6f4kjdwdqmb00d4k5dh51kd2nhil2spxz") (yanked #t)))

(define-public crate-moss_windowing-0.1 (crate (name "moss_windowing") (vers "0.1.0") (deps (list (crate-dep (name "moss_input") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "raw-window-handle") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0yjdz257n4crj0f207bs3p8gmqrw0jn0wlydvaq5yn9qsjlhcyby") (yanked #t)))

(define-public crate-mossback-0.0.0 (crate (name "mossback") (vers "0.0.0") (hash "0nz3vx2nxfnsdyr430k7ddsqgwxs481afp8shw96dn1zh90vjll4")))

(define-public crate-mossfets-game-of-life-0.7 (crate (name "mossfets-game-of-life") (vers "0.7.0") (deps (list (crate-dep (name "rayon") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0z3ajdlym92rbdv36bk26c6gfa9wpcpdyzsxqiwrgsniyj37mknl") (features (quote (("default" "advanced_threading" "advanced_hashing")))) (v 2) (features2 (quote (("advanced_threading" "dep:rayon") ("advanced_hashing" "dep:rustc-hash"))))))

(define-public crate-mossfets-game-of-life-0.8 (crate (name "mossfets-game-of-life") (vers "0.8.0") (deps (list (crate-dep (name "rayon") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "06i9w9gw78ab267x3wrrp0ms7mwq9jdbfrh6a4qwzqg05gshlj00") (features (quote (("default" "advanced_threading" "advanced_hashing")))) (v 2) (features2 (quote (("advanced_threading" "dep:rayon") ("advanced_hashing" "dep:rustc-hash"))))))

(define-public crate-mossy-0.1 (crate (name "mossy") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "175inf1h1d0wdpgvix125ai1mlx87hmj6a0a9krsmprpd7xjjq77")))

(define-public crate-mossy-0.1 (crate (name "mossy") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "10pijbga93jrk53nl4iac9cgca26r5715x2xi7r6nq60s2c5b8xm")))

(define-public crate-mossy-0.0.1 (crate (name "mossy") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "037bx6mniv29nmss4il4ql73nb5s5h0h8qr5swgmiamhw8w8hrxs")))

(define-public crate-mossy-0.1 (crate (name "mossy") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0cfamrpm86051qkzh8ywxq6cjj0z5s4r96lbv8rq60paqrc7hpkd")))

