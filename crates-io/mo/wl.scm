(define-module (crates-io mo wl) #:use-module (crates-io))

(define-public crate-mowl-1 (crate (name "mowl") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0") (default-features #t) (kind 0)))) (hash "13zmssw3d8dda44ba94ka04s82g8r8i4dhyy1i89879dwyxgksn2")))

(define-public crate-mowl-1 (crate (name "mowl") (vers "1.0.1") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0") (default-features #t) (kind 0)))) (hash "0ky1kq1vg9i57zrzqiizxspdcpsnb2hb2flcxr407js6k81h3k2v")))

(define-public crate-mowl-1 (crate (name "mowl") (vers "1.0.2") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0") (default-features #t) (kind 0)))) (hash "0ang4nz0pjyhh3qas2kvv0ksz14d6n9hfl2y1499z1f6vbl4slsx")))

(define-public crate-mowl-1 (crate (name "mowl") (vers "1.0.3") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0") (default-features #t) (kind 0)))) (hash "0sah7zy4qqhhdd0n4wm34zswlqhnr04p9caapxc307ma0y3lrlr3")))

(define-public crate-mowl-1 (crate (name "mowl") (vers "1.1.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0") (default-features #t) (kind 0)))) (hash "16xmdshgx36b1w47vpg8p53ldaqdpn8hg479c4rbjf8f1r1zd3f2")))

(define-public crate-mowl-1 (crate (name "mowl") (vers "1.2.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0") (default-features #t) (kind 0)))) (hash "10lpjldyj0j9vqfdv5lzyk8srrilb6b3fwyhkj2ilw3lkwg283zn")))

(define-public crate-mowl-1 (crate (name "mowl") (vers "1.3.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0") (default-features #t) (kind 0)))) (hash "0b72jja5x3zg0jzggirgn9k6g3y9ijfflw7n4qggyw5x60iac8n4")))

(define-public crate-mowl-2 (crate (name "mowl") (vers "2.0.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.3") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0kas5x2a9nhcky95p7zd5mlqb04vlzar6pg4mcag80pmks7f8azx")))

(define-public crate-mowl-2 (crate (name "mowl") (vers "2.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "1v1x4sc833j6435ixhdrzxl0gkiai6ll8q49xq1w266z310gcyf0")))

(define-public crate-mowl-2 (crate (name "mowl") (vers "2.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.25") (default-features #t) (kind 0)))) (hash "02jd4z5m40xcbi4afvl9g09zpnipdkm45qj1r8819dir36xpx31l")))

