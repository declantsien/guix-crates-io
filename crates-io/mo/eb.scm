(define-module (crates-io mo eb) #:use-module (crates-io))

(define-public crate-moebius-tools-0.1 (crate (name "moebius-tools") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("rustc-serialize" "serde" "unstable-locales"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive" "serde_derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "1b73mhg51w8hdm9wax5yppzlma007d9p8kikahcjf9n8nz4r40dx")))

