(define-module (crates-io mo in) #:use-module (crates-io))

(define-public crate-moin-0.0.0 (crate (name "moin") (vers "0.0.0-0") (hash "0dmxg399g2jvgl51cnfdx0k1jxgxm2a8r9mjdjzkjfpxf5pzy7cd")))

(define-public crate-moins-0.1 (crate (name "moins") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "0kqk7cisv4cl7i3r73kg5mxxkqmkxqnafggaham8crw52k9km9j1")))

(define-public crate-moins-0.1 (crate (name "moins") (vers "0.1.1") (deps (list (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "09b0wdy22syx7s0mh54b4fsa1zsqgv6aha30g47d1i3ni73b0d84")))

(define-public crate-moins-0.1 (crate (name "moins") (vers "0.1.2") (deps (list (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "08dfff7qal61ddd9f6s19qlsq9lw1xhzzx1hld8a3daqlknyfr78")))

(define-public crate-moins-0.4 (crate (name "moins") (vers "0.4.0") (deps (list (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "07hkkg4fvdc4xj57cih7d3lk3dwqbharbih4cch9v2ik9v5m40bc")))

(define-public crate-moins-0.5 (crate (name "moins") (vers "0.5.0") (deps (list (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1va9i3cpkm6zq2fsc4jds8kgbg7gnzjlf1r1aqr2dmlnl18nrqkr")))

