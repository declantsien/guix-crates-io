(define-module (crates-io mo si) #:use-module (crates-io))

(define-public crate-mosis-0.1 (crate (name "mosis") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1vxndf3z5746zmxwg1fh54736i4bz5c8bzcrs5dklnlrfnsdwmbd")))

(define-public crate-mosis-0.2 (crate (name "mosis") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0a3lsb0xh02p9iqmwgrrrc3wxvfj35n4jigjg95n9im28jgiyzsw") (yanked #t)))

(define-public crate-mosis-0.2 (crate (name "mosis") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1z5da15mvbf0ddqlglb0vwkmr824sa6phcnsxzz6hamy3p5x0val")))

