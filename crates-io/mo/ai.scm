(define-module (crates-io mo ai) #:use-module (crates-io))

(define-public crate-moai-0.0.0 (crate (name "moai") (vers "0.0.0") (hash "0n70ynp2p1hiqvg3gddgav41p7lb509i2nb6zs3zm5hzfvsq214f")))

(define-public crate-moai-0.0.1 (crate (name "moai") (vers "0.0.1") (deps (list (crate-dep (name "moai_core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "moai_window") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "glfw") (req "^0.51.0") (default-features #t) (kind 2)))) (hash "05m75ln47m5mykbb713mqxmjxrcv3w0bas5r8fi20wvvvybdpajj") (features (quote (("default" "moai_window"))))))

(define-public crate-moai-0.0.2 (crate (name "moai") (vers "0.0.2") (deps (list (crate-dep (name "moai_core") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "moai_window") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "glam") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "glfw") (req "^0.51.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 2)))) (hash "0yasfpd17vgjzvrv4qa19cvb1702firfhz1ymnvrf5rp9r15fbcp") (features (quote (("default" "moai_window"))))))

(define-public crate-moai_core-0.0.1 (crate (name "moai_core") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1x7wfy4kcvj9a58hh0kb86kpc2pbxc9yh16y1cz7qzv7m7zgaqrd")))

(define-public crate-moai_core-0.0.2 (crate (name "moai_core") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0kipg32y0ykb438z6jyjw9dxsv9g7316r42rgl0982jadjngjjn4")))

(define-public crate-moai_render-0.0.1 (crate (name "moai_render") (vers "0.0.1") (deps (list (crate-dep (name "glam") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "04nnswn31jasgmg9yfhpx1a40d09ir1wxr115gf9aaa96drgxadi")))

(define-public crate-moai_window-0.0.1 (crate (name "moai_window") (vers "0.0.1") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51.0") (default-features #t) (kind 0)))) (hash "0fr4ghxgpcg71lyd9gfc04pxcsmlsna3vj915a6h7i9axxrad9bw")))

