(define-module (crates-io mo nc) #:use-module (crates-io))

(define-public crate-monc-0.1 (crate (name "monc") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "monk-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ritelinked") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1bcdgyb7w24s8bhjrn9092xkzm4kil8ppkyzi2h8pm35lyzwykwv")))

(define-public crate-monch-0.1 (crate (name "monch") (vers "0.1.0") (hash "0zsn0w15sszj7v43kl3wjnipbf7l2lnd9cyf6m6lqyybw44z1xh0")))

(define-public crate-monch-0.2 (crate (name "monch") (vers "0.2.0") (hash "1f7yf2yhj1fvskyqar5hg1qflcn53qpjwn1lwmlq40b9gbr3f19m")))

(define-public crate-monch-0.2 (crate (name "monch") (vers "0.2.1") (hash "1i7hx8bx2y5ddac890xi5vx84wvrhk8zf00pnfdm5nysmn1f5qn5")))

(define-public crate-monch-0.3 (crate (name "monch") (vers "0.3.0") (hash "113v5v53kdxb4m93fpjw5zc29d490rx64limslgx28xa35n3vz7d")))

(define-public crate-monch-0.4 (crate (name "monch") (vers "0.4.0") (hash "1yksyqpc8i0hq17zy2h1813a7sxkk9pgaa8h7bfbk9f9xp1y2ggi")))

(define-public crate-monch-0.4 (crate (name "monch") (vers "0.4.1") (hash "0vv4kz8wwlcm1p05ffqfx5w2wbai9whrmb497g5xm35bjamw280i")))

(define-public crate-monch-0.4 (crate (name "monch") (vers "0.4.2") (hash "1q2h240mciaqs2ln7rvhndwhjfhp3px4azzjgss1lbi3gpff2wxv")))

(define-public crate-monch-0.4 (crate (name "monch") (vers "0.4.3") (hash "1hybn617653895f9qp9j7zlismvaxih10gy57nnmwbds8y4ah6a5")))

(define-public crate-monch-0.5 (crate (name "monch") (vers "0.5.0") (hash "1shy00250d4ck00c6ilmbnmsg2mnk79qn4qkxbn2l54qzwrinb5m")))

