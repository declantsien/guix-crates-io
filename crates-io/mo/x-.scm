(define-module (crates-io mo x-) #:use-module (crates-io))

(define-public crate-mox-impl-0.10 (crate (name "mox-impl") (vers "0.10.0-alpha") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snax") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0y01rqq3w0aylpbp68hilzrm7as5wxh50bam6svjwf3fkpd5fy77")))

(define-public crate-mox-impl-0.10 (crate (name "mox-impl") (vers "0.10.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snax") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0m71f2sagcvh0wyvmyvbd3jn1d66dgq451qb9yk9wkrynmaxblrd")))

(define-public crate-mox-impl-0.11 (crate (name "mox-impl") (vers "0.11.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn-rsx") (req "^0.8.0-beta.2") (default-features #t) (kind 0)))) (hash "079icjw8f0xhm3jk1l32jrzjg7nrihizs6a8clr6nl4vica03jx4")))

