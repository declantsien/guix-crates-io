(define-module (crates-io mo s_) #:use-module (crates-io))

(define-public crate-mos_6502_cpu-0.1 (crate (name "mos_6502_cpu") (vers "0.1.0") (hash "1mvfjadx5841qqsb7cvczad9i3ab1gjdyjm1avll7jqwzf0q9m37") (yanked #t)))

(define-public crate-mos_6502_cpu-0.1 (crate (name "mos_6502_cpu") (vers "0.1.1") (hash "1212fzin1d5a8vq41zj0bqqvbpqirijppjvwb1cqfz89xwmkw224")))

(define-public crate-mos_6502_cpu-0.1 (crate (name "mos_6502_cpu") (vers "0.1.2") (hash "0c49cgjha897s2gfzafw6hg56zlxx4szfwib8lmz1ncmnvw8as9s")))

