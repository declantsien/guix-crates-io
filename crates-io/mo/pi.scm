(define-module (crates-io mo pi) #:use-module (crates-io))

(define-public crate-mopidy-0.0.0 (crate (name "mopidy") (vers "0.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01psd0hnbzxghw8ihazzkv47x8ifmsks3aqxq6mkg40qavb2vnc9")))

