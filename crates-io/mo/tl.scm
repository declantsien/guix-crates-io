(define-module (crates-io mo tl) #:use-module (crates-io))

(define-public crate-motley-0.0.1 (crate (name "motley") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1i6xxp99dm858hzy51nc712daimyzabn766x4khyjf4mql39iipg")))

