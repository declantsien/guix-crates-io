(define-module (crates-io mo lr) #:use-module (crates-io))

(define-public crate-molrs-0.1 (crate (name "molrs") (vers "0.1.0") (hash "1nhrlb73rxba8qsp0xl3wd64ghrq5vaf1jl4yhy88sybndah9kws")))

(define-public crate-molrs-0.1 (crate (name "molrs") (vers "0.1.1") (deps (list (crate-dep (name "pertable") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "159mv8y2i1mpqrjkf7xrcjl6wc00991ls1f558q37pym1ij58a87")))

