(define-module (crates-io mo gr) #:use-module (crates-io))

(define-public crate-mogrify-0.1 (crate (name "mogrify") (vers "0.1.0") (deps (list (crate-dep (name "mogrify_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bhhpxxpcqsqadk55gkzpiqbqrsz27mqhq5minvps9iyc5icsjva")))

(define-public crate-mogrify-0.1 (crate (name "mogrify") (vers "0.1.1") (deps (list (crate-dep (name "mogrify_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1rryzpdf7sbvadivj5cdnnk3g0x7di4q9p462r41dg6290vshx7c")))

(define-public crate-mogrify_derive-0.1 (crate (name "mogrify_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "08fc1mrw0jida6838dsjm7jf5radpr37dhny1z7v2ydljjdw7063")))

(define-public crate-mogrify_derive-0.1 (crate (name "mogrify_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1nm9w9q31lkfh4878pwlsa3vg06nbw8xkhfbs67x71926b6pmqsp")))

