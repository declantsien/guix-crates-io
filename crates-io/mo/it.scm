(define-module (crates-io mo it) #:use-module (crates-io))

(define-public crate-moite_moite-0.1 (crate (name "moite_moite") (vers "0.1.0") (hash "1l4p5sl0clrkgh6icqzvwirkghmr75ky10db5r4cvs0bv6zpf14m")))

(define-public crate-moite_moite-0.2 (crate (name "moite_moite") (vers "0.2.0") (hash "1bx14klafi3m6ck6cn48r11nl4lgr9p2ngpf2vy2qbp1c56akdgf")))

