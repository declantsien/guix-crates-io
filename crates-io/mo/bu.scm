(define-module (crates-io mo bu) #:use-module (crates-io))

(define-public crate-mobugsnag-0.1 (crate (name "mobugsnag") (vers "0.1.0") (deps (list (crate-dep (name "ureq") (req "^2.0.1") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0r8yz7z9hd95530hk956lbh1klcymn05hij5qhdl5ildsg24284g")))

(define-public crate-mobugsnag-0.1 (crate (name "mobugsnag") (vers "0.1.1") (deps (list (crate-dep (name "ureq") (req "^2.0.1") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0vf8cf0ykk64wls71lglk4b7r4qxikxdvfakr6mmszx79r67ppl2")))

(define-public crate-mobugsnag-0.1 (crate (name "mobugsnag") (vers "0.1.2") (deps (list (crate-dep (name "ureq") (req "^2.0.1") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1g6blcgr9xwidaadxmk8jianr2p1r70bxf5izr7mvjkjr1vsfbjv")))

