(define-module (crates-io mo s6) #:use-module (crates-io))

(define-public crate-mos6502-0.0.1 (crate (name "mos6502") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vzr9f89dlx940zrxwvzc6l5vnqd52nx9qwxjak2zqxwv0l9r53l")))

(define-public crate-mos6502-0.2 (crate (name "mos6502") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3") (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "0mkz2y9f8im6g7d15q14bd5xka6ajm7k48mx9zzb21azqnjfa05y")))

(define-public crate-mos6502-0.3 (crate (name "mos6502") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 2)))) (hash "0vq1hg9xq993q88l2j084vg5x8c93zpsdkk0p1mxrbb44wvdc7dk")))

(define-public crate-mos6502-0.4 (crate (name "mos6502") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (kind 0)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 2)))) (hash "14bxpwvnrq439460w9dfplmy8p8bm26sc5ggdi7m3g1xvfjb55i7") (features (quote (("default" "decimal_mode") ("decimal_mode"))))))

(define-public crate-mos6502-0.5 (crate (name "mos6502") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1wckxzm7xwah157f1jy90xbqxyah2qvjw4j1wvvfsamcdhjc7zxp") (features (quote (("default" "decimal_mode") ("decimal_mode"))))))

(define-public crate-mos6502_assembler-0.1 (crate (name "mos6502_assembler") (vers "0.1.0") (deps (list (crate-dep (name "mos6502_model") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pk36lyxcrihcik4brs3apkjb9icqr11rb573627qw7ysy3nf9lw")))

(define-public crate-mos6502_assembler-0.2 (crate (name "mos6502_assembler") (vers "0.2.0") (deps (list (crate-dep (name "mos6502_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "1big9mjhmv8ahkgvm5wn667q2qk8yz8m05dpk9dhsc8780b7af86")))

(define-public crate-mos6502_assembler-0.3 (crate (name "mos6502_assembler") (vers "0.3.0") (deps (list (crate-dep (name "mos6502_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q3dcz3ds0qwb4i7s7h1fmiaydk18lbxqn7krsggfgmsw57zz7nc")))

(define-public crate-mos6502_model-0.1 (crate (name "mos6502_model") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bjzfja982sbbp08rk5jpy3s3nadvyh201h6f0pkr9s3dsw6wsx8") (features (quote (("serialize" "serde"))))))

(define-public crate-mos6502_model-0.2 (crate (name "mos6502_model") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16647hw2alf6yg5z47hp3ahbadvy6176hrckyhpbjl3nqszw5369") (features (quote (("serialize" "serde"))))))

(define-public crate-mos6502_model-0.2 (crate (name "mos6502_model") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08xwmhbyssywdf4llaqg7vcx13mwxraadda3k096xbrp75l7ab0h") (features (quote (("serialize" "serde"))))))

