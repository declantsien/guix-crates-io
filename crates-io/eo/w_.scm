(define-module (crates-io eo w_) #:use-module (crates-io))

(define-public crate-eow_words_quiz-0.1 (crate (name "eow_words_quiz") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "02pxmzbb19n6432qvyqx3k9y6fjbb6n33f966jl76jciymrmvvkm") (yanked #t)))

(define-public crate-eow_words_quiz-0.1 (crate (name "eow_words_quiz") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0sg7sb4cvrgl5057hvgjz0ndvf7vmsl24s9g4zfs9clg6lz33lcn") (yanked #t)))

(define-public crate-eow_words_quiz-0.1 (crate (name "eow_words_quiz") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0w92a5r1ymxccrg588sj73mkpj4b1v680d9j16rhsv2zc19rfbn7") (yanked #t)))

(define-public crate-eow_words_quiz-0.1 (crate (name "eow_words_quiz") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1011innk7lcyqmqw4yjvggpy1ynqw3rvv5xmx82ydc705c1x7n28")))

