(define-module (crates-io eo pl) #:use-module (crates-io))

(define-public crate-eopl-0.1 (crate (name "eopl") (vers "0.1.0") (hash "0whsx80msl912zam7vkjizjicq362rhbb2rs4w285bq60i7b9cdr") (yanked #t)))

(define-public crate-eoplus-1 (crate (name "eoplus") (vers "1.0.0-RC1") (deps (list (crate-dep (name "antlr-rust") (req "=0.3.0-beta") (default-features #t) (kind 0)))) (hash "0d2n7ppv34l3nzzcnw263awsj0rldyd7hb3fvy55jp9clfxwb2kg") (rust-version "1.61.0")))

