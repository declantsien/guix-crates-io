(define-module (crates-io eo ni) #:use-module (crates-io))

(define-public crate-eonil_apple_oslog-0.1 (crate (name "eonil_apple_oslog") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)))) (hash "0l35frjkrhqgk39k1z5ck1zikzk9h20s0jagjqrk7n8z5vvhmlq7")))

(define-public crate-eonil_apple_oslog-0.1 (crate (name "eonil_apple_oslog") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)))) (hash "1vpj9zjd8jf6q9sx2l84l2k9bq7wfdbjilfkwaqs3p4disvf9fpm")))

(define-public crate-eonil_apple_oslog-0.1 (crate (name "eonil_apple_oslog") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)))) (hash "1vj4czwqyfiy8mxv0p7d0klih9ql0k7nhz17hn60plvghlr86p48")))

