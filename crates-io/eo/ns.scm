(define-module (crates-io eo ns) #:use-module (crates-io))

(define-public crate-eons-0.1 (crate (name "eons") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0vy92jb2bfssjzyv9l24kr8rvcvahp9r4gbphgbsish4h7kd5qif")))

