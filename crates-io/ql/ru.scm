(define-module (crates-io ql ru) #:use-module (crates-io))

(define-public crate-qlrumap-0.0.1 (crate (name "qlrumap") (vers "0.0.1") (deps (list (crate-dep (name "ahash") (req "^0.7.6") (kind 0)) (crate-dep (name "hashbrown") (req "^0.12.1") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "sn_fake_clock") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)))) (hash "1a62zai9hv9cxj476g5g8r28w95xihg5xglfhcba4y36a8rzis8y") (features (quote (("fake-clock" "sn_fake_clock"))))))

(define-public crate-qlrumap-0.0.2 (crate (name "qlrumap") (vers "0.0.2") (deps (list (crate-dep (name "ahash") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.1") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "sn_fake_clock") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)))) (hash "0j0rbmhp0m36gm53zc8si0xlh6lpb78fci2myx6qn2cr51zaigr6") (features (quote (("fake-clock" "sn_fake_clock"))))))

