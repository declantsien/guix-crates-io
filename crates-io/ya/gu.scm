(define-module (crates-io ya gu) #:use-module (crates-io))

(define-public crate-yagui-0.0.1 (crate (name "yagui") (vers "0.0.1") (deps (list (crate-dep (name "conrod_core") (req "^0.71") (default-features #t) (kind 0)))) (hash "1yvj1rvb2sx47zll1g69yfaflphyyx67q4gc7wb34ddbvhaypnq3")))

(define-public crate-yagui-0.0.2 (crate (name "yagui") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "conrod_core") (req "^0.71") (default-features #t) (kind 0)) (crate-dep (name "conrod_glium") (req "^0.71") (default-features #t) (kind 0)) (crate-dep (name "conrod_winit") (req "^0.71") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pl5p4jvz7xf4cz4x8lcc9rnpj2xrxwklz531zg9s3bkh5mgc8da")))

