(define-module (crates-io ya wn) #:use-module (crates-io))

(define-public crate-yawn-0.1 (crate (name "yawn") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "0m4mpknf38rs7r0vhgnp1sxj4gbx7v4nj08g53kv1466v8xg7rp2")))

(define-public crate-yawn-0.1 (crate (name "yawn") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "1bah431i6wry1pqxf1rwkb213wbljlsac1dlhwfqyzag28a1blmh")))

(define-public crate-yawn-0.1 (crate (name "yawn") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "0x1hc00b1c6iwqqxy70l3xhr7m0ykq976c69rrvhpsyvwaz7cyi3")))

(define-public crate-yawn-0.1 (crate (name "yawn") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "13rkc4y8h96pzzjwsdx5k0d7czgmi7iml4mllf0dmhiyz5acmcms")))

