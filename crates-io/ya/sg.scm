(define-module (crates-io ya sg) #:use-module (crates-io))

(define-public crate-yasg-0.0.0 (crate (name "yasg") (vers "0.0.0") (hash "1nwca2nmlk8dwgx3avm4w3h1m53b98bdd4lxqfx36pz7qr2pp001")))

(define-public crate-yasg-0.1 (crate (name "yasg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "mustache") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "055xyd1imw500010r5ca5sgwlc6bba0zmp3xrcisxqdwj1izv7a6")))

(define-public crate-yasgs-0.1 (crate (name "yasgs") (vers "0.1.0") (hash "1rd8dx2a10zmx61kfi16xqpyvahhr8x8g4vzgvvrm0l228glrwcw")))

(define-public crate-yasgs-0.1 (crate (name "yasgs") (vers "0.1.1") (hash "1r5san0idi2hkh6yxfrkyl03x03dslzr1y834c4rx49qnmw9w0i4")))

