(define-module (crates-io ya ns) #:use-module (crates-io))

(define-public crate-yansi-0.1 (crate (name "yansi") (vers "0.1.0") (hash "1qsa5pcvlk4snv7a1kivkww2pa7xlm5rq0gmbmg02kplkphkdj9z") (features (quote (("nightly"))))))

(define-public crate-yansi-0.1 (crate (name "yansi") (vers "0.1.1") (hash "112vhabxy24f3gr6k3gqx4zz5r7aq7n6i1ks38v1j2kxnkx363xr") (features (quote (("nightly"))))))

(define-public crate-yansi-0.2 (crate (name "yansi") (vers "0.2.0") (hash "1n0ii1vds80lply7670k25kr3zllvy3qrmkq4hj4011zf2hcawha") (features (quote (("nightly"))))))

(define-public crate-yansi-0.3 (crate (name "yansi") (vers "0.3.0") (hash "00n9s67hvbgcqgbzk30pq3dp57395glqrn1n527rid0kgidz2cdp") (features (quote (("nightly"))))))

(define-public crate-yansi-0.3 (crate (name "yansi") (vers "0.3.1") (hash "1pnmgsj3lkyc86rsqka5yi38dz54wiwnqqdbmkxysa2d6xbglbfx") (features (quote (("nightly"))))))

(define-public crate-yansi-0.3 (crate (name "yansi") (vers "0.3.2") (hash "02sw7ydxwa33swyqr0lrnhqlpyb8npixhb5qjhwfd802q0gj2wb8") (features (quote (("nightly"))))))

(define-public crate-yansi-0.3 (crate (name "yansi") (vers "0.3.3") (hash "0fjpc038g2j7ffbk0aj95pcqpbl4l8b0cl6pr172vflcm0gm4042") (features (quote (("nightly"))))))

(define-public crate-yansi-0.3 (crate (name "yansi") (vers "0.3.4") (hash "0d7r70nzh342wb8fpikia7gbkcw8pbfixvf8jfk4bw99lvpf80x5") (features (quote (("nightly"))))))

(define-public crate-yansi-0.4 (crate (name "yansi") (vers "0.4.0") (deps (list (crate-dep (name "parking_lot") (req "^0.5") (features (quote ("nightly"))) (default-features #t) (kind 2)))) (hash "0j6yjgzglk8fjgriljk43kxf21bl11dlpf5k0vxl5v6dr543n36n")))

(define-public crate-yansi-0.5 (crate (name "yansi") (vers "0.5.0") (deps (list (crate-dep (name "parking_lot") (req "^0.5") (features (quote ("nightly"))) (default-features #t) (kind 2)))) (hash "0wdx8syhc61lphmgw5cw1vq73isi4szjqriz1k07z19r3r59ziwz")))

(define-public crate-yansi-0.5 (crate (name "yansi") (vers "0.5.1") (deps (list (crate-dep (name "serial_test") (req "^0.6") (default-features #t) (kind 2)))) (hash "1v4qljgzh73knr7291cgwrf56zrvhmpn837n5n5pypzq1kciq109")))

(define-public crate-yansi-1 (crate (name "yansi") (vers "1.0.0-beta") (deps (list (crate-dep (name "is-terminal") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)))) (hash "10pgcwn1azz059n88axjrxaqhds6c35wh7ngcpgpr59445yzz67m") (features (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (rust-version "1.61")))

(define-public crate-yansi-1 (crate (name "yansi") (vers "1.0.0-gamma") (deps (list (crate-dep (name "is-terminal") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)))) (hash "16k5w0vlbfs0jiqjmxcxcqc6i53k69acqapdpgakyzsm5z25zb3g") (features (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (rust-version "1.61")))

(define-public crate-yansi-1 (crate (name "yansi") (vers "1.0.0-rc") (deps (list (crate-dep (name "is-terminal") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)))) (hash "0gp4lxs1h49drdxraj4yja6jg68bn2rql0ja1v23ppai72nldrwy") (features (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (rust-version "1.63")))

(define-public crate-yansi-1 (crate (name "yansi") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "is-terminal") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)))) (hash "0xr3n41j5v00scfkac2d6vhkxiq9nz3l5j6vw8f3g3bqixdjjrqk") (features (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (rust-version "1.63")))

(define-public crate-yansi-1 (crate (name "yansi") (vers "1.0.0") (deps (list (crate-dep (name "is-terminal") (req "^0.4.11") (optional #t) (default-features #t) (kind 0)))) (hash "1lx0vs970w46d3x02g6k5bbjzcgp2zjb3f88az4qzv2qdzbn2a3c") (features (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (yanked #t) (rust-version "1.63")))

(define-public crate-yansi-1 (crate (name "yansi") (vers "1.0.1") (deps (list (crate-dep (name "is-terminal") (req "^0.4.11") (optional #t) (default-features #t) (kind 0)))) (hash "0jdh55jyv0dpd38ij4qh60zglbw9aa8wafqai6m0wa7xaxk3mrfg") (features (quote (("std" "alloc") ("hyperlink" "std") ("detect-tty" "is-terminal" "std") ("detect-env" "std") ("default" "std") ("alloc") ("_nightly")))) (rust-version "1.63")))

(define-public crate-yansi-term-0.1 (crate (name "yansi-term") (vers "0.1.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.4") (features (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "processenv"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1isvjdslb6vp67hj32d4wmavsvd55c25x6vadxsch4qa8x0ani3y") (features (quote (("derive_serde_style" "serde"))))))

(define-public crate-yansi-term-0.1 (crate (name "yansi-term") (vers "0.1.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.4") (features (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "processenv"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1lp5310bzh4znzn1wgzqy3wl1imsw70sjbddz9imhx2ya540s3d3") (features (quote (("derive_serde_style" "serde"))))))

(define-public crate-yansi-term-0.1 (crate (name "yansi-term") (vers "0.1.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.4") (features (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "processenv"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1w8vjlvxba6yvidqdvxddx3crl6z66h39qxj8xi6aqayw2nk0p7y") (features (quote (("derive_serde_style" "serde"))))))

(define-public crate-yansongda-utils-0.9 (crate (name "yansongda-utils") (vers "0.9.0") (deps (list (crate-dep (name "once_cell") (req "~1.17.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ngy0ai37n9dql6zq4cgmx7j64kqqrx3v8gpb078p45xiplq617c")))

(define-public crate-yansongda-utils-1 (crate (name "yansongda-utils") (vers "1.0.0") (deps (list (crate-dep (name "once_cell") (req "~1.17.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1vikp9gjndnprx83x7vy0ig11cywxdahv79alrwr2wvzcgql0d4q") (features (quote (("phone" "macros" "serde") ("macros" "regex" "once_cell"))))))

(define-public crate-yansongda-utils-1 (crate (name "yansongda-utils") (vers "1.0.1") (deps (list (crate-dep (name "once_cell") (req "~1.17.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0.92") (default-features #t) (kind 2)))) (hash "0afjmyy886mp75ma4h9mdzy00mp2xbzd9glsaq1vh03nvi2d4y0d") (features (quote (("phone" "macros" "serde") ("macros" "regex" "once_cell"))))))

(define-public crate-yansongda-utils-1 (crate (name "yansongda-utils") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "~1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~1.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "~1.0.130") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0.92") (default-features #t) (kind 2)))) (hash "11wjvzi85hfwr31lf7m4gc4cxdby96ap61ca8x6hp8jsj3f4wyy0") (features (quote (("phone" "macros" "serde") ("macros" "regex"))))))

