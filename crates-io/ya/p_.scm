(define-module (crates-io ya p_) #:use-module (crates-io))

(define-public crate-yap_streaming-0.1 (crate (name "yap_streaming") (vers "0.1.0") (deps (list (crate-dep (name "yap") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1bzpg2hb2ns1bnwwk2fm5wm23ds2mb00mqr7avw52aihi794m521") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-yap_streaming-0.1 (crate (name "yap_streaming") (vers "0.1.1") (deps (list (crate-dep (name "yap") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1a6pdz5l84cxn2hn4s456bpl5rxv0lqih1mcsyywp4748cyl3x4m") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-yap_streaming-0.2 (crate (name "yap_streaming") (vers "0.2.0") (deps (list (crate-dep (name "yap") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0agzhkdzbzx5lgr5svx7i1443lsp2775f7lw81lzzqvhpwrav7jx") (features (quote (("default" "alloc") ("alloc"))))))

