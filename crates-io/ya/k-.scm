(define-module (crates-io ya k-) #:use-module (crates-io))

(define-public crate-yak-sitter-0.3 (crate (name "yak-sitter") (vers "0.3.0") (deps (list (crate-dep (name "streaming-iterator") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.20.10") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-json") (req "^0.19.0") (default-features #t) (kind 2)) (crate-dep (name "tree-sitter-rust") (req "^0.20.3") (default-features #t) (kind 2)))) (hash "0smk35fawmj8l3yrvam26k4y19nzkqawnwfphh55zs9qlbsqgi1i")))

(define-public crate-yak-sitter-0.3 (crate (name "yak-sitter") (vers "0.3.1") (deps (list (crate-dep (name "streaming-iterator") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-json") (req "^0.20.2") (default-features #t) (kind 2)) (crate-dep (name "tree-sitter-rust") (req "^0.20.4") (default-features #t) (kind 2)))) (hash "1m38qjn2vljd73v6bvax6pqmbwxjvbq8n62wgd7gga2ayk5z72zr") (yanked #t)))

(define-public crate-yak-sitter-0.4 (crate (name "yak-sitter") (vers "0.4.0") (deps (list (crate-dep (name "streaming-iterator") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-json") (req "^0.20.2") (default-features #t) (kind 2)) (crate-dep (name "tree-sitter-rust") (req "^0.20.4") (default-features #t) (kind 2)))) (hash "1ns0k7mz3j0jykss8zsb5c2fg51fw6n6jppb1dk8k4qjndiisgf8")))

