(define-module (crates-io ya rc) #:use-module (crates-io))

(define-public crate-yarcd-0.3 (crate (name "yarcd") (vers "0.3.0") (deps (list (crate-dep (name "bio") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "bzip2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rust-lzma") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dcs89xm7dr221wxg9digi0hr0yl597xrr3j0zbd4n8sq4gq4dcs")))

(define-public crate-yarcd-0.4 (crate (name "yarcd") (vers "0.4.0") (hash "0fbb471xb7ylhldlsaf81mi08ggrzig3lwyfxik35hkx59c9hncp")))

