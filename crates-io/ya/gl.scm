(define-module (crates-io ya gl) #:use-module (crates-io))

(define-public crate-yagl-0.0.1 (crate (name "yagl") (vers "0.0.1") (deps (list (crate-dep (name "a2d") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1fy5j634br5lngmhigjw63csknqf7549cczk8njqknjgqvrcpngk")))

(define-public crate-yagl-0.0.2 (crate (name "yagl") (vers "0.0.2") (deps (list (crate-dep (name "a2d") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0lifg8hpgr2ry756vz37hjc0ckpmpnbdms4z5a8609bzqq82bfgv")))

(define-public crate-yagl-0.0.3 (crate (name "yagl") (vers "0.0.3") (deps (list (crate-dep (name "a2d") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1r28xqcrbkvwvbyqnyk0rb0v03lc914kplxp990b0q12lqyidj3m")))

(define-public crate-yagl-0.0.4 (crate (name "yagl") (vers "0.0.4") (deps (list (crate-dep (name "a2d") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "16qfl9ikyx0ji0vsq54cq9dyafl1acvl5yfxs9q3dbsyz97x4a1s")))

(define-public crate-yagl-0.0.5 (crate (name "yagl") (vers "0.0.5") (deps (list (crate-dep (name "a2d") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1xy4zx69bdzzf7vgcz960m31jl9axnfaq1n6qj2wg97dp9vjr9qa")))

(define-public crate-yagl-0.0.6 (crate (name "yagl") (vers "0.0.6") (deps (list (crate-dep (name "a2d") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "07fg1bxas7p0vfga7h1apaw5ddjr5hfmilaha622adv529wxhy5f")))

(define-public crate-yagl-0.0.7 (crate (name "yagl") (vers "0.0.7") (deps (list (crate-dep (name "a2d") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0fpc70avz7kp0jx0hdvp1vj4snanya5624ncg32cm6yn2kz330gv")))

(define-public crate-yagl-0.0.8 (crate (name "yagl") (vers "0.0.8") (deps (list (crate-dep (name "a2d") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0j4fd0rwg76y9mqk5j32f0q52j71yn5sfc8lgvvsvr412738ib3p")))

(define-public crate-yagl-0.0.9 (crate (name "yagl") (vers "0.0.9") (deps (list (crate-dep (name "a2d") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "064j14zfnf8n8s9h3376caama46bnxmm1pxsmb4qq7162cwmv6d0")))

(define-public crate-yagl-0.0.10 (crate (name "yagl") (vers "0.0.10") (deps (list (crate-dep (name "a2d") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "gilrs") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "06wg69x94byiwg5fa9qbqxqifyj6lccrqkwvk1x9fzfhc3vnjfb2")))

