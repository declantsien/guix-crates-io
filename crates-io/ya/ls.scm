(define-module (crates-io ya ls) #:use-module (crates-io))

(define-public crate-yalskv-0.1 (crate (name "yalskv") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "10mp71ynrq80812xmp7bm0fg6lc1b4qf3kbfhz26yx18ajjyk5sd")))

