(define-module (crates-io ya ku) #:use-module (crates-io))

(define-public crate-yakui-0.1 (crate (name "yakui") (vers "0.1.0") (deps (list (crate-dep (name "yakui-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "yakui-widgets") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1chamdwis7wsh96xhxy8jqmhg34vgys5dh07i85nsv0jf37k2iy2")))

(define-public crate-yakui-0.2 (crate (name "yakui") (vers "0.2.0") (deps (list (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "yakui-widgets") (req "^0.2.0") (kind 0)))) (hash "065q55vv2cqqykc0ji141fi92hykgwf32018aqlcnl7d32dmcwnk") (features (quote (("default-fonts" "yakui-widgets/default-fonts") ("default" "default-fonts"))))))

(define-public crate-yakui-core-0.1 (crate (name "yakui-core") (vers "0.1.0") (deps (list (crate-dep (name "anymap") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "profiling") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "thunderdome") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1ds1f3x8if39vw7ffc0ksglsgddhfnh9gvav7vlyzlk1bancb4fn")))

(define-public crate-yakui-core-0.2 (crate (name "yakui-core") (vers "0.2.0") (deps (list (crate-dep (name "anymap") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "keyboard-types") (req "^0.6.2") (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "profiling") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "thunderdome") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1q4mg07fsvm7ws5y926hbx4w328ablsaacd0bkqcvzzs8jsngn01")))

(define-public crate-yakui-macroquad-0.1 (crate (name "yakui-macroquad") (vers "0.1.0") (deps (list (crate-dep (name "macroquad") (req "^0.3.24") (kind 0)) (crate-dep (name "yakui-miniquad") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "18ng10mqsl28s4z3xb11gzffnnfxj3c5f7b30blhrkk5s5albr5v")))

(define-public crate-yakui-macroquad-0.1 (crate (name "yakui-macroquad") (vers "0.1.1") (deps (list (crate-dep (name "macroquad") (req "^0.3.24") (kind 0)) (crate-dep (name "yakui-miniquad") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "16hn4f8944klnxw3h5ggr95gwzrs6rsaxw7vdf3f38k4qaa1xdqz")))

(define-public crate-yakui-macroquad-0.2 (crate (name "yakui-macroquad") (vers "0.2.0") (deps (list (crate-dep (name "macroquad") (req "^0.3.24") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-miniquad") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02cwa4gwfcqg9appc61j4yif2hghc8vwaypvf20z7qjmkdy8jwvj")))

(define-public crate-yakui-macroquad-0.2 (crate (name "yakui-macroquad") (vers "0.2.1") (deps (list (crate-dep (name "macroquad") (req "^0.3.24") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-miniquad") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1dl9mg710vc4xj2rsc4lhr8337cxkynlhn16ch8gp5b77y0kwh67")))

(define-public crate-yakui-macroquad-0.2 (crate (name "yakui-macroquad") (vers "0.2.2") (deps (list (crate-dep (name "macroquad") (req "^0.3.24") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-miniquad") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "01r7yyp9qx6z1kin82gvk5dzi5lrgn0r99775idamd0sgp477qnc")))

(define-public crate-yakui-macroquad-0.3 (crate (name "yakui-macroquad") (vers "0.3.0") (deps (list (crate-dep (name "macroquad") (req "^0.4.5") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-miniquad") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0hp8jyi1pxqd8g406vzydfgn7w6r01splx7g74qlpc24s04q7wyr")))

(define-public crate-yakui-macroquad-0.3 (crate (name "yakui-macroquad") (vers "0.3.1") (deps (list (crate-dep (name "macroquad") (req "^0.4.5") (kind 0)) (crate-dep (name "send_wrapper") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-miniquad") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1f02i208kywamra56sdmsyds3ld149zk33sl7qkjlgggi7nzm3w7")))

(define-public crate-yakui-miniquad-0.1 (crate (name "yakui-miniquad") (vers "0.1.0") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0lkpnmspacyxy4lnsmsmnx766fzk1r87z7gkz40vjm8wifmx4qjh")))

(define-public crate-yakui-miniquad-0.1 (crate (name "yakui-miniquad") (vers "0.1.1") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1znlg1v1j9j6qb06582r3apa270l5aw8z6gqgzr185mv6ham55fx")))

(define-public crate-yakui-miniquad-0.1 (crate (name "yakui-miniquad") (vers "0.1.2") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04yd7abmxb4hahz5j8iklvvzbfzm7pmnxwqvvvhkgangclww73gi")))

(define-public crate-yakui-miniquad-0.1 (crate (name "yakui-miniquad") (vers "0.1.3") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lb5fv1ah7cdssbkb88m9gl5nnjsxxv2w5bzg30sq9p9sqkxrrcr")))

(define-public crate-yakui-miniquad-0.1 (crate (name "yakui-miniquad") (vers "0.1.4") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ah0nbx81acpm6blff5np2scnhra1w3lfh199z4dccp3rj703h09")))

(define-public crate-yakui-miniquad-0.2 (crate (name "yakui-miniquad") (vers "0.2.0") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1q8x9m707raqafq6wymk67yc8c9w23zkqwqdbdzjjvc9sdmpfml5")))

(define-public crate-yakui-miniquad-0.2 (crate (name "yakui-miniquad") (vers "0.2.1") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0x79wi2z8b3r13kaj845agrsjzr1v1l3i3k29a2sv97k2bp2nbm0")))

(define-public crate-yakui-miniquad-0.2 (crate (name "yakui-miniquad") (vers "0.2.2") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1hanigbmkdsjy6f5xmv99cdc4dj14n1s5ibf5id3bpcr2pk0wls7")))

(define-public crate-yakui-miniquad-0.2 (crate (name "yakui-miniquad") (vers "0.2.3") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "01a7y2h3840wairvg5wq1rkgbjqi5hkam42ik8jlbl8pv9y8gwbp")))

(define-public crate-yakui-miniquad-0.2 (crate (name "yakui-miniquad") (vers "0.2.4") (deps (list (crate-dep (name "miniquad") (req "^0.3.16") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1s9kmg6zi5jjx060cdm3rafjgg175h140mcrf8wpql3fb1kwkw7r")))

(define-public crate-yakui-miniquad-0.3 (crate (name "yakui-miniquad") (vers "0.3.0") (deps (list (crate-dep (name "miniquad") (req "^0.4.0") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "07gjpyw6cn8yaazbvxshbzl1qb1l22br453xdl0p58wbhqypa8dm")))

(define-public crate-yakui-miniquad-0.3 (crate (name "yakui-miniquad") (vers "0.3.1") (deps (list (crate-dep (name "miniquad") (req "^0.4.0") (kind 0)) (crate-dep (name "yakui") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1249dqsyxqijnbi8gqg726zfbb06z9mjwgqwxg9ji35vhrv2ncci")))

(define-public crate-yakui-wgpu-0.1 (crate (name "yakui-wgpu") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.10.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.21.2") (features (quote ("bytemuck"))) (default-features #t) (kind 0)) (crate-dep (name "thunderdome") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "yakui-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17ldjqznbz6cizfwfdi2mc0ylmi48nl437vkmqs8a0rwpmnqj6gw")))

(define-public crate-yakui-wgpu-0.2 (crate (name "yakui-wgpu") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.10.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.21.2") (features (quote ("bytemuck"))) (default-features #t) (kind 0)) (crate-dep (name "profiling") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "thunderdome") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wdhhmqfq9aqdrhf8lxvsr9qmyc4pvn8vr4zq29kv0cb2a8rz34x")))

(define-public crate-yakui-widgets-0.1 (crate (name "yakui-widgets") (vers "0.1.0") (deps (list (crate-dep (name "fontdue") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "yakui-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "048fvb1541hrjqwfvzkdsrflczapd1qvb1chb8qh1gn3mi2s6klw")))

(define-public crate-yakui-widgets-0.2 (crate (name "yakui-widgets") (vers "0.2.0") (deps (list (crate-dep (name "fontdue") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.15.0") (default-features #t) (kind 2)) (crate-dep (name "smol_str") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "thunderdome") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1171mm5yg9p9kcnmhhjpc1lgih6lk8r5dlp0daivjs13q5n43bcn") (features (quote (("default-fonts") ("default" "default-fonts"))))))

(define-public crate-yakui-winit-0.1 (crate (name "yakui-winit") (vers "0.1.0") (deps (list (crate-dep (name "winit") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "yakui-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "132kpky12kd30pf9cvpcfd9y4mnc3fzgvfgvdvafqkcxjnvg2w5m")))

(define-public crate-yakui-winit-0.2 (crate (name "yakui-winit") (vers "0.2.0") (deps (list (crate-dep (name "winit") (req "^0.27.3") (default-features #t) (kind 0)) (crate-dep (name "yakui-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1y8s5b4gmqwg76w63fklbsqjyh29l0k7qznajsrsvvw31ka25z4k")))

