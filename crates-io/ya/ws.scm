(define-module (crates-io ya ws) #:use-module (crates-io))

(define-public crate-yaws-0.0.0 (crate (name "yaws") (vers "0.0.0") (deps (list (crate-dep (name "lunatic") (req "^0.9") (default-features #t) (kind 0)))) (hash "1kccrkynmjczmdw95c6wdbma8v7x4m8k17iwdyg2aw6xvjmyzi0y")))

(define-public crate-yaws-0.0.0 (crate (name "yaws") (vers "0.0.0-202301.Jan.15") (hash "126hykwlcaawnyagyjyq7ql40vhmnynaqca2jk9a6qd3bw12rf45")))

(define-public crate-yaws-run-0.0.0 (crate (name "yaws-run") (vers "0.0.0-202301.Jan.15") (hash "1q02w2v2ylibmdkb3jdlpiwxl5n0c6085kaaa06jxfw7xgggmjgm") (rust-version "1.60.0")))

(define-public crate-yaws-run-lunatic-0.0.0 (crate (name "yaws-run-lunatic") (vers "0.0.0-202301.Jan.15") (hash "0sv1bl5lv42y69s216ax8c6d7rgsb9lqlxa4ra0nyciysfw66jp1")))

(define-public crate-yaws-run-uring-0.0.0 (crate (name "yaws-run-uring") (vers "0.0.0-202301.Jan.15") (hash "1xsmkvxyn9raaziya247kxzvaf9m1if6d321b0ggkj65cg6shgc9")))

(define-public crate-yaws-spec-h1-0.0.0 (crate (name "yaws-spec-h1") (vers "0.0.0-202301.Jan.15") (deps (list (crate-dep (name "httparse") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tokio-uring") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rs5g0piii57jnd20jclmgmqh3bwa4dva1xxzsicxhk6qlan205c")))

(define-public crate-yaws-spec-h2-0.0.0 (crate (name "yaws-spec-h2") (vers "0.0.0-202301.Jan.15") (hash "04507lii0fphylcr25l1m076a6rp5qkpyg8338hzjz8cgk48b9g1") (rust-version "1.60.0")))

