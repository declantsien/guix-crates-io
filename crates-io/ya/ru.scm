(define-module (crates-io ya ru) #:use-module (crates-io))

(define-public crate-yarustc8e-0.1 (crate (name "yarustc8e") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1l0ygkg37x4l29hdqylglr2444bz2h6qfqc9njz96xc5s9kqmwai") (yanked #t)))

(define-public crate-yarustc8e-0.1 (crate (name "yarustc8e") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "12n925sa8dgf0ahxldq6jv0drli8p8pdav2kjx3y7z6gz2swn97n") (yanked #t)))

(define-public crate-yarustc8e-0.1 (crate (name "yarustc8e") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0yrjfrf687gbsyv7n6sn5s4fp0bwri5737lascl66fmf7hy7wq4i") (yanked #t)))

(define-public crate-yarustc8e-0.1 (crate (name "yarustc8e") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0hrclg9spmp4bpw0bwmpq8yqfmq86pcrrwhdmaz1x75pri1v2dhm") (yanked #t)))

(define-public crate-yarustc8e-0.1 (crate (name "yarustc8e") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "19z4mcbn6n1p3miqb06a1swxvn2fb3dwn8qj2sv4mlimdjavnx90") (yanked #t)))

(define-public crate-yarustc8e-cli-0.1 (crate (name "yarustc8e-cli") (vers "0.1.1") (deps (list (crate-dep (name "booldisplay") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "yarustc8e") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "08l1il8fjq39ld3pchpp4fcb2jkjvwc9g8p45vsg4az4zmdhmi9n") (yanked #t)))

(define-public crate-yarustc8e-cli-0.1 (crate (name "yarustc8e-cli") (vers "0.1.3") (deps (list (crate-dep (name "booldisplay") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "yarustc8e") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0vsa6g82righj1cckxzl0d0l0slp78w1cpvbx7f8l9bjv2wzqw3j") (yanked #t)))

(define-public crate-yarustc8e-cli-0.1 (crate (name "yarustc8e-cli") (vers "0.1.4") (deps (list (crate-dep (name "booldisplay") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "yarustc8e") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "17ryjaci8l17dnla8ds3ikv7r6yisiqncijvmrs8rvg1076i03zk") (yanked #t)))

