(define-module (crates-io ya go) #:use-module (crates-io))

(define-public crate-yago-4 (crate (name "yago") (vers "4.0.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (features (quote ("cloudflare_zlib"))) (kind 0)) (crate-dep (name "percent-encoding") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rio_api") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rio_turtle") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0i04farjshf3prg0jc0h5ix5dwvxi662ld6ka6z07kak4zzj45n7")))

(define-public crate-yagoll-0.1 (crate (name "yagoll") (vers "0.1.0") (hash "07szm5p4irbm8mii182ac8y2cw653sck6wgnaw7d47nh13p99mzk")))

(define-public crate-yagoll-0.1 (crate (name "yagoll") (vers "0.1.1") (hash "0150sk84k5cql4m55zwg1cz8ccp24wd3c37a4sa4h75wbln4h1i0")))

(define-public crate-yagoll-0.1 (crate (name "yagoll") (vers "0.1.2") (hash "064h0gb0x5cg90qzpc5hwa5vd5lxrrv6x2virhi42cia716b8iib")))

