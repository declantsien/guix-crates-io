(define-module (crates-io ya -p) #:use-module (crates-io))

(define-public crate-ya-packet-trace-0.1 (crate (name "ya-packet-trace") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^0.9") (default-features #t) (kind 2)))) (hash "0k3izcd0hkdy53dh65dlrsqfwgfbh7rgdnia9mc9llax8x2597ia") (features (quote (("enable") ("default"))))))

(define-public crate-ya-pkg-config-0.0.1 (crate (name "ya-pkg-config") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "13r2pa3lc5scv9w1yrg1axr4vl7ylmmlf9rv1jj2qi7aprjbsxzp")))

