(define-module (crates-io ya ga) #:use-module (crates-io))

(define-public crate-yaga-0.1 (crate (name "yaga") (vers "0.1.0") (deps (list (crate-dep (name "dialoguer") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "yaga-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05cxzmwq94f0ypfy6m6rwmjvady23c4fgwyc7wwn2wkrwxa6gpvh")))

(define-public crate-yaga-derive-0.1 (crate (name "yaga-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "derive" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0pizdw1ngcp7vnf2swg1klf6jli5bbg39084wbh7q4gvyz83qy4f")))

