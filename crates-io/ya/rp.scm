(define-module (crates-io ya rp) #:use-module (crates-io))

(define-public crate-yarp-0.1 (crate (name "yarp") (vers "0.1.0") (hash "01875wvj0rrrkwxjh2awh666660ka1s4z4x0dp8pv980xgjca3fs") (yanked #t)))

(define-public crate-yarp-0.1 (crate (name "yarp") (vers "0.1.1") (hash "0jgpjr3hhjm2gvhkm3v8v6dzbnjsyi674p5ajmb94bs0hg015h7h") (yanked #t)))

(define-public crate-yarpl-0.0.1 (crate (name "yarpl") (vers "0.0.1") (hash "15vw77z4s4d24fx4axyvlggaxw3429k6br66s38j0nbg96h1nq6q") (yanked #t)))

(define-public crate-yarpl-0.0.2 (crate (name "yarpl") (vers "0.0.2") (hash "19imm5w51p6qw6aiglss49hpmfb07whms0pihawz5ilp3fq86a3m") (yanked #t)))

(define-public crate-yarpl-0.0.3 (crate (name "yarpl") (vers "0.0.3") (hash "0k042m3z2wlvpmhq8vq6iw2mpgwxkp9qyzk3sxg9dzw4ridn6rwr") (yanked #t)))

(define-public crate-yarpl-0.0.4 (crate (name "yarpl") (vers "0.0.4") (hash "0arybb564m4spcy4i0s7zijdm0gvx6wclai9cycaa05lfnkapbwa") (yanked #t)))

(define-public crate-yarpl-0.0.6 (crate (name "yarpl") (vers "0.0.6-pre-release") (hash "0rw5i2qhprhn0w060y69xz0vnqkywh1qk1vgprqivm49v1m3pwm8") (yanked #t)))

(define-public crate-yarpl-0.0.7 (crate (name "yarpl") (vers "0.0.7") (hash "1bgn6rvfa5c1y2z18jrvjwm3a34l2x442fn89lishmdx6v4qb4cy") (yanked #t)))

(define-public crate-yarpl-0.0.9 (crate (name "yarpl") (vers "0.0.9") (hash "0b37dq327p7y88bz8lq1i01cvjbsn51grmifr7vmh48v617jgbiv") (yanked #t)))

(define-public crate-yarpl-0.0.10 (crate (name "yarpl") (vers "0.0.10") (hash "141p651pfgvdb54hi52g7xicaswhr245wjvmlz7xwz5hh2z5wkh0") (yanked #t)))

(define-public crate-yarpl-0.0.11 (crate (name "yarpl") (vers "0.0.11") (hash "14nc287npx9f92l2qv58l4xqqm8iy76m31x32gc4wxc59d87ymnf") (yanked #t)))

(define-public crate-yarpl-0.0.12 (crate (name "yarpl") (vers "0.0.12") (hash "173s3pnqqwj95127k2vf8dl7fri18c710ywbx823gqwqy5hrhnp1") (yanked #t)))

(define-public crate-yarpl-0.0.13 (crate (name "yarpl") (vers "0.0.13") (hash "0qkcndhdfl9aqm4kcsv61p7ris98pynhp4djs58r5dmvn0gkb757") (yanked #t)))

(define-public crate-yarpl-0.0.14 (crate (name "yarpl") (vers "0.0.14") (hash "098i5nkh0a5kgxji7axrc0ig9yw2cn86aa5grank8bbqkmb3g2gj") (yanked #t)))

(define-public crate-yarpl-0.0.15 (crate (name "yarpl") (vers "0.0.15") (hash "0ngnjjhli50kdx92g2pihgr4f4q78wqh38s0346xrh57s9c93yvk") (yanked #t)))

(define-public crate-yarpl-0.0.16 (crate (name "yarpl") (vers "0.0.16") (hash "0a5bgp1i7jsrmhzwa0f0wcxcp7ivfilcl2pfq65j759ly58x5369") (yanked #t)))

(define-public crate-yarpl-0.0.17 (crate (name "yarpl") (vers "0.0.17") (hash "0knqi0liwn04ymjj4kcm74i3v6qpxqzw1q2ly9gn5vfg79lcrbza") (yanked #t)))

(define-public crate-yarpl-0.0.18 (crate (name "yarpl") (vers "0.0.18") (hash "1nz18z2k16lwn5fjlqz3kmgskhjh5pmhmnxib3f40x0v2dnqlhbb")))

(define-public crate-yarpl-0.0.19 (crate (name "yarpl") (vers "0.0.19") (hash "1sdavq2x2747zviq34ls27z0pccg4y6m1icr8cfi0zqq4v1c88jr")))

(define-public crate-yarpl-0.0.20 (crate (name "yarpl") (vers "0.0.20") (hash "12p7xj5fd66qsz1blp1jgjx59gcq7w3kinkbpq3gvbnhd7m49zlx")))

(define-public crate-yarpl-0.0.21 (crate (name "yarpl") (vers "0.0.21") (hash "1syk5is454xazrac4f6lkk5lm4vajp7nid3p4iinlfyfwmnnd2cr")))

(define-public crate-yarpl-0.0.23 (crate (name "yarpl") (vers "0.0.23") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0dlm7ir5a9m9i3a19fli2349n41kfxn0zbbhwfj9xyqchp3vvs81")))

(define-public crate-yarpm-0.1 (crate (name "yarpm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.3.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ng27g073n353vv71m18vh1n3743688fk5khbg9bfczap486avhc")))

(define-public crate-yarpm-0.1 (crate (name "yarpm") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.3.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10xdrjncn60wmr63p30vmnwd3x4z6vs9lsa8cvnqlic6ragxrn8c")))

(define-public crate-yarpm-0.1 (crate (name "yarpm") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.3.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ic31cbwkjlkwisdx4kfjbqy87gm9kdlkfbc4dajbplayqcvfxp1")))

(define-public crate-yarpm-0.1 (crate (name "yarpm") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.3.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l8yhx4z0gwzlwa1anxsiicbw2qzqk6sxxaqcvcybh3knj2mfgg6")))

