(define-module (crates-io ya rg) #:use-module (crates-io))

(define-public crate-yargg-0.1 (crate (name "yargg") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0cwrn5sf23f6r9bmzkr70w8r56imxclwmxhmnyg6wi8vhxblkcvv") (yanked #t)))

(define-public crate-yargg-0.1 (crate (name "yargg") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "017ks56asfmvpxln49q2r4bb4k804va4dw12ypi1r4gwm52pc3ya")))

(define-public crate-yargg-0.1 (crate (name "yargg") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19y037p59rrmj1dr7g0hvxacylj68hj2iqfwsmzj8r5w012v4f77")))

(define-public crate-yargs-0.1 (crate (name "yargs") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1r4d9v39yqigjxhxjy2qli57agd76vrpjk6xwzsv32lc1lw5lbli")))

(define-public crate-yargs-0.1 (crate (name "yargs") (vers "0.1.1") (deps (list (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "067jylivji7amki5gj5nchzmp3d5f4y07cj7crw4q7yb17qxcvkm")))

