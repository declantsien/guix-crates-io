(define-module (crates-io ya cl) #:use-module (crates-io))

(define-public crate-yacl-0.1 (crate (name "yacl") (vers "0.1.0") (hash "0z3h9vsyz5l7rgqdld2vlflfnpn6mwg75158ilvxsrbah6ih5jcb")))

(define-public crate-yacli-0.1 (crate (name "yacli") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6.78") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1zz52mx38bm5pp1x1nz00rrl0y5v8zis7yks02gi84afk8l76gly")))

(define-public crate-yacli-0.2 (crate (name "yacli") (vers "0.2.0") (deps (list (crate-dep (name "docopt") (req "^0.6.78") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0yn39fnl215260g43m2cmd67virqmc2fa875izkf6lxk2byfbpr6")))

(define-public crate-yacli-0.3 (crate (name "yacli") (vers "0.3.0") (deps (list (crate-dep (name "docopt") (req "^0.6.78") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "12p0i75f515agrkvgqyg36b46wms14dcjy8klyy6qdybf1digh5l") (yanked #t)))

(define-public crate-yacli-0.2 (crate (name "yacli") (vers "0.2.1") (deps (list (crate-dep (name "docopt") (req "^0.6.78") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19i8y939y25mzn4q0ki17x1xxdq1qgs723yrmccjkdb0yhcrl500")))

(define-public crate-yacli-0.4 (crate (name "yacli") (vers "0.4.0") (deps (list (crate-dep (name "docopt") (req "^0.6.78") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0gabw74azj1lvf0iahxyjccarq7fdhqd7wfg692ivfd39j7xr63x")))

(define-public crate-yacli-0.4 (crate (name "yacli") (vers "0.4.1") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vc6gxbgfn6ncfiyjsh4f3qv1a0cmbw240hndamqwb8j1r1xdxjz")))

(define-public crate-yacli-0.5 (crate (name "yacli") (vers "0.5.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3") (default-features #t) (kind 0)))) (hash "097vzh4dhcfpjy3i6ngdhyr1rc5676glag3iv5b37rxccylhpijr")))

(define-public crate-yacli-0.5 (crate (name "yacli") (vers "0.5.1") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dylzkvq27gizjmw872l9g7axddnlyf7yinhbrmwn9yyf83vqf8y")))

(define-public crate-yacli-0.6 (crate (name "yacli") (vers "0.6.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3") (default-features #t) (kind 0)))) (hash "0a7x6ychg4fq9279cl5j03m4pfg9s6zwr25cpb7g4jcpcqh4zv8p")))

(define-public crate-yacli-0.6 (crate (name "yacli") (vers "0.6.1") (deps (list (crate-dep (name "docopt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "shell") (req "^0.3") (default-features #t) (kind 0)))) (hash "08vpf430ij0mbsj837inimd47zp6ds7avmfwd610dcx85ixyfkkz")))

(define-public crate-yacll-0.0.0 (crate (name "yacll") (vers "0.0.0") (hash "0qplbm0wckiqlmdbvawd88sfirzsnpqsmky9sj46pwhbsbq72k4f")))

(define-public crate-yacll-0.0.1 (crate (name "yacll") (vers "0.0.1") (hash "0jdl92b7invxr71cva0pwwvx2wq5yla9b0yj5gbrrcvhdhc1jgb9")))

(define-public crate-yacll-0.1 (crate (name "yacll") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "005y1s34q9bv3l75nwhp1z5v6wimsc6qicb6kga4mccw2hpzg74i")))

(define-public crate-yacll-0.2 (crate (name "yacll") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1c1krkxah2zsjmmw5fc14mx69jxapbank6qij7qjwdjh6p2r7mf1")))

(define-public crate-yacll-0.3 (crate (name "yacll") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "01rzqs6irbyi972p6n6s05q4iraqdks0mmnynhzfwwd0ymjr3yfx")))

(define-public crate-yacll-0.4 (crate (name "yacll") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1ax40yqqmnv8mlzjld4k8xd0vb11pqsrc7i8zqyg3frj5nwrdsr8")))

(define-public crate-yacll-0.5 (crate (name "yacll") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1i0jpdianwzr7jm3cbmyaamfn19vam2gyhmwg6b9dhpwpc1fl67s")))

(define-public crate-yacll-0.6 (crate (name "yacll") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0fvnqclg2a2gqv69xxppyqxcdp5qfyx2xv1r8nhhsx64ppwyv2g5") (v 2) (features2 (quote (("serde" "dep:serde" "crossterm/serde"))))))

(define-public crate-yacll-0.6 (crate (name "yacll") (vers "0.6.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1zygih8d0wcifxk00lzyb0knpig849zr3xmga3zkb9sqbrm56pgk") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-yacll-0.7 (crate (name "yacll") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1csbdlfsmv2kg0yyhigf8bffcbdinhy3c0h1sy3zik97zba9hqky") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-yacll-0.7 (crate (name "yacll") (vers "0.7.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1dhhvjy74j805jqndr3584z2ikrrkisxlrbkh523rsqas160wcdd") (v 2) (features2 (quote (("serde" "dep:serde"))))))

