(define-module (crates-io ya gg) #:use-module (crates-io))

(define-public crate-yagg-0.1 (crate (name "yagg") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "147qk7dj429na93ybwvjlzv5m4mg31xr4p87k5fzl5hhq6k3v173") (yanked #t)))

(define-public crate-yagg-0.1 (crate (name "yagg") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "02nxfm4hnbh9zzj7d0wiab0v544f94vkxrxqnfc5iy4b3s09jy46")))

(define-public crate-yagg-0.1 (crate (name "yagg") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1z3iaam5v2wpxlx1a8krkk9sgqjhda97z9yg2hk9sbglxi8xxf2x")))

