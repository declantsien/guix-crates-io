(define-module (crates-io ya md) #:use-module (crates-io))

(define-public crate-yamd-0.1 (crate (name "yamd") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0p06sakbvgqshq1msf9q0miajdbz66bvrgv5yk8jlkbk65gapfkq")))

(define-public crate-yamd-0.2 (crate (name "yamd") (vers "0.2.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0h0rk0qb4s2lfaa4bvxf6czvxd1ybzwn421q2jfvnbxchnkd3fvg")))

(define-public crate-yamd-0.3 (crate (name "yamd") (vers "0.3.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0ri9wihkx0vprrd2lbqvdrcryfzf79ghk8an6cpkh4zvdypn8z2n")))

(define-public crate-yamd-0.4 (crate (name "yamd") (vers "0.4.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "14siv9p1kzpx1jwi9s6kq5jz7hkip04ckqnixmjbngzzgkwfkqq7")))

(define-public crate-yamd-0.5 (crate (name "yamd") (vers "0.5.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "13izsffqm8jaj9nx1v5pvd72sygqzxbbr36yc9z3fs85szfd87l6")))

(define-public crate-yamd-0.5 (crate (name "yamd") (vers "0.5.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1a3l9dmai9ihc250vh4h3c6vkzba5ffpvyii6la4i6jsg99yxbqk")))

(define-public crate-yamd-0.6 (crate (name "yamd") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "17yyihid1qadp2l79l8qbi96qajs4mphxk58zff3f59wajyc9sw1")))

(define-public crate-yamd-0.6 (crate (name "yamd") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "117agcdclm5gc4b3hqw6j3d4axwj2sr96g9kw6hzlbp9wgsp52dn")))

(define-public crate-yamd-0.7 (crate (name "yamd") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0z3hy5pl5qmcrykgpjl17dc1ddj455a61ycbyv526ynlq9pvl76w")))

(define-public crate-yamd-0.8 (crate (name "yamd") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1gqhhb1siw9n8fk9g54aggacp054yb5x9va7y564jbx20gnhh65f")))

(define-public crate-yamd-0.9 (crate (name "yamd") (vers "0.9.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1n4shqmgj9k6yfrzr2s530vl0lml9nvkvgm6p6in6ac9k4vbzwc6")))

(define-public crate-yamd-0.10 (crate (name "yamd") (vers "0.10.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.27") (default-features #t) (kind 0)))) (hash "1axm2x6bclj4aa6ic6frzl27pkgzz4g0ayxn3h1vwfzr9hg65v21")))

(define-public crate-yamd-0.11 (crate (name "yamd") (vers "0.11.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.27") (default-features #t) (kind 0)))) (hash "0y3khlqbfwmfa4zkj3bpqbmj0c43kl89maa3v09534r8vsc43yzq")))

(define-public crate-yamd-0.12 (crate (name "yamd") (vers "0.12.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.27") (default-features #t) (kind 0)))) (hash "00ajsncxq1awq3k3sv9qbici6klyp5dawl1d2di608cjal71scmp")))

(define-public crate-yamd-0.12 (crate (name "yamd") (vers "0.12.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.27") (default-features #t) (kind 0)))) (hash "0hjfy2l1kqnb3gh5lixbhsdl3hlszrh95bprgnha0ipm77hllylw")))

(define-public crate-yamd-0.13 (crate (name "yamd") (vers "0.13.1") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.34") (default-features #t) (kind 0)))) (hash "1rgafqrbgbrzwda6zh4ahx489kgbl7s7lz5mm1b9gz574kh62l48")))

(define-public crate-yamd-0.13 (crate (name "yamd") (vers "0.13.2") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.34") (default-features #t) (kind 0)))) (hash "1xz9qvda74x34nw9yjnfz48q5nv6linsr5ckmg2nwjs290pxfsrr")))

(define-public crate-yamd-0.13 (crate (name "yamd") (vers "0.13.3") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.34") (default-features #t) (kind 0)))) (hash "19n6p6ms8bzpqpx9mzkdlhhyb39jjn66d07fa8dwa7yn6q71ijns")))

