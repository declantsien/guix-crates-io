(define-module (crates-io ya so) #:use-module (crates-io))

(define-public crate-yason-0.0.0 (crate (name "yason") (vers "0.0.0") (deps (list (crate-dep (name "decimal-rs") (req "^0.1.30") (default-features #t) (kind 0)) (crate-dep (name "sqldatetime") (req "^0.1.23") (features (quote ("oracle"))) (default-features #t) (kind 0)))) (hash "0c3qv9wn4bkjdbvp5ga02fzf83n4wp96d8nvswjh79bm23919b6k")))

(define-public crate-yason-0.0.1 (crate (name "yason") (vers "0.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 2)) (crate-dep (name "decimal-rs") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (features (quote ("arbitrary_precision"))) (default-features #t) (kind 0)))) (hash "0pi248ych11wsajz60x7ll6d7gmam1lv7i7vliapi1gjzd18vih5")))

(define-public crate-yason-0.0.2 (crate (name "yason") (vers "0.0.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 2)) (crate-dep (name "decimal-rs") (req "^0.1.39") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.141") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (features (quote ("arbitrary_precision"))) (default-features #t) (kind 0)))) (hash "0zm5c8nkq5k9sqa5j4af49ikd89yi61n5z74x2mj27y72wbmh727") (rust-version "1.57")))

