(define-module (crates-io ya pb) #:use-module (crates-io))

(define-public crate-yapb-0.1 (crate (name "yapb") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 2)))) (hash "11vx8ax8qqg81nc50xv0rl35c0ygr2j1x3nsafal7yka8f2kp4lm")))

(define-public crate-yapb-0.1 (crate (name "yapb") (vers "0.1.1") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 2)))) (hash "0k5hgi5b4gj3i3q6332gyhhr3g96ivn9qp3c3ndy39r8jlgf8qdz")))

(define-public crate-yapb-0.1 (crate (name "yapb") (vers "0.1.2") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 2)))) (hash "1sis01cm252xbb459dvb126441k22gn1ffqmn202hmc3sgfvcqsf")))

(define-public crate-yapb-0.2 (crate (name "yapb") (vers "0.2.0") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 2)))) (hash "0ly4iq37gdbv92if39qjv2nfgq75w19fsa23wbd7w8q5qac3j7vc")))

(define-public crate-yapb-0.3 (crate (name "yapb") (vers "0.3.0") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 2)))) (hash "147ywin94kxnd9665nyj7jj5isad2ck7mmsf973wgnkz4zd1r9n5")))

(define-public crate-yapb-0.3 (crate (name "yapb") (vers "0.3.1") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 2)))) (hash "1n477agmd2ki0y19hcz11mjcnmhp7yvcjyn95lpyw74s21vgx30f")))

(define-public crate-yapb-0.3 (crate (name "yapb") (vers "0.3.2") (deps (list (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 2)))) (hash "1wahjacg4xma2ksbs6ph2vlba1562d9xbvnfh61d6hmjbqvj9idr")))

