(define-module (crates-io ya pp) #:use-module (crates-io))

(define-public crate-yapp-0.1 (crate (name "yapp") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "0.12.*") (default-features #t) (kind 2)))) (hash "1wki0w2sjvb30dl466z3rpjkwg6xy94cyxaxq8pxfnm5zz0wa57i")))

(define-public crate-yapp-0.1 (crate (name "yapp") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "0.12.*") (default-features #t) (kind 2)))) (hash "05yyg6niv2zfa6vy9kyfan4gvqw0b3xikvc7hja14j8ppqyp8wp7")))

(define-public crate-yapp-0.1 (crate (name "yapp") (vers "0.1.2") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "0.12.*") (default-features #t) (kind 2)))) (hash "0qmrkzfw7iyxga84afx5a89flrrdd5l9aj1sf9cbx1clahsq6smd") (rust-version "1.70.0")))

(define-public crate-yapp-0.2 (crate (name "yapp") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "0.12.*") (default-features #t) (kind 2)))) (hash "0b065va5bgava2phalavr1vqcb579j5n8fan3pxg16srnwnga4bc") (rust-version "1.70.0")))

(define-public crate-yapp-0.3 (crate (name "yapp") (vers "0.3.0") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "0.12.*") (default-features #t) (kind 2)))) (hash "1zg7xf5g76njzvfpiffbyn4sv4wz8mxbda9629p7rggi39jga02x") (rust-version "1.70.0")))

(define-public crate-yapp-0.4 (crate (name "yapp") (vers "0.4.0") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "0.12.*") (default-features #t) (kind 2)))) (hash "09kcbfkg0v4lgpvs2yqbf4ygfv4cx50444chi5157i06c0n7x8lq") (rust-version "1.70.0")))

(define-public crate-yapp-0.4 (crate (name "yapp") (vers "0.4.1") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "0.12.*") (default-features #t) (kind 2)))) (hash "0k9993ibaiwv8lmv7nfzksl1h199xqm0ldjcm4isspkzy35b2q37") (rust-version "1.70.0")))

