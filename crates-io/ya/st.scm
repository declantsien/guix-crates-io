(define-module (crates-io ya st) #:use-module (crates-io))

(define-public crate-yastl-0.1 (crate (name "yastl") (vers "0.1.0") (deps (list (crate-dep (name "flume") (req "^0.10") (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)))) (hash "0k4qxppg52s57qj2h8461164n1ch22cnff8ab54z5pg9ry4h7swq")))

(define-public crate-yastl-0.1 (crate (name "yastl") (vers "0.1.1") (deps (list (crate-dep (name "flume") (req "^0.10") (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)))) (hash "12h7q28qvw7kgcvbymvcvf6c23wdzs0121w47gyz6kgw63wh3rd2")))

(define-public crate-yastl-0.1 (crate (name "yastl") (vers "0.1.2") (deps (list (crate-dep (name "coz") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)))) (hash "06iwvg3psa9zx9singh3wpkksd177ivz8741c6i9w6kcssjcb9lc")))

