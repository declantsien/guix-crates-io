(define-module (crates-io ya do) #:use-module (crates-io))

(define-public crate-yadon-0.1 (crate (name "yadon") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1wkjsz529nkcyypk2kdn4sfn5xcra266mdvh6rjf4zha0d7mvpr7") (yanked #t)))

(define-public crate-yadon-0.1 (crate (name "yadon") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0hskqz2kxzchihccciqjj17lrwfjy45126dlhdyn1ywfadilfxmc")))

