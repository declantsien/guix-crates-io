(define-module (crates-io ya up) #:use-module (crates-io))

(define-public crate-yaup-0.1 (crate (name "yaup") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1ikkq6ikj39mz341fdlkp5kslqsvpz2z7xfjka5jqx6v30yiv3kz")))

(define-public crate-yaup-0.2 (crate (name "yaup") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0i9b2mmr26icamqkck7cry5jfkqq8jy1xwxb182mgs7pcdlyzj9v")))

(define-public crate-yaup-0.2 (crate (name "yaup") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1n99wkirrb2r6ch04blh584shdflm4p1kxaxq8vpqgylpqkpv7m5")))

