(define-module (crates-io ya io) #:use-module (crates-io))

(define-public crate-yaiouom-0.1 (crate (name "yaiouom") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)))) (hash "00rjy8w82cy7s48vmci972b7srnl9mvfj8djn8k49b3qapb8qqnh")))

(define-public crate-yaiouom-0.1 (crate (name "yaiouom") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)))) (hash "1h2wgz6y59p3a2jq60jvj87dc3jzv4qz7a4a6k05kwl1pmjx28g4")))

(define-public crate-yaiouom-0.1 (crate (name "yaiouom") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)))) (hash "0gxjk3b44i50b2dys3j7mmrx1ads7cjhm7qylzpya9rc8zqk85c0")))

(define-public crate-yaiouom-0.1 (crate (name "yaiouom") (vers "0.1.3") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h3ilaqvsgmssp94rh10azq777vqd9xpa78l3ja9z9ds9zq9ih2l")))

(define-public crate-yaiouom-check-0.1 (crate (name "yaiouom-check") (vers "0.1.0") (hash "18nrkcarqpj8rfn7mx3pq944x83llv8xbfg80nib9ivm1b77lb8v")))

