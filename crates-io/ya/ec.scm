(define-module (crates-io ya ec) #:use-module (crates-io))

(define-public crate-yaecs-0.1 (crate (name "yaecs") (vers "0.1.0") (deps (list (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0is78c3fvh9h5f1v9949znrnw2idsni5npdzphg6dsw14c1hmxzq")))

(define-public crate-yaecs-0.3 (crate (name "yaecs") (vers "0.3.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0zi34sh8nxgxpyv19y39cdnzq1qwwhmdij26xlfny2p9px7zr225")))

(define-public crate-yaecs-0.4 (crate (name "yaecs") (vers "0.4.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1g33d2yrbl2c5a9cgjnnx01cvda6p44s5lr6hxgf1g4fslw8qz62")))

(define-public crate-yaecs-0.5 (crate (name "yaecs") (vers "0.5.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1146gljwms7bn33z01m5yy2hh470p28i9ahwvxcmd9mcy85nl8n8")))

(define-public crate-yaecs-0.5 (crate (name "yaecs") (vers "0.5.1") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1a9cvml3mdrj8jb2sjiy8jp5gzwclhffnz409wv283w10jarwliy")))

(define-public crate-yaecs-0.5 (crate (name "yaecs") (vers "0.5.2") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1wpmiaxdnr9qlfccclka46s4bn0wrhqn4cwxvwn2p3lpk59xnj6m")))

(define-public crate-yaecs-0.5 (crate (name "yaecs") (vers "0.5.3") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1djsy34hplzmapc5np9qc9w3a0n6bqckr288np8akxq81cdmal2h")))

(define-public crate-yaecs-0.6 (crate (name "yaecs") (vers "0.6.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1dy4wiib8lmzpry58h7s0aiax04pnjscraibaazzy2x9r3axrmfj")))

(define-public crate-yaecs-0.7 (crate (name "yaecs") (vers "0.7.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0prmwnjw5n5gbgrwvdc6l3fm2gsis852ic0lvqh23xcqwf8pzhbv")))

(define-public crate-yaecs-0.7 (crate (name "yaecs") (vers "0.7.1") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0mbw7hjpjgj39xsyr71542kal7ypbdrmajw9sxry0zvs5r1309m0")))

(define-public crate-yaecs-0.7 (crate (name "yaecs") (vers "0.7.2") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1a4fkm2cz9hsa0zfjk6fh47rjcgnhwxjswki9wz6n0db4mpm9zqa")))

(define-public crate-yaecs-0.8 (crate (name "yaecs") (vers "0.8.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0vcffgb747wyi8hyhlfvqaiqi13zdxwlv3syn2hb7cz1jdzw1r0c")))

(define-public crate-yaecs-0.8 (crate (name "yaecs") (vers "0.8.1") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0im2j7nrb39nkl3972mj9wfsfhsarlgryzsbmrsbfqaiivrlik2g")))

(define-public crate-yaecs-0.9 (crate (name "yaecs") (vers "0.9.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0kcf99alpa6d7902yn3x7yx239waywhx9hsk4hnxwlapgxip57d6")))

(define-public crate-yaecs-0.9 (crate (name "yaecs") (vers "0.9.1") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1137ikqiqnf29d8y7gx5irk8rpxwz5ff1lgh1xcr9kp0sz704rq6")))

(define-public crate-yaecs-0.9 (crate (name "yaecs") (vers "0.9.2") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1k4yrs8vg01b8ms6ryr6m0rs1dqwk23v29i0fxmmzablgn3a27vb")))

(define-public crate-yaecs-0.9 (crate (name "yaecs") (vers "0.9.3") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1fmsw3p11m0nx6alxnnlkil61k621ggsjhg6ylsz7kfkx6fk7643")))

(define-public crate-yaecs-0.10 (crate (name "yaecs") (vers "0.10.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "173bd5kapq9zrw47lc7ic15hrz318ipxvpn3mdnlpa019kk8qaqn")))

(define-public crate-yaecs-0.11 (crate (name "yaecs") (vers "0.11.0") (deps (list (crate-dep (name "anymap") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "avow") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "03zph8zhnrvci314q0x0cmarzfikqy3qpg3wgjx5fkkq24446fzl")))

