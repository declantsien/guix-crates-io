(define-module (crates-io ya ht) #:use-module (crates-io))

(define-public crate-yahtzee-0.1 (crate (name "yahtzee") (vers "0.1.0") (deps (list (crate-dep (name "ncurses") (req "^5.101.0") (features (quote ("wide"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1fgahqcycv4qmjxvjnln0n2jmlr9r7by9ywdzwzijs982pg553c9")))

(define-public crate-yahtzee-0.2 (crate (name "yahtzee") (vers "0.2.0") (deps (list (crate-dep (name "ncurses") (req "^5.101.0") (features (quote ("wide"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0mzzkqadkyl7bqck784csn6j19fwcrjrqip1b3r3wvpfwjwdmh74")))

(define-public crate-yahtzee-0.3 (crate (name "yahtzee") (vers "0.3.0") (deps (list (crate-dep (name "ncurses") (req "^5.101.0") (features (quote ("wide"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0q3k1yrrlv14r2fgdaaw5v64fnl3h3b5mg8c825zkws3ldwicfka")))

