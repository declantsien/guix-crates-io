(define-module (crates-io ya pv) #:use-module (crates-io))

(define-public crate-yapv-0.1 (crate (name "yapv") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.17.6") (default-features #t) (kind 0)))) (hash "01abw8lgmf4bvzhl9wvwwjx1zmr6531d8yjm2ff0s5kmcbmqacfd")))

(define-public crate-yapv-0.2 (crate (name "yapv") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "05m81c1pzf8j9rjmg4aia9xvg1jmw15m394f9qzmp01kshy2hm89")))

