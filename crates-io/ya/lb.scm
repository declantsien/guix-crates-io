(define-module (crates-io ya lb) #:use-module (crates-io))

(define-public crate-yalb-0.0.1 (crate (name "yalb") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "0.*.*") (default-features #t) (kind 0)))) (hash "0x5bp6m8lqr4hpv50d6rklrqd2y8nij5msvndx33az088ijlbfnh")))

