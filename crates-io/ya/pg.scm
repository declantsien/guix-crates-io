(define-module (crates-io ya pg) #:use-module (crates-io))

(define-public crate-yapg-0.1 (crate (name "yapg") (vers "0.1.0") (deps (list (crate-dep (name "cargo-make") (req "^0.32.14") (default-features #t) (kind 2)) (crate-dep (name "cargo-semver") (req "^1.0.0-alpha.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1r6v1pqfsxhwgz86c7w4d3hm9g6yxamxbib2ghqyhdj5zv8ghmrv")))

