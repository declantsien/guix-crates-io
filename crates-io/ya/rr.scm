(define-module (crates-io ya rr) #:use-module (crates-io))

(define-public crate-yarr-0.0.0 (crate (name "yarr") (vers "0.0.0") (deps (list (crate-dep (name "riscv") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1sj84rprgvs36glk2147bavgn563j87dv2wqr5xlql12a9hwkzb5")))

(define-public crate-yarr-0.0.1 (crate (name "yarr") (vers "0.0.1") (deps (list (crate-dep (name "riscv") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "riscv-rt") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "yarr-common") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "yarr-riscv") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0phwh65i5g2k9mizfly7gx9pl8xy0kb4km2smp6r7nafjdhjx2xc")))

(define-public crate-yarr-common-0.0.0 (crate (name "yarr-common") (vers "0.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (kind 0)))) (hash "1f8m6bamacz8kwb7pilb3f1lab96a5wrq8z7063914410ks8z37r")))

(define-public crate-yarr-riscv-0.0.0 (crate (name "yarr-riscv") (vers "0.0.0") (deps (list (crate-dep (name "riscv") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "yarr-common") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "14vwkchrph2k85nd9rywjw0z630gp6aiv34hcr2jybafjfpswr7w")))

(define-public crate-yarrow-rs-0.1 (crate (name "yarrow-rs") (vers "0.1.0") (deps (list (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.30") (default-features #t) (kind 0)))) (hash "1rbxlw3ibs5qmc0vxhijakxbq0lhi1nzpwbibbzy41xqymf0mc1g")))

(define-public crate-yarrr-0.0.0 (crate (name "yarrr") (vers "0.0.0") (hash "1dp67vdq20jdgljjmy3xr2l05rn2g6rvm37x68rgf44w6ash6m0g")))

