(define-module (crates-io ya ce) #:use-module (crates-io))

(define-public crate-yacexits-0.1 (crate (name "yacexits") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.14") (kind 0)))) (hash "07887y5fyp6aw03r9zd9qdb9f48304ylwn8k4hla9xrjm89hdf00") (yanked #t)))

(define-public crate-yacexits-0.1 (crate (name "yacexits") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.14") (kind 0)))) (hash "1glrs039ifjbh49n1p5hna4plicirqnpwp55nfagx5gz1lkvxbxk")))

(define-public crate-yacexits-0.1 (crate (name "yacexits") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.14") (kind 0)))) (hash "0vqlqryr73xv5mq878k08vj6a0alf4xp1vgv08iib5zxg31frbp6")))

(define-public crate-yacexits-0.1 (crate (name "yacexits") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.14") (kind 0)))) (hash "066w1mrz1drmmsffk2jiw2ah38xyq7k45s1ijj8vq6sws06p9zjk")))

(define-public crate-yacexits-0.1 (crate (name "yacexits") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.14") (kind 0)))) (hash "0nyykxs7iqav7q7lm5xsbb47jnrnaqri81gkidkf48vhki38a3gq")))

