(define-module (crates-io ya um) #:use-module (crates-io))

(define-public crate-yaum-0.1 (crate (name "yaum") (vers "0.1.0") (hash "18qd492zpw8c834nh68fkfpv62sxjymkvrz0ckbgrqzphr27h4kq") (features (quote (("double_precision"))))))

(define-public crate-yaum-0.1 (crate (name "yaum") (vers "0.1.1") (hash "0cgwd6w2znx43bj4kvc627z0dxfm9yx0civxvqlcz1ivd9zmzdjs") (features (quote (("double_precision"))))))

(define-public crate-yaum-0.1 (crate (name "yaum") (vers "0.1.2") (hash "05llx6bg56j8dr4lxcvnqhdb2872yakyrmgc61cjmmlxyz65ajzj") (features (quote (("double_precision"))))))

