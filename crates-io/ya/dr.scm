(define-module (crates-io ya dr) #:use-module (crates-io))

(define-public crate-yadro-0.0.1 (crate (name "yadro") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "matchit") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.31.0") (features (quote ("rt" "rt-multi-thread" "net" "io-util" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "yadro-codegen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0xmxjjq2jvgna5aj690ib1l624j3dhqp1cq1filrdkx4qh3a1kxr")))

(define-public crate-yadro-codegen-0.0.1 (crate (name "yadro-codegen") (vers "0.0.1") (hash "1pv75r9hggabjszbccsa5aadsirv3yyyjk5b6hd75n8544n80rv3")))

