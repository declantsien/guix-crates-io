(define-module (crates-io ya np) #:use-module (crates-io))

(define-public crate-yanp-0.1 (crate (name "yanp") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.0.0") (kind 0)))) (hash "1pqn44253l4j3vycxxd8c1iz04zkanhpkc2rq4jmm0fab32z5rd5") (features (quote (("default") ("alloc")))) (yanked #t)))

(define-public crate-yanp-0.1 (crate (name "yanp") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5.0.0") (kind 0)))) (hash "0hxsxppr2zvvlgmmm91j0flpqvx6ax9abc9sh1fwkbs13j4xz7jj") (features (quote (("default") ("alloc"))))))

