(define-module (crates-io ya tl) #:use-module (crates-io))

(define-public crate-yatl-0.1 (crate (name "yatl") (vers "0.1.0") (hash "16256gmwwyqg680pbvvfn70hphqig060fzgm6c33n4924ajf89va")))

(define-public crate-yatlv-1 (crate (name "yatlv") (vers "1.0.0") (hash "14spy2gj4nnj9kbyb8clxmg4rfzwi948vibjw6fb6f46q3h48wsr")))

(define-public crate-yatlv-1 (crate (name "yatlv") (vers "1.1.0") (hash "1pi8f9g57y8g5awc561p8ww2q9wnp2wmz97fmfdr7gzbmkn2r7jn")))

(define-public crate-yatlv-1 (crate (name "yatlv") (vers "1.2.0") (deps (list (crate-dep (name "uuid") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "00wvzsr5w0plbdyq1rdqbl79sw4dysddbxb4l6kpq7qrrgz8cm6m") (features (quote (("default" "uuid"))))))

(define-public crate-yatlv-1 (crate (name "yatlv") (vers "1.3.0") (deps (list (crate-dep (name "uuid") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "0ny4z9k1b8pzd8k4p0q2pygb8k418s0zrih37q1kqs26vs8xjm7k") (features (quote (("default" "uuid"))))))

