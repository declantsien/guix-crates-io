(define-module (crates-io ya de) #:use-module (crates-io))

(define-public crate-yade-0.1 (crate (name "yade") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "01qsb8gkddf5s0l29h65bmm08wbc0mmcjqpafpjlrf6hgm8vgz3s")))

(define-public crate-yade-0.1 (crate (name "yade") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "19518r5s0n4cgcnsbzgvm8bmzjs81r25lhpi3qhn82l6yxp9887l")))

(define-public crate-yade-0.1 (crate (name "yade") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "001dv9mnpvj9063nh65fjkbnzzgriawhp7q2f29h2jv60ad3yz7h")))

