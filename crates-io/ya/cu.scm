(define-module (crates-io ya cu) #:use-module (crates-io))

(define-public crate-yacurses-0.0.0 (crate (name "yacurses") (vers "0.0.0") (deps (list (crate-dep (name "chlorine") (req "^1") (default-features #t) (kind 0)))) (hash "0wmzkyarb1ixpbpwxqlgqp4rq841yc1vi5zsc35igvqng0k66hxp") (yanked #t)))

(define-public crate-yacurses-0.0.1 (crate (name "yacurses") (vers "0.0.1") (deps (list (crate-dep (name "chlorine") (req "^1") (default-features #t) (kind 0)))) (hash "11ga7xrpjzwiivsnswqbwvk30n03py5r49hbclcyb08xiy17dfsn")))

(define-public crate-yacurses-0.1 (crate (name "yacurses") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "chlorine") (req "^1") (default-features #t) (kind 0)))) (hash "0fwjyvwsy8xkkn3y1wxz6nri7qd6zpm72c8fkjz227qagsv08yym")))

(define-public crate-yacurses-0.1 (crate (name "yacurses") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "chlorine") (req "^1") (default-features #t) (kind 0)))) (hash "092a4h8pgmax1235a1zbw2yisnn9wn7x5d7y2iqmqanch44n32ks")))

(define-public crate-yacurses-0.2 (crate (name "yacurses") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "chlorine") (req "^1") (default-features #t) (kind 0)))) (hash "0mkcabfvbvgfcwg0i2amfhg48sms5kb205npjhq0vwx1a6jv0zsl")))

(define-public crate-yacurses-0.2 (crate (name "yacurses") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "chlorine") (req "^1") (default-features #t) (kind 0)))) (hash "102dyh91frrya2r7z0zx2h6iql99qldmykh4bk2banrsydqy3m2q")))

(define-public crate-yacurses-0.2 (crate (name "yacurses") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1b1v0mcq4a6nsiwjq0sc47vax0lfs1cvpzlqyimflkjbqiz8q93m")))

(define-public crate-yacurses-0.2 (crate (name "yacurses") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1mr3bjxnhlc36jvl72f9kzkk8z41lcbn4hksaa9hs4zdy3q0a449")))

(define-public crate-yacurses-0.2 (crate (name "yacurses") (vers "0.2.4") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1kw9yxw2f9xc8djn767aqx17fs57jkb596yyl9sfh2zq112jl8hx")))

