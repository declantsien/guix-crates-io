(define-module (crates-io ya fn) #:use-module (crates-io))

(define-public crate-yafnv-0.1 (crate (name "yafnv") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (kind 0)))) (hash "0w1b0mx8940ig1c3k568fzpvchqbv6y0gdiwbd92mc10vx26jkw0") (features (quote (("std"))))))

(define-public crate-yafnv-0.1 (crate (name "yafnv") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (kind 0)))) (hash "0x6f96lw6sdprqcr5jxwzfbk6pnvv5r87gqifwwy9blrg207alkg") (features (quote (("std"))))))

(define-public crate-yafnv-1 (crate (name "yafnv") (vers "1.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (kind 0)))) (hash "06xajg6irx6zqvcpjpdh96yb0hlqnjqa14lnh09cgbj95h1n78a2") (features (quote (("std"))))))

