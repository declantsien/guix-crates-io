(define-module (crates-io ya #{65}#) #:use-module (crates-io))

(define-public crate-ya6502-0.1 (crate (name "ya6502") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rustasm6502") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1k18d4qns1cdyimzzmf58y8n05v10ipaw4qg5dam1bfdcdd1f7kx")))

