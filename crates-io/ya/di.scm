(define-module (crates-io ya di) #:use-module (crates-io))

(define-public crate-yadi-1 (crate (name "yadi") (vers "1.0.0-alpha") (hash "0py51l1vfgm1y4g3h95xv5q8x0h4dvh67f83cs5hpip98apx3vs6")))

(define-public crate-yadi-1 (crate (name "yadi") (vers "1.0.0-alpha.1") (hash "17k9sh21lql4pxzpx2q5ap6mg5bc0zk6m5hpn1yz798p0vblvd1x")))

(define-public crate-yadict-0.1 (crate (name "yadict") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "1jcnfnxxi4dlak6hn7lz6xkqwljynnb6r0m0ah733r653im0m9jl")))

(define-public crate-yadict-0.1 (crate (name "yadict") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "07ldzaqhvji1fnk666b1mspxijn8vfds4anavnkfqnif18qmcnd3")))

(define-public crate-yadio-0.1 (crate (name "yadio") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "ringbuf") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "symphonia") (req "^0.5.3") (features (quote ("aac" "mpa"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "youtube_chat") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "yt_tsu") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "146qs51y98z2qdk9dh4gj19bx2x76b40hsxrhpyw8ya2v6svxbha")))

