(define-module (crates-io ya rs) #:use-module (crates-io))

(define-public crate-yarsi-0.1 (crate (name "yarsi") (vers "0.1.0") (deps (list (crate-dep (name "columns") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nixinfo") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0qmksqhxbz8qka8syb8is4wdzf0w3rqgcgl8qzsg3lph1r5fpbfg")))

