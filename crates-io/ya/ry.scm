(define-module (crates-io ya ry) #:use-module (crates-io))

(define-public crate-yary-0.1 (crate (name "yary") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)))) (hash "0b7zycmvbgsw3csp1gb1iw2bpdz8mbv57xxs3b6p7y3mz9s51786") (features (quote (("test_lazy") ("test_buffer_small" "test_buffer") ("test_buffer_medium" "test_buffer") ("test_buffer_large" "test_buffer") ("test_buffer"))))))

