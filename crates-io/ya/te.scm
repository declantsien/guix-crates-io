(define-module (crates-io ya te) #:use-module (crates-io))

(define-public crate-yate-0.0.1 (crate (name "yate") (vers "0.0.1") (deps (list (crate-dep (name "html-to-string-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1iqp37vchbd1bjb6n034xbj3sr645v8phyff3y6ps3palsbdj91x") (yanked #t)))

(define-public crate-yate-0.1 (crate (name "yate") (vers "0.1.1") (deps (list (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "html-to-string-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "08ci05aky63nifpka27clagfxbnarabh9qm40qw3z7k62n9z2939") (yanked #t)))

(define-public crate-yate-0.1 (crate (name "yate") (vers "0.1.2") (deps (list (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "html-to-string-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "10da8nrawwm99vjl8mp9c2hbgaps9djbqcgrdfg17bjd9iw9567x") (yanked #t)))

(define-public crate-yate-0.1 (crate (name "yate") (vers "0.1.3") (deps (list (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "html-to-string-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hg7mlc9s5pvrk60l564jci75klf4bi1d5i2djihkvi8hq3fgmb6") (yanked #t)))

(define-public crate-yaterbilang-0.1 (crate (name "yaterbilang") (vers "0.1.0") (hash "0zp6yx1ppqsriqza23082yx3nlinnd3ms6x74ayrg35i96h661gl")))

(define-public crate-yaterbilang-0.1 (crate (name "yaterbilang") (vers "0.1.1") (hash "0vvi7cf605hm08sywrlsjljd1x7xaxwdk7082rldgz08xkdidpay")))

(define-public crate-yaterbilang-0.1 (crate (name "yaterbilang") (vers "0.1.2") (hash "0c1sk474zm4k6hj1gg0h74x95ny2vh0icpfy17sbr0zj20gjgqm8")))

(define-public crate-yaterbilang-0.1 (crate (name "yaterbilang") (vers "0.1.3") (hash "14m0ndaql3c9i0dk2mxyxj5d8g778l8k82ciby96wa23ygmb331g")))

(define-public crate-yaterbilang-0.1 (crate (name "yaterbilang") (vers "0.1.4") (hash "0jn6m427pd1bx686cg46f4zdyg47shjgsxc66whdx8vhm9sc3b7p")))

(define-public crate-yaterbilang-0.1 (crate (name "yaterbilang") (vers "0.1.5") (hash "0bcdxc3vkzrsgrlkavyg72xd4zf1zz337g6pgvb96kb22kciamky")))

(define-public crate-yaterbilang-0.1 (crate (name "yaterbilang") (vers "0.1.6") (hash "0kxd020kqzpzxs1a26jjjy7s7kvnxb6rv6q2p69x7d7f1c5qihrs")))

