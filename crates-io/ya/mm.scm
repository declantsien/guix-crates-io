(define-module (crates-io ya mm) #:use-module (crates-io))

(define-public crate-YAMM-0.1 (crate (name "YAMM") (vers "0.1.0") (hash "10k5nbinqhgrsvrwnwl7pxwrqbldh4y3hnbnvyfq6lrpjzys2zas")))

(define-public crate-yamm-sys-0.1 (crate (name "yamm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)))) (hash "11zjdpsy7mlh5ramgcq417iqgkjbw0jyy1msp0jpa6abm90zldfa")))

