(define-module (crates-io ya rf) #:use-module (crates-io))

(define-public crate-yarf-0.0.1 (crate (name "yarf") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)) (crate-dep (name "yarf-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1g5k7b0cqk6n8v6vjxynfcaqyagza4vpi1a5k909vaaaz1s6di38")))

(define-public crate-yarf-0.0.2 (crate (name "yarf") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)) (crate-dep (name "yarf-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1s6pk0jxiv8pf2msffxjp090mi4bazyca84wflrf65pbi35ib41v")))

(define-public crate-yarf-fetch-0.1 (crate (name "yarf-fetch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "185vhfmv5ks9g9szp5gg367asyxa13bjhhigcy4z9crz7m1ig71n")))

(define-public crate-yarf-sys-0.0.1 (crate (name "yarf-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "013nfrrxccx751c4biiy2r56b1v2hc046a9dbm742inp8xkwnhg6")))

(define-public crate-yarf-sys-0.0.2 (crate (name "yarf-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1khndcidsaj4dq5mi0r9fb33r2vfk02rgv9mp9c5wihmni1fgqif")))

