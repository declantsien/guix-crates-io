(define-module (crates-io ya d_) #:use-module (crates-io))

(define-public crate-yad_semver-1 (crate (name "yad_semver") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)))) (hash "1bgf6l7rgwxwpi0jj38nrgdc1bp85sxgfcrw5nvfb717py5yqy0n")))

