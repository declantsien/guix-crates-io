(define-module (crates-io ya gr) #:use-module (crates-io))

(define-public crate-yagraphc-0.1 (crate (name "yagraphc") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "0b0hcm9dm3y1blvsmlz4hklwcjz0fqrwczin1a427s9k327i1zpg")))

(define-public crate-yagraphc-0.1 (crate (name "yagraphc") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "13206mgqmybgkzd7bp718hbkr95z2may95hdjndrqi9xy7m1hcgr")))

(define-public crate-yagraphc-0.1 (crate (name "yagraphc") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "1amjsn6v1qzqdapjvpm9lnalxi62g02r9isg05h1fnps2a3z07x1")))

