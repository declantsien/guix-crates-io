(define-module (crates-io ya rd) #:use-module (crates-io))

(define-public crate-yard-0.1 (crate (name "yard") (vers "0.1.0") (hash "1wraci53139my96b0laz81qi9fxyggv977ncg85ry9w202s3vv7j")))

(define-public crate-yard-0.2 (crate (name "yard") (vers "0.2.0") (hash "1b7g6g7qxqag91kpk4jmxcjlfy2q5anx43gszg3j7n5lc0i2mq6w") (yanked #t)))

(define-public crate-yard-0.1 (crate (name "yard") (vers "0.1.1") (hash "0245zdnc59qi3wk1bap68l2yyfmmzn6g5fns9fcvjm4cjz36vizd")))

(define-public crate-yard-0.1 (crate (name "yard") (vers "0.1.2") (hash "1d55cgalzkrgbqg3m1bpvi1dxplzfqa1m4jq346klnd4ga54qvc1")))

(define-public crate-yard-0.1 (crate (name "yard") (vers "0.1.3") (hash "077q48s5vxnmwsjy2k7r8brcsc3cm3g1pk94gdbhpz6wgjykizcw")))

(define-public crate-yard-0.1 (crate (name "yard") (vers "0.1.4") (hash "0x6c4c6y3fxrxw7zfkqvwnzcfixa979j3cw13l9qvzpa1b07kwn6")))

(define-public crate-yard-0.2 (crate (name "yard") (vers "0.2.1") (hash "0q51h8x7c7jlir8mrrrh1j5ax4szdiw95rmkjfisz2yn899p4pr2")))

(define-public crate-yard-0.2 (crate (name "yard") (vers "0.2.2") (hash "1ifavdpyxxg4vb0qz1ixm9mjdphsivwnq75jsaaix2yqljfxix96")))

(define-public crate-yard-0.2 (crate (name "yard") (vers "0.2.3") (hash "0wx9mqcw0n4vlrn5qzwn32gyb42xzrxkbxisf58297az2bbadbha")))

(define-public crate-yard-0.2 (crate (name "yard") (vers "0.2.4") (hash "0z1dppdnrw318gmh0nqlnw6yl20nnbyw8i3223xzfr9j23w96vm2")))

(define-public crate-yard-0.2 (crate (name "yard") (vers "0.2.5") (hash "0s8l8bih337c75diwxbfhl4jn04x4pbqc4sgrnqzfrv7fbf2ygxq")))

(define-public crate-yard-1 (crate (name "yard") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1nw615y46dpgypvjc1cc9gj8adyp87mwmsnlhv58vx0v9n7zvi0w")))

(define-public crate-yardstick-0.0.0 (crate (name "yardstick") (vers "0.0.0") (hash "0fgxz6h8rnc1nh9089npdfgf1w04hi74cw6c9sz8xh1fii2kr238") (yanked #t)))

