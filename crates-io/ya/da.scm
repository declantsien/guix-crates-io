(define-module (crates-io ya da) #:use-module (crates-io))

(define-public crate-yada-0.1 (crate (name "yada") (vers "0.1.0") (hash "1ak7by18n26snzq14xd6c4ci9xrpn0j2kc3iqbp705yni5bzhqjn")))

(define-public crate-yada-0.2 (crate (name "yada") (vers "0.2.0") (hash "0xlnqhpsv5c6wqagdxxf32njzgfbiwhfwqb3ljl7s59xzgg8sqkm")))

(define-public crate-yada-0.3 (crate (name "yada") (vers "0.3.0") (hash "1xh92bmpjy3r8fb4yfnjb60v3s569v8ym8irrczfyp5ydsi5mgw1")))

(define-public crate-yada-0.3 (crate (name "yada") (vers "0.3.1") (hash "08lnd1dv7rjxi05xrzw703kwix6j2pvdhikb79d28154x94585v6")))

(define-public crate-yada-0.3 (crate (name "yada") (vers "0.3.2") (hash "05hagr5p19ladllzc8xypa03bg4bjhs24z7lmb1rjcb09dnyd068")))

(define-public crate-yada-0.4 (crate (name "yada") (vers "0.4.0") (hash "1yrc1zr3i8wb41iypk9ri7fb364b7i55765x7vnca0s0cgqi3m1b")))

(define-public crate-yada-0.4 (crate (name "yada") (vers "0.4.1") (hash "19bi13skglzsgvx5sdnbnf02xg3md86l538z9yz829w97mwvcyy8")))

(define-public crate-yada-0.5 (crate (name "yada") (vers "0.5.0") (hash "1hc5wl40406fi6d2ffchgxa4i034wfx4b5gdf2v2mgvvlnvjrldn")))

(define-public crate-yada-0.5 (crate (name "yada") (vers "0.5.1") (hash "1pgzmm965f5396q1mj5rfbxmmd7hmnynr435hx8h5a28ksyi3ldf")))

(define-public crate-yada_mod-0.4 (crate (name "yada_mod") (vers "0.4.0") (hash "1xj1gm68qg7fy83akpymc5ixq3sy8y35qkp3y3qycp3g4kaqi1ys")))

(define-public crate-yadacha-0.0.1 (crate (name "yadacha") (vers "0.0.1") (deps (list (crate-dep (name "getrandom") (req "^0.2.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.22.0") (default-features #t) (kind 2)))) (hash "1fsagcsdws40a4wqsawwd88k07s7xryg3xizcbq67br4nph7apqi") (features (quote (("cli" "getrandom" "memmap")))) (yanked #t)))

(define-public crate-yadacha-0.0.2 (crate (name "yadacha") (vers "0.0.2") (deps (list (crate-dep (name "getrandom") (req "^0.2.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.22.0") (default-features #t) (kind 2)))) (hash "0f84xpk273fk987g48rj1xmg2bfi5ar9s1xl5h4rr67qywq6673k") (features (quote (("cli" "getrandom" "memmap")))) (yanked #t)))

(define-public crate-yadacha-0.0.3 (crate (name "yadacha") (vers "0.0.3") (deps (list (crate-dep (name "getrandom") (req "^0.2.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.22.0") (default-features #t) (kind 2)))) (hash "1yx46v3c6z0n1d610wqzgqh0dp8yd32lnxjmzy8fr3i8z10sa5xa") (features (quote (("cli" "getrandom" "memmap")))) (yanked #t)))

(define-public crate-yadacha-0.0.4 (crate (name "yadacha") (vers "0.0.4") (deps (list (crate-dep (name "getrandom") (req "^0.2.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.22.0") (default-features #t) (kind 2)))) (hash "1smfjfz4ipc8nc2ydya4zm35mwrspsb4k21av9ip4chvvahrijjb") (features (quote (("cli" "getrandom" "memmap"))))))

(define-public crate-yadacha-0.0.5 (crate (name "yadacha") (vers "0.0.5") (deps (list (crate-dep (name "getrandom") (req "^0.2.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.22.0") (default-features #t) (kind 2)))) (hash "09iqvwq57klnlgij5dynzchd2zfpqwdi1xvw638ac06ly449zy5x") (features (quote (("cli" "getrandom" "memmap"))))))

