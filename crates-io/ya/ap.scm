(define-module (crates-io ya ap) #:use-module (crates-io))

(define-public crate-yaap-0.0.1 (crate (name "yaap") (vers "0.0.1") (hash "0fdd4xwa8nhaxgi3s6yr6wsj20m3y7ldca5c88pza6l8ah2hgvl7")))

(define-public crate-yaap-0.0.2 (crate (name "yaap") (vers "0.0.2-dev01") (hash "0mkjx6g6lldpp9bnramhahabmgjy10jq8vaiqldanlw7as8n1qdn")))

(define-public crate-yaap-0.0.2 (crate (name "yaap") (vers "0.0.2") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.12") (default-features #t) (kind 2)))) (hash "12v739ksf72iadwbjqgqvhr54km32bfpg0crd4a64w8yyxgga85x") (features (quote (("default" "alloc") ("alloc"))))))

