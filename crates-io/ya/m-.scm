(define-module (crates-io ya m-) #:use-module (crates-io))

(define-public crate-yam-core-0.1 (crate (name "yam-core") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "135ss4djscirj2qwmfm2igsg091fr37w20akpi0xib3hia5kabfy") (rust-version "1.65")))

(define-public crate-yam-rs-0.1 (crate (name "yam-rs") (vers "0.1.0") (hash "18vrzpqjlj58s28w7bi5fvbrbkhm3a41yi808h6hmpb5rnqyj6rk")))

