(define-module (crates-io ya db) #:use-module (crates-io))

(define-public crate-yadbmt-0.1 (crate (name "yadbmt") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.6.3") (features (quote ("runtime-async-std-native-tls" "postgres"))) (default-features #t) (kind 0)))) (hash "143qdrzhdv0i40k5wg9wdblavr9474iy8fw0dzsxh8fxyxh97s2x")))

(define-public crate-yadbmt-0.1 (crate (name "yadbmt") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.6.3") (features (quote ("runtime-async-std-native-tls" "postgres"))) (default-features #t) (kind 0)))) (hash "170hnzazbs4y5fm1jy4lnkhyv10sz6amlkp23mncn5aldlbdhpa9")))

