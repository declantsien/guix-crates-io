(define-module (crates-io ya ll) #:use-module (crates-io))

(define-public crate-yall-0.1 (crate (name "yall") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "08lajkymgdx7ig5s9l0ix3hqnprywbwaad5vmj2bvhpr14154j2v")))

(define-public crate-yall-0.1 (crate (name "yall") (vers "0.1.1") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "1d8asa4r99jy7wn34brqf1hzqkl0wdmj5y359sscj2qihww26x3k")))

(define-public crate-yall-0.2 (crate (name "yall") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0z1g6yvmahvih7kayl0m80q96ysssh9l0zg52v5zmrnyifs8zbyl")))

(define-public crate-yall-0.3 (crate (name "yall") (vers "0.3.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0vdz0klj63fgimif5f3421sf1w8dyq6xyd7ylzb2mgf9grq90n7y")))

(define-public crate-yall-0.4 (crate (name "yall") (vers "0.4.0") (deps (list (crate-dep (name "atty") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req ">=2.0.0, <3.0.0") (kind 2)) (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req ">=1.1.0, <2.0.0") (default-features #t) (kind 0)))) (hash "0anr37h9x5y9rigdir5yliv7hmm9bxx03snai2baagaja2jn7qrd")))

(define-public crate-yall-0.5 (crate (name "yall") (vers "0.5.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "1scx4bv3jppdgp6rm9k9v8lgbfzrvxj61p5sr0zh7kii7dn0bi8m")))

(define-public crate-yall-0.6 (crate (name "yall") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("cargo"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0sf6zqkv4hpcbxw430585rfqpp697wxqkdgkgm4ixyb04qpblxab")))

(define-public crate-yall-0.6 (crate (name "yall") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("cargo"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0g02x521mi4y93dk177vg8h4cicgmvm8wci424jy3qpwg8xxyamw")))

