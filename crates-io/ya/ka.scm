(define-module (crates-io ya ka) #:use-module (crates-io))

(define-public crate-yakataN_guessing_game-0.1 (crate (name "yakataN_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1bcpzkw1dfn9vh9wslpqbprfc5fi49kf07kgyf0smxsspg0n6iq2")))

(define-public crate-yakataN_guessing_game-0.1 (crate (name "yakataN_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "102xz2fzkpd4n0nj7h202a77ih25n04b23mxbcncg7gi6m77s0x8")))

