(define-module (crates-io ya ca) #:use-module (crates-io))

(define-public crate-yaca-0.0.1 (crate (name "yaca") (vers "0.0.1") (hash "1aywwid5jz76zd33h5asd41w9r5rmlmvqs3amysf6jmrvfpg0b6m")))

(define-public crate-yaca-0.1 (crate (name "yaca") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1jz62jrws0blcn7z0lv6aw2g5z9qcv9w8g1a0jrj5wyyy0cq59b8")))

(define-public crate-yaca-0.1 (crate (name "yaca") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0p728ywrpsyg6y3vi2pcbzsf0wpl8f2pax76739g7izmzc2nv824")))

(define-public crate-yaca-0.1 (crate (name "yaca") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1rshg83bnai2vwq7j1l3xfl79bhxm5p9vak9zbdalgh7jp3br27m")))

(define-public crate-yaca-0.1 (crate (name "yaca") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1nbx7n4zbzzy5fiyynjxifkqjsq60qvw4mdfja46kypd6mqcs7g3")))

(define-public crate-yaca-0.1 (crate (name "yaca") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0j1b9jlqfb3ccyim455f11brvqhs0zdfssrr8wfvyfbli299vsq6") (features (quote (("static"))))))

(define-public crate-yaca-0.1 (crate (name "yaca") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vlrg2g0i63dj81ncgara4cz0h2dqps3kdic1a2l0hds21snxzzy") (features (quote (("static"))))))

(define-public crate-yaca-0.1 (crate (name "yaca") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "1lamg6h33npzm7z01rkwc9hd14m3glkchpmzpxnslh14xavnxmnn") (features (quote (("static"))))))

