(define-module (crates-io wf c-) #:use-module (crates-io))

(define-public crate-wfc-rs-0.1 (crate (name "wfc-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v6i034bdv9m22y44zggxr5qqbxpaps4jhpnj9q7lhw6nc0kxqwa") (links "wfc")))

(define-public crate-wfc-rs-0.2 (crate (name "wfc-rs") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vzqyzjx5nark7njvrpxg8ad6xp98v3paqpjrlp2pl5nasf1jwcz") (links "wfc")))

(define-public crate-wfc-rs-0.3 (crate (name "wfc-rs") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12pz3mllip9qh64vb1mw2jsw2p5i2h2ygs2vlc7qmsv5x7f9cgp6") (links "wfc")))

(define-public crate-wfc-rs-0.3 (crate (name "wfc-rs") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zlv7gy9iwhbslpfxn29mmnlkbj0s61z0asd2l3vqgya798bh5iy") (links "wfc")))

(define-public crate-wfc-rs-0.3 (crate (name "wfc-rs") (vers "0.3.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w3b9f1y8mjg394g2zbx36npicg13zgmnh83smbs8g6sjxbab5dg") (links "wfc")))

(define-public crate-wfc-rs-0.4 (crate (name "wfc-rs") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18j565x5wlxaz3gwsyd07whrn2alkxnpmv894mrl6ls1y2vv8za0") (links "wfc")))

(define-public crate-wfc-rs-0.4 (crate (name "wfc-rs") (vers "0.4.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r2hq4a4lnxqyzzrx9kpy8y5p6mwvdk0lx2j1027y9xxnb51ll8p") (links "wfc")))

(define-public crate-wfc-rs-0.4 (crate (name "wfc-rs") (vers "0.4.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "151r3i27kdl8kjyrzc01pz2bpxna12f5prqi3s2j7aq2fccnq8rn") (links "wfc")))

(define-public crate-wfc-rs-0.5 (crate (name "wfc-rs") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hwli2nzfl463ky6nfwwf3jqhhqq4c65jq0205zhiqa9889chgs2") (links "wfc")))

(define-public crate-wfc-rs-0.5 (crate (name "wfc-rs") (vers "0.5.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ki86lq6n64g9br3740icdk67z65qdh7y0gvr261a27x3apy4dsx") (links "wfc")))

(define-public crate-wfc-rs-0.5 (crate (name "wfc-rs") (vers "0.5.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xjhsqjb0sh2yj38bnvf81ad7iw5p08whyhdb2rrcxyvwxzpi04a") (links "wfc")))

(define-public crate-wfc-rs-0.5 (crate (name "wfc-rs") (vers "0.5.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k3i9hjdifg01gfvfb0jxx174nkgv5qy45zvd1353my3mllj529z") (links "wfc")))

(define-public crate-wfc-rs-0.6 (crate (name "wfc-rs") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "001b4j90cmx04p8j5ggi70v2vk18301gphmdwq0iljghkd8mnynr") (links "wfc")))

(define-public crate-wfc-rs-0.6 (crate (name "wfc-rs") (vers "0.6.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jjn2zg3vil03gfpx85sf0zwyy5586acwnjwzmvdjvfilfmnqkjh") (links "wfc")))

