(define-module (crates-io wf a-) #:use-module (crates-io))

(define-public crate-wfa-wts-sim-0.1 (crate (name "wfa-wts-sim") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "mockstream") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0zw9v1qzc0pvclx19xhd4s7l261q3ma39763a6qqw6mlsdgp1c4g")))

