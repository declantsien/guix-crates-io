(define-module (crates-io wf cg) #:use-module (crates-io))

(define-public crate-wfcgen-0.1 (crate (name "wfcgen") (vers "0.1.0") (deps (list (crate-dep (name "fastwfc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "10p1057jgckqwlxcydirm918bbn0hr2hvzklfzlij6rwnrx2zf72")))

