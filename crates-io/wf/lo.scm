(define-module (crates-io wf lo) #:use-module (crates-io))

(define-public crate-wflow-0.1 (crate (name "wflow") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cli_printer") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1g0sjw9wriymqgdykg0mcd8wgy9xbdiq9jxmkgh53d9lw5gqy2fq")))

