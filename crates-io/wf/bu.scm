(define-module (crates-io wf bu) #:use-module (crates-io))

(define-public crate-wfbuf-0.1 (crate (name "wfbuf") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mmap") (req "^0.1") (default-features #t) (kind 0)))) (hash "1782zyyy40hcc9lgr0jb4nmf11mhdawhzrr499pn8pf5yrzaxv40")))

(define-public crate-wfbuf-0.1 (crate (name "wfbuf") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mmap") (req "^0.1") (default-features #t) (kind 0)))) (hash "0i3scrwq7iqxmhxlprm8jldm83z77wixbghvv3bi5c99qx6nv09h")))

