(define-module (crates-io wf ir) #:use-module (crates-io))

(define-public crate-wfir-0.1 (crate (name "wfir") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16") (default-features #t) (kind 0)))) (hash "1zjh1fz6ldc6nra6r5plhzhv9gqiv6smmfsm50q6mdc2jayswryl")))

(define-public crate-wfir-0.1 (crate (name "wfir") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16") (default-features #t) (kind 0)))) (hash "03kvd8m7jbdz6ninc71md8is6fzdjcjsk04p531m5slbiac0f8mv")))

(define-public crate-wfir-0.1 (crate (name "wfir") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16") (default-features #t) (kind 0)))) (hash "0v3hvrdr5h0xd5y69l7cdbvriixj4p34w7likzgsnblq28vrbg9s")))

