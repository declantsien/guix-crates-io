(define-module (crates-io wf db) #:use-module (crates-io))

(define-public crate-wfdb-rust-0.1 (crate (name "wfdb-rust") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "09dknndqag2k8w8nlvr9h4kp4v7h65xnzx1z7yd08mnbwi9w1f45")))

(define-public crate-wfdb-rust-0.1 (crate (name "wfdb-rust") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "15mbg0cbflqccjz5ch9p9xwk8dgy4iys28czf0qp4zgkfarffqx1")))

(define-public crate-wfdb-rust-0.2 (crate (name "wfdb-rust") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hgd70dgdi5xsqxcwsbmm3rrn4a619w2byw7j5przzi8p78dg41r")))

(define-public crate-wfdb-rust-0.3 (crate (name "wfdb-rust") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "090nac3cb6gbmifijwcz70k7mpr4jxz9kqgbf3s2k013gilx5bh8")))

