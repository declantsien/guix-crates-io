(define-module (crates-io wf qu) #:use-module (crates-io))

(define-public crate-wfqueue-0.1 (crate (name "wfqueue") (vers "0.1.0") (deps (list (crate-dep (name "cache-padded") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fw3hw33pb8vinr9ikf15j9xbbr86bad0df6i840jqnlqxiyndrf") (yanked #t)))

(define-public crate-wfqueue-0.1 (crate (name "wfqueue") (vers "0.1.1") (deps (list (crate-dep (name "cache-padded") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "12h541l85qnkvn9ml49x1k7jka69vjkp19cip20ssp5s0zbjnc5q") (yanked #t)))

(define-public crate-wfqueue-0.2 (crate (name "wfqueue") (vers "0.2.0") (deps (list (crate-dep (name "cache-padded") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "per-thread-object") (req "^0.4") (default-features #t) (kind 0)))) (hash "17i16i1k0ccjdnhbhk4flijaph5rh5gd3qmhz8i181lrzq75qh9a")))

(define-public crate-wfqueue-0.2 (crate (name "wfqueue") (vers "0.2.1") (deps (list (crate-dep (name "cache-padded") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "per-thread-object") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nsb7l97zbpiyjrdizby7vx83xfv7xrcmv4qfkcvg6xczgih0gp7")))

