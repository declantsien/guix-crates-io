(define-module (crates-io nm -b) #:use-module (crates-io))

(define-public crate-nm-binutils-0.0.0 (crate (name "nm-binutils") (vers "0.0.0") (hash "1cp3r9n8nrdhw8pwqk35jhgmy48zk047zcx81ccsp5xi0i428jy3")))

(define-public crate-nm-binutils-0.1 (crate (name "nm-binutils") (vers "0.1.0") (deps (list (crate-dep (name "goblin") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1dlvky9hjgm6a6886lz4lvb031jrh1bh8c07rv15a07qnvcqyfib")))

(define-public crate-nm-binutils-0.1 (crate (name "nm-binutils") (vers "0.1.1") (deps (list (crate-dep (name "goblin") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0jmpw464xw7l2pc9jg9h3d226aglp63871qkcwhhjjhwxmy1yh2h")))

(define-public crate-nm-binutils-0.1 (crate (name "nm-binutils") (vers "0.1.2") (deps (list (crate-dep (name "goblin") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0xi95r8gzfm39n0248ama4ivvgxzjhrqg5r2faxmqb7ax4xg2m83")))

