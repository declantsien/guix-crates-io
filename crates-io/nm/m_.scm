(define-module (crates-io nm m_) #:use-module (crates-io))

(define-public crate-nmm_lib-0.1 (crate (name "nmm_lib") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1chdnq6n4aq9yz3kmvfb74s15qfq6f72rv05ykc5g3c6v93pa0fg")))

(define-public crate-nmm_lib-0.1 (crate (name "nmm_lib") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qy6vasvfhwhkkfpaib9xvk1mlmyrc5kn2bz8cwvlnzvn3xzcbrs")))

(define-public crate-nmm_lib-0.2 (crate (name "nmm_lib") (vers "0.2.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "03lm7nrmflk15lgp4vhcclf9zpajybjvywvab2w7vvd3sp1jd21f")))

