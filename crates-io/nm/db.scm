(define-module (crates-io nm db) #:use-module (crates-io))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.10.14") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "1517k8q5zz7a17pjzqk21km3ay6zgiq29p0ij5zfz3xk78x84bvr")))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.14.6") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "0rm66kpsw9jfqdgqdlsdp1v1cskfi2dbrpjp4p6jb9i31a5lc6nx")))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.18.10") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "0lj24v6h5mbnxcsgrw5b7x6wgdl9msdp5xmgkpbfaa1bhinb3y3g")))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.22.16") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "1js0wz7src8jhmrgrxkc6n5phqm9mgncaqqrvszwwjrzwvbarjyf")))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.26.8") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "0jispsf5n2f27spvxlz385qrzary5ic298ih4kv1dvwqdqmp4b7g")))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.31.90") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "0cifinvp56h8ygbgd0gzcnqn4qzf9xryalwwq1ghkb1lx3v6qnnv")))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.33.90") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "0d79727bjdqg8i5df8a8h3klz2qsk5vyk2r6h2zvj6kisirgd50z")))

(define-public crate-nmdbus-1 (crate (name "nmdbus") (vers "1.37.3") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)))) (hash "1s2am3qlfk9757lq5x81vx6fyry4c3njdpl18l2hdrsgjjxay5lb")))

