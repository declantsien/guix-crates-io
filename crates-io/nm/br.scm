(define-module (crates-io nm br) #:use-module (crates-io))

(define-public crate-nmbr-bounded-0.0.1 (crate (name "nmbr-bounded") (vers "0.0.1") (hash "0lm59130wifrvy3v7q0wp4d4yqbk2nj9lxs1c1w92f0qpdh9ndby") (yanked #t)))

(define-public crate-nmbr-float-0.0.1 (crate (name "nmbr-float") (vers "0.0.1") (hash "0pw7kbs4qnp67pbp986wai3bpw2c4cplpy3mccgyrs3ksdn3n331") (yanked #t)))

(define-public crate-nmbr-identities-0.0.1 (crate (name "nmbr-identities") (vers "0.0.1") (hash "1n4caq9xvfg6bpgqpyp6rik2pkwvjllmmr7fjfi49h2p1jlpxk4h") (yanked #t)))

(define-public crate-nmbr-signed-0.0.1 (crate (name "nmbr-signed") (vers "0.0.1") (hash "1gmha7bwibds8j2bsvf3dhd503dfbkg2aqhki37n7hi630yxkyhs") (yanked #t)))

