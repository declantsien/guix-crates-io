(define-module (crates-io nm at) #:use-module (crates-io))

(define-public crate-nmatoolkit-0.1 (crate (name "nmatoolkit") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1j28n83gvw7kqiy1jmr4bi3jlmmagbaq5n62shnrxamqa45szzlx")))

