(define-module (crates-io nm os) #:use-module (crates-io))

(define-public crate-nmos6502-1 (crate (name "nmos6502") (vers "1.0.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.11") (kind 0)))) (hash "0xy7wm8xhi5cy5qgyynb9v2fh9407mn9l1b8bscw8sxahndi83is")))

(define-public crate-nmos6502-1 (crate (name "nmos6502") (vers "1.0.1") (deps (list (crate-dep (name "num_enum") (req "^0.5.11") (kind 0)))) (hash "1zzhvnwq1cdgqclca66n0cmdpwm9118l8k46h3v2ilrv5l9dbdd8")))

