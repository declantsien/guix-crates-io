(define-module (crates-io nm on) #:use-module (crates-io))

(define-public crate-nmoney-0.0.0 (crate (name "nmoney") (vers "0.0.0") (hash "06vm133ga08lkdh684p8kqpd6rm88ygcm6lyribmz7x13690s8v9")))

(define-public crate-nmoney-1 (crate (name "nmoney") (vers "1.0.0") (hash "1mhyv2jhgishh14jfpsvcbci92ahqqalvs4k41kn8rvsibjdi1r6")))

