(define-module (crates-io nm l-) #:use-module (crates-io))

(define-public crate-nml-matrix-0.1 (crate (name "nml-matrix") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ln6246bsaapin7axz38rwnmhxaf3ya5xnycwwnq17n3cbc6m1a2")))

(define-public crate-nml-matrix-0.1 (crate (name "nml-matrix") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "04h5v92vva43b3h4asj03xzhg7hq2wh9qmnd96nifpbqz9llxdcx")))

(define-public crate-nml-matrix-0.1 (crate (name "nml-matrix") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1b28jh424yrhbnyb06izlbqwq0himhzlvjha3j2h0gg28h6irn6f")))

(define-public crate-nml-matrix-0.1 (crate (name "nml-matrix") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "17jb9r43hw9k1qvqfnihsj629iwhlq51yn90csznvyw7a77dvk3b")))

(define-public crate-nml-matrix-0.1 (crate (name "nml-matrix") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1591rylxbgnvx54s5gl17kgzsls9snlqfbjqkbflyhdllm1r4psd")))

(define-public crate-nml-matrix-0.2 (crate (name "nml-matrix") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wq3kb40hh9xxq8lax9w0kga2xfiy0jnz39d5f50cnmfrc45kc3k")))

(define-public crate-nml-matrix-0.2 (crate (name "nml-matrix") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02dy49v6h3q7fym77ldynpxyhc0nn61kjx4gskad4x1661mq0pph")))

(define-public crate-nml-matrix-0.3 (crate (name "nml-matrix") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03da0rnrncfsa8gqvndnzr1vj1g4nkwxpjfxry1xavp2z02zia7c")))

(define-public crate-nml-matrix-0.4 (crate (name "nml-matrix") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0hsr3r3fx36drbh4s82a8vwmz8c77i1qlba0kygf8vwbbpmn76bp")))

