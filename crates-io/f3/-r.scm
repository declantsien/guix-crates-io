(define-module (crates-io f3 -r) #:use-module (crates-io))

(define-public crate-f3-rs-0.1 (crate (name "f3-rs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "12j9nwvknmpfmjzrcndfrhlm3iy47ch9jn016fxllqmwjfv7d73d")))

