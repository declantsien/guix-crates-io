(define-module (crates-io hf s_) #:use-module (crates-io))

(define-public crate-hfs_nfd-0.1 (crate (name "hfs_nfd") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0xmzc83g2rvfldy0g9l3ca5kz7k3j57a14bx7nppvwzrxdbs8zli")))

(define-public crate-hfs_nfd-0.1 (crate (name "hfs_nfd") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "07rcsbckr5ddsd54xm1wk2v15i5k99i3d2axl1km5hlk7l5156y2")))

(define-public crate-hfs_nfd-1 (crate (name "hfs_nfd") (vers "1.0.0") (deps (list (crate-dep (name "hashbrown") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "18424n3jy6fqkszdwahacam2b600xlrbr8b3qx1k095m8hpaac5k")))

(define-public crate-hfs_nfd-1 (crate (name "hfs_nfd") (vers "1.1.0") (deps (list (crate-dep (name "ahash") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1gr5cnmyl6ihajr5869ljh7lwx9snx6mqx6nxa98vf831g3bjjnc")))

(define-public crate-hfs_nfd-2 (crate (name "hfs_nfd") (vers "2.0.0") (deps (list (crate-dep (name "ahash") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1qsv5vpqc7rzlbnbkbp564jm7dn8y30nb5fd5j9isz34ysm3vwfa") (features (quote (("bench"))))))

(define-public crate-hfs_paths-0.1 (crate (name "hfs_paths") (vers "0.1.0") (deps (list (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0i89gkzkbf550v0y53r5hn2yb1ikyq6xy7yp4bklp4blky7lcwc3")))

