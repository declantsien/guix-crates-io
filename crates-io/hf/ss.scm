(define-module (crates-io hf ss) #:use-module (crates-io))

(define-public crate-hfss_fld-0.1 (crate (name "hfss_fld") (vers "0.1.0") (deps (list (crate-dep (name "colour") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "1hbli22hkgn1mcwawjw8shan4g27rxs6xxqb9dxr6vxy2pxbwlaw")))

(define-public crate-hfss_fld-0.1 (crate (name "hfss_fld") (vers "0.1.1") (deps (list (crate-dep (name "colour") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "182gvx6npay9zfasi5hjwgkbywnzbbznwa4knnylfs2w0x15ldsv")))

(define-public crate-hfss_fld-0.1 (crate (name "hfss_fld") (vers "0.1.2") (deps (list (crate-dep (name "colour") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "0zsv6kcg0m87ikagm710rwq0vipn474szi3cvq31zp0d3nmn5aa6")))

(define-public crate-hfss_fld-0.1 (crate (name "hfss_fld") (vers "0.1.3") (deps (list (crate-dep (name "colour") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "0ymw7lxahamdz173xin08x49w9cm3v40zfwx41wjlkcdnsvkidzk")))

(define-public crate-hfss_fld-0.1 (crate (name "hfss_fld") (vers "0.1.4") (deps (list (crate-dep (name "colour") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "09xw91pcv36k7hzsi47xjzziv5y50g26sl0yqlylbs1fw99jlacw")))

