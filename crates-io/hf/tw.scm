(define-module (crates-io hf tw) #:use-module (crates-io))

(define-public crate-hftwo-0.1 (crate (name "hftwo") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08q0yrrr9klm5ryaqbjxc23wxy3ha4pcyh1kb4rky6z1ilbygzgk") (v 2) (features2 (quote (("defmt-03" "dep:defmt"))))))

(define-public crate-hftwo-0.1 (crate (name "hftwo") (vers "0.1.1") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0drzjnbcf20hpvr1bcrflzgp3a7ifwzm77b7wcn31sxyh7d1fcba") (v 2) (features2 (quote (("defmt-03" "dep:defmt"))))))

(define-public crate-hftwo-0.1 (crate (name "hftwo") (vers "0.1.2") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qn33h46cjj7q8qpq3ygri0v1ifyj0s2rrk6qjml6lx7vkd522nz") (v 2) (features2 (quote (("defmt-03" "dep:defmt"))))))

