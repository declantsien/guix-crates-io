(define-module (crates-io fq n-) #:use-module (crates-io))

(define-public crate-fqn-estimator-0.0.1 (crate (name "fqn-estimator") (vers "0.0.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (optional #t) (default-features #t) (kind 0)))) (hash "0g5121057yyyba2w0gm68b000v7rnjjprh3h64frhqdbjzndk4d4") (features (quote (("default" "num-traits")))) (v 2) (features2 (quote (("num-traits" "dep:num-traits"))))))

(define-public crate-fqn-estimator-0.1 (crate (name "fqn-estimator") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (optional #t) (default-features #t) (kind 0)))) (hash "0g92y6glr8shh234cc7vh9q5f693rnhqmgs4pa3labx8bq21k134") (features (quote (("default" "num-traits")))) (v 2) (features2 (quote (("num-traits" "dep:num-traits"))))))

(define-public crate-fqn-estimator-0.2 (crate (name "fqn-estimator") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (optional #t) (default-features #t) (kind 0)))) (hash "0isisqp6ylcnvlmdlisypqlmzfbnlas17ycpwzw13kdcys95s0s2") (features (quote (("default" "num-traits")))) (v 2) (features2 (quote (("num-traits" "dep:num-traits"))))))

(define-public crate-fqn-estimator-0.2 (crate (name "fqn-estimator") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (optional #t) (default-features #t) (kind 0)))) (hash "1xijqnyd74i6s5h6cidnphqlniziq22g941ssa4rq4hvs8ai1h5a") (features (quote (("default" "num-traits")))) (v 2) (features2 (quote (("num-traits" "dep:num-traits"))))))

