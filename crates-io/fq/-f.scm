(define-module (crates-io fq -f) #:use-module (crates-io))

(define-public crate-fq-filter-reads-0.1 (crate (name "fq-filter-reads") (vers "0.1.0") (deps (list (crate-dep (name "bio") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0f7n9wdnzzzlw4ka5z409vfq08bpiyh203l9si24za194l45jnzh")))

