(define-module (crates-io fq l_) #:use-module (crates-io))

(define-public crate-fql_deserialize-0.2 (crate (name "fql_deserialize") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.7.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "151swgxvmgl1w1y0pq09nl780rlc8iq9j9f7d3inqd13n9vl41nc")))

(define-public crate-fql_deserialize-0.2 (crate (name "fql_deserialize") (vers "0.2.7") (deps (list (crate-dep (name "indexmap") (req "^1.7.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "13m24m1q6fc1zs5cml31a8s3d3cs6zkv6xpl89pyf79kglkblf85")))

(define-public crate-fql_deserialize-0.2 (crate (name "fql_deserialize") (vers "0.2.8") (deps (list (crate-dep (name "indexmap") (req "^1.7.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fbigag5kalqcpav7ga4048alran7imd0yh6k14y1617wb9ps431")))

(define-public crate-fql_serialize-0.2 (crate (name "fql_serialize") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)))) (hash "0x1y9bzpp12609cp49cqzynmn0x4b298r35bkhm893qh3a3nag0r")))

(define-public crate-fql_serialize-0.2 (crate (name "fql_serialize") (vers "0.2.4") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)))) (hash "0yly7i6748a5m62z8hjz8pz5zzfc2j0fqsi28w7hmymr9fih4h9f")))

(define-public crate-fql_serialize-0.2 (crate (name "fql_serialize") (vers "0.2.6") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)))) (hash "13kdmn2n1bld4ymgqzyasl3mzc0j68qpsxykk54dg125igf3ra9c")))

(define-public crate-fql_server-0.1 (crate (name "fql_server") (vers "0.1.0") (deps (list (crate-dep (name "configparser") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "file_sql") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "nickel") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0gpfwmhvhqamllxmxnpi11jf7f5k8h9i0n5qzhcpclz50x8niv3n")))

