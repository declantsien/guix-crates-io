(define-module (crates-io xp su) #:use-module (crates-io))

(define-public crate-xpsupport-0.1 (crate (name "xpsupport") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "~0.3") (default-features #t) (kind 1)))) (hash "1j23wrri7kgiy3xlsrp1p8ggzfck7x01r010hx8hdsb256s9pf2g")))

(define-public crate-xpsupport-0.1 (crate (name "xpsupport") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "~0.3") (default-features #t) (kind 1)))) (hash "1issjwwg44x322w1728ai89bdv51013swkll3704qax1lswqaykn")))

(define-public crate-xpsupport-0.1 (crate (name "xpsupport") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "~0.3") (default-features #t) (kind 1)))) (hash "1kql9g6d1mpm9az1sfa7jhrriv8agq916w9n61n14wm35ycrs5jv")))

(define-public crate-xpsupport-0.1 (crate (name "xpsupport") (vers "0.1.3") (deps (list (crate-dep (name "gcc") (req "^0.3.45") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "1ndy70bmzsdphxlzb2xqcg0yr2kn2hv43fcd0cc0i947kdznz2qx")))

(define-public crate-xpsupport-0.1 (crate (name "xpsupport") (vers "0.1.4") (deps (list (crate-dep (name "gcc") (req "^0.3.45") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0bgqvr7bar4dsax6f16mabn1hi9k8llw56i446hxml7pxy09rm1p")))

(define-public crate-xpsupport-0.1 (crate (name "xpsupport") (vers "0.1.5") (deps (list (crate-dep (name "gcc") (req "^0.3.45") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "15ljydxzb4qywradla2c9m1ddkdrdfkj938hlp2c024ghwdm0ldp")))

(define-public crate-xpsupport-0.2 (crate (name "xpsupport") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.46") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.2.3") (default-features #t) (kind 1)))) (hash "0gkx64kjziz3sfvx08wlj6rgaqnv9w5bvb4k1v7wpfvq6rw0xhm1")))

(define-public crate-xpsupport-0.2 (crate (name "xpsupport") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.46") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.2.3") (default-features #t) (kind 1)))) (hash "0q5qhwciw83dh7pnyy1070jwrl7hnk7ql21n71vab000py6bpd37")))

(define-public crate-xpsupport-0.2 (crate (name "xpsupport") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.46") (default-features #t) (kind 1)))) (hash "1yw5571v5i0cja0c2rjqi43cmakwq1w95a3n4rxqvg1g6ipxplcg")))

(define-public crate-xpsupport-sys-0.1 (crate (name "xpsupport-sys") (vers "0.1.0") (deps (list (crate-dep (name "xpsupport") (req "~0.1") (default-features #t) (kind 0)))) (hash "0zz7siqqr8cqqxnrsv2a3k44axwsxgr382aisywdyqlwb2spps3k")))

