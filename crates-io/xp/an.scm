(define-module (crates-io xp an) #:use-module (crates-io))

(define-public crate-xpanda-0.1 (crate (name "xpanda") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1rsq47qg7nik46021s8q8w6xlj7bbr6dlsmlx41d3lw9gjyihkmx")))

