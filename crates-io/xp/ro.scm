(define-module (crates-io xp ro) #:use-module (crates-io))

(define-public crate-xprogram-0.1 (crate (name "xprogram") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "solana-program") (req "^1.7.11") (default-features #t) (kind 0)) (crate-dep (name "solana-sdk") (req "^1.7.11") (default-features #t) (kind 2)))) (hash "0p6j15i35fzwmgn0w30hywfnm3jkijxw4x4jxzx65qywmc1id2b8") (features (quote (("no-entrypoint"))))))

(define-public crate-xprompt-0.1 (crate (name "xprompt") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "clap_derive") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.12") (kind 0)))) (hash "1isg082fdvd9f7nhznl8li7jgzr73qmnyizc353amlhzgpx5x8yj") (yanked #t)))

(define-public crate-xprompt-0.1 (crate (name "xprompt") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "clap_derive") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.12") (kind 0)))) (hash "00kr04amnyw8p0r0vxi3qv55v8hdgbzgi80an27p6fxzgclzk199")))

(define-public crate-xprompt-0.2 (crate (name "xprompt") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req ">=0.12.1, <0.13.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req ">=3.0.0-beta.2, <4.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap_derive") (req ">=3.0.0-beta.2, <4.0.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req ">=0.13.12, <0.14.0") (kind 0)))) (hash "00ms4mvx3v0g09dfajir5acqa0kah757qfmahlw6ds99nzz8cr2f")))

(define-public crate-xprompt-0.2 (crate (name "xprompt") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "clap_derive") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.12") (kind 0)))) (hash "0yadyjcsncpnqpvv2jz40k8nv19k6dhkq0i6i78q2n6sk3kjvd12")))

(define-public crate-xprompt-0.2 (crate (name "xprompt") (vers "0.2.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "clap_derive") (req "^3.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.12") (kind 0)))) (hash "0bz5xb4pz2zzn2mm488jw95awvqa14dmyvdni4p3is5hsxb819j0")))

(define-public crate-xprompt-0.2 (crate (name "xprompt") (vers "0.2.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-rc.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.12") (kind 0)))) (hash "1h99akjy6xfvm0yw2478qfz0db1qqr5awvy5bigkim7islhyfy3d")))

(define-public crate-xproto-0.1 (crate (name "xproto") (vers "0.1.0") (hash "13q9aw5gc4kldxxzf3hj8dpbx6fw18x9izfdgc6x98ym37vv7wnb")))

(define-public crate-xproto-1 (crate (name "xproto") (vers "1.0.0") (hash "0c4mihlsl8calj4d0rbip7p8z3lcd97z9kjlmfp495bnbqp6fbb2")))

(define-public crate-xproto-1 (crate (name "xproto") (vers "1.1.0") (deps (list (crate-dep (name "protocol") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0icxz58yv9hvsf4iifaj62shjxqj5zz9hxqfz6nprvx3mh3aq464") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1 (crate (name "xproto") (vers "1.1.1") (deps (list (crate-dep (name "protocol") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "057pyw5hm830jgw4mdmd63vw3a1y0ds7vsyiagn6sy8jlc47379m") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1 (crate (name "xproto") (vers "1.1.2") (deps (list (crate-dep (name "protocol") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1rbq17gk9gkbkn6qrg7qrzclalyab4ygfzbf8n9hzvalbmiiwzix") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1 (crate (name "xproto") (vers "1.1.3") (deps (list (crate-dep (name "protocol") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "11jymlmgifz6yrybd4zqpp1lq44lx528jwrmjn3zw08g194f3xsi") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1 (crate (name "xproto") (vers "1.1.4") (deps (list (crate-dep (name "protocol") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ayb018d48bw3kzxkw2dh1zzjrx9hzmdq67rknysarlsd7xvzc7x") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-1 (crate (name "xproto") (vers "1.1.5") (deps (list (crate-dep (name "protocol") (req "^3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1wrjimn500d2x88jy29i1q3ggdjh20qykx33kvq4n0avazkvg1dd") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-2 (crate (name "xproto") (vers "2.0.0") (deps (list (crate-dep (name "protocol") (req "^3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^3.1") (optional #t) (default-features #t) (kind 0)))) (hash "1kp598wbl5hrdk7bxj1rzl221apf7218pgljz6zkq6rxvxg0zlv1") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproto-2 (crate (name "xproto") (vers "2.0.1") (deps (list (crate-dep (name "protocol") (req "^3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "protocol-derive") (req "^3.1") (optional #t) (default-features #t) (kind 0)))) (hash "0arn7spb8kjwz7bzvz0hlyrs4688dgf7rizyabnh6j11nbns9b36") (features (quote (("protocol-support" "protocol" "protocol-derive") ("default" "protocol-support"))))))

(define-public crate-xproxy-0.1 (crate (name "xproxy") (vers "0.1.0") (hash "052jwycw9apnhgnqyh9ga7xmvy0n01jmcd8a9par63cxgx9zi51s")))

