(define-module (crates-io xp x-) #:use-module (crates-io))

(define-public crate-xpx-supercontracts-sdk-0.2 (crate (name "xpx-supercontracts-sdk") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11682m9gzwwkh3ijd1q51mq10s8zgngpdn737lqbmnm8jf4j2pr3")))

(define-public crate-xpx-supercontracts-sdk-0.2 (crate (name "xpx-supercontracts-sdk") (vers "0.2.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fjyj6rhngqkpvaskk554xb2vm10xsfn6s7hl69npwn2ddp82i22")))

(define-public crate-xpx-supercontracts-sdk-0.2 (crate (name "xpx-supercontracts-sdk") (vers "0.2.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nbq4r8yz84lzw5sjr5haw5bf7r2cr1zad1lyn6nzblnczxjxggq")))

