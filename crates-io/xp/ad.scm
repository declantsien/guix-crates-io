(define-module (crates-io xp ad) #:use-module (crates-io))

(define-public crate-xpad-0.1 (crate (name "xpad") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0yblxvdinbwf33papqajf2g7mxdbwvzi75y4a0108fgnh2793js5")))

