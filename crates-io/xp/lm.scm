(define-module (crates-io xp lm) #:use-module (crates-io))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "127z6qzrcb052d41mf0v7llkz1c1qd4faw6am4dnp676gw14g0ak")))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "19q2qx56yydi000sm1pxjcpqqy11cp39qr7vv755xgliyk76yl64")))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "04f1nc8am3gaph7pqlmwa95pxavmxzh201gfjlxn5j8d7b4d11pf")))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ryk2zq7cygslm979bvnr394hw1bbijmq6dqkyxv0hgv492dzar6")))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "08s6nn38icv94l61ri2i4jfmk3xrs1bmr5k8f1mnfr7s7cdpkrwp")))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hy6mcwi5vdrmarqb2140739249yab5cjpl09swc6k5zbipq729z")))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1bb8lxq93zbv55wgq7yqpm2wb44x74cgfj7gmwcwi62kli6fg9nk")))

(define-public crate-xplm-0.1 (crate (name "xplm") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1glai6g5j5y4i08q4qy9ssc2b0218imspn7z4ia74a4wfl9j5kkm")))

(define-public crate-xplm-0.2 (crate (name "xplm") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0ym5my7irj2v37rijji6jf1vf4cm78a2lfjqib2rqiq76gas3zrr")))

(define-public crate-xplm-0.2 (crate (name "xplm") (vers "0.2.2") (deps (list (crate-dep (name "hfs_paths") (req "^0.1.0") (default-features #t) (target "cfg(all(target_os = \"macos\", not(feature = \"xplm210\")))") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.2.1") (kind 0)))) (hash "02rbqnqcg8ha204xkvad5bi2qavj17haxkd8248i5fhgiq38lkpg") (features (quote (("xplm210" "xplm-sys/xplm210") ("xplm200" "xplm-sys/xplm200") ("default" "xplm200" "xplm210"))))))

(define-public crate-xplm-0.3 (crate (name "xplm") (vers "0.3.1") (deps (list (crate-dep (name "quick-error") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "09ah6wsdqqqhwzxvhf7wwx0accjv2g30fzp2mgmd5h8r0jl6vr1h")))

(define-public crate-xplm-0.4 (crate (name "xplm") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0vj268j5mmbpd5djzjhcqc7qp731pmi70hc92x3k64jpz3krx5x0")))

(define-public crate-xplm-0.4 (crate (name "xplm") (vers "0.4.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "xplm-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "18jhfbjkjfp04bliq2gvxkcan2qsd3r3allzrlvc630z0jfldl3b")))

(define-public crate-xplm-sys-0.1 (crate (name "xplm-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w08c81hg58zll7wvanshahl5sp1jdz7hf7kbnvgj19rd1cyaaqw")))

(define-public crate-xplm-sys-0.1 (crate (name "xplm-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "167h2izj5bhhyjn1lri63bha97w8y89g98ws8dsdyd37h9rwc82d")))

(define-public crate-xplm-sys-0.1 (crate (name "xplm-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n5g6z6fbzmhlcddx6md3s08c1d6db7bbn50rjz156l0d9p8rz73")))

(define-public crate-xplm-sys-0.1 (crate (name "xplm-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ni4dcswv9i6n1bk82zai9hxg2pkf395l8hvcqq6621sr1k0pzr2")))

(define-public crate-xplm-sys-0.1 (crate (name "xplm-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "02d504bjrm2i15l6d2zvqd2jz6cvpp626qj5d0x85w99zanzapck")))

(define-public crate-xplm-sys-0.1 (crate (name "xplm-sys") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b8x9xxnkn2ykrvzqb8jlz0a1q6cny8d3nndh2m3p74121bphx49")))

(define-public crate-xplm-sys-0.2 (crate (name "xplm-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.20.0") (default-features #t) (kind 1)))) (hash "18kna31rq030rgd0pf182b0hkcq7ms66jfd3lvzfvs3vr9b7g7gh")))

(define-public crate-xplm-sys-0.2 (crate (name "xplm-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.20.0") (default-features #t) (kind 1)))) (hash "0fa369lxg5q1mbwlx6yz6ka3h1ww05bkc32xfr5rz4yx3ckcyakg")))

(define-public crate-xplm-sys-0.2 (crate (name "xplm-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.20.0") (default-features #t) (kind 1)))) (hash "0ljvpjvg62d47wyq9rrk9z8pd9kr86zzknh8brvwkicrl0c003m1") (features (quote (("xplm210") ("xplm200") ("default" "xplm200" "xplm210"))))))

(define-public crate-xplm-sys-0.3 (crate (name "xplm-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.30.0") (default-features #t) (kind 1)))) (hash "1ylshs00wjsraxxb0gsld13jmdayr4zxrvy2za1vqvf2cylikr5q") (features (quote (("xplm300") ("xplm210") ("xplm200"))))))

(define-public crate-xplm-sys-0.3 (crate (name "xplm-sys") (vers "0.3.1") (hash "181g430jcsqs5nq240kmfjwahmbpnkibq01fqljdknc9j26xnczw")))

(define-public crate-xplm-sys-0.4 (crate (name "xplm-sys") (vers "0.4.0") (hash "0xwn5kbfxd5cp616l565k8dys6x9n9c5gvwij0shhcxa7yr340d5")))

(define-public crate-xplm-sys-0.5 (crate (name "xplm-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "17jn4szb1n1ffhkq0j1a00bjql17ja23c3mvjcky5xd20n4d06p5")))

