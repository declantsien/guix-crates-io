(define-module (crates-io ib mf) #:use-module (crates-io))

(define-public crate-ibmfloat-0.1 (crate (name "ibmfloat") (vers "0.1.0") (hash "16lvl1vpaqxayvngw5d120dxw27z7rd1nfmh0j5qz7nbm5lhslsr")))

(define-public crate-ibmfloat-0.1 (crate (name "ibmfloat") (vers "0.1.1") (hash "0pf8y5w36qlicb134fgmychb8q08nca4hiwwk883vvck8jxl6n47") (features (quote (("std") ("default" "std"))))))

