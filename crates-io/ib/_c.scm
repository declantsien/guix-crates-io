(define-module (crates-io ib _c) #:use-module (crates-io))

(define-public crate-ib_client-1 (crate (name "ib_client") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "15f4jg6vlgcrkavj093j5b4g3nhipfb8fbrpar6ikpigjnvc0827")))

