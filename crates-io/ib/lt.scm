(define-module (crates-io ib lt) #:use-module (crates-io))

(define-public crate-iblt-0.1 (crate (name "iblt") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x41k8vvcrgi3kya58r6fc234s3pcl4zn3z99gsk7rqjqm5ms2na")))

