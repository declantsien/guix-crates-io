(define-module (crates-io ib ve) #:use-module (crates-io))

(define-public crate-ibverbs-0.1 (crate (name "ibverbs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.25") (default-features #t) (kind 1)))) (hash "0xrfbjc4gwk51b4xj69bvaszndn3hf2ggwxqbjmzanzkir3qsvfy")))

(define-public crate-ibverbs-0.2 (crate (name "ibverbs") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.25") (default-features #t) (kind 1)))) (hash "1bnvrbjn4z0i1ifd3dxcy849hkjsr22x209jwccvp6imciri3ags")))

(define-public crate-ibverbs-0.2 (crate (name "ibverbs") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.25") (default-features #t) (kind 1)))) (hash "0zi8v6d048f68jz1bmgnbdh4pr8g0v909kbvfj0qazsab0mg0a14")))

(define-public crate-ibverbs-0.2 (crate (name "ibverbs") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.25") (default-features #t) (kind 1)))) (hash "0wppgdaxmv9m66c9kdx54h5wnslp9yishny44gys6jz7hifr6mmw")))

(define-public crate-ibverbs-0.2 (crate (name "ibverbs") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.25") (default-features #t) (kind 1)))) (hash "1fjjgqrx10h4i7njjzr77c54m2cfm0dvl9flm6b8nc7gip88ixfg")))

(define-public crate-ibverbs-0.2 (crate (name "ibverbs") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.25") (default-features #t) (kind 1)))) (hash "1gn1bi85ffrhx35y9b2fr4hfzc98rw3l2d0wiij8cxsn4wywf73p")))

(define-public crate-ibverbs-0.3 (crate (name "ibverbs") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.25") (default-features #t) (kind 1)))) (hash "0mmsw9m6x02k3688vw1w7926z5g3gq0qmz9d0wm45khmma0qxydp")))

(define-public crate-ibverbs-0.4 (crate (name "ibverbs") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)))) (hash "0kxkjnd8spchb3m0middm8crr2zi146k7sq30w5h0560809y1ih8") (links "ibverbs")))

(define-public crate-ibverbs-0.4 (crate (name "ibverbs") (vers "0.4.1") (deps (list (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)))) (hash "0nx2vmn604f246287924wzsydykb7w2x28f9g85mzi45bldzi9j2") (links "ibverbs")))

(define-public crate-ibverbs-0.4 (crate (name "ibverbs") (vers "0.4.2") (deps (list (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)))) (hash "1dr8kcxrc00n5brd9bvkzqf2gd1gbrp03ccw4b3r62p9mpn6nyi3") (links "ibverbs")))

(define-public crate-ibverbs-0.4 (crate (name "ibverbs") (vers "0.4.3") (deps (list (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)))) (hash "1339767hjigbj2yzkqssk7n4lx392x2bdkrv8wpid4fdcxfb6fpl") (links "ibverbs")))

(define-public crate-ibverbs-0.4 (crate (name "ibverbs") (vers "0.4.4") (deps (list (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)))) (hash "1fdsmm64bfji6l7s7iwbl6rba6bh5g4p5lgzfcizhldsz75q653g") (links "ibverbs")))

(define-public crate-ibverbs-0.4 (crate (name "ibverbs") (vers "0.4.5") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ndmr9q8g1xmqxkid6pr6jpc8x9j8gydvq03qn8zdy2nbd2nz3vn") (features (quote (("default" "serde")))) (links "ibverbs")))

(define-public crate-ibverbs-0.5 (crate (name "ibverbs") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zdfz2fq1hyzn0h51a38hqf393bs7lgaj6qvswdaych04cxx2vi6") (features (quote (("default" "serde")))) (links "ibverbs")))

(define-public crate-ibverbs-0.6 (crate (name "ibverbs") (vers "0.6.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xxp7x206xwj7y4q2ifpbxx90naylf8s3cars9x57y8f0c2inqz8") (features (quote (("default" "serde")))) (links "ibverbs")))

(define-public crate-ibverbs-0.6 (crate (name "ibverbs") (vers "0.6.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "13wacj8ywavl21yz5jslcn905irxqkn42ar4qxcrsyvq5yir2zaz") (features (quote (("default" "serde")))) (links "ibverbs")))

(define-public crate-ibverbs-0.7 (crate (name "ibverbs") (vers "0.7.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.0") (default-features #t) (kind 0) (package "ibverbs-sys")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mf9kw51w293zgsykgs1wk5k60x4y45jqfmqaay56cb588vp0nfi") (features (quote (("default" "serde"))))))

(define-public crate-ibverbs-0.7 (crate (name "ibverbs") (vers "0.7.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.0") (default-features #t) (kind 0) (package "ibverbs-sys")) (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0m6i41fcqahp3lq1ynccqz7xhna1z0k1f4ig45hj2kyx84p9ih1m") (features (quote (("default" "serde"))))))

(define-public crate-ibverbs-sys-0.1 (crate (name "ibverbs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0jg8j6f1x7wxx9033jwddrgnckgdlby8qxy5z80m3k9cm7kvpyl6") (links "ibverbs")))

(define-public crate-ibverbs-sys-0.1 (crate (name "ibverbs-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)))) (hash "1nasawy130n0filzdbx01dg1s7wackkdx6rhkpw3dan7hxqdddkp") (links "ibverbs")))

(define-public crate-ibverbs-sys-0.1 (crate (name "ibverbs-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (target "cfg(any())") (kind 0)))) (hash "0798np0ygw78agmxgm1lbys97bm6zhbarzmkb89bc34ygrq7dwvh") (links "ibverbs")))

