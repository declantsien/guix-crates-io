(define-module (crates-io ib us) #:use-module (crates-io))

(define-public crate-ibus-0.1 (crate (name "ibus") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1xncnln8q4440ddvqhc6i0p4fx0jbmf0sbi9m0gvh5j0bw74qa1r")))

(define-public crate-ibus-0.2 (crate (name "ibus") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0sj4pjrvbi49yq6f4zp20x642ns8j4x5paip2fk0wfwxhh6drgrj")))

(define-public crate-ibus-dl-0.1 (crate (name "ibus-dl") (vers "0.1.0") (deps (list (crate-dep (name "dlib") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)))) (hash "0yxp92xbkrni5h7pzj3lb2dr46y4mrfydg9qn857sjh6fk6q0wyq")))

