(define-module (crates-io ib ex) #:use-module (crates-io))

(define-public crate-ibex_alpha-0.1 (crate (name "ibex_alpha") (vers "0.1.0") (deps (list (crate-dep (name "grass") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "ibex_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ibex_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "symlink") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1i4g1z107z7ipspwvx8ay7khadklbpah0gidp1cpww7vs7hcsygp")))

(define-public crate-ibex_core-0.1 (crate (name "ibex_core") (vers "0.1.0") (hash "0z2vi0dcfcm64jhgn40zynb95brbg1vi37ykh98llhmj98has242")))

(define-public crate-ibex_macros-0.1 (crate (name "ibex_macros") (vers "0.1.0") (deps (list (crate-dep (name "ibex_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0qcqa3p6fj2ha4w9xxhlbnlfcp4a02v68znl8b0jyhv7i3qszrbs")))

