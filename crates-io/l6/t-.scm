(define-module (crates-io l6 t-) #:use-module (crates-io))

(define-public crate-l6t-file-0.1 (crate (name "l6t-file") (vers "0.1.0") (hash "1qi9l90jqzgamikz8p15yygppaz9inv4vmqz0gd7jma9sgy75kax") (rust-version "1.70")))

(define-public crate-l6t-symbolic-0.1 (crate (name "l6t-symbolic") (vers "0.1.0") (deps (list (crate-dep (name "file") (req "^0") (default-features #t) (kind 0) (package "l6t-file")))) (hash "1hkmhrbvkrp8pgk2vxiri915483w4cdrk5hknwfyq0vz1zmi7wik") (rust-version "1.70")))

