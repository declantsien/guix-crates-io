(define-module (crates-io ko mg) #:use-module (crates-io))

(define-public crate-komga-1 (crate (name "komga") (vers "1.9.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1d682hq7lsa4clm8l49jk8ixfm8zfsnnikibbsnjwc3c1ydra5m4")))

