(define-module (crates-io ko ku) #:use-module (crates-io))

(define-public crate-kokua-0.1 (crate (name "kokua") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "kokua-commands") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hai0c8flnal13mrby335yvvrxidy3cdbqd4rsb0zk44rswxlayh") (yanked #t)))

(define-public crate-kokua-commands-0.1 (crate (name "kokua-commands") (vers "0.1.0") (hash "157l9aw0fbxbk35w6drf1jzcsq7psi7yhbbbc7cglrc72d6kpxb1")))

