(define-module (crates-io ko ro) #:use-module (crates-io))

(define-public crate-koro-0.1 (crate (name "koro") (vers "0.1.0") (hash "1mzphw75m7gwq97z8sn04gfw2sck25h93r2mblyy3n9afc8yqvbl")))

(define-public crate-korome-0.9 (crate (name "korome") (vers "0.9.1") (deps (list (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yz5wjf8bkgkaf3ix665gzp776icrp16hmy2a013kxwzjkzbslr0")))

(define-public crate-korome-0.9 (crate (name "korome") (vers "0.9.2") (deps (list (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "07p40nnslv39w928lk13phw81gk20n1nwyqnj3bdi5iwwjcldsnb")))

(define-public crate-korome-0.9 (crate (name "korome") (vers "0.9.3") (deps (list (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1l0zjp612xb8c4yrbd0dcx1hiw3nkxl402bvlzy2ivas4r5b51jy")))

(define-public crate-korome-0.10 (crate (name "korome") (vers "0.10.0") (deps (list (crate-dep (name "glium") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1441k64k42id3dllqczzmm7gkbnkm1hmcpsl86sqlr825ka7a8ym")))

(define-public crate-korome-0.10 (crate (name "korome") (vers "0.10.1") (deps (list (crate-dep (name "glium") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1d9cy1vffmqbi3mjaipv88m04yckwb6iw53a5ya66b4vx25kzpz5")))

(define-public crate-korome-0.10 (crate (name "korome") (vers "0.10.2") (deps (list (crate-dep (name "glium") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13668rj3f2jj65nskccdqi4a3rkkjvy0pjzwldm0wdnwilh7wh22")))

(define-public crate-korome-0.11 (crate (name "korome") (vers "0.11.0") (deps (list (crate-dep (name "glium") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0qjnq6ljr3j968pr67gjgwiqnsbb1qx5lk0zy3k5wp2qr669pizg")))

(define-public crate-korome-0.11 (crate (name "korome") (vers "0.11.1") (deps (list (crate-dep (name "glium") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "010afdjxk94njycbsgwj9h8y5c1brpp68bszqfwhlpqi7cd1r0pn")))

(define-public crate-korome-0.12 (crate (name "korome") (vers "0.12.0") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "052arss3z89vbkp7klwjwzxx8cy38zs56xk2mjjgkqcafrm4ybhq")))

(define-public crate-korome-0.12 (crate (name "korome") (vers "0.12.1") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "1iid9sfq3afqgdmai0b3yj381apalkn5w31xjlnyxggid25c6shl")))

(define-public crate-korome-0.12 (crate (name "korome") (vers "0.12.2") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "13svqjnb0qqvb4rl9h66fl7fwryldvqc3cw9jyqsac95lzlxnsan")))

(define-public crate-korome-0.12 (crate (name "korome") (vers "0.12.3") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "0s5fpkcg3nk5b47fw41qqywplkv738qcdg2729ziy2mcd2wnz6j0")))

(define-public crate-korome-0.12 (crate (name "korome") (vers "0.12.4") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "1yx0y7s0yjbgyj5fl001i6h2sx6pkm6ss22a3ydg73mc0bzasz7w")))

(define-public crate-korome-0.13 (crate (name "korome") (vers "0.13.0") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "0awc24w9q1d2pxymwhwc9mx38d5bp7hf392xdrp82w8f847hnj2h")))

(define-public crate-korome-0.13 (crate (name "korome") (vers "0.13.1") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "02x7d0r91waanax6qipmrvjmxd3xmi1hdnzws1zvg21cax2lmmwr")))

(define-public crate-korome-0.14 (crate (name "korome") (vers "0.14.0") (deps (list (crate-dep (name "glium") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.10") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "046g72fs12fvhqn8a3cfins7152jb7clvzskldvayrg4dd6sfypm")))

(define-public crate-korome-0.14 (crate (name "korome") (vers "0.14.1") (deps (list (crate-dep (name "glium") (req ">= 0.15, < 0.17") (default-features #t) (kind 0)) (crate-dep (name "image") (req ">= 0.10, < 0.13") (features (quote ("png_codec"))) (kind 0)) (crate-dep (name "quick-error") (req "~1") (default-features #t) (kind 0)))) (hash "0r84yj1ayfpxgqad78bn3y3plg9kjbp6rpx130m679m4vm089yfg")))

(define-public crate-koron-query-parser-1 (crate (name "koron-query-parser") (vers "1.0.0") (deps (list (crate-dep (name "sqlparser") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1ypn86cfl4gzzzh8lr4crqjvn58vryblmascs2hhwparnhaiakq5")))

