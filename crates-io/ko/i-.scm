(define-module (crates-io ko i-) #:use-module (crates-io))

(define-public crate-koi-cli-0.1 (crate (name "koi-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "eventsource-client") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6") (features (quote ("console"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15xgcdnrv3lfr73sgj65fy6vrfcc4jqq70fsvl4ar3lm42h36vjw") (yanked #t)))

(define-public crate-koi-core-0.1 (crate (name "koi-core") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1f8xmzmqhcip0vispn4xx8wchkviq625l00gvfhl0vw6lb307fcp") (yanked #t)))

(define-public crate-koi-core-0.1 (crate (name "koi-core") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1pg3gmlqbqkz39pmdwjrw4054kz3i489ardfvjq0gkl20gygy9y2") (yanked #t)))

(define-public crate-koi-core-0.1 (crate (name "koi-core") (vers "0.1.0") (hash "1jr5bdj3873yvb7d98vf6m341zfdvyp1d3aha7fv970g1wfhjj4b") (yanked #t)))

(define-public crate-koi-lang-0.1 (crate (name "koi-lang") (vers "0.1.0") (hash "19fasmh7q7c0x90wyi1z1zmscl69szl3w7wzkggmv0ak022lr2kz")))

(define-public crate-koi-lang-0.1 (crate (name "koi-lang") (vers "0.1.2") (hash "0ij5ac9w63mklwnvhjn66zpq8bjzn18rgj1lb4z74v757acm397r")))

(define-public crate-koi-lang-0.1 (crate (name "koi-lang") (vers "0.1.3") (hash "1qm15dg6ghv3243v1nndfhwk2x2q6jmh798ap098jks105i36wsi")))

(define-public crate-koi-lexer-0.1 (crate (name "koi-lexer") (vers "0.1.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1030brrdv86zh7bdkxvjqa5d913mr42bhhm826gl4wyw1373y6cn")))

(define-public crate-koi-lexer-0.2 (crate (name "koi-lexer") (vers "0.2.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0nis8aicf3245rr70vqgxy3qdp6ci8kchy5pxhsk9ww0dj666ngb")))

(define-public crate-koi-lexer-0.3 (crate (name "koi-lexer") (vers "0.3.0-alpha") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0inzavimx4yh5s3wsabb8xrs9001vh3798lhc0r0jchvvzcff028")))

(define-public crate-koi-lexer-0.3 (crate (name "koi-lexer") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0nw01pg11zn0z7r8agpwinjk3d7jzsz730x02zyldcwcswmsw8ig")))

(define-public crate-koi-lexer-0.3 (crate (name "koi-lexer") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0xmpnbapwxw4m7slyhhm8hl89nkd68q8iqycnh76dp2py2jpn30j")))

(define-public crate-koi-lexer-0.3 (crate (name "koi-lexer") (vers "0.3.0-alpha.3") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0dif7ymaaibvip9c62fpjysp0zqv4skl1l09czh0pi1d9rjf4k79")))

(define-public crate-koi-lexer-0.3 (crate (name "koi-lexer") (vers "0.3.0-alpha.4") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1x4phr1wj9hb0imdfa36phj7gfa4jldm1qw4p4z0s3hanmkg6776")))

