(define-module (crates-io ko lo) #:use-module (crates-io))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "01k56kadk7psh98zwk4gbgzr0g39rr5bfadsl1iiki2rzidzlvhr") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f32") ("default" "color-matrices" "f32" "std") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0d7kxrf03j9iyabhi5q2l12g48dkkzjzsn8xijrawzbzks2h1w00") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f32") ("default" "color-matrices" "f32" "std") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.2") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "01yqkj67w0l18w044grx8flxp88s04iq1z9n6v0bj6shz74pql6v") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f32") ("default" "color-matrices" "f32" "std") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.3") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qqm0wg4aijsf91lj3bpfv0qb9acid2vw49azcdfpmj9zm73d7br") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f32") ("default" "color-matrices" "f32" "std") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.4") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "00579nw96h3gk60jpj9mvy650y5y3gxv7n1npb93pygw4ci16z52") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f32") ("default" "color-matrices" "f32" "std") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.5") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hcjc9vbf6c0q1ap2znmsxq9lfpv4i3lljs2am2zwk8mqy4wcw18") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm" "num-traits" "num-traits/libm") ("f32") ("default" "color-matrices" "f32" "std") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.6") (deps (list (crate-dep (name "glam") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "144wqsm9yvp8ccvxw3wr9j2lmcsbr3kjbhhnaachr11wn1ijwjch") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "num-traits" "num-traits/libm" "glam/libm") ("f32") ("default" "color-matrices" "f32" "std" "glam") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.7") (deps (list (crate-dep (name "glam") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0fma99qyfkk44bqm2y7c05qac7i92w2mk94b7dizxz3y5rvpf3f0") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "num-traits" "num-traits/libm" "glam/libm") ("f32") ("default" "color-matrices" "f32" "std" "glam") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.8") (deps (list (crate-dep (name "glam") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1rrdzz7f2l8ckz4kjfif86y0zcg76vh2lhwjvfhyq59p2n4m0gwk") (features (quote (("std-glam" "std" "glam/std") ("std") ("serde1" "serde" "glam/serde") ("libm-glam" "libm" "glam/libm") ("libm" "num-traits" "num-traits/libm") ("f32") ("default" "color-matrices" "f32" "std-glam") ("color-matrices"))))))

(define-public crate-kolor-0.1 (crate (name "kolor") (vers "0.1.9") (deps (list (crate-dep (name "glam") (req "^0.23") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bnrx51dihgydznlp1iqnwrzsk1013ikwpdj7asmwqdd3bmjh2xy") (features (quote (("std-glam" "std" "glam/std") ("std") ("serde1" "serde" "glam/serde") ("libm-glam" "libm" "glam/libm") ("libm" "num-traits" "num-traits/libm") ("f32") ("default" "color-matrices" "f32" "std-glam") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hcvwji3zlagi0srxxg1h2bm00jfhyd5ls9f48h1kx488255ibkf") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f64") ("default" "color-matrices" "f64" "std") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ci4x4a2sl27399npq3aqpnciqnbp0x8xy0iyq037yayk41k1rwc") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f64") ("default" "color-matrices" "f64" "std") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.2") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1j7jld4n14mymcsi0fb79k7wbpbxngfvpbvgm514y42hn6lhka91") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f64") ("default" "color-matrices" "f64" "std") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.3") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0i03nm12rsfzc495k2k85vrm3bv9qhwzkf0ysz5pynkagh4n4k2m") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f64") ("default" "color-matrices" "f64" "std") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.4") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bxqzim9zvqq1yl12gyhsy4cn47fzb8419kn97h169g1hninmsmr") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm") ("f64") ("default" "color-matrices" "f64" "std") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.5") (deps (list (crate-dep (name "glam") (req "^0.13.1") (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "157qqyxvpljzl3wzmcsjkbq2sd24v43lsmwpngj1dc4yv198hwxp") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "glam/libm" "num-traits" "num-traits/libm") ("f64") ("default" "color-matrices" "f64" "std") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.6") (deps (list (crate-dep (name "glam") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ifi545vpf43dqz816kcx25mq2xwccyccdhxjfkqmg8wwgbcq69c") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "num-traits" "num-traits/libm" "glam/libm") ("f64") ("default" "color-matrices" "f64" "std" "glam") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.7") (deps (list (crate-dep (name "glam") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0i082j8cbblm0s37lmn53d7p067nbxpjaq7qfxji90l12d6h43gp") (features (quote (("std" "glam/std") ("serde1" "serde" "glam/serde") ("libm" "num-traits" "num-traits/libm" "glam/libm") ("f64") ("default" "color-matrices" "f64" "std" "glam") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.8") (deps (list (crate-dep (name "glam") (req "^0.17.1") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0099iycy3ava0kfcv7zsbrjwm4yad5mr9ajqdsbwh0fwv8xlm25x") (features (quote (("std-glam" "std" "glam/std") ("std") ("serde1" "serde" "glam/serde") ("libm-glam" "libm" "glam/libm") ("libm" "num-traits" "num-traits/libm") ("f64") ("default" "color-matrices" "f64" "std-glam") ("color-matrices"))))))

(define-public crate-kolor-64-0.1 (crate (name "kolor-64") (vers "0.1.9") (deps (list (crate-dep (name "glam") (req "^0.23") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pgic7fk33j35pw79n8qlbfgg4xxcsb4j6pg685lrb6y1p3jizsf") (features (quote (("std-glam" "std" "glam/std") ("std") ("serde1" "serde" "glam/serde") ("libm-glam" "libm" "glam/libm") ("libm" "num-traits" "num-traits/libm") ("f64") ("default" "color-matrices" "f64" "std-glam") ("color-matrices"))))))

(define-public crate-kolorwheel-1 (crate (name "kolorwheel") (vers "1.1.0") (deps (list (crate-dep (name "all_asserts") (req "^2.3.1") (default-features #t) (kind 2)) (crate-dep (name "assert_float_eq") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "eframe") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "egui") (req "^0.23.0") (default-features #t) (kind 2)))) (hash "1za3mg198cpgg4czpxy16fk4f2n7cr53i3zd7rb78283j3d79bfk")))

(define-public crate-kolorwheel-1 (crate (name "kolorwheel") (vers "1.1.1") (deps (list (crate-dep (name "all_asserts") (req "^2.3.1") (default-features #t) (kind 2)) (crate-dep (name "assert_float_eq") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "eframe") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "egui") (req "^0.23.0") (default-features #t) (kind 2)))) (hash "1cpxci3s6bql52dmdqdk0riy8vk2pz44ijgslhnxzaay81iwmydg")))

(define-public crate-kolorz-0.1 (crate (name "kolorz") (vers "0.1.0") (hash "1wdxw2qhmb0qc5vz0gvlj1bcc69412hlz7n5zwj96kxsrw8ypbm9")))

(define-public crate-kolorz-0.2 (crate (name "kolorz") (vers "0.2.0") (hash "1m24gjphnzjwlmbxcz39k1j9h15ky1qf2350bdmgwmb35airbiqi")))

(define-public crate-kolorz-0.3 (crate (name "kolorz") (vers "0.3.0") (hash "06jjwsbbr3j20jhzx5gxdqbxn01zgf4s9l0y14hbdibawk6xc7hm")))

(define-public crate-kolorz-0.4 (crate (name "kolorz") (vers "0.4.0") (hash "1jdd90dwxs7a78fv6pfwy84m7q6fxc88aym37fsvglnh7pnnsh2y")))

(define-public crate-kolorz-0.5 (crate (name "kolorz") (vers "0.5.0") (hash "0kysf4hz1vb7g32wq88vy0wyxj1h3f6czh5xfk2vnzaicsv2qn5c")))

(define-public crate-kolorz-0.6 (crate (name "kolorz") (vers "0.6.0") (hash "0r5srjs1nksqkiv16cq4jn1x89sv82dig0701ynlxigzkvn1pmwl")))

(define-public crate-kolorz-0.6 (crate (name "kolorz") (vers "0.6.1") (hash "1k92wbkkf820af98q59glzhwmmbqq1pb9amgqbsj92lp2ifci9h9")))

(define-public crate-kolorz-0.7 (crate (name "kolorz") (vers "0.7.0") (hash "1iag22m67wkwh0vgdk2jx8r3sccfbz41wcqf0aw9rxl33m50p95c")))

(define-public crate-kolorz-0.8 (crate (name "kolorz") (vers "0.8.0") (hash "15dbjg6gh7n3pw20g2zvr4vmfwrfqbdn6h4p1dhn2masg7156gin")))

(define-public crate-kolorz-0.9 (crate (name "kolorz") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xd0rhcgba3551yarkirg09xxi20z2nx991dvbaiwp6s1pk7qpi2")))

(define-public crate-kolorz-0.10 (crate (name "kolorz") (vers "0.10.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1hmghcfpmvkdqw7j7ar4vpf5h3w32z44k7hmc0phmblajxl42n2q")))

