(define-module (crates-io ko yo) #:use-module (crates-io))

(define-public crate-koyomi-0.1 (crate (name "koyomi") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "011mqj8801qjc4y1kkgrrkfjwl2qf7ckibrm8fr48099frgsd0j1")))

(define-public crate-koyomi-0.2 (crate (name "koyomi") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0xzgjavgd54axfmfkb8dcbswjz1wsg1qybgqwsqzh981scdz85r9")))

(define-public crate-koyomi-0.3 (crate (name "koyomi") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k25gc017dlp95vklm97r8mc4qgiklzzlp18xj89va3yb47nadgn")))

(define-public crate-koyomi-0.4 (crate (name "koyomi") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "12f8svhd3irv4i5fj7npl8vz8d2q1i4g447hn10sq5syia0da03s")))

(define-public crate-koyomi-rs-0.1 (crate (name "koyomi-rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "13hjwib19fng1b0qvsy2k9gwwwlaw2jnnx5vxhydfgh2fqi5hana")))

