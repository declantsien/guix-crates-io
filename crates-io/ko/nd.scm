(define-module (crates-io ko nd) #:use-module (crates-io))

(define-public crate-kondo-0.1 (crate (name "kondo") (vers "0.1.0") (deps (list (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0jm3qvqgh2ncxzb2184zgsacqyrjfr3mbz8kf1r05cns2z3khsk1")))

(define-public crate-kondo-0.2 (crate (name "kondo") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0q7d3x6hd6ahl1xrf4y8p9msgh2nvq258s28n3pax3dx8lwpbyvf")))

(define-public crate-kondo-0.3 (crate (name "kondo") (vers "0.3.0") (deps (list (crate-dep (name "kondo-lib") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0n93v69nry0xjwb8xba50dsgwbmpy23v7x375k2zrsjdgrc8hr95")))

(define-public crate-kondo-0.4 (crate (name "kondo") (vers "0.4.0") (deps (list (crate-dep (name "kondo-lib") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "16gsyz6qx4fj1j0z2lin78iji7d76mvi3ijv5wy79aggyv92dd6f")))

(define-public crate-kondo-0.6 (crate (name "kondo") (vers "0.6.0") (deps (list (crate-dep (name "kondo-lib") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0x1q5yg62a1lxgzdh1pgmrpyzdd31kfl6gfnfs77s4f0zjj4y4gm")))

(define-public crate-kondo-0.7 (crate (name "kondo") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "kondo-lib") (req "^0.7") (default-features #t) (kind 0)))) (hash "0rh19x41llygw8svb76njjkx5qzb39mxqw9a06zvf736vb4a2j4b")))

(define-public crate-kondo-0.8 (crate (name "kondo") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "kondo-lib") (req "^0.8") (default-features #t) (kind 0)))) (hash "1f5krilx0p7wb1nfwyir9rskcdmkzcw8wr9z42r88hdvpyas14yr")))

(define-public crate-kondo-lib-0.1 (crate (name "kondo-lib") (vers "0.1.0") (deps (list (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "08190b20ycyaks3akgnm45xz3sykzcr35xjj9028wfmfl9yla4xz")))

(define-public crate-kondo-lib-0.2 (crate (name "kondo-lib") (vers "0.2.0") (deps (list (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1cyc9q7xs951djfzixc1p1gwh7l8arfw1ski6n9q07bpnslj72cq")))

(define-public crate-kondo-lib-0.6 (crate (name "kondo-lib") (vers "0.6.0") (deps (list (crate-dep (name "ignore") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0ihkp7i7pqdssjv3yclqa7b5x21vhn9j5a8gbvdxx8jhcn3y421l")))

(define-public crate-kondo-lib-0.7 (crate (name "kondo-lib") (vers "0.7.0") (deps (list (crate-dep (name "ignore") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "101yrl40p9rnk7zkvhdvlmv713iscmlp687zjsgpb3vfg3y98ksi")))

(define-public crate-kondo-lib-0.8 (crate (name "kondo-lib") (vers "0.8.0") (deps (list (crate-dep (name "ignore") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "09z22b98sympcmh9afljyzxwh5dw8n93jv000kidp6xin5p1rld8")))

(define-public crate-kondo-ui-0.1 (crate (name "kondo-ui") (vers "0.1.0") (deps (list (crate-dep (name "druid") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "kondo-lib") (req "^0.1") (default-features #t) (kind 0)))) (hash "039bfkmnacm0fpp6mkxqwn5wxdz3gw2jrvhrpf8j9i6qrkp501mk")))

(define-public crate-kondo-ui-0.2 (crate (name "kondo-ui") (vers "0.2.0") (deps (list (crate-dep (name "druid") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "kondo-lib") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mzxpa3k8c7wdds6lpjwbaaa9xjydjrhqilk5ma83crv4fcgvn3c")))

(define-public crate-kondo-ui-0.6 (crate (name "kondo-ui") (vers "0.6.0") (deps (list (crate-dep (name "druid") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "kondo-lib") (req "^0.6") (default-features #t) (kind 0)))) (hash "000vily2pm7aabskhlqh091qinq9iz2d5m492abcinw6am1dqz26")))

(define-public crate-kondo-ui-0.7 (crate (name "kondo-ui") (vers "0.7.0") (deps (list (crate-dep (name "druid") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "kondo-lib") (req "^0.7") (default-features #t) (kind 0)))) (hash "1p8b3wbkbhgwlqybjz24cx6d5qfarhx8qvfyld2357p181n1rpk2")))

(define-public crate-kondo-ui-0.8 (crate (name "kondo-ui") (vers "0.8.0") (deps (list (crate-dep (name "druid") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "kondo-lib") (req "^0.8") (default-features #t) (kind 0)))) (hash "04fzws04p1pj9cly2vb0kzh5kd10jggsgaarv6mkfl92764if0ac")))

