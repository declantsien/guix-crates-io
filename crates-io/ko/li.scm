(define-module (crates-io ko li) #:use-module (crates-io))

(define-public crate-kolibri-embedded-gui-0.0.0 (crate (name "kolibri-embedded-gui") (vers "0.0.0-alpha.1") (deps (list (crate-dep (name "embedded-graphics") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics-simulator") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-iconoir") (req "^0.2.2") (features (quote ("all-resolutions"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (features (quote ("serde" "ufmt-impl"))) (default-features #t) (kind 0)))) (hash "0xxyndwnjx23xq15jl3p1xv9zwbk6x00b5hin6d6qnrqndh7lsid")))

