(define-module (crates-io ko pe) #:use-module (crates-io))

(define-public crate-koper-0.1 (crate (name "koper") (vers "0.1.0") (hash "1jc42x8yx6dc9vvkk0v7xrfki10z32b62kq4998j3vrlaayhvwv3")))

(define-public crate-koper-0.2 (crate (name "koper") (vers "0.2.0") (hash "0wdwp8hfvkns489fmwx3bnz1bwij0z36h6jms8v0kcla7qmmnv7k")))

(define-public crate-koper-0.3 (crate (name "koper") (vers "0.3.0") (hash "1mk3zfrh8a8ykxkdj1b41jirfr30hn3bjxcvb2zpg6waf029qcq6")))

