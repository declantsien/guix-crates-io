(define-module (crates-io ko ok) #:use-module (crates-io))

(define-public crate-kook-0.0.0 (crate (name "kook") (vers "0.0.0") (hash "05dgzj5jy61lnlcl5y2xbpc343jlqc6xz47xgc70vgil3acgm2dz")))

(define-public crate-kooka_lib_messenger-0.0.1 (crate (name "kooka_lib_messenger") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "08kg1parnblc3wy2xabjpak7cq1az9yi3k9rv97wx5h8a7j9qxq4")))

(define-public crate-kooka_lib_messenger-0.0.2 (crate (name "kooka_lib_messenger") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1wlrasjgdd1w4ysq380agk2d5897ns3imq107g4d4kn7amsqzwwx")))

(define-public crate-kooka_lib_messenger-0.0.3 (crate (name "kooka_lib_messenger") (vers "0.0.3") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1dv5ia8hnylx0g09dqnb2ixmj60jxlr3bsza8c3wijm2d0nsdhjs")))

(define-public crate-kooka_lib_messenger-0.0.4 (crate (name "kooka_lib_messenger") (vers "0.0.4") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "16z95498m63837x32jnzf4a0jq9ixzhf4j4pqj1wplxi1saihr7j")))

(define-public crate-kooka_lib_messenger-0.0.5 (crate (name "kooka_lib_messenger") (vers "0.0.5") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "089zz3mz8wm3d2kw41s24ds0h76mkmnidvmkx4firv68b5srz80z")))

(define-public crate-kooka_lib_messenger-0.0.6 (crate (name "kooka_lib_messenger") (vers "0.0.6") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0flv4kr17ikzl0wjxnbgskk4vl099aahx2nhi5v2m8pblbynh0zs")))

(define-public crate-kooka_lib_messenger-0.0.7 (crate (name "kooka_lib_messenger") (vers "0.0.7") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1i5dwas8myrz9nriwf5z1w7w6vcw7w7x4x3q75k0bq8yd9i4glkh")))

(define-public crate-kooka_lib_messenger-0.0.8 (crate (name "kooka_lib_messenger") (vers "0.0.8") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1xk0b93qcm28mkxr44wipjnhqsnl5lqx27ys26iknrdkx2xjpm09")))

(define-public crate-kooka_lib_messenger-0.0.9 (crate (name "kooka_lib_messenger") (vers "0.0.9") (deps (list (crate-dep (name "env_logger") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1f8vr8rbag70pn29hrlc3yv3wvi4k9jask4m5k7ims32ibss85q6")))

(define-public crate-kooka_lib_messenger-0.1 (crate (name "kooka_lib_messenger") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1sm6zvs8yf2cryqfnnv61hw2ybq19656h718fy8inayal3xgg57h")))

(define-public crate-kooka_lib_messenger-0.1 (crate (name "kooka_lib_messenger") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0hmzl1p3m06ng1f9b2l5r7g6lmssagwm765d79wb9dmksm06gy17")))

(define-public crate-kooka_lib_messenger-0.1 (crate (name "kooka_lib_messenger") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1spbwij5blzwzl0xkjghkisygbfv1cg4h4igjmn3x4bnb70ggxsv")))

