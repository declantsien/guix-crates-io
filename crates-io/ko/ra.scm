(define-module (crates-io ko ra) #:use-module (crates-io))

(define-public crate-korat-0.1 (crate (name "korat") (vers "0.1.0") (deps (list (crate-dep (name "quick-error") (req ">= 1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_dynamodb") (req ">= 0.25.0") (default-features #t) (kind 0)))) (hash "1pqkxry6vbvdwab8akdb35bkqmnpsjhlqi9xliwn62hap4vaniy6")))

(define-public crate-korat-0.1 (crate (name "korat") (vers "0.1.1") (deps (list (crate-dep (name "quick-error") (req ">= 1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_dynamodb") (req ">= 0.25.0") (default-features #t) (kind 0)))) (hash "05wqp0nh2qsvpkl5ak6afzx8nxinl7z0glyzyzznraymgrw3fynx")))

(define-public crate-korat-0.1 (crate (name "korat") (vers "0.1.2") (deps (list (crate-dep (name "quick-error") (req ">= 1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_dynamodb") (req ">= 0.25.0") (default-features #t) (kind 0)))) (hash "1mxqnp9b5rmc8wz1zvi1193vapcgm0lmjdxjvr62kbxcs0dn6zdq")))

(define-public crate-korat-0.1 (crate (name "korat") (vers "0.1.3") (deps (list (crate-dep (name "quick-error") (req ">= 1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_dynamodb") (req "^0") (default-features #t) (kind 0)))) (hash "0ckqck3nn5dhj9b6w47fdirflxln1zvjzd8ckbrjhp7hfzrv5x74")))

(define-public crate-korat-0.2 (crate (name "korat") (vers "0.2.0") (deps (list (crate-dep (name "quick-error") (req ">= 1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_dynamodb") (req "^0") (default-features #t) (kind 0)))) (hash "1rf42dv4zandlidi232wiip787qi94af9lsy2vg17g3h5xd29g50")))

(define-public crate-korat_derive-0.1 (crate (name "korat_derive") (vers "0.1.0") (deps (list (crate-dep (name "korat") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req ">= 0.3.15") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req ">= 0.25.0") (default-features #t) (kind 2)) (crate-dep (name "rusoto_dynamodb") (req ">= 0.25.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">= 0.11.11") (default-features #t) (kind 0)))) (hash "1v66xvlsa3zk29zbw0qmf49j4wjp39bmrhvykf7iincp9xbil3n2")))

(define-public crate-korat_derive-0.1 (crate (name "korat_derive") (vers "0.1.1") (deps (list (crate-dep (name "korat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req ">= 0.3.15") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req ">= 0.25.0") (default-features #t) (kind 2)) (crate-dep (name "rusoto_dynamodb") (req ">= 0.25.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">= 0.11.11") (default-features #t) (kind 0)))) (hash "1bb1jlh47x3z79b01f6rgh77lhp4z2pzhfgzwz914vy2pf6lv92d")))

(define-public crate-korat_derive-0.1 (crate (name "korat_derive") (vers "0.1.2") (deps (list (crate-dep (name "korat") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req ">= 0.3.15") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req ">= 0.25.0") (default-features #t) (kind 2)) (crate-dep (name "rusoto_dynamodb") (req ">= 0.25.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">= 0.11.11") (default-features #t) (kind 0)))) (hash "111zp8yfvi9wm367b7wmcvn7kajjjbayqqv8kp4b77qgav753vqx")))

(define-public crate-korat_derive-0.1 (crate (name "korat_derive") (vers "0.1.3") (deps (list (crate-dep (name "korat") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req ">= 0.3.15") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rusoto_dynamodb") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">= 0.11.11") (default-features #t) (kind 0)))) (hash "0364ij4wmkpjnw24hmb65q4xsppw859388wf498xm5rgiv7ks9rg")))

