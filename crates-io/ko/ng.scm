(define-module (crates-io ko ng) #:use-module (crates-io))

(define-public crate-kong-0.1 (crate (name "kong") (vers "0.1.0") (hash "19ghmppmgyija16xcxm76ni7djxk6gqnpyfmy59ps78jwn7y191i")))

(define-public crate-kong-log-server-0.1 (crate (name "kong-log-server") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ywkzji4w10vzr1zacysynq2rg565r1qyc8agddjykxl5nhwwzqc")))

