(define-module (crates-io ko bj) #:use-module (crates-io))

(define-public crate-kobject-uevent-0.1 (crate (name "kobject-uevent") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "netlink-sys") (req "^0.7") (default-features #t) (kind 2)))) (hash "038n4c06pk9v1y4xfp005887snf2vsha7ns8aw1m3gky3hxfr0n1")))

(define-public crate-kobject-uevent-0.1 (crate (name "kobject-uevent") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1aqwnsqx3p28v3p7ykqyzqd4sn9hc52amy3jb55xranrg4lynlh5")))

