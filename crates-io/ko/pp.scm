(define-module (crates-io ko pp) #:use-module (crates-io))

(define-public crate-kopperdb-0.1 (crate (name "kopperdb") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0g6qk4gxjbww9d1b7waklsxl0p70h9whx2rpfl9rqqrr6lzc1sp8")))

