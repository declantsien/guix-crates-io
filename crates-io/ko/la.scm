(define-module (crates-io ko la) #:use-module (crates-io))

(define-public crate-kola-0.1 (crate (name "kola") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "ropey") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt-multi-thread" "macros" "io-std"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.22.6") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-kotlin") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "1rbwkw1br6h5p4zvw7mhwa6afxm5i4da18b1f3qmriq659scczjf")))

(define-public crate-kolakoski-0.1 (crate (name "kolakoski") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1vhhr7scxi949wc951i6az236b8j5vswz663957ds5kl45w9ggr9") (features (quote (("num-traits" "num") ("default"))))))

(define-public crate-kolakoski-1 (crate (name "kolakoski") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0xb4q1sj8xj1dzydsdyy4v9si4flaq38mlkp00ymlgkj7arj52ym") (features (quote (("num-traits" "num") ("default"))))))

(define-public crate-kolakoski-1 (crate (name "kolakoski") (vers "1.0.1") (deps (list (crate-dep (name "num") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0q96csdhgw2r6shqsh9gb9mjp5c2vgqzjxcn31gmxim44gmdhc5j") (features (quote (("num-traits" "num") ("default"))))))

(define-public crate-kolakoski-2 (crate (name "kolakoski") (vers "2.0.0") (deps (list (crate-dep (name "num") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0vglyyp5jnyqx1759b01pyb4flhw38x3a67f5i4ncnw7a3lz7qn6") (features (quote (("num-traits" "num") ("default"))))))

(define-public crate-kolas-0.1 (crate (name "kolas") (vers "0.1.0") (hash "1dq147478dfs8r8d1148vz4slr2x3q11536faz05gh3nhym37d87")))

