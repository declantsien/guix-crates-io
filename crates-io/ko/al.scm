(define-module (crates-io ko al) #:use-module (crates-io))

(define-public crate-koala-0.1 (crate (name "koala") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0b8cw20x0jirmzbbkq6xs1xj4266swlbqa0mq4z4yv8bdj6vnn8h")))

(define-public crate-koala-0.1 (crate (name "koala") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0lx4gkjjgk39s4qay5pwp1yrm5ywdihp60g9shaqjghrzynhbrkp")))

(define-public crate-koala-0.1 (crate (name "koala") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "04pm7mij5bp2kyqwl92smg57g3zxplvsk0zbmnbxk31s3wmcw40w")))

(define-public crate-koala-0.1 (crate (name "koala") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1a4k5jy9j1q0xil73w9l10gibwcgww6d89pad19hkad0pyjn0vkp")))

(define-public crate-koala-0.1 (crate (name "koala") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1lacnkwfavrg8si6k9cjijz8ch8xbj6pp7j69h78v6bfl65myick")))

(define-public crate-koala-0.1 (crate (name "koala") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0z9k9x44vy8ipk4mb6hcnw7p58y56av16gk47fffr8jrj55sd26i")))

