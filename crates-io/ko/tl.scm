(define-module (crates-io ko tl) #:use-module (crates-io))

(define-public crate-kotlike-0.1 (crate (name "kotlike") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("fold" "full"))) (default-features #t) (kind 0)))) (hash "1fx20n1vx393p3j0f77cn1k52l04v3zjm10n3wd0l1mc0xm5kaan") (rust-version "1.70.0")))

(define-public crate-kotlin-bridge-0.1 (crate (name "kotlin-bridge") (vers "0.1.0") (hash "1fbryiwl1l3w36b37djbaf20y569x1w26df9qjdxjdypr20bnlkc")))

(define-public crate-kotlin-bridge-build-0.1 (crate (name "kotlin-bridge-build") (vers "0.1.0") (hash "1847w5p363nnhybyzrx39x70rr9b26fndm21w9kvayj03n9ccbz8")))

(define-public crate-kotlin-bridge-cli-0.1 (crate (name "kotlin-bridge-cli") (vers "0.1.0") (hash "1m54yw0v776hmp20fwaxvvjkcb9z974rlqf4ggfld1vl5jdyz41h")))

(define-public crate-kotlin-bridge-ir-0.1 (crate (name "kotlin-bridge-ir") (vers "0.1.0") (hash "0wxbwfn3mnqf5ncv5ibjyv4c4wvw47b39xac4lwmjxmx8c9vwjvp")))

(define-public crate-kotlin-bridge-macro-0.1 (crate (name "kotlin-bridge-macro") (vers "0.1.0") (hash "143nc0gm8h4zff1d7d4z7f0bik8v1cyppk1b36mba62kbnlkm1dd")))

