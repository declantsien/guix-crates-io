(define-module (crates-io ko rr) #:use-module (crates-io))

(define-public crate-korrektor-0.1 (crate (name "korrektor") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0l183qi5dv3lv53fbwm43l5yixmb7wa8vxbpkr34kyli5kj72kda")))

(define-public crate-korrektor-0.2 (crate (name "korrektor") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "19fvsnxkd37lxcl2r7bi7zdk87q27h2gvp8dhxxi50yd3v0qav8w")))

(define-public crate-korrektor-0.2 (crate (name "korrektor") (vers "0.2.2") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0dmrbl04jbfm9jag0lwx3a0k25aldyzssk2864ijncx74k312d6g")))

(define-public crate-korrektor-0.2 (crate (name "korrektor") (vers "0.2.3") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1lvhzy9wdk3dwc80y0j3xy9fbfly77gqp838wl8fjf6583ywwbg1")))

(define-public crate-korrektor-0.2 (crate (name "korrektor") (vers "0.2.4") (deps (list (crate-dep (name "fancy-regex") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "korrektor-utils") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "041l554676iflizfdcrjmfmnjmr188na2r2l75xnk2i590bl2f7i") (yanked #t)))

(define-public crate-korrektor-0.3 (crate (name "korrektor") (vers "0.3.0") (deps (list (crate-dep (name "fancy-regex") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "korrektor-utils") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03158md8ah9fkzwkwnmrs24jixfcaykv4pwbi77a8xs3zxvap382")))

(define-public crate-korrektor-0.3 (crate (name "korrektor") (vers "0.3.1") (deps (list (crate-dep (name "fancy-regex") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "korrektor-utils") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10r54m47jx13m5dmxf53h14d533s382xcl5ii0nigzh4rg9yrqdk")))

(define-public crate-korrektor-utils-0.1 (crate (name "korrektor-utils") (vers "0.1.0") (deps (list (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "11kafrq5n3n8bvxl06nkraqnm2hb0wp9z6g1qabyghff4c36ds2y")))

(define-public crate-korrektor-utils-0.1 (crate (name "korrektor-utils") (vers "0.1.1") (deps (list (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "00s0bn9l7h9b7f7kx2c4aq5kadclzr5pw11y3xgs15269xh0p27r")))

(define-public crate-korrektor-utils-0.1 (crate (name "korrektor-utils") (vers "0.1.2") (deps (list (crate-dep (name "pcre") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1z1f3s5naisxm15qk0g45m29rk77c61fv05xg9d6xi0m2s6kkjvp")))

