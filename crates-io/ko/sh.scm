(define-module (crates-io ko sh) #:use-module (crates-io))

(define-public crate-kosh-0.1 (crate (name "kosh") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.26") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "17rxcgr80y6205sfz1aka7q42xki9rdngn0hi9hhcvnbvy99llbc")))

(define-public crate-kosh-0.1 (crate (name "kosh") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.26") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows") (req "^0.46.0") (features (quote ("Win32_System_Console"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0fzrdpwdx5kqx200a4knb499wfwnb2rsw2xajc56q24fp0fzf8gn")))

