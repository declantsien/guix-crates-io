(define-module (crates-io ko nj) #:use-module (crates-io))

(define-public crate-konj-0.1 (crate (name "konj") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0xzykrc19dfz3nbgdv7f1py5d0fjvwghm087w46sa3l9c70f531j")))

