(define-module (crates-io ko ca) #:use-module (crates-io))

(define-public crate-koca-0.1 (crate (name "koca") (vers "0.1.0") (hash "0dw79bj6rwsfbdnnqbzf1fih21p3zm8bm3ldbx0mk2s740993gah")))

(define-public crate-koca-fmt-0.1 (crate (name "koca-fmt") (vers "0.1.0") (hash "120x996f8sxca74cgiqa8lfai7j05anliyqladjsq2p2578d1884")))

(define-public crate-koca-lsp-0.1 (crate (name "koca-lsp") (vers "0.1.0") (hash "1pwb34gkbcdmxak5g6dakiaa0ryhdqcjmz2mxiblami5plz2ajnw")))

(define-public crate-koca-rs-0.1 (crate (name "koca-rs") (vers "0.1.0") (hash "10hbzygwsjc6z3zxi8l62n6bkrqdcbk3iij6wmgblin9ccc5i8y0")))

