(define-module (crates-io ko nt) #:use-module (crates-io))

(define-public crate-kontex-0.1 (crate (name "kontex") (vers "0.1.0") (hash "0f3fihxr3qr3msf7x2qj24w573kq3m89csfnmn6m6dgv42a3spib")))

(define-public crate-kontex-0.2 (crate (name "kontex") (vers "0.2.0") (hash "1358bh7zcp1y2a026gmm8sp2vzg6q9gw4p7sp9wzs0l7jfz4jlk5")))

(define-public crate-kontrak-sqlite-0.1 (crate (name "kontrak-sqlite") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sqlite3-sys") (req "^0.12") (kind 0)) (crate-dep (name "temporary") (req "^0.6") (default-features #t) (kind 2)))) (hash "00ynm22w8kgpyrwav3mihlwjxpy87z28924y44npfyhin5psp6j1") (features (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-kontrak-sqlite-0.1 (crate (name "kontrak-sqlite") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sqlite3-sys") (req "^0.12") (kind 0)) (crate-dep (name "temporary") (req "^0.6") (default-features #t) (kind 2)))) (hash "15xbyw2iwr4hqv0k6kiacql6svjl467rdvdg727dwmvb0fjzxpdf") (features (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-kontrak-sqlite-0.1 (crate (name "kontrak-sqlite") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sqlite3-sys") (req "^0.12") (kind 0)) (crate-dep (name "temporary") (req "^0.6") (default-features #t) (kind 2)))) (hash "01k2gp1sp1nrshvam5a074an83gq3fhvr3yf3rqvl8yd5aiv6axl") (features (quote (("linkage" "sqlite3-sys/linkage") ("default" "linkage"))))))

(define-public crate-kontrol2-0.0.0 (crate (name "kontrol2") (vers "0.0.0") (hash "18f6kh5arih2gfgvrsc2lkmh1q5ydb6lx53z6dq8nxizx73x955s")))

(define-public crate-kontroli-0.1 (crate (name "kontroli") (vers "0.1.0") (deps (list (crate-dep (name "colosseum") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^14.3") (default-features #t) (kind 0)) (crate-dep (name "lazy-st") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nested-modules") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "0zn0l1vyzdi3rbr9rczvb7nz16ln54cwlpddhaa9558zh4qzcpxd")))

(define-public crate-kontroli-0.1 (crate (name "kontroli") (vers "0.1.1") (deps (list (crate-dep (name "colosseum") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^14.3") (default-features #t) (kind 0)) (crate-dep (name "lazy-st") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nested-modules") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "0vhsfs10hrhkl8c0spxxsmgpny1k4sy5s40qyf7kc9mz7yvg9gwp")))

(define-public crate-kontroli-0.2 (crate (name "kontroli") (vers "0.2.0") (deps (list (crate-dep (name "colosseum") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "dedukti-parse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^14.3") (default-features #t) (kind 0)) (crate-dep (name "lazy-st") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nested-modules") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f2krky73ai387zxc95isp227czzs8wp33gixlcy6vjirm7yiixk")))

(define-public crate-kontroli-0.3 (crate (name "kontroli") (vers "0.3.0") (deps (list (crate-dep (name "colosseum") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "dedukti-parse") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^14.3") (default-features #t) (kind 0)) (crate-dep (name "lazy-st") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nested-modules") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zvqxza1h372iy0ws2a74nvk7cp6ls7ggjv56k39y4wa6wlp2y8g")))

(define-public crate-kontroli-0.4 (crate (name "kontroli") (vers "0.4.0") (deps (list (crate-dep (name "colosseum") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "dedukti-parse") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy-st") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nested-modules") (req "^0.2") (default-features #t) (kind 0)))) (hash "00066abidliad6yqaggs4mksghwnq5m1y22nnwsyilmc3ysfjyqj")))

(define-public crate-kontrolluppgift-0.1 (crate (name "kontrolluppgift") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "19ii044dydpjc3c3pprikzx4q9j063f3pfa6f8kc4vk85sxg7iz6")))

(define-public crate-kontrolluppgift-0.2 (crate (name "kontrolluppgift") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "003m8fmbwlppf47vsk29c8v87c3fgc7z1lla708gvq4rvmrd75rz")))

(define-public crate-kontrolluppgift-0.3 (crate (name "kontrolluppgift") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "0z46jq1iafrv1hh83ywddar4yfh92qzqmk8bicd4f3vdf2d6360z")))

(define-public crate-kontrolluppgift-0.4 (crate (name "kontrolluppgift") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "128czd77w6r6m3w2w4pkg0dla926dhisf1vl6d2md5r1kcl0kaw2")))

(define-public crate-kontrolluppgift-0.5 (crate (name "kontrolluppgift") (vers "0.5.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16rfkvcw7b8gv76dxp2v3d57mz3l9pzx25nj09in0bs042a6gw48")))

(define-public crate-kontrolluppgift-0.6 (crate (name "kontrolluppgift") (vers "0.6.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zz9v7absjgqzng6x274lacyv5wi2fa3s3n426n5cdq3n8m7napz")))

(define-public crate-kontrolluppgift-0.7 (crate (name "kontrolluppgift") (vers "0.7.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16l0fh6xs52p0fchvck5b1fz6lm4c39yqmqm67i9i49jzyhfv4rk")))

(define-public crate-kontrolluppgift-0.8 (crate (name "kontrolluppgift") (vers "0.8.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "1mn101kd1z5q6hvz6jh9dgq4lhrwhvd4gjf9afwjfcfi23vzng4i")))

(define-public crate-kontrolluppgift-0.9 (crate (name "kontrolluppgift") (vers "0.9.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "03qp4dkx3d663p9cirnyg5a48iigwrh900y4kj7bzjzdv71vpymm")))

(define-public crate-kontrolluppgift-0.9 (crate (name "kontrolluppgift") (vers "0.9.1") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "17xgq472awag506kjixh4qw55im2k12ai3xhg84gd6n6lavci309")))

(define-public crate-kontrolluppgift-0.10 (crate (name "kontrolluppgift") (vers "0.10.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "05knbil2y757rwf1npl82h6298da3wpn6pmccn0zpsfqc1x9m6b8")))

(define-public crate-kontrolluppgift-0.11 (crate (name "kontrolluppgift") (vers "0.11.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "045gna1mqyj9ihhlfd5arrp96fj793grrz2027gi2q77hs3k92pl")))

(define-public crate-kontrolluppgift-0.12 (crate (name "kontrolluppgift") (vers "0.12.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "073wd2ihc3jq8rb3rp9abc6994v89bjdh20azq3ckg562pb9wbgw")))

(define-public crate-kontrolluppgift-0.13 (crate (name "kontrolluppgift") (vers "0.13.0") (deps (list (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "0n159b1s355pw3k741apwzh28r7r78qxxwvlxn9fqlsjqnbilhnh")))

(define-public crate-kontrolluppgift-0.13 (crate (name "kontrolluppgift") (vers "0.13.1") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "12q4wyxd509swas55rz2wnvxmkdl4g4ya2vwv7flzj2m5i55kn77")))

(define-public crate-kontrolluppgift-0.13 (crate (name "kontrolluppgift") (vers "0.13.2") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "13gyhwsrvf74lprkgykgn1zfzhnbf2vrf0p1mz9qzjcfj0w0k1zz")))

(define-public crate-kontrolluppgift-0.14 (crate (name "kontrolluppgift") (vers "0.14.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "1mbkncnjz5f2l8zyy0km3xll6f8ciy0ijqdzrxlj4lrzhiahidig")))

(define-public crate-kontrolluppgift-0.15 (crate (name "kontrolluppgift") (vers "0.15.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "1axhfk0ncab9d3smdc7zmqg8v55g43lk10ank3isk5qvrqf6fc1w")))

(define-public crate-kontrolluppgift-0.16 (crate (name "kontrolluppgift") (vers "0.16.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "0gbqf7xk6wh97dhf0fln58if4fdwh8yccr5ykji54gpddmcn68l1")))

(define-public crate-kontrolluppgift-0.17 (crate (name "kontrolluppgift") (vers "0.17.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "0cwqzppmw6hiw09ifqz395qgnd8dxz9xrq8z0jg21dgkwd80ay8v")))

(define-public crate-kontrolluppgift-0.18 (crate (name "kontrolluppgift") (vers "0.18.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "126pvvx9a0z6jcg9l7mh6y0cyd9jx5mpqkfrkzqygw247hyslnq7")))

(define-public crate-kontrolluppgift-0.19 (crate (name "kontrolluppgift") (vers "0.19.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "00pz8hpawf07riinw6qs7dcjxiwv6qwcil09vp5v4my3jnm928ch")))

(define-public crate-kontrolluppgift-0.20 (crate (name "kontrolluppgift") (vers "0.20.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.25") (features (quote ("parsing" "formatting"))) (default-features #t) (kind 0)))) (hash "0w0vwmdga7fw1zvpjgq9vsvvrhrs6rbfhxqbcgfr7jc98i22h6yv")))

(define-public crate-kontrolluppgift-0.21 (crate (name "kontrolluppgift") (vers "0.21.0") (deps (list (crate-dep (name "kontrolluppgift_macros") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.25") (features (quote ("parsing" "formatting"))) (default-features #t) (kind 0)))) (hash "1myac2v1n3m3n9brf0kr0l9xydjgalqqa4r3waajh89967wfk231")))

(define-public crate-kontrolluppgift_macros-0.1 (crate (name "kontrolluppgift_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1g6g4kxz2biavd8d51g1xvmxip3xjg5c4fzxyn7zim7clj1pzwhf")))

(define-public crate-kontrolluppgift_macros-0.2 (crate (name "kontrolluppgift_macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "02f9p95q7sg05y35rnf2lr5x4jr8q3km3xbjz4ks49bq5f6b2lxl")))

(define-public crate-kontrolluppgift_macros-0.3 (crate (name "kontrolluppgift_macros") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "12l2vq8nxrngvkgzbdzqr65affqvca2gc3yca258bzix2cl0n921")))

(define-public crate-kontrolluppgift_macros-0.4 (crate (name "kontrolluppgift_macros") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1mx7468hd4vhbxqxggq4iscfq5v5g8k6sgpr540ihnq8i54z8dah")))

