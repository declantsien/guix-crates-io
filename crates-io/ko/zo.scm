(define-module (crates-io ko zo) #:use-module (crates-io))

(define-public crate-kozo-0.1 (crate (name "kozo") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02wb9vj26jxnxgyx8qsh4lwjym5qrjl49hbzv4hpkc2r0rlb8vxs")))

