(define-module (crates-io ko ni) #:use-module (crates-io))

(define-public crate-koni-0.1 (crate (name "koni") (vers "0.1.0") (hash "1x9nxr4q7hqm33w52fsh59sq6hx8v0gp8lnc4y1f0s9nw2qr2qy8")))

(define-public crate-konig-0.1 (crate (name "konig") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0ra7l60bz2z18kyn4bhb2iqrfpj5x04wynsp45si6hf9v8z0wzhd")))

(define-public crate-konig-0.1 (crate (name "konig") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0a830h2q5ay6d3d5c62a1vklvrcmlfa8xqvxpz2zdr8hg7c4741q")))

(define-public crate-konig-0.1 (crate (name "konig") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nonmax") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0i8zcizvjkbbyxk2vy2zpx3ypd7xl3d6lfncq4d0j9ki89z1hbhm")))

(define-public crate-konig-0.1 (crate (name "konig") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nonmax") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1yhr4257hh47zh9r9vzcq4b869izw9js6lprvsmvr0b4bwzqb22j")))

(define-public crate-konig-0.1 (crate (name "konig") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nonmax") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "12dslm4vqy1cs2mddcvqrq0vqimhwaiddrmlv2sa5jplbmw46vk6")))

(define-public crate-konig-0.1 (crate (name "konig") (vers "0.1.5") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nonmax") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0ry3438aziqqipksvn6hl7fqdzkwfn4zd1xf2dr72xf5n8dwh2jr")))

(define-public crate-konisto-0.1 (crate (name "konisto") (vers "0.1.0") (deps (list (crate-dep (name "notify") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "notify-debouncer-mini") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "06qqza4i6gdwycaymgmp2lpdhkgzmnmg3mg3aa49j9hm91gpbj1k")))

