(define-module (crates-io ko mb) #:use-module (crates-io))

(define-public crate-kombi-0.1 (crate (name "kombi") (vers "0.1.0") (hash "007cjw19g00c082iq08cb4csni0jf8695f5bf6csq9r7pk183lpa")))

(define-public crate-kombi-0.1 (crate (name "kombi") (vers "0.1.1") (hash "0zlxzs1cp6a4iq90n1p1dwfqy131fyl4n28vsylji3dim42a64zx")))

(define-public crate-kombi-0.1 (crate (name "kombi") (vers "0.1.2") (hash "0iymxb0v4ix6zyvmwc7i92lnkpdw94z1zdnjdiq7ncv5vqwndp4q")))

(define-public crate-kombi-0.1 (crate (name "kombi") (vers "0.1.3") (hash "1fzq2kw8sgjj61r3fwr1z9f3ydmijixdpjy74fqvipxiyg5wyyik")))

(define-public crate-kombi-0.2 (crate (name "kombi") (vers "0.2.0") (hash "0sfn07b9j6mrivzdyqasjw2vv49mlv2xcm00vmr9kmzdqjq9203v")))

(define-public crate-kombi-0.2 (crate (name "kombi") (vers "0.2.1") (hash "129wbs7k42r52vg1n65z057mqi9wbz5559czvmniivc7d58ygkj9")))

(define-public crate-kombi-0.2 (crate (name "kombi") (vers "0.2.2") (hash "0vzc088hdj3imdyz6ag91gwz3f7x21rcnar4nbba10bq1f4545bq")))

