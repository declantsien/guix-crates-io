(define-module (crates-io ko lm) #:use-module (crates-io))

(define-public crate-kolmogorov_smirnov-0.1 (crate (name "kolmogorov_smirnov") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "0kdpn3g7r65q53pkis9ljvy0fxhbdx67306wi71wybr9281rcrm5")))

(define-public crate-kolmogorov_smirnov-1 (crate (name "kolmogorov_smirnov") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "0rnrxf4z12gbjpyj6di3vj0qapjjng2crid393bv6rhaix7acbf6")))

(define-public crate-kolmogorov_smirnov-1 (crate (name "kolmogorov_smirnov") (vers "1.0.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "0fpl2bfrcbd30k7pjigg0q5pdhan2vm4pmn6s4wkvw9m96ryjgz7")))

(define-public crate-kolmogorov_smirnov-1 (crate (name "kolmogorov_smirnov") (vers "1.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "1darpj63crzw9yvg651v70yd4lcxi8nj6ffpqjswxlzz62vavkln")))

