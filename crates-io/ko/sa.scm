(define-module (crates-io ko sa) #:use-module (crates-io))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "165fgq1262v7isf5hjvpm7bg8nrvxmcdvgpqng5fwylq3dsiwhfa")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1qlk29sz5dp2v7wav7cfgdqx1fw9laxqi15ybsgvh76kiks0596p")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.0.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "0myagmmw9bsvhl9gzv3j7ywribv0j7b9g3c2y0bh9b3194yv14l9")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.0.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "0wp10bq6cw60h4i7v78308n7xbs2wbry11hz970hr02s91v4bahp")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1a5lj8as8lxx9wbwwdmz03cnrm9gbn2y0mxhfs19ylakcbq64lmp")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "15szhahccd4xg97m8bnh0f2wkmwmrmah3gjb5s3wkcbw7wk24c0z")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1jlb5h4a22rwh3a5nib3h5p23ihfq3w6nlk1544zjci3ql5ip7bd")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "182bn243w4pbcj1rnq37idckpxhmgh0y5jr6pnij8lgnlk56pfb5")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)) (crate-dep (name "string-join") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1jmvza64ba78rnc1ngzv9l61l58bif583fkhg8vha9ay5jg2y588")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)) (crate-dep (name "string-join") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1n2bgq2ycgz8cvg4vc0cwp0yddnh96wpw32svhwhvhbmfb1irqc3")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.6") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)) (crate-dep (name "string-join") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "07f3js27a0q7xjbfp7ni77wnnq7ax69z2dgw7kf63hwznn94ldj9")))

(define-public crate-kosalt-3 (crate (name "kosalt") (vers "3.1.7") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)) (crate-dep (name "string-join") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1zx34qbygr39rjaa63jamvsjxbf0v5kchcr5kvzl2svmm4iz6hkc")))

