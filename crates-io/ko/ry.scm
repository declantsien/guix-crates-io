(define-module (crates-io ko ry) #:use-module (crates-io))

(define-public crate-koryto-0.0.1 (crate (name "koryto") (vers "0.0.1") (hash "03k2ib1bbmmr0hqz19ga4x3p8f8lz9x3qrm13xhqx9h7klaqgx0z")))

(define-public crate-koryto-0.0.2 (crate (name "koryto") (vers "0.0.2") (hash "1pbrblcy79m2yk2f5lhg38hrm9sv48wxvzwapnissl1c86w1bdlz")))

(define-public crate-koryto-0.1 (crate (name "koryto") (vers "0.1.0") (hash "1iwcma39mxwgfk80w55nramigbka82vlg4481kmzkdjfnfwk2629")))

(define-public crate-koryto-0.1 (crate (name "koryto") (vers "0.1.1") (deps (list (crate-dep (name "thunderdome") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "07rs4039ikca7zxfnbdsy386lnm8kkzcdlpwxm4vn2wjs4vymfhy")))

