(define-module (crates-io ko mi) #:use-module (crates-io))

(define-public crate-komi-hash-0.1 (crate (name "komi-hash") (vers "0.1.0") (hash "0dl06minkpl4znlxi3cr03xz5hmfg2rf6n2zdgxb80fl3gy8k46h") (yanked #t)))

(define-public crate-komi-hash-0.1 (crate (name "komi-hash") (vers "0.1.1") (hash "0ajzssfgvq4d0wh9q9bk96ml45s2rpafv1p05xy4qyidp5ifxsq9") (yanked #t)))

(define-public crate-komi-hash-0.1 (crate (name "komi-hash") (vers "0.1.2") (hash "1mlypyfriz15xxspxjvy527kjs80k2niniwzz1yw82rxx0kcrzq2") (yanked #t)))

(define-public crate-komi-hash-0.1 (crate (name "komi-hash") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "pcg") (req "^4.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "14j8f4yqdqqw7w842if4pz25jb2pfrz2phhk643cxaix6jgm8alx") (yanked #t)))

(define-public crate-komi-hash-0.1 (crate (name "komi-hash") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "pcg") (req "^4.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0ynim6sb9v8whwl965p5h1m9flm5cqfayrw9fj3s0dvgympq9fz3") (yanked #t)))

(define-public crate-komichi-0.1 (crate (name "komichi") (vers "0.1.0") (hash "0v913yv968hmfnz1frlzlh8srz2hmr4yvzihfnhg2070zys62dvc") (yanked #t)))

(define-public crate-komichi-0.2 (crate (name "komichi") (vers "0.2.0") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "dirs-sys") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0qdj2b1hwlw0hdax208il224d1wa9sqdqz75j42i4d3m6vj2lag1") (yanked #t)))

(define-public crate-komichi-1 (crate (name "komichi") (vers "1.0.0") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "dirs-sys") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.11.0") (default-features #t) (kind 0)))) (hash "0vwzn0y5mmzlv4slrkbrkx64yh02s7gmr69ig6z1hg36a5i5rl2s") (yanked #t)))

(define-public crate-komichi-1 (crate (name "komichi") (vers "1.0.1") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "dirs-sys") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.11.0") (default-features #t) (kind 0)))) (hash "19m7i0mxlyhzqafdy00c5db2f0y1w6v91n51v2ilvsc3a7wnlfky") (yanked #t)))

(define-public crate-komichi-1 (crate (name "komichi") (vers "1.0.2") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "dirs-sys") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.11.0") (default-features #t) (kind 0)))) (hash "1xddnl0fx2c0nzgr3098fnvp2k7g20j77kd31wjk5dlsy8ahxgi7")))

(define-public crate-komichi-1 (crate (name "komichi") (vers "1.0.3") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "dirs-sys") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.11.0") (default-features #t) (kind 0)))) (hash "0cqz6dg68rncjcfcshpm8z8pjihnvvpv8nsqivy0cmq1xsrkq8ka")))

(define-public crate-komihash-0.2 (crate (name "komihash") (vers "0.2.0") (hash "0k33a7qn97fn7p68wdmfjapc593kx15sadw7rv2qvx8ml5j0m7bk")))

(define-public crate-komihash-0.2 (crate (name "komihash") (vers "0.2.1") (hash "0lrgrpdrn5ns2hmz5lqli1dpzz2nsans879vdwxyyvj40mspp08h") (yanked #t)))

(define-public crate-komihash-0.2 (crate (name "komihash") (vers "0.2.2") (hash "0lfbgwsc5swimvj2whldl9yimvmwlnqb6iaq5j40caqrjpin56g2") (yanked #t)))

(define-public crate-komihash-0.2 (crate (name "komihash") (vers "0.2.3") (hash "1v13nnzwz8dxrbc8nkp1bhjnlzybl8xl2ca27iz9zrncf7ayki7q")))

(define-public crate-komihash-0.3 (crate (name "komihash") (vers "0.3.0") (hash "0hgyqq12lgnhhwh2pma2m947i1his3xzigk326kshr3pwan9053p")))

(define-public crate-komihash-0.4 (crate (name "komihash") (vers "0.4.0") (hash "1z9q1vr5f0ncx8v0kb0binrdgscp716i85p1ksv0cdqxvmg3x43m")))

(define-public crate-komihash-0.4 (crate (name "komihash") (vers "0.4.1") (hash "123nf3q2ddqz8hj3llim43rw68kh34k0ljj5hlnarwbds5xlhcr8")))

