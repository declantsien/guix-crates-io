(define-module (crates-io ko da) #:use-module (crates-io))

(define-public crate-koda-0.1 (crate (name "koda") (vers "0.1.1") (hash "03gy5h8pwyln3vba3m1dk72dhg1pnhmjdajq2xngxb56n20b05hk")))

(define-public crate-kodak-0.0.1 (crate (name "kodak") (vers "0.0.1") (deps (list (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1p3l1ykf7jpkbrylx1pcaa3wjbwhylkzigwdm64y5shb0frnb6my")))

(define-public crate-kodama-0.1 (crate (name "kodama") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "0jiwivabfs256y3gdwlcxh8lcjgfxqfngzy9xrapawzcz457d6d1")))

(define-public crate-kodama-0.1 (crate (name "kodama") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "069p02s4bbnqf8yy1nd4b026ndps40pzgbgxbhfh2kplvqndk795")))

(define-public crate-kodama-0.2 (crate (name "kodama") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)))) (hash "1jzhyjr04b8099rivzwa5vs933kr5nardz9y7lz3c0g7m3sdgk22")))

(define-public crate-kodama-0.2 (crate (name "kodama") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)))) (hash "12ppgxlfgshb5hr1ywz4whfhbmqxdx7cyviz57yc0y5h469pr76j")))

(define-public crate-kodama-0.2 (crate (name "kodama") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "00qjqhjz4dfz39pqk4nwgxlkmjslipa3qbdlwc9q85ylnzhkhh57")))

(define-public crate-kodama-0.2 (crate (name "kodama") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "14chipddp2w5s5638aawzva5kyddr5yxnll1wffg9ys43akz6i3s")))

(define-public crate-kodama-0.3 (crate (name "kodama") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "10mgknzw6vnhn9a42dfhb609rwn5cwk2652wj91ch0s7xp7gws7q")))

(define-public crate-kodama-bin-0.1 (crate (name "kodama-bin") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "kodama") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "1wjyvdimj4dg38yl30wf7hn9hyci5lh17l3qj0ay5c75i029af7l")))

(define-public crate-kodama-bin-0.1 (crate (name "kodama-bin") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "kodama") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "05jxng5sxpxg4pg8w0v1amp8lmrdkw3b57cixig91hpf9nrcpaxs")))

(define-public crate-kodama-bin-0.1 (crate (name "kodama-bin") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "kodama") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "0gm1fv6x6x8mpb4avaiqimykjyydvdxyv0xbimdmaym7wj1d5ibj")))

(define-public crate-kodama-capi-0.1 (crate (name "kodama-capi") (vers "0.1.0") (deps (list (crate-dep (name "kodama") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03xxk0xx1ng3i6q57rz1d7c7503cs93hvv794kc03yzz72hiz6n4")))

(define-public crate-kodama-capi-0.1 (crate (name "kodama-capi") (vers "0.1.1") (deps (list (crate-dep (name "kodama") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1acc0cmspyfchmjm6gyq20drqifb1m1ajjf50cq26afjdrprwcqv")))

(define-public crate-kodama-capi-0.1 (crate (name "kodama-capi") (vers "0.1.2") (deps (list (crate-dep (name "kodama") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y1gk4cgbbw3an5pkgh1mjpjy6pg1msrp0wpxmih06x57mxqwsid")))

(define-public crate-kodama-capi-0.1 (crate (name "kodama-capi") (vers "0.1.3") (deps (list (crate-dep (name "kodama") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ak06lg8qbfxknc38ji0dw8fl8bzlcvrn3lmpisv0qvlh3zd1k29")))

(define-public crate-kodama-capi-0.2 (crate (name "kodama-capi") (vers "0.2.0") (deps (list (crate-dep (name "kodama") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0ybp32q8yxnb37dmbplwaz78c5m4gdjvifqi6c2m4xx3j1r6q8ih")))

