(define-module (crates-io ko an) #:use-module (crates-io))

(define-public crate-koan-0.1 (crate (name "koan") (vers "0.1.0") (hash "1xl2a89icmzqn8jjhnwwlj4wij1gldmr60li3ld403yhj8gb9grl")))

(define-public crate-koans-0.2 (crate (name "koans") (vers "0.2.0") (hash "0k391l01vvf53wi7pkc1p0gbrdmyz0yyr8p4mb2cal2pdw3vnkhv") (yanked #t)))

(define-public crate-koans-0.3 (crate (name "koans") (vers "0.3.0") (hash "02vsswic8zgzm95fa1pyij2y4xqwanckfpp1vbydvzdw31qn9xk5") (yanked #t)))

