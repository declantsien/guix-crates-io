(define-module (crates-io ko ek) #:use-module (crates-io))

(define-public crate-koek-classify-0.1 (crate (name "koek-classify") (vers "0.1.0") (deps (list (crate-dep (name "koek-redact") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0lfw8kkjjs07qflsm90ra3kcfbs1ikr46107bfks91njdn0ighh5")))

(define-public crate-koek-redact-0.1 (crate (name "koek-redact") (vers "0.1.0") (hash "091015n0ikp9mlvxmxapirnkmgk35fcsqr10hk6a2ayymbknj694")))

(define-public crate-koek-redact-0.2 (crate (name "koek-redact") (vers "0.2.0") (hash "0dw7qid0fqfsm2l0yi2habvpm6c18idp15bp693f5ha65zdzjdvj")))

