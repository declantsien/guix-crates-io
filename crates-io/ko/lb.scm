(define-module (crates-io ko lb) #:use-module (crates-io))

(define-public crate-kolben-0.0.1 (crate (name "kolben") (vers "0.0.1") (deps (list (crate-dep (name "postcard-cobs") (req "^0.1.5-pre") (default-features #t) (kind 0)) (crate-dep (name "rcobs") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rzcobs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "15ryh6yqlmwyg8cfzgg349xcl6gqj8y0dqfwxi3wgqi6ww0gy3a9")))

(define-public crate-kolben-0.0.2 (crate (name "kolben") (vers "0.0.2") (deps (list (crate-dep (name "insta") (req "^1.7.1") (default-features #t) (kind 2)) (crate-dep (name "postcard-cobs") (req "^0.1.5-pre") (kind 0)) (crate-dep (name "rcobs") (req "^0.1.1") (kind 0)) (crate-dep (name "rzcobs") (req "^0.1.1") (kind 0)))) (hash "0qk76vkv6dq1z6xsybyybrw65j3k7j4fkbl7dwhqckykr6n5bp8r") (features (quote (("use-std" "postcard-cobs/use_std" "rcobs/std" "rzcobs/std") ("default" "use-std"))))))

(define-public crate-kolben-0.0.3 (crate (name "kolben") (vers "0.0.3") (deps (list (crate-dep (name "insta") (req "^1.7.1") (default-features #t) (kind 2)) (crate-dep (name "postcard-cobs") (req "^0.1.5-pre") (kind 0)) (crate-dep (name "rcobs") (req "^0.1.1") (kind 0)) (crate-dep (name "rzcobs") (req "^0.1.1") (kind 0)))) (hash "1nnrjw7vmfcn1djw19ws9f7zlj3xmmfscnixkgvypnmpbc5w440z") (features (quote (("use-std" "postcard-cobs/use_std" "rcobs/std" "rzcobs/std") ("default" "use-std"))))))

