(define-module (crates-io ko rw) #:use-module (crates-io))

(define-public crate-korwin-0.1 (crate (name "korwin") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1sw25a8ix312ymh6rxrwrpgqj6158vl0p53yah0ff27bwhgynkf1") (yanked #t)))

(define-public crate-korwin-0.2 (crate (name "korwin") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1p68z8sar3papi5pxzbdsi83ma7j95dzsv0cgf5dpa6kxxdqlfxd") (yanked #t)))

(define-public crate-korwin-0.3 (crate (name "korwin") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1nbwgawnlipri5zbnnvawc95d8f6gr0f33sm3ngp51h0zwmn95wz") (yanked #t)))

(define-public crate-korwin-0.4 (crate (name "korwin") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15ncahvbcvmgalbzsns2x1cyg7543si9vd9jc82xlxvm5mld0cp4")))

