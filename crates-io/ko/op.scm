(define-module (crates-io ko op) #:use-module (crates-io))

(define-public crate-koopa-0.0.1 (crate (name "koopa") (vers "0.0.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "intrusive-collections") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.9") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1hfvms4a5bml0j1fajsyahm8glq85csm07n5jcfdk8nflmywxw0h") (features (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.3 (crate (name "koopa") (vers "0.0.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "key-node-list") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1pnahmdczmxnzfbhr6jjswsn9gyj5nwig9pm71b1lggxs5hn7471") (features (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.4 (crate (name "koopa") (vers "0.0.4") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "key-node-list") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0kxb7asrikd646gbvfs9nmzi8c9hl84nkk089dybbw94w3rpchbv") (features (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.5 (crate (name "koopa") (vers "0.0.5") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "key-node-list") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1bxchsn9r9n0hxnlk4qdzgp5jlxm65371n6ly0kah8ld7s2x85bq") (features (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.6 (crate (name "koopa") (vers "0.0.6") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "key-node-list") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 2)))) (hash "0npd1c4ynw5xii960xs1bjccgz5izjqvkf93gbcrzdnciyls79wg") (features (quote (("no-front-logger"))))))

(define-public crate-koopa-0.0.7 (crate (name "koopa") (vers "0.0.7") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "key-node-list") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7") (default-features #t) (kind 2)))) (hash "0nv0k2jrdj4c3nqfdrzq640hkwb8xnxbvjvz747w84k3yqhi814m") (features (quote (("no-front-logger"))))))

