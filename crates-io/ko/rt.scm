(define-module (crates-io ko rt) #:use-module (crates-io))

(define-public crate-korten-0.0.1 (crate (name "korten") (vers "0.0.1") (deps (list (crate-dep (name "rustix") (req "^0.38.21") (features (quote ("mm"))) (kind 0)) (crate-dep (name "scudo") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0ks2pkjq4878qg0g36j5qaad8dl2yr0rr0c4qkkf9r5crfsa0ddd")))

(define-public crate-kortex-0.1 (crate (name "kortex") (vers "0.1.0") (hash "07i5dfn3flsbz9avcrl85lwrf3m0sf00j37f62kidfajicnpd1v7")))

(define-public crate-kortex_gen_grpc-1 (crate (name "kortex_gen_grpc") (vers "1.11.8") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "064gmzi8pb5725f73rjn9nkp77k0qz120qq1xkqf30afpa8iaqji") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-1 (crate (name "kortex_gen_grpc") (vers "1.12.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "1wp60j6phg5nzaf0dbgdsc9w3b7jw90kdrysc5nar27a7lwhqa9a") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-2 (crate (name "kortex_gen_grpc") (vers "2.0.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "1s62mpwfg35nc65g5gmr7b7iifk30srnbglqcwkw3089vfyfpbp9") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-2 (crate (name "kortex_gen_grpc") (vers "2.1.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "1pz80sfqxs7q6q43xpcpp17q8hf0l79fb00z2h4xbywfc9m6v6zh") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-2 (crate (name "kortex_gen_grpc") (vers "2.2.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "01s71i276mhm1z05k9lpy489myhmp7b61k4yacp9bvxxl7lh8l36") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-2 (crate (name "kortex_gen_grpc") (vers "2.3.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "1n08paqhsmzas4864ri9p63cjrhp8d967fvr7m38paf2nl1w7lmi") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-2 (crate (name "kortex_gen_grpc") (vers "2.4.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "1y7sp6i4s1sbgwkvf424c2b003x88wsy2sc7qyh30h1v74rz9jan") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-2 (crate (name "kortex_gen_grpc") (vers "2.5.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "19dxc4j21wbkskfsd5mc5xdxh7a7hw97yv15kpdvnjlv92cpa7h4") (features (quote (("hstp-v1"))))))

(define-public crate-kortex_gen_grpc-2 (crate (name "kortex_gen_grpc") (vers "2.6.0") (deps (list (crate-dep (name "futures-core") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)))) (hash "0hnxcwn1hbniqnj3mn3yh3scp73fzilm0qlk1d3y12sy5yw2g74f") (features (quote (("hstp-v1"))))))

