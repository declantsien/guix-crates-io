(define-module (crates-io td -r) #:use-module (crates-io))

(define-public crate-td-rs-0.0.0 (crate (name "td-rs") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "02d0z6nrbzwx60948fqkhrvzrk8lsc10bg3yngdp8j2kr7nlv08x")))

(define-public crate-td-rs-0.1 (crate (name "td-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0fbhlvkx9mix6i2l7lsh269m64q2svlmn5i1d7dz1cjggzy91rpj")))

(define-public crate-td-rs-0.1 (crate (name "td-rs") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0lwkpf2x7871b1nc4nxz0bvcs35snjl6v22m7r7ffjl5dnji3xji")))

(define-public crate-td-rs-0.1 (crate (name "td-rs") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0bqjy261l3cgp95bzkyg840f2m7x36czgg2lqaq0fqsknsq1wk8a")))

(define-public crate-td-rs-0.1 (crate (name "td-rs") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0a3r7x4h2ajy6fi2hi1riq76k2zxid8dhwqvqwrsyywn2jaypysg")))

(define-public crate-td-rs-0.1 (crate (name "td-rs") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "1cb1i6zskppnln7bamam3hdrqi3niz01vzn6ifmixnxp7rsia4a4")))

(define-public crate-td-rs-0.1 (crate (name "td-rs") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "16flqrz8qylrpgxkqsmx722j0vd7vnib42cznwyvl3v2f5mj0xi6")))

