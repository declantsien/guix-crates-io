(define-module (crates-io td ri) #:use-module (crates-io))

(define-public crate-tdrip-0.1 (crate (name "tdrip") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0iv9ml6jwh2mqf7r1ln43s8lvlfhd0902j9r00ygna5yywqfppvz")))

