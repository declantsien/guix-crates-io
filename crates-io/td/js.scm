(define-module (crates-io td js) #:use-module (crates-io))

(define-public crate-tdjson-0.1 (crate (name "tdjson") (vers "0.1.2") (deps (list (crate-dep (name "tdjson-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "11hwb05lbxdkhldhbd7sfb4svh7pql7hyj5b1qf51ffq63ymsyqg")))

(define-public crate-tdjson-0.2 (crate (name "tdjson") (vers "0.2.0") (deps (list (crate-dep (name "tdjson-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "06lw4il6q0mb747ypkmjff0sj2nzasax5andyv6vadiqrgsvbn7r")))

(define-public crate-tdjson-0.2 (crate (name "tdjson") (vers "0.2.1") (deps (list (crate-dep (name "tdjson-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "09h98hp4h0aj7f4dp3lsn4hlmyk30fp65azia0wm8a5gs3cvbxg7")))

(define-public crate-tdjson-0.2 (crate (name "tdjson") (vers "0.2.2") (deps (list (crate-dep (name "tdjson-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0qcxbncg08rbbr2jvqxg9lxyrkyijdssmm6zkk53qdf6i3nqzdhq")))

(define-public crate-tdjson-copy-0.2 (crate (name "tdjson-copy") (vers "0.2.2") (deps (list (crate-dep (name "tdjson-sys-copy") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kxhvgxa7h8fra6x9983s4xsdky6vqyv1c9xnhim1g7v9268cxy6") (yanked #t)))

(define-public crate-tdjson-copy-0.1 (crate (name "tdjson-copy") (vers "0.1.0") (deps (list (crate-dep (name "tdjson-sys-copy") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jqkbxihk0h762sprmvfz287faccqv164vijfk20nqiizgh4bky3")))

(define-public crate-tdjson-copy-0.1 (crate (name "tdjson-copy") (vers "0.1.1") (deps (list (crate-dep (name "tdjson-sys-copy") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0msp53gj3k413ff9nfvmlpm6rgd9nz8q2n883xaps5q96nkd248a")))

(define-public crate-tdjson-sys-0.1 (crate (name "tdjson-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "16k1sawajgcrq9wacnxn3kszy4hzklfk14jyr56692psxf96isk5")))

(define-public crate-tdjson-sys-0.1 (crate (name "tdjson-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "0p4dgnh87i806isilbjh82cpmwlqadzmw7n8r9g8wnlach89kkd7")))

(define-public crate-tdjson-sys-0.1 (crate (name "tdjson-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "0bj6a42airb3qqpakjygdyvnql8pl9zb0vqq51v3dacjr1cd090z")))

(define-public crate-tdjson-sys-0.1 (crate (name "tdjson-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "1n7j7c66am8zj2p9f1lr6ka1hknbhnnzjw6wjjwyvlg77m2j2n00")))

(define-public crate-tdjson-sys-0.1 (crate (name "tdjson-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "0idg0k2nhl3vrgnv26qzs868r51kxxz9z24aaci4dsink8mdi862")))

(define-public crate-tdjson-sys-0.1 (crate (name "tdjson-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "072nbsh27gghynshrbxc0qkrr0jxx0ndhzmfgq099m5qxf5pxi5j")))

(define-public crate-tdjson-sys-copy-0.1 (crate (name "tdjson-sys-copy") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)))) (hash "16l7ff9njnqfdnfanxiabr5qs5b4913m2cfqwlqwrh01qx02zj6z")))

