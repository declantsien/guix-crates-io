(define-module (crates-io td _p) #:use-module (crates-io))

(define-public crate-td_proto_rust-0.1 (crate (name "td_proto_rust") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "1jqmhjzv7xwrq3arma90dx68ir053h5yx5jzv9vlai8b9z4a4van")))

(define-public crate-td_proto_rust-0.1 (crate (name "td_proto_rust") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0ihf1wk270mk2bmjjsx9kjs6fc1xypwgzaw5m3vyb9lqldlsmra1")))

(define-public crate-td_proto_rust-0.1 (crate (name "td_proto_rust") (vers "0.1.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "05fvxvyzkv0c3yn63wndm2qa5wb1dp8l6i1rsl6mm1a8br7s51ia")))

(define-public crate-td_proto_rust-0.1 (crate (name "td_proto_rust") (vers "0.1.3") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0igm1hcgi5b3qim4a1qdnb62djnlic6klfgfl9wmbn4i0hr9ac8x")))

(define-public crate-td_proto_rust-0.1 (crate (name "td_proto_rust") (vers "0.1.4") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "148xilshb5zqvwygi8rxhd3vw8pky71vnk942f5g8jvk7bjkrvzw")))

