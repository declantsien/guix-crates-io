(define-module (crates-io td x_) #:use-module (crates-io))

(define-public crate-tdx_attest-0.1 (crate (name "tdx_attest") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "0w8dfwh2hv7bvf13b5abiv6f6y6wrfsnx0ichd1kl196g3qv3ms1") (yanked #t)))

(define-public crate-tdx_attest-0.1 (crate (name "tdx_attest") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "114bwjf24ncngch33rs305cxxnpjzzgswdqp3rbfcjvmhfgrnlcv")))

