(define-module (crates-io td f-) #:use-module (crates-io))

(define-public crate-tdf-derive-0.0.0 (crate (name "tdf-derive") (vers "0.0.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09bdifzzgnrlkcyxyijvflhhwmckg2sx8hpfpx0bf0w6rclznabg")))

(define-public crate-tdf-derive-0.1 (crate (name "tdf-derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0k5h58l0ihpkxbm73226r88h2k7f8y18g0n1vbdj55r8rasf44pk")))

(define-public crate-tdf-derive-0.1 (crate (name "tdf-derive") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0wxb5c969jqaw3p8h37qls5hc4iga1j1jz95x9xvg335b64csmfd")))

(define-public crate-tdf-derive-0.2 (crate (name "tdf-derive") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1hbyb1vca1xrpnj5b3a148gi689szh5cpzyy44hln1h9fjdcp7i3")))

