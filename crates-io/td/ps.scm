(define-module (crates-io td ps) #:use-module (crates-io))

(define-public crate-tdpsola-0.1 (crate (name "tdpsola") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 2)) (crate-dep (name "wav") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "120kvyl2jxi6vfjrk986cdzs0hfsqhrw14rnis0bbpvnz3dvkf6j")))

