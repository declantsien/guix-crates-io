(define-module (crates-io td yn) #:use-module (crates-io))

(define-public crate-tdyne-peer-id-1 (crate (name "tdyne-peer-id") (vers "1.0.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1i4v8l4ydgcslkf42wf3441lha61lwk4x4ikmz7isad5h50sbqlp") (yanked #t)))

(define-public crate-tdyne-peer-id-1 (crate (name "tdyne-peer-id") (vers "1.0.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1ipp23i3h4c534g74slkicqrizaz46vw1v2kfl2f8qjsg8w115vv") (yanked #t)))

(define-public crate-tdyne-peer-id-1 (crate (name "tdyne-peer-id") (vers "1.0.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "0ghc6ws1bgj7zxfrq7a28zxsm2kp7xqg9m7r91b08dgqlrdjipkd")))

(define-public crate-tdyne-peer-id-registry-0.1 (crate (name "tdyne-peer-id-registry") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11") (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tdyne-peer-id") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3") (default-features #t) (kind 2)))) (hash "0q57mkalssydc4p4gmyicb3f34kvvh2319n6h2h4fh59mqd3i10p") (yanked #t)))

(define-public crate-tdyne-peer-id-registry-0.1 (crate (name "tdyne-peer-id-registry") (vers "0.1.1") (deps (list (crate-dep (name "phf") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11") (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tdyne-peer-id") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3") (default-features #t) (kind 2)))) (hash "09cchq723qwshp6rxg705zghjcr9vrc9phs7x2zfi070av9v48qr")))

