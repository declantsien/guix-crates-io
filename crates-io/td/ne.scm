(define-module (crates-io td ne) #:use-module (crates-io))

(define-public crate-tdnet-0.1 (crate (name "tdnet") (vers "0.1.0") (hash "1rzlqy3kwz9zkc5z6r32c0s2zms2hj0z592iq3n7chf12fr85k1h")))

(define-public crate-tdnet-0.1 (crate (name "tdnet") (vers "0.1.2") (deps (list (crate-dep (name "actix-files") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "actix-rt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^2.0") (default-features #t) (kind 0)))) (hash "00vnb1lxnn2yk49dh6gyghzwyg7xdyqb7h5dmf2m2az01wd64bb9")))

