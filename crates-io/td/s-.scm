(define-module (crates-io td s-) #:use-module (crates-io))

(define-public crate-tds-meter-0.0.0 (crate (name "tds-meter") (vers "0.0.0") (hash "0fkmmfb96zsrp95cgirahl9liv6zrrkn9vpf0r711jz1zzxyvab7")))

(define-public crate-tds-meter-0.1 (crate (name "tds-meter") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "0263qnbxfgkjhwan6rwnws48vccmxzz4dy1aj69g3fmnn324sn5b")))

