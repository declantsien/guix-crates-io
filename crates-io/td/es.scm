(define-module (crates-io td es) #:use-module (crates-io))

(define-public crate-tdesktop_theme-0.1 (crate (name "tdesktop_theme") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "106nqlj4r67z45n68pymfv6y9q5xv0y0rbaisrh3wlkfdmm4p6j4")))

(define-public crate-tdesktop_theme-0.1 (crate (name "tdesktop_theme") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1k752z26j83nqhy7n0mqrg6xayz44vpzwhkb1jy9n2gd5qyilndp")))

(define-public crate-tdesktop_theme-0.1 (crate (name "tdesktop_theme") (vers "0.1.2") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "16wv1bynbv9mggx1zvb2vgfc2k82vmmvfl7crlmzbs8sfli9nm4v")))

(define-public crate-tdesktop_theme-0.2 (crate (name "tdesktop_theme") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "12dr6xm04d7hi7fayc894g4iw6fkrc6qfhzlmi3ixlkx40rhsxpy")))

(define-public crate-tdesktop_theme-0.3 (crate (name "tdesktop_theme") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0k3zlzdd4v43h94si4mbvf10w4z4c15w7rhl5jkw1qy3kfr7m4rs")))

(define-public crate-tdesktop_theme-0.3 (crate (name "tdesktop_theme") (vers "0.3.1") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1gsb4vmfkkdc0c5a5dbz644a143wshqn290lpdyzh2h3razqbq43")))

(define-public crate-tdesktop_theme-0.3 (crate (name "tdesktop_theme") (vers "0.3.2") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "1pvffnggpdl8m2nng79l2h92zcawxxv9qxyl5rlbvxkw75mpzh75")))

