(define-module (crates-io td x-) #:use-module (crates-io))

(define-public crate-tdx-guest-0.1 (crate (name "tdx-guest") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (default-features #t) (kind 0)) (crate-dep (name "raw-cpuid") (req "^10") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.10") (default-features #t) (kind 0)))) (hash "1xl7ss1vwv0dfh9l8lk5r5lglmqz3c229rg9m5w0ccl1gfdxjnv7")))

(define-public crate-tdx-guest-0.1 (crate (name "tdx-guest") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "iced-x86") (req "^1.21.0") (features (quote ("no_std" "decoder" "gas"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "raw-cpuid") (req "^10") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.10") (default-features #t) (kind 0)))) (hash "0qhk8hvmf4ji9apkbckxg4b24lalmwj6b88a6dy6c0x9i6f6pipy")))

