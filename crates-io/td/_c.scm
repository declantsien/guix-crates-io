(define-module (crates-io td _c) #:use-module (crates-io))

(define-public crate-td_clua-0.1 (crate (name "td_clua") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0yy3wiww4sz7rag6sl3pfn4x5gc9vhh28q1zspd2qr6rcg9wfyxj")))

(define-public crate-td_clua-0.1 (crate (name "td_clua") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1i78gxrnb8nf6gyaq0aazf6n727lrnwvlnj45db4gbg5bmliffax")))

(define-public crate-td_clua-0.1 (crate (name "td_clua") (vers "0.1.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "13glspc063r9w4w6dzkdva7lj127pszki2b7yy4j5dwppbp8ppdc") (links "lua")))

(define-public crate-td_clua_ext-0.1 (crate (name "td_clua_ext") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "td_rlua") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qspr443yq1xyb5wh7wbhlbpy1gh7rich3bpwpnglslps52fka84")))

