(define-module (crates-io td oj) #:use-module (crates-io))

(define-public crate-tdoj-data-0.1 (crate (name "tdoj-data") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.85") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cjmjq5sdpw1bzrcpk91j83i95yb29svvlysvahfp08rn5ki7wy2")))

