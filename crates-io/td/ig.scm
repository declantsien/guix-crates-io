(define-module (crates-io td ig) #:use-module (crates-io))

(define-public crate-tdigest-0.1 (crate (name "tdigest") (vers "0.1.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mzvkcblhzq30dfq7npc4dp5nsidkdvjzzlbapbdbl7fk1rlm2sq")))

(define-public crate-tdigest-0.1 (crate (name "tdigest") (vers "0.1.1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "0km1r9hj5zyih3p24k4lzcq1r6k88blv66xi2pfr90k3cwbsh1v2")))

(define-public crate-tdigest-0.2 (crate (name "tdigest") (vers "0.2.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "1znz1a4kk441kvkjphvbbj97qyh1id6l8jivx43v6k6aybi7ma1z")))

(define-public crate-tdigest-0.2 (crate (name "tdigest") (vers "0.2.1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v71923knncbd5qbqwb7b8zmsf2c2rf6mra2gccv2jhf69in6i9z")))

(define-public crate-tdigest-0.2 (crate (name "tdigest") (vers "0.2.2") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0) (package "serde")))) (hash "0b3s39pzp205qa6zwdzazi4l6zp6yifc030rpg139y0xc8f6qxwv") (features (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

(define-public crate-tdigest-0.2 (crate (name "tdigest") (vers "0.2.3") (deps (list (crate-dep (name "ordered-float") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0) (package "serde")))) (hash "1j7kifbpa5jgl30yql1pknr0bax8dq3b8vflqzhg1k7b11d24pf4") (features (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

(define-public crate-tdigest-rs-0.2 (crate (name "tdigest-rs") (vers "0.2.4") (deps (list (crate-dep (name "ordered-float") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0) (package "serde")))) (hash "1djqw8irwfb4jv48r1qp8z9rh11kpv5h2xalxd334wv98kmwb2xf") (features (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

(define-public crate-tdigest-rs-0.2 (crate (name "tdigest-rs") (vers "0.2.5") (deps (list (crate-dep (name "ordered-float") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0) (package "serde")))) (hash "0lgycyv168dkybg79l91197mzba74kbqcjr2b1c3bfdr9mbmi84r") (features (quote (("use_serde" "serde" "serde/derive" "ordered-float/serde"))))))

