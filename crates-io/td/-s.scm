(define-module (crates-io td -s) #:use-module (crates-io))

(define-public crate-td-shim-interface-0.1 (crate (name "td-shim-interface") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "r-efi") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (features (quote ("derive"))) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.31") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18643lk7463v4vmc19azmzxif86ia9rfvjbkdan55hx4i5nxzkgh")))

(define-public crate-td-shim-interface-0.1 (crate (name "td-shim-interface") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "r-efi") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (features (quote ("derive"))) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.31") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0n140kaw10ln8x4001a0l4dn9qhapa0rwkdwb3mmi4xsv7zfih46")))

