(define-module (crates-io td c1) #:use-module (crates-io))

(define-public crate-tdc1000-0.1 (crate (name "tdc1000") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "061czvaszakg3g1pgvzxg0n0q40c01p4m1qxgbq4d4hzgbh914iv") (yanked #t)))

(define-public crate-tdc1000-0.1 (crate (name "tdc1000") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1aviz4419c1liyqpmh2f2rpsac8bnb419jswsmbnz5gg3w5p3qz7")))

(define-public crate-tdc1000-0.1 (crate (name "tdc1000") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "02lnnvmww8v3dlzmwx8122h3xhljq2raqk9y1bdv17mmiy8w94z5")))

