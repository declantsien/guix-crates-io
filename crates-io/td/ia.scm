(define-module (crates-io td ia) #:use-module (crates-io))

(define-public crate-tdiag-0.1 (crate (name "tdiag") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "differential-dataflow") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tdiag-connect") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "timely") (req "^0.9") (default-features #t) (kind 0)))) (hash "11gsrmynzy76sf8m2kas8g9i96q11bpdi6bsxllfg52ig61ga7ql")))

(define-public crate-tdiag-0.2 (crate (name "tdiag") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "differential-dataflow") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tdiag-connect") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "timely") (req "^0.10") (default-features #t) (kind 0)))) (hash "1dn4g9ckdbidgzyjbzgdv5dmczq8c4avcrv27qgv8g9ihq8y59qx")))

(define-public crate-tdiag-connect-0.1 (crate (name "tdiag-connect") (vers "0.1.0") (deps (list (crate-dep (name "timely") (req "^0.9") (default-features #t) (kind 0)))) (hash "0dgaficrz75j3r8z86avqxwvxfb203cx0dh6dfjx2bhlpih6s8qy")))

(define-public crate-tdiag-connect-0.2 (crate (name "tdiag-connect") (vers "0.2.0") (deps (list (crate-dep (name "timely") (req "^0.10") (default-features #t) (kind 0)))) (hash "1h094dcfj9qz8bykd0385s4p3jrfq0qalyqr2rn1bkv8xcg0ql1r")))

