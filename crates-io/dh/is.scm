(define-module (crates-io dh is) #:use-module (crates-io))

(define-public crate-dhist-0.1 (crate (name "dhist") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0ndl371dfyh3i4j4hl5ikzxpi8pfkqimljkcgibfr6gqcwgm1gvq")))

(define-public crate-dhist-0.1 (crate (name "dhist") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (features (quote ("color" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0wmkcjq69q5f8gc7sw9ihs9lhmn09gdv8y13i437rkkdz96zzmwl")))

