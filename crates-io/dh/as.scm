(define-module (crates-io dh as) #:use-module (crates-io))

(define-public crate-dhash-0.1 (crate (name "dhash") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gs733zbyrcfvkgkzr7g5cc19a4f3xvacf5bp7djsk3vm987zh1v")))

