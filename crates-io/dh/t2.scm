(define-module (crates-io dh t2) #:use-module (crates-io))

(define-public crate-dht20-0.1 (crate (name "dht20") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "18vcf9b9x602fybw5m3p9a9chvmxr2fnj4bjdg4c0hfrzhpaka3v")))

(define-public crate-dht22-driver-0.1 (crate (name "dht22-driver") (vers "0.1.0") (deps (list (crate-dep (name "critical-section") (req "^1.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "01m58nswmikwpndnv6cgv6a05rdb579wqv229rvihjlg7ly68acf") (features (quote (("std") ("default" "critical-section")))) (v 2) (features2 (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-dht22-driver-0.1 (crate (name "dht22-driver") (vers "0.1.1") (deps (list (crate-dep (name "critical-section") (req "^1.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "0mq1dl5wrlbbq87haf3l150380p5200d4xs112x90g5f5jz94n5d") (features (quote (("std") ("default" "critical-section")))) (v 2) (features2 (quote (("critical-section" "dep:critical-section"))))))

(define-public crate-dht22_pi-0.1 (crate (name "dht22_pi") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17abwi2cdcz4g7p865af5ikpdkj26hflxnndnxsp568p3mp0d85d")))

(define-public crate-dht22_pi-0.2 (crate (name "dht22_pi") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1n447r5q5sxr3y7xh7z0bxcv9i6cbwbz4dcm6m8939m2cqijl4b4")))

(define-public crate-dht22_pi-0.3 (crate (name "dht22_pi") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "05l2xwam61z6pdpm8c114bdq1nzcw4i3z3qqmr27v5hqlf5vm3n9")))

(define-public crate-dht22_pi-1 (crate (name "dht22_pi") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "1k2ac84xd95zq3nfix3wphjs9l0czhfbzb3f4g0ay80pfz4482sz")))

