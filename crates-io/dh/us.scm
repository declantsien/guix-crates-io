(define-module (crates-io dh us) #:use-module (crates-io))

(define-public crate-dhust-0.1 (crate (name "dhust") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06bhwqsy7g1hwr0jlf9a02hpw2z3ycphh2i419dnnx0gkc1mn93d")))

