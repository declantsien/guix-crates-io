(define-module (crates-io dh ra) #:use-module (crates-io))

(define-public crate-dhravya_api-0.1 (crate (name "dhravya_api") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "16kl1jbd4yci81pvb1fq68ii692qbky71fgjc90sd2k3gdk5jbfw") (yanked #t)))

(define-public crate-dhravya_api-0.1 (crate (name "dhravya_api") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y3c2wpy43c0bnjcjy0y9b1a6kvw4ink9890nnq4jnbimyljkyln") (yanked #t)))

(define-public crate-dhravya_api-0.1 (crate (name "dhravya_api") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gpik2dsmvxif9hxghd4vs7y9bzv7jx1sg3nysljh33b8fpj1ifq")))

