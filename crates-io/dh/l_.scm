(define-module (crates-io dh l_) #:use-module (crates-io))

(define-public crate-dhl_tracking-0.1 (crate (name "dhl_tracking") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0n7s7s8fgwl0dwvv0ah19d5694m37xwjj8lywppavfha02q5g7z3")))

(define-public crate-dhl_tracking-0.1 (crate (name "dhl_tracking") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1249yif2jf652rq145hjwzcajpmr8q040yf5540qls6idq5j61za")))

(define-public crate-dhl_tracking-0.1 (crate (name "dhl_tracking") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "14fw1w251dbqb5wks2b3rqdqqnhnmjf645hhwpx020yh38rlki81")))

(define-public crate-dhl_tracking-0.1 (crate (name "dhl_tracking") (vers "0.1.4") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0zg6a33wpknlfnpc5v9g54y413dmz9cri52hjib0avizf47sxzcz")))

