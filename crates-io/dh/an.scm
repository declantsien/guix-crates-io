(define-module (crates-io dh an) #:use-module (crates-io))

(define-public crate-dhandho-0.1 (crate (name "dhandho") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)))) (hash "13r1lmx7y7ba6blr8h48irc5q2c9c7gz827znm5cgjifl1hhc9n2")))

(define-public crate-dhandho-0.2 (crate (name "dhandho") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)))) (hash "0y7kk8chnfcw6z1rv3jrhdvndx20qxjjycy10fnl6pcifcpac0y2")))

