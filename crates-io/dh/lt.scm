(define-module (crates-io dh lt) #:use-module (crates-io))

(define-public crate-dhltest-1 (crate (name "dhltest") (vers "1.0.0") (hash "1cg58lvl8wd76m7pmj1ddq1n6m0qiz6ikvgaplcny11dj33qwwk0")))

(define-public crate-dhltest-dash-1 (crate (name "dhltest-dash") (vers "1.0.0") (hash "0wna6ys0b3c64p91pd0qvhqzj7h6mr94b55jin5dfaffsv657bp3")))

(define-public crate-dhltest_underscore-1 (crate (name "dhltest_underscore") (vers "1.0.0") (hash "06bkzym61y50pjf71y7928myg0vqlsy5icnlmy3pvydbrkbl2f4y")))

