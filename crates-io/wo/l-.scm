(define-module (crates-io wo l-) #:use-module (crates-io))

(define-public crate-wol-rs-0.0.1 (crate (name "wol-rs") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1il83plwj95cnhyql6nisxn5yy2l6jzd0wcs150552mshv1bsr1a")))

(define-public crate-wol-rs-0.0.2 (crate (name "wol-rs") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sbz7v4lyi1z5fssnlapa86x730zh33khsb54s3z7b3hqrfgwz0y")))

(define-public crate-wol-rs-0.9 (crate (name "wol-rs") (vers "0.9.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zigf49sqfl5kf2yf92fjarsimdqrq10clnahvf3nk56yv091476")))

(define-public crate-wol-rs-0.9 (crate (name "wol-rs") (vers "0.9.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wp19ibfz396n0xym3rnm1526bhkax8c4wi4n37nq9cbn9lpxyfp")))

(define-public crate-wol-rs-1 (crate (name "wol-rs") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rd7hhp98hwfhlmzks1z6abcd7pmigfhqdwda4aib8rldr45xp28") (v 2) (features2 (quote (("bin" "dep:clap"))))))

(define-public crate-wol-rs-1 (crate (name "wol-rs") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "13qbhw0300fwgxj4zc29mj9jcazdb0cpcijrign0icpr7q1qlniw") (v 2) (features2 (quote (("bin" "dep:clap"))))))

