(define-module (crates-io wo bb) #:use-module (crates-io))

(define-public crate-wobbly-0.1 (crate (name "wobbly") (vers "0.1.0") (hash "1xm8mi8prp5wk0g32gz14z19gzlndsglzpar3f392x2zs2dzbs2z") (features (quote (("std")))) (rust-version "1.56")))

(define-public crate-wobbly-0.1 (crate (name "wobbly") (vers "0.1.1") (hash "1mmxvg0bdzfnd18rnc1grqb3wnyz9sivgmkcxry6l3r6nkk7xi0h") (features (quote (("std")))) (rust-version "1.56")))

(define-public crate-wobbly-atlas-0.0.0 (crate (name "wobbly-atlas") (vers "0.0.0") (hash "1478f3342dha5sxg0wb591wycg41bzq1ric5rcwl9075yyjmid7h")))

