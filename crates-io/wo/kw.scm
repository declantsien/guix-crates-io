(define-module (crates-io wo kw) #:use-module (crates-io))

(define-public crate-wokwi-server-0.2 (crate (name "wokwi-server") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("env"))) (default-features #t) (kind 0)) (crate-dep (name "console-subscriber") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "espflash") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "opener") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0jjczrz38izga066idny8547ns8ac3pi8hs9rd4byl1f446dil0z") (v 2) (features2 (quote (("tokio-console" "dep:console-subscriber"))))))

(define-public crate-wokwi_chip_ll-0.1 (crate (name "wokwi_chip_ll") (vers "0.1.0") (hash "1ixidha52g6d3crkbvhqay1jqgag4n0jqlq26059336l6x9qbfyb")))

(define-public crate-wokwi_chip_ll-0.1 (crate (name "wokwi_chip_ll") (vers "0.1.1") (hash "0f13naq4lspxmyaf0jzzbjyv63a6ix341lyy9p8rli21waa1prlw")))

(define-public crate-wokwi_chip_ll-0.1 (crate (name "wokwi_chip_ll") (vers "0.1.2") (hash "1yqjnnb09r8jcz846grfbp0q2sf2c50v8938vsfhnm7461w2qs2f")))

(define-public crate-wokwi_chip_ll-0.1 (crate (name "wokwi_chip_ll") (vers "0.1.3") (hash "0ppgwpb6vgsh1y904s15ssiswkn5cq2chw0c9h9vhhm2lxa050y9")))

