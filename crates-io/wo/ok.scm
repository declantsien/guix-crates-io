(define-module (crates-io wo ok) #:use-module (crates-io))

(define-public crate-wookie-0.1 (crate (name "wookie") (vers "0.1.0") (deps (list (crate-dep (name "futures-micro") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k4w79aflflwiy6x20fpgp7pdnpw18z7rny2syk4w8bwzmvn9j3s")))

(define-public crate-wookie-0.1 (crate (name "wookie") (vers "0.1.1") (deps (list (crate-dep (name "futures-micro") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ijf3pcnyxibniw9pfzmgv6h4m8f8plic7m74bwna0c6rlykja8b")))

(define-public crate-wookie-0.2 (crate (name "wookie") (vers "0.2.0") (deps (list (crate-dep (name "futures-micro") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0cvh9m8wb2qmlw49jkpnpz3z19axfqca78lrslf8n6lykag5m21k")))

(define-public crate-wookie-0.3 (crate (name "wookie") (vers "0.3.0") (deps (list (crate-dep (name "dummy-waker") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "144040w6xhf58i6ld2y6v63bwabki33swsgbgzzsfibl00z5a7z7") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-wookie-0.3 (crate (name "wookie") (vers "0.3.1") (deps (list (crate-dep (name "dummy-waker") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "12fy8rfliz7350vkw7hhzvpd9403fcw43gza675702js598v8sjc") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-wookie-0.3 (crate (name "wookie") (vers "0.3.2") (deps (list (crate-dep (name "dummy-waker") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0kkjz26dmmixcvf1dl42ph6i4jy7k2h2xbqy1ry0br0n1an1y0xs") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-wookong-solo-0.1 (crate (name "wookong-solo") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0hdkb56g7y7xzbspnwanggr0ws66r0kcd1d12qaz1wlyv79icgha")))

(define-public crate-wookong-solo-0.1 (crate (name "wookong-solo") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ywplds779c1nq33nvr223r3gf9i2cdnp9rvzz59mzvw9ajjn8gd")))

(define-public crate-wookong-solo-0.1 (crate (name "wookong-solo") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0scj7qhrjlid5sawmsx64a133fl7w0bqs1zwlp0mjimz90p9f8m5")))

