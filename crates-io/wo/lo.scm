(define-module (crates-io wo lo) #:use-module (crates-io))

(define-public crate-wololo-0.1 (crate (name "wololo") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0j70rqp4z06znzc0nkwwl6sgwa2ca9iincdjj6insr77aghsnfmv")))

(define-public crate-wololo-0.1 (crate (name "wololo") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1xd3li2rqljhr73731hdg5xy8s1zsyms1zrq9m9ik36ic7xiz14l")))

(define-public crate-wololo-0.2 (crate (name "wololo") (vers "0.2.0") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-error") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.15") (features (quote ("registry" "env-filter"))) (default-features #t) (kind 0)))) (hash "1anq1d5656amrsym5ykjp2z6vlnjxw33inxgap2zwr0pll2l05rj")))

