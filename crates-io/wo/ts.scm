(define-module (crates-io wo ts) #:use-module (crates-io))

(define-public crate-wots-0.1 (crate (name "wots") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1bf73nczqxzsjdrp95ng1aah3gwvgbx08miqv60m639bik4zla12")))

(define-public crate-wots-rs-0.1 (crate (name "wots-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha256-rs") (req "^1.0") (default-features #t) (kind 0)))) (hash "00lnmmvbdph5avyi0hnah4ymga0pblb7ys9isp8bjj57iznmqpf0")))

(define-public crate-wots-rs-0.2 (crate (name "wots-rs") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha256-rs") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rwcbidrk29qdd1bakd18k8m2cxfwj2r86s5gp2a8476fwkd1r18")))

