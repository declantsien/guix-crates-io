(define-module (crates-io wo on) #:use-module (crates-io))

(define-public crate-woongdle-0.1 (crate (name "woongdle") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0ww89lzrgfsg2n372x7fxihq5ngc32id5yi6nzwxikdkv94jikdi")))

(define-public crate-woongdle-0.1 (crate (name "woongdle") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1436jsjqal4cywj5qp8ac8wkdw75hdi44i2sfq594s7fnwwabrms")))

(define-public crate-woongdle-0.1 (crate (name "woongdle") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1mbxmjs86iqr463z06aqs1ysv4hdj4cp6dd96v7z64b5xbdnhs9f")))

