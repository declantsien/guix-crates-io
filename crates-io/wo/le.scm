(define-module (crates-io wo le) #:use-module (crates-io))

(define-public crate-wole-0.1 (crate (name "wole") (vers "0.1.2") (hash "1cyn91l4mgiqsj66kf45bp9gn50pimnqky6d9rjq1rn20ras76m3")))

(define-public crate-wole-0.1 (crate (name "wole") (vers "0.1.3") (hash "103fsfg571ms377xpdhk2ac7ijy4v4s5g5y361pq3fv0ik2k418k")))

(define-public crate-wole-0.1 (crate (name "wole") (vers "0.1.4") (hash "1yznycspxg57qhc33nccjm5cfjas5nbmjir2vnl4ryd72bdpd396")))

(define-public crate-wole-0.1 (crate (name "wole") (vers "0.1.5") (hash "0cw934249gh5gmfcmpnsdyh252sixnnw41vwfv9yyl4x32clkb9s")))

(define-public crate-wole-1 (crate (name "wole") (vers "1.2.6") (hash "1iqll8dn96s6dsr0wy2vkiyj5qijacgwyzf90b92i63bpxri91xj")))

(define-public crate-wole-1 (crate (name "wole") (vers "1.2.7") (hash "0y8vr1290h4rz45v667rq5m1h576sk6gv40jxvrwnf0ph5d5w1mf")))

(define-public crate-wole-1 (crate (name "wole") (vers "1.2.8") (hash "0zy0gi1zlh2gkrswk9gjc7rc4j2yq1qq2rlczizxgxacwxk3zkdf")))

