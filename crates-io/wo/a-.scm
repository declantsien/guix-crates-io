(define-module (crates-io wo a-) #:use-module (crates-io))

(define-public crate-woa-ballet-stats-0.0.0 (crate (name "woa-ballet-stats") (vers "0.0.0") (hash "16smscv2kkkc9c57andm9f7pb7jvbyq489k0pq0zp2vkl9xc5qpq")))

(define-public crate-woa-bridge-0.0.0 (crate (name "woa-bridge") (vers "0.0.0") (hash "1cbh7yhrlrmfwawgaiw3rj3l8v0pba89whnll2fdsnfjj52sria0")))

(define-public crate-woa-bridge-contracts-0.0.0 (crate (name "woa-bridge-contracts") (vers "0.0.0") (hash "0in9nk0mcxygllmd0apvly56zipxq0zcaz35zg55jxzrjx6rfqmg")))

(define-public crate-woa-governance-notifications-0.0.0 (crate (name "woa-governance-notifications") (vers "0.0.0") (hash "08nbj9c0awshv3lvrnqj1aajlysq9kmdqbza5r1dcns8lgsv5n5n")))

