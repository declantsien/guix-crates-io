(define-module (crates-io wo oj) #:use-module (crates-io))

(define-public crate-woojin-0.1 (crate (name "woojin") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0ig54x7c37r03ac8hl3n3a9abc0sg03n0axcm2ysf7plgsrsxj63") (yanked #t)))

(define-public crate-woojin-0.1 (crate (name "woojin") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1i1dfd1nxwxgx4jzrb2na2wkzsb3y339cm8kh8vyzmjw3rc2rddk") (yanked #t)))

(define-public crate-woojin-0.1 (crate (name "woojin") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "012hsqdrribfdf5wnpqks2axb7kc2w8dm3japp9v70sh9w5gw7an") (yanked #t)))

(define-public crate-woojin-0.1 (crate (name "woojin") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1wg4995miq4mfn81skykprnd9gd23hw9pfr6jjnkcg0lz970q79n") (yanked #t)))

(define-public crate-woojin-0.1 (crate (name "woojin") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0gayn5ljcjskx5agjsjzv6lnl6nrc3pavlr6dk4wnp8xh483kpig") (yanked #t)))

(define-public crate-woojin-0.1 (crate (name "woojin") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0c545n68mamlv4685rg97c0k0f15g0znbmr03j6w46n2s6g2hm7v")))

