(define-module (crates-io wo k_) #:use-module (crates-io))

(define-public crate-wok_log-0.0.0 (crate (name "wok_log") (vers "0.0.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.82") (features (quote ("std"))) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.59") (features (quote ("console"))) (default-features #t) (kind 0)))) (hash "1bs7v2fl6x02csx701736rrqpppvnszwzx03kvv9dmza4rhn92za") (features (quote (("wrapped") ("macros" "wrapped"))))))

