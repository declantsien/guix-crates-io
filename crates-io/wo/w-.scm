(define-module (crates-io wo w-) #:use-module (crates-io))

(define-public crate-wow-framework-0.1 (crate (name "wow-framework") (vers "0.1.0") (hash "1jsk2xrbriqjzqv9h0y61vsi8y8a86igidwl7c4bd92rg2iqbvyw")))

(define-public crate-wow-rs-1 (crate (name "wow-rs") (vers "1.0.0") (hash "1gn2nlwqa1n8ajx3hj45y3wb33p9v90j8fwjqbpjkr0rx05njw7p")))

(define-public crate-wow-web-0.1 (crate (name "wow-web") (vers "0.1.0") (hash "1wmbwfvx6m81849h4h33knl690x33hd1hl9wa5allqmnq49vaybr")))

