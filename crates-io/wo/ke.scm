(define-module (crates-io wo ke) #:use-module (crates-io))

(define-public crate-woke-0.0.1 (crate (name "woke") (vers "0.0.1") (hash "1rmbvzknawb418i6ibksbl0ngyz11zzqx5ayn2s5fcl2d7dbvpgh")))

(define-public crate-woke-0.0.2 (crate (name "woke") (vers "0.0.2") (hash "1cfv2xdi1ipy42ics4xs2ixxg2da69w111b5rac94m7fr5vspzga")))

(define-public crate-woke-0.0.3 (crate (name "woke") (vers "0.0.3") (hash "0y58jwyglc3wql32l5gmiaflxirh4a7612wfgqd9311pwc5krgzq")))

(define-public crate-woke-0.0.4 (crate (name "woke") (vers "0.0.4") (hash "02qv4r5pc0v2j00mk30zabf91bn8p7amix5l1rwk28bnjp7hljpq") (rust-version "1.63")))

