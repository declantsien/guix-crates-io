(define-module (crates-io wo lt) #:use-module (crates-io))

(define-public crate-wolt-0.1 (crate (name "wolt") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.7.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "09jrfq7kymn47xl35chhmdvccrl8fk2jrz4ykpyg36iiql8kmnns")))

