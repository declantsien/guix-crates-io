(define-module (crates-io wo of) #:use-module (crates-io))

(define-public crate-woof-0.0.1 (crate (name "woof") (vers "0.0.1") (hash "0hn38m1zfrzq1jvb7wqakr25pyjzdaz1aqm7c25lsqi769x0k091")))

(define-public crate-woof-cli-0.0.1 (crate (name "woof-cli") (vers "0.0.1") (hash "1fb37n8yp03sd9bxvxfz9mhygw3gy1kk23fjb5pnczj76lmxapc2")))

(define-public crate-woof-core-0.0.1 (crate (name "woof-core") (vers "0.0.1") (hash "0wm7vwhyiv9xrb34zc2bnwdwkj641ivij4p4ib9qh13r6ya724si")))

(define-public crate-woof-derive-0.0.1 (crate (name "woof-derive") (vers "0.0.1") (hash "07dpv9jf3kawfllv5chcwwp64j68zmw8vf22cccipwzk5604yd75")))

(define-public crate-woof-engine-0.0.1 (crate (name "woof-engine") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "macro_rules_attribute") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1qsjw8339n33vagavqb2vaskiyf8shhy6qvlhqlvygvi505ka7q5")))

(define-public crate-woof-pkg-0.1 (crate (name "woof-pkg") (vers "0.1.0") (hash "17aa6ialsq1jjfl92h6m3yw69bzqcqyswrd9ai46qygz6pz1kr2n")))

(define-public crate-woofytest-0.1 (crate (name "woofytest") (vers "0.1.0") (hash "0bb8gs2j2nz01vdpb94wn0277ysl2zwr4mnxk885r6p6lpflzhjb") (yanked #t)))

(define-public crate-woofytest-0.1 (crate (name "woofytest") (vers "0.1.1") (hash "1lka0mygbqwirnf0f67w4kxssv7pcr8a135qrc63y59wvvcjy48h")))

(define-public crate-woofytest-0.1 (crate (name "woofytest") (vers "0.1.2") (hash "1ipszh7mypickwadynlp6j8nkjh48i97gs1mddxcx5icbsqbhwq0")))

