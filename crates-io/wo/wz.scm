(define-module (crates-io wo wz) #:use-module (crates-io))

(define-public crate-wowza-rest-rust-0.0.0 (crate (name "wowza-rest-rust") (vers "0.0.0") (hash "14f7h51x6xv19h3m6rjcnw4w0g157cnm7a7wdbcz6i6gqw56fd7d")))

(define-public crate-wowza-rest-rust-0.1 (crate (name "wowza-rest-rust") (vers "0.1.0") (deps (list (crate-dep (name "isahc") (req "^0.7") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "12i45p62n6gsyd40ik5m60ikcinrgys7yl2p45pn0x12hzhcm420")))

(define-public crate-wowza-rest-rust-0.2 (crate (name "wowza-rest-rust") (vers "0.2.0") (deps (list (crate-dep (name "isahc") (req "^0.7") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pp92nxxh5bgn82ddq5rqv2kgzb0fbi7fm1iirfn549w8vw1xwmn")))

(define-public crate-wowza-rest-rust-0.2 (crate (name "wowza-rest-rust") (vers "0.2.1") (deps (list (crate-dep (name "isahc") (req "^0.7") (features (quote ("json" "static-curl"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1508pfnax2mls0jaa7503b16lg230pl1gq018fmsivrm80fmizpv")))

