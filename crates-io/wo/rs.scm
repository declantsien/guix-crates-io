(define-module (crates-io wo rs) #:use-module (crates-io))

(define-public crate-worst-executor-0.1 (crate (name "worst-executor") (vers "0.1.0") (deps (list (crate-dep (name "async-channel") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "async-fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "async-net") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std" "async-await"))) (kind 2)))) (hash "09z58qf98yrlqvpkwqz150camhhr1ic4x3lyj7h4vyq41cdpv1nz")))

(define-public crate-worst-executor-0.1 (crate (name "worst-executor") (vers "0.1.1") (deps (list (crate-dep (name "async-channel") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "async-fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "async-net") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std" "async-await"))) (kind 2)))) (hash "0h6dkgs3rm1k2ci714agp0a6wjhfy29410xy4vwiks79arxhvyx0")))

