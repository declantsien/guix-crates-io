(define-module (crates-io vt mk) #:use-module (crates-io))

(define-public crate-VTMK-0.1 (crate (name "VTMK") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.5") (features (quote ("yaml" "env" "color" "suggestions" "unicode"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "zstd-safe") (req "^4.1.1") (default-features #t) (kind 0)))) (hash "0dlxazag2dj1gs99aij0q7zch27wigymvbg405l72cf420njlhyx")))

