(define-module (crates-io vt pa) #:use-module (crates-io))

(define-public crate-vtparse-0.1 (crate (name "vtparse") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vlpallzkiyrbxiimd3d84pis0w2m8cgaairjgsjpn3dib57023a")))

(define-public crate-vtparse-0.2 (crate (name "vtparse") (vers "0.2.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.1") (default-features #t) (kind 0)))) (hash "133jgg28bqz25m01jwnd9shgii5fvccx6sqbyrcxfa2a6wgmgf6j")))

(define-public crate-vtparse-0.2 (crate (name "vtparse") (vers "0.2.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zx8fbw7pa64id4f5cbzxmr5f0id83b5rcssq3254mdasz2awvsp")))

(define-public crate-vtparse-0.2 (crate (name "vtparse") (vers "0.2.2") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.1") (default-features #t) (kind 0)))) (hash "0k6ir49syjmy7z1kkmj39p3b60hncllxshbw1r9g53mn1ljb279s")))

(define-public crate-vtparse-0.4 (crate (name "vtparse") (vers "0.4.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j4k9882b4lzk538i3vnr2f75r2vv99n3hv0jm3162cab6vsbr6r")))

(define-public crate-vtparse-0.5 (crate (name "vtparse") (vers "0.5.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hkiygwd1px6s2vrb30v048zplm5pyvx2wy73ggham3ba2zvx83q")))

(define-public crate-vtparse-0.6 (crate (name "vtparse") (vers "0.6.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.2") (default-features #t) (kind 0)))) (hash "00alva5ifbliidyszsqyw468awxfnmxwcihcvm1izpjd9hqwjhcg")))

(define-public crate-vtparse-0.6 (crate (name "vtparse") (vers "0.6.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p4cyil2b1mf8sqngjmpjcxa1jzd777cv27l93fq8b30f8wr1kin")))

(define-public crate-vtparse-0.6 (crate (name "vtparse") (vers "0.6.2") (deps (list (crate-dep (name "k9") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "utf8parse") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l5yz9650zhkaffxn28cvfys7plcw2wd6drajyf41pshn37jm6vd")))

