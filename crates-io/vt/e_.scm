(define-module (crates-io vt e_) #:use-module (crates-io))

(define-public crate-vte_generate_state_changes-0.1 (crate (name "vte_generate_state_changes") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1d809ick2gpd47hb160vlcg7cz9033i6s6jxbfw9prn08qlq2gdi")))

(define-public crate-vte_generate_state_changes-0.1 (crate (name "vte_generate_state_changes") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1zs5q766q7jmc80c5c80gpzy4qpg5lnydf94mgdzrpy7h5q82myj")))

