(define-module (crates-io vt t2) #:use-module (crates-io))

(define-public crate-vtt2csv-0.1 (crate (name "vtt2csv") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)))) (hash "05yhk9bi96ccj3vf3risk0dm3h74i5ny81wgywapi3xl9cl1hn1y")))

(define-public crate-vtt2csv-0.1 (crate (name "vtt2csv") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)))) (hash "1srfl9h3p4214iigab3zwcgj5qrzmfw2k59rdynab0akqrf556ni")))

(define-public crate-vtt2srt-0.1 (crate (name "vtt2srt") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1rgggw6dx1f0h0s9wyw8385l9kl8w6fdiwc7bx9adhfr9j87iyfh")))

