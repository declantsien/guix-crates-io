(define-module (crates-io vt fl) #:use-module (crates-io))

(define-public crate-vtflib-0.1 (crate (name "vtflib") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "vtflib-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1l4r6vwq39nq70160pi4w09w8wjv6k0csjwcgvlrncywk60q2jb9") (features (quote (("static" "vtflib-sys/static")))) (yanked #t)))

(define-public crate-vtflib-0.1 (crate (name "vtflib") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "vtflib-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07ahk9aqcd8fdz0rmwc0xj6ay0qvwfhnh87ijbvrlx4977fgdpky") (features (quote (("static" "vtflib-sys/static")))) (yanked #t)))

(define-public crate-vtflib-0.2 (crate (name "vtflib") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "vtflib-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0kpg156xp93jpvgrc6iffg7610laknx930gx3m0yk7ai9pq3cqz8") (features (quote (("static" "vtflib-sys/static"))))))

(define-public crate-vtflib-0.2 (crate (name "vtflib") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "vtflib-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "19sq3yh2bvb8hzvkq2n181nwgxndv1krl1frbzbmgvfvx5pbinyv") (features (quote (("static" "vtflib-sys/static"))))))

(define-public crate-vtflib-sys-0.1 (crate (name "vtflib-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "0d60lkmimrlz5kal7knk6yp828v3ac43zamc6h4lhjsj7fdpbwfl") (features (quote (("static")))) (links "VTFLib")))

(define-public crate-vtflib-sys-0.1 (crate (name "vtflib-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "1jcsjbri2xajz69v4mckmv0dinlydsyakz3ifzyngkw69wigqibl") (features (quote (("static")))) (links "VTFLib")))

(define-public crate-vtflib-sys-0.1 (crate (name "vtflib-sys") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "0sv7y8cfpapznl3jhdikvh3hibfflrdj00br8jq1k7yr42w3md7h") (features (quote (("static")))) (links "VTFLib")))

(define-public crate-vtflib-sys-0.1 (crate (name "vtflib-sys") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "0s99i69b0gdcd5ji83ajmc2hlzn1x77hi6w5m7fv1gz0259ydclf") (features (quote (("static")))) (links "VTFLib")))

(define-public crate-vtflib-sys-0.1 (crate (name "vtflib-sys") (vers "0.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "1x6f8s8rwam6a7g7idwac5x9ci9xfscimb8gd1q1drdsrq2w8np6") (features (quote (("static")))) (links "VTFLib")))

(define-public crate-vtflib-sys-0.1 (crate (name "vtflib-sys") (vers "0.1.5") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "0x1pf53xzj7n47b1rsvi1fs6paipj356hf2yspkylif9b76f7lvv") (features (quote (("static")))) (links "VTFLib")))

