(define-module (crates-io vt s-) #:use-module (crates-io))

(define-public crate-vts-rs-0.1 (crate (name "vts-rs") (vers "0.1.0") (hash "0ildnn78z08m4636vcm75b4g274hwaddj1hgvqid33h8ggicpxlm")))

(define-public crate-vts-rs-0.1 (crate (name "vts-rs") (vers "0.1.1") (hash "1k2vk6amd1bq71rjsj2gff52b469yjniv9l2kwm1kyzvaiy8m7r5")))

(define-public crate-vts-rs-0.1 (crate (name "vts-rs") (vers "0.1.2") (hash "1bmqwzyxfcma0i8jn2m7w9535wff9dqdjw5vv46hiin7j2dlxsq9") (yanked #t)))

(define-public crate-vts-rs-0.1 (crate (name "vts-rs") (vers "0.1.3") (hash "0fpiqwrqyfdwyis9kdx4x825nby2nrvp7xa1ck828bv4w5h0zfgs")))

(define-public crate-vts-rs-0.1 (crate (name "vts-rs") (vers "0.1.4") (hash "1q79nz69pczb9c1r20axlhwc4966z5lv5lx3d34awvl66z0j9jib")))

(define-public crate-vts-rs-0.1 (crate (name "vts-rs") (vers "0.1.5") (hash "18ijzd99a606j9415gfk3q15zphaw4vmkgj72y000fdm5fnzk2qv")))

