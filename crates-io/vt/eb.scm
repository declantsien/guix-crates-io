(define-module (crates-io vt eb) #:use-module (crates-io))

(define-public crate-vtebench-0.1 (crate (name "vtebench") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "terminfo") (req "^0.4") (default-features #t) (kind 0)))) (hash "182gcnxqj3pkbhh71ja1qafby1i2zxxaaln8498h284sgfpgc7ld")))

(define-public crate-vtebench-0.1 (crate (name "vtebench") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "terminfo") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wpw48s095lp214smpxkzdzqm2pxj67c26vnxbch1sh23wfjx42m")))

(define-public crate-vtebench-0.2 (crate (name "vtebench") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "terminfo") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1m9246qkb36fd03pmqwmwb5xy0zmcnc1yqqhwr8rkag44w0djyq5")))

(define-public crate-vtebench-0.3 (crate (name "vtebench") (vers "0.3.1") (deps (list (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0mrpcmqg2kh8sw537n8pr9n1s2y4xs3mrhi8ihhiw5yjpscii3xc")))

