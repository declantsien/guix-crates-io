(define-module (crates-io vt x-) #:use-module (crates-io))

(define-public crate-vtx-bin-0.1 (crate (name "vtx-bin") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vtx") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1") (default-features #t) (kind 0)))) (hash "11m3hhkjk0w2npxqrwvgyypf8vrvnwq0g9qbg2h7ys74228w3kx2")))

(define-public crate-vtx-bin-0.1 (crate (name "vtx-bin") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vtx") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1") (default-features #t) (kind 0)))) (hash "0yy5mriyrf84bd0hamwmcm4nirfb5dshqym8pv0j8k3gzkvkhyb9")))

(define-public crate-vtx-bin-0.1 (crate (name "vtx-bin") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vtx") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1") (default-features #t) (kind 0)))) (hash "067brziniwy7y77w7pdrg89g77lf94zzn0hb6d2d3pb82wbgmgcp")))

(define-public crate-vtx-bin-0.15 (crate (name "vtx-bin") (vers "0.15.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vtx") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0") (default-features #t) (kind 0)))) (hash "05dv2l1yy24a0mw070s8wi8i9vhz15jq12lxhhy3wiyd2fqrnxna")))

(define-public crate-vtx-bin-0.16 (crate (name "vtx-bin") (vers "0.16.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vtx") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qss60xm8560xr3161npa2fgbydiaknbspf1phc4ikddrrba8pg6")))

