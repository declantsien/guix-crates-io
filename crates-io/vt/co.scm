(define-module (crates-io vt co) #:use-module (crates-io))

(define-public crate-vtcol-0.42 (crate (name "vtcol") (vers "0.42.0") (hash "0vnghzpg4mpbszfwilbcd1fr8fckccx1ifr2hxb9afqd0qq0fm9c")))

(define-public crate-vtcol-0.42 (crate (name "vtcol") (vers "0.42.1") (hash "05a44khkqqsa3xzczmfl37h3ljl0nynxxr6fy0fgsa8avgww9qg9")))

(define-public crate-vtcol-0.42 (crate (name "vtcol") (vers "0.42.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15ds9s5im2f9s079q4d1k04lwr8gxdj2q4x8lgfnyys9k918k7bj")))

