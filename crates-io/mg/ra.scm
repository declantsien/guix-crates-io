(define-module (crates-io mg ra) #:use-module (crates-io))

(define-public crate-mgrad-0.1 (crate (name "mgrad") (vers "0.1.0") (deps (list (crate-dep (name "color_space") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)))) (hash "1iqrsq48yh98f9gj3q28z7wv37lmzqikbgbw6zldlx5dkzggqfc7")))

(define-public crate-mgraph-0.1 (crate (name "mgraph") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "182034b5hvvqsgb6dbd9p2g7w67glp62hgkg45mc776fzlx7zg6j")))

(define-public crate-mgraph-0.1 (crate (name "mgraph") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "106ki3y8w0y3vckw12bkz3kjhj1pc2yx9bnrix4v7cpidh96x3f7")))

(define-public crate-mgraph-0.1 (crate (name "mgraph") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vqa3zwmafrxn498appj9h5mw12km6lsd7jr9v4kwhhpxw7pljji")))

(define-public crate-mgraph-0.1 (crate (name "mgraph") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gpkz802wmf1gi5257xzjz4zbmqrdc3431vb4n9f3w4xs7pxp36b")))

(define-public crate-mgraph-0.1 (crate (name "mgraph") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ibixw8w5zhb94x8v1cc2hhcfrz2068pvvr7mfbdj13s58ghbc9w")))

(define-public crate-mgraph-0.1 (crate (name "mgraph") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0njprj745gwr8g0yzyaxnf91znaqqbpmazfzinc1pn0647lr981h")))

