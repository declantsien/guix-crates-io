(define-module (crates-io mg rr) #:use-module (crates-io))

(define-public crate-mgrrs-0.1 (crate (name "mgrrs") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "15427j7zwdsgr1gbw32xslba9yk3wjjvnbmwkjzg9kp9cq80y1dy")))

