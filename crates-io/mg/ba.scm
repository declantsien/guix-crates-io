(define-module (crates-io mg ba) #:use-module (crates-io))

(define-public crate-mgba_log-0.1 (crate (name "mgba_log") (vers "0.1.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.15.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)) (crate-dep (name "voladdress") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1s04d96qz20qkqmzlcmjf986a11wsgb08shmgdls2h1y6lnc2r3x")))

(define-public crate-mgba_log-0.2 (crate (name "mgba_log") (vers "0.2.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.15.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)))) (hash "1wnwdxxnssrbw22i0hkdmjfsaxiyazknjfmhsyqxc2ifdcghf6s8")))

(define-public crate-mgba_log-0.2 (crate (name "mgba_log") (vers "0.2.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.15.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)))) (hash "0z1lzcyq3bq9m4zhpmrfqv4m4yajaxrpdmwgl6q69afl78drq1xp")))

