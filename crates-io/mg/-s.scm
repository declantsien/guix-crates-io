(define-module (crates-io mg -s) #:use-module (crates-io))

(define-public crate-mg-settings-0.0.1 (crate (name "mg-settings") (vers "0.0.1") (hash "199ykcxi74w3j7cgy2nk0xh8xg0ypc77235x86myrka85hdnvs2w") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.2 (crate (name "mg-settings") (vers "0.0.2") (hash "1l6g4g86kc5glas822j3vmjmcyhsy4cln92hz9b4jriyicd6ljgb") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.3 (crate (name "mg-settings") (vers "0.0.3") (hash "0amgyli9xcbfihmbj5lgjqwfzw43w9f1gpziy9zad9y0pn34zifw") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.4 (crate (name "mg-settings") (vers "0.0.4") (hash "1fq08qjnf19w8hxx8a0z6rwjkbfwpm467r7r13n8w0rxiwkzkgn8") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.0.5 (crate (name "mg-settings") (vers "0.0.5") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "01gjcl8zllpxqlmawgzi1xkpirgcsgmj470maa8a9frxpqbj0x51") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.1 (crate (name "mg-settings") (vers "0.1.0") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0f3pfdf0qsfmzai3jqadlp1092i99gkl00n9riy9c9mwazja1j36") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.1 (crate (name "mg-settings") (vers "0.1.1") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0qcrkr0da4c4v0n6iizwhg7f3axlaiwmagzbj5ra9i6mlzpr8l49") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.1 (crate (name "mg-settings") (vers "0.1.2") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13r72n8rmgn34svj4v6j44lqqzn7rxiyr8ji5bpkyclrm2l8gd9k") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.1 (crate (name "mg-settings") (vers "0.1.3") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ps7iqn0g1mmil483rsly6kf0xdiyacc7xz71ssknhpslnj416yw") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.1 (crate (name "mg-settings") (vers "0.1.4") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0143pjh3l6g49g4m9pfb12wrv328wjzk8wk9fb1hddkbciv83vwb") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.1 (crate (name "mg-settings") (vers "0.1.5") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17bksnarqpfci6r6v0mdq5asa15a5xb1xhjw1rs2ymq8y1802zh4") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.1 (crate (name "mg-settings") (vers "0.1.6") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p46gh2v1vsiz1pay59b7x3vqybmdzbzsqc38n2mzlxhbz2b277a") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.2 (crate (name "mg-settings") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "mg-settings-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1c27z8l4fqkbclpibp416nlcbywnrfg8hcmch0z19mmr9as4h285") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.2 (crate (name "mg-settings") (vers "0.2.1") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "mg-settings-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0riyzs2nfbkms3iagl5ras1cgchfz42qz7ls3si7xgvqw6p9q8m5") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.2 (crate (name "mg-settings") (vers "0.2.2") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "mg-settings-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ndp42d02cav7acjc2lv75kif0905vzdlh8ghc573w84a4blvwzy") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.3 (crate (name "mg-settings") (vers "0.3.0") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "06a3qn9889lgl2ddlcx1scq8ksssscy2ph55bx03hy805krwccqq") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.3 (crate (name "mg-settings") (vers "0.3.1") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qlqm902x41yrskcjgnbpbqisyri1qp2mg7g296rjs8al3rx90g3") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.3 (crate (name "mg-settings") (vers "0.3.2") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1n3f0rkhzvnm6irk4ckfasgl1mp1x5zs4s1ii68wakwgnxlhkzh5") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.4 (crate (name "mg-settings") (vers "0.4.0") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0845z2j53gzbr6sfk788iyh6955lym8431yx8py9hpxb40dadmyb") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.4 (crate (name "mg-settings") (vers "0.4.1") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0v5a9nwhnwv6cj4fyrbf5p7q4brxwzsn1yfxqccjgjwid6il94xg") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.4 (crate (name "mg-settings") (vers "0.4.2") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1nsjgky4c9a74c51plb1b9z2l5w34cbiminq2d9zx82ki54k4pph") (features (quote (("nightly"))))))

(define-public crate-mg-settings-0.4 (crate (name "mg-settings") (vers "0.4.3") (deps (list (crate-dep (name "mg-settings-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0779hq3wlx8a1lpr1fwlkk23yg3l5nbk49az9259r8gpfm8qyhy5") (features (quote (("nightly"))))))

(define-public crate-mg-settings-macros-0.0.1 (crate (name "mg-settings-macros") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.9") (default-features #t) (kind 0)))) (hash "1p140ksyqwpah9lgc9ndy9wg05wl73x0zxk9qgq0dh49ind6sxpv")))

(define-public crate-mg-settings-macros-0.0.2 (crate (name "mg-settings-macros") (vers "0.0.2") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.9") (default-features #t) (kind 0)))) (hash "1xqm4b14c6xpgjlcllf3izbjxp6fznmbk6czfnfhmv823abm8igd")))

(define-public crate-mg-settings-macros-0.0.3 (crate (name "mg-settings-macros") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "043lgm5cqn4v54i8b2pdpzzfk4bir7f07izv6iwxsawwpn5g7png")))

(define-public crate-mg-settings-macros-0.1 (crate (name "mg-settings-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0zbqs8ci7z7mp0g59yvx3dik2jmf2icmvm0xm299xs59qfbhqp2m")))

(define-public crate-mg-settings-macros-0.1 (crate (name "mg-settings-macros") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1jxaf6ikm172kkwkd4lvsimw9p87cwacvax0lwviayfpg4ipz5g8")))

(define-public crate-mg-settings-macros-0.1 (crate (name "mg-settings-macros") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "15gaph1iq0xsqa9sqi7dv7i8r1q81yx3q8wnx0zinsp50wj4wfvp")))

(define-public crate-mg-settings-macros-0.1 (crate (name "mg-settings-macros") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "10fwbxq9r1i041hqj6236bxjj5f1nm9hsffags8a3i5zbic360fb")))

(define-public crate-mg-settings-macros-0.1 (crate (name "mg-settings-macros") (vers "0.1.4") (deps (list (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1kwainwjbsxm2gmccl2b6fpx8g89ay191ybngvsj6v8mdaxd6fny")))

(define-public crate-mg-settings-macros-0.2 (crate (name "mg-settings-macros") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "16sar1ilz292vapns8zfzq6a94fgav2dyrj4wanbpnjx3inmcb93")))

(define-public crate-mg-settings-macros-0.2 (crate (name "mg-settings-macros") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "mg-settings") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "083dvj6pr8ydxqm250ym0mngpl9w4q21fd71k3hh8agmsy76s3b4")))

(define-public crate-mg-settings-macros-0.2 (crate (name "mg-settings-macros") (vers "0.2.2") (deps (list (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "mg-settings") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "163iib8razn5c0g2snyx48wnc5mq1mc68gzkg8j8k2w9mr1snair")))

(define-public crate-mg-settings-macros-0.3 (crate (name "mg-settings-macros") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "mg-settings") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0qdc1ssaxjyygl66g4a7naar0gjl3hks2vfynqa50780ah2kl2jz")))

(define-public crate-mg-settings-macros-0.4 (crate (name "mg-settings-macros") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0y5k1z0vjw96smv113m6ggvzkfpq4l7l4l5mswzshp7sfgsibvax")))

(define-public crate-mg-settings-macros-0.4 (crate (name "mg-settings-macros") (vers "0.4.1") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mg-settings") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0hn4i0kslfap72r6xiavmzhvgg44sq7a5p61q2380hf5l4zc94g7") (yanked #t)))

(define-public crate-mg-settings-macros-0.4 (crate (name "mg-settings-macros") (vers "0.4.2") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mg-settings") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "01lp5xllidhpgy84v7dc8a332fm2pq4fnglp3slwmk29rhyghnqn")))

(define-public crate-mg-shamir-0.1 (crate (name "mg-shamir") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hmac") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1k1vsb0c0ra71hvqpmmmlr9pd9va43jw0vzv0wzsgfwk6kvql941")))

(define-public crate-mg-shamir-0.2 (crate (name "mg-shamir") (vers "0.2.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hmac") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0d127vdj0s6wdn6c34hdsb1am6a7a8mjsad9a485413l452583p2")))

(define-public crate-mg-shamir-1 (crate (name "mg-shamir") (vers "1.0.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hmac") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1c5pnlncfvy894pfq60m0gaplihkdjp56pdnl33l6lnfgsjjlgz9")))

(define-public crate-mg-shamir-1 (crate (name "mg-shamir") (vers "1.1.1") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 2)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0cqmn8nr24h4pfia118j16m3f60smlhdxsbv9kr5sihr6k59zzav")))

(define-public crate-mg-shamir-1 (crate (name "mg-shamir") (vers "1.2.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 2)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0nnbpnp2d28za2173pz0ppxx3hmsjykwwymdssl2b7yf0z72g724")))

(define-public crate-mg-shamir-1 (crate (name "mg-shamir") (vers "1.3.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 2)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "1jkan18kn5f4ns20kwh1lmwyx7r54zaf40jkh396abdh1qr75prs")))

