(define-module (crates-io ub un) #:use-module (crates-io))

(define-public crate-ubuntu-ami-0.1 (crate (name "ubuntu-ami") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0d1jphhmjicq65cz14bk81zsrndvga464i6apd5hapvsvikscvba")))

(define-public crate-ubuntu-ami-0.1 (crate (name "ubuntu-ami") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1xj1xh5m6drwx6k2vljgxw9z7h3g882sf9ig20iys3635kr2yc6y")))

(define-public crate-ubuntu-ami-0.1 (crate (name "ubuntu-ami") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0il4k3l8srm7gggmqjajkr4gz8p39rjjsv127fp674js5dqj6m9q")))

(define-public crate-ubuntu-ami-0.2 (crate (name "ubuntu-ami") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "11aic3rn5fn0bbm2lhkqxpk2c1x80py8npdn2782g7v8k0lzqsnr")))

(define-public crate-ubuntu-distro-info-0.2 (crate (name "ubuntu-distro-info") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "distro-info") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0c3s8f5r7jryhicaqkpm63d1p917pw2zb782yc5m5nhhx3l9gi7p")))

(define-public crate-ubuntu-distro-info-0.3 (crate (name "ubuntu-distro-info") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "distro-info") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0vg76s5myfimpz6m0lh7pzkahsj17146n5d92mfbf676vfp7pqqa")))

(define-public crate-ubuntu-version-0.1 (crate (name "ubuntu-version") (vers "0.1.0") (deps (list (crate-dep (name "err-derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0k6yr9if9h358xv4wvmyr0qgn8fb8pm395ybx1d5a7v6qls5j6p8")))

(define-public crate-ubuntu-version-0.1 (crate (name "ubuntu-version") (vers "0.1.1") (deps (list (crate-dep (name "err-derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z2mwk09gkx1ddgynld89ayi2vss8ia2n6c5cbyq6gk3jgn6i7sh")))

(define-public crate-ubuntu-version-0.1 (crate (name "ubuntu-version") (vers "0.1.2") (deps (list (crate-dep (name "err-derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0c1n462mga22zrr8b2p1gqijyx1sqx1fhc32dby4sjpqmrwj8zwh")))

(define-public crate-ubuntu-version-0.1 (crate (name "ubuntu-version") (vers "0.1.3") (deps (list (crate-dep (name "err-derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kyqcq9ajh2r1mbjv1qg0fx1518gwfhi956j3n24f1kn1928r8sh")))

(define-public crate-ubuntu-version-0.1 (crate (name "ubuntu-version") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "err-derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0igx29xgjg3yk9w8gwkfrbik3v46qv4a2gf7ba0jii6w2kqx5jf4")))

(define-public crate-ubuntu-version-0.1 (crate (name "ubuntu-version") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1vyiisaj1i55szkklsvz47mb4f72y43jv8fnpkr9xwm4x5h9fvd0")))

(define-public crate-ubuntu-version-0.2 (crate (name "ubuntu-version") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "02m232803g2rxwxhqv5jdkjpbbccq5zi0822dq4ij9ny306qp514")))

(define-public crate-ubuntu-version-0.2 (crate (name "ubuntu-version") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "15ym15q6i682cbx58vb3r9km17rg1arxin061miadpn1fs97kj4m")))

(define-public crate-ubuntu-version-0.2 (crate (name "ubuntu-version") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07ia2xrxvyim6hfp1pz0xgdg0hg1dhycagrv5iwva6xkbbb4fiah")))

(define-public crate-ubuntu-version-0.2 (crate (name "ubuntu-version") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0y41nnd489rd3qmdy8fy1gl1x7p702hc6yyripp0xla4bvdpb0fm")))

(define-public crate-ubuntu-version-0.2 (crate (name "ubuntu-version") (vers "0.2.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1z8lcwylhkh4f5qyph2ymhgkbcv66874vhwpmifknxry1wli39dc")))

(define-public crate-ubuntu-version-0.2 (crate (name "ubuntu-version") (vers "0.2.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "os-release") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0h5qd1v5g8z32bp8ixcczdv8mk16szi2wcvj1j0bl8hjx0klsxpy")))

