(define-module (crates-io ub lk) #:use-module (crates-io))

(define-public crate-ublk-0.0.0 (crate (name "ublk") (vers "0.0.0") (hash "00f3q50janrvyi7bv51i74nd8bgglqfn37rkd3v7hilwdn24rkgm")))

(define-public crate-ublk-sys-0.0.0 (crate (name "ublk-sys") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0nd26rr32shvdkxn9bpzjipry38wzl24j2whl9h05snn5xvv9x0p")))

