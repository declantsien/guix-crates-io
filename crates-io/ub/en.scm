(define-module (crates-io ub en) #:use-module (crates-io))

(define-public crate-ubench-0.0.0 (crate (name "ubench") (vers "0.0.0-alpha0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-time") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "13h1gqs4ah219wnimn864i29hs7x4l1ja1y25yj5v8gfdl7fcmzf") (features (quote (("reporters" "embedded-hal" "json") ("metrics" "cortex-m" "riscv" "embedded-time") ("default" "embedded-hal" "json") ("all" "std" "metrics" "reporters")))) (v 2) (features2 (quote (("unicode-width" "dep:unicode-width") ("std" "serde?/std" "owo-colors/supports-colors" "unicode-width") ("riscv" "dep:riscv") ("json" "dep:serde") ("embedded-time" "dep:embedded-time") ("embedded-hal" "dep:embedded-hal" "dep:nb") ("cortex-m" "dep:cortex-m")))) (rust-version "1.62.0")))

(define-public crate-ubend-0.1 (crate (name "ubend") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1i2i9pb77kn9yyfs2khcjgvcycjylwi2a5d6bzbmq6l9bnqlvas1")))

(define-public crate-ubend-0.1 (crate (name "ubend") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "15ygnhbhkgflfdghsp85rfghnfqsx1y97kjk2gk3dazig5ljwdlw")))

(define-public crate-ubend-0.1 (crate (name "ubend") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0j63gyvzs1hcvqhqrpv78phd0pq4rbdd30a04i51l0b71mnh6n7v")))

(define-public crate-ubend-0.2 (crate (name "ubend") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1h8zn748jv1fwgll2sxwm3m1jghbb2hkzkx6117bkiq99f8k5vkq")))

(define-public crate-ubend-0.2 (crate (name "ubend") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "09jwlnv1irffskw9d2wa7bfmsv700pz4wckwb58l6jcg7c1i2gs5")))

(define-public crate-ubend-0.2 (crate (name "ubend") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1as6hjb5n1ygarn4xw55yxg8vylbg2czkyfy7h96p192dqp6sfc2")))

