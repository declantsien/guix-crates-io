(define-module (crates-io ub yt) #:use-module (crates-io))

(define-public crate-ubyte-0.9 (crate (name "ubyte") (vers "0.9.0") (hash "1kfpszjdadbrwwkw43kdl0wdgqbbjixd4rq9n8qf97gvb131pag3") (yanked #t)))

(define-public crate-ubyte-0.9 (crate (name "ubyte") (vers "0.9.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "030v4z9sfnrsdg1g2m0dj5vmzpjjh44yr7mf4sbg6w0qzrpp2z06") (features (quote (("default")))) (yanked #t)))

(define-public crate-ubyte-0.10 (crate (name "ubyte") (vers "0.10.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0yc4iazjzjkmwzg80bjf4zw3g05n5xph27n05q7234afqzhyqxl5") (features (quote (("default")))) (yanked #t)))

(define-public crate-ubyte-0.10 (crate (name "ubyte") (vers "0.10.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1zp8x55w57dkcy20vnc50izsjcgz7mj9b0d9z3i5v188wywnnxa2") (features (quote (("default"))))))

(define-public crate-ubyte-0.10 (crate (name "ubyte") (vers "0.10.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "152fs2qpcy2xd16av6wj91z1qfznlpyxfjp1g6xjj6ilcgr2k3m5") (features (quote (("default"))))))

(define-public crate-ubyte-0.10 (crate (name "ubyte") (vers "0.10.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1rlg6sr14i3rd4kfhrwd7b7w7krlg6kpjxkd6vcx0si8gnp0s7y8") (features (quote (("default"))))))

(define-public crate-ubyte-0.10 (crate (name "ubyte") (vers "0.10.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1spj3k9sx6xvfn7am9vm1b463hsr79nyvj8asi2grqhyrvvdw87p") (features (quote (("default"))))))

