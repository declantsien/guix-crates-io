(define-module (crates-io ub pf) #:use-module (crates-io))

(define-public crate-ubpf-0.1 (crate (name "ubpf") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "likely") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ubpf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0daax0l0q39sbk1dsx028p7s0g4xzbkwmmvk2dky6sj2cx5mc6ij")))

(define-public crate-ubpf-rs-0.1 (crate (name "ubpf-rs") (vers "0.1.0") (hash "1088vmp6ajn30nskjvkqh95ym7dwl419hda96kfbafs8hibw3rnh")))

(define-public crate-ubpf-sys-0.1 (crate (name "ubpf-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "17anb9iaphgnc38y3anfdra96v6b6p0jxs1qbxp5cldax4p8n8xd")))

(define-public crate-ubpf-sys-0.1 (crate (name "ubpf-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0arss3kqmhwapyw1dr8xk4gph2m5gszfkbys1qsmrxjky5da0gdd") (links "ubpf")))

