(define-module (crates-io ub it) #:use-module (crates-io))

(define-public crate-ubits-0.1 (crate (name "ubits") (vers "0.1.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safe-transmute") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1n5sba3plnkk1nzbrhdnfy289bbvvgl83lbl1qhqmlvfs7rlkasf")))

(define-public crate-ubits-0.2 (crate (name "ubits") (vers "0.2.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safe-transmute") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "17kzmw9xlaa8wbfnvll1qrab2j4qgpj2kllprsc93glwinglsjmr")))

(define-public crate-ubits-0.2 (crate (name "ubits") (vers "0.2.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "safe-transmute") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0wzj62ilqj9y2h7kryc290dbv6b82gg9hidqsfvwgjngv1azpq51")))

(define-public crate-ubits-0.3 (crate (name "ubits") (vers "0.3.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "safe-transmute") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1x2sspizsm0rmqjd963p0qk728c61jnmhqn2x168wdydmdyxfcaw")))

(define-public crate-ubits-0.3 (crate (name "ubits") (vers "0.3.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "safe-transmute") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1vasdamaydy62k0xzxz14z9cdya08s7wpz3pymfshx3xqfyfwxrf")))

