(define-module (crates-io ub us) #:use-module (crates-io))

(define-public crate-ubus-0.1 (crate (name "ubus") (vers "0.1.0") (deps (list (crate-dep (name "storage_endian") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0w4k4hwhpgbgq5wzbxvxph30s8cpjamrw3wyd3pgh4nxnlc10a06") (features (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1 (crate (name "ubus") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive" "std"))) (kind 0)) (crate-dep (name "storage_endian") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pl4xld9jkpfr93q63jd3x7irygqpbfmkah3bp21qnwipmbz02cw") (features (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1 (crate (name "ubus") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "storage_endian") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "046bdnrna4c8ljmksa2rcjh5mcbnp292j4qn3dy3y6fsj64amj3f") (features (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1 (crate (name "ubus") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "storage_endian") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "0yyzyaciw4xcw5zsbhgxxxqak6z18s1wd47a16mp355hb0kn6y6n") (features (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1 (crate (name "ubus") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "storage_endian") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "0p2jj30c40zhlqib9712ri24pf5hdyrnsmrdcy3gc2y907s4bkzk") (features (quote (("no_std") ("default"))))))

(define-public crate-ubus-0.1 (crate (name "ubus") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "storage_endian") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "0aigi5fnfzrwcm57ai2976j161mwjx61h8wr1rlaqzgrq3v7jalv") (features (quote (("no_std") ("default"))))))

