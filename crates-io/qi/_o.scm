(define-module (crates-io qi _o) #:use-module (crates-io))

(define-public crate-qi_openapi-0.1 (crate (name "qi_openapi") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "std"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1rnv3vxp3lrcz3k19xzqys44pgr8h9cp5m8ynbkf68gn2vacq4b0") (features (quote (("std" "serde/std") ("default" "std"))))))

