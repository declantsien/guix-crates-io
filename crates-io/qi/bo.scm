(define-module (crates-io qi bo) #:use-module (crates-io))

(define-public crate-qibo-0.0.1 (crate (name "qibo") (vers "0.0.1") (hash "1mspqqd640mjrg1mhl9lgfyqf87f462k8gpyxawshn229dnvsyn7") (rust-version "1.60.0")))

(define-public crate-qibo-core-0.0.1 (crate (name "qibo-core") (vers "0.0.1") (deps (list (crate-dep (name "enum_dispatch") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0ad40z7v4vkivpdbf6qb4pbvsvy9f8rn51x1pp7ifsgljcccy4xy") (rust-version "1.60.0")))

(define-public crate-qibo-core-c-0.0.1 (crate (name "qibo-core-c") (vers "0.0.1") (hash "0n22a9wqxi0s1kxwqhfd4l5n4wgxvn94gfgb20sv3dp9zzxmnrjl") (rust-version "1.60.0")))

(define-public crate-qibo-core-cxx-0.0.1 (crate (name "qibo-core-cxx") (vers "0.0.1") (hash "15mnk3b08xkm8lj6rvzy2a5l0dm7wzgzms712ibq2m4ada5yffim") (rust-version "1.60.0")))

(define-public crate-qibo-core-f-0.0.1 (crate (name "qibo-core-f") (vers "0.0.1") (hash "19kirnbi2g3j2rlgqqkrkwhnxixp5lljv9iz4l16v69v238fn79g") (rust-version "1.60.0")))

(define-public crate-qibo-core-jl-0.0.1 (crate (name "qibo-core-jl") (vers "0.0.1") (hash "1iy8vgp1qf6x2hbxch7zv3vgnqzkjvm9v4r5snp4x5dkb7pm9xn4") (rust-version "1.60.0")))

(define-public crate-qibo-core-py-0.0.1 (crate (name "qibo-core-py") (vers "0.0.1") (hash "0w8dglhllcc4scp2fyl5nrlmb10ci9clfmh7dhy44hbq6kx9ij91") (rust-version "1.60.0")))

(define-public crate-qiboc-0.0.1 (crate (name "qiboc") (vers "0.0.1") (hash "1vszjva03hwlhf6n1j6lmcl68jgkn9yz4j6brp7vqp151fbwsb22") (rust-version "1.60.0")))

(define-public crate-qibocal-0.0.1 (crate (name "qibocal") (vers "0.0.1") (hash "1avl2chp6wc57lpgf10izk4lsd279yh5rfmyi00smjg75m62yqz9") (rust-version "1.60.0")))

(define-public crate-qibolab-0.0.1 (crate (name "qibolab") (vers "0.0.1") (hash "07rp66q2kynj8i344r8lv95lrrka8zblx0fv7n2q2lygzrdlq773") (rust-version "1.60.0")))

