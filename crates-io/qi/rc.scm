(define-module (crates-io qi rc) #:use-module (crates-io))

(define-public crate-qirc-0.0.1 (crate (name "qirc") (vers "0.0.1") (hash "1r23bkj81w4ms2mcqaibcwq42983wfrry9wlyzhp901vmfiz6x1i") (yanked #t)))

(define-public crate-qirc-0.0.2 (crate (name "qirc") (vers "0.0.2") (hash "1dzalba7yhsfvkk6ghj9fq9dg969w09fq46nz93823lvmcnhy7sr")))

