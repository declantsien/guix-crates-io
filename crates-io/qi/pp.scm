(define-module (crates-io qi pp) #:use-module (crates-io))

(define-public crate-qippy-0.0.1 (crate (name "qippy") (vers "0.0.1") (deps (list (crate-dep (name "nalgebra") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "161awxgk8mdi2d8rafbjg936lhyldxkff967agd9kvchzs272bd9")))

