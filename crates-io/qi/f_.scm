(define-module (crates-io qi f_) #:use-module (crates-io))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "15l6l448hsb68kxc630gp7h88604qird9vk7h29nbyms3051fmxa")))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gsls9vshfbkw5x171jign9mn78bv79fv8jbx0jgfvndnr1vyj8s")))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rvpgdq1ym27008mppfixs0y06k8mr9fjwjv8dzbl1fmnqfiwm42")))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "06hmk3m5wcqlzfckmfb5v9jzly1jqlglqbnwpahdh6rb0qmbmsbr")))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1a9cj72i363g08s5kbqw91p0c89zwn9jsh4h6jsvg41nrqwk2dzm")))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1f4cw5m1nffhsg6lfx2cpykc3i6fn2m54dlalhfnj96p2wgnk2hq")))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fl9q5jmk11d2zksm8blynzm206hl962ni3qzhrw3lz29nfdgjv7")))

(define-public crate-qif_generator-0.1 (crate (name "qif_generator") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0s6sihg27mci9grx32ni3pyhij781ap4y54nmn8d84d11n5vw50h")))

(define-public crate-qif_parser-0.0.1 (crate (name "qif_parser") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "071y0lqacvixna96rqy3q3pj4v7zls71c4rs6cgz73f8ydylg878")))

(define-public crate-qif_parser-0.0.2 (crate (name "qif_parser") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0y38ypxhzn91rzqhvwbl1x61ww5h6a8hq8kfyk20ni0zq46b2vns")))

(define-public crate-qif_parser-0.0.3 (crate (name "qif_parser") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0r93p86f0afl271ka5mwpkvy8a1bwycycgsvvhpkpjs7hji8hayj")))

(define-public crate-qif_parser-0.0.4 (crate (name "qif_parser") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k9h0ngrn9d3i2wxiw1yg0v96pi7a38210xavl19ngahqcs115iw")))

(define-public crate-qif_parser-0.0.5 (crate (name "qif_parser") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x7nlrvrfhrij4w8gx1vn4dmff1ckfxw8g5kxadxxxba2xcmnfx7")))

(define-public crate-qif_parser-0.0.6 (crate (name "qif_parser") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qpdk1sai6c3xpl3mh72z13r2y0a11mnx0ciqq5qh4p5avn5ml7s")))

(define-public crate-qif_parser-0.1 (crate (name "qif_parser") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "14lpiz9503pr1d4ablfwi2f3mvg15cnbv8b8kc0xwcivn6a4kbcs")))

(define-public crate-qif_parser-0.2 (crate (name "qif_parser") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1w9w4syigg2n4bz5m9plp1ip5kfnfknqrv2g7zx3rcaa2hljpifr")))

(define-public crate-qif_parser-0.3 (crate (name "qif_parser") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pajb5rslhw5wnjldjia99pv10x6arqi95alayywb45b85glmpv3")))

(define-public crate-qif_parser-0.4 (crate (name "qif_parser") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1m0cbxikj43f474az01277b64g2mi3p2892a3cxxwhgszgc4pbb8")))

