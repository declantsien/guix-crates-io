(define-module (crates-io qi it) #:use-module (crates-io))

(define-public crate-qiiterm-0.0.1 (crate (name "qiiterm") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.8") (default-features #t) (kind 0)))) (hash "0nhyrcxmfcm0vcrznck0inp6vhc54561rkcfqdz791yvwn7pmz18")))

