(define-module (crates-io qi fi) #:use-module (crates-io))

(define-public crate-qifi-rs-0.3 (crate (name "qifi-rs") (vers "0.3.0") (deps (list (crate-dep (name "bson") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "mongodb") (req "^1.1.0") (features (quote ("sync"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)))) (hash "1482vwy0qkpfhqgxii6fmgmhxr75jbz4gaw9yyn7gpa1f00qllpz")))

