(define-module (crates-io qi k-) #:use-module (crates-io))

(define-public crate-qik-rs-1 (crate (name "qik-rs") (vers "1.0.0") (deps (list (crate-dep (name "serial") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "02sqpf0krdqjn0wjmwyi72ql5fsdpvgmwh7q9zcymx10z6qqz028")))

