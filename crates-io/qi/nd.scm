(define-module (crates-io qi nd) #:use-module (crates-io))

(define-public crate-qindex_multi-0.1 (crate (name "qindex_multi") (vers "0.1.0") (deps (list (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n1w05hp811j4yqcwmi27mjrhf8m5pglyy5rk8l0qkc78wd5jiry")))

(define-public crate-qindex_multi-0.1 (crate (name "qindex_multi") (vers "0.1.1") (deps (list (crate-dep (name "qcollect") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "003v2z4yw19r6s5j3s1qn8m98r53ql3vfj2jwv9g9hhk9prck91p")))

(define-public crate-qindex_multi-0.1 (crate (name "qindex_multi") (vers "0.1.2") (deps (list (crate-dep (name "qcollect") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0xxv20y571iw5j2smqj2apy77alwwa0j56ibjxl3w7m6m95d7dhc")))

(define-public crate-qindex_multi-0.2 (crate (name "qindex_multi") (vers "0.2.0") (deps (list (crate-dep (name "qcollect-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "096s6q5s7ii2b21w6r5cr60d33144x3l0dpla4617f1basf2y8rv")))

(define-public crate-qindex_multi-0.3 (crate (name "qindex_multi") (vers "0.3.0") (deps (list (crate-dep (name "qcollect-traits") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0mf3s1d08fck0am8dv82hjrwnck3bdq71dxrvq61z83r1jsg4rwz")))

(define-public crate-qindex_multi-0.4 (crate (name "qindex_multi") (vers "0.4.0") (deps (list (crate-dep (name "qcollect-traits") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3") (default-features #t) (kind 0)))) (hash "1810h976ihmnd17y3g5bdildn0w3a2gsx31520kdxj1mdg3izr6v")))

