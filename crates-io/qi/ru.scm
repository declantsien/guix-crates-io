(define-module (crates-io qi ru) #:use-module (crates-io))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "1ry1s7hp53kb546y7fv7synms0w0zwxcfg5zx56gxz00z36pfsc6")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "18vlfgmmx329nzd3zw81nx1b0wly84kv8ys3yi0gfg0fqijzh7iv")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "1lkc5an0nljyzfmbvd1dza9iwjijkahiaq8nchb0iyhwh6ysiqld")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "1lp0wdpb5w2mhpncqrsj7slsxzcfhzlqv63v9jcgp5fmbmhwi2yh")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "0q1bgyb2w7lb7l681r53siivdcnlfdjgzwg8sfq5isi59c4ay2xa")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "189v0ni1by02s5zn1pa0cfnkc5ghbsdh8zpfr4fhibch9dlarp3w")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "0zgv6nyz9waq92lrz3qg4m8z3km3yfa4jy4nnb2sr6z135gvirv3")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.7") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)))) (hash "15d3781nar99pj1zxcpiajwd9i2dcbms8qaxgidwcrhbfybm7qca")))

(define-public crate-qirust-0.1 (crate (name "qirust") (vers "0.1.8") (deps (list (crate-dep (name "image") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0mp090122amyqd6qnzrinmcq8g281srnjbla3xlxz2l7mbgdr68a")))

