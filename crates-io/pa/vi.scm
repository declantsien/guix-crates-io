(define-module (crates-io pa vi) #:use-module (crates-io))

(define-public crate-pavise-0.1 (crate (name "pavise") (vers "0.1.0") (hash "0sgv0w5agzjvvhqhp4p4frh4p9ran5ziddhz01lfxj99kpbqljpy") (yanked #t)))

(define-public crate-pavise-0.0.1 (crate (name "pavise") (vers "0.0.1") (hash "0kb5n3khz299napfz0f64d4rdfbagsnwwlhy2k50n5fpcgdpn7nv")))

