(define-module (crates-io pa pm) #:use-module (crates-io))

(define-public crate-papm-0.1 (crate (name "papm") (vers "0.1.0") (hash "1gicqcl1mjs3rgry3ikr5kb6bbcwv6bma6jcgx8clc82ibzkw6ch") (rust-version "1.77.2")))

(define-public crate-papm-0.2 (crate (name "papm") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "env"))) (default-features #t) (kind 0)))) (hash "12fsp82xqw711s9lycqvdh0nlfwxxzz1rslmq37imcylmsqw7jjs") (rust-version "1.77.2")))

(define-public crate-papm-0.3 (crate (name "papm") (vers "0.3.1") (deps (list (crate-dep (name "argon2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11mi75j64mmy5gwsxk19bzficcs4c217ck68j24g8rrbx65ys965") (rust-version "1.77.2")))

(define-public crate-papm-0.3 (crate (name "papm") (vers "0.3.2") (deps (list (crate-dep (name "argon2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1c20j07r9cir0zrsd6qy0qfdr3kspbkgqxlhhj2a20wk4albrjp2") (rust-version "1.77.2")))

