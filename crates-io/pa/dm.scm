(define-module (crates-io pa dm) #:use-module (crates-io))

(define-public crate-padme-core-0.0.1 (crate (name "padme-core") (vers "0.0.1") (deps (list (crate-dep (name "enum_dispatch") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)))) (hash "1nybfjh2lvlvfiywdn3m1frdbaq1l31b8s6z8s8imqhpy8qg6b0n")))

(define-public crate-padme-padding-0.1 (crate (name "padme-padding") (vers "0.1.0") (hash "09s6gqyh4vhk4k365sd4xcj3266xzqid7k7lfg1950ixms60bnpq")))

(define-public crate-padme-padding-0.1 (crate (name "padme-padding") (vers "0.1.1") (hash "0nlwphcpaifi3yp3lfcyr0zrm0ki0hvc4pf2y63pj6i96gyybx9p")))

