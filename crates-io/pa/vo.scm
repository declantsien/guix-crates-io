(define-module (crates-io pa vo) #:use-module (crates-io))

(define-public crate-pavo-common-0.1 (crate (name "pavo-common") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1q42323xa4717qi737xbfj4archf5fsjj91pwqr8kcbxphnfdf75")))

(define-public crate-pavo-common-0.1 (crate (name "pavo-common") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "041b9p761agrhazlknbnmpvz5pjj75hw6la1kvkna1dkl763cnks")))

(define-public crate-pavo-traits-0.1 (crate (name "pavo-traits") (vers "0.1.0") (hash "0kdgqhnqcpa7cpwsn1a2x8rp0ag0hckwgyida0frxi24xjx6zkk4")))

(define-public crate-pavo-traits-0.1 (crate (name "pavo-traits") (vers "0.1.1") (hash "1vjvp3xrdlg47kjvxgfkpkm8lq5bn9ibinqgg1yhphki1y4id6r8")))

(define-public crate-pavo-traits-0.1 (crate (name "pavo-traits") (vers "0.1.2") (hash "0y44mvnwyz9pf845lkx150bawcffvwvz5frgm943804a4fssfvyv")))

(define-public crate-pavo-traits-0.1 (crate (name "pavo-traits") (vers "0.1.3") (hash "1v13x6m605ry1cypg0kf5a9b9kx1dc65a5c2cnr599l97rcphvx7")))

(define-public crate-pavo-traits-0.1 (crate (name "pavo-traits") (vers "0.1.4") (hash "0112mnw79fq4jvbvy2cd786vy78ymcw8dyclrhsasl0ax6asib23")))

(define-public crate-pavo-traits-0.2 (crate (name "pavo-traits") (vers "0.2.0") (hash "0nspwd4gamq4mmxdq5182ca803ja7dn4jqjvlbp2xqd8cmdnkwg0")))

(define-public crate-pavo-traits-0.3 (crate (name "pavo-traits") (vers "0.3.0") (hash "18inc29pzhrc7g4a6afa6ann6f35vw6im6akifnaakxlwxjz9q9f")))

