(define-module (crates-io pa r_) #:use-module (crates-io))

(define-public crate-par_io-0.3 (crate (name "par_io") (vers "0.3.7") (hash "0sdhj6gq20d7n7p2ccpa7719c3avypp8jzf16dsd6l0cr1f5h27p") (rust-version "1.60")))

(define-public crate-par_io-0.3 (crate (name "par_io") (vers "0.3.8") (hash "194ja1lac784qj9mf0xb4g6w8nlavx3sq44xwcmxwkz0rdyx9y44") (rust-version "1.60")))

(define-public crate-par_io-0.4 (crate (name "par_io") (vers "0.4.2") (hash "0qpd36pb5s1whrh672adcxh2wa5hm5mrfch2qldlbx70dwm0fp58") (rust-version "1.60")))

