(define-module (crates-io pa lg) #:use-module (crates-io))

(define-public crate-palgrad-0.0.1 (crate (name "palgrad") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2") (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("jpeg" "png"))) (kind 0)) (crate-dep (name "palette") (req "^0.5") (features (quote ("std"))) (kind 0)))) (hash "0dmdpqk46z4imykmh6i14nvi2ar4bzy8j02xra2jxz6a60x7a6q6")))

