(define-module (crates-io pa sp) #:use-module (crates-io))

(define-public crate-paspio-0.1 (crate (name "paspio") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1a0vs4704rpdf8ng38gyk70f2576zm557srvwa4ksigzgd5fqx1f")))

(define-public crate-paspio-0.1 (crate (name "paspio") (vers "0.1.1") (hash "18l878zd8varh1ircq98dgfmwasq5lqw89g0vc9c1c60grnym6g0")))

(define-public crate-paspio-0.2 (crate (name "paspio") (vers "0.2.0") (hash "1hfgh5hv08i2lihagr45g5y8169n02f6fx9x719rr0gqcz5rn2r9")))

(define-public crate-paspio-0.2 (crate (name "paspio") (vers "0.2.1") (hash "0zajl5f7i5agmsj8kc6vmgk809k1rcch3gyy2c7rm6yw3s76fkhz")))

(define-public crate-paspio-0.2 (crate (name "paspio") (vers "0.2.2") (hash "1c7jfxy22p4g6r981aywlzgwc9jslb2q7jn3n61qbpcdcwk0w02a")))

(define-public crate-paspio-0.2 (crate (name "paspio") (vers "0.2.3") (hash "0xbvvzgh5nw2mfpbbj4qlwynswyz64a9sdb5qrpskn2468xkd24f")))

(define-public crate-paspio-0.2 (crate (name "paspio") (vers "0.2.4") (hash "0l9y6gjbhysipwz92yqlvidkdnskjjmd906q444kiza2cvyy4dn6")))

(define-public crate-paspio-0.3 (crate (name "paspio") (vers "0.3.0") (hash "0agllvdiv3z2nycpvcigwwhlw9f7fhvhgwk03rykrv9xyarkr5c4")))

(define-public crate-paspio-0.3 (crate (name "paspio") (vers "0.3.1") (hash "06v72c21zd2i8q2c7z13jzijzax4ya1hxv0wcswx7mqnwg8qxyng")))

(define-public crate-paspio-0.3 (crate (name "paspio") (vers "0.3.2") (hash "0k1wzfymcm36qydpg75xwfbggfzm3fb84fc1jamdsydxp178b71n")))

(define-public crate-paspio-0.3 (crate (name "paspio") (vers "0.3.3") (hash "0n8a8w7pl9b0fmk8l19p6n1qxkcgr0bnf5lqv29z8vc2wxz2jkl8")))

(define-public crate-paspio-1 (crate (name "paspio") (vers "1.0.0") (hash "1hdw9nlp0q5sxcsklbnljzdw02qhj3qcph0jj2jg5z13dfdq17s8")))

