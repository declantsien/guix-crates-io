(define-module (crates-io pa go) #:use-module (crates-io))

(define-public crate-pagong-0.1 (crate (name "pagong") (vers "0.1.0") (deps (list (crate-dep (name "atom_syndication") (req "^0.9.0") (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1pm559nfkfy1v1psn7fgc0rkdh38majkwy9wnl1xjl6xyps1l2zz")))

(define-public crate-pagong-0.1 (crate (name "pagong") (vers "0.1.1") (deps (list (crate-dep (name "atom_syndication") (req "^0.9.0") (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0ngy697w16dgxv530wac49b7lh2fbz1pvcwiy70jrkissdfxwipq")))

(define-public crate-pagong-0.2 (crate (name "pagong") (vers "0.2.0") (deps (list (crate-dep (name "atom_syndication") (req "^0.10") (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (kind 0)) (crate-dep (name "hyperbuild") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8") (kind 0)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)))) (hash "1p1s2m3w0xs6w11xshz9vpfim7w0s422hjzcr7zdiwfvs8yr6aw8")))

