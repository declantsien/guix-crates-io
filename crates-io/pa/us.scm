(define-module (crates-io pa us) #:use-module (crates-io))

(define-public crate-pausable_clock-0.1 (crate (name "pausable_clock") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0j5zplili7dda1bdzkx6xr9gn812c76qh7r7p44ngvlx7d3yl16q")))

(define-public crate-pausable_clock-0.2 (crate (name "pausable_clock") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1a3wj2fp6b0n38c8gcz3bfbxlkwy5ar4csbv6kp6yq7h78f13baa")))

(define-public crate-pausable_clock-0.3 (crate (name "pausable_clock") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.3.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1zjha8nypx950nl9m3h8fgr2c8r6ccbiqiqimyqvi0bd823gqn2y")))

(define-public crate-pausable_clock-0.3 (crate (name "pausable_clock") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.3.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "15nd0543is1v9bsqlmvnf4hamf0lmqrf2ni1hki6sza87nm9spsg")))

(define-public crate-pausable_clock-0.3 (crate (name "pausable_clock") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.3.5") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "12xarj7lv9zclrbnbnd7hif38rkagfd1ppvyacb24n79lvm2rry0")))

(define-public crate-pausable_clock-1 (crate (name "pausable_clock") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5.0") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "162is1bwaqz7n3vab9ijr34xy87xdnrjaskh8xrd56hvlzfqxvv3")))

(define-public crate-pausable_clock-1 (crate (name "pausable_clock") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5.0") (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "0id71p05ld92gv6l0flaarx8dpkgk52b58bnk5s180yx9r60ggsk")))

(define-public crate-pausable_future-0.1 (crate (name "pausable_future") (vers "0.1.0") (deps (list (crate-dep (name "pin-project-lite") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "18drjzr4nj75gq8rgqiafr3rc4cgf84m8dsm5p4zgfvsdjhc0i99")))

(define-public crate-pause-0.1 (crate (name "pause") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "1rb1w6phl6cl1qpwdd7nd0ibhz17j9wibwwv5gpirjsj0shi313p")))

(define-public crate-pause_console-0.1 (crate (name "pause_console") (vers "0.1.0") (hash "1r8bsr5dvwgjqcd17baqy5gjgg61778wsvz1fy6zcqp3l7bhpfki")))

(define-public crate-pause_console-0.1 (crate (name "pause_console") (vers "0.1.1") (hash "1q6ljq016jgqpfy2vgryq28g9hkm7n57jyq7f006ycf7f5176ds0")))

(define-public crate-pause_console-0.1 (crate (name "pause_console") (vers "0.1.2") (hash "104m9wpwshvzs13s4gcd2sa45n47k8z39357j8k5wd6idd0a6zpq")))

(define-public crate-pause_console-0.1 (crate (name "pause_console") (vers "0.1.3") (hash "1wqpn8vvdp79kpmwaklqs0xq0glf67h6m65kva6wn9m21y11ma8j")))

(define-public crate-pause_console-0.2 (crate (name "pause_console") (vers "0.2.0") (hash "0rfrpbpk49h3wvyvkncd1qhjx9slalr1hp32yd6mr0wnnvdk7ad9")))

