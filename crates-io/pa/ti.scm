(define-module (crates-io pa ti) #:use-module (crates-io))

(define-public crate-pati-0.0.1 (crate (name "pati") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "100xnlzndm8iv1r0pi0lmhm4ljiv315353al72zdl4r3cr95rk3x")))

(define-public crate-pati-0.0.2 (crate (name "pati") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0bmrs9xzmkc8ddgdk3x1sgrb9sddp61ky9siwk04gbqknzlbkc6y")))

(define-public crate-pati-0.1 (crate (name "pati") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "09j8v4hn1ypk8iamx6fbvarkrn3sh9m76wdrgn37q6cmwyryh4s3")))

(define-public crate-pati-0.2 (crate (name "pati") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "03n39hi0afmrsk24947jjcw1z6hmws1brz0s79dah5bcqcvd45qk")))

(define-public crate-patica-0.0.1 (crate (name "patica") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copic_colors") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "pagurus") (req "^0.7.2") (features (quote ("image" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "pagurus_tui") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0wfq6igaka68s7dn9zznv4s4azlj44xsplrhkwnq58m2yffvzy95")))

(define-public crate-patica-0.0.2 (crate (name "patica") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copic_colors") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "orfail") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "pagurus") (req "^0.7.2") (features (quote ("image" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "pagurus_tui") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "pati") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1vzlc7sb7ar9fk0v1yjv8hqgx4ypjixihmf4hb25qyydbdglmwhf")))

(define-public crate-patica-0.0.3 (crate (name "patica") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "copic_colors") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "orfail") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "pagurus") (req "^0.7.2") (features (quote ("image" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "pagurus_tui") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "pati") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.182") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "01fwpij8lmmq6wp2pr344p5apjii1lj78pb0xyz2a08awlgcxid2")))

(define-public crate-patience-diff-0.1 (crate (name "patience-diff") (vers "0.1.0") (deps (list (crate-dep (name "lcs") (req "*") (default-features #t) (kind 0)))) (hash "0zdx7ns8fali8pd61rxf21qn7p60nkcj8f8ypsq0q8fwhnwyls28")))

(define-public crate-patience-diff-rs-0.1 (crate (name "patience-diff-rs") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "1ivs57vyazwa0iwr8bhpkwx67vlqhachhkagmvma6jcs6dh3h2fj")))

(define-public crate-patience-diff-rs-0.1 (crate (name "patience-diff-rs") (vers "0.1.2") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "1pcf7y5cjb6hjs98ghvb6x6r0nq7bpi3q15a7d2r92kaikr2cwjy")))

(define-public crate-patience-diff-rs-0.1 (crate (name "patience-diff-rs") (vers "0.1.3") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "0k16f99kh4v6hs7v6g35jqp2nnnrnhqjd9l8lzsi35g9dvkgmgh6")))

(define-public crate-patience-diff-rs-0.1 (crate (name "patience-diff-rs") (vers "0.1.4") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "1fx20csjx4l6b4v1c2grqnnkhpl30p37786j6k21djgrsxq2q823")))

(define-public crate-patiencediff-0.1 (crate (name "patiencediff") (vers "0.1.0") (hash "00k9gg2wvwjxdwamnxnyj49z5p73m6czdkrfr7fw8wrsrld239p0")))

(define-public crate-patiencediff-0.1 (crate (name "patiencediff") (vers "0.1.1") (hash "1zy7fxgw6rwsbkl3vmfwyf536gqbhj5j0y0ca0brzs4ail34a5ym")))

(define-public crate-patientia-0.1 (crate (name "patientia") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.2.3") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "1sa5gfm4092889p1drinri2gdg7b5p04nh3yj1mj4k9jgnswhphk")))

(define-public crate-patina-0.0.1 (crate (name "patina") (vers "0.0.1") (hash "0w0bbaclicshhqkq2hv96zpbp30gymk5zinmis3b71y4yrhzyrxj")))

