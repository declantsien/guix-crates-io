(define-module (crates-io pa xo) #:use-module (crates-io))

(define-public crate-paxos-0.0.1 (crate (name "paxos") (vers "0.0.1") (deps (list (crate-dep (name "sled") (req "^0.15") (default-features #t) (kind 0)))) (hash "1qsz59lnahgn9dl5cx57vs6fdh3wir83x1259nqvrx5v84xibw2d")))

(define-public crate-paxos-0.0.2 (crate (name "paxos") (vers "0.0.2") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.15") (default-features #t) (kind 0)))) (hash "0ksfkn2acwb11xvaamzwak19lyyrgrz01xd6hvgxvkz8wvikjl4d")))

(define-public crate-paxos-0.0.5 (crate (name "paxos") (vers "0.0.5") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "1dqvvfwf3aq5xn4h4lb0hw2446s0cnfzbszhm7qdgp3zgrpzw3cp")))

(define-public crate-paxos-rust-0.1 (crate (name "paxos-rust") (vers "0.1.0") (hash "0kgnb1a6lr84k0gzliaa9xlch0qq4rgrxw9wk6zl2w5psd4jnb41")))

(define-public crate-paxos-rust-0.1 (crate (name "paxos-rust") (vers "0.1.1") (hash "0rwlsb2jwky93p1xgix20x41mnd0djvz9zpp80m6h8lsyjp19dkj")))

(define-public crate-paxos-rust-0.2 (crate (name "paxos-rust") (vers "0.2.0") (hash "0rawfz4qmbsps9xhczs7sa32c90sv7q1r7b8bfx5g6g7dxnpgcx9")))

