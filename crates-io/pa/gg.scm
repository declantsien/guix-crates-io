(define-module (crates-io pa gg) #:use-module (crates-io))

(define-public crate-paggo-0.1 (crate (name "paggo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("net" "bytes" "mio" "io-std" "io-util" "macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0y0qf4pn3iw31scq15w0920wv289icsfcxf57assl14xzsdd5yms") (features (quote (("default" "cli") ("cli" "clap"))))))

(define-public crate-paggo-0.1 (crate (name "paggo") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("net" "bytes" "mio" "io-std" "io-util" "macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "07mnd98q5g2j84pbkwmiwpizgdqgbx0s10frcpr2qy3pryysd70v") (features (quote (("default" "cli") ("cli" "clap"))))))

