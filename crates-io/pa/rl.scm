(define-module (crates-io pa rl) #:use-module (crates-io))

(define-public crate-parle-0.0.0 (crate (name "parle") (vers "0.0.0-reserved") (hash "060x8scjmh9qxy388b8rhkfpwkfv4qd8lrq6vhb5ckbkcip0yhc9")))

(define-public crate-parley-0.1 (crate (name "parley") (vers "0.1.0") (deps (list (crate-dep (name "fontique") (req "^0.1.0") (kind 0)) (crate-dep (name "peniko") (req "^0.1.0") (kind 0)) (crate-dep (name "skrifa") (req "^0.19.1") (kind 0)) (crate-dep (name "swash") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1hvrjcbl22rpzap2ix0ma0kk5p8a153msjaznwp2gn8bkg4w21dy") (features (quote (("system" "std" "fontique/system") ("std" "fontique/std" "skrifa/std" "peniko/std") ("libm" "fontique/libm" "skrifa/libm" "peniko/libm") ("default" "system")))) (rust-version "1.70")))

