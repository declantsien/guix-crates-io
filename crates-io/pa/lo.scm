(define-module (crates-io pa lo) #:use-module (crates-io))

(define-public crate-palombe-0.1 (crate (name "palombe") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.37") (default-features #t) (kind 0)))) (hash "1jihd851ybbn337wjhhg2rv2kfh2gpbv17safsw1y1w1w5j3p107")))

(define-public crate-palombe-0.2 (crate (name "palombe") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cxf2n07d1d39phnilll7gh9lr5r9r1lq0wfrq9v3afhk4f612dd")))

(define-public crate-palombe-0.3 (crate (name "palombe") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18z0r1p455f95bn8vyqdgx20abp77ck9brriz9vki9ng4dng3vz5")))

(define-public crate-palombe-0.3 (crate (name "palombe") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mkaa3jwqm10b11d7xcwyb0r7qhn1sjxxb82qklff1v425yrfnp8")))

(define-public crate-palombe-0.3 (crate (name "palombe") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0iwdfgswx2bqza6k2mhc7f6dg2gjllbcdy8b62grah7y0rc0zk44")))

(define-public crate-palombe-0.4 (crate (name "palombe") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xwpdgizwh97y26cnlc9b4zw67k5y52js6s8sm50a9d69wl2f350")))

(define-public crate-palombe-0.4 (crate (name "palombe") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b5drvjipkn7az9c6pvl5fz2pqkbnjwkl389yfhdqz68mb4d1pxi")))

(define-public crate-palombe-0.4 (crate (name "palombe") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hi9yl53b8b6w24nw1mkx4cdk1zms3q0405hmi4mx67wwzd0070y")))

(define-public crate-palombe-0.5 (crate (name "palombe") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p0l41x1n8h0crvpp4xrfs687h9cvrkxzpgmbpb6581axhlcaqfq")))

(define-public crate-palombe-0.5 (crate (name "palombe") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1inxzccgwy2xa9a9nxqr96q3h6dqj8z85qhyq82bqfnkjvzp1229")))

