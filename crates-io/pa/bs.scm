(define-module (crates-io pa bs) #:use-module (crates-io))

(define-public crate-pabst-0.1 (crate (name "pabst") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "las") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rivlib") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sdc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ll5a011ililyvkw5hmcbfi7p1rm4hcq7ff1l2f79c9kqa819p4q") (features (quote (("rxp-source" "rivlib"))))))

