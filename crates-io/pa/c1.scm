(define-module (crates-io pa c1) #:use-module (crates-io))

(define-public crate-pac194x-0.1 (crate (name "pac194x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10") (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "register_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q842k240mj8ps8gjm9r98khgg5h2yg5n3r5j4a39byhgy9wpnjs")))

(define-public crate-pac194x-0.1 (crate (name "pac194x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10") (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "register_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gh97anbaa3ps4isc46xby5h2ikqm88pxqdvfd5fhg5fkhsdg1iz")))

(define-public crate-pac194x-0.1 (crate (name "pac194x") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10") (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "register_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04063szwjkmjl010jwaz8lwash48b855wkfrymmz1pr96pwzjs44")))

