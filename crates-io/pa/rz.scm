(define-module (crates-io pa rz) #:use-module (crates-io))

(define-public crate-parz-0.0.1 (crate (name "parz") (vers "0.0.1") (deps (list (crate-dep (name "bytemuck") (req "^1.9.1") (optional #t) (default-features #t) (kind 0)))) (hash "18p3x9qmhplrwcppj10zjaqhrwdmq0rdvlh8hn7zblz7xwknj3yy") (v 2) (features2 (quote (("bytemuck" "dep:bytemuck"))))))

(define-public crate-parze-0.1 (crate (name "parze") (vers "0.1.0") (hash "0d6mmzh2bdz6pww2r4wfgr5va5ma89avp2qcix42n71kaq2vjyj1")))

(define-public crate-parze-0.2 (crate (name "parze") (vers "0.2.0") (hash "0pwj7dk2gr2gw5imks9lnlnz2zmyppbkaf9qva8qxy7gshxzg4vl")))

(define-public crate-parze-0.3 (crate (name "parze") (vers "0.3.0") (hash "1w6z48l20whxdzqpx5wzvdlig0nqgrhm3hwb7as8f9kcgmkkz59j")))

(define-public crate-parze-0.4 (crate (name "parze") (vers "0.4.0") (hash "0xxxcr56bjqb1zh25r8jbipi2zmw6dm9dncbzj163l5xxdbz6amm")))

(define-public crate-parze-0.4 (crate (name "parze") (vers "0.4.1") (hash "1c7p2wf4wfkg25hjkrmwag06pymxzl942v39ivzk9s6ckzh3g2s5")))

(define-public crate-parze-0.4 (crate (name "parze") (vers "0.4.2") (hash "0n4nrpq3jkvmfvx4dm2pcxgbblh9m6k73zixmwjl8cl9562g5vns")))

(define-public crate-parze-0.5 (crate (name "parze") (vers "0.5.0") (deps (list (crate-dep (name "pom") (req "^3.0") (default-features #t) (kind 2)))) (hash "0y6x03vgs38lcrdc49y0afanlhdzyvvx9xhxlsdmniwkrrxfblsj")))

(define-public crate-parze-0.5 (crate (name "parze") (vers "0.5.1") (deps (list (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 2)) (crate-dep (name "pom") (req "^3.0") (default-features #t) (kind 2)))) (hash "0hx8k2ilkzlmnknh91asdp3arljfaxbaaapnljd229xq3x5sf6zq")))

(define-public crate-parze-0.6 (crate (name "parze") (vers "0.6.0") (deps (list (crate-dep (name "parze-declare") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pom") (req "^3.0") (default-features #t) (kind 2)))) (hash "013lkb0brryclji6lm4fv9qzdqyqn6635k48ywhva4vlf3rs496c") (features (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7 (crate (name "parze") (vers "0.7.0") (deps (list (crate-dep (name "parze-declare") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pom") (req "^3.0") (default-features #t) (kind 2)))) (hash "1p7vxwdwdym7nddb952523dkf6dk2y1pywj8rw7n6d8ha62br115") (features (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7 (crate (name "parze") (vers "0.7.1") (deps (list (crate-dep (name "parze-declare") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pom") (req "^3.0") (default-features #t) (kind 2)))) (hash "13ncidw620mjqrxivjfszq3xvz9lwpmh2rfqlfq5rlv2797fznjn") (features (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7 (crate (name "parze") (vers "0.7.2") (deps (list (crate-dep (name "parze-declare") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pom") (req "^3.0") (default-features #t) (kind 2)))) (hash "1fm29wypplhpvhkdaa6hy5m8k9m5jj9hva9marjj9npp6ijgh4zs") (features (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-0.7 (crate (name "parze") (vers "0.7.3") (deps (list (crate-dep (name "parze-declare") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pom") (req "^3.0") (default-features #t) (kind 2)))) (hash "0kch2d1rjwypzz06a6wflwq4hpw7w6jbblliwvmy36y02z70b2c2") (features (quote (("nightly") ("macros" "nightly") ("default" "nightly" "macros"))))))

(define-public crate-parze-declare-0.1 (crate (name "parze-declare") (vers "0.1.0") (hash "1iwy8a49hrxs24x7vlszw000mslw86x5y8iy5sy4z26zyfgj39ci")))

(define-public crate-parze-declare-0.2 (crate (name "parze-declare") (vers "0.2.0") (hash "0v23ab2r63hmdjwdnianpkl5mws92yi0kgqi90z7di0z01hkm2gs")))

