(define-module (crates-io pa gi) #:use-module (crates-io))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.0.0") (hash "1w8ds7hrfylcdiy6fgag24vjanxhisi6hbgxbj1ln5ng91ckg6hp")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.0.1") (hash "1zfjfhxxyczkx68cdkq6ghqzhaf5b7a7bi3g344y5w08clywqab8")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.0") (hash "0sdxi7zpby3dy0wn7anj4cca57fk8278x8bkw53wkxcrrxcsfqar")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.1") (hash "0m8472gw87di91fyf3x3yp6ags3qigx905j4rkva1hm1jcz1aj3x")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.2") (hash "07h6vrcal3aj18ylm7ynhs6g8jj9w82cdzc6i1mqb0qhxd5qag8a")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.3") (hash "1402zlss3r9cykd6zzvsf8rfn17kq4wx8wa4w5xfir8r5k0fnvy5")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.4") (hash "0f794c32rab4x8m30cis5f9d7dkmsi3vzsrxj4yq1ahwlfwykp4w")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.6") (hash "07v941rih91c59wr30zbzinlswagrzx0adpgc9vfr4df5s9dwfdl")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.7") (hash "0sklfvvgkfx966lf2xvfva3crx940068l7qkzrqdli84x56x6pzi")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.8") (hash "15440k6y839ddqirhp3iy5nas02m3868s299aks2mlbdd05sy52b")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.9") (hash "065j4658kjda5xdivxcljxghb6r6f15wq5wpam33g86ah0389jbi")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.10") (hash "1k2873y62fdgphsky3b587d5qr20dpqfm8mwhm0kf94jizi559yn")))

(define-public crate-paginate-1 (crate (name "paginate") (vers "1.1.11") (hash "1j348rk2i65g9fhqrsr0rl348jcqc7blwx2v8gjchvcxklhdphas")))

(define-public crate-pagination-0.1 (crate (name "pagination") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0yjw15qmna3y5f4jnh62xncyqpf44b8rxgivbkj2j9ggjms3shzk") (features (quote (("with-serde" "serde" "serde_derive") ("page-size-50") ("page-size-5") ("page-size-40") ("page-size-30") ("page-size-20") ("page-size-15") ("page-size-10") ("default" "page-size-20"))))))

(define-public crate-pagination-0.2 (crate (name "pagination") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1d4s4146qw2jnd6z8gsifk0imwrddfvf6nj7wy0zqygwk7c42apd") (features (quote (("with-serde" "serde" "serde_derive") ("page-size-50") ("page-size-5") ("page-size-40") ("page-size-30") ("page-size-20") ("page-size-15") ("page-size-10") ("default" "page-size-20"))))))

(define-public crate-pagination-0.2 (crate (name "pagination") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1cr1nvazrqsh3npb7wbssg5gj9m80xayvng4al63dr4cwr39a6q3") (features (quote (("with-serde" "serde" "serde_derive") ("page-size-50") ("page-size-5") ("page-size-40") ("page-size-30") ("page-size-20") ("page-size-15") ("page-size-10") ("default" "page-size-20"))))))

(define-public crate-pagination-0.3 (crate (name "pagination") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "07163zhd6mpfss57fy4lbwdw9k1ibx4f5i9b0036q1721lsipvp9") (features (quote (("with-serde" "serde" "serde_derive") ("page-size-50") ("page-size-5") ("page-size-40") ("page-size-30") ("page-size-20") ("page-size-15") ("page-size-10") ("default" "page-size-20"))))))

(define-public crate-paginator-0.1 (crate (name "paginator") (vers "0.1.0") (hash "1bif9lqqdnliqkbwyb4j5wxmrg5xc1aplpg4jpbsbv53k3g81r7c") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1 (crate (name "paginator") (vers "0.1.1") (hash "1dvpcyi88p4z39pvjn4aw54xv44irq8bxch0n51kl62l2hw4k8hw") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1 (crate (name "paginator") (vers "0.1.2") (hash "1wiapanndw203g7mlhk9k0r7ghwghnpi7d94mzqmdb5rv5k6sg3x") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1 (crate (name "paginator") (vers "0.1.3") (hash "1x4dw4crjp234w5s5zzfz3v39g5gi218v7bwd1gh8giqc2cdh5xx") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1 (crate (name "paginator") (vers "0.1.4") (hash "1728wld5x9bwygw8x905l0ikg1df76155akc2srxvnn5sxhip18h") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.1 (crate (name "paginator") (vers "0.1.5") (hash "1f0kjidwzhg4qiwc8d81cvqkymn0f10d5l0pczrj75q4s9lybrws") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2 (crate (name "paginator") (vers "0.2.0") (hash "19hmmfz054n8qjsfdb8pj5r46jmxzanafn5mykqq19maibp4mb32") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2 (crate (name "paginator") (vers "0.2.1") (hash "0fhvdz2y8kb8c0zsx8b7x0k0bn3flcas5bi94lpsk9hy96dcyv5w") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2 (crate (name "paginator") (vers "0.2.2") (hash "073h652gzdl6ya9craz3ldw15gfwzmy4d9db2gzv6vsd94mh1wcd") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2 (crate (name "paginator") (vers "0.2.3") (hash "16s6jhq8mzygp0qq6chn39jw9pm0niayqmdgzraqh10893d3d4z1") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2 (crate (name "paginator") (vers "0.2.4") (hash "1k45d4pcy19aidizsv3d0hvf1nhvalynlkdiidb64fssqfblqbr1") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2 (crate (name "paginator") (vers "0.2.5") (hash "0bf61zmnirsxwlgz57snk5dhpna3a4d5gw62wra4c8066q0r051x") (features (quote (("std") ("default" "std"))))))

(define-public crate-paginator-0.2 (crate (name "paginator") (vers "0.2.6") (hash "16s2in69ls6yk2jwcy33mbhfmh2im2z2dl0a5ywjb1i1fhhq3awc") (features (quote (("std") ("default" "std"))))))

(define-public crate-paging-0.0.0 (crate (name "paging") (vers "0.0.0") (hash "055j4a8yzqmcsbxmi6yq8vagmxi9dgs2wk4xwmkyamdz55msckjd")))

(define-public crate-paging-0.1 (crate (name "paging") (vers "0.1.0") (hash "1fra3k12fv9qwj91663imnniv5hv1954pwysyyd53yrmay4816nz")))

(define-public crate-paging-calculator-0.1 (crate (name "paging-calculator") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "nu-ansi-term") (req "^0.46") (default-features #t) (kind 0)))) (hash "1z4qa3nzqc5ymg687l4yfq0kcw337wmip59xg41s52zh681kl9jz")))

(define-public crate-paging-calculator-0.1 (crate (name "paging-calculator") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "nu-ansi-term") (req "^0.46") (default-features #t) (kind 0)))) (hash "0p90psjhqfkmg8sfra3484crlpwmmnghhsyhidyh6vxv17ymrsxq")))

(define-public crate-paging-calculator-0.1 (crate (name "paging-calculator") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "nu-ansi-term") (req "^0.46") (default-features #t) (kind 0)))) (hash "1x23v9cyci9030lc1f3x1v8a2g9gjld59nv9i4pbifzgx75600vp")))

(define-public crate-paging-calculator-0.2 (crate (name "paging-calculator") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "nu-ansi-term") (req "^0.46") (default-features #t) (kind 0)))) (hash "0nxgssbjx48fp9dwir7sa0yfm5vhjhxl2g9hnknya8lsm2kg4smb")))

(define-public crate-paging-calculator-0.3 (crate (name "paging-calculator") (vers "0.3.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.4") (features (quote ("std" "color" "help" "usage" "error-context" "suggestions" "derive" "unicode" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "nu-ansi-term") (req "^0.49.0") (default-features #t) (kind 0)))) (hash "0lacjs90yx2k269534ya6dklv8f0bszgvypd15lis56mydk06pxc")))

(define-public crate-pagino-1 (crate (name "pagino") (vers "1.0.0") (hash "076gl5iap5ljggcfg79wbl2fkfnwgshyyrzs9363z5ysjh99pyia")))

(define-public crate-pagino-1 (crate (name "pagino") (vers "1.0.1") (hash "1njbbwbssbd43zlzxaaa6q6cymz3vn4xh468bxqf26wg244mmqhg")))

(define-public crate-pagino-1 (crate (name "pagino") (vers "1.0.2") (hash "1vcidc4432pd7lc5llpl177i359sfpsgll4vsw49zr2wk8iwv2kb")))

(define-public crate-pagino-1 (crate (name "pagino") (vers "1.0.3") (hash "0w8mf97s5vysygq4imrfqy38c792c5zd3nvxb2bnm84k09kc2bjn")))

(define-public crate-pagino-1 (crate (name "pagino") (vers "1.0.4") (hash "1nx7synrwd3ysk6nfj1x3cxzxa65vw2i83g5678j4a130yhdlh66")))

(define-public crate-pagino-1 (crate (name "pagino") (vers "1.0.5") (hash "1rvjddawqhc3zc5bbs7v9108lc8738wmv61dcn28vymqcv2linmd")))

