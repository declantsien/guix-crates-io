(define-module (crates-io pa co) #:use-module (crates-io))

(define-public crate-paco-0.1 (crate (name "paco") (vers "0.1.0") (hash "01fvkhaqd6npkwaz9s1insdpq73rm59adb50gaimh1w100cq8v2m")))

(define-public crate-paco-0.1 (crate (name "paco") (vers "0.1.1") (hash "05myklcpwjji3qic0lkpidbmr671i7py41hjpzqdg6hpwjp77pds")))

(define-public crate-paco-0.1 (crate (name "paco") (vers "0.1.2") (hash "18mjklybpzm1x90m19rpc6n9py7qsl3ihaslalx6nbh3af2d9n97")))

(define-public crate-paco-0.1 (crate (name "paco") (vers "0.1.3") (hash "1ca1flvdfsw3s1a2c4bkm4gn1zv2f0m5qzagfld9w5iq5xrci8p4")))

(define-public crate-pacops-0.0.1 (crate (name "pacops") (vers "0.0.1") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "1rl1zgd1jpm4ay05qhgfa8if992zcxnlqvrnxxa3afwf9rpy4nln")))

(define-public crate-pacops-0.0.2 (crate (name "pacops") (vers "0.0.2") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "0arnl62axd5c8wn1q0hl6n8v1fns1rp6kni4lm8z9i7yqvrs64yg")))

(define-public crate-pacosso-0.2 (crate (name "pacosso") (vers "0.2.2") (hash "085n3dgsjcr6svm2r69rb59pg7kk3a54bwvqgd80kngflg9n15yg")))

(define-public crate-pacosso-0.2 (crate (name "pacosso") (vers "0.2.3") (hash "1xnnrimy4zldipgr3v9yihwiz94cmwi12jaad7qpb64mymjri1yx")))

(define-public crate-pacosso-0.2 (crate (name "pacosso") (vers "0.2.4") (hash "1b5vc5c3kr2cwnxj8iwkhxmv030h8bc4nbwx2dm17byy9n30pk8y")))

(define-public crate-pacosso-0.2 (crate (name "pacosso") (vers "0.2.5") (hash "1z325ib6j7345rxd4lb3pn66v46j74znzcgsf8fkjnqr124p55si")))

