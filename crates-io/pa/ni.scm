(define-module (crates-io pa ni) #:use-module (crates-io))

(define-public crate-panic-0.1 (crate (name "panic") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "concolor") (req "^0.0.11") (features (quote ("auto"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "os_info") (req "^2.0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.0") (features (quote ("v4"))) (kind 0)))) (hash "16rpc8i6qjxs8360ka3z8y6cm3l9a1qcnvnamjk1440vpj025xyf") (features (quote (("nightly") ("default" "color")))) (v 2) (features2 (quote (("color" "dep:termcolor" "dep:concolor"))))))

(define-public crate-panic-0.0.0 (crate (name "panic") (vers "0.0.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "concolor") (req "^0.0.11") (features (quote ("auto"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "os_info") (req "^2.0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.0") (features (quote ("v4"))) (kind 0)))) (hash "0rp6qb3mqzs9jzzf0bqi8rsyrybw2w0ys33mblqf86ypgjj92v3p") (features (quote (("nightly") ("default" "color")))) (yanked #t) (v 2) (features2 (quote (("color" "dep:termcolor" "dep:concolor"))))))

(define-public crate-panic-0.2 (crate (name "panic") (vers "0.2.0") (deps (list (crate-dep (name "anstream") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "anstyle") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "backtrace") (req "^0.3.67") (default-features #t) (kind 0)) (crate-dep (name "os_info") (req "^3.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (features (quote ("display"))) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4"))) (kind 0)))) (hash "1vhc6yg6rbycggpwggcihz8yd49n0xs3l12xbxhbcp4cpkkf3d1n") (features (quote (("only-release") ("nightly") ("default" "color" "only-release")))) (v 2) (features2 (quote (("color" "dep:anstyle" "dep:anstream"))))))

(define-public crate-panic-0.3 (crate (name "panic") (vers "0.3.0") (deps (list (crate-dep (name "anstream") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "anstyle") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "backtrace") (req "^0.3.67") (default-features #t) (kind 0)) (crate-dep (name "os_info") (req "^3.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "text_placeholder") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (features (quote ("display"))) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4"))) (kind 0)))) (hash "0m94qj49m89mbjpvdmcliv1jq9iqjc9f6vib6d1h7wc8g2n4a75n") (features (quote (("only-release") ("nightly") ("default" "color" "only-release")))) (v 2) (features2 (quote (("color" "dep:anstyle" "dep:anstream"))))))

(define-public crate-panic-0.3 (crate (name "panic") (vers "0.3.1") (deps (list (crate-dep (name "anstream") (req "^0.6.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "anstyle") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "backtrace") (req "^0.3.67") (default-features #t) (kind 0)) (crate-dep (name "os_info") (req "^3.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "text_placeholder") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (features (quote ("display"))) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4"))) (kind 0)))) (hash "0acbhwmyawkmqys3had3q2w4lllrhz6mc9qnrdr0v92xh63zc0v6") (features (quote (("only-release") ("nightly") ("default" "color" "only-release")))) (v 2) (features2 (quote (("color" "dep:anstyle" "dep:anstream"))))))

(define-public crate-panic-abort-0.1 (crate (name "panic-abort") (vers "0.1.0") (hash "1jlywsldk0h5n2dakjsqkq6sw5klfyykzfy8g99hff6rmznyca06")))

(define-public crate-panic-abort-0.1 (crate (name "panic-abort") (vers "0.1.1") (hash "0birc161hd6nf66n1lbb3kdf206vrv46yr2aybcs49ql64q3qmbm")))

(define-public crate-panic-abort-0.2 (crate (name "panic-abort") (vers "0.2.0") (hash "0gppi8v07s4dwgxiksfq1ypyaynn7x9miig7zza5cw7j4339divb")))

(define-public crate-panic-abort-0.3 (crate (name "panic-abort") (vers "0.3.0") (hash "1i5x06imydb8r3l05f4qxwcp2fbn2v3zpyxwm6bpwv67x3l5pn65")))

(define-public crate-panic-abort-0.3 (crate (name "panic-abort") (vers "0.3.1") (hash "03v3gvwcmnwma7m5xc8xi5nq61yjgy6qcsr5njlbc5zd25jsc51c")))

(define-public crate-panic-abort-0.3 (crate (name "panic-abort") (vers "0.3.2") (hash "1vkqqwsgylnpgcmx4prxfnh6yv1mdcs44jmhh3r15i5vkd4yc82f")))

(define-public crate-panic-analyzer-0.1 (crate (name "panic-analyzer") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0x8y7f0xr2qhhnzkqczmv67hg0vp8p38k4wr3ssxb1ll6mvjp3zj")))

(define-public crate-panic-analyzer-0.1 (crate (name "panic-analyzer") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "14lxawrk7r3n1z2p3qcr09kmbn2svlf2bgpg9pdi5s07yh953z9i")))

(define-public crate-panic-analyzer-0.1 (crate (name "panic-analyzer") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1dv8sfcnaf92pmrj20a8xybsdlgn3rqxzmkvxzgqziwybrqmyp7p")))

(define-public crate-panic-analyzer-0.1 (crate (name "panic-analyzer") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "05bl3h8f90c58j3a6sb77qbynzvhh6chcanh1176ircabrh0awpz")))

(define-public crate-panic-analyzer-0.1 (crate (name "panic-analyzer") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1z6586kdwbnvs42ac5ciqzph9a7w2y05kpaar1nd4a276dks8zy3")))

(define-public crate-panic-at-the-disco-rs-1 (crate (name "panic-at-the-disco-rs") (vers "1.0.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "0mm628jnzziihxlzzh1chfk1gxgryc5g0b1lk2nv4kgk7zp2gddz")))

(define-public crate-panic-context-0.1 (crate (name "panic-context") (vers "0.1.0") (deps (list (crate-dep (name "gag") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0rqmhy61x1x6xbplw9nlbi5202jbx9sazmwqc8hl8yma6krnvl9c") (features (quote (("keep-debug-context"))))))

(define-public crate-panic-control-0.1 (crate (name "panic-control") (vers "0.1.0") (hash "0z4b6sgxfp9i0y44dhchikrr2qyql0jjv33drnbj4bza7hdk6fqg")))

(define-public crate-panic-control-0.1 (crate (name "panic-control") (vers "0.1.1") (hash "0hyflgwjdj1412hhqap0vsikmvxkgn6rykgrizykfsnh8bf344j0")))

(define-public crate-panic-control-0.1 (crate (name "panic-control") (vers "0.1.2") (hash "1i1q8m4y08vgh6ym2z64g1r5zylz3s2155vzq15dr42qlmgvxnvf")))

(define-public crate-panic-control-0.1 (crate (name "panic-control") (vers "0.1.3") (hash "0dxjizvsav2azln539mib2sfr66g9l552il2c193i1fi9zz0l8d3")))

(define-public crate-panic-control-0.1 (crate (name "panic-control") (vers "0.1.4") (hash "1iyng766mbh611yf6jd902039dp3c3qknaq434369d23pjp7768m")))

(define-public crate-panic-custom-0.1 (crate (name "panic-custom") (vers "0.1.0") (deps (list (crate-dep (name "panic-custom-proc-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "08mck6wz2ydq2vcyri1g83p0ssyp4arh4yvcgzlqy1b856kiwv8j") (features (quote (("proc_macros" "panic-custom-proc-macros") ("abort_on_release") ("abort_on_debug"))))))

(define-public crate-panic-custom-0.1 (crate (name "panic-custom") (vers "0.1.1") (deps (list (crate-dep (name "panic-custom-proc-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "147dzk52gk1jzp017fyzxj8068yl2qkdd194j4ipapd5vzdv1vjl") (features (quote (("proc_macros" "panic-custom-proc-macros") ("abort_on_release") ("abort_on_debug"))))))

(define-public crate-panic-custom-proc-macros-0.1 (crate (name "panic-custom-proc-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dsxs6c3linm3c8fg3y1vm9y9dqhcwgnwgqmsghqwa7jynh64nzw")))

(define-public crate-panic-free-analyzer-0.1 (crate (name "panic-free-analyzer") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0p9qg806w45fgdhpvm8qk3gp20l29isd5lj8hhjxslp9pmvmv6wm") (yanked #t)))

(define-public crate-panic-halt-0.1 (crate (name "panic-halt") (vers "0.1.0") (hash "1p0p6nfi23pvbxfwd5r2jcnizv39y3fxmk32k72jg707x9c4hkfd")))

(define-public crate-panic-halt-0.1 (crate (name "panic-halt") (vers "0.1.1") (hash "1asj7g021clsicdvc5yyg6lwirhjlba4kzd9ir8pckjq8a3rnhyp")))

(define-public crate-panic-halt-0.1 (crate (name "panic-halt") (vers "0.1.2") (hash "1v8srsn6hykj37ha61j7ffr0lbgba5j2ds1yvcsfwmzdjgccfqkb")))

(define-public crate-panic-halt-0.1 (crate (name "panic-halt") (vers "0.1.3") (hash "138khyhq59s98fb2kpv0is5c3ihs3ax7bbpzrcz9b2lmyg2im3g7")))

(define-public crate-panic-halt-0.2 (crate (name "panic-halt") (vers "0.2.0") (hash "04nqaa97ph20ppyy58grwr23hrbw83pn0gf7apf73rdx1q7595ny")))

(define-public crate-panic-handler-0.0.0 (crate (name "panic-handler") (vers "0.0.0") (hash "0nfq4fb4sarxr3n7153y2kp365v40zx8rzii40iakn3618wfxd1j") (yanked #t)))

(define-public crate-panic-handler-2 (crate (name "panic-handler") (vers "2.0.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.38") (default-features #t) (kind 0)))) (hash "15x1qwfhhm1y3bw8z8hislddh17374irwriwhbq5lqa3bvj26i1i")))

(define-public crate-panic-handler-2 (crate (name "panic-handler") (vers "2.0.2") (deps (list (crate-dep (name "backtrace") (req "^0.3.38") (default-features #t) (kind 0)))) (hash "14grxda01bdl9wq9zkp0iv85x1rk073147mbhdfa5vq4r8x1pjm0")))

(define-public crate-panic-handler-2 (crate (name "panic-handler") (vers "2.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3.38") (default-features #t) (kind 0)))) (hash "07qiazg6a0rbkcg0adwzrf56sazsy39b0qfa65gmzj8j87r231vc")))

(define-public crate-panic-hook-0.1 (crate (name "panic-hook") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0976rxr4xr6vdpga30zzv22jwzzbbmyblz85md2b6nia1wwprkcc")))

(define-public crate-panic-itm-0.1 (crate (name "panic-itm") (vers "0.1.0") (deps (list (crate-dep (name "aligned") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0zyy36zmrl2v4w0qmhsrymb4cbb31076abjkm97k0apgvnnkh42z")))

(define-public crate-panic-itm-0.1 (crate (name "panic-itm") (vers "0.1.1") (deps (list (crate-dep (name "aligned") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1nsxc4vcar8xrhqdqqsiafbs8951qir5rdy7wljvbalinl99fq0w")))

(define-public crate-panic-itm-0.2 (crate (name "panic-itm") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "025vh88nyq094kpilvgzwlv7aw4f81kdp3sa8g965a9bqs11nmvb")))

(define-public crate-panic-itm-0.3 (crate (name "panic-itm") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0mkz7x6bx07bramzzhq7rc1dqjfwqwxv34l97m4n2bn50jvpjs41")))

(define-public crate-panic-itm-0.4 (crate (name "panic-itm") (vers "0.4.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0rl6hfbpq581zk1mxgca45h20f6rnw28cn1l9i5xgfardy6mby6j")))

(define-public crate-panic-itm-0.4 (crate (name "panic-itm") (vers "0.4.1") (deps (list (crate-dep (name "cortex-m") (req ">= 0.5.6, < 0.7") (default-features #t) (kind 0)))) (hash "1hvcsrknl94qmhw3a3jb2g72dls3n44k1apd85z211smm4bhv0wq")))

(define-public crate-panic-itm-0.4 (crate (name "panic-itm") (vers "0.4.2") (deps (list (crate-dep (name "cortex-m") (req ">=0.5.8, <0.8") (default-features #t) (kind 0)))) (hash "0k0s76aicj6hzjkxrnc7xrgfz5371r3z5pbdgc46h4mks6bpsmrx")))

(define-public crate-panic-message-0.2 (crate (name "panic-message") (vers "0.2.0") (hash "097kkvlgdh7i8963ddq972xdaqpl4p9gls7sp73yflsrvq0cf7fn")))

(define-public crate-panic-message-0.3 (crate (name "panic-message") (vers "0.3.0") (hash "0ba75hapwknxljlcw2719l9zk8111hk1d0ky64ybwk5xizym4kiq")))

(define-public crate-panic-msp430-0.1 (crate (name "panic-msp430") (vers "0.1.0") (deps (list (crate-dep (name "msp430") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ww20vfp4asbm89b0y0wlyajflm9qsmp9w6nx4a7qrcldl1zbzbf")))

(define-public crate-panic-msp430-0.2 (crate (name "panic-msp430") (vers "0.2.0") (deps (list (crate-dep (name "msp430") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0klq06i8ar8imirgddrw5yqvph45vfvbgxh97igchmb9sdlmcg2d")))

(define-public crate-panic-msp430-0.3 (crate (name "panic-msp430") (vers "0.3.0") (deps (list (crate-dep (name "msp430") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "04piaan0vn6pgcdgdqp0hpznpgl8j1zii6hpdyxh8jwxx3n7i77r")))

(define-public crate-panic-msp430-0.4 (crate (name "panic-msp430") (vers "0.4.0") (deps (list (crate-dep (name "msp430") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "15rw7xcwknyg68lh7rirbh45h9i7a72glqgdbj5q8g2p9sm32kc4")))

(define-public crate-panic-never-0.1 (crate (name "panic-never") (vers "0.1.0") (hash "0w6kz0nv51ngpggnmdnmsbqxf4m5gljngzl3zrl78gjlyv8pbp5h")))

(define-public crate-panic-no-std-0.0.1 (crate (name "panic-no-std") (vers "0.0.1") (deps (list (crate-dep (name "exit-no-std") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (target "cfg(all(not(target_os = \"dos\"), not(windows)))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("fileapi" "handleapi" "processenv" "winbase"))) (default-features #t) (target "cfg(all(not(target_os = \"dos\"), windows))") (kind 0)) (crate-dep (name "pc-ints") (req "^0.3.2") (default-features #t) (target "cfg(target_os = \"dos\")") (kind 0)))) (hash "0v5kv0bb3b6axrvfd6j6hn5zaa90nrnfpldx8091gjih8jzx02hq")))

(define-public crate-panic-persist-0.2 (crate (name "panic-persist") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0wqr44h02znh0qf1ryq4i5xwnd4h970cq2hra5jsnqv81zx9zijl") (features (quote (("utf8") ("default"))))))

(define-public crate-panic-persist-0.2 (crate (name "panic-persist") (vers "0.2.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0hpfypb9bb84ih9iyljib3c7haikqissmnzm2q6fpqslb4m6jbnx") (features (quote (("utf8") ("default"))))))

(define-public crate-panic-persist-0.3 (crate (name "panic-persist") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1mczyfpgirn0ih0i3jhwnfvl73ds01kxb7yzsk9c9lpci4k3ifrj") (features (quote (("utf8") ("min-panic") ("default") ("custom-panic-handler"))))))

(define-public crate-panic-probe-0.0.0 (crate (name "panic-probe") (vers "0.0.0") (hash "0zfilbb2dknhr3x7a3cy03bgy8p8w7wmr4al7anh2n2c47jkqpzd")))

(define-public crate-panic-probe-0.1 (crate (name "panic-probe") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "1w5mzv4nf9yr060xd7z8slvqlzqkp1057b1i5dblckzdczgnmq0d") (features (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.2 (crate (name "panic-probe") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "1p60wq68am4pk0aqhjngsjdb5zyzk32hvcs0paribx2idcfwida5") (features (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.2 (crate (name "panic-probe") (vers "0.2.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1japj39ya7gf7q7mrizyi93pb9v51lpq0wyk1bbx0w2wylka43va") (features (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.3 (crate (name "panic-probe") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0fngdpm4cdr07vp5dddmcv0s1cr051gmbsfawpw1ig92mh7g1c9s") (features (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.3 (crate (name "panic-probe") (vers "0.3.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1fajcv95jjx2v0k8jzwyq3433q8wkjpr5ahf6k6n19zmbrjaavxa") (features (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-probe-0.3 (crate (name "panic-probe") (vers "0.3.2") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1c159xprb4q7dmf804vc9lalykdmvmzd19vxr5ndc8qlblixjis0") (features (quote (("print-rtt" "rtt-target") ("print-defmt" "defmt" "defmt-error") ("defmt-error"))))))

(define-public crate-panic-ramdump-0.1 (crate (name "panic-ramdump") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0klfj164i09d5nkzm7mijh6lp52z2jch1slg5sqdx1z97a65z7k9")))

(define-public crate-panic-ramdump-0.1 (crate (name "panic-ramdump") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "054cj4zbxy17xpcms2y8xb1fwiw8vmpwknrqkrcwpk3rnm7v2ygr")))

(define-public crate-panic-reset-0.1 (crate (name "panic-reset") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)))) (hash "19d8lhkm630ay9il8c35narjqi8h8wl8gvk11p3wb1w4lcsqbvmc")))

(define-public crate-panic-reset-0.1 (crate (name "panic-reset") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req ">0.6, <0.8") (default-features #t) (kind 0)))) (hash "1xd4lc1kkb83a2c1dl6bf4hffc36dd3l7akj8pcqsiqsbcmgzwbc")))

(define-public crate-panic-rtt-core-0.1 (crate (name "panic-rtt-core") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.1.0") (features (quote ("cortex-m"))) (default-features #t) (kind 0)))) (hash "0n6cyxwgjjpyv9ibryl1f0r2bnh7ywnx2hbzw2pi8b5gwrq5g60i")))

(define-public crate-panic-rtt-core-0.1 (crate (name "panic-rtt-core") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.1.0") (features (quote ("cortex-m"))) (default-features #t) (kind 0)))) (hash "1wpaxq4v15bivz4shmx0sphgi3fdfjvdwyg3r8ihr9flfgf1sfhn")))

(define-public crate-panic-rtt-core-0.2 (crate (name "panic-rtt-core") (vers "0.2.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.2.2") (features (quote ("cortex-m"))) (default-features #t) (kind 0)))) (hash "1jzsizbml7vqn9nwwzlsbqc1xsm86x1hk4ggmw5f8b5j7xwr9mf7")))

(define-public crate-panic-rtt-target-0.1 (crate (name "panic-rtt-target") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1d6yr1qrqd3rrmjwh5vjaaciay6k9w6rynxminr66kc3imfyqgdb")))

(define-public crate-panic-rtt-target-0.1 (crate (name "panic-rtt-target") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "101i8hxbvimpnhjp5r48dmg966xb1f7l8c85rmysvpjyrzn4vkfg")))

(define-public crate-panic-rtt-target-0.1 (crate (name "panic-rtt-target") (vers "0.1.2") (deps (list (crate-dep (name "cortex-m") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1sfwavv406npgzl1c6mwhzm70dihq5bwcn7rj163wic1r1xvcshd")))

(define-public crate-panic-rtt-target-0.1 (crate (name "panic-rtt-target") (vers "0.1.3") (deps (list (crate-dep (name "critical-section") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rtt-target") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1znbn6r7lnl897hfm6cbv49hla5bqxwi5634hdg0v5nqkn01v3b0")))

(define-public crate-panic-semihosting-0.1 (crate (name "panic-semihosting") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0zvjisxi9pw0mczryj8x7q4hxyaqiv373p8zagikysqd0vn30j11")))

(define-public crate-panic-semihosting-0.2 (crate (name "panic-semihosting") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1vgbmv1f2fmwdx4clzzn7aqkbk3jbpkdjqjyydbxv0iwg8acrj20") (features (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.3 (crate (name "panic-semihosting") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1z0k1szp7wr0bivqs2rkjycwh0xmp8049as0ih7x9ygbkb6lvxjj") (features (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.4 (crate (name "panic-semihosting") (vers "0.4.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0ss6ja239582bghaswxmzrj9802jg69c4b97vrb21z82k7vkz1cm") (features (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.5 (crate (name "panic-semihosting") (vers "0.5.0") (deps (list (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "043jxa6148sgs1v764j5rfqxqmlsn64rnddci1j266k2n56qa5qh") (features (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm"))))))

(define-public crate-panic-semihosting-0.5 (crate (name "panic-semihosting") (vers "0.5.1") (deps (list (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "12c6am63xscsaf5l9ljphv0ra761kzb9dkb6qzm9vxh0gcgdhix6") (features (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5 (crate (name "panic-semihosting") (vers "0.5.2") (deps (list (crate-dep (name "cortex-m") (req ">= 0.5.6, < 0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "097qzq57dn61avabzdgp4kfkplfcih0qai7iig0hqprv3myb7kwp") (features (quote (("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5 (crate (name "panic-semihosting") (vers "0.5.3") (deps (list (crate-dep (name "cortex-m") (req ">= 0.5.6, < 0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req "^0.3") (default-features #t) (kind 0)))) (hash "0b34ia0pz16j7jnqgps5mililzr1mbs8cllg61mc2xi8hsn68f60") (features (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5 (crate (name "panic-semihosting") (vers "0.5.4") (deps (list (crate-dep (name "cortex-m") (req ">=0.5.6, <0.8") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req ">=0.3, <0.5") (default-features #t) (kind 0)))) (hash "0n7yrnwfg0xjhakaa84p1yr16y00r0wcn68kvmhr3vnhc6vnxldf") (features (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.5 (crate (name "panic-semihosting") (vers "0.5.5") (deps (list (crate-dep (name "cortex-m") (req ">=0.5.6, <0.8") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req ">=0.3, <0.5") (default-features #t) (kind 0)))) (hash "0a1dyacabbghy229y0r92dqr6xmbgq8dvim098x2nfj2jx2d7hyy") (features (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit")))) (yanked #t)))

(define-public crate-panic-semihosting-0.5 (crate (name "panic-semihosting") (vers "0.5.6") (deps (list (crate-dep (name "cortex-m") (req ">=0.5.6, <0.8") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req ">=0.3, <0.5") (default-features #t) (kind 0)))) (hash "1v0ydab8js85abppgq9aq5my6v01szs0lvk42hjx1pq1spnmvmf3") (features (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("inline-asm" "cortex-m-semihosting/inline-asm" "cortex-m/inline-asm") ("exit"))))))

(define-public crate-panic-semihosting-0.6 (crate (name "panic-semihosting") (vers "0.6.0") (deps (list (crate-dep (name "cortex-m") (req ">=0.5.6, <0.8") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-semihosting") (req ">=0.5.0, <0.6") (default-features #t) (kind 0)))) (hash "1axisy9dgmivq52s953s0qya9vjfrq93a8khm1v3s1yr6c93x2pf") (features (quote (("jlink-quirks" "cortex-m-semihosting/jlink-quirks") ("exit")))) (rust-version "1.59")))

(define-public crate-panic-serial-0.1 (crate (name "panic-serial") (vers "0.1.0") (deps (list (crate-dep (name "ufmt") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "083bdmn9qri0by6rfxl6zaw6x6ygjp1b6352b53a0928rj3x6qwi") (features (quote (("message") ("location") ("full" "location" "message"))))))

(define-public crate-panic-serial-0.1 (crate (name "panic-serial") (vers "0.1.1") (deps (list (crate-dep (name "ufmt") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rl4088zclyqb81mcdrzml7sfbqhbkpxdxysvinnrff1dagp0sqv") (features (quote (("message") ("location") ("full" "location" "message"))))))

(define-public crate-panic-serial-0.1 (crate (name "panic-serial") (vers "0.1.2") (deps (list (crate-dep (name "ufmt") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ndga3j7kp5nwfn2qfdrsdxbixwqc1ws5n6q5nimx0mhb06wdby3") (features (quote (("message") ("location") ("full" "location" "message"))))))

(define-public crate-panic-soundcloud-1 (crate (name "panic-soundcloud") (vers "1.0.0") (hash "0f1s2krapfx26d5zs124a01pc96vqwkwc340db35qkra5dbmpwxx")))

(define-public crate-panic-usb-boot-0.1 (crate (name "panic-usb-boot") (vers "0.1.0") (deps (list (crate-dep (name "rp2040-hal") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1vkq1gysf18wl928m7nkyz61a2nc5lzgdlsmacx378qgb51hp34n") (features (quote (("usb_mass_storage") ("picoboot") ("default" "picoboot" "usb_mass_storage"))))))

(define-public crate-panic-usb-boot-0.2 (crate (name "panic-usb-boot") (vers "0.2.0") (deps (list (crate-dep (name "rp2040-hal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1vly39nvxghqs4nsc2qh3c2v66ffgzbnh72clxask5krnl33liiz") (features (quote (("usb_mass_storage") ("picoboot") ("default" "picoboot" "usb_mass_storage"))))))

(define-public crate-panic-usb-boot-0.3 (crate (name "panic-usb-boot") (vers "0.3.0") (hash "0ni63kzavi7hwlfk0znwr7h6lgsj9ga191r9vcnfkjgjckcwxm2f") (features (quote (("usb_mass_storage") ("picoboot") ("default" "picoboot" "usb_mass_storage"))))))

(define-public crate-panic-write-0.1 (crate (name "panic-write") (vers "0.1.0") (hash "0d2v5mm3w5abh3l7s3izxfxnv3xi34wnv9sdzgdn22vm3bk0hih7")))

(define-public crate-panic_at_the_disco-1 (crate (name "panic_at_the_disco") (vers "1.0.0") (deps (list (crate-dep (name "open") (req "^1.2") (default-features #t) (kind 0)))) (hash "0rjv4i4l0qhl7n3s24hvn87rnkalfrhy09az3dvm91xxlqwmfbix")))

(define-public crate-panic_discard-0.1 (crate (name "panic_discard") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ir22rd58q18y77k2bykdybrk3mfgk9knligj1fb0g1dwa8vnnm7")))

(define-public crate-panic_monitor-0.2 (crate (name "panic_monitor") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "10byw468kfmik1v309w62yaxndkb8rz6z57rj4g2c5wyzax18s8m")))

(define-public crate-panic_rtt-0.1 (crate (name "panic_rtt") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "~0.5") (default-features #t) (kind 0)) (crate-dep (name "jlink_rtt") (req "~0.1") (default-features #t) (kind 0)))) (hash "0562jgqnq9chcxz95byj4sjxf0bzfpaf1yw08k63s7fisp2lc1h6")))

(define-public crate-panic_rtt-0.2 (crate (name "panic_rtt") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "~0.5") (default-features #t) (kind 0)) (crate-dep (name "jlink_rtt") (req "~0.1") (default-features #t) (kind 0)))) (hash "1byzs5ng6j9spmvsfq5bs3qzs1kydmr412v2kc8l67j90m5fh5gz")))

(define-public crate-panic_rtt-0.3 (crate (name "panic_rtt") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "jlink_rtt") (req "~0.2") (default-features #t) (kind 0)))) (hash "10pb66i2p1kdz302n4b71i41zyzj7yfn2a4fkwywk3shpaml7nzq")))

(define-public crate-panic_search-0.1 (crate (name "panic_search") (vers "0.1.0") (deps (list (crate-dep (name "open") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "18sm357mjs4q8fxz34zfp66xw62228rmp7gk697b9073vlmpkfgq")))

(define-public crate-panic_search-0.1 (crate (name "panic_search") (vers "0.1.1") (deps (list (crate-dep (name "open") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1ijii50g124a65gip1vj01pbp868yhwp6bsdmcrjpqxslwral78r")))

(define-public crate-panic_search-0.1 (crate (name "panic_search") (vers "0.1.2") (deps (list (crate-dep (name "open") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1b11fqpqnszcvga3m6qfibhiyryp7c2pjkgblh0lynh2an3rgwzb")))

(define-public crate-panic_search-0.1 (crate (name "panic_search") (vers "0.1.3") (deps (list (crate-dep (name "open") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "08b9xc4jmf2icvip1b9dmgj0wavi85crjgq5268bqq2ylgpv088l")))

(define-public crate-panic_search-0.1 (crate (name "panic_search") (vers "0.1.4") (deps (list (crate-dep (name "open") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1ycf2c5agrab2k718cfibw95a3dsd4zhjna6721ykhb6mai4p3b9")))

(define-public crate-panic_unwind-0.0.0 (crate (name "panic_unwind") (vers "0.0.0") (deps (list (crate-dep (name "sgx_libc") (req "= 1.1.0") (default-features #t) (target "cfg(not(target_env = \"sgx\"))") (kind 0)) (crate-dep (name "sgx_unwind") (req "^0.1.0") (default-features #t) (target "cfg(not(target_env = \"sgx\"))") (kind 0)))) (hash "00jd0wb3svagghrrwblqh3grlaijz7iqqmq61kf4z4lzr4bma8ji")))

(define-public crate-panicdump-0.1 (crate (name "panicdump") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "145m8ij09m2ay92jgix11scn74kxndrp7vdmaywav796w12mi4ky")))

(define-public crate-panicking-0.0.1 (crate (name "panicking") (vers "0.0.1") (hash "1dvpz44f3kzv29h3f4lxyk4z1b6nrhl4hj3kf51k2606rrkzilhy") (features (quote (("std") ("default" "std"))))))

(define-public crate-panicking-0.0.2 (crate (name "panicking") (vers "0.0.2") (hash "0gsjg4a1kyk6lh1c4cpyq5vpg4rm8mr4q7mh3jdzdr8lzfvw6nqw") (features (quote (("std") ("default" "std")))) (rust-version "1.59")))

(define-public crate-panicking-0.1 (crate (name "panicking") (vers "0.1.0") (hash "1nkfrlcmsigqi9zv79i5vv7znxlvjfrmzdx8pxppicac6z31f3p2") (features (quote (("std") ("default" "std")))) (rust-version "1.59")))

(define-public crate-panicking-0.1 (crate (name "panicking") (vers "0.1.1") (hash "04ivn6sg3c0f63bs1nfcpl6pix9y2z083c9k6isbv4mx6m5i2j6l") (features (quote (("std") ("default" "std")))) (rust-version "1.59")))

(define-public crate-panicking-0.1 (crate (name "panicking") (vers "0.1.2") (hash "0i0kl14w1gmafih6rb8krllim2pxri9l8zz3sqd9sc1xvqzi6cgj") (features (quote (("std") ("default" "std")))) (rust-version "1.59")))

(define-public crate-panicking-0.2 (crate (name "panicking") (vers "0.2.0") (hash "1wvc7dcx819a8vyypn38p27fah5xmnmjylgz8kxjrvq6pxjxcjkr") (features (quote (("std") ("default" "std")))) (rust-version "1.59")))

(define-public crate-panicking-0.3 (crate (name "panicking") (vers "0.3.0") (hash "0iwfarkivp4b8vh3f4v3mj505jgiz631yngrl8bj7hynkdrqg8vl") (features (quote (("std") ("default" "std") ("abort")))) (yanked #t) (rust-version "1.59")))

(define-public crate-panicking-0.3 (crate (name "panicking") (vers "0.3.1") (hash "1zvyj4dnvkf7zmnrfw2gsr2qf2v33f1d4x6kj83mxhia1kp73r1z") (features (quote (("std") ("default" "std") ("abort")))) (rust-version "1.59")))

(define-public crate-panicking-0.4 (crate (name "panicking") (vers "0.4.0") (hash "1pl0njhi94xyqxghsqzsy90l0bqylx6ff5s6px1zkbb7sr65ksr1") (features (quote (("std") ("default" "std") ("abort")))) (rust-version "1.59")))

(define-public crate-panicky-0.0.0 (crate (name "panicky") (vers "0.0.0-0.0.0-0.0.0") (hash "0cyqrpawl1vbznrchw4f0vyylymz09dzcr83nbfx6i910d4xv4v3") (yanked #t)))

(define-public crate-panicky-0.0.0 (crate (name "panicky") (vers "0.0.0--") (hash "0pdzrlaz9yskh2pjjwpkksln744m1p8jd7spqcq8wwbjab3ij6sd") (yanked #t)))

(define-public crate-paniclogger-0.1 (crate (name "paniclogger") (vers "0.1.0") (hash "11x6qxc8274iv66cjmdrp26j7wcsxgypqwj3jmflqjjkc6jdyx9p")))

(define-public crate-panicui-0.1 (crate (name "panicui") (vers "0.1.0") (deps (list (crate-dep (name "fltk") (req "^1.3.27") (default-features #t) (kind 0)))) (hash "0slw5rrdk26v7mkmv5wc8c5h7f54lk28c60pmlrr0zzxb7zikvfl") (features (quote (("shared" "fltk/fltk-shared") ("default" "bundled") ("bundled" "fltk/fltk-bundled"))))))

(define-public crate-panik-0.1 (crate (name "panik") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog-scope") (req "^4.4") (default-features #t) (kind 2)) (crate-dep (name "slog-stdlog") (req "^4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2.6") (default-features #t) (kind 2)))) (hash "1qxi3axpi7xjzyjlgmac4vmwpnqn3pszhi4xnlq6jimc63l44ah1") (features (quote (("use-stderr") ("use-slog" "slog" "slog-stdlog") ("use-parking-lot" "parking_lot") ("use-log" "log") ("default" "use-log"))))))

(define-public crate-panik-0.1 (crate (name "panik") (vers "0.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog-scope") (req "^4.4") (default-features #t) (kind 2)) (crate-dep (name "slog-stdlog") (req "^4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2.6") (default-features #t) (kind 2)))) (hash "1mglraijwk9bykczg65bcm34p4aiwp573p1fgyf3dhqy2ixg2qj6") (features (quote (("use-stderr") ("use-slog" "slog" "slog-stdlog") ("use-parking-lot" "parking_lot") ("use-log" "log") ("default" "use-log"))))))

(define-public crate-panik-0.2 (crate (name "panik") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog-scope") (req "^4.4") (default-features #t) (kind 2)) (crate-dep (name "slog-stdlog") (req "^4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slog-term") (req "^2.6") (default-features #t) (kind 2)))) (hash "00mbgjqzaqxh7m05imdg5w293gn1v4r6i52ws305h1krx0xsw2cc") (features (quote (("use-stderr") ("use-slog" "slog" "slog-stdlog") ("use-parking-lot" "parking_lot") ("use-log" "log") ("default" "use-log"))))))

(define-public crate-panik-handler-0.1 (crate (name "panik-handler") (vers "0.1.0") (hash "0xhr57ck6jdi9v37hswibayvamjxp8qx56s280i2sd6dayzvrw0w")))

(define-public crate-panim-loader-0.1 (crate (name "panim-loader") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1q531ra6nmw8xndl6r3xsanmr7syp8j9w1b530q8sm9gy0m1khds")))

(define-public crate-panim-loader-0.1 (crate (name "panim-loader") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0r8fixkfx6rib8jw4grh0c55ygnvfv2na8lrab05qlhjc0x7i7v6")))

(define-public crate-panim-loader-0.2 (crate (name "panim-loader") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0mx2kdba80ajfp92g6qbssizqng1vz7rs7fz4f1skn7xj252yz60")))

(define-public crate-panim-loader-0.2 (crate (name "panim-loader") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0g6rf6rc2rs5ygl9lmsa8d3rlp4v257n7iij3n7fgsnx4g6i5fng")))

(define-public crate-panim-loader-0.2 (crate (name "panim-loader") (vers "0.2.2") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "06wj9q9ngy8bz3iyz656klimbkrxjyk9k9xvzwwzdbh65nbg3982")))

(define-public crate-panim-loader-0.2 (crate (name "panim-loader") (vers "0.2.3") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "04cbjbgxfxhw097bxw8bwh3q0dngdxdah0hv3njc3ddd552mvwjn")))

(define-public crate-panini-0.0.0 (crate (name "panini") (vers "0.0.0") (deps (list (crate-dep (name "cfg") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gearley") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ref_slice") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.2") (default-features #t) (kind 0)))) (hash "0r61sylz44qi2pj6ss88qn3d1yqhlwwiqgx75cy15hbg4fnkaddx")))

(define-public crate-panini_codegen-0.0.0 (crate (name "panini_codegen") (vers "0.0.0") (deps (list (crate-dep (name "aster") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "bit-matrix") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cfg") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "cfg-regex") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gearley") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quasi") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quasi_macros") (req "^0.10") (default-features #t) (kind 0)))) (hash "137f4qshypzskj0gs61bwr7skahqhsj70gpy8m7gs5jqpifxbzhp")))

(define-public crate-panini_common-0.0.0 (crate (name "panini_common") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0l2irvxc12jz1v4ilrvvq354saal55rqdpahdx1wc90bbjvxcmp5")))

(define-public crate-panini_logic-0.0.0 (crate (name "panini_logic") (vers "0.0.0") (deps (list (crate-dep (name "bit-matrix") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cfg") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cfg-regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "enum_coder") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "gearley") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.10") (default-features #t) (kind 0)))) (hash "0b1ripqfajzscdnniww0r5azpzpvb070qqa8ryvqh9571s92vlk1")))

(define-public crate-panini_macros-0.0.0 (crate (name "panini_macros") (vers "0.0.0") (deps (list (crate-dep (name "aster") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "bit-matrix") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cfg") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "compiletest_rs") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "enum_stream_codegen") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "gearley") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "panini") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "panini_codegen") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "panini_macros_snapshot") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "quasi") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quasi_macros") (req "^0.10") (default-features #t) (kind 0)))) (hash "15l821bd18wj6rfgjhiw05f5gb09swcx0zb4mdw790nk0knnv2x8")))

(define-public crate-panini_macros_snapshot-0.0.0 (crate (name "panini_macros_snapshot") (vers "0.0.0") (deps (list (crate-dep (name "aster") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "bit-matrix") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cfg") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "enum_stream_codegen") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gearley") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "panini") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "panini_codegen") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "quasi") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quasi_macros") (req "^0.10") (default-features #t) (kind 0)))) (hash "1g5hq9wfvi1g8w0crjgbcp6ndcsrdcqlxkvsmjcfm865h7glg7ih")))

