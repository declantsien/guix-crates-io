(define-module (crates-io pa to) #:use-module (crates-io))

(define-public crate-patoz-0.1 (crate (name "patoz") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1jigyy2r1c1mby186m4bdq4ms969350bkk7dijjzgw4c9irbdhbn")))

