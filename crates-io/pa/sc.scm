(define-module (crates-io pa sc) #:use-module (crates-io))

(define-public crate-pascal-0.1 (crate (name "pascal") (vers "0.1.0") (hash "097hcp3a58n42h94ppvgnk4pbcqm11lk8z41agni1af5pn6ik31v")))

(define-public crate-pascal_ident_to_string-0.1 (crate (name "pascal_ident_to_string") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "1z7wn14ifxqn1bxzs2wxlzbw723vw4fbqx3iqc8vjiy0qkck8vwz")))

(define-public crate-pascal_string-0.0.0 (crate (name "pascal_string") (vers "0.0.0") (hash "0y5y4zg1djdfmyy311ya5vl6rjw49h4yys9xiwn26imzir3rp68p")))

(define-public crate-pascal_string-0.1 (crate (name "pascal_string") (vers "0.1.0") (deps (list (crate-dep (name "ascii") (req "^0.7") (default-features #t) (kind 0)))) (hash "1yvhcf9s61nxf0hf8cr6rpjxjxrzvdn8wcq8cmsw1b54a37nrsvr")))

(define-public crate-pascal_string-0.2 (crate (name "pascal_string") (vers "0.2.0") (deps (list (crate-dep (name "ascii") (req "^0.7") (default-features #t) (kind 0)))) (hash "0wsn7335hb6n0kknscg0la9yhv1q2gm4wpm6jbfq5fy5ggxsd9wj")))

(define-public crate-pascal_string-0.2 (crate (name "pascal_string") (vers "0.2.1") (deps (list (crate-dep (name "ascii") (req "^0.7") (default-features #t) (kind 0)))) (hash "00rsv2cmd47sdd9apsidxsi8hkz1y2j80szphdl7d4vlxhj6vkm8")))

(define-public crate-pascal_string-0.3 (crate (name "pascal_string") (vers "0.3.0") (deps (list (crate-dep (name "ascii") (req "^0.7") (default-features #t) (kind 0)))) (hash "1xy6r54qy6imy4wqzxb44r4s1kdzggb0xxcikxpba8pfgfnh86ag")))

(define-public crate-pascal_string-0.4 (crate (name "pascal_string") (vers "0.4.0") (deps (list (crate-dep (name "ascii") (req "^0.7") (default-features #t) (kind 0)))) (hash "1541sjjhhw43gaq7rzdl1q8xsnx4cvxxajjqjwsw35qrfmi9fvla")))

(define-public crate-PascalCase-0.1 (crate (name "PascalCase") (vers "0.1.0") (hash "0mykhfzpzpsyqp2ax4zg0z9bwj8sc4mhs1h41075hp21yd1rhj4p")))

(define-public crate-PascalCase-2 (crate (name "PascalCase") (vers "2.0.0") (hash "130ipbjw73sdf5k3hlpnqhdc8ygxy2nsk233nvxvbbi0yybp9jy4")))

(define-public crate-PascalCase-1 (crate (name "PascalCase") (vers "1.0.0") (hash "0jffx0ggmbhzcy7n737rid344pj65np2l9w5xb6vcwz1ggvsqdk6")))

(define-public crate-PascalCase-0.3 (crate (name "PascalCase") (vers "0.3.0") (hash "14n05ylqmcr9i7rkldffwv06v5xb58lhjbqjnvbcxajabawzr215")))

(define-public crate-pascii-0.1 (crate (name "pascii") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (features (quote ("png" "jpeg"))) (kind 0)) (crate-dep (name "rgb2ansi256") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "05vgg74cd10xjlc9zqvwrhv4n4d9pcrmziwj2gyb0m0acxh8x3jf")))

