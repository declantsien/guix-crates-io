(define-module (crates-io pa v_) #:use-module (crates-io))

(define-public crate-pav_regression-0.1 (crate (name "pav_regression") (vers "0.1.0") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1c4py2pjgcc00c81vq4d7vwygbi4hpwnpzknikjrfc9i77dfd36k")))

(define-public crate-pav_regression-0.1 (crate (name "pav_regression") (vers "0.1.1") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1rkgv0r0k6vw2rxf7g4sxd0l9yi8ffimcbc0iqnnyn3xxs9rq4w4")))

(define-public crate-pav_regression-0.1 (crate (name "pav_regression") (vers "0.1.2") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1h0blchkn5w8qb8x2yf7cx1zndcpxzb7xrgfkzsxg1avygs319w9")))

(define-public crate-pav_regression-0.1 (crate (name "pav_regression") (vers "0.1.4") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1ix9i668fn8ampxvry8nxi00s24y60irg5n624ns8df7g2k936mr")))

(define-public crate-pav_regression-0.1 (crate (name "pav_regression") (vers "0.1.5") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0rpkafjhia3fpind731n1gml11g6hxrjg148hw1szmq2njgfa84z")))

(define-public crate-pav_regression-0.1 (crate (name "pav_regression") (vers "0.1.6") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0rcj12drw7szdns7irycnmrmqyv9kg23srlq6zx7i9zkfwf47slx")))

(define-public crate-pav_regression-0.1 (crate (name "pav_regression") (vers "0.1.7") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "013qagawfzyk4qwv00bxifsa5ca0pkbhzg8y66cw4hblfz4zznf8")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.0") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0891dyxgxl6dadi6z3c6h7pgi71039c8qnmsqw2xndnpqzdmqafq")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.1") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "16ls39m5l83qdpvg4g2zjq6i666s6f7w5qpba10908q1q2gfipxp")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.2") (deps (list (crate-dep (name "ordered-float") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1314rgm6cn7swc7npip5hxi4qfjclp81mnv4r9sk5wcf37gfzpz4")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.3") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "0z7grqvvndx603wx01y94g82wrfc11804zjhaplcykj4qhvgjdw4")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.4") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "09jrvc1fd7adc73k48kfvf55p47z8idrmb6hxx5bcgqsnn5dmm4y")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.5") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "171l753w4spgyv5cdsj1b8f8jx2zh9a9xcayi89nnb9jlqss1vm4")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.6") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "162j9ywq3a1drik13mbvna0yj9lldki74xfrvnrziav27n27x97r")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.7") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "1fgyxf7z9msar4dajd5a5bzfxnil9rlz6hj53pllq33a8rlj1480")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.8") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "098bw7jpwqvpfivalbnkdqfjwphk1bk2snq7igxndxlh4wm1p10j")))

(define-public crate-pav_regression-0.2 (crate (name "pav_regression") (vers "0.2.9") (deps (list (crate-dep (name "ordered-float") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "0pvvn94xm09wn4qjpkm5bri8l6m4j8pc3b0rczjc1qbmdav4414a")))

(define-public crate-pav_regression-0.3 (crate (name "pav_regression") (vers "0.3.0") (deps (list (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "0wn20v6s1lbv2gwd76w9jsq3qj1zxq928ahhwlzcs4smb45z0k4y")))

(define-public crate-pav_regression-0.3 (crate (name "pav_regression") (vers "0.3.1") (deps (list (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1h2i9098nsilavh2ha4wvzmnjq5wljg32r8jv3wjpxwmw0vpvsby")))

(define-public crate-pav_regression-0.3 (crate (name "pav_regression") (vers "0.3.2") (deps (list (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "12s81l0sv8vry41pij1fvdq3v7a22k36dfpq3w4acgf2i9dccjb0")))

(define-public crate-pav_regression-0.3 (crate (name "pav_regression") (vers "0.3.3") (deps (list (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1qgd4rpnclrh7n4mx0yh1g8bxi7sjnw2v17qqxz1lrlh3a19a79q")))

(define-public crate-pav_regression-0.3 (crate (name "pav_regression") (vers "0.3.4") (deps (list (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "017kih0z3zgjgg5sjd3almkdjzk13mbsbxx9c8cys5iix835g53d")))

(define-public crate-pav_regression-0.4 (crate (name "pav_regression") (vers "0.4.0") (deps (list (crate-dep (name "ordered-float") (req "^3.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "15jrrpdi5b3yi0lv876v00hdnpc41bg3cii1yn4jpgd5xb4h5rjm")))

