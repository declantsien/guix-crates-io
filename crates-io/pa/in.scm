(define-module (crates-io pa in) #:use-module (crates-io))

(define-public crate-pain-0.0.1 (crate (name "pain") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0nxzsz3z5nih079xv01ragpl9ag2bkcy2vy3a731g8fid49kl1d7") (features (quote (("default")))) (rust-version "1.66.1")))

(define-public crate-painless_input-0.1 (crate (name "painless_input") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "1ifz1mybm5c07c95lp08s4psiypsgp5rn6qn54gk6ynzcsa3ri26") (yanked #t)))

(define-public crate-painless_input-0.1 (crate (name "painless_input") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "19qa2jl728mwl3ryhr9bsxl6zd9634ajkscj4msxfxqnl08b4572") (yanked #t)))

(define-public crate-painless_input-0.1 (crate (name "painless_input") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "04z848vl487dyjdhsp6b6s84lssr2rl95qfcv39c0zn48dzfmgrd") (yanked #t)))

(define-public crate-painless_input-0.1 (crate (name "painless_input") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "0339xffyx8nbrywsnad32p9i4mk94d7qzqvymr67ymycy80gcyg8") (yanked #t)))

(define-public crate-painless_input-0.1 (crate (name "painless_input") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "0bsx9w6h3ngl5px7arpjnszgxl9jg8k710hvjq60n7v91s57j2fh") (yanked #t)))

(define-public crate-paint-0.1 (crate (name "paint") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.29.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "07bs5hsr93h7prl62yk5x4wwk5yczwpjwi9wfm90sgqaffmabxbd")))

(define-public crate-paintbrush-0.1 (crate (name "paintbrush") (vers "0.1.0") (hash "0d46c2jdqpwwhrdcclf1hp299yikqzv2q3c776gqj3g5gyx7hdv9")))

(define-public crate-painted-1 (crate (name "painted") (vers "1.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rspec") (req "=1.0.0-beta.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("consoleapi" "processenv" "winbase"))) (target "cfg(windows)") (kind 0)))) (hash "0wr6zf3a2a76m23d32vxcndmx6y834h70vaffdqkilr86i2v386s") (features (quote (("no-color"))))))

(define-public crate-painter-0.0.0 (crate (name "painter") (vers "0.0.0") (hash "144hjxx5hgckxidbswdqgzmxh6ync90q6ar8bn643valfd9mg5qx")))

(define-public crate-painter-cli-0.0.0 (crate (name "painter-cli") (vers "0.0.0") (hash "0xpijdi6h212xkzamkaqqk18k845lavg2g3zhyh4rnjrxymdjxsa")))

