(define-module (crates-io pa nf) #:use-module (crates-io))

(define-public crate-panfix-0.4 (crate (name "panfix") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1gi0n3lzj8cl2zgs5w2ysa9db9r5j71abn9wcz1q51c2l7vns01v") (features (quote (("debug_mode"))))))

