(define-module (crates-io pa w-) #:use-module (crates-io))

(define-public crate-paw-attributes-0.0.0 (crate (name "paw-attributes") (vers "0.0.0") (hash "1f9lz46a629q3hjsnkiynlqx5nv3zg9pxjwbhqhs0p3i4jv6rxfq")))

(define-public crate-paw-attributes-1 (crate (name "paw-attributes") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.30") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1sasafgblzl87629qlr1arwdfv6qpbqhvyf056snxf16lrlw34fp")))

(define-public crate-paw-attributes-1 (crate (name "paw-attributes") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fda1v7y5pfmg8d2v7m0pyvif6c44qjz914jjn718pdyclrmhd8g")))

(define-public crate-paw-clap-0.0.0 (crate (name "paw-clap") (vers "0.0.0") (hash "1rls277kyrpxm7d3wb9gn2arlhbdk5hq4z12636fzp8fx8gnnmv4")))

(define-public crate-paw-raw-1 (crate (name "paw-raw") (vers "1.0.0") (hash "1wk76ipp34gjh42vivmgdkb2rgr26gwhn34gk7z5l378ixk5j2vz")))

(define-public crate-paw-structopt-1 (crate (name "paw-structopt") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1iwg83xqjpfgpy8wrq173cy7zgkyxfryd230sh34f5qsjdx7zap4")))

