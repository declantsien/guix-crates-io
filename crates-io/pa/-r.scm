(define-module (crates-io pa -r) #:use-module (crates-io))

(define-public crate-pa-rs-0.1 (crate (name "pa-rs") (vers "0.1.0") (hash "16i8bbsp3vhb94f4p777cyh4c20rm26hwxaa63saipc21qmdfcz0")))

(define-public crate-pa-rs-0.1 (crate (name "pa-rs") (vers "0.1.1") (hash "18ijvbi1qk0xhg39288bh5yjzyw88w9rk7y5m9s2gam2pnmirzil")))

(define-public crate-pa-rs-0.1 (crate (name "pa-rs") (vers "0.1.2") (hash "11w6a9jmwgdks4fdwkha46fr24n8mbbgz7d599jjyp6zmzvfbfp9")))

(define-public crate-pa-rs-0.1 (crate (name "pa-rs") (vers "0.1.3") (hash "1hppqm3n057xjq5jflk0szfxrv44vg0plmzwhqygagk1d4gcyyac")))

(define-public crate-pa-rs-0.1 (crate (name "pa-rs") (vers "0.1.4") (hash "07657zjqq1d8f4lbc4qlx9jwgd2ijq75cwqgp5iggxvxg6b8fmpy")))

(define-public crate-pa-rs-0.1 (crate (name "pa-rs") (vers "0.1.5") (hash "0jipghvzjk1ngy3ygv1x5xg1wq4620mhvlkknqjgskclym522jrb")))

(define-public crate-Pa-rsE-1 (crate (name "Pa-rsE") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive" "std"))) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (kind 0)))) (hash "00y2cy1jm21gy0jywsrg30l63mwmiv8xpa9p5xbj0077s681jr2d")))

