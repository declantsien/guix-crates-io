(define-module (crates-io pa ks) #:use-module (crates-io))

(define-public crate-paksir-0.1 (crate (name "paksir") (vers "0.1.0") (hash "0v2jg8q57y0a8liz8l6qbrzz84iwgx7ik5xlcq2mizgz8jsqcqf1")))

(define-public crate-paksir-0.1 (crate (name "paksir") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req ">=0.3.20, <0.4.0") (default-features #t) (kind 0)))) (hash "1wdyaw7agx9mwj08r0lvla505m8avrhy3a85zh0vcgjsgmzqhpbc")))

