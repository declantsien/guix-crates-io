(define-module (crates-io pa vu) #:use-module (crates-io))

(define-public crate-pavucontrolrs-0.1 (crate (name "pavucontrolrs") (vers "0.1.0") (deps (list (crate-dep (name "libpulse-binding") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0d2jpfbhvhkbaviv17v0704s20zgcgvg0jskblayrr74h621476z")))

(define-public crate-pavucontrolrs-0.1 (crate (name "pavucontrolrs") (vers "0.1.1") (deps (list (crate-dep (name "libpulse-binding") (req "^2.28.1") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (features (quote ("termion"))) (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "12r4dibgsv1vsas12g8jiahya1cj49vmc02k7s19jdvxqb2kv9l6")))

