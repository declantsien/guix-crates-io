(define-module (crates-io pa tu) #:use-module (crates-io))

(define-public crate-patum-0.1 (crate (name "patum") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1hxgsx2g6yjnhhvqs0igbrxh0r4j23l1rv9z2a62k2j40q8rh7aw")))

