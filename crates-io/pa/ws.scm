(define-module (crates-io pa ws) #:use-module (crates-io))

(define-public crate-paws-0.0.0 (crate (name "paws") (vers "0.0.0") (hash "0x02jd7vnzah1zmw3nh2rv2yz19j4p0pmiw6dqi70ylgz7djiyfk")))

(define-public crate-paws-0.1 (crate (name "paws") (vers "0.1.0") (hash "05aqkpnnchiy7fwkm6zkrsg7s2xcpspvy1f9b1c82hirnk91qwz6")))

(define-public crate-paws-0.2 (crate (name "paws") (vers "0.2.0") (hash "0pxac196ah07n4qb5cljbhkg71z0fnpqrr1ysr2sphj9n9rabgr4")))

(define-public crate-paws-0.3 (crate (name "paws") (vers "0.3.0") (hash "1jav8lfryc94jk3cdrm7l77lhsvzhxrx20b4h5hbny5942lna0av")))

(define-public crate-paws-0.3 (crate (name "paws") (vers "0.3.1") (hash "0j0fhbnz1znmx9dmlh2zmzh50gpwb9v6fxvh8lccvv82mdnyjflm")))

(define-public crate-paws-0.3 (crate (name "paws") (vers "0.3.2") (hash "049a20kilhrr22r37mm250wd7ph7y1sr41n35aj0xkh3ymmw1c3l")))

(define-public crate-paws-0.3 (crate (name "paws") (vers "0.3.3") (hash "1qghh4njd5l9cs015v6l2gkl5hvrhvl57j66ryb58hfp2lpfij7h")))

