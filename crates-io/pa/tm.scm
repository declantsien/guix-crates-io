(define-module (crates-io pa tm) #:use-module (crates-io))

(define-public crate-patmatch-0.1 (crate (name "patmatch") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1gnrhpbi96gk4z2ix76fcmp2hv7p5yxbnpf0adp7322zkq6dwr6k")))

(define-public crate-patmatch-0.1 (crate (name "patmatch") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1wb9ia88vdv14d4i2x86k1dh6smpd8lznaq6nv2fjy3w87mxcglg")))

(define-public crate-patmatch-0.1 (crate (name "patmatch") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0yqknm4sfj5dxv7qcnwn3mbjiqdqgqnsygmkkzvqq5xkx0vyya95")))

(define-public crate-patmatch-0.1 (crate (name "patmatch") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "18l9x6npkibp6m0zxvq1pldhsga9y9pyvaibcngwkl9zvxv3ahs7")))

(define-public crate-patme-0.1 (crate (name "patme") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.0") (features (quote ("yaml_conf"))) (kind 0)) (crate-dep (name "lettre") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "powershell_script") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.144") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.144") (default-features #t) (kind 0)))) (hash "0cgz70zx4pmq119y941fpiiq6d3c9hs42lkyz084m1j5cqa1dg9n")))

