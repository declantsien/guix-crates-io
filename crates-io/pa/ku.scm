(define-module (crates-io pa ku) #:use-module (crates-io))

(define-public crate-paku-0.0.1 (crate (name "paku") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1zdljcc2nd3hrsp303j9dfq9jxkpk17c8aaf8gm66gf392sj6510")))

(define-public crate-paku-0.0.2 (crate (name "paku") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "031vllhbmgmi339mn80rxbh9ljj5xj96j80wfs11gq6fd6zq54j6")))

