(define-module (crates-io pa hs) #:use-module (crates-io))

(define-public crate-pahs-0.1 (crate (name "pahs") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "0bjgyk16vh2lmri5nsjjg96n42y39dqhxzvrj0pjcjnlzf79j2j9") (features (quote (("with_snafu"))))))

(define-public crate-pahs-0.1 (crate (name "pahs") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "0c1bjkz4s9yi42s2pvzc6sggxy9nzviidqfhnwp0ai3qqqpcw81a") (features (quote (("loop_assert") ("default" "loop_assert"))))))

(define-public crate-pahs-0.1 (crate (name "pahs") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "0695bm4nmlziylnwjshmxx3dyyq3rqwflzdpijbc77b9lvx7d1ax") (features (quote (("loop_assert") ("default" "loop_assert"))))))

(define-public crate-pahs-0.1 (crate (name "pahs") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "0k5nn2zvcsj4d2xn3rq8j07xjnwnhs7bxwjslvg3l7ppfg7jnw6n") (features (quote (("loop_assert") ("default" "loop_assert"))))))

(define-public crate-pahs-snafu-0.1 (crate (name "pahs-snafu") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "easy-ext") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "pahs") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "051rg64k11q16lgkah6jj5mvvdpc7mig78hkpjck84dkvr9l9cyi")))

(define-public crate-pahs-snafu-0.1 (crate (name "pahs-snafu") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "pahs") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1nk6svqm84mwsh0cfrjj9c26c852k33kql1prni6lz5lgz64hbrl")))

