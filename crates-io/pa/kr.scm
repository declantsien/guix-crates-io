(define-module (crates-io pa kr) #:use-module (crates-io))

(define-public crate-pakr-0.1 (crate (name "pakr") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "iced") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "09rcri1pmq6mpsad0g3s79csflhi52v8328hhpibzj14k02ca70s")))

(define-public crate-pakr-assert-size-1 (crate (name "pakr-assert-size") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jigi9f86rkg6bbh9faffs1aqj019hyc7ggaib74vi0pgvg485i1") (yanked #t) (rust-version "1.57.0")))

(define-public crate-pakr-assert-size-1 (crate (name "pakr-assert-size") (vers "1.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s6a8kpii8ilmyizpyr01qgbiicjq6958zgniqxz7pa6wbvb7ay9") (rust-version "1.57.0")))

(define-public crate-pakr-fsm-0.1 (crate (name "pakr-fsm") (vers "0.1.0") (hash "1wkqs4gxnwxkvsay8va4pd8r0agzx8q3sw4441hzhg7cckgsw3sa")))

(define-public crate-pakr-fsm-0.1 (crate (name "pakr-fsm") (vers "0.1.1") (hash "1j8mnjbjlcgvbm4akacx0i1i9hpd5aqdv5jivaiiygmsbaikv2wi")))

(define-public crate-pakr-iec-1 (crate (name "pakr-iec") (vers "1.0.0") (hash "131bks03d09wiw97rpb3fq69gf68hq3n3ag4ap7y80rra1prk4ai")))

(define-public crate-pakr-iec-1 (crate (name "pakr-iec") (vers "1.0.1") (hash "1qnn4dxbb10fdq8bpnywqihnnqs379w5k82y3pcf23q6za3cp5a3")))

(define-public crate-pakr-managedrawfd-1 (crate (name "pakr-managedrawfd") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0942lyncam5idp5n7hx62wijvlki4bnyyw43gwxgwm9pn0l58v4v")))

(define-public crate-pakr-mio-afpacket-0.2 (crate (name "pakr-mio-afpacket") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.7") (features (quote ("tcp" "os-util" "os-poll"))) (default-features #t) (kind 0)) (crate-dep (name "pretty-hex") (req "^0.2") (default-features #t) (kind 2)))) (hash "1ynj8kqxmhw44q4bz6g11nc9qycqlnxi18bfdmnzwrinm6qqfx30")))

(define-public crate-pakr-mio-signalfd-1 (crate (name "pakr-mio-signalfd") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.7") (features (quote ("os-poll" "os-util"))) (default-features #t) (kind 0)) (crate-dep (name "pakr-signals") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v66xa0wa9x6aayjcsi8i6h4ph324l3lxbf0q6cbix37a8a6hm2x")))

(define-public crate-pakr-rawata-1 (crate (name "pakr-rawata") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "097rvk7dy2l90khgfy2mdcgs182kanal9cc1ay2ysxa387x5nblg")))

(define-public crate-pakr-signals-1 (crate (name "pakr-signals") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15qr9pybq86b7nm2va2b18yw2hsky6njplj629irz6rip4kgmw1w")))

