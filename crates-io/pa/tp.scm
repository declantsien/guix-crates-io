(define-module (crates-io pa tp) #:use-module (crates-io))

(define-public crate-patp-0.1 (crate (name "patp") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xinpjd0vyqh6d2pxa10l84igd9l6w3bwz557axpsvxzbdgy3wlh") (yanked #t)))

(define-public crate-patp-0.1 (crate (name "patp") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nd7s21r166rkgsddyd4v7mrvcv89d71yknsi662ls975z5wjdd4")))

(define-public crate-patp-0.1 (crate (name "patp") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g3c7h6nvrllxx3azhp6g5w5226w6345mrhzplzks5p9bdbpjns6")))

