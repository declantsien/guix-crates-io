(define-module (crates-io pa ls) #:use-module (crates-io))

(define-public crate-pals-0.1 (crate (name "pals") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cmdline_words_parser") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "escape8259") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)) (crate-dep (name "trees") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "utf16_reader") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "119nxknd3q7f8k3blxkvhfqb35g6iw31r72fmpc43zbppvy9c8d8")))

(define-public crate-pals-0.1 (crate (name "pals") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cmdline_words_parser") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "escape8259") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)) (crate-dep (name "trees") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "utf16_reader") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1g8whdd3a0yr9babfn8qvksvrh2lsskv121va9p8yxrz2jag948a")))

(define-public crate-pals-0.1 (crate (name "pals") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cmdline_words_parser") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "escape8259") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)) (crate-dep (name "trees") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "utf16_reader") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0168ysaaxam0884xnk15361w8lraizwbjla87hbymhgr0j5r1bpq")))

(define-public crate-PALSerial-0.1 (crate (name "PALSerial") (vers "0.1.0") (hash "0g0ln4l0gbhygyilsimcmnxy0fbbbap07y7mlzagcp87n5s8x572") (yanked #t)))

(define-public crate-PALSerial-0.2 (crate (name "PALSerial") (vers "0.2.0") (hash "0bmh4r1kky1czdvfv1rkwrclxirz1ycrcan7r259wdqzjhbx7hn9")))

(define-public crate-palserializer-0.2 (crate (name "palserializer") (vers "0.2.0") (hash "0qfm3fv0l1yqa5bkggghk3d0f2yx8l7qpyd8x9jviixrlmq43c78")))

(define-public crate-palserializer-0.1 (crate (name "palserializer") (vers "0.1.2") (hash "1rwpjbdvb8y540pcba2745rvhxhbsi16zkaharbl574yhng0xyqn")))

(define-public crate-palserializer-0.2 (crate (name "palserializer") (vers "0.2.2") (hash "1qvb2kqa8gdlid424kf3gcyfbh044ln52vh359rbi2rapcamgyg4")))

(define-public crate-palserializer-0.2 (crate (name "palserializer") (vers "0.2.3") (hash "0cz3x91hchlhakws1qxdg530qrrq8354hd36bqb2gqz68km13lqk")))

(define-public crate-palserializer-0.2 (crate (name "palserializer") (vers "0.2.4") (hash "01vsinj48varzri8ifyg77d0rlra8p5dxlc8hahq4x5jq4g05mfp")))

(define-public crate-palserializer-0.2 (crate (name "palserializer") (vers "0.2.5") (hash "18qnlb3hf3hw02z099a4lz3sbj57fpdh05zxczv956g3axng1kin")))

(define-public crate-palserializer-0.3 (crate (name "palserializer") (vers "0.3.0") (hash "1qfcc3yzdgxnzr84y3cr8inhm304hcbd8xwibrapbj6a3sz0fqjx")))

