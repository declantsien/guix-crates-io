(define-module (crates-io pa ms) #:use-module (crates-io))

(define-public crate-pamsm-0.1 (crate (name "pamsm") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0iczf9fmy8zqqgmmpncm1wh803srw4mvrhw7hi2ydsgl6gxnfvf1")))

(define-public crate-pamsm-0.2 (crate (name "pamsm") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "03687yi5mamsl8p3vwnlka9pb6cfqfdk410cv8ahl83ligb27nan")))

(define-public crate-pamsm-0.3 (crate (name "pamsm") (vers "0.3.1-alpha.0") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "03fwa2722bzp7ap21z0y4kcwax466h5nacdjpinahv7ar7caffbh") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.3 (crate (name "pamsm") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "0r48d23jacq01bk0bjda1cik4n30f2xyazv0rzqzq1gwngal9z84") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.3 (crate (name "pamsm") (vers "0.3.3") (deps (list (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "01z3cqilm22npirgpnfk6dx6ncfjfqvygsf9fq33bjlmpq2fjxq4") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.3 (crate (name "pamsm") (vers "0.3.4") (deps (list (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "00msj83idp6cxl3l0xpyz5csfq22cic0n91a2m3v3wj72hca6hqn") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.4 (crate (name "pamsm") (vers "0.4.0") (deps (list (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "1p5a5nqhz2h0pam5hq7lv9h16irlwavc4lv9fi5nc1pyjxn8c1rm") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.4 (crate (name "pamsm") (vers "0.4.1") (deps (list (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "1pa3kvhqdhg918fnkis0ka6vq1sbpy532crjb3dp8p07pqpfv01m") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.4 (crate (name "pamsm") (vers "0.4.2") (deps (list (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 2)))) (hash "0sga9brxszyf52hg2bwil0japard7aqisiw23x5rfjhwxpnam853") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.4 (crate (name "pamsm") (vers "0.4.3") (deps (list (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "16xc0kjpqdqcvzha16hrm5l8jqz7zxsl0nhgg8cjp7ih3374gr5l") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.5 (crate (name "pamsm") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "09302mc1mgjdgqrcz3ddssyv672mx4j1278q2b7krjbmcipyn8l9") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.5 (crate (name "pamsm") (vers "0.5.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "07pilh1qw2d6w3cm1sivf0pyl6vw2j5gwrl4f1fgxvhccdv0q74a") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.5 (crate (name "pamsm") (vers "0.5.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "0imdraklnw3avm8xxcp63jr3bwgxdjdcsbiky4lp1bynhbzf5mv2") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.5 (crate (name "pamsm") (vers "0.5.3") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "1vwrllyhmkqhq82dscdhffkjcgaq092jqi3chkf55fffmqnsjd98") (features (quote (("libpam"))))))

(define-public crate-pamsm-0.5 (crate (name "pamsm") (vers "0.5.4") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "02207ssjrjknv69agvwcyv7xpgy871p1ndhgjsn46rlpj159z84y") (features (quote (("libpam"))))))

