(define-module (crates-io pa wa) #:use-module (crates-io))

(define-public crate-pawan_art-0.1 (crate (name "pawan_art") (vers "0.1.0") (hash "13vjklf81m4c5kymc167f0j17lqm93lgfmnnilcgwyq106w7ffcn")))

(define-public crate-pawawwewism-0.1 (crate (name "pawawwewism") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "16sy0z1nv22bw6qvip3dba7isyqx3v6sqg05q2ca5s0yk031r1sz")))

(define-public crate-pawawwewism-0.1 (crate (name "pawawwewism") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0l8ki9q3jn1v0zzcvrp54j5g563spm0vszfkdwxd19g25cp54khg")))

(define-public crate-pawawwewism-0.1 (crate (name "pawawwewism") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "07swvhvks1vk1vx2qg25mvc4i7dxv9qzx7nzialfv7a11cz6z5mc")))

(define-public crate-pawawwewism-0.1 (crate (name "pawawwewism") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1dsfr22vrznxq26yahbgx2pp8xgjd2nv0453a3ara5jv3a74ayp5")))

