(define-module (crates-io pa nm) #:use-module (crates-io))

(define-public crate-panmath-0.1 (crate (name "panmath") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.22") (default-features #t) (kind 0)))) (hash "05zfly81439808abfyb2ks55dskaj34840jwqc224qhd5j3sbgmb")))

(define-public crate-panmath-0.1 (crate (name "panmath") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.22") (default-features #t) (kind 0)))) (hash "1axkyk6l82qcy2cdfhjyvlmdalsi3brgl2ysi22hwka4m0i4m3h0")))

