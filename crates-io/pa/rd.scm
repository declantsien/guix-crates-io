(define-module (crates-io pa rd) #:use-module (crates-io))

(define-public crate-pardiso-0.1 (crate (name "pardiso") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "ndarray-linalg") (req "^0.12") (features (quote ("intel-mkl"))) (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pardiso-src") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "pardiso-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y9dxcxc9ncsv0s9kw9myv9n8xqby8skyzjyk6bvk9rfbqp9q2gc")))

(define-public crate-pardiso-src-0.1 (crate (name "pardiso-src") (vers "0.1.0") (deps (list (crate-dep (name "intel-mkl-src") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0shglqh33dy86saip1m0xln2hrm5prl1634vzlwdma0903dhksm9")))

(define-public crate-pardiso-sys-0.1 (crate (name "pardiso-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w8nsann3bs5wa5vn467w9ij25bx8yj59mm1yb4phzi5q03wx3pn")))

(define-public crate-pardiso-sys-0.2 (crate (name "pardiso-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1imnl5bf8i750hmk7p67rynq5jvz7cm99slskhy89sqapb573kjg")))

