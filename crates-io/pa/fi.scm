(define-module (crates-io pa fi) #:use-module (crates-io))

(define-public crate-pafi-0.1 (crate (name "pafi") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "05350r08l65ynyrghc123cycvarjv8nlvqgdpcdrjdzq441sfqpb")))

(define-public crate-pafi-0.1 (crate (name "pafi") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "06xyydsnwi1rim4d74v6g94pxik733wnji302d7z74k3bpd9d6af")))

