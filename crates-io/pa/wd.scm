(define-module (crates-io pa wd) #:use-module (crates-io))

(define-public crate-pawd-0.1 (crate (name "pawd") (vers "0.1.0") (deps (list (crate-dep (name "psutil") (req "^3.2.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.192") (default-features #t) (kind 0)))) (hash "1wpm8xz8sc5hmy4xcz45dxbrnvdwqmaq1482fqnqp725pgahgaka")))

(define-public crate-pawd-0.1 (crate (name "pawd") (vers "0.1.1") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "psutil") (req "^3.2.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1a2pmqkw8frnfsxn02jc0qpb7knyg6xalgcnpga7a7nkx5pj9pgm")))

