(define-module (crates-io pa ym) #:use-module (crates-io))

(define-public crate-payment-0.0.0 (crate (name "payment") (vers "0.0.0") (hash "0nk83a57gwpi7swak2wfwnqj4hdz2vp10vncs9yr209s3k9wkn0v")))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.0") (features (quote ("cpi"))) (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.6.9") (default-features #t) (kind 0)))) (hash "1vmw66k6ba3hjh2k282qfwa7jbfq4b20s883lj27kx6414yjnf4r") (features (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.1") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.6") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "0x9r2xjvs73fd8iywf9720imy4rykhdpfz226kn0ipjjgxwbqnr3") (features (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.8") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "0m6bd2y3sxvkk9hq6r6x04p5gwcaqqi2apqm7psgnx688f959nnq") (features (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.3") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.8") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "1zwn41yfxf55jhgzp6d17cd4w2xdn20mp4sgp0n2x2vkp5l7m7as") (features (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.4") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.8") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "1lvyzkjdp0d007m6sbmv2773hw1l4c6i0k2r7c2sgvrg8v3c85cv") (features (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.5") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.8") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "0wi50jk0x7p5894385rn69hcrspy6yl4q4vkcffpk32d6v6xzpmh") (features (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.6") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.8") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "18ii9dxgw9h8jw87yjgvx5fzwg6zyxcs8xanqm0ws70v4cz2g86y") (features (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment-program-0.1 (crate (name "payment-program") (vers "0.1.7") (deps (list (crate-dep (name "anchor-lang") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "index-program") (req "^0.1.11") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "104ycddwjwk5blhy5g8q63486xckrhaghnymdh0phkr9l8xbii9a") (features (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-payment_strings-0.1 (crate (name "payment_strings") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xfl4dyaw8zh4q5idasay8zs8r3m8k36l3qippbzlxvnc9h88868")))

(define-public crate-payment_strings-0.1 (crate (name "payment_strings") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jvyypj8k4c4d6fc1zvywpcx29mza1yb9rssnprvs6hqqda6ms8z")))

(define-public crate-payments-0.1 (crate (name "payments") (vers "0.1.0") (hash "0m2ff9m6jydri83dmgfhggd75rdyf47jb9clgzvvnl8v8srk616r")))

