(define-module (crates-io pa li) #:use-module (crates-io))

(define-public crate-palimpsest-0.0.0 (crate (name "palimpsest") (vers "0.0.0") (deps (list (crate-dep (name "abscissa") (req "^0") (default-features #t) (kind 0)))) (hash "10b0jnny7vrmjz3c3nhb4bib0j2wmafwx2kb90djd00nj4hiiyib") (yanked #t)))

(define-public crate-palin-0.1 (crate (name "palin") (vers "0.1.0") (hash "117ysvjzrin8z4g7l7mzagf4m5zqdj3wni9yri6mfb0w4024g0jg")))

(define-public crate-palin-0.2 (crate (name "palin") (vers "0.2.0") (hash "12d64lfh6ar6kakwb4zrb2s4h9rvlalwr8qadgyqf96lgd5jhz2k")))

(define-public crate-palin-0.3 (crate (name "palin") (vers "0.3.0") (hash "1g3vls1n8y0jc9q7aks293cvjrn85qjjwdi4y20nxbnm6bahi2xh")))

(define-public crate-palin-0.4 (crate (name "palin") (vers "0.4.0") (hash "1m0hyz11z9g4i12qkvqz7k00769fk6n3xv07gwfwdxvmkb96a3sg")))

(define-public crate-palindrome-0.1 (crate (name "palindrome") (vers "0.1.0") (hash "101a4rccq9fpql68kqiv1pxfxz7bscbqhd0ry5k6qmgdvinyjphh")))

(define-public crate-palindrome-0.1 (crate (name "palindrome") (vers "0.1.1") (hash "0687kzrv0si9sgkhwny2rfr2y5phx3rn4ma3izsg2qppjpc2s3md")))

(define-public crate-palindronum-0.0.1 (crate (name "palindronum") (vers "0.0.1") (hash "04k8byifz3rzrmqxdzx9w13wnnq5afgqml25bkfnszgx1jpmr36q")))

(define-public crate-palindronum-0.0.2 (crate (name "palindronum") (vers "0.0.2") (hash "0jvmhpc369am1jzwvdjs3w44ibyia9hsh4g2y829qxvhbg6yqv8k")))

(define-public crate-palindronum-0.0.3 (crate (name "palindronum") (vers "0.0.3") (hash "16hvk40q07d12msbgrdn1l8vcrgncs4yjq809nrdlz0aqndbdy13")))

(define-public crate-palindronum-0.0.4 (crate (name "palindronum") (vers "0.0.4") (hash "15547m7xjjms8qsdbj41gzn1w6bw06y8idswyvcg62hz3h09y8ba")))

(define-public crate-palindronum-1 (crate (name "palindronum") (vers "1.0.0") (hash "1gwcg698fvdxwad0zrxg2s4m0mbs3fzcxwky3nvlg4ix2y65jp9i")))

(define-public crate-palindronum-1 (crate (name "palindronum") (vers "1.0.1") (hash "0ss3qhhrb4vaafqqf8dzfs11mn52rkcixi6ns5qx4xyp7av2qnhk")))

(define-public crate-palindronum-1 (crate (name "palindronum") (vers "1.0.2") (hash "1d87gigw08s1vj10sc2xnim474r0l8xp19dvdvylhyydlm4fx8cn")))

