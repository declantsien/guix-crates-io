(define-module (crates-io pa rf) #:use-module (crates-io))

(define-public crate-parfait-0.1 (crate (name "parfait") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1silm8syhks06jm030c1xh2n48bhyjykvkb2rdm17rnc6l9s886a")))

(define-public crate-parfait-0.1 (crate (name "parfait") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01vpvifnlsbkj0n29r7k92hj925f6h6fr198n0h9nhyizmsq2rcl")))

(define-public crate-parfo_openapi-0.1 (crate (name "parfo_openapi") (vers "0.1.0") (hash "14vq8q3j7f6vigcd6jjx8k7fh790fr15irp06r54bf6cbzxwhfa7") (yanked #t)))

(define-public crate-parfo_openapi-0.1 (crate (name "parfo_openapi") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "serde") (req "^1.0.115") (features (quote ("derive" "std"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.13") (default-features #t) (kind 2)))) (hash "0f29vhy7nd01x4qpx156dnzpv6dvv275di3h2v4y4sydpapwdlhf") (yanked #t)))

(define-public crate-parfo_openapi-0.1 (crate (name "parfo_openapi") (vers "0.1.1-alpha.2") (deps (list (crate-dep (name "serde") (req "^1.0.115") (features (quote ("derive" "std"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.13") (default-features #t) (kind 2)))) (hash "1kg97rqr8b8vj40r1pqsd9gf6phz3nq5flj94w90isznl2iic29s") (yanked #t)))

