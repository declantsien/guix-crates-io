(define-module (crates-io pa km) #:use-module (crates-io))

(define-public crate-pakman-0.1 (crate (name "pakman") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.55") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "09b45cjpdng4frk80p23r34gf7g4pya4wq4z74zfpy36343pyc2i")))

