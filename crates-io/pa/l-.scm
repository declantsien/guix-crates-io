(define-module (crates-io pa l-) #:use-module (crates-io))

(define-public crate-pal-sys-0.1 (crate (name "pal-sys") (vers "0.1.0") (deps (list (crate-dep (name "autotools") (req "^0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "0.3.*") (default-features #t) (kind 2)) (crate-dep (name "pkg-config") (req "0.3.*") (default-features #t) (kind 1)))) (hash "16cc3g2pzm98jvlr1m8da0dwxb3lcv5f8vzvpkskf59c4v8v4ay9") (features (quote (("static" "autotools")))) (links "pal")))

