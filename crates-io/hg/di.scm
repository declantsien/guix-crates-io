(define-module (crates-io hg di) #:use-module (crates-io))

(define-public crate-hgdirectffi-0.1 (crate (name "hgdirectffi") (vers "0.1.0") (deps (list (crate-dep (name "hg-core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q1z82908w0qyrcjwbpa4a7jc1h1wcz7y9z9aqp7isfy8274bzhy")))

