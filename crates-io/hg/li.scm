(define-module (crates-io hg li) #:use-module (crates-io))

(define-public crate-hglib-0.1 (crate (name "hglib") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "0aivmhcyl4vnb1a742l4xa91sb0wqpqkqz1pjb3p9224anya085b")))

(define-public crate-hglib-0.1 (crate (name "hglib") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "1yzg3sq6qw0bhhy0v2zw8kdb10d7x07kwhs43n350dq4cbd0fp94")))

(define-public crate-hglib-rs-0.1 (crate (name "hglib-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w8n232h6nwa1ixnkalqiqpx575lr2b06c91s5z7yr2wgpm32dy0")))

(define-public crate-hglib-rs-0.1 (crate (name "hglib-rs") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "1x329n728pm1hzx5dp3wm7m2g4pqpvgq58cgcxfpbg4hjzn4m888")))

