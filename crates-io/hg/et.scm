(define-module (crates-io hg et) #:use-module (crates-io))

(define-public crate-hget-0.0.0 (crate (name "hget") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1iinm041hz8yf4hhxfkyqdxqihcvzvha5pac4yjldf0wrvkhpbgp")))

(define-public crate-hget-2 (crate (name "hget") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0mrsnilpp7fqg9ca72slzsgby5k2mrrabhza6arwj2qqbj9fcjrr")))

