(define-module (crates-io hg -c) #:use-module (crates-io))

(define-public crate-hg-core-0.0.1 (crate (name "hg-core") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "~0.6") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "~0.1") (default-features #t) (kind 2)))) (hash "1163acgf283jsb530mr8cav37qinf01dr7rjdbdbkbgbmppniqbz")))

(define-public crate-hg-cpython-0.0.1 (crate (name "hg-cpython") (vers "0.0.1") (deps (list (crate-dep (name "cpython") (req "~0.2.1") (kind 0)) (crate-dep (name "hg-core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "python27-sys") (req "~0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "python3-sys") (req "~0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "1fikqci2bzm4wh5dbwf27l71wg9m0s59ldybvwdb07b7j6r6fy9w") (features (quote (("python3" "python3-sys" "cpython/python3-sys" "cpython/extension-module") ("python27" "cpython/python27-sys" "cpython/extension-module-2-7" "python27-sys") ("default" "python27"))))))

