(define-module (crates-io tu ti) #:use-module (crates-io))

(define-public crate-tutil-0.1 (crate (name "tutil") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.54") (optional #t) (default-features #t) (kind 0)))) (hash "0idgjp5xf8glwj2x2vnyyy4i0g3xa37r0xkckya4kq4k5yjs2x8b") (features (quote (("lints" "clippy") ("default")))) (yanked #t)))

(define-public crate-tutil-0.1 (crate (name "tutil") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.54") (optional #t) (default-features #t) (kind 0)))) (hash "0a2ll8cny9dy7pn1kq3hyq278v3fpvyjawsmx6crszc6xvd03z1k") (features (quote (("lints" "clippy") ("default")))) (yanked #t)))

(define-public crate-tutil-0.2 (crate (name "tutil") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "~0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1fcl9mnvsrl2gik3b4xgh1iv61878a77zfm36nni9vxwia35mdw8") (features (quote (("lints" "clippy") ("default")))) (yanked #t)))

