(define-module (crates-io tu xp) #:use-module (crates-io))

(define-public crate-tuxphones-0.1 (crate (name "tuxphones") (vers "0.1.0") (deps (list (crate-dep (name "libpulse-binding") (req "^2.26.0") (default-features #t) (kind 0)))) (hash "0rfwj443x72zala3n9qsfq7s57npaf71bpwq9ql7piac7kw7cnlm")))

