(define-module (crates-io tu ck) #:use-module (crates-io))

(define-public crate-tuck-0.0.0 (crate (name "tuck") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-rc.9") (features (quote ("std" "derive" "cargo"))) (kind 0)))) (hash "0xa6pbhq946m47pv55g6b5386g2yjvq6k26fa5ard8c0vxd3ghzc")))

(define-public crate-tuck5-0.1 (crate (name "tuck5") (vers "0.1.1") (deps (list (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1k2s6xh34x9h1j6y8yqgw169hhr564041yb27027w6jc0vgjz5h9")))

(define-public crate-tuck5-0.1 (crate (name "tuck5") (vers "0.1.2") (deps (list (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1az5m98lbfs5v5qa6l8p9i7hjbv7dnzk12kpp1l1m4iq68xbskjv")))

(define-public crate-tuck5-0.1 (crate (name "tuck5") (vers "0.1.3") (deps (list (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0id8xrzl4l7ggriw9cbdsp8161p7nxz84vpiyg2vq81zm3s2jvsr")))

(define-public crate-tuck5-0.2 (crate (name "tuck5") (vers "0.2.0") (deps (list (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0vy0gvjbjlck38rchxf3j1fb4sg2k6ql0iy51ya8sdldpgq0c7s7")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.0") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "1fbv040vzsrncwg76b0wl6v3a88wcyi6m545gplk7ggi0zxh9vij")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.1") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "0kpjkavj8vgqwackda7z6h91f30bj3l7n26qd5whjjdf6rc6hh34")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.2") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "1lzd13qd4pg79k2sbz5y2wph2shlg039q3awc29jz8rsmqx23x5i")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.3") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "089c1k8xq426pw4rpn9fbppkaz1x6zy9w9bvkcwn3lrbw3srkxnj")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.4") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "0ivckcncnlz3g6z4xya9im27l332n6xc4vknlrkm51k6q818hrs2")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.5") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "19qwx824qr3c3n0w51zfv3s8ggkc2hadq2k6l8a9ap733yl70gvh")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.6") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "18rbz7qmsdfh4yczj02wj9mpiv4cmpgaf1vyxfmk5p38np7ix07v")))

(define-public crate-tuckey-0.1 (crate (name "tuckey") (vers "0.1.7") (deps (list (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 0)))) (hash "0sawm6y32w0fbzvv21j85zsyy9g50jgfrkaz6qa8691ih6hizdl9")))

