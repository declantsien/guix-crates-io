(define-module (crates-io tu rl) #:use-module (crates-io))

(define-public crate-turl-0.1 (crate (name "turl") (vers "0.1.0") (deps (list (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0z2c040fwb4y2v5lzd1w2nrl3hl93l8vm9yr0qyn6ly8k01hdisp")))

(define-public crate-turl-0.2 (crate (name "turl") (vers "0.2.0") (deps (list (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1rpd6crb68zpyjn66mp525912wsv4f997k53gfyvqy8f086064qi")))

(define-public crate-turl-0.3 (crate (name "turl") (vers "0.3.0") (deps (list (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1qisal2pkpk7cn1lirf5kvp45awham14rnmcjh3hqkrwh42bf30b")))

