(define-module (crates-io tu du) #:use-module (crates-io))

(define-public crate-tudu-1 (crate (name "tudu") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)))) (hash "17wkzp5i81a5rjy8snm35sv4rrhf4iqv74jr4g2ccp2x7hfhharx")))

(define-public crate-tudu-1 (crate (name "tudu") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)))) (hash "09cw6mwqjicmm322zyial3wz3zh3g63ca84c2l4cv1i861jgj83p")))

