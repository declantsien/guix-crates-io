(define-module (crates-io tu in) #:use-module (crates-io))

(define-public crate-tuine-0.0.0 (crate (name "tuine") (vers "0.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.13.0") (kind 0)))) (hash "0hs1z1r3flp6nqkarf4jny0f1fhxi9dh9j21nwhvnbczfwnx24gx")))

