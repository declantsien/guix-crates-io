(define-module (crates-io tu rk) #:use-module (crates-io))

(define-public crate-turk-0.1 (crate (name "turk") (vers "0.1.0") (hash "0s5f1s94ycga0c2xxl555lw3jfzhjacazvl5mdwyinsa0h0l8k3n")))

(define-public crate-turk-0.2 (crate (name "turk") (vers "0.2.0") (hash "1dg41y35sljr5anb26xcx6qq9lg6z97mihbg1cf0abaw20arrj3p")))

(define-public crate-turk-0.3 (crate (name "turk") (vers "0.3.0") (hash "1xn0ys5bkvj3sig2nmkm67j3bmrwcvwjkk1a848h0rlfwjxnqmxx")))

(define-public crate-turko-0.1 (crate (name "turko") (vers "0.1.0") (hash "1v0la84dwc4cgk2gnmbq7a93mrjf9src081bvdc5xwpsb9cnndwi")))

