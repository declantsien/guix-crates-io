(define-module (crates-io tu iw) #:use-module (crates-io))

(define-public crate-tuiwindow-0.1 (crate (name "tuiwindow") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1dd2qv79h5lywcijr216j6cad9nzlqp9vhw7bdn0cc66fmmvnjmq")))

(define-public crate-tuiwindow-0.1 (crate (name "tuiwindow") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0b2i7akj0rcfa5n181a4zv52n0krc098glyw7xljch9nzgxr97p1")))

