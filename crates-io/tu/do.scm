(define-module (crates-io tu do) #:use-module (crates-io))

(define-public crate-tudo-0.1 (crate (name "tudo") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "16wfic6pcinmirva64xqmcz3i83gjgwllsw2dl4mj9i72pbhlbli")))

(define-public crate-tudor-0.0.1 (crate (name "tudor") (vers "0.0.1") (hash "1y3axqg33i5pnb3z53b7vi22h9jy3xgaazjyb4fnn0zgg57qnkla")))

(define-public crate-tudor-0.0.2 (crate (name "tudor") (vers "0.0.2") (hash "1ql9r4p9bbvzgpgjv43adjkiw2qnxis3l77plpsf296l2d0y7k2k")))

(define-public crate-tudor-0.0.3 (crate (name "tudor") (vers "0.0.3") (hash "19k2i9dlw35vbxh6pz4gdfny799ja6vfm3v98jkhw8dcz8hjq0wg")))

(define-public crate-tudor-cli-0.0.1 (crate (name "tudor-cli") (vers "0.0.1") (hash "01025rqg74zwfc7yam2j4lg0y82kjqn0nllm5p35y2c65rrgx6fv")))

(define-public crate-tudor-sql-0.1 (crate (name "tudor-sql") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_consume") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "simplelog") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1cfyflmidakrsb2kh7wll3mw0vr6sfa0k783kz3a1c09qcl9z7al")))

(define-public crate-tudor-sql-0.2 (crate (name "tudor-sql") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_consume") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "simplelog") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1l55s16vd2ywr2v5lk0jnqvj8hba225g8dsj3pj2hbck3y8x24pn")))

(define-public crate-tudor-tui-0.0.1 (crate (name "tudor-tui") (vers "0.0.1") (hash "1nvqshlcy83knwz6fwaqxl6iv58n61w1sa5xkdxvz00x7idmn9sz")))

