(define-module (crates-io tu xf) #:use-module (crates-io))

(define-public crate-tuxfetch-0.1 (crate (name "tuxfetch") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nixinfo") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1zirh129js5b3in5fwm7jq17zrdai8564h7340qvgr5vhlc43g2m")))

