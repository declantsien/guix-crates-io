(define-module (crates-io tu rr) #:use-module (crates-io))

(define-public crate-turret-0.1 (crate (name "turret") (vers "0.1.0") (hash "0ys4mf8jgx40x3k1arfvl7agk34720d83nwc6hkw856gb9l8fr4x")))

(define-public crate-turron-0.0.0 (crate (name "turron") (vers "0.0.0") (hash "1xamyg6z7hbvmpq97k1i0hzfqmj3dl0nl3b12195w5fhd4jihb3v")))

