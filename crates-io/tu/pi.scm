(define-module (crates-io tu pi) #:use-module (crates-io))

(define-public crate-tupiter-0.1 (crate (name "tupiter") (vers "0.1.0") (deps (list (crate-dep (name "tupiter-proc-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jy0lfhzblhjy6x5j0yvbkwrnajmajlrz1vqfb5pw7zqsai1vw5b")))

(define-public crate-tupiter-0.1 (crate (name "tupiter") (vers "0.1.1") (deps (list (crate-dep (name "tupiter-proc-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05jj6smj56fih52fv4awbkwq956b8a1yi3nvc6dhanl39jy0pvr6")))

(define-public crate-tupiter-proc-macro-0.1 (crate (name "tupiter-proc-macro") (vers "0.1.0") (hash "02la9lmqb5bnqf8vqvzcbywlhpa8v57jvvrr5fi64f2n0g1qh41h")))

