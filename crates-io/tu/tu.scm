(define-module (crates-io tu tu) #:use-module (crates-io))

(define-public crate-tutu-0.1 (crate (name "tutu") (vers "0.1.0") (hash "10dkrn4zngg8i6qyg68lbilriaid719s685agr1dr4l1lc8hq4aw")))

(define-public crate-tuturu-0.1 (crate (name "tuturu") (vers "0.1.0") (hash "0qzfr83nwjyriwv9hvam8068zzjr9h4s7ldkd2ns47g8v3xnqrgg") (rust-version "1.67")))

