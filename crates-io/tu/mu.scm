(define-module (crates-io tu mu) #:use-module (crates-io))

(define-public crate-tumult-0.1 (crate (name "tumult") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "serde_xml") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "09xadhzikdis03wmvfsh869x2pln78nrh8xd1ylr6zfhxk7l2rv5") (features (quote (("nightly-testing" "clippy" "nightly") ("nightly"))))))

