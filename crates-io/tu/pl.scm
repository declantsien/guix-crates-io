(define-module (crates-io tu pl) #:use-module (crates-io))

(define-public crate-tupl-0.1 (crate (name "tupl") (vers "0.1.0") (hash "0wj3rwd1brrpmi4d3pvcb8pfp1fv22liwp29p8kkmi6xx8gi723f") (rust-version "1.65")))

(define-public crate-tupl-0.2 (crate (name "tupl") (vers "0.2.0") (hash "152gwh6w0z65lrzpys0kdgpcfvmmfxzw6jcfpi0vg7rldi407ik7") (rust-version "1.65")))

(define-public crate-tupl-0.3 (crate (name "tupl") (vers "0.3.0") (hash "0m2drq6v3ghh98a4dhwgrjq5js130dx1izrqb59l500aynpjlbi8") (rust-version "1.65")))

(define-public crate-tupl-0.3 (crate (name "tupl") (vers "0.3.1") (hash "17hhnrscxm4vs0271jqi20za4kz2x3hrvjcj00rhlxidr17lz4ch") (rust-version "1.65")))

(define-public crate-tupl-0.4 (crate (name "tupl") (vers "0.4.0") (hash "1i0lhb26zvks8f1pj58nx7a0xhay4knw1k8q2jg3bmvlca4hj0pp") (rust-version "1.65")))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.0") (hash "1lkp7qz0309al6h52fdvn6sg9wlp5i0d3sj12908mb7wifqi8i9d")))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)))) (hash "1r1ayy8h2x56xxxg75r53gr7lbvlhdvykjbn3dwxbi5a8d7hxx89") (features (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)))) (hash "0h1ayvhai9bwq56h9fv51m4n2wh5b3j9as6jb446ms6b4nbhw2zr") (features (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)))) (hash "0mnxbinb4drqb95sid7zfmhfrsqmdalla6i35frk31xhwxvkfq3h") (features (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)))) (hash "1nlyhv1ldn62m6i05391ngx06436ixdkymzgyqwgd8j0jsmil1nv") (features (quote (("impl_num" "num-traits") ("default" "impl_num")))) (yanked #t)))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.5") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)))) (hash "1avvgp18xr12470vgig3slimsxzv8nsnhj151wvdxy1h72br20yx") (features (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.6") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)))) (hash "1mha1vk2x645kcffgzsrhrflsd4pfzvy96inhn1zz68j6zj6hpyl") (features (quote (("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.1 (crate (name "tuple") (vers "0.1.7") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "0dryai5hkz7m3nzh9qdfl58m8pyy15p6d7zlgnvbygjrs5n96sp5") (features (quote (("impl_simd" "simd") ("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.2 (crate (name "tuple") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "1fkh76kdjh789vw7r6ychril9whj8y019lw672xs0ih9y1vgggys") (features (quote (("impl_simd" "simd") ("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.2 (crate (name "tuple") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "04qiaczf6hkyfzdpr4vlcd1a5g5mva4nclbkv19dpdkxl26v0p5y") (features (quote (("impl_simd" "simd") ("impl_num" "num-traits") ("default" "impl_num"))))))

(define-public crate-tuple-0.2 (crate (name "tuple") (vers "0.2.2") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "16j7xck6w0qfm1q1phpjv7jv7xszbamw8rphs45z4m3zn133glps") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.2 (crate (name "tuple") (vers "0.2.3") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "1kc0iyaglapklka6sqf4c56sik5916p5n0d99hl4af9dxkxbbkpf") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "0xzikmbwiws8qn7d5ay2ryl7n6jc0dhp8sxsn744858vshdfb2xl") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "1n5ks368kslbqbh0l22jv414wvrdhmh6wsw8d6kgv6bncnjnpmdb") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.2") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "0xhv1bqv6bypaws3k64r9hyzl1qk9l8f7pxzq5r5d9dkqa3cymbp") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.3") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "0q4gcr98bcxn2b556qx93p9isachppjpslnwlrpxnywcq2d606za") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.4") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "102kk7gr9xm1vyqhj5wrnwk7rjpz8yz7qabypimzlmjzml9hkfxf") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.5") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "0.2.*") (optional #t) (default-features #t) (target "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (kind 0)))) (hash "1y19gy4s172vfzkl0485m2glcrcqa0i879kp3qn1qfwpfibmm2hl") (features (quote (("impl_simd" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.8") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stdsimd") (req "^0.0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0lhzf2j4lnvg07w5xidh5k1ypim1b7p3rkbsf9p6ydq4fyha8qv6") (features (quote (("impl_simd" "stdsimd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.3 (crate (name "tuple") (vers "0.3.9") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stdsimd") (req "^0.0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0j8npmrvxl31qnjwhxxcnq9igxqihnij019818nlrvkn0fa9b387") (features (quote (("nightly") ("impl_simd" "stdsimd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.4 (crate (name "tuple") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "1anyzp2s3dlcwwnhs5vk00m66gj76wh49zwr0jhfr3mrdwbjgm9y") (features (quote (("nightly") ("impl_simd" "nightly") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.4 (crate (name "tuple") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1y4smbdzvzpf71sr721ig4c0hmq38blrq347ix2ahjkz1drfa6q5") (features (quote (("nightly") ("impl_simd" "nightly" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.4 (crate (name "tuple") (vers "0.4.2") (deps (list (crate-dep (name "num-traits") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "simd") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1whpydck7jkx6kz4wzws6pwsyaskd2rbasl9xc1kafbz3fl96i01") (features (quote (("nightly") ("impl_simd" "nightly" "simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.5 (crate (name "tuple") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "packed_simd") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "15iq6laxmi7myc07w99hg01r5nlx7dj1hy3i4dmhsxdw37ja0acp") (features (quote (("nightly") ("impl_simd" "nightly" "packed_simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.5 (crate (name "tuple") (vers "0.5.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "packed_simd") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "03kxril16i1q33v2y6nn72wnd8a1q5hmzp17r5s12zh486i0p91r") (features (quote (("nightly") ("impl_simd" "nightly" "packed_simd") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde"))))))

(define-public crate-tuple-0.5 (crate (name "tuple") (vers "0.5.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zn3q7dly0d2jqpg04rxc9a37adc2zgkxvm8icaq3527ffyzdfcv") (features (quote (("std") ("nightly") ("impl_simd" "nightly") ("impl_serde" "serde") ("impl_num" "num-traits") ("default" "impl_num" "impl_serde" "std"))))))

(define-public crate-tuple-arity-0.1 (crate (name "tuple-arity") (vers "0.1.0") (hash "0vrgvvsilm36qrcyz9b8zxyawiv19jynzqgazxw9hz311jd17vvf")))

(define-public crate-tuple-arity-0.1 (crate (name "tuple-arity") (vers "0.1.1") (hash "1fqw64knyr53x3cyyi3da7lqnnnj0bl2yam37gl1cf5zjrnlz8db")))

(define-public crate-tuple-arity-0.1 (crate (name "tuple-arity") (vers "0.1.2") (hash "0n6lw3a84j1s5v901ih754d9jyz26xw7m46flfphbn0xdh5vagq1")))

(define-public crate-tuple-combinator-0.1 (crate (name "tuple-combinator") (vers "0.1.0") (hash "07k37ap1b705fcd2hx4j7rz97c9khg734csbw7nh7pimdzkjvf0w")))

(define-public crate-tuple-combinator-0.1 (crate (name "tuple-combinator") (vers "0.1.1") (hash "0b3ycnc243nm8yb17fxa1001ymcz6a280sv950vbvsrgqka3iz8s")))

(define-public crate-tuple-combinator-0.2 (crate (name "tuple-combinator") (vers "0.2.0") (hash "1w2qyx9j81drig6w3d52ynhhxqjdzn3f8ccv111amvv6gpjfw1yz")))

(define-public crate-tuple-combinator-0.2 (crate (name "tuple-combinator") (vers "0.2.1") (hash "1gbiv9zzpxg5069p48dfbvx0xa49f6f0ls0ikmcldskvikckjg8w")))

(define-public crate-tuple-conv-1 (crate (name "tuple-conv") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1lvynrv8x540n1kwn9ykagbjsrd9b84an985v0k1kphwbs7zkahw")))

(define-public crate-tuple-conv-1 (crate (name "tuple-conv") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1p8nh7xqfxwainr31bsgrdqfid5cprgscw9xpg0j80rmhd333mhz")))

(define-public crate-tuple-fn-1 (crate (name "tuple-fn") (vers "1.0.0") (hash "1q20qwd8pdn9fln8s7m24v06w7134yi81aahf4f8nn6d2knmyf9s")))

(define-public crate-tuple-fn-1 (crate (name "tuple-fn") (vers "1.1.0") (hash "13cbm69kf2ccpahr3032ssiz73pvi86si1p5iw29lvpqplsk1s89")))

(define-public crate-tuple-fn-1 (crate (name "tuple-fn") (vers "1.2.0") (hash "1fnmi9yx7526pin021p74jx62jz0q8cq52jnkqj1d6qa8jz6f26g")))

(define-public crate-tuple-iter-0.1 (crate (name "tuple-iter") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (default-features #t) (kind 0)))) (hash "1xdgsqbbwz4191hxgzbzavlgaq63md6fw2kvlcmlzlkg0kdx9aic")))

(define-public crate-tuple-map-0.0.1 (crate (name "tuple-map") (vers "0.0.1") (hash "1ppw15vk1p3wm4ia8figzv8k50n9a8h4mjlaxy3ypnkhcngb2gx3")))

(define-public crate-tuple-map-0.1 (crate (name "tuple-map") (vers "0.1.0") (hash "1rqkp7vzchas68byjkn2466lapz9smzszfacnysj1cjzx8kh42rx")))

(define-public crate-tuple-map-0.2 (crate (name "tuple-map") (vers "0.2.0") (hash "08ap1sbypw5sb0wlmlz41gwi5695mzqrlbk44svmk2zh51jsgkvm")))

(define-public crate-tuple-map-0.3 (crate (name "tuple-map") (vers "0.3.0") (hash "0phm9z4kl9askl5cxi8c0qiny31y78p2xgy4rx9vh566bi5xk3ra")))

(define-public crate-tuple-map-0.4 (crate (name "tuple-map") (vers "0.4.0") (hash "0qixmgjy86dmqfp9gra34sm1wysra0j9iymphgv7l8r1f6fr3m93")))

(define-public crate-tuple-transpose-0.1 (crate (name "tuple-transpose") (vers "0.1.0") (hash "13fif3p5q8iwds99rsr682w00jcck2ir4q0j32qz6nlp15nzlwqq")))

(define-public crate-tuple-types-0.1 (crate (name "tuple-types") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1xkimdk9gn3ab40m1m6v4c1c6kagqvxzla75sih2g42a4v1a8626")))

(define-public crate-tuple-types-0.1 (crate (name "tuple-types") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "06x205wf84a8ralqiwp7qkd4mx19vpzg9m3byny0sqmcqa76b675")))

(define-public crate-tuple-unwrap-0.1 (crate (name "tuple-unwrap") (vers "0.1.0") (hash "11nmkwm6kny5bzp4fqygsdlpzdq983xficgxpdc567wi42ai76cb")))

(define-public crate-tuple-unwrap-0.2 (crate (name "tuple-unwrap") (vers "0.2.0") (hash "0fhvapl6cx88i8p8cxk0d6m5z9k1053svy5gagnxfdnm3ngcp70r")))

(define-public crate-tuple_db-0.1 (crate (name "tuple_db") (vers "0.1.0") (hash "0rarg3rclr7rsi62sqd4mw533nn0cqqxcgri78pwza80ch8qbwqp")))

(define-public crate-tuple_join-0.1 (crate (name "tuple_join") (vers "0.1.0") (hash "0rn7cplbibykb77kdrahny9wh4cabnc38lbjxbrzw5436li54mmf")))

(define-public crate-tuple_key-0.1 (crate (name "tuple_key") (vers "0.1.0") (deps (list (crate-dep (name "buffertk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tuple_key_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kb0lyl56pkwgi0w0z7lrfxp08fapixsq5k4r6anaqdx99xhlrda")))

(define-public crate-tuple_key-0.1 (crate (name "tuple_key") (vers "0.1.1") (deps (list (crate-dep (name "buffertk") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tuple_key_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gn2qzp7xyx8vw6ddwfb2lckdi7f22y9lsvf9p0pqpv7ll0wqgml")))

(define-public crate-tuple_key-0.2 (crate (name "tuple_key") (vers "0.2.0") (deps (list (crate-dep (name "buffertk") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tuple_key_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "15jwfwmfwq7r8cdxddsifgqxdw45i76x36506qvbdy93d9mg1124")))

(define-public crate-tuple_key-0.3 (crate (name "tuple_key") (vers "0.3.0") (deps (list (crate-dep (name "buffertk") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tuple_key_derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "zerror_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "10gmcb0chy25dg6a4kfbr1gsy8mm4a2grcmql1lc5xl11h4shr2c")))

(define-public crate-tuple_key-0.4 (crate (name "tuple_key") (vers "0.4.0") (deps (list (crate-dep (name "buffertk") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tuple_key_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zerror_core") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dncfz6rr2809pshwkz0ay5hfnlcwpl3f3xbd4kd0d3vmbx2s2ns")))

(define-public crate-tuple_key-0.5 (crate (name "tuple_key") (vers "0.5.0") (deps (list (crate-dep (name "buffertk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "prototk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tuple_key_derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zerror_core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "zerror_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "0iji6z82896r2qlvlbqy7kzxsnyrz3xx35xifmh9b8i1v4ks1ljg")))

(define-public crate-tuple_key_derive-0.1 (crate (name "tuple_key_derive") (vers "0.1.0") (deps (list (crate-dep (name "buffertk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "derive_util") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "118pycayphmk8cpyc4a9whk1k8b89a5hjdql2y9izwy5p88lm7bg")))

(define-public crate-tuple_key_derive-0.1 (crate (name "tuple_key_derive") (vers "0.1.1") (deps (list (crate-dep (name "buffertk") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "derive_util") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "10qfdbwpxpbs7g9mw7l8zj0wlmjn8m2fy40d31yz2505lq9hxwp6")))

(define-public crate-tuple_key_derive-0.2 (crate (name "tuple_key_derive") (vers "0.2.0") (deps (list (crate-dep (name "buffertk") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "derive_util") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x5gd77fr00frml519fdxh0v4za1g2s0nb69rsassfa3yzlcaa0p")))

(define-public crate-tuple_key_derive-0.3 (crate (name "tuple_key_derive") (vers "0.3.0") (deps (list (crate-dep (name "buffertk") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z3zvs9fbxqf64nmffivqj9n4c4iccsvav5gvywnqqfv92cw1l71")))

(define-public crate-tuple_key_derive-0.4 (crate (name "tuple_key_derive") (vers "0.4.0") (deps (list (crate-dep (name "buffertk") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vym3n9br417ab2zhhs35394b229imca85f66slzkbg1n9jzjlyr")))

(define-public crate-tuple_key_derive-0.5 (crate (name "tuple_key_derive") (vers "0.5.0") (deps (list (crate-dep (name "buffertk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w93szgiz7nbbbgrmji5jfvcxakwvp7i9r1f8dl2zfciwsh8yy6n")))

(define-public crate-tuple_len-0.1 (crate (name "tuple_len") (vers "0.1.0") (hash "1knlz1lrb9py6hbbay58qvxj8sdh1hak7ryn3l7lwryhwj21mhvv")))

(define-public crate-tuple_len-0.1 (crate (name "tuple_len") (vers "0.1.1") (hash "17g1iw2w56xd6ka50zcl23l0mqnzgf2j47gzbqy69vv5cvqdizwm")))

(define-public crate-tuple_len-1 (crate (name "tuple_len") (vers "1.0.0") (hash "0aglj0fyj03mgiszf1b1ymjz47a9i1z3ksbbzxw8ldksddqfrxbi")))

(define-public crate-tuple_len-1 (crate (name "tuple_len") (vers "1.1.0") (hash "1fy2bb8ln45sph2ymh47g6gc66i2jl8l5r8x9df8zvzfk5hik4i7")))

(define-public crate-tuple_len-2 (crate (name "tuple_len") (vers "2.0.0") (hash "14c6afc74xgpxz0rz142qjdk4hyfylg4y3n93p0wxqpz7znp3q48")))

(define-public crate-tuple_len-3 (crate (name "tuple_len") (vers "3.0.0") (hash "16ijkawaylqnwmgblz61irha8hgbjm12g8vhgbslcx95zcb4drzi")))

(define-public crate-tuple_length-0.1 (crate (name "tuple_length") (vers "0.1.1") (deps (list (crate-dep (name "tuple_macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0zn693fqaapr7vn2qpzpanv93r2vpzkspcd6ddj567xq0fz6hd9a") (features (quote (("64" "tuple_macro/64") ("32" "tuple_macro/32") ("16" "tuple_macro/16"))))))

(define-public crate-tuple_length-0.1 (crate (name "tuple_length") (vers "0.1.2") (deps (list (crate-dep (name "tuple_macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1mvjmc07iv26c25hjz7ffxv1nhw4nflqpwgpgsghmsgyp10d5zgk") (features (quote (("64" "tuple_macro/64") ("32" "tuple_macro/32") ("16" "tuple_macro/16"))))))

(define-public crate-tuple_length-0.2 (crate (name "tuple_length") (vers "0.2.0") (deps (list (crate-dep (name "tuple_macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1cxxlv173sdh0kkhvr5ijqnpx54hzrh49xv2sj83pcb11p80ppzg") (features (quote (("tup_len_64" "tuple_macro/tup_len_64") ("tup_len_32" "tuple_macro/tup_len_32") ("tup_len_16" "tuple_macro/tup_len_16"))))))

(define-public crate-tuple_list-0.1 (crate (name "tuple_list") (vers "0.1.0") (hash "0f5r6gsnabkrggxk34qi7wgddb9k0w9aivc4bnkppsf1gli7yfdx")))

(define-public crate-tuple_list-0.1 (crate (name "tuple_list") (vers "0.1.1") (hash "0yhn66d7nny1zaz7zp114rapng8c3y3rmx2ywxd40d0wfrqiyhjb")))

(define-public crate-tuple_list-0.1 (crate (name "tuple_list") (vers "0.1.2") (hash "0qr7qlp673mpiww8yp57xk0i9i5iykkfv92a4lsqbnqqxlxh2sx2")))

(define-public crate-tuple_list-0.1 (crate (name "tuple_list") (vers "0.1.3") (hash "0bjh3n26idlifl7i884531hn154fxylxbr6nsxbdk1p53vvvj7ql") (features (quote (("std"))))))

(define-public crate-tuple_macro-0.1 (crate (name "tuple_macro") (vers "0.1.1") (hash "06hp2xj5ccskwwv0d1jk89hpyy2ysch04r8dm4ggf1lfmpfiwdvs") (features (quote (("64") ("32") ("16"))))))

(define-public crate-tuple_macro-0.2 (crate (name "tuple_macro") (vers "0.2.0") (hash "0lv45719fi6av5y1iral0bzvsi9q6y6kmfwszxqvx4l96cjxgzyb") (features (quote (("tup_len_64") ("tup_len_32") ("tup_len_16"))))))

(define-public crate-tuple_macro-0.2 (crate (name "tuple_macro") (vers "0.2.1") (hash "06jsmrdzi34lh651h78dgnxnnsnir2wrx4gvy99yv4c9hxi75v7n") (features (quote (("tup_len_64") ("tup_len_32") ("tup_len_16") ("from_tup_64") ("from_tup_32") ("from_tup_16"))))))

(define-public crate-tuple_macro-0.2 (crate (name "tuple_macro") (vers "0.2.2") (hash "131rbrl4zr20qbr06hj97rw5r4z79gb2bi2lm5cj8f782ifaxin1") (features (quote (("tup_len_64") ("tup_len_32") ("tup_len_16") ("from_tup_8") ("from_tup_64") ("from_tup_32") ("from_tup_16"))))))

(define-public crate-tuple_split-0.1 (crate (name "tuple_split") (vers "0.1.0") (deps (list (crate-dep (name "blk_count_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tupleops") (req "^0.1.1") (features (quote ("concat"))) (default-features #t) (kind 0)))) (hash "01473x68f3ygg58ngsaknskjwv25bws4p2npqqksn5wp1n79znkl")))

(define-public crate-tuple_split-0.1 (crate (name "tuple_split") (vers "0.1.1") (deps (list (crate-dep (name "blk_count_macro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tupleops") (req "^0.1.1") (features (quote ("concat"))) (default-features #t) (kind 0)))) (hash "1jg2yar1zlxv37gls0y5vj8kmgx21qpz7gvmc3w0wvc2n7jyc9cx")))

(define-public crate-tuple_storage-0.1 (crate (name "tuple_storage") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1p8bbg224i4qc0c2mqhnyy873kr4y5ds1kcv7ndkirz513ss9s8r")))

(define-public crate-tuple_storage-0.1 (crate (name "tuple_storage") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "1f962pjwvg4rkvg73fgr7ring1hsx6lgdq1x4d9kywh8xc2sb3kd")))

(define-public crate-tuple_swizzle-1 (crate (name "tuple_swizzle") (vers "1.0.0") (hash "1a464ds025cbkj0qlar1ny0hsa21ryxllspb8977bhfi769ff8xf")))

(define-public crate-tuple_swizzle-1 (crate (name "tuple_swizzle") (vers "1.0.1") (hash "1wcbhclf9dip6wgb0yxdd1y9jihla8lfzrxfpsqqgsh8xjgvlzl9")))

(define-public crate-tuple_tricks-0.1 (crate (name "tuple_tricks") (vers "0.1.0") (deps (list (crate-dep (name "make_tuple_traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pdl85b452r3pnz7r84gp9k98yxbz79qv5q071m4wzjabj5a05az")))

(define-public crate-tuple_tricks-0.2 (crate (name "tuple_tricks") (vers "0.2.0") (deps (list (crate-dep (name "make_tuple_traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1jrx6hylvgj5qg7hzic7w8w4h1h0si36c2mfs6j5ijbl24ig640c")))

(define-public crate-tuple_tricks-0.2 (crate (name "tuple_tricks") (vers "0.2.1") (deps (list (crate-dep (name "make_tuple_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "03ic6l4xbxvfkw9cp9h1c9z69qh7bajy8riv07zdr3n22p3by0w0")))

(define-public crate-tuple_unpack-1 (crate (name "tuple_unpack") (vers "1.0.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1z3w8cn5xh3zjrjmh4akbi3y183d5x1qf45f5sg8469nbrb7wcck")))

(define-public crate-tuple_unpack-1 (crate (name "tuple_unpack") (vers "1.0.1") (deps (list (crate-dep (name "seq-macro") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1p00bsd071yj4nvz1vnp27q1dq1xxva80bw7ahkskflkhri1igg0")))

(define-public crate-tuple_utils-0.1 (crate (name "tuple_utils") (vers "0.1.0") (hash "16gdgnbpk6ldylnbc0ksxbscnykiqzjqwmd5ysflb6xqxglci6h6")))

(define-public crate-tuple_utils-0.2 (crate (name "tuple_utils") (vers "0.2.0") (hash "0zhfnby28530hks9203wfs4aambsczr6mi1i7dmyk8zhp1xwvznb")))

(define-public crate-tuple_utils-0.3 (crate (name "tuple_utils") (vers "0.3.0") (hash "1wgl7a32a9gvcxiyxznmglc9kc4d2j7c4dfzpr3nzcf5w8c490s4")))

(define-public crate-tuple_utils-0.4 (crate (name "tuple_utils") (vers "0.4.0") (hash "1sch8brl6j1xlr7r6mkql4vzm8mafqj1b5w7h8qcswzgja9szyng")))

(define-public crate-tuple_zip-0.1 (crate (name "tuple_zip") (vers "0.1.0") (hash "0557d5xgfzkzw2m5k6irzld07xrx5qbd5w90hk7137y0d9y08cz1")))

(define-public crate-tupleops-0.1 (crate (name "tupleops") (vers "0.1.0") (deps (list (crate-dep (name "same-types") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "tupleops-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "061maaar6gnzff1agmxd45v026av6mkqlw73q9jfnd29591xl2mx") (features (quote (("unprepend") ("unappend") ("tuple") ("ref-mut") ("ref") ("prepend") ("option") ("max-len" "256") ("map" "feature-generic_associated_types") ("length") ("into") ("gat-ops" "map") ("full" "all-ops" "max-len") ("feature-generic_associated_types") ("feature-const_fn_trait_bound") ("dont_hurt_yourself_by_using_all_features") ("default-ops" "all-ok" "all-some" "append" "apply" "apply-mut" "concat" "concat-many" "length" "option" "prepend" "ref" "ref-mut" "tuple" "unappend" "unprepend") ("default-len" "16") ("default" "default-ops" "default-len") ("concat-many" "concat") ("concat" "prepend") ("apply-mut") ("apply") ("append") ("all-some") ("all-ops" "default-ops" "gat-ops") ("all-ok") ("96" "64") ("8") ("64" "32") ("32" "16") ("256" "224") ("224" "192") ("192" "160") ("160" "128") ("16" "8") ("128" "96"))))))

(define-public crate-tupleops-0.1 (crate (name "tupleops") (vers "0.1.1") (deps (list (crate-dep (name "same-types") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "tupleops-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mhic6jc57nw82raq5lkafww745c99yrhjdwmqwmqxqgzrw00acy") (features (quote (("unprepend") ("unappend") ("tuple") ("ref-mut") ("ref") ("prepend") ("option") ("max-len" "256") ("map" "feature-generic_associated_types") ("length") ("into") ("gat-ops" "map") ("full" "all-ops" "max-len") ("feature-generic_associated_types") ("feature-const_fn_trait_bound") ("dont_hurt_yourself_by_using_all_features") ("default-ops" "all-ok" "all-some" "append" "apply" "apply-mut" "concat" "concat-many" "length" "option" "prepend" "ref" "ref-mut" "tuple" "unappend" "unprepend") ("default-len" "16") ("default" "default-ops" "default-len") ("concat-many" "concat") ("concat" "prepend") ("apply-mut") ("apply") ("append") ("all-some") ("all-ops" "default-ops" "gat-ops") ("all-ok") ("96" "64") ("8") ("64" "32") ("32" "16") ("256" "224") ("224" "192") ("192" "160") ("160" "128") ("16" "8") ("128" "96"))))))

(define-public crate-tupleops-generator-0.1 (crate (name "tupleops-generator") (vers "0.1.0") (hash "0wb2kgb1hj45s5g7zagmf8n67l1y3jnswdnpw7bg0qw97zg20q8k")))

(define-public crate-tupleops-macros-0.1 (crate (name "tupleops-macros") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)) (crate-dep (name "tupleops-generator") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1akwf6sd9bwbf1fm5vgmg3g1kh7ll2qchc2dllj6xiz1bl10nfxf")))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 1)))) (hash "1lkwr8ci52qpkycbi0cqyypw5n80xkfvbcwyp4xv9b8ygx3z22f6") (features (quote (("tuple_map") ("tuple_iter") ("tuple_combin") ("re-exports") ("default" "tuple_combin" "tuple_iter" "tuple_map" "re-exports"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 1)))) (hash "0rrkfvh4iw57yv4j682464nm15chgdnz5xv59b4jb66rvpgw8544") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_as") ("transpose") ("shorthand") ("re-exports") ("default" "combin" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "re-exports") ("combin"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 1)))) (hash "04s617162shhwp3073y62kdma8fnwbdb9izvnniy4x446macjpp1") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_as") ("transpose") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.3.0") (hash "0fbaabpdkx5142fk8khppdjx29fwl2pya9qnf0flrpqclr1ax73c") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_as") ("transpose") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.4.0") (hash "00h88zf04zmz1dxj4gllir4rk2d2kddsnyf0h06rbyg22vfcms8c") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.4.1") (hash "09xmk25ck6ch9kwisyqf2v28qsblyqalzkawc959h0gzrhxnkv58") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.5.0") (hash "15sg71pwxl0n6dhsbi7npsgsgg5bpgzmvhiiv84k6pkr43jvir6x") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.6.0") (hash "1w63a17vy5s2drk0wx08ygk3k6y30j3vlbpk75cxr81hwfjr2rs4") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.7.0") (hash "04ma92cikp8fl5gv337fgmhin31kbmbhyqvb7ydw3rj9350wf2y7") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.8.0") (hash "000q7w3a1sbizh00zr81zw5am8zk5jqgvmpr5x0vja0v62yzb9wn") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.9.0") (hash "03ysw9f5vrpqm4ibsw1hissy148s032ab7bkwdmnsq144235wi5z") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.10.0") (hash "0iqdj4h2fzgllf3i338xmm6qsk7ylzvz4aafqq0crhwi5a55cj94") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("shorthand") ("re-exports") ("flatten") ("default" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.11.0") (hash "188lzj5pzdwasv2wxa3530k52gzfh5vax1x6w04v7sl6167lhh6r") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("shorthand") ("re-exports") ("flatten") ("default" "apply_tuple" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports") ("combin") ("cloned") ("apply_tuple"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.12.0") (hash "0ky9n62f1y2nxkrrsyprwdjjmj5aqw1cqfhnjgpxc1rjlcgr2dva") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("shorthand") ("re-exports") ("flatten") ("default" "apply_tuple" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports" "capt") ("combin") ("cloned") ("capt") ("apply_tuple"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.13.0") (hash "07ppj80pza7i76mdk5hml5qingyxrhyazwjwlh0ka6xw5brllfr4") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_get") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("shorthand") ("re-exports") ("flatten") ("default" "apply_tuple" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports" "capt" "tuple_get") ("combin") ("cloned") ("capt") ("apply_tuple"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.14.0") (hash "1id3bxfwn5d5j0sjd9iiw1wambvrqvmx99ybmn41iadn62g403v1") (features (quote (("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_get") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("sort" "tuple_meta" "tuple_get") ("shorthand") ("re-exports") ("flatten") ("default" "apply_tuple" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports" "capt" "tuple_get" "sort") ("combin") ("cloned") ("capt") ("apply_tuple"))))))

(define-public crate-tuples-1 (crate (name "tuples") (vers "1.15.0") (hash "0i3b1y0y1abs9bd7j3v8amgrg3dzl4mwzwi6cmilr0f710hrg215") (features (quote (("tuple_swap_n") ("tuple_meta") ("tuple_map") ("tuple_iter") ("tuple_get") ("tuple_call") ("tuple_as") ("transpose") ("split_to_tuple_by") ("split_to_tuple_at") ("split_parts") ("split_by") ("split_at") ("split" "split_parts" "split_by" "split_to_tuple_by" "split_at" "split_to_tuple_at") ("sort" "tuple_meta" "tuple_get") ("shorthand") ("re-exports") ("flatten") ("default" "apply_tuple" "combin" "split" "tuple_iter" "tuple_map" "transpose" "tuple_as" "tuple_meta" "shorthand" "flatten" "cloned" "tuple_call" "re-exports" "capt" "tuple_get" "sort") ("combin") ("cloned") ("capt") ("apply_tuple") ("all" "default" "tuple_swap_n"))))))

(define-public crate-tuplestructops-0.1 (crate (name "tuplestructops") (vers "0.1.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jpha4n9pdz7sqhb74as8j52dag8k64ikgmbl79ycl3nh9barw4a") (features (quote (("tuple_32") ("tuple_28") ("tuple_24") ("tuple_20"))))))

(define-public crate-tuplestructops-0.1 (crate (name "tuplestructops") (vers "0.1.1") (deps (list (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hz2a8m1kjkz257g3g58vrkwggm4d8gdk1sp4izkbzgyc3gvv77d") (features (quote (("tuple_32") ("tuple_28") ("tuple_24") ("tuple_20"))))))

(define-public crate-tuplestructops-0.1 (crate (name "tuplestructops") (vers "0.1.2") (deps (list (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "070n8l4k9yca59pj1bpx2j0r21zwgk4ryv6f7qxhxm8iaq3h2sns") (features (quote (("tuple_32") ("tuple_28") ("tuple_24") ("tuple_20"))))))

(define-public crate-tuplestructops-0.2 (crate (name "tuplestructops") (vers "0.2.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "1h55n9bj0zpkyd5lfkj4ka4m64cjq43ddpvbpdwv0inp4wf4zgi7") (features (quote (("tuple_32") ("tuple_24") ("impl_docs"))))))

(define-public crate-tuplestructops-0.3 (crate (name "tuplestructops") (vers "0.3.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "04kr11hv8fhlg4n1sm38y4jz3cqc6sly1m5gn6f860dfrmhc1nba") (features (quote (("tuple_32") ("tuple_24") ("impl_docs"))))))

(define-public crate-tupletools-0.1 (crate (name "tupletools") (vers "0.1.0") (hash "178dxaxv14hqkdpj16a51izk803ly7gx7ragz9vk1p507zn336mp") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.1 (crate (name "tupletools") (vers "0.1.1") (hash "1hy65b9mkb00ma5c2aw30br8vjqysaxy9v6zdhwh19xclj1p15jh") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.2 (crate (name "tupletools") (vers "0.2.1") (hash "11mm63vwg527zpnvs0hg1p5gbal7vhqq5b92ggdd70f40bvax8jq") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.2 (crate (name "tupletools") (vers "0.2.2") (hash "1gi84m1ip077lis528msznddsw9gdb4dv4kcv6r7wllxhl2rw530") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-tupletools-0.2 (crate (name "tupletools") (vers "0.2.3") (hash "1nqky8lgmr1cpcf8xxvx185bafnc0w4mb2hhn5rmnnfr0g1c865y") (features (quote (("no_std"))))))

(define-public crate-tupletools-0.2 (crate (name "tupletools") (vers "0.2.4") (hash "04dmlzn8cyz4vzpqi07crd8y83xp5c4jp3ckj50zsa3p3k7zndm4") (features (quote (("no_std"))))))

(define-public crate-tupletools-0.2 (crate (name "tupletools") (vers "0.2.5") (hash "07z3q0qm0ygdxjvznabd0dpkpif92k2sjzk0wy795igj81iakz5m") (features (quote (("no_std"))))))

(define-public crate-tupletools-0.3 (crate (name "tupletools") (vers "0.3.0") (hash "1fzf9x97x5i4sa4jd0gw32q3nyp5r4sqvfar22wrb30smvk4pnh3") (features (quote (("no_std"))))))

(define-public crate-tupletools-0.4 (crate (name "tupletools") (vers "0.4.0") (hash "05gdh34az4ickpc6iafh84d003vqpskqrkvmr3408dkh6r6jhfb9") (features (quote (("no_std"))))))

(define-public crate-tupletools-0.4 (crate (name "tupletools") (vers "0.4.1") (hash "0yc2m5pj2achwxvnwph687lxgm3ipij6iywkd8gmx8psnzas1k2c") (features (quote (("no_std"))))))

(define-public crate-tuplex-0.0.1 (crate (name "tuplex") (vers "0.0.1") (hash "002vm1g36hpr8asdq1ip7n9mj73ijvzxvf27bgw17d2jb988iy7n")))

(define-public crate-tuplex-0.1 (crate (name "tuplex") (vers "0.1.0") (hash "08982g9572ck1l8rnyma3cl8ph2570di4wpwr9qndgad9qa7p912")))

(define-public crate-tuplex-0.1 (crate (name "tuplex") (vers "0.1.1") (hash "0hgsnsj32y36z94n3mvxxc05j4lmr84h02g15lxrc9mgyv9vbmqh")))

(define-public crate-tuplex-0.1 (crate (name "tuplex") (vers "0.1.2") (hash "1ah85a6id43v9ahnrf24yy839pkfcal4zlsmg7rxri2lahfwhsk7") (features (quote (("std") ("default" "std"))))))

(define-public crate-tupley-0.0.1 (crate (name "tupley") (vers "0.0.1") (hash "066hikj2s4xzdafw3v8d2i63hrhmj2da04kl01lx9qsb0g620b50") (yanked #t)))

(define-public crate-tupley-0.0.11 (crate (name "tupley") (vers "0.0.11") (hash "0v5fkr7draqjm4iadalwy9d9p2xjmhkrng5dccgc67a47v1sxkvg") (yanked #t)))

(define-public crate-tupley-0.0.12 (crate (name "tupley") (vers "0.0.12") (hash "098fs4is2b19dqspdzar3svrx07w9xhawwwjakkrp3pn54xghbcc") (yanked #t)))

(define-public crate-tupley-0.0.13 (crate (name "tupley") (vers "0.0.13") (hash "14hx1hnviq2xiz08780x2zih6fnm37kgk6x6bw0hjsxpznvjpyps") (yanked #t)))

(define-public crate-tupley-0.1 (crate (name "tupley") (vers "0.1.0") (hash "1xcsqwmg2807izhnxqngllcp05pbcar9r56l9ggxqi8q43fgcsj1") (features (quote (("len-generic") ("full" "len-generic") ("default"))))))

(define-public crate-tupley-0.1 (crate (name "tupley") (vers "0.1.1") (hash "0b3viwshc33xs320cipfnhsbk01asiihavnxkyrylhb030lmjhwg") (features (quote (("len-generic") ("full" "len-generic") ("default"))))))

(define-public crate-tuplez-0.1 (crate (name "tuplez") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "167n15ssz1xc0asr60jpcnfdn4645iga3f29lr6p6ph8xb0q39rd") (features (quote (("default") ("any_array")))) (yanked #t)))

(define-public crate-tuplez-0.1 (crate (name "tuplez") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1mnhvhc3m5mjab775mk288lc7gl9ych8x6pig730nh4xgbyfsvag") (features (quote (("default") ("any_array"))))))

(define-public crate-tuplez-0.1 (crate (name "tuplez") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "07a4mj8ax13pd16ljslqdcgzwcnav3qdygifm4z87w3q4sw35bm0") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.1 (crate (name "tuplez") (vers "0.1.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0vrkph1bdiqwqjj6fn96b09h1ynwcc3m3qfwgp38z0a05gk47hv8") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.2 (crate (name "tuplez") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0dd9zx68zf7qkc2w9wifwz7kxl5p5bky0rzryi8l5s2dz36bw123") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.2 (crate (name "tuplez") (vers "0.2.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "07h6rv20sp0i7v3nphjrcgj6xbkw2xl5q85dm0hzmxsqjz6h65gg") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.3 (crate (name "tuplez") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vkrnqbfz4k7dparcw8c4pkfjirggqchra4h6s9n6n5kjcgbj1wb") (features (quote (("unwrap") ("default" "unwrap") ("any_array")))) (yanked #t)))

(define-public crate-tuplez-0.3 (crate (name "tuplez") (vers "0.3.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "055y8m1mvmzfhhg3cm604k5lrlrpir2abacn7znhfd1rx6k3fdcl") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.3 (crate (name "tuplez") (vers "0.3.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0kx9775yiryh9973263vxh33q6g2hvrr486kndqibwyqvgyndjnj") (features (quote (("unwrap") ("default" "unwrap") ("any_array")))) (yanked #t)))

(define-public crate-tuplez-0.3 (crate (name "tuplez") (vers "0.3.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02vxg1cl74icpqv7n1n8hg0778w5b1l8kmn4cwnwkrd7aql4bl3v") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.3 (crate (name "tuplez") (vers "0.3.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "142210qn79s1rv077p22lpb34f2983iz8iqclk64wa9p421929mp") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4 (crate (name "tuplez") (vers "0.4.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0h5m4hi91n6as4bzqi4fkpxqzbznnaldvv0hjb7fn3010jn90f2c") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4 (crate (name "tuplez") (vers "0.4.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "16sy7q9c4ffxsgykjg9v6c8zmv4620x4jmijzynqvnry5cvfa67x") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4 (crate (name "tuplez") (vers "0.4.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1rlw180k2h847nxwzi2l9g7byrm1j1yriq6lmf6i4yhx47vbz3rh") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.4 (crate (name "tuplez") (vers "0.4.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1a3il2kipd82q179c1dn72n4frjdamb0jxzdzhhln4k5qxhivdb5") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.5 (crate (name "tuplez") (vers "0.5.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1z74icpn5grr7mdsx6wmqyallqs7ph44hkd9h8vd60wcyphmqxrq") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.5 (crate (name "tuplez") (vers "0.5.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1lhsf73yn3x68ffyfrnbbqyzwx96mds6pk96p0l4jdpfsnk5rp4w") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.5 (crate (name "tuplez") (vers "0.5.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "03qglifm58x6ax421h5kpmwlyzy6iw2b9f9lgh84rfgr0r2ms9nr") (features (quote (("unwrap") ("default" "unwrap") ("any_array")))) (yanked #t)))

(define-public crate-tuplez-0.5 (crate (name "tuplez") (vers "0.5.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0kkx426a6gyyx6mhl8phpyq4na5h8gd972bl3lnssc0m63xx23fn") (features (quote (("unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6 (crate (name "tuplez") (vers "0.6.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1v0armd5l3r4la30s81x3llwfqa0ss5hwnc25qcw8h89cpgm2k4k") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6 (crate (name "tuplez") (vers "0.6.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0k7hph0i4m31dnrilk7rvb7sslk7a6s38ksk0qshzvdphb8swy1g") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6 (crate (name "tuplez") (vers "0.6.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1s20frnmcpz501hlwn00f8ly4nrap13528w32hx3f77d9frhvjyg") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.6 (crate (name "tuplez") (vers "0.6.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "11cxrn8lzbkk2k6nai368hkk9srs8wghmrgni1n7r7k7xfvvhvc2") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.7 (crate (name "tuplez") (vers "0.7.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0q69bjfx8bk92yqzdzdcms1k39cfbkmsda2n4j5yfmdhyg263j3s") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array")))) (yanked #t)))

(define-public crate-tuplez-0.7 (crate (name "tuplez") (vers "0.7.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0xfn18i6y9i1mkpgfjpd8xg9hl19q9h310a3qk2zqb0jm5709myz") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.8 (crate (name "tuplez") (vers "0.8.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "02s5bcfks0bz7mkd1ky62qal4031a69xblkqz6715faxrw58mxd4") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.8 (crate (name "tuplez") (vers "0.8.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0w1qpfgmwgx3z34szv3gmq2p3ql0kw13lqg7rr3id2vz8g38zpp7") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.8 (crate (name "tuplez") (vers "0.8.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0waq3hkv1gxznw8vspj0db5kb8bmabyn047rq6w2mdzv4fa5mamb") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.9 (crate (name "tuplez") (vers "0.9.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0jwbyydfsgmmchkqfd1bny857as2mrmyikgi64dv505g482g4fxj") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.9 (crate (name "tuplez") (vers "0.9.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1gh2w7bvny5kbc1sqqbljs5gzfbaxxm3z9w260nf479db49y9gbj") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.9 (crate (name "tuplez") (vers "0.9.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1pacjq11xgim0jg8g0b3sfxp5y0nszwz8sx9x9z66slv4vkm0f0f") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.10 (crate (name "tuplez") (vers "0.10.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0hra5wm103w63m4b259zk74d5lbjnwvzivqyvhnils8flvn6yrr5") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.10 (crate (name "tuplez") (vers "0.10.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1jykfp6kbfjq3fgjym7m6jib4r36lfsdishamw3m7w9nkcbz5syg") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.10 (crate (name "tuplez") (vers "0.10.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "176w6virbbqwr654hqa7pddqd3ih3756yz263hjr5zwyx479fyba") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.11 (crate (name "tuplez") (vers "0.11.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0ak317pg6aiy0byfpcjya6h02q7raz21iscif67pfnqv0px6q51k") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.11 (crate (name "tuplez") (vers "0.11.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0vs7a4w64b6hbm29hqd77di8m98vis7k0aq4ri7d917gr3bwphn9") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.11 (crate (name "tuplez") (vers "0.11.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0462divl9wlnfsj3g93ssvb24kzi786blc2xcrqj5mdfi467c7d2") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.12 (crate (name "tuplez") (vers "0.12.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "15k5gzb3c8sgv40gfaqqd5fz2ggir1g536b2dfy5dp00jbhs2313") (features (quote (("unwrap") ("full" "any_array" "serde" "unwrap") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.13 (crate (name "tuplez") (vers "0.13.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1shha975khm18ckjc3xy28miyiaqv1lfgcky20xd9sdnmh2s277a") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array")))) (yanked #t)))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0ykrbzcvqlc0pyhj63mwp2iskv28fv2li466jfinwr9xirbi307x") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1f8dbqnrmrgvp262kxirzp368wwbfjbyhb2ajliybczi5gr7br49") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1kk48jyiq3qnmn7w4hqxn1q08y7nbm7axsvirsgzhzy0n3qxcgra") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "00s79qziqvm7whcfc55xjbsbhvbv7cgqzzhqgniaz4k2f3lq7nqa") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0srrll671vdhjp3dxs2g89h76h3pcyjdpcxgjplw5yg5dkrm5pcv") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.5") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1sszr5njxjcri9l8lijaj4z8jvzi3zc34la8q47k01lrvy8m74yy") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.6") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0mfyxp7ln38df2x36mcv53702mvncmdv5xj00k9k828slfpylyw9") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.7") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "03jpvs74a03pgdgm2iw665v9b5abnj6rjxv608g0mjc5kppyzwsq") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.8") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1rmxq764gv5fwp3vywrscw0qgrqmmxczlnkw1dv4c8razvvjbb8k") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array")))) (yanked #t)))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.9") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "12wpxzyvk55abj15rrccvcrni8n03hh96r34f33idav2hp30268v") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.10") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0k7qpgxw727mgbgj4ggb1ykigjvi63y97dd3ay9ign20dn6w7ayk") (features (quote (("unwrap") ("uninit") ("full-nightly" "full" "any_array") ("full" "serde" "unwrap" "uninit") ("default" "unwrap") ("any_array"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.11") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1h2nx32jc5y8l0hxy11hw94z75lbjjfapxzg6l1rbsdmsi3r5qbx") (features (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (v 2) (features2 (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.12") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "03774by2kyhjqmnxhnn7pjzdjyqh5zyaamnpikavglrb2zd1lxvd") (features (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (v 2) (features2 (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.13") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "08c81xnripfgk9vx2xqdgdjix2g9ybk4r3854ww9kq6l8cmzv9g5") (features (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (yanked #t) (v 2) (features2 (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.14-alpha") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0v2ca2zf11pw6qfck37g0llipys6bzyvnkv15013sfgmgi3dnjvp") (features (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (v 2) (features2 (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-0.14 (crate (name "tuplez") (vers "0.14.14") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "tuplez-macros") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "142slwfx9iijmjpzhm9i2s1sc59ja0kjzxxrf9ic1rq7vrprrjjp") (features (quote (("unwrap") ("uninit") ("full-no-std" "serde" "uninit" "unwrap") ("full-nightly" "full" "any_array") ("full" "std" "full-no-std") ("default" "std" "unwrap") ("any_array")))) (v 2) (features2 (quote (("std" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-tuplez-macros-0.1 (crate (name "tuplez-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cgxqhpra4sjz2580n8li70yrvn5ddhim5kvjs8nbfblv65bm11h") (yanked #t)))

(define-public crate-tuplez-macros-0.1 (crate (name "tuplez-macros") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "188yxspll9m15aiqhxww0kzjlrf7h47jb5f36m7j3zkhaid02m4s")))

(define-public crate-tuplez-macros-0.1 (crate (name "tuplez-macros") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03fpdx47jjsfv7dsi9pp1y1my6z8s65a75bdbirhk04ybj604bq7")))

(define-public crate-tuplez-macros-0.1 (crate (name "tuplez-macros") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04d3ldacy6pv4aynbkn47297kpl57qzn5vysznzkhv2birr9hg5w")))

(define-public crate-tuplez-macros-0.2 (crate (name "tuplez-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rxym9k4nprhwrzch3sm3mq6vbir58zp995ydya3bm3rk6szirq9")))

(define-public crate-tuplez-macros-0.3 (crate (name "tuplez-macros") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ah3c8c9brrdxbmhqsy51a9c3mq922rc62xavmkgz2c9g2339mrb")))

(define-public crate-tuplez-macros-0.3 (crate (name "tuplez-macros") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kgy7vj60mj8zix41rx70gxq1djrpnj13cz2xjnq833mdxnjiri6")))

(define-public crate-tuplez-macros-0.3 (crate (name "tuplez-macros") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i15axhbn5h74wqvz47gj131al4mgf8ccsg48csx8727gpqd3ybj")))

(define-public crate-tuplez-macros-0.3 (crate (name "tuplez-macros") (vers "0.3.3") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18mawfn7pk098wvyqx3c8f7rhj2g3vp39xnn6fqgrrlazf78nkwc")))

(define-public crate-tuplez-macros-0.3 (crate (name "tuplez-macros") (vers "0.3.4") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rqdiv1x0m0gpa7nxkf1ayd5amjh0dyfyjy0gmnjqc7mkiq9igf3")))

(define-public crate-tuplez-macros-0.3 (crate (name "tuplez-macros") (vers "0.3.5") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zd2hfcwz0v4m0zr34sqxfgakfiq05n4dn8ypmcah6fbr7hxsla7")))

(define-public crate-tuplez-macros-0.4 (crate (name "tuplez-macros") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12f7q23z3jac65lqfwn91h9rhsqiapmfkwb2q8g9f80h1vldq3xf")))

(define-public crate-tuplez-macros-0.5 (crate (name "tuplez-macros") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yzrdb95i2ik63xamaqq221mdcqci9cwnm3wrsnfdj0xf169n1rg")))

(define-public crate-tuplez-macros-0.5 (crate (name "tuplez-macros") (vers "0.5.1") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0d5wnxk67f1mzv9y4b07nv13a31rzs3bshynq0dkyi09v3fvf7ww")))

(define-public crate-tuplez-macros-0.5 (crate (name "tuplez-macros") (vers "0.5.2") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "083122hzn2a3jihfwjqmcsavq6hfianf7xlj8hjj5d5h3rc34wgq")))

(define-public crate-tuplez-macros-0.6 (crate (name "tuplez-macros") (vers "0.6.0") (deps (list (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dhimj1rg0cghznxd2g3gd9p6fclqwl5zy5rjk2r7jxbv1rl4k45")))

(define-public crate-tuplez-macros-0.6 (crate (name "tuplez-macros") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cfvz740hwam0fv2p8a5v0hhrzdg9y5zsvimwynpacc9acjhns3x")))

(define-public crate-tuplez-macros-0.7 (crate (name "tuplez-macros") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17hign831gmp161h2p69q9c5qjcfswnsxch6daq2mj3cnjcac29a")))

(define-public crate-tuplify-1 (crate (name "tuplify") (vers "1.0.0") (hash "157z7g4gfm3pcp507bl72i7ijhx1vj6s6ws5nnzaxpm6ymvhjvm2")))

(define-public crate-tuplify-1 (crate (name "tuplify") (vers "1.0.1") (hash "0m5b5qzah5561fm8swacvpcaypmlalfwq58as64n128kcphcbi2c")))

(define-public crate-tuplify-1 (crate (name "tuplify") (vers "1.0.2") (hash "0pxvam5l391iwgra18n73if3a7hxsla6nhm6bpklmvk79bkidiah")))

(define-public crate-tuplify-1 (crate (name "tuplify") (vers "1.0.3") (hash "1dgmpw09nrbif3x37zhdj9h0awmlk9p8pzx2byqrlqw4i85awvx7")))

(define-public crate-tuplify-1 (crate (name "tuplify") (vers "1.1.3") (hash "1rkmxqdwfyp5im5wb4cqbk1lp95qcizlyfvhl3xa11yxfrrj0j6b")))

(define-public crate-tuplify-1 (crate (name "tuplify") (vers "1.1.4") (hash "11s0phsh25j6xlk7yh05467gcbsmb7nb64fnfddawg2wwn5v3jhk")))

