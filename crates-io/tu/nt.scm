(define-module (crates-io tu nt) #:use-module (crates-io))

(define-public crate-tuntap-0.0.1 (crate (name "tuntap") (vers "0.0.1") (hash "031nx8ssi3fs155jibj61r1y2fq353ayvs0ckgm9ffssrd1bi4p1")))

(define-public crate-tuntap-0.1 (crate (name "tuntap") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.0") (features (quote ("async-await-preview"))) (default-features #t) (kind 0)))) (hash "14qjv98qrhlrp2sk084xr8pd8xa1g8kxbfrvbyk4ls9w038n4yfa")))

(define-public crate-tuntap-0.2 (crate (name "tuntap") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.0") (features (quote ("async-await-preview"))) (default-features #t) (kind 0)))) (hash "1lbs889d81ixmwklyij1l6nf091gahydsf2wgjlls1m5ar3brwc0")))

