(define-module (crates-io tu it) #:use-module (crates-io))

(define-public crate-tuit-0.0.0 (crate (name "tuit") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0) (package "thiserror-core")))) (hash "0kr7bc04n5qcw0x810qnk55m5lmg86vnbc5fp99izf0v4kgycci4") (features (quote (("widgets") ("std" "alloc") ("default" "widgets") ("ansi_terminal" "std") ("alloc"))))))

(define-public crate-tuitactoe-0.1 (crate (name "tuitactoe") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.15") (features (quote ("crossterm"))) (kind 0)))) (hash "1m31z6kc6j9n55ikzp8d09l3881gamkzljdywfp738p7lphcmlv9")))

(define-public crate-tuitactoe-0.1 (crate (name "tuitactoe") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.15") (features (quote ("crossterm"))) (kind 0)))) (hash "1fh1877dkcd1ngcfdjqkx4z0m2bvmjs8s218sd0b5wzlpw0qcj94")))

(define-public crate-tuitactoe-0.1 (crate (name "tuitactoe") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.15") (features (quote ("crossterm"))) (kind 0)))) (hash "0mxx0ql47i8xndkp4sd13rmyhd8wpy907bajqi5h91zxdlz43v15") (yanked #t)))

