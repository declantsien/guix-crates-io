(define-module (crates-io tu bu) #:use-module (crates-io))

(define-public crate-tubular-0.0.1 (crate (name "tubular") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1fmfg9lvzpqwc4xarvydhk079blnvsk714x1gzblh9gqsid86w7w")))

