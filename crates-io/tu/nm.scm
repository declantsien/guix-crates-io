(define-module (crates-io tu nm) #:use-module (crates-io))

(define-public crate-tunm-0.2 (crate (name "tunm") (vers "0.2.0") (hash "025lqpzbi6nw6h3pk69lf7i0f5qgabfji12kx8ip5hsfh06q42jn")))

(define-public crate-tunm-0.2 (crate (name "tunm") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "commander") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "mysql") (req "^22.1.0") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2.23") (kind 0)) (crate-dep (name "psocket") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.27.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.34") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "td_clua_ext") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "td_proto_rust") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "td_revent") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "td_rlua") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "td_rredis") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "td_rthreadpool") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.11.0") (kind 0)) (crate-dep (name "tunm_proto") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "websocket-simple") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1pnj07cadirv25sx0731m3m59j05cf0i99frxaack26psa16y0l6")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.9") (deps (list (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0slsx206kffcacg5g5la9ld9l4brnf1x9vd7qca7zl11l3j45sn3")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.10") (deps (list (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "19bm3kb0l94ck1wvlqf6vs38hfp71gffcwrjvv0ik0wji54xpj8z")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.11") (deps (list (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0yxzrqa8m6jslwiw1n85xldikxvmja1q3g8i7x8ihncr8q06rfyj")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.12") (deps (list (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "1f00vxxjal36b62kixyj8y2qbl63w6rh4ang2qi0l8n3rzjm7nn8")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.13") (hash "03krz9l4aa8p5j0iis8hbg6sz9ljm93hrwzn7w2pb39rgrbyzgxl")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.14") (hash "1f7xq6v1skqv547zcvag0hj4d3kg6m9azlpb8i8x0c8zz37a3jwz")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.15") (hash "033m7jyfni0nfy6wyb5y6ryixrjrz5j765ysrzjhi4777bcvqs1p")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.16") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "14zi9y450skrz6bm2w9ww949s761gqi63zi43yfqgmgfmrbg4fw1")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.17") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "17mbkkrmlv3n9d90mvrsia5yg0r68yxm8kr0kp9l9l5qb58ih05j")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.18") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "0b4w4i1n0jq8wdkdw7ajn1m2723874wfravbxcymj81y6n5k8yjz")))

(define-public crate-tunm_proto-0.1 (crate (name "tunm_proto") (vers "0.1.19") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)))) (hash "1lsn2adcaisjifr47phil7hk8dkjizl4511gzvik71b9r8w2bfmi")))

(define-public crate-tunm_timer-0.1 (crate (name "tunm_timer") (vers "0.1.0") (deps (list (crate-dep (name "rbtree") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ifg4z9ik58a2sh233m62rzd4627x52aqm54id23xpzczzk7f1aj")))

(define-public crate-tunm_timer-0.1 (crate (name "tunm_timer") (vers "0.1.1") (deps (list (crate-dep (name "rbtree") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rid32xjq151fg5brv2236si1kv5d6gj8hmbklydkr6jm2m3pnch")))

(define-public crate-tunm_timer-0.1 (crate (name "tunm_timer") (vers "0.1.2") (deps (list (crate-dep (name "rbtree") (req "^0.1") (default-features #t) (kind 0)))) (hash "1201w58prdwv1m83i0x2xx5syxfn00amrwx9398xk2qg3zrmij1w")))

(define-public crate-tunm_timer-0.1 (crate (name "tunm_timer") (vers "0.1.3") (deps (list (crate-dep (name "rbtree") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ks2hv8fw0nkh34x08a0c5iyjpa0jr04z4y54i5cdqy4rmi04my4")))

(define-public crate-tunm_timer-0.1 (crate (name "tunm_timer") (vers "0.1.4") (deps (list (crate-dep (name "rbtree") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fncm1kmb0m4kqm5acn126d49i5m7nscs76x1g11rd0hq9clkw11")))

