(define-module (crates-io tu ib) #:use-module (crates-io))

(define-public crate-tuibale-0.1 (crate (name "tuibale") (vers "0.1.0") (deps (list (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1pwy0bggx90vbszlq2xnxvlpjprl3hmsky713mzwgxflfyl40rvp")))

