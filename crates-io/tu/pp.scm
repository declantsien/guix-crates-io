(define-module (crates-io tu pp) #:use-module (crates-io))

(define-public crate-tuppari-0.1 (crate (name "tuppari") (vers "0.1.0") (hash "12cr4h0sygna2lx4xgasdiilmxqn4dpk2yn840b3fyngh9xphkix")))

(define-public crate-tuppari-0.1 (crate (name "tuppari") (vers "0.1.1") (hash "029rimxc344l6sy02yqfbmbknvgm2wv0mh9plxwjnb383pzhknhr")))

(define-public crate-tuppence-0.0.1 (crate (name "tuppence") (vers "0.0.1") (deps (list (crate-dep (name "actix-web") (req "^4.3.1") (features (quote ("rustls"))) (default-features #t) (kind 0)) (crate-dep (name "cashu") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "08yfk5sdrlqxh37dl74xg6dlwi4lx0szdfwmwdlhpkdrixacx0ln")))

(define-public crate-tupperware-0.1 (crate (name "tupperware") (vers "0.1.0") (hash "0w159dzpz2yx9i94m7c44f8vm7q35hlklfb125ggj2a6600hyl7b")))

(define-public crate-tupperware-0.2 (crate (name "tupperware") (vers "0.2.0") (hash "0k75x10vd43304fs4im499w6yhk2gps7kis9pva12bzn2s1v4hvx")))

(define-public crate-tuppipe-0.1 (crate (name "tuppipe") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0shi0zpv4rgq76jcg16pf6j9q9r08svlmqsdiq154asy69692ayq")))

(define-public crate-tuppipe-0.1 (crate (name "tuppipe") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jkscrrb4wqciacvn93v4j7p4anrrqds907dgc1p5fjkcmhzgyq7")))

(define-public crate-tuppipe-0.1 (crate (name "tuppipe") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nv6bjph2vihjg12x3jk38xyw4yshawgqanjb5sjwr7i7j3bwjl5") (features (quote (("fn-pipes") ("default" "fn-pipes"))))))

(define-public crate-tuppipe-0.1 (crate (name "tuppipe") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hrfrsmd1s07jnxvx04s7rikqj9iskm5nq5m7k5kgwb3585x0daw") (features (quote (("fn-pipes") ("default" "fn-pipes"))))))

