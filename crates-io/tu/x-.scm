(define-module (crates-io tu x-) #:use-module (crates-io))

(define-public crate-tux-i-0.1 (crate (name "tux-i") (vers "0.1.0") (hash "0vll5awj4c52ba1v24m87rjkharjd92rb0xpf7q43cwa1z0p4qgk")))

(define-public crate-tux-o-0.1 (crate (name "tux-o") (vers "0.1.0") (hash "18ysp37ijgh26ypw93lyw58cq4dl0qznbg4wkg6r56fqb63xfvaf")))

(define-public crate-tux-owned-alloc-0.2 (crate (name "tux-owned-alloc") (vers "0.2.0") (hash "07q4bvfvqa5mm7q7nk5fxab2qjiivd3wh9xzgq14l7j11gksildz")))

