(define-module (crates-io tu an) #:use-module (crates-io))

(define-public crate-tuan-0.1 (crate (name "tuan") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ymd65vsbbwb3i2vimjnxwpzq3a4hxssjr79fcvjmvlb6cb2rmkr")))

