(define-module (crates-io tu s_) #:use-module (crates-io))

(define-public crate-tus_async_client-0.1 (crate (name "tus_async_client") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "=0.10.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "=0.11.3") (default-features #t) (kind 0)))) (hash "0pjc8k8bxby8n6aln2g81a0fmpzbxsy913724ba74av3si2280m2")))

(define-public crate-tus_async_client-0.2 (crate (name "tus_async_client") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)))) (hash "1sg4s05mcccxgr4ih0ng5kapr9jggn14pafi9wi0crf0xf37iz5j")))

(define-public crate-tus_async_client-0.2 (crate (name "tus_async_client") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)))) (hash "0hiw8s2m91a5d4byjxcr7kpca612hzps4m30ljmcyqs268lbfj8p")))

(define-public crate-tus_client-0.1 (crate (name "tus_client") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0wicnlyp473hzybm7cfqfg8si506lq3ljl9m4rn6jhk9qjwnvwrc")))

(define-public crate-tus_client-0.1 (crate (name "tus_client") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1s3hzn07ngmmail8fays0kh3476pax7s0g853kxmm7rcr3lc5z6z")))

(define-public crate-tus_client_extra-0.1 (crate (name "tus_client_extra") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "02k97ssqfrq2098q73xvzps07q020y4wvb6dhk7598dn75fhkr8m")))

