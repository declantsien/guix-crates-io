(define-module (crates-io tu io) #:use-module (crates-io))

(define-public crate-tuio-rs-0.1 (crate (name "tuio-rs") (vers "0.1.0") (deps (list (crate-dep (name "dyn_partial_eq") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "ringbuffer") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rosc") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0f6wxhgficp2pyphws1sk3lc56hj2s7vqyl1bafwp93xmvqgy2ks")))

(define-public crate-tuio-rs-0.2 (crate (name "tuio-rs") (vers "0.2.0") (deps (list (crate-dep (name "dyn_partial_eq") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "ringbuffer") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rosc") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0mvai4kmz4p4bbd1r980sih7dn1dl6k4qm75k56kjp68k218bii4")))

