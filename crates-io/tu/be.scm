(define-module (crates-io tu be) #:use-module (crates-io))

(define-public crate-tube-0.0.1 (crate (name "tube") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.2.13") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.18") (features (quote ("http2" "tcp"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "06jaynypnq0j1cmjw0ivnrfyhaq1a9gq9lm5cp70xmmxpk7590cv") (features (quote (("server" "hyper/server") ("client" "hyper/client"))))))

(define-public crate-tube-0.0.2 (crate (name "tube") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3.2.13") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.18") (features (quote ("http2" "tcp"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "0ysfs8ry8f0pv7viapkpjm7drx1md05s4yvnfcrv1z9ymhxvyc7r") (features (quote (("server" "hyper/server") ("client" "hyper/client"))))))

(define-public crate-tube-get-0.0.0 (crate (name "tube-get") (vers "0.0.0") (hash "1bfa2xdivqsq4pw48czbh20b2dspvlknvl0s0h9j7d7svxlchk9d") (yanked #t)))

(define-public crate-tube-rs-0.1 (crate (name "tube-rs") (vers "0.1.0") (hash "1rijiw5gg2ilf8p2cjnjxnz3db6yb62dk778sdrjc6dq5wbkbkv8")))

(define-public crate-tubed-0.0.0 (crate (name "tubed") (vers "0.0.0") (hash "07756vvjm4zvmc1sdzj0gkqjc5hclwmj2pnr8n2ajlvkzk4spmm0") (yanked #t)))

(define-public crate-tuber-0.0.0 (crate (name "tuber") (vers "0.0.0") (hash "1ln37pfc5sfsd0h4cv996qwnxrq7zizk8chd6r9xmxwfw94bx793") (yanked #t)))

(define-public crate-tuber-engine-0.1 (crate (name "tuber-engine") (vers "0.1.0") (hash "0hg1gsv05c9r8h8qg7i6p8vij0pzcpqs871ywxpg5rxwvpgpf07f")))

(define-public crate-tubereng-0.1 (crate (name "tubereng") (vers "0.1.0") (hash "0dic95gpl2zzgddzln4qv4j0n00q88r3n6glq71fljpi4a2gzxmf") (yanked #t)))

(define-public crate-tubes-0.0.0 (crate (name "tubes") (vers "0.0.0") (hash "16pzq5lwxzakm24pcwxbxi2k1ka17q8hbspaizsxvdqcw4651w7m") (yanked #t)))

(define-public crate-tubez-0.0.1 (crate (name "tubez") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.2.13") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.18") (features (quote ("http2" "tcp"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "0lp9zs269q5brjfhipqw7hqfqqj8l001kh3bsh956h6y1g66mmp7") (features (quote (("server" "hyper/server") ("client" "hyper/client"))))))

