(define-module (crates-io tu pe) #:use-module (crates-io))

(define-public crate-tupelo_guessing_game-0.1 (crate (name "tupelo_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "06lfwfhv5wwshdidz1swia3nqqrydai2hvjz0b38kf5wifmdhs06")))

