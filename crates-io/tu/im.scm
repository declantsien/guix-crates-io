(define-module (crates-io tu im) #:use-module (crates-io))

(define-public crate-tuimager-0.1 (crate (name "tuimager") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.21") (features (quote ("crossterm" "macros"))) (kind 0) (package "ratatui")) (crate-dep (name "viuer") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0ls3kvjsb7mcw6k5qaym12a9jyyb8hmzvp9cik1qm8mywcrh3ggv")))

