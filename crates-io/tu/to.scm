(define-module (crates-io tu to) #:use-module (crates-io))

(define-public crate-tutorial-0.0.0 (crate (name "tutorial") (vers "0.0.0") (hash "0vrci2gff7axmf101ncsx6p7g5ga3blc5v83n92nncmb4mhvdnh6")))

(define-public crate-tutorials-chapter1-0.1 (crate (name "tutorials-chapter1") (vers "0.1.0") (deps (list (crate-dep (name "crypto-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "jdks") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "ksre-tui") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sdkman-cli-native") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2-rsl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1j6yhvjklsmw010pxq53af2jznwr9dy0grh0pnjmb76xc1rbyhls")))

(define-public crate-tutorials-chapter2-0.1 (crate (name "tutorials-chapter2") (vers "0.1.0") (deps (list (crate-dep (name "tar-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rsl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zzzvdas987n94hlfq4r13f8g2h7idrrlsr8dqlwfpb0z9zm9xmp")))

(define-public crate-tutorials-chapter3-0.1 (crate (name "tutorials-chapter3") (vers "0.1.0") (deps (list (crate-dep (name "crypto-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tar-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml-rsl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rsl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14h6kjm5jxhfyzdniimx94h6336n5c03hkc7r9cp1j7r2w09545l")))

