(define-module (crates-io tu lb) #:use-module (crates-io))

(define-public crate-tulb-0.11 (crate (name "tulb") (vers "0.11.21") (deps (list (crate-dep (name "bevy") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1azvmn51mnwp8b42v21jk73jcxvb9iak1iifznsr02vzk7yd8sa2") (yanked #t)))

