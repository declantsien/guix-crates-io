(define-module (crates-io eb ac) #:use-module (crates-io))

(define-public crate-ebacktrace-0.2 (crate (name "ebacktrace") (vers "0.2.0") (hash "1g6fhrb5c64yxs1rs1q1bvnil5kj76skpcg3bj0gzsl66dyk4nj1") (features (quote (("force_backtrace") ("derive_display") ("default" "force_backtrace" "derive_display"))))))

(define-public crate-ebacktrace-0.3 (crate (name "ebacktrace") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "04h1k0cpgk5lh14i3vwwhbf072fjjmlwi46vfsraj84j8hxmclws") (features (quote (("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.3 (crate (name "ebacktrace") (vers "0.3.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v2akgnggf9addzkf2qlnrydm6j0n665dmhbpij4dfdj6y90m9ai") (features (quote (("force_backtrace") ("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.3 (crate (name "ebacktrace") (vers "0.3.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "12sf395g08fn78fp8jrjfnvg9hqhk0dd7p6c2adnah0cs80qhcjj") (features (quote (("force_backtrace") ("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.4 (crate (name "ebacktrace") (vers "0.4.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pkbj1cvpr8a246qpbr1h4r4ga57m9qhr81yzili8v7pqmwmvd44") (features (quote (("force_backtrace") ("derive_display") ("default" "derive_display"))))))

(define-public crate-ebacktrace-0.5 (crate (name "ebacktrace") (vers "0.5.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1czgm0mnakmwsmvxi2salqhq76ihyj5f0njlyqzg55hixg8b6c96") (features (quote (("force_backtrace") ("default")))) (yanked #t)))

(define-public crate-ebacktrace-0.5 (crate (name "ebacktrace") (vers "0.5.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1w4qhysklbhdmx5y8bfrrl7ivs3g0xx5kk1i0rfy4314780xw64a") (features (quote (("force_backtrace") ("default"))))))

