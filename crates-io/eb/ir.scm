(define-module (crates-io eb ir) #:use-module (crates-io))

(define-public crate-ebirsu-0.1 (crate (name "ebirsu") (vers "0.1.0") (deps (list (crate-dep (name "GSL") (req "^6.0.0") (default-features #t) (kind 0)))) (hash "03y0k049zd7v1agddkbgz8fb3460mcd0cvmfmfvcmiaxgwqx07kw")))

(define-public crate-ebirsu-0.1 (crate (name "ebirsu") (vers "0.1.1") (deps (list (crate-dep (name "GSL") (req "^6.0.0") (default-features #t) (kind 0)))) (hash "09wni4nc1vv4pdg8r5mdvpl8hmvk7hd60w712ym453gyk0mlan2j")))

(define-public crate-ebirsu-0.2 (crate (name "ebirsu") (vers "0.2.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("openblas-static"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray-stats") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "scilib") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1qf5mpq30ww1f18qja79mm9sghda9dwkvc77yj6x85jv42hqzxfk")))

