(define-module (crates-io eb pf) #:use-module (crates-io))

(define-public crate-ebpf-0.0.0 (crate (name "ebpf") (vers "0.0.0") (hash "1pa8g56acj6w2cpkp5b05362jfw8r9v0lp597cfi6659cvxb2m4n")))

(define-public crate-ebpf-0.0.1 (crate (name "ebpf") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.35") (default-features #t) (kind 0)) (crate-dep (name "xmas-elf") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0f0fv38zig5w24mriq2glg1pskdh0sv25f78r16pmxxsnkk7j2by")))

(define-public crate-ebpf-0.0.2 (crate (name "ebpf") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)) (crate-dep (name "perf") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "xmas-elf") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0k5hcbdlaszzz5ijy4by5r54wg9zx78q8ifvw4mfcdqf3ssyiayb")))

(define-public crate-ebpf-0.0.3 (crate (name "ebpf") (vers "0.0.3") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)) (crate-dep (name "perf") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "xmas-elf") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0xw0a1k277vj3p38n09fqxchkfw3risx47a70kckprbidc1yl95n")))

(define-public crate-ebpf-0.0.4 (crate (name "ebpf") (vers "0.0.4") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)) (crate-dep (name "perf") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "xmas-elf") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "zero") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0lj38szlck552lbd5avzj3jh451hlfxw0q1wi130w38if9pxlkmh")))

(define-public crate-ebpf-for-windows-0.1 (crate (name "ebpf-for-windows") (vers "0.1.0") (hash "1wwm4yvay6iz0ghj10cibjzk75shsafk3zf60qn6nkhq63wpljsr")))

(define-public crate-ebpf-histogram-0.1 (crate (name "ebpf-histogram") (vers "0.1.0") (deps (list (crate-dep (name "aya") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "prometheus") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "1qhls1hw464vq3knkfc70c0qzdha6c7vfwchwayv3h1pggnips3v")))

(define-public crate-ebpf-histogram-ebpf-0.1 (crate (name "ebpf-histogram-ebpf") (vers "0.1.0") (deps (list (crate-dep (name "aya-ebpf") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yxp36dz1cpbdmd18fi5pa7bz504l05dhj30gnms9pyrjxaczhyw")))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.0") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1brj6z7i64bw035b6zi5q3s2agsar7gvqzls6j0sgxk98wrmad6d") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.1") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0bdsw4dwqd793hy9nalspfdm67qbf2gbqnvlw5nx9gwi7525pjn4") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.2") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vliifz6i3c663a39wqkm9j7b7sc2hnwm9bj6cxslm7drszn3sn6") (features (quote (("macros" "ebpf-kern-macros")))) (yanked #t)))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.3") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0fbj8zl8rdsic69d0inix5df9d5fswy8r5aqad91mxmbaj5glvfj") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.4") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1acbj63vj5havhr8c74zzzmkp3xfj6n0dp1m59czyp0p6qrvpzn6") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.5") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1x9v2fa66i92034xa39zpqky8lywp35g6w705iyyz8rdhfiw51nh") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.6") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "109prjwsdhcq659j3m7vlcjygzq17b4w8cldzxdy4sav5ysm4rad") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.1 (crate (name "ebpf-kern") (vers "0.1.7") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-kern-macros") (req "^0.1.7") (optional #t) (default-features #t) (kind 0)))) (hash "1wa4k6w6pw7cqayy5bd19gh4hc0pcg3kgp7jm1p7lcy2x8gry068") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-0.2 (crate (name "ebpf-kern") (vers "0.2.0") (deps (list (crate-dep (name "ebpf-kern-macros") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "08qhx3r4jrx2x9vx05bwjr718gaafkw9mz3y166i4w788rv5gpn7") (features (quote (("macros" "ebpf-kern-macros"))))))

(define-public crate-ebpf-kern-macros-0.1 (crate (name "ebpf-kern-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "1c4q0w0dzfgy1p16hll85f2qnyphkd1q6mbnrlv60bjmi547jc2x")))

(define-public crate-ebpf-kern-macros-0.1 (crate (name "ebpf-kern-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "0bcl4w9m8n6xs737s9d07biaf2sm2wxcg2nf001w3vbvq5b9lp36")))

(define-public crate-ebpf-kern-macros-0.1 (crate (name "ebpf-kern-macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "10lj19yn5bn54qdqma49znkmmwk57762sirl9si9y11z6p96s2yr")))

(define-public crate-ebpf-kern-macros-0.1 (crate (name "ebpf-kern-macros") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "0y8snlslrpkc3rqj7yls0v4pkkxrb63hwd2dri94yc9hjq41yldx")))

(define-public crate-ebpf-kern-macros-0.1 (crate (name "ebpf-kern-macros") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "0qky2fjyni4y0nyphblvvdaaa01n2h6kzrisy0ikvmvbqxzm09yg")))

(define-public crate-ebpf-kern-macros-0.2 (crate (name "ebpf-kern-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "10pal2rfqm83vrgz7728ma3ac21svdn4s4713wy67w16ksiqb9h6")))

(define-public crate-ebpf-support-0.1 (crate (name "ebpf-support") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "081qkppvbqpkqdz9bn488v2qxv8gj8f7gxpdsmjhyfrx2pagsbaa")))

(define-public crate-ebpf-user-0.1 (crate (name "ebpf-user") (vers "0.1.0") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-user-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libbpf-sys") (req "^0.3.0-1") (default-features #t) (kind 0)))) (hash "1ihj7r6c51mviapz2ggp02iyd6lz6h2wr65sk72w08chcnkld6fi") (features (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1 (crate (name "ebpf-user") (vers "0.1.2") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-user-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libbpf-sys") (req "^0.3.0-1") (default-features #t) (kind 0)))) (hash "0mcdd7z6c33p2v1b82lvlp5rr8wiyk74xpsw0xhq7nyzxml4d0r3") (features (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1 (crate (name "ebpf-user") (vers "0.1.3") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-user-macros") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libbpf-sys") (req "^0.3.0-1") (default-features #t) (kind 0)))) (hash "1l4lphgxk11121g000gkaqd85l4fi1dzvf9q2rjag9sc48jlia5a") (features (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1 (crate (name "ebpf-user") (vers "0.1.4") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-user-macros") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libbpf-sys") (req "^0.3.0-1") (default-features #t) (kind 0)))) (hash "0rkzd3ihwd8zaijvskq4sgc7y4s75m13mkp9jb92sfb1y7p3ziys") (features (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1 (crate (name "ebpf-user") (vers "0.1.5") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-user-macros") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libbpf-sys") (req "^0.3.0-1") (default-features #t) (kind 0)))) (hash "1q5cfdy4g8c5iacxh149xahnii4v6xrxccsplnppi8i2rpd4m8rd") (features (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.1 (crate (name "ebpf-user") (vers "0.1.6") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-user-macros") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libbpf-sys") (req "^0.3.0-1") (default-features #t) (kind 0)))) (hash "00m6bwv58gzhh3chsljrj94qgaidqjb8p3lkja5asp99plsydbg0") (features (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-0.2 (crate (name "ebpf-user") (vers "0.2.0") (deps (list (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ebpf-user-macros") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libbpf-sys") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0xngr07mp2lcj7zx189jf5r2mqkxvr3lfrmpriarqp0vg7b87mw6") (features (quote (("macros" "ebpf-user-macros"))))))

(define-public crate-ebpf-user-macros-0.1 (crate (name "ebpf-user-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "0npcl6pkynbmkf7bbs3kb7vw11bwxk97jrffzbhvz65n5znbhpjf")))

(define-public crate-ebpf-user-macros-0.1 (crate (name "ebpf-user-macros") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "125xyn8andwg6z1hdiaaqd8qx7qjjdlxn1v5dym88q5zw5nnykpm")))

(define-public crate-ebpf-user-macros-0.1 (crate (name "ebpf-user-macros") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "0r3ana5mhyxh0316qwpqc6zpq23ghb0f0ds9zcnyzmc6dqppgzar")))

(define-public crate-ebpf-user-macros-0.2 (crate (name "ebpf-user-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "08j3zv4halwa63gc3xvzd34f4q1946xz766qrp4msk4wqc6sigan")))

