(define-module (crates-io eb ml) #:use-module (crates-io))

(define-public crate-ebml-0.0.0 (crate (name "ebml") (vers "0.0.0") (hash "1napkqq2amvwpn99fy6mb3kky5gmh6fmc8l2gnmh3f4k6rvgryzv")))

(define-public crate-ebml-iterable-0.1 (crate (name "ebml-iterable") (vers "0.1.0") (hash "014543ylia7pp1vxwcq385nhqxw3h4sf3d5plhcyzb418sjiiyf7")))

(define-public crate-ebml-iterable-0.2 (crate (name "ebml-iterable") (vers "0.2.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pl1vh31b8m0sn98i3iv121lbfy17r2gfvwga5dcm6id20zfdj61") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.3 (crate (name "ebml-iterable") (vers "0.3.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1s4rp3slsd6vlzkm51rq17rq18xzzrm5qf6r8hqbayc9y6pc2l1g") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.3 (crate (name "ebml-iterable") (vers "0.3.1") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0qbals26xc3ygbb7p5c8vj81mb85ibr2rc1rywi0nlhrqm192l1v") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.3 (crate (name "ebml-iterable") (vers "0.3.2") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0hxgnpag55ajpv85ypqbbsdgiy2blj4skf7dqj8qpd4wns7yimp6") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4 (crate (name "ebml-iterable") (vers "0.4.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0a9aw1wys9a1lig4vxss7hiz83v458480y03lgaljf8vz2973rf5") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4 (crate (name "ebml-iterable") (vers "0.4.1") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1hw8h97hgd8485mcq0cfsypipw2yzhdwq6yd56j0vpmzm82czwqn") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4 (crate (name "ebml-iterable") (vers "0.4.2") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0y60fdca0l695x2z1wakba5ndl16gmad58lqxyp8iyrspali47dk") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4 (crate (name "ebml-iterable") (vers "0.4.3") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1va640375d985w60x2ms1vrlnvf030l1g9pa7fhirciyxmkc4g8v") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.4 (crate (name "ebml-iterable") (vers "0.4.4") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1jdaklv9f8nj5zmpr935ii97l74ml93xwn7n29nk6y63hc33z9mg") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.5 (crate (name "ebml-iterable") (vers "0.5.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0g2yz9nfp1dgw67f7shby5wcrwxrvgqh5n381hl45pfad2wm7a65") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.6 (crate (name "ebml-iterable") (vers "0.6.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (optional #t) (default-features #t) (kind 0)))) (hash "0h76gaqn1z9bwh6cy6yl3fpds19lww63m9cdc9q1mk2gza714892") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.6 (crate (name "ebml-iterable") (vers "0.6.1") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (optional #t) (default-features #t) (kind 0)))) (hash "0jyxjw8hin075sl536b1sf3c6j0v70g804x3253gyn6yapj1y1i2") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-0.6 (crate (name "ebml-iterable") (vers "0.6.2") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ebml-iterable-specification-derive") (req "=0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (optional #t) (default-features #t) (kind 0)))) (hash "1c517b4b0ahf6k4mkfsfh0hhka01qhdxfpy8cpcf3jmxdr4jam80") (features (quote (("derive-spec" "ebml-iterable-specification-derive"))))))

(define-public crate-ebml-iterable-specification-0.1 (crate (name "ebml-iterable-specification") (vers "0.1.0") (hash "021p181j904ymgk4v945y0bckpyrakljldxzv0v56vr0rqr0wz2j")))

(define-public crate-ebml-iterable-specification-0.2 (crate (name "ebml-iterable-specification") (vers "0.2.0") (hash "074pc67ajf7m85y7dggd8sdy0fxwa8m813gb3an6pibjblm03ibn")))

(define-public crate-ebml-iterable-specification-0.3 (crate (name "ebml-iterable-specification") (vers "0.3.0") (hash "1i0zrqj6j2n6vlgcdx43kxdfcj419pd6gx4hibhkpz3jf84wk4d9")))

(define-public crate-ebml-iterable-specification-0.4 (crate (name "ebml-iterable-specification") (vers "0.4.0") (hash "113720zsjjcnp41z2rij03k1ks85lpm567r38ifp764s2npnfr7m")))

(define-public crate-ebml-iterable-specification-derive-0.1 (crate (name "ebml-iterable-specification-derive") (vers "0.1.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ahmrg335yiajbq925mf1s5vjhmaj07lixi6kg8zjj66gix3qirb")))

(define-public crate-ebml-iterable-specification-derive-0.2 (crate (name "ebml-iterable-specification-derive") (vers "0.2.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kfc991x6f5z3sn2gp135hnwg3iav1c11vqyfk7w1h162rqgjkb1")))

(define-public crate-ebml-iterable-specification-derive-0.3 (crate (name "ebml-iterable-specification-derive") (vers "0.3.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v8naf2mjb0dx6c2z40akmg9nvlshclnnvlm0shricr29ip1xivb")))

(define-public crate-ebml-iterable-specification-derive-0.4 (crate (name "ebml-iterable-specification-derive") (vers "0.4.0") (deps (list (crate-dep (name "ebml-iterable-specification") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rp0gpwbbcasy8lasvzr98r9j9hhb8sxpi3i1zjdq3rh308bhrmh")))

(define-public crate-ebml-rs-0.0.0 (crate (name "ebml-rs") (vers "0.0.0") (hash "0s7l5vzpnxsa9ddir2nd6if1lf71mssivfjcaks251i797mg0sry") (yanked #t)))

