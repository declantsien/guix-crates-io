(define-module (crates-io eb oo) #:use-module (crates-io))

(define-public crate-ebook-creator-0.1 (crate (name "ebook-creator") (vers "0.1.0") (hash "14c7y99jax8y27k9yywj9hya243x28znlfldz6cj2wzcsbb5lnv0")))

(define-public crate-ebookit-0.1 (crate (name "ebookit") (vers "0.1.0") (hash "0d6ikmz226hn7my6m941s69s55n832mgr9kn02m86x1p8wcx32iw")))

(define-public crate-ebookkit-0.1 (crate (name "ebookkit") (vers "0.1.0") (hash "1ibqlvhzbwlvbpzqxf1xmj4padilxppz8k0yaxvcb508bgwpmxf3")))

