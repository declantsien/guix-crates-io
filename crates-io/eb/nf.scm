(define-module (crates-io eb nf) #:use-module (crates-io))

(define-public crate-ebnf-0.1 (crate (name "ebnf") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "parse-hyperlinks") (req "^0.23.3") (default-features #t) (kind 0)))) (hash "1ydgk4ynaa40nba52l2z6blrs1jx11di5c61xx3r46bnajqqmain")))

(define-public crate-ebnf-0.1 (crate (name "ebnf") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "parse-hyperlinks") (req "^0.23.3") (default-features #t) (kind 0)))) (hash "0lh5dfp0d1jvblj8yx7cq5vxzfw1qww184y6zcg7fpyxsipvmzzm")))

(define-public crate-ebnf-0.1 (crate (name "ebnf") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "parse-hyperlinks") (req "^0.23.3") (default-features #t) (kind 0)))) (hash "058fhyzwam4dvws9c6f92syp67ld12s9v4g2j6qx687sh4x9d8bj")))

(define-public crate-ebnf-0.1 (crate (name "ebnf") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "parse-hyperlinks") (req "^0.23.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.26.0") (features (quote ("yaml"))) (default-features #t) (kind 2)))) (hash "167y8fv7zc9p6f8xzv8dw7fzgb4cpfkfa2x0qxyi5yv0qdbhmqhq") (features (quote (("strict"))))))

(define-public crate-ebnf-0.1 (crate (name "ebnf") (vers "0.1.4") (deps (list (crate-dep (name "insta") (req "^1.26.0") (features (quote ("yaml"))) (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "parse-hyperlinks") (req "^0.23.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13szmv96fkjmlhwj2c33zmzxv7axrcas4bwpdy7cv50qrhswwalv") (features (quote (("strict"))))))

(define-public crate-ebnf-fmt-0.1 (crate (name "ebnf-fmt") (vers "0.1.0") (deps (list (crate-dep (name "ebnf-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08nvj0d5ak9624jl55y7dkavfxdkn5mqr2828wp0b13mr5hlz76b") (features (quote (("fromstr" "strum") ("default"))))))

(define-public crate-ebnf-parser-0.1 (crate (name "ebnf-parser") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0r4m9xyclrgnrgsrd63igl6zm3hbpi82qy21ggy84ywhmz8lakhd")))

