(define-module (crates-io eb us) #:use-module (crates-io))

(define-public crate-ebus-0.1 (crate (name "ebus") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.58") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1.2.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1hf13vjkqb8qzs3493m8zbvzjwijlwcdzfd9h7klvp2qxaiwjrcr")))

(define-public crate-ebus-1 (crate (name "ebus") (vers "1.0.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.58") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1.2.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0p8w7w0mhgyy8qx45kvdc0l2b2yssc1hn38sdi5fpz08cv658my2") (features (quote (("uuid") ("full" "uuid"))))))

(define-public crate-ebus-1 (crate (name "ebus") (vers "1.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "087igpjw6iplgap9axxf7r0igmqz8q9wkxh1lqbxlgk76m7nc9bz")))

(define-public crate-ebus-1 (crate (name "ebus") (vers "1.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0pa6b6klbmb858xqgsss6d2imgwcp5kh9l8f636347siap14jw8x")))

(define-public crate-ebus-1 (crate (name "ebus") (vers "1.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1fjqhs6xv6rkf7xfsq4vj7kvmh6n0axkc98kkqx08r6pr26qfzin")))

(define-public crate-ebus-1 (crate (name "ebus") (vers "1.3.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0b9k7pks8i2p6xm66s5a0pwdynr30kshmsd9c16dv9f6x4dkcpag") (features (quote (("default" "async_subscriber") ("async_subscriber"))))))

(define-public crate-ebustl-0.1 (crate (name "ebustl") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "iso6937") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.1") (default-features #t) (kind 0)))) (hash "1nvqbz7jklbbza9acwir8an962bpf1ali94n1xvvb0xfnf323jsx")))

(define-public crate-ebustl-0.2 (crate (name "ebustl") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "iso6937") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1j3sv4ffk3qz0x708y4ijw7cyflkvfida1324mf25ncv6dly6cq0")))

(define-public crate-ebustl-0.3 (crate (name "ebustl") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "iso6937") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "044hw4h5wq5bp38wpdjw0j8zhnir2v1zi7n9dhp062v0asa2h14j")))

(define-public crate-ebustl-0.4 (crate (name "ebustl") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "codepage-strings") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "iso6937") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "02yl9qxnib8211ycksz042hj8bj32x356pwcz43qgmh2dk7syp3w")))

