(define-module (crates-io eb -p) #:use-module (crates-io))

(define-public crate-eb-prime-factor-0.1 (crate (name "eb-prime-factor") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "03y4z08yg6bazhcbamzbzsd6zfcwjw0ficb8qg61mqd9ffchxgv5")))

(define-public crate-eb-prime-factor-0.2 (crate (name "eb-prime-factor") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1inxy63wdf5bbccixikjph52mcpn80yfk2kcdwp3ml0pvmn70jd4")))

(define-public crate-eb-prime-factor-0.2 (crate (name "eb-prime-factor") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1bpk03jfc2qkwx8nmfj18lwvbld35vclz73r6024br3cljf8vicx")))

