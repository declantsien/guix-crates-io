(define-module (crates-io ly h-) #:use-module (crates-io))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1cjq3rhhayxr9wxpwvnvmw8xsndj3cwlph7ywsgl8a5fa02320vq")))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0nrfj904hqz62bnbc3ijdncgimi68h4v9rhy20g8awpf3v1b0qq4")))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.2-pre.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0aj5clsg335n6zhagqzk5kfqfd55v2jw57rd4y2z49m9xfv0g3mw")))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.2-pre.1") (hash "107cf1bjlb5sa0ai5zwwys4z4skdvsbgzr3kkxszgzx81d8rmyjk")))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.2") (hash "0k35yaqw4ah99n6sgqdm0h5ghg1n59ylj13a3zp2d498gh158m6z")))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.3-pre.0") (hash "1dfl05bvvhyfn3r9ypdlqyzqwdsdilj2r4ypnfjijh46n60v7lzn")))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.3-pre.1") (hash "0ynxdjdrgw2gnicjninylyk8a6hrl96l23z7wy2y2gpbx1bz4qlw")))

(define-public crate-lyh-crateio-test-0.1 (crate (name "lyh-crateio-test") (vers "0.1.3") (hash "184vr3s29mzpd0v03p5j10i0kavmib18l0lfzxi3cdnxd2dy0ijh")))

