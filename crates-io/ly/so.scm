(define-module (crates-io ly so) #:use-module (crates-io))

(define-public crate-lysogeny-broth-1 (crate (name "lysogeny-broth") (vers "1.0.0") (hash "1f9wf83r60yb5w9h4yvib75a2gq88mfhqhlvc7cr8l1znvwsapki")))

(define-public crate-lysogeny-broth-1 (crate (name "lysogeny-broth") (vers "1.1.1") (hash "0smjadjw5kmiayafa0rj3qmgryzmiy3mw764339cbk1346kbcb2x") (features (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1 (crate (name "lysogeny-broth") (vers "1.1.2") (hash "09gvxspnqnwzn98zlmgj67p7f2yqacqsg504g2yvpz7zncckqdam") (features (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1 (crate (name "lysogeny-broth") (vers "1.1.3") (hash "1anp92n80lxmzs4isj2g9qxwcy7wp3yzacdfbplpnv1v5vsca43q") (features (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1 (crate (name "lysogeny-broth") (vers "1.1.4") (hash "0ncbncp08kd6xmqwdgc5179r8idhg8y595j3kr1ggmj0i16gsv7v") (features (quote (("default" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-group-u8" "dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

(define-public crate-lysogeny-broth-1 (crate (name "lysogeny-broth") (vers "1.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1r6h91542qcf6m3931jk0bkrl3a6xfzpvsazy1pfm0n03bq9rzl1") (features (quote (("default" "dead-alive-only") ("dead-alive-u8-utils" "dead-alive-only") ("dead-alive-only") ("dead-alive-into-bool" "dead-alive-only"))))))

