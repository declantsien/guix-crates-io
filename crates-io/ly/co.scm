(define-module (crates-io ly co) #:use-module (crates-io))

(define-public crate-lycopersicum-0.1 (crate (name "lycopersicum") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "184vzf6p94wzz2sxf3pvi19rrwhn2biav81zgc6lx45xb9y8h9p9")))

(define-public crate-lycopersicum-0.2 (crate (name "lycopersicum") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.8.0") (default-features #t) (kind 0)))) (hash "0mvia9cj4djsc94zvcfgcanxgh558hsjinw1jv39jl5022m73rvg")))

(define-public crate-lycopersicum-0.3 (crate (name "lycopersicum") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.8.0") (default-features #t) (kind 0)))) (hash "0cykgwzcdizpah1qnq2gz31dcsgizwsv327qvbmm5fzrr3iii033")))

(define-public crate-lycopersicum-0.3 (crate (name "lycopersicum") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.8.0") (default-features #t) (kind 0)))) (hash "0isn29w966hq3m2f7xv3s3xlk5qkqzi2yc0i6dn8pqwp53xri5lk")))

(define-public crate-lycopersicum-0.3 (crate (name "lycopersicum") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.8.0") (default-features #t) (kind 0)))) (hash "16mg28124gh4rxffnqfs0301qlgnqzszh7s2m0ibzk98pzaql6mq")))

(define-public crate-lycopersicum-0.4 (crate (name "lycopersicum") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.8.0") (default-features #t) (kind 0)))) (hash "030fj830xvz03pqm72dqcyk8wr4rr759mhn0i18w029v951kfn4z")))

