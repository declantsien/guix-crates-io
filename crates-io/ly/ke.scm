(define-module (crates-io ly ke) #:use-module (crates-io))

(define-public crate-lyken-0.0.0 (crate (name "lyken") (vers "0.0.0") (hash "0gf1hqipdpcf9ang3cgzz0hfgalhwx1hp6ryc6zb8l7nz952ilry")))

(define-public crate-lyketo-0.1 (crate (name "lyketo") (vers "0.1.0") (hash "1qzg7skv06xdi5rcd8g2hzf9kgh2wajrkvcmwkpa7by0j6cqhbmr")))

