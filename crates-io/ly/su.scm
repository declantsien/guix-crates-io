(define-module (crates-io ly su) #:use-module (crates-io))

(define-public crate-lysutil-0.0.1 (crate (name "lysutil") (vers "0.0.1") (deps (list (crate-dep (name "string-builder") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rz1nwq7wlbmycn53l4iy0fkf4bwy0rfz45371769266yi41lqj9")))

(define-public crate-lysutil-0.1 (crate (name "lysutil") (vers "0.1.0") (deps (list (crate-dep (name "string-builder") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yci154z1zkwa17gq73bfmxca1jbyjib5w5497729khw5ipgnkjk")))

(define-public crate-lysutil-0.1 (crate (name "lysutil") (vers "0.1.1") (deps (list (crate-dep (name "string-builder") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kxa83mg87spcrpy7mw9ldj4hf0gw844jxwlrbizcwldq642bx1r")))

