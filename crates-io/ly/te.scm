(define-module (crates-io ly te) #:use-module (crates-io))

(define-public crate-lyte-0.1 (crate (name "lyte") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.21") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.10") (default-features #t) (kind 0)))) (hash "1ph3x5ymwv5m3qglmnkz11fz4r4w9cxpplmndpmk3f3fvbwb1hmr")))

(define-public crate-lytelog-0.1 (crate (name "lytelog") (vers "0.1.0") (hash "0cxczdbk952zcdb3wyq4av39p84q7pk0c4scx6xv28v6xcam8lbh")))

