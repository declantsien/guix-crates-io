(define-module (crates-io ly ne) #:use-module (crates-io))

(define-public crate-lyne-0.0.1 (crate (name "lyne") (vers "0.0.1-alpha") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "14z1s6rdvfsa213i9rym0wcay1vqkmlfqyqx7n167r7n11lkls03")))

(define-public crate-lyne-0.0.1 (crate (name "lyne") (vers "0.0.1-alpha.1") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1rnfn09a9mwgbwv7ar9n07k5w0mm911a3wwqx7xymis5r4qjb9rp")))

(define-public crate-lyne-0.0.1 (crate (name "lyne") (vers "0.0.1-alpha.2") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0yq6wl717fmyvg3k75ls5m2d88l86cf4zv8wrjqm2zl57i6f8n8w")))

(define-public crate-lyne-0.0.1 (crate (name "lyne") (vers "0.0.1-alpha.3") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "13rs3j846f5bh01p08byd7xjr6dl8va3qfjqk3vfdhvyqpf49x5h")))

(define-public crate-lyneate-0.1 (crate (name "lyneate") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "10m4sxzyjcnmak9bn7vnm7cyflyc9zznr4a0yv46q98v3f597pf5")))

(define-public crate-lyneate-0.1 (crate (name "lyneate") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "06irqva12fwp03nl5spgix6sssq3b92i8wpi27jrbp9nf3fd8jg5")))

(define-public crate-lyneate-0.2 (crate (name "lyneate") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0wdbxqd9qidlbls1qxpq30vs5wcra6lxyla04y49jsmk5ir0f08r")))

(define-public crate-lyneate-0.2 (crate (name "lyneate") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1qx5bw98jyl3711dhamb6m347n2ynzr62sjwdxwm44m7gqsg14yv")))

