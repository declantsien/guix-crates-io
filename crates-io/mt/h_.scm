(define-module (crates-io mt h_) #:use-module (crates-io))

(define-public crate-mth_calc-0.1 (crate (name "mth_calc") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "04mb4b0bvjjx0g2q28k7wil79a59nd6k1vffxizafj2vdlr9q7l6")))

(define-public crate-mth_calc-0.1 (crate (name "mth_calc") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1fd4lhavxac17aj7m49wwnn4ax77n8hfm8iswvxwzhhpfvv43vmm")))

(define-public crate-mth_calc-0.1 (crate (name "mth_calc") (vers "0.1.2") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "04aibj321q4v7py3f5imzknliy72gcp0cr6i9j9vlxk09x1xja6x")))

(define-public crate-mth_calc-0.1 (crate (name "mth_calc") (vers "0.1.3") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0r711wgxdz8i8wa0jrshw607ywmk29g8s4yx8qdbld6rhf8vpd22")))

(define-public crate-mth_calc-0.1 (crate (name "mth_calc") (vers "0.1.4") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0m75rnj282ixbdps9jpnbazf4r6c6mrcz4afwg7jvm0v462bpm7b")))

(define-public crate-mth_calc-0.1 (crate (name "mth_calc") (vers "0.1.5") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1v16ba4s53dlj3n5x86vfq7q2fpk7yf7ss7y8jcg99lzm1ncfm1y")))

(define-public crate-mth_calc-0.1 (crate (name "mth_calc") (vers "0.1.6") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "04fw7zmmp092jph8z8q55b2gx2agzp91pzjzfvbsr6dpsyyq6kc5")))

