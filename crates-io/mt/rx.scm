(define-module (crates-io mt rx) #:use-module (crates-io))

(define-public crate-mtrx-0.1 (crate (name "mtrx") (vers "0.1.0") (hash "11lxff8c3iw8fpzxkzc44k32rvykigkqxqw3spja1y7prk1a4fba")))

(define-public crate-mtrx-rs-0.1 (crate (name "mtrx-rs") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.23") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "matrix-sdk") (req "^0.5.0") (features (quote ("e2e-encryption" "sled" "markdown"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18") (default-features #t) (kind 0)))) (hash "00zqmn6c9xnp6i9fpdh47jna9461miak2dlp1zkx5lbqmjh28xnv")))

