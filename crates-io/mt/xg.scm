(define-module (crates-io mt xg) #:use-module (crates-io))

(define-public crate-mtxgroup-0.1 (crate (name "mtxgroup") (vers "0.1.0") (deps (list (crate-dep (name "spin") (req "^0.9.8") (kind 0)))) (hash "0w1jys8p14wqjy5axnbq5xrmp2c1jrsvy0b7sg6a2f1b1h9lqbm8") (features (quote (("std") ("spin" "spin/mutex" "spin/spin_mutex") ("default" "std")))) (yanked #t)))

(define-public crate-mtxgroup-0.1 (crate (name "mtxgroup") (vers "0.1.1") (deps (list (crate-dep (name "spin") (req "^0.9.8") (kind 0)))) (hash "1bjv48ijxf550kfpmyx2p0fzc1nbq8i1nmkkmq03pv95289cr02y") (features (quote (("std") ("spin" "spin/mutex" "spin/spin_mutex") ("default" "std"))))))

