(define-module (crates-io mt cn) #:use-module (crates-io))

(define-public crate-mtcnn-0.1 (crate (name "mtcnn") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tensorflow") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0wcvfzr8zws3ilc0333n6iy5nqfs82r6ymb32n2faihqvnl1s8kp")))

