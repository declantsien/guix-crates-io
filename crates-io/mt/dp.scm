(define-module (crates-io mt dp) #:use-module (crates-io))

(define-public crate-mtdparts-0.1 (crate (name "mtdparts") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1is8q4lj3f7m8ra0affh6lqpd5r1ba190n54q8f37kbr5vg2pwwj")))

(define-public crate-mtdparts-0.1 (crate (name "mtdparts") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dshv873sm16pzc2kdfkz19kyhxspnjx6grnzkmaljqicn584q1f")))

(define-public crate-mtdparts-0.2 (crate (name "mtdparts") (vers "0.2.0") (hash "0s1x6368xzvkn35yis3dbmpm7v8glrznd6wsymc8br20f6v9lhpq")))

