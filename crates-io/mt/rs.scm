(define-module (crates-io mt rs) #:use-module (crates-io))

(define-public crate-mtrs-0.1 (crate (name "mtrs") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "08169zw97bg8zc9d74qzh5rcp1qwy0zqxxi4zv9cqxfygphpvcdq")))

(define-public crate-mtrs-0.1 (crate (name "mtrs") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mwibnqqbjxjh6qhsbl2mv5s78dgwnkh8806rxp7k7j5qkmdxfj3")))

(define-public crate-mtrs-0.1 (crate (name "mtrs") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "041sgb6rdcirhgh1gp6gbsqjgvlchl51lfmz7857243arrzsg6nh")))

(define-public crate-mtrs-0.1 (crate (name "mtrs") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gm0akp8qza5xwqj7b4ir2xiv8px529iyvcpk3xk4vhc8h4g4ghw")))

(define-public crate-mtrs-0.1 (crate (name "mtrs") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "12h237mr30ff8ksnwg9dy0wnmaaw4wprd70s5hx5h23pcm9kzmfj")))

(define-public crate-mtrs-0.2 (crate (name "mtrs") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p2cjpfbqzqq62dn4a2wf9zyzrj73kk0j4mnbn7b4305njql0acl")))

