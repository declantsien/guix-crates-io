(define-module (crates-io mt #{19}#) #:use-module (crates-io))

(define-public crate-mt19937-0.1 (crate (name "mt19937") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "1a1b2sg37zclrfpmm450qh6vlm182pr5lwf4hzjxkfndykmq9wxs")))

(define-public crate-mt19937-1 (crate (name "mt19937") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "18p4i7m9vyr2w335zkhrm5lz8834583d3g5r02sash91jlbcgyhj")))

(define-public crate-mt19937-1 (crate (name "mt19937") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "1gq8anmd5xn2rqpg3513lpj7xlhmr81chcx6iw53c1n7m8yjjx66")))

(define-public crate-mt19937-2 (crate (name "mt19937") (vers "2.0.0") (deps (list (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "0n21hcw8v61ni64a16l1njabn8s6v42k4k5dqk0gpnrf8ssd2v9y")))

(define-public crate-mt19937-2 (crate (name "mt19937") (vers "2.0.1") (deps (list (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "18g3i5b5lz9kf2b8hbskiay6bs4766l1dv6am68mj39pxli7zjhj")))

