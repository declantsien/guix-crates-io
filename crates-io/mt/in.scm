(define-module (crates-io mt in) #:use-module (crates-io))

(define-public crate-mtinit-0.1 (crate (name "mtinit") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "figlet-rs") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "048nh58iy975jxi4cbijdh1i8wx6zw85yv06blhhgwcrjs180yw8")))

