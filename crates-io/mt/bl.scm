(define-module (crates-io mt bl) #:use-module (crates-io))

(define-public crate-mtbl-0.1 (crate (name "mtbl") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.18.131") (default-features #t) (kind 2)) (crate-dep (name "mtbl-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.1.4") (default-features #t) (kind 2)))) (hash "1206nnhwyarq9wziwkg4np6144prwmkg4yxvcp8i3w5ybdp6gkpl")))

(define-public crate-mtbl-0.2 (crate (name "mtbl") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "mtbl-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.1.4") (default-features #t) (kind 2)))) (hash "1nixvqwqas12k14fkpvdbwjr0qiydpy9flrwhakvfrdq3rlbpbw9")))

(define-public crate-mtbl-0.2 (crate (name "mtbl") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "mtbl-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.1.4") (default-features #t) (kind 2)))) (hash "07rbxh8i16jc6704vnvi8xnz8gy647qw7bfn569qb8vvlhns66c5")))

(define-public crate-mtbl-sys-0.1 (crate (name "mtbl-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^1.1.1") (default-features #t) (kind 2)))) (hash "0pqfxx7kyb2ip9pfzd5gqff0dvsvvwljhyixqxdw6a13qajwmpni")))

(define-public crate-mtbl-sys-0.1 (crate (name "mtbl-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.1.2") (default-features #t) (kind 2)))) (hash "0bbk0l4y8vm7d7hc6wwic3qf1m2i41qvn7110wb3xp8dkbwkv71v")))

(define-public crate-mtbl-sys-0.2 (crate (name "mtbl-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.1.4") (default-features #t) (kind 2)))) (hash "0wq782kq6xb8dnzdbf2y6hq3xfpr8rkj9wbfhg380nab6d1098c3")))

