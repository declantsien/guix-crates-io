(define-module (crates-io mt #{9v}#) #:use-module (crates-io))

(define-public crate-mt9v034-i2c-0.1 (crate (name "mt9v034-i2c") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "17hba9sm8bsp1axlaikmxr5kjly8a516xxr2csm5vkbzrbdpa8r6") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-mt9v034-i2c-0.1 (crate (name "mt9v034-i2c") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0fj5wmyn668nn91zj857r7hx9a448vq5xcw72byi6dp0gs96dq66") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-mt9v034-i2c-0.1 (crate (name "mt9v034-i2c") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1vr8ywdyrc6n7729583r19l30nndavv6h2jwz59gb0ykh6rkm3wz") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-mt9v034-i2c-0.1 (crate (name "mt9v034-i2c") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1jxz3r7008v6z0lj1k8h5g5711y0v7v1k0n8lww6bfcqbsjaqlmy") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-mt9v034-i2c-0.1 (crate (name "mt9v034-i2c") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1q3yfbsfn6v3k5v5a6igx5hrjnxlba2n3f1sm6rkyf8f7i4s1ydn") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

(define-public crate-mt9v034-i2c-0.1 (crate (name "mt9v034-i2c") (vers "0.1.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0pl7gpgicfa3bb9hhzsky1d7rwk8f1dhdp51mri79ydp484wxi8k") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

