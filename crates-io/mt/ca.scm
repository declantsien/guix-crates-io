(define-module (crates-io mt ca) #:use-module (crates-io))

(define-public crate-mtcap-0.1 (crate (name "mtcap") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.23") (default-features #t) (kind 0)))) (hash "0hcd5i71pirx5h8599kqfkbm5kdcdwljpanklc4wqb3l84ncx5m4")))

(define-public crate-mtcap-0.1 (crate (name "mtcap") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.23") (default-features #t) (kind 0)))) (hash "1ih3pj071lwicv6np2byk05xajkb9fqlka5bj4i6jkmi2ndbib8g")))

(define-public crate-mtcap-0.2 (crate (name "mtcap") (vers "0.2.0") (deps (list (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ab60cwn0nbs376d1h6fhh6pb5d09l4fzai24f2ix6cb921fa42h")))

(define-public crate-mtcap-0.3 (crate (name "mtcap") (vers "0.3.0") (deps (list (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "012yzajm4mykp3csy9cshpjvqyv9y7ljbllz658hgv4ahl5fmw98")))

(define-public crate-mtcars-0.0.1 (crate (name "mtcars") (vers "0.0.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "0s55020z9q7bd2jgcax20hsw1bcp55nbngm43knbygvymg090mrx")))

