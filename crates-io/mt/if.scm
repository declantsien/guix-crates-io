(define-module (crates-io mt if) #:use-module (crates-io))

(define-public crate-mtif-0.1 (crate (name "mtif") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("std" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.26.0") (default-features #t) (kind 2)))) (hash "1w079ng7nzhs9fw9anzpz1mrb3v27f8hf6anrxvmhpmnhrjz9bw8") (yanked #t)))

(define-public crate-mtif-0.1 (crate (name "mtif") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("std" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.26.0") (default-features #t) (kind 2)))) (hash "09w7jgc6rqwz3hj0wv59axzzap9wl16zlzq1dh2gcf9761yiqw9h") (yanked #t)))

(define-public crate-mtif-0.1 (crate (name "mtif") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("std" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.26.0") (default-features #t) (kind 2)))) (hash "0kdz9a48rn096nsw4blm615li85rn4g3sgfrhd6sc8rwya5xniqh")))

