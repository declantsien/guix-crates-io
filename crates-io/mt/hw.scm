(define-module (crates-io mt hw) #:use-module (crates-io))

(define-public crate-MTHW-0.1 (crate (name "MTHW") (vers "0.1.0") (deps (list (crate-dep (name "aurelius") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.27") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("rt" "macros" "io-util" "process" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "07g6d0jjampzyy50ladvriwjwdlzcbp6811za3nrsvs8p488clwp")))

