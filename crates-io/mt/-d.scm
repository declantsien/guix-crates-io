(define-module (crates-io mt -d) #:use-module (crates-io))

(define-public crate-mt-debug-counters-0.1 (crate (name "mt-debug-counters") (vers "0.1.0") (deps (list (crate-dep (name "bytesize") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0d387sg2pv286mdj4w9rraq1vqwrrji6w1w7sv34hqn86hyxr1w1")))

(define-public crate-mt-debug-counters-0.1 (crate (name "mt-debug-counters") (vers "0.1.1") (deps (list (crate-dep (name "bytesize") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "166p3sv3hr0kanmh813rnx85i80za5d7b2l63vnggrk8jmwv0r84")))

(define-public crate-mt-debug-counters-0.1 (crate (name "mt-debug-counters") (vers "0.1.2") (deps (list (crate-dep (name "bytesize") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1hhvsb7bcmr23jjwfkb0ffxbqpf8dzhz6s9w2h5bkhnpgvza9q6b")))

(define-public crate-mt-debug-counters-0.1 (crate (name "mt-debug-counters") (vers "0.1.3") (deps (list (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1vszzadqdvgz2kr4mpwly6vs113s5gvvpryr20f0amsr9a057c0v")))

(define-public crate-mt-dom-0.1 (crate (name "mt-dom") (vers "0.1.0") (hash "0r6kw5mzlx1mwzm541axwgzkq9b0pb1axpk5wld4zrinp99n5bnr")))

(define-public crate-mt-dom-0.1 (crate (name "mt-dom") (vers "0.1.1") (hash "12fs2ikfip1dczm5wx4cf5gbd49bjw7bwm2gy1akyrp1a9w5n967")))

(define-public crate-mt-dom-0.2 (crate (name "mt-dom") (vers "0.2.0") (hash "1rnvqd3v6r1vxi3askgbpzklhwyj82a7rcwgbs4s0c4ddgr7spla")))

(define-public crate-mt-dom-0.2 (crate (name "mt-dom") (vers "0.2.1") (hash "1maj6n5w4x1n5wznw4x34pd01sbg7fjrr1pw5sgdzk3njp0q0r6y")))

(define-public crate-mt-dom-0.2 (crate (name "mt-dom") (vers "0.2.2") (hash "1s44r300gyy8w15ckhcnbnblhx9z9ximby7ibhg7l6kzfk70p1sn")))

(define-public crate-mt-dom-0.3 (crate (name "mt-dom") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k65shq9a1f6brdpvm8ji3blf6mm9gscl6z9r1q0vjpfj0338d6i")))

(define-public crate-mt-dom-0.4 (crate (name "mt-dom") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0kab5vw5j2wikqnr514hqpl3vh8ix637zz3hk438pyf0ym309whg")))

(define-public crate-mt-dom-0.4 (crate (name "mt-dom") (vers "0.4.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pxjmgjxqh2sqij9jzq0gl600ypzap622bshg5h5pbzjf332gkcy")))

(define-public crate-mt-dom-0.5 (crate (name "mt-dom") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1n5m30s2vyqkb2z59ay4bma9n6arra7ycyw006ffgb1x5rbxnd4v") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.5 (crate (name "mt-dom") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0k2rv1fr6rqdha9sx4mfcjq3pqldyfcf4fmf7q3n92y2xsbfpmv8") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.5 (crate (name "mt-dom") (vers "0.5.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0c2m1awsc0fnbibx55w85h260v33g444jvr42rksz8kaf0wihcj8") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.5 (crate (name "mt-dom") (vers "0.5.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "04rnzjf5pnvb0xv795p2gp9dfr8q8n14hscaflvzjsah505ir2hc") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.6 (crate (name "mt-dom") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0riw2gl3blfq1zb4qqq7xiaa3ggbsxgp3vyr51rnisk3rf9pizaa") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.6 (crate (name "mt-dom") (vers "0.6.1") (deps (list (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 2)))) (hash "06zpsllkq736y6kpq0p64iybi6li7rycyym6vpsl5nair36m5p05") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.6 (crate (name "mt-dom") (vers "0.6.2") (deps (list (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 2)))) (hash "1py358cpcp9i39yavk9f43gk8xngnlam1gy7mpfidmjhryhnyd57") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.6 (crate (name "mt-dom") (vers "0.6.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "18y7jsif4znfriyp0ch8p6hdh6cmymn3vdpa9r70g6i0azw9nf0f") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.7 (crate (name "mt-dom") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0bvmfnpqwrs3hlbq0xhx5c5b81riyy2s2v4500gnfd127j5slfpx") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.7 (crate (name "mt-dom") (vers "0.7.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0pys4xd2vpx8iaryx5952x1llnk4q48diq40snavsyazpjff467v") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.8 (crate (name "mt-dom") (vers "0.8.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "184l5rfjygd9ydhcl8klznay89fzscmhb9frp0b57xb8jpccz7mp") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.9 (crate (name "mt-dom") (vers "0.9.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "14yvrp4kzkmqja8jjj7m06nk699i29l451jh1p5knaax7qyrd8q3") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.10 (crate (name "mt-dom") (vers "0.10.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "062x1479y3b068cjh2vgjzk8zk8mcxdbs4q1cx8q8x1ch7vmpm7b") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.11 (crate (name "mt-dom") (vers "0.11.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "061ibgg16x142wb50bbp980za4d7i6kh4dismy9g72bz858aq76l") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.11 (crate (name "mt-dom") (vers "0.11.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0sxa9im21c6x006lmv9kbx0nkzjjzlhf3pjz8chdhz15cznzsjcn") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.12 (crate (name "mt-dom") (vers "0.12.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0i7jfnfv5l5aw1bac60mk1s8mkkxzb3wdd5vp0qmr6glhzmc93ma") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.12 (crate (name "mt-dom") (vers "0.12.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1zv916g1ry6qh1qpn8q1q52l58kc2c0zwcssmivma6zwqlkrdpl3") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.12 (crate (name "mt-dom") (vers "0.12.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "069f8awq5wrgg343jvz9jjm780x1jsizd85c3y7g3w4zvwky2r20") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.12 (crate (name "mt-dom") (vers "0.12.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1zqapbxpdr618gl2ayd5yqx51p3rpz8qfp5w6md2gfsvqi1ashiv") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.13 (crate (name "mt-dom") (vers "0.13.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0wvvbh24b3x2n5hgl8jfsc442alqlslvvvssx71w4bnp7y9ikyfr") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.14 (crate (name "mt-dom") (vers "0.14.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "01934ws4cahmw4zkz65js5kgwfgbgmkingf3ppwjkvm25lmcs45q") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.15 (crate (name "mt-dom") (vers "0.15.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0zdx60qjw6in07qavbmq2j0mnyn35czxmq2qvm8d7pbmhvqc2gb0") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.16 (crate (name "mt-dom") (vers "0.16.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "16r3hjxaasqrxf4s6bfqpba8y3fq2f564q4fy1im01rprml3qd1a") (features (quote (("with-measure")))) (yanked #t)))

(define-public crate-mt-dom-0.16 (crate (name "mt-dom") (vers "0.16.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1nk33g2cq25mrd3a0qibadr7lyjwj6x60igyjwbmws7zhdbs3b0w") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.17 (crate (name "mt-dom") (vers "0.17.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1n2ckq5xms2nnilqcgkpdj2ki2xfl1sf0qypgsqikxi1w0097r59") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.18 (crate (name "mt-dom") (vers "0.18.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1whiigis3gckqwxwad98s6z4hqbd1dl3aaly4w17m853i49dp41v") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.19 (crate (name "mt-dom") (vers "0.19.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0jn0va6yb50f2zsh8fc94spj6cyasv77d09z94ml1xqsq5m1qzfi") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.19 (crate (name "mt-dom") (vers "0.19.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1gagyyspbqlwbx2jb998m8a4wl0ljx1pc7n2pjd7w3qnmsq8wkkb") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.19 (crate (name "mt-dom") (vers "0.19.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0bjndkv3840i3bfpgl3rcq3fhsb2zsq1k5c7msiwx42l7jvpmn35") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.19 (crate (name "mt-dom") (vers "0.19.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0fd0xqfg9yhbcrw3ya8y92z5yp6r8ddbwm6wy0rc5cv052pi76ij") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.19 (crate (name "mt-dom") (vers "0.19.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1qpsn0hh749qgy1q0dbd243r9g53wbcsbhdydqvjxllarbaf598p") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.20 (crate (name "mt-dom") (vers "0.20.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1jx14lhfz9y4a36q857ys6c9jp7hghpc81n8d46gl9d7yp3iwlmf") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.21 (crate (name "mt-dom") (vers "0.21.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "17ni2wxx3b2q5x48dw0xwgqpdh9m8ys6av5lwdnwkhk32y5m06yc") (features (quote (("with-measure"))))))

(define-public crate-mt-dom-0.21 (crate (name "mt-dom") (vers "0.21.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0awaihbsrcl99qx02s15zhy8f8n1qijhq6920lbdpwwp5kcs5c1z")))

(define-public crate-mt-dom-0.22 (crate (name "mt-dom") (vers "0.22.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1hrg011hgg5rm4kc7r9jmwdr4hllyfq3pmnl9dz5nvihwxcngpbs")))

(define-public crate-mt-dom-0.54 (crate (name "mt-dom") (vers "0.54.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "18hswfk97yifxi8n4zxbvi9cc6i7hycklz811x27sn9dyhhcyhkl")))

(define-public crate-mt-dom-0.54 (crate (name "mt-dom") (vers "0.54.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1zkj6n70na8y3dp353v564s5ck0mjf8kp6as56dbnhsk1grdx25w")))

(define-public crate-mt-dom-0.54 (crate (name "mt-dom") (vers "0.54.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1ljs15k447xw7b2wrnkrlp89amcsllj6a4wz427bd2kdr18bbh9n")))

(define-public crate-mt-dom-0.55 (crate (name "mt-dom") (vers "0.55.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1w3c2i8z5lblpzb77747sdsgmfvsh69msr5071vhrmvsr22i1jiz")))

(define-public crate-mt-dom-0.55 (crate (name "mt-dom") (vers "0.55.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0s3px9hzd4chfvj049rmxsv33j1ndlyff4i1k4r6cx6idxd75rgx")))

(define-public crate-mt-dom-0.55 (crate (name "mt-dom") (vers "0.55.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1r76msfn1gl4wdm3wqha47m6g5qmqbf3gzggjgv8kyfn67bp3wdr")))

(define-public crate-mt-dom-0.56 (crate (name "mt-dom") (vers "0.56.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1i26c5wk0d2zcliaw0vf4q99gcz66xb63ljqfkv2qb1f43ccdic9")))

(define-public crate-mt-dom-0.56 (crate (name "mt-dom") (vers "0.56.1") (deps (list (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "11h2rzbwb09q9hrasby2l49135gbhyfpiarj98l8pbv14llns7sn")))

(define-public crate-mt-dom-0.57 (crate (name "mt-dom") (vers "0.57.0") (deps (list (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "189c8d7zjcvm3xm81qmskgwr5bhxy3xy3sbl0y3z70kc74mz2m6p")))

(define-public crate-mt-dom-0.58 (crate (name "mt-dom") (vers "0.58.0") (deps (list (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "023dxcwf7xinfrr1knj8sjgwjdq897cj9sxcib9iv2g3cjzf3c4f")))

(define-public crate-mt-dom-0.59 (crate (name "mt-dom") (vers "0.59.0") (deps (list (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1b4cg87x6wx0bhlrlq90a19wzsgqnhzvpg74ncrppzigw6yz6d0a")))

(define-public crate-mt-dom-0.59 (crate (name "mt-dom") (vers "0.59.1") (deps (list (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1i9ag7hwc9jdxmi01jnrfqwb8yxri4m2a860xrcan22axx2fjps7")))

(define-public crate-mt-dom-0.59 (crate (name "mt-dom") (vers "0.59.2") (deps (list (crate-dep (name "longest-increasing-subsequence") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0hfb9acrkq0mcli45i19s2lgyn5614j0i8b2mwln11rxq4mxv6kd")))

