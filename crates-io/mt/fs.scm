(define-module (crates-io mt fs) #:use-module (crates-io))

(define-public crate-mtfs-0.0.1 (crate (name "mtfs") (vers "0.0.1") (deps (list (crate-dep (name "fuse") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0wqmcv00zgbfbf9v3hmriw8404n2419ny4k6sqh372c7ndhhxv02")))

