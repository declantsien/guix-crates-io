(define-module (crates-io mt zi) #:use-module (crates-io))

(define-public crate-mtzip-0.1 (crate (name "mtzip") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "03xfk5zqlkbb70gkjx0nn2ywwrf2gwabh8ig9npsjq4inwzy8zr3")))

(define-public crate-mtzip-0.1 (crate (name "mtzip") (vers "0.1.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hwzqbarygz0ck2k7lr1kscnddi5cc0w047agjs0lyfmszppw506")))

(define-public crate-mtzip-0.2 (crate (name "mtzip") (vers "0.2.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kmpmpjaksa5w58xcj564fikd961aiznafsk2cv0fsv56z6kr8fk")))

(define-public crate-mtzip-0.2 (crate (name "mtzip") (vers "0.2.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ww2qxqqvrzkvzw5c0z8zmznis58zcbkzdhylh78hs2mb8w5ms9y")))

(define-public crate-mtzip-0.3 (crate (name "mtzip") (vers "0.3.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "13ryxlpwafjvi549rpp190zn0872f7g7vkfb0k8y045lsvvnsryi")))

(define-public crate-mtzip-0.4 (crate (name "mtzip") (vers "0.4.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v4zq2xg4jyykdirkvgvciv80nwqdzc8prhydl2h581mfz7pl0b9")))

(define-public crate-mtzip-0.4 (crate (name "mtzip") (vers "0.4.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z8r4c1hshfyk15nqqm1ir8xyda3jyc8zk07qiyzxxbaplkyz2jw")))

(define-public crate-mtzip-0.5 (crate (name "mtzip") (vers "0.5.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s4cjsmqy0d7fp4bz0yla1jinxgpgim8mk09qnwn7v1jp9lvjyrb")))

(define-public crate-mtzip-0.6 (crate (name "mtzip") (vers "0.6.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.25") (optional #t) (default-features #t) (kind 0)))) (hash "073cns15xl0j0scvvyxlfja2qbmn4axg0yc88vkcvkq0qs0dqml5") (features (quote (("default" "auto-threading") ("auto-threading" "sysinfo"))))))

(define-public crate-mtzip-0.6 (crate (name "mtzip") (vers "0.6.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.25") (optional #t) (default-features #t) (kind 0)))) (hash "0xwmaxhr07xwv5diqp9ncpdabwx5j8gcdrbhh4hr21nnjq9n5kk6") (features (quote (("default" "auto-threading") ("auto-threading" "sysinfo"))))))

(define-public crate-mtzip-0.7 (crate (name "mtzip") (vers "0.7.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x5c7px2cfpzgkybkgd07gbwqnnnc1p761dzzbzf629m4y3kbhcf")))

(define-public crate-mtzip-0.7 (crate (name "mtzip") (vers "0.7.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ywi17qyfzv7y22pwdaj4s48vsarr91n3ns9gvzwy1r9rwsi4k0m") (yanked #t)))

(define-public crate-mtzip-0.7 (crate (name "mtzip") (vers "0.7.2") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "16j6ny1qf3n7d13z9jrmj099b0zwqdvdj5r9aga7l0izs39fk8g1")))

(define-public crate-mtzip-0.7 (crate (name "mtzip") (vers "0.7.3") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1n33lx97w3iy673c1gpmh98dglb2j51f115yh4kq4xw1bg0ljdmk")))

(define-public crate-mtzip-1 (crate (name "mtzip") (vers "1.0.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cc6vd1g89cv6fhm60ras91fcpc0nrvrd1h6m0mk041hlarbf4dy")))

(define-public crate-mtzip-1 (crate (name "mtzip") (vers "1.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m3z54b0ck6p0k4wjw5jy0gqjfxbn34y6pvj9yzzzrrba62is89x")))

(define-public crate-mtzip-1 (crate (name "mtzip") (vers "1.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "18rp02xmrsxcnwc745h8yn3dm7sfg3zg7q8jnmjnyxinl627afya")))

(define-public crate-mtzip-1 (crate (name "mtzip") (vers "1.3.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pckbq59l5s4bznxj1737ikals9xg8d6dkr1qwmflvj8z8xz8n43")))

(define-public crate-mtzip-2 (crate (name "mtzip") (vers "2.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (kind 0)))) (hash "0mchjn51zcj549kx8ldygk1x1glxcq6ammllqs3fqddcawlc9pwr") (features (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend"))))))

(define-public crate-mtzip-2 (crate (name "mtzip") (vers "2.0.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (kind 0)))) (hash "0pfcw9v9hnrh5cck67vzsd3hnbid3wk17a2gnn3hmvwsjgcm415h") (features (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend"))))))

(define-public crate-mtzip-3 (crate (name "mtzip") (vers "3.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "176q355r22y8j8i8hskk7fak4vxwnc4xlf5r32sf6mf4jni3xj1s") (features (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend")))) (yanked #t) (v 2) (features2 (quote (("rayon" "dep:rayon"))))))

(define-public crate-mtzip-3 (crate (name "mtzip") (vers "3.0.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zm86r07naswaxbq1hg0i3wjz52iqibmkxy2jifmn47wc07iwd5y") (features (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend")))) (yanked #t) (v 2) (features2 (quote (("rayon" "dep:rayon"))))))

(define-public crate-mtzip-3 (crate (name "mtzip") (vers "3.0.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "0cg6nxycanshncgw0v0hbl5kc7fdc11zcxdbsb6cbjwjs4yr3n5l") (features (quote (("zlib" "flate2/zlib") ("rust_backend" "flate2/rust_backend") ("default" "rust_backend")))) (v 2) (features2 (quote (("rayon" "dep:rayon"))))))

