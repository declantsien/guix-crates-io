(define-module (crates-io mt l-) #:use-module (crates-io))

(define-public crate-mtl-info-0.2 (crate (name "mtl-info") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "~1") (default-features #t) (kind 0)))) (hash "1b9y4xlb03x4jvnn0bzvm13m7z4y46cym661n9yy8dfwfaymbsfm")))

