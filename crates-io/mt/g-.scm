(define-module (crates-io mt g-) #:use-module (crates-io))

(define-public crate-mtg-deck-stats-0.1 (crate (name "mtg-deck-stats") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0ffq7k2iz2qhs04a1xa7hi1qq9vi3j06v2gfivzffni6d6ay6066")))

