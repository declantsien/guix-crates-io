(define-module (crates-io lo du) #:use-module (crates-io))

(define-public crate-lodust-0.1 (crate (name "lodust") (vers "0.1.0") (hash "1dsvwj2svfrixqmqhv1zi6r8hmn6140a75jflwp6klf0vgxzhggd")))

(define-public crate-lodust-0.1 (crate (name "lodust") (vers "0.1.1") (hash "1k5ijp35wvc0326hik1rzxgpmfag4x7smh8jm2n95xb9q916w1w0") (features (quote (("string") ("default" "string"))))))

(define-public crate-lodust-0.1 (crate (name "lodust") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "08bz73lyi2gbzry6zwzn3d0vcd2q9bgqxfl5rm63bklyfydy6li8") (features (quote (("string") ("default" "string"))))))

