(define-module (crates-io lo af) #:use-module (crates-io))

(define-public crate-loaf-0.1 (crate (name "loaf") (vers "0.1.0-alpha1") (hash "1c3c74iwph4h1ilk1dyrd2pdamkhc28239xf59y67p0qx0rjj0ry")))

(define-public crate-loaf-0.1 (crate (name "loaf") (vers "0.1.0-alpha2") (hash "1xhw6cislqi2vpgjficf2c2bh1jh0l38bgla89ifvscsbshnp76v") (features (quote (("nightly") ("default") ("alloc"))))))

(define-public crate-loaf-0.1 (crate (name "loaf") (vers "0.1.0-alpha3") (hash "1k10n0r4b07i3a3y5yjcjyfrz9hk8jfn3k600l64bsniy4505y4z") (features (quote (("nightly") ("default") ("alloc"))))))

(define-public crate-loaf-0.1 (crate (name "loaf") (vers "0.1.0-alpha4") (hash "1mybp5gxbzil5467s8spbbhd287yw0l3jyknj9ddmgb10b5im5j4") (features (quote (("nightly") ("default") ("alloc"))))))

(define-public crate-loaf-0.1 (crate (name "loaf") (vers "0.1.0-alpha6") (hash "0v2ibrzcilkjc82q9hxr1wzyycfb8skphv98sfknf7s5cx1j6hvc") (features (quote (("nightly") ("default") ("alloc"))))))

(define-public crate-loaf-0.2 (crate (name "loaf") (vers "0.2.0-alpha2") (hash "1kgnmwgjr9vasmvmjyna93438mrclb0yfp4b4b3x6gs5lw66nnq3") (features (quote (("default") ("alloc"))))))

