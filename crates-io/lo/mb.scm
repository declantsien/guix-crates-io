(define-module (crates-io lo mb) #:use-module (crates-io))

(define-public crate-lombok-0.1 (crate (name "lombok") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0a6zyg8nl73fr17yzqqcijni3znff0xyazsjzj3k1a4bclnwaybm")))

(define-public crate-lombok-0.1 (crate (name "lombok") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pjh9hk3l4dk51kknimhfjck4dxymg22m7zx64w0sj38d14n5zlk")))

(define-public crate-lombok-0.2 (crate (name "lombok") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "161igps4s94xmmg3dzvsisagqh26129l7jxbsvz2cha7w2pfnzq2")))

(define-public crate-lombok-0.3 (crate (name "lombok") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pn75nrhpygbjayxx8chf1lj1brp6al5bfrp4akivaw422dkr7fv")))

(define-public crate-lombok-0.3 (crate (name "lombok") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q4bv0qxsd4jwcpjlswcj6nzf4x9bn2a37nd746p1wf8dafln62x")))

(define-public crate-lombok-0.3 (crate (name "lombok") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bl983dhfigq07p8akgbrcpbg6idyjr9dys5wvixvvjwbc0bv5ml")))

(define-public crate-lombok-0.3 (crate (name "lombok") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sldg5dn2pqz9qvp749vs0acw7cn187zgiyyxkzjnql728nn91si")))

(define-public crate-lombok-0.4 (crate (name "lombok") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xxxbba65v56wiwdw0bk5jbgyg0lgr2kgp1xwqh79pb9gk4yrlqg")))

