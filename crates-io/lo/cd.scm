(define-module (crates-io lo cd) #:use-module (crates-io))

(define-public crate-locdev-0.1 (crate (name "locdev") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04mqcy8chfvr5s56jl95hxr78kjprsfa5kd67ilgmy7c3y3pi738")))

(define-public crate-locdev-0.1 (crate (name "locdev") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "12i8gb3lwl75aidxx2gvx4j5l8r8rb018jwgflmxzddpis2idsz1")))

(define-public crate-locdev-0.1 (crate (name "locdev") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0albacx3xv6w7s32p2165ffwjf2f8fhkcbrkx1xdwq3wdp2gfxl7")))

