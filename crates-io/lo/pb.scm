(define-module (crates-io lo pb) #:use-module (crates-io))

(define-public crate-lopbox-0.1 (crate (name "lopbox") (vers "0.1.1") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0ki7gs0pz35cw4301qrs73p6fgpr6lylq1kl3xxjmr2rsp61iza8")))

