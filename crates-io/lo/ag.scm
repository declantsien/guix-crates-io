(define-module (crates-io lo ag) #:use-module (crates-io))

(define-public crate-loago-0.1 (crate (name "loago") (vers "0.1.0") (hash "1ngqq9p5fm7zsfa4lsk3j8z9wy7kxkdpgiy8sl2769k7pksa456r")))

(define-public crate-loago-0.2 (crate (name "loago") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1rs09d8ab0fifnwvrfmrmzdslpkknw7c5hvgrjl6aaa1v8izb758") (yanked #t)))

(define-public crate-loago-0.2 (crate (name "loago") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0mnn6vf3n85gysldb0pq3naln0bzcryivj67w9zl675zny5qcb76")))

(define-public crate-loago-0.3 (crate (name "loago") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "05myn5s2917gwgbxgx6lshfj4i5scilr6qy08al5jig9i4li65kn")))

