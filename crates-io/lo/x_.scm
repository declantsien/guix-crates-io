(define-module (crates-io lo x_) #:use-module (crates-io))

(define-public crate-lox_one-0.1 (crate (name "lox_one") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "parameterized") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "parse-display") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0afgljhwh17cihdfhwbvv0vf0rz30l0vjv02m58b6c32fwz20c9c")))

(define-public crate-lox_utils-0.1 (crate (name "lox_utils") (vers "0.1.0") (deps (list (crate-dep (name "lox-library") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1lzaab4zvybsz70wgp5zbvkrq6sndd6dx3mgklnh5wp8f1vry6bk") (features (quote (("full")))) (rust-version "1.65")))

