(define-module (crates-io lo ir) #:use-module (crates-io))

(define-public crate-loirc-0.1 (crate (name "loirc") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0j8rqgqykbglj8m86qvrv6cyx4rjrhh4phhrpqsik3fbpnjh822v")))

(define-public crate-loirc-0.1 (crate (name "loirc") (vers "0.1.1") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1xrg8slb3qinpwbqjipm1c289vc07la23lli0bvc5577df59b09c")))

(define-public crate-loirc-0.1 (crate (name "loirc") (vers "0.1.2") (deps (list (crate-dep (name "time") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "05p5vff6vgscwnyb7371fmv34zln9mg7qz5w1i68ia8p4341ipj1")))

(define-public crate-loirc-0.1 (crate (name "loirc") (vers "0.1.3") (deps (list (crate-dep (name "time") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "1qkq6vsw9xkz6p8d8a56f9pi3xpvlx58ldff7350jrm0rv6grshd")))

(define-public crate-loirc-0.1 (crate (name "loirc") (vers "0.1.4") (deps (list (crate-dep (name "encoding") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0l7cd584bbrbxcxb7j4n12mkg744wbq7a82vjzw8vmsvxnr0d6ij")))

(define-public crate-loirc-0.2 (crate (name "loirc") (vers "0.2.0") (deps (list (crate-dep (name "encoding") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "1jqr6ak3rgmwxvdmf0fng54zh1k86adxg23lld0f51d0ln36ji90")))

