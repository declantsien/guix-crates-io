(define-module (crates-io lo ve) #:use-module (crates-io))

(define-public crate-love-0.2 (crate (name "love") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "platform-lp") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "version-lp") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "1rwrird1nj77xnsb90sh027lk9clylgikyzn6j8978nrygrr1riv")))

(define-public crate-love-fetch-1 (crate (name "love-fetch") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.16.5") (kind 0)) (crate-dep (name "whoami") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0bxw6yjcbl0d1ch7cpqfb8scdkn1xp6rcad8bznb428x3imndld2")))

(define-public crate-love-fetch-1 (crate (name "love-fetch") (vers "1.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.16.5") (kind 0)) (crate-dep (name "whoami") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "08ng62c4dkhxl6wwaxzj4zzniasp9br9d48h020l4hjsn0x1n5za")))

(define-public crate-love-fetch-1 (crate (name "love-fetch") (vers "1.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.16.5") (kind 0)) (crate-dep (name "whoami") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1j3x80byvkgixmznk5p0b4m64mjbzwkahhncgzycnlsjirywqdiq")))

(define-public crate-love-fetch-1 (crate (name "love-fetch") (vers "1.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.16.5") (kind 0)) (crate-dep (name "whoami") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "197qy1ghc6rkn7bx3pbri8hkf694h502zkn1cvb0fni0y9dk86p5")))

(define-public crate-love-on-0.0.1 (crate (name "love-on") (vers "0.0.1") (hash "1rj5nyms6dlc8kbzq3z4cc4m2xq1370hvc9xp982y099210ivj8p")))

(define-public crate-love_rust-0.1 (crate (name "love_rust") (vers "0.1.0") (hash "1i1smh9mb9jdnkwdsk5r1pqdfdva3yp1ja7dra9w0nbl6ck56wba")))

(define-public crate-love_rust-0.2 (crate (name "love_rust") (vers "0.2.0") (hash "1yvxpqb4v3q3m5qnk7dk3phinzm2jwd402xcx0qb8zq26mfchi7w")))

(define-public crate-love_rust-0.2 (crate (name "love_rust") (vers "0.2.1") (hash "0788nzbz7lry644s7kfsy02kyg8rirxpqck47v14yxi7nf21vxi3")))

(define-public crate-love_rust-0.2 (crate (name "love_rust") (vers "0.2.2") (hash "04428c3j8pqr17847fsyk2cgnj18v1qzkv8ndvvnfanyy8vaa4h2")))

(define-public crate-love_rust-0.3 (crate (name "love_rust") (vers "0.3.0") (hash "11xqs25rrckyiqjcmdc0gdf90lf9mcxylrg11l7rncw8m4bjjsbf")))

(define-public crate-lovecraft-0.1 (crate (name "lovecraft") (vers "0.1.0") (hash "0pjmnsfwn4dvmwrdwqs2wy3nbakdad0b8lg29qa9d4b9hsbwrk9b")))

(define-public crate-lovecraft-0.2 (crate (name "lovecraft") (vers "0.2.0") (hash "15vknp54i2nprn4njhbywrk7ll4fwghksbmwxxj7prk5shzqhrbq")))

(define-public crate-lovely_env_logger-0.1 (crate (name "lovely_env_logger") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "17iidk35as6vlxpdawh6na4pn5pw6hgncbgsvbzjy2lddcsz9x7i")))

(define-public crate-lovely_env_logger-0.5 (crate (name "lovely_env_logger") (vers "0.5.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0x1d4xq17j1q2l8la42jb6bbzw3djsh0nwzdxzc35wi1qa6gycr1")))

(define-public crate-lovely_env_logger-0.6 (crate (name "lovely_env_logger") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (features (quote ("auto-color"))) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "19k6av04na4g45zjg189186aq80r5zf2jqf6jil91l38jr2ni3g9") (features (quote (("regex" "env_logger/regex") ("humantime" "env_logger/humantime") ("default" "humantime" "reltime" "regex")))) (v 2) (features2 (quote (("reltime" "dep:chrono"))))))

(define-public crate-lovely_env_logger-0.6 (crate (name "lovely_env_logger") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11") (features (quote ("auto-color"))) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "00hmdlvx23glw96l8fc97aghimv2ph5md2gssszbxdmb6n7n34kj") (features (quote (("regex" "env_logger/regex") ("humantime" "env_logger/humantime") ("default" "humantime" "reltime" "regex")))) (v 2) (features2 (quote (("reltime" "dep:chrono"))))))

(define-public crate-lovelypeach_art_test-0.1 (crate (name "lovelypeach_art_test") (vers "0.1.0") (hash "1d1260ii70wjxg5rzqwk478pcr313cmpxs6g8kahr0m7nqwxmap0") (yanked #t)))

(define-public crate-lovesay-0.1 (crate (name "lovesay") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1q08kiw7c3h0gjz12g8ky1s82d9bsfxh83d53xkbvbngbs3n0c3p")))

(define-public crate-lovesay-0.1 (crate (name "lovesay") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1bj5cpap24xqp4fsilj5si94p2mn7zfasihjclm4jng7gg3gwp1c")))

(define-public crate-lovesay-0.2 (crate (name "lovesay") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1r6d5c9r5122cyqg8pa316lkglrl78671gw2424kp187f21ajf5b")))

(define-public crate-lovesay-0.3 (crate (name "lovesay") (vers "0.3.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1ppk438rzmgf6f0zda2a17ij94irwxc9dh2zjl3q2gbrh0q7c6h6")))

(define-public crate-lovesay-0.4 (crate (name "lovesay") (vers "0.4.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1dl5f5snvh5mhld72lv2zc9bpld07n4chbv15grwkh3ln957pfgj")))

(define-public crate-lovesay-0.4 (crate (name "lovesay") (vers "0.4.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1rzcc27ifcm3fgvg8zrw4g2mpbg066jlzpjn1b6r3fgm3xyns908")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0aicbnnk4svd0ifl8qqfd542mzs8m9ssql7wqlx9h2cc6591ni5q")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "00ssksrk4s5wa42r92rnp8ybk7gx3aaxbdcf4d8b77x0y9hylvgp")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "12c1x7kr3zpnv3zgwdn7x4famsj3wlb1amfy7km1hqmf3d1k6k3z")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1zagar11l9raipkh8l744wrydzdg4zra3g01mkravp4cwgv72g4l")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1b1g7d18sjhafiidl0xqzzjp3s2n7phg8z5pck55gzkrbar8w29h")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.5") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0j56p50552ffs32pk2rr5yvaa0g3wyhk9lizsywj7nf3xz85zj4y")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.6") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1vnz4g62mg24m8slkhrkcgfy0njx2l9amhfpsq7vmvcs8byfwhf1")))

(define-public crate-lovesay-0.5 (crate (name "lovesay") (vers "0.5.7") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0nimlkbvb4m6lnw7pj5g5bnb3c3pivhvnwy1zwdxdxr7flf7qvcv")))

(define-public crate-lovesay-0.6 (crate (name "lovesay") (vers "0.6.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.9") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "kolorz") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "076myn8j348xsksjzdjmdngzkhazfrndk5f7rshrabaagm5h8768")))

(define-public crate-loveseat-0.0.0 (crate (name "loveseat") (vers "0.0.0") (hash "15l24vnni2mjpkyyf19mijihgdipg8lwga5m04kpkm88qn472jsr")))

(define-public crate-lovesegfault-0.1 (crate (name "lovesegfault") (vers "0.1.0") (hash "0h95i5kjhvr133dizv4xwzbv5r6p33nyqrgqjd60f920g1nwr61z")))

(define-public crate-lovesense-0.1 (crate (name "lovesense") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0azldl5g3gq2zikfwa6zy78zqmjyf3nzcgf4npwz986miwm1pza4")))

