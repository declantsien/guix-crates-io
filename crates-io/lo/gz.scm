(define-module (crates-io lo gz) #:use-module (crates-io))

(define-public crate-logz-0.1 (crate (name "logz") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "08wfaqcyac61v7krm27hy3fzxgi48km1mlcgm7ac610dp77rmaxz")))

(define-public crate-logz-0.1 (crate (name "logz") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "15r52914y6vs1nyn52qkslpl8cmh49h7bwqcags9sl2czcmij1kp")))

(define-public crate-logz-0.1 (crate (name "logz") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "07yp9cl0mxndqw4ijlz3pmh912nwy144n07fjbn76l0kbfkfjppz")))

