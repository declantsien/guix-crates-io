(define-module (crates-io lo uv) #:use-module (crates-io))

(define-public crate-louvre-0.1 (crate (name "louvre") (vers "0.1.0") (hash "1zyya1gkm00m2fc9lx96jd4421gr9bcpn52xky6bnws516q1c9n4") (yanked #t)))

(define-public crate-louvre-0.1 (crate (name "louvre") (vers "0.1.1") (hash "010jl3gsnzwfr0x1jr1r98h37v4mx5lc6mqy7zr6073ivn8344rm") (yanked #t)))

(define-public crate-louvre-0.1 (crate (name "louvre") (vers "0.1.2") (hash "1jvz361b60mfahqm6fdnl4brx01l40mkmgc608xzkiqaa1i66783")))

(define-public crate-louvre-0.2 (crate (name "louvre") (vers "0.2.0") (deps (list (crate-dep (name "gloo-utils") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.69") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("HtmlCanvasElement" "CanvasRenderingContext2d" "DomRect" "Element" "Window" "HtmlInputElement" "Event" "EventTarget" "MouseEvent" "KeyboardEvent" "InputEvent"))) (optional #t) (default-features #t) (kind 0)))) (hash "1vcbm1jwjpix0k9nd5yd1zzi6nicsyjfdpfp4l4s7fcwkg1c8plw") (features (quote (("default")))) (v 2) (features2 (quote (("html" "dep:web-sys" "dep:gloo-utils" "dep:js-sys" "dep:wasm-bindgen"))))))

