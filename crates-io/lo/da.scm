(define-module (crates-io lo da) #:use-module (crates-io))

(define-public crate-lodash-0.1 (crate (name "lodash") (vers "0.1.0") (hash "0qxyq6w6mli3v7py0g62d5mmkbcrxfhvghzsphnkz750cx9lw725") (yanked #t)))

(define-public crate-lodash-rs-0.1 (crate (name "lodash-rs") (vers "0.1.0") (hash "0wpj4byq4ppvnqjrivm0acrzx2al169wgm0km5h6j2v4jwzdhz2p")))

(define-public crate-lodash-rs-0.1 (crate (name "lodash-rs") (vers "0.1.1") (hash "02jsbzg363mgjbkyj4hjwdmacp2563s18scdl3x71c5ba5l7c89d")))

(define-public crate-lodash_rust-0.1 (crate (name "lodash_rust") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1l8qxs98f81lj4d0h4p0bpjj3kqjg80wrk96kiasi046wb7bqa5g")))

(define-public crate-lodash_rust-0.1 (crate (name "lodash_rust") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0gsgpx9yiwqis9wbl87xmcilxs5gxb4iscdq2bdqa60zpky09csk")))

(define-public crate-lodash_rust-0.1 (crate (name "lodash_rust") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1m7vx0vsiclzrvrra1cfzg69vbx75p87gxmqv335fqaahv1klavi")))

