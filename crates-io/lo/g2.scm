(define-module (crates-io lo g2) #:use-module (crates-io))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1p5p48b6qkwz2jni8siqvac2r1dvaj1d8kini19chkgicip0b4im")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0xsgwnjgh7q3p7chdqrx32afdxwh65y0hsrdwicih5z7j3yyx4k5")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1gksja7gy51g0nmz49jrl0zkxap819c1g2bw231hixswwamg9g6k")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1417z2b9n8ashxbbgx04yym7hcqd2bm0iy604jiir25fgzjpsd9x")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "15flkh4c8bggwlhmi9lywlga43qgjxycxpcdycr49j0zzk7r8fx1")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "09srd8fv3hgb4y15x4rndlvkjyifaif4dc63aybxy4k2fmz59rs3")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0ig540i792bzn8ir4hq56smk95wcf1zy5p0mba75834867k7wyam")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1bqhpvvpx70j2bvx8fnxlpv6lz0a033m567gqgsidi50ymm3vv91")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "015jiww32x366pb5n4kd00w4kdy0f6fnpk7gj7k0gxw8b07769w5")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.9") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "06c0c86v26s6c87yx6395bb4sjs41fgj0g02xy3zbl20mn0fhz27")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.10") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0bkww9d8lq53dv52jcqsg3xyvcqimj816rl7axh9hvnbqsb4ihwd")))

(define-public crate-log2-0.1 (crate (name "log2") (vers "0.1.11") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0rhlnxab6pjlsav06jvazd42nh8jvakc43w9i78m7jj0xik6lrb3")))

(define-public crate-log2fix-0.1 (crate (name "log2fix") (vers "0.1.0") (hash "0vrpmm077iwa18ld0zcwzrszfxc01qvwj4l0r7ijki9fsa6y8biz")))

(define-public crate-log2graph-0.1 (crate (name "log2graph") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("macros" "time"))) (default-features #t) (kind 0)))) (hash "0f83fq4mji48h5bykk118mwz8v9k4skzslsp3vl1mmy0l5rrykcr")))

(define-public crate-log2graph-0.1 (crate (name "log2graph") (vers "0.1.1") (deps (list (crate-dep (name "petgraph") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "15wh0rr3yvr12w5r39g452yqs08hficbzi7sxpw2gw5qyfw7ak4m")))

