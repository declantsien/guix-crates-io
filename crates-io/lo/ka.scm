(define-module (crates-io lo ka) #:use-module (crates-io))

(define-public crate-lokacore-0.0.1 (crate (name "lokacore") (vers "0.0.1") (hash "07qsx15ipabvzmyjbkipj7mnrp4sdjjxg6bk9hrdckb5raa3j3xl")))

(define-public crate-lokacore-0.0.2 (crate (name "lokacore") (vers "0.0.2") (hash "1cn698swwjxfffshyg8vr420dgd0f2c0gc5k5i4lzq4x8i5ip1j2")))

(define-public crate-lokacore-0.1 (crate (name "lokacore") (vers "0.1.0") (hash "0bvmrmm0y4vksf2qnfvn3hvn66sahdijrpx7qbc6l9x4alxp34zd")))

(define-public crate-lokacore-0.1 (crate (name "lokacore") (vers "0.1.1") (hash "1b762909az875832kkrc9ynia9gjzyhc33s8ii2wzr8xpqh4h6vp")))

(define-public crate-lokacore-0.1 (crate (name "lokacore") (vers "0.1.2") (hash "12sm7ig577wri73qddwgsn6m885qfncs8kb8vzbl2zmb33cf03np")))

(define-public crate-lokacore-0.1 (crate (name "lokacore") (vers "0.1.3") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (target "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1q4bxkpw9ln721k2wvf8qsqzya6qjpjpj4rpvcqmck6nz2mwgmph") (features (quote (("default" "serde"))))))

(define-public crate-lokacore-0.1 (crate (name "lokacore") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (target "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "11bxrwjkb1k4jfw1x3cl637rn7ysr6kwvjvrf11miajpz9i6hkbn") (features (quote (("default" "serde"))))))

(define-public crate-lokacore-0.1 (crate (name "lokacore") (vers "0.1.5") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (target "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "064jmbgassm1ss7vb2k221m1kvgp6ddy9z743ibgw4qwapn5svmv") (features (quote (("default"))))))

(define-public crate-lokacore-0.2 (crate (name "lokacore") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (target "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0r4wklgw30nnmm0qcrxdinnijwqpsm1yyzgvx4wzr022sg5kl41a") (features (quote (("default"))))))

(define-public crate-lokacore-0.3 (crate (name "lokacore") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (target "cfg(not(all(target_feature = \"sse\", target_feature = \"sse2\")))") (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0rslkh4i9bih35qa3k67j3l51mjiv8pqfm9lfswhj6xnba2ricgf") (features (quote (("extern_crate_alloc" "bytemuck/extern_crate_alloc") ("default"))))))

(define-public crate-lokaproc-0.0.0 (crate (name "lokaproc") (vers "0.0.0") (deps (list (crate-dep (name "lokacore") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f3ma7xzm7j839j0sb9l9xjy0xjy232h1r7bh7s84pnhyiny1xh7")))

