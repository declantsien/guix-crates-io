(define-module (crates-io lo ga) #:use-module (crates-io))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "1akwwb3ghgd4kj7bwr6cbz0d7vvs9vwr9ijkbm0nbqvv2dqb08b6")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "13dvw248vqi2cps7ryfcng11xq0sb4srz0vdn38ppkar0b44r97a")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "06m2fgscm891h0xr2j0783dfjwp5h3xxkfggwfska5408gv0bk6k")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "11j5bgjmhbc4z257qwmpy810lk7h49vb5i33ip2yyvidx4l3b62c")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "02xlimmmzmjjrmksd59qr608vwwinlbim3il5sl4fxfi1a3xil2f")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1zg2kk8adda02b64yg8bcvz8jlg4nlbh29xcxmlyn7v6mh665rsl")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1h8w70man3hsiyrv0sg7q7s3mr18ghh74qfxidgzwxyv6dkh752v")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1gblj3qlp882x0szmlgwdzviw2p54c6cfpgqksv059rgajm8qkpw")))

(define-public crate-loga-0.1 (crate (name "loga") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1qnwwq5lc6rd116k6i8mbqd0c6sfpkkiis06wrvz5czj1na93kmr") (yanked #t)))

(define-public crate-loga-0.2 (crate (name "loga") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1jzwjk8v428prcf5hc05b3qi8q2q01crl3v69l28ig2i94v1a2wy")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1064rrihmni7p0rkr2hkxnvnc8mmi6ic17gwnzk4rhw1387g4c35")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "0j5h0dr0vq3yk0lqlhnlak5l1qi5zwqv3aip8glh1rv9b0szc8id")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1j189idkjn23kj5jxjwlmgxhxk7n2ld4aaszqww6jlala80l9psz")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.3") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1qm0f41w4magl9c8r5gzc8cpw49a22r4j0vhk7dm432qgspnf0n3")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.4") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "01plah48r9qnm1wkqmb36730idgp1l37lgiikrxdh6xgg7r3y5dg")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.5") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "015yrkpc447swblw3ihs0dzh02fphbbvb5fmsdsgzb3xn1axrns7")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.6") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "08ri9lj1z6bpa3ckqm5qbk53m1b0nfa2j0yyrx0zjcn6kd1jh5pb")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.7") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "0cby08sf2iwp03i9820hdx09nvb8ya4ybfq5hp4s7hqimcm3v3k1")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.8") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1h9rc387sh38llf29w07iiawzlfjvfd7y2jvl4gnyjvgy46vvigx")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.9") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "03wbl31sa0fv4a047ryak8w5wnvqj4rcibx270y1f6dq3fk2zchr")))

(define-public crate-loga-0.3 (crate (name "loga") (vers "0.3.10") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "12pc6pjwdkh9146bvwphrd7hzakbfda8fzpgr3128mi9bfl1czym")))

(define-public crate-loga-0.4 (crate (name "loga") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "0x3bj6f25bbdzf8xlhif2hgsfsanxszpcgw9plvjf92mllx84k0z")))

(define-public crate-logaddexp-0.1 (crate (name "logaddexp") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fjvn9awilqkrmb39x737v6pv14m9g5lsqi9ygrnh3glv2b6zp32")))

(define-public crate-logaddexp-0.1 (crate (name "logaddexp") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kg2pxfn97jb2l74kh9xa8cz4g3n7ci58bcxix4swba4szm8z595")))

(define-public crate-logaddexp-0.1 (crate (name "logaddexp") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zmp2g2bh6v4l62zvp2wd2lxx4zqvl3hpxdfbddpskip98mybkmq")))

(define-public crate-logaddexp-0.1 (crate (name "logaddexp") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cyl5f98hwd5bmra771krrgy6rw8kvnpj9b4cgq94k49mmxrqns1")))

(define-public crate-logan-0.0.0 (crate (name "logan") (vers "0.0.0") (deps (list (crate-dep (name "derive_more") (req "^0.9") (default-features #t) (kind 0)))) (hash "0qg8rwsdzjiwaczgi0gs96pp4v2rzab38dxwg2lyvjj9nn53jwbb")))

(define-public crate-logan-0.0.1 (crate (name "logan") (vers "0.0.1") (deps (list (crate-dep (name "derive_more") (req "^0.9") (default-features #t) (kind 0)))) (hash "0k1z7jcf7dda3g9vgfj934cm517r4jhvlhj511k11na41hb5gdx7")))

(define-public crate-logappend-0.1 (crate (name "logappend") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("clock"))) (kind 0)) (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ylgh1r83fx55kk0h6bgan02vcrmi8d6g1i0fila5qszc9mzh5y1")))

(define-public crate-logappend-0.2 (crate (name "logappend") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("clock"))) (kind 0)) (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10srliavxrdgv7afydhlf5g1d7fmbljxydx0yp62hg6rmp0kf4ji")))

(define-public crate-logappend-0.2 (crate (name "logappend") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("clock"))) (kind 0)) (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ip5j93i8y2jidhr2inbjzmhaz060snas0j9dfhdfqraqyp16fsk")))

(define-public crate-logappend-0.3 (crate (name "logappend") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("clock"))) (kind 0)) (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0ph9b8kjhh6292vnvvizlv2j4yjn861fwz8q8wc6nwk0fbwwpp2g")))

(define-public crate-logappend-0.3 (crate (name "logappend") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("clock"))) (kind 0)) (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1v2ph992pj7c6b78f99rky70qixbxpj3y554v4saafjk8sql01rw")))

