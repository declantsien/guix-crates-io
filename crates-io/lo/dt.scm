(define-module (crates-io lo dt) #:use-module (crates-io))

(define-public crate-lodtree-0.1 (crate (name "lodtree") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.30") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "0m5i0pq1990ww6bfn2yb629kwaj2sgdjgzxl92wll3smka6qr9g3")))

(define-public crate-lodtree-0.1 (crate (name "lodtree") (vers "0.1.1") (deps (list (crate-dep (name "glium") (req "^0.30") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "0ph40n0k8qrg3wl3ikvfjy5qg2wyi3wx3kxxgfj4pkyygw4i30bi")))

(define-public crate-lodtree-0.1 (crate (name "lodtree") (vers "0.1.2") (deps (list (crate-dep (name "glium") (req "^0.30") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "1w3cmixm77gc9mn7244b4lq5x25bicnbsncra5wr1ywg3byrybfn")))

(define-public crate-lodtree-0.1 (crate (name "lodtree") (vers "0.1.3") (deps (list (crate-dep (name "glium") (req "^0.30") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "02kdl3l30mdizfw5983vn9545z0dshi69bk9vz8pnq1xk4cmvhql")))

(define-public crate-lodtree-0.1 (crate (name "lodtree") (vers "0.1.4") (deps (list (crate-dep (name "glium") (req "^0.30") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "0kkd3mjp6yz1j7s3rkmpk4mggi67qwz00vzpkc9ddkzk4cmlgb3b")))

