(define-module (crates-io lo lo) #:use-module (crates-io))

(define-public crate-lolog-0.1 (crate (name "lolog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0l61d6arq018p8a54gcmy6m9hi57zg895j1w6jxknq3aj8wl2f5l")))

(define-public crate-lolog-0.1 (crate (name "lolog") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "01daw12nwmgdpmdidjyhzqni8b11jsj47bajzapkzgx6b1rw432n")))

(define-public crate-lolog-0.1 (crate (name "lolog") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "17dmc8ylv5yrcc3fpkr16xclq6qyaxd7inajrsmb4pkvd2ss7mal")))

