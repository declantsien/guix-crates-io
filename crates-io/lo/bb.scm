(define-module (crates-io lo bb) #:use-module (crates-io))

(define-public crate-lobby-0.0.1 (crate (name "lobby") (vers "0.0.1") (hash "07qglc33v19mb3q5gydnprnvgj3g17mgisiisrry28la4vbzip20")))

(define-public crate-lobby-queue-0.1 (crate (name "lobby-queue") (vers "0.1.0") (hash "1p9xs99kcicrpwlkq7y033w2f9whjpjs2kzznzskg0ka9m858y7w")))

(define-public crate-lobby-queue-0.2 (crate (name "lobby-queue") (vers "0.2.0") (hash "1m631qhnlrbs9av069w87r6iqcb6qy3imxwc6z115h2421vb7bgs")))

