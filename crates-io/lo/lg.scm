(define-module (crates-io lo lg) #:use-module (crates-io))

(define-public crate-lolgrep-1 (crate (name "lolgrep") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "08r5ayrlxzhlghybz7v3gcgpsbfkdr6j19cwhrl0spbqljd4hv7p")))

(define-public crate-lolgrep-1 (crate (name "lolgrep") (vers "1.5.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0zczv5zxa8vz9hy1jjs99736qadqgali4y55lpag2b223kkjphal")))

