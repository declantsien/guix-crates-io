(define-module (crates-io lo cs) #:use-module (crates-io))

(define-public crate-locspan-0.1 (crate (name "locspan") (vers "0.1.0") (hash "04spggm6kagrwxyhlnvqb6f7wlzp9y89xca2bspyzxkjhfpbkfgr")))

(define-public crate-locspan-0.1 (crate (name "locspan") (vers "0.1.1") (hash "1qr3dx6n0cy4a85y5sz60dqrsgrcgy9576qn957br5pj5746hx9v")))

(define-public crate-locspan-0.2 (crate (name "locspan") (vers "0.2.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "12i9mwddcd79d79spg84kbizn5dwma3c6vf2dx5dngyj5fx17bxr") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2 (crate (name "locspan") (vers "0.2.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1b94p5d5d11czwvnz01j03ppsxw2mkb08dpa13mqwp1zl0k9fy0r") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2 (crate (name "locspan") (vers "0.2.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "18dlvhbyww3iwdaz7b36lg439zdni7ap8jw22b1w4r808gg4lvwz") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2 (crate (name "locspan") (vers "0.2.3") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "02vbz844c87qbwqsd4jv1c9gvayxl1c1wba6gi69jsv57sn5nph8") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.2 (crate (name "locspan") (vers "0.2.4") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "10hr51236669api79jkgp1m0z13fl09mjv7gf5vzspxkc42fqrzi") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.3 (crate (name "locspan") (vers "0.3.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1y5wwk7ij6945vy7h9vawrl2cvpkp0w084x72v8yv7smwywz2hl3") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.3 (crate (name "locspan") (vers "0.3.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "0dvr9s4lsf4sm53rvc7hfi4dpmb2jmfg46x0xdihwvhn4hvgacrk") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.3 (crate (name "locspan") (vers "0.3.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "10iz2zxs3c9alc9ad4hwrb3y251i0msg5iiway0mgx759l7yjzgp") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4 (crate (name "locspan") (vers "0.4.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1d2n1s0958x3ch6i3jbi75xa8j4jij256cacyplskc18qazzamn9") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4 (crate (name "locspan") (vers "0.4.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "0shzlycyx8ax1rr2aqlc8jfkkhi0g41bjg1ix224s2fibg6vs553") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4 (crate (name "locspan") (vers "0.4.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "0j7wmskc6q7frg9cs0l2wywxi290fpc086942wcygarjj459cvrs") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4 (crate (name "locspan") (vers "0.4.3") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "0xld854ichvkkzggv6lwwnzn450yr9yw91sgg4gmipvdjpcnb7si") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4 (crate (name "locspan") (vers "0.4.4") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1kvsn4q0fy9ysc2xj4p35ns7gijw4kh9fzbxz7lqkm2wfy8nm91c") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.4 (crate (name "locspan") (vers "0.4.5") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "09abz4iqci00zynl1hr2bf8dwnl097p2022by84rp7fa9hqhjl6c") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5 (crate (name "locspan") (vers "0.5.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1w1j7awlkgakap75cv1fbhhhlrxm3n6bidf02i06lwn2vn3p9jfd") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5 (crate (name "locspan") (vers "0.5.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1kll6isxlppcs3d7jg5gvmmr9irbw6fdq3m1q1h9rr6pab9slnky") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5 (crate (name "locspan") (vers "0.5.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "18is20qdszm27c4nza86srwyhlmgj99fvpl5cgm0lqms3kcmx6zd") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.5 (crate (name "locspan") (vers "0.5.3") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1kwk2j2agypkvfm7yqvviwwgihw2g1aqvlc75d8sqb9j2nvz0jx3") (features (quote (("reporting" "codespan-reporting") ("default")))) (yanked #t)))

(define-public crate-locspan-0.5 (crate (name "locspan") (vers "0.5.4") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "102yn35mjh0yc36zqpzqcjasqbza827hj06wbprsfdx4r9qmd432") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.6 (crate (name "locspan") (vers "0.6.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1rxgh5irpgimgr5kv53kkzs78x78phnv2d0pc0ryhy0fjccwlmi9") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "0a7dp846nisq4vsh968bm59y7p8wvic1l17sg7fwly53kb0dnwrj") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "086y6v4xadiwblfilfsn950k37jkhdd1hf3ssfqsknbnxb86j5y7") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "0izfv13lmkghscgjlq03rmvjj823zmx267nkh8sn69warr5zxgrx") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.3") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1js5bd9vc5aj7lk23il373ipa2nz1wfnk449wqj5911kk55ymy1v") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.4") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1g8ap1f9rs22h0bcs2w7ih2ddj7nrhw0k4h4bqfx2ydg4ip7fb7p") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.5") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1qshilv4l2y7kngriv38vzs832q2351gncv1yamkx9mpxkj126sw") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.6") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "05z9s7q4cgslyfpbppns9r3s65d0fmnwqgvl3rl3k5k194dy047i") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.8") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1gabgnv6fljd36r4yvdid24dw9dyjccj7336hjdsdhcmz4h1z8dp") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.9") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0s7bccv936q2kgyyhazrkk1rr3r825vby3ldrwa041s0968f02w3") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.10") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "078q7lnyn1rzh9ga14hl704byl9asyhw3v477sgzlqil1fplvil3") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.11") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0nvjqgn64dnszaq2997p2grb8abry77k3z7b3is8a2rdv3pif1z1") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.12") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)))) (hash "1b5rc3x9gzk93qap2x3qq73d3165ssi3fm7s51v9bh2w12xx7cn4") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.13") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)))) (hash "0m1fwx0vqk1zid8aqw1j57s40zh979wh0g5g3csnq9p4lfsjcpk8") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.14") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)))) (hash "0xm3x3sf5zmwl6biqvyf3a59ib2bhj6pvj11bgc3h6ylj3i11fsh") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.15") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1w997gl6mh0wyv54xjkcnbg4mpmg57rl63n2qmwjbwbv6ksrps51") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.7 (crate (name "locspan") (vers "0.7.16") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wqxakalxj9xhrx7qb46s70asq9r1dxzmyn45rw903y5pbx43dbf") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-0.8 (crate (name "locspan") (vers "0.8.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ifjazfn870r5a5agz5mnl4cd27g9f6dwn0in9vh9fwrgd3d1al3") (features (quote (("reporting" "codespan-reporting") ("default")))) (yanked #t)))

(define-public crate-locspan-0.8 (crate (name "locspan") (vers "0.8.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1klmvm0mvdvgbw2lzjal1mrp340ma1m3rf27i9gni8r6hkabbwmp") (features (quote (("reporting" "codespan-reporting") ("default")))) (yanked #t)))

(define-public crate-locspan-0.8 (crate (name "locspan") (vers "0.8.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "contextual") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "180029rmaz68a5prla2fxdfmxx91yd5r94i06na8xj7szi4h929k") (features (quote (("reporting" "codespan-reporting") ("default"))))))

(define-public crate-locspan-derive-0.1 (crate (name "locspan-derive") (vers "0.1.0") (deps (list (crate-dep (name "locspan") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l86b78mg08k4wmqafqkwabbg2j81q18paslyiry2q0h9ym4qn9p")))

(define-public crate-locspan-derive-0.2 (crate (name "locspan-derive") (vers "0.2.0") (deps (list (crate-dep (name "locspan") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "12fh9lmsw9mw4jl3dgzz0asbmvnk0vkbykj44nfi7hisa60wmvm0")))

(define-public crate-locspan-derive-0.2 (crate (name "locspan-derive") (vers "0.2.1") (deps (list (crate-dep (name "locspan") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1igshv2biji4pd0wr1dal97jbjbyjsw68aqhcqkfna3nhj0y35c4")))

(define-public crate-locspan-derive-0.2 (crate (name "locspan-derive") (vers "0.2.2") (deps (list (crate-dep (name "locspan") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ny5akwnm4kz9jc4j5646gp37h1bs85w8wdb7y8qs795gb2s1nvq")))

(define-public crate-locspan-derive-0.3 (crate (name "locspan-derive") (vers "0.3.0") (deps (list (crate-dep (name "locspan") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "14267gvz36jn9hfrxa6hi38pv8lls5cvdbhcns2xi8003wy4f99v")))

(define-public crate-locspan-derive-0.4 (crate (name "locspan-derive") (vers "0.4.0") (deps (list (crate-dep (name "locspan") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i9hp6m2r3six1m8vvsqa57jky035ipy29dvb56dbr8k1nsszd8q")))

(define-public crate-locspan-derive-0.5 (crate (name "locspan-derive") (vers "0.5.0") (deps (list (crate-dep (name "locspan") (req "^0.5.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vla517zzp2dd33cbdn9snbxa4wf07js3f0j0mp05irs0sx8q4i0") (yanked #t)))

(define-public crate-locspan-derive-0.5 (crate (name "locspan-derive") (vers "0.5.1") (deps (list (crate-dep (name "locspan") (req "^0.5.4") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "01ry5fmw2jr1rzb16xxdd5wsr7qdkdh2dbnxdfb1b5an3drncc42")))

(define-public crate-locspan-derive-0.5 (crate (name "locspan-derive") (vers "0.5.2") (deps (list (crate-dep (name "locspan") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z4rcq9r698c4sgiy1mrn1byw9dy12m8xli54xi44642zhi7y6fy")))

(define-public crate-locspan-derive-0.6 (crate (name "locspan-derive") (vers "0.6.0") (deps (list (crate-dep (name "locspan") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "06lnqh8wrpnxb6kghjzfyfh3c4sq75bc0q0zr8lkv6h47ci932g8")))

