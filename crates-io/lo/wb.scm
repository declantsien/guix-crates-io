(define-module (crates-io lo wb) #:use-module (crates-io))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "18vrx8f41fz5caslahl9f6awkq3vq2svra1wqb0szdl6cyx4ymq4")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1jcvji0i875ffx6s9bmssiz729v0nksj07lpq3vn6r4aj3agw5j0")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0vs0x101ad08kxw0w9cigf270y5nhzl7iwwg69ljc30kb2ngr8jv")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0nswz5chdhimfwyclp8zi01n7vg08mmlac5bm5i3ykdjdgn3k7xi")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.13") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "14adcg5r3sh7ycmr5x5ivbd3h1byl5pfjmhl7mwqgy4yl2h1xm97")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.14") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0lfv3f44dxlf32ml5dzz1nmrh2dz3cfhias8vjsix9qvrvla2abg")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.15") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0jrdv3n6zzzp74d3bmdjyw5v0gfc1mymh314bcs76r9nnx0rg4sl")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1dbdpkxv58zg3ccyidgcz690h5qr1n7l8lf11zvfwkqxxzr2pzjl")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.21") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1ppk8d6ldq737pj531alv4pnfdjjwmwjpy05js7k7dkvwb0nl11p")))

(define-public crate-lowbulls-0.1 (crate (name "lowbulls") (vers "0.1.22") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0m6pzi2pcb7fq1birav18swhj5ag902a43i52d5wahgvg7i3mh31")))

