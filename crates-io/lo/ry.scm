(define-module (crates-io lo ry) #:use-module (crates-io))

(define-public crate-lory-0.1 (crate (name "lory") (vers "0.1.0") (hash "137jxl4dk2dx0qnzsc3cbdis5j3y38mlsmihs5zzswr6sndscsm0")))

(define-public crate-lory-2020 (crate (name "lory") (vers "2020.3.16") (hash "1gyi3xiibmwrysj9138krdy2npjn0lkgla5d8sjgdk4vnhx5j6kf")))

