(define-module (crates-io lo ts) #:use-module (crates-io))

(define-public crate-lots-0.0.0 (crate (name "lots") (vers "0.0.0") (hash "0wks33l5n2f0s9z0qlk7pz93rqh5hln457v7v3qw4wnlgpk7cd28")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.0") (hash "18l24ldbggdsld0wziz9cvsb45514yqzc3d494m7gjr2j2i7a7ii")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.1") (hash "1j03didhgh19b00s1bydvp9afyvrf1wk4h9an1lp2zwfd0vgxxlf")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.2") (hash "1zqklym9snqi6bkv8n3pb5czyc5br290mv8jz965l893cg45w443")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.3") (hash "0ng0vdfc9p269wm0j0dwph7rrx5bj23if3d8qysbqm7406hrzk6a")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.4") (hash "09cy6nx0v9avdpmd117bzv7yhm0hkj493jpvpw85ph3dxi6i2a8j")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.5") (hash "12ywa8r3bhbh3x5vq5airc9gqimpmba5v09a1mg1mhdq8gkrdphh")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.6") (hash "13202znykihwix8yin8382x624sn70qspncq5cnwk2k094396b4h")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.7") (hash "03877jm3dmldjb6jf20p122518rimywlzpgx3lkypvr02gfhbsi1")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.8") (hash "1gh0iw7i7044gkfbp9k5jg897kxxpb5dily6m7radqn5k19sqm3v")))

(define-public crate-lotsa-0.1 (crate (name "lotsa") (vers "0.1.9") (hash "0z2lpb2jgygz83l8c482caklyv9cr8035908b76drr5szsqjdw8b")))

(define-public crate-lotsa-0.2 (crate (name "lotsa") (vers "0.2.0") (hash "1w4f5bdwhghqg5ajxh14y1cdmq9sgzlh2bc0qxpka71yv2dqrdc9")))

