(define-module (crates-io lo kk) #:use-module (crates-io))

(define-public crate-lokker-0.1 (crate (name "lokker") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1sgkpg10sznw62g6cq1nmdcfgcn65kx9zl21ic78d212ajv2a1pl") (rust-version "1.75.0")))

