(define-module (crates-io lo ce) #:use-module (crates-io))

(define-public crate-locenv-0.1 (crate (name "locenv") (vers "0.1.0") (hash "029grqicwkjqbdb8412hd0nqzdnwys9aig6k0xv56c300pvm7bzb")))

(define-public crate-locenv-0.2 (crate (name "locenv") (vers "0.2.0") (hash "1qy3633m4f9yb1sg81p69i433s1s8ysa12h0vcwdgi0g52akad09")))

(define-public crate-locenv-0.3 (crate (name "locenv") (vers "0.3.0") (hash "06xca8n12j8p5c8r4xgsln06mxrmgr4n8s06jy6awh6j2lsw43vd")))

(define-public crate-locenv-0.4 (crate (name "locenv") (vers "0.4.0") (hash "0g9gzl2qhz1lbwivm2iv59bznm9a3cwrfzcjxv2vv0vnqipz15kh")))

(define-public crate-locenv-0.5 (crate (name "locenv") (vers "0.5.0") (hash "1f7y3imdxsrjxqb14h9ignxrrcyvclf43qqw2arjjf7r1s4a5rcv")))

(define-public crate-locenv-0.6 (crate (name "locenv") (vers "0.6.0") (hash "0z75zkf9jvdnfiy29k6k9xraqdss30zb7c00a7p41baykmk8i7mx")))

(define-public crate-locenv-0.6 (crate (name "locenv") (vers "0.6.1") (hash "00r2r2b5a624j2w55553hw2896y514n91zm63gf82padlkf2xz5q")))

(define-public crate-locenv-0.7 (crate (name "locenv") (vers "0.7.0") (hash "0jqyj9nmnr1wb2zzn3ixx628pdzm89rvih36rfkp2bqw8q2zrkw4")))

(define-public crate-locenv-macros-0.1 (crate (name "locenv-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1530kkwn9i4kvk6jx85zbsyg469qm0wsw21kwkcqx73xn37vxn7s")))

(define-public crate-locenv-macros-0.2 (crate (name "locenv-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rnbwhz0ln2lbiv5v51jkjnxrnis1il4f23ja04gmh3k0702m3xr")))

(define-public crate-locenv-macros-0.3 (crate (name "locenv-macros") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cmpm28a7gwrsv9qprabm82nkkrq5l27jbi1xh168wk84fhvv1k0")))

(define-public crate-locenv-macros-0.4 (crate (name "locenv-macros") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lnalj9hpkfx3gh4bv3nq8n28q9l6cnsmknsvrsyisriwxiqcpc9")))

(define-public crate-locenv-macros-0.5 (crate (name "locenv-macros") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hfr30c4gpviwv1a04cj14bsnlrblajshsdrax9j9zgw58dqdsbn")))

(define-public crate-locenv-macros-0.6 (crate (name "locenv-macros") (vers "0.6.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xm40zfrqjs8r3wxadiwy8hnlz0qbsjvgi9qd15gjxpjil6493m4")))

(define-public crate-locenv-macros-0.6 (crate (name "locenv-macros") (vers "0.6.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09qlw5k62y3np2fmjvzh96jvs5mw69xyxp1f80p0wkvbnnglis4q")))

(define-public crate-locenv-macros-0.7 (crate (name "locenv-macros") (vers "0.7.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wxnx456krwhdk3mn556ng3678dsx2chsscb5rp6g86n8iy917xz")))

