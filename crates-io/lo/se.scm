(define-module (crates-io lo se) #:use-module (crates-io))

(define-public crate-loser-0.1 (crate (name "loser") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rqbnp867d3cxzidz9dzwg6k12rrv3na2gbkm3fzdpbk2c01jj3r")))

(define-public crate-loser_case-0.1 (crate (name "loser_case") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1z0ckp558q38yvib7mgd2ikmj33xzsymmpx81vvkfkcd4gglnlb5")))

(define-public crate-loser_case-0.2 (crate (name "loser_case") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1dqzmqad3r4fqyckdna5y2j8v78zabg4z608sw2qi5nd5hwpzqf1")))

(define-public crate-loser_case-0.2 (crate (name "loser_case") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0alqgxm0xf70gbci0ilkip0vg45jw4nms6n3ddcihkfqv3hb8kjz")))

(define-public crate-loserland_exec-0.1 (crate (name "loserland_exec") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)) (crate-dep (name "pelite") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi" "sysinfoapi" "winnt" "libloaderapi" "winbase"))) (default-features #t) (kind 0)))) (hash "11ilbkdc4rgcbnrkj8jhy88ljj38w66pg1fa0dyi54lgay6x9b74")))

