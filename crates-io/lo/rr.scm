(define-module (crates-io lo rr) #:use-module (crates-io))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1j20rcxb3gsdhf2qwp28v768imf9cbp22jjwvmxk0zjacd67n6i9")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r6jx09fzkw0m6c0804ycb6drajk32gs1h0mdzzbm6n6xcra299d")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04slri3p9j4vlp0m4p4b118nfcs5v9c5g5f67i4sj8fc4kqvrfkh")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09zf6lqyvrzdngn0vpfdixrm2p35l33kdcsjma4hrx3m3f7jj55s")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16jccr1f3d8za62iy2zr3sj7pf1nxzg3gnvmdf5js5cqzhhkjqkf")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.5") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0rv6q2prm4qm4dg94krc34rpgnaxnxl1b5p19xqqz80f0p7dha9m")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.6") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ab4z6rarn8mn8i39vfxr0pqvqa0jqp88fci7c97srck2fzl8nv9")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.7") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0b6yczsjgg8laljcb8rm9hxw794wps6sh8nxcs9fnz6npij1x28c")))

(define-public crate-lorry-0.1 (crate (name "lorry") (vers "0.1.8") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "029gd3jfs83yhd9n24nalfkin211qgk1g55m00qzqncnm25hv84w")))

(define-public crate-lorry-cli-0.1 (crate (name "lorry-cli") (vers "0.1.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.15") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "lorry") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "0qpg4yvis6gna7f3xas0pl62wbifj071fvjg9rfqgndd3fs06ivz")))

(define-public crate-lorry-cli-0.1 (crate (name "lorry-cli") (vers "0.1.5") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.15") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "lorry") (req "^0.1.6") (default-features #t) (kind 1)))) (hash "1pw6scxcpdc9zq130xswr7299h30b55awnjv4l3an8rfnbs4p4cd")))

