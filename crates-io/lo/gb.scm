(define-module (crates-io lo gb) #:use-module (crates-io))

(define-public crate-logbar-0.1 (crate (name "logbar") (vers "0.1.0") (hash "1lwhp2acd9gnjilnazz875kqfh4m2mlm70qngvnpzym2bqnq11xf")))

(define-public crate-logbar-0.1 (crate (name "logbar") (vers "0.1.1") (hash "0kdy8b9i821jf16sy9lbl4mhq1l8wgw2na360xn9gd94nz9xq1gf")))

(define-public crate-logbook-0.1 (crate (name "logbook") (vers "0.1.0") (hash "0p7q3v7b2c1k0fhsaj3ai3p5wlgcnxh9qagqb10fh93v78gcmvjs")))

(define-public crate-logbook-0.1 (crate (name "logbook") (vers "0.1.0-alpha.1") (hash "0nbm7kn7w9r4k1jfr8d9gmkk1y4hfl763jn6d2bf2y8fpjs3ziby")))

