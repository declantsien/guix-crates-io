(define-module (crates-io lo rg) #:use-module (crates-io))

(define-public crate-lorgn_lang-0.1 (crate (name "lorgn_lang") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vv1wprad8y19r0miylj1scjghlv2p8smyf5mz91nlxwgm63rbmz")))

(define-public crate-lorgn_runtime-0.1 (crate (name "lorgn_runtime") (vers "0.1.0") (deps (list (crate-dep (name "gc") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gc_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lorgn_lang") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)))) (hash "03yvzh0g2nx75sk3il5m4baacymhzm08whaisv259wbb0d3hpnhh")))

