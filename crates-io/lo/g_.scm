(define-module (crates-io lo g_) #:use-module (crates-io))

(define-public crate-log_64-0.1 (crate (name "log_64") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.0") (kind 0)))) (hash "13yx31f0yqkq464byyhla4na8dnc696dnma5kxfz8kfi5vpkngvb") (yanked #t)))

(define-public crate-log_64-0.1 (crate (name "log_64") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.0") (kind 0)))) (hash "1bikjv7n9y5015ap2v5zrfca49zp6k9fpkp8snr11yxbibqa1zw0")))

(define-public crate-log_buffer-0.1 (crate (name "log_buffer") (vers "0.1.0") (hash "1sg5hh0lrs7zza6ni1kl0mgw693hps6wn8s19z029n15a5iarzzd")))

(define-public crate-log_buffer-1 (crate (name "log_buffer") (vers "1.0.0") (hash "0npj6jqwypqyp1x6pv51h9z6l22yvv24ai47b2cjz9gc4jxbbgpq")))

(define-public crate-log_buffer-1 (crate (name "log_buffer") (vers "1.1.0") (hash "14x45b5qq3rarf1kmpn62wrlfrwynp4njcd9dbvvvrxvhhxp4mzc") (features (quote (("const_fn"))))))

(define-public crate-log_buffer-1 (crate (name "log_buffer") (vers "1.2.0") (hash "069497i5128721frhk3lnrd3kbl36h55r2cwlybzxdw6jhy1fczh") (features (quote (("const_fn"))))))

(define-public crate-log_domain-0.3 (crate (name "log_domain") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "0lpszrfhv7dvcv42ljzdxi0gk85l0vif9cq3lwgrdavi62wqqdmb")))

(define-public crate-log_domain-0.4 (crate (name "log_domain") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1j681n5qqbsg691f8ffxmmla18d7qrn44phcdpwl3wmqcci9s0a5")))

(define-public crate-log_domain-0.4 (crate (name "log_domain") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "04c4fp3v8aaki286x63cg0xz1y4qvns0x8a4w419zjy1kxi7qddb")))

(define-public crate-log_err-1 (crate (name "log_err") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "0.10.*") (default-features #t) (kind 2)))) (hash "080ma11yz4hml0vnn8qy8231h1m5kd6j039d9dzpsj47q27maakk")))

(define-public crate-log_err-1 (crate (name "log_err") (vers "1.0.1") (deps (list (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "0.10.*") (default-features #t) (kind 2)))) (hash "11axyxykbij3sifffskpg13gzldsglbsq3m31dnjifnr1l8457dz")))

(define-public crate-log_err-1 (crate (name "log_err") (vers "1.1.0") (deps (list (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "0.10.*") (default-features #t) (kind 2)))) (hash "0lq0n0v2y14rh4ikiwk2mjk9b42qcl4iglvg58dvn5wyc51pks38")))

(define-public crate-log_err-1 (crate (name "log_err") (vers "1.1.1") (deps (list (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "0.10.*") (default-features #t) (kind 2)))) (hash "1xv0m3xfjncrphws9fq0ykrhd0q136z1ny9swlkq58cda209x0p2")))

(define-public crate-log_file-0.1 (crate (name "log_file") (vers "0.1.0") (hash "1kyrd55zljyhn7sma548w23a5y9g62c0018nmzs9mmgfki0rpn4h")))

(define-public crate-log_file-0.1 (crate (name "log_file") (vers "0.1.1") (hash "163mx1gx1kdiamk165fgp4cy7cpl0hk4c0fi1wawqzjridlvwmnr")))

(define-public crate-log_hal-0.1 (crate (name "log_hal") (vers "0.1.0") (hash "1nad64g9hlijcz2wbm75da4qbznclj8xmla28mvbfppiq4csn2a1")))

(define-public crate-log_kv-0.1 (crate (name "log_kv") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8.23") (default-features #t) (kind 0)))) (hash "063zqzypgsg8h1gpy75px10qaaxhs5nss0i52q88fpizdcs9vypn")))

(define-public crate-log_lib-0.1 (crate (name "log_lib") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (default-features #t) (kind 0)))) (hash "06kkm1dwrqjp6jxvr3xyxsbv6njz3gj7k1gmnjkgz4pyl16a85ws") (yanked #t)))

(define-public crate-log_loki-0.1 (crate (name "log_loki") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("charset"))) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "03qzyrfkz26dclpj739w9imnhnrr5hy9kkc133f5dysinry3i2w2") (features (quote (("tls-native-certs" "ureq/native-certs") ("kv_unstable" "log/kv_unstable") ("default" "tls" "tls-native-certs" "logfmt" "compress")))) (yanked #t) (v 2) (features2 (quote (("tls" "ureq/tls" "dep:rustls") ("logfmt" "dep:bitflags") ("compress" "dep:flate2"))))))

(define-public crate-log_loki-0.1 (crate (name "log_loki") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kanal") (req "^0.1.0-pre7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5.0") (features (quote ("charset"))) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0nrpjbhia5h3pj7x2538m6fxwyg5k8vf9piq91ym04zn3f444521") (features (quote (("tls-native-certs" "ureq/native-certs") ("kv_unstable" "log/kv_unstable") ("default" "tls" "tls-native-certs" "logfmt" "compress")))) (v 2) (features2 (quote (("tls" "ureq/tls" "dep:rustls") ("logfmt" "dep:bitflags") ("compress" "dep:flate2"))))))

(define-public crate-log_macro-0.1 (crate (name "log_macro") (vers "0.1.0") (hash "17dfljkg48i801z5zq1jcqd75c92kv9yrmr811k0m25hz5ribvjb")))

(define-public crate-log_macro-0.1 (crate (name "log_macro") (vers "0.1.1") (hash "0sv5zdrhngkh5mqwi97p1pky6lviass5f0fivs59krhxw7r47ig3")))

(define-public crate-log_macro-0.1 (crate (name "log_macro") (vers "0.1.2") (deps (list (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)))) (hash "16nj2sgs6viy2kqnkhna0h2vgpd017fl56yl9f9kb078xjlh1kgs")))

(define-public crate-log_macro-0.1 (crate (name "log_macro") (vers "0.1.3") (deps (list (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)))) (hash "1rwfmvv3nl04bgf1gb6inmfzbvy7lr3rspgh73kffs38khm0mlvg")))

(define-public crate-log_macro-0.1 (crate (name "log_macro") (vers "0.1.4") (deps (list (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)))) (hash "0j64wlavbab2p7cdvsws733ckrrzfkxdg4y9gkvwdxnv7z5g0mh9")))

(define-public crate-log_macro-0.1 (crate (name "log_macro") (vers "0.1.5") (deps (list (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)))) (hash "1wi3cjqvf7gnq7d52rq80gpfwjcwyg0j4k14fp8x1nx020q58iki")))

(define-public crate-log_macro-0.1 (crate (name "log_macro") (vers "0.1.6") (deps (list (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 2)))) (hash "00a30azn9skpx0jbsf3n3a70bmn856zj7sqv4dcwm4780v2wbsj1")))

(define-public crate-log_parser-0.1 (crate (name "log_parser") (vers "0.1.0") (hash "1s6vyws9k71njzvn68hksrfknxp7k5v5i1sf50g99da2n18jz2k7")))

(define-public crate-log_pwease-0.1 (crate (name "log_pwease") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "02z6fwvqkjzzvwhcgmjydwvf6vbl5h9l2xyjj0xb82z5pkpcywqd")))

(define-public crate-log_settings-0.1 (crate (name "log_settings") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "09s7pvyvnm632bgjrxi0bkr4ll0kxkv48097lwn6ji1c54755zdv")))

(define-public crate-log_settings-0.1 (crate (name "log_settings") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1dhm75hbnqwas55j7gslig3hzsmxalrdp6a81iwhkg0gx8r2ff1x")))

(define-public crate-log_settings-0.1 (crate (name "log_settings") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kg7cf5ll4a1cznhdgsdxj4wxmfl885sslw10nr1jz2xavq43bqr")))

(define-public crate-log_std-0.1 (crate (name "log_std") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "log_hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n4qhxjglr0cx1yjwc3sx92lq1rx5b2b997sgnh70827mznpqghv")))

(define-public crate-log_t-0.1 (crate (name "log_t") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "contracts") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0hl9hwlkh0gigpgn9bnspv3aph6f21fcx92nfh97v5x3dx0gjg99")))

(define-public crate-log_wrapper-0.1 (crate (name "log_wrapper") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1kv8cgy98bd5rghb5hm1g3w484lpm7knq07vsbzr3syhhckisf1l")))

(define-public crate-log_wrapper-0.1 (crate (name "log_wrapper") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1r3l3vs6c6jgxsajpqfp4fdlvsyj9jxgb7l1gzjjh0qrf42wsnpm")))

