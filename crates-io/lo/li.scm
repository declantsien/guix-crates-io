(define-module (crates-io lo li) #:use-module (crates-io))

(define-public crate-loli_lib-0.0.1 (crate (name "loli_lib") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0nqjgn7vdcfrprbcm86qqgp40rynsj5hz29wj5gkxf0wxnhf1hl9") (rust-version "1.68.0")))

(define-public crate-loli_lib_dev-0.0.1 (crate (name "loli_lib_dev") (vers "0.0.1") (hash "04xnhqrgd3nrag0ia3jgi2dgzqxkbdavglph5sw3qlfxh9wqiwba") (rust-version "1.68.0")))

(define-public crate-loli_lib_dev-0.1 (crate (name "loli_lib_dev") (vers "0.1.1") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0wlckm7lr80africrxkw0a13jvsg37zw4h904wl9pn1bvdn735pj") (rust-version "1.68.0")))

(define-public crate-lolicon-0.1 (crate (name "lolicon") (vers "0.1.0") (deps (list (crate-dep (name "lolicon_api") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "1nsxxc88gk87agcrzz1xiv9fal2fcmx795dk7v98p71rjh253c19") (yanked #t)))

(define-public crate-lolicon-0.1 (crate (name "lolicon") (vers "0.1.1") (deps (list (crate-dep (name "lolicon_api") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "1ppcvl0rp9kwwawxrizh922csiaims4rziz0vm19662wppvi6s72")))

(define-public crate-lolicon-0.2 (crate (name "lolicon") (vers "0.2.0") (deps (list (crate-dep (name "lolicon_api") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "1mjag7c6imrh0wiy07967671hahc1w5g3zq0n4r2cmjkj0f5ipd3") (yanked #t)))

(define-public crate-lolicon-0.2 (crate (name "lolicon") (vers "0.2.2") (deps (list (crate-dep (name "lolicon_api") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0kzm3jyy0r0385j9q7grxj2nx6fg8395wy10yrfhjd1c3nxblprz") (yanked #t)))

(define-public crate-lolicon-0.2 (crate (name "lolicon") (vers "0.2.3") (deps (list (crate-dep (name "lolicon_api") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0slipnn5zhpihw46i4899bj145lm32jpph1k0zh30yczfipprins") (yanked #t)))

(define-public crate-lolicon-0.2 (crate (name "lolicon") (vers "0.2.4") (deps (list (crate-dep (name "lolicon_api") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "1m50ddmdywg6bmisqp54vcx6g5cgrvf324sbwl1crmwx0vk89vis")))

(define-public crate-lolicon-0.3 (crate (name "lolicon") (vers "0.3.0") (deps (list (crate-dep (name "lolicon_api") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "08rjhxw5gy895ypwzvqfkpaa1gpgnxiy66kh1fwa3rj5r0y3bysm") (yanked #t)))

(define-public crate-lolicon-0.3 (crate (name "lolicon") (vers "0.3.1") (deps (list (crate-dep (name "lolicon_api") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "1bn62yqfw2vx2h68hspdd71argj3b09jzp3blmi23x1gxp8j9gwc")))

(define-public crate-lolicon-0.4 (crate (name "lolicon") (vers "0.4.0") (deps (list (crate-dep (name "lolicon_api") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "18xxg763lqgxz3zxcp9fx4cwi4f39r834anx668jidfk4bpba0x7")))

(define-public crate-lolicon_api-0.1 (crate (name "lolicon_api") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0qp2zb4dfkd7wn1xn0l4wckg5pqbr56hyxcrqzlm73djhj0nql6x")))

(define-public crate-lolicon_api-0.1 (crate (name "lolicon_api") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "19d01dmah50aslzibr0ilzsjxm5df2kswajis6amvwihqn3i9rjq")))

(define-public crate-lolicon_api-0.2 (crate (name "lolicon_api") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0n1x2hbg35bflz1blqggvxi01k067ka5bhmavxbyhybvcnp9gp77")))

(define-public crate-lolicon_api-0.2 (crate (name "lolicon_api") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1bl257yh51bpvkr67qlzbhk73k5biac4skdjfv9byn3mfcml160c")))

(define-public crate-lolicon_api-0.3 (crate (name "lolicon_api") (vers "0.3.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0pri593722l121hfz0ncj4dyxjnpx29l941g920xpc6v8zq4mxhl")))

(define-public crate-lolicon_api-0.3 (crate (name "lolicon_api") (vers "0.3.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0qml0ds959g6b16q2kx1kibn7p7847h12h1hlw24rpjn2f0grdbw")))

(define-public crate-lolicon_api-0.3 (crate (name "lolicon_api") (vers "0.3.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0apq9rnqbmvq7aa10cvakr7v02gfn2kcmnzmv28ivw7ky01sl1y6")))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "14062y09zy5iky9kl6mqlg261aivxacphqssy74zfm879z6sw1db")))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1v389z7pp3zxb9ywhabr5maiyvl4756gcs19d7fcs6pw7f7wfarb")))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0q84n727xa7ph8nnf3rhad60m220269rra1rymwmf5x0phapqw58")))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "079grnz1hk4yg0ms7z8jb3xn2p9ngf01qq1q5r6akxgzcwv9x9zy")))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.3.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1mbzmvdf3g7dws89kxfk1x3b541wxv2a0vq4c4jc2vxg6klzw67k")))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1avgxs8rzzdq0ww359mk8lcz0cahzppkrqr7i6siwisrcq8137cd") (yanked #t)))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.4.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "16zv2z9n50ip8f07d2n6axrndz5rkyns046pkphmm2gz35fm9lf7")))

(define-public crate-lolicon_api-1 (crate (name "lolicon_api") (vers "1.4.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1irqb6fbcg37mj3yqmsz8bqxrm8hvmi940kgw0xaw31hcj9299kh")))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "str-buf") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1cgin5c3rzzcva7ydmpylvi1fah7ixg11r0z7zaw8a04wflj3ymg") (features (quote (("prng" "wy") ("osrng" "getrandom"))))))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.1") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "str-buf") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1inlxh7az2fj4jdhc7yn6jf3hmch2jcwhm0msmk2c36h5k8jir6d") (features (quote (("prng" "wy") ("osrng" "getrandom"))))))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.2") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "str-buf") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0yclvbfdy7nw335jharwr2jz2h0fv43nzhmlgiphmlnb5cnrd6hl") (features (quote (("prng" "wy") ("osrng" "getrandom"))))))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.3") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "str-buf") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0zh4iv5sqvf09n0bdvwrdp81842aplns33pmbwwk55c43ngis0dr") (features (quote (("prng" "wy") ("osrng" "getrandom"))))))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.4") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "str-buf") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0vc7910a2a8z4qa3hlnf8dvsl54pz0pp88pnn6993ykfg3ssfwkn") (features (quote (("prng" "wy") ("osrng" "getrandom"))))))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.5") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1hcl0qwj3agbk6gc3lcj2acy4fpscfv9rs689fa27q1mvrzh1249") (features (quote (("std") ("prng" "wy") ("osrng" "getrandom"))))))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.6") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "lhash") (req "^0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0b6rbbw7522dvciyfaxwg5x0w87k2r5mja90d3ldkqnwg7zv8l8z") (features (quote (("std") ("sha1" "lhash/sha1") ("prng" "wy") ("osrng" "getrandom") ("md5" "lhash/md5")))) (yanked #t)))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.7") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "lhash") (req "^0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "055ppnl5b3nzbcbzb7fvw33857wk188kj0zz4hpvhsvpwvfi93da") (features (quote (("std") ("sha1" "lhash/sha1") ("prng" "wy") ("osrng" "getrandom") ("md5" "lhash/md5")))) (yanked #t)))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.8") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "lhash") (req "^1.0.1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "188f848lnnpmf57z5cy47mifsxids70j6119jww1gqkn45qzckc7") (features (quote (("std") ("sha1" "lhash/sha1") ("prng" "wy") ("osrng" "getrandom") ("md5" "lhash/md5"))))))

(define-public crate-lolid-1 (crate (name "lolid") (vers "1.0.9") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "lhash") (req "^1.0.1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "wy") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0az7493ardir3ypziplc6nypw20a03csswz4n6qyg1irdspbpl69") (features (quote (("std") ("sha1" "lhash/sha1") ("prng" "wy") ("osrng" "getrandom") ("md5" "lhash/md5"))))))

(define-public crate-lolid-2 (crate (name "lolid") (vers "2.0.0") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "lhash") (req "^1.0.1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "1njri238j7maslgjbjgzgxah2vjabn7qxss302anmvrs805x5jz9") (features (quote (("std") ("sha1" "lhash/sha1") ("osrng" "getrandom") ("md5" "lhash/md5"))))))

(define-public crate-lolid-2 (crate (name "lolid") (vers "2.0.1") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "lhash") (req "^1.0.1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "squares-rnd") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "0x1cl717nck7hwnv9ap4hdqllivq8az0zld6wz8gpkkdawh74nn5") (features (quote (("std") ("sha1" "lhash/sha1") ("prng" "squares-rnd") ("osrng" "getrandom") ("md5" "lhash/md5"))))))

(define-public crate-lolita-0.1 (crate (name "lolita") (vers "0.1.0") (hash "0wh13hc44mv1slc8krj6s9r6yzmkwas4gifvhjp0wli6n46i4i8l")))

