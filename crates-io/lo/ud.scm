(define-module (crates-io lo ud) #:use-module (crates-io))

(define-public crate-loudnessnorm-1 (crate (name "loudnessnorm") (vers "1.2.0") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zk254rxj412fhxsjvygbbkyyrsdphr9alm04zrslsl1ckh56fj7")))

(define-public crate-loudnessnorm-1 (crate (name "loudnessnorm") (vers "1.1.2") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mwzh1jj1fpgayiyscqz8lvz78c9fvihnmsn8ajclhx0v2aqpkln")))

(define-public crate-loudnessnorm-1 (crate (name "loudnessnorm") (vers "1.1.3") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dlpz2q2q2j7naw9dc15gpgqb1piyprbsh69aqmgjpfq1h55xn14")))

(define-public crate-loudnessnorm-1 (crate (name "loudnessnorm") (vers "1.3.0") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "02z6pbglwddslmv8dyfh0v1hvfxanddhjk3skj0h6adlssh9f94z")))

(define-public crate-loudnessnorm-1 (crate (name "loudnessnorm") (vers "1.3.1") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0idjw1fqwlykjfq8fhr90qxqm2cs508lzpgjfm90yzy3bi1h6p2h")))

(define-public crate-loudnessnorm-1 (crate (name "loudnessnorm") (vers "1.3.2") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xg60askdfyh45nb3798qgakq93wsxkd0hm194w28niad42zx84f")))

(define-public crate-loudnessnorm-1 (crate (name "loudnessnorm") (vers "1.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "12lcbqh6w5qs2ab5w32jq4vanhid5d4xvi7215prk3dc8ars95x8")))

(define-public crate-louds-0.1 (crate (name "louds") (vers "0.1.0") (deps (list (crate-dep (name "fid") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "128xwb2nk8alz84vxdb5s2cggl8vm7489k6hyr1aapp2p3k5mf1c")))

(define-public crate-louds-0.1 (crate (name "louds") (vers "0.1.1") (deps (list (crate-dep (name "fid") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0x9ksbn3c19jx42b6iclxymjnznn4xq9kgcyx44bf9zvgv16b9yw")))

(define-public crate-louds-rs-0.1 (crate (name "louds-rs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "1axllwjzcr613nw85yhfyb84ysny5b74b1vccgx41jycyyij6da1")))

(define-public crate-louds-rs-0.1 (crate (name "louds-rs") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0zbzpxgp1qha38hicn8ikbbm4p90d52jm1d4maqg1k9plk2vnydx")))

(define-public crate-louds-rs-0.2 (crate (name "louds-rs") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1k5a80v5m1rh37kg2rwrmmfilhmcrdpxi6mv18wh06589wja265d")))

(define-public crate-louds-rs-0.3 (crate (name "louds-rs") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0lhncdgq6nh4inkf7k38qfmrab0889s6v7jkyr8cb841w7a9cn0s")))

(define-public crate-louds-rs-0.4 (crate (name "louds-rs") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1sal98m4iidkyhczxyrlzai2bxaai2i060cafnd6sjzp43xr2sp1")))

(define-public crate-louds-rs-0.5 (crate (name "louds-rs") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0pbhnzmmjpyw54bz32v8dzx52l7ffmsi9hlfpl53sf76zj7w6ajh")))

(define-public crate-louds-rs-0.6 (crate (name "louds-rs") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "13yniawr8jpaqx7xsys53n747z440l2n2x8pam5x1kj55705g6xq") (features (quote (("rayon" "fid-rs/rayon")))) (v 2) (features2 (quote (("serde" "fid-rs/serde" "dep:serde") ("mem_dbg" "dep:mem_dbg" "fid-rs/mem_dbg"))))))

(define-public crate-louds-rs-0.7 (crate (name "louds-rs") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "fid-rs") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zz0v0fryjv33xcvahjg1j48h3fdga8dm2hzj9d17rq85z1fcvck") (features (quote (("rayon" "fid-rs/rayon")))) (v 2) (features2 (quote (("serde" "fid-rs/serde" "dep:serde") ("mem_dbg" "dep:mem_dbg" "fid-rs/mem_dbg"))))))

