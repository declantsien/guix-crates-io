(define-module (crates-io lo ot) #:use-module (crates-io))

(define-public crate-loot-0.1 (crate (name "loot") (vers "0.1.0") (hash "1lajx0vxdvh3x6a0z4lhnnvp9w453zr6gdlgcr3sdjy618lk60s2")))

(define-public crate-lootr-0.3 (crate (name "lootr") (vers "0.3.0") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dv0h1m2i5bk4as3yybdspz9hnawdssyhcbdbvl5g9h345vbhh95")))

(define-public crate-lootr-0.4 (crate (name "lootr") (vers "0.4.0") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1x5r2mii9fdyxf97bn32x13m3lgaclyq0mx4152yrh85mgf3v7sg")))

(define-public crate-lootr-0.7 (crate (name "lootr") (vers "0.7.0") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "06nlk8pz2qaksxy4z7ixjmqffy97b9sqxdcvmn2hr3a8kfp27583")))

