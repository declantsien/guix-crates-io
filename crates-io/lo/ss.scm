(define-module (crates-io lo ss) #:use-module (crates-io))

(define-public crate-loss-0.1 (crate (name "loss") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (kind 0)) (crate-dep (name "tracing") (req "^0.1") (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (kind 0)))) (hash "1mck3r80w1vm6fh2vspw4hz3yrh56c3km37yqd0xdl10vbdjwj8r") (features (quote (("tracing-log" "tracing-subscriber/tracing-log") ("smallvec" "tracing-subscriber/smallvec") ("json" "tracing-subscriber/json") ("env-filter" "tracing-subscriber/env-filter") ("default" "alloc" "attributes" "env-filter" "json" "smallvec" "tracing-log") ("attributes" "tracing/attributes") ("alloc" "tracing-subscriber/alloc"))))))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.1") (hash "0hjyccs6gdn2k781z4qqjypb6696am5w9rrk1x0dg7p20a7xpvfi")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.2") (hash "110q7bhg4q5bjw6lz4vgm67qz9i43cw95nzcflx85qkcy5is1yyn")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.3") (hash "0daq5hj7xkcr9raz3qhb5qh7z7756l7py0d3fw4xf69anqcrhv0l")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.4") (hash "1s8vcjygrzadj1ws9g8izkpmv8ymrcsn3k2wsli9k2y45bhwm3iw")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.5") (hash "003kksqlg0qwm6c7xsp2zmmbiv9wj0k5yc3zh2w966niz9cwibqy")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.6") (hash "1sgfak6j6nqd1app3dhib4yib50q9db05wfh8xsxnmd9v6xpz0ck")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.7") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "03mw5fldzswbl8kakvplj8xmd3j779rf72cdapz8pkdhc7kbdmb5")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.8") (hash "02da98xd4pyxwwfchc27wgxfllnby3vcf5ihjb5ddrxgh7a1a9ik")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.9") (hash "1c63lj275fs8msikkxv4d0g4grzsh16fgqi0s8h4ql78x7yni9yr")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.10") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ncsnjpff7bhg5w8dlqbxgrkymj7xnkfscbivi6pn7lzirglz9nl")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.11") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qhhpaxydafnkjkp9swpfq5zqdg65hzskf2pzmds75i2vj19i2wa")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.12") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "04qg6y8b7zjwzgx15hn974bf9jrp089fm6jhm1hpkr4h8kjl3zxx")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.13") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rjcz8ibjhrllmrn3ccsvc9pavr2bnwij15dnd413pl1933x16x0")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.14") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "06pvbhn8385fk7y16kv35aib81xkgdr92bbhbx21j6azbhlxwgaf")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.15") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "125ff304f7v2xxpc7mkbh2k4yldqq4lm698d1dfv5flf315avdhn")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.16") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qs6qj8ly8d3lh8v6bdajdvnq7c2igi4kpfv232i95hamq0bb6n6")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.17") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dr37akgkj8k2lpsp398bg8r2zi2n6z6rcd4pwbaf6sgkyapffr8")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.18") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "1w5065yqnf7g5abx1z561p85sap6j6yrxzgaxcnvf2cdds53hjsi")))

(define-public crate-lossyq-0.1 (crate (name "lossyq") (vers "0.1.19") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "0v0ic5f1aj591yrx1yyyrk1mn1brppi93pzdr7ff8si5x36d60ks")))

