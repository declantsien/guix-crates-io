(define-module (crates-io lo w-) #:use-module (crates-io))

(define-public crate-low-level-virtual-machine-0.0.1 (crate (name "low-level-virtual-machine") (vers "0.0.1") (hash "0236n1j4dn1g4aiacrs6flmmsqwiyqg871bmkhblyd6704kv80qn")))

(define-public crate-low-level-virtual-machine-0.1 (crate (name "low-level-virtual-machine") (vers "0.1.0") (deps (list (crate-dep (name "llvm-sys") (req "^100.1.0") (default-features #t) (kind 0)))) (hash "1frdi51bqc367hj0xpxhqai0zcf13cs7vhix13ymcdy0cigpvjwc")))

(define-public crate-low-level-virtual-machine-0.1 (crate (name "low-level-virtual-machine") (vers "0.1.1") (deps (list (crate-dep (name "llvm-sys") (req "^100.1.0") (default-features #t) (kind 0)))) (hash "1b3baqr8fj0fny2m3xjjpvvrxwzy2d6fwqx3niypdvbnq3r96mvc")))

(define-public crate-low-level-virtual-machine-0.1 (crate (name "low-level-virtual-machine") (vers "0.1.2") (deps (list (crate-dep (name "llvm-sys") (req "^100") (default-features #t) (kind 0)))) (hash "1nb65qh073d503xxgrnnxqrgdlb558jrqybhaj7f3l6bfkdxp2wy")))

(define-public crate-low-map-0.1 (crate (name "low-map") (vers "0.1.0") (hash "1yf3saiswdh6b7lh806w1gfnc8akj0b3q6z76srrap48z8iyimmq")))

(define-public crate-low-profile-0.0.0 (crate (name "low-profile") (vers "0.0.0") (hash "05hzl3n4q37iancw3s58laa8r4g9q8fsq8j335cc19wdhhikywhh")))

