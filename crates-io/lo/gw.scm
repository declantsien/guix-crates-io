(define-module (crates-io lo gw) #:use-module (crates-io))

(define-public crate-logwatch-0.1 (crate (name "logwatch") (vers "0.1.0") (hash "1phv25hmaalx283159sq6ah8whdi5k9ips4hb6gzbpyvxvsvhgp9")))

(define-public crate-logwatch-0.1 (crate (name "logwatch") (vers "0.1.1") (hash "1fa2wh2fzb40h3xq264dmy77qxkr3sz8basbdi10cl7dq4w2v570")))

(define-public crate-logwatch-0.1 (crate (name "logwatch") (vers "0.1.2") (hash "0ghca8akcgyyjc7sd59nynmw93nwkkwqjcl5l67nkw4vb0hmhqvw")))

(define-public crate-logwatch-0.1 (crate (name "logwatch") (vers "0.1.3") (hash "0qimpwilypzbn7y924jy6lfgw3rljwn16srfbrprwxhyy35m293j")))

(define-public crate-logwatch-0.1 (crate (name "logwatch") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "17kd163d9i1468zhhw2j1az2nfnlxapb4cw4xx3qw5z3jmw4clgq")))

(define-public crate-logwatcher-0.1 (crate (name "logwatcher") (vers "0.1.0") (hash "19fy30h4gk74i6df04ani701pipn82crvvy03c9g9s8gmqxiccd8")))

(define-public crate-logwatcher-0.1 (crate (name "logwatcher") (vers "0.1.1") (hash "00y99v2gdaz7w5xr01s1rpdm3w0cj5dd4f7laycdabmisnp2a29x")))

