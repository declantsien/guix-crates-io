(define-module (crates-io lo up) #:use-module (crates-io))

(define-public crate-loupe-0.1 (crate (name "loupe") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "loupe-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ryadqd9bzvigy575i0igcyj54xcdlf6ydvgm8aki9n2lsz0ww7h") (features (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

(define-public crate-loupe-0.1 (crate (name "loupe") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "loupe-derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "18y9sniwvmz1qcyhkxd9779ha8hlcgn2fz50csyann7ba5dhdkqs") (features (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

(define-public crate-loupe-0.1 (crate (name "loupe") (vers "0.1.2") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "loupe-derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rphd6x1hnc2h856m85b71a7z6s1wyka0hpnfj9aalkmmb1hr6yp") (features (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

(define-public crate-loupe-0.1 (crate (name "loupe") (vers "0.1.3") (deps (list (crate-dep (name "indexmap") (req "^1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "loupe-derive") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kb3bc62xh20i405afr8bf65m6gznbp0fhxrfrg5pqaglkgp4slv") (features (quote (("enable-indexmap" "indexmap") ("derive" "loupe-derive") ("default" "derive"))))))

(define-public crate-loupe-derive-0.1 (crate (name "loupe-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yvcqklrca886zf9p4k6pfisraqv64mmqgqyz803cx9979sjz6ns")))

(define-public crate-loupe-derive-0.1 (crate (name "loupe-derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0d28684f5drl9r94n607i2ch1xld7i4iy9n0yjq2c7v8873q2274")))

(define-public crate-loupe-derive-0.1 (crate (name "loupe-derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h6wzdvxnaksj9pbg59bsrmr85sc73ynl8263klnvhzfrqi5nm0a")))

(define-public crate-loupe-derive-0.1 (crate (name "loupe-derive") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ljrdhp4yk87xnbdq53f679yzm7yghanxq4s5sgjfs3i6f4gryy0")))

(define-public crate-loupedeck-0.0.0 (crate (name "loupedeck") (vers "0.0.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2.3") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qyd3zd0a4q8f3kdkzx6li82clsws2qrjilinmb78qkis1clg86j")))

