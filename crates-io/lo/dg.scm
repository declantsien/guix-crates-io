(define-module (crates-io lo dg) #:use-module (crates-io))

(define-public crate-lodge-0.1 (crate (name "lodge") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1z56679c26jipc012c991hvxgkdzzqxm9ab6xl33hbg4ahbc8iv0")))

(define-public crate-lodge-0.2 (crate (name "lodge") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0lsxv7463j2lg1kj63c3nd7n3jj5mkjq0rbdd9w6nc0nbwymv8wv")))

