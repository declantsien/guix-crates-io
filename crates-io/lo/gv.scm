(define-module (crates-io lo gv) #:use-module (crates-io))

(define-public crate-logv-0.2 (crate (name "logv") (vers "0.2.0") (deps (list (crate-dep (name "rocket") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xqx2x2xl4vyhjsq5bzjkjza7r9jyy20xgj7ryijrq0a0xhs1zlw")))

(define-public crate-logv-0.2 (crate (name "logv") (vers "0.2.1") (deps (list (crate-dep (name "rocket") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13h7b8gr74gbgk87bxdk6pdmwr52r8g9vl7mhzwzf49j7iqm2rvi")))

(define-public crate-logv-0.2 (crate (name "logv") (vers "0.2.2") (deps (list (crate-dep (name "rocket") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "140n8c7h103qdl606yb7irvhbyazwmpijy7903h51jl2k5vp3cls")))

(define-public crate-logv-0.3 (crate (name "logv") (vers "0.3.0") (deps (list (crate-dep (name "rocket") (req "^0.5.0-rc.2") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "067lvavz2669az4xlaw4chb243yvwnlwdni92k2l1a56lrcjsxy5")))

