(define-module (crates-io lo gm) #:use-module (crates-io))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1fx3s2qaxwpzb6wc47vgh24957g87ykk41gmmyi69ibsjz0gqq37") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0ws0b7lp9wx7y1qy814lqc1xr8dl173aqjr52xc7s4arasfimr71") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0klvv4bld5a115w0rcx6f8vmpy5bjxqsbdn6zwmh9f52jhdbsxy4") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0zrnmy778jnrg41sg93h2g7qagrk9rg49fhdxn824i1by7djgv6k") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0mw1gg13iiq21a6d2446h950cc2n20ksjlskscsvkw4rad951kz0") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0k6mgzzi1h6j947av7h6ankf919bssw824z49gjy1dkx25yaqj5q") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0k0w45y10p15aw0xw580nni82ahp091k2kak3mzd0ny246rr97ks") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.7") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "06n93gygzmwvhd7yj0iq6dqm895k2ab2x0kj56jqnp3ip8xkiyam") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.8") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1v15iqvbfvbimafygw2h384qclkl4nzyk4aina6rbx0j31z30y9c") (yanked #t)))

(define-public crate-logma-0.1 (crate (name "logma") (vers "0.1.9") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1hpi4zlb9kqlyw0kc2bcjjr9fb0zh91z7l7rz4ii17rmdlivrcha")))

(define-public crate-logmap-0.1 (crate (name "logmap") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0-pre.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "15f52ywi4763srfixfqqq91xvlh57z79fxnvc8v9brj5fh0zsr2j")))

(define-public crate-logmap-0.2 (crate (name "logmap") (vers "0.2.0") (deps (list (crate-dep (name "abox") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "core_affinity") (req "^0.5.9") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "owning_ref") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0-pre.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.39") (default-features #t) (kind 2)))) (hash "0wavkck2626gn68i6r973q3xbxv8562z26877ixmzh7vv32cbbmm")))

