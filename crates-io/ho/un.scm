(define-module (crates-io ho un) #:use-module (crates-io))

(define-public crate-hound-0.1 (crate (name "hound") (vers "0.1.0") (hash "01bx5zh7sdm8bp29hi6nf1mw1j07krn67zlbcjz596nmjy1wax17")))

(define-public crate-hound-0.2 (crate (name "hound") (vers "0.2.0") (hash "0ygpfajhinhswb45b5lfkm0k43j0bpblazq0sipwlqx0346r2xpq")))

(define-public crate-hound-0.3 (crate (name "hound") (vers "0.3.0") (deps (list (crate-dep (name "cpal") (req "^0.0.20") (default-features #t) (kind 2)))) (hash "1152prsw04wbdb91nnyxl39amv3b9cskpn1pvzyf200c60ry0qmf")))

(define-public crate-hound-0.4 (crate (name "hound") (vers "0.4.0") (deps (list (crate-dep (name "cpal") (req "^0.0.20") (default-features #t) (kind 2)))) (hash "0900i14g0fdnp29fgsb225kn6b6y7zafznnba50gw3x0mvqgvyq8")))

(define-public crate-hound-1 (crate (name "hound") (vers "1.0.0") (deps (list (crate-dep (name "cpal") (req "^0.0.20") (default-features #t) (kind 2)))) (hash "0fpy6xw8zlyb960y820z0mz90k34avg1mnvf7vlpnzv0277d8xw6")))

(define-public crate-hound-1 (crate (name "hound") (vers "1.1.0") (deps (list (crate-dep (name "cpal") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1a2nfs282d59rf1fwjmyb0ja6jdq8kkimbn6vdlih6i5mn6azckw")))

(define-public crate-hound-2 (crate (name "hound") (vers "2.0.0") (deps (list (crate-dep (name "cpal") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "03xxag5ypqwm4qazwlybzh0q3ch88laxjvw0cvfcrlq7cbi1z5qy")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.0.0") (deps (list (crate-dep (name "cpal") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0c25k7rwgwr0bp9i7ffkqsigyb4kbnwa6x8n418pyk0v8y4y1k6b")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.0.1") (deps (list (crate-dep (name "cpal") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1gpkb4vga89n0qa6sqms963vcarqrczq5pnkw7fh576q50w4cr04")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.1.0") (deps (list (crate-dep (name "cpal") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "= 0.2.0") (default-features #t) (kind 2)))) (hash "1v32wl159flpr87wg1ihgkq5xl8qcaibx5v28mwcagsrmhvbhlfp")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.2.0") (deps (list (crate-dep (name "cpal") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "1f9bg04xdsy2wpkvfnbzzf53dki3szhis4gasby1r44j8spfx0c7")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.3.0") (deps (list (crate-dep (name "cpal") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "1s8vxy7g7ccvs2q2aavfrm1xzldsgk9x828nz7hcggszc4drpkig")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.3.1") (deps (list (crate-dep (name "cpal") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "1q50z1i47bqdwl9bxiaqd6c8bn74rip0k583av5hbsj10k1r7p73")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.4.0") (deps (list (crate-dep (name "cpal") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "0jbm25p2nc8758dnfjan1yk7hz2i85y89nrbai14zzxfrsr4n5la")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.5.0") (deps (list (crate-dep (name "cpal") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "1cadkxzdsb3bxwzri6r6l78a1jy9j0jxrfwmh34gjadvbnyws4sd")))

(define-public crate-hound-3 (crate (name "hound") (vers "3.5.1") (deps (list (crate-dep (name "cpal") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "0kw5yybfc7hdwxwm6d3m3h4ms52fkw0n0zch35drb52ci2xsmbb2")))

(define-public crate-hounddog-0.1 (crate (name "hounddog") (vers "0.1.0") (hash "0ir4yq195wb14brni9879caw6qll4dq2ivmkc5n7ipbpj3jia0hr")))

(define-public crate-houndify-0.1 (crate (name "houndify") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1yybc95rq182bc1h77ylmfqjsxh7wcq51sfnacshkcjyf80igv0g")))

(define-public crate-houndify-0.2 (crate (name "houndify") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "19czm17h0hyyr7j3wnq56nby9slavkgr9w2h7mq589m6l5pwqcyc")))

(define-public crate-houndify-0.2 (crate (name "houndify") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0kzdzfm4b3jcm4mawy64c9kl7pcarg5j73rrk9fjsdjhb6z4paxj")))

(define-public crate-houndify-0.3 (crate (name "houndify") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0y39jj9mr1zp8p2844ng76lj9xi4q46drvrcbbxyx9ms9s6wkc9d")))

(define-public crate-houndify-0.3 (crate (name "houndify") (vers "0.3.1") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1hdwm3f945kb73knqd4259z7mx0fsj5czikay7vavmy70sxzvgq4")))

(define-public crate-houndify-0.3 (crate (name "houndify") (vers "0.3.2") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0jz7rmsszp5kf0h0fsjdwmsyqf8ar5lgzgc2dg9hnplngxabvljz")))

