(define-module (crates-io ho st) #:use-module (crates-io))

(define-public crate-host-port-pair-0.1 (crate (name "host-port-pair") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1722wgg9b17qgvlm3549v5j6bm4mksg78mlabg523lipcn1xqxkb")))

(define-public crate-host-port-pair-0.1 (crate (name "host-port-pair") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0369y5mz1550db3g0mh7v89vzbqq91qys00wszra0r2b37g8b0j2")))

(define-public crate-host_discovery-0.1 (crate (name "host_discovery") (vers "0.1.0") (hash "0q1mnk795ginl2ppk6z3shj1nm4155q83v3j4dk5my0cba69rd7q") (yanked #t)))

(define-public crate-host_discovery-0.1 (crate (name "host_discovery") (vers "0.1.1") (hash "1lhsnis01i82gqz55ham3kg8in5i8rx00llhz2sk5vdwlhx2ydqf") (yanked #t)))

(define-public crate-host_discovery-0.1 (crate (name "host_discovery") (vers "0.1.2") (hash "073b3z0lrmxz8llq7ybikbknsf2bmmn526v0cjwhps8079lllq19") (yanked #t)))

(define-public crate-host_discovery-0.1 (crate (name "host_discovery") (vers "0.1.3") (hash "0dm9ddklsrrmb4lqbrmkd9qwh1bgb5l9x9pgg995sb818x4244cc") (yanked #t)))

(define-public crate-host_discovery-0.1 (crate (name "host_discovery") (vers "0.1.4") (hash "0z04bf4w58g439mrgv0wynrj7wxvqk9vm4qwg9qcd3jzbnk5z7p0") (yanked #t)))

(define-public crate-host_discovery-0.1 (crate (name "host_discovery") (vers "0.1.5") (hash "0ap0113idwjcglqlpfb73lpxym4q3q1p41pjwzf5mx6bd2whxivq") (yanked #t)))

(define-public crate-host_discovery-0.1 (crate (name "host_discovery") (vers "0.1.6") (hash "0vrzp5l9x1dsil6240hqrh6sxm7svxnqdjiw9y6g2y1k7pqjqahq") (yanked #t)))

(define-public crate-host_discovery-0.5 (crate (name "host_discovery") (vers "0.5.0") (hash "1a3k370qmhp2yn6qrarnbwdqgbc718r8sr79m41sgb520c55rsw6") (yanked #t)))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.0") (hash "1zqasdzsycrk3dm646zapd3rqc2hjndqakbnhpkhgmmncw8hpfrg")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.1") (hash "1acyksgyjb667gwh8qk05wishz6bg78jvylqfcfhb5z7c1jk25m3")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.2") (hash "0il4p8pxd6imk5i3w3igkwlpw4vf78w3825m3lf5pinaikra5z50")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.3") (hash "0yimnclmrxw6bizk2bn560bh1mjw03c8vz49kiln18ghyxylmgf1")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.4") (hash "0p3jfr1bwfnhixd5hvzq25bp2vwr17wmfpxqxrfp0xlihzn01c9w")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.5") (hash "0y85ak5yfnqrvxdlk6v5fz0919b334l7hbgcyzpjvl1n9w358gqz")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.6") (hash "1q4ap8yxn2frv826y85lgckxc0y500j0x8kav5azb1c8hbadl502")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.7") (hash "0v7457wvmw19xcsac3py37imy70797095ll2zhijlp7kdajspyqq")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.8") (hash "0vmbnzy6h7dbw7813aihlahzjpy1gj4glv4ic46961r01bd7brxb")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.0.9") (hash "1qbi9kyiwy2xzzwl96wq33zl257qvyzyrpvsf17qvjm0j5rc77wq")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.2.0") (hash "0n2bmbzh2a26k569pi2gbn6wnv9dh6wc9zpf7hb5vyd4rp8nqchy")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.2.1") (hash "1rl3krqjf9hgaw5ga6lyb9pmk2safm35walzwwvfcymv8l2ascn8")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.2.2") (hash "1jkrxin1c2m2sw1pkgv2sgh9gygrgbnmj35skyqrv215wr8g3pi3")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.2.3") (hash "1b40lml9d4cbzd44rf9vfxqvj14lf1k8xngfys55f7g26a285izh")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.2.5") (hash "1mp05n95n7ngy65vfj2q11i8v1690gikv82lvwnhlk4jbl1mlgpq")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.3.0") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (kind 0)))) (hash "0ikdr4455dkffayzykvzjc1vr93k71m36jg4wxzcn0fny00rkyjh")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.3.1") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0yvysdx7fac35zvh2hf2pr2n896gb4x1k4akx758wy16lq7lcfir")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.3.2") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0vhnrq8xv64abab0vw65lfwjz50nv9s0s41gf2yj05ixjz91g9cw")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.3.3") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "133cx12zisw6ciksrcwza19j9q1499nm3k5knzlwdrjvh5km2mkh")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.3.4") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "152dr5wwn0xnfzay7f5j3s6hll84w2pvawa317b3lydi2sm9swbm")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.4.0") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "01xxqcsw1k2jgradpxrw7m5qqqskpc087h9rphw54g12nn0jqzn5")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.0") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0xw8rlcj1xrjjb204fs3bsb7d3wh4wdl1vcq9cw90rvavcqzwx2f")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.1") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "186xrzx8dm1pkdrdw4ryx0dfvnfsqly9b5pvw7gh7j7fj6a2b6da")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.2") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0s6gv1l0xarbasfgd2938b3pz3sdir6qmk4jfm6gflz8q3745pp4")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.3") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "124lx2qlnjvqxsqx3pip4zwv9wy7i3254av4d85hlf4wgg1avk6s")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.4") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0bb25b1palks45smkagnv24cr7i8cyg2dqncicfbna0vlmbjy9w3")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.5") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0dkgrwsqfyhv1yw5nnbc0zbbarqmrpms1yhr1221hy0d6br7daf6")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.6") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1zixzqdxbalg382h23jlxmgjz6smhzcij6fkdinqv2d36bq72j14")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.7") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1aln62mhl22h2hzbbzwxb158ph8a6cn37wggbiigiflxkslwxljp")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.8") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "11rbkla74ls6lzj56vyczsigmvrjybv3ng51pibvcm8cb0ygxcxm")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.5.9") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1y5rc9xir1lbn3qmlrfpjxra8gx2qyk67hgaf6xlp5aqshizk3d5")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.0") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1kz74jgr2iqj17vmv9akcwwz90l1c21pp8fkz32xxl3rlmmzp477")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.1") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1wd2hljbnjhixydlhnnnhkljhz8zgajcgnl0ynh3kvh2ssh7jccj")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.2") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1s370rl8wa6ggjcx06zsz90bnzrk122c07d5r4wj2iap6aq1ng0g")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.3") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0jsp6xhxwklx4i4jmgadksw1hpxmqmmj35d3dbqwrcdvq5fxa54z")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.4") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "17vpi89v4cf5zmm5isrccaxbrpvxv7i9m7dqf9nj0afj0bhx2m6p") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.5") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "03mj2lz959h1yhxk942y2brdzkw9hvknkmdqq68d3w2mw0kcm5db") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.6") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0bipar0a6vq6kclzwf1m6h9k76p81hdv99cha9nj876m7xb48phh") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.7") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ydzrh8jbd06l3m4kyv8y3rzv1whlym96d8w3wym8vjlf3wjhpb9") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.6.8") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0fh0l5za0vma8qaxnsa02y3v6yg6l6dhrbs2wp68rgr4v24d7i5z") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.7.0") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ghnchmm1h5f5r9y3a1ci83hxcwbaa85ybj080i5hb400xlkzkh0") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.7.1") (deps (list (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "187nfsjxrml78naki13xlj5p6z6wnyvvxbpbrnlza82zdzi8fcpr") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.8.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "10sk6ikg5r7rdii3llvjr0bl7j2w5xjv2rip726by6j9dmv0hj36") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.8.5") (deps (list (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.52.0") (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "18glqvgjngk84p0gcy32cpwsav0nidm7nwhpa1fvsp7dipr5394r") (v 2) (features2 (quote (("windows" "dep:winreg"))))))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.9.0") (deps (list (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0vsq6k46pb8za0qg2m92qg6yqwsx6xvlgkdkx979jg89n909441r")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.9.1") (deps (list (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1cyvkai1n9vqa4l1nb3as2d1n1g4nk5svxiwbz1fzvvk91474zfz")))

(define-public crate-host_discovery-1 (crate (name "host_discovery") (vers "1.9.2") (deps (list (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.52.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1l8naa1z4s6ygvq953wyk2r3q2b81vsgsm2mj3j61gl7icgwxkwz")))

(define-public crate-hostcat-0.1 (crate (name "hostcat") (vers "0.1.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "13g3mxwmwbpdc9z1rq3lr14h283d3b0fcgf7yanw780yzd03pdg6")))

(define-public crate-hostcat-0.2 (crate (name "hostcat") (vers "0.2.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1xdhcw3ijx9hd9qhgmkcm9ipwllymw7yh2k8wccnpdjyih48ba7b")))

(define-public crate-hostcat-0.3 (crate (name "hostcat") (vers "0.3.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1hn0qpngcyx9ci5a1igmybasgy66rj13yldcl9m38ffg32p2n8z4")))

(define-public crate-hostcat-0.4 (crate (name "hostcat") (vers "0.4.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1ijg5bqkpzr2p521v6mpy7x2bv8qxlxqwp9f1vsb5bcdgjd0w6k1")))

(define-public crate-hostcat-0.5 (crate (name "hostcat") (vers "0.5.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)))) (hash "1sq988nb987zm92rq5s71q7kw5i4w38ga030zk70smzbp91q7vy0")))

(define-public crate-hostcat-0.5 (crate (name "hostcat") (vers "0.5.1") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)))) (hash "0rr09ry93ylkpx3f198rx83j70lq291fg8cpvxq7ixyazv0wfvbw")))

(define-public crate-hostcat-0.5 (crate (name "hostcat") (vers "0.5.2") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1ypg88mm5lj6d7i7fp64y7nsw883ahchzms96m0wlcsj5jvgcjza")))

(define-public crate-hosted-git-info-0.1 (crate (name "hosted-git-info") (vers "0.1.0") (deps (list (crate-dep (name "claim") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "derive_builder") (req "^0.10.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "095gv8mn6hf9pmg4cwa8hrai9ddb3zrq0z56x0ndr9ygf023682d")))

(define-public crate-hosted-git-info-0.1 (crate (name "hosted-git-info") (vers "0.1.1") (deps (list (crate-dep (name "claim") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "derive_builder") (req "^0.10.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1qrrqqw1ni38xpv0m2m3bjhb3nfrhjdsnb6k89hizcnmrw9dhyy7")))

(define-public crate-hosted-git-info-0.1 (crate (name "hosted-git-info") (vers "0.1.2") (deps (list (crate-dep (name "claim") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "derive_builder") (req "^0.10.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0l49p4zcb7hmr0rb4kvpca8dsna3byd0bv03dq6kmlnd2n78zl6l")))

(define-public crate-hoster-0.1 (crate (name "hoster") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1.9.0") (features (quote ("const_generics" "const_new" "union"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0vb00k6s7yc5idfmds9r5vh9rp6dy51kxdq7gsafqn7q007x96ki")))

(define-public crate-hoster-0.1 (crate (name "hoster") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^1.9.0") (features (quote ("const_generics" "const_new" "union"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1l2cqdc26gsnparfmc7rv402q1g6akfih679lmy251pjx43s3crx")))

(define-public crate-hostess-0.1 (crate (name "hostess") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "13qrl1daw7v7dpa2bqrvkc95q3w1qwrfzdxafxv1ax4i0sbvjy30")))

(define-public crate-hostess-0.1 (crate (name "hostess") (vers "0.1.0-beta") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "12nna8z7hkkwxmfdfjrlvp5w2yjswad658xca613wbadhspy55q8")))

(define-public crate-hostess-0.0.1 (crate (name "hostess") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0p1q84lql9k7fw6bmazpp69mwxx52h186ipzgkq00isf0spg9kbs")))

(define-public crate-hostess-0.2 (crate (name "hostess") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1x62s4arslgkql7jafrawccbb1g3vvfi80ps2nvam8pr1n02pjar")))

(define-public crate-hostess-0.2 (crate (name "hostess") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0a8610hkav8k5q545h4bc07jk0lyxj2w4hfhf5yspshdsvs2ghss")))

(define-public crate-hostess-0.2 (crate (name "hostess") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "050dqrz0rsklxr78h15bf96h0v658xq2gsz690bvgs31yrnjj74c")))

(define-public crate-hostess-0.2 (crate (name "hostess") (vers "0.2.4") (deps (list (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0xhrf89niksihmvncmnp0687s3hd0vkqfhvls15z6mr5kw7wjirg")))

(define-public crate-hostess-0.2 (crate (name "hostess") (vers "0.2.5") (deps (list (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0a0q0sq42lc9nc6li7xj6m0qcjvjy0bl9v8gxqq37kak3pbvamfm")))

(define-public crate-hostess-0.2 (crate (name "hostess") (vers "0.2.6") (deps (list (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "11jkb59hjdqqj473yvs9jdzglicgjy505rnj63nyb333xsayidxz")))

(define-public crate-hostfile-0.1 (crate (name "hostfile") (vers "0.1.0") (deps (list (crate-dep (name "mktemp") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0ksyyizgfar7wp5x8g0vn7ir17rpsa7k6xxh9qp2j5959ny14wk9")))

(define-public crate-hostfile-0.2 (crate (name "hostfile") (vers "0.2.0") (deps (list (crate-dep (name "mktemp") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0n6nc0ckcjwhxwv0qk0w53bqdx2rm254vijhnhahlmf0ph334wy2")))

(define-public crate-hostfile-0.3 (crate (name "hostfile") (vers "0.3.0") (deps (list (crate-dep (name "mktemp") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "034y75axgbdkvrqzf523n6vq34q9r86nd1dhk2qbs1kgr53xhbyp")))

(define-public crate-hostfxr-sys-0.1 (crate (name "hostfxr-sys") (vers "0.1.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0djqp0x524bnid77b6hls8qkk4764xn0r4pw1is2xzfzb8vgfgdi")))

(define-public crate-hostfxr-sys-0.1 (crate (name "hostfxr-sys") (vers "0.1.1") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "03sqy7brrx208w2bfyhyh3byrjgblacjz42b303gfydlxcnprd7h")))

(define-public crate-hostfxr-sys-0.1 (crate (name "hostfxr-sys") (vers "0.1.2") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "09yapv9siilzzcwylkzpyyya0sx1h3vh8wq9vca441ym98i5ri3v")))

(define-public crate-hostfxr-sys-0.1 (crate (name "hostfxr-sys") (vers "0.1.3") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1n7pwif5lpcdnicglsfm74sg9ixd56xd13c2vrd77xi4r0vipfq4")))

(define-public crate-hostfxr-sys-0.1 (crate (name "hostfxr-sys") (vers "0.1.4") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rm70xd8ixvania4abmdsq8mc2sjd8qp6gmm9a6cdshij7ndm0yj")))

(define-public crate-hostfxr-sys-0.2 (crate (name "hostfxr-sys") (vers "0.2.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0s52rm4qamqpb2a0x9a9rxmca00mhr8qqwdhgx2sl2rn4hrbs8vb") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("doc-cfg") ("default" "netcore3_0"))))))

(define-public crate-hostfxr-sys-0.2 (crate (name "hostfxr-sys") (vers "0.2.1") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pv9nlyjmvsdjv6crq29cggkb34yzkdj9yaqdfsb9zgm3bf6x6f4") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("doc-cfg") ("default" "netcore3_0"))))))

(define-public crate-hostfxr-sys-0.3 (crate (name "hostfxr-sys") (vers "0.3.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sphqvxw3qq6d516dvb9dpjlqmz66gs582r49c9y3zv104898vgk") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("doc-cfg") ("default" "netcore3_0"))))))

(define-public crate-hostfxr-sys-0.3 (crate (name "hostfxr-sys") (vers "0.3.1") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dqfbyhbcsbqy1xxl9pfx0pxrhlyhkwhmqwx14x22dmdr16y4pq7") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("doc-cfg") ("default" "netcore3_0"))))))

(define-public crate-hostfxr-sys-0.4 (crate (name "hostfxr-sys") (vers "0.4.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wn402n44rym9v2c4bmdmzxhrda26ff03dsds008m1dn44x2xlw3") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("doc-cfg") ("default" "netcore3_0"))))))

(define-public crate-hostfxr-sys-0.5 (crate (name "hostfxr-sys") (vers "0.5.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.4") (default-features #t) (kind 0)))) (hash "035cxdqgq9k4ksmvzvxp4bqk2my6yiyd36w22dq9zqyakbzhwx6z") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("doc-cfg") ("default" "netcore3_0"))))))

(define-public crate-hostfxr-sys-0.6 (crate (name "hostfxr-sys") (vers "0.6.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.5") (default-features #t) (kind 0)))) (hash "12mjc9vsbkv4fcq62kry78c8514czhr1x3ydj5bdggys9msx3fi3") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("doc-cfg") ("default" "netcore3_0"))))))

(define-public crate-hostfxr-sys-0.7 (crate (name "hostfxr-sys") (vers "0.7.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.5") (default-features #t) (kind 0)))) (hash "081z2m432mfv4chq7b9xph2pyhw8zdg90pcph0d1s5w19pqsxskc") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("net8_0" "net7_0") ("net7_0" "net6_0") ("net6_0" "net5_0") ("net5_0" "netcore3_0") ("doc-cfg") ("default" "net6_0")))) (yanked #t)))

(define-public crate-hostfxr-sys-0.7 (crate (name "hostfxr-sys") (vers "0.7.1") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.5") (default-features #t) (kind 0)))) (hash "14jl2nlfghq2nlv6qfjqla6sdck2ff30mrjs2s5v422vifb8xibq") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("net8_0" "net7_0") ("net7_0" "net6_0") ("net6_0" "net5_0") ("net5_0" "netcore3_0") ("latest" "net8_0") ("doc-cfg") ("default" "net6_0"))))))

(define-public crate-hostfxr-sys-0.7 (crate (name "hostfxr-sys") (vers "0.7.2") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.5") (default-features #t) (kind 0)))) (hash "0fg8k7d8v2n9fjn9yf66kyjqy21rpkyfaf42m64hh2520qh1q4l8") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("net8_0" "net7_0") ("net7_0" "net6_0") ("net6_0" "net5_0") ("net5_0" "netcore3_0") ("latest" "net8_0") ("doc-cfg") ("default" "net6_0"))))))

(define-public crate-hostfxr-sys-0.8 (crate (name "hostfxr-sys") (vers "0.8.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.5") (optional #t) (kind 0)))) (hash "1rbdq35j23fpwhxsgqh47jg0f60v7iay5l9jiyhr9wckv39gf34f") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("net8_0" "net7_0") ("net7_0" "net6_0") ("net6_0" "net5_0") ("net5_0" "netcore3_0") ("latest" "net8_0") ("doc-cfg") ("default" "net7_0"))))))

(define-public crate-hostfxr-sys-0.9 (crate (name "hostfxr-sys") (vers "0.9.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.6") (optional #t) (kind 0)))) (hash "160gy6izc3gkfhjlx0nibqvmjsdc1qvslay7kfj6dr7936xnpvlr") (features (quote (("undocumented") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("net8_0" "net7_0") ("net7_0" "net6_0") ("net6_0" "net5_0") ("net5_0" "netcore3_0") ("latest" "net8_0") ("doc-cfg") ("default" "net7_0"))))))

(define-public crate-hostfxr-sys-0.10 (crate (name "hostfxr-sys") (vers "0.10.0") (deps (list (crate-dep (name "coreclr-hosting-shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dlopen2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.6") (optional #t) (kind 0)))) (hash "1fsq84j89f7giny0ikmchmbrjsbvsdmpfz9fx45j9q6ya8hlf0sz") (features (quote (("wrapper") ("undocumented") ("symbor") ("optional-apis") ("netcore3_0" "netcore2_1") ("netcore2_1" "netcore2_0") ("netcore2_0" "netcore1_0") ("netcore1_0") ("net8_0" "net7_0") ("net7_0" "net6_0") ("net6_0" "net5_0") ("net5_0" "netcore3_0") ("latest" "net8_0") ("doc-cfg") ("default" "net7_0" "symbor" "wrapper"))))))

(define-public crate-hostile-0.1 (crate (name "hostile") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "04l03pyzhpkq0l4zyraaqncssvqc81yn59avvv568mdf4ggnci7y") (yanked #t)))

(define-public crate-hostile-0.1 (crate (name "hostile") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0bwpl97b344k2hax6ayafi7a74rkg3x96dsrdhd2b0czshqnsg17") (yanked #t)))

(define-public crate-hostile-0.1 (crate (name "hostile") (vers "0.1.2") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0fsb8069qa7hrcnwrly6nsypqfrj60y2ryprcn24k62s2a95v8vq") (yanked #t)))

(define-public crate-hostile-0.1 (crate (name "hostile") (vers "0.1.3") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1b86k1ahfivi950c0k39zqfalvqydb3q432828s6m440krpzg34q") (yanked #t)))

(define-public crate-hostile-0.1 (crate (name "hostile") (vers "0.1.4") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0ni8zagc5mb14gqwnpq62ds6n9rlcd8vyjfwc10wksq7wy3dhsw3") (yanked #t)))

(define-public crate-hostile-0.1 (crate (name "hostile") (vers "0.1.5") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "01bvy41bg51g0hykq3a90m30ccv0lpifd6hn18s2blh28bhf6a3g") (yanked #t)))

(define-public crate-hostless-0.0.0 (crate (name "hostless") (vers "0.0.0") (hash "10v287664078gmm0akkjjq3as7whna03rkjgw9hr7kng8w534jm2")))

(define-public crate-hostlist-0.1 (crate (name "hostlist") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^4.1") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "13cpnnzpwlsqgi9kkjw0q3n38s238450cbp6prxyhfrslk55k11v")))

(define-public crate-hostlist-0.1 (crate (name "hostlist") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^4.1") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "0kqiqvrgfnpq8w83kg224hj4c82fhdhw58vd41225kjlrcg3vww1")))

(define-public crate-hostlist-0.1 (crate (name "hostlist") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^4.1") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "189hwd7p6969dzlhdmdl1syiwa2j3hcrwxcxjbmsxj806jdjnb0h")))

(define-public crate-hostlist-0.2 (crate (name "hostlist") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^4.1") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "09didbcymppwx2pc7krm1si3q10yiajdzsibgmx94q6wxpa9fz7a")))

(define-public crate-hostlist-0.2 (crate (name "hostlist") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^4.1") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "03vnrg9hlmsa9y8fagxgaf8n77yb51ymjcwqq9dk1j2vcckhaysz")))

(define-public crate-hostlist-0.3 (crate (name "hostlist") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)))) (hash "021qbn5xg0m0dndp3967mqgrc0671c4xhvv2fhlndvp7fcjp3z6g") (features (quote (("default" "clap"))))))

(define-public crate-hostlist-0.3 (crate (name "hostlist") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)))) (hash "0vixwyw5l4mngkafa8d36z6g4jm1ay9mjhsn61z7gm7df32ay6w1") (features (quote (("default" "clap"))))))

(define-public crate-hostlist-parser-0.1 (crate (name "hostlist-parser") (vers "0.1.0") (deps (list (crate-dep (name "combine") (req "^4.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "03z9i1vm757ics5svvlyngxylkb17sslyy42vm3vw047a6vpcn5s")))

(define-public crate-hostlist-parser-0.1 (crate (name "hostlist-parser") (vers "0.1.2") (deps (list (crate-dep (name "combine") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "042il2zrlxmcibp79ns5rdvllbi8rz2ygzzy85d6p5arwjvazvnr")))

(define-public crate-hostlist-parser-0.1 (crate (name "hostlist-parser") (vers "0.1.3") (deps (list (crate-dep (name "combine") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)))) (hash "02mkn7a10727c8fygr2hsldfhvlrxxil9qlpbs2bx5gc6mlyhx5y")))

(define-public crate-hostlist-parser-0.1 (crate (name "hostlist-parser") (vers "0.1.4") (deps (list (crate-dep (name "combine") (req "^4.6") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.12") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "1amnv00sdfc90qnvgqjqxzzmc9rf4h6n6wdyd3q50b9j3s2qki5m")))

(define-public crate-hostlist-parser-0.1 (crate (name "hostlist-parser") (vers "0.1.5") (deps (list (crate-dep (name "combine") (req "^4.6") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.12") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "1d6n7maz19rfnxni0kfi7lh8jlmvrm6wskn6hl399sycsqnnfpv6")))

(define-public crate-hostlist-parser-0.1 (crate (name "hostlist-parser") (vers "0.1.6") (deps (list (crate-dep (name "combine") (req "^4.6") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)))) (hash "14c7l4pdj7p0g64iw7jbh9qlph7r0x19f06pipfksnyi8lz6a49s")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0q4nwvvgvf1fi4bmn4nqn78w56g6ikij0qvrshskxan81rwh6lkd")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1kig2bssm77fjl29y0q5rdbdv21hlnd7wbl40scfnlzrm5hx8amd")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1mf4vv115qz7w58n6gcs3bj7m9s3ikp1dym43zf58707kq07csrp")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1wyr1x6rp51xl21knzvmjpg951xcgmhp7v13rb3cbckbjb4m0pkd")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1z7aq22ldyabw5lp21bjx9j93976ylnw37zg4mwxbpz8lg0fbxrq")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "15fskcg11hwsacf539c791qg2pvd905yvfmzjpkim4n2a1njqxy9")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0051r3i5di85qvjpkm2kh280l3y3li06jqdsrvv8rh4nwxik5hdj")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "147m5jjnwvfs8q57rw79m8n8xdv8jmzzhg1yxz327hz81r6ydah1")))

(define-public crate-hostman-0.1 (crate (name "hostman") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0by1f6lyx209r1nswsdbd2wrpwrl4p2wnf719mb1qj2wf2hw3mvk")))

(define-public crate-hostman-0.2 (crate (name "hostman") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "08g40nq3if3x598b3dp00ws0b4zigwk4pb3sqmklikn2ikx50jx0")))

(define-public crate-hostman-0.3 (crate (name "hostman") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "self_update") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "17vap9sdwjd4ggfsp8rvbx77vxhkjngl1x9wyr6pkgmnk3hs3ddl")))

(define-public crate-hostman-0.3 (crate (name "hostman") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "self_update") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "05rzc48q0rq066pbdp7491sq1adw9cpcqggxrzr9y53czgikcjv1")))

(define-public crate-hostman-0.3 (crate (name "hostman") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "self_update") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0qv48vjrp7xvx3dpjcaxf66bcpnh1s8m4fw5ka2xlx5750i1c2f9")))

(define-public crate-hostman-0.4 (crate (name "hostman") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hosts") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "self_update") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "05xidssdi7qf134n8ipgv57ma9s2h2q9qjcdb96q5iqx2zmsmlqb")))

(define-public crate-hostman-0.5 (crate (name "hostman") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "hosts-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.0-alpha2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "self_update") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0l30wd24amrnnniwrrdl1bvc9i8mgwnm1mm72sg545qhm6v78dpp")))

(define-public crate-hostman-0.5 (crate (name "hostman") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "hosts-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "self_update") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1r8vb55zxhknk2zpi6m86gq8bvwp78iggn3kfrsl5km8g3kn3z5g")))

(define-public crate-hostman-0.5 (crate (name "hostman") (vers "0.5.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "hosts-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "self_update") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0mgnwm07fyzjrpb8y49bq90m47am3k7q8hfqsxgbplhnag7ndhib")))

(define-public crate-hostname-0.1 (crate (name "hostname") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winutil") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1jy4lfis3179l7kmwrr8x2rzrc1b5jsrqyq7qsiswjx2afc7dh2f") (features (quote (("unstable"))))))

(define-public crate-hostname-0.1 (crate (name "hostname") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winutil") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gfhad6gakmq3vxivqh91sfaglcd52fgsc7wv4dnh3i6h8w406l3") (features (quote (("unstable"))))))

(define-public crate-hostname-0.1 (crate (name "hostname") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winutil") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0k5a5c628165zm3qnn6lrjlvgpll8wpz2wh2x1c0n1yrry85hhif") (features (quote (("unstable"))))))

(define-public crate-hostname-0.1 (crate (name "hostname") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winutil") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0hvr2hq28w5177pgydk8rx7fzjlkgmh43hxq64f0177614az0rfq") (features (quote (("unstable"))))))

(define-public crate-hostname-0.1 (crate (name "hostname") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winutil") (req "^0.1.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1jyij48azryaxdd8iccjsyldgmjvmamlsjilrns0njs3fzhvdyjq") (features (quote (("unstable"))))))

(define-public crate-hostname-0.1 (crate (name "hostname") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(any(unix, target_os = \"redox\"))") (kind 0)) (crate-dep (name "winutil") (req "^0.1.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0kprf862qaa7lwdms6aw7f3275h0j2rwhs9nz5784pm8hdmb9ki1") (features (quote (("unstable"))))))

(define-public crate-hostname-0.2 (crate (name "hostname") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(any(unix, target_os = \"redox\"))") (kind 0)) (crate-dep (name "match_cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "185rylvalaqr2rbl3lss0hs12xgzxas7ynnadxmijxrqqvk60lnw")))

(define-public crate-hostname-0.3 (crate (name "hostname") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(any(unix, target_os = \"redox\"))") (kind 0)) (crate-dep (name "match_cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "11kqn05h0da5ka9d7m3k93vj1vcshw6zziir3kgak2q6dn6szc81") (features (quote (("set") ("default"))))))

(define-public crate-hostname-0.3 (crate (name "hostname") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(any(unix, target_os = \"redox\"))") (kind 0)) (crate-dep (name "match_cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0rz8yf70cvzl3nry71m4bz9w6x4j9kdz3qng6pnwhk2h20z1qwrw") (features (quote (("set") ("default"))))))

(define-public crate-hostname-0.4 (crate (name "hostname") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(any(unix, target_os = \"redox\"))") (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "windows") (req "^0.52") (features (quote ("Win32_Foundation" "Win32_System_SystemInformation"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1fpjr3vgi64ly1ci8phdqjbha4k22c65c94a9drriiqnmk4cgizr") (features (quote (("set") ("default")))) (rust-version "1.67")))

(define-public crate-hostname-validator-1 (crate (name "hostname-validator") (vers "1.0.0") (hash "0af10mlyz4iwgbafpsjifj0lwyglliydwz2jy0skmxnr92wvrf3h")))

(define-public crate-hostname-validator-1 (crate (name "hostname-validator") (vers "1.1.0") (hash "1m49mnav5pipz95xq6j6bpl6masb277miq0xj3lhr7xwr2aa255y")))

(define-public crate-hostname-validator-1 (crate (name "hostname-validator") (vers "1.1.1") (hash "1qh5sxkckalibc28029ndnfd7w0s8mwvb68d82xbb25gr55acn7m")))

(define-public crate-hostname1-zbus-0.1 (crate (name "hostname1-zbus") (vers "0.1.0") (deps (list (crate-dep (name "pico-args") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "zbus") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0m0g4r2ikqvj69a44wiyw972s9xqfcz7b0445x7mv36jmvnajyga")))

(define-public crate-hostparser-0.1 (crate (name "hostparser") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "governor") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("native-tls" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tldextract") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1m28g39x9cadzgqsjrdrwa0cqf6w1dv8xwvq4gijcqqk2j4lh15j")))

(define-public crate-hostparser-0.1 (crate (name "hostparser") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "governor") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("native-tls" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tldextract") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rangh2m00g2bkgfdqnixg0c1989bxfafg0sa543mgzrcg404jq9")))

(define-public crate-hostparser-0.1 (crate (name "hostparser") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "governor") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("native-tls" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tldextract") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dzfa9by0hjp2lm4b8lbw062awyfrs2kq3sx9w04j5a7cyhkh1zm")))

(define-public crate-hostparser-0.1 (crate (name "hostparser") (vers "0.1.3") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "governor") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("native-tls" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tldextract") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rl6gd5f1hf94vrjs9z9c0c20djzwsyjwvhfpmw3c59j8allbjbi")))

(define-public crate-hostr-0.0.0 (crate (name "hostr") (vers "0.0.0") (hash "08w8pm8s8ss9zsdpfn97vqy2g3z8h9s5r772piq3r8jkdr0wzc5z")))

(define-public crate-hosts-0.0.0 (crate (name "hosts") (vers "0.0.0-alpha.0") (hash "1zq5vh38d7i5fg4c99s4y1d41bdfx58yasyv5vxj343kpxbbrr5m")))

(define-public crate-hosts-digger-0.1 (crate (name "hosts-digger") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0xxnyn6yiw3pbhz3ar3a7r4c10rrwp23ahdyk5h4ayywrryi0gfw")))

(define-public crate-hosts-parser-0.1 (crate (name "hosts-parser") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1vflvmh6kpzwp9r4zivqfna0m5rkdm3cv0qv6wl3izhk52aj932l")))

(define-public crate-hosts_to-0.1 (crate (name "hosts_to") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.23.2") (optional #t) (kind 0)))) (hash "1ngqfg6a7l2a100b3srrw0cvlx7yiq6s2p85186c82pfr2df0mvw") (features (quote (("default")))) (v 2) (features2 (quote (("trust-dns-proto" "dep:trust-dns-proto")))) (rust-version "1.60")))

(define-public crate-hosts_to-0.1 (crate (name "hosts_to") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.23.2") (optional #t) (kind 0)))) (hash "02jfj2f5vzdbwv7vg7f3arspypi3s1if9s7s7b0izq61kfa2df2q") (features (quote (("default")))) (v 2) (features2 (quote (("trust-dns-proto" "dep:trust-dns-proto")))) (rust-version "1.60")))

(define-public crate-hosts_to-0.1 (crate (name "hosts_to") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.23.2") (optional #t) (kind 0)))) (hash "18ckdrnzq9l471g0mz0qdijr2z7cnlaq5gp0ba4hgwybl13b09wd") (features (quote (("default")))) (v 2) (features2 (quote (("trust-dns-proto" "dep:trust-dns-proto")))) (rust-version "1.60")))

(define-public crate-hosts_to-0.1 (crate (name "hosts_to") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-proto") (req "^0.23.2") (optional #t) (kind 0)))) (hash "0hkaxx261wf85njxr0agzwr0lp8bqiymkbsaqxy5zrxn4ia6da1d") (features (quote (("default")))) (v 2) (features2 (quote (("trust-dns-proto" "dep:trust-dns-proto")))) (rust-version "1.60")))

(define-public crate-hostsmod-0.2 (crate (name "hostsmod") (vers "0.2.2") (deps (list (crate-dep (name "hostname") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "09s2iyzcfcd2im0npkkss3vgmpz21rasvwhsyvaic8ykifmjwmhg")))

(define-public crate-hoststatus-0.1 (crate (name "hoststatus") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fastping-rs") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "macros-rs") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.33") (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "tide-tracing") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1v3qijj1c1vhrvmvdvagxmi31iicq74qhj8yygvbvcia1fr7xznd")))

(define-public crate-hoststatus-0.1 (crate (name "hoststatus") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fastping-rs") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "macros-rs") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.33") (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "tide-tracing") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1aqksvr2g5h98nwbxa4b5flx3ijkd4azfh2z2895qwlgdz3n55q9")))

