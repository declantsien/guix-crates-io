(define-module (crates-io ho sh) #:use-module (crates-io))

(define-public crate-hoshi-0.1 (crate (name "hoshi") (vers "0.1.0") (hash "151izmxz8md3v573wf9z9ggid5rqvdyd15clrdd92f1iy6wqdfqw")))

(define-public crate-hoshii-0.1 (crate (name "hoshii") (vers "0.1.0") (hash "08l916vb5pgxkv99hif1y8d81j75l1k3h0cff7yvlyzihwk6v4p4")))

(define-public crate-hoshino-0.1 (crate (name "hoshino") (vers "0.1.0") (hash "03msbv0ppnxv2llky1h0253r9iz1bgf2qqf1zzrw2k98d6sdx3gl")))

