(define-module (crates-io ho ve) #:use-module (crates-io))

(define-public crate-hover-0.1 (crate (name "hover") (vers "0.1.0") (hash "0m8n35rkby9azbnv64hpwj8ww37cr3lcc0wi2681ar878iywnmjj")))

(define-public crate-hoverbear-0.1 (crate (name "hoverbear") (vers "0.1.0") (hash "0m0s60qrgkb54csvx0ql4qyysf3v7pw7pgm5h0m8b9ikcvwggssc")))

