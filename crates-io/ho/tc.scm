(define-module (crates-io ho tc) #:use-module (crates-io))

(define-public crate-hotcorners-0.1 (crate (name "hotcorners") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.21") (default-features #t) (kind 1)))) (hash "0scaxl2ganv8gkgn2sfj378fyvknb7nqsnk76wh4gdcn1i7lm0c5")))

(define-public crate-hotcorners-0.1 (crate (name "hotcorners") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.21") (default-features #t) (kind 1)))) (hash "0806p43m0nvcfdj2lmgmw8w2z15nm7arcyhvigrwp8p9n1bbf1nr")))

(define-public crate-hotcorners-0.1 (crate (name "hotcorners") (vers "0.1.2") (deps (list (crate-dep (name "windows") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.21") (default-features #t) (kind 1)))) (hash "04ndp7v6rxv71c1v65y9bingqxnb5wxcdxvl8z3b9zbxjclcsfv5")))

(define-public crate-hotcorners-0.2 (crate (name "hotcorners") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.22") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_KeyboardAndMouseInput" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "0wasdfd389bjdx70115cjfskkrmjfznjl83sv9gn8pncv9j5rrvd")))

(define-public crate-hotcorners-0.2 (crate (name "hotcorners") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.22") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi" "Win32_UI_KeyboardAndMouseInput" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "0qrbfhr2jl3jf2mp0mf2g7pvkd8sj6vslk2lqdld91kkn7p3cf2r")))

