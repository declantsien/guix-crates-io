(define-module (crates-io ho la) #:use-module (crates-io))

(define-public crate-hola-0.1 (crate (name "hola") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0prk8l800cmqcgig3srfxnrxflsgk4i336p4ccdjisspgmrzz09k") (yanked #t)))

(define-public crate-hola-0.1 (crate (name "hola") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03kavxh22n1912q43slriwdvcwg91cz9gjg544y5mwyfc8gchpdv")))

(define-public crate-hola_mundo-0.1 (crate (name "hola_mundo") (vers "0.1.0") (hash "0rjxkhay9ncr4bf0p0f6bhks2mg051iw31nhq7g2h2bfz0s2ifxp")))

(define-public crate-hola_mundo-0.1 (crate (name "hola_mundo") (vers "0.1.1") (hash "0wgsm0frrqnrqq6aj2sacdkzsdf9b0wbaf9kac7fx391magpqw9j")))

(define-public crate-holaq-0.1 (crate (name "holaq") (vers "0.1.0") (hash "02y4c9j8pddgn24qxq22cy67lgl8n751la22zpccx5pmn49pd715")))

