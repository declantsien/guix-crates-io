(define-module (crates-io ho ur) #:use-module (crates-io))

(define-public crate-hour-0.0.0 (crate (name "hour") (vers "0.0.0") (hash "1wi3yv8bsnd435i9hqr0x1yms5sg0jlf824y4g2jy8cjpcnkh5cx")))

(define-public crate-hour-0.1 (crate (name "hour") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "0gcinn2nrxcgk18pvczzvdi2zcj9xf22jpc5cssmvd51sc5b15zx") (features (quote (("with-num-traits" "num-traits") ("with-chrono" "chrono") ("default" "with-chrono"))))))

(define-public crate-hourglass-0.4 (crate (name "hourglass") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "1agpgkkrxg28kn5gyap7gaffz49dm4ykr7nqigk21rm7jindjcgg")))

(define-public crate-hourglass-0.4 (crate (name "hourglass") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "0q5340yg4i3vnhysn16yfivama4fazc8fcdpns4n5cr28niisijs")))

(define-public crate-hourglass-0.4 (crate (name "hourglass") (vers "0.4.4") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "00dhxhaxl4kwp8wzw7cy6cb3fhwk4q16ynjjxj71gpds17lm1qyb")))

(define-public crate-hourglass-0.5 (crate (name "hourglass") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gph4p4y72rcm8silbymbfjfkrqgx9cavd829ff2zc17vqn6ha4l")))

(define-public crate-hourglass-0.6 (crate (name "hourglass") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1q29n3jljg8869giblqyb3m0snm6l3s9p8h5i35a0995lzs337qm")))

(define-public crate-hourglass-0.6 (crate (name "hourglass") (vers "0.6.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "15wnpfra9drgajc8dlh8sr8qjn9xbz8jdfx693qv4i160b6kb3z8")))

(define-public crate-hourglass-0.6 (crate (name "hourglass") (vers "0.6.2") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "09l0lm95gi80239b0j8my22sh45lqjx88mdci1d68hjkp9p78a6f")))

(define-public crate-hourglass-0.6 (crate (name "hourglass") (vers "0.6.3") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0avgb7ljgdxj69dvxsy6y9x5fxv2r4xrfakkvlygr6hlhsxbb0na")))

(define-public crate-hourglass-0.6 (crate (name "hourglass") (vers "0.6.4") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1rw44df0gx0avgp1bw6f1gs8y2yj4mdzxw6sgf36xhpind48d626")))

(define-public crate-hourglass-0.6 (crate (name "hourglass") (vers "0.6.5") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0y3mqajbaf43lj3r6iwlbdkxgrasja5l3s393cixkf5md4wixdw8")))

(define-public crate-hourglass-0.7 (crate (name "hourglass") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0jxb4pxqb8dx7prra9i4r5v5hdhiaw4f02hxmfhs55kr94z0fizr")))

(define-public crate-hourglass-0.7 (crate (name "hourglass") (vers "0.7.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "05k6ps7x15g8637znvyc4ki4600vgy5x869q8a5z45wbbny0i6kx")))

(define-public crate-hourglass-0.8 (crate (name "hourglass") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "01vgm1w4zl7ia4v1v65hyhayv3vwzvsr7mhzdxhpxldigrqv76iw")))

(define-public crate-hours-0.0.1 (crate (name "hours") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "050346dqrprj97wf2hgkq18ilyrivldakis81lbd2y1irij6fnbq")))

