(define-module (crates-io ho yo) #:use-module (crates-io))

(define-public crate-hoyomi-0.1 (crate (name "hoyomi") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.40.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_ec2") (req "^0.40.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "0lnbj35qzzw6aw1vkx67lipx6njcss1h3r9xg34z5infsiz58g3x")))

