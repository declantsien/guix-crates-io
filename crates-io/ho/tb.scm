(define-module (crates-io ho tb) #:use-module (crates-io))

(define-public crate-hotboot-0.1 (crate (name "hotboot") (vers "0.1.0") (deps (list (crate-dep (name "openssl") (req "^0.9.7") (default-features #t) (kind 0)))) (hash "1nxyj54wzfry33d8w540ynfpfpvpmn4my1y4d9y9cafr41qcw1rg")))

(define-public crate-hotboot-0.1 (crate (name "hotboot") (vers "0.1.1") (deps (list (crate-dep (name "openssl") (req "^0.10.29") (default-features #t) (kind 0)))) (hash "1vd19zkz25x4sbw9ll4kbq8kgq7a1c5g3k9pldfvky2namaklanr")))

