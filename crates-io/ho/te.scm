(define-module (crates-io ho te) #:use-module (crates-io))

(define-public crate-hotel-0.1 (crate (name "hotel") (vers "0.1.0") (hash "12lq4mzvl2whb7w7zqd2kspln62467lndhz1l2xx9pk5gxqp87g6")))

(define-public crate-hotel-0.1 (crate (name "hotel") (vers "0.1.1") (hash "09sanfzlbkyj3cnrx5aga985ch0b7phm6yr3xr6v9y7m81zfglzb")))

(define-public crate-hotel-0.1 (crate (name "hotel") (vers "0.1.2") (hash "0zlw064y9fsw4mjsani68640jq7g06xgw5gk5c82qgr4kmmmpdxf")))

(define-public crate-hotel-1 (crate (name "hotel") (vers "1.0.0") (hash "0zq4al9rni673gx3dr24z7lk9b2dcvzivnnkwhsj5n5jdawncs6x")))

(define-public crate-hotel-1 (crate (name "hotel") (vers "1.0.1") (hash "15v4x4q6xb7931bdw0rj3kpg60fj5bi0jxs6bgf574a2xaja75kd")))

(define-public crate-hotel-1 (crate (name "hotel") (vers "1.0.2") (hash "1ynjg97qy9y7zy7b3vad12m6pawci92lcj11wnc3izfbs9jkw74f")))

(define-public crate-hotel-1 (crate (name "hotel") (vers "1.0.3") (hash "013yixyd7iwbkvcdzgpp8zrsx8gjjrysdpy04yzyz0cv6yigqyjy")))

(define-public crate-hotel-1 (crate (name "hotel") (vers "1.0.4") (hash "1gydn4nldfnsdv9yih09rvkfs598g03wn9bf99dcccpym1fcrqmh")))

(define-public crate-hotel-1 (crate (name "hotel") (vers "1.0.5") (hash "0ggiwrs3ifz8wj71h7kbs266wxswyh7ryyrjxprx7wk3r93lbyb8")))

(define-public crate-hotelier-0.1 (crate (name "hotelier") (vers "0.1.0") (hash "1i45f534jnvxyf0s6nvkvv5b9pprxcsn52n0m5fg5gy369n4pmxf")))

