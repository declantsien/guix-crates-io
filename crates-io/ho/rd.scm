(define-module (crates-io ho rd) #:use-module (crates-io))

(define-public crate-horde-0.0.1 (crate (name "horde") (vers "0.0.1") (hash "0f7l4p9aa38spjmgmam53q7xjdfsligrmw9732w5x7x05ii7q10r")))

(define-public crate-horde-0.1 (crate (name "horde") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0qk0vwpd9k82hhz5r6gamrgzbrnr1hcs7789qp5kzjmahacdjgma")))

(define-public crate-horde-rs-0.1 (crate (name "horde-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1yill9ca58iipxxyv9zna07xhh6xygw2hjplbwdyykdsw7hk6zlq")))

(define-public crate-horde3d-sys-0.1 (crate (name "horde3d-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "15jqixzs3bh13z7zbcxy227yxghci4245yf67k8da5m4x5bh340l")))

(define-public crate-horde3d-sys-0.1 (crate (name "horde3d-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1bybw7dhk81v5v9byykqsbrh8x651c27byr5agjjj5p06ggp4py8")))

