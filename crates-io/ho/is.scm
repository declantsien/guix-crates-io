(define-module (crates-io ho is) #:use-module (crates-io))

(define-public crate-hoist_temporaries-0.1 (crate (name "hoist_temporaries") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "05g5m1vqh08vwyma7yh6bcq3yvbigbp2h0adnbhvdl7jgdhrg061")))

(define-public crate-hoist_temporaries-0.2 (crate (name "hoist_temporaries") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1zgf1w7am7634v0db9xnxlxjqg3kg7c6b93xafhkyh53cdfcn3hl")))

(define-public crate-hoist_temporaries-0.2 (crate (name "hoist_temporaries") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0jfa9malc33g5f9x065pxn2alplrgdj0nv63hgy3wh88c6mgpr1p")))

