(define-module (crates-io ho dg) #:use-module (crates-io))

(define-public crate-hodgepodge-0.1 (crate (name "hodgepodge") (vers "0.1.0") (deps (list (crate-dep (name "strum") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "0d6x7xc71k70dz3xa1chfnv8xvfkz2z9zn00csqq3l65n9spyfxf")))

(define-public crate-hodgepodge-0.1 (crate (name "hodgepodge") (vers "0.1.2") (deps (list (crate-dep (name "strum") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "1y4lrli0ac4lhfacwrsaj8pazim5kb04sbjd74ipnnl92vbq1vvx")))

(define-public crate-hodgepodge-0.1 (crate (name "hodgepodge") (vers "0.1.3") (deps (list (crate-dep (name "strum") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "1ghyc3d962vjrf8n9pb5s79r6n6b7d3qv904zqcjag8nh5ajv7j1")))

(define-public crate-hodgepodge-0.1 (crate (name "hodgepodge") (vers "0.1.4") (deps (list (crate-dep (name "strum") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "1d86kg0h2z6p5pih18k3wvfb3nbng34r45vg4lzkkydl3hxcm8hd")))

(define-public crate-hodgepodge-0.1 (crate (name "hodgepodge") (vers "0.1.5") (deps (list (crate-dep (name "strum") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "1lbpz9vmljc3kwfdmqc9jxn5yk97yg8api8v8a63fp91kjbqwlkv")))

(define-public crate-hodges-0.1 (crate (name "hodges") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y1ps66yh2dbl2bk7jv64vw3vch2zpylx31kak868j5g3xwwqw7k")))

(define-public crate-hodges-1 (crate (name "hodges") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ri366p32p5dh6943mrwa8a5pjbk9zj8caybjir4rnf9y3pxx6xa")))

