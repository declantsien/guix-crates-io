(define-module (crates-io ho gw) #:use-module (crates-io))

(define-public crate-hogwild-0.1 (crate (name "hogwild") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.11") (default-features #t) (kind 0)))) (hash "1a8s7qxflfixscg1c6sy63b1235h04xcwlj6yp3h46j5rkcllya2")))

(define-public crate-hogwild-0.2 (crate (name "hogwild") (vers "0.2.0") (deps (list (crate-dep (name "ndarray") (req "^0.11") (default-features #t) (kind 0)))) (hash "0yh63lnjj1ch41f99xrnrc944k8hy15n756bmsv2khwzsk0p5x2f")))

(define-public crate-hogwild-0.3 (crate (name "hogwild") (vers "0.3.0") (deps (list (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "1dwvrhnl6k369jk50infv9qf501427y3s8lyb3nakwnv4xbxjx8c")))

(define-public crate-hogwild-0.4 (crate (name "hogwild") (vers "0.4.0") (deps (list (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "102rmg1r9gc0ifcf9zkn4xrb1f7px4ca16lajhkk3ra590v1xss1")))

(define-public crate-hogwild-0.4 (crate (name "hogwild") (vers "0.4.1") (deps (list (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "0z9bvk3g9h1rj9xryw63zh04l7yy0wyl5y42gg4mn1k5bsasippd")))

(define-public crate-hogwild-0.5 (crate (name "hogwild") (vers "0.5.0") (deps (list (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "15kj8alkxm59akicl5ikkq1m7s1zb77m59gah5b9f3vgds03idpd")))

(define-public crate-hogwild-0.6 (crate (name "hogwild") (vers "0.6.0") (deps (list (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "09wbh7akn0am2lg2lggfr2bwbd3pv0im9k2ym698jmnmyaggfgv0")))

(define-public crate-hogwild-0.6 (crate (name "hogwild") (vers "0.6.1") (deps (list (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "1vfr95cz8iidzsbgpx3k1ix1kpla4bgjk6dpc3vgbzsxwd3i0rha")))

