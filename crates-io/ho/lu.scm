(define-module (crates-io ho lu) #:use-module (crates-io))

(define-public crate-holup-0.1 (crate (name "holup") (vers "0.1.1") (hash "0bp1wg209vd7yrfjw1sxd743bxnpgrws2ws1ljanbpg4ix64m192")))

(define-public crate-holup-0.2 (crate (name "holup") (vers "0.2.0") (hash "0x1bjhsm1pmglfjdph2hjydh92vv3ri7fij2np0pvbzpf8pmwryx")))

