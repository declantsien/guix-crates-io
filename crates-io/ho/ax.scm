(define-module (crates-io ho ax) #:use-module (crates-io))

(define-public crate-hoax-1 (crate (name "hoax") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hmsylriz8wmcdll3xg8q6cpqjqkbbxylbfz3k4x6v510yjqfl0z")))

