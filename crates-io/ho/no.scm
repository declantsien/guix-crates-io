(define-module (crates-io ho no) #:use-module (crates-io))

(define-public crate-hono-0.1 (crate (name "hono") (vers "0.1.0") (hash "1cd54irpj0xz631c2zhsbkqi612p7y2nlxps9qniz6j9hy0i6nbn")))

(define-public crate-honor-0.1 (crate (name "honor") (vers "0.1.0") (hash "0ii8kiwyxs2niknh4ilhkr1l843i77iczpxlxisgszwzrfrxsfm0")))

