(define-module (crates-io ho ud) #:use-module (crates-io))

(define-public crate-houdini-1 (crate (name "houdini") (vers "1.0.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("fileapi" "handleapi" "libloaderapi" "ntdef" "minwindef" "winnt" "minwinbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "09kdgaf0g7v07wipywyi4v9j3h6bqc4rhdwi6bh5p6qwzwsar5iy") (features (quote (("debug"))))))

(define-public crate-houdini-1 (crate (name "houdini") (vers "1.0.1") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("fileapi" "handleapi" "libloaderapi" "ntdef" "minwindef" "winnt" "minwinbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "19p0a1339phz4kiwsqbnf1pn396k8gyh9jpvrdyva1pd49n7j8my") (features (quote (("debug"))))))

(define-public crate-houdini-1 (crate (name "houdini") (vers "1.0.2") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("fileapi" "handleapi" "libloaderapi" "ntdef" "minwindef" "winnt" "minwinbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1vvyp5ri4r8v91f642crm3y36282s4gqm3qsds9ybsrivx3w08cs") (features (quote (("debug"))))))

(define-public crate-houdini-2 (crate (name "houdini") (vers "2.0.0") (deps (list (crate-dep (name "windows") (req "^0.46.0") (features (quote ("Win32_Foundation" "Win32_Storage_FileSystem" "Win32_System_LibraryLoader" "Win32_Security"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1m9sm5zq7ixxiyfzsyph4fz0rpyaac2mmrzd2app4znzi8jl1zk5") (features (quote (("debug"))))))

