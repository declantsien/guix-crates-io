(define-module (crates-io ho pa) #:use-module (crates-io))

(define-public crate-HOPA-0.1 (crate (name "HOPA") (vers "0.1.0") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "121g287hfikgzfv3v4pxj0ci93f72c0idnkd7xkxjn8b8lhzbzhp")))

(define-public crate-HOPA-0.1 (crate (name "HOPA") (vers "0.1.1") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17gpzp10ijm5qi4swvdd2pmqdg9xp3haihddgn2q115ldmyxx7n0")))

(define-public crate-HOPA-0.1 (crate (name "HOPA") (vers "0.1.2") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1m9r7w3a3f8wvn4gylwbai5zz4aa5vya49zhnmpb7lkvqx4abji0")))

