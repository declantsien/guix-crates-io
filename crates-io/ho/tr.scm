(define-module (crates-io ho tr) #:use-module (crates-io))

(define-public crate-hotreload-0.1 (crate (name "hotreload") (vers "0.1.0") (deps (list (crate-dep (name "notify") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1iggydqmh625cf5xx5qhq8ldjacq9xs7s9hpcmsmr6dbkp7h0gpc")))

(define-public crate-hotrod-0.1 (crate (name "hotrod") (vers "0.1.0") (hash "0ipmvjqfbayjady1hj6x7g8cln3jkh4b5arj88b2d9zp0r8xc0w1")))

