(define-module (crates-io ho rt) #:use-module (crates-io))

(define-public crate-horticulteur-1 (crate (name "horticulteur") (vers "1.0.0") (hash "1d6psfwskyip7dp51299m2qjlzcwvrnpwbk7xdwdqfn9pqxyz83j")))

(define-public crate-horticulteur-1 (crate (name "horticulteur") (vers "1.1.0") (hash "000f6i1aybhv3ajka1sbqdmql7fwm8vvlhpcv60czd11a4p2krgb")))

(define-public crate-hortus-0.0.0 (crate (name "hortus") (vers "0.0.0") (hash "19y78hkc6ki0dvzpfyxmzfbw4815dcp4bp120njcvil98qqyg7ih")))

