(define-module (crates-io ho cr) #:use-module (crates-io))

(define-public crate-hocr-parser-0.1 (crate (name "hocr-parser") (vers "0.1.0") (deps (list (crate-dep (name "roxmltree") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "11h0vbk7g9xhavdql2g05dln6rmf3ds1wikzshcp0w185qvibmyj") (v 2) (features2 (quote (("serde" "dep:serde"))))))

