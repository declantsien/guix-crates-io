(define-module (crates-io ho lp) #:use-module (crates-io))

(define-public crate-HOLP-0.1 (crate (name "HOLP") (vers "0.1.0") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "06lh8qzi1jc4xx9bnbl3xidrdnqx3zj2c4kppmnmr4gbzbsfv5v0")))

