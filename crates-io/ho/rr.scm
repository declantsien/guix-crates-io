(define-module (crates-io ho rr) #:use-module (crates-io))

(define-public crate-horrible-katex-hack-0.1 (crate (name "horrible-katex-hack") (vers "0.1.0") (hash "11313y0hkwyw2fqxpib7pa7nj0jq2jvv35kf8804a2clldv6nr4w")))

(define-public crate-horrible-katex-hack-0.1 (crate (name "horrible-katex-hack") (vers "0.1.1") (hash "03llddnzina8idl54z6m9c3n2j59wkakq0idmd0g2vw5g53czizw")))

(define-public crate-horrible-katex-hack-0.1 (crate (name "horrible-katex-hack") (vers "0.1.2") (hash "1ncjvsryp9dj5s4jmbi50gjasg06lhbb8sc0r1s4a34sygn6n708")))

(define-public crate-horrible-katex-hack-0.1 (crate (name "horrible-katex-hack") (vers "0.1.3") (hash "0s70pglxwlhbdc72q9m3fgwylbq6i32rxp7qc6z6vxvg6rrr7bq1")))

(define-public crate-horror-0.1 (crate (name "horror") (vers "0.1.0") (hash "0s8vhm95v0gdi1h4g0d35cvifwwcavhjjfbkkmnw64vs29n1l73y") (yanked #t)))

(define-public crate-horror-0.1 (crate (name "horror") (vers "0.1.1") (hash "0a16j1sqrc3k9j5c8k43zf2nac6v65w5v2zk7k7zvzif6wprpfr8")))

(define-public crate-horror-0.1 (crate (name "horror") (vers "0.1.2") (hash "0wnwzinryii2sh2j3wf4qj6ry40cd7ks4pvnaqfav1anhfvs2qyn")))

(define-public crate-horrors-0.1 (crate (name "horrors") (vers "0.1.0") (hash "0rr4wbzaa4z1zpqxi1idhxdnf3baa7vvng8qadqabjjkkqivfbx7") (yanked #t)))

(define-public crate-horrorshow-0.3 (crate (name "horrorshow") (vers "0.3.0") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "1hg1msis30y9f49riiwwp9if04z3h24qj8a3iw1dcqgvl2rn260m")))

(define-public crate-horrorshow-0.3 (crate (name "horrorshow") (vers "0.3.1") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "0phka2l0fl0nxb3p98rivhpich5l0k424ii46qih6hxgc7by5l2p")))

(define-public crate-horrorshow-0.3 (crate (name "horrorshow") (vers "0.3.2") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "0r4m1lv4cih8ivlivz92bb589lnqzzl1ni8phv97jzmvrn34kbv1")))

(define-public crate-horrorshow-0.3 (crate (name "horrorshow") (vers "0.3.3") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "1z2mr17i9b08bja901xz5v12azggg69vz4dwibrk7kxg5ljp7ckb") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.3 (crate (name "horrorshow") (vers "0.3.4") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "10qjgdc8izn736ygw6iylyfv08dpm8x5znm82dha4hyy7hi9ps2q") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.3 (crate (name "horrorshow") (vers "0.3.5") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "15dcp98rwgx7ppragnm0aki89kwhhpa07bszx70d46fz20laahmn") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.4 (crate (name "horrorshow") (vers "0.4.0") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "1a9mn4vfznxkys66bj9pcacylfp3mawbbkw429pkqxhjqdra9nwx") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.0") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "0vg7y9mi493j1fp25rbgsd7aq984rpkmvxwbc9dazn8lp2dc52yl") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.1") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "1srllv4grbzkqg32wyy3zjlzmhiggs6yaf911i4s1z9nr0b9f6ac") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.2") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "1p76952w1q9sckr3chqxi6i42hw1rjk0av0yh3l9xznqf1wrh09p") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.3") (deps (list (crate-dep (name "maud") (req "*") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "*") (default-features #t) (kind 2)))) (hash "03146ijkw6ighrcs2h8ljdmym1axzm136gig2p45hhak7wwhi1ah") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.4") (deps (list (crate-dep (name "maud") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1wrpj2mdrww4sbqv88kmf45w9zsk38wvnhjam9hn0vj8a7r06gz3") (features (quote (("ops") ("default" "ops")))) (yanked #t)))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.5") (deps (list (crate-dep (name "maud") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "0fqycg93mfv74zr90mssizi5hl2qfrd58zix4iqi9793kqmi5sfv") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.6") (deps (list (crate-dep (name "maud") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1if3r8l2xrsb44bd1mgm80vkq7v5hdlabfjwg30kmpfyiwan47ak") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.7") (deps (list (crate-dep (name "maud") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "0gr1hya2l6c9fv2k8ilddah922fgvgxkarzcwhjf6wcjjsq2dfl2") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.5 (crate (name "horrorshow") (vers "0.5.8") (deps (list (crate-dep (name "maud") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.11") (default-features #t) (kind 2)))) (hash "0hwmaqbz6ckdz9dq59vi3gahx78hrw4giqv9f3chlasnkpw2hii3") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6 (crate (name "horrorshow") (vers "0.6.0") (deps (list (crate-dep (name "maud") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.13") (default-features #t) (kind 2)))) (hash "01pzz5lq25cas15hc9nb4v0zhqdwbsd1f3maqizhjq9y37jgf31a") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6 (crate (name "horrorshow") (vers "0.6.1") (deps (list (crate-dep (name "maud") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.13") (default-features #t) (kind 2)))) (hash "0nr04dar7vqghwfpfcprmpdpi8ms9rmisijl84asj8bkchasav13") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6 (crate (name "horrorshow") (vers "0.6.2") (deps (list (crate-dep (name "maud") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.16") (default-features #t) (kind 2)))) (hash "1klbjd4i1wkmd8pr32fy76iv3jbx0ynzrqw81hzn2kmz2m8pd1ym") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6 (crate (name "horrorshow") (vers "0.6.3") (deps (list (crate-dep (name "maud") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "maud_macros") (req "^0.17") (default-features #t) (kind 2)))) (hash "174gns3kmpwx4ycnqzda6x4dsj9zhswish5h67sfsz0gbkfn0nf3") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6 (crate (name "horrorshow") (vers "0.6.4") (hash "1bvgbym6h6gsa6qx6drf1k57ac56zs6r9hc5q6h8j87s785jdxcr") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6 (crate (name "horrorshow") (vers "0.6.5") (hash "0hr1j0bkzmlmqdyfdf5dk5wrrprbxf0gvc6iw5pndzlc4dhyx5r5") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.6 (crate (name "horrorshow") (vers "0.6.6") (hash "095njs49v9ma3yb9dpawjdggim09d2nklf550nnv31fcq3v78xjr") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.7 (crate (name "horrorshow") (vers "0.7.0") (hash "0yk9xsdigmxh3r5lvssjldfp5s502vg9jl8sjk4kb72b6rplzf3k") (features (quote (("ops") ("default" "ops"))))))

(define-public crate-horrorshow-0.8 (crate (name "horrorshow") (vers "0.8.0") (hash "07lyckgbd8cx3qh3dnv5ds0a2rnqwsrh7x2h5xiak1xx1wg5vn5x") (features (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8 (crate (name "horrorshow") (vers "0.8.1") (hash "0kahf2dxv6xx1v4yzdgsqxgj4wnzl39k3260isa9djdy7pqf34si") (features (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8 (crate (name "horrorshow") (vers "0.8.2") (hash "1pzh0j45xvkdp77mnsb93smvbpq43c1h4jxh6mkqb71x28r7cldd") (features (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8 (crate (name "horrorshow") (vers "0.8.3") (hash "1yykh6hly8dk2ghvzgjil41aa4g8cib4xjdbjsc4ir683c57xkl7") (features (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

(define-public crate-horrorshow-0.8 (crate (name "horrorshow") (vers "0.8.4") (hash "185yb8mzxiivqfm1v1nll5k797v9yrxi3jzpahd0n5a032cgnwc3") (features (quote (("std" "alloc") ("ops") ("default" "ops" "std" "alloc") ("alloc"))))))

