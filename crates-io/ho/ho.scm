(define-module (crates-io ho ho) #:use-module (crates-io))

(define-public crate-hohoho-0.1 (crate (name "hohoho") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "brainfuck") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "01m01jbh54ln9bkc28kfqgj3jvqhq3z4j8jhvqz70rna5mvvz1hy")))

(define-public crate-hohoho-0.1 (crate (name "hohoho") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "brainfuck") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0va0l51iwj3ilyxhrnlqsbm2j59x7glna1g4bjdmawgqqvcy7jbz")))

