(define-module (crates-io ho ri) #:use-module (crates-io))

(define-public crate-horizon-0.1 (crate (name "horizon") (vers "0.1.0") (hash "0384x99b6kgihk1kb18vg2yxzcff3vakir11nbn5r4ndfz7yyvi4")))

(define-public crate-horizons-0.0.1 (crate (name "horizons") (vers "0.0.1-alpha") (hash "0linrh3lkdxl60nmvc4as3chbwkdqljagpf5mpz3hi4q0aim2ry0")))

(define-public crate-horizontal_mixer-0.1 (crate (name "horizontal_mixer") (vers "0.1.0") (deps (list (crate-dep (name "kira") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1541i01qmsy1dn8vm0qwv7dczb7jiv79j7f451s6789c4bbdn2ha")))

(define-public crate-horizontal_mixer-0.1 (crate (name "horizontal_mixer") (vers "0.1.1") (deps (list (crate-dep (name "kira") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0npqbkx1wj1gdw76s67h6nkl8lx1knpn8la796fwhk14zk31dwgm")))

(define-public crate-horizontal_mixer-0.1 (crate (name "horizontal_mixer") (vers "0.1.2") (deps (list (crate-dep (name "kira") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "07y230arvs6zifknq7f4zrf3wr9w6426dlxkk7kbzngd0kgiv4yr")))

(define-public crate-horizontal_mixer-0.1 (crate (name "horizontal_mixer") (vers "0.1.3") (deps (list (crate-dep (name "kira") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1k42wivz6xbcz73qifsy80ly8spf68hzqbp8n0jdixg8byzcn3fc")))

