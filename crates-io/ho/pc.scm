(define-module (crates-io ho pc) #:use-module (crates-io))

(define-public crate-hopcroft-karp-0.1 (crate (name "hopcroft-karp") (vers "0.1.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0qnfa5zn7g2r7cxa03b17j52x12j1hj47afwg57vb65gjba8krw3")))

(define-public crate-hopcroft-karp-0.1 (crate (name "hopcroft-karp") (vers "0.1.1") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1pvnsmvgk721mr8i0b7b3zm2xd32flgwhnzxnp3qmmbssdcpq4q5")))

(define-public crate-hopcroft-karp-0.2 (crate (name "hopcroft-karp") (vers "0.2.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "04jh86jh9fhkhv177xi2vm4c8v5806fa9nx20zvsgz81g0m0v6xb")))

(define-public crate-hopcroft-karp-0.2 (crate (name "hopcroft-karp") (vers "0.2.1") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0bknn9622mx2aw73jvz3haw4wkkmvav08df2s7snyxinm6dksbx7")))

