(define-module (crates-io ho nk) #:use-module (crates-io))

(define-public crate-honk-0.0.0 (crate (name "honk") (vers "0.0.0") (hash "1xkyz8cjy4rzmk97xq044lkb44qp7lj1aynsb5rjn2hchr8s6qw5")))

(define-public crate-honk-rpc-0.1 (crate (name "honk-rpc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bson") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mrcsn0qv0y2wf4fiq22v06n1p73ix8l581nzkhi0v25q2ib9nbr") (rust-version "1.66")))

(define-public crate-honk-rpc-0.1 (crate (name "honk-rpc") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "bson") (req ">=2.0, <=2.4.0") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ahfwp54v43v8dc9l0s1pgxd30sd27alfrgmwkqfq7vbjlvfpc4c") (rust-version "1.63")))

