(define-module (crates-io ho le) #:use-module (crates-io))

(define-public crate-hole-0.1 (crate (name "hole") (vers "0.1.0") (hash "1gqldqgnk8kvs4cmb6wg4wf1lkkm0pyr1h61460wplisl48afgb6")))

(define-public crate-hole-punch-0.0.0 (crate (name "hole-punch") (vers "0.0.0") (hash "1ccmh11m330xf63yk600ipz4595r335id37wbsvmf04p9brnx8a2")))

(define-public crate-hole-punch-0.0.1 (crate (name "hole-punch") (vers "0.0.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.11") (default-features #t) (kind 0)))) (hash "1gixhmjzpn34xp1v99wdby760w6qn8d6dm2m8iv1blb9cn4nh8am")))

(define-public crate-hole-punch-0.0.2 (crate (name "hole-punch") (vers "0.0.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("ioapiset" "winioctl"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1hdf6ysjzbiq36pnn11ll2n6hdag1h87hgafsm0d1icss9pr5b40")))

(define-public crate-hole-punch-0.0.3 (crate (name "hole-punch") (vers "0.0.3") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.4") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("ioapiset" "winioctl"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1sysy5ww7syg92jqq90liggfphlklcxraw0qr34fddyz72drvgns")))

(define-public crate-hole-punch-connect-0.1 (crate (name "hole-punch-connect") (vers "0.1.0") (hash "1xjd9avgqaqzr3snn298a91q5fvqkx9m46npblblzg1msz80x91g")))

(define-public crate-hole-punch-connect-0.1 (crate (name "hole-punch-connect") (vers "0.1.1") (hash "0dmqkck0hyivzgdvkipl0qnxi7wsa5wsl4549428wxfhnhvmk3xa")))

(define-public crate-hole-punch-connect-0.1 (crate (name "hole-punch-connect") (vers "0.1.2") (hash "1932gkr3jk658j6b59ngz49v7pfyz9mvlal2bwhaa5f218d27910")))

(define-public crate-holen-0.1 (crate (name "holen") (vers "0.1.0") (hash "18yjaf31rnf8b58cg7k08w6326v28skrqfydw8qq8hja8hk4wnn8")))

(define-public crate-holen-http1-0.1 (crate (name "holen-http1") (vers "0.1.0") (hash "01fly3bn54xwx3im9hjqycg6x7mf0fglh513xni7f4r3ay2xqn8n")))

(define-public crate-holen-http2-0.1 (crate (name "holen-http2") (vers "0.1.0") (hash "0pp16vbidl6cd7gh80x8zmqxylywl84ai7h9xl8lak664p9zq6s5")))

(define-public crate-holen-http3-0.1 (crate (name "holen-http3") (vers "0.1.0") (hash "1wx3j5b8jbwgrv320x1chw978zfb0xmh051m9vmcmdd4iay3mm8r")))

(define-public crate-holepunch-0.0.0 (crate (name "holepunch") (vers "0.0.0") (hash "1hvaw6zcrq8hjqrrp062vl0agjihb4nx98hazqz7bvfzri64qjjm") (yanked #t)))

(define-public crate-holepunch-0.0.1 (crate (name "holepunch") (vers "0.0.1") (hash "162nsymv2pnzdyry66qxxyq0qqyqyjc7pl6xl397qza6h7idwsj4") (yanked #t)))

(define-public crate-holepunch-0.0.2 (crate (name "holepunch") (vers "0.0.2") (deps (list (crate-dep (name "icmp-socket") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02ss5x2mf25r265j0dljp3937cxaqw23bm137w463sa25if9b70s") (yanked #t)))

(define-public crate-holes-0.0.1 (crate (name "holes") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "0kf8v1p1hs4lpdnji7ak6d8q2jhh19yfm9irij8d8ljlid4n2ffd") (yanked #t)))

(define-public crate-holes-0.0.2 (crate (name "holes") (vers "0.0.2") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time" "sync"))) (default-features #t) (kind 2)))) (hash "09bymwnqjy8zdbn32aspawcnkbxlnqzd1kcvhb3g8p93im4x7y26") (yanked #t)))

