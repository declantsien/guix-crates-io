(define-module (crates-io ho cu) #:use-module (crates-io))

(define-public crate-hocus-pocus-0.0.0 (crate (name "hocus-pocus") (vers "0.0.0") (hash "1mw6hzvsa4p7y8ldpw05q0a229516rydwashjk6y2gkfr6z9vq48")))

(define-public crate-hocuspocus_rust_crate-0.1 (crate (name "hocuspocus_rust_crate") (vers "0.1.0") (hash "15fp2l724mbzfkhzzz44nv503bxlqilixc9gpv058avv68il67gc")))

