(define-module (crates-io ho ly) #:use-module (crates-io))

(define-public crate-holy-0.1 (crate (name "holy") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03y36aimhb6p76k2349p0apg9583dv0gnsb4qrnmcj0i7ccw66cw") (rust-version "1.75")))

(define-public crate-holy-0.1 (crate (name "holy") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r85vwy3msn4bnlv81ff2jx24xzb5zsnj4jgr0chn8bvbf9pfbqj") (rust-version "1.75")))

(define-public crate-holy-0.1 (crate (name "holy") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11k04a3i56yzds45h270wk1r64vq5m4lnamqpy87pqhp36y871c7") (rust-version "1.75")))

(define-public crate-holy_crate-0.1 (crate (name "holy_crate") (vers "0.1.0") (hash "080ykh5f853a492bkdih5pcq6c56y4lv5snrxg2m77xvmaqcfc5r") (yanked #t)))

(define-public crate-holy_crate-0.1 (crate (name "holy_crate") (vers "0.1.1") (hash "1d0yx2ghsh3plrv55dqscrrz9qkhgnjdxpyyqf2nxi1yd4k65v9r") (yanked #t)))

(define-public crate-holy_crate-0.1 (crate (name "holy_crate") (vers "0.1.2") (hash "07l6gzahnysnlxqamghkjh4j2pabdi02wjgfinjf2x2ynyx8n5z1") (yanked #t)))

(define-public crate-holyhashmap-0.1 (crate (name "holyhashmap") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0n5zsfknhzmknjc32d6375bxp35g5yvxkprqvzvrwb31wskm1jrf")))

(define-public crate-holyhashmap-0.1 (crate (name "holyhashmap") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0fdzwn1798byd7pl5y94rhhbra3q5970x1mklzfscdmp4cmfi3nj")))

(define-public crate-holyhashmap-0.1 (crate (name "holyhashmap") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1nf9ppzds2l3d610h7frhmasjpxbz97xkzdgp5wr2as1c949ka0y")))

