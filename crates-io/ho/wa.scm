(define-module (crates-io ho wa) #:use-module (crates-io))

(define-public crate-howami-0.1 (crate (name "howami") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "systemstat") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1valvvzlwvqqrxdhvyvl41bx21xi7jrqqibs4zfz70grn3y0jbgb")))

(define-public crate-howami-0.1 (crate (name "howami") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "systemstat") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1myzy8lcy200yfcw2b00wj4dg6khmqia69akyd0nrrqbs598w98a")))

(define-public crate-howami-0.1 (crate (name "howami") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "systemstat") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1901487xgzl4hqp4pbpgjb6sl21bg6r9waibyrh6wn6rddvd0wkx")))

(define-public crate-howami-0.1 (crate (name "howami") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "systemstat") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0qhw1r3n0sljm2x2p51xkjxmlgaicfvylsih52jcnw76ap6qkjpj")))

