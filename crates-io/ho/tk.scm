(define-module (crates-io ho tk) #:use-module (crates-io))

(define-public crate-hotkey-0.1 (crate (name "hotkey") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0fc4696px2pi3ijn6afawzg9db6gqcn3w4c0cl0y9gn6kskawbfj")))

(define-public crate-hotkey-0.2 (crate (name "hotkey") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "04qzrjmn7h2x31j7m09vv4w67c51mg9ns57yxga0jp3dgh35wzvm")))

(define-public crate-hotkey-0.3 (crate (name "hotkey") (vers "0.3.0") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "04n96xsz6kfmznpig0jpvaqcs33xb3iwmy1mi79gdnw6c9wcfjx1")))

(define-public crate-hotkey-0.3 (crate (name "hotkey") (vers "0.3.1") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1j6mpq9dvv38n67dv4zmanqrzhs3ahccgdv7qskcx0rynfkqrvb8")))

(define-public crate-hotkey-rs-0.1 (crate (name "hotkey-rs") (vers "0.1.0") (hash "0n7r0nwn22jz62diq55mbshmbsiss1kkf27366r3344x9f5m0b4n") (yanked #t)))

(define-public crate-hotkey-rs-0.1 (crate (name "hotkey-rs") (vers "0.1.1") (hash "16h65vyvjiwjddgrlvdy2pqj9m47p54hafrm44ypm3n6ws3l7q6b") (yanked #t)))

(define-public crate-hotkey-rs-0.1 (crate (name "hotkey-rs") (vers "0.1.2") (hash "0x2yq4fpgfyrx12fxgah7ks8alvnblfjb5msa8i3jfp6dc5cj3rm")))

(define-public crate-hotkey-soundboard-0.0.1 (crate (name "hotkey-soundboard") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 1)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11-dl") (req "^2.18") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "03nnsj5l8xsxdqfx3k96gkyf0a939scf4j6dvw89n9swyqix9x3h")))

(define-public crate-hotkey-soundboard-0.0.3 (crate (name "hotkey-soundboard") (vers "0.0.3") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 1)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser" "errhandlingapi"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "x11-dl") (req "^2.18") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "16lmi0pasp73rnqirxb6ylraa2dvlxlp44r5v3x6ya689ijh18ja")))

(define-public crate-hotklicker-0.1 (crate (name "hotklicker") (vers "0.1.0") (deps (list (crate-dep (name "enigo") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "inputbot") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)))) (hash "162gbrja5n9wsyd74f9w01b46w8cbqgzwffmgfzkiwv4kyy25lzj")))

