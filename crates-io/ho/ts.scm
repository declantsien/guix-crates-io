(define-module (crates-io ho ts) #:use-module (crates-io))

(define-public crate-hotsauce-0.0.0 (crate (name "hotsauce") (vers "0.0.0") (hash "0zj48lsmnrl2pzh94k9ljiqyywx0z0aq8pzfr6c19spqgdg3ldfl")))

(define-public crate-hotsauce-0.1 (crate (name "hotsauce") (vers "0.1.0") (deps (list (crate-dep (name "expect-test") (req "^1.4.0") (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.10") (features (quote ("std"))) (kind 0)))) (hash "14i56bq2758gq3zyy27hgxk9prhwsvqsz2ldvdr3vy7zl67mdank")))

(define-public crate-hotsauce-0.1 (crate (name "hotsauce") (vers "0.1.1") (deps (list (crate-dep (name "expect-test") (req "^1.4.0") (kind 2)) (crate-dep (name "regex-automata") (req "^0.1.10") (features (quote ("std"))) (kind 0)))) (hash "1w3zsy78wrb6p1r7ik5psyfiyriqa0mcm12zfmvrvn3vzj8pn29v")))

(define-public crate-hotsax-0.1 (crate (name "hotsax") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1cbnydr9s8ahlw6mqks19vfv8msjkxlsjyi48af8v01fv0dq8kaj")))

(define-public crate-hotsax-0.1 (crate (name "hotsax") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "09k31kkv1yg64jdg0h9y8dkliqpb57azjlwjbwybxgggm28g98pp")))

(define-public crate-hotsax-0.1 (crate (name "hotsax") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0drib8kz31x2hfq92az20sz5blhkirqvwqmx0521bm26gjgrafi3")))

(define-public crate-hotsax-0.1 (crate (name "hotsax") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1496cxsbzy6r0v1p25lwigxmr2ki3ba39n5p243y0vn4zmgsyf6l")))

(define-public crate-hotsax-0.1 (crate (name "hotsax") (vers "0.1.4") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "10187mw4gycpgaaqjdl8c6q4h2ix51xqq3cb8y1nnyss9ba9pv4d")))

(define-public crate-hotsax-0.1 (crate (name "hotsax") (vers "0.1.5") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0l6dfzbcvjmw2j0xqsqcdzn8jbiclp0i2aq76cnw73zzmk31gkbi")))

(define-public crate-hotsax-0.2 (crate (name "hotsax") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0758ilsgg0madf31kw7sqg2czid8bnhqamx9pplk13ml7y7dnsjf")))

(define-public crate-hotsax-0.3 (crate (name "hotsax") (vers "0.3.0") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0d0y2y0h4grcbnd4nx7ly093mpz65mm9dc8s9481a5l57jdb8i6r")))

(define-public crate-hotsax-0.4 (crate (name "hotsax") (vers "0.4.0") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0da936dspinl1ib40sjmranr4rdqn4c9fwbicjydyja47w01k7mh")))

(define-public crate-hotspot-0.0.1 (crate (name "hotspot") (vers "0.0.1") (deps (list (crate-dep (name "assert") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0c9nx1xkgm0p8q43sknpgzldbg3s2lkir9lxqjmmcab8wbhnc984")))

(define-public crate-hotspot-0.0.2 (crate (name "hotspot") (vers "0.0.2") (deps (list (crate-dep (name "assert") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1ibr2paml5drxz14z9nrq3c9n72hbi91nhw6gpr3wjn32ksmvmq6")))

(define-public crate-hotspot-0.0.3 (crate (name "hotspot") (vers "0.0.3") (deps (list (crate-dep (name "assert") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "09wgnkynyzgin39rk8xaxfgshzcp8kn5ry0v35b4cn7pvy5jbjsj")))

(define-public crate-hotspot-0.0.4 (crate (name "hotspot") (vers "0.0.4") (deps (list (crate-dep (name "assert") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "1va6dajd9c3cpacsnn1255zj154dy5wr4qzwysahwh8wm6h9yhw8")))

(define-public crate-hotspot-0.0.5 (crate (name "hotspot") (vers "0.0.5") (deps (list (crate-dep (name "assert") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "0xysw1j5a8ra6hzybqdzffwvl77wqwdhr6vkal0by8k9sibvd82b")))

(define-public crate-hotspot-0.0.6 (crate (name "hotspot") (vers "0.0.6") (deps (list (crate-dep (name "assert") (req "^0.0.3") (default-features #t) (kind 2)))) (hash "0mis6z1a80nlvw9piw4vd5qv0qxf5j65sd306yyssfjx9nfg4zyl")))

(define-public crate-hotspot-0.0.7 (crate (name "hotspot") (vers "0.0.7") (deps (list (crate-dep (name "assert") (req "^0.0.4") (default-features #t) (kind 2)))) (hash "0f35fb1i5bh8jpyr8xx8yc1k08j347j48hc7yhx6m3ykhsr33kha")))

(define-public crate-hotspot-0.0.8 (crate (name "hotspot") (vers "0.0.8") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "0b4x7kng746z16jvywfdgam60zc646dizks2vdjj4byz9c386y8k")))

(define-public crate-hotspot-0.0.9 (crate (name "hotspot") (vers "0.0.9") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "1n3kzcb730ryb1k4j84gvnr4hhwqjaf9f68hsmbnxczs6b00mwap")))

(define-public crate-hotspot-0.0.10 (crate (name "hotspot") (vers "0.0.10") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "01fy2id6agxkjwc258lkavrj8h1cffcy66chad3d9yzs7prjidqj")))

(define-public crate-hotspot-0.0.11 (crate (name "hotspot") (vers "0.0.11") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "12rmmqsw9v6x4xf816jfkbkm2gyrm8ifi6kgw672hj3cmvwnd555")))

(define-public crate-hotspot-0.0.12 (crate (name "hotspot") (vers "0.0.12") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "01nbdddzsz0fj6i5flcndf3fh7yirpdas6yvv3kzz3i5sdxxzpjh")))

(define-public crate-hotspot-0.0.13 (crate (name "hotspot") (vers "0.0.13") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "16wcb16ycac9a9vhj65m3nclg07p1j8slmy84nf2v71vd2q135i9")))

(define-public crate-hotspot-0.0.14 (crate (name "hotspot") (vers "0.0.14") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0lfqli1jzr6g2vj6p6w8x79ki9pb1hx07n6852l7brm5c29h46f3")))

(define-public crate-hotspot-0.0.15 (crate (name "hotspot") (vers "0.0.15") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0qx4ikzvl98qwd4nlqpicfb1qad19m399vzxj18nnad0v82rcax1")))

(define-public crate-hotspot-0.0.16 (crate (name "hotspot") (vers "0.0.16") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "185bk3svvxnyr7ha9g4dkb6a9bjz4ijjf5fw8cckx4c3jqglzqws")))

(define-public crate-hotspot-0.0.17 (crate (name "hotspot") (vers "0.0.17") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "026lz7531yyp54kc50h0ljdf1g46f67s6wwyhr8lri5z82jxs1xx")))

(define-public crate-hotspot-0.0.18 (crate (name "hotspot") (vers "0.0.18") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1xv6b0s7h3p1cwp5kx0x5qzy6amdd2q7vac3mj0n6skgda7p6svw")))

(define-public crate-hotspot-0.0.19 (crate (name "hotspot") (vers "0.0.19") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0bd6r61dnd50vqgszzxa49654qqw6ahl556cyr4mcz1gvpvb7ag9")))

(define-public crate-hotspot-0.1 (crate (name "hotspot") (vers "0.1.0") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1v896way3q6k5iw1q9ff6283sw2nx25d34bxl4zv7h2ngi89vb8v")))

(define-public crate-hotspot-0.1 (crate (name "hotspot") (vers "0.1.1") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1cpqdpcsgqp4h09icrbc9f8n41pvf10xq45ha82mlfzylh916clg")))

(define-public crate-hotspot-0.2 (crate (name "hotspot") (vers "0.2.0") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "024wlwr9as7jdi6m524a09hxvir8ajhj61m29rqbipwjy162wd1h")))

(define-public crate-hotspot-0.3 (crate (name "hotspot") (vers "0.3.0") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1018wc09ys4jdyjz34a9a9k2pq168myhgfd2spjsf8cqdrpkpxrk")))

(define-public crate-hotspot-0.3 (crate (name "hotspot") (vers "0.3.1") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1x09qwyk1w05ihvav4g9swf35v9pqdziqm9y0fqznnjvr3c4d3gh")))

(define-public crate-hotspot-0.4 (crate (name "hotspot") (vers "0.4.0") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.11") (default-features #t) (kind 0)))) (hash "1rvpsjrvc14igahxwr56qzwwvzh78i4j87dwdcmdim3b8jsgb0ng")))

(define-public crate-hotspot-0.4 (crate (name "hotspot") (vers "0.4.1") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.12") (default-features #t) (kind 0)))) (hash "1bcglqjw4lb0l3isnvya0wkrr645sfb04xkq7vrkd622rb5zz3rr")))

(define-public crate-hotspot-0.4 (crate (name "hotspot") (vers "0.4.2") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.13") (default-features #t) (kind 0)))) (hash "07zjn2qiamrba8zn69z83g06dg3xh28lwyh8f6p4blral54bb1h9")))

(define-public crate-hotspot-0.4 (crate (name "hotspot") (vers "0.4.3") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.14") (default-features #t) (kind 0)))) (hash "1abkpxqqbm8zk3smhlxd4b7m2k7lnkgjhksplybishpgmqv3p45q")))

(define-public crate-hotspot-0.4 (crate (name "hotspot") (vers "0.4.4") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.15") (default-features #t) (kind 0)))) (hash "0xc7sfxyj17l5ipickbnjvh4sdrdayxggrcdgl4jzygd1053h9yv")))

(define-public crate-hotspot-0.4 (crate (name "hotspot") (vers "0.4.5") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.17") (kind 0)))) (hash "0cjvrcax5id25gnz7csr3baszsb0vvh0md0hafkv4x4jf9w5lp5w")))

(define-public crate-hotspot-0.5 (crate (name "hotspot") (vers "0.5.0") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.17") (kind 0)))) (hash "0hhh0921rr0fw2pmdap3ffy0gm1n2jzzxmk1wn1rv5a1iq8igaps")))

(define-public crate-hotspot-0.5 (crate (name "hotspot") (vers "0.5.1") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.18") (kind 0)))) (hash "1qlr43xkh6mhmgzfyvxld81aysalq5rlkxnh5rk0n7kvyn5rk1l0")))

(define-public crate-hotspot-0.5 (crate (name "hotspot") (vers "0.5.2") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.19") (kind 0)))) (hash "0chxdn9yqiqdxw3jlf9ygz94ya9prn6ziza96687aj4lxc5hvqgc")))

(define-public crate-hotspot-0.5 (crate (name "hotspot") (vers "0.5.3") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.20") (kind 0)))) (hash "10jjpk4kwfd8zipcyfd18s51a35za0ykb81fqdaj0dkdhfg7w4ac")))

(define-public crate-hotspot-0.5 (crate (name "hotspot") (vers "0.5.4") (deps (list (crate-dep (name "assert") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.20") (kind 0)))) (hash "13iqazp1njj2swcavh32lh3zfnh5jzn77hzy37lh4jchw4g2jjx8")))

(define-public crate-hotspot-0.5 (crate (name "hotspot") (vers "0.5.5") (deps (list (crate-dep (name "assert") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.21") (kind 0)))) (hash "1yiwy0gdmc08y6fgh0wzh6hqh1wlh24s13gjnz7dk2c01dqrrj77")))

(define-public crate-hotspot-0.6 (crate (name "hotspot") (vers "0.6.0") (deps (list (crate-dep (name "assert") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "matrix") (req "^0.21") (kind 0)))) (hash "0sgjj3i695hm22a46kbpdqy1ar3f2hylr9by8h45ra066r8s9hr4")))

(define-public crate-hotspots-discovery-0.0.4 (crate (name "hotspots-discovery") (vers "0.0.4") (deps (list (crate-dep (name "detect-lang") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0dbg8giwii63k9rq0rljsq5vs7bb1albdzf5sc6zygf0xjgwdpgx")))

(define-public crate-hotspots-discovery-0.0.5 (crate (name "hotspots-discovery") (vers "0.0.5") (deps (list (crate-dep (name "detect-lang") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0vj56w6vb46g9q5jmsvbpprgy72hh0zbhdm5w3lmyi80zvjl9k0d")))

(define-public crate-hotspots-discovery-0.0.6 (crate (name "hotspots-discovery") (vers "0.0.6") (deps (list (crate-dep (name "detect-lang") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.6") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "09lq727rr44wmcqzaq65rwqzhkn1hg9y90lw7sf1df8p6dffmi05")))

(define-public crate-hotspots-discovery-0.0.7 (crate (name "hotspots-discovery") (vers "0.0.7") (deps (list (crate-dep (name "detect-lang") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1pzbmcpygs460zpg0dp3sjhmjwh73sivs83x365wml52k5g6bswh")))

(define-public crate-hotspots-discovery-0.0.8 (crate (name "hotspots-discovery") (vers "0.0.8") (deps (list (crate-dep (name "detect-lang") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.8") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0yxbwr8isiva019kcnnl4268sl7r096bvh1dacswwswgb0v12w85") (rust-version "1.69")))

(define-public crate-hotspots-insight-0.0.5 (crate (name "hotspots-insight") (vers "0.0.5") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "grep-matcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-regex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-searcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0y6giw1p83aag90rh7yvarv8a9f0vh784hfpzdlkhk8da8l35p0b")))

(define-public crate-hotspots-insight-0.0.6 (crate (name "hotspots-insight") (vers "0.0.6") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "grep-matcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-regex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-searcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.6") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1cr21ix46lf3wrvlnsbggr9wxrfczv7ag8wsx10wwyavyyylyxxr")))

(define-public crate-hotspots-insight-0.0.7 (crate (name "hotspots-insight") (vers "0.0.7") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "grep-matcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-regex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-searcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.7") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0k3lg91np92jg9dzjbjby65dq47qsnvmn7gd3y8vf2kc6nap5g5l")))

(define-public crate-hotspots-insight-0.0.8 (crate (name "hotspots-insight") (vers "0.0.8") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "grep-matcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-regex") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "grep-searcher") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "hotspots-utilities") (req "^0.0.8") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0i0pw4brpvwrb63alqbh7qw5fglbax1mzsqn4xv0w5ni6cxx8061") (rust-version "1.69")))

(define-public crate-hotspots-parser-0.0.5 (crate (name "hotspots-parser") (vers "0.0.5") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "hotspots-discovery") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "include_dir") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "npm_rs") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req ">=0.20") (default-features #t) (kind 0)))) (hash "1djszh6yblra898zqxlfrj6z21iz0zvhz1z7dia761ri3zsqr7j9")))

(define-public crate-hotspots-parser-0.0.6 (crate (name "hotspots-parser") (vers "0.0.6") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "hotspots-discovery") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "include_dir") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "npm_rs") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req ">=0.20") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-go") (req ">=0.19") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-lua") (req ">=0.0.14") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0m2p2j2sj8xcabb2b5n6xslxbpdizm8wncm2c38f66lsd0p60jym")))

(define-public crate-hotspots-parser-0.0.7 (crate (name "hotspots-parser") (vers "0.0.7") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "hotspots-discovery") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "include_dir") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "npm_rs") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req ">=0.20") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-go") (req ">=0.19") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-lua") (req ">=0.0.14") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0yyqgksjmc4irb4f6w0avw5qr1vn501f6rrq8jyk102lhs4qdyg4")))

(define-public crate-hotspots-parser-0.0.8 (crate (name "hotspots-parser") (vers "0.0.8") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "hotspots-discovery") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "include_dir") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "npm_rs") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "speculoos") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req ">=0.20") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-go") (req ">=0.19") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-lua") (req ">=0.0.14") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-rust") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0s17xmq5aws1z5xzydpj4b5ppknx4fdcdyy7srg39bjri0dwyyi3") (rust-version "1.69")))

(define-public crate-hotspots-utilities-0.0.4 (crate (name "hotspots-utilities") (vers "0.0.4") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0f5scks55kxm5dg6a26m32z2n9rlsnyfyn2abqzsqmiq8qjf90di")))

(define-public crate-hotspots-utilities-0.0.5 (crate (name "hotspots-utilities") (vers "0.0.5") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0yxhhiyi8b5vjqxsa75d07bl01g71vlrlarhyrgv7yjjjjs442xh")))

(define-public crate-hotspots-utilities-0.0.6 (crate (name "hotspots-utilities") (vers "0.0.6") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "17m58vqi5sfaaa8h824dzdzajpvac9gji6a4x65129x7isr8y72h")))

(define-public crate-hotspots-utilities-0.0.7 (crate (name "hotspots-utilities") (vers "0.0.7") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1vlr37fivh97q5kf2qbipnsj7kqlbaacf2qkrmm0sqnxs9a2wwcq")))

(define-public crate-hotspots-utilities-0.0.8 (crate (name "hotspots-utilities") (vers "0.0.8") (deps (list (crate-dep (name "git2") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "09bjrw5bqy9pmlimf9y2zv6qmhxc0g0swf370gn0b7iq0fx8k4gx") (rust-version "1.69")))

(define-public crate-hotstuff-0.0.0 (crate (name "hotstuff") (vers "0.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "04pqvrg819mlcbw9in54wq3n8ffa5xdab1aw8ylfbv7wlx8c9psd")))

(define-public crate-hotstuff_rs-0.0.0 (crate (name "hotstuff_rs") (vers "0.0.0") (hash "1w0iphi2ijxaqa4icfpca1jm67pqp1x0hszwpnqp8jgspb2fvk92")))

(define-public crate-hotstuff_rs-0.2 (crate (name "hotstuff_rs") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "1rij12q4gslfx34a7nd8l2p0fjdy1n6pxjry3axsb9ia5azz9fs0")))

(define-public crate-hotstuff_rs-0.2 (crate (name "hotstuff_rs") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "0fdcnw6fjbf8l33b5hg3cvgkkxz0agil0lxdm4s9ci1zljrp8zzy")))

(define-public crate-hotstuff_rs-0.2 (crate (name "hotstuff_rs") (vers "0.2.2") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "0iknvwan4d6zfz4fvy1a2vzzzpbvz55k483wzfpid4nk1h7miwfi")))

(define-public crate-hotstuff_rs-0.3 (crate (name "hotstuff_rs") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.0.0") (features (quote ("rand_core"))) (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "1yyb7i0kq5mnfsmmzvc11r1pfgxr0il376mmi5j5r8m8biy8wkvq")))

(define-public crate-hotstuff_rs_types-0.1 (crate (name "hotstuff_rs_types") (vers "0.1.0") (deps (list (crate-dep (name "borsh") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "1j9sn07ccaryxphbgk6x0mqx61jsazy8l9iwi7hylk8jrkanrn1k")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.0") (deps (list (crate-dep (name "aster") (req "0.18.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1z2x6lk2nfwpm3z3yll5avwgni2an2qmh2jswia8ydxsa00ax5pg")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.1") (deps (list (crate-dep (name "aster") (req "0.18.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0c4p2avyjfhacvjcck00bhdv3xmsjrb50prq3qxhha0h0cmacmn6")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.2") (deps (list (crate-dep (name "aster") (req "0.18.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0kx3cgwafz2i8hrkpdnq8pdx2qdc0wndiamgz90k122jim4w9052")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 0)))) (hash "12kahbgwrgls6k24m573r1f7hp865m1ghkn7wqp74vwhiglx5wc7")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.4") (hash "1a8s5bvfz17gda4yw4zkry677fvxr7fr08xlxad99hay33k1fkpj")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.5") (hash "1i6xmp82xr39gv7k5d4m14bvlyr2b9vf1gswq27fbc28rjkk7mp9")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.7") (deps (list (crate-dep (name "libloading") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "0.2.*") (features (quote ("nightly"))) (default-features #t) (kind 0)))) (hash "00k35ijgxs3i82p377l13g4r21bcrcb7vlq34047pm8r889zh548")))

(define-public crate-hotswap-0.1 (crate (name "hotswap") (vers "0.1.8") (deps (list (crate-dep (name "libloading") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "0.2.*") (features (quote ("nightly"))) (default-features #t) (kind 0)))) (hash "00ra7508dqp261kkckz2nsfmamzq4fdgw0c8fma4v0i58gmwvcld")))

(define-public crate-hotswap-0.2 (crate (name "hotswap") (vers "0.2.0") (hash "09g3m2mzm79dvhc59wzvjlmi5j0iqi44s3snwn0jbrgnc758xifm")))

(define-public crate-hotswap-0.3 (crate (name "hotswap") (vers "0.3.0") (hash "1d2q4lnbpifhb34bwwg11is4zyfpi42lmcdyijp6xdhqy671cvh9")))

(define-public crate-hotswap-0.3 (crate (name "hotswap") (vers "0.3.1") (hash "1gx5zq5nn4mp2sm01hi1fngn3w9i3zzvmm6kmkrz07h4n5kqjh4c")))

(define-public crate-hotswap-0.3 (crate (name "hotswap") (vers "0.3.2") (hash "0hcpdmqqj7kgv0k3alz68686isyvd53k64vpylhsnw9kmn5qalgy")))

(define-public crate-hotswap-0.4 (crate (name "hotswap") (vers "0.4.0") (hash "0r43rdss4x5qyz8f3yvld0vf7c684qn12gjghzjlkv1pbhibiccx")))

(define-public crate-hotswap-0.4 (crate (name "hotswap") (vers "0.4.1") (hash "0vbflzq5pmb4krrpgp62smjlpqlg85bl13zjacihlfpgqv8c5fhx")))

(define-public crate-hotswap-0.4 (crate (name "hotswap") (vers "0.4.2") (hash "01wh7nhn0b3gw7hr0cslybv968y3cixyhyvbsm4dckrhr7kjv89p")))

(define-public crate-hotswap-0.5 (crate (name "hotswap") (vers "0.5.0") (hash "1mz69aw7w91avbhmgaisc9ifr1mw16vsdad3amgp72wv4al9l031")))

(define-public crate-hotswap-0.6 (crate (name "hotswap") (vers "0.6.0") (hash "0iws8f0hskvp963mhzwzd95cksyyryjhrn1f7j2bkzlhp56m12g9")))

(define-public crate-hotswap-runtime-0.1 (crate (name "hotswap-runtime") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.3") (features (quote ("nightly"))) (default-features #t) (kind 0)))) (hash "1aa1zppvzdy6dgjp12dlh53yyymsqfp2k3dyazr7j973riv2v9y1")))

(define-public crate-hotswap-runtime-0.2 (crate (name "hotswap-runtime") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.5.3") (features (quote ("nightly"))) (default-features #t) (kind 0)))) (hash "1kpvlzmwi2d1dgjvdflvmibm9fn9mmmi5h4w90dzma4paiij6pwc")))

