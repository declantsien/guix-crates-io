(define-module (crates-io ho do) #:use-module (crates-io))

(define-public crate-hodoku-0.1 (crate (name "hodoku") (vers "0.1.0") (hash "0fq8vv0n8yyvy4lyq7a7cw945kyjcxqigjm8k7973z66hhiafqwn") (yanked #t)))

(define-public crate-hodoku-0.1 (crate (name "hodoku") (vers "0.1.1") (hash "028ps7sr70nmlq5m4gfmmayzcbirv2gww8yi991bwfq7glzaqcx3") (yanked #t)))

(define-public crate-hodoku-0.1 (crate (name "hodoku") (vers "0.1.2") (hash "047qwr7vwjl81pcvih48agm8c7h81xd8k74kayqsxwvc8rb30xf9")))

(define-public crate-hodoku-0.1 (crate (name "hodoku") (vers "0.1.3") (hash "1b1hdvjjipylpa5wphrwpnrvjdzxjxpqh4m0qrq5cznxg6jw2sjz")))

(define-public crate-hodoku-0.1 (crate (name "hodoku") (vers "0.1.4") (hash "123k11qnqf9r8jb97gnnd4s61zha3isy00ip3zpzfrdasr79jbfc")))

(define-public crate-hodoku-0.1 (crate (name "hodoku") (vers "0.1.5") (hash "186g9vx1rbz5synd4vvv6kk6vl6qyqgn49zycrphmpc7vcr57w3d") (rust-version "1.56")))

(define-public crate-hodor-program-0.1 (crate (name "hodor-program") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.10.26") (default-features #t) (kind 0)) (crate-dep (name "spl-token") (req "^3.3.0") (features (quote ("no-entrypoint"))) (default-features #t) (kind 0)))) (hash "0nz8spdd8y08rbpj0mqzihics34lgqzpnvws9i73l75vww2pqnz6") (features (quote (("no-entrypoint"))))))

