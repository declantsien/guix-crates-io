(define-module (crates-io cq t-) #:use-module (crates-io))

(define-public crate-cqt-rs-0.1 (crate (name "cqt-rs") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "hann-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("rayon" "matrixmultiply-threading"))) (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.1") (default-features #t) (kind 0)))) (hash "1120isiicw8s6hwh5v5xk3r9wx7ak3p05c35lklyp1ifwpvw7pkc")))

