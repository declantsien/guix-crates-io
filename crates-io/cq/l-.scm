(define-module (crates-io cq l-) #:use-module (crates-io))

(define-public crate-cql-ffi-safe-0.0.3 (crate (name "cql-ffi-safe") (vers "0.0.3") (deps (list (crate-dep (name "cql_ffi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "*") (default-features #t) (kind 0)))) (hash "0xgixl7n1h2rlcb4lcjziq0igq5mjhqvwawhwyhhaj52sp14lc7y")))

(define-public crate-cql-ffi-safe-0.0.4 (crate (name "cql-ffi-safe") (vers "0.0.4") (deps (list (crate-dep (name "cql_ffi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "*") (default-features #t) (kind 0)))) (hash "00qpcp5g6frblllvkh5kyggqi6vaynbv5vy838m2ksk6xd8pvy88")))

(define-public crate-cql-ffi-safe-0.0.5 (crate (name "cql-ffi-safe") (vers "0.0.5") (deps (list (crate-dep (name "cql_ffi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "*") (default-features #t) (kind 0)))) (hash "0s2zgrhb1cwn7p7cq0h7v1nm7524nlq1pk14i6m2b0nz3sgrln7v")))

(define-public crate-cql-ffi-safe-0.0.6 (crate (name "cql-ffi-safe") (vers "0.0.6") (deps (list (crate-dep (name "cql_ffi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "*") (default-features #t) (kind 0)))) (hash "0jhl9b1hkijsb1qkhinzpci005ricw87j2d3im0jiinyrb2kjikg")))

(define-public crate-cql-ffi-safe-0.0.7 (crate (name "cql-ffi-safe") (vers "0.0.7") (deps (list (crate-dep (name "cql_ffi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "*") (default-features #t) (kind 0)))) (hash "012z3lymb9lg7wb1rfbjfihnfh689fcpqfnij8cl5wl3da5lad2g") (yanked #t)))

(define-public crate-cql-protocol-0.1 (crate (name "cql-protocol") (vers "0.1.0") (hash "0b84y75spwnch5avsqw65wgwf0q2x1h38lr0hrfi04djwb1zhwnj")))

(define-public crate-cql-rust-0.0.1 (crate (name "cql-rust") (vers "0.0.1") (hash "0flf0f5cdpsxbgbp8bafgjal3sy7i0r4y75a8q1bjj5kx721q2lv")))

(define-public crate-cql-rust-0.0.2 (crate (name "cql-rust") (vers "0.0.2") (hash "1ni68gawnnddkrdqq4577nj5rzg4g9fjxsh5dpg06fhl1d3nphvj")))

(define-public crate-cql-rust-0.0.3 (crate (name "cql-rust") (vers "0.0.3") (hash "12r8rbzp38z5mhav4y9rnklxzp07izs5pmvcjcw9q88w60grz0f1")))

