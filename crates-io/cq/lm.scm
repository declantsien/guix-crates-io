(define-module (crates-io cq lm) #:use-module (crates-io))

(define-public crate-cqlmig-0.0.1 (crate (name "cqlmig") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.61") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1j60p8nwfxg5h9rylz6bnzlfkjysmx3vnpw0p66mbn9lxw1r5k94")))

(define-public crate-cqlmig-cdrs-tokio-0.0.1 (crate (name "cqlmig-cdrs-tokio") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.61") (default-features #t) (kind 0)) (crate-dep (name "cdrs-tokio") (req "^7.0.3") (default-features #t) (kind 0)) (crate-dep (name "cqlmig") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "dockertest") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1zpyvbqjfkglfan096ca1q3piin7bskwql8l274ljhwssqcm1hv7")))

