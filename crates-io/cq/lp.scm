(define-module (crates-io cq lp) #:use-module (crates-io))

(define-public crate-cqlparser-0.1 (crate (name "cqlparser") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "serde-wasm-bindgen") (req "^0.3.0") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0796jif54x49mgb7in70lx9gbx0n1v7wg74x1n0m4w7dbhjjsci9")))

