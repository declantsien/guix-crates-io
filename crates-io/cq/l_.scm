(define-module (crates-io cq l_) #:use-module (crates-io))

(define-public crate-cql_bindgen-0.0.1 (crate (name "cql_bindgen") (vers "0.0.1") (hash "1d2xddiff4ylhv35ivmm39vp9acym7bny5y9gxgsnqb8av5a8hnp")))

(define-public crate-cql_bindgen-0.0.2 (crate (name "cql_bindgen") (vers "0.0.2") (hash "0s2fhxdvvc1ps3bi1d9w29dv56f7qgfb8m9zbsyvbh31v7b6wgc4")))

(define-public crate-cql_bindgen-0.0.3 (crate (name "cql_bindgen") (vers "0.0.3") (hash "0j2ml1kxqzyp157ms518krn0wcrhsmkqpkig3fax2k6amn8aczlj")))

(define-public crate-cql_bindgen-0.0.4 (crate (name "cql_bindgen") (vers "0.0.4") (hash "0ay9plzjv9rkdqzyl7ckvm4nxcab22iwrf71jx49cxyvcrck8pyd")))

(define-public crate-cql_bindgen-0.0.5 (crate (name "cql_bindgen") (vers "0.0.5") (hash "0pyn990b3y83i88jvfpzjp799k2hvrs4knn6hvrczgnm4brzkwpb")))

(define-public crate-cql_bindgen-0.0.6 (crate (name "cql_bindgen") (vers "0.0.6") (hash "12w8y44z02pbsc8iqjslg2axck2qafrr9qbssr0ylb8hqr7b3059")))

(define-public crate-cql_bindgen-0.0.7 (crate (name "cql_bindgen") (vers "0.0.7") (hash "00qphm3n5x94ib8rznk3y5bccdlck3xl8df0mq0acjp5z05mrqya")))

(define-public crate-cql_bindgen-0.0.8 (crate (name "cql_bindgen") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1ychll7cnw34q2hyl06k02jjb14mr1p95g8bwsdwj4ggaxz2wndf")))

(define-public crate-cql_bindgen-0.0.10 (crate (name "cql_bindgen") (vers "0.0.10") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "18bvq5cyi783d7f1gmwp0f43xa8lf7w5qsm2mp2r9xrh2yf7f73m")))

(define-public crate-cql_bindgen-0.0.11 (crate (name "cql_bindgen") (vers "0.0.11") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "13axsqyhl31hv30619m71yqlzvmir4v34p1p6wlm1q5bd9pd1srm")))

(define-public crate-cql_bindgen-0.0.12 (crate (name "cql_bindgen") (vers "0.0.12") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1sdp0r9dqvh36vk8li4wnvz4x1c2p6v6ygqs63nwmml24kj0igxw")))

(define-public crate-cql_bindgen-0.0.13 (crate (name "cql_bindgen") (vers "0.0.13") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "139p30zz9nk9409gyq2h2ix16av7pbg4dxhkv1ifmhpba61526qf")))

(define-public crate-cql_bindgen-0.0.14 (crate (name "cql_bindgen") (vers "0.0.14") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "077bp1ns4nr7jxrb3n8kcfaad5v64k7xw2yn6wlj1c0g1lh2as7i")))

(define-public crate-cql_bindgen-0.0.15 (crate (name "cql_bindgen") (vers "0.0.15") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0fankz6hc7vl6nd9i16d69ailip078ky3b64943p5cygincvdlya")))

(define-public crate-cql_bindgen-0.1 (crate (name "cql_bindgen") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "*") (default-features #t) (kind 0)))) (hash "06lri7mjhv783mw1vpqzi7r5m0dh4k16hb1nxm0ja2rhsmqrrxbl")))

(define-public crate-cql_bindgen-0.1 (crate (name "cql_bindgen") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0iyh22gbs51xz7p97gm9khfr14gl8mwm6sfngxagxjw4pb37av7n")))

(define-public crate-cql_bindgen-0.1 (crate (name "cql_bindgen") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1jqbw2c6p4dhihyylnlmc9pr0bahi0dw1c5cbxnlcd1hq5czq4ak")))

(define-public crate-cql_bindgen-0.2 (crate (name "cql_bindgen") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1j53iqmc6fy1n3nxq0bgagjymdchwlj3b0z1y5p8ingbyll6xb6i")))

(define-public crate-cql_db-0.1 (crate (name "cql_db") (vers "0.1.0") (deps (list (crate-dep (name "cql_model") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "cql_u64") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1z7a2zphp5567kyi1j47659r0j9mp7q280w6lxxd6zgcx565wr4x")))

(define-public crate-cql_db-0.2 (crate (name "cql_db") (vers "0.2.0") (deps (list (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cql_u64") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1d8jqwg2s4nbr5ax2skgkfv69w44wh0cyxjv7h4m70i4jk2kbpif")))

(define-public crate-cql_db-0.2 (crate (name "cql_db") (vers "0.2.1") (deps (list (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cql_u64") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1xnda3213pj485hjwh428k1bci3crsmqlrj410lrmbh0rh2y4x3c")))

(define-public crate-cql_db-0.2 (crate (name "cql_db") (vers "0.2.2") (deps (list (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cql_u64") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0pbw4bsi5g4c2yd7yz78gkqv06aj2bi2cjn74wy4hqj95a1l8j9n")))

(define-public crate-cql_db-0.2 (crate (name "cql_db") (vers "0.2.3") (deps (list (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cql_u64") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1jjcy84g18dfbbgsccmjpwv2dwxdffqmj95z5mpqwgrav2h947pl")))

(define-public crate-cql_db-0.2 (crate (name "cql_db") (vers "0.2.4") (deps (list (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cql_u64") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "14mq43yn5i6ks4zjq4fplp1v8cdhvkx3npbi4ni8xiiyk4rkv391")))

(define-public crate-cql_f64-0.2 (crate (name "cql_f64") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cql_storage_type_testing_lib") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1p4cdsld7q9xd472k102lchpddxj32f4175v4jpryap3liwxbw6h")))

(define-public crate-cql_ffi-0.0.2 (crate (name "cql_ffi") (vers "0.0.2") (hash "0z9z6bdqnf3l20xfdsjijawwqdx6m1szlld7lmrzfk4frvgcxlln")))

(define-public crate-cql_ffi-0.0.3 (crate (name "cql_ffi") (vers "0.0.3") (hash "1jp86m039bscms81447yrmv3i6swip5lm9xayq0d6yzcasp1i43y")))

(define-public crate-cql_ffi-0.0.4 (crate (name "cql_ffi") (vers "0.0.4") (hash "0mj3n7dak3p4v5x76x7klhxi9my4zbv140dhxfdxnjb7sixbabvb")))

(define-public crate-cql_ffi-0.0.5 (crate (name "cql_ffi") (vers "0.0.5") (hash "1gjbr1hwppiaa38glscjxz5d22z12wqglfpypmxdzri726wxpyw4")))

(define-public crate-cql_ffi-0.0.6 (crate (name "cql_ffi") (vers "0.0.6") (hash "1vfvr4y2lk19nb3npc3dii41wnswhv88nr1066z8zlc5jjh0224d")))

(define-public crate-cql_ffi-0.0.7 (crate (name "cql_ffi") (vers "0.0.7") (hash "0fgnw1lx1r997r3nbjvn441f5s69nm7zgg9crzg8q4wshby84kw7")))

(define-public crate-cql_ffi-0.0.8 (crate (name "cql_ffi") (vers "0.0.8") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0cbh0mxqwzjq4gxrwmcvxjzak78bk59yi0ha9vbmra37l9akir48")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.0") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0ykk2cx2g3fln1s2jmb7p2h4gl5fw0jqa6nkkmkv7nxw35n7lk95")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.1") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "078gzll5waafgs7spknpbilb4y32iyvrslj2qn9v1h26sy7045fr")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.2") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0djr0xzc19hhzhabzbrbb1455fmfm04fgnmqx1cjqvv6j9d4q2k4")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.3") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1fg95c7aq3mp06ziiv9i52i2d9b08k7ci403kjk0ixwi8fdzscxl")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.4") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0ir91mhffwh20rz587723m5lhdqlbrrgr689z91y4vjih0bigbwf")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.6") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1i6cqd2z8mf0sf4hcxj91xwm1yg70s7di9c6x70l60f8mr1yahfn")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.7") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0ik2nk11bmrg8y9qc1ni68zph8n9z3cp730bpmz9rqrdwy0acgkd")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.8") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1a1zqiqrzyhphaj6752fh17m5nj2m107f87m6a1w7di4ghhny94n")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.9") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1p191zk3j09i119zqdafvp4h19as0b880rk8xgx1k5r2c0m5imv2")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.10") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "100gq8zflv7fv9plc7x5902dq5ayyqmcj86li9dwnpv89d3jhsl1")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.11") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1mk0fyylsm3pr7idvynli88bgqjj532r5yig87bccsjch5dkk2g7")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.12") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "184yfs3bz9grii9h04k114mgksdxj6kjxsdvvipwya12aawbkhrg")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.13") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "183mrmd7bdpph587hfqanz61id9xxhl4gzc6q9mh6p6c3wbcdpax")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.15") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1krc467xpawfg9xd8xqsq0p6yjx9iacrpbv4x6ra7pjlbcg10g5b")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.16") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1z53wk1ww8sz7w689wgj0qxw446n3w2xzpzfxw65n72bjmfj27ly")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.17") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1xs25z50zw92x5wngm54wh8y1ybv790yhgq0kxqpjyvypnmvikki")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.18") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "0ihx6i01g8fpj24r929kvl6n9vg3c0x1ifyj1vlwhhw3ja1rc705")))

(define-public crate-cql_ffi-0.1 (crate (name "cql_ffi") (vers "0.1.19") (deps (list (crate-dep (name "cql_bindgen") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "136246748knj842aizqsf0wfdrv0ndfmcmfsrfjdm831vqvn603q")))

(define-public crate-cql_ffi-0.2 (crate (name "cql_ffi") (vers "0.2.1") (deps (list (crate-dep (name "cql_bindgen") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1w9mj4ygbrg5i87ajyn975whjhp553mkzy0d6wlpdywizcvnfz6b") (yanked #t)))

(define-public crate-cql_i16-0.2 (crate (name "cql_i16") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1m16286f2yj7dr4sv1mff5rbvfw6i1s4a1791xsf8bi2y7lx0ji2")))

(define-public crate-cql_model-0.1 (crate (name "cql_model") (vers "0.1.0") (hash "1lkkvhv4zbfkcsq44f4sxycb1rskfh3p7cv5ii33gj3ljws5gr4j")))

(define-public crate-cql_model-0.2 (crate (name "cql_model") (vers "0.2.0") (hash "1f00f0w2cz5fdap3vd78hn5nx2fa52j6g60mg8bwwqh1l0xlrn33")))

(define-public crate-cql_nullable_f64-0.1 (crate (name "cql_nullable_f64") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1m28yfavx4lc44pdyb9izrzqjmsafbd2499m62hi76mmfrvaaxk1")))

(define-public crate-cql_nullable_f64-0.2 (crate (name "cql_nullable_f64") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0j0y5svl9pvvyqy8jdjspjqdwcg55kk7slnxmkk8hdx0qq07nmx3")))

(define-public crate-cql_nullable_f64-0.2 (crate (name "cql_nullable_f64") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1ha2r5as970hqmlajinljq2cy7zi712vaz1p631bxk5bpnwkxwf3")))

(define-public crate-cql_nullable_f64-0.2 (crate (name "cql_nullable_f64") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1gkwlw0hsi79yp0sc8vkvw89s4jxd99nszdbqhd41nfc5gnn46i2")))

(define-public crate-cql_storage_type_testing_lib-0.2 (crate (name "cql_storage_type_testing_lib") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wmz7zfkks0vz78gpcg9g3mhf0b7ashbbwvvp7m9b6ak0zj1za1b")))

(define-public crate-cql_storage_type_testing_lib-0.3 (crate (name "cql_storage_type_testing_lib") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "08qi6bjwfikaljid2bq0hcz0yqzyf8rpj5gmbl37l0gidqzrk5va")))

(define-public crate-cql_storage_type_testing_lib-0.4 (crate (name "cql_storage_type_testing_lib") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x67jmnf8407sv58vy1j65ni0sbz2a5jfrmq1z5nlb3yfah8xras")))

(define-public crate-cql_storage_type_testing_lib-0.4 (crate (name "cql_storage_type_testing_lib") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q2nqjmyccrwwqhrdv0lc4amj79s42ax2lfly5jl59a7197gwrwn")))

(define-public crate-cql_tiny_text-0.1 (crate (name "cql_tiny_text") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1nkmlg3176djnbr2dgg6c04pc06w8wr78lp5qdswcrnllb5mi7ii")))

(define-public crate-cql_tiny_text-0.2 (crate (name "cql_tiny_text") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "16330hy8hx9l2jpi72q8h81087lmi7v48cn8ynlkpklzm8s2j56v")))

(define-public crate-cql_tiny_text-0.2 (crate (name "cql_tiny_text") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0b5zlv1lmsyjgf4ijr4cl8ps5g4km0k3ifh2m3x8w40l58fljflf")))

(define-public crate-cql_u64-0.1 (crate (name "cql_u64") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_model") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "128ndfrci4hhvqaxi4fx0vdky8nsnj34yscq2rlikr53xhmmzq46")))

(define-public crate-cql_u64-0.2 (crate (name "cql_u64") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0n3q2y3iiicmvywz60ky4h4kfy0a7psijp6sxs15cx57dxppc5y9")))

(define-public crate-cql_u64-0.2 (crate (name "cql_u64") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "07dm8m8r2cxcywxq4q8n78qpi18jmal7iznh85q7fiaab43zaglq")))

(define-public crate-cql_u64-0.2 (crate (name "cql_u64") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "08s08hjgj6piszmy9s0xnlp3d4y9h0gij5sjznxbwg6jqf7phg2y")))

(define-public crate-cql_u64-0.2 (crate (name "cql_u64") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cql_db") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "cql_model") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0waarjpc66zf8c1kq1w79jzdgawpj0q18r2f94fpbh8xzz4jhyy4")))

