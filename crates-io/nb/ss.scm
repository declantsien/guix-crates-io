(define-module (crates-io nb ss) #:use-module (crates-io))

(define-public crate-nbssh-0.1 (crate (name "nbssh") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "1camqn0sr5sv7ld1yb4yckqr0fjfzn0428y7spd6jvhkmcn8hxpp")))

(define-public crate-nbssh-0.2 (crate (name "nbssh") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "020vaihlyxn9bisr8r9dcbsy8jblnpml2vhhnyka3yq0d4y3dbcm")))

(define-public crate-nbssh-0.3 (crate (name "nbssh") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w0pbkrkfczydhai9vm4i52vhsv2vkxryq9f4kvibiy96x81pv2c")))

(define-public crate-nbssh-0.4 (crate (name "nbssh") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "0s9xjbb5l9hbvfb2cpsqkd769acl9mv9pgmb20brbf1cqgf1jgmh")))

(define-public crate-nbssh-0.5 (crate (name "nbssh") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "0665dxlxaqivhm0qz50kfyrhwcliv9g63dds7yhkb1l6wm5dgc5l")))

(define-public crate-nbssh-0.6 (crate (name "nbssh") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vrb9xsa5ymn780899zw0hmrdf875v0yngngm3v9xix91j2f9v1l")))

(define-public crate-nbssh-0.7 (crate (name "nbssh") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jdr2729qsf0mhsdrmikbmdr71jrg3v39a400zqyymk8zcwwxsr5")))

(define-public crate-nbssh-0.8 (crate (name "nbssh") (vers "0.8.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "0k68xi964b61xsmxmmbshlajd6i12jxx617mam63lshgbsb8il0l")))

(define-public crate-nbssh-1 (crate (name "nbssh") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "subprocess") (req "^0.1") (default-features #t) (kind 0)))) (hash "10badsb8dhqd70z3bg71f300asxj7qkn1mdyap4d1wkb9wg0lkjw")))

(define-public crate-nbssh-2 (crate (name "nbssh") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "10nkrh6a8isb373cxagknnww4gxah7c0fc4d57a45bg9varad9k2")))

(define-public crate-nbssh-2 (crate (name "nbssh") (vers "2.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hr36jj7y8gvqf77i788rvibqxpzm7vqa458n0safz4mw8fj4mkj")))

(define-public crate-nbssh-3 (crate (name "nbssh") (vers "3.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0375jvsbvs5yhkyqqs95j4v0ljgkad5jwbwbj5b0vlljl5cgzjmr")))

(define-public crate-nbssh-3 (crate (name "nbssh") (vers "3.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0za3kkhw1kjkimsd5z3lyk3hahj7g39625f0k99ajjsj4rrw736a")))

