(define-module (crates-io nb ib) #:use-module (crates-io))

(define-public crate-nbib-0.1 (crate (name "nbib") (vers "0.1.0") (hash "0d1s86rv8fqp9sivfhl82xdz79p60dsah2q577k2s1rlwsbjlr4y")))

(define-public crate-nbib-0.1 (crate (name "nbib") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0wlza66s821s1skvc2fflwvfpw0w8hf4q9ymw2fs68m6fv7pr8g6")))

