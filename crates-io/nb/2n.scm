(define-module (crates-io nb #{2n}#) #:use-module (crates-io))

(define-public crate-nb2nl-0.1 (crate (name "nb2nl") (vers "0.1.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0i8vyq8wnirmz5d4y0xa35pd8qqd6ligzn3kqa1hk5skpnl5d8iq")))

(define-public crate-nb2nl-0.2 (crate (name "nb2nl") (vers "0.2.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19.6") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "ordslice") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1yq3vc2kzvzwzazrvnm1az1g29pnb2h69czn9ssyqvgagfi0751s")))

