(define-module (crates-io nb s-) #:use-module (crates-io))

(define-public crate-nbs-rs-0.1 (crate (name "nbs-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "131zi3al7wgr4fyr9gzvws59vwxcbifdnzl7kgs1zih0kwzqwsak")))

(define-public crate-nbs-rs-0.1 (crate (name "nbs-rs") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "19p0n2xzmdym9s78bl9r99yxx637blrqrf12lkv21s0dbrin4mrr")))

