(define-module (crates-io nb od) #:use-module (crates-io))

(define-public crate-nbody_barnes_hut-0.1 (crate (name "nbody_barnes_hut") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1s403pywpg1nl2sg5v3bicka3csbknp48b05sbkxaz66p88vzgpm")))

