(define-module (crates-io nb co) #:use-module (crates-io))

(define-public crate-nbconf-0.5 (crate (name "nbconf") (vers "0.5.0") (hash "1acia26aqfpjp0h41f2kpsh28rawhn4j109vp004fxqwpav7lvmz")))

(define-public crate-nbconf-0.6 (crate (name "nbconf") (vers "0.6.0") (hash "01rpighwjg2fc6if9f6c5lvgqp6jsnjvyzvvag9j93s8r22klyzy")))

(define-public crate-nbconf-0.7 (crate (name "nbconf") (vers "0.7.0") (hash "1a3vgnkas0abfln3vf37ayz1mpa1cpcf25hbj2zr16i3bc6x8in3")))

(define-public crate-nbconf-0.8 (crate (name "nbconf") (vers "0.8.0") (hash "1h60hpgsy71syxxm61kddwaf9ac7jdw3hj5l9da8r8balshk7ymh")))

(define-public crate-nbconf-0.9 (crate (name "nbconf") (vers "0.9.0") (hash "0njrvyf9a0bvi4py6jlxdrnblw7w7w3y0f4aag9w1v22yn6bj8w6")))

(define-public crate-nbconf-1 (crate (name "nbconf") (vers "1.0.0") (hash "06bj2ny4hkdzgaqg8mrkfx60y4fvrpyl9jgrn7mc29jmig44gibn")))

(define-public crate-nbconf-1 (crate (name "nbconf") (vers "1.0.1") (hash "12prqz3wal9m0ahn1pbrg5p3p0g734qj2pkrvnvb0vssq2ybqrq1")))

