(define-module (crates-io nb lo) #:use-module (crates-io))

(define-public crate-nblock-0.1 (crate (name "nblock") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.20") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "spinning_top") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0157cpv658k0vmpsicf033s0pjr8a68wg7d4jsbd75i1zs08gi1m")))

(define-public crate-nblock-0.1 (crate (name "nblock") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.20") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "spinning_top") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1qfmv0s5xf4xqp3k6bfi05k8dqp2jcjliqaf693yyc2xw0yrxk28")))

