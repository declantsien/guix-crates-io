(define-module (crates-io nb a_) #:use-module (crates-io))

(define-public crate-nba_api-0.1 (crate (name "nba_api") (vers "0.1.0") (hash "1x74yrkzn2pgsxbry4sgc0synvzs4mhpkazbxvny5cnln76vwsmj")))

(define-public crate-nba_api-0.1 (crate (name "nba_api") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "10kcxg8h6m5lqfc62f3jx7nany25wip5qa0jpfd9yi1q9azci2nk")))

