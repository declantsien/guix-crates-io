(define-module (crates-io nb ez) #:use-module (crates-io))

(define-public crate-nbez-0.1 (crate (name "nbez") (vers "0.1.0") (deps (list (crate-dep (name "gfx") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "gfx_window_glutin") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "191s108axzb1arn10rhidwk6vfplnqjpl6azlwqbrfmz9gsb5imp")))

(define-public crate-nbezier-0.2 (crate (name "nbezier") (vers "0.2.1") (deps (list (crate-dep (name "nalgebra") (req "^0.31") (default-features #t) (kind 0)))) (hash "1v0zddww69ajknb8z7nxqwn386648mw74lpgjzhgxphz19m4ac59") (features (quote (("draw-svg" "draw") ("draw") ("default"))))))

