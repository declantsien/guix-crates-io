(define-module (crates-io nb t-) #:use-module (crates-io))

(define-public crate-nbt-parser-1 (crate (name "nbt-parser") (vers "1.0.0") (deps (list (crate-dep (name "combine") (req "^3.3.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0ka61bswwl7kpzi7806nfgcj0fpy4m1gqhss28jhp2ik3ydqkjy6")))

