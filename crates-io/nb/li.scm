(define-module (crates-io nb li) #:use-module (crates-io))

(define-public crate-nblistener-0.1 (crate (name "nblistener") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "ws2_32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1ahwgpcr13xhhzfzdfbbksl6s5n7720ysh2xqjc6mlb0w15sswwv")))

(define-public crate-nblistener-0.1 (crate (name "nblistener") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1pnxszz3432jq7nhj6cjw8p5ymbx5l46rrsgjxdsbi9wkp6q483n")))

