(define-module (crates-io nb tq) #:use-module (crates-io))

(define-public crate-nbtq-0.1 (crate (name "nbtq") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.1") (kind 0)) (crate-dep (name "hematite-nbt") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "0nzssj4hwfbz9rnykl57frw6v8i9n1vbx3z4alcf9v8hwviin307")))

