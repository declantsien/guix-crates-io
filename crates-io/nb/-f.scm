(define-module (crates-io nb -f) #:use-module (crates-io))

(define-public crate-nb-field-names-0.1 (crate (name "nb-field-names") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "1rabhrm793bryvgxkgk6vcs2zj4ihn0lphqzr7y6zs51h20f90xv")))

(define-public crate-nb-from-env-0.1 (crate (name "nb-from-env") (vers "0.1.0") (deps (list (crate-dep (name "from-env-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "14kap4jkhljs9wpi18kqrflpgzq3r88az1kimkdirn6rb7sb97mx")))

(define-public crate-nb-from-env-0.1 (crate (name "nb-from-env") (vers "0.1.1") (deps (list (crate-dep (name "from-env-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "11qjs19ychkgklw0f89an5hd5v9w0f71lj9kjrlrzbd96paqpnjy")))

(define-public crate-nb-from-env-0.2 (crate (name "nb-from-env") (vers "0.2.0") (deps (list (crate-dep (name "from-env-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mpmf32bay08n0ylibk1cslrmfmyb3iyn00ixv8n966fv2mp8f6v")))

(define-public crate-nb-from-env-0.2 (crate (name "nb-from-env") (vers "0.2.1") (deps (list (crate-dep (name "from-env-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qlbgl1v8rgqaryqwmf7vw6zmz706pcbsq8qz5c88a3j5xq2fhlq")))

