(define-module (crates-io nb it) #:use-module (crates-io))

(define-public crate-nbits_vec-0.1 (crate (name "nbits_vec") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.1") (default-features #t) (kind 0)))) (hash "11ag4c5g7kxg93d9gzamxij9q2mbmqx1n12m27h7zwgs6v6yb1mx") (features (quote (("nightly" "clippy") ("default"))))))

(define-public crate-nbits_vec-0.1 (crate (name "nbits_vec") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.1") (default-features #t) (kind 0)))) (hash "10hb60v0r5xh2ia5c422v5zpcvkrzkjwhvymdw769zn7l2gdflrr") (features (quote (("nightly" "clippy") ("default"))))))

