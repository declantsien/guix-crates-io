(define-module (crates-io nb -b) #:use-module (crates-io))

(define-public crate-nb-blocking-util-0.10 (crate (name "nb-blocking-util") (vers "0.10.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qswh0qlzrnycr7bb7mqlkwx8844icw1ddwg72z8f67wl464vnz1")))

(define-public crate-nb-blocking-util-0.10 (crate (name "nb-blocking-util") (vers "0.10.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "125n59j2bdc0yjsa6yc9l0r6b0m1az1v7wk34inpkn215h40iqh6")))

