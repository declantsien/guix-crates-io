(define-module (crates-io nb ts) #:use-module (crates-io))

(define-public crate-nbtscanner-0.0.1 (crate (name "nbtscanner") (vers "0.0.1") (deps (list (crate-dep (name "assert_matches") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.29.1") (default-features #t) (kind 0)))) (hash "0yihywwn5ay7w8d5kff52483g8z1fw7zknmngy28r11yvpcfmd26")))

