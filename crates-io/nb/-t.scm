(define-module (crates-io nb -t) #:use-module (crates-io))

(define-public crate-nb-to-query-0.1 (crate (name "nb-to-query") (vers "0.1.0") (deps (list (crate-dep (name "nb-to-query-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gn2gjzrbm7kn1k04kh89z9ffz0iklnrn8ynlgl07k5q9nglmsk0")))

(define-public crate-nb-to-query-0.1 (crate (name "nb-to-query") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "nb-to-query-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "03x46a10fy11aja8xm5yn28r5v4hq9h85pyqnlcz4nh4axws0l9s") (features (quote (("chrono"))))))

(define-public crate-nb-to-query-derive-0.1 (crate (name "nb-to-query-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "13vdz0nc1llq812yv236apcblgl7ncrkxdgw2ldnhi8yga3rabjv")))

(define-public crate-nb-to-query-derive-0.1 (crate (name "nb-to-query-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "0ha1k351rm4af194ybbil4ws87xw1vxhk6pp6h1y1lqmqldz0gli")))

