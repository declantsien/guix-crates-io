(define-module (crates-io nb ds) #:use-module (crates-io))

(define-public crate-nbdserve-0.1 (crate (name "nbdserve") (vers "0.1.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nbd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (kind 0)))) (hash "0wmih34128bk5fyga6ka0j1vq29v1im5syb15p3vdy00y3w5km2s")))

(define-public crate-nbdserve-0.1 (crate (name "nbdserve") (vers "0.1.1") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nbd") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (kind 0)))) (hash "0k6dqdvd384sa39cr5c4nv5jn0v7ms1k5lsi97lk11acvqwdliw8")))

