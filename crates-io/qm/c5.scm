(define-module (crates-io qm c5) #:use-module (crates-io))

(define-public crate-qmc5883l-0.0.1 (crate (name "qmc5883l") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (kind 2)))) (hash "1wnflda62l4dbv7dmxl3g636050vcrf9c6p7pqab402cyk7frhfk")))

