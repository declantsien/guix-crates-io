(define-module (crates-io qm lr) #:use-module (crates-io))

(define-public crate-qmlrs-0.0.1 (crate (name "qmlrs") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "0sbf4qqynxs8jafx396l8bscz30gyrjvyvr1fd3rsak5n28yvpcm")))

(define-public crate-qmlrs-0.1 (crate (name "qmlrs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.6") (default-features #t) (kind 1)))) (hash "04qplc0gq81sf7s71hjz9c57r40v7bxfyq17qjrfci3j6fahjvbk")))

(define-public crate-qmlrs-0.1 (crate (name "qmlrs") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "0qxzm51238w5jlh2713mk6vfhz68hdc0wsbv9iw51w7nv7zscr65")))

