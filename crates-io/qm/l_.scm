(define-module (crates-io qm l_) #:use-module (crates-io))

(define-public crate-qml_formatter-0.1 (crate (name "qml_formatter") (vers "0.1.0") (deps (list (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "00mh839jx8w40im8n5hwk8l4rzbm9isrjf2ckrysnl5qw9baa6m5") (rust-version "1.60")))

(define-public crate-qml_formatter-0.2 (crate (name "qml_formatter") (vers "0.2.0") (deps (list (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0x3awsxngpyf5nbkwynnw71hcgjvjnq49ivblw67znxn7477bkbb") (rust-version "1.60")))

