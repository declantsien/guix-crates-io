(define-module (crates-io qm ac) #:use-module (crates-io))

(define-public crate-qmachina-0.1 (crate (name "qmachina") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0i4f8nrzibhk2pl1ai4diz6clhg1fyg1c8n9qpwgiv74xqam4hdw")))

(define-public crate-qmachina-0.1 (crate (name "qmachina") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "162ccrss98r6znsafvp7prarknh8czp43m1nrjfgi5g537vyb0ss")))

(define-public crate-qmachina-0.1 (crate (name "qmachina") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)))) (hash "0rjrgsh051kl6frh8vv09hj3c5yy807ysll2h9sbgx8m39mq69h0")))

(define-public crate-qmachina-0.2 (crate (name "qmachina") (vers "0.2.0-dev") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)))) (hash "1d3jb33v5ch7hgwpc1dsnsv1rvbwr35spa0xiyp6hm0ciw2hy9li")))

(define-public crate-qmachina-0.2 (crate (name "qmachina") (vers "0.2.0-dev.1") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)))) (hash "0s9qxf08inb64h1rbfsdchrbsrs71134q7n0dyafqs27q4nq5rdx")))

(define-public crate-qmachina-0.2 (crate (name "qmachina") (vers "0.2.0-dev.2") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)))) (hash "0am6vxlndl0bhll6294cvhabl2kihx334dw81228g3azxc6mvwc6")))

(define-public crate-qmachina-0.2 (crate (name "qmachina") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)))) (hash "1mk026mrnf2xmrd1cfx5af9syk4ncb65p48sqi07qn4dxnmykmdp")))

(define-public crate-qmachina-0.3 (crate (name "qmachina") (vers "0.3.0-dev") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "=0.36.2") (optional #t) (default-features #t) (kind 0)))) (hash "0mf5xjf4nn81csdv2b9c219s131cn7mfav8jkbs3c9hc87hjk3b3") (v 2) (features2 (quote (("polars" "dep:polars"))))))

(define-public crate-qmachina-0.3 (crate (name "qmachina") (vers "0.3.0-dev.1") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "=0.36.2") (optional #t) (default-features #t) (kind 0)))) (hash "156y6w452kazcw3d51nh8jy8gz93a05m2z0lqsi77nk7gvavhkbw") (v 2) (features2 (quote (("polars" "dep:polars"))))))

(define-public crate-qmachina-0.3 (crate (name "qmachina") (vers "0.3.0-dev.2") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "=0.36.2") (optional #t) (default-features #t) (kind 0)))) (hash "1zrls6hxng6f821gdc6gmwbjya161jfqb25zfg9mm53xhlhlp0ql") (v 2) (features2 (quote (("polars" "dep:polars"))))))

(define-public crate-qmachina-0.3 (crate (name "qmachina") (vers "0.3.0-dev.3") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "=0.36.2") (optional #t) (default-features #t) (kind 0)))) (hash "1022wc5ciqiwjxmjknl8dhrml4xqs68gazgzjvjl00sc96blz4x3") (v 2) (features2 (quote (("polars" "dep:polars"))))))

(define-public crate-qmachina-0.3 (crate (name "qmachina") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "=1.0.79") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "=0.36.2") (optional #t) (default-features #t) (kind 0)))) (hash "0dayh6xqkd6c2xzwm59snb7l83qwikh9vnazdcvg0hvpyvxlw92f") (v 2) (features2 (quote (("polars" "dep:polars"))))))

