(define-module (crates-io qm ap) #:use-module (crates-io))

(define-public crate-qmap-0.1 (crate (name "qmap") (vers "0.1.0") (deps (list (crate-dep (name "data-encoding") (req "^2.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0h2si90kyd5zkwyk1a7kh7nb1fgq6w48s90awzr43kz7hcxxdr0g")))

