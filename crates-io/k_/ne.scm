(define-module (crates-io k_ ne) #:use-module (crates-io))

(define-public crate-k_nearest-0.1 (crate (name "k_nearest") (vers "0.1.0") (hash "1wgs3bajr1ghqf3sicazwawliczkfg0wilmyxxwn28amjppjfpzg")))

(define-public crate-k_nearest-0.2 (crate (name "k_nearest") (vers "0.2.0") (hash "04n98ralldmkhyhfy9b747bkbjgwh5s2v1g0zsi7xndfh6g630k9")))

