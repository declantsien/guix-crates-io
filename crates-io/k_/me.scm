(define-module (crates-io k_ me) #:use-module (crates-io))

(define-public crate-k_means-interactive-0.1 (crate (name "k_means-interactive") (vers "0.1.0") (deps (list (crate-dep (name "slint") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "slint-build") (req "^1.5.1") (default-features #t) (kind 1)))) (hash "06ja9fhqpx1dmjsfjwvcdny9563rldcyny24q3z4jnpr3kjsl6bp")))

