(define-module (crates-io ab su) #:use-module (crates-io))

(define-public crate-absurdler-0.1 (crate (name "absurdler") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "11s2w7q1rnc4rk726jipla604cfv2awvn81p1idmj03ilg1xwiyv")))

(define-public crate-absurdler-0.1 (crate (name "absurdler") (vers "0.1.1") (deps (list (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "1qx9fwws2bgwrc9xvl7w24gq11gr0xsdqyg560yhcl2x1iz1w4p1")))

