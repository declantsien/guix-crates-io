(define-module (crates-io ab tr) #:use-module (crates-io))

(define-public crate-ABtree-0.1 (crate (name "ABtree") (vers "0.1.0") (hash "0kkl8qiylpa5z10izljpb8zgn561gfg04b8c8yw1d5xjy7hkhbi1") (yanked #t)))

(define-public crate-ABtree-0.1 (crate (name "ABtree") (vers "0.1.1") (hash "1xndm2d4mdqh1msv3mljknm8b44whaikx0xy4kmpi9hf62mcclb9") (yanked #t)))

(define-public crate-ABtree-0.1 (crate (name "ABtree") (vers "0.1.2") (hash "13vly9952h2kasr376zk58x4fsh5spqrcxjrm2625p92j0rpw0fz") (yanked #t)))

(define-public crate-ABtree-0.2 (crate (name "ABtree") (vers "0.2.0") (hash "0q5d4pknayp3x7y5kk0sb0i8s5fsjgxjg638wx304wpa06vn6l0w") (yanked #t)))

(define-public crate-ABtree-0.3 (crate (name "ABtree") (vers "0.3.0") (hash "1fqbwdql9yaj8kcf57zfrlwygjb7vf2f7alavz7ygzbbxsja8gcz") (yanked #t)))

(define-public crate-ABtree-0.4 (crate (name "ABtree") (vers "0.4.0") (hash "0s50nz0pr7g7x9hlpgbls169cbi1azdw289vbq29xpwm3p2y59p4") (yanked #t)))

(define-public crate-ABtree-0.5 (crate (name "ABtree") (vers "0.5.0") (hash "10k57yj36qm2pikap8z19v0vnnmikpv5pbwfwvwil4vld4v060jk") (yanked #t)))

(define-public crate-ABtree-0.6 (crate (name "ABtree") (vers "0.6.0") (hash "16wws14q7v2v6w9xz24l8by0g38hhpwi38qdjcph7nv0xf52qcaj") (yanked #t)))

(define-public crate-ABtree-0.7 (crate (name "ABtree") (vers "0.7.0") (hash "0l0qy8z9gdg9ni5lkz3yhggwmskh118k6fr2cmk1skiyv21x89jv") (yanked #t)))

(define-public crate-ABtree-0.8 (crate (name "ABtree") (vers "0.8.0") (hash "1ywhxp8ak0h6qif0kpaskpxyb9gjc67ypnn5j30viqg5k7hw3jmq")))

