(define-module (crates-io ab er) #:use-module (crates-io))

(define-public crate-aberth-0.0.0 (crate (name "aberth") (vers "0.0.0") (hash "0hx3bvcp0d1l81izg4pls0myd8458ddkxjy40638rqq4sh6l2l6s")))

(define-public crate-aberth-0.0.1 (crate (name "aberth") (vers "0.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ji8i2njy0vh41fk47rd71m5y1gzkikmw4dakq64mvw46dmvdf0p") (yanked #t)))

(define-public crate-aberth-0.0.2 (crate (name "aberth") (vers "0.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "12ynkh5mv6fhzycipz3pqq78wmv40wfia8h6qmhjlq6yd7nd2p7j") (yanked #t)))

(define-public crate-aberth-0.0.3 (crate (name "aberth") (vers "0.0.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1za0wwvyzjcg54qsf58y7m3gxiwpx080mlbqh0x80kkpkqj99fab")))

(define-public crate-aberth-0.0.4 (crate (name "aberth") (vers "0.0.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1y7nd7k0s2a4mbcnx1wz2a3m35w96zramxqmrxxif5f0gkzwl6y2") (features (quote (("std" "num-traits/default" "num-complex/default") ("libm" "num-traits/libm" "num-complex/libm") ("default" "std"))))))

(define-public crate-aberth-0.4 (crate (name "aberth") (vers "0.4.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1yr6hr0lmmld1634zvgk3xwr3fgd4dyxrva1ynl7k0fqzgx87dww") (features (quote (("std" "num-traits/default" "num-complex/default") ("libm" "num-traits/libm" "num-complex/libm") ("default" "std"))))))

