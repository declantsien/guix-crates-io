(define-module (crates-io ab so) #:use-module (crates-io))

(define-public crate-absolut-0.1 (crate (name "absolut") (vers "0.1.1") (deps (list (crate-dep (name "absolut-macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "033nsdvvc06igwpj63qahphbmvxfs9ax6vv7qpdxqb7c1prjmfav")))

(define-public crate-absolut-0.2 (crate (name "absolut") (vers "0.2.0") (deps (list (crate-dep (name "absolut-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wkhx5w9ccmgfkqabcissnbxp2yx4z5jzl25s1chkcbdqzyl20h6") (features (quote (("sat" "absolut-macros/sat")))) (yanked #t)))

(define-public crate-absolut-0.2 (crate (name "absolut") (vers "0.2.1") (deps (list (crate-dep (name "absolut-macros") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1ma63sbwbzyl7il3l3m6s6a7d7vg1yyp97iblcdyyl4sdm7m1azz") (features (quote (("sat" "absolut-macros/sat"))))))

(define-public crate-absolut-core-0.1 (crate (name "absolut-core") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "z3") (req "^0.11.2") (features (quote ("static-link-z3"))) (default-features #t) (kind 0)))) (hash "16xfraj8faz0gyfjah14ykp6945jfnqlfvxblc4815l7g8vkp1v6") (yanked #t)))

(define-public crate-absolut-derive-0.1 (crate (name "absolut-derive") (vers "0.1.0") (deps (list (crate-dep (name "absolut-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.26") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0azr0ggvmhar28w676i4c1d77ngb4r58awn2rpz3xaadwg3239b5") (yanked #t)))

(define-public crate-absolut-macros-0.1 (crate (name "absolut-macros") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.26") (default-features #t) (kind 0)) (crate-dep (name "z3") (req "^0.11.2") (features (quote ("static-link-z3"))) (default-features #t) (kind 0)))) (hash "1ym60mrr0qv18waigrs8aiiy11dn8y37pk58ks6zcdl1kmypbkzi")))

(define-public crate-absolut-macros-0.2 (crate (name "absolut-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.26") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "132xpjxznakcp3lvp1n5hpvm5dmsxsx3pxbna2qci1xkzdhlw2q7") (yanked #t) (v 2) (features2 (quote (("sat" "dep:varisat"))))))

(define-public crate-absolut-macros-0.2 (crate (name "absolut-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.26") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "varisat") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "1z7210ycdpw562yg7ylah9grbscx17hnnv4shb5924ipzmd1kwn2") (v 2) (features2 (quote (("sat" "dep:varisat"))))))

(define-public crate-absolution-0.0.1 (crate (name "absolution") (vers "0.0.1") (hash "11fvg9vj660vdyhkz3cn1kcl8rhl3fqp1dpxpa0p4975zk6ijixy")))

(define-public crate-absolution-0.1 (crate (name "absolution") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "18jrnpahzpa67mnqnpschi2jmmi1wmpcbx9i1gdlkj4cj8n264vw") (features (quote (("nightly-doc"))))))

(define-public crate-absolution-0.1 (crate (name "absolution") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0wbyp93pny6jnzc6xp7j0bnp3yfyyz2ynqlkpb5zjim2hjw76qfn") (features (quote (("nightly-doc"))))))

