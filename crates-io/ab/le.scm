(define-module (crates-io ab le) #:use-module (crates-io))

(define-public crate-able-0.0.0 (crate (name "able") (vers "0.0.0") (hash "1k1skhjkssv21ybp3cy6xigxzfysr65rwk1bmxwc340q8iy24iay")))

(define-public crate-ablescript-0.2 (crate (name "ablescript") (vers "0.2.0") (deps (list (crate-dep (name "logos") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ir8clp4i8jc1c6wq724lwpl21xxvn3j1qsq97d3im8qdqqracw4")))

(define-public crate-ablescript-0.3 (crate (name "ablescript") (vers "0.3.0") (deps (list (crate-dep (name "logos") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0fkcm1dq238pnapj7i3g7q51rx1clp0ab3n2iacnmj1bwrcjj8xk")))

(define-public crate-ablescript-0.4 (crate (name "ablescript") (vers "0.4.0") (deps (list (crate-dep (name "logos") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "15jdpv9drayp53xsdfy3spyrrk71xw5g23kcpybnjgq7wkxmls0l")))

(define-public crate-ablescript-0.5 (crate (name "ablescript") (vers "0.5.0") (deps (list (crate-dep (name "logos") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "07flbk058rbxqi6y2v5kh0kr8ig60xv9q648wqigszz3gm5d7v48")))

(define-public crate-ablescript-0.5 (crate (name "ablescript") (vers "0.5.2") (deps (list (crate-dep (name "logos") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1yr7blycgixah87y87b0assj482jkxm6nvkv5wq3rwi8z24syw1g")))

(define-public crate-ablescript-0.5 (crate (name "ablescript") (vers "0.5.3") (deps (list (crate-dep (name "logos") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0rm58nxhxc7lafcbs0i6h5vijvcbdqwrb20h3bvag731bwsciyyf")))

(define-public crate-ablescript-0.5 (crate (name "ablescript") (vers "0.5.4") (deps (list (crate-dep (name "logos") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0j92l9086yq6hzhc1j21dcm2aaq0wwr5gxanhrqm0d7akl37cgsz")))

(define-public crate-ablescript_cli-0.2 (crate (name "ablescript_cli") (vers "0.2.0") (deps (list (crate-dep (name "ablescript") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "0ivdx66yms5ia7mb1i1n1mqg8g29nfrsp2nik3hb9afggnb1asd6")))

(define-public crate-ablescript_cli-0.3 (crate (name "ablescript_cli") (vers "0.3.0") (deps (list (crate-dep (name "ablescript") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1") (default-features #t) (kind 0)))) (hash "00f2sc27vffwq6z42ifrlw2dxcq8blxr2wi100cb5jqj6hz1w7k1")))

(define-public crate-ablescript_cli-0.4 (crate (name "ablescript_cli") (vers "0.4.0") (deps (list (crate-dep (name "ablescript") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1") (default-features #t) (kind 0)))) (hash "0m1756mb13j3v1qgk1dwcl4gzydy9zcq5zb7fl9agq1mqwd1q70z")))

(define-public crate-ablescript_cli-0.5 (crate (name "ablescript_cli") (vers "0.5.0") (deps (list (crate-dep (name "ablescript") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1") (default-features #t) (kind 0)))) (hash "02ag7dnrl2m0da2waalkl3rpjsfbp7kmjcrb1w68111v4jpq9yxk")))

(define-public crate-ablescript_cli-0.5 (crate (name "ablescript_cli") (vers "0.5.2") (deps (list (crate-dep (name "ablescript") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1") (default-features #t) (kind 0)))) (hash "1cbg35jyc9avwf97p4w4ks3815553795npmvin576s6g4hx1c129")))

(define-public crate-ablescript_cli-0.5 (crate (name "ablescript_cli") (vers "0.5.3") (deps (list (crate-dep (name "ablescript") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0") (default-features #t) (kind 0)))) (hash "07l5ps0kd4z6gwbl798hyn7y05rxnf2c7bni550dl6ssxxrn806s")))

(define-public crate-ablescript_cli-0.5 (crate (name "ablescript_cli") (vers "0.5.4") (deps (list (crate-dep (name "ablescript") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0") (default-features #t) (kind 0)))) (hash "1bha6wwkdcn27h3vyl0m9pyf2fy041apikyi8gwp836j1dh7y2w9")))

(define-public crate-abletime-0.1 (crate (name "abletime") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)))) (hash "1pmgh9nna05p9a6yy5qdrv32girsas82yrn6drf8blja6zcx62qm")))

(define-public crate-abletime-0.1 (crate (name "abletime") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0nyvvjwxa5b6yp62zha6x9pd007qaq4wrdnb5yv9ivj6k25mq8xs")))

(define-public crate-ableton-link-0.1 (crate (name "ableton-link") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1vfw9aa8q5rdk9dqkvd4pdq4hm8bv42ggj526yaf6lxjb8rg28iq")))

