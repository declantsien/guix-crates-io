(define-module (crates-io ab ou) #:use-module (crates-io))

(define-public crate-about-0.1 (crate (name "about") (vers "0.1.0") (hash "0rbqgy3zgk1rbc7nxcpqxcsbf6g8360pgjg9svz8girccb61r4ac") (yanked #t)))

(define-public crate-about-0.1 (crate (name "about") (vers "0.1.1") (hash "0w0jvw5bxnfaxavp17xycvxb7zhdkbgm6fl317pa6y9ssk27yh56") (yanked #t)))

(define-public crate-about-filter-0.1 (crate (name "about-filter") (vers "0.1.0") (deps (list (crate-dep (name "comrak") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "rst_parser") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rst_renderer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0ady9rhbdbkxy2h72s9w88g9yq3hpxi72pvpx86b814dmi6gj33f")))

(define-public crate-about-filter-0.1 (crate (name "about-filter") (vers "0.1.1") (deps (list (crate-dep (name "comrak") (req "^0.13.0") (kind 0)) (crate-dep (name "html-escape") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "rst_parser") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rst_renderer") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1b4az2kw25wpvy1jlq7kwv10c0nvjj94rgwcl4r8xvflw1vg5jgk")))

(define-public crate-about-system-0.1 (crate (name "about-system") (vers "0.1.0") (deps (list (crate-dep (name "cpuid") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "quick-error") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "rs-release") (req "^0.1.7") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "semver") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.3.17") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "uname") (req "^0.1.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "win32-error") (req "^0.9.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "13r87p7l0ddpkivqv7vy67p6r0v3fnkzlc9kzx77dryswz0pyzlb")))

