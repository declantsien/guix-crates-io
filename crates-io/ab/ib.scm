(define-module (crates-io ab ib) #:use-module (crates-io))

(define-public crate-abibool-0.5 (crate (name "abibool") (vers "0.5.0") (hash "1kdaj3pmgn1i0vbq9cmsq6h81a2b390gavj5mzmqrhwzskixlcj1")))

(define-public crate-abibool-0.5 (crate (name "abibool") (vers "0.5.1") (deps (list (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ixhili7bllj17l3was25qfl7dbi9rspx7licz6vy9w59w1f33sh")))

(define-public crate-abibool-0.5 (crate (name "abibool") (vers "0.5.2") (deps (list (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1k4hhdycpclfmpnhhzl1fgqbnbnfdyb5arpb33zmif73p1kxwi2g")))

(define-public crate-abibool-0.5 (crate (name "abibool") (vers "0.5.3") (deps (list (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0pifmpzwx9n7x404gr9x0vyy21ajaivm22116kd0y0b6w1chzlqb")))

