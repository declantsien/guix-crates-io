(define-module (crates-io ab a-) #:use-module (crates-io))

(define-public crate-aba-cache-0.1 (crate (name "aba-cache") (vers "0.1.0") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("sync" "time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "rt-core"))) (default-features #t) (kind 2)))) (hash "1mq0m0knmgm8xb84zkl334dbw78b6wxfwvwz3p6ahb1ag2bjachf") (features (quote (("default" "asynchronous") ("asynchronous" "tokio"))))))

(define-public crate-aba-cache-0.1 (crate (name "aba-cache") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("sync" "time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "rt-core"))) (default-features #t) (kind 2)))) (hash "01066rc8sa23ahqirhqzjq0i028xbjsxsxl35bx4yhybyrkh9akv") (features (quote (("default" "asynchronous") ("asynchronous" "tokio"))))))

