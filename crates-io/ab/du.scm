(define-module (crates-io ab du) #:use-module (crates-io))

(define-public crate-abduco-0.1 (crate (name "abduco") (vers "0.1.0") (hash "0f0cq0zqw5z9bxmp2804j4x12vckpfpk7md6f76ivm526v034j3d")))

(define-public crate-abdulbasit_crate_2-0.1 (crate (name "abdulbasit_crate_2") (vers "0.1.0") (hash "1b97dc4yaccb6vvlgp1fkrnawl6jy8dkdla5vvd1j6xr1albzs91")))

(define-public crate-abdulbasit_first_crate-0.1 (crate (name "abdulbasit_first_crate") (vers "0.1.0") (hash "0pqp6f27nib1vwq1r0n5rg2xlg63wqhkh2crdvxhgxfak6aa8b5n")))

