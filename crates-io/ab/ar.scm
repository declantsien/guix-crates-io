(define-module (crates-io ab ar) #:use-module (crates-io))

(define-public crate-abar-0.2 (crate (name "abar") (vers "0.2.1") (hash "1fwmwfmscs1r0piakg7pc6b9l8g48c0is5ka2x255izl1hx5gw64")))

(define-public crate-abar-0.2 (crate (name "abar") (vers "0.2.2") (hash "1vb6vwymxybzn33lg1aszdf8dn1ird4p4vjkn9bi8l096ia0788d")))

(define-public crate-abar-0.3 (crate (name "abar") (vers "0.3.1") (hash "06166flcnfg9vjrp67rflxx8fk9rz9m1lp5hk857i67c07grxcw8")))

(define-public crate-abar-0.4 (crate (name "abar") (vers "0.4.0") (deps (list (crate-dep (name "spmc") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qvnvjhzcw6lbpanmdgcjnsivir8bmmzxvxn0g7283hlsv3v5ypb")))

(define-public crate-abar-0.5 (crate (name "abar") (vers "0.5.0") (deps (list (crate-dep (name "spmc") (req "^0.3") (default-features #t) (kind 0)))) (hash "19l1i8i0536hbqix8mjj64lyp1h6icsxqswg4jin7bxsk9afh1ya")))

(define-public crate-abar-0.6 (crate (name "abar") (vers "0.6.0") (deps (list (crate-dep (name "flume") (req "^0.10.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0a5gv942vsai5rxhq11xbx5c053l2gzq1s2whn6wzqqv38piwyrd")))

(define-public crate-abar-0.6 (crate (name "abar") (vers "0.6.1") (deps (list (crate-dep (name "flume") (req "^0.10.9") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1k0vd7jnxywc6900cg45mlfjcrrag41a50xcnb37gg547mbrp1xh")))

(define-public crate-abar-0.6 (crate (name "abar") (vers "0.6.2") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "1znv2yn6nr86vdshk6s0gcrdw5j7qwsdqqykf7yazy6r53izqm12")))

