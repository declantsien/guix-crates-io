(define-module (crates-io ab pf) #:use-module (crates-io))

(define-public crate-abpfiff-0.1 (crate (name "abpfiff") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.7") (kind 0)) (crate-dep (name "libc") (req "^0.2.108") (kind 0)))) (hash "04n5i6ngfkdlwf9pjc1zzpq53s6vg51r5qpdzcbqd852j5393cn2") (rust-version "1.65.0")))

(define-public crate-abpfiff-0.1 (crate (name "abpfiff") (vers "0.1.1-alpha.with.debug") (deps (list (crate-dep (name "bytemuck") (req "^1.7") (kind 0)) (crate-dep (name "libc") (req "^0.2.108") (kind 0)))) (hash "1dz4bmsj65c6msvvanra337bl3ak5c4f0khd4pbalyr2r6bmg57a") (rust-version "1.65.0")))

