(define-module (crates-io ab an) #:use-module (crates-io))

(define-public crate-abandonment-issues-0.1 (crate (name "abandonment-issues") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "1s9dml899xkm4i2x4ry2103k9837bfha8xw0r66fi8fgbxim8zyj")))

