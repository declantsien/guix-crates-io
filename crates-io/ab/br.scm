(define-module (crates-io ab br) #:use-module (crates-io))

(define-public crate-abbrev-0.1 (crate (name "abbrev") (vers "0.1.0") (hash "1v8xbckahf94rd0p58cp5181rx3gjbhvv0pbkhmzhj4hb8lgzwn6")))

(define-public crate-abbrev-0.2 (crate (name "abbrev") (vers "0.2.0") (hash "0d3jjqbpx3g7rdnyl58k4hlb8xv8lazafgh3lq4v4kbm4vg2bqnx")))

(define-public crate-abbrev-0.2 (crate (name "abbrev") (vers "0.2.1") (hash "133dd1lxndpm035m5rfj6177my8f78r1zqs32md06gk122vs2xdc")))

(define-public crate-abbrev-0.2 (crate (name "abbrev") (vers "0.2.2") (hash "1w6lb01mbj3bpzpffq0alanx16fsh9gd27hakz0540s12zkl1yvq")))

(define-public crate-abbrev-num-0.1 (crate (name "abbrev-num") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.35.0") (kind 0)))) (hash "1h700n442jkrdq6zidk7j3jv9qk90p3zdc6iwwc9kbgyllqb50lc")))

(define-public crate-abbrev-tree-0.1 (crate (name "abbrev-tree") (vers "0.1.0") (hash "1xbv0zq614x7y1wdhqhcn7mqw4r24d8698jhd9cfk0nlispx42sz")))

(define-public crate-abbrev-tree-0.1 (crate (name "abbrev-tree") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qjcmab3k0sh2mihqn0y425npd25n6kvq0l83dsgawfwnhcd32i1")))

(define-public crate-abbreviate-3 (crate (name "abbreviate") (vers "3.1.1") (hash "0fr3as2ghmqvds4bjinalacl02y1kf1v461xxhc5hlnx3ff5am6x")))

(define-public crate-abbreviation-0.2 (crate (name "abbreviation") (vers "0.2.1") (hash "0w5qv2j26bwg1cqlnvhl42ppl7n9pw9yyym5b7jixk78kv2id3dg")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.0") (hash "04k4vcw5628kdlrw555dwmxfkfp1hzxn0fcc09awxchjkii92wjz")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.1") (hash "11cww2m7hjvsf5sbnrmlqkfxxfgf8nh8hb87ip80h33464w392mk")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.2") (hash "13wd1lv9mvrjkv4138zq9aw85lvvjsxmk53fvy76azb3izr6khnj")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.3") (hash "1dj3y5xdrgrhalxmjyg8dw3np3f2zkfnbm6346hp1dyska37zhqk")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.4") (hash "14i3gshql2mzkxmqb71440l1f8ni4qqpnfpzddhs4lci4mw5anmp")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.5") (hash "00ihwbwr6p16wia3q65zds098bz89n4bnk0nr4ir6gp0ay84cqhh")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.6") (hash "0v89dpbp6bch2kayk5gij64d8rnf8zlgq76slyp36l6wkfn17fcw")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.7") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "12n78jr9m76wfc5vsyvbsnmiclpc38wnw408mhd42x9ddh62fbd8")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.8") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "118bnkp261jg2zrpfwk21bp3qby7w1mbaij70l9acxrf8y6r0gxd")))

(define-public crate-abbreviator-0.1 (crate (name "abbreviator") (vers "0.1.9") (deps (list (crate-dep (name "unicode-segmentation") (req "^1") (default-features #t) (kind 0)))) (hash "0807d92n9sfrzqd3ian043ryk2kc5bc0ivxapfp4qs8c7wjdq9jj")))

