(define-module (crates-io ab sp) #:use-module (crates-io))

(define-public crate-abspath-1 (crate (name "abspath") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "mhlog") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1kwwy1alb3khsab3mkb5nw5dpac50lgxlzwzlxsb2g188zp1qkh7")))

(define-public crate-absperf-minilzo-0.1 (crate (name "absperf-minilzo") (vers "0.1.0") (deps (list (crate-dep (name "absperf-minilzo-sys") (req "^2.10") (default-features #t) (kind 0)))) (hash "00i734ljdf2r0kmwfd1k8c85v6bbhgks7s8q2l7q0jd97z9g6mhm")))

(define-public crate-absperf-minilzo-0.3 (crate (name "absperf-minilzo") (vers "0.3.0") (deps (list (crate-dep (name "absperf-minilzo-sys") (req "^2.10") (default-features #t) (kind 0)))) (hash "1d9l4icwvz2c5zimhs8sx7gb919yfnyylq9w2b2lnw07jzhicrb3")))

(define-public crate-absperf-minilzo-0.3 (crate (name "absperf-minilzo") (vers "0.3.1") (deps (list (crate-dep (name "absperf-minilzo-sys") (req "^2.10") (default-features #t) (kind 0)))) (hash "0d2adnvh4cb59bma9884awiyy1zbplxz9529kbbda8y1vdqp7vps")))

(define-public crate-absperf-minilzo-0.3 (crate (name "absperf-minilzo") (vers "0.3.2") (deps (list (crate-dep (name "absperf-minilzo-sys") (req "^2.10") (default-features #t) (kind 0)))) (hash "0xcb5b3hcyxzqn9cnh7447h5rb03icbdhz0s991byl12lvi84daf")))

(define-public crate-absperf-minilzo-0.3 (crate (name "absperf-minilzo") (vers "0.3.3") (deps (list (crate-dep (name "absperf-minilzo-sys") (req "^2.10") (default-features #t) (kind 0)))) (hash "0sv45gk5khxp0lvmc78fanj32hwjvsxfd2r0xzsfhzdwqyv5xbfc")))

(define-public crate-absperf-minilzo-0.3 (crate (name "absperf-minilzo") (vers "0.3.4") (deps (list (crate-dep (name "absperf-minilzo-sys") (req "^2.10") (default-features #t) (kind 0)))) (hash "1xijpnysvbfnfpd9jyj6khsrl4rhplxrp517r757jwbm53fi6z7z")))

(define-public crate-absperf-minilzo-sys-0.1 (crate (name "absperf-minilzo-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ry9gab5nma8sgz0n52bfb2z6plqz0n82bq5c24c46d9m8lyil5m")))

(define-public crate-absperf-minilzo-sys-2 (crate (name "absperf-minilzo-sys") (vers "2.10.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1bnsc1ksiszv3kr3ahm90vzjv4irj5qpqddpzbp92k2in4xppn2s")))

