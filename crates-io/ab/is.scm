(define-module (crates-io ab is) #:use-module (crates-io))

(define-public crate-abistr-0.1 (crate (name "abistr") (vers "0.1.0") (deps (list (crate-dep (name "abistr-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "02sng8llijvm5wbj169y54kmzs1a1npgifsdl45gcffs0jx80iz1")))

(define-public crate-abistr-0.1 (crate (name "abistr") (vers "0.1.1") (deps (list (crate-dep (name "abistr-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "08qjlvb4qn85vm6l6hw1vrbmp86qn0km2s42b4clj1sm3xkjfp99")))

(define-public crate-abistr-0.2 (crate (name "abistr") (vers "0.2.0-rc1") (deps (list (crate-dep (name "abistr-macros") (req "^0.2.0-rc1") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "widestring-0-4") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "widestring")))) (hash "1baw3dc7aqlpq7bngz4xcjz4xnr511iwvrqzbfqailm7sp7czw5z")))

(define-public crate-abistr-0.2 (crate (name "abistr") (vers "0.2.0-rc2") (deps (list (crate-dep (name "abistr-macros") (req "^0.2.0-rc2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1") (optional #t) (default-features #t) (kind 0) (package "widestring")))) (hash "0kgxq6yi1dp0nf59s911fqa77asl535agax9700vb59480b2fr2q") (features (quote (("std") ("default" "std"))))))

(define-public crate-abistr-0.2 (crate (name "abistr") (vers "0.2.0-rc3") (deps (list (crate-dep (name "abistr-macros") (req "^0.2.0-rc3") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1") (optional #t) (default-features #t) (kind 0) (package "widestring")))) (hash "0llza5cd7cacnb5p8c3pz2nk6w3zz6flp5xcyj14nmnj3aa77pp7") (features (quote (("std") ("default" "std"))))))

(define-public crate-abistr-macros-0.1 (crate (name "abistr-macros") (vers "0.1.0") (hash "0m1y59kpsxqwmdxw00bjcy6hyagci5lry30j44vplin19mclkd3q")))

(define-public crate-abistr-macros-0.2 (crate (name "abistr-macros") (vers "0.2.0-rc1") (hash "181p9ml51md1wqqwmgrs5ng76wl1x1w42knijfivxi6sd60g1c4g")))

(define-public crate-abistr-macros-0.2 (crate (name "abistr-macros") (vers "0.2.0-rc2") (hash "14fvfcq5mgfj1nvwx11gyqs7snwvj7mm83s67654m90gpa40vwhk")))

(define-public crate-abistr-macros-0.2 (crate (name "abistr-macros") (vers "0.2.0-rc3") (hash "05cfvzhh7ds7gd196b77bqxjvp2fv227kmflifr5alls6qprbswa")))

