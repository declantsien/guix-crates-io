(define-module (crates-io ab c-) #:use-module (crates-io))

(define-public crate-ABC-ECS-0.1 (crate (name "ABC-ECS") (vers "0.1.0") (deps (list (crate-dep (name "anymap") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1w5ifpydr1ll4w1fnmvx2knhpalxk530i2p36ibh1w0kr0pvjafv") (yanked #t)))

(define-public crate-ABC-ECS-0.1 (crate (name "ABC-ECS") (vers "0.1.1") (deps (list (crate-dep (name "anymap") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1f7ilxqd5hgvk2cmvlyjz6iqxzyygcc9g1h013bri6imhspb2p7y") (yanked #t)))

(define-public crate-ABC-ECS-0.1 (crate (name "ABC-ECS") (vers "0.1.2") (deps (list (crate-dep (name "anymap") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1kwba0wgafl1kf1mmy3fp5v1x93wich9y2vydl6isxfxkvq3wyg0") (yanked #t)))

(define-public crate-ABC-ECS-0.1 (crate (name "ABC-ECS") (vers "0.1.3") (deps (list (crate-dep (name "anymap") (req "^1.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0flx80vq528rnnhxly3wr4k7gqq9fkd6f1hm2wsjinkp0vhbry30")))

(define-public crate-ABC-ECS-0.1 (crate (name "ABC-ECS") (vers "0.1.4") (deps (list (crate-dep (name "anymap") (req "^1.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "nohash-hasher") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "04dr2xfkldvxzhwd0pqnb84f7v44k6mrr1mdrmwv8dygdb2yfkb3")))

(define-public crate-ABC-ECS-0.1 (crate (name "ABC-ECS") (vers "0.1.5") (deps (list (crate-dep (name "anymap") (req "^1.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "nohash-hasher") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "17nz15dyrrg4cfdfdz10hf8nbpkdslx798pbnxlmmjr7w7il022h")))

(define-public crate-ABC-ECS-0.2 (crate (name "ABC-ECS") (vers "0.2.0") (deps (list (crate-dep (name "anymap") (req "^1.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "nohash-hasher") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1jrz2asc7f8k3fdcjm8iaq1fh1svxkahlxm1yk1wcb3c4wxhfrgb")))

(define-public crate-abc-parser-0.1 (crate (name "abc-parser") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "0fz42scby7bkgxb67zg7qx1da8ai33gmyl7844zsks5dypwaka2d") (yanked #t)))

(define-public crate-abc-parser-0.1 (crate (name "abc-parser") (vers "0.1.1") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "1vsj7dindjgdgip937plgf5cihsdvhwjd8j6r887jv14nai4w6n0") (yanked #t)))

(define-public crate-abc-parser-0.1 (crate (name "abc-parser") (vers "0.1.2") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "1d28n9wkmqfkwixqvhd7i181qkapm95742jv5ygvxrccyfiq8xx8")))

(define-public crate-abc-parser-0.1 (crate (name "abc-parser") (vers "0.1.3") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "00w0977kdg2yjl6mhnza50hxdxg4aqrd4rhahwp75a0mh2wzr4pi")))

(define-public crate-abc-parser-0.1 (crate (name "abc-parser") (vers "0.1.4") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "07dfb0hadhsbfq89xaz2ph42igs09hvrj36wiz6rsddciaqm3a1c")))

(define-public crate-abc-parser-0.1 (crate (name "abc-parser") (vers "0.1.5") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "1iv3k39jgim2dpyhkdh19db186pkh4kczqbbphapq44b70g7qcdf")))

(define-public crate-abc-parser-0.2 (crate (name "abc-parser") (vers "0.2.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "10xwkvzgxwwjnczlb7k15l3cpl8jng4pfky693dx1gl20fiikmr1")))

(define-public crate-abc-parser-0.3 (crate (name "abc-parser") (vers "0.3.0") (deps (list (crate-dep (name "peg") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0h2gcncb8xj5jjc46igh79gppcgjlsz6rs5ln4wgwpgcr4ih9rk5")))

