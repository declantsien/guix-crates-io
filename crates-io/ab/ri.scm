(define-module (crates-io ab ri) #:use-module (crates-io))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "more-asserts") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0dbfqnqf929d7lm5iv9sciyyzxi2drlfb8w43qr3hv6i5zmp93j1") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0qxisj4qiwv42l62v0605n2b991jc5ry255q32papdnmniqrh3i0") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0yci12spw95rb9q3cj92vg5g0h604x9dg5wjb5rl04b5xvl6rfhm") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1zcfkl5kd3icgp40yyc5zqkn7jwclbck09j93j2myq39f9z5xf4a") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0mzbrbirhk42k8lzwp1wmcs56lvf3i5cx96npvhyqrzhlzaf4261") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "06ych4xf31lm7bk0b9yik8qhb8bppnv4w35d5azifj3sf7xdrymx") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1kr949as5zz722ysh5lvflhvfl02m6zgpmlrkwsr37sd0zg5wicn") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "15lix2pj9x1k5p00scv4d9g6i7ai817dxsyiz6nw8l5a07rllq00") (yanked #t)))

(define-public crate-abridge-1 (crate (name "abridge") (vers "1.1.6") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1wfp6rm0v19wfpw565fvw0j61pbjkrzz4vqf4ar2bmwbz5q5b8gw")))

(define-public crate-abridgment-0.0.1 (crate (name "abridgment") (vers "0.0.1") (hash "1rf3zabnm60j48qf632hmfqf3phha574hqwf5af6dgwhmx5anqfc")))

