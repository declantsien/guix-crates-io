(define-module (crates-io ab sa) #:use-module (crates-io))

(define-public crate-absal-0.1 (crate (name "absal") (vers "0.1.0") (hash "0mx0lbdbdabpwhrsc68a38abnymvr76cv143wpi5l2a02sb4vilr")))

(define-public crate-absal-0.1 (crate (name "absal") (vers "0.1.1") (hash "0f21lpx0irs9lx2ry2y1laxrxfsw5gcqf95pwh6a9sdalsb8f7lq")))

(define-public crate-absal-0.1 (crate (name "absal") (vers "0.1.2") (hash "0c0h3bls64yi317pijrq8x5b9ip1fn29xailv7vr4pjn3hxms9gw")))

(define-public crate-absal-0.1 (crate (name "absal") (vers "0.1.3") (hash "0x9q9cd1idmbynda558i2hc5hgliy50nnfgzdxn3b55jza4m7iqn")))

(define-public crate-absal-0.1 (crate (name "absal") (vers "0.1.4") (hash "0rq99p8dmblq22h41jq4qd1z6k0ssahzqv63bcp84ljcbga1g36s")))

