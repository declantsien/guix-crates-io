(define-module (crates-io ab nf) #:use-module (crates-io))

(define-public crate-abnf-0.1 (crate (name "abnf") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "022jqlx9m1j975wfmbbb4cazaj4kcbbvrmrvafl8sd2ijdxdcm41")))

(define-public crate-abnf-0.1 (crate (name "abnf") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "1dkp4k8ivkvpsllpp42h5f9wzgfzv0zra82nnbikk2wgqp72rm9w")))

(define-public crate-abnf-0.1 (crate (name "abnf") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "1nw2y5gvi5fw6diiskssayanr5k3sxmm8m184wxnkm6jz80swwnr")))

(define-public crate-abnf-0.2 (crate (name "abnf") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "1v3bgkv6k3qvl18a8zqwbnhjkrhimshjwy00zqgm6aky6dkiahzr")))

(define-public crate-abnf-0.3 (crate (name "abnf") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "1734h4p44mfxiwihnmsxsv6v01q1m0ygfnx1pl6brbg0vd5qia6q")))

(define-public crate-abnf-0.4 (crate (name "abnf") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1d8wgr42c4zfrmnamh1zz51pza0m6hi1i3hj834k0xjgr1qgm2jz")))

(define-public crate-abnf-0.4 (crate (name "abnf") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0l6rchqvagr7bh2kvr1qdy22dbaj6xj1c9jkzgxyh5x89nm6fvin")))

(define-public crate-abnf-0.5 (crate (name "abnf") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0if7vp2z453ln85s3jvza2lq53v1j96w3y9k5y6hryi0dmg6b1b7")))

(define-public crate-abnf-0.6 (crate (name "abnf") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "055chbk42cgch85p4whvf236nr9595vqxk2l75cqlhzryaf02rg0")))

(define-public crate-abnf-0.6 (crate (name "abnf") (vers "0.6.1") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0qjlh11h3j8mgcifwxd004d1df7ahwmcl14fyag6607prvxvkzj7")))

(define-public crate-abnf-0.7 (crate (name "abnf") (vers "0.7.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0a10zchv11c0i5q15s4dpcw11sbs4y3vy78y4h606gflnip6p7dz")))

(define-public crate-abnf-0.8 (crate (name "abnf") (vers "0.8.0") (deps (list (crate-dep (name "abnf-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "13sjy1g0i61a9lckmpvylpgg5q0r3xd8wyxk3fwwlbhx9q962xlh")))

(define-public crate-abnf-0.9 (crate (name "abnf") (vers "0.9.0") (deps (list (crate-dep (name "abnf-core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1d9hrdgggg83z9qmr5b708qbx992plrrf2k63dqdavr3s9izkmbn")))

(define-public crate-abnf-0.10 (crate (name "abnf") (vers "0.10.0") (deps (list (crate-dep (name "abnf-core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0saz7cx667l0822r7lvl2rcg7mfav14ixszrzz7kkciinkcsn2q2")))

(define-public crate-abnf-0.10 (crate (name "abnf") (vers "0.10.1") (deps (list (crate-dep (name "abnf-core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1drbrg15ps64v6qjriy65b0x826nv0cn26kxpm961d7wmiwc68zb")))

(define-public crate-abnf-0.10 (crate (name "abnf") (vers "0.10.2") (deps (list (crate-dep (name "abnf-core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1k7sc7lyvqwgzjrr9lyb8nnp4frl942rpqbn0gapli23vgkn727x")))

(define-public crate-abnf-0.11 (crate (name "abnf") (vers "0.11.3") (deps (list (crate-dep (name "abnf-core") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "148prid9r3w3pbyb3f5ifff71d22shyn0bbm3s9qf3aj1hr6krln")))

(define-public crate-abnf-0.12 (crate (name "abnf") (vers "0.12.0") (deps (list (crate-dep (name "abnf-core") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0-alpha1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0swyvqkzbnqv5hwgpjq6mm3jij3agk5gzs65vqzy91id8sm1nx1k")))

(define-public crate-abnf-0.13 (crate (name "abnf") (vers "0.13.0") (deps (list (crate-dep (name "abnf-core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 2)))) (hash "0fh314g36l1n2yszms7w6barklf7fq25vv8fhljcxbfra2yi6w88")))

(define-public crate-abnf-core-0.1 (crate (name "abnf-core") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "1fj1lf959xrcfmsls1ybgjf0jpmmp6sknjqz8223z0nmmw88j3xa")))

(define-public crate-abnf-core-0.2 (crate (name "abnf-core") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "0wv2215f8lxrjwh9qgy76mjbpsyg8l4q1v2ca7mkzb85vd7qjz0k")))

(define-public crate-abnf-core-0.3 (crate (name "abnf-core") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "167swynvkl3d1zwjwhc3lnnppcmr4qvlkpk1xpy75jc215fz3g34")))

(define-public crate-abnf-core-0.4 (crate (name "abnf-core") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)))) (hash "1d6xwf1cllh6wgdy7y0bshvlygkacxcbq1j4yl0l570rnx69855m")))

(define-public crate-abnf-core-0.4 (crate (name "abnf-core") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)))) (hash "1v1fmz2s4adn5igv1yc6rnbc7rg3fza5fv96wqayr11fsm75f6l7")))

(define-public crate-abnf-core-0.5 (crate (name "abnf-core") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^7.0.0-alpha1") (default-features #t) (kind 0)))) (hash "0zfxh7kfs54w5279w6vyrwk7q26hf9j5cfm0j7xnihz17b20jkn4")))

(define-public crate-abnf-core-0.6 (crate (name "abnf-core") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1shdbi2ffzvyf6v3wwlgwp5sa58m22pqk716b6gnm40v0wgjs67c")))

(define-public crate-abnf-parser-0.1 (crate (name "abnf-parser") (vers "0.1.0") (deps (list (crate-dep (name "quick-error") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0xi7g0r2f9wxswfa1yz2xjam2mnil5v0x0dz2l2whcngpkmi7j1z")))

(define-public crate-abnf-parser-0.1 (crate (name "abnf-parser") (vers "0.1.1") (deps (list (crate-dep (name "quick-error") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0q65nc2w20zh8zi5r695ihczjzs06r18fx9wc9s4yw3p9qj1sn6f")))

(define-public crate-abnf_to_pest-0.1 (crate (name "abnf_to_pest") (vers "0.1.0") (deps (list (crate-dep (name "abnf") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "14sif549hpiddsbcm6xrnywqsgcig6ckkkzng5hp06j1zixg4f3g")))

(define-public crate-abnf_to_pest-0.1 (crate (name "abnf_to_pest") (vers "0.1.1") (deps (list (crate-dep (name "abnf") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0cy4110dkmy1fkslz236wglz8a4p6fa0kx9jka9w4ik8hncmin3a")))

(define-public crate-abnf_to_pest-0.1 (crate (name "abnf_to_pest") (vers "0.1.2") (deps (list (crate-dep (name "abnf") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "17clqr4rsk600q7xhk67nizxahj3awza6jkpiyz7902kk5gjh3ph")))

(define-public crate-abnf_to_pest-0.2 (crate (name "abnf_to_pest") (vers "0.2.0") (deps (list (crate-dep (name "abnf") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0yvgmjh6f3z4g4fki7zhzg6w9d3xyyszxb4sx19sinbmpfnyym1i")))

(define-public crate-abnf_to_pest-0.5 (crate (name "abnf_to_pest") (vers "0.5.0") (deps (list (crate-dep (name "abnf") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0bmypcs1cjpkpb1vrv7p2j38q1r0q6rcsfsidf0xh8m4sfjslarp")))

(define-public crate-abnf_to_pest-0.5 (crate (name "abnf_to_pest") (vers "0.5.1") (deps (list (crate-dep (name "abnf") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "0cg37yi8k083slnlzrifflq7c44w9k9bj4jk7959d9yrdmk5k7ck")))

