(define-module (crates-io ab ra) #:use-module (crates-io))

(define-public crate-abra-0.0.1 (crate (name "abra") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "roaring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1xp4xr8bf96v0dkqr7bb5hhrjg53rxygg8psb6jkxp0rws1zf7y4")))

(define-public crate-abramowitz-stegun-0.0.0 (crate (name "abramowitz-stegun") (vers "0.0.0") (hash "1y9zvri2ibh2z5albsg98zq5dqc672fsb2zzysw3s56r9y5j1fky")))

(define-public crate-abraxas-0.0.0 (crate (name "abraxas") (vers "0.0.0") (hash "0g9r0arvb09xhjgrsgk2vh6ql5m6i488i2zhcx5id850bxzbsmhr")))

(define-public crate-abraxis-0.0.1 (crate (name "abraxis") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "q_compress") (req "^0.11.7") (default-features #t) (kind 0)))) (hash "0q12w4kkxyrhhp9nx3iiacmfgz94p9aww11v6xxgg9l4db0mqln4")))

