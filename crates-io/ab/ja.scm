(define-module (crates-io ab ja) #:use-module (crates-io))

(define-public crate-abjad-0.1 (crate (name "abjad") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0767wvmjqa4qmq4fy7nm1r4nz13yy03m06i7kzyg3irxxwr2q7c8")))

(define-public crate-abjad-0.2 (crate (name "abjad") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1cwg99cdjwmar2kk382w891v58j6zz3gwcyly4brf2krk95jmc54")))

(define-public crate-abjad-0.3 (crate (name "abjad") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "071wpkl7mpc1nmgybwblc7m4c5h9rkyaqbv3qxmnz9h419yqiyj0")))

(define-public crate-abjad-0.3 (crate (name "abjad") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0iq72azsjxqqr4ldf24ppb3ilsrzcxavanhm1885n14appj74m7r")))

(define-public crate-abjad-0.3 (crate (name "abjad") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0k1rgrc0d6z0zckapahgrd5xr9f06zxx111pqxay5jj7qmrf4vgx")))

(define-public crate-abjad-0.4 (crate (name "abjad") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1wpl86mpzmf30w8ambgcp75ppfscrljgdz6maf4dy7f9j1zfag3q")))

(define-public crate-abjad-0.5 (crate (name "abjad") (vers "0.5.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0wjr7g72d322dnmpccbnrnp5barnpaqjsh118gvmpkfq5y2qmvwr")))

