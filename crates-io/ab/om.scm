(define-module (crates-io ab om) #:use-module (crates-io))

(define-public crate-abomonation-0.1 (crate (name "abomonation") (vers "0.1.0") (hash "16afpsa28m2kbcgcjmvrwq8yyfgdqrza9df374jnl2627xydq1dz")))

(define-public crate-abomonation-0.2 (crate (name "abomonation") (vers "0.2.0") (hash "13yij06rrda3ca4dpppiis9yywwpcydjdbpsz6wjwrckp39a2a1y")))

(define-public crate-abomonation-0.2 (crate (name "abomonation") (vers "0.2.1") (hash "1k54s9458c2w7h42xjp459lm9znqpi6zv85vwqzklzi2b7vnkv3q")))

(define-public crate-abomonation-0.2 (crate (name "abomonation") (vers "0.2.2") (hash "03qgjdwk2661aq54vpcg79sqf11dgf2jhqj5z62gf7g19l3xyjjv")))

(define-public crate-abomonation-0.2 (crate (name "abomonation") (vers "0.2.3") (hash "1ldwgr4i3pv0h53ic8z7mw7nvsggk4kjsfhsvmvjknhqfw0xj3wg")))

(define-public crate-abomonation-0.3 (crate (name "abomonation") (vers "0.3.0") (hash "07qn7w6qvsr86w6xcx47lv2li5pjrpsvsai984wslmz9csi3738p")))

(define-public crate-abomonation-0.3 (crate (name "abomonation") (vers "0.3.1") (hash "0afqaqisd3m4z4s4lz867njfbx687y0v8czwjbbgzjkih2pdfdrs")))

(define-public crate-abomonation-0.3 (crate (name "abomonation") (vers "0.3.2") (hash "161g4cbw82qc1nkah7hi4lakfpnpxfffk7427gkd5gywh8873rwb")))

(define-public crate-abomonation-0.3 (crate (name "abomonation") (vers "0.3.3") (hash "0n2yja7673zlmx128jsz7p3wjsd8sd0iy02jchib5ya8shnvy3y5")))

(define-public crate-abomonation-0.4 (crate (name "abomonation") (vers "0.4.0") (deps (list (crate-dep (name "recycler") (req "*") (default-features #t) (kind 2)))) (hash "18ywl5kma0yd2b15b7497fqznhr3z9vi76jyn2vy63mr7d9dv3xk")))

(define-public crate-abomonation-0.4 (crate (name "abomonation") (vers "0.4.1") (deps (list (crate-dep (name "recycler") (req "*") (default-features #t) (kind 2)))) (hash "1caxkl7r9j0xqn1bvrdfpcjal5w5dk3zi340nkhbgy1s607w8f90")))

(define-public crate-abomonation-0.4 (crate (name "abomonation") (vers "0.4.2") (deps (list (crate-dep (name "recycler") (req "*") (default-features #t) (kind 2)))) (hash "0jm7fgzjdfz3ghh7y1jqbx5q1aisazpwb0vnkbnkqgy5jhh9xfb4")))

(define-public crate-abomonation-0.4 (crate (name "abomonation") (vers "0.4.3") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "1hrlzx9jcrn05ln20ivy1xdwjc90mp2mdabvf0ghi75l6b0y4ajv")))

(define-public crate-abomonation-0.4 (crate (name "abomonation") (vers "0.4.4") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "0z5z6s4p5ll68pf46lym91vmq245fsdbbdf11aplq4kwr23chxib")))

(define-public crate-abomonation-0.4 (crate (name "abomonation") (vers "0.4.5") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "02g405la85ixyd91qb3qzrf946hcla81pllkvkvn1idqjwxn2cb3")))

(define-public crate-abomonation-0.4 (crate (name "abomonation") (vers "0.4.6") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "0mgghcpwmzpx47wmm5c5yvliazpjm60cmqz77ng53ds273r5yxi6")))

(define-public crate-abomonation-0.5 (crate (name "abomonation") (vers "0.5.0") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "19h4s5ai8pbaap7n8pcd6yinqp22hx29ls9d2gdwsjka3m9xy6gv")))

(define-public crate-abomonation-0.7 (crate (name "abomonation") (vers "0.7.0") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "00c6mmwb6jlmw5his875li988rrz9vr2x08mhn9lg1rx498jidzn")))

(define-public crate-abomonation-0.7 (crate (name "abomonation") (vers "0.7.1") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "0577h48ji1dn31xmrz1psg86h39hr1ib03az3pnsps43gvlw4crr")))

(define-public crate-abomonation-0.7 (crate (name "abomonation") (vers "0.7.2") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "09v1byr09icxw8aiccs3izmd1pgijrmv56jnky48i9c7lqh67fvk")))

(define-public crate-abomonation-0.7 (crate (name "abomonation") (vers "0.7.3") (deps (list (crate-dep (name "recycler") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "1cjg3hjf028n447pdj7zcdgrkngx30as8ndxlxx947wvr49jkrsn")))

(define-public crate-abomonation_derive-0.1 (crate (name "abomonation_derive") (vers "0.1.0") (deps (list (crate-dep (name "abomonation") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mqsawpxw8rb42s5fpcyvl98c4fmq8qz4ndfwxlxbzv5bcgq4ql5")))

(define-public crate-abomonation_derive-0.1 (crate (name "abomonation_derive") (vers "0.1.2") (deps (list (crate-dep (name "abomonation") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.4") (default-features #t) (kind 0)))) (hash "08lmb5s5ska7fwgnqfwkvyw16lz8as756404fhp75zbqd5wydz83")))

(define-public crate-abomonation_derive-0.2 (crate (name "abomonation_derive") (vers "0.2.0") (deps (list (crate-dep (name "abomonation") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.5") (default-features #t) (kind 0)))) (hash "0csffn1mqhjm4242fqf4midwzg1c3cs4r6sgsniw4ss4p000x4ph")))

(define-public crate-abomonation_derive-0.2 (crate (name "abomonation_derive") (vers "0.2.1") (deps (list (crate-dep (name "abomonation") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.5") (default-features #t) (kind 0)))) (hash "1s424l30gkpgk49yqzj0wha9d15vibv77yyafrm65zy7g1m0gbpq")))

(define-public crate-abomonation_derive-0.2 (crate (name "abomonation_derive") (vers "0.2.2") (deps (list (crate-dep (name "abomonation") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.5") (default-features #t) (kind 0)))) (hash "0w5mx3vfg1h2sfl848f262afyrkq048692mq0gnfklhzpg6xf0m0")))

(define-public crate-abomonation_derive-0.2 (crate (name "abomonation_derive") (vers "0.2.3") (deps (list (crate-dep (name "abomonation") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.6") (default-features #t) (kind 0)))) (hash "1lr7yqzxfk39w1qcqn9dxjl646c0sxavv9k9zqw7y27vzpi9vmhl")))

(define-public crate-abomonation_derive-0.3 (crate (name "abomonation_derive") (vers "0.3.0") (deps (list (crate-dep (name "abomonation") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.6") (default-features #t) (kind 0)))) (hash "0g373spwjw311904p45cijv8v372hd96pi9rz27cv1qbvf4iifz0")))

(define-public crate-abomonation_derive-0.4 (crate (name "abomonation_derive") (vers "0.4.0") (deps (list (crate-dep (name "abomonation") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.10") (default-features #t) (kind 0)))) (hash "1xs900c4dagc8rxgnx192433i0j6i5c21wgfgjmbz6961y1fyiq8")))

(define-public crate-abomonation_derive-0.5 (crate (name "abomonation_derive") (vers "0.5.0") (deps (list (crate-dep (name "abomonation") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "1yhydqgq4dipjv3v1qbbcip5jb57zm9p2yyrc968cspmd822l3p5")))

(define-public crate-abomonation_derive_ng-0.1 (crate (name "abomonation_derive_ng") (vers "0.1.0") (deps (list (crate-dep (name "abomonation") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.17") (default-features #t) (kind 0)))) (hash "1w6902lmrm2c0629c8ysb2idpz13dv50pbz9i0f42409dq9mch63") (rust-version "1.63")))

