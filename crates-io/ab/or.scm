(define-module (crates-io ab or) #:use-module (crates-io))

(define-public crate-abort-0.1 (crate (name "abort") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1hfwdlam3jw9yvjkjh13mzwdxgnih8ksjjblnwalj2525z30ca83") (yanked #t)))

(define-public crate-abort-0.1 (crate (name "abort") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0gh91h3ab950m695s8pqqbj1nqxw05xmnnqzsaif0vs34lgryv08")))

(define-public crate-abort-0.1 (crate (name "abort") (vers "0.1.2") (hash "1z1hm9bdlc7ar79aga8jlxsjbhbr5j3w9093grlqvir11vk0rhgz")))

(define-public crate-abort-0.1 (crate (name "abort") (vers "0.1.3") (hash "1fkp2asvn6yb35f82cybnxi38y5ig223yaflijm58hhhs7vy3pdd") (features (quote (("nightly"))))))

(define-public crate-abort-if-0.1 (crate (name "abort-if") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("visit-mut" "full" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0rmq3r1jsb78fvq0yjm9v265j2qby54masc64g5jmgwgvw4jwk8x") (features (quote (("keep_going" "custom_abort") ("default_abort") ("default" "default_abort") ("custom_abort"))))))

(define-public crate-abort-if-0.1 (crate (name "abort-if") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("visit-mut" "full" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "05dzdmw7pp71f9k6ch0v3gmiq3k67jl55zljzlmpwcc0gw9w15jg") (features (quote (("keep_going" "custom_abort") ("default_abort") ("default" "default_abort") ("custom_abort"))))))

(define-public crate-abort-if-0.1 (crate (name "abort-if") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("visit-mut" "full" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "11rzpq0hq8vylzfb98ihhnikxw2ldv80llnfw4yhcygn5vg1p6c9") (features (quote (("keep_going" "custom_abort") ("default_abort") ("default" "default_abort") ("custom_abort"))))))

(define-public crate-abort-nostd-0.1 (crate (name "abort-nostd") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)))) (hash "0ybd6x0zjn04vhnnb2jhsx23h829gaz0a0clshsk5ymmj9y9k04q") (features (quote (("std")))) (yanked #t)))

(define-public crate-abort-on-drop-0.1 (crate (name "abort-on-drop") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "0fxvhjqv2szdx85ccgag077z3k5d7yf6l8hf2z9b7q3cj81ipdfp")))

(define-public crate-abort-on-drop-0.2 (crate (name "abort-on-drop") (vers "0.2.0") (deps (list (crate-dep (name "futures-util") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "06hfcyh0y74jf12wl783d2k0i342zflhi9wb8y8qd7mg2j9xp82p")))

(define-public crate-abort-on-drop-0.2 (crate (name "abort-on-drop") (vers "0.2.1") (deps (list (crate-dep (name "futures-util") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "1gfvhj6zascj4b6wgpk2cghmkgvk31ygf2vyvg6n8csc130akcq2")))

(define-public crate-abort-on-drop-0.2 (crate (name "abort-on-drop") (vers "0.2.2") (deps (list (crate-dep (name "futures-util") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "0ahl6b4d6j4lh4pv3r8n611x3r1d1nc6fz8z1i4l3xlsml0dgmjx")))

(define-public crate-abort_on_panic-0.0.1 (crate (name "abort_on_panic") (vers "0.0.1") (hash "1h8gwggc535kksh6jdcs485j3pci330jj9l6ipzgd4byd6fzmvjz")))

(define-public crate-abort_on_panic-0.0.2 (crate (name "abort_on_panic") (vers "0.0.2") (hash "1razqdzhdgsfx63q2ww92bqi7kn0j7pxganj22kl9ci2smcasswy")))

(define-public crate-abort_on_panic-0.0.3 (crate (name "abort_on_panic") (vers "0.0.3") (hash "0s4cxy8kl6rqixvw2p03lla369xsky8q5myj67rallgprla1i939")))

(define-public crate-abort_on_panic-0.0.4 (crate (name "abort_on_panic") (vers "0.0.4") (hash "1wg4ka2ck4xizn1fkdq3ckqlxdqbgn41rn3h0lrsiws4kvxmp76p")))

(define-public crate-abort_on_panic-0.0.5 (crate (name "abort_on_panic") (vers "0.0.5") (hash "0lxbipnlbqgwb3bvhclayyyf5p7w0ds74k4bcbsx4mzn4z1ywpym")))

(define-public crate-abort_on_panic-0.0.6 (crate (name "abort_on_panic") (vers "0.0.6") (hash "0c47d52f1k8kc1dlq8slxags1zwlda93rr2dwmqd1akgr7ldq3m1")))

(define-public crate-abort_on_panic-0.0.7 (crate (name "abort_on_panic") (vers "0.0.7") (hash "0bhkg6byz4cs92kr8gbncv4g5hjmr5lf2rsybrgmjy06yy6iyqir")))

(define-public crate-abort_on_panic-0.0.8 (crate (name "abort_on_panic") (vers "0.0.8") (hash "0dwzq2zzv1dima8dl7bqzy1n86bfpkb0fdq8a9yy4p638bfdkszw")))

(define-public crate-abort_on_panic-1 (crate (name "abort_on_panic") (vers "1.0.0") (hash "06gr9ryabrcg59b4496c6gwlwxy51b84zgpgap4mq2czxkwliacz") (features (quote (("unstable"))))))

(define-public crate-abort_on_panic-2 (crate (name "abort_on_panic") (vers "2.0.0") (hash "05k1dbkab092ini24x5x54i2ifnclikapj47qsx1c95gb2n3fpwm")))

(define-public crate-abortable_parser-0.1 (crate (name "abortable_parser") (vers "0.1.0") (hash "03bw4hsh4ns4w6v5cip3nfnzpm3rcdf5bcki5fkfwphnd49f6a56")))

(define-public crate-abortable_parser-0.2 (crate (name "abortable_parser") (vers "0.2.0") (hash "034hil5ynga4yfp4fkw24crc4dp2zaxb7pbgmb32661lrrb0ha5x")))

(define-public crate-abortable_parser-0.2 (crate (name "abortable_parser") (vers "0.2.1") (hash "01jr1n4z6q9gp5v3z380rgch49fbkx8j3rl6m2ghfjjyicvzbk89")))

(define-public crate-abortable_parser-0.2 (crate (name "abortable_parser") (vers "0.2.2") (hash "04rj1s2rb17dc6gzkxi6caiy9708760dc2abyfvxcrimraxrjpb8")))

(define-public crate-abortable_parser-0.2 (crate (name "abortable_parser") (vers "0.2.3") (hash "1r27wxx3z6jq7lrl1rihw74g9ggv4ikkvn90q56ag30kaq2q5dgi")))

(define-public crate-abortable_parser-0.2 (crate (name "abortable_parser") (vers "0.2.4") (hash "1v6l4xc41hbdsg09q08argvf7j32in9xb02m4m3pi17dmqrbrzn2")))

(define-public crate-abortable_parser-0.2 (crate (name "abortable_parser") (vers "0.2.5") (hash "0dg4zx0yb9xmn33lgcrzq3n19yblk1s1saxc5xkvxcl6v58hallg")))

(define-public crate-abortable_parser-0.2 (crate (name "abortable_parser") (vers "0.2.6") (hash "1lg9wlzs43p8g3vyvdl1pnnj8gngsfr9hj4inwj8x0qx865v7ixy")))

