(define-module (crates-io ab io) #:use-module (crates-io))

(define-public crate-abio-0.3 (crate (name "abio") (vers "0.3.0") (deps (list (crate-dep (name "abio_derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "02hcfv4hs56p6d318cbm1n3p4wgzp4r88y709hb4121jcs394hpw") (features (quote (("std") ("derive" "abio_derive") ("default" "derive"))))))

(define-public crate-abio_derive-0.1 (crate (name "abio_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (features (quote ("nightly" "span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "11hp5xrfc1knc35vzcwm9jy7yblqkgnr4z5qahwlgbcr7nf4agbl")))

