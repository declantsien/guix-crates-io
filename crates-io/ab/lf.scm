(define-module (crates-io ab lf) #:use-module (crates-io))

(define-public crate-ablf-0.2 (crate (name "ablf") (vers "0.2.0") (deps (list (crate-dep (name "binrw") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zune-inflate") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qn8yyrng45nfzr370qipn319q8hk88pzcvsabslcmalj8d7f0cg")))

