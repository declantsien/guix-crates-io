(define-module (crates-io ab c_) #:use-module (crates-io))

(define-public crate-abc_sum-0.1 (crate (name "abc_sum") (vers "0.1.0") (hash "0ss5ndj3wl11mby6b2z1cifli5gbi9yx8fnq6kbv9dhh3m61gmrj")))

(define-public crate-abc_sum-0.1 (crate (name "abc_sum") (vers "0.1.1") (hash "0m3b17wi7ck41fwriszm38vsdnw5d1ssdkwiq9fnxndfga851s1b")))

(define-public crate-abc_sum-0.1 (crate (name "abc_sum") (vers "0.1.2") (hash "0jf51ay0703fnfijhzixscxn1bhpspr7jrxccagghh8kk0g71rpk")))

