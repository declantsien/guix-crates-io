(define-module (crates-io lj mr) #:use-module (crates-io))

(define-public crate-ljmrs-0.0.1 (crate (name "ljmrs") (vers "0.0.1") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "183g7lby5apwk4zahapgrj2qs0ppwq72kjnhqg4dr3v2n6sm45mf")))

(define-public crate-ljmrs-0.0.2 (crate (name "ljmrs") (vers "0.0.2") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0nszqr9yn6s09749bhb8k3h6gs7gwdhnsqp8fimi7pmiy8hykrb6")))

(define-public crate-ljmrs-0.0.3 (crate (name "ljmrs") (vers "0.0.3") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ygzaaw0drgycqnz5rn0q9rr9k8xwqf3b8m86ng4lfjqwm05k4l9")))

(define-public crate-ljmrs-0.0.4 (crate (name "ljmrs") (vers "0.0.4") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0cyl2sjnq6waq4sxrwi0fk2klf9yrnpy4fxk6kvnjwx9kny65prm")))

(define-public crate-ljmrs-0.0.5 (crate (name "ljmrs") (vers "0.0.5") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0x6gzmvyjfff8wfhxazzis3axbiqggwkaq7qjirkhnsq8qgzp1qi")))

(define-public crate-ljmrs-0.0.6 (crate (name "ljmrs") (vers "0.0.6") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0wbxw682x30r2g5vzif8npnacn2hv4fzskmfca2mpq4yviqsiarg")))

(define-public crate-ljmrs-0.0.7 (crate (name "ljmrs") (vers "0.0.7") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "16riid0qwl75agibvy3i39ddyr931pp3bfgimjgnh846337qfrcy")))

(define-public crate-ljmrs-0.0.8 (crate (name "ljmrs") (vers "0.0.8") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0yjv7ipfnl9bajw10q3i5rwky7nbl9nkyqvdj7742s7kqnvb58r8")))

(define-public crate-ljmrs-0.0.9 (crate (name "ljmrs") (vers "0.0.9") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0fr9vw03sp1mb6yl48ai8y476m1kh96qx4yw35kb7c57zb8zvib7")))

(define-public crate-ljmrs-0.0.10 (crate (name "ljmrs") (vers "0.0.10") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ihra2q9s85rs4rywpy4d4xx91djkjzifycfsb57scz3mgi34a18")))

(define-public crate-ljmrs-0.0.11 (crate (name "ljmrs") (vers "0.0.11") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "1w2wadzxzszbw0sggg7nd8fd47js25vqd9br61kcfxcvw2xv0x1l")))

(define-public crate-ljmrs-0.0.12 (crate (name "ljmrs") (vers "0.0.12") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "15k91dw8jq4f113hsw7zj2qgzqd4xf8gygbsk4vqgwjaakcldhyj")))

(define-public crate-ljmrs-0.0.13 (crate (name "ljmrs") (vers "0.0.13") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "1djypip3ssnhszdav8mx7lj4vx5v8rb1yghh8jwx63rp559gyw3a") (yanked #t)))

(define-public crate-ljmrs-0.0.15 (crate (name "ljmrs") (vers "0.0.15") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0cdg517ijhl21j3yi9rkm723v6742ilhllxnsg85g73s76pbg22z")))

(define-public crate-ljmrs-0.0.16 (crate (name "ljmrs") (vers "0.0.16") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "1xjrzm043d0phxkc9adjfgxwbfri23cijiwpd6fg7nz009yp64w7")))

(define-public crate-ljmrs-0.0.17 (crate (name "ljmrs") (vers "0.0.17") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "022fzqwk73djni5ihgjh7rrrxdfp5wzwx91l3jgqh4zr18l94fhd")))

(define-public crate-ljmrs-0.1 (crate (name "ljmrs") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "0m6v4z33sl0pxwi3fx011pwyc1fym5n88ff7a0nvcg6v8iyks4hv")))

(define-public crate-ljmrs-0.1 (crate (name "ljmrs") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "00z7ybrbzh4lc3cpk8n366h8wjmi1dqb0n93bkqbi0q2wrgwv62x")))

