(define-module (crates-io lj m-) #:use-module (crates-io))

(define-public crate-ljm-sys-0.1 (crate (name "ljm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1b2q1r9v8kh8dywf8ir69rkkp843sbxf1c8fgv2fzh3swm03jaxb")))

(define-public crate-ljm-sys-0.1 (crate (name "ljm-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "15h474816p7xscgl1fmc38gf1zbvra633rcajls4lfdr4z2n61gn")))

