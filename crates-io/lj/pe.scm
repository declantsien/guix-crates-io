(define-module (crates-io lj pe) #:use-module (crates-io))

(define-public crate-ljpeg-0.1 (crate (name "ljpeg") (vers "0.1.0") (hash "0w1b70p5famzx0hvz1g6gshqw4pxa0ix1sp2f2isn32b07p7rlm4") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-ljpeg-0.1 (crate (name "ljpeg") (vers "0.1.1") (hash "04gj7891ai2ijhjr2lhf8r8qqw3jkc22nv8y8qv0dms53y3zpsjl") (features (quote (("std") ("default" "std"))))))

