(define-module (crates-io lj pr) #:use-module (crates-io))

(define-public crate-ljprs_async_pool-1 (crate (name "ljprs_async_pool") (vers "1.0.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 0)))) (hash "1yydd9gx394zvj9rrmvhhv5xpld6sdd2ckmsr42wv4lr3ig0shxy")))

(define-public crate-ljprs_es-0.1 (crate (name "ljprs_es") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00axyk3rzp24n76z00zp42dv91nf2ib2s67g6kxpm0xlb1v4prc2")))

