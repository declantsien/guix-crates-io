(define-module (crates-io iw li) #:use-module (crates-io))

(define-public crate-iwlib-0.1 (crate (name "iwlib") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "iwlib_sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.88") (default-features #t) (kind 0)))) (hash "1z854laaz6mydgaq0dbjdx72sfifqjprirdbayww3wfil1371c3z")))

(define-public crate-iwlib-0.1 (crate (name "iwlib") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "iwlib_sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.88") (default-features #t) (kind 0)))) (hash "179vr86m6cp4gbahri01pfspzmanziiaw379khjfnfkvd8b14xiz")))

(define-public crate-iwlib_sys-0.1 (crate (name "iwlib_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1hgrjd845ay9bhjcp7ikcb7d0zn08xfqdlfa706dm7qn3aqcqwn6")))

(define-public crate-iwlib_sys-0.1 (crate (name "iwlib_sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "0vhyv3xw1mcm8ymzkmc954lcv2ry7jvjmhfwvskf4xhspfjz009a")))

