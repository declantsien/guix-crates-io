(define-module (crates-io iw as) #:use-module (crates-io))

(define-public crate-iwasin_guessing_game-0.1 (crate (name "iwasin_guessing_game") (vers "0.1.0") (hash "0qszwgqba3q85zikdhijwbjahm4d1h6yi745zid90iiwbg9k13q4")))

(define-public crate-iwasin_guessing_game-0.2 (crate (name "iwasin_guessing_game") (vers "0.2.0") (hash "1j505pyrfjdcp415izb83niii3kk6ksaxqyvc4d45w21dp1428h2")))

