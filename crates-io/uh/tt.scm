(define-module (crates-io uh tt) #:use-module (crates-io))

(define-public crate-uhttp_body_bytes-0.5 (crate (name "uhttp_body_bytes") (vers "0.5.0") (hash "1zhw1bgyyc3ak0qcqwjkipkfajabg7jrjqrlqm8d1c6al4wzxq4z")))

(define-public crate-uhttp_body_bytes-0.5 (crate (name "uhttp_body_bytes") (vers "0.5.1") (hash "1w63phkdl8nfw0g1q6069ws0ass9x0c9p18my5yy0mwi21023n60")))

(define-public crate-uhttp_body_bytes-0.5 (crate (name "uhttp_body_bytes") (vers "0.5.2") (hash "1b6fa807ikj8divbfgbllv68z8kprzx6pibi2i9y7lca7f227c6b")))

(define-public crate-uhttp_chunked_bytes-0.5 (crate (name "uhttp_chunked_bytes") (vers "0.5.0") (hash "19iys9vgr63rh28424ha97jd97v176dqxh1fpskqwldqq0fy790j")))

(define-public crate-uhttp_chunked_write-0.5 (crate (name "uhttp_chunked_write") (vers "0.5.0") (hash "0my605jzi3y0pvwjsp572gmgjidliyh2bbpipzlmy0icpwsvij7d")))

(define-public crate-uhttp_chunked_write-0.5 (crate (name "uhttp_chunked_write") (vers "0.5.1") (hash "1kns0vr8fna3v2brk6k19z4fc9sjcv5qwb96f0j328kax9vviv8a")))

(define-public crate-uhttp_content_encoding-0.5 (crate (name "uhttp_content_encoding") (vers "0.5.0") (hash "1fhmmpapx5hg35cr2fwgqsa28zvy52x4cpqj166rskm4q5lmgzjq")))

(define-public crate-uhttp_content_encoding-0.5 (crate (name "uhttp_content_encoding") (vers "0.5.1") (hash "1h9c3c54h52zl044mwxx81x2zcan3f2d7v19h1pq5v0la8sg2r0m")))

(define-public crate-uhttp_json_api-0.5 (crate (name "uhttp_json_api") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "subslice_index") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_body_bytes") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_chunked_bytes") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_content_encoding") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_media_type") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_method") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_request") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "uhttp_request_target") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_status") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_transfer_encoding") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_uri") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "uhttp_version") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0kc4dxj33hcsj5pgrrwgfjvyqg8y64v222554dkyv881l00vfr80")))

(define-public crate-uhttp_json_api-0.6 (crate (name "uhttp_json_api") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "subslice_index") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_body_bytes") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_chunked_bytes") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_content_encoding") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_media_type") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_method") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_request") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "uhttp_request_target") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_status") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_transfer_encoding") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_uri") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "uhttp_version") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0d3s7hd4bbdi30cy2yndwmzhg54pfvqzkpc8ypripxh047mwja47")))

(define-public crate-uhttp_json_api-0.6 (crate (name "uhttp_json_api") (vers "0.6.1") (deps (list (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "subslice_index") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_body_bytes") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_chunked_bytes") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_content_encoding") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_media_type") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_method") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_request") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "uhttp_request_target") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_status") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_transfer_encoding") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uhttp_uri") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "uhttp_version") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "12s687xhbaikr41fs21bgimg5p0f4bp493ad9sqbifbckmgsgpnm")))

(define-public crate-uhttp_media_type-0.5 (crate (name "uhttp_media_type") (vers "0.5.0") (deps (list (crate-dep (name "memchr") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fh4wlkm9hqzsfsqm8wy7jqhh9xq8c4sb2zg84kjgf9gc4br0vzy")))

(define-public crate-uhttp_method-0.10 (crate (name "uhttp_method") (vers "0.10.0") (hash "0k3d31m7a6ly8rfiz58basm5fx8r2qcx8yqvkywndn7ksbgv9ksn")))

(define-public crate-uhttp_request-0.5 (crate (name "uhttp_request") (vers "0.5.0") (deps (list (crate-dep (name "memchr") (req "^1.0") (default-features #t) (kind 0)))) (hash "11s6p8c1nl6gwc2r6n450rp0m2cj489sqgdnqwyhv45id6wah115")))

(define-public crate-uhttp_request-0.5 (crate (name "uhttp_request") (vers "0.5.1") (deps (list (crate-dep (name "memchr") (req "^1.0") (default-features #t) (kind 0)))) (hash "10qjmdga4hn4wgm8pgdmzgfg6hy97h3vfwviqbacf9lnrik97qgs")))

(define-public crate-uhttp_request_target-0.5 (crate (name "uhttp_request_target") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hxj6mx0qf8khw6nqhrzidnnlzrrz5hrr7777zwsqhh3n9rx4kv6")))

(define-public crate-uhttp_request_target-0.6 (crate (name "uhttp_request_target") (vers "0.6.0") (hash "1xg10n51kb8cdrim0n0k99h0gwc7mwygsx1v4cfb3hmh1w75nskp")))

(define-public crate-uhttp_response_header-0.5 (crate (name "uhttp_response_header") (vers "0.5.0") (hash "0jwxwnycn35n80v2970rm2bpx7bclmy1bcapvxy46wc0w88ihp7x")))

(define-public crate-uhttp_sse-0.5 (crate (name "uhttp_sse") (vers "0.5.0") (hash "1kkhidr8akk1ryrplwj17dpmwqda1a028n6zh7hpbk1s1r7q16gv")))

(define-public crate-uhttp_sse-0.5 (crate (name "uhttp_sse") (vers "0.5.1") (hash "0i4lln00csnifm2h4cbm7d2djgd6w7na66mvn4q64852bcs97zy6")))

(define-public crate-uhttp_status-0.10 (crate (name "uhttp_status") (vers "0.10.0") (hash "0mw0nq8ihlyvah8lp610f0ja4x6ly0jw91cf46dm6xcgbirjsl6b")))

(define-public crate-uhttp_transfer_encoding-0.5 (crate (name "uhttp_transfer_encoding") (vers "0.5.0") (hash "0zywyi3brnpsj4ai7dhy246x75qgihi9y5m6x5hi817yzar6iwl6")))

(define-public crate-uhttp_uri-0.5 (crate (name "uhttp_uri") (vers "0.5.0") (hash "0802n9bhjicjmz68pma644fcq7aqjwlggddv75gg0ia4mhk386m2")))

(define-public crate-uhttp_uri-0.5 (crate (name "uhttp_uri") (vers "0.5.1") (hash "0nw0cbn2llcjfbp150mhqg4sn17s48xn6kc782j673w0vbk01dmr")))

(define-public crate-uhttp_version-0.5 (crate (name "uhttp_version") (vers "0.5.0") (hash "1xbz1qgzffwngmhx1zgfi2wcwsss4w6597nhs13wbq2af8cbz96q")))

(define-public crate-uhttp_version-0.6 (crate (name "uhttp_version") (vers "0.6.0") (hash "1nv786wkq1w4j53fg6dpx7xw2rlqlls3vdxrm2s27aa45jrg6gmm")))

