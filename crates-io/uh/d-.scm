(define-module (crates-io uh d-) #:use-module (crates-io))

(define-public crate-uhd-sys-0.1 (crate (name "uhd-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "0xpidgmnvm0rj639mid26wvy3nrsm1lhrc4wgbzbkv6vwxnakw6a") (links "uhd")))

(define-public crate-uhd-sys-0.1 (crate (name "uhd-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "1fdbfr50d6mp5gqlpgswgzipw0bj30rc1d82q3vpsfkf6jkz180s") (links "uhd")))

(define-public crate-uhd-sys-0.1 (crate (name "uhd-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "1m9mn90bx2aa7rdzmdx8z8r26y89j0wdmrivlc2i3rq51ddnwsbm") (links "uhd")))

