(define-module (crates-io uh id) #:use-module (crates-io))

(define-public crate-uhid-fs-0.0.1 (crate (name "uhid-fs") (vers "0.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "enumflags2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uhid-sys") (req "^1") (default-features #t) (kind 0)))) (hash "0f1ixzv5dicw5wrq8fyj6zxb2ic4jaddb351021mgsq4qkh0gcym")))

(define-public crate-uhid-sys-1 (crate (name "uhid-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.35.0") (default-features #t) (kind 1)))) (hash "01jpaa56bzvgsrd4r8z1az7lk9fh0l7yimhwcvb514n3cy0ysjkg")))

(define-public crate-uhid-virt-0.0.2 (crate (name "uhid-virt") (vers "0.0.2") (deps (list (crate-dep (name "enumflags2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uhid-sys") (req "^1") (default-features #t) (kind 0)))) (hash "08fqr46jm8ajagm746nlvcx99wsfqnylfnhbk0xym1xphpkavijp")))

(define-public crate-uhid-virt-0.0.3 (crate (name "uhid-virt") (vers "0.0.3") (deps (list (crate-dep (name "enumflags2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uhidrs-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h3338iha8s51h9gphl5wxw9p0i0pkp8i6lpvp2p72z38y3y7kh3")))

(define-public crate-uhid-virt-0.0.4 (crate (name "uhid-virt") (vers "0.0.4") (deps (list (crate-dep (name "enumflags2") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "uhidrs-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0360jvimb59d6kxp2yz7gbvwxxsv6cwp6m90qabzsq7r10kcnzh5")))

(define-public crate-uhid-virt-0.0.5 (crate (name "uhid-virt") (vers "0.0.5") (deps (list (crate-dep (name "enumflags2") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "uhidrs-sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "08n8rys35bb948q2ydglinl4713r38k64bc0yjcvf6r8n3xpncby")))

(define-public crate-uhid-virt-0.0.6 (crate (name "uhid-virt") (vers "0.0.6") (deps (list (crate-dep (name "enumflags2") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "uhidrs-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16cl8pwbwmipvahghdshy5pzf2paqy3bg338c23dzlvsz1jy25jg")))

(define-public crate-uhid-virt-0.0.7 (crate (name "uhid-virt") (vers "0.0.7") (deps (list (crate-dep (name "enumflags2") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "uhidrs-sys") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "17r42j8p32d2d9hg5707gwzkm63a97j1plyd4ivg0vdr0gvg2w2g")))

(define-public crate-uhidrs-sys-1 (crate (name "uhidrs-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)))) (hash "1amx3nlzs63pm6ddvcw5niagy80jarzi5jdw06sw77bnl1l2m2yp")))

(define-public crate-uhidrs-sys-1 (crate (name "uhidrs-sys") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.54") (kind 1)))) (hash "0vlim703i6zggwnl1y93gd75315bzwkw9cv41vzbqf4k34d3pk5z")))

(define-public crate-uhidrs-sys-1 (crate (name "uhidrs-sys") (vers "1.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.63") (kind 1)))) (hash "061wbn0x85mr997fg4gc0f32z2lvy7m51i6y8dc2saa3p06v4ck4")))

(define-public crate-uhidrs-sys-1 (crate (name "uhidrs-sys") (vers "1.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (kind 1)))) (hash "17rgcl5i0v6c0my4pxdarh8ksmnifd99ljqhv4g5f4g5rvxq0j72")))

