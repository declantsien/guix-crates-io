(define-module (crates-io uh pp) #:use-module (crates-io))

(define-public crate-uhppote-derive-0.1 (crate (name "uhppote-derive") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "09zkpv13j4kfj0s9abmwsanmj88y8sa6jkmkn16cl62rgkr4vn9s")))

(define-public crate-uhppote-rs-0.0.1 (crate (name "uhppote-rs") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "uhppote-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0mci7hl3j4izbjmdkm9hiqdja0r36ddm8inzadk9qpzq82mfq4l3")))

(define-public crate-uhppote-rs-0.0.2 (crate (name "uhppote-rs") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "uhppote-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "12x42gqxm7chy0h3aa9q10n6v14h1iydxxh6l3apfaki9z8j3943")))

(define-public crate-uhppote-rs-0.0.3 (crate (name "uhppote-rs") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "uhppote-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "14q4sw9aypjb4rjyq8v277g3zxhsi5zmrn308fxgzn9n6hzhr82z")))

(define-public crate-uhppote-rs-0.0.4 (crate (name "uhppote-rs") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "uhppote-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "163ya3nd9hl75ddai16fhfislhw2qn4wap6gbwvsqnr9k3kdrgag")))

(define-public crate-uhppote-rs-0.0.5 (crate (name "uhppote-rs") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^2.0.0-rc.1") (features (quote ("derive" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "uhppote-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z7fc533lxnkxrvjvm5hzl6nlms6nrcfdx7gm71y0vrk1m981pv7")))

(define-public crate-uhppote-rs-0.1 (crate (name "uhppote-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^2.0.0-rc.1") (features (quote ("derive" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "uhppote-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18cm93n2faazcwlid9n4bnl2rmah9jmcypzmlv5hh96qq69yvf6v")))

(define-public crate-uhppote-sys-0.0.1 (crate (name "uhppote-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0cif33ygm8wnjk410262dbsakcha70dl1v8h54aarcxhbv8qbqvl") (yanked #t)))

(define-public crate-uhppote-sys-0.0.2 (crate (name "uhppote-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1h88xjwlic72vid9brkd8lr6kzqfigrhlxchbn4gccskjx7rdfzx") (yanked #t)))

(define-public crate-uhppote-sys-0.0.3 (crate (name "uhppote-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1xgqy2sy9zp0l22zw4wxf7w1n0w52jrs68jsa95iw4hg0jf1sjxl") (yanked #t)))

(define-public crate-uhppote-sys-0.0.4 (crate (name "uhppote-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0ffbclyhn9x5zmcv2cb30z66lh7rmag88qrinjz5hda9yyl767d5") (yanked #t)))

