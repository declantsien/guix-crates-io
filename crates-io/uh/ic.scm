(define-module (crates-io uh ic) #:use-module (crates-io))

(define-public crate-uhicqcrsht-0.0.1 (crate (name "uhicqcrsht") (vers "0.0.1") (hash "0h5shj3q0yw5l2dl5nl8vdg9sfqzx6kdv29rsbiyb26zy4lqcc65")))

(define-public crate-uhicqcrsht-0.0.2 (crate (name "uhicqcrsht") (vers "0.0.2") (hash "0b419v0gasdfvn7zmq0sv12gj3zsrrjgv0n9sm6czdq6wlzpchxq")))

(define-public crate-uhicqcrsht-0.0.3 (crate (name "uhicqcrsht") (vers "0.0.3") (hash "078441bagdjrrc2xxp87m6x0n1v275iiawmpa3wkm7a78n1p1kmv")))

