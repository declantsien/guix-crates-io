(define-module (crates-io yp wt) #:use-module (crates-io))

(define-public crate-ypwt-0.0.1 (crate (name "ypwt") (vers "0.0.1") (hash "0wgzyk3r0qzp7fvkiqgc9s4kzy76r69jrv8070wvxjxf77w5s8xa")))

(define-public crate-ypwt-0.0.2 (crate (name "ypwt") (vers "0.0.2") (hash "06yb57i893k6zrk67bx5xxwipwwxlam6kk98qzyrr8kfhfwvv8bh")))

(define-public crate-ypwt-0.0.3 (crate (name "ypwt") (vers "0.0.3") (hash "1546pcf7w10nh40clhskjn2vmm6fy1kmdw0hbydflp7b7bl2v630")))

