(define-module (crates-io yp to) #:use-module (crates-io))

(define-public crate-yptoscr-1 (crate (name "yptoscr") (vers "1.1.1") (deps (list (crate-dep (name "bors") (req "^1.1.1") (kind 0)) (crate-dep (name "const-sha1") (req "^0.2.0") (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (kind 0)))) (hash "1m3rqqv25dal6ymaxzwipnzvwdg12vlhraa4d2n402v90mdqx36k") (features (quote (("std" "hex/std" "bors/std") ("default" "std") ("alloc" "hex/alloc" "bors/std"))))))

