(define-module (crates-io yf ft) #:use-module (crates-io))

(define-public crate-yfft-0.1 (crate (name "yfft") (vers "0.1.0") (deps (list (crate-dep (name "num-complex") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "num-iter") (req "^0.1.33") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "packed_simd") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1l6w4qvldhp8c9hi4ykkcll6vywyhxbsxlc1j31svnlpdzi0ir3n")))

