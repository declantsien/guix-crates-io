(define-module (crates-io xw ar) #:use-module (crates-io))

(define-public crate-xwarn-0.0.10 (crate (name "xwarn") (vers "0.0.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1vs8y214d1m20vincg60bz7xil4agpxfc786qm3rj7mvln69ynvx")))

