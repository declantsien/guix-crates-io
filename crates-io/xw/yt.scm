(define-module (crates-io xw yt) #:use-module (crates-io))

(define-public crate-xwytools-0.1 (crate (name "xwytools") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "number_prefix") (req "^0.4") (default-features #t) (kind 0)))) (hash "0i5vbgbww8nsss6xzshcxd04hm8nm86xx0hf24nync3i59a7infc")))

