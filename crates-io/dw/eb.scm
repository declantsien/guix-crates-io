(define-module (crates-io dw eb) #:use-module (crates-io))

(define-public crate-dweb-0.1 (crate (name "dweb") (vers "0.1.0") (hash "0dx2c0jsjjv9gdr3g9mcvz6q1xvamikwiqjzgdqja6rndhjc97yg")))

(define-public crate-dwebp-0.1 (crate (name "dwebp") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "png") (req "^0.17") (kind 2)) (crate-dep (name "tempfile") (req "^3.2") (kind 2)) (crate-dep (name "webp-animation") (req "^0.5") (features (quote ("image"))) (kind 0)))) (hash "11gmshmz8062kfdzmaxknjflhprsgfvvkr8wsh6jksdrjips5lkp")))

(define-public crate-dwebp-cli-0.0.0 (crate (name "dwebp-cli") (vers "0.0.0") (hash "0qv00mv9biwd7v7yv7w3zac2fkv1dvyrrlrwm3n5fap7qms1mxzi")))

