(define-module (crates-io dw al) #:use-module (crates-io))

(define-public crate-dwal-0.1 (crate (name "dwal") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "086phva185w3fx7haxkvk6qb02fvvs1gd13vrh8s0mv3bgjzl9ya")))

(define-public crate-dwal-0.1 (crate (name "dwal") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0b9x7xlv56v9lpqnv6zjwf8097573zxwwj7yh2b8bnw6p5kczc9f")))

(define-public crate-dwal-0.1 (crate (name "dwal") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0rkg6vpd5frd7kn93b46mk6ggqpyhif2aj9f36fm0qfcp6smqg1z")))

(define-public crate-dwal-0.1 (crate (name "dwal") (vers "0.1.3") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv_codegen") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y1cn35845k7h6px0jmzgawxkglg00s4s9lf1a6vhrdfsfg7p4i3")))

