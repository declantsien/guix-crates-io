(define-module (crates-io dw -b) #:use-module (crates-io))

(define-public crate-dw-bytebuffer-0.1 (crate (name "dw-bytebuffer") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0lqzyar3fzs6jp0qvw5r666wihvdjcxj6fcn0bglz5a3ww6g2pwr") (yanked #t)))

