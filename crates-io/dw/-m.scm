(define-module (crates-io dw -m) #:use-module (crates-io))

(define-public crate-dw-models-0.1 (crate (name "dw-models") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (features (quote ("chrono"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bxv4y0rsf8a65z2lzaw1ydxiqis3a23v1r5f0lpnazxfh7fza37")))

