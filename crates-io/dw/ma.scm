(define-module (crates-io dw ma) #:use-module (crates-io))

(define-public crate-dwmapi-sys-0.0.1 (crate (name "dwmapi-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "140nwgq2dwydar0dr3l3aapbxl8iq1wk2czy63nf3prxc0bd3yvd")))

(define-public crate-dwmapi-sys-0.1 (crate (name "dwmapi-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "1zj11h02idy0hz94fdmmad8cn5pf1hfkg42d1ay1jr1rgg6cgi07")))

(define-public crate-dwmapi-sys-0.1 (crate (name "dwmapi-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0xiyc8vibsda0kbamr9zkjvkdzdxcq8bs1g5mq4yc4mbmr168jxl")))

