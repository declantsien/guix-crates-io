(define-module (crates-io dw ee) #:use-module (crates-io))

(define-public crate-dweet-0.1 (crate (name "dweet") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.6.13") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "15f7vgi16fjnm820aayn8sm33cwq34w583dr9l2wq04z64k3qz2d")))

