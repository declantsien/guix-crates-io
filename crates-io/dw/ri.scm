(define-module (crates-io dw ri) #:use-module (crates-io))

(define-public crate-dwrite-sys-0.0.1 (crate (name "dwrite-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0a0qy7l2f8nzksza0vgyc93f6ckhb99fnwalfaxbzxf4fcil2r9b")))

(define-public crate-dwrite-sys-0.2 (crate (name "dwrite-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0vldjn16bid1x565zl9g82mqf6lv2kr78b918a2vqqiqyf0854d7")))

