(define-module (crates-io dw pr) #:use-module (crates-io))

(define-public crate-dwprod-0.1 (crate (name "dwprod") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fallible-iterator") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "gimli") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0bqyl6hbdfpbjcw611xk6jx85454w643h44lg1q3fnaww63cmsji") (features (quote (("exe" "clap") ("default" "exe"))))))

