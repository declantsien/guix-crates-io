(define-module (crates-io dw -s) #:use-module (crates-io))

(define-public crate-dw-sys-0.1 (crate (name "dw-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0wx9anfiv11s6kqfmqa74nkjwdx5v73jpcqv0b8nqqpngz43bsql") (links "dw")))

