(define-module (crates-io dw t-) #:use-module (crates-io))

(define-public crate-dwt-systick-monotonic-0.1 (crate (name "dwt-systick-monotonic") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-alpha.0") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "0sp91fm7rnjxdmqw46zw2y7cdnzwrzvlmdmrhi3c16zvjxh8k8ml")))

(define-public crate-dwt-systick-monotonic-0.1 (crate (name "dwt-systick-monotonic") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-alpha.0") (default-features #t) (kind 0)))) (hash "1qjxidmb0f5za97n5qq8clqk8ngq3acly8jxd1wnd4azf2map14n")))

(define-public crate-dwt-systick-monotonic-0.1 (crate (name "dwt-systick-monotonic") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "1qni9c4j50xllh8jqqn45bxcksbqz282rbpc5i6mgd97klplvlli")))

(define-public crate-dwt-systick-monotonic-0.1 (crate (name "dwt-systick-monotonic") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)))) (hash "0sxasi44s4g4qggqblim1ny6pbnn5s6y3lxd8xa82h327dp4h4m1")))

(define-public crate-dwt-systick-monotonic-0.1 (crate (name "dwt-systick-monotonic") (vers "0.1.0-rc.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-rc.1") (default-features #t) (kind 0)))) (hash "06pl3v7gslrks0j2k2v8qkcswcw18giy4bx3q4an4yjkam8h097f")))

(define-public crate-dwt-systick-monotonic-0.1 (crate (name "dwt-systick-monotonic") (vers "0.1.0-rc.2") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^0.1.0-rc.2") (default-features #t) (kind 0)))) (hash "14f6a55h397igimvdqdcsl7sw4fgj97wdn8y2zrlbmhsnqnkw2z5")))

(define-public crate-dwt-systick-monotonic-1 (crate (name "dwt-systick-monotonic") (vers "1.0.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0gpisqv1h20rbshnyyhjw84dw5ljw68gq5cfwb9xwcibw2hyy7fm")))

(define-public crate-dwt-systick-monotonic-1 (crate (name "dwt-systick-monotonic") (vers "1.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rtic-monotonic") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ky0i0lbld353l4niz86fa9yz2z5v91byawqqqgzkkgijiqq1kw9") (features (quote (("extend"))))))

