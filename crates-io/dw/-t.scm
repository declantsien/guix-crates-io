(define-module (crates-io dw -t) #:use-module (crates-io))

(define-public crate-dw-transform-0.1 (crate (name "dw-transform") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dw-models") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "12pv40x8fljdq7gk130lsh35cc0wj9cmzf1y9r2j85csxiqxzrxy")))

