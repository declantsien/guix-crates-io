(define-module (crates-io ds dl) #:use-module (crates-io))

(define-public crate-dsdl-0.0.1 (crate (name "dsdl") (vers "0.0.1") (deps (list (crate-dep (name "cyphal") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0pzqx2a5mbws3bjsfrdi4qaplfv5hvjwwf2wzhjh5acs5cwxz0cx")))

(define-public crate-dsdl-0.0.2 (crate (name "dsdl") (vers "0.0.2") (deps (list (crate-dep (name "cyphal") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1r97majjsp2ga6mpz7z2harcz0sidq5mfcsnsf4fd6vvhgrs48qb")))

(define-public crate-dsdl-0.0.3 (crate (name "dsdl") (vers "0.0.3") (deps (list (crate-dep (name "cyphal") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1hlx731b75ms29iii198cqjhgp34n2k5znyd70142g417a4nh14m")))

(define-public crate-dsdl-0.0.4 (crate (name "dsdl") (vers "0.0.4") (deps (list (crate-dep (name "cyphal") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0i0zafbij65wazxrxlijrws8p4rly60959jp9d2lmm9al08k22l1")))

(define-public crate-dsdl-0.0.5 (crate (name "dsdl") (vers "0.0.5") (deps (list (crate-dep (name "cyphal") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "02g01ib2720q818iklj1qpbn9xjpwgpa4gmkdm2ild87h70lbcwq")))

(define-public crate-dsdl-0.0.6 (crate (name "dsdl") (vers "0.0.6") (deps (list (crate-dep (name "cyphal") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0q4mpl7pfhc6wpisz598q6sp23fnkmmpb9qdd69hlx86y175a363")))

(define-public crate-dsdl-0.0.7 (crate (name "dsdl") (vers "0.0.7") (deps (list (crate-dep (name "cyphal") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "0n24jxi9k8ywq6d8dkgijh51ydn7fpnnskmbwmp068fyw0fjwv1n")))

(define-public crate-dsdl-0.0.8 (crate (name "dsdl") (vers "0.0.8") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)))) (hash "1va5hkhdil0bb923vk2r1w04fzlw03xi8pygprkrwi9vgxfvdqnp")))

(define-public crate-dsdl-0.0.9 (crate (name "dsdl") (vers "0.0.9") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cyphal-dsdl") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.60") (default-features #t) (kind 0)))) (hash "0si2rwgzwbfqvfqchaq38a5vasf3zrydrbiv7fd9wmpmw0w0w935")))

(define-public crate-dsdl_compiler-0.0.1 (crate (name "dsdl_compiler") (vers "0.0.1") (deps (list (crate-dep (name "badlog") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dsdl_parser") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "inflections") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zysgvs9vhxdrkkdzx36qk3121wb4c2x2mgm9hcd9i7vpkv5v9xy") (features (quote (("default" "binary") ("binary" "getopts" "badlog"))))))

(define-public crate-dsdl_parser-0.0.1 (crate (name "dsdl_parser") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "test-logger") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1i0akk0clb7hm9050zqywl8bjq8wpiivnwh4059iwblrllbkb817")))

(define-public crate-dsdl_parser-0.1 (crate (name "dsdl_parser") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "test-logger") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1wqzcyw3kxqzwm86lp21ld0brfhmfjvykr0ckwa036ffbcx3bnz6")))

(define-public crate-dsdl_parser-0.1 (crate (name "dsdl_parser") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "test-logger") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1bkf7xygyav33vsgy59z6x465lqxk4av9wc0d8h8w7y5mrpsf4i0")))

(define-public crate-dsdl_parser-0.1 (crate (name "dsdl_parser") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "test-logger") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "02m45n4aya1cjslrvvdccv0lviqgy0pxjjrvn6vxs3qfy0lvdghj")))

(define-public crate-dsdl_parser-0.1 (crate (name "dsdl_parser") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "test-logger") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0dsvm7k1ca1n5wpi7m2syi5lk4gfqwhvz797iawd52pr8mal24w1")))

