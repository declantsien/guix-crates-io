(define-module (crates-io ds mr) #:use-module (crates-io))

(define-public crate-dsmr5-0.1 (crate (name "dsmr5") (vers "0.1.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1a119r1phncasy6wsgjylz56pkc9q2pyfs57w8lcw3hd25mzl4w6")))

(define-public crate-dsmr5-0.1 (crate (name "dsmr5") (vers "0.1.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0jap721i6nqr0ram29x22sylymqqb7jg2bfh7pja2wjx5irnf5pm")))

(define-public crate-dsmr5-0.1 (crate (name "dsmr5") (vers "0.1.2") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1iymm4y6hcc9g0d78hfjid0y1mwqi5lma1xx7y27806h0axlxifs")))

(define-public crate-dsmr5-0.2 (crate (name "dsmr5") (vers "0.2.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0m0qk2y49n299ds4l3k91gp2cnhlqqypg8gjq3dszwf9ad3g2dgc")))

(define-public crate-dsmr5-0.2 (crate (name "dsmr5") (vers "0.2.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0j0hqvc73gd6ap0ygmaarkgxj99w6yn7nw7l7s4nzikxaynnamz3")))

(define-public crate-dsmr5-0.2 (crate (name "dsmr5") (vers "0.2.2") (deps (list (crate-dep (name "crc16") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (kind 0)))) (hash "0w6wcayzrn50b8riy85iqnrx7m9man2aaxbp4m6awjsacandrf7p")))

(define-public crate-dsmr5-0.3 (crate (name "dsmr5") (vers "0.3.0") (deps (list (crate-dep (name "crc16") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (kind 0)))) (hash "1y86vvp5pak1saj266inrhg162b6l4q1l04x3f897pwwvclchcvz")))

(define-public crate-dsmr5-0.4 (crate (name "dsmr5") (vers "0.4.0") (deps (list (crate-dep (name "crc16") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (kind 0)))) (hash "0fxpvzwd74nb985hbqxzz3wp5fd753lxjkjcr0h9s9vxs122bfsr")))

