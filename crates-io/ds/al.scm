(define-module (crates-io ds al) #:use-module (crates-io))

(define-public crate-dsalgo-0.1 (crate (name "dsalgo") (vers "0.1.0") (hash "0dhl5gqwcnsm98p9ybxravlbk6vv8gi1ki8dr61mza0g9kcm4z57")))

(define-public crate-dsalgo-0.1 (crate (name "dsalgo") (vers "0.1.1") (hash "0dr0sqpxnacdf320gly28xjg1d1z7f5n2n864a7dc5fki1mnbk5d") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.1 (crate (name "dsalgo") (vers "0.1.2") (hash "1vnh9ddpyn3p77k8xck88zg73pk1kqlrsk6s65f74f7kbs0iwfvj") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.0") (hash "1f2mm69z1v644w4y39hjks1r2rnckzisi32v4qnh48a06ipjgb95") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.1") (hash "0hrxwacsjs8z94dy5i8x3hq9w3kzr9fiwnvdvkj59s6x4f6rjc4y") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.2") (hash "0vyfrd923jjqzislxj8a5y60d1r5jab7f1xg8hwc37iw4njnigjq") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.3") (hash "16l15lqvwglif1w7nb7bfnbpgh7bk4w8wc4i0gyv44dlg8g93ycg") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.4") (hash "1113lmhwwyrml8cqyfhib0fj9g97q2wr0y6kghyyg0f503rk1pgn") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.5") (hash "0h9b77730a9yjprbz44rw8nnax09s6kg23d6y8ma7v3kkijdcr4y") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.6") (hash "1hba6zz3gm00llpnhs4wz62d2b9f8nvv570vm44b7b03xs309gy8") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.7") (hash "06zvx2dkhvj97vxcrggng6rv9yq34cy06jvnwnxk9w685156vlfb") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.8") (hash "1pwndwr0y3m9aldrkxfrc112jfg2z1fadi6kg50q4ybcbckcllii") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.9") (hash "16c7bgc9dx5nqaja5w0cws3rrf2l7s5gw1mxbyqd55mh89kl92h1") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.10") (hash "0sfnf33nja240mhapyizxr305xv0h1p6j9w2d3vmpc8a9ah43jnb") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.11") (hash "00vp3968vi58jipmgzqwiway0mdgpzj041m304p5pvxvyp5nvkqp") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.12") (hash "0r9h5c22bmix33l0a4yj2aw2krma039b0nky1az8x6ipy763h6df") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.13") (hash "1x7nasw2ngb9lzjd6cia4ac0gm584hp6ljaik987qhjq17szr30p") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.14") (hash "18xljhbg624kyx2nizmzn5x76x04ay5i6hwn9l4w5svc42f3bcy0") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.15") (hash "0shxb987m1758b499sdj3b6cd6mzzpiz4kr1hjg10cahm05b3sz5") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.16") (hash "143ab46h851788r1kik56wnrxflxzz173dqq924yy15l1a1xdavr") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.17") (hash "07m09nd26gyck08nwdv5g4rp6cyb2v3k29ip0nzixhyn8k94y70n") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.18") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1ayzbvrwhaza9a80sv4zwkql6ya6ccg966filll5h65r8gxamhwi") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.19") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vza34r7j689fmnwijyglih431rbz7s12ksjga1wmd003x5xyz5y") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.20") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1jg7izllbix2vicng2s802nj6qr7rfmmdf3wn2n9cj4q3xh5b6yz") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.21") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0gaifaddqsixqdqc9kkhzkd1060mqfy785jf2cvpi4cjnx5nv3my") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.22") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "10invrac0j9crcjmnj1k3jx5way7v25y90kwjvbfkx2ngld8a24w") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.23") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1x8320v89jc45i8gf0jqs6c2rs6iw3pq66nf460m21m9xwz3m8jc") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.24") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1fk2y5glpx61bzyi8gindi36v09q35hr7c6fky23z6hcaxw4irdd") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.25") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ys3vgwgf7p61i2imjyh024ddxpzn1rm20i98ak4bs577ggq1xr4") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.26") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zgk0q3kcsw2x4pjwiazcw29ja55hz663ab1wpgiys6r6z6s0aq1") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.2 (crate (name "dsalgo") (vers "0.2.27") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0p5qkwgmrkqp182jc463308x7m99l1b5bywcbv3ldd3kfm6mxww5") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xz05sbm9yirfk9126i2ica90knl1696ksmslmhchsgrkkkn0dv7") (rust-version "1.58.1")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0f7sad8qcnmi9cryislp0flq04ndxxi5617rarxqs3n2qrazdsqd") (rust-version "1.63.0")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1g3gq2p9b0fk7rypprki2mnf2d3wlvvli9103dqd1w0cpg2j6lkl") (rust-version "1.63.0")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0w5q6vcsbyri3j4jkxbpyhyhx12pm6492901q39x6irar3y6apqr") (rust-version "1.63.0")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zd0gf7fibppazaawjwvvmhi5y6zc1jd2fhs0m48r02bh79zv21a") (rust-version "1.63.0")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0786h1djcvzlp4cnvgngi2hb58qik6v3p4h1p3rpcxg4pagwyh1c") (rust-version "1.63.0")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1qk5wilw48yqmd89p6rvj41bbzlwrm37b8hyw54l8c9n4w14fr42") (rust-version "1.63.0")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0srapxcks5yysmmxpvvrsccjycav04fpymgsmqaqja3ph5xfg6kr") (rust-version "1.63.0")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.8") (hash "0vdmws3qkg5x00zglg771pdhq61jmhxlnr4zb3l3xdjxd104ap9n")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.9") (hash "1i7n36lgaxchfqmp53n8jl2yanijazlzj1wdlk6xafkkvwpa6gk4")))

(define-public crate-dsalgo-0.3 (crate (name "dsalgo") (vers "0.3.10") (hash "05bh8hqa1him947lf5fqlbicqf9xqcils81w72lv8fxv5zz25rhz")))

