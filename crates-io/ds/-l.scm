(define-module (crates-io ds -l) #:use-module (crates-io))

(define-public crate-ds-learn-rust-0.1 (crate (name "ds-learn-rust") (vers "0.1.0") (hash "1kv7fmrdzzjwa3sh0kpni3dsyyl5ppyw1ylmrcqij7jz6xnxv7s4")))

(define-public crate-ds-learn-rust-0.1 (crate (name "ds-learn-rust") (vers "0.1.1") (hash "1sk7g8x23sh2b0n2nac42mdhl9hi22rhkl0gfabc7l54g0r2q5jn")))

(define-public crate-ds-list-0.1 (crate (name "ds-list") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "15rqf1w1mphqwm3r7dpgaka458hf91lzdpb6zlfxz6fg22zr6jdi")))

(define-public crate-ds-list-1 (crate (name "ds-list") (vers "1.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "11p992bgqrhphzbcab0yhzl7bqqk4iwnlq7b64n26gy0s2wbaklz")))

(define-public crate-ds-list-2 (crate (name "ds-list") (vers "2.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0j1460srdz62zxijk8ivxw5gzdnylkb7hpvj70kpxfad27lc3bjc")))

