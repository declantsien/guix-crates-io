(define-module (crates-io ds a-) #:use-module (crates-io))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0pzkg2hnjbvkzi9clj2mqss65647sgypyiyv2dvk7apxmdmq7jkj")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0gai6qjbii94i7rixw41rnczi5kls1ibjyc5hzd0sp41lnlg63fg")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.2") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1l85hgrvnf7np6qz1vqxi386njm0m6y78ww39cjhkhcim96wn3yp")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.3") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0hh5006a5nfgmp9wrp6d3zghil4zcvidjscc810dlsa17vviqmkn")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.4") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0fgr5sbxyzvagw44l39j9yyckhs26v1bqwg4qvpwbl6b67vx2f7r")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.5") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0yb6qp0fzxysmlkxqwxp4yi3s60chmi48vmfgafg08yrff44iskz")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.6") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0izhwqc6hvnv466pb8qszc88byfgiwv66hrvk4r6v6b7g783g281")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.7") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "152nzs990z2vnp0q0fdrw32pm397vxkw1wka2r5rhbx4k52yif37")))

(define-public crate-dsa-lib-0.1 (crate (name "dsa-lib") (vers "0.1.8") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1w0fxa4yk0rp4ryld7zs5cxa7ixgrj9b4xxzh8z1548xr3363702")))

(define-public crate-dsa-rs-0.1 (crate (name "dsa-rs") (vers "0.1.0") (hash "0x1wmnagqlb4vz8dybzqgm30v9mg7646bx95lincadbfcymgdc51") (yanked #t)))

(define-public crate-dsa-rs-0.0.0 (crate (name "dsa-rs") (vers "0.0.0") (hash "0fj8rrhqgzg6rh6gglra5sqyfrq1qwwy9bvr46ywyj35av47p73j") (yanked #t)))

(define-public crate-dsa-rs-0.1 (crate (name "dsa-rs") (vers "0.1.1") (hash "03mska825gb7pbgx98acgjdsqqmsqc9bdh6mm8wilwrykmxmf2bh") (yanked #t)))

(define-public crate-dsa-thu-0.0.1 (crate (name "dsa-thu") (vers "0.0.1") (hash "1pbpp2jp31vc2rg6nj195f80r5c5r2chnjrcx41wn6mwmwbzvry2")))

