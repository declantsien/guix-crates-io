(define-module (crates-io ds a_) #:use-module (crates-io))

(define-public crate-dsa_sport-0.1 (crate (name "dsa_sport") (vers "0.1.0") (hash "1ifz2g7911vw6zyp31x546ky577jxcy24y5wlnmy2m6zfi32wx05")))

(define-public crate-dsa_sport-0.1 (crate (name "dsa_sport") (vers "0.1.1") (hash "01zmib3g5ml2s9yanfah5kzjv52g8vhmc40s49p8nkmrx936vss3")))

(define-public crate-dsa_sport-0.1 (crate (name "dsa_sport") (vers "0.1.1-a") (hash "015ranyr13iccr9nx1pmd0n2mihpp2sdhxby9cpyzz5l6fiwpic8")))

