(define-module (crates-io ds v-) #:use-module (crates-io))

(define-public crate-dsv-seeker-0.1 (crate (name "dsv-seeker") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0khh46ajcgp442plb9mj1m1jp4mfmq3ks79jyinbxx1ypdh7h8dy")))

