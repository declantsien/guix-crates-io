(define-module (crates-io ds _s) #:use-module (crates-io))

(define-public crate-ds_store-0.1 (crate (name "ds_store") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (kind 0)))) (hash "1vz8kd1zlw76d2h1zfm38pav5fyr33q4gzqjkch8bnrlilkxlgvy")))

(define-public crate-ds_store-0.1 (crate (name "ds_store") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (kind 0)))) (hash "0r98r6zzsra3gjidw4zs2610ppy86i2bhh9dlfkv7i89xhq6sazx")))

(define-public crate-ds_store-0.1 (crate (name "ds_store") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.2") (kind 0)))) (hash "04vgm2zpdb23bhbj7glww0x42r16qjfx8kw463xirr9lk35kjhq3")))

(define-public crate-ds_store-0.2 (crate (name "ds_store") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1nxra51ssaq6p0a88ryvzwvn564i3xrh6x5l9mqlpawgglxlwc3r")))

(define-public crate-ds_store-0.2 (crate (name "ds_store") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1klslpi6wq0dhczpw45869qx7n78xn6qn4q9vnv8zq1ifv1c0vmn")))

(define-public crate-ds_store-0.3 (crate (name "ds_store") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "18hxyxplmc0kmdr3fs7cw65q994bzq50qxyyp5c7rc17aia6qxi1")))

