(define-module (crates-io ds if) #:use-module (crates-io))

(define-public crate-dsif-1 (crate (name "dsif") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "traceidr") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0bavhw2ac51nrdr15x0pqvndh2vwky5h0hj0dycxiyriallh2frc")))

