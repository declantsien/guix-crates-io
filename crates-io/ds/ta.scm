(define-module (crates-io ds ta) #:use-module (crates-io))

(define-public crate-dstat-0.0.1 (crate (name "dstat") (vers "0.0.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1b9nhvx2d3kkbx572pfkgxxhcd48c2aa5sb2r7lv7d4fnqp5hxa6")))

(define-public crate-dstat-0.0.2 (crate (name "dstat") (vers "0.0.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0qwmia55fwcrx3p9ssy7np3db3af4w64m9pvvpr64j552xjrxya4")))

(define-public crate-dstation-0.0.0 (crate (name "dstation") (vers "0.0.0") (hash "0d4ypidizh9hc45zdfrdv7lkkqqmy24sbc1lbwz3m40lnawzwi6m")))

