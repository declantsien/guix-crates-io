(define-module (crates-io ds #{18}#) #:use-module (crates-io))

(define-public crate-ds18b20-0.1 (crate (name "ds18b20") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "one-wire-bus") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1g09366zdnwjx0lbcnsjfh6mpm8jxyzldqxifc9sw3a4jrpzf21g")))

(define-public crate-ds18b20-0.1 (crate (name "ds18b20") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "one-wire-bus") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1kjk3flsn4qyn1w7iiim87ir7r4da0sp3mjchz130v21w2n3gkf9")))

