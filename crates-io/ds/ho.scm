(define-module (crates-io ds ho) #:use-module (crates-io))

(define-public crate-dshot-frame-0.1 (crate (name "dshot-frame") (vers "0.1.0") (hash "0h3v8grmcl833xka8ysbab5lb5gh0633yxzmbi82qafxxq1qqv41")))

(define-public crate-dshot-frame-0.1 (crate (name "dshot-frame") (vers "0.1.1") (hash "1vhw1761zhmf1r9s921pim4nvf807ys227mcwrhl7vpd2h65yggf")))

(define-public crate-dshot-frame-0.1 (crate (name "dshot-frame") (vers "0.1.2") (hash "13dil3iddgy1pgz7gziwa5620nk7znspplp9d7afdncss8n6456a")))

(define-public crate-dshot-frame-0.1 (crate (name "dshot-frame") (vers "0.1.3") (hash "1q0y893ndf3dil3d490zhjxsgg09zqga8yjhc2ipz09k3pq3qrl4")))

