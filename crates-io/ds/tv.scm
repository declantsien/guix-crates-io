(define-module (crates-io ds tv) #:use-module (crates-io))

(define-public crate-dstv-0.1 (crate (name "dstv") (vers "0.1.0") (hash "18wcfbd7pzbqx5yqxw4ayx4gvmayr7j5kcz6amk2cjxnc16qw51s") (rust-version "1.56")))

(define-public crate-dstv-0.2 (crate (name "dstv") (vers "0.2.0") (hash "1gkb8mgyjw4gi4p2pkzbzjx1blj8z5236n5c8d72b874z15i2svz") (rust-version "1.56")))

(define-public crate-dstv-0.3 (crate (name "dstv") (vers "0.3.0") (hash "03dhr9vwigxi66sv358c669a5dqwmw21g5c5yg2qz7khpdjwgg36") (rust-version "1.56")))

(define-public crate-dstv-0.4 (crate (name "dstv") (vers "0.4.0") (hash "1m5w3j8lvilbwvpz18dxbzlfmvhn2blrr29pw6d1yn6zj0rvwch0") (rust-version "1.56")))

(define-public crate-dstv-0.5 (crate (name "dstv") (vers "0.5.0") (hash "0683rbf5plc7347k2zdkqmx2gsz4bp68l8z6dm8y26c0wcs8fbry") (rust-version "1.56")))

