(define-module (crates-io ds t-) #:use-module (crates-io))

(define-public crate-dst-container-0.1 (crate (name "dst-container") (vers "0.1.0") (deps (list (crate-dep (name "dst-container-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zwzgynjaskaxvcd6f1qg59dxpf9a17a6z786vbsra0n990pidsj")))

(define-public crate-dst-container-0.1 (crate (name "dst-container") (vers "0.1.1") (deps (list (crate-dep (name "dst-container-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "00rfxacldzzxqc6knyq0zhqasdbnp6x7zafk0pq08c0s76g60di3")))

(define-public crate-dst-container-derive-0.1 (crate (name "dst-container-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-crate") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vagbif3p7fg3x5j2hb2bjr49xx73dska0yyh0xk4rblqkqxcrwj")))

(define-public crate-dst-init-0.1 (crate (name "dst-init") (vers "0.1.0") (deps (list (crate-dep (name "dst-init-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1aqrh0rrzlq29ksvajhsinz0b7wxia2dk30qnrb000fgr2yc231h")))

(define-public crate-dst-init-0.1 (crate (name "dst-init") (vers "0.1.1") (deps (list (crate-dep (name "dst-init-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05v5s5fxicjbpsmx83zfbb0zba8phq37qsy09bq1widrq5cf2hq5") (yanked #t)))

(define-public crate-dst-init-0.2 (crate (name "dst-init") (vers "0.2.0") (deps (list (crate-dep (name "dst-init-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mq6y001jd02r5kbdg6nj7p886knj6y6bxj4xgppxgv7nhkgn2m2") (yanked #t)))

(define-public crate-dst-init-0.3 (crate (name "dst-init") (vers "0.3.0") (deps (list (crate-dep (name "dst-init-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qam3k49gwnhraf0z7w6w58zg8lrblfhdscwhzjww0rlwjk4vr1v")))

(define-public crate-dst-init-0.4 (crate (name "dst-init") (vers "0.4.0") (deps (list (crate-dep (name "dst-init-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0dkyysyf9hcp2ajx9w8m6sahsbniwflr07fjfa6lqi0zsaxx4ics")))

(define-public crate-dst-init-macros-0.1 (crate (name "dst-init-macros") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.*") (features (quote ("full" "extra-traits" "derive"))) (default-features #t) (kind 0)))) (hash "132crw60clqp6q2ykr8s53clf0wm1kz2zx8xff95aj8b4b9vg2nr")))

(define-public crate-dst-init-macros-0.1 (crate (name "dst-init-macros") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.*") (features (quote ("full" "extra-traits" "derive"))) (default-features #t) (kind 0)))) (hash "041400q24825a5k4anq4ky25fav3gbk4mzd3naimn0fp6qhx22ky")))

(define-public crate-dst-init-macros-0.2 (crate (name "dst-init-macros") (vers "0.2.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.*") (features (quote ("full" "extra-traits" "derive"))) (default-features #t) (kind 0)))) (hash "17cyjba3j1r0ybr3l1k3qmdk7vybacdyb0l92qhfa4j12f6h45j0")))

(define-public crate-dst-init-macros-0.3 (crate (name "dst-init-macros") (vers "0.3.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.*") (features (quote ("full" "extra-traits" "derive"))) (default-features #t) (kind 0)))) (hash "1dgd7lfl4qnhn8mv6b6vw38ha8rps8qvm7h11s4jwf6a8zdnm56k")))

(define-public crate-dst-init-macros-0.4 (crate (name "dst-init-macros") (vers "0.4.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.*") (features (quote ("full" "extra-traits" "derive"))) (default-features #t) (kind 0)))) (hash "1k36p7rkp8v7yz36z3ll41n9h0cnskpgxppgsahxhakqagy6i633")))

