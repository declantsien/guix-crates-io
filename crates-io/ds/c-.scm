(define-module (crates-io ds c-) #:use-module (crates-io))

(define-public crate-dsc-commands-remover-0.1 (crate (name "dsc-commands-remover") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "poise") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cys0mj1a0fi47zjiczkc183hpgy5pl0qv83b5gb9jwbji884i5g")))

(define-public crate-dsc-commands-remover-0.2 (crate (name "dsc-commands-remover") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0p0vqf1w7356ma3nrhx1jmdbqxqpsvasppir2f6w27d83wh70isb")))

