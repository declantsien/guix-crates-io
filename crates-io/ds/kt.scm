(define-module (crates-io ds kt) #:use-module (crates-io))

(define-public crate-dsktp-0.1 (crate (name "dsktp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1q9q9vy690b3xdl5svci03n7pk4bz534494ib5gcfv6zyp1y7c7k")))

