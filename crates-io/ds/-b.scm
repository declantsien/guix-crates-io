(define-module (crates-io ds -b) #:use-module (crates-io))

(define-public crate-ds-bst-0.1 (crate (name "ds-bst") (vers "0.1.0") (hash "1p6103nm3hhm2sva0iigjlq4x3r74pb6b5ki2p3g0rmiv20ziqwk")))

(define-public crate-ds-bst-0.2 (crate (name "ds-bst") (vers "0.2.0") (hash "144y040xqm1fmcj319w92a0hl0skw2m3q00jb7gc7clwjv3my2p9")))

(define-public crate-ds-bst-0.3 (crate (name "ds-bst") (vers "0.3.0") (hash "1brmj3m1v4mfmw2hivawrzlcdy6jzjw0f7nlb143l9r0h17rpsw1")))

