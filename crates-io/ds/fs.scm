(define-module (crates-io ds fs) #:use-module (crates-io))

(define-public crate-dsfs-0.0.1 (crate (name "dsfs") (vers "0.0.1") (deps (list (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (default-features #t) (kind 0)))) (hash "1hb3nf3ri85flfqrghvjgh6lp1liq1q9c987z5s99ljhmh85n3i2")))

(define-public crate-dsfs-0.0.2 (crate (name "dsfs") (vers "0.0.2") (deps (list (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (default-features #t) (kind 0)))) (hash "1f7l77flb68k1qha15x0gbybw4x0ypq9jfgk8pdgsda592f8qnj9")))

(define-public crate-dsfs-0.0.3 (crate (name "dsfs") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (default-features #t) (kind 0)))) (hash "10gxn8155x6i551zvsxckcn94x6hibgiyh6nkqx0zyf88rp3irgm")))

