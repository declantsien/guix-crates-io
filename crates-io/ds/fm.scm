(define-module (crates-io ds fm) #:use-module (crates-io))

(define-public crate-dsfmt-rs-0.0.1 (crate (name "dsfmt-rs") (vers "0.0.1") (hash "14c2r130nry20c7nkxa3ndjsbhc3ss24ji0wg78hvpqw01jw6jag")))

(define-public crate-dsfmt-rs-0.0.2 (crate (name "dsfmt-rs") (vers "0.0.2") (hash "1hzj166andir00wl1m7j14r097z0yh209l7wf1465nksix4c47n9")))

(define-public crate-dsfmt-rs-0.1 (crate (name "dsfmt-rs") (vers "0.1.0") (hash "0wdnwn42wvl1x4wx0f88dmgdx3xnrviismkqbhgr9cykwlpcqwrk")))

(define-public crate-dsfmt-rs-0.1 (crate (name "dsfmt-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jrqspp9kn811m93kssggih2l8h3jdv77g5m6p63lg3q8rbl41m7")))

