(define-module (crates-io ds p-) #:use-module (crates-io))

(define-public crate-dsp-chain-0.2 (crate (name "dsp-chain") (vers "0.2.0") (deps (list (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0pb2qg22m1jlckzdw3v490hwi9f6zi25ahrlrbdk89j9fj4gqzwf")))

(define-public crate-dsp-chain-0.3 (crate (name "dsp-chain") (vers "0.3.1") (deps (list (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "1v3v5n45q8m5kkiym3275lfcwicn61ydrnbhxh89173r47nq39vm")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.0") (deps (list (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0n5zjn2i8372c98ni1nc6cl9l76ss5dlikc41siancdsnsi1y453")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.1") (deps (list (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "1mjf8zbgby2a9lmwn54pxd4k2yyvkr2bkvq1qc9aq5wmycf5b3bj")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.2") (deps (list (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "17i9p1kz8k5jg72xp39qph20y1a5swpjs9sjzhv5bd18p7slv8qf")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.3") (deps (list (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0kgb67wp18j3w4557d5rnqpykwillphvqilw5w3nn5z6hd6is39b")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.4") (deps (list (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "129awh05szlai2rp3hc7bz0i5azhzkcpj5ccgpza9wsfnb28nqia")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.5") (deps (list (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "1034syiw5rx96fr6wll1hhz1md0ya226vcg93kvbafbjy4k03940")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.6") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0x9b047k6jxlzsk9wn6qbk4gmni6iwwcjns697fh0i2c7bqkn1c2")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.7") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0xl219wp6rl45mgr3cfpdiyrhf435m1w08rjw0dsxcd50icly491")))

(define-public crate-dsp-chain-0.4 (crate (name "dsp-chain") (vers "0.4.8") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "1y33whwx9pnq70nsvdjcrj5icjizhcf5llk871gqcpqr8668v8n6")))

(define-public crate-dsp-chain-0.5 (crate (name "dsp-chain") (vers "0.5.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0c8b9jzldxs04dbrw6cny2n2zybdq784c8j8llc6jd28kb6gjdyn")))

(define-public crate-dsp-chain-0.5 (crate (name "dsp-chain") (vers "0.5.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0nkj7rj6v1d63rkv5qg18x261h5gfafq2hrfhdg08rps2nlqy4fc")))

(define-public crate-dsp-chain-0.5 (crate (name "dsp-chain") (vers "0.5.2") (deps (list (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "11gnbs655gxyywfiizbgkf3yh5b4g4pmlyrj6wiaqzcszrm75941")))

(define-public crate-dsp-chain-0.5 (crate (name "dsp-chain") (vers "0.5.3") (deps (list (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "06wg12pkcanl7bw9wa4z9nmnzb5q4bhvn20c6nhq9v4igy6cg3qi")))

(define-public crate-dsp-chain-0.5 (crate (name "dsp-chain") (vers "0.5.4") (deps (list (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "*") (default-features #t) (kind 0)))) (hash "0crp1ysjx5srd4kw7c9blrdv4jc1p3fga3vnw5f2xzf6674frxks")))

(define-public crate-dsp-chain-0.5 (crate (name "dsp-chain") (vers "0.5.5") (deps (list (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "05sx617nbgf807dv44ia7fdgf9b7klzwvxrq816k08hn9p33ki7r")))

(define-public crate-dsp-chain-0.5 (crate (name "dsp-chain") (vers "0.5.6") (deps (list (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "03grg6sd4a4izky1q77v2fbm4rdsl82nmh0kyimmm1dvi68kmxbi")))

(define-public crate-dsp-chain-0.6 (crate (name "dsp-chain") (vers "0.6.0") (deps (list (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "petgraph") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "sound_stream") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0zsq7kg9i6nyynknnj1wc8diz6yjsm4d4klj9l7hx0v2j31kx88v")))

(define-public crate-dsp-chain-0.7 (crate (name "dsp-chain") (vers "0.7.0") (deps (list (crate-dep (name "daggy") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "sound_stream") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "100vbjxdibhmyyfy1ksw03zibknvrr7id6rvwg81ki5z8l2gfw4a")))

(define-public crate-dsp-chain-0.7 (crate (name "dsp-chain") (vers "0.7.1") (deps (list (crate-dep (name "daggy") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "sound_stream") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0sy8z43m6360xmdxi9q3zlwykzz8rxvp2psb7sdbdq8g7xvik7ia")))

(define-public crate-dsp-chain-0.7 (crate (name "dsp-chain") (vers "0.7.2") (deps (list (crate-dep (name "daggy") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "sound_stream") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0ik1hidkh8kgifr92apnrfflymxzjjrflmhraly5vbprf8jfv0y6")))

(define-public crate-dsp-chain-0.8 (crate (name "dsp-chain") (vers "0.8.0") (deps (list (crate-dep (name "daggy") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "sound_stream") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1cbb4076371ni5xbx4n3lj8bvb5yj726na2bnk6qk9iycg0sccih")))

(define-public crate-dsp-chain-0.8 (crate (name "dsp-chain") (vers "0.8.1") (deps (list (crate-dep (name "daggy") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "sound_stream") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0vvb9sdfs12995kcy5w2bxzfp51bwzwidk5ksddg14ncygqh98kd") (yanked #t)))

(define-public crate-dsp-chain-0.9 (crate (name "dsp-chain") (vers "0.9.0") (deps (list (crate-dep (name "daggy") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "sound_stream") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0jdsi98l8afjqcwd70sq7b3cdl68324ncf9hnbr3r05k9h3kx99h")))

(define-public crate-dsp-chain-0.10 (crate (name "dsp-chain") (vers "0.10.0") (deps (list (crate-dep (name "daggy") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "sample") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0mx3wb6f3slfviqvcgwfis3qf51lp6hd24vybvvgq6ibqby7gip2")))

(define-public crate-dsp-chain-0.11 (crate (name "dsp-chain") (vers "0.11.0") (deps (list (crate-dep (name "daggy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "sample") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1dh8lqzhipls11rg0k9xm1bzb3xmsf7925r4psim76xg0wr83ycg")))

(define-public crate-dsp-chain-0.12 (crate (name "dsp-chain") (vers "0.12.0") (deps (list (crate-dep (name "daggy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.3") (default-features #t) (kind 2)) (crate-dep (name "sample") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0aciaj3w1p6xwc1wa976s74zk59lsgas7g8i32mrb4ks1hqsd3s6")))

(define-public crate-dsp-chain-0.13 (crate (name "dsp-chain") (vers "0.13.0") (deps (list (crate-dep (name "daggy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "sample") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "03qwsx6hbwnqqdmv6jxhln0sdfxfhx1acrdpnywjkc8wim8660ln")))

(define-public crate-dsp-chain-0.13 (crate (name "dsp-chain") (vers "0.13.1") (deps (list (crate-dep (name "daggy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "sample") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "05vyl4q76m4mm1jwd9fip9i1c303dxd1lfs0a2zzzbr7ayqvnlaj")))

(define-public crate-dsp-rs-0.1 (crate (name "dsp-rs") (vers "0.1.0") (hash "1q2sj5ji1wd1y35yv9jkjklnh262i44as3sl3b8rc7ab7sd4gg8p")))

