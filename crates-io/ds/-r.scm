(define-module (crates-io ds -r) #:use-module (crates-io))

(define-public crate-ds-rs-0.1 (crate (name "ds-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)))) (hash "0d9ml2q9p7pyadvvx8ksbvvgv2sbk8nydsp1103j06lmckx8nww4")))

