(define-module (crates-io ds #{2d}#) #:use-module (crates-io))

(define-public crate-ds2d-0.1 (crate (name "ds2d") (vers "0.1.0") (deps (list (crate-dep (name "glow") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0kdp3p3wvy4v0q0fr2isg5mgax5yla9nmzj1cp7kgndp4g5lyj6i")))

