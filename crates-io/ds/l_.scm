(define-module (crates-io ds l_) #:use-module (crates-io))

(define-public crate-dsl_macros-0.1 (crate (name "dsl_macros") (vers "0.1.0") (hash "16fynw215a1ndlavkpkhi8af2hnwj7lx8a208jk455wzhm0mvks5")))

(define-public crate-dsl_macros-0.1 (crate (name "dsl_macros") (vers "0.1.1") (hash "0gnrd92wi4nraiwyy8jvmmbj4iiyy2n4yn9xbkxdxnj41q2mqim2")))

(define-public crate-dsl_macros-0.1 (crate (name "dsl_macros") (vers "0.1.2") (hash "0i43cnl3cziv3wpcjhww2flbcwnpzj0pm2jdprxrdn0rpb6vdvjd")))

