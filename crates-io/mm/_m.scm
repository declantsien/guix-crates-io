(define-module (crates-io mm _m) #:use-module (crates-io))

(define-public crate-mm_math-0.0.1 (crate (name "mm_math") (vers "0.0.1") (hash "1ckwh8q4a7mcabrgxyn82ij092i9lwb2avs7ygx5k1gfk72izich")))

(define-public crate-mm_math-0.0.2 (crate (name "mm_math") (vers "0.0.2") (hash "0dcz4svy079207b8k9zkrrjka1nppix7jpjf6mbg1k4mndi0hg7g")))

(define-public crate-mm_math-0.0.3 (crate (name "mm_math") (vers "0.0.3") (hash "16j111g7adrcwwpz4ml0av908kpx7c29mwdrx4knswqx45m2bpjl")))

(define-public crate-mm_math-0.0.4 (crate (name "mm_math") (vers "0.0.4") (hash "1a5anqgm9m8hzblnnyllmf0jyg1jr939cffa481gzdc594ykj9y6")))

(define-public crate-mm_math-0.0.5 (crate (name "mm_math") (vers "0.0.5") (hash "1cf1silwxvp9lcfln7mwwgg9rxrgv54wgd7h959fd3j59bglvp6y")))

(define-public crate-mm_math-0.0.6 (crate (name "mm_math") (vers "0.0.6") (hash "1ibyrrxs7xfn49ka4l07fdajm6673qaf94svk555y4j07j146n9y")))

(define-public crate-mm_math-0.1 (crate (name "mm_math") (vers "0.1.0") (hash "1zf8gcxzc01460m9smh6ahqdx96pgqagv8cwxbkwh66j95c86299") (features (quote (("int64") ("float64")))) (yanked #t)))

(define-public crate-mm_math-0.1 (crate (name "mm_math") (vers "0.1.1") (hash "0xzsc48lsc40992pm2kpvbjw81x43fbvma6iky0yiyakgvnwxpxr") (features (quote (("int64") ("float64")))) (yanked #t)))

(define-public crate-mm_math-0.1 (crate (name "mm_math") (vers "0.1.2") (hash "015ja74mw5kbi213y64cxpv3sl9zqf5kh4mmacwhs7idnjqbvn8r") (features (quote (("int64") ("float64")))) (yanked #t)))

(define-public crate-mm_math-0.1 (crate (name "mm_math") (vers "0.1.3") (hash "03gwy6l5n2asvsyjivrzvb1glhww2hpcdcpqyrn556gljhb2v4fd") (features (quote (("int64") ("float64")))) (yanked #t)))

(define-public crate-mm_math-0.1 (crate (name "mm_math") (vers "0.1.4") (hash "095vqmsf4gax5f7wrr7zmla15zg7q6z70r29g8k78zwmxc8ibrly") (features (quote (("int64") ("float64")))) (yanked #t)))

