(define-module (crates-io mm ft) #:use-module (crates-io))

(define-public crate-mmft-0.1 (crate (name "mmft") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "bio") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xnvfp7hzz5f980hf7qjzrf7ic62g6xlbjwypj4ydcxdq3zql6x3")))

