(define-module (crates-io mm _i) #:use-module (crates-io))

(define-public crate-mm_image-0.0.1 (crate (name "mm_image") (vers "0.0.1") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "1f9gp63nphp1diq62syh6vhkpz818vy1q9fyy5qyxnxr05zwhbm2")))

(define-public crate-mm_image-0.0.2 (crate (name "mm_image") (vers "0.0.2") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "0xz2k7nb5zd018x77ryfx3mid20k4g8g1y28yab66cgxj005lw0r")))

(define-public crate-mm_image-0.0.3 (crate (name "mm_image") (vers "0.0.3") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "1707llxpawz739ycn60lzciyls20lp2j63272nwyi814z5nvxnl1")))

(define-public crate-mm_image-0.1 (crate (name "mm_image") (vers "0.1.0") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "1ys4y2i1lvg1xhdgqsi6ml73m5zlf16xx6sqiabnjabv248sl8hc")))

(define-public crate-mm_image-0.1 (crate (name "mm_image") (vers "0.1.1") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "1z36jlvca9f9mys4v9bhh9g4rhn0wq67lifp7i5ai7wlarf6nr49") (yanked #t)))

(define-public crate-mm_image-0.1 (crate (name "mm_image") (vers "0.1.2") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "0xx51c92y6vwyn9ffdh8x4mgix4rzf5jqnmwxqga52p7ksj0sqci") (yanked #t)))

(define-public crate-mm_image-0.1 (crate (name "mm_image") (vers "0.1.3") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "1rxj462zcv1jxd7xzz2qk9yq4z3vnrjpw1si9la5kjq478jyvkfy") (yanked #t)))

(define-public crate-mm_image-0.1 (crate (name "mm_image") (vers "0.1.4") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "1i4b837460fv2x2d615ksqfn3czs31dr8yfv5h8kqlsmzs5rn1f3") (yanked #t)))

(define-public crate-mm_image-0.1 (crate (name "mm_image") (vers "0.1.5") (deps (list (crate-dep (name "mm_math") (req "*") (default-features #t) (kind 0)))) (hash "1vcljc6w18f2qiqgycs88p3srqc02rlmqdkpvwxsbwl7aiq5bslv") (yanked #t)))

