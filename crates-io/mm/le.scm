(define-module (crates-io mm le) #:use-module (crates-io))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1vn78q2wyxbd9cfjdds1j280m0q6s4khq4rk07w778r3hyf60qw5")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0ai9hyj5yd4kbhz33q7jyvkchbxrd1y3qdrvw7aqbpkx24ryl3pb")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0j358dwr89pvmcrn38w3cgk5ngm94zgr2z2mzqyn8j3pxg19p27d")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "1s1g8ji2k1p0p3vv7rcwkas0w5s8c2fqbikm7c4nbf1ds04zm60p")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "1n7pc7bbr185hz74m2lkvvaj5ci6bmxjc83gm5wr4cyghf68dxpp")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "1125vlwjwsq3h2y8jd8b0c2i77qdwjsybfk0cdnsrqvhyngdfdlf")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.6") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "1fq7lfb04sm07sddf9225vc789kcszdqabn9f1i72n9fm80s6667")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.7") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0swy6jsrh18ff2dsy7fbfnqlpkd1bdis48nmvshaz6ycfa8hp19g")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.8") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "1sl7q00mr0p1bwdl6qyys9w1p2qrvlc7lr5hfpyfwkf5dnrn3282")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.9") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "197fvfs177xycl174r0sapjpzj12mp2hr593xhdnvjgndp5fsazx")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.10") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0j4dypqlr0y8bzqzh9z7hbisb6qdzwpcp13a91wfpd4yja2g6s1f")))

(define-public crate-mmledger-0.1 (crate (name "mmledger") (vers "0.1.11") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "032h8hfndhx4afh1p02ljwz5a2w6fryjz4kwkfjd6j1hvqz0c9nf")))

(define-public crate-mmledger-0.2 (crate (name "mmledger") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "lset") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "primordial") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.12.0") (default-features #t) (kind 2)))) (hash "1kizyz70fvaci535v48ald98i0inip5li2p0r7xl7vrghz7clvq2") (rust-version "1.57")))

(define-public crate-mmledger-0.3 (crate (name "mmledger") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "lset") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "primordial") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.12.0") (default-features #t) (kind 2)))) (hash "08q0y5d9vp49fbcqmwxn1nnzsscy09kv7d0d46yrx9028ha9gs05") (rust-version "1.57")))

(define-public crate-mmledger-0.4 (crate (name "mmledger") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "const-default") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lset") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "primordial") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1vxpg2bfkw7mbp7qs1db4xzja9rshj6ii2z65kq76dyrjinz7syx") (rust-version "1.57")))

