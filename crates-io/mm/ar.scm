(define-module (crates-io mm ar) #:use-module (crates-io))

(define-public crate-mmarinus-0.1 (crate (name "mmarinus") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "07x1ws4hp7d7nl6n4akb1yd5wq7k4b9zf9m6jphcxlbz7izqcb67")))

(define-public crate-mmarinus-0.2 (crate (name "mmarinus") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0znqixvpz4zm0rm8flq6saqzi3zj0pnhqgb4prv9zbg9dv4gv4j2")))

(define-public crate-mmarinus-0.2 (crate (name "mmarinus") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b463v3rqmqb70fg1avv00m6frlggy50p2i4v0i5nxiy68n1q03g")))

(define-public crate-mmarinus-0.3 (crate (name "mmarinus") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19n9kainf7lfysmjafckxfrl8rw9sdhpckgqfsnplpn70fi0n0kl")))

(define-public crate-mmarinus-0.4 (crate (name "mmarinus") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wwlgwsydf90qbq94k753958kwrs1l0nk5gg6yi3z0m3wzk9i01b")))

