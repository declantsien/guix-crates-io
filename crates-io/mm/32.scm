(define-module (crates-io mm #{32}#) #:use-module (crates-io))

(define-public crate-mm32f3270-pac-0.1 (crate (name "mm32f3270-pac") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1r8h0xrk8ycbmvbnzfbw8ira5iqx15qcm334zibsd5s61an41ljb") (features (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-mm32f5-0.1 (crate (name "mm32f5") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qd9l73dinv6kdnk568s2s1fwyphyhms3g3l0yj61g4ck0mv8vwq") (features (quote (("rt" "cortex-m-rt/device") ("default" "critical-section"))))))

