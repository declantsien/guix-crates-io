(define-module (crates-io mm b-) #:use-module (crates-io))

(define-public crate-mmb-parser-0.1 (crate (name "mmb-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "1qzw8aff49mxhy1g48vprr3x77072zw304aldba3wjxa0smsilgk")))

(define-public crate-mmb-parser-0.2 (crate (name "mmb-parser") (vers "0.2.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1cr4j0d6gn354cvynfc697yyn6gv6g7d0jxg14w3yhb161hvp9nw")))

(define-public crate-mmb-parser-0.3 (crate (name "mmb-parser") (vers "0.3.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1clkcjv8vxnf2fr991p7x9lqkzbapv6k1g82i85s5y59kyl76pr8")))

(define-public crate-mmb-parser-0.4 (crate (name "mmb-parser") (vers "0.4.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1407d08vr7jihnvmy0mjrgpjl9z4sxkl9rhh6xqa7izz54570f00")))

(define-public crate-mmb-parser-0.5 (crate (name "mmb-parser") (vers "0.5.0") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1mbpbz482pcv09yky7wyv0iyczjjhwk3qpg8n2ryrqa6x3xvbl52")))

(define-public crate-mmb-parser-0.5 (crate (name "mmb-parser") (vers "0.5.1") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "15fzwg3gsvli8symc09cny7xaxawrnknca3via3wjbhjcslxc9sd")))

(define-public crate-mmb-parser-0.6 (crate (name "mmb-parser") (vers "0.6.0") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1isya56nxwc7ja466darxdbay1jdp5acnpa1rpnm25gvybbxjkdm")))

(define-public crate-mmb-parser-0.7 (crate (name "mmb-parser") (vers "0.7.0") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1f7l03nki8ayml30wr9hbj0v3ip511mhkmd4lsnvl9ws3f19hf8x")))

(define-public crate-mmb-types-0.1 (crate (name "mmb-types") (vers "0.1.0") (hash "074fljkdywhcs12kv9fkvd8zclhcn6day03m4dx1ia2f43g8vjx4")))

(define-public crate-mmb-types-0.1 (crate (name "mmb-types") (vers "0.1.1") (hash "1f1axb3w452pa1dcfq3hp24444af64v1ip7y4hwkvbkp7idqwppa") (yanked #t)))

(define-public crate-mmb-types-0.1 (crate (name "mmb-types") (vers "0.1.2") (hash "17picdhz734i06s0w23krl6r4chv8s0lkgqxp5anqjixx1977q2d") (yanked #t)))

(define-public crate-mmb-types-0.2 (crate (name "mmb-types") (vers "0.2.0") (hash "0kipj6gapx15bfwmrsx8vfn9bpcycam9hwnrzghp6mvhi6d8fc40")))

(define-public crate-mmb-types-0.3 (crate (name "mmb-types") (vers "0.3.0") (hash "0j7i5q86ygf059nf3zjwqqkbzm0fj46w6f0ks7fkms61zswg1055")))

(define-public crate-mmb-types-0.3 (crate (name "mmb-types") (vers "0.3.1") (hash "0qs0gnbxk4mvvlz958bknsqi1q1jc1baqqa0lmli36shia7h118i")))

