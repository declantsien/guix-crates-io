(define-module (crates-io mm f_) #:use-module (crates-io))

(define-public crate-mmf_parser-0.1 (crate (name "mmf_parser") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0i48iibywbbkp80jls6qy3k8rrwnvy40sg581bdw52z6v6bac1r0")))

(define-public crate-mmf_parser-0.1 (crate (name "mmf_parser") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1mmi47yvzc9rymyvh7z9zmwx960jkjl3af2gdjndckxha932mhl6")))

(define-public crate-mmf_parser-0.1 (crate (name "mmf_parser") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "104c812i4fs456jjk3hibrzpj5q8nd5q9d01h1y50qfh88hdc9bl")))

