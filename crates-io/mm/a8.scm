(define-module (crates-io mm a8) #:use-module (crates-io))

(define-public crate-mma8452q-0.1 (crate (name "mma8452q") (vers "0.1.0") (deps (list (crate-dep (name "accelerometer") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "cast") (req "^0.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "157cniszv9k25prbbii6mkahvas9pbam6n98ggvh8ba8fi4qrrgl") (features (quote (("out_f32") ("default"))))))

(define-public crate-mma8x5x-0.1 (crate (name "mma8x5x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1xappkg4pabp5ai108yd5wfrp48hi34x2vzqk8mvjxrwd1pz0y8f")))

(define-public crate-mma8x5x-0.1 (crate (name "mma8x5x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1f4warh5arab2anl5bkpq2hdgf0skafgs1xw5nrgpin8sqwsk6ri")))

