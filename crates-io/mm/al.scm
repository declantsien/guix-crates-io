(define-module (crates-io mm al) #:use-module (crates-io))

(define-public crate-mmal-sys-0.0.0 (crate (name "mmal-sys") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x4q9n8ygmf6gxfjzzbl75xj6gw8h0xc83s0r41709yfcyrap79a")))

(define-public crate-mmal-sys-0.1 (crate (name "mmal-sys") (vers "0.1.0-1") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "172f29z440f99mc1wd2sk3gkck4fqlfv6g5dav1rjm6rqfxrwm5m") (features (quote (("generate_bindings" "bindgen") ("default"))))))

(define-public crate-mmal-sys-0.1 (crate (name "mmal-sys") (vers "0.1.0-2") (deps (list (crate-dep (name "bindgen") (req "^0.40.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05ggh31pflwi6yq57l2c69pjibrf0wba1rnlv10qspv4ns0zxisr") (features (quote (("generate_bindings" "bindgen") ("default")))) (links "mmal")))

(define-public crate-mmal-sys-0.1 (crate (name "mmal-sys") (vers "0.1.0-3") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "185a2sds01jjwfy3yyyzws1h5a98095rx1kx4xfixhfb1k43wg58") (features (quote (("generate_bindings" "bindgen") ("default")))) (links "mmal")))

(define-public crate-mmalloc-0.1 (crate (name "mmalloc") (vers "0.1.0") (deps (list (crate-dep (name "sc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yhzvqgph6r140s2bcfd1riwxqmxrzwq801wi74lr6kfqq94hbv1") (features (quote (("default" "allocator") ("allocator"))))))

