(define-module (crates-io mm px) #:use-module (crates-io))

(define-public crate-mmpx-0.1 (crate (name "mmpx") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("jpeg" "png"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0mray79m5ibag278imi1l4czpqagf97lvzbjgsa3mj17vax08vyd")))

