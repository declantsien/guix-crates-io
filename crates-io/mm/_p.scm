(define-module (crates-io mm _p) #:use-module (crates-io))

(define-public crate-mm_publish-0.1 (crate (name "mm_publish") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "minimonkey") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1c3dhik4brn89nycdm6a6xakfhii4iacg1n9dp9dzx235ydqrlq3")))

