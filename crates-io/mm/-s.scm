(define-module (crates-io mm -s) #:use-module (crates-io))

(define-public crate-mm-std-embedded-nal-0.3 (crate (name "mm-std-embedded-nal") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-nal") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "embedded-nal-tcpextensions") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8") (features (quote ("os-ext"))) (default-features #t) (kind 2)))) (hash "1nnrfaws7vrpgsf9q03cgrrmv1pybphanigpl90idv8nchznpk9x")))

