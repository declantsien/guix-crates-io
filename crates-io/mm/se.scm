(define-module (crates-io mm se) #:use-module (crates-io))

(define-public crate-mmseg-0.0.1 (crate (name "mmseg") (vers "0.0.1") (deps (list (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "0ggqp7jh5qzzym5nzpfvln125va5qxr9cwakvgghikswab4ax52g") (features (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.1 (crate (name "mmseg") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "0xv02adgfc5kfpy900g186driyvdrkgf485402ahl2p5lbwwzwhg") (features (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.1 (crate (name "mmseg") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rq5zsjjiydpj9zjmxkp13jd76rjxg3jkrik0456xhp1981xgz95") (features (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.2 (crate (name "mmseg") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5") (default-features #t) (kind 0)))) (hash "1v0nnbchlf6iqn4x68j34r92nargmp6qx4vrv10820yihzkbhskw") (features (quote (("embed-dict") ("default" "embed-dict"))))))

(define-public crate-mmseg-0.3 (crate (name "mmseg") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)))) (hash "142jkq47la69yc4snahi5jf3px5jydgrrd3aj5hvvjrxqwa863wv") (features (quote (("embed-dict") ("default" "embed-dict"))))))

