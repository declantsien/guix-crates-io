(define-module (crates-io mm i-) #:use-module (crates-io))

(define-public crate-mmi-code-0.1 (crate (name "mmi-code") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0i88445szyvwqjax9v2ay3qqksv85c7ynyx0qhnjd6s5g5b2gvp3")))

(define-public crate-mmi-code-0.1 (crate (name "mmi-code") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pbj44ipdjbz9xvif96iryzl60gj80bw8zc1j2kn7jdbz0acxhjg")))

(define-public crate-mmi-code-0.2 (crate (name "mmi-code") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0b37a4myr1aw18lr22xnifiwzj4rzs2phrkbqp4pxck80msrmn89")))

(define-public crate-mmi-parser-0.1 (crate (name "mmi-parser") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10phillxcazdf3ki32hx6p5fml1sjhcc602a423p8y8qp06nl0jz") (features (quote (("dumb_terminal" "colored/no-color"))))))

(define-public crate-mmi-parser-0.1 (crate (name "mmi-parser") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "018wacg9jza1j3vcpp0kxgz71pwgp01lcgpz59sxq8699rczdj6v") (features (quote (("dumb_terminal" "colored/no-color"))))))

(define-public crate-mmi-parser-1 (crate (name "mmi-parser") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xigkv0h5cymygvfhjqrjs6ifk1lkml4rggwhshlp4qz5a6wli6x")))

(define-public crate-mmi-parser-1 (crate (name "mmi-parser") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fgpk6fav4kx2d5jvi5y2i1fry0rcc09r758x1352b7x7c9sjggm")))

(define-public crate-mmi-parser-1 (crate (name "mmi-parser") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dmhqqmryw3ryw5v9vv4xxgbfl4h7r2ywihgnf7r9wnc0l2cc7qq")))

(define-public crate-mmi-parser-2 (crate (name "mmi-parser") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1sj71av66gbs9lz41jlxwz474v5nym65gsdrb8m9mafhddqgzka9")))

