(define-module (crates-io mm io) #:use-module (crates-io))

(define-public crate-mmio-0.0.0 (crate (name "mmio") (vers "0.0.0") (hash "14vg4j3hgbj594lfi94snvr8jjl6cnmqagn57br755zy40j9rpcj")))

(define-public crate-mmio-0.1 (crate (name "mmio") (vers "0.1.0") (hash "01z6r6dyfkvh5zfy5bdgr860zqs8077ybdygc2xhyzna9jb6jk73")))

(define-public crate-mmio-1 (crate (name "mmio") (vers "1.0.0") (hash "11vhp4gbmhaacgqj1qfib7f8llyibidyx263avkqj7w98zcqx4xf")))

(define-public crate-mmio-1 (crate (name "mmio") (vers "1.0.1") (deps (list (crate-dep (name "array-macro") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "09lvjjp2z61yzrcd5qklrvhd0k45pxyv9pqjrfyq9jnjpmmkm1h3")))

(define-public crate-mmio-2 (crate (name "mmio") (vers "2.0.0") (hash "08gskwdy6777q8wr7sfkx161bg17k79j4g9b80sai3m2b7f6gc37")))

(define-public crate-mmio-2 (crate (name "mmio") (vers "2.0.1") (hash "06nwypp840aqw5igl39s1yn6psjs6wp47qg7qkphswc95wl5v1aq")))

(define-public crate-mmio-2 (crate (name "mmio") (vers "2.1.0") (hash "022l5xkq3d5v6ryy8l6fl9wj2aki9fyyg3np0wslyf9p1gypp1gf")))

