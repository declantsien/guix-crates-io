(define-module (crates-io mm s-) #:use-module (crates-io))

(define-public crate-mms-rs-1 (crate (name "mms-rs") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1falzz5zm94k1x3pf8k09dr6xz31i536jmp7nd8hnkzglpijgfa8") (features (quote (("use_results") ("default" "use_results"))))))

(define-public crate-mms-rs-2 (crate (name "mms-rs") (vers "2.0.0") (deps (list (crate-dep (name "cbindgen") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "cbindgen") (req "^0.24.5") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "csbindgen") (req "^1.7.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "04niqnxx0126gkx3dyzmiakxrm34chfj6qb0z0088rdsxf6p93vy") (features (quote (("use_panics") ("dotnet" "c_api_internal" "csbindgen") ("default" "c_api" "cxx_api") ("cxx_api" "cpp_api") ("cpp_api" "c_api_internal" "cbindgen") ("c_api_internal" "use_panics") ("c_api" "c_api_internal" "cbindgen"))))))

