(define-module (crates-io mm ox) #:use-module (crates-io))

(define-public crate-mmoxi-0.1 (crate (name "mmoxi") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo" "wrap_help"))) (default-features #t) (kind 0)))) (hash "1yr77axg6j9m1ap62r9j3n9vda22lq0ccya8s83zczm7hf78pf9v")))

(define-public crate-mmoxi-0.1 (crate (name "mmoxi") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo" "wrap_help"))) (default-features #t) (kind 0)))) (hash "0d94n483bydapd7s55jbz30yqpl31yksczpqlwagkg0dx541fclf")))

(define-public crate-mmoxi-0.1 (crate (name "mmoxi") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo" "wrap_help"))) (default-features #t) (kind 0)))) (hash "0i186g8p1xr7vqhsval00dcg1f8g6cpibq7j1yg0d8sn4cgfsi40")))

(define-public crate-mmoxi-0.2 (crate (name "mmoxi") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("cargo" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0vbh86jlv5yvw6kkqnsk1ndrmxwxrch2xyi6gkqaxdfrw70x0dg8")))

(define-public crate-mmoxi-0.2 (crate (name "mmoxi") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("cargo" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1jshbyfc78s21wyxlljsqnb8nffjwdkxq0wk7ginrqdix57yjij8")))

(define-public crate-mmoxi-0.2 (crate (name "mmoxi") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("cargo" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "1lydr5l59ldc66f6bzgq86nyfrz1xm8p0lgdl7pibb1s2li7qgcy") (rust-version "1.70")))

