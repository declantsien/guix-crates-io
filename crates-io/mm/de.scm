(define-module (crates-io mm de) #:use-module (crates-io))

(define-public crate-mmdeploy-0.2 (crate (name "mmdeploy") (vers "0.2.1") (deps (list (crate-dep (name "mmdeploy-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)))) (hash "14jawdd9dvhj18l6py4ylxqd6l78wwj7pw4qy4amv3h6ap92byfc")))

(define-public crate-mmdeploy-0.3 (crate (name "mmdeploy") (vers "0.3.0") (deps (list (crate-dep (name "mmdeploy-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)))) (hash "0432876dqd2mnygwx3bl7v3b3pzzb6b2wlr2276q3bpnh2dbg0bs")))

(define-public crate-mmdeploy-0.4 (crate (name "mmdeploy") (vers "0.4.0") (deps (list (crate-dep (name "mmdeploy-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "04fv4xlww9az58bkam2gdz6gf79nbmkcbpgdsz9n5h7y84g183a9")))

(define-public crate-mmdeploy-0.5 (crate (name "mmdeploy") (vers "0.5.0") (deps (list (crate-dep (name "mmdeploy-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0yq1h0hhgqjim71s83zisky3aqk2l4v4n49a0cpf7vayjssnw1m9")))

(define-public crate-mmdeploy-0.6 (crate (name "mmdeploy") (vers "0.6.0") (deps (list (crate-dep (name "mmdeploy-sys") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0pp07llmzgcaw2y6hazxscq0hwdaxnd73l2y84g3m18skfhvzyjw")))

(define-public crate-mmdeploy-0.7 (crate (name "mmdeploy") (vers "0.7.0") (deps (list (crate-dep (name "mmdeploy-sys") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02g6f3mrzxbrk7sn6l3chx3s9px0pcgw2hhqdk42ih138gd7nlgp")))

(define-public crate-mmdeploy-0.8 (crate (name "mmdeploy") (vers "0.8.0") (deps (list (crate-dep (name "mmdeploy-sys") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1y2pl09hgq15jvwycgcsx409zd5m62dfh1zr7vaxscplz6ylilyr")))

(define-public crate-mmdeploy-0.8 (crate (name "mmdeploy") (vers "0.8.2") (deps (list (crate-dep (name "mmdeploy-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "07p0s2wa9ankzkd0ddbzanx4wmjsmb23s603fd0266xs4iwfh2kk")))

(define-public crate-mmdeploy-0.9 (crate (name "mmdeploy") (vers "0.9.0") (deps (list (crate-dep (name "mmdeploy-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.68") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "07ngf2nya0m9ypnd8xqgbkc4l01b62fyr327lmi51y4l2379n4bs")))

(define-public crate-mmdeploy-sys-0.1 (crate (name "mmdeploy-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1v0lz7505bcl1gbsf887pqbbdk7vhh0lw2kvbnlrz2wlcfqnjyhn")))

(define-public crate-mmdeploy-sys-0.2 (crate (name "mmdeploy-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "10026wr48njkbagfml3k5l8sd8ax7ldzx0m4qdl1kqadzgrwcf5s") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.3 (crate (name "mmdeploy-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1yz5dchlxwfv23cch0bafm2zxwdcqiizfklnsyxi5svklziq4a76") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.3 (crate (name "mmdeploy-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "182x031bi7lyhn3n7r6av44gfpki4bqmkw072zj1xb9yhy67crqg") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.4 (crate (name "mmdeploy-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0r470bs3cpl3xdsgd66d67ba900vb2j3ps3b7cka6s87k69nv998") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.5 (crate (name "mmdeploy-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "194ypghfmay0hpa74nax428jsz35jr8v9nhp75k9jw41qk3m3pys") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.6 (crate (name "mmdeploy-sys") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1cx5y5di87qg8rab3xh57sd0bbwz1hb2sjpanjb9flz5y1dp5whp") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.7 (crate (name "mmdeploy-sys") (vers "0.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1hf13h48lswm4wkcfj604c29kgp957pq3mz7bx7gq2l5w4asj5db") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.8 (crate (name "mmdeploy-sys") (vers "0.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0gvrdp99snk5i6864wjmpfy9pwkn8wrnmgp7pk6chn3sj3vn7pad") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.8 (crate (name "mmdeploy-sys") (vers "0.8.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1rdcic5ixwml0ddlvrj391k911bm7aq4rgn5qa2cnr1ngkbqw9p9") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.9 (crate (name "mmdeploy-sys") (vers "0.9.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "006bz2ws451smzcv7y8kdr3a0qhxbcp08fq5k62kkwfazy1y85z2") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-0.9 (crate (name "mmdeploy-sys") (vers "0.9.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0xhh2g6sd4cc45hwp0966sdyxvv2jgaizvv0d8r89w509iaa72cp") (features (quote (("static") ("build" "static"))))))

(define-public crate-mmdeploy-sys-1 (crate (name "mmdeploy-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0srhs9azbixb6343xsx94363wx164xdz2r7fzrxdb246xc1m3a6d") (features (quote (("static") ("build" "static"))))))

