(define-module (crates-io mm a7) #:use-module (crates-io))

(define-public crate-mma7660fc-0.1 (crate (name "mma7660fc") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0nachy63xbgp072yj9n5a1x8rsw1grxnk3fc1wd4sld6sc276svz")))

(define-public crate-mma7660fc-0.1 (crate (name "mma7660fc") (vers "0.1.1") (deps (list (crate-dep (name "cast") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "067g9kpz0w8gqpg3g3y4x45ym201d58v4nddkbk76vhf409zsxkn")))

(define-public crate-mma7660fc-0.1 (crate (name "mma7660fc") (vers "0.1.2") (deps (list (crate-dep (name "cast") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1v3m95w1wciyiy4h1pm7yrwwgdjylpdizmyhhp1km4asw36811wc")))

