(define-module (crates-io mm he) #:use-module (crates-io))

(define-public crate-mmheaps-0.1 (crate (name "mmheaps") (vers "0.1.0") (hash "0srpljyy10aa17r9x8g9wc6qg3jmlf7i00z9ib2y43wigx3blwmf") (yanked #t)))

(define-public crate-mmheaps-0.1 (crate (name "mmheaps") (vers "0.1.1") (hash "0k1gx7zzihw7pb2d16na9biqdbl5kzvvni67yl9cha7lcv3cny9s") (yanked #t)))

(define-public crate-mmheaps-0.1 (crate (name "mmheaps") (vers "0.1.11") (hash "1601f71s6rf3d2c13d0a1lvddjazijy00a3d37j9hmbk662xkihq")))

