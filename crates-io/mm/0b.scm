(define-module (crates-io mm #{0b}#) #:use-module (crates-io))

(define-public crate-mm0b_parser-0.1 (crate (name "mm0b_parser") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "mm0_util") (req "^0.1.1") (kind 0)) (crate-dep (name "zerocopy") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fnfn8wvnhyfhcbvmf7hh6f95nihk9r19swwf5cz7na1gm01nayh")))

(define-public crate-mm0b_parser-0.1 (crate (name "mm0b_parser") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "mm0_util") (req "^0.1.1") (kind 0)) (crate-dep (name "zerocopy") (req "^0.5") (default-features #t) (kind 0)))) (hash "03b86626i1ydfgykah3r6lndm54aqkb2w7m81myy50p5i6dq0ya1")))

(define-public crate-mm0b_parser-0.1 (crate (name "mm0b_parser") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "mm0_util") (req "^0.1.4") (kind 0)) (crate-dep (name "zerocopy") (req "^0.5") (default-features #t) (kind 0)))) (hash "14mv6lxdv2w6a6kh7c53wls5g7jk20kx5pap7m6909ydazw3v5v6")))

(define-public crate-mm0b_parser-0.1 (crate (name "mm0b_parser") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "mm0_util") (req "^0.1.4") (kind 0)) (crate-dep (name "zerocopy") (req "^0.5") (default-features #t) (kind 0)))) (hash "0svn62syyq7icl0cidygbqck9c7c8j6z3n209sa4bl4k1yc53z1v")))

(define-public crate-mm0b_parser-0.1 (crate (name "mm0b_parser") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "mm0_util") (req "^0.1.4") (kind 0)) (crate-dep (name "zerocopy") (req "^0.5") (default-features #t) (kind 0)))) (hash "1db5v8zif3kmd8afz18i50snxap47a3wfwhy4g2sv8jpfqw2g0wh")))

