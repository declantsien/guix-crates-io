(define-module (crates-io mm _s) #:use-module (crates-io))

(define-public crate-mm_subscribe-0.1 (crate (name "mm_subscribe") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "minimonkey") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11a4dvsqchjm9xfm4jsm7bj7dbcp3wyb4z6dj2l30dvz0k4chimd")))

