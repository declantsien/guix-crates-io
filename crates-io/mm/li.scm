(define-module (crates-io mm li) #:use-module (crates-io))

(define-public crate-mmlib-0.1 (crate (name "mmlib") (vers "0.1.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)))) (hash "16k3jhag09j93amfmixgwicss1cj8zakij2d560dnf1az475xngg")))

(define-public crate-mmlib-0.1 (crate (name "mmlib") (vers "0.1.1") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)))) (hash "0dgmrnnl0chflply1jmh3xg8nri2k61md6v942lbk4qx27lrc9mv")))

(define-public crate-mmlib-0.1 (crate (name "mmlib") (vers "0.1.2") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)))) (hash "1nw5072psb7c1asapn89zz8h2abv0jzn5l97yc549df7879i0z95")))

