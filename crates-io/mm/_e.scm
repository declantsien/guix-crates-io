(define-module (crates-io mm _e) #:use-module (crates-io))

(define-public crate-mm_example_crate-0.1 (crate (name "mm_example_crate") (vers "0.1.1") (deps (list (crate-dep (name "macro_magic") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1czsiwb8y3kk4vmna050rrivhzsyj20lch8q00izix1kgi6hqgb9") (features (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate-0.1 (crate (name "mm_example_crate") (vers "0.1.3") (deps (list (crate-dep (name "macro_magic") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1x6fgi3gmcjkxcbfbq2nw9a2kmrzgh2x2gd01vpwvmvpj3i4v5wi") (features (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate-0.1 (crate (name "mm_example_crate") (vers "0.1.4") (deps (list (crate-dep (name "macro_magic") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0byw22bmnyfdp5za4mxhkcbknzfpwgnmblhaw52178yd1nybnrdy") (features (quote (("indirect-write" "macro_magic/indirect-write") ("indirect-read" "macro_magic/indirect-read") ("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate2-0.1 (crate (name "mm_example_crate2") (vers "0.1.1") (deps (list (crate-dep (name "macro_magic") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "085rkdwrzhy83b9krflb35r9p5bihhy1j13klfvvfvzx6kc66g66") (features (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate2-0.1 (crate (name "mm_example_crate2") (vers "0.1.3") (deps (list (crate-dep (name "macro_magic") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0k3135g8hqradn35ws2cx7fc4gnv6g7s0wh6hyna7fpiwmhhrn7x") (features (quote (("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_crate2-0.1 (crate (name "mm_example_crate2") (vers "0.1.4") (deps (list (crate-dep (name "macro_magic") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "004kldb52vbg57brsgfjyarj9mzn56h541l5ip3mynxvab8dh0gp") (features (quote (("indirect-write" "macro_magic/indirect-write") ("indirect-read" "macro_magic/indirect-read") ("indirect" "macro_magic/indirect") ("default"))))))

(define-public crate-mm_example_proc_macro-0.1 (crate (name "mm_example_proc_macro") (vers "0.1.1") (deps (list (crate-dep (name "macro_magic") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mm_example_crate") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "14qh4fhk5gnh3scd5slv2g47dx2rysxfk2h22k3dr1bbrfdj4ywv") (features (quote (("indirect" "macro_magic/indirect" "mm_example_crate/indirect") ("default"))))))

(define-public crate-mm_example_proc_macro-0.1 (crate (name "mm_example_proc_macro") (vers "0.1.3") (deps (list (crate-dep (name "macro_magic") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "mm_example_crate") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1a2kkg9cq1k7168k565dywldgmwqvqwbzhyr28i15a1lilzlg583") (features (quote (("indirect" "macro_magic/indirect" "mm_example_crate/indirect") ("default"))))))

(define-public crate-mm_example_proc_macro-0.1 (crate (name "mm_example_proc_macro") (vers "0.1.4") (deps (list (crate-dep (name "macro_magic") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "mm_example_crate") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "06qx8gwckaia74fp8rcbx2hxbp5rliiwcviqghqjqdg5q0kyl933") (features (quote (("indirect-write" "macro_magic/indirect-write") ("indirect-read" "macro_magic/indirect-read") ("indirect" "macro_magic/indirect" "mm_example_crate/indirect") ("default"))))))

