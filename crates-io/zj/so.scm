(define-module (crates-io zj so) #:use-module (crates-io))

(define-public crate-zjson-0.1 (crate (name "zjson") (vers "0.1.0") (hash "035rx4fp9v7x9y538qnsb5vb5cjshfl877n7n6bj6ksiq0jjparj") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1 (crate (name "zjson") (vers "0.1.1") (hash "1ipbq9k24zwzr44f4bviw43l6jfw6cgq9lql5qsx1ginsfrfb79r") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1 (crate (name "zjson") (vers "0.1.2") (hash "0mzchvsfqnk0hsj59kgf9n1r81sirgg7xs51190y8k2jil40jl7k") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1 (crate (name "zjson") (vers "0.1.3") (hash "0yhf7b3h7490q9ixl4fdhhdqaf03yak1d7fsnlycb21sq4g6cy8g") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1 (crate (name "zjson") (vers "0.1.4") (hash "0sdnk90gxg93j49nq8xgi39vn5lsn07b6w4bnrmbn390myp4dnw0") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-zjson-0.1 (crate (name "zjson") (vers "0.1.5") (hash "11i9xxy9fdz2aqwrps4s656jrdiz60xzcmk601rzk0ga6x5p7ggv") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

