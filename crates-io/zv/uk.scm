(define-module (crates-io zv uk) #:use-module (crates-io))

(define-public crate-zvuk-0.1 (crate (name "zvuk") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17") (default-features #t) (kind 0)))) (hash "1mnr0slcb7ri6b54wns1rh4y302lfl7f4kp6r8ylz54ji1i7y4pa")))

