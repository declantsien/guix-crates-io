(define-module (crates-io zv t_) #:use-module (crates-io))

(define-public crate-zvt_builder-0.1 (crate (name "zvt_builder") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "yore") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "zvt_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17q5r4p8xq6pxfic8r9vgk5h8j5sc3wrvsqlw0njyhs2hi8lwid4")))

(define-public crate-zvt_derive-0.1 (crate (name "zvt_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "08zh3z948fh4gaq4742by1pvd05klm0bflniyknky639f7qjkdna")))

