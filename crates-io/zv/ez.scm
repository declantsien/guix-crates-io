(define-module (crates-io zv ez) #:use-module (crates-io))

(define-public crate-zvezda-0.1 (crate (name "zvezda") (vers "0.1.1") (hash "0q48sblnrinqngs4d334ckngndp8qnrnzl29l93lffix0bc8b44y")))

(define-public crate-zvezda-0.1 (crate (name "zvezda") (vers "0.1.2") (hash "0qjr14w6pfsj5aq710g5hxwrgzsr58gq21ngpgh44jcn2hlkgphq")))

(define-public crate-zvezda-0.1 (crate (name "zvezda") (vers "0.1.3") (hash "03q6aa6dhfvwx4yv34bjvxymyv7ymwsl2hhb91p4p7l6sim30mj3")))

(define-public crate-zvezda-0.1 (crate (name "zvezda") (vers "0.1.4") (hash "0pfdm82yiigwqzma10yp915va5x3qyh6cqmnk1lyp9ih6x8pi1yk")))

