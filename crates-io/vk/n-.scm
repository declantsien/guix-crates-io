(define-module (crates-io vk n-) #:use-module (crates-io))

(define-public crate-vkn-raw-0.0.1 (crate (name "vkn-raw") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0w33445y3bj6dian2d7lwdv9cgvgn9skms41bmn384qxyp6hdwi6")))

