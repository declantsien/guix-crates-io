(define-module (crates-io vk _a) #:use-module (crates-io))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.0") (hash "1aqra4g5n29k7wmpiq4d4s6rlryjy2ym6fwm7a1fmzla65jj7l3n")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.6") (hash "1w2hmxhm52933z88apprhsxfmlgka9qr9kg53ap46wd9w9i9hadp")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.7") (hash "1l99hx65k887jdxif0w1y7cdi71k009vff86nz8gz9zkypasss00")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.8") (hash "15sy7mnng4ls224gjpajc7bs8kqzphr5rqblp1y4ylnwgva44r4k")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.9") (hash "1njwmmxi50bz1bcpcg01w1qm3a7s875xynqndgsnzjad6k7plhsy")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.10") (hash "17yafgms0vma4gz0q0m7hg0jyg45wqmbf8pn0yykb2ssy4j8mjb0")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.11") (hash "0jxhvvhy1ggzvfx7xvlcgxkj4d02ica4r323gq0z3hr93zill50r")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.12") (hash "1f4yl9isq7ifd42hpryb6gb12p348x5baln11l7qpdd1rp8xnp00")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.13") (hash "1185jm8hp9jp2fc4nnr2gzjv42plbgp25blxylcbaj2h078xgk6a")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.14") (hash "0pprx21xzj7fk4vrrkljsbmygir38p9bfi3q9dj2b7wr0grwfnw6")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.19") (hash "06ix5dvgnsw6655qrqlgpwl2rmdrichhc7vwdvqbd4yblsa24q8s")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.22") (hash "079fyvs72ssq9a49flhx2xr6rcmir6qvfpyib5wfljdf8hmg061i")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.27") (hash "15q09l9zxa07h8rxsj5vjrxzvydakanrxki5mpmncvpiwad6ifn1")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.29") (hash "02gmynb59dr05yarl99s7q59j5kpnv7yqrvd2gawibj0wir6fnf0")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.36") (hash "0s1jkn26j3n1nni9zwc6m6p1w7hixjjlklaix4zqf9qm1sc3cxs0")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.42") (hash "0zgf06fq8gb8pnhiki30cmfc1s086ilsk5hc1ckzwcyng2f4cf78")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.64") (hash "13h4h3rihf0jc6sfm38vz81kwjhax0g8znwd4fcq8jpcy80x0vkk")))

(define-public crate-vk_api-1 (crate (name "vk_api") (vers "1.0.69") (hash "1q7n7fb0wwazvvxqjaqgmkklfhfq2cm2dsc3qzz0w9jpbhgxsh1s")))

(define-public crate-vk_api_21343254-0.1 (crate (name "vk_api_21343254") (vers "0.1.0") (hash "12w2vidcqrpzn1rcp0g1my4v06dqr6zwjr648p23k7dm297ddkgv") (yanked #t)))

