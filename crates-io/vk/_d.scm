(define-module (crates-io vk _d) #:use-module (crates-io))

(define-public crate-vk_deps-0.1 (crate (name "vk_deps") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0i5yq7w502s6fyg1d58qv2gwnaavq0p66scr3hpclbnl7wqyrm6r")))

(define-public crate-vk_dot_xml_parser-0.1 (crate (name "vk_dot_xml_parser") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.4") (kind 0)))) (hash "02v98dpyvjkvr2rii6z900hdsi21rnaw9jamxmy977y4yclrz709")))

(define-public crate-vk_dot_xml_parser-0.2 (crate (name "vk_dot_xml_parser") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.4") (kind 0)))) (hash "0zh920ahq7qxgvbpj8mpah4kq4vfps5073wchi0jfm8w0m8pwlml")))

(define-public crate-vk_dot_xml_parser-0.2 (crate (name "vk_dot_xml_parser") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.4") (kind 2)))) (hash "00jrzrfyiprdw56gx3sbrkmm78bsfwjgllpgi92g5m8zgdnjr431")))

(define-public crate-vk_dot_xml_parser-0.2 (crate (name "vk_dot_xml_parser") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.4") (kind 2)))) (hash "1qja834zhfjsb7pch93skmxj11vbgzqhwiwkcb6s50gankq91l7r") (yanked #t)))

(define-public crate-vk_dot_xml_parser-0.3 (crate (name "vk_dot_xml_parser") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.4") (kind 2)))) (hash "10gpgli8485584cjscqd0r8s3p1hfvwl8146gj0k4224v2smbj6s")))

