(define-module (crates-io vk -k) #:use-module (crates-io))

(define-public crate-vk-keyboard-0.1 (crate (name "vk-keyboard") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bspmzb7yjw2sqydgnaj52663xk7gvk827vbkr4h6m74sggxn8q8")))

(define-public crate-vk-keyboard-0.2 (crate (name "vk-keyboard") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "00lx2c07ij77giy98wx9rjv3j1zqipivdjpzp1lin8bj9y1zap57")))

