(define-module (crates-io vk op) #:use-module (crates-io))

(define-public crate-vkopt-message-parser-0.1 (crate (name "vkopt-message-parser") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "07b4nivig2cvq6f27spvb0drq2v4ckyl0bydyzgv28zhyh2ysa3y")))

(define-public crate-vkopt-message-parser-0.3 (crate (name "vkopt-message-parser") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1313yn6rchx3s6zskhysg3f4zdx4mc6smgvywhpfmgiac9z7wby2")))

