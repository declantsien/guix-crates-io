(define-module (crates-io vk _l) #:use-module (crates-io))

(define-public crate-vk_llw-0.0.1 (crate (name "vk_llw") (vers "0.0.1") (deps (list (crate-dep (name "ash") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0hkhpqvckf19k4q2qlxjwqxxwpfyiqch28cj88xcvv5grn1w0793")))

