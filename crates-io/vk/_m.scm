(define-module (crates-io vk _m) #:use-module (crates-io))

(define-public crate-vk_method-0.1 (crate (name "vk_method") (vers "0.1.1") (deps (list (crate-dep (name "ijson") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "14n30fhpfqjyrw4p0dsc2zagbirh5mry42z1yw2lzjmb6x2hd73i")))

(define-public crate-vk_method-0.1 (crate (name "vk_method") (vers "0.1.2") (deps (list (crate-dep (name "ijson") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zl831ybrf78v0n8hij74l64r8yjvicyvv2h1zrmfwjh52jhyk92")))

(define-public crate-vk_method-0.1 (crate (name "vk_method") (vers "0.1.3") (deps (list (crate-dep (name "ijson") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "007cgs4x2839npwk68ii2308s6n1bzcsg1h37rarbnippmwy3wqm")))

(define-public crate-vk_method-0.1 (crate (name "vk_method") (vers "0.1.4") (deps (list (crate-dep (name "ijson") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10q4s9n49niwxfsvnvspd9rfk2pj6bqwcifnfxldcwicr6xn1ah5")))

(define-public crate-vk_method-0.1 (crate (name "vk_method") (vers "0.1.5") (deps (list (crate-dep (name "ijson") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "039nlv189gbnnmnsvxwviwvzsik0aksy911wbysm1hrz1xnllck6")))

(define-public crate-vk_method-0.2 (crate (name "vk_method") (vers "0.2.0") (deps (list (crate-dep (name "ijson") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fnz0mwlvgba37hpn70nbxlyl6ik0r61nfxncx2cssvz948alada")))

(define-public crate-vk_method-0.2 (crate (name "vk_method") (vers "0.2.1") (deps (list (crate-dep (name "ijson") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w3vbm0w41h6ca4k57s7aw8sv4wrgfzy67fwx3v4jbc5yxdfahk3")))

