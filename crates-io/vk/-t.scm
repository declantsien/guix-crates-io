(define-module (crates-io vk -t) #:use-module (crates-io))

(define-public crate-vk-token-manager-0.1 (crate (name "vk-token-manager") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "1g1rp9w7amg35khfqsaic98c8wgl7fnppjairqjcsy5c5f8r2cfn")))

(define-public crate-vk-token-manager-0.2 (crate (name "vk-token-manager") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "09jvkr05ixdkf6dvc734951gvyqxiwgdf2mixp2z155qp4xcacy9")))

