(define-module (crates-io vk xm) #:use-module (crates-io))

(define-public crate-vkxml-0.3 (crate (name "vkxml") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "1qhvx77bi3gj9wls4167w0pfbbrkr5yysc1jd5dymraqngga1wy7")))

(define-public crate-vkxml-0.3 (crate (name "vkxml") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "1fnjw3srqzkyfhs8p82s8xnww6psxsykdsrspqgmvjn04g1gig8z")))

