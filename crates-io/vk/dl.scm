(define-module (crates-io vk dl) #:use-module (crates-io))

(define-public crate-vkdl-0.1 (crate (name "vkdl") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0392baynknb9lm69dnd5xz070cwfii8v7kslngli1m52ha6466jy") (yanked #t)))

(define-public crate-vkdl-0.1 (crate (name "vkdl") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07g76f6qld1fb9piggdfx19a73phzhw1vgqf8byggrqacd4wxywx") (yanked #t)))

(define-public crate-vkdl-0.1 (crate (name "vkdl") (vers "0.1.2") (deps (list (crate-dep (name "futures-util") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01115is2q6zgjdvpmzvw9br3n4fscvxqqmsnfnzm19431zp5ylyf")))

