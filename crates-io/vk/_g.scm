(define-module (crates-io vk _g) #:use-module (crates-io))

(define-public crate-vk_generator-0.0.1 (crate (name "vk_generator") (vers "0.0.1") (hash "0ag9ic2zznpprkfkpa23b8bgsfghqrr4j1iviylbj7vc9qs2zzql") (yanked #t)))

(define-public crate-vk_generator-0.1 (crate (name "vk_generator") (vers "0.1.0") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1gzg5mxmidzqc8y0c4diips21b9wnqw5a7widc0dhya6yz4n5baa") (features (quote (("unstable_generator_api")))) (yanked #t)))

(define-public crate-vk_generator-0.1 (crate (name "vk_generator") (vers "0.1.1") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "04x79qr1xpmf6fzp3h7w6i1vr2za2s5c0yia3jrv9f7pi10m9aba") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.1 (crate (name "vk_generator") (vers "0.1.2") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "11iw9lxhlqiqkmdz1638642n2xcbb55l3mzqcz590injx9y7har1") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.1 (crate (name "vk_generator") (vers "0.1.3") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "132kj0mmy9r2rh24yxib1cpmbzsgyxanqcca7ipdczkg2kh8mg8k") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.2 (crate (name "vk_generator") (vers "0.2.0") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0nxlr00kx3979cbg0cwx9bxdlvvg20vpi6dglrrfzczc1r9q1ff7") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.2 (crate (name "vk_generator") (vers "0.2.1") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0bcw5dl9pm9vxvik0q2jhw0hnvcnfs6j8fgc5yiklmq3rf0jkz9i") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.2 (crate (name "vk_generator") (vers "0.2.2") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0g5x6zvbxnl3l472rb5a0sc64axcmnxm2h5j3y5yl60ax6aj9l4y") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.3 (crate (name "vk_generator") (vers "0.3.0") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1n7fqvkcszhj1qa19dsinigq1q4phcs3cjlf21mjdrqck8nbr7w9") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.3 (crate (name "vk_generator") (vers "0.3.1") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0yz4r3m07rl0pf90gsf90n20y8lp3j3x8vbakili177j3p5isi1x") (features (quote (("unstable_generator_api"))))))

(define-public crate-vk_generator-0.3 (crate (name "vk_generator") (vers "0.3.2") (deps (list (crate-dep (name "boolinator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vk_api") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0xhxi6vgd0w0zv7ncz8p285rrv2b841s7fz28v3bf2grssgmzld1") (features (quote (("unstable_generator_api"))))))

