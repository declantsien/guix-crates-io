(define-module (crates-io vk fe) #:use-module (crates-io))

(define-public crate-vkfetch-rs-0.0.1 (crate (name "vkfetch-rs") (vers "0.0.1") (deps (list (crate-dep (name "ash") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0f5s9b3jwd7n01bg8vhmiq82c0q1n4zv9vjp46gssx28rvqraa60")))

(define-public crate-vkfetch-rs-0.0.2 (crate (name "vkfetch-rs") (vers "0.0.2") (deps (list (crate-dep (name "ash") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0fn98inyzw15sds9ip8qjz21k4rjm0w1l2idish14wjh4l8nycp3")))

(define-public crate-vkfetch-rs-0.0.3 (crate (name "vkfetch-rs") (vers "0.0.3") (deps (list (crate-dep (name "ash") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0i9vk8b760b5an7hmqaq1l61x2qs212nnr6rfafn772y6l92grqk")))

(define-public crate-vkfetch-rs-0.0.4 (crate (name "vkfetch-rs") (vers "0.0.4") (deps (list (crate-dep (name "ash") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "1s8m4wjkpslrjw425qpswxz54hniwcbz9m1bx1np8py3mjzfmbba")))

(define-public crate-vkfetch-rs-0.0.5 (crate (name "vkfetch-rs") (vers "0.0.5") (deps (list (crate-dep (name "ash") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "0zf8bhk1ibpsnw5irxcq6spqqnzd2ws6908lib6pn81qwxxyr9rl")))

(define-public crate-vkfetch-rs-0.0.6 (crate (name "vkfetch-rs") (vers "0.0.6") (deps (list (crate-dep (name "ash") (req "^0.38.0") (features (quote ("std"))) (kind 0)))) (hash "0ig5cvb6v2r0iavv5q9vacl1jpg4kv6s54j0iinwgcg8n1xgky3c") (features (quote (("loaded" "ash/loaded") ("linked" "ash/linked") ("default" "linked"))))))

(define-public crate-vkfetch-rs-0.1 (crate (name "vkfetch-rs") (vers "0.1.0") (deps (list (crate-dep (name "ash") (req "^0.38.0") (features (quote ("std"))) (kind 0)))) (hash "0b82vq8afyc87lc5jahvaadz5cy3lrdxjqqw1zjrilizrx9wcgdi") (features (quote (("loaded" "ash/loaded") ("linked" "ash/linked") ("default" "linked"))))))

