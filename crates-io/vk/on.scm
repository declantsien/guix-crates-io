(define-module (crates-io vk on) #:use-module (crates-io))

(define-public crate-vkontakte_api-0.0.1 (crate (name "vkontakte_api") (vers "0.0.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1p9j1iwb2y1kly0jfckg46ak14r3nnq0dv5bxnddnnz94p0v1pgc")))

(define-public crate-vkontakte_api-0.0.2 (crate (name "vkontakte_api") (vers "0.0.2") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0grmkkfbqv88s350qxwpsizn91l9d1xy4mdp18cv2dn1ms0k256m")))

(define-public crate-vkontakte_api-0.0.3 (crate (name "vkontakte_api") (vers "0.0.3") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "19kbbrcds5p7bkv6dbq0szswmw70s966ac8fa36sgvw0fnndb725")))

(define-public crate-vkontakte_api-0.0.4 (crate (name "vkontakte_api") (vers "0.0.4") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1rkzlpjc1fmnlvygpv7b8nxajyf3xb9nd0abm7mnw1srckl8b98w")))

(define-public crate-vkontakte_api-0.0.5 (crate (name "vkontakte_api") (vers "0.0.5") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1j88rj126kinlccbj8lvn5rphz8vq4hlpy0hfgvrvwby4n1arn1z")))

(define-public crate-vkontakte_api-0.0.6 (crate (name "vkontakte_api") (vers "0.0.6") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0f39n0lg36k6dbamn5595x2a96qhgh2ypr2m4zzq7np20g7mnmw6")))

