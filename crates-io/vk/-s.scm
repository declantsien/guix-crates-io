(define-module (crates-io vk -s) #:use-module (crates-io))

(define-public crate-vk-shader-macros-0.1 (crate (name "vk-shader-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.1.0") (kind 0)))) (hash "1anla9i2srsqr5whzrajf08mfb3rygrn0jlc9haj25vspaady10d") (features (quote (("default" "build-native-shaderc") ("build-native-shaderc" "vk-shader-macros-impl/build-native-shaderc"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1va145dw56p9ky4prighjrpri16a4jpw8z0gi8ay867giyp6yv2g") (features (quote (("strip" "vk-shader-macros-impl/strip"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0q4hwgg5ma913qgq99yy3z6g9b44503c4zyd3242c54rz825yx5v") (features (quote (("strip" "vk-shader-macros-impl/strip"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1ld4f29r7k09n7hmv8gccpmwhflpgd80f59yk1fr948rfhqn6955") (features (quote (("strip" "vk-shader-macros-impl/strip") ("build-from-source" "vk-shader-macros-impl/build-from-source"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "170g4awd2p5q8wfl3njzrn8sydaf7aarxw5h0d5js3j6iw94njyk") (features (quote (("strip" "vk-shader-macros-impl/strip") ("default-optimize-zero" "vk-shader-macros-impl/default-optimize-zero") ("build-from-source" "vk-shader-macros-impl/build-from-source"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "193caq2pm17ai03bgnav1zf3b6dplahhh0c5m6ag8yx6lkmghiar") (features (quote (("strip" "vk-shader-macros-impl/strip") ("default-optimize-zero" "vk-shader-macros-impl/default-optimize-zero") ("build-from-source" "vk-shader-macros-impl/build-from-source"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.6") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "00gjzlv36lymbwvyaflpd45lrnzsj61ksivfbfchn297fvi2hl0b") (features (quote (("strip" "vk-shader-macros-impl/strip") ("default-optimize-zero" "vk-shader-macros-impl/default-optimize-zero") ("build-from-source" "vk-shader-macros-impl/build-from-source"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.7") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "vk-shader-macros-impl") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "149zphc02bccwvxxg2ldcrsjglj28hrm573z1g6q7a5ryj1i5bxb") (features (quote (("strip" "vk-shader-macros-impl/strip") ("default-optimize-zero" "vk-shader-macros-impl/default-optimize-zero") ("build-from-source" "vk-shader-macros-impl/build-from-source"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.89") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "1bccnv2qyxd69jh8yiypwy5xjk2d6dwc5jpbnc3kjs2yxnx3znbs") (features (quote (("strip") ("default-optimize-zero") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-shader-macros-0.2 (crate (name "vk-shader-macros") (vers "0.2.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "0hwws0kg7kfarlr85ghc4d2imgfl70vkqvqffafg8kginbjac2pg") (features (quote (("strip") ("default-optimize-zero") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-shader-macros-impl-0.1 (crate (name "vk-shader-macros-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.3.10") (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "19rjpp9p6d4j6g6gdbhd0d8yb9ghf3ysyv488d63wqqlxj42ayjh") (features (quote (("default" "build-native-shaderc") ("build-native-shaderc" "shaderc/build-native-shaderc"))))))

(define-public crate-vk-shader-macros-impl-0.1 (crate (name "vk-shader-macros-impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.3.10") (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "0brxik113yk62gfk55dmxnl447y4ykc15kknys73a5kyckvh7r9i") (features (quote (("default" "build-native-shaderc") ("build-native-shaderc" "shaderc/build-native-shaderc"))))))

(define-public crate-vk-shader-macros-impl-0.1 (crate (name "vk-shader-macros-impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.3.10") (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "1i0rm6yy1vjfb6ayql0py8yhblq293vgxzkm63098224fxg3fs52") (features (quote (("strip") ("default" "build-native-shaderc") ("build-native-shaderc" "shaderc/build-native-shaderc"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "1djai6q1l6xcpyabxnbr6l1sk7fyc7n3grdm2lnmjld5wxsa8ixf") (features (quote (("strip"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "1vybsyr2gyb2m5j07b4dpcr85acp73zdzznacjsb1ri8qw0zfvvw") (features (quote (("strip"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "1dzp2fl0mzx21m88cmf6i7bx4lzzb7bdq2v8v7mp1vk712fclha9") (features (quote (("strip") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "16kwahadysd1dswiwpk309dsi4a80m6amfj030qimyv0nbylrqdf") (features (quote (("strip") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "0psmh2qmx8rf46l0b2sf9fs45g23rb61vhd7j3b4zclai0l20nc7") (features (quote (("strip") ("default-optimize-zero") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "0rnl72xkdb8j724kvdpyfcz04nc0lbgqlwc1jhcwysq4jv57wm13") (features (quote (("strip") ("default-optimize-zero") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.6") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "178iprra5dgy95mkbaf0mcjr99zmzxgvh9g425drms1ki2wb5y0s") (features (quote (("strip") ("default-optimize-zero") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-shader-macros-impl-0.2 (crate (name "vk-shader-macros-impl") (vers "0.2.7") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("parsing" "proc-macro" "derive"))) (kind 0)))) (hash "1fm59d6pyi5r1z9c6kmz6hg2qiwk2qc012fd3g665n1x3x4yrrpm") (features (quote (("strip") ("default-optimize-zero") ("build-from-source" "shaderc/build-from-source"))))))

(define-public crate-vk-sync-0.1 (crate (name "vk-sync") (vers "0.1.0") (deps (list (crate-dep (name "ash") (req "^0.24.4") (optional #t) (default-features #t) (kind 0)))) (hash "05mqcqzwq9lrkjrmz2asds4qr8waqjhqcirkisn5ijbiqlikm9c7") (features (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1 (crate (name "vk-sync") (vers "0.1.1") (deps (list (crate-dep (name "ash") (req "^0.25") (optional #t) (default-features #t) (kind 0)))) (hash "05g5pwnp79wanqc7zv0kmlnxcnxig2gbsb96xjvkdqxcmayjcilr") (features (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1 (crate (name "vk-sync") (vers "0.1.2") (deps (list (crate-dep (name "ash") (req "^0.26") (optional #t) (default-features #t) (kind 0)))) (hash "16fdvag03g9alh2npsmq9qkm20wwksxg1n4ihhqzl1rhn7g0fsfg") (features (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1 (crate (name "vk-sync") (vers "0.1.3") (deps (list (crate-dep (name "ash") (req "^0.26") (optional #t) (default-features #t) (kind 0)))) (hash "0nw03895x8b05v38syd5241zdg12nc1rq3m433rdwxi4074dqzvc") (features (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1 (crate (name "vk-sync") (vers "0.1.4") (deps (list (crate-dep (name "ash") (req "^0.26") (optional #t) (default-features #t) (kind 0)))) (hash "1j63xxbi8jwg4zafhk1bh38fan6dg27ycyfkx3m6xk7kx0yvy32n") (features (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1 (crate (name "vk-sync") (vers "0.1.5") (deps (list (crate-dep (name "ash") (req "^0.29") (optional #t) (default-features #t) (kind 0)))) (hash "0rssi5wri069i1ffcci0djl663ivpshmh9vjmsk3wnnix3yd6xh5") (features (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-0.1 (crate (name "vk-sync") (vers "0.1.6") (deps (list (crate-dep (name "ash") (req "^0.29") (optional #t) (default-features #t) (kind 0)))) (hash "16j8wqavxmqp8fgz4inrd95775vb0p893dcckqiqb01l4d6vapqj") (features (quote (("default" "ash_bind") ("ash_bind" "ash"))))))

(define-public crate-vk-sync-fork-0.2 (crate (name "vk-sync-fork") (vers "0.2.0") (deps (list (crate-dep (name "ash") (req "^0.33") (default-features #t) (kind 0)))) (hash "125m6aqys09aw1cpg04imb8jkz6spy2wp7fy3aa7j33h6mwcx8xy")))

(define-public crate-vk-sync-fork-0.2 (crate (name "vk-sync-fork") (vers "0.2.1") (deps (list (crate-dep (name "ash") (req "^0.33") (default-features #t) (kind 0)))) (hash "14kc0db2jl1c79vb6l04kg9k7kr0ghv4m686rc8i5i1bkcyr7i6y")))

(define-public crate-vk-sync-fork-0.2 (crate (name "vk-sync-fork") (vers "0.2.2") (deps (list (crate-dep (name "ash") (req "^0.33") (default-features #t) (kind 0)))) (hash "18mbpdlsq5vhn44g4y6vjmmnn152y0qwwzn76lm2kkn0rl49nm26")))

(define-public crate-vk-sync-fork-0.3 (crate (name "vk-sync-fork") (vers "0.3.0") (deps (list (crate-dep (name "ash") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "1l570pp2a8p61l00avzzdsqsqgcfaqjnb3a6d0f1f43bj085d0yl")))

(define-public crate-vk-sync-fork-0.4 (crate (name "vk-sync-fork") (vers "0.4.0") (deps (list (crate-dep (name "ash") (req "^0.37.0") (default-features #t) (kind 0)))) (hash "0szsrrkipr1dpndcxx76vvf243vb5a5ixwp3dfkyqjrjfw2cb3wv")))

(define-public crate-vk-sys-0.1 (crate (name "vk-sys") (vers "0.1.0") (hash "0zx44isj82sy064mnhm1jrikr5dphg692wr5k71hw750j5zwhpn1")))

(define-public crate-vk-sys-0.1 (crate (name "vk-sys") (vers "0.1.1") (hash "1bq8h4a9nz002nb24n9pbzxqxav1spp6mbng1h57w236bh2w3yhr")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.0") (hash "05zn1xzsychi84q8s0pj3wdbnwinsclxnbn828caaflny6pancmx")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.1") (hash "1syc58l2gca9pxcp9fy307hbms3y70r26cda91vpbcr98xvz423x")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.2") (hash "00yna1wb83g0zi568md1ljj8bhpy5qh3hvavikdf3mhcf4cj2lg9")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.3") (hash "1k9hc3wzmdidzssydjagqmgyag29jh8jnd99mh53j05nid7h69cj")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.4") (hash "0ndr4r27whqfb8cv2jiv45y1n4m5b5jqmn9zi3szs15zppnd3mqj")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.5") (hash "0f7xyr7i99l2hx3829w01jzmx3mgiy2gnvf798pfgkp7j4cri2kq")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.6") (hash "1wcmi13h9z1l7027smsnkpf9xrfzb92dnbi8rcv4y256djn1s9hk")))

(define-public crate-vk-sys-0.2 (crate (name "vk-sys") (vers "0.2.7") (hash "19621y08k4dbq3y110ma9s5xgwkzrvf4syx9wspwsq1mx5pkxhb8")))

(define-public crate-vk-sys-0.3 (crate (name "vk-sys") (vers "0.3.0") (hash "1l9yvmdsncwxwdfg6cain41lawsh66slbmwjxr7cmphhbw5qp92z")))

(define-public crate-vk-sys-0.3 (crate (name "vk-sys") (vers "0.3.1") (hash "0zw0nj10hhpy4cy3rckvydxbqgja6py84bjg5n2j1ylvkab1sws9")))

(define-public crate-vk-sys-0.3 (crate (name "vk-sys") (vers "0.3.2") (hash "00anlqry1hsn6nnifnv6xq4da3pj18mdfmy09zbgyra8xh7xkmwr")))

(define-public crate-vk-sys-0.3 (crate (name "vk-sys") (vers "0.3.3") (hash "00w19n47jdicbhb1rd4in3ga34fy0fwsj5qq418gwbdpg5mywrwm")))

(define-public crate-vk-sys-0.3 (crate (name "vk-sys") (vers "0.3.4") (hash "0xs1dhhpv06z59zy4b0534r70jmzwcb3xmj6994dllvw8g9n9nai") (yanked #t)))

(define-public crate-vk-sys-0.4 (crate (name "vk-sys") (vers "0.4.0") (hash "0ng050d86dn4f75x9ji7cmap3lmfq0qk2n0hhs01jpbdgm5gvx9n")))

(define-public crate-vk-sys-0.5 (crate (name "vk-sys") (vers "0.5.0") (hash "10971fzfpjq2zj74pyaqpvrmscrwk009migih54dy5dvi5xcwbvp")))

(define-public crate-vk-sys-0.5 (crate (name "vk-sys") (vers "0.5.1") (hash "0z6m6k9858l2m8khzawb1qbrx366plyhzp6j6ka6m1820iy10k62")))

(define-public crate-vk-sys-0.5 (crate (name "vk-sys") (vers "0.5.2") (hash "02zg3daww1n8mmyg6v4sgn4h91cdzxr8w9wa51ik13fm5cpil3xf")))

(define-public crate-vk-sys-0.5 (crate (name "vk-sys") (vers "0.5.3") (hash "01y915m9cazkagc2ax56gd332nkr7zcdzqvj89gadmaqp007nbki")))

(define-public crate-vk-sys-0.6 (crate (name "vk-sys") (vers "0.6.0") (hash "1j01nrympyy3lny8w6mqzs0hhwi7zigcr9aw3hvr3rks3sks3i6h")))

(define-public crate-vk-sys-0.6 (crate (name "vk-sys") (vers "0.6.1") (hash "0wyxhab42d1rfgxm3byyxyqhydmk7j9f7c8hy644lxjynmw87kqk")))

(define-public crate-vk-sys-0.6 (crate (name "vk-sys") (vers "0.6.2") (hash "0bi8c0wmjq0clzc77a99kz5qq5zpac20axjfhzxy3lz2gmz2fmcl") (yanked #t)))

(define-public crate-vk-sys-0.7 (crate (name "vk-sys") (vers "0.7.0") (hash "0ffjw15qfl1gxrs9hd59dzsqi1x26wpk69573430p01mqyph6h2q")))

