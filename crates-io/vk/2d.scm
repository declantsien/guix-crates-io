(define-module (crates-io vk #{2d}#) #:use-module (crates-io))

(define-public crate-vk2dfd-0.1 (crate (name "vk2dfd") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0qynfs9rlvyngz3m23ss07lvdvsq5y8afwxxg1yr0rhfdynxhy12") (rust-version "1.70")))

