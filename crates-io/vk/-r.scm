(define-module (crates-io vk -r) #:use-module (crates-io))

(define-public crate-vk-rs-0.1 (crate (name "vk-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winuser" "libloaderapi"))) (default-features #t) (kind 0)))) (hash "1cvrp9gdj2j7zy09r10r6kjlka37qn3iq73b78agxd7jj82qgm2n") (yanked #t)))

(define-public crate-vk-rs-0.1 (crate (name "vk-rs") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winuser" "libloaderapi"))) (default-features #t) (kind 0)))) (hash "0w00d5zlm376b89cpkl41njdiahc10jzmns3ss1b2wgf2hws9rb7") (yanked #t)))

(define-public crate-vk-rs-0.1 (crate (name "vk-rs") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winuser" "libloaderapi"))) (default-features #t) (kind 2)))) (hash "0flk1dhvwmq96y1hda9lbcwymqffm7qss996h60v4wrb4mndgi7r")))

(define-public crate-vk-rs-0.1 (crate (name "vk-rs") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winuser" "libloaderapi"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (features (quote ("xlib"))) (optional #t) (default-features #t) (kind 0)))) (hash "1zq9dfjqhdfg67imi4xbmakcdpmwwwl0hhkfl0bx9bd7jpb48al9") (features (quote (("xlib" "x11") ("xcb") ("win32" "winapi") ("wayland") ("android"))))))

(define-public crate-vk-rs-0.1 (crate (name "vk-rs") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winuser" "libloaderapi"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.18.2") (features (quote ("xlib"))) (optional #t) (default-features #t) (kind 0)))) (hash "08vydq3psvds1h76241bgf3b03c2vijxy58i1bgaj7mywqj2jvfa") (features (quote (("xlib" "x11") ("xcb") ("win32" "winapi") ("wayland") ("android"))))))

