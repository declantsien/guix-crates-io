(define-module (crates-io vk ff) #:use-module (crates-io))

(define-public crate-vkfft-0.1 (crate (name "vkfft") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "vk-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "vkfft-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.22") (default-features #t) (kind 0)))) (hash "0w9453dlqn0zlhf24pyl1irlb8x1723x5g62nhbsns6an11mwz1f")))

(define-public crate-vkfft-0.1 (crate (name "vkfft") (vers "0.1.1") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "vk-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "vkfft-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.22") (default-features #t) (kind 0)))) (hash "0h76q48160arv8zc26djxdjj97zq5dcr8pfchwd3pfh3aq4shspj")))

(define-public crate-vkfft-sys-0.1 (crate (name "vkfft-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0hrhv1cfcd9bmr3p7p0i3iqxssw58ysya2f45yn0knyzwxv89bx5")))

(define-public crate-vkfft-sys-0.1 (crate (name "vkfft-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0d5qr9kidwld1m13cwrr6fqgf7rxd9pn1wi1231ih7w4g8kdzl5w")))

