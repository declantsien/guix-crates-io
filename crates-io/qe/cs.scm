(define-module (crates-io qe cs) #:use-module (crates-io))

(define-public crate-qecs-0.0.1 (crate (name "qecs") (vers "0.0.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qecs-core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.4") (default-features #t) (kind 0)))) (hash "0384am2m1jpfp9lsxwr48cb3qy172gbidcr1nhg9jhjg4xzn4j51")))

(define-public crate-qecs-0.0.2 (crate (name "qecs") (vers "0.0.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qecs-core") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.4") (default-features #t) (kind 0)))) (hash "0n11rqqinvwwpjvhsp0c07ad5wxg3qrmqyf834lg0qimrz0m27bs")))

(define-public crate-qecs-0.0.3 (crate (name "qecs") (vers "0.0.3") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qecs-core") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rm7gv76r4pj9vj1bf3mjwdcqlm6c7xzmzfayjrp31aqmnfqg83l")))

(define-public crate-qecs-0.0.4 (crate (name "qecs") (vers "0.0.4") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qecs-core") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.4") (default-features #t) (kind 0)))) (hash "0p67xswbv8ykh0xiiifxrax4ddn6symf0qc9jr4bpb1jah1ydnxc")))

(define-public crate-qecs-0.0.5 (crate (name "qecs") (vers "0.0.5") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qecs-core") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gxxazr0v1w5wnawsbv2xdgxx5an91ja3jpzln04lkn7md0fq3vw")))

(define-public crate-qecs-0.0.6 (crate (name "qecs") (vers "0.0.6") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qecs-core") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.4") (default-features #t) (kind 0)))) (hash "0i7xz9npjyfvjp799nb6bchvg6s2ar222n7rn8zarsxzaa8mqm9l")))

(define-public crate-qecs-0.0.7 (crate (name "qecs") (vers "0.0.7") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "qecs-core") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.4") (default-features #t) (kind 0)))) (hash "10qhk071i80d8yfrpjbdqd0awwaax8anr1nq4l0bqhn12afnnjqw")))

(define-public crate-qecs-core-0.0.1 (crate (name "qecs-core") (vers "0.0.1") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "19j02i22amwby6l83qpgf4a7sq68rcc5966wdfy6mbizkhs7anic")))

(define-public crate-qecs-core-0.0.2 (crate (name "qecs-core") (vers "0.0.2") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0i81z8fgjn40wmqqfvln2scankrd3cnw7zxd507ykgaj16mjfxa9")))

(define-public crate-qecs-core-0.0.3 (crate (name "qecs-core") (vers "0.0.3") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "00634bzskncxgb1kbpgc295p3rq9ajx4pllrzbiqb2s2dyzvbigv")))

(define-public crate-qecs-core-0.0.4 (crate (name "qecs-core") (vers "0.0.4") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0hvxk967s7kfk0lrycqlp5mcwh5dq9ahjmcpjhwf8icyzc1aq6q2")))

(define-public crate-qecs-core-0.0.5 (crate (name "qecs-core") (vers "0.0.5") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "134qapdv2ks4f1xkd1n5cwdcp8q25zxw06358z4ay4bnbrlfvckg")))

(define-public crate-qecs-core-0.0.6 (crate (name "qecs-core") (vers "0.0.6") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "18an0wkdjqmkcpz4wkw8nyszwrjmnamy5vyh5d3vpas6lffw198n")))

(define-public crate-qecs-core-0.0.7 (crate (name "qecs-core") (vers "0.0.7") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0273xcw5nvlaxhr9pnr60l0s2vv746sqik89v3vgx27bs8dqpb4a")))

(define-public crate-qecs-core-0.0.8 (crate (name "qecs-core") (vers "0.0.8") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1ppyfjnf6918lxgz9apdarqzx0j0p2dkcqaigxq3ph29mkx16s7m")))

(define-public crate-qecs-core-0.0.9 (crate (name "qecs-core") (vers "0.0.9") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0z7hlkgqann2l79nhfv4vjczadmxvakc4vprayqq30ny0l8a4ffn")))

(define-public crate-qecs-core-0.0.10 (crate (name "qecs-core") (vers "0.0.10") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0hvs6hjw9sbvc04g6z1wam4gd5z8b1qsf65sfjfi745as7ds02fk")))

(define-public crate-qecs-core-0.0.11 (crate (name "qecs-core") (vers "0.0.11") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1k7d4grynh3b6z1n35kydxmxj6953yxrxvnaw1p22kfkdd9lh3cq")))

(define-public crate-qecs-core-0.0.12 (crate (name "qecs-core") (vers "0.0.12") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "05q6lv3pqgkd75nvki6bpkss0w8d01ym0p03nxy0x6bikr73krjp")))

(define-public crate-qecs-core-0.0.13 (crate (name "qecs-core") (vers "0.0.13") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1inagp94ybpf5jrbkd7g56kwhmvyqaa45aab63dk8yjrv1rd4ar6")))

(define-public crate-qecs-core-0.0.14 (crate (name "qecs-core") (vers "0.0.14") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1dl8qllpyy5vr5s9qdng3p4wrbnr72y3hnnp98knnz918nn9amb7")))

(define-public crate-qecs-core-0.0.15 (crate (name "qecs-core") (vers "0.0.15") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "146g2qqn6bwzymai3xkf1kfmb7gpahk1kcrzwrh15xw3df775cf5")))

(define-public crate-qecs-core-0.0.16 (crate (name "qecs-core") (vers "0.0.16") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0jdfwimg595gwrslldx1qayyhd7yz33zaf0iva2mbb1gqf9kaa1w")))

(define-public crate-qecs-core-0.0.17 (crate (name "qecs-core") (vers "0.0.17") (deps (list (crate-dep (name "downcast") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ioc") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "06rdsxivhzcan26anz6jfplpyspfxq4r60jvfv89mjzkw1qv8799")))

