(define-module (crates-io qe sp) #:use-module (crates-io))

(define-public crate-qesp-0.1 (crate (name "qesp") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fbl7km0wif2gf8k7gbrrl8a2m66lpfavcn8qlvnqw0gxfqrm64z")))

(define-public crate-qesp-0.2 (crate (name "qesp") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0fqrialrp1nynn0y8sbaf2ncinb1h9iiv5y6d83jpqh6fbzlk05p")))

(define-public crate-qesp-0.2 (crate (name "qesp") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jfn7q8b07qqxk3kgg6xgv9yn2ih7qn644wzqyhrn6afci6ac71z")))

(define-public crate-qesp-0.2 (crate (name "qesp") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xj5p2vcz8ijqbiw7n6l43sbx2fq53dljk790xpb9kin25fkc0sq")))

(define-public crate-qesp-0.2 (crate (name "qesp") (vers "0.2.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "11wlbfgiq8wcp2pv8hlxi1pv8zv267s7crcljz45bbd44839c5m5")))

(define-public crate-qesp-0.2 (crate (name "qesp") (vers "0.2.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1px9sm73kaglg04h3fnsgi024wmn1z2xc8gz6y929c97fd0jlqjp")))

