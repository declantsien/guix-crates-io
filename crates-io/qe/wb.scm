(define-module (crates-io qe wb) #:use-module (crates-io))

(define-public crate-qewby_parser-0.1 (crate (name "qewby_parser") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0hbfcwsq935vj0idsp7qnwqdp4kjy0hh8cbv1rkwd26f4jcf6vgj")))

