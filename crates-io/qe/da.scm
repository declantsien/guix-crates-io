(define-module (crates-io qe da) #:use-module (crates-io))

(define-public crate-qeda-0.0.1 (crate (name "qeda") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)))) (hash "0qamsxjjbvv6k3rqbajkypqir0s60pgccx388cg9314877zj47fb")))

(define-public crate-qeda-0.0.3 (crate (name "qeda") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "crypto-hash") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0zmic1v4syflk5h29rnalmjmbg3hrysc9sswyhzndrrbphs48p2x")))

