(define-module (crates-io r0 ke) #:use-module (crates-io))

(define-public crate-r0ket-l0dable-0.0.0 (crate (name "r0ket-l0dable") (vers "0.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "lpc13xx") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "r0") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0bqlvimnk5v4cwlajv17fwlv1szzgdck3qjgmc20r201r4lslij6")))

(define-public crate-r0ket-l0dable-0.1 (crate (name "r0ket-l0dable") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "lpc13xx") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "r0") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "028avmyjz7dvvcgks76x58wda91zgl5z1vnxl1idfrbdpfaqjl4n")))

