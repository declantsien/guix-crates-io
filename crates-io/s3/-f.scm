(define-module (crates-io s3 -f) #:use-module (crates-io))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.0") (hash "0dr0kwfznlzd6fdsw2b961da721s8hffzzkbilk4vbyhqrhk0ip4") (yanked #t)))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.1") (hash "1ar3r6860fn8hy0jwcz7lggjzwfvmgbwcg8l0m032i3z5qdv519v")))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.2") (hash "0wid7nig2hhgpchsyr1fyhv9wqbw8hb3jpja1dx465z3997xdaqr") (yanked #t)))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.3") (hash "0yv7ah07c16zqyminpzx59gyzx6jn34sc1xc5bkci232bikis76a") (yanked #t)))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.4") (hash "1fsqy56b0g0r8z944786y9y6v3m18v0fcrrd2f83p7v0wpbag28n") (yanked #t)))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.5") (hash "06w80ipzfs8d3rgpbjqgyh9xrcsffznr1650w531jxsnfd8xla2k") (yanked #t)))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.6") (hash "1kk03y8lmm3yqycdl5mf8c0sz5pqdxcrpvjnwx309cnppsa3f3ja") (yanked #t)))

(define-public crate-s3-fifo-0.1 (crate (name "s3-fifo") (vers "0.1.7") (hash "1a0jgm80k63ic2i3spjvhr3h5hl4avib4xkzlpgbw1kf3navmllq")))

(define-public crate-s3-filesystem-0.0.1 (crate (name "s3-filesystem") (vers "0.0.1") (deps (list (crate-dep (name "aws-config") (req "^0.57.1") (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-s3") (req "^0.35.0") (default-features #t) (kind 0)) (crate-dep (name "aws-smithy-runtime-api") (req "^0.57.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("fs" "io-util" "io-std"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "00cdqx7yfgzjdqbzwqwcn78sqqvji411kdw1hkzjbmhn4ar9hzjp")))

