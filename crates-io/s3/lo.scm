(define-module (crates-io s3 lo) #:use-module (crates-io))

(define-public crate-s3logger-0.1 (crate (name "s3logger") (vers "0.1.0") (deps (list (crate-dep (name "rust-s3") (req "^0.32.0") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1rf7rwjjwx3j2m9wki6g5dcv5fjjkzzq25ax8zsb4mrj6y0hkw8b")))

(define-public crate-s3logger-0.1 (crate (name "s3logger") (vers "0.1.1") (deps (list (crate-dep (name "rust-s3") (req "^0.32.0") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0rlhh221pz658h0lynq3i0mny0rbcxf6cik3p2sr5y6rnzga7xg0")))

(define-public crate-s3logger-0.2 (crate (name "s3logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "rust-s3") (req "^0.32.0") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1wgyn9kcpp6zgy754rg4b622vamdvpzwslk9b3192jfkv97y3spk")))

