(define-module (crates-io s3 rm) #:use-module (crates-io))

(define-public crate-s3rm-0.1 (crate (name "s3rm") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "problem") (req "^5.3.0") (default-features #t) (kind 0)) (crate-dep (name "s3-sync") (req "^0.3.0") (features (quote ("chrono"))) (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)))) (hash "1hk9ldj1zkwyf63j40ycq8cplh1fmza04g6kn0n6b5al57jb3nrn")))

