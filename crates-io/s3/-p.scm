(define-module (crates-io s3 -p) #:use-module (crates-io))

(define-public crate-s3-presign-0.0.1 (crate (name "s3-presign") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0y9yk4vzkkyyniqhcv00a35gzhn2bsrx54abcpqk4gphbbgy54r2")))

(define-public crate-s3-presign-0.0.2 (crate (name "s3-presign") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "14kfskyzcjgczbjw5mksxi7dwhwf7kiv4vcsgsc1g0xvlmxj13kl")))

