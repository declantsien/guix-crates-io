(define-module (crates-io s3 gr) #:use-module (crates-io))

(define-public crate-s3graph-0.0.1 (crate (name "s3graph") (vers "0.0.1") (hash "0kvcqshbbwnzlg3792rl2s43akvq81wvfqd5dlk95d9fwxfl12np")))

(define-public crate-s3graph-0.0.2 (crate (name "s3graph") (vers "0.0.2") (hash "1pbykmyw1mkapq35p59k75a8hic47gmv3zg8zb1wljmzwslrf3g8")))

