(define-module (crates-io s3 vf) #:use-module (crates-io))

(define-public crate-s3vfs-0.0.0 (crate (name "s3vfs") (vers "0.0.0") (hash "0rg7shhakl55acbbqvhz3l3p3ppsg7q2r8l6giw0rqd2nhq9480z")))

(define-public crate-s3vfs-0.1 (crate (name "s3vfs") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0hir48kp12wbypx2n20ay41n5z5jljgnr6mwqrm9xwyj0mzg9g0m")))

(define-public crate-s3vfs-0.1 (crate (name "s3vfs") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 0)))) (hash "1qsgapqmaqm07vll3lqqb33skynyxqpsx0424mq7qh7ba2ardkfg")))

