(define-module (crates-io al am) #:use-module (crates-io))

(define-public crate-alam_sum-0.1 (crate (name "alam_sum") (vers "0.1.0") (hash "03npfjwyc0hfx6ys7kg999vzzsy29s7548kp8zydc1xfs09qx4s9")))

(define-public crate-alam_sum-0.1 (crate (name "alam_sum") (vers "0.1.1") (hash "0mgq2y00p57wh3dwv8g4as9ps8m1w8rfyb5416mndjm4sr1hdsr9")))

(define-public crate-alam_sum-0.1 (crate (name "alam_sum") (vers "0.1.2") (hash "03afcmbi7h67p5px44k6d4j456kcvrnz6b88rcm8pl8a7pn4bfkg")))

(define-public crate-alamofire-kit-0.1 (crate (name "alamofire-kit") (vers "0.1.0") (deps (list (crate-dep (name "semver") (req "^1.0") (kind 0)))) (hash "03j6pl5bvbzas2wdsmcgjvjqvrakrs7102pmgwdpjfxmn3wjgz4h")))

(define-public crate-alamofire-kit-0.1 (crate (name "alamofire-kit") (vers "0.1.1") (deps (list (crate-dep (name "semver") (req "^1.0") (kind 0)))) (hash "1vcf6kdwzs850wgfv905pj9gqbyaighlgspiwm1k506bdq9hw2lg")))

(define-public crate-alamofire-kit-0.1 (crate (name "alamofire-kit") (vers "0.1.2") (deps (list (crate-dep (name "semver") (req "^1.0") (kind 0)))) (hash "01lb7b2hvkvyvfqpa26jdwhdwq0m4v5bkz3jz0axl53v2nbrn29g")))

(define-public crate-alamofire-kit-0.2 (crate (name "alamofire-kit") (vers "0.2.0") (deps (list (crate-dep (name "semver") (req "^1") (kind 0)))) (hash "0iy28wbryb2i23wqmsc15928fnim4vkwr7xbbyhyyh44xiy9vq56")))

(define-public crate-alamofire-kit-0.3 (crate (name "alamofire-kit") (vers "0.3.0") (deps (list (crate-dep (name "semver") (req "^1") (kind 0)))) (hash "1sc0351p1gw8lzvym0idjaga61jh7xj273y3fkgpwzh679d5jirj")))

