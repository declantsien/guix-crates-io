(define-module (crates-io al ts) #:use-module (crates-io))

(define-public crate-altscr-0.1 (crate (name "altscr") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "0512z206n9py7h0p463yx0nmndkq061j8w192saim1pwhjn3g5kl")))

(define-public crate-altsvc-0.1 (crate (name "altsvc") (vers "0.1.0") (hash "01lcbxmazddlsfc7830wx467d7z5vpjx4jzsvphk94c544k90bjq")))

(define-public crate-altsvc-0.1 (crate (name "altsvc") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "15f3qx4c6ny21i4n7arkxraxk64jb7q2cy889a656hnkjsyfs5kx")))

(define-public crate-altsvc-0.1 (crate (name "altsvc") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v95ilaaqzxp918jqibl087a8nvb2x9in367lvya374a805mqqrz")))

