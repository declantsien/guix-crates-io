(define-module (crates-io al lw) #:use-module (crates-io))

(define-public crate-allwords-0.1 (crate (name "allwords") (vers "0.1.0") (hash "05xya1w968fymg7vya9f4zv2bzwzb4nsw4bjxr5fpi7jqcph2xk6")))

(define-public crate-allwords-0.1 (crate (name "allwords") (vers "0.1.1") (hash "1i0xqjb7z4ng8l8rrdzyh0w2nz5w7zz113h2ffc08lwx6zlsgmiw")))

(define-public crate-allwords-0.1 (crate (name "allwords") (vers "0.1.2") (hash "0pf9av4yx8hv9s341yiz7x2b3k8cd49i5vjh9rgbgvshjy5s0n74")))

