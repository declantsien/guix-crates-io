(define-module (crates-io al e-) #:use-module (crates-io))

(define-public crate-ale-sys-0.0.1 (crate (name "ale-sys") (vers "0.0.1") (hash "16ljrgry8kxn4s67hk6scj1zbgbvlzhgjsxla5mkvvj6i1f2g15z")))

(define-public crate-ale-sys-0.1 (crate (name "ale-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0ayq8igxjanvml648mjh3alndcnc1k64j2aaihbvnrvfkw9yrvqa")))

(define-public crate-ale-sys-0.1 (crate (name "ale-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 1)))) (hash "09lr9wfjhl1vl1blnva01113yz7zp7nmxhkvjcbk3y6q141x66yc")))

(define-public crate-ale-sys-0.1 (crate (name "ale-sys") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 1)))) (hash "0c0vkpq5bbd87chsprysf51q1mj629sqbpny09hy0s5hzya3mgwn")))

