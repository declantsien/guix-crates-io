(define-module (crates-io al lf) #:use-module (crates-io))

(define-public crate-allfarbe-0.1 (crate (name "allfarbe") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "noise") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1vsby86sx1jh09sgb9kc6p0j3vymj3vlh8xqgaxl2gb3i708zknh")))

(define-public crate-allfarbe-0.1 (crate (name "allfarbe") (vers "0.1.1") (deps (list (crate-dep (name "glium") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "noise") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0zmpd407vvkq7cp2a18asl2wwnbsf3bnl9ri6b2yh4ngg025pjgd")))

