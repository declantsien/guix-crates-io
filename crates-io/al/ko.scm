(define-module (crates-io al ko) #:use-module (crates-io))

(define-public crate-alkomp-0.1 (crate (name "alkomp") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.6") (default-features #t) (kind 0)))) (hash "03331c7zkd5lyqdsi04cqdj5j9sxnfj6my3s0zirbkm073a98ba5")))

