(define-module (crates-io al es) #:use-module (crates-io))

(define-public crate-alesis-samplepad-lib-0.1 (crate (name "alesis-samplepad-lib") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "08jqji5q1p1bw5nvqlg47bn1y5a5p7sg1v2036x4gp7asxxc57wy")))

