(define-module (crates-io al bu) #:use-module (crates-io))

(define-public crate-albus-solana-verifier-0.1 (crate (name "albus-solana-verifier") (vers "0.1.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.29.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "arrayref") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.16") (default-features #t) (kind 0)))) (hash "1a2vndwpddgm8fif1ypvca68qyzafabrzf7y2ck2fvkqvqc3wh5r") (features (quote (("cpi" "anchor-lang"))))))

(define-public crate-albus-solana-verifier-0.1 (crate (name "albus-solana-verifier") (vers "0.1.3") (deps (list (crate-dep (name "anchor-lang") (req "^0.29.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "arrayref") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.16") (default-features #t) (kind 0)))) (hash "0qyk7b3rnj347hdlw1d14mj7c1lhkqhyck2lb3slqb04mc0j5xzp") (features (quote (("cpi" "anchor-lang"))))))

(define-public crate-albus-verifier-0.1 (crate (name "albus-verifier") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.14") (default-features #t) (kind 0)))) (hash "14nri7rs6fcaywg9gm7ly096z57q2f7ymc0jk5jrasdzwz581vyg") (yanked #t)))

(define-public crate-albus-verifier-0.1 (crate (name "albus-verifier") (vers "0.1.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.14") (default-features #t) (kind 0)))) (hash "1bmqd1m8582ywvbpm972gp7drd1jdhq8drfk3b72jwvyy0lgn8m8") (yanked #t)))

