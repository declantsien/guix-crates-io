(define-module (crates-io al ek) #:use-module (crates-io))

(define-public crate-aleksandr-vinokurov-cv-2023 (crate (name "aleksandr-vinokurov-cv") (vers "2023.10.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1qr36rs28q3vppy7yhl0md62xn3xy2gkhqlzl2rbhbj4y8g4nf5j")))

(define-public crate-aleksandr-vinokurov-cv-2023 (crate (name "aleksandr-vinokurov-cv") (vers "2023.10.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0vfpv6l029v9jigh36wyl0ri95sysizw5ix23344k1pbx1v87lca")))

(define-public crate-aleksandr-vinokurov-cv-2024 (crate (name "aleksandr-vinokurov-cv") (vers "2024.1.16") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "19yxl9jky3k8v7axzyqxhh9rsg8pidm0ixspnmvl0favp6l7dr2r")))

(define-public crate-aleksandr-vinokurov-cv-2024 (crate (name "aleksandr-vinokurov-cv") (vers "2024.2.5") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "04jjphp7nxjcji24z2vh32fbspm5ghayqdzcs3wyx0j4q1njdn69")))

(define-public crate-aleksandr-vinokurov-cv-2024 (crate (name "aleksandr-vinokurov-cv") (vers "2024.2.6") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0mizjs0043qvdc3zkbpdw5bv2s8xids7q9xr7dj6218yyc671ahm")))

