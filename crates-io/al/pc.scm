(define-module (crates-io al pc) #:use-module (crates-io))

(define-public crate-alpc-0.0.2 (crate (name "alpc") (vers "0.0.2") (hash "0zjdp2bbmqipj6lq3a9mz17byim5nirp04x803ip0yy6i2gdw88b")))

(define-public crate-alpc-1 (crate (name "alpc") (vers "1.0.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1861y8x45rjwdbmz5g1fhnmvb2iidya7j8ra6v20aki0kg3k4lj9")))

(define-public crate-alpc-1 (crate (name "alpc") (vers "1.0.1") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "076mmppzhh79mgl7wm5i99ky2ayxbmrncyxs9svkgfr9m6412x9n")))

