(define-module (crates-io al ci) #:use-module (crates-io))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "0n8p82r6z3ygk1c87d5d5sgqnl888shxxcfbrllch1q2591jrrpc")))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "1ik6rq05nxrykg6nd8gzd09r1prw0g2csjmwpczcak23yb9hbqy9")))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "1hajsp1hfsrxq5vijp15wp8i9mdqymny2xyk07lhdck7dcy9p92z")))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "1i811zjrj1fdfb0r4bmy0wpzfa4rmmk7yy5nwi6l7c00862s64hl")))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "0bzj3aqnngn58nw4kdilahwpi3w5vx8w04ibiipbxjk25vfk95js")))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.5") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0vnnc0wn535r49cvhxbrgkv2cypc58x3ni99jislin0diy1rxa4p")))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.6") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1ggci7z70f84knm487fr0s2p6m5yd9j2jsdwlr6lsiiapsg9lpcb")))

(define-public crate-alcibiades-0.2 (crate (name "alcibiades") (vers "0.2.7") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "16mhal7lnvdqgnbyvkvww5ls0r8g6pglxjfjp2365sf46fil6fw5")))

(define-public crate-alcibiades-0.3 (crate (name "alcibiades") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0y66szzm01xspch98l47vc8mrmqxnrc7rpy5z43vs2y7q33kqz4y")))

(define-public crate-alcis-0.0.0 (crate (name "alcis") (vers "0.0.0") (hash "111mzk0jxi05m69d4rv3j4sf825jg7zhvrg4c81vp898gnmpf7ib")))

