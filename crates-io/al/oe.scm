(define-module (crates-io al oe) #:use-module (crates-io))

(define-public crate-aloe-0.1 (crate (name "aloe") (vers "0.1.0") (hash "19x616kfd8anzcnrq9hmlkajhz0kazrb7hcl36jr692g7j0gga2i") (yanked #t)))

(define-public crate-aloe-0.0.0 (crate (name "aloe") (vers "0.0.0") (deps (list (crate-dep (name "glow") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.24") (default-features #t) (target "cfg(not(any(target_arch = \"wasm32\")))") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("HtmlCanvasElement" "WebGl2RenderingContext" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1q5pbg1whdjn1rwbr2ia0cb9kdg2f90k6gv50jq54qsa71bj07ig") (yanked #t)))

