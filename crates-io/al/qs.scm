(define-module (crates-io al qs) #:use-module (crates-io))

(define-public crate-alqs-0.1 (crate (name "alqs") (vers "0.1.0") (hash "07j0nd3xjay7xpb1hham4gz1cxacbkh4n88y81g290q490cckmd1")))

(define-public crate-alqs_cli-0.1 (crate (name "alqs_cli") (vers "0.1.0") (deps (list (crate-dep (name "alqs_shared") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yp2m5k10wwky3hgkqa191wgq407z1yrgdvww6q776m7gm6dr94w")))

(define-public crate-alqs_db-0.1 (crate (name "alqs_db") (vers "0.1.0") (deps (list (crate-dep (name "alqs_shared") (req "^0.1") (default-features #t) (kind 0)))) (hash "0q9bpikfv8jij9q94kpmhrb9mbk8cwdwhznmk70y3sdhx3zfybqx")))

(define-public crate-alqs_shared-0.1 (crate (name "alqs_shared") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.10") (default-features #t) (kind 1)) (crate-dep (name "tonic-health") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1nc67prs07g5nzv141hjhqncx9ik2ms415mmnfffs46qka6b7l10")))

