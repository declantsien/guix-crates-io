(define-module (crates-io al zh) #:use-module (crates-io))

(define-public crate-alzheimer-0.1 (crate (name "alzheimer") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.5.0") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "0mjc3sjllwjl1wlax33dgc4jii4cpcngdvg87qk5hcr99zyvyv1n")))

