(define-module (crates-io al -s) #:use-module (crates-io))

(define-public crate-al-sys-0.1 (crate (name "al-sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "1sccl6ghg4jy7bihapncm2ww8wyvcp71finamlsajqgcr5qxx6ma")))

(define-public crate-al-sys-0.2 (crate (name "al-sys") (vers "0.2.0") (deps (list (crate-dep (name "libloading") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1pkv8f9i9qwnjjiqsfa4jbq2iasyvl136c1b31mjbarplawd0y06") (yanked #t)))

(define-public crate-al-sys-0.2 (crate (name "al-sys") (vers "0.2.1") (deps (list (crate-dep (name "libloading") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1xz9za2cbwiwb7ymav3lsgv18ghfw5bvpginz72sjyaz9s4gbqwm")))

(define-public crate-al-sys-0.3 (crate (name "al-sys") (vers "0.3.0") (deps (list (crate-dep (name "libloading") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "090dcv9701bp4h1zv8qj0bkfdm330j60ki87ia6q3ycm2jqnygmx")))

(define-public crate-al-sys-0.3 (crate (name "al-sys") (vers "0.3.1") (deps (list (crate-dep (name "libloading") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0h96w0ijiqf6ww5119nrjzi629b8z6p5024l26jw0dp4ajhlddzi")))

(define-public crate-al-sys-0.3 (crate (name "al-sys") (vers "0.3.2") (deps (list (crate-dep (name "libloading") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1ni6fcvc9ljg85ph1kay6gfpc38b1jfqfq5lrdpjiimic32z7yyi")))

(define-public crate-al-sys-0.3 (crate (name "al-sys") (vers "0.3.3") (deps (list (crate-dep (name "libloading") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "11kg4rlxja8xlficslyvnyfsryh1d2sk24cvm20081gcismi43np")))

(define-public crate-al-sys-0.4 (crate (name "al-sys") (vers "0.4.0") (deps (list (crate-dep (name "libloading") (req "^0.3.1") (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "07ycag4iz3fng32wg1xb0xlz488d03dkqr54c0b31yk3nhv06j02")))

(define-public crate-al-sys-0.4 (crate (name "al-sys") (vers "0.4.1") (deps (list (crate-dep (name "libloading") (req "^0.4") (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0zfxsgcxcmy051mscgghkny00i6ayn09gg9xnf4m3zaqd2wfhvlb")))

(define-public crate-al-sys-0.5 (crate (name "al-sys") (vers "0.5.0") (deps (list (crate-dep (name "libloading") (req "^0.4") (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1k20grj0qj7b6kar1lh492hbvh8pac3ib8dmg02jfx7ijipdw8kc") (yanked #t)))

(define-public crate-al-sys-0.5 (crate (name "al-sys") (vers "0.5.1") (deps (list (crate-dep (name "libloading") (req "^0.4") (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0a77wfy03j0qiw67ria4f6gfir7b82x6afy3bxa8rnzza7gg69i1")))

(define-public crate-al-sys-0.5 (crate (name "al-sys") (vers "0.5.2") (deps (list (crate-dep (name "libloading") (req "^0.4") (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0wrm24k9sikazzih2w5ry7bcscssrjam0j69kzail6rc2g0x804z")))

(define-public crate-al-sys-0.5 (crate (name "al-sys") (vers "0.5.3") (deps (list (crate-dep (name "libloading") (req "^0.5") (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1a9bznqb3kx2xdvbkvyrg6b8nxdwzzd2pdp2xl8rlp75k1rkx4vn")))

(define-public crate-al-sys-0.6 (crate (name "al-sys") (vers "0.6.0") (deps (list (crate-dep (name "libloading") (req "^0.5") (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)) (crate-dep (name "rental") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1b5lir8cmjrmal5zxq6awgnsdxvhlci9a7kafdsk54239ycyz581")))

(define-public crate-al-sys-0.6 (crate (name "al-sys") (vers "0.6.1") (deps (list (crate-dep (name "cmake") (req "^0.1.40") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.5") (optional #t) (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)) (crate-dep (name "rental") (req "^0.5.0") (optional #t) (default-features #t) (target "cfg(not(target_os = \"emscripten\"))") (kind 0)))) (hash "08whlcfrhn4gqi4nbglkdqv5ysdpnvnlsqg51q34q9hh9l7rp3gz") (features (quote (("dynamic" "libloading" "rental") ("default" "dynamic"))))))

