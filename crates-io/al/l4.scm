(define-module (crates-io al l4) #:use-module (crates-io))

(define-public crate-all4art-authservice-cqrs-0.1 (crate (name "all4art-authservice-cqrs") (vers "0.1.0") (hash "1p1qbna86l207n2vqqgqbq1sd06g2kzln7x9x75dhjmcwj8d8k8b")))

(define-public crate-all4art-authservice-cqrs-0.1 (crate (name "all4art-authservice-cqrs") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "10m2zh7l3ih13iay83q6k47xlzmgh5ckk1pxpbd1lpd49ij635vj")))

(define-public crate-all4art-authservice-cqrs-0.1 (crate (name "all4art-authservice-cqrs") (vers "0.1.2") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "all4art-authservice-persistence") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1l6kcfyprs1s5y6qdca3a8ii5rqysh69cdfs4j720xwhxb9y0i8m")))

(define-public crate-all4art-authservice-cqrs-0.1 (crate (name "all4art-authservice-cqrs") (vers "0.1.3") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "all4art-authservice-persistence") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "00z3agdxmcwcvd6x724qqpl9iiqhifgydqa134v49b77ppdgjyfk")))

(define-public crate-all4art-authservice-cqrs-0.1 (crate (name "all4art-authservice-cqrs") (vers "0.1.4") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "all4art-authservice-persistence") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1y8c22fhv4nyfd8p7babi6yypdjlpccchdc88hiyhz4imxza0w13")))

(define-public crate-all4art-authservice-cqrs-0.1 (crate (name "all4art-authservice-cqrs") (vers "0.1.5") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "all4art-authservice-persistence") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "116alap6xhkxa4f28c3zjqv9bx2s2pzxyna756mcqj4hyfl5v4bq")))

(define-public crate-all4art-authservice-domain-0.1 (crate (name "all4art-authservice-domain") (vers "0.1.0") (hash "1qvzf52cmdlh190p1fjgc4vwnwlxjq5ijmb7nvndbqlfqa7z2dmn")))

(define-public crate-all4art-authservice-domain-0.1 (crate (name "all4art-authservice-domain") (vers "0.1.1") (hash "040zad70kwj2h71jyzrncnhicwgp19r0nj78wh49i304nhniqn8h")))

(define-public crate-all4art-authservice-domain-0.1 (crate (name "all4art-authservice-domain") (vers "0.1.2") (hash "0m3fzfh9cfy1dh09gwd9h78am6y7r67aws72d1wihv1s8jgjni67")))

(define-public crate-all4art-authservice-domain-0.1 (crate (name "all4art-authservice-domain") (vers "0.1.3") (hash "0rwjfix7ysr3n7rcv54islsxxp3x38fxn4560v5gxj6p3b98mypk")))

(define-public crate-all4art-authservice-domain-0.1 (crate (name "all4art-authservice-domain") (vers "0.1.4") (hash "1qb0cp7sbrawm9navqqgb9g644rwrnd4hpi7v2msvhkqkgfmzfs6")))

(define-public crate-all4art-authservice-domain-0.1 (crate (name "all4art-authservice-domain") (vers "0.1.5") (hash "11jc11h29n9jqjd9r8gxsf5qkhmxxrlyjzmx5wv816gj10k5lyb4")))

(define-public crate-all4art-authservice-domain-0.1 (crate (name "all4art-authservice-domain") (vers "0.1.6") (hash "0s1w2c31c3f3ms14wm0bizxxq05p1vf2yhdy6zkazw6y6b356xln")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f7w89msxnc60yw1yl5j2pr84scqj8hgsn6n2r3pk1jnynxlc8kw")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1411w1nmrwxkms5kqz33j8gzcqlrpgs7i8wqblzzqw84i51m1gcr")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.2") (deps (list (crate-dep (name "all4art-authservice-cqrs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rviiw9dpwz8qm63h0k6gik1cpgk4zamcj85ggv35d9jgj9vmqfn")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.3") (deps (list (crate-dep (name "all4art-authservice-cqrs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zkap0yc9qia0ry9lrwkh0c62fcyblpw49ipiyx2gyx8bs204ahr")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.4") (deps (list (crate-dep (name "all4art-authservice-cqrs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f42qlnkxwqfdkjpbvkrvch10fsfjd1fppin9wg42sdhnz43f8d7")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.5") (deps (list (crate-dep (name "all4art-authservice-cqrs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11xp184zvl2yfdr0dvvck5y5njswgvzh67hq8wi6amhxzizbylzy")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.6") (deps (list (crate-dep (name "all4art-authservice-cqrs") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ykm99knjygmbs4ylskym0f35wcrb4mfly14hvn1ff8ld6r31msq")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.7") (deps (list (crate-dep (name "all4art-authservice-cqrs") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "all4art-authservice-domain") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b0ycbpd8ccflirhsh3b45c180lzq3pvwrfhx05r8g5qv57qdvww")))

(define-public crate-all4art-authservice-dto-0.1 (crate (name "all4art-authservice-dto") (vers "0.1.8") (deps (list (crate-dep (name "all4art-authservice-cqrs") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "all4art-authservice-domain") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04z6c8rlyqmfk7wmny96rndzb3nfi0s3s58irxmxmibcifs7dn6l")))

(define-public crate-all4art-authservice-mocks-0.1 (crate (name "all4art-authservice-mocks") (vers "0.1.0") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "15lndqnrw788f2wlcjc53yhv53896ml0q52s9236bmdc6mz56i6p")))

(define-public crate-all4art-authservice-mocks-0.1 (crate (name "all4art-authservice-mocks") (vers "0.1.1") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0dl3mln7wly0pdyyl7iy4bf4mnfbqwzdm1iks22126g5nc9xb59q")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.0") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bv1qpgas8qf1hk8a4plvq64z33l95zvyf42qykw5k7cc7mfbxck")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.1") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a48ff2xglzc3y4qgmxqy5x6nkl6s02lbhkkaslyhfllhjj74z1r")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.2") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0g4smz2fcp1gh6bj55igzdmcfsny9dgg4x2xwcwh1qiw4vkr4hdn")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.3") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0p9sqq2v1dck9bdns1v86gdnv5vxbsx8dvlxrl2hgb826y76xj16")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.4") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "08fizcx6vsfsni2ww8s54i6wzax9dkvgpqs9443374ixnx2kxch1")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.5") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0v489vlm1n0bgn59dghdcw9fs0n9bs1psg30yn0sm24bq90xq4rh")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.6") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)))) (hash "0ayc3spx28g6mgka1ly1j5n502448qr87izhvba6vjc58jvr3gi5")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.7") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)))) (hash "1k72srf17d347sqnlh82i1nl028vm4jd7283fdpk4xyrx47hg8ld")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.8") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)))) (hash "13qra2zb6kwf7bdz6qpinxmignhrd0va87r3m3ydd58avj15s559")))

(define-public crate-all4art-authservice-persistence-0.1 (crate (name "all4art-authservice-persistence") (vers "0.1.9") (deps (list (crate-dep (name "all4art-authservice-domain") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)))) (hash "13b619mrnyh7wb1h66611x8g1w00n10100y284n5yqdkzdkbqpld")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09vav30z3wghdx31ky6dbbh2b73h2fhhwzcalk73jx1z2d5pd8q4")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kylrhq90250yslnjlrp6akmkvw1nv4k5p9pg1ya4iw35imdb4ap")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "19ym15z0j3vlx5x0949ab9zipp0miw9djfvf0kv9nh7gqrcw99sj")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xi7vrc5n0zn1k7ia4jy0w5jp058x94kyf0c8b0qwxqqpwcz2izq")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k498ak65r10k22k1fh4bhkl7xmdj6kf3msrkkabp4dvv48wxjr7")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h7s3m2cjlh24kjvmcxwvgni3hhv6mgjgwgpx0dz0j0i7xv1arqa")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.6") (deps (list (crate-dep (name "jsonwebtoken") (req "^8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h5djzfhrggd32n58l7djdxr9nkcapihzn9r58aa5rwq1synv50k")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.7") (deps (list (crate-dep (name "jsonwebtoken") (req "^8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "08wmsxbrqj7hw29lr5v01mvdhf54f1cp177ap5hgd48waqnpdpjs")))

(define-public crate-all4art-commons-utils-0.1 (crate (name "all4art-commons-utils") (vers "0.1.8") (deps (list (crate-dep (name "jsonwebtoken") (req "^8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mivx08k20ylkiawf13l4yq7qnfbmriq5j2nlhrg3wkx27d1x1s8")))

