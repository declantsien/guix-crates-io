(define-module (crates-io al wa) #:use-module (crates-io))

(define-public crate-always-4 (crate (name "always") (vers "4.0.0") (hash "1wxc5r086j591bwngk1666p3df49zqynaz76wa22ladv24dcibj7")))

(define-public crate-always-assert-0.1 (crate (name "always-assert") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "17c4rwi4n8rjfxw7xwfkc87dsbrc66cwamrx0plkzn99yfbbhmsm") (features (quote (("force"))))))

(define-public crate-always-assert-0.1 (crate (name "always-assert") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0q2snm4qgdwpm49l5pxp2v5q887pi5jn2c8q06lcvh2vikvqcxvj") (features (quote (("force"))))))

(define-public crate-always-assert-0.1 (crate (name "always-assert") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "04ad9wbh70nii1ilcd1mxv85yqx18jf9vsmh3ddps886bmi8ixpv") (features (quote (("force"))))))

(define-public crate-always-assert-0.1 (crate (name "always-assert") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "09w16ab0jg4jimf89br6h7z7ai3hbqh62g4p88dn7fxi58ly0dj4") (features (quote (("force"))))))

(define-public crate-always-assert-0.2 (crate (name "always-assert") (vers "0.2.0") (deps (list (crate-dep (name "tracing") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "14hq43ganc8jwgxlks97f3fnpmwn269as4c65n3v2d0yrshqy1x1") (features (quote (("force"))))))

(define-public crate-always42-0.1 (crate (name "always42") (vers "0.1.0") (hash "1p057qmcla8nbqdng4l7606zmjdzq794lc2idkbp69k3nxqk4q3c")))

(define-public crate-always42-42 (crate (name "always42") (vers "42.1.1") (hash "04wqhn6mysfwywzvdzrkcydrb6fk0iqlcxf4sjr8xp1nnz2hzrx4")))

(define-public crate-always42-42 (crate (name "always42") (vers "42.1.2") (hash "0qkngi850pzvq9dgdkm4wy73h44ynv89cknbv2862i8p7j4qywbs")))

(define-public crate-always42-42 (crate (name "always42") (vers "42.1.3") (hash "1p7z860w72n2wwd9v86mh4bkcnjsbac9ni110x3pyli1ffy3674w")))

(define-public crate-always_42-42 (crate (name "always_42") (vers "42.0.0") (hash "0r81yi0kclr0nlwj1i3x85pq9qfbw4yyjzjn9jxpm1j0fbhjkl4w")))

(define-public crate-always_cell-0.1 (crate (name "always_cell") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nmxl97klz2isdryqh1pbs9vlg4frbc4fc0apzgwyannzb97jj49")))

(define-public crate-always_equal-1 (crate (name "always_equal") (vers "1.0.0") (hash "06pc88fm4bpk5wghs5pizvcpi1fgb853q2flyfzsnl79ac0zprsa")))

(define-public crate-always_equal-1 (crate (name "always_equal") (vers "1.0.1") (hash "1f9hkfbjsq4fl1w50dgxvj41pr6cwar05yxz3rbahczcjk2w4ph3")))

(define-public crate-always_equal-1 (crate (name "always_equal") (vers "1.0.2") (hash "1f42dk6jqjcr888yqi6wadiaw8wwdpvdq0j6g2jikfzmx8f88gw2")))

(define-public crate-always_equal-1 (crate (name "always_equal") (vers "1.0.3") (hash "04j2iqg8k0x8di0y3s9xfrrjcgniywn6xd4lllzkyhabh5xbqhjm")))

