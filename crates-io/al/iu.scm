(define-module (crates-io al iu) #:use-module (crates-io))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.0") (hash "0wrkj2dfyz4hp6cn99jdrq5wxi2x18qd2cb8wjs2bifw8pwsjlh4")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.1") (hash "0ys866h1ziwx2zscgvfarj4xy3fkh36g6c3j3d2wcf62g5z5n4nx")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.2") (hash "1fdwp0sz9fdwvlvcj4ax8f6kzi9wyk1qx3mbgkzqnqf24j408b6c")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.3") (hash "1d2rgk7prcfx46mp4y0rfcriavringwg5ri532wlvzwx474xzzvw")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.4") (hash "0iwq4f5mhbxdqvs54cwpqyb9pg7wv7xkxlwjnsq9l8nqr9zj0fbg")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.5") (hash "1hpbyvldhxmr9m4h68bvr77gpqfiqniy9bzz4rq4xfqzjncf0277")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.6") (hash "0blqipaaaqqqzjp63c5jgi3ray3v5y7yyf135ayqjvbma1psvfma")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.7") (hash "1wpx555mg3yd7hnjdzrbypnbkymchm4474kqj2yc97q8xgxgd61r")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.8") (hash "1zp9m6r35b61qjm7aldjr66d36qjrfs0h3ypfzc548amx5bjbvqz")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.9") (hash "05bb11rdwka1bv9683nlspi1xwsq1ky8ny8mw53016d18wxzkzpr")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.10") (hash "1vnfbc3q7kpi7i7f2irid4k6l7ji3hvxz2xq1dbl93vzng2qb24l")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.11") (hash "1mpc6n3mac67iaamkwppqxgrz14zf6ff96n1yzp44kb504216ry2")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.12") (hash "1dbcgzwggkh2kd4ylq766g4bfr4kx773mqi0d96ri5cncibcic53")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.13") (hash "1q8p2iscfz4c9yjhx0mgn8wb0fqmhixxnnydn3zlqk3qwznfd5i5")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("basetsd" "minwindef" "sysinfoapi" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1l7bdm6w0g5a56rg0xqn51pmcv3gpzdb3v46434lrmk4nbkahb9r")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.15") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("basetsd" "minwindef" "sysinfoapi" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1a33z92k631px0iryvzgfwfhi116zsngnpyw9n30rnqzkapbg807")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.16") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("basetsd" "minwindef" "sysinfoapi" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "02n5xbmky14km8f7qlrynvynf8hsrl339vzgkx952fzgpfjsixly")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.17") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0apfr4sh2lgynp4llvzgz226z2q05d1irab9c30lhcj6yi1w29cl")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.18") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1c2qd9mh704334fja04dvp56xv80327ghdp4yisih6jw33jy6kga")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.19") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1v86s4riigy7nv9jz63jmqpi9pdn6g3bbbj8i7p1n9w6c48w857y")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.20") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1fiflpvizyxxdqmwfjpjsx5bp125zp577xnhpcyr096rp84i6air")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.21") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0l342h06ryk8jk3zbhbik9jkmgbdl8vrkyr2zsd306k4fr3ngkgr")))

(define-public crate-aliu-0.1 (crate (name "aliu") (vers "0.1.22") (deps (list (crate-dep (name "libc") (req "=0.2.107") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "memoryapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "07lx0z19siwpw0sicd3kfrqw4gmy2sj6yrn5sw438p797avd0i9f")))

