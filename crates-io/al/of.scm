(define-module (crates-io al of) #:use-module (crates-io))

(define-public crate-aloft-0.1 (crate (name "aloft") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0bpfs244js6djy4qcf2zziykph7alkwjvs8m5rkiw6a72d3xild9")))

(define-public crate-aloft-0.2 (crate (name "aloft") (vers "0.2.0") (deps (list (crate-dep (name "hyper") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "023y1kvqhq1jxkpczl31pq4zkzabf5556vaajp8vfrl0rkckaand")))

(define-public crate-aloft-0.3 (crate (name "aloft") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1v05wi2ls4bqa7lkwp0ijh2d19d5dz2nh71mqlmk94fdjglmcbq3")))

(define-public crate-aloft-0.3 (crate (name "aloft") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0rjac6v27pjxjag9sx8gj35dls4p5khhpzq804k6lbhll5rps4sk")))

