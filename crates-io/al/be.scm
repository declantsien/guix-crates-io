(define-module (crates-io al be) #:use-module (crates-io))

(define-public crate-albedo-0.0.1 (crate (name "albedo") (vers "0.0.1-beta.0") (deps (list (crate-dep (name "enumflags2") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "glam") (req "^0.11.2") (default-features #t) (kind 2)) (crate-dep (name "gltf") (req "^0.15.2") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23") (features (quote ("hdr"))) (kind 2)) (crate-dep (name "pollster") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wgpu") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "winit") (req "^0.25") (features (quote ("web-sys"))) (default-features #t) (kind 2)))) (hash "0s50cqx66jh5rxxnzckz6z016acl00l433l66cyg9ygag5d3x1ab")))

(define-public crate-albert-0.1 (crate (name "albert") (vers "0.1.0") (hash "1nd371h7yc3y0y7vrc0fb6a0976zjzdcjpa4nmkn17yyqcsjdjm5")))

(define-public crate-albert-0.1 (crate (name "albert") (vers "0.1.5") (hash "0ywr0rv9kgsskp3i9fy07w12q8n8y99l8vz5siwfmppj0jd8f558")))

(define-public crate-albert-0.1 (crate (name "albert") (vers "0.1.6") (hash "0k848b3djblshszsvnbkr6lh2pv9vp45h20hfzxpvwac8s942y8p")))

(define-public crate-albert-0.1 (crate (name "albert") (vers "0.1.7") (hash "1s0cn8r7ddpzzb3ky5dcrarbpgsw3a0rdb1bbg05c7s5xgaf2g97")))

(define-public crate-albert-0.1 (crate (name "albert") (vers "0.1.8") (hash "0xryjlzl87jnfsvk489azbqx80smiq8jyw7jafryx0zxqppma41d") (rust-version "1.56.1")))

(define-public crate-albert_stream-0.0.1 (crate (name "albert_stream") (vers "0.0.1") (hash "1aqfin4hacwrqbm2mfxx8ilxzvp6jf49s2z89bdc6jcqj18vj3a9")))

(define-public crate-albert_stream-0.0.2 (crate (name "albert_stream") (vers "0.0.2") (hash "0blw81iyi81hj0njn1an1lx2wjf3g6f7kpfmg2rlp5s2f37sirr1")))

(define-public crate-albert_stream-0.0.3 (crate (name "albert_stream") (vers "0.0.3") (hash "1063ng9vqllsyp3ggr0i206r51rwb2hasfqbh8zw99wkaqhvcan4")))

(define-public crate-albert_stream-0.0.4 (crate (name "albert_stream") (vers "0.0.4") (hash "1jpi17wdvvwzxh4rmjqcn61ag1xjjpssw4lq6fraw2kn1lp4bsy0")))

(define-public crate-alberto-0.0.1 (crate (name "alberto") (vers "0.0.1") (hash "1642qv8hgysr7n85387vrpggzis0s9wsbzwmfrx0lja0ppx7w5lf")))

