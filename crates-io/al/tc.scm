(define-module (crates-io al tc) #:use-module (crates-io))

(define-public crate-altcoin-bech32-0.11 (crate (name "altcoin-bech32") (vers "0.11.0") (deps (list (crate-dep (name "bech32") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0kjakqln71hlar3k20fzsjfvkynz8dl7m9mc6rfjm66bhj2s6wr7") (features (quote (("strict"))))))

(define-public crate-altcoin-bech32-0.12 (crate (name "altcoin-bech32") (vers "0.12.1") (deps (list (crate-dep (name "bech32") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0lk3482x3ibswjwz9laxfibfw4x7wldbbfdahf43psgd3xps6qgr") (features (quote (("strict"))))))

