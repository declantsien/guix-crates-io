(define-module (crates-io al ot) #:use-module (crates-io))

(define-public crate-alot-0.1 (crate (name "alot") (vers "0.1.0") (hash "103qxcdqvrjzw7ai5c3004zblj0ljm8ylyqk2j7j42p2wi99n455") (rust-version "1.65.0")))

(define-public crate-alot-0.2 (crate (name "alot") (vers "0.2.0") (hash "0jkbd5jys8wp0gql5pkikjvjzqgbka8an432vf9my5ax8d6by4a5") (rust-version "1.65.0")))

(define-public crate-alot-0.3 (crate (name "alot") (vers "0.3.0") (hash "0sa1afxl50l7880fdb4f3fglcnsd7x2cnmfbzxqx7iiaxvxn640g") (rust-version "1.65.0")))

(define-public crate-alot-0.3 (crate (name "alot") (vers "0.3.1") (hash "0aqfn3zjvghwgc6ypz7diw7cmnhisymdpv1d9qay98vk9clgqwmh") (rust-version "1.65.0")))

