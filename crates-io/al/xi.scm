(define-module (crates-io al xi) #:use-module (crates-io))

(define-public crate-alximo-core-0.1 (crate (name "alximo-core") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0gkkfvdgz90250w0yda0frax387zr0a0zbinr4jxszw5lssxldr4")))

(define-public crate-alximo-corr-0.1 (crate (name "alximo-corr") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "02r44g3pfwvfiavwdp9kf0qpdmijprdm07jbysg7q52vh0liim0w")))

(define-public crate-alximo-corr-0.2 (crate (name "alximo-corr") (vers "0.2.0") (deps (list (crate-dep (name "alximo-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "06gk90gq3kwpnymsb64q5i25p4k1v44a50s063m255dnql6s1aqj")))

