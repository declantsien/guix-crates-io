(define-module (crates-io al gt) #:use-module (crates-io))

(define-public crate-algtype-0.1 (crate (name "algtype") (vers "0.1.0") (deps (list (crate-dep (name "algtype_derive") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "11c02n4hs5hhhjll2q9fsy3gbh3fmpd0b39n9wnfw5ml85ia3nzg")))

(define-public crate-algtype_derive-0.1 (crate (name "algtype_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (default-features #t) (kind 0)))) (hash "0y7m4r2b65k94ggdzx1vrg0i7p9iwb415zkqy52zgrxa6j1w4rqw")))

