(define-module (crates-io al og) #:use-module (crates-io))

(define-public crate-alog-0.2 (crate (name "alog") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1sjwnk6vykaqj2f7zbiwz5j0grb8r5jqryb7qmly87q971nza1xx")))

(define-public crate-alog-0.2 (crate (name "alog") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0nwxsx76l0sidamngc0n895jkw5fizhway35bmb7phv2clnc0ch1")))

(define-public crate-alog-0.2 (crate (name "alog") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0q7gaaax33l0qlfmk3nzf100agvgmq44s2jxlxavnkl69bx5fkjj")))

(define-public crate-alog-0.3 (crate (name "alog") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "1lbjhn5s1nqn11z6pajbrnmmcb77rdcgncn5mjnjxz2w40hmi004") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.4 (crate (name "alog") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "012nyl71visss843mmld77w63yadinnybbd5fssg3zdy3vhn1jlk") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.4 (crate (name "alog") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "1b4s9a1yck7bilikqi75r6sgw5s1w5ycb48rkxqfklyhx706pjwc") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.4 (crate (name "alog") (vers "0.4.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "0xd18mb5cfj045nxzg2vfl26b2lyvgb10i4ahrhz9w3n8l1m70p8") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "0bwx326hbyjx1cpvj8kmyk0qlk5vxwmshb5khq217ri6w24mngwj") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "0hsckyjhv96i8p6idakrj4kwmb5jn9gfknzjk0wssxdimj0dr1k4") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "182kazxys8q2kkfb5pszh4is4k4m6la9cwzzhnbzdx1xr0zvm922") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.5") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "1gbqmpv505hsb062qsfqm6v7zp2x9c4y58jwdldcsdd0vhxkjwlq") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.6") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "1rs0pjw2h05mm4vv25gxfinrfl27bx3xw11jwxgbxa5gcyq2hkn4") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.7") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "10yf9kgmchllxqhp10r12ab5sfrjmpbnbrkl23qmipbvp9ibgsyk") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.8") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "0nkaw26r1893pf4h4k2859bap0c71xs2xss2fixrz33jqymmrjp5") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.9") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "0njyv18k642fmhf2l8rkdjrq50x2kxmk3r453kg7wjjkxjrzg49q") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.5 (crate (name "alog") (vers "0.5.10") (deps (list (crate-dep (name "clap") (req "^2.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "1igs0dd4dfymmgjbmnkzixx62k9755amh15vyyzrad3l54q30f77") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6 (crate (name "alog") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (features (quote ("std" "perf-cache"))) (kind 0)))) (hash "0phb2dvsk3nr7lv8b6579h4wyxqvsjvqfrsqrpx5yk6pkacamki7") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6 (crate (name "alog") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "^2.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (features (quote ("std" "perf-cache"))) (kind 0)))) (hash "0yiaqvjcymgljjajgb41dkckxjahyigg4wqsj3ibgkv3qw7nzdaj") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6 (crate (name "alog") (vers "0.6.2") (deps (list (crate-dep (name "clap") (req "^2.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (features (quote ("std" "perf"))) (kind 0)))) (hash "1rch3jhn35hfc47kjsbra3d9jxqdfmznarivgbl6yi34xf9mzsdq") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6 (crate (name "alog") (vers "0.6.3") (deps (list (crate-dep (name "clap") (req "^2.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (features (quote ("std" "perf"))) (kind 0)))) (hash "18bisrxl8i7l1l0i1v9sd9ccmq5hfmbp4fv8dvjnfaznrw62zsvi") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6 (crate (name "alog") (vers "0.6.4") (deps (list (crate-dep (name "clap") (req "^2.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (features (quote ("std" "perf"))) (kind 0)))) (hash "180vr275vxsaf7qvcjx2bndkbk34jrbly77m627zlr3y948v528f") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.6 (crate (name "alog") (vers "0.6.5") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (features (quote ("std" "perf"))) (kind 0)))) (hash "1n8mbyj1ccbs6igm5gzgg65g7ac8x1mj6rx14gs2bix76zsvsdhx") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7 (crate (name "alog") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (features (quote ("std" "perf"))) (kind 0)))) (hash "1kf9qcg0kyd4697yf147zqr21c4yrgsn2ngsp69y3w6dix463qkj") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7 (crate (name "alog") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (features (quote ("std" "perf"))) (kind 0)))) (hash "0vz7aq14nq08sajvra8ml5rz11l1pzvdjc0fxcnvmgs4fjq6xgzj") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7 (crate (name "alog") (vers "0.7.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (features (quote ("std" "perf"))) (kind 0)))) (hash "16wqpnq467nwk59yndyf03hgk4b4a49axahivh39fzzcgm38wknj") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alog-0.7 (crate (name "alog") (vers "0.7.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (features (quote ("std" "perf"))) (kind 0)))) (hash "0c6sfw7sikghngxf60aj4xb4fsm0lgd8kjg7b916jxami96a8vpk") (features (quote (("alog-cli" "clap"))))))

(define-public crate-alogfmt-0.1 (crate (name "alogfmt") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "base16") (req "^0.2.1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "dtoa") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("std"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("std" "derive"))) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11.12") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "0hyqxfy61srv8rn18vmdpwkcg5hmhzfs0i77id06j4mpqj7ck61v")))

