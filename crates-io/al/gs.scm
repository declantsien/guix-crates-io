(define-module (crates-io al gs) #:use-module (crates-io))

(define-public crate-algs-0.1 (crate (name "algs") (vers "0.1.0") (hash "0sf41bkpdybxpy8p2kmwkyxqfgh4p0h476fl6yidg2zxy5c6zpxn")))

(define-public crate-algs-0.1 (crate (name "algs") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "funty") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0nnv86xrxwq4k56i34kmh0scrx63qp7s8y4fg7ibp1p4z5qarklk")))

(define-public crate-algs-0.1 (crate (name "algs") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "funty") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0v5dxgqmxxyxni9mxlp8vpp4ch5cd39j32jixj4615kk1z91jfsa")))

(define-public crate-algs4-0.1 (crate (name "algs4") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "02y7hpnpzkp8q6vk9hq532sih1j9ssk6anm1h6j8317am1r5xwc8")))

(define-public crate-algs4-0.2 (crate (name "algs4") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1b5kixa7x9w0m3hs8yhc2n65cpxszw34zfcnxckv79ab8vsh71mk")))

(define-public crate-algs4-0.3 (crate (name "algs4") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1vzxchj9sjjg4190rdhnhzspdppbapvmy819cwwx5axl5h26wmam")))

(define-public crate-algs4-0.4 (crate (name "algs4") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "13mzisazww8z3a16lghx0548fziad7cv2fmw30m8bg845ibkcfbv")))

(define-public crate-algs4-0.5 (crate (name "algs4") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sdl2_gfx") (req "*") (default-features #t) (kind 2)))) (hash "1wpxz3d4r8v33wnamidyzxzzd7ixmyvmxwj3szpcjy9xnv52k04l")))

(define-public crate-algs4-0.6 (crate (name "algs4") (vers "0.6.0") (deps (list (crate-dep (name "adivon") (req "*") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mtl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "*") (default-features #t) (kind 2)) (crate-dep (name "sdl2_gfx") (req "*") (default-features #t) (kind 2)))) (hash "0n5wwb1ym3zbrmkq6zwr628ls241byg10f2c05kf1z48v28xkmkd")))

(define-public crate-algs4-0.7 (crate (name "algs4") (vers "0.7.0") (deps (list (crate-dep (name "adivon") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mtl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "*") (default-features #t) (kind 2)) (crate-dep (name "sdl2_gfx") (req "*") (default-features #t) (kind 2)))) (hash "1z20swqs4zhnc6xjnk3f656h4sjiifzk9inxjfc33153gj16x6wd")))

