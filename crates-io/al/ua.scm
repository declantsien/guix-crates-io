(define-module (crates-io al ua) #:use-module (crates-io))

(define-public crate-alua-0.1 (crate (name "alua") (vers "0.1.0") (deps (list (crate-dep (name "alua-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0g2nakzrg43awsvdk86215y24l2ah302ffk6n81bg1snsa877jry")))

(define-public crate-alua-0.1 (crate (name "alua") (vers "0.1.1") (deps (list (crate-dep (name "alua-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16gk4yy8grxnzj3f43l5lg5p7gb3yds4k30rnf2z0pgvacz13ssh")))

(define-public crate-alua-0.1 (crate (name "alua") (vers "0.1.2") (deps (list (crate-dep (name "alua-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0j2ygy98cs3aga2b01c71j2z8klrifs4jawvfybc6absx5fhylzl")))

(define-public crate-alua-0.1 (crate (name "alua") (vers "0.1.3") (deps (list (crate-dep (name "alua-macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.9.7") (features (quote ("luajit" "macros"))) (default-features #t) (kind 2)))) (hash "0hv51q4vzd5q0x353hxkgq09hfiwsa817hqgqdz6yg3j8kw2d043") (features (quote (("userdata" "alua-macros/userdata"))))))

(define-public crate-alua-macros-0.1 (crate (name "alua-macros") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "1wyyx2majb01rlx6d2ysx3ca6jsxblf7wqw12vrs1nx7hx9fsv6j")))

(define-public crate-alua-macros-0.1 (crate (name "alua-macros") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "09m5jhqcc29wd2niwqkxk66xb17kf5ng7p9wnyvqf0b2xpvw1nrh") (features (quote (("userdata"))))))

(define-public crate-aluasm-0.1 (crate (name "aluasm") (vers "0.1.0") (deps (list (crate-dep (name "aluvm") (req "^0.4") (features (quote ("all"))) (default-features #t) (kind 0)) (crate-dep (name "amplify") (req "^3.7") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_apfloat") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0aiyypwr5m88xvbr4n9acsshbcv8wx03c2wiw5zw83anzm4ixqf6")))

