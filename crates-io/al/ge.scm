(define-module (crates-io al ge) #:use-module (crates-io))

(define-public crate-algebloat-0.0.0 (crate (name "algebloat") (vers "0.0.0") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.0") (default-features #t) (kind 0)))) (hash "15cm3ynr1hb99fsgzvivvzg9p5zshmyib62z9f04ccjpnfm0abbw")))

(define-public crate-algebloat-0.0.1 (crate (name "algebloat") (vers "0.0.1") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.1") (default-features #t) (kind 0)))) (hash "0rnrhbxkcwq4ffcd7nli99dgzq0m4lwl9q9d7fx4mwnqkbx729yz")))

(define-public crate-algebloat-0.0.2 (crate (name "algebloat") (vers "0.0.2") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.2") (default-features #t) (kind 0)))) (hash "12v2b8ysdcsq6pgajz4m5r07yij6yg113py10c0162i1yyl0y7bc")))

(define-public crate-algebloat-0.0.3 (crate (name "algebloat") (vers "0.0.3") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.3") (default-features #t) (kind 0)))) (hash "0v0kn6mhn4yl5mx2px3znksaci5psgpk20zy92frz9gla06pknah")))

(define-public crate-algebloat-0.0.4 (crate (name "algebloat") (vers "0.0.4") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.4") (default-features #t) (kind 0)))) (hash "15sphv0zhq41my78bfk43b30k8ij69yjv2pzsz9yh3chn3mq7jja")))

(define-public crate-algebloat-0.0.5 (crate (name "algebloat") (vers "0.0.5") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.5") (default-features #t) (kind 0)))) (hash "198h48s09hijp075yf4x44z7y7g610w9wfbxx90pd3ix9q8h1bzz")))

(define-public crate-algebloat-0.0.6 (crate (name "algebloat") (vers "0.0.6") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.6") (default-features #t) (kind 0)))) (hash "1whyvlz2fdzafcwdfjlsxj20swdakvgkx07fwl5qadz910j1vd4l")))

(define-public crate-algebloat-0.0.7 (crate (name "algebloat") (vers "0.0.7") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.7") (default-features #t) (kind 0)))) (hash "0i1f6kn65b2b8kl3d05vxn4q74k9m68vcwz6bycdcbhmq80g01v8")))

(define-public crate-algebloat-0.0.8 (crate (name "algebloat") (vers "0.0.8") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "= 0.3.7") (default-features #t) (kind 2)))) (hash "0ap5m6vxm9dvm2hd3nk82xzfv9r0473mx62c5szzbgcs564gl4hb")))

(define-public crate-algebloat-0.0.10 (crate (name "algebloat") (vers "0.0.10") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "= 0.3.8") (default-features #t) (kind 2)))) (hash "1agwgwzmd7ck6zjxmfny2s9cqcpz14xas03mxyf2g0zhhmx8721m")))

(define-public crate-algebloat-0.0.11 (crate (name "algebloat") (vers "0.0.11") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.11") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "= 0.2.16") (default-features #t) (kind 2)))) (hash "1fkphg75xdm6qsm141ckcn2dplicn3nqhhzvkdazlwny5qdyr026")))

(define-public crate-algebloat-0.0.12 (crate (name "algebloat") (vers "0.0.12") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.12") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "= 0.2.22") (default-features #t) (kind 2)))) (hash "1mdxxf05yq01mhgzvfzalvh58wjfbh8sz17qmgdknhk1kai5n8ac")))

(define-public crate-algebloat-0.0.13 (crate (name "algebloat") (vers "0.0.13") (deps (list (crate-dep (name "algebloat_macros") (req "= 0.0.13") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "= 0.3.1") (default-features #t) (kind 2)))) (hash "0xwyffjalcgi87v0blp9vnnk19inynzgfcrzmkiklhdjijcify44")))

(define-public crate-algebloat_macros-0.0.0 (crate (name "algebloat_macros") (vers "0.0.0") (hash "141d09ylignqvni700f2n9bz8rmkql84pmr14w9rvjrcwvb3r73i")))

(define-public crate-algebloat_macros-0.0.1 (crate (name "algebloat_macros") (vers "0.0.1") (hash "057zh8qdcdh2n3ci2sdvarzq9pa1l9glh5k78xjh8wfzdgfpcbyr")))

(define-public crate-algebloat_macros-0.0.2 (crate (name "algebloat_macros") (vers "0.0.2") (hash "0clxs5nk96995l79hj60lmn0qi9573lqfa11sjlfabk8l60asg6z")))

(define-public crate-algebloat_macros-0.0.3 (crate (name "algebloat_macros") (vers "0.0.3") (hash "1s1q6khz3c0glrjlzyv1ml8zshams2sjc4iydsys8l2b362r00yp")))

(define-public crate-algebloat_macros-0.0.4 (crate (name "algebloat_macros") (vers "0.0.4") (hash "0mwqrw3nnnnrqg6dxwzk628a3dpkz83rif3qafklw1xrhpcll6l7")))

(define-public crate-algebloat_macros-0.0.5 (crate (name "algebloat_macros") (vers "0.0.5") (hash "11jhiq5dzfmqb9r3klgsfc2acr9643606f6hh1w719r235plm0hz")))

(define-public crate-algebloat_macros-0.0.6 (crate (name "algebloat_macros") (vers "0.0.6") (hash "0qs336i8iv5zy8q5gqzfm3f17c0a6pdl0rvgamwcz6g3k4z7y1mm")))

(define-public crate-algebloat_macros-0.0.7 (crate (name "algebloat_macros") (vers "0.0.7") (hash "0cjd1l8qadz2z9hqybsz38zmghknkj61c6npmsq4dk985rs4lkgh")))

(define-public crate-algebloat_macros-0.0.8 (crate (name "algebloat_macros") (vers "0.0.8") (hash "1bsi3z040r1wv2jvgryv9w7kfxps936rbrxwfgcshj6lj8d10rd1")))

(define-public crate-algebloat_macros-0.0.10 (crate (name "algebloat_macros") (vers "0.0.10") (hash "07ls4dkc4klbghh23kjjifs6yhxnh369ssj22vfjccdhdbdmy2i3")))

(define-public crate-algebloat_macros-0.0.11 (crate (name "algebloat_macros") (vers "0.0.11") (hash "03fzgxx6798y5w7gma8bvkfkxdi3w0k677a52nd2habqaxlphax4")))

(define-public crate-algebloat_macros-0.0.12 (crate (name "algebloat_macros") (vers "0.0.12") (hash "14pcrq8720kzr88wzy3g2z9l12ic918d6jlcj38136s5hl30jid2")))

(define-public crate-algebloat_macros-0.0.13 (crate (name "algebloat_macros") (vers "0.0.13") (hash "126vxwr1hfi3z6658a503k4w8vnmdjhja235yif9l5rj1qw10war")))

(define-public crate-algebr-0.1 (crate (name "algebr") (vers "0.1.0") (hash "118y4m190wh06myhcqdliy3w515nfaj905lsb40p93r021af9qk3")))

(define-public crate-algebr-0.1 (crate (name "algebr") (vers "0.1.1") (hash "08waa1c35s06h5fxz4qa1fwc7dipbhh16dllniwcgjbxhjpcay7h")))

(define-public crate-algebra-0.1 (crate (name "algebra") (vers "0.1.0") (hash "1pmlamwcfi6fpgzlx3zg71v5scgjw6k01mvpd88bqaylcddqpl5g")))

(define-public crate-algebra-0.2 (crate (name "algebra") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.2") (default-features #t) (kind 2)))) (hash "0p7mrdj9z47xflfssvf1wmj2vrgarwzpd5svidq40ik45i0j7nrc")))

(define-public crate-algebraic-equation-over-finite-prime-field-0.1 (crate (name "algebraic-equation-over-finite-prime-field") (vers "0.1.0") (deps (list (crate-dep (name "modulo-n-tools") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "polynomial-over-finite-prime-field") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "primal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "ring-algorithm") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "17rm0aiz448fzxlbwakk63pyahd00vk30c0zssfzrg0w5b4f9zdj") (features (quote (("default") ("__internal_inject_debug"))))))

(define-public crate-algebraic-equation-over-finite-prime-field-0.2 (crate (name "algebraic-equation-over-finite-prime-field") (vers "0.2.0") (deps (list (crate-dep (name "modulo-n-tools") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "polynomial-over-finite-prime-field") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "primal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "ring-algorithm") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0f4flrynj3r27i06j7dmx3pc60sqxvrzbwskrpj14sw58iqnbs2m") (features (quote (("default") ("__internal_inject_debug"))))))

(define-public crate-algebraic-equation-over-finite-prime-field-0.2 (crate (name "algebraic-equation-over-finite-prime-field") (vers "0.2.1") (deps (list (crate-dep (name "modulo-n-tools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "polynomial-over-finite-prime-field") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "primal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "ring-algorithm") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1r47qi9h2nn2d4ykx7m6wbqzagkfrv9dsxcifz59g6l62rq7lqqn") (features (quote (("default") ("__internal_inject_debug"))))))

(define-public crate-algebraic-gen-0.1 (crate (name "algebraic-gen") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nmfz7akcdiy0m44wsmbamahjd30z7092bynp6vi0ksfkf6xx089")))

(define-public crate-algebraics-0.0.1 (crate (name "algebraics") (vers "0.0.1") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "04mbjbk6l219vlil7kh9zpn025142rw4aa0bgb760iph3ng4gjjk")))

(define-public crate-algebraics-0.0.2 (crate (name "algebraics") (vers "0.0.2") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0asfiyillklixj5k74il02whxi2b7fsjvypihk3bzhv94h9k4lg4")))

(define-public crate-algebraics-0.1 (crate (name "algebraics") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "081ycrb17z1570srcjhpk1pi67ljz1c4kqr1nj8h3n8sw5d5ycan")))

(define-public crate-algebraics-0.1 (crate (name "algebraics") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13j63qcm4j32kayyvnvwlpp8zkc82dyqd2vlcclas8caqbi348qi")))

(define-public crate-algebraics-0.1 (crate (name "algebraics") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.8.2") (features (quote ("num-bigint"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1n9m5l2f5mgx9z1vpdvbgasklbnsb5bdgy0fvm97bhg7xwsxs8xh") (features (quote (("python-extension" "python" "pyo3/extension-module") ("python" "pyo3") ("default"))))))

(define-public crate-algebraics-0.2 (crate (name "algebraics") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.9.0") (features (quote ("num-bigint"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "16r3vxw2h6pw9c8sy2dk6848qhw65gmv3ihwa1qh904salpfp0bs") (features (quote (("python-extension" "python" "pyo3/extension-module") ("python" "pyo3") ("default"))))))

(define-public crate-algebraics-0.3 (crate (name "algebraics") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.44") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16") (features (quote ("num-bigint"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0sy9sh8yr0311r6vhki2g4g8yw771da1scan4xz49yqld63afyl8") (features (quote (("python-extension" "python" "pyo3/extension-module") ("python" "pyo3") ("default"))))))

(define-public crate-algen-0.1 (crate (name "algen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "0ral5ylcp7mdv82d187q2vfswf24rkx3b22xcc2d9lfnlnwcb1z3")))

(define-public crate-algen-0.1 (crate (name "algen") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "15bcmq47ls79d4v0pz9jb33cbz50lgv5pyf62yb7ri8adcff2bf1")))

(define-public crate-algen-0.1 (crate (name "algen") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "1bbg1fhhridb6k33lv3gm2dihgxrky04y0cg8pkwqsg3mj2z4wvq") (v 2) (features2 (quote (("tracing" "dep:tracing"))))))

(define-public crate-algen-0.2 (crate (name "algen") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "0x5ac79rphs90w9w5idd0b3y22glakbjdysxpm20chs4lmq3jlls") (v 2) (features2 (quote (("tracing" "dep:tracing"))))))

(define-public crate-algeo-0.1 (crate (name "algeo") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "xops") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ps3xvrl9bn4aqqj26sfyxxh5xjzkdqlhhhzzpp1p7gjrpyvdzzx")))

(define-public crate-algexenotation-0.1 (crate (name "algexenotation") (vers "0.1.0") (hash "0gff8m1srxydyxam5skmjahzln3pv07a2c5ackbdgyaxl2812swz")))

(define-public crate-algexenotation-0.2 (crate (name "algexenotation") (vers "0.2.0") (deps (list (crate-dep (name "num-prime") (req "^0.2.0") (features (quote ("big-table"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0mks5c52sbx5bigz1yg6jh3zf8wbawwmwq9q4zi6nhf8zqfyiiwm")))

(define-public crate-algexenotation-0.2 (crate (name "algexenotation") (vers "0.2.1") (deps (list (crate-dep (name "num-prime") (req "^0.2.0") (features (quote ("big-table"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1wypwavh2rzw8pgvfhf4sq2bfdy31f8ymkgmx0zydagwn8fllsaa")))

(define-public crate-algexenotation-0.3 (crate (name "algexenotation") (vers "0.3.0") (deps (list (crate-dep (name "num-prime") (req "^0.2.0") (features (quote ("big-table"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1vikpgm82ilqmrfn829m9c0l293dx4r0ina2yfzn5j7ki3wz4p00")))

(define-public crate-algexenotation-0.4 (crate (name "algexenotation") (vers "0.4.0") (deps (list (crate-dep (name "num-prime") (req "^0.4.3") (features (quote ("big-table"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0bgblslprlkk4yc1nwljxgc17cq4imcb24882wgz6hcb3ix48sj5")))

