(define-module (crates-io al e_) #:use-module (crates-io))

(define-public crate-ale_python_parser-0.1 (crate (name "ale_python_parser") (vers "0.1.0") (deps (list (crate-dep (name "aleph-syntax-tree") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lm4q3s1xjb1fmd7y4ixmk53s4h1z4pf2mcjrxgxizz8vvv6b04f")))

