(define-module (crates-io al em) #:use-module (crates-io))

(define-public crate-alemat-0.2 (crate (name "alemat") (vers "0.2.0") (hash "1ybi3m98qr6hvapz83pavsch2knjbvxk9696xb4j075k893il0jz")))

(define-public crate-alemat-0.3 (crate (name "alemat") (vers "0.3.0") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "07sv76fjiz0vwlgbvixig7xd23k17kxpyvgp1vi58yzca52scp8f")))

(define-public crate-alemat-0.4 (crate (name "alemat") (vers "0.4.0") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0s5f5gw25270brl6lmvl4h3mdcaiz2a63fxfxnwg6wwpx9j00myi")))

(define-public crate-alemat-0.4 (crate (name "alemat") (vers "0.4.1") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0b0r3n0ikr6s3h6vxbmib29ng0pq95crkyk9zay4s1hvixr0ky9d")))

(define-public crate-alemat-0.5 (crate (name "alemat") (vers "0.5.0") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0mp7ck3ivx0cjgpyncj50dz36r8iaza8rypx5yczzg9361xi0baa")))

(define-public crate-alemat-0.5 (crate (name "alemat") (vers "0.5.1") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "171bw27af68cd1ig3rgvc1k3mdc1hg913jwmjcfhy2kkzg8d7jls")))

(define-public crate-alemat-0.6 (crate (name "alemat") (vers "0.6.0") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1yn48sbxjbq1fhp6f5q656y9wl9kkmkw2kv6r5y7h4jy798i4k3a")))

(define-public crate-alemat-0.6 (crate (name "alemat") (vers "0.6.1") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1147ln8z6hp1dg9kfgc672xihd9h7adfld2r2ag0ym4bvfy0qr5k")))

(define-public crate-alemat-0.7 (crate (name "alemat") (vers "0.7.0") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1jslvnqgjk0fkfw2ylaqr1r6g817s7g54lc1fv5c5gj6xyqirj4r")))

(define-public crate-alemat-0.8 (crate (name "alemat") (vers "0.8.0") (deps (list (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)) (crate-dep (name "xmlem") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1ia4bm16jnmizc0drzn9iav4d1lf8nxv67ayq8d18l6lg1q2sznx")))

(define-public crate-alembic-0.1 (crate (name "alembic") (vers "0.1.0") (hash "0v4cx36yiy401mz24a55pync2426052z049i9ysgvxhkjca0772m")))

(define-public crate-alembic-sys-0.0.0 (crate (name "alembic-sys") (vers "0.0.0") (hash "1r332k5f33nvarhb04xyvc5bs8hbmcrvpi251sy2j5jq1gjhgyrb")))

(define-public crate-alembiq-0.0.1 (crate (name "alembiq") (vers "0.0.1") (hash "145wibdr7cg0cqqmxs0gvbnc2mpalsxpg805mb5d6j8llmljryxs")))

