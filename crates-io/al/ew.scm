(define-module (crates-io al ew) #:use-module (crates-io))

(define-public crate-alewife-0.0.1 (crate (name "alewife") (vers "0.0.1") (hash "1lvgdfngpny6xm1wq5cwfik7y54856mviimf2cjvgicvjjgp5231") (yanked #t)))

(define-public crate-alewife-0.0.2 (crate (name "alewife") (vers "0.0.2") (hash "1zbfk196as6gmax4mmg1di8aw45lk4rwrnnsdnclc2yj2pa12pr1") (yanked #t)))

