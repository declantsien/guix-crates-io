(define-module (crates-io al l_) #:use-module (crates-io))

(define-public crate-all_args_string-0.1 (crate (name "all_args_string") (vers "0.1.0") (hash "062lsz3bmrkvbyla9vcv04nknsmy4dl7hd2wiisq0cc17l7dikh4")))

(define-public crate-all_asserts-0.1 (crate (name "all_asserts") (vers "0.1.0") (hash "16m1b3i5z7hi421xryxqgfd8grwppz5b73af2yhylmkrrx74z66y") (yanked #t)))

(define-public crate-all_asserts-0.1 (crate (name "all_asserts") (vers "0.1.1") (hash "1fi2la2si0prm7yhxldwqv6360l2gzykvx8grp6a8bldayylprjw") (yanked #t)))

(define-public crate-all_asserts-0.1 (crate (name "all_asserts") (vers "0.1.2") (hash "1cz885w5r1cjv1r33bwg6yf97jfw3j170xb9hng9rlm3gqgw8qcg") (yanked #t)))

(define-public crate-all_asserts-0.1 (crate (name "all_asserts") (vers "0.1.3") (hash "146w883iljdjc0jih8mmsi1sx26ycwz8m6cdhq34cbhdqxsgisfd")))

(define-public crate-all_asserts-0.1 (crate (name "all_asserts") (vers "0.1.4") (hash "173gmsc4d120b9gkc7p5565xdgcnn5mkhd3znaflnzbq85rb146v")))

(define-public crate-all_asserts-1 (crate (name "all_asserts") (vers "1.0.0") (hash "1vjklzyivgygx9g3bwy5c57sy51jhm6iczxjiwkibpf0l4bw2rqa")))

(define-public crate-all_asserts-1 (crate (name "all_asserts") (vers "1.0.1") (hash "1h845b6lkxi3ivlb4rs8jzdi2hzdhb7caw408j9cj4c8paigblrr")))

(define-public crate-all_asserts-2 (crate (name "all_asserts") (vers "2.0.0") (hash "0r2k7d9xv5pi5fy13xgs2d4djmhjb44hh5jdzjjd7vkdbcgmg56y")))

(define-public crate-all_asserts-2 (crate (name "all_asserts") (vers "2.0.1") (hash "0grrmgw1k42k2bsghv1bhxmbksavx1c3k95qn9y0h5ihmc4qzp4h")))

(define-public crate-all_asserts-2 (crate (name "all_asserts") (vers "2.1.0") (hash "16wx0s5hmcm7qg4irvrpvvzs8xr1hdv3ymn0119yfdy01070pfj6")))

(define-public crate-all_asserts-2 (crate (name "all_asserts") (vers "2.2.0") (hash "0cxj7cnaxhcl5d393lx13lbrajz3jz3nwaqhbjb1a8imi1zi3bnx")))

(define-public crate-all_asserts-2 (crate (name "all_asserts") (vers "2.3.0") (hash "0czn9zfv417hy3nv1cmsy5d45q2j67vmx5qih3xgxb9x0c6yk7sd")))

(define-public crate-all_asserts-2 (crate (name "all_asserts") (vers "2.3.1") (hash "0051psbhdpz4zmm68bjzmmy2p29x6shws0x1rmsc4mqhrbqclxya")))

(define-public crate-all_env-0.1 (crate (name "all_env") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g227gvpi1srb74a04ax142zpib032qnldim6m119mcq24hbiyhs")))

(define-public crate-all_shortest_path-0.1 (crate (name "all_shortest_path") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g75d461gjhixab5d0r3nqy6cwb20p68s60zb7mci10yvgk8mf3b")))

(define-public crate-all_term-0.1 (crate (name "all_term") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "consoleapi" "processenv" "winbase" "handleapi" "ntdef" "impl-default"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0qahxlqnxa5hk3pbhn0fzqwb1jfams9r63azf9gd9h85acvzki43")))

(define-public crate-all_variants-0.1 (crate (name "all_variants") (vers "0.1.0") (deps (list (crate-dep (name "every_variant") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0l8vnhpmg6sr2mcyn8a3j3yd45a72l7ndfjpzhfb1dmh1669lqxy") (yanked #t)))

(define-public crate-all_variants-0.1 (crate (name "all_variants") (vers "0.1.1") (deps (list (crate-dep (name "every_variant") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "075rw9qpvm82s89z2448vsw33wlcz0zg9b0ks5wicpzp4k05byi6") (yanked #t)))

(define-public crate-all_variants-0.1 (crate (name "all_variants") (vers "0.1.2") (deps (list (crate-dep (name "every_variant") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17d6xgii6k985a1qy4jlgn99ldmbih1fvz8vb96kf27mrfwc3jwq") (yanked #t)))

(define-public crate-all_variants-0.1 (crate (name "all_variants") (vers "0.1.3") (deps (list (crate-dep (name "every_variant") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1574vsnrfyfmmmd7rwnc12xy64i4266zg376brh6zypgpgigp9i7") (yanked #t)))

(define-public crate-all_variants-0.1 (crate (name "all_variants") (vers "0.1.4") (deps (list (crate-dep (name "every_variant") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0000cjkac6l78m2mjxmykkx1da5g35c78c44ybr9z7r5mlbm9v4i") (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.0") (deps (list (crate-dep (name "every_variant") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "015y1s8nf0j5r2g503d5gjc7zwggbq5csxa9rbsyk373k11ma986") (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.1") (deps (list (crate-dep (name "every_variant") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1k9whnvcfpnjxzkybgrimygyliwz96jqc0qf02ahryb9bll6l1f1") (features (quote (("ev_heapless" "heapless") ("default")))) (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.2") (deps (list (crate-dep (name "every_variant") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1a1kpbrml2ibh7rigk1j0him23wn1fjrg1w38b6df5z0y7cyxj8j") (features (quote (("ev_heapless" "heapless") ("default")))) (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.3") (deps (list (crate-dep (name "every_variant") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0a9j20285d5ah4nxw0n9xlx819bclqnz136rnir5zbkdcm2pvhwm") (features (quote (("ev_heapless" "heapless") ("default")))) (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.4") (deps (list (crate-dep (name "every_variant") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1pyf8nxskm54w7v7zl3czkibk9l2ngs8b3zdzxrvpdvljq2w7510") (features (quote (("ev_heapless" "heapless") ("default")))) (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.5") (deps (list (crate-dep (name "every_variant") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1sik7b6kzjqn1zc6r62qcgly0s9h92l4d6sgnhypxdfl737idmfm") (features (quote (("ev_heapless" "heapless") ("default")))) (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.6") (deps (list (crate-dep (name "every_variant") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1zbln1axly6r6iwc12c44zicq0h2rcgk3dyvlx6hyg8s5380dgay") (features (quote (("ev_heapless" "heapless") ("default")))) (yanked #t)))

(define-public crate-all_variants-0.2 (crate (name "all_variants") (vers "0.2.8") (deps (list (crate-dep (name "every_variant") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0jdmvh7la02pvd2x9lj6lw2klcjphw4yjhf11nd3vr5sxxbj9v1f") (features (quote (("ev_heapless" "heapless") ("default"))))))

(define-public crate-all_variants-0.3 (crate (name "all_variants") (vers "0.3.0") (hash "0bc6x4gggbkjp4wf71z40wmc70n8mpbnx4pq0nwjn14r818idq6i") (yanked #t)))

(define-public crate-all_variants-0.3 (crate (name "all_variants") (vers "0.3.1") (hash "0vxc3mix2xlvcqvbiggy03qhbal0cihzlma09gxy4a2jx9bdrn0z")))

