(define-module (crates-io al eg) #:use-module (crates-io))

(define-public crate-alegen-0.1 (crate (name "alegen") (vers "0.1.0") (deps (list (crate-dep (name "aleph-syntax-tree") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1birxxf76dccxmzsamcgc7dzpw4h7lhxyip2i4x696l95xkcp6dg")))

