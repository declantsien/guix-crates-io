(define-module (crates-io al ea) #:use-module (crates-io))

(define-public crate-alea-0.1 (crate (name "alea") (vers "0.1.0") (hash "0vfva1b8snm9h3zba90q5gbc3n63nkh7wbw6f34qrg0jv902q4jw")))

(define-public crate-alea-0.2 (crate (name "alea") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1") (default-features #t) (kind 2)))) (hash "1rbnvb4nz9k40wgkxg6a9k9d1rw4sp8x3ni4cpx71ss0jcvwiffy")))

(define-public crate-alea-0.2 (crate (name "alea") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0047qkdxq08gmdzf6n3vy3v6p7fv9cm89pbrniycdrcdnsw97rpd")))

(define-public crate-alea-0.2 (crate (name "alea") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ilkn4xv7an94qx9iafk92b1bihzpcmajdh7sl1chqanb1v6945m")))

(define-public crate-alea-js-0.1 (crate (name "alea-js") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "10ypmd9lsiqp3safwa91ba29xsk346z07mb20yzx381aq4ajxnjh") (yanked #t)))

(define-public crate-alea-js-0.1 (crate (name "alea-js") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "12w4q5yxidffzpgvswr7wav903v6pv4lbdlsc80lgxlprvyz687x")))

