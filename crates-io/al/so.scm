(define-module (crates-io al so) #:use-module (crates-io))

(define-public crate-also-0.1 (crate (name "also") (vers "0.1.0") (hash "1l700ldbdkg027y9ag8k533l526yy5mirgj529ijjrd8y2z63ryv") (features (quote (("std") ("nightly") ("default" "std") ("all" "nightly" "std"))))))

(define-public crate-also-sprach-ami-0.1 (crate (name "also-sprach-ami") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "053rnsfrhm0zw55i4lri881k528xagi7hfisadv262gzfqnyxm9x")))

(define-public crate-also_sync-0.1 (crate (name "also_sync") (vers "0.1.0") (deps (list (crate-dep (name "also_sync_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "async-dns") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "1448284gn9m4sn13h6i3g0ixq7753xslgaa70v4baycna1yjzc0i") (features (quote (("default" "tokio")))) (v 2) (features2 (quote (("tokio" "dep:tokio" "also_sync_macros/tokio"))))))

(define-public crate-also_sync_macros-0.1 (crate (name "also_sync_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0mvndxihk7n3hf0dq15vrv5wbjsz4r9vadnpps0604pkndchnhxf") (features (quote (("tokio") ("default" "tokio"))))))

(define-public crate-also_useless_from_container-0.1 (crate (name "also_useless_from_container") (vers "0.1.0") (hash "0vjx54gibrbhq3qrbq79g2v2prrwrhfwmywf4zwilgq04ipd2pk4")))

(define-public crate-also_useless_from_container-0.2 (crate (name "also_useless_from_container") (vers "0.2.0") (hash "0lw32hlc7bv7j0r7759cbx7yb9pg7x11w4zl4bcmv5q5fwbj8xh8")))

(define-public crate-also_useless_from_container-0.3 (crate (name "also_useless_from_container") (vers "0.3.0") (hash "0k8c5yiq5q4axhbialdgd2kwd05hpha1vmxybm5dd0a86swc3z8w")))

(define-public crate-also_useless_from_container-0.4 (crate (name "also_useless_from_container") (vers "0.4.0") (hash "1x1dg92zcbdxmv4zwad3l6xvp73nfch0dcw6c15f98zvmny4yr2l")))

