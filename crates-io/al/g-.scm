(define-module (crates-io al g-) #:use-module (crates-io))

(define-public crate-alg-grid-0.1 (crate (name "alg-grid") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "map_vec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3") (kind 0)) (crate-dep (name "num-rational") (req "^0.3") (features (quote ("num-bigint"))) (kind 0)))) (hash "1n15wcgw4w2iy1sp6csipssmajdny15z105i21bcs6w5vy5w8l3a") (features (quote (("two_dim") ("three_dim") ("reverse_path") ("default" "two_dim"))))))

(define-public crate-alg-grid-0.1 (crate (name "alg-grid") (vers "0.1.1") (deps (list (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "map_vec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3") (kind 0)) (crate-dep (name "num-rational") (req "^0.3") (features (quote ("num-bigint"))) (kind 0)))) (hash "1bzgsmviwmzpnz26sdqsq83lj46ljynz5p5i07b0skrix92n07kn") (features (quote (("two_dim") ("three_dim") ("reverse_path") ("default" "two_dim"))))))

(define-public crate-alg-rs-0.0.1 (crate (name "alg-rs") (vers "0.0.1") (hash "1q8ghhhhda31q8gvw7jlm24dwkcxiv86lsr70gfl21w39p5a6789")))

(define-public crate-alg-seq-0.0.1 (crate (name "alg-seq") (vers "0.0.1") (hash "1w9f27gyc3xax3876hnah6i6xr5mixafmldi95v712vb0wv8nyrf")))

(define-public crate-alg-seq-0.0.2 (crate (name "alg-seq") (vers "0.0.2") (hash "0dl4lv4xrp9a5j63kv2i36zz088qpjw86c1mi6rds4414n3ar5h4")))

