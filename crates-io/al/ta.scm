(define-module (crates-io al ta) #:use-module (crates-io))

(define-public crate-altair-0.0.1 (crate (name "altair") (vers "0.0.1") (hash "0lng0sczyzzlxjvgds4lybsl9yy0wafkjc36clh5av6wjnpk06jk")))

(define-public crate-altaway-0.1 (crate (name "altaway") (vers "0.1.0") (hash "0gkbsfaqsgy0n31r13f6xcdvvnnrljgkr1r7c7k0hss5wzfc5pd1")))

(define-public crate-altaway-0.1 (crate (name "altaway") (vers "0.1.1") (hash "07sgbp10a243sbw2asldqvj9qya15lm28ly7r97jh39f1iqgpfri")))

(define-public crate-altaway-0.1 (crate (name "altaway") (vers "0.1.2") (hash "05vq3x4aa8zzwhvk8nrlym6kgh5mgz0v4dkcyavsl992v6pznfly")))

(define-public crate-altaway-0.1 (crate (name "altaway") (vers "0.1.3") (hash "0884fkr144x5wg7yw690q5qd8js3q40nxgxcx9x9hz4qlp01synm")))

(define-public crate-altaway-0.1 (crate (name "altaway") (vers "0.1.4") (hash "0p00fa82hgp4hyh050gipixlin396927kd2397ccykbhk862w90f")))

(define-public crate-altaway-0.1 (crate (name "altaway") (vers "0.1.5") (hash "054gf9bcpsl5pfdgv9vqmdynclbyk7aj15l9apmlfchd90vzyn7s")))

(define-public crate-altaway-0.1 (crate (name "altaway") (vers "0.1.7") (hash "0kwikbp6dyqbj70ymjzbgqsj05r6p6520h3f1d4riznapwwhzh2p")))

(define-public crate-altaway-0.2 (crate (name "altaway") (vers "0.2.0") (hash "1wa46v07gg0qz51i14mnhji1pa3y4526p8zb0dj3vklzncjbvd81")))

(define-public crate-altaway-0.2 (crate (name "altaway") (vers "0.2.1") (hash "0xmf73myckp7jv20n4lzwdici8723vclq0aa4v908d5mkxwsnzn7")))

(define-public crate-altaway-0.2 (crate (name "altaway") (vers "0.2.2") (hash "16rhxm0rslhinxc6cc2h8vhflrpvafahpd6c3q1lrdjcdw7h726z")))

(define-public crate-altaway-0.3 (crate (name "altaway") (vers "0.3.0") (hash "094rb9g4857ipcpaapybafs9b0zmq99fs3n6ps8hzydgpckl0al5")))

(define-public crate-altaway-0.3 (crate (name "altaway") (vers "0.3.1") (hash "1ijc6lz0l34vd1xw1ziw0ykiwpmabfhhp591yxixgn9fffhwxy3a")))

(define-public crate-altaway-0.3 (crate (name "altaway") (vers "0.3.2") (hash "0jh6v2fmmfx9yacq4r3i8yig5nrdmwciq9mgdsbad7giwny6xnv3")))

(define-public crate-altaway-0.3 (crate (name "altaway") (vers "0.3.3") (hash "08i0pl7lgn22aylmdwl7084cc3nc9ypxrwlp8117b21zqflbnp0b")))

(define-public crate-altaway-0.3 (crate (name "altaway") (vers "0.3.4") (hash "0xzjwb68rz8y97amrnwp8jq637dsxz36rxps47vsay66r4k3p2jg")))

(define-public crate-altaway-0.3 (crate (name "altaway") (vers "0.3.5") (hash "1zw39wvv5vqczjmzwvzv2vz37r6ls1g9259sdypj287xsdv5khx8")))

