(define-module (crates-io al te) #:use-module (crates-io))

(define-public crate-alter-0.1 (crate (name "alter") (vers "0.1.0") (hash "0svbpjk04wm4bsw6gc4i9glswfh4d3hg0fycraqfyv79a034rq03")))

(define-public crate-alter-ego-0.1 (crate (name "alter-ego") (vers "0.1.0") (deps (list (crate-dep (name "posix-acl") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1lzp9844fz6f62n8wzyhpn7792zq7pwrbfj0fnvcryxj9mlcmckk")))

(define-public crate-alter-ego-0.2 (crate (name "alter-ego") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "posix-acl") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0izirrgqpbkl7hqlqxf8jjv7c83082j6gfbxvjqgcnpgc89qzrii")))

(define-public crate-alter-ego-0.3 (crate (name "alter-ego") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "posix-acl") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^3.1.0") (kind 0)))) (hash "1v5dnyj384bvnrrf87012i89nzfb22j7rryv7qvr6f7gymck8hy8")))

(define-public crate-alterable_logger-1 (crate (name "alterable_logger") (vers "1.0.0") (deps (list (crate-dep (name "arc-swap") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "0js0rvblyf8an4ww5g7c5ja5zzhabqnn2m0ms647vmg8l20jqjli")))

(define-public crate-altered_perception-0.1 (crate (name "altered_perception") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.34") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)))) (hash "09b03rxcvm4vp0gb9dh6xbihaw74kayna17fa6810m42kddqbc9l")))

(define-public crate-altered_perception-0.2 (crate (name "altered_perception") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)))) (hash "1626cvcblkvvd0127bpdvkg94qllrwim25mk908ijvavrya9gpxl")))

(define-public crate-altered_perception-0.3 (crate (name "altered_perception") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)))) (hash "046nvr4r9scp1jhakww85nyfzz1rybc9xz11y5j11cf69ing7hrw")))

(define-public crate-altered_perception-0.3 (crate (name "altered_perception") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)))) (hash "19fnn0p40p11wq16bjdzj1gasxlnnvgbb3pb9jqsnb4v12km5n78") (yanked #t)))

(define-public crate-altered_perception-0.3 (crate (name "altered_perception") (vers "0.3.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)))) (hash "1jp6j15b1lm6xp2nljw32kyxy2nzdqmawdb28l24xngx26jpl5cc")))

(define-public crate-altered_perception-0.4 (crate (name "altered_perception") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)))) (hash "16mwpsyv3dj0597lalk2j3iciw4awhqyxav2qd36an0srmdr2fbi")))

(define-public crate-altered_perception-0.5 (crate (name "altered_perception") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.34") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)))) (hash "16ihsn5dsxfjmvarf0w5awgcfw5j8pvdwmw8s469f3k5m3cbl3ja")))

(define-public crate-alternate-0.0.0 (crate (name "alternate") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "callee") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "portable-pty") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.36") (features (quote ("macros" "formatting"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full" "process" "fs"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("time" "std" "fmt"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.26.2") (default-features #t) (kind 0) (package "ratatui")) (crate-dep (name "tui-term") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1manh9mb7l1p18dw13qzxs0mlbcynrjv47b53p2nvx8qwlrq27gi")))

(define-public crate-alternate-future-0.1 (crate (name "alternate-future") (vers "0.1.0") (hash "0zvkzf34c5gcyd7sw4xy2m7ydvj0pz5pp1qj5ja3zxxb3a2j68aw")))

(define-public crate-alternate-future-0.1 (crate (name "alternate-future") (vers "0.1.1") (hash "0y19xrkawwa3d8ic9whfqqk1kx5c27wacr0crcgmpk31z6s1rl0n")))

(define-public crate-alternate-future-0.1 (crate (name "alternate-future") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jcakv3ban2r7c049wq56dnxnsl20q5z9657a4p75yxk1zb4iinh")))

(define-public crate-alternate-future-0.1 (crate (name "alternate-future") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.3") (default-features #t) (kind 0)))) (hash "0aqrshgbkpwg0lhklk2rzvnfcdsmxabs52139zwcaik3f6blm2fn")))

(define-public crate-alternate-future-0.1 (crate (name "alternate-future") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hy20zwgw25jg13kw1whvamz27x29f1dlfa1gflhs7rjkyg4gr5b")))

(define-public crate-alternate_buffer-0.1 (crate (name "alternate_buffer") (vers "0.1.0") (hash "1rpzywivc25lvdpr2pm2i04jncga62z0r9yzqifalgbmnhc7584s")))

(define-public crate-alternating-iter-0.1 (crate (name "alternating-iter") (vers "0.1.0") (hash "1px8zj3d4p5571sjpjpyqd32nsm7jbcs62217f4ibv3i9s14xl84") (yanked #t)))

(define-public crate-alternating-iter-0.1 (crate (name "alternating-iter") (vers "0.1.1") (hash "0srv7jcgrbn7fkixdxxxhh7pyqg8iki24vlgqd1452yc4p6xikx4") (yanked #t)))

(define-public crate-alternating-iter-0.1 (crate (name "alternating-iter") (vers "0.1.2") (hash "1x0zzgxfwby38bg1vkp8r4yqxx6yf2i12140qf609dm51prcmawv") (yanked #t)))

(define-public crate-alternating-iter-0.2 (crate (name "alternating-iter") (vers "0.2.0") (hash "1kbs6dk03ldnwrbiy1zyhzpczyl32gfmk0klscxjj6qdyjpyw7zn")))

(define-public crate-alternator-0.1 (crate (name "alternator") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "ghost-cell") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nm8j0z1x7g7y0bp2255lww4kkfm1l3lm34x691yws9xz47xqbnh")))

(define-public crate-alternator-0.2 (crate (name "alternator") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "ghost-cell") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 0)))) (hash "129jj7zyvcdqk5z2h95gmh94x0s3zc9p85smhxdl7f3xxrsc88x4")))

(define-public crate-alternator-0.3 (crate (name "alternator") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "ghost-cell") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l72lwalangrnm7i8m1fys6sfi8bxmid5ai9dcd154lx3gqm3qi3")))

