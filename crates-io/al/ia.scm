(define-module (crates-io al ia) #:use-module (crates-io))

(define-public crate-alias-0.1 (crate (name "alias") (vers "0.1.0") (hash "06wjk71w8c62krcmyw606wjrqy8c31sbdxqda1rxd5pvc6ixfav8") (features (quote (("unstable"))))))

(define-public crate-alias-manager-0.1 (crate (name "alias-manager") (vers "0.1.1") (deps (list (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0mwb4shh9q0vj40x3ip6zsniwj494j2g8cjh1rjabj4xf355bx08") (yanked #t)))

(define-public crate-alias-manager-0.1 (crate (name "alias-manager") (vers "0.1.3") (deps (list (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1nyck5lrk5jy1clmawpv3kfshfchn6hzsfb93p1abh95jb2knlh9") (yanked #t)))

(define-public crate-alias-manager-0.1 (crate (name "alias-manager") (vers "0.1.4") (deps (list (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1q4kizfp95qg5ip01ixkc8hv5bk7fs8fc05hqchjjmnqwxf88jdp") (yanked #t)))

(define-public crate-alias-manager-0.1 (crate (name "alias-manager") (vers "0.1.5") (deps (list (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1h4n384r7r97j7kvrs35sk13sn98csjb4h5hv10nwaqdy2swdphb") (yanked #t)))

(define-public crate-alias-ptr-0.1 (crate (name "alias-ptr") (vers "0.1.0") (hash "1vymqkbz4jpn4jnljaiiw2wcz21wf9y9dvjzg5vh05xzpcrc93s1")))

(define-public crate-alias_trie-0.9 (crate (name "alias_trie") (vers "0.9.0") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0pcv31p37px6ia5fmgydz0lw3bscphgnvfb19k7np9chg2zfcy86")))

(define-public crate-alias_trie-0.9 (crate (name "alias_trie") (vers "0.9.1") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0adg0bl7v8sdj3l49mwd0m1d6xnkxqwlsxlrag1hqcbcyzhgr78j")))

(define-public crate-aliasable-0.1 (crate (name "aliasable") (vers "0.1.0") (deps (list (crate-dep (name "aliasable_deref_trait") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.2") (optional #t) (default-features #t) (kind 0)))) (hash "0fcyz60833w3wmbkcnkirdn1iyi4ij36la1671g0ndv2a0maxlbb") (features (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default"))))))

(define-public crate-aliasable-0.1 (crate (name "aliasable") (vers "0.1.1") (deps (list (crate-dep (name "aliasable_deref_trait") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.2") (optional #t) (default-features #t) (kind 0)))) (hash "0r4bkivrnjq1aw8nhhldqfq9j20pj6mzfsnqk3wilpli3a376sww") (features (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default"))))))

(define-public crate-aliasable-0.1 (crate (name "aliasable") (vers "0.1.2") (deps (list (crate-dep (name "aliasable_deref_trait") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.2") (optional #t) (default-features #t) (kind 0)))) (hash "04cyll1ivgfvwzfqj028xbvmxv1s969q42jp5qxmraysf5kg5m1m") (features (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default"))))))

(define-public crate-aliasable-0.1 (crate (name "aliasable") (vers "0.1.3") (deps (list (crate-dep (name "aliasable_deref_trait") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.2") (optional #t) (default-features #t) (kind 0)))) (hash "1z8548zdjlm4ps1k0d7x68lfdyji02crwcc9rw3q3bb106f643r5") (features (quote (("traits" "stable_deref_trait" "aliasable_deref_trait") ("default" "alloc") ("alloc"))))))

(define-public crate-aliasable_deref_trait-0.1 (crate (name "aliasable_deref_trait") (vers "0.1.0") (hash "0wa067v9hkpcsxwvads1f2klnas2lw4v7igz1g02fbbvmjx0afp3") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-aliasable_deref_trait-0.2 (crate (name "aliasable_deref_trait") (vers "0.2.0") (hash "0kjfy3x9b0fmw586m7yr7ibkrs3xzg114244xdk714qn4jsx4wgw") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-aliasable_deref_trait-0.2 (crate (name "aliasable_deref_trait") (vers "0.2.1") (hash "0abzqsxd09jxl01brzjnwk8zg7k8zma76gzq87385q8fqm710jxb") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-aliasable_deref_trait-1 (crate (name "aliasable_deref_trait") (vers "1.0.0") (hash "10hr5zbiwhj5rl6r0lpcb5bashydjrbg9bfnwc1512q70qkvrx9y") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-aliasmethod-0.1 (crate (name "aliasmethod") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pr5ibm97phfdqf4lf5m7ya1ar2gw28pgsjjx5dw2g0s0y5mymk5")))

(define-public crate-aliasmethod-0.2 (crate (name "aliasmethod") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0smyjcpyin0l0dcis2d108am6iwh4n4ja30xi8im8848qrw78kzc")))

(define-public crate-aliasmethod-0.3 (crate (name "aliasmethod") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dlsmk0275gkz8p7wylpjkyd5b2h61msxc9yyz8b5ndvab4gasj5")))

(define-public crate-aliasmethod-0.4 (crate (name "aliasmethod") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1l8r205wsky4zi7iy468gmmyjd9qywryb5f7i4k15cg7983nksij")))

