(define-module (crates-io al ly) #:use-module (crates-io))

(define-public crate-allyaudio-0.1 (crate (name "allyaudio") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1040d86clwlsk2gqnxdbrmg0aqcp845nm9dqbcsrms9s0r4294vd") (yanked #t)))

(define-public crate-allyaudio-0.2 (crate (name "allyaudio") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1znzhbxk848bwz45nr8ingp8g1jh7d9wkjhim9b8zpjx3c51n6g4") (yanked #t)))

(define-public crate-allyaudio-0.2 (crate (name "allyaudio") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "01q52r68yx4kvhpkhq9lqfzzc2dvpsn503ihmxrm94smn4gmbbx6") (yanked #t)))

(define-public crate-allyaudio-0.2 (crate (name "allyaudio") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0fmh73599mimnzdf5h8hgjgghgvc94zr0jk2vxlw2pwnsaw22b0r") (yanked #t)))

(define-public crate-allyaudio-0.3 (crate (name "allyaudio") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (features (quote ("symphonia-all"))) (default-features #t) (kind 0)))) (hash "1mcivwig2h0gnpwwmjcmphhg3is7rdm0730yl5k5jqngz82b0lxj") (yanked #t)))

(define-public crate-allyaudio-0.4 (crate (name "allyaudio") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (features (quote ("symphonia-all"))) (default-features #t) (kind 0)))) (hash "1kgf98d4bzq8il1834clfpjlpqqm53rjly3wp0hkfsz4zk4zpzgy") (yanked #t)))

