(define-module (crates-io al oo) #:use-module (crates-io))

(define-public crate-alookup-0.0.0 (crate (name "alookup") (vers "0.0.0") (hash "0ii7ikmlv3bmg3yzhxdiypxyk44ijc3fj2yccznjrpg2k1c5xxbk")))

(define-public crate-alookup-0.1 (crate (name "alookup") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "resolv") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "17377jjmggwbpgkvybas86iwgycgw0r6dc20gdi11586aabr8fb0")))

(define-public crate-alookup-0.1 (crate (name "alookup") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "resolv") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1s0l8c18jirk1ni68pqqny6xznkjl7i5qpyvr0vxd05wmxz7f16z")))

(define-public crate-alookup-0.2 (crate (name "alookup") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "resolv") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1v8k2f5xnq495gjm7c9092fc66jvc6pvla9fc7a0m6pifwxj7wc5")))

