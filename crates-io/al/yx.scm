(define-module (crates-io al yx) #:use-module (crates-io))

(define-public crate-alyx-0.0.0 (crate (name "alyx") (vers "0.0.0") (hash "1qkk2aqml6xm787031nc4j4apvrnxdglzi2cbim2cjq166lxfppc") (rust-version "1.56")))

(define-public crate-alyx-0.0.1 (crate (name "alyx") (vers "0.0.1") (hash "019dgxjzcadxh98sk8l7shjmhiyb7iywhpqizbc3gx2jw3wyb5mw") (rust-version "1.56")))

(define-public crate-alyx-0.0.2 (crate (name "alyx") (vers "0.0.2") (hash "063f4g66mdwn2fiqs7nvqih2a2wh0cg94j8msfv4saad9z85ldk0") (rust-version "1.56")))

