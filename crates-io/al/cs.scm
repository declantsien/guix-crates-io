(define-module (crates-io al cs) #:use-module (crates-io))

(define-public crate-alcs-0.1 (crate (name "alcs") (vers "0.1.0") (hash "0600lgh14d395zpgj4z95sa1d1qc2fngfc6mhyykwqpv4bgkq2w2")))

(define-public crate-alcs-0.1 (crate (name "alcs") (vers "0.1.1") (hash "18690yd84c2b2m0sh7pgciba44rpp33y77jzz8y2rhv3dhqh9rvw")))

(define-public crate-alcs-0.1 (crate (name "alcs") (vers "0.1.2") (hash "17mf0zlgjpq68nh7i6701d4fgviizdj71bp5dw3bcyagdbrnjh7h")))

(define-public crate-alcs-0.1 (crate (name "alcs") (vers "0.1.3") (hash "0ir4i3pd5z2zahkf125am0873j45shz80yj2sy4p85qnkx3fq4gl")))

