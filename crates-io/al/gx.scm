(define-module (crates-io al gx) #:use-module (crates-io))

(define-public crate-algx-0.1 (crate (name "algx") (vers "0.1.0") (hash "0zn4xfk0y0m8idbwbjwj1yqnmcvjmlraj942460npn0q4p2sxi1i")))

(define-public crate-algx-0.2 (crate (name "algx") (vers "0.2.0") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "076dmhvkmcx911caxm7m0vicg3inrh3mp2rshgmgcq9slvr1lapw")))

