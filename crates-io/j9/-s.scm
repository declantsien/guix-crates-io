(define-module (crates-io j9 -s) #:use-module (crates-io))

(define-public crate-j9-sys-0.1 (crate (name "j9-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 1)) (crate-dep (name "autotools") (req "^0.2.6") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "1qsgfbx3j1s3vn0jii59j1jg3k3prni6935zrky15zfakrw4hcvs")))

(define-public crate-j9-sys-0.1 (crate (name "j9-sys") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 1)) (crate-dep (name "autotools") (req "^0.2.6") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "1kvvy0vyb1pvvxwmhn3bgds627b222sspx7q31v3rl98j2nj0phg")))

(define-public crate-j9-sys-0.1 (crate (name "j9-sys") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 1)) (crate-dep (name "autotools") (req "^0.2.6") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "00a0p89lanvinw0jh7lm82b7qdhr24n4d2p84gi9rv7x2b35g1if")))

(define-public crate-j9-sys-0.1 (crate (name "j9-sys") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 1)) (crate-dep (name "autotools") (req "^0.2.6") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "1z8agprh04dbmx68ihrsjcfhiyfj6z9bw50n37j5c6wdbyfam37y")))

