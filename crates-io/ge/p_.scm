(define-module (crates-io ge p_) #:use-module (crates-io))

(define-public crate-gep_toolkit-0.1 (crate (name "gep_toolkit") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "01q9daj6agjj427w4d6dy54m201ni8d6vcrzzjy9jc7k8rnb2pz7")))

(define-public crate-gep_toolkit-0.2 (crate (name "gep_toolkit") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.142") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0822mz95a4zd1498hpvr8j1b2ys4a0xyq1p9v4c0yqz77q3iqg9y")))

(define-public crate-gep_toolkit-0.2 (crate (name "gep_toolkit") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.142") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "01gkv4s8s8mfrcl6zlljpjgizqxl2l5kgnglxi88nzhkqjx1v7j1")))

(define-public crate-gep_toolkit-0.2 (crate (name "gep_toolkit") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.142") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13ymfwp1mpfg5ny869q60v8yc5l1yv2dmdsilfhs4zqgqia05fdw")))

(define-public crate-gep_toolkit-0.2 (crate (name "gep_toolkit") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.142") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ky6mabl92kfr77dx2npsk8in50h238qzrrxvaddl2rf50g2a35n")))

