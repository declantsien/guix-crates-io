(define-module (crates-io ge th) #:use-module (crates-io))

(define-public crate-gethostid-rs-0.1 (crate (name "gethostid-rs") (vers "0.1.0") (deps (list (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)))) (hash "0xlhj9mqz74alb6p5nr82hfm27cml0x0iv9f9x9q58nnr0bqn4fs")))

(define-public crate-gethostid-rs-0.1 (crate (name "gethostid-rs") (vers "0.1.1") (deps (list (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)))) (hash "1fzmdykgzfjj89b0dms6qq32x1w87bajx7hcbdxmrna015xif419")))

(define-public crate-gethostname-0.1 (crate (name "gethostname") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)))) (hash "0pxirqbr5lz7j4r2b7gylf0yypj82kmj5qakrqv6xwz1gjlsavvl")))

(define-public crate-gethostname-0.2 (crate (name "gethostname") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "07g0wbx45qrbah6c36mnblya8nday763fn5i835bc7m3l8y2gayl")))

(define-public crate-gethostname-0.2 (crate (name "gethostname") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0a609j9dhk816il2f2a01avvi5sqzxh0p38nxwrja7dcpybf54p6")))

(define-public crate-gethostname-0.2 (crate (name "gethostname") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1lals1bchjd9ghd9xqrr5cnp853rvfxmsh2cfxkd0li8jdjc3paa")))

(define-public crate-gethostname-0.2 (crate (name "gethostname") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0bl6wf7zclzmjriipzh98rr84xv2ilj664z8ffxh0vn46m7d7sy1")))

(define-public crate-gethostname-0.3 (crate (name "gethostname") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("sysinfoapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0xy1wbx5k2bzi4cbaqj9wqgqsbn4f8pm6nsm1d86mibk66xd8rdv")))

(define-public crate-gethostname-0.4 (crate (name "gethostname") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0gw6v5r25dm7xss8y8ymxwcv1dhlziangzrxdm4x3l1y71kyrqhm") (rust-version "1.64")))

(define-public crate-gethostname-0.4 (crate (name "gethostname") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0114bh2p97xi9ygxgpq0wgm0m5bv7nia8ff6s8sqpmvdhqi9wcla") (rust-version "1.64")))

(define-public crate-gethostname-0.4 (crate (name "gethostname") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1pc72k1ivpg438v6jb8xlw9h6dmms95qqpki81v48axgf2x2758r") (rust-version "1.64")))

(define-public crate-gethostname-0.4 (crate (name "gethostname") (vers "0.4.3") (deps (list (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows-targets") (req "^0.48") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "063qqhznyckwx9n4z4xrmdv10s0fi6kbr17r6bi1yjifki2y0xh1") (rust-version "1.64")))

