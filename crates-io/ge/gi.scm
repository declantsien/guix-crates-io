(define-module (crates-io ge gi) #:use-module (crates-io))

(define-public crate-gegit-1 (crate (name "gegit") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pndpacx7wq8jgqajcxgvzbqai3fg7jyc3nfpq8r59v8s6dp058i")))

(define-public crate-gegit-1 (crate (name "gegit") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jkm1npmvfl1bgnx02v8la8mfizp3vb2w6xjyxgk37bf0r4d4ci6")))

