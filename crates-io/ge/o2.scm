(define-module (crates-io ge o2) #:use-module (crates-io))

(define-public crate-geo2city-0.1 (crate (name "geo2city") (vers "0.1.0") (deps (list (crate-dep (name "reverse_geocoder") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0ir90is7l8jlkfzga4cs3ymrzv21ry5930qm7chz8nnqsr6fsiba")))

