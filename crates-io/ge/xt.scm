(define-module (crates-io ge xt) #:use-module (crates-io))

(define-public crate-gext-0.1 (crate (name "gext") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)))) (hash "0f62n10lqycm0985xiqydb6ycqzinr3lchgyhwgs3557nrsbc8bs")))

(define-public crate-gext-0.1 (crate (name "gext") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)))) (hash "1m6k2qvrb673md5s9qf5ynihcp1qbbhrbcf5imxkpwsrcffc9mkz")))

