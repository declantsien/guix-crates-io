(define-module (crates-io ge ti) #:use-module (crates-io))

(define-public crate-geticons-0.1 (crate (name "geticons") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linicon") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (kind 0)))) (hash "0kycfc5mik6510i2lhdvdnl52xq75prvdhw9a0ayzdyf9lbcpbiz")))

(define-public crate-geticons-1 (crate (name "geticons") (vers "1.0.0") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linicon") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (kind 0)))) (hash "0i94nlpj6iqh9cci633661y8nw6bwzjcn2bdmsfy2pr590b5ifhn")))

(define-public crate-geticons-1 (crate (name "geticons") (vers "1.1.0") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linicon") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (kind 0)))) (hash "08259l7sg9rfsa9g18wk1d1l9zsra68y1y89411vjfg9bjzxgm84")))

(define-public crate-geticons-1 (crate (name "geticons") (vers "1.2.0") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linicon") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (kind 0)))) (hash "1q70j75cppg8z9cyfcpqgxrfazzcwn4xxhgxd5864s98dghvx4pf")))

(define-public crate-geticons-1 (crate (name "geticons") (vers "1.2.1") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linicon") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (kind 0)))) (hash "1qmmfp41sznblqjlp0x8697vvdzzqf3va4ls841xwnccg2w0rgmy")))

(define-public crate-geticons-1 (crate (name "geticons") (vers "1.2.2") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linicon") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.9") (kind 0)))) (hash "0pim6wn0yg6dva7yjcyhxgj2saa6vahw8mwpvly3zkqxh9v3g2kv")))

(define-public crate-getid-0.1 (crate (name "getid") (vers "0.1.0") (deps (list (crate-dep (name "cuid") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.2") (features (quote ("eq-separator"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0zh0n5wky39v3v05f7qixfjmgn0famz2hxdras8p7gxgmrym0gli")))

(define-public crate-getid-0.2 (crate (name "getid") (vers "0.2.0") (deps (list (crate-dep (name "cuid") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.2") (features (quote ("eq-separator"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1lah7m7wb3zhiqk5ay64ivxpml4s83ij0s3imdfl4qhg7k03v0fl")))

(define-public crate-getid-0.3 (crate (name "getid") (vers "0.3.0") (deps (list (crate-dep (name "cuid") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.2") (features (quote ("eq-separator"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1dd9xs87kjm3fxkh13ik8m4h0gz0bcfxcddjcnwvb9zrzfvnys9h")))

(define-public crate-getid-0.4 (crate (name "getid") (vers "0.4.0") (deps (list (crate-dep (name "cuid") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.2") (features (quote ("eq-separator"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1i9cynrl60r1lxs8nzyvhjw7awiiq94a528xfvhjln9pav097wgp")))

(define-public crate-getid-0.4 (crate (name "getid") (vers "0.4.1") (deps (list (crate-dep (name "cuid") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.2") (features (quote ("eq-separator"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0lapp56aisw9zy61ycr8r5y6bg9pszj2g8javc3cc25vsv3q9pmr")))

(define-public crate-getignore-0.1 (crate (name "getignore") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.44") (default-features #t) (kind 0)))) (hash "1dzx5qhx0nj7b6vvx2swa4zj5hywjazr9q4drkbzsf69b3vrcly2") (yanked #t)))

(define-public crate-getignore-0.1 (crate (name "getignore") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.44") (default-features #t) (kind 0)))) (hash "1agv7r8gkl39bpacy0gl0m9car6j7nkm7g1gnz2bk08500ww1mq6") (yanked #t)))

(define-public crate-getignore-0.1 (crate (name "getignore") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.44") (default-features #t) (kind 0)))) (hash "1hgsjlaqjra2jxwy9sis92c5xdbjsi1wmmrlvki5f3h9lh29p44r") (yanked #t)))

(define-public crate-getimg-0.0.1 (crate (name "getimg") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nix56dj5bczjk7p8r0qj2yy8fv8jfwrv5dr149sb2689n373f15") (features (quote (("cli" "clap"))))))

(define-public crate-getin-0.1 (crate (name "getin") (vers "0.1.0") (hash "0szgj7b2k982yxyx6k15js3as400vqxxxfg8qcm11spfddbv99hb")))

(define-public crate-getin-0.1 (crate (name "getin") (vers "0.1.1") (hash "0p4h78wddvmhrmiziqvydvdwfn40wy40cjic5y7c4r05z2ihgp4a")))

