(define-module (crates-io ge nt) #:use-module (crates-io))

(define-public crate-gent-0.1 (crate (name "gent") (vers "0.1.0") (hash "0q7hhr1jhmkp9mwydg3zxpna3c983x5vsawlg21icvg2hc27wjl1")))

(define-public crate-gentex-0.0.1 (crate (name "gentex") (vers "0.0.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0wsnvlwpj5pfwaykrlrxc7l0kbqdfp50lk5kgrispgffq4l4zfps") (features (quote (("tikz") ("default" "tikz"))))))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.0") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1dd27ij00wcdjrlha0ps76lkc55h5hxndhkdzq7cjbv4i95jq20h") (yanked #t)))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.1") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0w2jmd9xxgybmv6xbvshchj4yq9dd8pxy8mmi6h3s45ip5zly8jv")))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.2") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0pbx00di14q0b2kpqhf5sfwm6gzm376qsmcgfckrn7nbgm2cfmgj")))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.3") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1iklf7dbq69yjk6ml4gz8l6ly078a0wykmqjr9hrwi97i4wcwyy0")))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.4") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "04p3rs6i4gn0rlijqxpr2854yvkcmc77szk2lkiap5kvc0k180cp")))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.5") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0mi0da5z7nbaq5v3wfwnbg0akcpjvsrcrwxagis575kkynb1r64c")))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.6") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1igg8xvlxnzdlf57qlzba198xsd72jvc4grq0c6sgibylrnkvr6f")))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.7") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "053dj8cb7f6k8d4s881qyplhjj8jp1150az55q32hbbdirddkd7d")))

(define-public crate-gentian-0.1 (crate (name "gentian") (vers "0.1.8") (deps (list (crate-dep (name "bae") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "066fvy4w6mfc1rdffxcicmryqnlqfdmpxxar4ka0b2fxmnsqia23") (features (quote (("default" "co_await") ("co_await"))))))

(define-public crate-gentoo-cleaner-0.1 (crate (name "gentoo-cleaner") (vers "0.1.0") (deps (list (crate-dep (name "byte-unit") (req "^4.0.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (features (quote ("acct"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive" "strum_macros"))) (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "uname") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1cachvcwhip0yrljlhckvwmmg61g0vbm2830hp35rbaqxlfqxhdk")))

(define-public crate-gentoo-cruft-1 (crate (name "gentoo-cruft") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.10") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "fs-tree") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "0hckx4ks2iz58imqyf41jj0ab0zhfkswrq8mrw7r06gl7nbrbr9v")))

(define-public crate-gentoo-cruft-1 (crate (name "gentoo-cruft") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.10") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "fs-tree") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "1kcj899ni2rsiik8xj85akpr73krk76wd9mvbdq6sahly5a2d476")))

(define-public crate-gentoo-cruft-1 (crate (name "gentoo-cruft") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.10") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "fs-tree") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "059dxljjxrrh04csp0rhb3r33iv829vs2a11xi0gjhld9k7ziz33")))

(define-public crate-gentoo-cruft-1 (crate (name "gentoo-cruft") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.10") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0gpb8d8hvdfwdriyrrgcgfa2wivhl8nay75i7jihzc3va9iaw3kj")))

(define-public crate-gentoo-cruft-1 (crate (name "gentoo-cruft") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.10") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0zifi2nkn4wsrdij7bs4g09r1p32l66nv1n2jqp1hpps0jgxqpix")))

(define-public crate-gentoo-cruft-1 (crate (name "gentoo-cruft") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.13") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "threadpool") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0xqy6w8i7kgknwlya2p9h9bjdrgw5x39ib861kmg16dkllpkk4a8")))

(define-public crate-gentrix-0.1 (crate (name "gentrix") (vers "0.1.0") (hash "0mqj3a1gbk0cwnhwr33v1q4jiw1pxxcm7w3mrp3vy6qc64zz19gj")))

(define-public crate-gentrix-0.1 (crate (name "gentrix") (vers "0.1.1") (hash "1syz69kyls6nkp1rb0kd3g595877lxq50781dlbv60sz8kc7nzs5")))

(define-public crate-gentrix-0.1 (crate (name "gentrix") (vers "0.1.2") (hash "1c8a40zv666ajwrxi8868bn4kay3zdw3lmqxr56dr9zgj1263hvh")))

(define-public crate-gentrix-0.1 (crate (name "gentrix") (vers "0.1.3") (hash "0m5xx1if0xid1yyrjgiad6bkxngsrvaw28pnwpb3bvnbj8zmgg60")))

(define-public crate-gentrix-0.1 (crate (name "gentrix") (vers "0.1.4") (hash "113pqz4vssbly7rb6mzr7hl3md1diryfyarqmsarp10fyl4r54lc")))

(define-public crate-gents-0.1 (crate (name "gents") (vers "0.1.0") (deps (list (crate-dep (name "gents_derives") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y3h8pnah3p3418knrngnf4hv5in4fbbxziic4mga9af675gcl2f")))

(define-public crate-gents-0.1 (crate (name "gents") (vers "0.1.1") (deps (list (crate-dep (name "gents_derives") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02hw8yizwcz347bsmasfd6bzlfs0p2j4r8jr1ayglqfvn340g5vv")))

(define-public crate-gents-0.1 (crate (name "gents") (vers "0.1.2") (deps (list (crate-dep (name "gents_derives") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01r9cgkpywhyj6m3xk0ra67p8zsnp7wavgbbjng2wyd9y8i74h8s")))

(define-public crate-gents-0.2 (crate (name "gents") (vers "0.2.0") (deps (list (crate-dep (name "gents_derives") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09qi4p4xzcsx24f1bhxvbyhna2pf2fi41ifvsb1z9ciqgjy1vxzl")))

(define-public crate-gents-0.3 (crate (name "gents") (vers "0.3.0") (deps (list (crate-dep (name "gents_derives") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "05cm0ayn4621xhk88l5hmr9ws0dgrjlxgsnns2qyzp63qwmvaslw")))

(define-public crate-gents-0.3 (crate (name "gents") (vers "0.3.1") (deps (list (crate-dep (name "gents_derives") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1yh6bx75yi9p87llg42b8fq6zdzrh1khfv4i4pwcn3drmhgi940g")))

(define-public crate-gents-0.3 (crate (name "gents") (vers "0.3.2") (deps (list (crate-dep (name "gents_derives") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1q2x14grbkjg4xivcgpjjd04sxqkaj6mngd7bhz1x7hl7n1bziyi")))

(define-public crate-gents-0.4 (crate (name "gents") (vers "0.4.0") (deps (list (crate-dep (name "gents_derives") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "15hrrxg7yzsgdwj74k8k7ljzz7k8bmvkv6jiw4ssbmwp7yw4j4lf")))

(define-public crate-gents-0.4 (crate (name "gents") (vers "0.4.1") (deps (list (crate-dep (name "gents_derives") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0gq68k520rsjzgis2x7agm4dkp0qdcrkkc5zpl3yxc55adkb6jb1")))

(define-public crate-gents-0.5 (crate (name "gents") (vers "0.5.0") (deps (list (crate-dep (name "gents_derives") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "0i7n9jhwagrv7d1b33rk1l10ayrfq16sy68fkzwbdzvjwj0pm5l3")))

(define-public crate-gents-0.6 (crate (name "gents") (vers "0.6.0") (deps (list (crate-dep (name "gents_derives") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "040mhgdm6422kfli5zr9bwggjbgfpv763drrgsni6c86zjghb9yn")))

(define-public crate-gents-0.7 (crate (name "gents") (vers "0.7.0") (deps (list (crate-dep (name "gents_derives") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0613ixp5kijs8rw5924zcmfp9hmk62n80kk5mgzbjmn0q3cxnch2")))

(define-public crate-gents-0.8 (crate (name "gents") (vers "0.8.0") (deps (list (crate-dep (name "gents_derives") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0n7cqmflh094g32mz7cz180r6bh77jaiki9kx49p3cmlnvwz5czp")))

(define-public crate-gents_derives-0.1 (crate (name "gents_derives") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cyb9h9cnd082ss3pj165r9njif0pzz20avpmcy7z01949dgkfm6")))

(define-public crate-gents_derives-0.3 (crate (name "gents_derives") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lr8n68bnskf7kac22npxvrfbx0acrmy5d3iv1jyvrnh5znd5vgf")))

(define-public crate-gents_derives-0.3 (crate (name "gents_derives") (vers "0.3.1") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19jw645liasmiwlc6b3pvdcl2f30k7h07ccpxxc3dkn11gcdfbmp")))

(define-public crate-gents_derives-0.3 (crate (name "gents_derives") (vers "0.3.2") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03id519ld1zb356zr2b3lv3ip8s8s1w4b7b8r7v56d7zbbirkffh")))

(define-public crate-gents_derives-0.4 (crate (name "gents_derives") (vers "0.4.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1d29h9vvqdbkskw9r9yclr400s81710xr7ng3ccgm3zakp9k08bx")))

(define-public crate-gents_derives-0.4 (crate (name "gents_derives") (vers "0.4.1") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c0a5i84cq9ww57py9hazbd2xd2wga79kdwvcb900p17nfxg2qf0")))

(define-public crate-gents_derives-0.5 (crate (name "gents_derives") (vers "0.5.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jy411jgiccxikqm00r2vk9v707m6q5svn6k5d18py4pipf78lcc")))

(define-public crate-gents_derives-0.6 (crate (name "gents_derives") (vers "0.6.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lb24mkfcnb9yxc21p7pwrwqb0khsywcd2g67548z7m8wakdi808")))

(define-public crate-gents_derives-0.7 (crate (name "gents_derives") (vers "0.7.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a1fvw80dc56z6pyggmwy88mcvpl1s0wpymldj7hkjng9dkprg43")))

(define-public crate-gents_derives-0.8 (crate (name "gents_derives") (vers "0.8.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05hslzfpiawxg99k33f0kwy7qng5y1ayn0wcdjp6nkg079bqg37g")))

