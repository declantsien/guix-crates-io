(define-module (crates-io ge qs) #:use-module (crates-io))

(define-public crate-geqslib-0.1 (crate (name "geqslib") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "gmatlib") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "14b1mk99b695zakxpfk7zxm32mqc4jaz87cnylcmzd3s2fvsc6nx")))

(define-public crate-geqslib-0.1 (crate (name "geqslib") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "gmatlib") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1nssxxwfhphlnxbhzp5f2vrg3kcwz1hxwrq15a4pdd1k3a641l9x")))

(define-public crate-geqslib-0.1 (crate (name "geqslib") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "gmatlib") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0klkh8z1zrm82ymjnvm2iyk6wxvn55gda3ay5xqk77bfwfcwnws4")))

(define-public crate-geqslib-0.1 (crate (name "geqslib") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "gmatlib") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1sjhclarzhgzykag486gwwj73l9h7b4w8icyam0s83q5py642ldl")))

