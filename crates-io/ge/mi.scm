(define-module (crates-io ge mi) #:use-module (crates-io))

(define-public crate-gemina-0.1 (crate (name "gemina") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "cbc") (req "^0.1.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11") (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1nrnnilrl5q9jgspwsc9bfg7gi62dy5wi0hxyp8cmsbhslvfmiqj") (yanked #t)))

(define-public crate-gemina-0.1 (crate (name "gemina") (vers "0.1.1") (deps (list (crate-dep (name "aes") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "cbc") (req "^0.1.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11") (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "13h388n707q8w9zggg6yjhpli69lar4sk7gxknb2ipf0nr0da0la") (yanked #t)))

(define-public crate-gemina-0.2 (crate (name "gemina") (vers "0.2.0") (deps (list (crate-dep (name "aes") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "cbc") (req "^0.1.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11") (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0xd839klm7502ag1v90gjr70b7g9ay0l324i565k7s7hdzfjcdix")))

(define-public crate-gemini-0.0.1 (crate (name "gemini") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "clone-impls" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1pjg8mknhyckqj0dr7xzqskw929vf0a8cl1krsbjmzxsyr9bq1p4") (features (quote (("sync") ("default")))) (yanked #t)))

(define-public crate-gemini-0.0.2 (crate (name "gemini") (vers "0.0.2") (deps (list (crate-dep (name "url") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1zkfimcm30r83h8500cki8llpp81rfbjc5ffpaf2xjjs8fawf7fz")))

(define-public crate-gemini-0.0.3 (crate (name "gemini") (vers "0.0.3") (deps (list (crate-dep (name "nom") (req "^6.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1yviji78f104k0kn1wxdvf6fwm84qid75jkcjrbkqvrwjrjb7ds7") (features (quote (("parsers" "nom" "paste") ("default" "parsers"))))))

(define-public crate-gemini-0.0.4 (crate (name "gemini") (vers "0.0.4") (deps (list (crate-dep (name "nom") (req "^6.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "075hhvxbcz06cd4lkilylz3ssvnv3hc404ikzpgxqzlglc5qa4hn") (features (quote (("parsers" "nom" "paste") ("default" "parsers"))))))

(define-public crate-gemini-0.0.5 (crate (name "gemini") (vers "0.0.5") (deps (list (crate-dep (name "nom") (req "^6.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1q6lphyiawxqfcpy33sw4w4m17bmgg8whkcl6hxl6vxcm7sj1a0j") (features (quote (("parsers" "nom" "paste") ("default" "parsers"))))))

(define-public crate-gemini-engine-0.1 (crate (name "gemini-engine") (vers "0.1.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xics3xmzycw2i5asfxa9qpgfvxz6ri1m9r6mskkpbpsxvbin9p9")))

(define-public crate-gemini-engine-0.1 (crate (name "gemini-engine") (vers "0.1.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hcbdzadja227wky6w93xpbng1bc9zzmqazj1jxc56446hzr16fz")))

(define-public crate-gemini-engine-0.2 (crate (name "gemini-engine") (vers "0.2.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vdmkw7ls471lc59qkzg7ypq80bp36wkg77p9x4b99lcyqcp1nlb")))

(define-public crate-gemini-engine-0.2 (crate (name "gemini-engine") (vers "0.2.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "11clsc5738nfidgc3van5ka1953gdhb91zanm9xf03njf968ymz6")))

(define-public crate-gemini-engine-0.3 (crate (name "gemini-engine") (vers "0.3.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0znj8psnqril37c6zz3kpa2plzcgzzhrnlrzq6dqfkmj2j0zk9r4")))

(define-public crate-gemini-engine-0.3 (crate (name "gemini-engine") (vers "0.3.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0arnnrddws97yh5c1jmhjpkhq56w9f9c4vcxb7plnfvmim6mi54l")))

(define-public crate-gemini-engine-0.3 (crate (name "gemini-engine") (vers "0.3.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0whpng1jw5b8yryl9rjj2p8r9fdp3cs54lfks35sh9092p7swrcb")))

(define-public crate-gemini-engine-0.3 (crate (name "gemini-engine") (vers "0.3.3") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "04cc8p7b5ivi94dg778v9lf7sq0h76fz0ndbgdddfl2m1rv8c53q")))

(define-public crate-gemini-engine-0.4 (crate (name "gemini-engine") (vers "0.4.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "09rjk1hxw5ag9j37kszs5k1nisxpmlmmfds70rfsdr03cxk1ibyl")))

(define-public crate-gemini-engine-0.4 (crate (name "gemini-engine") (vers "0.4.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "137x15107p23p38q8rp5a51gx63qbh1mck94kbnx3bfg24kf18gp")))

(define-public crate-gemini-engine-0.4 (crate (name "gemini-engine") (vers "0.4.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0276fg0y1vrcqg91p4i9gklj5h3axs2r66ayqd6v6i0qdwyx59v9")))

(define-public crate-gemini-engine-0.5 (crate (name "gemini-engine") (vers "0.5.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bz1n0gw52diq7hzm5b82xf562lrl5g2wsw5a3r1jsw4v57hp92p")))

(define-public crate-gemini-engine-0.5 (crate (name "gemini-engine") (vers "0.5.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y5ygvdkr865brfpqcjv9psyfnksm0wkcnkjvli2h0xkg7payi0n")))

(define-public crate-gemini-engine-0.6 (crate (name "gemini-engine") (vers "0.6.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fm7d2nhb5lnfi9z1x8sp0gankfv9hc52myfh52gpsgcs6k74bwb")))

(define-public crate-gemini-engine-0.6 (crate (name "gemini-engine") (vers "0.6.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0f981iygd9v0a1y414dnhn6203412r4pq1p26vs2qasvxllj5bpc")))

(define-public crate-gemini-engine-0.6 (crate (name "gemini-engine") (vers "0.6.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yh15fj7sm82lb595dalz0jx5zy6wfygfmpsqnqbk18sm59hwkam")))

(define-public crate-gemini-engine-0.6 (crate (name "gemini-engine") (vers "0.6.3") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jjcvwlph4d4pwzjsji2wgqnjyxz2qrmm98crfd8vg16ns884z04")))

(define-public crate-gemini-engine-0.6 (crate (name "gemini-engine") (vers "0.6.4") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "03wpyfbxpmm68asvpk8jc18hcxv898h94jjp9fmldqx6zy9wl61a")))

(define-public crate-gemini-engine-0.7 (crate (name "gemini-engine") (vers "0.7.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i1sfwaqf6dqn7z3wm2cz6f6djga3i88mdk84l2acj7796zjjhpp")))

(define-public crate-gemini-engine-0.7 (crate (name "gemini-engine") (vers "0.7.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rhnj5zj3kbv7y922zv7dk9pz6sk03xxc1b58ys5swsn22l2cl3g")))

(define-public crate-gemini-engine-0.7 (crate (name "gemini-engine") (vers "0.7.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "10mz21vnjp1y3zp92gjn2884gvjxqpjdk91wa0whzlqlihflqswj")))

(define-public crate-gemini-engine-0.7 (crate (name "gemini-engine") (vers "0.7.3") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cpb9f2inh0x7bm384rgx6azp0qsrca3mamfcp8c14gfnwmyag78")))

(define-public crate-gemini-engine-0.7 (crate (name "gemini-engine") (vers "0.7.4") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v5cjxz9gr6dbfg40z6jz0282inkaal9vb3a1hvmnn7syv3nps7q")))

(define-public crate-gemini-engine-0.7 (crate (name "gemini-engine") (vers "0.7.5") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "04dkl84jbx8s7mjk8945xr0csn39dxgzd0v3ddgcyjfin50hbcc3")))

(define-public crate-gemini-engine-0.8 (crate (name "gemini-engine") (vers "0.8.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "07shiki2xavza4659hr81mzj1amr8b9lxzd994n014m68d44kpya")))

(define-public crate-gemini-engine-0.8 (crate (name "gemini-engine") (vers "0.8.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "07b8p8rwxmwhx3pbwnlxpghhnswn0wmr3c8n94w31bqpsh70ldkh")))

(define-public crate-gemini-engine-0.8 (crate (name "gemini-engine") (vers "0.8.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dbj0a8d95r48bblvs702g3mypgcyags7n6kv7aqpzjhvh44zbch")))

(define-public crate-gemini-engine-0.8 (crate (name "gemini-engine") (vers "0.8.3") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "055prymclmfdsi3k3ipvygx847s4vhl8j9fpnrrcv8l3mqhhxkm2")))

(define-public crate-gemini-engine-0.8 (crate (name "gemini-engine") (vers "0.8.4") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z4kh26nmkl1d6lbs2sj8mm5917cbmmvv07sskjis3fv3sicw874")))

(define-public crate-gemini-engine-0.9 (crate (name "gemini-engine") (vers "0.9.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "130rw62gna7m2pm473dcaja3x6g3z0s6csbwyl4vcj49b2dvdzsb")))

(define-public crate-gemini-engine-0.9 (crate (name "gemini-engine") (vers "0.9.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h0ysqz2yz7vi3nx75yvyr97w6i2k4gicc1hn0yh3xkm89c9x6p3") (yanked #t)))

(define-public crate-gemini-engine-0.9 (crate (name "gemini-engine") (vers "0.9.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1n80d3k7d902qdyhy9xa5si3wh900h650n9b9dbmi5x4k7a0slg2")))

(define-public crate-gemini-engine-0.10 (crate (name "gemini-engine") (vers "0.10.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jglrjx3z95wypipf9d3sz2qnj6v28ymhrnlqhffl220cxlnl3mw")))

(define-public crate-gemini-engine-0.10 (crate (name "gemini-engine") (vers "0.10.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "158z5fafs5a6p4z6h7y70915h644v39dfm8hpa86skphjamjyxnj")))

(define-public crate-gemini-engine-0.10 (crate (name "gemini-engine") (vers "0.10.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zlskjpsc23dq8arb1da65m1ccc67r9vfanwzkwd8xshaa3j5941")))

(define-public crate-gemini-engine-0.10 (crate (name "gemini-engine") (vers "0.10.3") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y0n7wjvm0x8gcayx675kl3a5w00ky8hm6gc9yi2jdnapalw5848")))

(define-public crate-gemini-engine-0.10 (crate (name "gemini-engine") (vers "0.10.4") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "04j15vji3wfarrsb1nv6s1l01gv48gsq3jy4cy62bkrvr344q8jk")))

(define-public crate-gemini-engine-0.10 (crate (name "gemini-engine") (vers "0.10.5") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ydsax36rklwid5sa5sj33qdxg18pm3caybbj9g8ar3hd3gpa8qm")))

(define-public crate-gemini-engine-0.10 (crate (name "gemini-engine") (vers "0.10.6") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mjyisbrzxdslrl1np17hby36mrv1j5a0hxig5pcspd3qmhh8kkk")))

(define-public crate-gemini-engine-0.11 (crate (name "gemini-engine") (vers "0.11.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fz6v35c0j2iqcqa4qd2jcg2wfaw476a0w5gky59syi479l0r26x") (yanked #t)))

(define-public crate-gemini-engine-0.11 (crate (name "gemini-engine") (vers "0.11.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "170r4fs9ilzmjgygcq5i1v4niffxvr514d847mkfx15d6afmijws") (yanked #t)))

(define-public crate-gemini-engine-0.11 (crate (name "gemini-engine") (vers "0.11.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sqi9ji05dby9ccqc3chpfj2lmgn8swr88yhfv2k7rhjynwmgyf2") (yanked #t)))

(define-public crate-gemini-engine-0.11 (crate (name "gemini-engine") (vers "0.11.3") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0a4pacxqdsjg5s1zza7nvfrjvrb7lx81vf5bgbn93g5wzqalkik9")))

(define-public crate-gemini-engine-0.12 (crate (name "gemini-engine") (vers "0.12.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kqix42q2fkfzxgnb8gykpriiqf431i2d6aqsrqhx9fsgvd1rppv")))

(define-public crate-gemini-engine-0.13 (crate (name "gemini-engine") (vers "0.13.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "03wk1dfr8k9qkjsnvy44qv2749axxlwh2nwhfhfgbhrzsk514294")))

(define-public crate-gemini-engine-0.13 (crate (name "gemini-engine") (vers "0.13.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "0s4clb3iijivyf9qvifpab00s60r2z73ls25a2bz08afinbgnjxy")))

(define-public crate-gemini-engine-0.13 (crate (name "gemini-engine") (vers "0.13.2") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "02sz40hrr81qai5aynqjj5g6606605hbhrmimfpnqdxkbs2mmpq7")))

(define-public crate-gemini-engine-0.14 (crate (name "gemini-engine") (vers "0.14.0") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y5sjsq0p5sik1l139cm70ar7slfw7bdzr53nxj0gm44pbpbk3jz")))

(define-public crate-gemini-engine-0.14 (crate (name "gemini-engine") (vers "0.14.1") (deps (list (crate-dep (name "termsize") (req "^0.1") (default-features #t) (kind 0)))) (hash "18p28jg4l53ylfm5h36xmlx0mgzss2az7q7dh4fhpp8blzfd15cn") (features (quote (("default" "3D") ("3D"))))))

(define-public crate-gemini-engine-0.14 (crate (name "gemini-engine") (vers "0.14.2") (deps (list (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yvk9vmc02dmf1w2y8zg8hlq3hr86zaazl251w4nl457bin7yyjb") (features (quote (("default" "3D") ("3D"))))))

(define-public crate-gemini-feed-0.1 (crate (name "gemini-feed") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gemini-fetch") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "03ixrkd0qivx203phjz3d8a95avbn38fn0pdp7fhvmw397di7r29")))

(define-public crate-gemini-fetch-0.1 (crate (name "gemini-fetch") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.18") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("stream" "tcp" "macros" "rt-core" "rt-threaded" "time" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "webpki") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "x509-signature") (req "^0.5") (default-features #t) (kind 0)))) (hash "0npvnb9ca2c4na94s6nwj136jzhlndagdihkf3j76bjq8jbbhyn6")))

(define-public crate-gemini-fetch-0.2 (crate (name "gemini-fetch") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.19") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("net" "macros" "rt" "time" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "webpki") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "x509-signature") (req "^0.5") (default-features #t) (kind 0)))) (hash "09a6l3lc0qx73kc5vp7gvj75mmarzfca2mlk0cdlc2gxjv0wb7fc")))

(define-public crate-gemini-fetch-0.2 (crate (name "gemini-fetch") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.19") (features (quote ("dangerous_configuration"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("net" "macros" "rt" "time" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "webpki") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "x509-signature") (req "^0.5") (default-features #t) (kind 0)))) (hash "1851jk1a7w6pcb9qcgmgp7vvg3dfihwr7ldlh5k9qxmyaymdy4ma")))

(define-public crate-gemini2html-0.1 (crate (name "gemini2html") (vers "0.1.0") (hash "0qjhvz9bcpahg7rakzw1zaw6aqyidm5n98686j8wsfx81szggsl0")))

(define-public crate-gemini2html-0.2 (crate (name "gemini2html") (vers "0.2.0") (hash "00r6nfmv71di016a0jibxa5wcfzbq809grzmqcn78lj09bb09bpi")))

(define-public crate-gemini2html-0.2 (crate (name "gemini2html") (vers "0.2.1") (hash "03gc8dgy4bgh3ik2ama9lbmc8vddagnmqzsavjfzxim9zf4hjmc7")))

(define-public crate-gemini2html-0.2 (crate (name "gemini2html") (vers "0.2.2") (hash "0d4m41ydgwk5v4zqs8qn34i6vnarimj9kcw2cvxmjj1arcz9r9jz")))

(define-public crate-gemini_client-0.1 (crate (name "gemini_client") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "0j45p73147fjcybq70pqdiyb4qwzkz35nk5z2m38kcm5l4b853n9")))

(define-public crate-gemino-0.1 (crate (name "gemino") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "175an1blgly42wvkghdniaffav786yh651bxw571jg77psi9482a")))

(define-public crate-gemino-0.2 (crate (name "gemino") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1p1g1gkq48qfr0baz3iac9xfhld1zf91jdr0ivx2kbnv3y8npdl4")))

(define-public crate-gemino-0.3 (crate (name "gemino") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0iiz97lyw5g75ldpsdb5c4i55ccwn7m9iq1gblj1b7jnf7ma181l")))

(define-public crate-gemino-0.4 (crate (name "gemino") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "17j1d4ni86i2l3jjwhk0wcds234wlm87x59zijlmxm59hqnc6xz7") (features (quote (("default" "clone") ("clone" "parking_lot"))))))

(define-public crate-gemino-0.5 (crate (name "gemino") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0wkjnjyv9zyiiaaly40nzq5nn0d55jknmk1dkjjg1i2828xvicks")))

(define-public crate-gemino-0.6 (crate (name "gemino") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0cfyji3kn3b23in0pjskqnp5gggl70h55np8j145jm93crhzhnsm")))

(define-public crate-gemino-0.7 (crate (name "gemino") (vers "0.7.0") (deps (list (crate-dep (name "event-listener") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1q6rkkb7yzp0fxrdksq3s9gh53x0bi3p8pv5czzyrlnhdc3fk8d5")))

