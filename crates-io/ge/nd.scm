(define-module (crates-io ge nd) #:use-module (crates-io))

(define-public crate-gender-0.1 (crate (name "gender") (vers "0.1.0") (hash "0r1rcmpi1bifkcp2bg0vwgkh7gxb9znldxsy3nd5g7w8sd97109s")))

(define-public crate-gender_guesser-0.1 (crate (name "gender_guesser") (vers "0.1.0") (hash "1g1ykc37hlsjdjc4v26gbhp6gkm8bzk4qq3br7p5x5d7jmbfk56x") (yanked #t)))

(define-public crate-gender_guesser-0.1 (crate (name "gender_guesser") (vers "0.1.1") (hash "1k3zmy9bcjmlrqbgybrnbfr9naz95a8ab7777g5k9jjnbqz305m7") (yanked #t)))

(define-public crate-gender_guesser-0.1 (crate (name "gender_guesser") (vers "0.1.2") (hash "05wcczkv03yh3f45ww2rpfzf34yi9z8ik0507b2sl331imdhdlwm") (yanked #t)))

(define-public crate-gender_guesser-0.1 (crate (name "gender_guesser") (vers "0.1.3") (hash "0ir0n3ahggylji13v8hyppirz2r83yf2a22qrqbqd663041rig9j") (yanked #t)))

(define-public crate-gender_guesser-0.1 (crate (name "gender_guesser") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "1kd309vqvdig2iqdgr01pp1p8rcdymcgi7d8rr32mig3xv0brcpf")))

(define-public crate-gender_guesser-0.1 (crate (name "gender_guesser") (vers "0.1.5") (deps (list (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "16v4l82z7l81vc55pqwa99p3kkvj6i7jm7al1jbqikn0p7cywn8w")))

(define-public crate-gender_guesser-0.1 (crate (name "gender_guesser") (vers "0.1.6") (deps (list (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "1ia32701cmi0fw1m3fhm6a6br52lndxwjn80h1gx739bbjigmmf4")))

(define-public crate-gender_guesser-0.2 (crate (name "gender_guesser") (vers "0.2.0") (hash "0gh3f0v0rfdipzcrv1r4rzbpawaqx3m469a6yi7z7xwqi8is94vv")))

(define-public crate-gender_identification-0.1 (crate (name "gender_identification") (vers "0.1.2") (hash "1v7iiawjy0k5wbrxn82v717alk8k3s8p41nccrlv1g4vsw482xx2")))

(define-public crate-gender_identification-0.1 (crate (name "gender_identification") (vers "0.1.3") (hash "11aldaisjg3w3s72ic1yh2i8pa6gkq64a9smy5rs8pj4mf45j929")))

(define-public crate-gender_identification-0.1 (crate (name "gender_identification") (vers "0.1.4") (hash "13jzh5l1dwdr81ffsqjzn6yhidskprqcdx790sghs3p2gskz0xjl")))

(define-public crate-gender_identification-0.2 (crate (name "gender_identification") (vers "0.2.1") (hash "0y3mh96sdv35byvp2d1ynqw36rirgbrnx5y7q2zp1r5v850mi8y1")))

(define-public crate-gender_identification-0.2 (crate (name "gender_identification") (vers "0.2.2") (hash "1hr3q8cabwy1s47afsg4xq3fdq0kmgr44y2hj72kwcaw41hc69jn")))

(define-public crate-gendoc-0.1 (crate (name "gendoc") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1fzn06bnh8qpk3l79vb1wxkdg6ghbjl7ll23rzbq9lhqb4lip9g9")))

(define-public crate-gendoc-0.2 (crate (name "gendoc") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1yf3r76j8bbbyca7n6lq3vhi69hlh8r46wsqp3q5n1z6h25myxpv")))

(define-public crate-gendoc-0.3 (crate (name "gendoc") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0kbsm30xl7dhy1dxi3xi548jn57mn80xr9hiix79kb3pb40lyf8x")))

(define-public crate-gendoc-0.4 (crate (name "gendoc") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0vy8xfgwj89g7pz7qims3hxsq7ykvyyix8ak115mv9q13hs01syz")))

(define-public crate-gendoc-0.3 (crate (name "gendoc") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "12zzsc458midif43npynbg6xxcr51qz8bx5yyabgmjwd9qsncihs") (yanked #t)))

(define-public crate-gendoc-0.4 (crate (name "gendoc") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0wl4y1mcb9ih45p3lpydglk0i18cfxnwlhdf0kv28r1i1n5ajf6s")))

(define-public crate-gendry-0.1 (crate (name "gendry") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tiberius") (req "^0.12") (features (quote ("async-std"))) (default-features #t) (kind 0)))) (hash "04ipqfgf2lfrszjdr7pd7b5dklyz55p2hn2b6wab8w34bv8ppsc2")))

(define-public crate-gendry-0.1 (crate (name "gendry") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tiberius") (req "^0.12") (features (quote ("async-std"))) (default-features #t) (kind 0)))) (hash "02d1i3p2wc5mhak33aszxk0r74716xify4as90gqanigp6ajrrf3")))

(define-public crate-gendry-0.1 (crate (name "gendry") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tiberius") (req "^0.12") (features (quote ("async-std"))) (default-features #t) (kind 0)))) (hash "16ivz3g26v1mf163h2spqd47yjh3cpl44zqwd94bjhrclfl42ai8")))

