(define-module (crates-io ge nr) #:use-module (crates-io))

(define-public crate-genr-0.1 (crate (name "genr") (vers "0.1.0") (hash "1v410mm5viyhigrik0fkvsr9df4imk9bh89zbqqd8ds7g3hs1fvf")))

(define-public crate-genrc-0.1 (crate (name "genrc") (vers "0.1.0") (hash "1mqbczw8kgk5k3s00aq49snjj646c9rvhqfsfmhgn4m7a8gcria7")))

(define-public crate-genrc-0.2 (crate (name "genrc") (vers "0.2.0") (hash "0dgyziihcvp457y102h7yw4akin3pngb7y86h27wi24ajnsmb04w") (features (quote (("allocator_api"))))))

(define-public crate-genrc-0.2 (crate (name "genrc") (vers "0.2.1") (hash "10drn83zgyc2mw4rzz446ydjc5b1s344b0iiglc2s2dsxsglqj89") (features (quote (("allocator_api"))))))

(define-public crate-genrc-0.3 (crate (name "genrc") (vers "0.3.0") (deps (list (crate-dep (name "bumpalo") (req "^3.13") (default-features #t) (kind 2)))) (hash "14943kl7y7ca36gwfs5qqnx5xmib2mv4r3mxnjn99m8wswn668xm") (features (quote (("allocator_api" "bumpalo/allocator_api"))))))

(define-public crate-genref-0.7 (crate (name "genref") (vers "0.7.0") (hash "1d9hn1cl0lq451avp9hpq4zznc6s31axjmqapjwxjfbivsyvh30g") (yanked #t)))

(define-public crate-genref-0.8 (crate (name "genref") (vers "0.8.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lock_api") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)))) (hash "1glxdk604iw1ykgnnbsac44sja3shllapxvprbicl07dcsxkdyhr") (features (quote (("default" "global")))) (v 2) (features2 (quote (("global" "dep:lazy_static" "dep:parking_lot" "dep:lock_api"))))))

(define-public crate-genrepass-1 (crate (name "genrepass") (vers "1.0.0") (deps (list (crate-dep (name "clipboard-ext") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "deunicode") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0qlr2xx3d4dixgp6hdb66vvp74yr08zkj3jcxas72qkksnn6b77s") (yanked #t)))

(define-public crate-genrepass-1 (crate (name "genrepass") (vers "1.0.1") (deps (list (crate-dep (name "copypasta-ext") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "deunicode") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1h59dlyf0ibhnvxhgagv18c4165r9pir5ddzrc4v8i7qav3i2s9r") (yanked #t)))

(define-public crate-genrepass-1 (crate (name "genrepass") (vers "1.1.0") (deps (list (crate-dep (name "deunicode") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "06f84lym1m42lvizr4qwv7i6i2188p6mmv4wj73rclcmaj96qsfb") (yanked #t)))

(define-public crate-genrepass-1 (crate (name "genrepass") (vers "1.1.1") (deps (list (crate-dep (name "deunicode") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "10jc81s4jdwd5ya5bfnwsr1db5mnwml83x9r72jzc9jz0xdywms3") (yanked #t)))

(define-public crate-genrepass-1 (crate (name "genrepass") (vers "1.1.2") (deps (list (crate-dep (name "deunicode") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6.9") (default-features #t) (kind 0)))) (hash "0az8gbb8b2xjnf1ms3nczb290qn44b067k8a7lbdz3hvfydv6whs") (yanked #t)))

(define-public crate-genrepass-1 (crate (name "genrepass") (vers "1.1.3") (deps (list (crate-dep (name "deunicode") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6.9") (default-features #t) (kind 0)))) (hash "1859cbwvif9sih9pyp4ximss5y6izq8wxhc2vj8n6m4517fpfdzp")))

(define-public crate-genrepass-1 (crate (name "genrepass") (vers "1.1.4") (deps (list (crate-dep (name "deunicode") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.7") (default-features #t) (kind 0)))) (hash "0bychk4f1rjzy5wxr48kmf231n3hxnpip448p4q6d8jk9z18adiz")))

(define-public crate-genrepass-cli-0.0.0 (crate (name "genrepass-cli") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "copypasta-ext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "genrepass") (req "^1.1") (default-features #t) (kind 0)))) (hash "01pvr2r960rvi1844y7id6nyag3v7c0qfw7rdxkf815lkzax4sxn")))

(define-public crate-genrepass-cli-1 (crate (name "genrepass-cli") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta-ext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "genrepass") (req "^1.1.3") (default-features #t) (kind 0)))) (hash "0k5z7a4x7gvf2pjvpfz5s1q5pl12r6vkzlzmxcrfq3qbi574md45")))

(define-public crate-genrepass-cli-1 (crate (name "genrepass-cli") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "copypasta-ext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "genrepass") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "143cb3rc6830975grv7841v5c071ixrsjaidsgfigy4k5z6iz7ax")))

(define-public crate-genric_logger-0.1 (crate (name "genric_logger") (vers "0.1.0") (hash "01b0rpzv1z28pkigv6qx5jz2b737p7niw14kcf095gv5hffigpij")))

(define-public crate-genrpc-0.1 (crate (name "genrpc") (vers "0.1.0") (hash "1i7k6df1yhf0i28khp4zv6yvw7d9ik41fp3xfrhdbamqahjqsw2f")))

