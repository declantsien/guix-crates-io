(define-module (crates-io ge nm) #:use-module (crates-io))

(define-public crate-genmac-0.1 (crate (name "genmac") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "05xdh132s77p6nyvb77ya70vm24zdgp2yhbkfbzx90fl13rc7nwz")))

(define-public crate-genmap-1 (crate (name "genmap") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "handy") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^0.4") (default-features #t) (kind 2)))) (hash "05dmag00rrq039akjx5ly4rch5ak9gjflwzxpmsv0bi5pj75zigz")))

(define-public crate-genmap-1 (crate (name "genmap") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "handy") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^0.4") (default-features #t) (kind 2)))) (hash "0q0rpvxcylm7c2aa72xyf7ra6kgbrfg912v6k1b9ayagp529wkmn")))

(define-public crate-genmap-1 (crate (name "genmap") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "handy") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^0.4") (default-features #t) (kind 2)))) (hash "1br5gcwbzspz89qq4xshy88x55rbxnnchsma0m6h0waidmbrf2xj")))

(define-public crate-genmap-1 (crate (name "genmap") (vers "1.0.3") (deps (list (crate-dep (name "criterion") (req ">=0.3.0, <0.4.0") (default-features #t) (kind 2)) (crate-dep (name "generational-arena") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 2)) (crate-dep (name "handy") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 2)))) (hash "1ly0shzg8bmxgs3ly55ivry1y7rylp52gj773jpxzk5pip82myi1")))

(define-public crate-genmesh-0.0.1 (crate (name "genmesh") (vers "0.0.1") (hash "1f2ab3izr4c58kmy3qljb9vy77q6s5a7gsib8zblwbbi2nraqcmj")))

(define-public crate-genmesh-0.0.2 (crate (name "genmesh") (vers "0.0.2") (hash "0nakjkl8lr0mnavwj7hgavskb7xp89rsyq7ivmdbnj2s5klhj7zs")))

(define-public crate-genmesh-0.0.3 (crate (name "genmesh") (vers "0.0.3") (hash "1b2jy52hz7c69qxh8yqas1x5sfarfn362bl29yg45izna2r7lp2a")))

(define-public crate-genmesh-0.0.4 (crate (name "genmesh") (vers "0.0.4") (hash "0hsa6dcn2sdmwy7sx07mvghrnczx2g22rhvqvhnzhqcykgmrl8sz")))

(define-public crate-genmesh-0.0.5 (crate (name "genmesh") (vers "0.0.5") (hash "05j4c429m0d9k48bjgbhrdr211rkz2n51znijzpvnfny6qh5mjy7")))

(define-public crate-genmesh-0.0.6 (crate (name "genmesh") (vers "0.0.6") (hash "0rgmh8rkxwpww6d5i48jnn9if63sci4nx5aw9nblj2r8w8qqjdv5")))

(define-public crate-genmesh-0.0.7 (crate (name "genmesh") (vers "0.0.7") (hash "1jldq7ipaw0983s56cjrlsxgphzf195y3qzrdg1zj720q0dhkxlg")))

(define-public crate-genmesh-0.0.8 (crate (name "genmesh") (vers "0.0.8") (hash "1i5xqpw7ihc69xbx9q6hhxdc2n15a1gc18w1721zp5v4am5dinzw")))

(define-public crate-genmesh-0.0.9 (crate (name "genmesh") (vers "0.0.9") (hash "11h4qigpa5sckiv5ldfc307b1az9ff4545q0yjw8xkb7g2z4n23j")))

(define-public crate-genmesh-0.0.10 (crate (name "genmesh") (vers "0.0.10") (hash "18bwz2xs0abkc356y2fir4b0607s10361r729grd4jrhd6kvqlzi")))

(define-public crate-genmesh-0.0.11 (crate (name "genmesh") (vers "0.0.11") (hash "148kzfgv1k4s675bpmsc3ck826vygbq2y341cgv8ijj4svnmhl9n")))

(define-public crate-genmesh-0.0.12 (crate (name "genmesh") (vers "0.0.12") (hash "11dgigrzrq0r49773rz7q85mfa7g2rl2b7q8ps78wx0jsjym6flx")))

(define-public crate-genmesh-0.0.13 (crate (name "genmesh") (vers "0.0.13") (hash "0j5afj2q93a3fk2mhwdq82cmgpmsrgix32id77scbq54y1sa4a8p")))

(define-public crate-genmesh-0.1 (crate (name "genmesh") (vers "0.1.0") (hash "0dj2x9sqmlp1kvwa3a19fz2cgx6qgilklj53w73z903lgiinflj2")))

(define-public crate-genmesh-0.2 (crate (name "genmesh") (vers "0.2.0") (hash "1z32y5lcx511bg9mj6v1lvgnz9s2iajnaxy1xdjg4ljm6mj9i2xw")))

(define-public crate-genmesh-0.2 (crate (name "genmesh") (vers "0.2.1") (hash "1kkjl2afqfc5jw9f2amhgddh9lwy78znpc4q9i3yl1466syzkd7j")))

(define-public crate-genmesh-0.3 (crate (name "genmesh") (vers "0.3.0") (deps (list (crate-dep (name "cgmath") (req "*") (default-features #t) (kind 0)))) (hash "0jy6dg3xpfc0ynsigh0c810n82zb2cbd733cz7rrg2vx05dcg9di")))

(define-public crate-genmesh-0.3 (crate (name "genmesh") (vers "0.3.1") (deps (list (crate-dep (name "cgmath") (req "*") (default-features #t) (kind 0)))) (hash "1mwzm40ba7vi4wr1l37vsw0qnzbcjvgrzlh21qz14wpghg4k5py3")))

(define-public crate-genmesh-0.3 (crate (name "genmesh") (vers "0.3.2") (deps (list (crate-dep (name "cgmath") (req "*") (default-features #t) (kind 0)))) (hash "07cfn4b4fc4k5i729q2hj6igm0rm8mbw7d9g8zgxxw44k4imsnd8")))

(define-public crate-genmesh-0.3 (crate (name "genmesh") (vers "0.3.3") (deps (list (crate-dep (name "cgmath") (req "^0.3") (default-features #t) (kind 0)))) (hash "1k67l8b296iac35wkg2pkc9day96g3kx78lpqsfs6vzhc4wpaxq7")))

(define-public crate-genmesh-0.4 (crate (name "genmesh") (vers "0.4.0") (deps (list (crate-dep (name "cgmath") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ncf8v3b43zmqdzyd43b64n6ad0s0pbrc2pima4dy8h17ds5lgpr")))

(define-public crate-genmesh-0.4 (crate (name "genmesh") (vers "0.4.1") (deps (list (crate-dep (name "cgmath") (req "^0.7") (default-features #t) (kind 0)))) (hash "14mlmyah9n83ffbjwdnvryknrqk07cqbgqmz5ab2fawhn4pkcyg4")))

(define-public crate-genmesh-0.4 (crate (name "genmesh") (vers "0.4.2") (deps (list (crate-dep (name "cgmath") (req "^0.14") (default-features #t) (kind 0)))) (hash "1634jn2py3czpaymd9n89wh2rj6hp6y5jyngfyxbbmvpmxvvjydp")))

(define-public crate-genmesh-0.4 (crate (name "genmesh") (vers "0.4.3") (deps (list (crate-dep (name "cgmath") (req "^0.14") (default-features #t) (kind 0)))) (hash "06yvllg11qa58jbfq7wrplmdh4lv4ji5y8l6m0q09fq7w0cacx54")))

(define-public crate-genmesh-0.5 (crate (name "genmesh") (vers "0.5.0") (deps (list (crate-dep (name "cgmath") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rhvaqqqg3zqwsb58fqzkrg52bf98fdxcmg9wdsm86sy763wcj23")))

(define-public crate-genmesh-0.6 (crate (name "genmesh") (vers "0.6.0") (deps (list (crate-dep (name "cgmath") (req "^0.16") (features (quote ("mint"))) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5") (default-features #t) (kind 0)))) (hash "10sqydcgc37i95abwmq6kiwxr7nbhzp37ad1glfha1hwgj901zd5")))

(define-public crate-genmesh-0.6 (crate (name "genmesh") (vers "0.6.1") (deps (list (crate-dep (name "cgmath") (req "^0.16") (features (quote ("mint"))) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5") (default-features #t) (kind 0)))) (hash "0c09n7pa3bzxl4dq80s2h2j58nnbaqcbfbiv155pcwapd6xxbhip")))

(define-public crate-genmesh-0.6 (crate (name "genmesh") (vers "0.6.2") (deps (list (crate-dep (name "cgmath") (req "^0.16") (features (quote ("mint"))) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5") (default-features #t) (kind 0)))) (hash "17qybydyblf3hjiw7mq181jpi4vrbb8dmsj0wi347r8k0m354g89")))

