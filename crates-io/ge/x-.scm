(define-module (crates-io ge x-) #:use-module (crates-io))

(define-public crate-gex-sys-0.1 (crate (name "gex-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.67") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0iwwhz37f6qsqa3p1givmsfxfhkiyrjgikz5bd4zs807gcl6sbja") (features (quote (("udp") ("mpi") ("ibv") ("default" "udp"))))))

