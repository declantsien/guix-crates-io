(define-module (crates-io ge vl) #:use-module (crates-io))

(define-public crate-gevlib-0.1 (crate (name "gevlib") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0mw9mxpnssc3cs0n2jnln0ncygaabk87w10yawa1kp53v6516si7")))

(define-public crate-gevlib-0.1 (crate (name "gevlib") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1lg4jxvmg86kjwjj54vv9wd5xwl76dlb0g7ipkvlkd84yilqq9cp")))

(define-public crate-gevlib-0.1 (crate (name "gevlib") (vers "0.1.2") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "031kwca6g3cpxfnglisy9k9jkchgqpykshf5qd7fl0nh9krxqqki") (yanked #t)))

(define-public crate-gevlib-0.1 (crate (name "gevlib") (vers "0.1.3") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0vfdxn4v2mjdh9246f3arggvplxw0l7v79009k4pd3n18xa6ff3z")))

(define-public crate-gevlib-0.1 (crate (name "gevlib") (vers "0.1.4") (deps (list (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0x2pvyldq9ybl0y4sn3p094ysv90ppl22r63vbzk668hfsyz2hyz")))

