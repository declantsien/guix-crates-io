(define-module (crates-io ge nc) #:use-module (crates-io))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.0") (hash "03dqjy5kcahrn4zs2f50rsxshizxd4z7la28szzqryhg0682wrkm")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.1") (hash "02qh1gj3mdnqda91q7m4g2kwjfll0dgwp03q8nxbbnpprndpiijy")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.2") (hash "029ygmxnzmbcfrirh5cy0yzadnz9mb49kkslvpv09pzi4br11yvv")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.3") (hash "0zlvhicc5l8hz8y5fxajvhwsx95qagk7m2l8b6b11gjw8njpdzga")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.4") (hash "002b9mkn44xqg7ym5a18a1axgslcmqrl2hrx75cwzbj167mr5bbz")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.5") (hash "0y9dqy47jamynjrbyczkdcjy16farmv23vyczhwi5y0myyy820q7")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.6") (hash "1hkgdy67g21zniqnk50v0d4pzl50r2f0mxwk0gwd5xs0rlszqwr2")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.7") (hash "12mhj77gvi1bdbjmawlh88lycy3mfm3i6zqpg0fvidryj1c1vib1")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.8") (hash "0alvdbknsfdnldvwj9zl9hrhgr8hmmych0valx78mcbmy3iv8y9b")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.9") (hash "0nlkbad52jf3cnfzpk2cg74rzx1yzjwxsxyiss9s5mm2cygl40lv")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.10") (hash "1pdrw3j615xmc33j0bb90535r918y7bx661w6hw1iz5yz33qljx5")))

(define-public crate-genco-0.1 (crate (name "genco") (vers "0.1.11") (hash "1gfpi8swsigil7lpxxcgqy79jv6km2wbscpg6rdpidvjnv4y51aj")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.0") (hash "1ibip1cwrnc4l8dlpv9l8wikcnisfn348sw4hygazvlpvigdv2xm")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.1") (hash "1i6j2s1bqrnv1a4wc6zjv9sybrlh2kvfs45mz8pkdwddpq3n0q3z")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.2") (hash "0q6vlp8sy43xvf40qd4fclmimxf4zh4k9qinbyvzvnb1i2mhaslb")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.3") (hash "0vkysn5681jn5bnp4rl22psclc8b7x6xc9hr8aai9j58viasc2jx")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.4") (hash "03ij9ni867rf0aiqvhnn9ib7av9lrfa4kal6w89irmjkvzijl6nw")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.5") (hash "175366kiacpavimiw2rxzzfs2xkx2b9yq4qwhmk9flv73nijgx09")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.6") (hash "08cpa1xaffyjr6nvfawzfji0w0rv0ijvvybwnh4v4s65mc8lyiji")))

(define-public crate-genco-0.2 (crate (name "genco") (vers "0.2.7") (hash "1nz40g2871g7dvjl5nv3w5f826icdliglfmfvs79l4g4s2469l13")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.0") (hash "0xk8f5lyqqlr4h31vlj0x2fbrs7zmbna40hk9zmgsc4ij51ymsnm")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.1") (hash "0fdpr35qx14k0maanqgy9dw0x290vphm67y7icjypmy37k7sv6gi")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.2") (hash "02fipxp3x1d0b5lifw8n8sahnrggvrbsv7ac8c8ifyc1krs0r1bf")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.3") (hash "06hx4f1kw5j6p17nqb6zs6m5gaf6vyipr3fi6z5qh4z4vbqp592q")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.4") (hash "1v3ky1h9870ajkqycww54sf8a1h0w9pp3487z4zqvxsmghr2sh6f")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.5") (hash "13vy8328icgkc7mn1jph520x44g1pvazw3y4sfi79n42pqa7pv2n")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.6") (hash "0w6mdc1ifpfcxskjv9gj6kvxy13a404xb3sv03628mqbwfwk0pk4")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.7") (hash "05mch0m24j8bx53wimjbjci30684ij0j4whwk77qgq89n47chd5h")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.8") (hash "18qvydk6a3z1l0jdnlg5cpmwg089gxspqy2l125j55m6zz62cmd7")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.9") (hash "036vmaz0rh7a5s2ffz1w28dmbbw63azb84080ln7pkfclv7q88rm")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.10") (hash "0ivr0ad3lzy4kpx8v22xa3qc45225n6hxk1xwbqkppxzhqizw0ya")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.11") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y4la8lyshlay1j6xg4l6ayqn54gglqaiibgm73fdv1majrj8d7c")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.12") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b65dr8i1b7q5m2751snpzs34nq1zbphabknd5cwhg2h1wrx0jly")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.13") (hash "0whnhcgswql6fi6cvlakbyn31zn13g1bxgjvyqv7yif6bscc092w")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.14") (hash "1rx9bz86b2lsas44vvb06rqdnq20in8kql9vbznvnqhy4737bafd")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.15") (hash "0xgy16ifz3ykg47wfqfnrcrs0im1w3c9hnmny8jflr6d9l0wkrnm")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.16") (hash "0x53gkm0x7m21p3c9b6hqdngh9c71na6wq9yv4v32mx0kp07m9ih")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.17") (hash "07q55hrgz3x9hlc6qlx9izi81lmkjm9hlkjfs07zzn6r2xh3m1gr")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.18") (hash "10y59zznv6mwmpcmkbi35hwkpxkfb975g0jxyvrwdhh98cxfgi7r")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.19") (hash "0pw9xnmqw8bqsvx00vclnswl3fpsx72aq9v0kyqqkmjpp338yclr")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.20") (hash "0vaq0mm1syfss48yv94067vsnvgyns8pnz3fzip1avi0y0wi54mb")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.21") (hash "0ri9r1nnssrkqcqw5y0ch6ddjkw1bwyxxjiakwl429jmxzvzimgx")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.22") (hash "1glvzz4cm72sscd58rzz0g94wzi9dzvk5pp9zxl93hxs229z9xww")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.23") (hash "00cb56sd69l6ixkw4pkg6fsj1j8jfqhji98gxsb9z5p9fqmnc533")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.24") (hash "0xzmhzq7y30g9qb8r5xrwd28v3x67ng1b92bkpm8v1rmk15sdk4s")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.25") (hash "0p81y9b8ly5x51qd1w409wp99sk349sah8rmkwbivsv4iajyq065")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.26") (hash "0sxwiwf77qzv7lspcwfvgd0m8830smy1mg38sjlbwl9ncplrdml7")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.27") (hash "0lbzm2f42lra5q0x3sip6nxf9jiwyjs4d3a3nh422lzlk9ywm61l")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.28") (hash "1s6lcggd8r4bfc0b4gi8966iph0kczs8fbrl3q0k99vgif1x3cvd")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.29") (hash "0wmhfvz5rp073cjmvjhm0kxraw3qqpff6q8xqfw14zchssbhzy60")))

(define-public crate-genco-0.3 (crate (name "genco") (vers "0.3.30") (hash "1x817avjvyhhm7fvny0rszw5bghnq0mygc9qid8gk4zbzaql4w39")))

(define-public crate-genco-0.4 (crate (name "genco") (vers "0.4.0") (hash "1rip282vwnz2kgz6jsyz3qpf1k4z2cv59p1fy4zzq137kra0yzh1")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-derive") (req "=0.5.0-alpha.1") (default-features #t) (kind 0)))) (hash "19phirda98r4xxbnvgzysdpw5p8ypb287a46cdv63p7kim3lkk0f")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-derive") (req "=0.5.0-alpha.2") (default-features #t) (kind 0)))) (hash "0v8psixgig7pda53dpa9pp68ms6lhkqcly3gma1kbxhh5ri3xmiz")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-derive") (req "=0.5.0-alpha.3") (default-features #t) (kind 0)))) (hash "0zafdl5488xjs241iv2kpbg8k16cwhi6sfgpghyl41gfgxghdmwy")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-derive") (req "=0.5.0-alpha.4") (default-features #t) (kind 0)))) (hash "0a5kh31r6wwfbpsi5sc0bwcpdf2z1vnbz1pq2j2xl2d97l16wnww")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-derive") (req "=0.5.0-alpha.5") (default-features #t) (kind 0)))) (hash "1ypman3lcj7dlfv6yrrws6arhd94vwyr3iczkkvwhhn8718bffby")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "15wlb7437lgyds3c2lkkqg6g46jgg0da44klq4n88vm4mzzvhyx3")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0dhj3ws1qyvxzi3k0fhgn9grw46zirmqbbn5qkssdsngp5bc1n8k")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0hra9wszhxl0m0iphjvcamns3m8y8820maamf749p9rs8qipm84i")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0q0d8siybrzs1rdpzqb0j54839a9q0nn4vmzniapg81bwi3mqclw")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1c26cjl13vykwcnjcylk5icrff8r8j5xg7ddffr9ygknyhs9jd8s")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.13") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "111q2zf4cd83kmwpc30xwqjmlbk5ddx94abwbqai13359yz6kjca")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.14") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "13j12pqmd32s7dk9747qh83rw6vdknxvwj2g5mshvgn6d9l6q7qi")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.15") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1w56yhwqhndsycvcy5srv3dhmc2z09ccrs1s6sm5h6z3qf8nvhly")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0-alpha.16") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.5.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0rawvd21vfa2kjgy4wmqyx05pafgkqdk1bq52c8g0f6i04r0g7nc")))

(define-public crate-genco-0.5 (crate (name "genco") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0fkang6lbmhsx5kz1hz5siaj8pkl9wddq1akyz7gv2fsnrgsc5mk")))

(define-public crate-genco-0.6 (crate (name "genco") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1ma5rafdim1s50dx39iyvinvsg2jjsxdszz3sxwk8qczcacxsam4")))

(define-public crate-genco-0.7 (crate (name "genco") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1ybd7xgih3vaajs7h596cqdr137nqrqbbsx0aixizlbq5zlk2ynh")))

(define-public crate-genco-0.7 (crate (name "genco") (vers "0.7.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1fyigkwd6g598vhld598l01chak5vk9awghx8vw2b98j2fb2kyss")))

(define-public crate-genco-0.7 (crate (name "genco") (vers "0.7.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "028bvj71mw4a3yda91fzl0sccnyd267bc30p149a90l1sni5q2pp")))

(define-public crate-genco-0.8 (crate (name "genco") (vers "0.8.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0858vaw8b0cx6f199a1k2y1mppd5ndfi57p19lf954h2scpnq01v") (yanked #t)))

(define-public crate-genco-0.8 (crate (name "genco") (vers "0.8.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0p13n50fk7hwsd46fmkv571i8maa9wd9qd43msh5m9wn1bvjdxn1")))

(define-public crate-genco-0.9 (crate (name "genco") (vers "0.9.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0lvrrl3p8a11hlw9k63v5awrldy70r90szdca8kh0jzy1zylh519")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1kf4v9xfk73zy0b44wpvk01drp86ln0xrh7mzxjp1facmbib7nqh")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "02cvvywgsirx872cprjwrlg63019d2d1ri7g4xiz98r2bn78mamc")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "18azr0iygr9cxbj7xvwir67qrmn26kbiaf5f8djiw088s44c5h0i")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "06q0y1b8ybs0vb81k8q8ci5irmd98fpp2vz7552hfmm9di5g3r79")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0jmw84v82pw086b3a14bwkpy49xkvmspsm9q9gn41gdhmqw58n73")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1xpnk6wglrzpg1pvkpjw89r8ydzc6w56n6nbcpvqpfdlvpacic9g")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0l6ba1pmlc2b1b6q65hrfbym15k02hv5q18vbw7h6ybj2wl55irw")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1gl5rs6pzmb5bswzdc4lhx6qnj9f13kh3dx7c6x92xhiz1hadhc5")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1bv0zy8sa1387p5kv27dpkzz1fxxsbyhym9gvmq4yz2rbnbri940")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1606wz4x1ddk0virxs54b9r8xwlmi83449chaihwzhsalag4lz8p")))

(define-public crate-genco-0.10 (crate (name "genco") (vers "0.10.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.10.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1jbgxb8lq05d5a0fqc3bfilrxnkfcrxvicxqjkdc4iqs4pbai0md")))

(define-public crate-genco-0.11 (crate (name "genco") (vers "0.11.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "08dny27j5k9blza4ifarlwd932mxhiansjzcbg8x3395zq0b7zbv")))

(define-public crate-genco-0.11 (crate (name "genco") (vers "0.11.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "144080crmq5mvsgvn9bg42kvfrpnzv1zba58z98m9kci5hc302i2")))

(define-public crate-genco-0.12 (crate (name "genco") (vers "0.12.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "150vaph41hpi644nv419iig2f8i5a38hdphiv0lgvmjibap3ggpb")))

(define-public crate-genco-0.13 (crate (name "genco") (vers "0.13.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1dw3fr3w1n0djp0x5c2g5bdh4sxmdi2i0bmn1d5f077d3ir0is0l")))

(define-public crate-genco-0.14 (crate (name "genco") (vers "0.14.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "03qg7gqzzmqbhhvykkfpfx80ih6svlfza0sm487qv0yhidsvl7bk")))

(define-public crate-genco-0.14 (crate (name "genco") (vers "0.14.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0alqp3zpqjz4np3mdqzbzsc0q0li741i84gfrk534701krjr6hf4")))

(define-public crate-genco-0.14 (crate (name "genco") (vers "0.14.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1xqp06mhldlla974kpw9wwjgdv83i21mhfd116dyj1fsrw3c5nyj")))

(define-public crate-genco-0.15 (crate (name "genco") (vers "0.15.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1kpib03j02jpl47zy8i1kimabsc37wz2pc3wq46vh5fg99kihlhy")))

(define-public crate-genco-0.15 (crate (name "genco") (vers "0.15.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1l4vy6r90la33ad7ma9bqjaqw5ic42i6lrbcnywkcarxpr066gka")))

(define-public crate-genco-0.16 (crate (name "genco") (vers "0.16.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "13i5qpzzrj1gvzrv52nfpnmi9zv8np9pmxmb0c3hrsmfqj7vhzd3")))

(define-public crate-genco-0.16 (crate (name "genco") (vers "0.16.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1wwspjgm1p3fzn96rlrh0b6dw3yirlvsp28mv4aya42gk67p4fz7")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "00pqwwp9myf1g8svldgklz1bcw47c9nbzrlbkgj8m3nfi2zg6z9d")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0bfdjpbp2gyyzypmfwq0qr6xfyh245jx1vsvq72j7ri1ddbycr27")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1x713l59awrc3iv6ivk2gddm3673qp0jgdwyrwqm5g1dg3q8ynfq")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "085dmd8q2pm9xa888yb861vzcjv0xzjb67xil62arm8yrv443pj3")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.17.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "02nf4jrpynwhz6c31mlv86vcibs8bzrkj3pgxhnxk3h90wy3iv03") (rust-version "1.58")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.17.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "12mw960n1wm7q3wxhsvaai2qa233nnjzca7l0ka732h6322wwwv9") (rust-version "1.58")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.17.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "11vpfsvmh9m45nmax7yq2w5rh8r1afwrk4ilrdshfih4psfzk5rm") (rust-version "1.58")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.17.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0k9hjdrblz694pmc7flm9nwyxqmv44fvnki2h5dwzsgzjd427zf4") (rust-version "1.58")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.17.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1bawc0kscby961zrp70v7ckn38qg6rlyc9k4c7p8ywwhhxcszmwq") (rust-version "1.58")))

(define-public crate-genco-0.17 (crate (name "genco") (vers "0.17.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "genco-macros") (req "=0.17.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "relative-path") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1ij2mm0glvj34flq2npq4yjsffbym3c61nwwxygsqsfv2jxkrb5g") (rust-version "1.66")))

(define-public crate-genco-derive-0.5 (crate (name "genco-derive") (vers "0.5.0-alpha.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1qgzkddfv3ja9vcq0qmbw8c48k0mxg3f8phpziaf1fmi4fkmwfs6")))

(define-public crate-genco-derive-0.5 (crate (name "genco-derive") (vers "0.5.0-alpha.2") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0nw3wzlffigsdwma2ixdldf87pxb2yg8hi8prhhc0108lmh4b9vp")))

(define-public crate-genco-derive-0.5 (crate (name "genco-derive") (vers "0.5.0-alpha.3") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1w72nnsw6cdkp9zxhi5bf44w10hbqw5f6p2bgspdkqb7ss2xpm73")))

(define-public crate-genco-derive-0.5 (crate (name "genco-derive") (vers "0.5.0-alpha.4") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.4") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1sx3151lf2asr342lvglyf1hd4cxh6k62sz08xnfndxs2gg1n8cf")))

(define-public crate-genco-derive-0.5 (crate (name "genco-derive") (vers "0.5.0-alpha.5") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.5") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0ms72n7vfadhd92kk6v35ma29iz70bfikys4d4axnpwy6y1bf3s2")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.7") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.7") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1ld2dfcy12wp9qdrk2z33qbnggay2bk2lvagb6ljrd92mhkf9cv1")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.8") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.8") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0bwaq8vnsxj3p5hxr2sdqc1wnx0brdn9v3lixvj9a73i1qary3i8")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.9") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.9") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "06d6xkj9ni6b18xnn6j1gw64vhx899kzym9zf17ld30nlfdal65f")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.10") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.10") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1gw235qi1f6k9r3zis3x0giq44fsnls2pa62cd4w8hg49da44jwp")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.11") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.11") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1z2k55256z9vhrwkncy874w3f4dxmxbf313iil6i3vqfiqkqfa9a")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.12") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.12") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1gcz2wgdf9q68lg7q698k0nmy69r16s99ih0yp1jlppqpwmcw6iv")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.13") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.13") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1d24dsfrlip3fx6kyi2a47l6pyn1kkwp6pz99bl1zyl2j8dkhcsa")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.14") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.14") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1rdws1b8jyq1rpk4220vl9d2xnlh3fciqkybnhzkl9s0n3ahpi3x")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.15") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.15") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "11qsyc52nlrlj5cp1qjj4q91lbbhyl68pcjnwj05qhgdlifdi0wq")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0-alpha.16") (deps (list (crate-dep (name "genco") (req "=0.5.0-alpha.16") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "00w7ms9m6zx3wrcw4z0y7xldjizlk1ic4p71kd4gpmg4nh9kidhh")))

(define-public crate-genco-macros-0.5 (crate (name "genco-macros") (vers "0.5.0") (deps (list (crate-dep (name "genco") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j1k60c0jdxrrcy4abfwj7v2ghpf95yllx0qglf6i0401izp4w1a")))

(define-public crate-genco-macros-0.6 (crate (name "genco-macros") (vers "0.6.0") (deps (list (crate-dep (name "genco") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gx9v6fd0r6w9cpqf94s7r21vsrwj6d207wj8f78kgdpra3dp5g9")))

(define-public crate-genco-macros-0.7 (crate (name "genco-macros") (vers "0.7.0") (deps (list (crate-dep (name "genco") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p2jap1dp13whqyd6v83q2c2s1galjqff9b0614ccrd8khhv1hd6")))

(define-public crate-genco-macros-0.7 (crate (name "genco-macros") (vers "0.7.1") (deps (list (crate-dep (name "genco") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hj4gw09jbci6f4142v3dwxibc2hfz2l8jndfx8v4apg9d880cv5")))

(define-public crate-genco-macros-0.7 (crate (name "genco-macros") (vers "0.7.2") (deps (list (crate-dep (name "genco") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05v0ahv2mkjdkpjrsv8n3hicnn7zjdsymr563f1wmm0gp3qnglq4")))

(define-public crate-genco-macros-0.8 (crate (name "genco-macros") (vers "0.8.0") (deps (list (crate-dep (name "genco") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0864rmcj52w1kxgzn71482b9pdndg6sn07yqb9wqa5yn93v9ipsx")))

(define-public crate-genco-macros-0.8 (crate (name "genco-macros") (vers "0.8.1") (deps (list (crate-dep (name "genco") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mpkh1vm364x5ri28fa4ydxl9nz4wsrs0gkndk79ghnwmnx7y146")))

(define-public crate-genco-macros-0.9 (crate (name "genco-macros") (vers "0.9.0") (deps (list (crate-dep (name "genco") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1naba6hc7dbdv1aj5pfwgx2fvansvdi7r3vjwfpjcxfq20hmfbpk")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.0") (deps (list (crate-dep (name "genco") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bvyi4i61p2758qp2g7g70pdakb4hzrkwxqsr763ir48dlxw9rla")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.1") (deps (list (crate-dep (name "genco") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vgincga50sz60gci3y15r7mqhdr42dm9k0yx9y3gdqawgj1zxcw")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.2") (deps (list (crate-dep (name "genco") (req "^0.10.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hcykvi1jxigaxdjpcwq14wcggryqzcc14rayj665r2mhklsah2g")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.3") (deps (list (crate-dep (name "genco") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04m3v4q835rqgbs0jxxbzvzh1600z2l8m7xi6mxfqycl8rgrzw2w")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.4") (deps (list (crate-dep (name "genco") (req "^0.10.4") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04w20brfjcsfqb4jzbahbrcgfamxm565iizsrrmgpzhgg6sm6aj6")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.5") (deps (list (crate-dep (name "genco") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l8f73dfvd3acvvxsg8whli2xk7g906y29anwz1g45za0zz2zl27")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.6") (deps (list (crate-dep (name "genco") (req "^0.10.6") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lh3bp8jfb55sz95h4jv93p48j8jfh7kxswd5pg3r3ingz8ixkqs")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.7") (deps (list (crate-dep (name "genco") (req "^0.10.7") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nnsl1x409bsggjlfixs829q71i1ig2yp14vszb19dhcqjjp0xax")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.8") (deps (list (crate-dep (name "genco") (req "^0.10.8") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bhmh4rpccjkc6wcq6a8mxmfyf5zvfd8vzhiknssj43nwm8s35sm")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.9") (deps (list (crate-dep (name "genco") (req "^0.10.9") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00hf4fm5wh6v9xgi7lsxa4idr6flnvhh7qk2xdcpp292gm99rk8r")))

(define-public crate-genco-macros-0.10 (crate (name "genco-macros") (vers "0.10.10") (deps (list (crate-dep (name "genco") (req "^0.10.10") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0567s26kiahlgsz2r494yixdbwl59ijh8qsq9q3bqj1zhspnajg3")))

(define-public crate-genco-macros-0.11 (crate (name "genco-macros") (vers "0.11.0") (deps (list (crate-dep (name "genco") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qbkazz5qwayadl5657ni1xsfs2sava1i2qh5alwj92ns2h2rgyh")))

(define-public crate-genco-macros-0.11 (crate (name "genco-macros") (vers "0.11.1") (deps (list (crate-dep (name "genco") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cwlmngna1aqkzr4rf8gfxbplcp73rsh91966z3drxwf6cnxri7k")))

(define-public crate-genco-macros-0.12 (crate (name "genco-macros") (vers "0.12.0") (deps (list (crate-dep (name "genco") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jpq0ryiqdfynjsqvyhl3z2af72bz60nb1c0v6jf94bfxr9q8dvf")))

(define-public crate-genco-macros-0.13 (crate (name "genco-macros") (vers "0.13.0") (deps (list (crate-dep (name "genco") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lfp1y9na2kgrk7wp98kn4mq0pyvpc93slxf1w7v6my5kbcvz7xh")))

(define-public crate-genco-macros-0.14 (crate (name "genco-macros") (vers "0.14.0") (deps (list (crate-dep (name "genco") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02am226zjlx2qrlka393fvxr5glmrw4mr4gsf5pc1h8lk7b3y3hk")))

(define-public crate-genco-macros-0.14 (crate (name "genco-macros") (vers "0.14.1") (deps (list (crate-dep (name "genco") (req "^0.14.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kq6r3d8i7pfsi0ngpg4576n3vk93k3paq962wwmqyc5lcdncdvb")))

(define-public crate-genco-macros-0.14 (crate (name "genco-macros") (vers "0.14.2") (deps (list (crate-dep (name "genco") (req "^0.14.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wb8w4fqnb7qk0jxc3ajwa138w2cydgbk93mgdpwhzfzmnfyz109")))

(define-public crate-genco-macros-0.15 (crate (name "genco-macros") (vers "0.15.0") (deps (list (crate-dep (name "genco") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0iihrfjgjy61xanki7150y1j979fbnyhc3nnbd1kb1fv7h62mhqm")))

(define-public crate-genco-macros-0.15 (crate (name "genco-macros") (vers "0.15.1") (deps (list (crate-dep (name "genco") (req "^0.15.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ny5a94w9mkwc1bvc814d4ki9m5zm6q3h31ys69wcz146ydpanwk")))

(define-public crate-genco-macros-0.16 (crate (name "genco-macros") (vers "0.16.0") (deps (list (crate-dep (name "genco") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "193r2m7df9f7djslnhp0z3655bfdydzarpg4icvnghdkl2cqyrrs")))

(define-public crate-genco-macros-0.16 (crate (name "genco-macros") (vers "0.16.1") (deps (list (crate-dep (name "genco") (req "^0.16.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fd2gkp73wwj747f4fhl5fcnvlwk8f0q2dbma1jfz6wg147x9bm9")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.0") (deps (list (crate-dep (name "genco") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p4x2bmpqd3kkbddzcmwxq0b5xj1wgrvp9ifd6iklb62ra6djdlw")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.1") (deps (list (crate-dep (name "genco") (req "^0.17.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06gsh63i5vcammznlfjml4k14ribrraq696ykanhnmdi5b38s83j")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.2") (deps (list (crate-dep (name "genco") (req "^0.17.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yv63nwk1al40dih67sga5r5mq7n644l4qlf11zqfk7qawkkz020")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.3") (deps (list (crate-dep (name "genco") (req "^0.17.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ah6yfsgzvqpwk43ajyf42p74lf0pzvzcjja6kk0ixxi914d6py8")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.4") (deps (list (crate-dep (name "genco") (req "^0.17.4") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "035a3x42601y2pxxh6j6b4yvgqhbyp59r3s0gkq3wb69q8iaxikg") (rust-version "1.58")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.5") (deps (list (crate-dep (name "genco") (req "^0.17.5") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0slj3ngssvl573ks45j9mcgl4jk0srch542kxpxx05qry267fb4w") (rust-version "1.58")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.6") (deps (list (crate-dep (name "genco") (req "^0.17.6") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0skh6cw8lj679ij2dyqlh3lyvjswb0alaxcj1qz823y3fd6cladh") (rust-version "1.58")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.7") (deps (list (crate-dep (name "genco") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s95a07ffyldjy3d3cny1bx4yd8cil7skniap82ywcigvv9qq74f") (rust-version "1.58")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qdxnhzxpky09lxzd7z3p89rmkicwlpr6wkg24jpiwaax9piikyl") (rust-version "1.58")))

(define-public crate-genco-macros-0.17 (crate (name "genco-macros") (vers "0.17.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "q") (req "^1.0.3") (default-features #t) (kind 0) (package "quote")) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y0c83q9mlgyjfbfqfk26gcn1f49vxgx57w4n1169dzpmpz30djm") (rust-version "1.66")))

