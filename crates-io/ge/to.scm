(define-module (crates-io ge to) #:use-module (crates-io))

(define-public crate-getopt-0.1 (crate (name "getopt") (vers "0.1.0") (hash "0fr8wcavaqkcill7dysz8vdav79jg9v2z97246q60g8rfy3rq18d")))

(define-public crate-getopt-0.2 (crate (name "getopt") (vers "0.2.0") (hash "03rvjm5xnqyca6r8ddpd0j8rijps54dj8wzdmi9fbvnrq97gz1rp")))

(define-public crate-getopt-0.2 (crate (name "getopt") (vers "0.2.1") (hash "08bv0jccy73krxwhfgv8lalvy00yzi0ib4b2hwy1xfrdlkizvkpi") (yanked #t)))

(define-public crate-getopt-0.2 (crate (name "getopt") (vers "0.2.2") (hash "0fx35phlrsmr3syjwj272jql0mqva3q706br0fy8vyxz1d7g1rsn")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.0") (hash "0l0smqwnyxavysmyc5vbdlnsazwn7lfp424bcv5xkz11i6wn1igx")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.1") (hash "131i657s4cxii1v7w3jrsrzzxhr0nq3n6kz9kmdzhbndrp1xvacv")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.2") (hash "160ialipmpzj1gp3pkw7d4p5c76wsj2c42l07mf7xwvj87asbajq")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.3") (hash "181yccsnilxwhmc7r72cvzng20x9s9p7szqm0hj087vgs3d6w538")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.4") (hash "1f3pqcp7lb1z1lkcip7b38pcbi2k3vd9q3d2alxsyvxz6nhzwx4x")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.5") (hash "0cnliqyir4b483zgpz94fcdh1n7dwihys357n79h3czbqlzvp3mj")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.6") (hash "1ki2w1sawffxnn5bsb97v0hg6ndl5a3g1xaff85j44ymb8aggvbp")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.8") (hash "18q276vmj44fw4qihdp18h2s1s6ffbj3b9v79plsixwj5rvvi53g")))

(define-public crate-getopt-0.3 (crate (name "getopt") (vers "0.3.9") (hash "1ykyysrspdwgqcp8rdx6fkqmx79rnafal1iyrb4109kws42m63am")))

(define-public crate-getopt-0.4 (crate (name "getopt") (vers "0.4.0") (hash "0dl0dv6bcqfkm9xj7l0q9yq5sngai4fpvgp8h4qk8bb0mbyi6b9y")))

(define-public crate-getopt-0.4 (crate (name "getopt") (vers "0.4.1") (hash "0v1rpplirhbjaf45lh0dyqxx7kl5rzgcv7zb9hi0rz49jyqv30gg")))

(define-public crate-getopt-0.4 (crate (name "getopt") (vers "0.4.2") (hash "0xva00848bjf1mwmb82c6v09vl3dm2hyzsgdyk1irr323nfyskvv")))

(define-public crate-getopt-0.4 (crate (name "getopt") (vers "0.4.3") (hash "08pw5r3khnrpn14mad1v4b8w6fsajznjhx66p9l3l0n0vym2alm9")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.0.0") (hash "0fkqf193r9gfkyj1pna8k2qg3fml9b4zmbpzzyqbvnaa1ns2062q")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.0.1") (hash "0h11nl11d9jgxq8wmklj643jz9sigvpj5gn7q35sdy6n4fz94mav")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.0.2") (hash "00n3dqrxh1qm282c25ziwgg093llk2db5gnp7rkxzspij0j7z5h9")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.0.3") (hash "0kldz60xww36bykmhfzc4bm8pph2d03wfp723hkny2qn1xagg5h8")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.1.0") (hash "18j9mjbgg668zqqk5wm66r9l40ns7fracifxfh7a00wcpiqvjp18")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.1.1") (hash "02shdxcn8a7bqg7qv1hsc621q7n0sd16zf6bkbbwswmsm0ywkihn")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.1.2") (hash "0d4dvbajfsfpcmfd3wvi7cdqbkhbpqqzh1ix5jigzc7nldy24gqm")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.1.3") (hash "1iz3lm6sl3m2irx2bzv517133ywakxcvsxlpkf5c5rqsrgk8j4pf")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.1.4") (hash "0yn4iz04y1l8ljmh20zjjlfm6ma6kg1ayy7155qca32y78jrjdlz")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.1.5") (hash "1hw3d54gphrmhw1ym1vk8hd617zckp56jswz3l9n1qjx807qr1xb") (rust-version "1.30.0")))

(define-public crate-getopt-1 (crate (name "getopt") (vers "1.1.6") (hash "01r8shfvh7ai2svqcs91s2h2n4hyv569kjrq2nzyvp5yz4ilwyym") (rust-version "1.30.0")))

(define-public crate-getopt-long-0.1 (crate (name "getopt-long") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "002ks8p8nh5cxss2npjyrh57dvq3jcvrlj01apljs9j74ghv3xsw")))

(define-public crate-getopt-long-0.1 (crate (name "getopt-long") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "0hdv6dx2gxxwlgp2393zdwf8h00m789bk1lxjf35ca15sbxgi02i")))

(define-public crate-getopt-long-0.1 (crate (name "getopt-long") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "1bfws1cxkqqpg52yjhdywddiaas6ml0bkglhmi8lr5rqicpnxqrk")))

(define-public crate-getopt-long-0.1 (crate (name "getopt-long") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "0aaaxr7zsi4k4gd09vlhddzv37679nr3xjlpsc7vykfwv9239c92")))

(define-public crate-getopt-long-0.1 (crate (name "getopt-long") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "01w5q9hpxdg2wi9nccrjrvrvh09fh0mfv4dfnqf19asm00fqdg2w")))

(define-public crate-getopt-long-0.2 (crate (name "getopt-long") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "19xw5b64maf8bvw1zkki79ic1p3irbx81nfcs9d2dyi7796vlv3s")))

(define-public crate-getopt-long-0.2 (crate (name "getopt-long") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "1zl7yy8i040blnaai3dwick2slnxcibnfi4yc20jz8k0ydh5grwm")))

(define-public crate-getopt-long-0.2 (crate (name "getopt-long") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "1bfnr42ing3i9rwk1jp42ns61l3f82hm3fg2sq8y532fqj8xzid3")))

(define-public crate-getopt-long-0.3 (crate (name "getopt-long") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0.50") (default-features #t) (kind 1)))) (hash "0jk78rzkw945vmn8hpai1zfvz1njzrr244kziri8n5wcjxniy626")))

(define-public crate-getopt3-1 (crate (name "getopt3") (vers "1.2.0") (deps (list (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 2)))) (hash "06441xn8akayj5w90vdywif73kbv850sbblw6rbrxsg8a197vfnl")))

(define-public crate-getopt3-1 (crate (name "getopt3") (vers "1.2.1") (deps (list (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 2)))) (hash "085yzfdmg86b0kbzls2hc4kvfq9mz5nd33zpm146v6cddkxwbixg")))

(define-public crate-getopt3-2 (crate (name "getopt3") (vers "2.0.0") (deps (list (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 2)))) (hash "1737iy6gmwb4h7xzmnx6yhzlliv5kg131n55a3bpm6hyjjhrakmy")))

(define-public crate-getopt3-2 (crate (name "getopt3") (vers "2.0.1") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "01hchvpqfkfgz4ydz59b1kmnkrf840jhwr8syinfaql0wxavgri9") (features (quote (("no_std") ("default"))))))

(define-public crate-getopt_rs-0.1 (crate (name "getopt_rs") (vers "0.1.0") (hash "12w7gwjz3pyjgaz4rgr24q7ipvbznzz9bswjpgz24gsbqyzfz0nx")))

(define-public crate-getopt_rs-0.1 (crate (name "getopt_rs") (vers "0.1.1") (hash "0ygc27c58f5ivw1mzhhx52bf75wa17x5mra411zn4ww91ahlaxsk")))

(define-public crate-getopt_rs-0.1 (crate (name "getopt_rs") (vers "0.1.2") (hash "1bxcgi3hnagiykyi6rx8s8jz70hqndi36bbw3h7lpr654csq0lzm")))

(define-public crate-getopt_rs-0.1 (crate (name "getopt_rs") (vers "0.1.3") (hash "1dlgmyqzbr584dxn9p3l8xlc517vxmcb0k8wlxz573zq0ldzmcnx")))

(define-public crate-getopts-0.1 (crate (name "getopts") (vers "0.1.0") (hash "0ma8hcswnq9mm6xf6p6qbqdkgnk8xqn0lbhy61lg9wyz312k13i2")))

(define-public crate-getopts-0.1 (crate (name "getopts") (vers "0.1.1") (hash "0r7cc9xmivh0y4dwjs0wlbh2lnhk29xscccid1k277xhhxg5vvd7")))

(define-public crate-getopts-0.1 (crate (name "getopts") (vers "0.1.2") (hash "0frhsq1sbxlmmlz14cp9rnpchnnm9z0nqia8nv3xgafq0h86b6mc")))

(define-public crate-getopts-0.1 (crate (name "getopts") (vers "0.1.3") (hash "1wdslw603vqg96csch9nrggkaimnx2msy96bjixmp9h6fs5x4jhg")))

(define-public crate-getopts-0.1 (crate (name "getopts") (vers "0.1.4") (hash "1cydr3q45sl98c6hcx8np9l070c9r2prb9bhfdpzxzvpzja2pzpa")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.0") (hash "1zgvaqpg3rc0kvwicscx3w5pj0dn00ds86ddgdv80d4r7banygni")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fhnd0xbhj14v9jf5ln890v5mfwr6d0ds0nnljca6m6w3pb9yw95")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)))) (hash "14hysvyslgqvbznc8d5g0b6qnd4yxfhylnipz632q67p9yqvry7b")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)))) (hash "14c1fhgz7l4jvszmlq5v8yvkzhx1jhjhs2xhdl4xnw5m0mrzpq0w")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.4") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)))) (hash "03gdblzazk01rbbap7jagx1w3pwmikc4lrpqs5dnsk4w6wl6lv34")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.5") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yyf02axlwnhkk29656j0rdhscr2ppwa46dnf3d0773r7gbim60c")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.6") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1r444fdqqa3l2yw46arafykxmw4fbjzdsv0wbgs0swy1mai881ai")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.7") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0j499c6kafjd5lyncg583dhj8366hhp273q9dcjwfyx41m27q81f")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.8") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0222xy9bhmi386d1anq3krfvl0x1ssi01camlanvd5nsxwmqawm1")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.9") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zgpjaw1y1ip2nmw2jmiawdi21xwc8gy6wmfaz4f9kj6mxg9rarv")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.10") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ipivajw7wmq98mkycf5sfzi7d2zqx4x24mw0j4igh25nzfs9glc")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.11") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "05g67hi28wgsvjspifqsgspxqihy7zwaiszp6pzzw7is8h9qsya5")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.12") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "11ixqrx5mdpb79sgqdc65zm4ig9yjnail8pyfs1dspgcn74qhw29")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.13") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lrny7zmyl9ys6m6fvylg9i3zzg57fjraps987frjvrylddg0fn1")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.14") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 2)))) (hash "11fnh4awj227c85ax1sixbl5yb25c3qmxlv3nd870hwas3xpq16r")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.15") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 2)))) (hash "1yggwpnqcpqbaa1dvhyrgal593k6cvfbmbhfxfi03wfjmdqji4k5")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.16") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 2)))) (hash "0zwzlb6y98mm9wzg92d9mqgnida8m1qhbcqrj8bw57axwjvbbbak")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.17") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 2)))) (hash "018yhq97zgcrcxwhj3pxh31h83704sgaiijdnpl0r1ir366c005r")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.18") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "15xpa5ljczsylavlnm7y2v2y91iaa41drxalncj59yrj079r4wha")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.19") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0l74ldicw6gpkly3jdiq8vq8g597x7akvych2cgy7gr8q8apnckj")))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.20") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "std") (req "^1.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-std")) (crate-dep (name "unicode-width") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "09vkkz6flm0hb5bpmx8dqlxg6h46vb25acdncpwc4wfx6f7lz990") (features (quote (("rustc-dep-of-std" "unicode-width/rustc-dep-of-std" "std"))))))

(define-public crate-getopts-0.2 (crate (name "getopts") (vers "0.2.21") (deps (list (crate-dep (name "core") (req "^1.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "std") (req "^1.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-std")) (crate-dep (name "unicode-width") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1mgb3qvivi26gs6ihqqhh8iyhp3vgxri6vwyrwg28w0xqzavznql") (features (quote (("rustc-dep-of-std" "unicode-width/rustc-dep-of-std" "std" "core"))))))

(define-public crate-getopts-long-0.1 (crate (name "getopts-long") (vers "0.1.0") (hash "0drbxaf58ygjg5fgri5xc24kjxlv9sis0ba1nm38ch5mnhj1srrj")))

(define-public crate-getopts-long-0.1 (crate (name "getopts-long") (vers "0.1.1") (hash "02ji8hf22lnbg60agjc9vmpzysv3cak3kh9bghga6fqjhqwm17h9")))

