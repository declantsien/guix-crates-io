(define-module (crates-io ge l-) #:use-module (crates-io))

(define-public crate-gel-o-0.0.1 (crate (name "gel-o") (vers "0.0.1") (deps (list (crate-dep (name "epoll") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "evdev-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.8.2") (kind 0)))) (hash "1hgv4ngp47cjayn74yrb7na4ngcs4iivmfs07zgpmvnrvrbxm1mp")))

(define-public crate-gel-o-0.0.2 (crate (name "gel-o") (vers "0.0.2") (deps (list (crate-dep (name "epoll") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "evdev-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.8.2") (kind 0)))) (hash "1is15malfa2gssxf8nab869pbf55nlkmwirr7hfrqm7j8m0lflzg")))

(define-public crate-gel-o-0.0.3 (crate (name "gel-o") (vers "0.0.3") (deps (list (crate-dep (name "epoll") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "evdev-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.8.2") (kind 0)))) (hash "1zfjc1hm3w7m6lkrn2wb6xz2l1icgaf6vyz0qv7827wnnq1yb64f")))

