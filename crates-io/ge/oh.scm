(define-module (crates-io ge oh) #:use-module (crates-io))

(define-public crate-geohash-0.1 (crate (name "geohash") (vers "0.1.0") (hash "10cigl33vmrjd10swl9z0xknp0xgmfq3022j050l4lx2mbif9l26")))

(define-public crate-geohash-0.1 (crate (name "geohash") (vers "0.1.1") (hash "0zirmlf8x0jz24dwsdwxfgxfyhqvpnj30a3jps2kpzr5j3yasrzr")))

(define-public crate-geohash-0.1 (crate (name "geohash") (vers "0.1.2") (hash "0lc0lsbnwib4vay9mv2hg4bqb0kx5vwmkxagzsb6m65j2sc5ynp2")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.0") (deps (list (crate-dep (name "geo") (req "*") (default-features #t) (kind 0)))) (hash "1lkwgya8g67v1177mfzmpf3mlq82fzv9ws8lcdnl90n5pp6wis3s")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.1") (deps (list (crate-dep (name "geo") (req "*") (default-features #t) (kind 0)))) (hash "0r45zjjhjrjwjl8vs97j2iqawz8ix03aw769q3481gmrgjm3n3yx")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.2") (deps (list (crate-dep (name "geo") (req "*") (default-features #t) (kind 0)))) (hash "1ci0fwjddig4hfl9cspvj32r02p9grfk9idn31fp8l3xprdd9qgb")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.3") (deps (list (crate-dep (name "geo") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 2)))) (hash "1vjnizy2fjv3mwy8j6fkg9zbfbmd46rywib89gnv3vi6cpdrd1ja")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.4") (deps (list (crate-dep (name "geo") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 2)))) (hash "0yprda3kjipbp7isbflqcwwmc763n50i9dzaax21dhk1bf15jzaf")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.5") (deps (list (crate-dep (name "geo") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 2)))) (hash "1my0lvl81fxiq8lhplxagx0f91kqglrz0frq22w8mvgrf9dx09lj")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.6") (deps (list (crate-dep (name "geo") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.30") (default-features #t) (kind 2)))) (hash "1w21w4sm1ndy027n87j3jsm18l5gqb1p2indmngl28rrzdglpz2m")))

(define-public crate-geohash-0.2 (crate (name "geohash") (vers "0.2.7") (deps (list (crate-dep (name "geo") (req "~0.0.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.30") (default-features #t) (kind 2)))) (hash "1cw633lmxrpc9kvfd5q3cfvhmdvjhcs9md709r5rwi41c52a5x1i")))

(define-public crate-geohash-0.3 (crate (name "geohash") (vers "0.3.0") (deps (list (crate-dep (name "geo") (req "~0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.30") (default-features #t) (kind 2)))) (hash "1j5cik0bfy76d94mzr4b3kz3p3p56qq0gkhjcq3jfj5n2s2cjmk5")))

(define-public crate-geohash-0.3 (crate (name "geohash") (vers "0.3.1") (deps (list (crate-dep (name "geo") (req "~0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.30") (default-features #t) (kind 2)))) (hash "0pg4njw67x4zzvzvbdc4kidsh2hz54l6if3b7n9hs4swn7l7bssk")))

(define-public crate-geohash-0.4 (crate (name "geohash") (vers "0.4.0") (deps (list (crate-dep (name "geo") (req "~0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.30") (default-features #t) (kind 2)))) (hash "1rjl9qc6vzz7fy2aswj158wj004nzqpl2prrh5dpvi32an1ybbyp")))

(define-public crate-geohash-0.5 (crate (name "geohash") (vers "0.5.0") (deps (list (crate-dep (name "geo") (req "~0.6.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.30") (default-features #t) (kind 2)))) (hash "0ahywwr3l8by9vhfkxdkfnwkj87zxxbvwfw3bhly537vs7k71r1f")))

(define-public crate-geohash-0.6 (crate (name "geohash") (vers "0.6.0") (deps (list (crate-dep (name "geo") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "1dp6nzrplx03d38d7xc94ki073b582ifvl5rnx5n83al1s3s5i2l")))

(define-public crate-geohash-0.7 (crate (name "geohash") (vers "0.7.0") (deps (list (crate-dep (name "geo-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "1xcwrjgsda4aa56vf44ff1v7hsx5b51pi4rfkdg92995623469iw")))

(define-public crate-geohash-0.7 (crate (name "geohash") (vers "0.7.1") (deps (list (crate-dep (name "geo-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "0grf00qhsmjcas7ly1pflnj9igyqsdamcx39m4bdrqp9s4yh9rlh")))

(define-public crate-geohash-0.8 (crate (name "geohash") (vers "0.8.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "0pa6m8n2fbgl79vb6w0cng827nihp80djzszi28gl12pyrlmm13r")))

(define-public crate-geohash-0.9 (crate (name "geohash") (vers "0.9.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "11mjfjyq993fa1hb71jv0rcp18pcgb9va2lzkjs5i3nhvcb77jd4")))

(define-public crate-geohash-0.10 (crate (name "geohash") (vers "0.10.0") (deps (list (crate-dep (name "geo-types") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "1kx0cfw4n3w3n2wfla4i0q6j40mb85ihrfs3n5g1hiv9a5kvfsg8")))

(define-public crate-geohash-0.11 (crate (name "geohash") (vers "0.11.0") (deps (list (crate-dep (name "geo-types") (req ">=0.6.0, <0.8.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "19qg0nc11fnvvm8prgj089l19yl0vmqigjh8bk8c47kd61pf0w66")))

(define-public crate-geohash-0.12 (crate (name "geohash") (vers "0.12.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "geo-types") (req ">=0.6.0, <0.8.0") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0kq0iacnpl7bl2vl02grki82f5fwb6jjsrzjs4pn1kys5s7gnc95")))

(define-public crate-geohash-0.13 (crate (name "geohash") (vers "0.13.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "geo-types") (req ">=0.6.0, <0.8.0") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0il53q682g9c89h52sw52wsil0i9qjf1g57xrk4z391zn44vh64a")))

(define-public crate-geohash-0.13 (crate (name "geohash") (vers "0.13.1") (deps (list (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "geo-types") (req ">=0.6.0, <0.8.0") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "1rmibl9jrc9qwnz43y3lcv114j1nm90912lm4aznq7a0cld4pf8g")))

(define-public crate-geohash-16-0.1 (crate (name "geohash-16") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "0qkj69qmyf0kcwj1nb44mwpy2hclngbi2p6ca3vv4x60cbwf1liy")))

(define-public crate-geohash-rs-0.1 (crate (name "geohash-rs") (vers "0.1.0") (hash "17gi2hylqfbkjq2zxj79mrla6mcl9k8ivfr76bba6xcp25lxjj89")))

(define-public crate-geohash-rs-0.1 (crate (name "geohash-rs") (vers "0.1.1") (hash "182qbi0hqhj9kg6hqd029jqk49bff719dn08qi0y92hd1w895pws")))

(define-public crate-geohash-rs-0.1 (crate (name "geohash-rs") (vers "0.1.2") (hash "1awm7nmd2xhqq174r4xvg98mdbqkv0nhmkfwgxmzm0h95bdvf308")))

(define-public crate-geohash-rs-0.1 (crate (name "geohash-rs") (vers "0.1.3") (hash "164cb19pn7arps9s00c9b395wma627a95pcs2piwwn9ny0m0qxc6")))

(define-public crate-geohash-tools-0.0.0 (crate (name "geohash-tools") (vers "0.0.0") (deps (list (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "06ykak3nplkxkfphyxadzdh8zhkdqb653h24k8yxbwijpzzc613r")))

(define-public crate-geohash-tools-0.1 (crate (name "geohash-tools") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "05v66n0q6v7spqvzydkb94nmbp005mgvrb09gj3y4apmhdm9563b")))

(define-public crate-geohash-tools-0.1 (crate (name "geohash-tools") (vers "0.1.1") (deps (list (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0nyi6854dhfhfwdnws3fpbw32cmx78iahwm2n7zsbq9yz2hcng9f")))

(define-public crate-geohash-tools-0.1 (crate (name "geohash-tools") (vers "0.1.2") (deps (list (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "14b6mjsvqjx2y5pbicdivn9f2nxs8kkkgl60f8bvi4i6yzp458wa")))

(define-public crate-geohash-tools-0.1 (crate (name "geohash-tools") (vers "0.1.3") (deps (list (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1wg5k1qv5yxiwh7xhlsfyx0n41iyw79iglj1pzdddghq8lzjnn14")))

(define-public crate-geohash_gps-0.1 (crate (name "geohash_gps") (vers "0.1.0") (hash "0nipcisbriswwsz3rvprv1hq5a8zlnbbfkdrd4znjafm7jh70p0m")))

(define-public crate-geohashing-0.1 (crate (name "geohashing") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "108mxhl7paxsp10iap8vwwmjphr2wiyif8pa4g2bsly2f9rbfg11") (features (quote (("online" "reqwest" "regex") ("offline") ("default" "offline"))))))

(define-public crate-geohashrust-0.0.1 (crate (name "geohashrust") (vers "0.0.1") (hash "1pjk804l7bcyd7p035jl7pl2wpvb45liddn67sw7d4vcrx7aribf")))

(define-public crate-geohashrust-0.0.2 (crate (name "geohashrust") (vers "0.0.2") (hash "1b4k6av5kmlg7v18xpc4hm3zkyaxwbfg049crgww2hnnyaiwldwf")))

