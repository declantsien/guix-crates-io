(define-module (crates-io ge mt) #:use-module (crates-io))

(define-public crate-gemtext-0.1 (crate (name "gemtext") (vers "0.1.0") (deps (list (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "05060fcyyj8n91vvn7i99myhdxpld45wjcw8g346gw2v6g0xi394")))

(define-public crate-gemtext-0.2 (crate (name "gemtext") (vers "0.2.0") (deps (list (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "16994mvf7rqppsagp4m0xkp2f3m0276zg02naigm537nsssxvqk3")))

(define-public crate-gemtext-0.2 (crate (name "gemtext") (vers "0.2.1") (deps (list (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "0nsbbj5j6sk30i7jwbb9anss8a42ggfr625adhw0ni4qnl26mdrf")))

