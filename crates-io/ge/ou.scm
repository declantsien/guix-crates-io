(define-module (crates-io ge ou) #:use-module (crates-io))

(define-public crate-geoutils-0.1 (crate (name "geoutils") (vers "0.1.0") (hash "0ik3bj20djl16r4pnngpaazvxd2nwgf830bcykm2a4hf7bsh0s6m")))

(define-public crate-geoutils-0.2 (crate (name "geoutils") (vers "0.2.0") (hash "0fm9fkvvrz09nj70iml2wk7nrdfh3b7wp8nafrca5iywy5psvzrb")))

(define-public crate-geoutils-0.3 (crate (name "geoutils") (vers "0.3.0") (hash "0qc35ylkrpazgvizxzwgplxk3dd100jy9cpv5lrarxh48w6j58xx")))

(define-public crate-geoutils-0.4 (crate (name "geoutils") (vers "0.4.0") (hash "1gq013x065pffmwjcql7c0bsaxlkmkqv6s8gzra2wz92g70cada6")))

(define-public crate-geoutils-0.4 (crate (name "geoutils") (vers "0.4.1") (hash "07nx8mv8v4i9s6dhg65hp2fihwzd8czvnbhxrrm3jza0d9hny04y")))

(define-public crate-geoutils-0.5 (crate (name "geoutils") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0q57qssrdzkg0ms6dlxiz0k7147wjsrw081kzpccyj5cyw3m5h9n") (features (quote (("default"))))))

(define-public crate-geoutils-0.5 (crate (name "geoutils") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bd6nzrk85y1qdx7wvf4xqajswr5g65jpbdbpig9nc8kh6h49lin") (features (quote (("default"))))))

