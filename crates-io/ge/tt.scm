(define-module (crates-io ge tt) #:use-module (crates-io))

(define-public crate-gette-0.0.1 (crate (name "gette") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^0.56.1") (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-s3") (req "^0.33") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "path-clean") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4") (default-features #t) (kind 0)))) (hash "05qi74d11m82hc2f774vg815pllk89wpg85m7cf9r6h5wn1qnj93")))

(define-public crate-gette-0.0.2 (crate (name "gette") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "aws-config") (req "^0.56.1") (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-s3") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "path-clean") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4") (default-features #t) (kind 0)))) (hash "13pgdawh0d7br6rxsa522csv5qaqj6570ik1l5sy0mdcfha1i8rr")))

(define-public crate-getter-derive-rs-1 (crate (name "getter-derive-rs") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0v0sf6z495ska7dajahwhnqfkgd93cxwlx88f28146wx7l00rq9l")))

(define-public crate-getter-derive-rs-1 (crate (name "getter-derive-rs") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0m943hpmiha556lbv070lswnaiicjpvhkry1pgzwi8wsb85gdsal")))

(define-public crate-getter-derive-rs-1 (crate (name "getter-derive-rs") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "016bc5dnasq765455850ld0bz3zca3ch75ccfi4dh29xr1skrfiw")))

(define-public crate-getter-methods-0.1 (crate (name "getter-methods") (vers "0.1.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bmgk8ai2d8pga2p9v4k28118basj6pcqr956m3njbwami2k7b74")))

(define-public crate-getter-methods-0.2 (crate (name "getter-methods") (vers "0.2.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0s0v4sizbgy48kn3vbkaqpb935g5s99gh3m8hcl0f5s8dzri8xmx")))

(define-public crate-getter-methods-0.3 (crate (name "getter-methods") (vers "0.3.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1w179pw3flb5hwqlcrpn48bjb8vs76bccgq88mcd7yxn7ajmz0ys")))

(define-public crate-getter-methods-1 (crate (name "getter-methods") (vers "1.0.0") (deps (list (crate-dep (name "assert2") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0f9qw22690m6qljkkl869a1xw4q4s5ccpgrj9g32zasjmfx995pk")))

(define-public crate-getters-by-type-0.1 (crate (name "getters-by-type") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1w0ah005m1rhdgdnfhax1ghnm9386r9alaavbkgfbz5j0ckvm3vn")))

(define-public crate-getters-by-type-0.1 (crate (name "getters-by-type") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1ff4576x43fmqxrrwy1vdcysvbhb0lqw8s7f4har4j23kvz6nzzs")))

(define-public crate-getters-by-type-0.1 (crate (name "getters-by-type") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "052kviixf9id8y5233hlvmiamnbhwcj92ammljw3kl050sdcj0cg")))

(define-public crate-getters-by-type-0.2 (crate (name "getters-by-type") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0vv4h704nxl3haja49rxbqw9s4mxdlqd6720g8wjp7kxyqwj66vn")))

(define-public crate-getters-by-type-0.2 (crate (name "getters-by-type") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1y5321m7pf7sa3b5c5gw2hgjzbqvy3vq3cgg88ar9s9k26fxh71r")))

(define-public crate-getters-by-type-0.2 (crate (name "getters-by-type") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1svr5zrx6jc9fkir5lqjwcdwwkbbjk3s0aznj79vs6m0rrbk5jzr")))

(define-public crate-getters-by-type-0.2 (crate (name "getters-by-type") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0i7bn3mn4i87p3zggcpmgygjsv4pl0ppp1f74al0j762s4xwvsri")))

(define-public crate-getters-by-type-0.2 (crate (name "getters-by-type") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1d5g46ifw3fdn2vllh5vs7nbc50dpi3zblqp0hf28asnrzfsjmz5")))

(define-public crate-getters-by-type-0.2 (crate (name "getters-by-type") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1b6003bpwcqxzzm6plblyl5709xqivd6ws7yh2x4bh483raajid0")))

(define-public crate-getters-by-type-0.2 (crate (name "getters-by-type") (vers "0.2.6") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0cpjg6xdhi63453ls27dzznxpdml3r777f6nx18qlli4bya3965v")))

(define-public crate-getters0-0.1 (crate (name "getters0") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0q90a42xck6yqa6licavvv96ljmbv50ll3m7srxjp1pnp4w000vx")))

(define-public crate-getters0-0.1 (crate (name "getters0") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g0wnsykx1nqyhy50921dlsf2bpk1jc3bnf8sf6fbmpm5klxsmjf")))

(define-public crate-getters2-0.1 (crate (name "getters2") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z1ydk988qjk2291lzx1zq80w5jf48idv2jwxjgnz51mnly8048x")))

(define-public crate-getters2-0.1 (crate (name "getters2") (vers "0.1.2") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vcy7dl0v0ba3x9zvgf0h0jhjnscydnhpqka6jh22gvbh04v7ai7")))

(define-public crate-getters2-0.1 (crate (name "getters2") (vers "0.1.3") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rl1kqnm3528nz3k66lnbqx41z676xm3apw7xk2qalzagidylq71")))

(define-public crate-getters2-0.1 (crate (name "getters2") (vers "0.1.4") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04png5gl9y18wz84xgvc891lpjk2wx0n9j1qd61axganbxg31rpp")))

(define-public crate-gettext-0.1 (crate (name "gettext") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "157z06b7asqvbq3m3d10diarsajs8yil6zyx807yv3qw3m36piv9")))

(define-public crate-gettext-0.2 (crate (name "gettext") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "0jpqd38hvh433cih1bcp2j1ri3q02jhh0f1zxi300qyiqk9x3h9r")))

(define-public crate-gettext-0.3 (crate (name "gettext") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "1x1iwbx7kfgasv3mid7ycbmzx3imbjd94h4gsjqgs76mkzhbhy23")))

(define-public crate-gettext-0.4 (crate (name "gettext") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "0wd9kfy7nmbrqx2znw186la99as8y265lvh3pvj9fn9xfm75kfwy")))

(define-public crate-gettext-macros-0.1 (crate (name "gettext-macros") (vers "0.1.0") (deps (list (crate-dep (name "gettext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gettext-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03a1yczhrxs80xh4i98wdv8a8jgjrbs1zr20x1iq9czqkicyy9j5")))

(define-public crate-gettext-macros-0.2 (crate (name "gettext-macros") (vers "0.2.0") (deps (list (crate-dep (name "gettext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gettext-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ncvsbyzbn76sc3ks84v18inzl6i2ix8k8gp0m4j9iq88yp2g2vn")))

(define-public crate-gettext-macros-0.3 (crate (name "gettext-macros") (vers "0.3.0") (deps (list (crate-dep (name "gettext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gettext-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a4ankj6cp4489v5jvp1jk5fva6flwgyv77lahf1ss6d1q4shxz9")))

(define-public crate-gettext-macros-0.4 (crate (name "gettext-macros") (vers "0.4.0") (deps (list (crate-dep (name "gettext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gettext-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13mb6bcdbl4yaqngr39mjyvv1aslwvdjzchz6jzdvjm6bhjv9pib")))

(define-public crate-gettext-macros-0.5 (crate (name "gettext-macros") (vers "0.5.0") (deps (list (crate-dep (name "gettext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "runtime-fmt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jahmk66cpiy441s5lld0qhqamk6r4yafrasm7g5krdc1dhwsm5x")))

(define-public crate-gettext-macros-0.5 (crate (name "gettext-macros") (vers "0.5.1") (deps (list (crate-dep (name "gettext") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "runtime-fmt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1k9rh49anjvva0z770i0fq5hrd6rgai2ya689nkbn09ss31hmxv2")))

(define-public crate-gettext-macros-0.5 (crate (name "gettext-macros") (vers "0.5.2") (deps (list (crate-dep (name "gettext") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "runtime-fmt") (req "^0.4") (default-features #t) (kind 0)))) (hash "0lcx9czgd1xz37rb6by4hx64vnbjk3m34apf512gcpxsdfg8xy7v")))

(define-public crate-gettext-macros-0.6 (crate (name "gettext-macros") (vers "0.6.0") (deps (list (crate-dep (name "gettext") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gettext-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.19") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gkci2j9gkfw5niy86990dn9b63j1l5m8bafz76l1kp9sc8ib058")))

(define-public crate-gettext-macros-0.6 (crate (name "gettext-macros") (vers "0.6.1") (deps (list (crate-dep (name "gettext") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gettext-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.19") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cy2zx9i49lmsy74ncjva22yipq2f1pzdfdcdbm92q50rnsv77b4")))

(define-public crate-gettext-ng-0.4 (crate (name "gettext-ng") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.32") (default-features #t) (kind 0)))) (hash "0d6glpyvdbh3vb2ds7jzb4n92195i829b0zwcppmbcnyf7l6pj62")))

(define-public crate-gettext-rs-0.1 (crate (name "gettext-rs") (vers "0.1.0") (hash "0n7ny2csry5dnh2cfkihrqxmmdwzv1n3jrfmc4g0ymydym2qn1zk")))

(define-public crate-gettext-rs-0.2 (crate (name "gettext-rs") (vers "0.2.0") (hash "0yckl8krdfqs5jy95rskqdkflj84rwc62lcyr3n13k9r0wi0y03d")))

(define-public crate-gettext-rs-0.3 (crate (name "gettext-rs") (vers "0.3.0") (hash "0pmyzzb1c6jffhw4q8834dcav460yk4ip699fry96pv11rfpwgc2")))

(define-public crate-gettext-rs-0.4 (crate (name "gettext-rs") (vers "0.4.0") (deps (list (crate-dep (name "gettext-sys") (req "^0.19.8") (default-features #t) (kind 0)) (crate-dep (name "locale_config") (req "^0.2") (default-features #t) (kind 0)))) (hash "0iqs52djp3xra1v8a5xzzmmzamcsrfg1y5msxslni3vmsl92931v") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4 (crate (name "gettext-rs") (vers "0.4.1") (deps (list (crate-dep (name "gettext-sys") (req "^0.19.8") (default-features #t) (kind 0)) (crate-dep (name "locale_config") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j30kjy5lc2aac53a3872zl0ifd5w9mqjyn8zvainrc8w1qj0l5j") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4 (crate (name "gettext-rs") (vers "0.4.2") (deps (list (crate-dep (name "gettext-sys") (req "^0.19.9") (default-features #t) (kind 0)) (crate-dep (name "locale_config") (req "^0.2") (default-features #t) (kind 0)))) (hash "082lxgc8ygc1nd9ibc4qg3srzg1b1rlf0sqz0hdygpkxwifmw0vd") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4 (crate (name "gettext-rs") (vers "0.4.3") (deps (list (crate-dep (name "gettext-sys") (req "^0.19.9") (default-features #t) (kind 0)) (crate-dep (name "locale_config") (req "^0.2") (default-features #t) (kind 0)))) (hash "19gwh76b9906fbwlkq953ba8mjqa43hk3941hwkvnd2rxi7kcs56") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.4 (crate (name "gettext-rs") (vers "0.4.4") (deps (list (crate-dep (name "gettext-sys") (req "^0.19.9") (default-features #t) (kind 0)) (crate-dep (name "locale_config") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z6fcsn1g3w9mlgfj6ln6qvqf8610w3zwvk6g062h657v114lifz") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.5 (crate (name "gettext-rs") (vers "0.5.0") (deps (list (crate-dep (name "gettext-sys") (req "^0.19.9") (default-features #t) (kind 0)) (crate-dep (name "locale_config") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qc9a63i54b9ad3jx951hn7xb6xf76c9f3hmi2cdy2m7rhczm58v") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.6 (crate (name "gettext-rs") (vers "0.6.0") (deps (list (crate-dep (name "gettext-sys") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "locale_config") (req "^0.3") (default-features #t) (kind 0)))) (hash "040nizg9l5ap2vqgq4d2va2hi6cpykj46g8q1z9xv393vjlygx1x") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-0.7 (crate (name "gettext-rs") (vers "0.7.0") (deps (list (crate-dep (name "gettext-sys") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "locale_config") (req "^0.3") (default-features #t) (kind 0)))) (hash "0r7kahqcjrkm83d3gzzkn83fnw2bnqj2ank5z6hsm66izalai7p4") (features (quote (("gettext-system" "gettext-sys/gettext-system"))))))

(define-public crate-gettext-rs-dummy-0.1 (crate (name "gettext-rs-dummy") (vers "0.1.0") (hash "10v86pxvxs2lh5rpljbv1pcv1cfz0ij0r8nbrgyyw5spx65rzn3z")))

(define-public crate-gettext-sys-0.19 (crate (name "gettext-sys") (vers "0.19.8") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1mmkgzmzq9h8dyy98fbhq90nzgqasczm683likxhcdxpp3049ik2") (features (quote (("gettext-system")))) (links "gettext")))

(define-public crate-gettext-sys-0.19 (crate (name "gettext-sys") (vers "0.19.9") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0lzi6ja81vc16mhcdmn3lw35120n9ijhvsy5dh5775mpbfxc8d70") (features (quote (("gettext-system")))) (links "gettext")))

(define-public crate-gettext-sys-0.21 (crate (name "gettext-sys") (vers "0.21.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 1)))) (hash "105d5zh67yc5vyzmqxdw7hx82h606ca6rzhsfjgzjczn2s012pc8") (features (quote (("gettext-system")))) (links "gettext")))

(define-public crate-gettext-sys-0.21 (crate (name "gettext-sys") (vers "0.21.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 1)))) (hash "0cvnam06cldkxrg795lpww8czvrga5bvknrxv95cyla3z1ppadkb") (features (quote (("gettext-system")))) (links "gettext")))

(define-public crate-gettext-sys-0.21 (crate (name "gettext-sys") (vers "0.21.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 1)))) (hash "0acrz4sd2fn2k8lg1pv7sxcn4lx13svs73k6mv4lqlg7p5mf1adg") (features (quote (("gettext-system")))) (links "gettext")))

(define-public crate-gettext-sys-0.21 (crate (name "gettext-sys") (vers "0.21.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 1)))) (hash "17c3qdbirxsf9csqzp4z4jaqck2n72z4nw3nh9vhd8jn1zhf4g66") (features (quote (("gettext-system")))) (links "gettext")))

(define-public crate-gettext-utils-0.1 (crate (name "gettext-utils") (vers "0.1.0") (hash "0w4djmvzq9z8pjcchsmz9zw96a147s0m3c3lmshzcmppg69hgpa6")))

(define-public crate-gettid-0.1 (crate (name "gettid") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0q1d9k08g10g46fa27228h60mndp3vi5074g725d1visicybkl4n")))

(define-public crate-gettid-0.1 (crate (name "gettid") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0yrx2lwkcld4izhp47d0n6rv8z82zhvar4grd9bx5pwa8xgpr7ry")))

(define-public crate-gettid-0.1 (crate (name "gettid") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "03f6nij2fmjpnb4s5wn86gki3xwgdmgr8bkyxjfqmyvx4x00zcil")))

(define-public crate-gettr-0.0.1 (crate (name "gettr") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.21") (features (quote ("csr"))) (optional #t) (default-features #t) (kind 0)))) (hash "0lhx5w8hdlvd00jzj2jg5qm6kwy7jq65f6bmm3aaxd999xz3dayj") (yanked #t) (v 2) (features2 (quote (("yew" "dep:yew"))))))

(define-public crate-gettr-0.0.2 (crate (name "gettr") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.21") (features (quote ("csr"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dyrq7xrl8h1cc5ryskfpzfrhqjf9c3f06w297lhczbnycnvgnka") (v 2) (features2 (quote (("yew" "dep:yew"))))))

