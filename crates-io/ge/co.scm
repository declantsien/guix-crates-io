(define-module (crates-io ge co) #:use-module (crates-io))

(define-public crate-geco-consul-connector-1 (crate (name "geco-consul-connector") (vers "1.0.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.1") (features (quote ("transport" "codegen" "tls"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.1") (default-features #t) (kind 1)))) (hash "1w3qs1s7ij0yciaav7hqn0ws2y9fgfsl80p2bgkpwvmd29pfh5iv")))

(define-public crate-geco-consul-connector-1 (crate (name "geco-consul-connector") (vers "1.0.1") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.1") (features (quote ("transport" "codegen" "tls"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.1") (default-features #t) (kind 1)))) (hash "191qzpymwy40n6bka965xx1r3m9kiiyihmcc7lmph09hk5qd3yrq")))

(define-public crate-geco-consul-connector-1 (crate (name "geco-consul-connector") (vers "1.1.0") (deps (list (crate-dep (name "prost") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.4.0") (features (quote ("transport" "codegen" "tls"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "0qhsf0g22ky8nyms8j142bsjcb8ffr30izr97byqk25087zj44l7")))

(define-public crate-gecode-0.0.0 (crate (name "gecode") (vers "0.0.0") (hash "1jsy5mlm09476wjjxj9n4rpyffrmlqmp6hn8wkyx9lhy1xxn7z2j")))

(define-public crate-gecode-sys-0.0.0 (crate (name "gecode-sys") (vers "0.0.0") (hash "1ma4mksji10l3avjagzjaia3wvx0m6zd7lmz1pgf1snkkbwzxnia")))

