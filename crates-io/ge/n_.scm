(define-module (crates-io ge n_) #:use-module (crates-io))

(define-public crate-gen_attributes_interface_generator-0.1 (crate (name "gen_attributes_interface_generator") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "derive"))) (default-features #t) (kind 0)))) (hash "02s207jd87k5m70cw0h6xah3zrdnscikv6rn35w5c5iva19m78pf")))

(define-public crate-gen_eval_table-0.1 (crate (name "gen_eval_table") (vers "0.1.0") (deps (list (crate-dep (name "bytepack") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1cp1swhq8dkf1ahvxl46rv4h264j45zm9iy8gwa50xx2qxvjyqn7")))

(define-public crate-gen_eval_table-0.1 (crate (name "gen_eval_table") (vers "0.1.1") (deps (list (crate-dep (name "read_write") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1cnz39hj76spqh1r05pwjfnfjc7984lifhqcdapqqaab0jmlqsbq")))

(define-public crate-gen_eval_table-0.1 (crate (name "gen_eval_table") (vers "0.1.2") (deps (list (crate-dep (name "read_write") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "18vqrfy0skanl42xqrcnjjri0azlzc9bbh9qbg0fvwhji2sagnmi")))

(define-public crate-gen_gcode-0.1 (crate (name "gen_gcode") (vers "0.1.0") (hash "06nqwvxy61qznp37ck5kvqb8kjlwbx8x39m1mdbjnairvg6x0dx5")))

(define-public crate-gen_indices-0.0.0 (crate (name "gen_indices") (vers "0.0.0") (deps (list (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1h9dx6b7jiqka6av4r68gfz809w5vh5hrzsmxkc3llcjmgya3y5l")))

(define-public crate-gen_layouts_sys-0.1 (crate (name "gen_layouts_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.46") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 1)))) (hash "1nsp5dc1cvk4hcfrnlxh2jrbsizgsp2jl0qhr51lbs9pkdwv9p9j")))

(define-public crate-gen_layouts_sys-0.2 (crate (name "gen_layouts_sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.46") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 1)))) (hash "0n6wx4l3s2wfmly511y2nb0c67ri5w9sb3xjn8b2pl49pq3k896r")))

(define-public crate-gen_layouts_sys-0.2 (crate (name "gen_layouts_sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 1)))) (hash "0wxvv9j4rh6yrnz9wxwpkh8324kj5b8gg2n3qy73syrz02k1vyjn")))

(define-public crate-gen_layouts_sys-0.3 (crate (name "gen_layouts_sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.46") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^0.6") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (optional #t) (default-features #t) (kind 1)))) (hash "0zbkhmwirayyrx0179kp92qxnicxc8s6wj45sm860q72hyc22bx6") (features (quote (("generate" "bindgen" "quote" "syn" "regex" "proc-macro2"))))))

(define-public crate-gen_lsp_server-0.1 (crate (name "gen_lsp_server") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "languageserver-types") (req "^0.51.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "04fs35gh1zqipxng5zzifqvfcy6228c41q75rhdl1fhkl60s0ifn")))

(define-public crate-gen_lsp_server-0.2 (crate (name "gen_lsp_server") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "lsp-types") (req "^0.57.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "0yfdfh0bai8yzzn097dr798h6arx7vj0zndmjxjpjlyjbqj0qdrd")))

(define-public crate-gen_ops-0.1 (crate (name "gen_ops") (vers "0.1.0") (hash "033szxsgjdy68x8rscy07s7dikb59ac2nlzvz6zad46axwczs1cy")))

(define-public crate-gen_ops-0.1 (crate (name "gen_ops") (vers "0.1.1") (hash "16gwhhdj3i92f16hcc4pw1qw50c88n066vqsz412p4q22p7daf08")))

(define-public crate-gen_ops-0.1 (crate (name "gen_ops") (vers "0.1.2") (hash "12k49frvh0hj02l7cmb3hivn8yglr861rfbcwnf4lx2779x62vng")))

(define-public crate-gen_ops-0.1 (crate (name "gen_ops") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)))) (hash "0x38saxdyrl5jbbvp3v14plv87ixrqm2x8mbw0ckw9ik8k5qsiqi")))

(define-public crate-gen_ops-0.2 (crate (name "gen_ops") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)))) (hash "12q81dc8fwlg6ljkwikpk812caminvvdm1lvak1868d39xzk8h9z")))

(define-public crate-gen_ops-0.3 (crate (name "gen_ops") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)))) (hash "0nl255b6a8crsycxfqvrgqajb2lng2nz82z48zahk0g7isnnrig7")))

(define-public crate-gen_ops-0.4 (crate (name "gen_ops") (vers "0.4.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)))) (hash "02hfbxyz79z284g4l9gdqhw99rn8pgyb0si9babj1102nyfy2k9h")))

(define-public crate-gen_passphrase-0.1 (crate (name "gen_passphrase") (vers "0.1.0") (deps (list (crate-dep (name "nanorand") (req "^0.7") (features (quote ("chacha"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "1gvy2ib8i24743jixb2ivw7fypqnsixj23cpq9mp4w0hlxlzwhby") (features (quote (("eff_short_2") ("eff_short_1") ("eff_large") ("default")))) (yanked #t)))

(define-public crate-gen_passphrase-0.1 (crate (name "gen_passphrase") (vers "0.1.1") (deps (list (crate-dep (name "nanorand") (req "^0.7") (features (quote ("chacha"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "11w2smzs1izdblqzx3hwwnmp0zxs6jc0mz3lqpjjz8f2zcraw2zj") (features (quote (("eff_short_2") ("eff_short_1") ("eff_large") ("default"))))))

(define-public crate-Gen_Prime-0.1 (crate (name "Gen_Prime") (vers "0.1.0") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1gwjvjfrysbp2srx6gi6nxcj6lpqmr11k5vf4k8yp9c9xri4xyl5")))

(define-public crate-Gen_Prime-0.1 (crate (name "Gen_Prime") (vers "0.1.1") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0dwv8s9chgg60s7n3m1rq9qxga5rwxf5wxxjp2fy0vcy5jk0hm15")))

(define-public crate-Gen_Prime-0.1 (crate (name "Gen_Prime") (vers "0.1.2") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1qgycp7y0554j52bbaiqh461539i95cp62a2fyzg9l0iaayr8k05")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.0.0") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "01vl659cz4va5c4ar06bp8z2mvfj71brn5gbvjnbqci1ps40d6l0")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.0") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "06zyxkw7aqx0b92mg7p78aih0zxw6ijnfljddnr536c1407pbkwm")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.1") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "101yymhfya9p8sl4nmk1747h6x6jrdw6rkch3yjwxifxf7nw42hq")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.2") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ayg7n1rlgbydacg2bdpnk9x0lhx11ppi6gz06m1bbkfs813v6da")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.3") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0lsmayadbrbmavp8rgjd8dprzgqw642lkjsxc6bvzwlg664mkshb")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.4") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qklvgjcnjwcfli8q36pwj6ncdwb6mcnyx4qg24giixyrpybih9b")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.5") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "05pikrvz9pd6qs5x6fyw1w35zixlmbm53bb066w586ap9j5qjlb1")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.6") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "10vnvdwqqj9zlxkbvh4xqmlcbw6ymxnjv10prqi36ibzvcv2mvkd")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.7") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0zbdc1fdis3rnzr4afjcfays2ffyjya11fbkwia3pxfq7jid2sky")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.8") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "14c8zva81immbrgb9djrcxml42v0lp796v0qsdh2bldzymjqyzls")))

(define-public crate-Gen_Prime-1 (crate (name "Gen_Prime") (vers "1.1.9") (deps (list (crate-dep (name "bigint") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1gi4f84x6f1rhc2b342zrxzd7bsxv2riwn62602w9p406p4pb13j")))

(define-public crate-gen_random-0.1 (crate (name "gen_random") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02j3mx922zr5g97pigk10wi10s49l1ic9ip06xcwczphdi4x3wnc") (yanked #t)))

(define-public crate-gen_random-0.1 (crate (name "gen_random") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m0yzyhldxfifxx9njwrnya2sss5ps307384bv2rqp27a2kqj4vl") (yanked #t)))

(define-public crate-gen_random-0.1 (crate (name "gen_random") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "050md2r10zdvak7zrq7p2k20qs20wz3gmvp1s78c962yfz4kc8cm")))

(define-public crate-gen_random-0.1 (crate (name "gen_random") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1py3x4d856b8rc4w22c29y31sag8z0rx051y70qpzdj1gzj9hkdf")))

(define-public crate-gen_random-0.1 (crate (name "gen_random") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0hkp6d2wl8wxvd2azi81l25kssib3bkfl5m68l113qx0xd3kqfsq")))

(define-public crate-gen_value-0.1 (crate (name "gen_value") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ngdy9gbsx38qjy3wa6as2ddbh9r89b5n5mhk7m98483sc5p6sc0") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-gen_value-0.2 (crate (name "gen_value") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1fbfvy0m3x4c0wzkbkzylr9flcxd6zxib0cyn1cnsnfinqdj6g36") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-gen_value-0.3 (crate (name "gen_value") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1x98qy1bkr1lk1dzb6y35dv8r538bhyrz3gjx0rq8hbfdmj71v7m") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-gen_value-0.4 (crate (name "gen_value") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0rswj8d4d48b96n3k3czizca9fpiw49fh4bl9mjkj61dhx1xai5j") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-gen_value-0.5 (crate (name "gen_value") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1wlzkpig0h93ncd1190r937nvx03w1z5l19l3fx5zgzdj37c23qw") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-gen_value-0.6 (crate (name "gen_value") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1fixasxsria6lyi9fnndvd1fxmpnrgqrlcb6w7226piifn2yi8r0") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-gen_value-0.7 (crate (name "gen_value") (vers "0.7.0") (hash "0cp5fjhwlw81jqvay3f0lb4f2f2lr9wkd9mp0f1r54lchinkxhvj") (features (quote (("std") ("default" "std") ("alloc")))) (rust-version "1.56.0")))

