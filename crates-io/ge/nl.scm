(define-module (crates-io ge nl) #:use-module (crates-io))

(define-public crate-genlib-0.1 (crate (name "genlib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0zl52i9wlimha7qlba587vs6gk5qkg3bj4wacc5h83l638pm2d0p") (yanked #t)))

(define-public crate-genlink-0.1 (crate (name "genlink") (vers "0.1.0") (hash "11jb9l4nysic6zp43is63wiwmzr7fk1bvwryzirws2qsdw41jqpm")))

