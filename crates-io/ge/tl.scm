(define-module (crates-io ge tl) #:use-module (crates-io))

(define-public crate-getline-0.1 (crate (name "getline") (vers "0.1.0") (hash "0h8x70qz0xxikwynkh07qj14ywjn04lm0vkyhwjri9c0nr3qw45c")))

(define-public crate-getline-0.1 (crate (name "getline") (vers "0.1.1") (hash "05c2h1p035mwwwcv045iya8qywr41k49l7wdvcv3vkh8xrgfx2fm")))

(define-public crate-GetLocalInfo-0.1 (crate (name "GetLocalInfo") (vers "0.1.0") (deps (list (crate-dep (name "pnet") (req "^0.34.0") (default-features #t) (kind 0)))) (hash "0brgqfddk5bs9hy3ja11b835vqll4gfv8cr5klrysppnf4fmglwj")))

