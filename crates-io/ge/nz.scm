(define-module (crates-io ge nz) #:use-module (crates-io))

(define-public crate-genz-0.1 (crate (name "genz") (vers "0.1.0") (hash "15kljqznfmlfyxvvclxy0458ksf963pn3inpg9cv64p3mcklpic6")))

(define-public crate-genz-0.2 (crate (name "genz") (vers "0.2.0") (hash "0y4f81zvvxqp3qh42djswjb0s4kqc5dkkr3m666rx1fa8ri513pq") (yanked #t)))

(define-public crate-genz-0.3 (crate (name "genz") (vers "0.3.0") (hash "0hgibqyn0qcdqwbx6axl463a1v58wkwqga3imyaqmsfv7bikkxla")))

(define-public crate-genz-0.4 (crate (name "genz") (vers "0.4.0") (hash "1xkpl5b1wrblv2f1ig2w4nrhqrvnv9r2c5xyrf1dyhrv24bqgxnx")))

(define-public crate-genztools-0.1 (crate (name "genztools") (vers "0.1.0") (hash "1iw9z2aiip53fay7bm13pjsb9n8snhrsdcqhvg5nvp10326a7ig3")))

(define-public crate-genztools-0.1 (crate (name "genztools") (vers "0.1.1") (hash "0d57m7j7n6llll85qv2zi8vq1gykd7jlhqnq42d525fh8kcmschg")))

(define-public crate-genztools-0.1 (crate (name "genztools") (vers "0.1.2") (hash "1m12wpijvkqbdn1szpsg8f9xd4ib44cf51pn921ksmnskbbfi552")))

(define-public crate-genztools-0.1 (crate (name "genztools") (vers "0.1.3") (hash "10xk5zgs68jykb0wyxpnqz8412br5vi84x2mwwb8ybjrdw6jka9m")))

(define-public crate-genztools-0.1 (crate (name "genztools") (vers "0.1.4") (hash "1alqr2151vd7v4vyjw8qwzj348dlgy498iznvhf6ypgwqbhc201w")))

(define-public crate-genztools-0.1 (crate (name "genztools") (vers "0.1.5") (hash "1wnrdsgr8f9c1db56lyxwcbjxyhg8byixwa6axna6rx4chslk69c")))

(define-public crate-genztools-0.1 (crate (name "genztools") (vers "0.1.6") (hash "122cj8jq04i6jbv9f9nwlyxvssz66c7hk0ckf2l1dqllimy1nana")))

