(define-module (crates-io ge tf) #:use-module (crates-io))

(define-public crate-getfn-0.1 (crate (name "getfn") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dfmb4fx612hgrvb5gdy97k8ncd6n6w07idx9bwi12s3xakin4py")))

(define-public crate-getfn-0.1 (crate (name "getfn") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (optional #t) (default-features #t) (kind 0)))) (hash "0c07f7fnbd4fam6sdls6r93kjnddi05c8v665arpqcsw3k3yzgvz")))

(define-public crate-getfn-0.1 (crate (name "getfn") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "11wk7pbwyknw05h0bw6hjx9c9w0xwdkaw1v9447449rnz93jiwrh")))

(define-public crate-getfn-0.1 (crate (name "getfn") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "1r4q1qdy5rfbcgz4l1brfgc2zqm0mhx709hk4ddv5n0g7ynsfwps")))

(define-public crate-getfn-0.1 (crate (name "getfn") (vers "0.1.4") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "1xyxgdsi13wdvr5z0xv2xncb6p9690nnsrvyim7657lp2zdz4ssb")))

(define-public crate-getfn-0.2 (crate (name "getfn") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "1xkpixkw3qw7v0hby96mp8i3cd25jymzmjfymgfprl5f2qf39rh7")))

(define-public crate-getfrompass-0.1 (crate (name "getfrompass") (vers "0.1.0") (hash "05xvlmw6j3h69ydnj869b7f0a4zdphdz56k94j46fljwfw5rjsfb")))

(define-public crate-getfrompass-0.1 (crate (name "getfrompass") (vers "0.1.1") (hash "1s184gbjb36l0cllazdc7cn6ci5hpdpabs0bjrph3hy4hh2sc0q1")))

(define-public crate-getfrompass-0.1 (crate (name "getfrompass") (vers "0.1.2") (hash "1zqhqjm5828dhmb36ppvlhiayfbyhzsgrsybb3zc3dvsnlng3gq9")))

(define-public crate-getfrompass-0.1 (crate (name "getfrompass") (vers "0.1.3") (hash "1nn89lkhc36k74y1rlmr5n6j09ij3mxwj3j9pq873va24np6nb6r")))

(define-public crate-getfrompass-0.1 (crate (name "getfrompass") (vers "0.1.4") (hash "0m6495mys8dddchc37b0a6v6qd0ds2naww1ksjqawf6h3gfm1v14")))

