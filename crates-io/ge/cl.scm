(define-module (crates-io ge cl) #:use-module (crates-io))

(define-public crate-gecl-0.0.1 (crate (name "gecl") (vers "0.0.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wmp1w4jm2869k31cdqkq87pw03fw6slji36mr5k8kllna7cllqc")))

(define-public crate-gecl-0.0.2 (crate (name "gecl") (vers "0.0.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mkwnm5l1y5xlr6yj2dy8nbnypwx1c45pvxrk6n9xa9rf0iacsz2")))

(define-public crate-gecl-0.1 (crate (name "gecl") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0s8lvmxq1x63zzpvdmzigyrm7zr89jcrh26pmm2jh3n833s24ah9")))

(define-public crate-gecl-0.1 (crate (name "gecl") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1pyak2qk4p6726367jixm402wjdzdhjk9ggn8s617309n1kiylkb")))

(define-public crate-gecl-0.1 (crate (name "gecl") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cxm38gl7rs1r9j8bz6rmh9bg218770vyi3h6i936xr13yad2zi6")))

(define-public crate-gecl-0.2 (crate (name "gecl") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rav5c2iw5j43granhwnnbjglmldsy4hj8d91kzc5acwk8gp7gd3")))

(define-public crate-gecl-0.2 (crate (name "gecl") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "14xbfdh1daz3002mgr3z43yr4qgwkkw03kcim5y3bnlj983ybxnm")))

(define-public crate-gecliht-0.1 (crate (name "gecliht") (vers "0.1.0") (hash "09s47200f0sd0asajm65da8m95dlp4vxymw3443vwwcdfkipgl1s")))

(define-public crate-gecliht-0.2 (crate (name "gecliht") (vers "0.2.0") (hash "0ypippnw44dpq9qiwc6s6bp8i57zm3pzimjlv7j4359yvjlkw8ai")))

