(define-module (crates-io ge m-) #:use-module (crates-io))

(define-public crate-gem-macros-0.1 (crate (name "gem-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1k5ic19hfnph0l10cwwzp5w00n8vkwm2i0r6a2151s9b0893vqxm")))

