(define-module (crates-io ge nv) #:use-module (crates-io))

(define-public crate-genv-0.1 (crate (name "genv") (vers "0.1.1") (deps (list (crate-dep (name "const-str") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0042n7mqlmipcvdl5j8psjzrq85y3jqj1p16riws3picjzrp8br8")))

(define-public crate-genv-0.1 (crate (name "genv") (vers "0.1.2") (deps (list (crate-dep (name "const-str") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0.3") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "16rgpdxybh6vxd7lwv5gdzal08xv5rm1ykz5gsv1nvckph3pkcv7")))

(define-public crate-genv-0.1 (crate (name "genv") (vers "0.1.3") (deps (list (crate-dep (name "const-str") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0.3") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "04jgpblgq90c21sdb6dm3ik91vhkcxg3ix4fhcgc49z3iwlxf00k")))

(define-public crate-genv-0.1 (crate (name "genv") (vers "0.1.5") (deps (list (crate-dep (name "const-str") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0.3") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1iaan1c5a1s25hlb53hg1w0lmzf6fza9k3z9qzbi4s6w67zmlkj4")))

(define-public crate-genv-0.1 (crate (name "genv") (vers "0.1.6") (deps (list (crate-dep (name "const-str") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0.3") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0pccca49s3yav32mqr7cni9gymmfdqmi6w0dmpdn9j64cj222yb2")))

(define-public crate-genv-0.1 (crate (name "genv") (vers "0.1.7") (deps (list (crate-dep (name "const-str") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0.3") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0s24h9msb682g61fshzalmp4l20cq2q910njypyh5q9g0gsazf64")))

(define-public crate-genv-0.1 (crate (name "genv") (vers "0.1.8") (deps (list (crate-dep (name "const-str") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0.3") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0ckk5ajqmxm36hrhphysgp87x3s23xvyqcmzvqzjcsqkhqbd9sjm")))

