(define-module (crates-io ge ta) #:use-module (crates-io))

(define-public crate-getaddrs-0.1 (crate (name "getaddrs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bf9l3x9mr6r74lbsayw4nwmrmv6v6gvbqmasffn78a5zi21vfpg")))

(define-public crate-getargs-0.1 (crate (name "getargs") (vers "0.1.0") (hash "1366vljimn52g8zrk3xprj0dybiz92jdmcs0y5idsr55h71qd7wv") (yanked #t)))

(define-public crate-getargs-0.2 (crate (name "getargs") (vers "0.2.0") (hash "0mh2zz22y9ivxcjnvzrdj1v0il3mzqyz1gl492x2zjagb2ga647z")))

(define-public crate-getargs-0.3 (crate (name "getargs") (vers "0.3.0") (hash "1y32a9l77h0ma135phsl6bnplaimm5ql0iiilphs10vpzn1aphsc")))

(define-public crate-getargs-0.4 (crate (name "getargs") (vers "0.4.0") (hash "0v33pmc8pwkvb67vhbln057f20ziyijrw20mzr5x0cl2nwijrgv2")))

(define-public crate-getargs-0.4 (crate (name "getargs") (vers "0.4.1") (hash "1wacn0n596z2l78n7bsj2y600h8v5wb41fgaya3zvnjnrjnwmylw")))

(define-public crate-getargs-0.5 (crate (name "getargs") (vers "0.5.0") (deps (list (crate-dep (name "argv") (req "~0.1.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 2)))) (hash "0vp6f2h82i1a7268fkq75gcv1aari36bk1i9y57wbph15cc6nk08") (features (quote (("std") ("default" "std"))))))

(define-public crate-getargs-os-0.1 (crate (name "getargs-os") (vers "0.1.0") (deps (list (crate-dep (name "argv") (req "~0.1.6") (default-features #t) (kind 2)) (crate-dep (name "getargs") (req "~0.5.0") (default-features #t) (kind 0)))) (hash "10lhpvjb103d6qigf3d6zdjbrrdv4yk584rk68rbk219gd0hkxay") (rust-version "1.56")))

(define-public crate-getargs-os-0.1 (crate (name "getargs-os") (vers "0.1.1") (deps (list (crate-dep (name "argv") (req "~0.1.6") (default-features #t) (kind 2)) (crate-dep (name "getargs") (req "~0.5.0") (default-features #t) (kind 0)))) (hash "0qnxb52migxdd2qm7c4knlzkrpzlhcm3x4rfkwmpikvk2a9mfl2g") (features (quote (("os_str_bytes")))) (rust-version "1.56")))

(define-public crate-getargv-0.1 (crate (name "getargv") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~4.0.27") (features (quote ("cargo"))) (default-features #t) (kind 2)) (crate-dep (name "getargv-sys") (req "~0.4.0") (default-features #t) (target "cfg(target_vendor = \"apple\")") (kind 0)) (crate-dep (name "libc") (req "~0.2.137") (default-features #t) (kind 0)))) (hash "0cxi14mcikpj931rjrj899gw3inqp3iq0ralifbv15w9mqcfzqld")))

(define-public crate-getargv-0.1 (crate (name "getargv") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~4.0.27") (features (quote ("cargo"))) (default-features #t) (kind 2)) (crate-dep (name "getargv-sys") (req "~0.4.0") (default-features #t) (target "cfg(target_vendor = \"apple\")") (kind 0)) (crate-dep (name "libc") (req "~0.2.137") (default-features #t) (kind 0)))) (hash "1prv1dad5gckl702q7i731wd0cgdisc4gf9s10yl1c2bclvdlshl")))

(define-public crate-getargv-0.1 (crate (name "getargv") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "~4.0.27") (features (quote ("cargo"))) (default-features #t) (kind 2)) (crate-dep (name "getargv-sys") (req "~0.4.1") (default-features #t) (target "cfg(target_vendor = \"apple\")") (kind 0)) (crate-dep (name "libc") (req "~0.2.137") (default-features #t) (kind 0)))) (hash "1yfam3wz0si7gd9a2sw3xnb4lni8k11nhpv0lj6q5367x98gh641")))

(define-public crate-getargv-0.2 (crate (name "getargv") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~4.0.27") (features (quote ("cargo"))) (default-features #t) (kind 2)) (crate-dep (name "getargv-sys") (req "~0.5.1") (default-features #t) (target "cfg(target_vendor = \"apple\")") (kind 0)) (crate-dep (name "libc") (req "~0.2.137") (default-features #t) (kind 0)))) (hash "1yz2rjh3cwaknbxjlml0dw7kmfaj91bx4gd97hf362y0d5kca49n")))

(define-public crate-getargv-0.2 (crate (name "getargv") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~4.5.0") (features (quote ("cargo"))) (default-features #t) (kind 2)) (crate-dep (name "getargv-sys") (req "~0.5.2") (default-features #t) (target "cfg(target_vendor = \"apple\")") (kind 0)) (crate-dep (name "libc") (req "~0.2.153") (default-features #t) (kind 0)))) (hash "1wv9js4kzjarz4g3b9z5r20j4ph1sfvcldkpmjyxrr00z34k102c")))

(define-public crate-getargv-sys-0.1 (crate (name "getargv-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (default-features #t) (kind 1)))) (hash "14gyhhnqqkf0sjl21p1x83rcishah8jy9x8gafgml2zg4nzmxb8z") (links "getargv")))

(define-public crate-getargv-sys-0.2 (crate (name "getargv-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (default-features #t) (kind 1)))) (hash "0vm1xcjfa84q3mcpvhwk1n5dzwxxl9icb6pln0c7ngz0ikhp0av4") (links "getargv")))

(define-public crate-getargv-sys-0.3 (crate (name "getargv-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (default-features #t) (kind 1)))) (hash "11368sdsysppa2flvfffsjcljqym390441lk6s5w49wf4a076z7z") (links "getargv")))

(define-public crate-getargv-sys-0.4 (crate (name "getargv-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "~0.63.0") (default-features #t) (kind 1)))) (hash "1w90l1nkj16rp1ibc7fag0vphjqgwrddr49pjrmivhilvf9943pl") (links "getargv")))

(define-public crate-getargv-sys-0.4 (crate (name "getargv-sys") (vers "0.4.1") (deps (list (crate-dep (name "bindgen") (req "~0.63.0") (default-features #t) (kind 1)))) (hash "0q5m5vjldri7zgmiblyc7i62vrjjmvnmg07jlf1kxjgd514mvg9i") (links "getargv")))

(define-public crate-getargv-sys-0.5 (crate (name "getargv-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "~0.63.0") (default-features #t) (kind 1)) (crate-dep (name "goblin") (req "~0.6.1") (features (quote ("mach64" "std"))) (default-features #t) (kind 1)))) (hash "1z2w6br8xfb9xi5zcswlllc0qipvj37y652pwapd110lnrz21as2") (links "getargv")))

(define-public crate-getargv-sys-0.5 (crate (name "getargv-sys") (vers "0.5.1") (deps (list (crate-dep (name "bindgen") (req "~0.63.0") (default-features #t) (kind 1)) (crate-dep (name "goblin") (req "~0.6.1") (features (quote ("mach64" "std"))) (default-features #t) (kind 1)))) (hash "19fi87xzxhlg7m9i9fdaxd8y1chvbhwn4gbg9k5gnskn8v5jp3w5") (links "getargv")))

(define-public crate-getargv-sys-0.5 (crate (name "getargv-sys") (vers "0.5.2") (deps (list (crate-dep (name "bindgen") (req "~0.69.1") (default-features #t) (kind 1)) (crate-dep (name "goblin") (req "~0.7.1") (features (quote ("mach64" "std"))) (default-features #t) (kind 1)))) (hash "09ib650b0vj1ssmlkg7l0dfxzp6wc0alww5k3psc5q61miz6n9ql") (links "getargv")))

(define-public crate-getargv-sys-0.5 (crate (name "getargv-sys") (vers "0.5.3") (deps (list (crate-dep (name "bindgen") (req "~0.69.4") (default-features #t) (kind 1)) (crate-dep (name "goblin") (req "~0.8.0") (features (quote ("mach64" "std"))) (default-features #t) (kind 1)))) (hash "0y5byswb222l8kw4cx7ina8b22gsv305rvgn6x0kcpadf7bcad2p") (links "getargv")))

