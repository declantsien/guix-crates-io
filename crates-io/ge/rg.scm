(define-module (crates-io ge rg) #:use-module (crates-io))

(define-public crate-gerg_geometric_shapes-0.1 (crate (name "gerg_geometric_shapes") (vers "0.1.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1i0kx3mn9x504z7m2v2n28qxchljpvhbswklp68lvl6hcs284n9v")))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)))) (hash "04hyx1yg5jnzll8vm2cwf68gvh7naxd4db4dg10qcrfl6kg73b9a") (yanked #t)))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.1") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)))) (hash "0grpvyv8fm9d5flh34cgpdd8pzc0say8v2ijclha9rx7n71zpf9x") (yanked #t)))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.2") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)))) (hash "0w6vqm3l1h9fmzhr553gf4vc0q03yhbbnkm4ivsjrb59yz2lp1h0") (yanked #t)))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.3") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)))) (hash "0blfvzwx5hd4l3lry7857kyh4m1m3flwdq97d305vyzj8cc2dq8w") (yanked #t)))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.4") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)))) (hash "1rxb93slk5sz65ziwxc45pab8fqws2ll7067rsp92c7f9drf8hmr") (yanked #t)))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.5") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0khgdh79mw1w2ja7hbqny34w987wnjvhk1qq5963ar4992ibfb7n")))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.6") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "112r6nx027sx0qg24qyd136z0bi9840xrd9r43i4nzplmahsjmww")))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.7") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy_mod_debug_console") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0ajm927i0f6l2v7j4xl5ws7rzf7j0n0x8ynawjhj8g832nsmbxzj")))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.8") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy_mod_debug_console") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0k8drbwm7r4d25rjnk3i9in3dwysirm3djxlsg81sfvm8b622jnn")))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.9") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy_mod_debug_console") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "166dfh30q3n7l0n0lgdjv35612sps0kb1yzq9rbri3fx0kzsq9ax")))

(define-public crate-gerg_ui-0.1 (crate (name "gerg_ui") (vers "0.1.10") (deps (list (crate-dep (name "bevy") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy_mod_debug_console") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "009pyql4maiwa6lgkbvv7df0cwdl6sgzvj8a48g3wfhwflsiqhm4")))

