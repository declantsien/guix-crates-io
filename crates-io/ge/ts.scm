(define-module (crates-io ge ts) #:use-module (crates-io))

(define-public crate-gets-0.0.1 (crate (name "gets") (vers "0.0.1") (hash "0z7mcm7cny9kr3jn6h8lg0dfvcygfyjn5whjvq85j8hh0lr2bky1")))

(define-public crate-getsb-0.1 (crate (name "getsb") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "knock") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "0z8gk4li0h8c4c2gaxzy3i8nhfcxz2x4szdkgwik44iqjw23xxln")))

(define-public crate-getsb-0.1 (crate (name "getsb") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "knock") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "0q8004969lyl554qvk6f8gkfpx5cjq4qlh1wvac0csldbqs50yqq")))

(define-public crate-getsb-0.1 (crate (name "getsb") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.134") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "knock") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "0kwplfni9w8hgcsznz4bnfaadvyza024nw74x2m1f7wsqx3yvg1n") (features (quote (("default"))))))

(define-public crate-getset-0.0.1 (crate (name "getset") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "17nc8n0kww22cwqb602yx4fkrwklks1g5163rlyb3gk121qlqnzg")))

(define-public crate-getset-0.0.2 (crate (name "getset") (vers "0.0.2") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "1imiyzq6f6z7qdqcki1qxdknr4z25yw68h8y3hsncza6g0hc7iax")))

(define-public crate-getset-0.0.3 (crate (name "getset") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "03i6203jw6yw0vcnbxnnh39b7aapnkaxrg343spl7df71aj37m6j")))

(define-public crate-getset-0.0.4 (crate (name "getset") (vers "0.0.4") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0wiy9d8py3wfksrs1ip5qa811a1nm170j112pmbcbg0cykfglh63")))

(define-public crate-getset-0.0.5 (crate (name "getset") (vers "0.0.5") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "16c2kvcr28j4hz5786kdc48flm73pn7jvrv5h0p3m0q9pv526q1w")))

(define-public crate-getset-0.0.6 (crate (name "getset") (vers "0.0.6") (deps (list (crate-dep (name "proc-macro2") (req "^0.3") (kind 0)) (crate-dep (name "quote") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.10") (default-features #t) (kind 0)))) (hash "10h740vb4sqdz15qr4fkdirzbxi8pcyjnsjdj0jvnf2p4dmg7isl")))

(define-public crate-getset-0.0.7 (crate (name "getset") (vers "0.0.7") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.21") (default-features #t) (kind 0)))) (hash "1jz2ark3zi0rpp9fcx2gyzr0jax2kdf1yjv9fja1y70cml7xxyqr")))

(define-public crate-getset-0.0.8 (crate (name "getset") (vers "0.0.8") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0rmq79q5wpb4wkc928vchd98b3l9v8nv5zmk3cb10kndmq9mnyhi")))

(define-public crate-getset-0.0.9 (crate (name "getset") (vers "0.0.9") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0aaldwfs2690rjqg2ygan27l2qa614w2p6zj7k99n36pv2vzbcsv")))

(define-public crate-getset-0.1 (crate (name "getset") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "18047rasp9491asd7sa039yrkwb8mhmg34n3chwnr15fb6f16apn")))

(define-public crate-getset-0.1 (crate (name "getset") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "016590lxhlqga016z1qnavl0zavk59b97aix2zcd4wad3b02icr4")))

(define-public crate-getset-0.1 (crate (name "getset") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1f8yc83hm5b7vzscxq20ivdv7wlfvabn79j653zh9k3m1qjjfmz4")))

(define-public crate-getset-macro-1 (crate (name "getset-macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.35") (default-features #t) (kind 0)))) (hash "0ds8ac6pnipy6rsagngi1fnh4z1qax0y8b4akyy9b4q09mqswskg")))

(define-public crate-getset-macro-1 (crate (name "getset-macro") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.35") (default-features #t) (kind 0)))) (hash "1iv660c4xfbv8fif9xpiw1pxxdi9lc7zi7nsbp26pvksamc9f285")))

(define-public crate-getset-scoped-0.1 (crate (name "getset-scoped") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0pf6cjva62qg5avpn3z68549rv7ngvcb050d1akmjafwgl5ayjz8")))

(define-public crate-getstartproj-0.1 (crate (name "getstartproj") (vers "0.1.0") (hash "1vpbljlhvbqlbvin1h52ymx9h18jypf4dlr016iwgs89pm154nir") (yanked #t)))

(define-public crate-getstartproj-0.1 (crate (name "getstartproj") (vers "0.1.1") (hash "0hhmwc4smv9f9727i5bcbmnn2hkx1a9i93chbs5fi5dal5452ml6")))

(define-public crate-getsys-1 (crate (name "getsys") (vers "1.0.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "16665sq4yg8w1bf35s37dq6413j6jq5lhlinkvbsg3s4bspz48a4")))

(define-public crate-getsys-1 (crate (name "getsys") (vers "1.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1zbr3kp2ma0yqgzsxxrgghshqpyxflc759vdjd9423sbfnwjziqz")))

(define-public crate-getsys-1 (crate (name "getsys") (vers "1.1.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "02vkb95c5f81zmdj980fs72q17kyq6vbd50cq0r533482ccqgfcp")))

(define-public crate-getsys-1 (crate (name "getsys") (vers "1.1.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0d2r2ra04k8f9qyxbyqm29wkx4rjza5n7j7jvy6n80g0z53jq00q")))

(define-public crate-getsys-1 (crate (name "getsys") (vers "1.1.3") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "11mfd6ag1v1657df4aj9lj74sdhjhq87049rkph65lrshfkgwfcj")))

(define-public crate-getsys-1 (crate (name "getsys") (vers "1.2.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19ibvyx29hfbij548db0w6ddfqcpl3alrd6ac49y2c2vs5cmz4n0")))

