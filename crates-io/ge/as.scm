(define-module (crates-io ge as) #:use-module (crates-io))

(define-public crate-geass-0.1 (crate (name "geass") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.3") (default-features #t) (kind 2)))) (hash "1yy21jy0m31zx020rfris0vaga128963n0bxlbdap0y2cr2j76d5")))

