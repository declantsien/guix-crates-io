(define-module (crates-io ge ny) #:use-module (crates-io))

(define-public crate-geny-0.1 (crate (name "geny") (vers "0.1.0") (deps (list (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1q6igfvbh462i4q36dgdfhq74868wbrw79mjjm18iw897lahjh6h")))

(define-public crate-geny-0.2 (crate (name "geny") (vers "0.2.0") (deps (list (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0566rbmp95x5jwr7x8j89r19ibpfv4a8lmirlxk8k2ig3l2aqjd4")))

