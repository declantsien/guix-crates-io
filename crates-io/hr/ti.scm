(define-module (crates-io hr ti) #:use-module (crates-io))

(define-public crate-hrtime-0.1 (crate (name "hrtime") (vers "0.1.0") (hash "1pw49d3i6fcicbgzfg7q9amb3fvrrs78r0g3iyczfpg7nl4gijld")))

(define-public crate-hrtime-0.2 (crate (name "hrtime") (vers "0.2.0") (hash "0v434kzy78d65fjm8ca29j7fzcfic66lmc2y4ribi9kmww78g01a")))

