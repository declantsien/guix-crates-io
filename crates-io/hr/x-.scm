(define-module (crates-io hr x-) #:use-module (crates-io))

(define-public crate-hrx-get-0.1 (crate (name "hrx-get") (vers "0.1.0") (hash "0qi09fg6f63cg7162vy5qjq5q5qakbd2n576h57an1ak4wxvcsmb")))

(define-public crate-hrx-get-0.1 (crate (name "hrx-get") (vers "0.1.1") (hash "0zs6fhhp394rjphdrbifvrcp1lvca4kycyhwa9f75m9sjq4h92vl")))

(define-public crate-hrx-get-0.1 (crate (name "hrx-get") (vers "0.1.2") (hash "1kr65hmfbkk00bnm5x43r0fq2f4rr12lj51la3za03wkrd6jc9w1")))

(define-public crate-hrx-get-0.1 (crate (name "hrx-get") (vers "0.1.4") (hash "1xfh9da2qc53mipq3fs61jna5pabk12m1h3nhnwj2p5485v5cqcc")))

(define-public crate-hrx-get-0.2 (crate (name "hrx-get") (vers "0.2.0") (hash "0dpc88bfvdhg36w6654bkvfqkczkqx54rxdr6943j82423f9pjbb")))

(define-public crate-hrx-parser-0.1 (crate (name "hrx-parser") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.19.1") (features (quote ("glob"))) (default-features #t) (kind 2)))) (hash "0a47q5l0qvlszhwpxdn3hvlqckyzzndnafsb7f7q0ap83aynyyz1")))

(define-public crate-hrx-parser-0.1 (crate (name "hrx-parser") (vers "0.1.1") (deps (list (crate-dep (name "insta") (req "^1.19.1") (features (quote ("glob"))) (default-features #t) (kind 2)))) (hash "0fkzv5grnyb4mfb5xa9876i215990brsacmhmdjw15ffdwfisx7b")))

