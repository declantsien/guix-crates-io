(define-module (crates-io hr st) #:use-module (crates-io))

(define-public crate-hrstopwatch-0.0.1 (crate (name "hrstopwatch") (vers "0.0.1") (deps (list (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Performance"))) (default-features #t) (kind 0)))) (hash "1zjhwk55ibpqwqvp01lmf84rhb5hpgb197py082c9ayc3vk82qy0")))

(define-public crate-hrstopwatch-0.0.2 (crate (name "hrstopwatch") (vers "0.0.2") (deps (list (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Performance"))) (default-features #t) (kind 0)))) (hash "15s17fh0aybr25d64j6sdyr94gc6if3wswvlqqj43frk16icdxvj")))

(define-public crate-hrstopwatch-0.0.3 (crate (name "hrstopwatch") (vers "0.0.3") (deps (list (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Performance"))) (default-features #t) (kind 0)))) (hash "1xqmhrzi1dvwpaxksalcg9h675n4nyf1cbw63qh3n8yj2khxd0mv")))

(define-public crate-hrstopwatch-0.1 (crate (name "hrstopwatch") (vers "0.1.0") (deps (list (crate-dep (name "snafu") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_Performance"))) (default-features #t) (kind 0)))) (hash "13awxi801c289z4b02rhlmgq04dws25db6clmzi5y52mza03spzq")))

