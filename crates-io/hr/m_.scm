(define-module (crates-io hr m_) #:use-module (crates-io))

(define-public crate-hrm_interpreter-0.2 (crate (name "hrm_interpreter") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.27") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1iy64ajwvg5kysblm6mhbwz93q5gn3snh83khdmbj4j57qlv0qxz")))

