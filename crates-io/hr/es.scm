(define-module (crates-io hr es) #:use-module (crates-io))

(define-public crate-hresult-0.0.1 (crate (name "hresult") (vers "0.0.1") (hash "0ymhnmxxyqzaksp9kdifdfyrv9y1gm53ld22372qcpjb9bkskikp")))

(define-public crate-hresult-rs-0.0.1 (crate (name "hresult-rs") (vers "0.0.1") (hash "04vv2vgw9pr76zn5rh039ia87wv3fakjgz747rk17899r0pjwznn") (yanked #t)))

