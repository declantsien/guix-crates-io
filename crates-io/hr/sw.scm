(define-module (crates-io hr sw) #:use-module (crates-io))

(define-public crate-hrsw-0.1 (crate (name "hrsw") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1fgdqx6k682vbwabvdlqbxwl2s05p0ldn7qbbm9zi8mvsm0191nn")))

(define-public crate-hrsw-0.1 (crate (name "hrsw") (vers "0.1.1") (hash "1gfzcczp8087v17ar3af5v7h7xqmmj2p25i9s1pglr0h965f2g8n") (yanked #t)))

(define-public crate-hrsw-0.1 (crate (name "hrsw") (vers "0.1.2") (hash "1zpp9zb97jirlm5s6b82x3iw9iz6imm6y68lvxjh41vji8ypbr0m")))

