(define-module (crates-io hr to) #:use-module (crates-io))

(define-public crate-hrtor-0.1 (crate (name "hrtor") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.19.7") (default-features #t) (kind 0)))) (hash "0skhm3mq18ycf69lc79fdhxzvfj46cisqrdqb02pvhr69c08s2qm")))

