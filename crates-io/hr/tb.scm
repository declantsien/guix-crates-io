(define-module (crates-io hr tb) #:use-module (crates-io))

(define-public crate-hrtb-lending-iterator-0.1 (crate (name "hrtb-lending-iterator") (vers "0.1.0") (hash "1qvkf5pnw3jrs59nmja3nvk2xl4fmv5d0hpq4nbrmcfgkw7jl44j")))

(define-public crate-hrtb-lending-iterator-0.2 (crate (name "hrtb-lending-iterator") (vers "0.2.0") (hash "0msrq7n8sa2jr8gap5vyp5042mvkwxhcw0yl831dmyp8bj3cq23c")))

(define-public crate-hrtb-lending-iterator-0.2 (crate (name "hrtb-lending-iterator") (vers "0.2.1") (hash "012bk5i647ss3dpzipp93kp3vjvgl1wmgln5f1lp8gr1aivsiqh0")))

(define-public crate-hrtb-lending-iterator-0.2 (crate (name "hrtb-lending-iterator") (vers "0.2.2") (hash "0icqb6dy1np3psqdn0m114xgfifickqik1ysffp4q5qrx9jj7hcl")))

(define-public crate-hrtb-lending-iterator-0.2 (crate (name "hrtb-lending-iterator") (vers "0.2.3") (hash "0m557zmjikwmll7s28fzsnvlh83s7s3lm1v8vn565ashvc92zzwk")))

(define-public crate-hrtb-lending-iterator-0.3 (crate (name "hrtb-lending-iterator") (vers "0.3.0") (hash "1c5981f5an8jar20g528g4yxpzzkdx9mxmwksffzyl65i4ai3z6f")))

(define-public crate-hrtb-lending-iterator-0.3 (crate (name "hrtb-lending-iterator") (vers "0.3.1") (hash "1z2schs4slxyh5psbmgcaqbcgyz5aj2rsqzqi5jilhp3vbjwabhg")))

