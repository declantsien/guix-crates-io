(define-module (crates-io hr s3) #:use-module (crates-io))

(define-public crate-hrs3300-0.1 (crate (name "hrs3300") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0qbzc68vq5wl22rvbfxx57vvr4inwbc57j8l8ab10309j60kvw51")))

