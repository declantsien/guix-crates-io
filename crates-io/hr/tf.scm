(define-module (crates-io hr tf) #:use-module (crates-io))

(define-public crate-hrtf-0.1 (crate (name "hrtf") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rg3d-core") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1lf9xpv02wm8fpcfly4qam1sgxh41vjykizkdncjrbfjrm5i8h66") (features (quote (("enable_profiler" "rg3d-core/enable_profiler"))))))

(define-public crate-hrtf-0.2 (crate (name "hrtf") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rg3d-core") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "16ll35ww5wba3x5hs4d652023bif0ip3n0s6hfbv7myclmbn2za4") (features (quote (("enable_profiler" "rg3d-core/enable_profiler"))))))

(define-public crate-hrtf-0.3 (crate (name "hrtf") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1xzyfrfjy7ayljgmznqllv7599vwv0a2amh2hvxfj372igxc2fvm")))

(define-public crate-hrtf-0.4 (crate (name "hrtf") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0gldmjcl95crrwv1fwqq6k09indfdm5rv44pip6mmg5d9m9cx8k3")))

(define-public crate-hrtf-0.5 (crate (name "hrtf") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "0wn0h0q8xmnpnhskg1pzh0fgiyfvg3pp251q716y4f6csj3g7m18")))

(define-public crate-hrtf-0.6 (crate (name "hrtf") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "1k0zbsk9yrq1f28dkgb9zh0nm0qpm4xn0vijvhp9vc7nm66rnjdb")))

(define-public crate-hrtf-0.7 (crate (name "hrtf") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "1nlmpqh93bvgpxgdalg289kdbbscfsrpb2gi77j6xm7x2wc0i2yr")))

(define-public crate-hrtf-0.8 (crate (name "hrtf") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "1gkmgs2vc2f4z0djdsc5zjxc8izjjqb62m1ajkp237bwbm58q23k")))

(define-public crate-hrtf-0.8 (crate (name "hrtf") (vers "0.8.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rubato") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "1rj808lzl8jkif4yf8xdzmlwiz8ld00kgwsym8rzlmgxhixf8k8g")))

