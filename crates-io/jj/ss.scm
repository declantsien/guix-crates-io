(define-module (crates-io jj ss) #:use-module (crates-io))

(define-public crate-jjss_tocargo-0.1 (crate (name "jjss_tocargo") (vers "0.1.0") (hash "12lfrc89x9ff9ah21p3r8vdpvhaqhl8kx6n0nqsw87izf9vkm2k2")))

(define-public crate-jjss_tocargo-0.1 (crate (name "jjss_tocargo") (vers "0.1.1") (hash "1dj7fmy0fxpp9bzwdxswb78w80p1wsmsvhcn5vxlzx7k0whi5plx")))

(define-public crate-jjss_tocargo-0.1 (crate (name "jjss_tocargo") (vers "0.1.2") (hash "16pij8zc8c140k7kfqgwzi1r4kpv99l6n8njqlvqzcpr327qmjkq")))

(define-public crate-jjss_tocargo-0.1 (crate (name "jjss_tocargo") (vers "0.1.3") (hash "1a7fl1ygzyw76ss43i8g8431mr4dafb109k93z2kagldf4hjy7y9")))

(define-public crate-jjss_tocargo-0.1 (crate (name "jjss_tocargo") (vers "0.1.4") (hash "0xjik9h3mh2hrdsimkvsjz9nkvsz5pm3l3zbfi5jpl1zapzns8jd")))

(define-public crate-jjss_tocargo-0.1 (crate (name "jjss_tocargo") (vers "0.1.5") (hash "1b6yhxfcn0nfy6y1i56x5n5dhfjag504v0cwbdyqnljwwrd0y0xf")))

(define-public crate-jjss_tocargo-0.1 (crate (name "jjss_tocargo") (vers "0.1.6") (hash "0hadvzwqyqjb8l6z9l23g5krj0v7drikzwvbls76lmhann4qdmd3")))

