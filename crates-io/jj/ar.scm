(define-module (crates-io jj ar) #:use-module (crates-io))

(define-public crate-jjar-0.1 (crate (name "jjar") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vx638q2kbqrjrfnj6v5804wnhx6rwm3z5fj1j8ps2wa9606p4s1")))

