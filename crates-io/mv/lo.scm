(define-module (crates-io mv lo) #:use-module (crates-io))

(define-public crate-mvlogger-0.1 (crate (name "mvlogger") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1dlxjay94l0wphgscrldd6qc2k09xypjfqrkgdjwi1xazpz34iws") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.1 (crate (name "mvlogger") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0whr1aa3p1xdv45glk0dd7f3372qr97a0zlvkv4mvv6z3x7b5n12") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.1 (crate (name "mvlogger") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1f87p1m2hflvxpgxcpvldd7g120r307s96ghv34qbhb30vz8yb5s") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.2 (crate (name "mvlogger") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0jl66xkbq4wm0yvabjzrxrs45dgdvpl883sc0738bsb0gkfqdvi1") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.2 (crate (name "mvlogger") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0bgjbwfmiim0wbw94ysr4prvd4bybg6mcwblgy0czas8pg305f5d") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.2 (crate (name "mvlogger") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "01xmj6c5mxkb68vfpsb1vj7nadjifjyz45pnfh90mi84jxcqm3jx") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.2 (crate (name "mvlogger") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0n9mwj50gspsjcs7ravil0i0sy63qk1zmrgyr0p8ywr9fn2r8ip4") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.2 (crate (name "mvlogger") (vers "0.2.4") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1i18x1r6frjkrk6dajlwa2nqrpjb24fbx7qdvf9ym60ci2ds5a65") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.2 (crate (name "mvlogger") (vers "0.2.5") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "10clsrd946y98v2y45hg8fkpj2pagbjs8w5977m190wlc46rkvws") (rust-version "1.75.0")))

(define-public crate-mvlogger-0.3 (crate (name "mvlogger") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1qh6m6855j7rw84cf3m0y05d86n18izxz7r95jyirz69sz2fp19q") (rust-version "1.75.0")))

