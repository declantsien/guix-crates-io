(define-module (crates-io mv bi) #:use-module (crates-io))

(define-public crate-mvbitfield-0.0.0 (crate (name "mvbitfield") (vers "0.0.0") (hash "050zkz08j6lq33c538m1lpzfp3547nh5dbwddqn3nh7b7vwqgdiq")))

(define-public crate-mvbitfield-0.1 (crate (name "mvbitfield") (vers "0.1.0") (deps (list (crate-dep (name "bitint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mvbitfield-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1cj8k2r3p2kffcz3nrdmiz38dlw4b5zn3xx06b28zd3k9vkpjwd1") (features (quote (("doc") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-mvbitfield-0.1 (crate (name "mvbitfield") (vers "0.1.1") (deps (list (crate-dep (name "bitint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mvbitfield-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1xdcb7pxlwnisb1r6y4wq2svylg03c5dnpgrqbkjismsyc3xxpag") (features (quote (("doc") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-mvbitfield-0.2 (crate (name "mvbitfield") (vers "0.2.0") (deps (list (crate-dep (name "bitint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mvbitfield-macros") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1xbg7vc0ssv296j1r5180s193l13zfawdl2skf434466r4ssp527") (features (quote (("doc") ("_trybuild_tests") ("_nightly"))))))

(define-public crate-mvbitfield-macros-0.1 (crate (name "mvbitfield-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "175bfs090kpwv9gk5yssdbk4f707cwyazhlicrqhag4ck060491p")))

(define-public crate-mvbitfield-macros-0.2 (crate (name "mvbitfield-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "14ch6p5k0rkcsd6kfhc5qgwph44xbcq9mar7mmixa6jw4cdf7fzc")))

