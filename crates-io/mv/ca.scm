(define-module (crates-io mv ca) #:use-module (crates-io))

(define-public crate-mvcapi-3 (crate (name "mvcapi") (vers "3.0.11") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0hfarqryrrlz1nkfhimh21d66xlwx5mvzxlqjzdl00ncx49nihkk")))

