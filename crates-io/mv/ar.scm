(define-module (crates-io mv ar) #:use-module (crates-io))

(define-public crate-mvar-0.1 (crate (name "mvar") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "shuttle") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ba08wd5z8bd68gaynyasgwcniyvm9fqp2bl11l698z49apwwhyw")))

