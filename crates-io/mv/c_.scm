(define-module (crates-io mv c_) #:use-module (crates-io))

(define-public crate-mvc_traits-0.0.0 (crate (name "mvc_traits") (vers "0.0.0") (hash "0h0v1q1d6lzp9fqxlrvp63vl82v9pphas3zc2j3pq77cr1sjianm")))

(define-public crate-mvc_views-0.1 (crate (name "mvc_views") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08vskhrb6wc220zg0hmx204xygb12hdqxnqydm3s20s9bcq3q0x2")))

(define-public crate-mvc_views-0.1 (crate (name "mvc_views") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1aq2w25nv8ljis33c0mvil86fqlx4sl2y44dw0dvksrdlh1q882n")))

