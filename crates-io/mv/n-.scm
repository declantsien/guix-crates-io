(define-module (crates-io mv n-) #:use-module (crates-io))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1jlg2d01pzy732s30by3mn5917aw2iv0zrn7z4h4ggjfjfv9557h")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0g3z4payz559i2ai1n9y6snpjc5qwmza2s2vbrim2w90qh5fci34")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0k0wgmpyhlp7z9jz6xmj4a7z0bwcp87hprzg9vmsh4jkjc97rlg9")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "0w09s5jhs5h8si406s302xn136qxfydlzx26sj68s80pxks3xwjw")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.4") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "18aj5ibf0gvq26g7ihlpnawjkihzc8108jic6f73wdxbjmw8r9yx")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.5") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "148lbhm2f6vgf69cr6vbq3hw0zz3aini67rphix936gaxdkadbzw")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.6") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "198wl6waj6p6jwamksmddm7a4m4yly8z8wwngld9d6d0ajqz92qr")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.7") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "1mj52s7x5nj8gjygyadp2qwg8l519j6dp3rdka1068qgy6dp45b8")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.8") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "0k7j394mxbx0s76w54ab9ah1djv9d8dap69qpcm62357isxfh93m") (yanked #t)))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.9") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "0vh35jwvx14h7pvvaqfp3ckbddizj04zq1iiv3m5xalj6v742vbk") (yanked #t)))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.10") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "1rcg2jib8pi548nfagx5gjfn500m3sraq5kh8chbngcaahhi6jah")))

(define-public crate-mvn-autoenforce-0.1 (crate (name "mvn-autoenforce") (vers "0.1.11") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "1dkaj0dy8w1xf78m9z47wfkakbaxbaaa4qflhm7y062hgz7jyk09")))

(define-public crate-mvn-autoenforce-1 (crate (name "mvn-autoenforce") (vers "1.0.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "1fhhbz4p8yss3nhlbhvvs85vb3k26qv78sdyki5m8jajvzwa62s7")))

