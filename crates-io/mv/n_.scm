(define-module (crates-io mv n_) #:use-module (crates-io))

(define-public crate-mvn_version-0.0.0 (crate (name "mvn_version") (vers "0.0.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)))) (hash "1zgwp4lx3agp4p6hasxybjhv7klln1hfwclfi0sab3r6ywdp6ikq")))

(define-public crate-mvn_version-0.1 (crate (name "mvn_version") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)))) (hash "18mmi47f2pkpbm5k5laj1d6vxq1vkm2dj35hpdhs9a9bv1qkbh2g")))

