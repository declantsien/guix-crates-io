(define-module (crates-io mv cc) #:use-module (crates-io))

(define-public crate-mvcc-0.0.1 (crate (name "mvcc") (vers "0.0.1") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.15") (default-features #t) (kind 0)))) (hash "1mglxx7lx5rsxnbjp08vnkgp1yhd4j81hhrl0sklkkg053jam8sy")))

(define-public crate-mvcc_cell-0.1 (crate (name "mvcc_cell") (vers "0.1.0") (deps (list (crate-dep (name "guest_cell") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1p7l88w3knczqr46yv295xiqa5xlxpava2y8486yy9pj95amhf2s")))

(define-public crate-mvcc_cell-0.1 (crate (name "mvcc_cell") (vers "0.1.1") (deps (list (crate-dep (name "guest_cell") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1na1748jvzkv99sqjjiaym703ag2grydsgq572zglqjfmjir79z6")))

(define-public crate-mvcc_cell-0.1 (crate (name "mvcc_cell") (vers "0.1.2") (deps (list (crate-dep (name "guest_cell") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "017zdaqs21gh7v4pj6jrw5xpanj0nfrfp0n66jg6d366nrvi4xnb")))

