(define-module (crates-io mv sd) #:use-module (crates-io))

(define-public crate-mvsdk-0.1 (crate (name "mvsdk") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.24.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.116") (default-features #t) (kind 0)))) (hash "104mnshyss0bp3nyijp0x7l602463xik5dgy3qdf9jgsqkx8pbj6") (features (quote (("polling_example" "image") ("default_callback_example" "image") ("default") ("custom_callback_example" "image") ("compile_bindings" "bindgen"))))))

(define-public crate-mvsdk-0.1 (crate (name "mvsdk") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.116") (default-features #t) (kind 0)))) (hash "0i1rayy56xr7b55vsk6a6wihkylaq6ff4xldd7jyd57ci8mmv7rh") (features (quote (("default") ("compile_bindings" "bindgen"))))))

