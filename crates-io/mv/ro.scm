(define-module (crates-io mv ro) #:use-module (crates-io))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.0.0") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "0fq85zrwmxvvccxxznbhg85avkrk2yql1l48d7f85k9m31p9n5x9")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.0") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "0bp3ak6a7yh8wznjnbjzcxpcvg4j8yfjrmwd3qrpj1x82hy89hbm")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.1") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "0b8nl1cgfc80ij6bz446yp7mnby50zx3vzc4np8arb1iqwvxjyz0")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.2") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "0amcvl04pppl31kvpj2ig5bvs8lbk4kyyd0bi9pn6chkld1a4iip")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.3") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "0bb0kzvqddcgjlb81gh56ac1lq1p6af5239qscb5vflfsw1qbxbi")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.4") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "0p90mfbjpmbdzmcv30np7yfdhqi32d6ckxmi9fn1q88dq1qlqvr1")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.5") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "1qmxbfcj9x4wasbc6jznl0i9apkba4977xhxnk7wdhx8bbnaw8cv")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.6") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "01v5gwz2ifpsbv6ik94ax6v8cys54nqn2454wn20q00yjcd9pwnf")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.7") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "134m37cb83xy01dkj43z59b022dmm9vlynj7q9d3rd8z22jc8x0z")))

(define-public crate-mvrocketlib-1 (crate (name "mvrocketlib") (vers "1.1.8") (deps (list (crate-dep (name "mvrocketlib-macro") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "mvutils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "1vi5jvs216yn8ma92d9pbjw1hnnf15pmk5x3mkc9ygfdaqyg8mbk")))

(define-public crate-mvrocketlib-macro-1 (crate (name "mvrocketlib-macro") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00lmqx7znqxa1gadf86j1imalq3z76q51k2x5zp1q3nzy4csyx6q")))

(define-public crate-mvrocketlib-macro-1 (crate (name "mvrocketlib-macro") (vers "1.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0750gzkrg0xr46jy9bm54z5pijvzbzw8kz24d1dr00qpvwr7hl96")))

(define-public crate-mvrocketlib-macro-1 (crate (name "mvrocketlib-macro") (vers "1.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11d37im36cx7mcw7j5xyjwgpmyb8kr3xbhlfk801xbqq72vzvq60")))

(define-public crate-mvrocketlib-macro-1 (crate (name "mvrocketlib-macro") (vers "1.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vs1rycj38gi09ddbkbn5awm62qrdk4jpga7w112bly37vzcp1g3")))

(define-public crate-mvrocketlib-macro-1 (crate (name "mvrocketlib-macro") (vers "1.1.3") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h0f8s2h8yj3ggfwqplr8x2y64khi45flbbbqiwxxjklhc6iaaai")))

(define-public crate-mvrocketlib-macro-1 (crate (name "mvrocketlib-macro") (vers "1.1.4") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bzfljad8glylg4dmg4ni198xp5kb05y3nl8dacrilk4c531sdc6")))

