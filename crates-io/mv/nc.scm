(define-module (crates-io mv nc) #:use-module (crates-io))

(define-public crate-mvnc-0.1 (crate (name "mvnc") (vers "0.1.0-prealphaempty") (hash "11f9cdrd480q44b9sfvaafdvr60sj6pis5lpj0nyckgzag0y67d3")))

(define-public crate-mvnc-0.1 (crate (name "mvnc") (vers "0.1.0-prealpha") (hash "1l44722vcdd581f7mwpc239ay6rw03hlffpknlfxrf7lxzxndg39")))

(define-public crate-mvnc-0.1 (crate (name "mvnc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "186nq9mzc4jlgjlljcd93c3yl9ll6ix14li6hzmgphj2n79lf43z")))

(define-public crate-mvnc-0.1 (crate (name "mvnc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0331pkkf39n50b1advqvn644h5di2ha8195hnbj4cir2w1hixxzr")))

(define-public crate-mvnc-0.1 (crate (name "mvnc") (vers "0.1.2") (hash "1yv9yq145bd0rrxqanq8g3yd216cab9s32bf2sizialhcf88xl4q")))

(define-public crate-mvnc-0.1 (crate (name "mvnc") (vers "0.1.3") (deps (list (crate-dep (name "half") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "189yx7f5600al1ssf5wm2b3qbi4qcwzn8nfp46riavdk3ffpc9md")))

(define-public crate-mvnc-0.2 (crate (name "mvnc") (vers "0.2.0") (deps (list (crate-dep (name "half") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0x538y94gdfcf1qprf46w3dqf1lj7z9wkv41ablmaqfr6mjslvva")))

(define-public crate-mvnc-0.3 (crate (name "mvnc") (vers "0.3.0") (deps (list (crate-dep (name "half") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0a3fkhkp1ismx71bx5zd9g8y3fxgc58sf1mlmqc31qxn1w0wi92w")))

