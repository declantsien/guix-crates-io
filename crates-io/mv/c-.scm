(define-module (crates-io mv c-) #:use-module (crates-io))

(define-public crate-mvc-rs-0.1 (crate (name "mvc-rs") (vers "0.1.0") (hash "1y7xh9x755mk0cqqky48vnpvdqskz175csx98aw1r4m6h0qbcp1z") (yanked #t)))

(define-public crate-mvc-rs-0.2 (crate (name "mvc-rs") (vers "0.2.0") (hash "19lijgp2gsqqd1icxky1l2hj2snlvaz2c1ygxqqq709320pqp7hl") (yanked #t)))

(define-public crate-mvc-rs-0.3 (crate (name "mvc-rs") (vers "0.3.0") (hash "1zhkjv861l6dv4a19ank59bv8d135h0drzp4xag2g2nnzlvs1ff5") (yanked #t)))

(define-public crate-mvc-rs-0.3 (crate (name "mvc-rs") (vers "0.3.2") (hash "093vn62xvid9cqa4111iv54gv8wc49hm60b3z757mnx6rb50j12j") (yanked #t)))

(define-public crate-mvc-rs-3 (crate (name "mvc-rs") (vers "3.3.0") (hash "0r7w6h74hz4f5024wi576dq899vx96mhmqw63whbllni95ihx06q")))

