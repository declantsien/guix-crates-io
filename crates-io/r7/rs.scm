(define-module (crates-io r7 rs) #:use-module (crates-io))

(define-public crate-r7rs-0.0.0 (crate (name "r7rs") (vers "0.0.0") (hash "0l6knp8ljwik72z1v6walc25kgv7x0fwjnhif5q542vrdzgchmxq") (yanked #t)))

(define-public crate-r7rs-parser-0.1 (crate (name "r7rs-parser") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0q38mydych4dwsjq3qsm8px8jp2k8ilnv3i0dkrqkb53sz1ddqlk") (features (quote (("fixnum64") ("fixnum32") ("default" "fixnum32"))))))

