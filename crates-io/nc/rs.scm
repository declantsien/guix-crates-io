(define-module (crates-io nc rs) #:use-module (crates-io))

(define-public crate-ncrs-0.1 (crate (name "ncrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wvwlycbk0k004qzxr5swx869ywkdwjkcw440xmxbzcph035l776")))

(define-public crate-ncrs-0.1 (crate (name "ncrs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xb8zq2j5p3hsa2zvr2ahnkll2dzs5df38rfxv80lk4ync0q51f8")))

