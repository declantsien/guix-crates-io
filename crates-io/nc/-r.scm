(define-module (crates-io nc -r) #:use-module (crates-io))

(define-public crate-nc-renderer-0.1 (crate (name "nc-renderer") (vers "0.1.0") (deps (list (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "rs-ctypes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rs-math3d") (req "^0.9.15") (default-features #t) (kind 0)))) (hash "0yjqi14jhq8sgnxvihflhqfv29yx3qv1h7krinld0qhnx7anzfrx")))

(define-public crate-nc-renderer-0.1 (crate (name "nc-renderer") (vers "0.1.1") (deps (list (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "rs-ctypes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rs-math3d") (req "^0.9.15") (default-features #t) (kind 0)))) (hash "0ii6q4xn4cpyjrdapssvv9d7rs17q20d2536mjjyfl223wffb7s2")))

(define-public crate-nc-renderer-0.1 (crate (name "nc-renderer") (vers "0.1.2") (deps (list (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "rs-ctypes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rs-math3d") (req "^0.9.16") (default-features #t) (kind 0)))) (hash "10fnb9v6bydz6x1gcfkj3fv3xcdzry6pnxnvjzpi9f61j9qcbx1f")))

(define-public crate-nc-renderer-0.1 (crate (name "nc-renderer") (vers "0.1.3") (deps (list (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "rs-ctypes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rs-math3d") (req "^0.9.16") (default-features #t) (kind 0)))) (hash "1w87gcj2hf2fxffxz7fydyy2lc2la591995799wd9863fk5szkld")))

