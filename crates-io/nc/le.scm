(define-module (crates-io nc le) #:use-module (crates-io))

(define-public crate-nclean-0.1 (crate (name "nclean") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "11l07izbf1q9awsqc2f4xnlccg92hbpvf0r4i107ahjjrv90chx4")))

(define-public crate-nclean-0.1 (crate (name "nclean") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0kjgm5jarfqh48720g5yjzjxqhrpxmvhqp8izbq6jhzjxj5lzrk8")))

