(define-module (crates-io nc li) #:use-module (crates-io))

(define-public crate-nclist-0.1 (crate (name "nclist") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "019m14lx9yzax27xczfr72v2ardh3lybf4vr2bk95lahf6y8x0pq")))

(define-public crate-nclist-0.1 (crate (name "nclist") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1fn2cnc0rwywkq6zgs7qba49bndczcbrkwz3yzvvh6i7zw7ilaqs")))

