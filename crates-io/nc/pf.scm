(define-module (crates-io nc pf) #:use-module (crates-io))

(define-public crate-ncpf-0.1 (crate (name "ncpf") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "libepf") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt-multi-thread" "macros" "net" "io-std"))) (default-features #t) (kind 0)))) (hash "0sqxk7x5aq9h0j3zlhjfkz55cvqj3nrc7a11j5ivk8z8qjb75aq5")))

