(define-module (crates-io nc cl) #:use-module (crates-io))

(define-public crate-nccl-0.1 (crate (name "nccl") (vers "0.1.0") (hash "04bfjvipj11gj1pd84yr9qi36ywb9j39pfbi0ap5jvfsxcb6i8i2")))

(define-public crate-nccl-0.2 (crate (name "nccl") (vers "0.2.0") (hash "0dnazgpjlmmfgdpvmrfdc3234ilzdanjl4y3x10g08iz6jxjp1fg")))

(define-public crate-nccl-0.2 (crate (name "nccl") (vers "0.2.1") (hash "11wb755jm0wmgxcj6viiiafmacaf1bvwa4rczchwa31c452g0g2k")))

(define-public crate-nccl-0.3 (crate (name "nccl") (vers "0.3.0") (hash "0wf7r2manq5xjpm59klky15sspvkcy47h2r08i243v1x6mmfk6ky")))

(define-public crate-nccl-0.3 (crate (name "nccl") (vers "0.3.1") (hash "01hchpw1b6053sysfbwbh38lqckyx6jbr1i0fk71msb8l1i9j1hv")))

(define-public crate-nccl-0.4 (crate (name "nccl") (vers "0.4.0") (hash "1wlgiw3vn3mcs2jgp8fxw46xz0wjz7adhx6ybj8jvcfmrxxz620a")))

(define-public crate-nccl-0.4 (crate (name "nccl") (vers "0.4.2") (hash "0j375dp92av5wq0hmqiw7nrpk4qvbp8bgfijm3zqk0jk15gdjm0l")))

(define-public crate-nccl-0.4 (crate (name "nccl") (vers "0.4.3") (hash "1cm05552558626ficlyazjpgan584czkn1pfa2mf4f2f8wjm5dvb")))

(define-public crate-nccl-0.5 (crate (name "nccl") (vers "0.5.0") (hash "10s3nw9r3hkvsinv9k6xq587x909s4j64r40w6wipq4dhsg7mwj6")))

(define-public crate-nccl-0.6 (crate (name "nccl") (vers "0.6.0") (hash "0xvx3kq1vvvwcilsq1mcj71q9857z1gd42x067kpilhznv2sgljy")))

(define-public crate-nccl-1 (crate (name "nccl") (vers "1.0.0") (hash "1hyy4wzprx41pwpis8q0p925l9753akcynk5l7pcld3m3zkl1pya")))

(define-public crate-nccl-2 (crate (name "nccl") (vers "2.0.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1hza76kcwcz925pf5937ggll4f99n16n894qfx02hn20z0c5yrwv")))

(define-public crate-nccl-2 (crate (name "nccl") (vers "2.0.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1q5ns3ashn32p3z64lcsgw1f7ijc2hzkfp8smlx90vg34pza6wk7")))

(define-public crate-nccl-2 (crate (name "nccl") (vers "2.0.2") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "058f5n8p6pixyq05aicnhkl6y3f9r2k7mj3njkwaz4673gcprsiz")))

(define-public crate-nccl-3 (crate (name "nccl") (vers "3.0.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1pwx1hdq0lp4zc5w8r2gy68yxn878p4xqpmfjblmcc68jk1m1f76")))

(define-public crate-nccl-4 (crate (name "nccl") (vers "4.0.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1hr7if7cc6pizd3nscn39n2f3p2sz12lhlj5z8ns7k8dxpdzpjya")))

(define-public crate-nccl-4 (crate (name "nccl") (vers "4.0.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0y592razmc6bqr3aj3ycjcz1vpimw3isiqaxrazhp0ksrkg5aslg")))

(define-public crate-nccl-4 (crate (name "nccl") (vers "4.0.2") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0w6v4nwd295glvkr267nsgdy2366m3vqfrk9s4iwy6bx6cpw36a6")))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.0.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0wf7pxbh6v9mb6gpq0dxr804ai8ljdkgrx2mi7cn9mwdl8m1rsyk")))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.0.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1xkwzpw9vnnd450m6pzr0ml5d8817ypds12ijlfxy5vx6ga6iqs9")))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.0.2") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1yshlk42a8fh1g61haas3qxb5v5ai3qg62sdfg24ijbxxvc85anv") (features (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1ac4is26w6i2wzkikq8dj7h5mk4323kqbdhvf59wdzpcvnfspp8v") (features (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.1.1") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "06qk87gdk7kwhdk11mwdhzfsxlgc56hkrfb2y9ja1ilnv3jfhd48") (features (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.2.0") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "17xf7r356189syqx3268315yw5gda6grax2k3hd4gwbvxsjplsvc") (features (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.3.0") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1bf2r544vdn8cnd7ii6yjm7bhv0p5zw0icry5ckval37isarl766") (features (quote (("fuzz" "arbitrary"))))))

(define-public crate-nccl-5 (crate (name "nccl") (vers "5.4.0") (deps (list (crate-dep (name "arbitrary") (req "^1.3.2") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1blz838d86mpl9jc4p3ym0q600bicaqqp9374hbq2wgpr4hy3hyp") (features (quote (("fuzz" "arbitrary") ("default"))))))

