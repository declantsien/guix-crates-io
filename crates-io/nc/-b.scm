(define-module (crates-io nc -b) #:use-module (crates-io))

(define-public crate-nc-broadcast-0.1 (crate (name "nc-broadcast") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.7") (default-features #t) (kind 0)))) (hash "15ik9cil9ldzzd451m9x4673f7cr3rmlcx9hclxk68ahaf8sglli")))

(define-public crate-nc-broadcast-0.1 (crate (name "nc-broadcast") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.7") (default-features #t) (kind 0)))) (hash "0pg3g81z5x11pv2lsgs54a38dnk8sbyybyp0kx21lfsrpqp2fsys")))

(define-public crate-nc-broadcast-0.1 (crate (name "nc-broadcast") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.7") (default-features #t) (kind 0)))) (hash "0b6qz6h4lgd8i1jk950wgijyylb2h1ajcg1f2ldp6f38d00my5y4")))

(define-public crate-nc-broadcast-0.1 (crate (name "nc-broadcast") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2.7") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "047gw357wmzlyl9kax1ib74wggzdj22i680wbhq8ywhj8wmw4yv2")))

