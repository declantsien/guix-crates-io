(define-module (crates-io nc w-) #:use-module (crates-io))

(define-public crate-ncw-convert-0.1 (crate (name "ncw-convert") (vers "0.1.0") (deps (list (crate-dep (name "hound") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "ncw") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lln1qs5m0z2w4zr5avpnsmcp6w5aid6hn102rj6ns0wb10j1nkm")))

(define-public crate-ncw-convert-0.1 (crate (name "ncw-convert") (vers "0.1.1") (deps (list (crate-dep (name "hound") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "ncw") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gknnjws4s7jhrxl33i1fmhi0qnajncsfsxw2lfgr2i7ygn9m2pr")))

(define-public crate-ncw-convert-0.1 (crate (name "ncw-convert") (vers "0.1.2") (deps (list (crate-dep (name "hound") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "ncw") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1x5rlmm3s0wsp87mmw003x7as1df1mplvrs4w8i3xkbppr2j96jh")))

