(define-module (crates-io nc p5) #:use-module (crates-io))

(define-public crate-ncp5623-0.1 (crate (name "ncp5623") (vers "0.1.0") (deps (list (crate-dep (name "derive-new") (req "^0.5.9") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "07w7c3gd47qalfsjyc18hxyp6p8cls60k6vvhgnb55cj58kxppl8")))

(define-public crate-ncp5623-0.1 (crate (name "ncp5623") (vers "0.1.1") (deps (list (crate-dep (name "derive-new") (req "^0.5.9") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 2)))) (hash "1wf7m4fd88alcvvcydhq6za48kahk3ndjlr1wz3fj3rhdxw8vby9")))

(define-public crate-ncp5623-0.1 (crate (name "ncp5623") (vers "0.1.2") (deps (list (crate-dep (name "derive-new") (req "^0.5.9") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 2)))) (hash "0hc6r0mpcphfhzvikghpykyqnzcdvjwy5nyh442ddy2mahyq283h")))

(define-public crate-ncp5623-0.1 (crate (name "ncp5623") (vers "0.1.3") (deps (list (crate-dep (name "derive-new") (req "^0.5.9") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 2)))) (hash "1s8f69fqwsklsw9sb1zgip15z3anmw55x03ai55lx5bf4jj4mi4p")))

(define-public crate-ncp5623c-0.1 (crate (name "ncp5623c") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "047qhdprivpvjl4cmwv1qvvzp9gyngwkynw0kg7zkzc04w6wva8y")))

