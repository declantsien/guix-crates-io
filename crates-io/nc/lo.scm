(define-module (crates-io nc lo) #:use-module (crates-io))

(define-public crate-nclosure-0.1 (crate (name "nclosure") (vers "0.1.0") (deps (list (crate-dep (name "chain-trans") (req "^1") (default-features #t) (kind 0)))) (hash "0ymaisq7zh7jm842v9dydnrk2nhnanmb9cjzh9cqg8fyhgj6fq2y") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

