(define-module (crates-io nc on) #:use-module (crates-io))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req ">=2.0.0, <3.0.0") (default-features #t) (kind 0)))) (hash "0fqn7acn24n10izz73ik7wn2pljp1n8wglzm7f903vhygyafys7f")))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req ">=2.0.0, <3.0.0") (default-features #t) (kind 0)))) (hash "0l90m3wmas44fm1ldgd30ghdnx8d5d73ig1h61hnsbfjpd3r96dx")))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1i29dm4gg4zdjly587rh09caqfv4gxj9rdiz5af8l3m82wa25nlq")))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1c17rz26585h9mz9r8ys17h54qwws961qhrqjci0mwgcmq55hr8j")))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0mxyy41lk87vbibimkjghhcif8rkaqflgn1kiqqdqk80yh3jbhlc")))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0f9s1a97gk03qgm2c50p9igy1abqqi306bizfjclck34jqaqwnf9")))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0pb1afx9byffb78lqv35p6mbk1nidh9nn3jh78wj8pnimzmmjzsb")))

(define-public crate-nconsole-0.1 (crate (name "nconsole") (vers "0.1.7") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1ijs1ngm481bddx3d82n6xy3l8br1abv5id3cgxjxjpc4l2g135b")))

(define-public crate-nconsole-0.2 (crate (name "nconsole") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0yqcy72as0nq8a8cbvly3wi0z7vylqy2vkh3xwfnzkpdbsdaij16")))

(define-public crate-nconsole-0.2 (crate (name "nconsole") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "052yh7ajgwsnw93mac3kl5xzj19qngvw0l52q0l93wr9lzw7gv6a")))

(define-public crate-nconsole-0.3 (crate (name "nconsole") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "154mk2xrw9lry8g7wv7fshal55r7g1fy6201309581s2hnz6lrdy")))

(define-public crate-nconsole-0.4 (crate (name "nconsole") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1y8ir6mz9fi491sj5hm12hlx4rcgb640r38l9vpvchrg0k4c70ii")))

(define-public crate-nconsole-0.5 (crate (name "nconsole") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "04swp4ciw784wbzs10456q6z5fbk5178qz98yfi9iv64bs62h0cc")))

