(define-module (crates-io nc om) #:use-module (crates-io))

(define-public crate-ncomm-0.1 (crate (name "ncomm") (vers "0.1.0") (hash "1zdfl23cqais6w6zsf3gxcflz6nmgfh9jazwlwkqhbja6kx9jbij")))

(define-public crate-ncomm-0.1 (crate (name "ncomm") (vers "0.1.1") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1lg1rgyppz8ljcgb8v6d1lmqfsvrh774xcvq4cf70dvb8agqnmdq")))

(define-public crate-ncomm-0.1 (crate (name "ncomm") (vers "0.1.2") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "09ljcyssi8wvalrc6xril00xwyp7g5kbf4kziq8lpcpwzrb3gm05")))

(define-public crate-ncomm-0.2 (crate (name "ncomm") (vers "0.2.0") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "0vqnswj3945jp88n8ks3fs0g97gyfgw7nr0nczm5rglbn7pglzss")))

(define-public crate-ncomm-0.2 (crate (name "ncomm") (vers "0.2.1") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "0yidlkw6hdc8yhsd24jnxzz0mdf0846lh0dhwp8c3bavmpk6gk17")))

(define-public crate-ncomm-0.3 (crate (name "ncomm") (vers "0.3.0") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)))) (hash "0mh4n9zv72s32jjx4jqn1ajbqhik08ajyvs4x83fgqd78pvlp1wv") (features (quote (("default")))) (v 2) (features2 (quote (("packed-struct" "dep:packed_struct"))))))

(define-public crate-ncomm-0.4 (crate (name "ncomm") (vers "0.4.0") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)))) (hash "03nvycmv9jvslj8qzxfjg6m2z2f065minkvkd1bphg1far8pbzmg") (features (quote (("default")))) (v 2) (features2 (quote (("packed-struct" "dep:packed_struct"))))))

(define-public crate-ncomm-0.4 (crate (name "ncomm") (vers "0.4.1") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)))) (hash "1rhmj71k82mdfwyj34hxb0afcd1z8kj5ih3x9xbnz96l8m0ga543") (features (quote (("default")))) (v 2) (features2 (quote (("packed-struct" "dep:packed_struct"))))))

(define-public crate-ncomm-0.4 (crate (name "ncomm") (vers "0.4.2") (deps (list (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "packed_struct") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)))) (hash "1njkv25m3f10siscf862l3s4zgz0jhq65zlzif9n12slhkldb6dm") (features (quote (("default" "packed-struct")))) (v 2) (features2 (quote (("packed-struct" "dep:packed_struct"))))))

