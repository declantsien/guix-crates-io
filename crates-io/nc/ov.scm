(define-module (crates-io nc ov) #:use-module (crates-io))

(define-public crate-ncov-cli-0.1 (crate (name "ncov-cli") (vers "0.1.0") (deps (list (crate-dep (name "cursive") (req "^0.17.0-alpha.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02a5xms397rvxbn3498wfkzfr7f0z7mn7dmwwhczybq9kq4g7v60")))

