(define-module (crates-io nc vm) #:use-module (crates-io))

(define-public crate-ncvm-0.0.0 (crate (name "ncvm") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0vj025x0vi3xnj2kzqazjq97zzaj6pa31c4mz12rkxd9am9mslj8")))

(define-public crate-ncvm-0.0.1 (crate (name "ncvm") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0fs666h82rld53iwwbmmfbpd87zrpa2dgbcqngxnnlrwrp0riavs")))

(define-public crate-ncvm-0.0.2 (crate (name "ncvm") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "00aq0fwf2g44az63mjkblh6zgjjz7p7vkibgnas69k4lzqbzazzi")))

(define-public crate-ncvm_rust-0.0.0 (crate (name "ncvm_rust") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0zv2c6jyjgjsfqblhd3fs49lgyrdz83bq5g2j013m41h27sf7m1x") (yanked #t)))

