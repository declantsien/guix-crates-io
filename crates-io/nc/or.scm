(define-module (crates-io nc or) #:use-module (crates-io))

(define-public crate-ncore-0.1 (crate (name "ncore") (vers "0.1.0") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("color" "derive" "std" "unstable-doc"))) (kind 0)))) (hash "1d6461byb1vi1zgc40xgqsiyhwimyrqrjj4ch2axgxyxa2n4nl2h")))

