(define-module (crates-io nc to) #:use-module (crates-io))

(define-public crate-nctok-0.1 (crate (name "nctok") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "human_format") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2.6") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "1himq3jjhrr25zjv9vc6wdylmjxfxhhs59fqjhwyc3krbas8v4ky")))

