(define-module (crates-io nc he) #:use-module (crates-io))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.0") (hash "0hdwbn34915yyzq1b0y329pcls0zqph9rqxkm0yh98m8r9nf7mly") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.1") (hash "1n9fyxwnxc4ah1yq52xqvc9gbn4i3zcic7z49p8zmmlp2rvllz3k") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.2") (hash "1zmjgs83lxvknv25dlld0900hgygzdg7zdrzqw1agxhavww3rz2k") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.3") (hash "0xj0ja5pr5wzajv3cks865gbbkm73bli5y2i0iwjam3y06fm6rcg") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.4") (hash "10ypzln7pbxim91bjzax1pswgh5mgpp5kgp5nhm8r3v1xd2z2pp1") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.5") (hash "0w3a6qcc8vi735p2xgyx59x3q4r85mgghxqhs2ch14n06bcdxh6r") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.6") (hash "1n4hw1dwh58lfff8l498cxzslcfnib30zr8405v2wmy8g13fhbkq") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.7") (hash "1hjw0pvx3vafpprsgq3z9gy1krjns473pdli78y7l2wplw1hbick") (yanked #t)))

(define-public crate-ncheat-0.1 (crate (name "ncheat") (vers "0.1.8") (hash "16dm5akmkjw734kq3h717y7r63xa4y7039rz8gm20cx0vxks9ndw") (yanked #t)))

(define-public crate-ncheat-0.3 (crate (name "ncheat") (vers "0.3.0") (hash "1syhiprv8rphhbkapgyb0xplh9za1xhydarijm8v3yd2z2ldji2l") (yanked #t)))

(define-public crate-ncheat-0.4 (crate (name "ncheat") (vers "0.4.3") (deps (list (crate-dep (name "unsafe_fn_body") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fw1623g5mikywfn7fcvh5rprvfpmg9i4bw47fpf9km5xk3r9f1b")))

(define-public crate-ncheat-0.4 (crate (name "ncheat") (vers "0.4.4") (deps (list (crate-dep (name "unsafe_fn_body") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a6cmxcm34v0pk66r1i3gq0i9zf2q10rp4c23ic1rwrng57yjd0d")))

