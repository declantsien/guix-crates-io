(define-module (crates-io lx -p) #:use-module (crates-io))

(define-public crate-lx-parser-0.1 (crate (name "lx-parser") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0f7wlm00gjjr95b99bhg3vd5axk6pc23qj2i3gqccdmg8198srqr")))

