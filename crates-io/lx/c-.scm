(define-module (crates-io lx c-) #:use-module (crates-io))

(define-public crate-lxc-idmap-0.7 (crate (name "lxc-idmap") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)))) (hash "1pmmm7mm7izzkxpma34yh0bd0ixn7d9xxhnd6rx79i7yiq908gzi")))

(define-public crate-lxc-sys-0.1 (crate (name "lxc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18yf8vp7kh6x5nwc4qn2hm1607n7bqfjbzixvnwi93dkgyykds6p")))

(define-public crate-lxc-sys-0.1 (crate (name "lxc-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.36") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i92w04i39k1ipazpmb47hzij7mw3nz2g2glm4g1ip92piynf7dg")))

(define-public crate-lxc-sys-0.2 (crate (name "lxc-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18aranwvp1hibhq3v60ic8cjv42r552mi186sddd489c2hi2bbpi")))

(define-public crate-lxc-sys-0.2 (crate (name "lxc-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "16kjspi7cpzlb04zilv651mmylxyd04i3vxd93l37ckhsa4c1mh2")))

(define-public crate-lxc-sys-0.3 (crate (name "lxc-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f7yy09ai2wgvm5mh759jy6k2rjqfnnk89f10abdb0w4r5x5n1il")))

(define-public crate-lxc-sys-0.4 (crate (name "lxc-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05nr2y4dawfgaiabkm37y3wp0rqbkrv0w3sla4nh2y5zrr3kq7x3") (links "lxc")))

(define-public crate-lxc-sys-0.5 (crate (name "lxc-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "172d7gl0lwq4xjix7ifv750za87bc7bfx2gvln20ibkj25v4y9aw") (links "lxc")))

(define-public crate-lxc-sys2-1 (crate (name "lxc-sys2") (vers "1.0.0") (hash "0y6dsfz3vaidjdnbn20lmll67y5xf6jqs2bgn1q1mf23r263rg3z") (links "lxc")))

