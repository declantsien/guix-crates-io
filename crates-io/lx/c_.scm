(define-module (crates-io lx c_) #:use-module (crates-io))

(define-public crate-lxc_profile-0.1 (crate (name "lxc_profile") (vers "0.1.0") (hash "1dmqzgs7s19y7pbk3d8y261svkyy9ylgv4rjph1d2bb4sq1vxs0p")))

(define-public crate-lxc_rust-0.1 (crate (name "lxc_rust") (vers "0.1.2") (hash "1ndza7qjgbik7d87n3j71qfls6nsm3pqg9sv9r2h18dag90cr64r")))

