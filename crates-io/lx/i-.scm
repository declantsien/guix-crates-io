(define-module (crates-io lx i-) #:use-module (crates-io))

(define-public crate-lxi-device-0.0.0 (crate (name "lxi-device") (vers "0.0.0") (hash "0ph3isz5g0nwkgi5zj7jf1b475cx4cr30lh89q4hdqjx8md97dbc")))

(define-public crate-lxi-hislip-0.0.0 (crate (name "lxi-hislip") (vers "0.0.0") (hash "17lacpx5ibx6bd10170a2anawmaahardys1q68afa0z4jj3f8y23")))

(define-public crate-lxi-raw-0.0.0 (crate (name "lxi-raw") (vers "0.0.0") (hash "1y3ipcrzki9r8ya3cyip03rmbcdmixa6fvayxmkr2x0ck6ymlxm4")))

(define-public crate-lxi-telnet-0.0.0 (crate (name "lxi-telnet") (vers "0.0.0") (hash "03l9zi3b53x9mqmvcp7y1mhwv4r20jqc0piw2c8n9f1a5q2q10pg")))

(define-public crate-lxi-vxi11-0.0.0 (crate (name "lxi-vxi11") (vers "0.0.0") (hash "075agfx12sylik43yf2ig59pnv42g9gdkkw4ynqn7y3vy766dldq")))

