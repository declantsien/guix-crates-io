(define-module (crates-io lx in) #:use-module (crates-io))

(define-public crate-lxinfo-0.1 (crate (name "lxinfo") (vers "0.1.2") (deps (list (crate-dep (name "byte-unit") (req "^4.0.18") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "15fd6p5a5ry0bw5i6n2l2lf0cc8jgcxgdjkp0yz8d32if1apxvpg")))

