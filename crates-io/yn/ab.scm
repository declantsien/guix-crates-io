(define-module (crates-io yn ab) #:use-module (crates-io))

(define-public crate-ynab-api-1 (crate (name "ynab-api") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "~0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0fq54ji4d6lf0r6kvg1mrf9djlgb34ijl7jg7aff0blmhvbq2i80")))

(define-public crate-ynab-api-2 (crate (name "ynab-api") (vers "2.0.0") (deps (list (crate-dep (name "reqwest") (req "~0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "1a1b7isqq4qijhai1rq19lb148x3bpdy2h8dzl4j4dvjx4qrf972")))

(define-public crate-ynab-api-3 (crate (name "ynab-api") (vers "3.0.0") (deps (list (crate-dep (name "reqwest") (req "~0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0i69dz3vcx8k4bk9ayz82m2f0c4w7ngas7z4znn75b1brc9fmdcp")))

(define-public crate-ynab-export-0.0.1 (crate (name "ynab-export") (vers "0.0.1") (deps (list (crate-dep (name "directories") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ynab-api") (req "^1") (default-features #t) (kind 0)))) (hash "1igl75lffgc62b1h9shp9zs6iijs5g6a0y6fnr7i4imb6kiq1f7c")))

(define-public crate-ynab-export-0.0.2 (crate (name "ynab-export") (vers "0.0.2") (deps (list (crate-dep (name "directories") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ynab-api") (req "^1") (default-features #t) (kind 0)))) (hash "189ianpggz0gv2wjhl8adr084g35fja9scn919112wkprc8glk46")))

(define-public crate-ynab-export-0.0.3 (crate (name "ynab-export") (vers "0.0.3") (deps (list (crate-dep (name "directories") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ynab-api") (req "^2") (default-features #t) (kind 0)))) (hash "0vgnjhm1a75cb8j6h3wph7dzv2cw35b1dmj0ms75i5wxqkf3pfm5")))

(define-public crate-ynab-export-0.0.4 (crate (name "ynab-export") (vers "0.0.4") (deps (list (crate-dep (name "directories") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ynab-api") (req "^3") (default-features #t) (kind 0)))) (hash "0a6c6i1i2j98z8kjr2gdsd7yg8j4kfrk8g5qa5gz0kivj2ik2w1q")))

(define-public crate-ynab-reimbursements-0.0.1 (crate (name "ynab-reimbursements") (vers "0.0.1") (deps (list (crate-dep (name "cursive") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "cursive_table_view") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ynab-api") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mpa53p8ysd2h331in1rs1xh8b5mhm51b4w3fzhkr433ak2a1p6a")))

(define-public crate-ynab-reimbursements-0.0.2 (crate (name "ynab-reimbursements") (vers "0.0.2") (deps (list (crate-dep (name "cursive") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "cursive_table_view") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ynab-api") (req "^2") (default-features #t) (kind 0)))) (hash "1maiz8320lnhlysn8c0vpg8vfiyhynihcy0j251d70nlf7c9ix1q")))

(define-public crate-ynab-reimbursements-0.0.3 (crate (name "ynab-reimbursements") (vers "0.0.3") (deps (list (crate-dep (name "cursive") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "cursive_table_view") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ynab-api") (req "^3") (default-features #t) (kind 0)))) (hash "0m6zi1jnhlk76508mb79kf0fw0m5yr2v609x13q0r1ayfdbi4v2h")))

(define-public crate-ynabifier-0.0.0 (crate (name "ynabifier") (vers "0.0.0") (hash "16ybk9ql0c1bxs184wrwd8ipfn8c2n5xpys7l3fck6xf0pm1qgwd")))

