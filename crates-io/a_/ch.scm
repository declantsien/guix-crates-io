(define-module (crates-io a_ ch) #:use-module (crates-io))

(define-public crate-a_chat-0.1 (crate (name "a_chat") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1594mdw0hldj8dghppfxcjz83qgrvgy4ikiazif0fhd8hrapj0pw")))

