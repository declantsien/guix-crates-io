(define-module (crates-io sr ch) #:use-module (crates-io))

(define-public crate-srch-0.0.1 (crate (name "srch") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1qqhdfidji2f837lg9khn38jgbfj48lc1dpvp8fjs6sh5vgcljb3")))

