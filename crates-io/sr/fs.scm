(define-module (crates-io sr fs) #:use-module (crates-io))

(define-public crate-srfs-0.1 (crate (name "srfs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "2.4.*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "srfs-core") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "05xbnmyk7bavb3hqm3q774r6naw2gl4knm5wmf1z59inljazzl1j")))

(define-public crate-srfs-0.1 (crate (name "srfs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "2.4.*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "srfs-core") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1378llhjl8rd62zz2r5syzvg5isn0n9nr75r50andazhbrvxq9kk")))

(define-public crate-srfs-0.1 (crate (name "srfs") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "2.4.*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "srfs-core") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "03qzczlg7kr7dgbsmi907nrwmxj4cp8wz5shalizgq2p71szx6k7")))

(define-public crate-srfs-core-0.1 (crate (name "srfs-core") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0jb59q9p1cl77251dc8acz3lh1xi6xrlfkbh0xybh1mrlirpjr8b") (features (quote (("std") ("default"))))))

(define-public crate-srfs-core-0.1 (crate (name "srfs-core") (vers "0.1.1") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1x72cm4axcxjn27r0i7f1y67y0nxaz5zxydfwr6w6li94z4x0k2w") (features (quote (("std") ("default"))))))

(define-public crate-srfs-core-0.1 (crate (name "srfs-core") (vers "0.1.2") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "18726r6bsf4zvb8h57drcwc7lsq9rdi7iqpgp3qggjcfh724yfvf") (features (quote (("std") ("default"))))))

