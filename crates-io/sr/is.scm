(define-module (crates-io sr is) #:use-module (crates-io))

(define-public crate-srisum-1 (crate (name "srisum") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ssri") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0b2s4w5ivawq9b3wq0y0995izng6mdysccjiyjldqk8g48sbp1sl")))

(define-public crate-srisum-2 (crate (name "srisum") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ssri") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1ih1k31xf0hc4ng3q6p1zhvfccfg7ppvhiblapbfcmhh1j19fx77")))

(define-public crate-srisum-3 (crate (name "srisum") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ssri") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "04cqr242rn75ilynsabp7qp1h6qm5b45f1sdpxn77dbj555xkl17")))

(define-public crate-srisum-4 (crate (name "srisum") (vers "4.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^4.7.1") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "ssri") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1qz7wxcck4i3iwvlml0q94hsyikxyhg7hj8hsm2vj5wcljgvi0m4")))

(define-public crate-srisum-5 (crate (name "srisum") (vers "5.0.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.5.0") (features (quote ("fancy"))) (default-features #t) (kind 0)) (crate-dep (name "ssri") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0hdh4zzxcv6dy8lg4pa810x2xj745318s5ffqmgysqhb6f7rhbcm")))

