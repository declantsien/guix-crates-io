(define-module (crates-io sr fn) #:use-module (crates-io))

(define-public crate-srfng-0.1 (crate (name "srfng") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0fipxg9vsqk1nrshjjr5lv9h23v4qx1zcv86090v3qsx191g5jsb")))

(define-public crate-srfng-1 (crate (name "srfng") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0w9arqi2y1ji3cixnziiq8md8zma95nclsc3mbwz4xpimpscfsg9")))

(define-public crate-srfng-1 (crate (name "srfng") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dcffs0v9s1yhqg8cjwjbi8n4195b54ahbszgrmnkfm9nyvaz4a7")))

(define-public crate-srfngout-1 (crate (name "srfngout") (vers "1.0.1") (deps (list (crate-dep (name "srfng") (req "1.*") (default-features #t) (kind 0)))) (hash "0r2dygfzz8fyh3f4wq4flsg2zqkci6mqf5y661j0l1irn45lv0xr")))

