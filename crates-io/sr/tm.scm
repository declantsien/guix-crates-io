(define-module (crates-io sr tm) #:use-module (crates-io))

(define-public crate-srtm-0.1 (crate (name "srtm") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0x748apwgy5hicgbrbib8jk2x1zbkhmz4kz7wlv6k3q7n6kq9h5f")))

(define-public crate-srtm-0.1 (crate (name "srtm") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "16kp36ffbk7qmzjwsw87bhizn5lfwsqsyw6fm99cmcvk8vxk99ns")))

