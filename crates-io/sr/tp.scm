(define-module (crates-io sr tp) #:use-module (crates-io))

(define-public crate-srtp-0.1 (crate (name "srtp") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "srtp2-sys") (req "^2.20") (default-features #t) (kind 0)))) (hash "19bk5qlxrywmh0yv0zysjmjwbwn7ax4wjhzqj7hb2db276g2zxfx")))

(define-public crate-srtp-0.2 (crate (name "srtp") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "srtp2-sys") (req "^2.20") (default-features #t) (kind 0)))) (hash "04rqgrg39xhxpp8ahzkgs9wva0q9jwxacsgfg8z3p5ii97mfdjbb")))

(define-public crate-srtp-0.3 (crate (name "srtp") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "srtp2-sys") (req "^2.20") (default-features #t) (kind 0)))) (hash "1855izkdc01sf32j6aas7flhpb70fwxhh49pl9fc328mzmy3x6gs")))

(define-public crate-srtp-0.4 (crate (name "srtp") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "srtp2-sys") (req "^2.20") (default-features #t) (kind 0)))) (hash "024nlc6lwnjr4y5pjc3c56va5armmlhwfahvp5y5dq04rcaq4zia")))

(define-public crate-srtp-0.5 (crate (name "srtp") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "srtp2-sys") (req "^2.30") (default-features #t) (kind 0)))) (hash "1plm88wa0d8ybsfxr891lmvh9h7q9k6f2f815zqgnj9hf9wgysrq")))

(define-public crate-srtp-0.6 (crate (name "srtp") (vers "0.6.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "srtp2-sys") (req "^2.30") (default-features #t) (kind 0)))) (hash "05vky1qh582dbgryzd7vr8qnsmhsv7lx9p2kjqgm5zqfp4rp7i1c")))

(define-public crate-srtp-0.7 (crate (name "srtp") (vers "0.7.0") (deps (list (crate-dep (name "bytes") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "srtp2-sys") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zv9n818pl9lm7cwq5c82v0knr432xlq70jv7s8wb0mgr8r0sm7v") (features (quote (("skip-linking" "srtp2-sys/skip-linking") ("enable-openssl" "openssl" "srtp2-sys/enable-openssl") ("default" "bytes" "log" "enable-openssl" "build-libsrtp") ("build-openssl" "enable-openssl" "srtp2-sys/build-openssl") ("build-libsrtp" "srtp2-sys/build"))))))

(define-public crate-srtp2-sys-2 (crate (name "srtp2-sys") (vers "2.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "08dka79k5hzvbgxdx9984dblc62mbpac8kpcfb5b10r22604592y") (yanked #t) (links "srtp2")))

(define-public crate-srtp2-sys-2 (crate (name "srtp2-sys") (vers "2.20.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "11frnn4dm05sy60r5gaixas4sg4w6pi2wyr7dpv7xn9av59cnzpw") (features (quote (("enable-log-stdout") ("enable-debug-logging")))) (yanked #t) (links "srtp2")))

(define-public crate-srtp2-sys-2 (crate (name "srtp2-sys") (vers "2.20.1") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0aigdfqj5lh212110fxl5dy0ziwjhqjw7p3qcbdwipn6lawlijv7") (features (quote (("enable-log-stdout") ("enable-debug-logging")))) (links "srtp2")))

(define-public crate-srtp2-sys-2 (crate (name "srtp2-sys") (vers "2.20.2") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1709la0ansmdj46p8mnkfd2ic66sp5zrigybc2wqygri2zs3c92z") (features (quote (("enable-log-stdout") ("enable-debug-logging")))) (links "srtp2")))

(define-public crate-srtp2-sys-2 (crate (name "srtp2-sys") (vers "2.30.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "10ls4ynajhyaw7d2i64fqady1rqh6qjfdmwp2m5610px2ykn0bqz") (features (quote (("enable-log-stdout") ("enable-debug-logging")))) (links "srtp2")))

(define-public crate-srtp2-sys-3 (crate (name "srtp2-sys") (vers "3.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "1fn4q0i5l5zbmxzsvv75hypzqcbvxqi4vmxah6b7i97d1igp946m") (features (quote (("enable-openssl" "openssl-sys") ("build-openssl" "enable-openssl" "openssl-sys/vendored") ("build" "make-cmd")))) (yanked #t) (links "srtp2")))

(define-public crate-srtp2-sys-3 (crate (name "srtp2-sys") (vers "3.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "07q5flg0r9i1dba6m3isq1affncd0b2m2jz4zxd1l4zji1f1lz6m") (features (quote (("enable-openssl" "openssl-sys") ("build-openssl" "enable-openssl" "openssl-sys/vendored") ("build" "make-cmd")))) (links "srtp2")))

(define-public crate-srtp2-sys-3 (crate (name "srtp2-sys") (vers "3.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "10bssaj7srffhihak0hjcmcjn88bv8bjzjw9nfskxl9czzqdpjwc") (features (quote (("skip-linking") ("enable-openssl" "openssl-sys") ("build-openssl" "enable-openssl" "openssl-sys/vendored") ("build" "make-cmd")))) (links "srtp2")))

(define-public crate-srtparse-0.1 (crate (name "srtparse") (vers "0.1.0") (hash "0697m661kid729m74r6pqjxzhivdqfhgwpxd1q9m3y1nbjrhla0k")))

(define-public crate-srtparse-0.1 (crate (name "srtparse") (vers "0.1.1") (hash "1zykqcg3ilqminkivv9iyg2bzjwxidqlsrqy2cqs1vwxr37zgdqn")))

(define-public crate-srtparse-0.2 (crate (name "srtparse") (vers "0.2.0") (hash "0z7l057gcqn3z2b2ghlmgv6fmms31f3zlx1ixfjvlx80s3w2kps7")))

