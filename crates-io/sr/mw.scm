(define-module (crates-io sr mw) #:use-module (crates-io))

(define-public crate-srmw-0.1 (crate (name "srmw") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "genawaiter") (req "^0.2") (features (quote ("futures03"))) (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "01k93qv1nvbilxj0p5kfma0vzds10izl7bgnkg4z12x9qcqq07rr")))

(define-public crate-srmw-0.1 (crate (name "srmw") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "genawaiter") (req "^0.2") (features (quote ("futures03"))) (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1hksfp5pac26ssvml11nyj278c2frrxrcp06y9d98mcmdp1xhm7m")))

