(define-module (crates-io sr lo) #:use-module (crates-io))

(define-public crate-srlog-0.1 (crate (name "srlog") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "leon") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0m62zn82z7n7yxlpxw304f5vwhlq0aii03s66ys0mp8wvqh6x0l6")))

