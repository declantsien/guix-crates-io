(define-module (crates-io sr es) #:use-module (crates-io))

(define-public crate-srestricted-0.1 (crate (name "srestricted") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0s6fi4isgi4c962r1c9jagmjabrqsd33xp4g48yvwr26ab33d8j1") (features (quote (("std" "alloc" "serde/std") ("impl_serde" "serde") ("default" "std" "impl_serde") ("alloc" "serde/alloc"))))))

(define-public crate-srestricted-0.2 (crate (name "srestricted") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "100wsnl3hbhfm2vmq87hc1h52rfi2605p41isr3l7j5qpwldrzy5") (features (quote (("std" "alloc" "serde/std") ("impl_serde" "serde") ("default" "std" "impl_serde") ("alloc" "serde/alloc"))))))

(define-public crate-srestricted-0.3 (crate (name "srestricted") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0gk180l6ryivwm8sf1dzf7x65k5h2zrbjm1s0kbxds9xspwpd5ld") (features (quote (("std" "alloc" "serde/std") ("impl_serde" "serde") ("default" "std" "impl_serde") ("alloc" "serde/alloc"))))))

