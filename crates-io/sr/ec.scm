(define-module (crates-io sr ec) #:use-module (crates-io))

(define-public crate-srec-0.1 (crate (name "srec") (vers "0.1.0") (hash "1pywghjvr1vg2gshfm3bah8m28kx4qyahffzzdnrcd723pm4kyaz")))

(define-public crate-srec-0.1 (crate (name "srec") (vers "0.1.1") (hash "02pn6d5705m8v5819nahacypmhqzk7ggzh19mfcbkqhgpg8ncf85")))

(define-public crate-srec-0.1 (crate (name "srec") (vers "0.1.2") (hash "10mdg8py3y1ahqbni4k22mffjwq9hd21n5k3nwsq37ixfkimz7qa")))

(define-public crate-srec-0.2 (crate (name "srec") (vers "0.2.0") (hash "1yxd0crpsgpn3z7l9x6pm6h2zahgfa5xrp1w6g6y6hn2ir9s1hqp")))

