(define-module (crates-io sr ce) #:use-module (crates-io))

(define-public crate-srce-0.1 (crate (name "srce") (vers "0.1.0") (deps (list (crate-dep (name "generativity") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "qcell") (req "^0.5.2") (features (quote ("generativity"))) (kind 0)) (crate-dep (name "selfref") (req "^0.4") (default-features #t) (kind 0)))) (hash "16agqxl3vx1vq42r540iq4vsap2lnxcbmlarjnfypr1z8ka8yh94")))

(define-public crate-srce-0.1 (crate (name "srce") (vers "0.1.1") (deps (list (crate-dep (name "selfref") (req "^0.4.1") (features (quote ("qcell"))) (default-features #t) (kind 0)) (crate-dep (name "qcell") (req "^0.5.2") (default-features #t) (kind 2)))) (hash "08ybkba9qrq5c2vfg72ndkrn1wk5z4zpdz2fhrh5p21qsi2lagdb")))

(define-public crate-srcerr-0.1 (crate (name "srcerr") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1bh2j2iqji5fmsbia0v1yb6hi8y6ycv7llicbq7dnqjzj3r60h1y") (features (quote (("default" "ansi_color") ("ansi_color" "ansi_term"))))))

(define-public crate-srcerr-0.2 (crate (name "srcerr") (vers "0.2.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1wpdhi2a5qks4rsnhggnvx43fbr5lsw27rf76zfbj2ql75y2k21p") (features (quote (("serialization" "codespan-reporting/serialization"))))))

(define-public crate-srcerr-0.3 (crate (name "srcerr") (vers "0.3.0") (deps (list (crate-dep (name "codespan") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1ny790cnbzx2wq5d30yr03lm91h29953xi2124yv9mmk4jfjn8a3") (features (quote (("serialization" "codespan-reporting/serialization"))))))

(define-public crate-srcerr-0.4 (crate (name "srcerr") (vers "0.4.0") (deps (list (crate-dep (name "codespan") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "01wn25l0hzbmk155fmbkk06bbpg28fry83sdvsqx9lzlychs58qx") (features (quote (("serialization" "codespan-reporting/serialization"))))))

