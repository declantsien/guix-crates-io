(define-module (crates-io sr -s) #:use-module (crates-io))

(define-public crate-sr-std-1 (crate (name "sr-std") (vers "1.0.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0vim9x359rj91kbrgq8bbgyxvqj4hv516yvmjfv4jwi5dkxmbvbv") (features (quote (("strict") ("std") ("nightly") ("default" "std"))))))

(define-public crate-sr-std-1 (crate (name "sr-std") (vers "1.0.1") (deps (list (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0j9slmhaxqdvh80sdqzia31mys9iz0phlr21kyczf68dyr3rz6k4") (features (quote (("strict") ("std") ("nightly") ("default" "std"))))))

