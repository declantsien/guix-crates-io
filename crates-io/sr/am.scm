(define-module (crates-io sr am) #:use-module (crates-io))

(define-public crate-sram22-0.0.1 (crate (name "sram22") (vers "0.0.1") (hash "1c1xnnbxlqci9m1wzb8aa26wccg3l2x6n92k4pmgqrz3aych8db1")))

(define-public crate-sram23x-0.0.0 (crate (name "sram23x") (vers "0.0.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "1kinqq61l5kyni7kyd0hn1arx6pniq13ml45czm5llb9ylk1d8pw")))

(define-public crate-sram23x-0.1 (crate (name "sram23x") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "09cbi727i6ww3v6f179xgm899p6ymkz4ynpbffdxxc6qvf63b0sr")))

(define-public crate-sram23x-0.2 (crate (name "sram23x") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "0gjfsawyw10li4sfk877dcmh914pls2n3r803d0jvl5llrf5jjxf")))

(define-public crate-sram23x-0.2 (crate (name "sram23x") (vers "0.2.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "0sn2wmsqk0zknjqid85kag0vz3pp21nbb6r1q0nllb476aikyqj4")))

(define-public crate-sram23x-0.2 (crate (name "sram23x") (vers "0.2.2") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "044bnr8hsp7s4j90wvwy16nhxhnvaja8vwgawx05n6ywyy3rwahy")))

(define-public crate-sram23x-0.3 (crate (name "sram23x") (vers "0.3.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "01jvk50cv102j66c5hhbzjqmkrpp9b7ac6r7wnq0r1qshzy94icf")))

(define-public crate-sram23x-0.3 (crate (name "sram23x") (vers "0.3.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "04zalaihmf1spl9vak80l5qd74x7zns2y5wi0j0l1bzh3i973xbc")))

