(define-module (crates-io sr ws) #:use-module (crates-io))

(define-public crate-srws-0.1 (crate (name "srws") (vers "0.1.0") (hash "14qwnvdc788kz224yyq8m6w9vj38ijp65xzcb8nhqfbrg6vcbyz7")))

(define-public crate-srws-0.1 (crate (name "srws") (vers "0.1.1") (hash "10fgr7jpvv5mrn0b8ya7bc8b1vrwq1k782jqs09i3nyk1izsjx2q")))

(define-public crate-srws-0.1 (crate (name "srws") (vers "0.1.2") (hash "1g3jx4jjvbqlxars1nnss608df2r5cpiv3nl68hglmwmlmlxm3pp")))

(define-public crate-srws-0.2 (crate (name "srws") (vers "0.2.2") (deps (list (crate-dep (name "readconfig") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "19cwpg3x165s16g4rfjzxj9rz96swh8h706jac3dwpxyaysfmr8s")))

(define-public crate-srws-0.2 (crate (name "srws") (vers "0.2.3") (deps (list (crate-dep (name "readconfig") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0vpfkmah9dp3r6xd4lkc968bg8xr5v123vj63kghpq8xg8zcb2k0")))

