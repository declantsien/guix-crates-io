(define-module (crates-io sr ix) #:use-module (crates-io))

(define-public crate-srix4k-0.1 (crate (name "srix4k") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nfc1") (req "^0.5") (kind 0)))) (hash "0gcr0ncrlb37w5fx0znwxpmf6xlsr252n2xyvwhr52vlhwd795b2")))

