(define-module (crates-io sr an) #:use-module (crates-io))

(define-public crate-srand-0.1 (crate (name "srand") (vers "0.1.0") (hash "1fd0nb4p466rw5rw7pj212lz9f16f24lnh1jxa51vg85p01kylqh")))

(define-public crate-srand-0.2 (crate (name "srand") (vers "0.2.0") (hash "173i5iakmm4rqr9fr8s8n5icqdrsf62j5kd3mbpjya29r5ksk98n")))

(define-public crate-srand-0.2 (crate (name "srand") (vers "0.2.1") (hash "0lycp7pnsvsvc1aqjzrdp9251b8956pxma462wnh4nwwml9k8jsy")))

(define-public crate-srand-0.3 (crate (name "srand") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "17zl7qw711yyvwh25cq8rni07xf67lavlmlz686m64lcy0xma95k")))

(define-public crate-srand-0.3 (crate (name "srand") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0fm3glh31jxpza97khn88pfz5737cfyqmkapzzwm8wpc7gclsq9b")))

(define-public crate-srand-0.3 (crate (name "srand") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1yf3w4kphx4rpghkv3016p8b03xr4vpxgksab28flyxbhmvxsql9")))

(define-public crate-srand-0.4 (crate (name "srand") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "140v8f177cfn8fizaw1bwkr5f549pyk8ba6hwmh982xhv835qjxz")))

(define-public crate-srando-0.1 (crate (name "srando") (vers "0.1.0") (hash "0ybrxh7w012k908prhc5f4nkg5301i3g3zmqxzzrvh7n7kdn8l49")))

(define-public crate-srando-1 (crate (name "srando") (vers "1.0.0") (hash "1ahgwc2f4j9zgq4d4lnzq1q57g61y61qn4z5z41qz7vxdlqhaqnm") (yanked #t)))

(define-public crate-srando-0.2 (crate (name "srando") (vers "0.2.0") (hash "0nxh7140njgisw1ckim8dc0jsf7wkgqli7xdc3pw30jildcpqsg5")))

