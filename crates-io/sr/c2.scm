(define-module (crates-io sr c2) #:use-module (crates-io))

(define-public crate-src20_sdk-0.0.1 (crate (name "src20_sdk") (vers "0.0.1") (deps (list (crate-dep (name "fuels") (req "^0.43.0") (features (quote ("fuel-core-lib"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "1qfvkcgf4w62cy65dw719iky10hnd41ari6qpd21pfakiby1nxnc")))

(define-public crate-src20_sdk-0.0.2 (crate (name "src20_sdk") (vers "0.0.2") (deps (list (crate-dep (name "fuels") (req "^0.43.0") (features (quote ("fuel-core-lib"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0gdfkph94his2slxr32hkg70a71339jk9sv427y5wdcazrrrkx0h")))

(define-public crate-src20_sdk-0.0.3 (crate (name "src20_sdk") (vers "0.0.3") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "fuels") (req "^0.41.1") (features (quote ("fuel-core-lib"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "1080gixvjg30nf667gw9s7ma3igadk135b5gg0m98sv8ykk5y5wy")))

