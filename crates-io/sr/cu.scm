(define-module (crates-io sr cu) #:use-module (crates-io))

(define-public crate-srcu-0.1 (crate (name "srcu") (vers "0.1.0") (deps (list (crate-dep (name "barrier") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08j2xsfkzrig5ysfyhz9kblj7simdz26nwam0id90sn97zfb1j9h") (rust-version "1.73")))

