(define-module (crates-io sr c-) #:use-module (crates-io))

(define-public crate-src-dst-clarifier-0.1 (crate (name "src-dst-clarifier") (vers "0.1.0") (deps (list (crate-dep (name "kalavor") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0gsfzpbxivmsy678phs0lxb301slx3jb1c6sq7bj0sqdf275jkr5") (yanked #t)))

(define-public crate-src-dst-clarifier-0.2 (crate (name "src-dst-clarifier") (vers "0.2.0") (deps (list (crate-dep (name "kalavor") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0vpqamm7fhgkvb23xayiqs2zf6jknxbbxcf9cwsjfidwgspf0gsd")))

(define-public crate-src-graph-0.1 (crate (name "src-graph") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.12") (default-features #t) (kind 0)))) (hash "1lnkn54y7v70nr8qb7gygg6pm5xqzham1x1fk269bi352pprm2ls") (yanked #t)))

(define-public crate-src-graph-0.1 (crate (name "src-graph") (vers "0.1.1") (deps (list (crate-dep (name "rustc-ap-graphviz") (req "^662.0.0") (default-features #t) (kind 0)))) (hash "1irvnz2isch3qz6j06awcgrdaxd4xz4ks01ln3adrdfx2ql8xn4b") (yanked #t)))

(define-public crate-src-graph-0.1 (crate (name "src-graph") (vers "0.1.2") (deps (list (crate-dep (name "rustc-ap-graphviz") (req "^662.0.0") (default-features #t) (kind 0)))) (hash "1vwz0p1yf0ss7szn73ww47m6m5mdwdgsng1x1l1zhagzzq343npr") (yanked #t)))

(define-public crate-src-graph-0.1 (crate (name "src-graph") (vers "0.1.3") (deps (list (crate-dep (name "rustc-ap-graphviz") (req "^662.0.0") (default-features #t) (kind 0)))) (hash "157flkb9r8mlqv4yiwcdj7nlzdysn14vpj1mv3r1svmcag0f971i") (yanked #t)))

(define-public crate-src-graph-0.1 (crate (name "src-graph") (vers "0.1.4") (deps (list (crate-dep (name "rustc-ap-graphviz") (req "^662.0.0") (default-features #t) (kind 0)))) (hash "0s4xqaxxpjd9gi0h9qmkvd53gp9xnj7300mgf87vkvk9dz36azsg")))

