(define-module (crates-io sr mc) #:use-module (crates-io))

(define-public crate-srmc-0.1 (crate (name "srmc") (vers "0.1.0") (hash "1svqj8vjmbkrfknjd7k6fj2z32lhhwsyx600fra72hgag5dvdy4k")))

(define-public crate-srmc-0.1 (crate (name "srmc") (vers "0.1.1") (hash "1ajhq8djnjbkscam9zkpi4zn3rdxmc5lm7fcjy22mlac7iqccs8k")))

(define-public crate-srmc-0.1 (crate (name "srmc") (vers "0.1.2") (hash "14q629k6jy9kbx6zrs4xfrlwa8i7pahjgp7fxjgsiin53x1z9ggc")))

(define-public crate-srmc-0.1 (crate (name "srmc") (vers "0.1.3") (hash "0n74ngc1zkr4liwbys0pagi2wrbxdfrd0dy98d5h9bm4rakr3di4")))

