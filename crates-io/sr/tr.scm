(define-module (crates-io sr tr) #:use-module (crates-io))

(define-public crate-srtree-0.1 (crate (name "srtree") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "03vq84ppkhqc5ivvsmsnfhg4cnrzg0yzkbbzf48aiswanw7ha8sf") (rust-version "1.63")))

(define-public crate-srtree-0.2 (crate (name "srtree") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1b0dbadf1g4fmpvmbxkvxlgd0r8v3i7dgcnfa59icwir3pg5gxxy") (rust-version "1.63")))

(define-public crate-srtree-0.2 (crate (name "srtree") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0b640gg47rzj7ppphhvnmm5r3wdb5nmjbvws3ifn3zyzvnbrz3q6") (rust-version "1.63")))

(define-public crate-srtresync-0.1 (crate (name "srtresync") (vers "0.1.0") (hash "0alzsj76yzfrslfvmbznwbw5ks3cia3l24f6rw6g2v5a9gfbraq0")))

(define-public crate-srtresync-0.1 (crate (name "srtresync") (vers "0.1.1") (hash "1f0rmq7z8qiap4hskslgbzxhapqw3ypma28lp48x6czc0sj8l4rg")))

