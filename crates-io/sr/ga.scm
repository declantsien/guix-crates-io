(define-module (crates-io sr ga) #:use-module (crates-io))

(define-public crate-srgat-0.1 (crate (name "srgat") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "10f9i15s1iq5rkvnfqag1wkk8abzgv152m2p9lrk7ifviyjwrmvf")))

