(define-module (crates-io sr cc) #:use-module (crates-io))

(define-public crate-srcconsole-0.1 (crate (name "srcconsole") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("tlhelp32" "winnt" "minwindef" "processthreadsapi" "psapi" "errhandlingapi" "memoryapi" "winbase" "synchapi" "handleapi" "minwinbase"))) (default-features #t) (kind 0)))) (hash "0y5bzra9r3d2k24lpjfz7dyxddlggs9shfxww6r5x07dwpzspnif")))

(define-public crate-srcconsole-0.2 (crate (name "srcconsole") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("tlhelp32" "winnt" "minwindef" "processthreadsapi" "psapi" "errhandlingapi" "memoryapi" "winbase" "synchapi" "handleapi" "minwinbase"))) (default-features #t) (kind 0)))) (hash "11510wgavbffxl57isc869l0d8v25q0hihkr5al95k0nsm16z9r3")))

