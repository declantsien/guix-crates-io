(define-module (crates-io sr vz) #:use-module (crates-io))

(define-public crate-srvzio-1 (crate (name "srvzio") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.1") (features (quote ("termination"))) (default-features #t) (kind 0)))) (hash "13qwgh0iybypqsq122i05bpwlx506wnxfyi5690pypaayya7k9fi")))

(define-public crate-srvzio-1 (crate (name "srvzio") (vers "1.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.1") (features (quote ("termination"))) (default-features #t) (kind 0)))) (hash "0hwbkp6kcnxn3fjb2sx54457dggn1sj2dvjklzmsi5g660wy7yb2")))

(define-public crate-srvzio-1 (crate (name "srvzio") (vers "1.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.2") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0mj1zjr8vs89sjc5zhwgqjph0mjhzk3byj71dbacgmylsn8hli8g")))

(define-public crate-srvzio-1 (crate (name "srvzio") (vers "1.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.3") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1mska63gy7p0bgv0isyifss6qdbflvw3cprcybwdh2rinx6w9zhc")))

