(define-module (crates-io sr tl) #:use-module (crates-io))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "15prf7r8hjq94wc35yywvj7aw31vnybax3cr4jm7jzmc55w35d1w")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "1lsxzx321vg0rqm65zda1y0q1wr85jsnj09my1770sim8bk79d8h")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.2") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "1ir2asy125imapgj78fwc8sc4g9sqsypswrdf93fbipvcqx2fwd2")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.3") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "1qvs04ai1v7929i2n9p8ydz8c6g64wj56mra0zbxcswcrcsskg4v")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.4") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "0paba9isf8cvp714c5asn4l23c767641azrszq7yx31q2040d5ii")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.5") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "0c7726hikacmw594cqzjzrzlnl2nhjivyk7w29zfx91rqy4mlp8y")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.6") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "0pnmad3h2bj1yplc0is7ckw7l1z1bvpz7fdk1ymhyvw21ixjqv2n")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.7") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "0z5h82swnslgl69y41ayar85gsmad74hlfi9awp1170kd0s867jz")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.8") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "1k03m3cgqq63fxbn61p6kvgv1llkfaj60i8vv2kkhjvd2d3h0qby")))

(define-public crate-srtlib-0.1 (crate (name "srtlib") (vers "0.1.9") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)))) (hash "1w23z27x9z6aa8ql37gp2wyqyhgwqbk6lk9a5dcyi6iznpq57rr7")))

