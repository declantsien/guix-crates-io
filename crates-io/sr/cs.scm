(define-module (crates-io sr cs) #:use-module (crates-io))

(define-public crate-srcsrv-0.1 (crate (name "srcsrv") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0jii9mhqv307ask7plx6q50ndims5r40pjbipqi8h4padr5h0if6")))

(define-public crate-srcsrv-0.2 (crate (name "srcsrv") (vers "0.2.0") (deps (list (crate-dep (name "memchr") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "pdb") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mkpphnr2cfpd4vrnhfvp2b2q9q8yj2kjdsysjb1wjpz5ly7rabr")))

(define-public crate-srcsrv-0.2 (crate (name "srcsrv") (vers "0.2.1") (deps (list (crate-dep (name "memchr") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "pdb") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "18n05xk7d4z3m4vi96pdahbhjz4ggigwrcccxjxq5nxg5r4f127s")))

(define-public crate-srcsrv-0.2 (crate (name "srcsrv") (vers "0.2.2") (deps (list (crate-dep (name "memchr") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "pdb") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w5cx028r4yfcmnvazp9mss9qqlxb0ixkc9cjnqljbjj1skkf902")))

