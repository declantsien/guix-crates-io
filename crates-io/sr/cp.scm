(define-module (crates-io sr cp) #:use-module (crates-io))

(define-public crate-srcpos-1 (crate (name "srcpos") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "19dzsqins9s1pyz893j1vh51fjsarvqrb3c5xmll6qzi90nbd4rn") (features (quote (("default" "serde"))))))

(define-public crate-srcpos-1 (crate (name "srcpos") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "10bh5i7r56al8s9lah62wsfskfz39dhrk0d9q3q002vhcf6mn3b6") (features (quote (("default" "serde"))))))

(define-public crate-srcpos-1 (crate (name "srcpos") (vers "1.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0gzqajx5qbcxlj46aq8q4chnjs3slrgrad64k379c3w0q2vng7gf") (features (quote (("default" "serde"))))))

(define-public crate-srcpos_get-1 (crate (name "srcpos_get") (vers "1.0.0") (deps (list (crate-dep (name "srcpos") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "srcpos_get_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0aylybgy24jxgjrf6n8klywacfmyxaxn1dk6qv0d2nl64imyxvdj") (features (quote (("derive" "srcpos_get_derive") ("default" "derive"))))))

(define-public crate-srcpos_get-1 (crate (name "srcpos_get") (vers "1.1.0") (deps (list (crate-dep (name "srcpos") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "srcpos_get_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1kgqzv53gm46m94yvmjjwwh63p33rm3ssjk7nlaj10fav72l09sp") (features (quote (("derive" "srcpos_get_derive") ("default" "derive"))))))

(define-public crate-srcpos_get-1 (crate (name "srcpos_get") (vers "1.1.1") (deps (list (crate-dep (name "srcpos") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "srcpos_get_derive") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "17kcfpg3ihq5a9d2ihbrrcika9b0y15pd8j7z3m6knkk6ad0a50p") (features (quote (("derive" "srcpos_get_derive") ("default" "derive"))))))

(define-public crate-srcpos_get_derive-1 (crate (name "srcpos_get_derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "srcpos") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "03wv1x78sksfbnmk1h990kdpdwiql247prwypfchppxvxn3ay6nj")))

(define-public crate-srcpos_get_derive-1 (crate (name "srcpos_get_derive") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "srcpos") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "16ydaw3x504hd73ywhqzls5d6c47ccbc3dc0b6b8hj2z7dfm9ii7")))

(define-public crate-srcpos_get_derive-1 (crate (name "srcpos_get_derive") (vers "1.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "srcpos") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lwbmj0bghlhs4llxj14c5qjniqrpmf71v8d6zsnkvp8npaswc1x")))

