(define-module (crates-io sr lp) #:use-module (crates-io))

(define-public crate-srlp-0.2 (crate (name "srlp") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "elastic-array") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.0") (kind 0)) (crate-dep (name "s-types") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0757p7rj4239z9lsavz6kzr7xc6x1bgh8nhanda4xnmh6bfair29") (features (quote (("sophon" "s-types") ("default" "sophon"))))))

(define-public crate-srlp-0.3 (crate (name "srlp") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.0") (kind 0)) (crate-dep (name "s-types") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1xqck9nx5b7081vamv02j9jiag7344dqqi03hkirihi7kl6igra6") (features (quote (("sophon" "s-types") ("default" "sophon"))))))

