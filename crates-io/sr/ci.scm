(define-module (crates-io sr ci) #:use-module (crates-io))

(define-public crate-srcinfo-0.1 (crate (name "srcinfo") (vers "0.1.0") (hash "08s9ap296xhsycalsk2kgrk1f12bl5h3ylgqfwal9pqham7mdfis")))

(define-public crate-srcinfo-0.1 (crate (name "srcinfo") (vers "0.1.1") (hash "0vp2ilmw2a5gxv8yva2pja5rgchhygvn4bqir8agha8m5i0fbxjk")))

(define-public crate-srcinfo-0.2 (crate (name "srcinfo") (vers "0.2.0") (hash "02327g6j6hwa9v9hylrlwh4l31wghmhpdswjdg44d3jxsxips1fc")))

(define-public crate-srcinfo-0.3 (crate (name "srcinfo") (vers "0.3.0") (hash "1lsa3lrkyrbyl9167wjgjqb5saxjz7ymimj2swmvxxi8pxsvnqdh")))

(define-public crate-srcinfo-0.3 (crate (name "srcinfo") (vers "0.3.1") (hash "100ibrp0j9pbi28jjc3yclslzw1npih6x97c95a4pnmlr2x598dy")))

(define-public crate-srcinfo-0.3 (crate (name "srcinfo") (vers "0.3.2") (hash "1qjggfi7gc04n2fd0n45nk36hl3wavvdkkknqbhcsiql4d5p3i9d")))

(define-public crate-srcinfo-1 (crate (name "srcinfo") (vers "1.0.0") (hash "06jpld0qc7g45r3621pwnary2nigm1a5z3fa1x806226i6757756")))

(define-public crate-srcinfo-1 (crate (name "srcinfo") (vers "1.1.0") (hash "06bmswzjj6hjh3jw4dl69w5v75z7czjf6pclcm9smqs9i7af1bnf")))

