(define-module (crates-io sr e-) #:use-module (crates-io))

(define-public crate-sre-engine-0.1 (crate (name "sre-engine") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "0v7sq4bd4mdxsq7vcn0zwkd5cy06x3v91sg9pf6wc77gf8xwjnj0")))

(define-public crate-sre-engine-0.1 (crate (name "sre-engine") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "0rf2fybjdf4gs90mjlvyhcymksl5kgg6lydw923627xpg8zqfls9")))

(define-public crate-sre-engine-0.1 (crate (name "sre-engine") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "1h8zd0qj37p825g4yxqkhb43lqj1c1zwnwy79gnlya3w52cj71z5")))

(define-public crate-sre-engine-0.2 (crate (name "sre-engine") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "00qsyld8fzrq62a1d2i97jrri1hanwbkqblp1ffp7234xkq87qjm")))

(define-public crate-sre-engine-0.2 (crate (name "sre-engine") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "091bf626q15ickp369jcz60fraszi54y9w9yi6hwvjwa568aj08l")))

(define-public crate-sre-engine-0.3 (crate (name "sre-engine") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "1r2vsx02npaarx5agbhhgbjb18fq54am7hpp27h01xadql0ijiia")))

(define-public crate-sre-engine-0.4 (crate (name "sre-engine") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)))) (hash "0a1dmk0y244d1dsibwd05lnynxg7cx6qxvp7ynkaknrmdk2cb454")))

(define-public crate-sre-engine-0.4 (crate (name "sre-engine") (vers "0.4.2") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)))) (hash "12k2z2jiz6lra51c4qa6h7xbyhr5axmd9vjhni8cl47bbiqa83dw") (yanked #t)))

(define-public crate-sre-engine-0.4 (crate (name "sre-engine") (vers "0.4.3") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)))) (hash "00iihj2429j3k5nqn7wqacc8w4pjmr2s5s6j69amvn6lrflfc80i")))

