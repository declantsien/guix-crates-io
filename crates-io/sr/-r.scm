(define-module (crates-io sr -r) #:use-module (crates-io))

(define-public crate-sr-rcd-0.5 (crate (name "sr-rcd") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "04f805d1bznlcs9zyhvk4kc0jwbxam9vx6x9mj9v9czzkac7k6wy")))

(define-public crate-sr-rcd-0.5 (crate (name "sr-rcd") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "07pysfp1fm05mga9xjana310iv73y5sr69zkg12davvz3sk2c9xl")))

(define-public crate-sr-rcd-0.5 (crate (name "sr-rcd") (vers "0.5.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "15vwy12mvk05f82670lxg8v1g0qv7rv80mpwl6np3j8g251xqdjc")))

(define-public crate-sr-rcd-0.6 (crate (name "sr-rcd") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "0pzgg5pk1vfijpzcya6jh5rnx8mxvcr3ahr84g0w4zygda9lbxps")))

