(define-module (crates-io sr cf) #:use-module (crates-io))

(define-public crate-srcfiles-0.1 (crate (name "srcfiles") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "visit"))) (default-features #t) (kind 0)))) (hash "1y0m9gfbwcly7a0vhk52b5r454kv2b4aa0wxjxvnlzb214dmhy4p")))

