(define-module (crates-io ri di) #:use-module (crates-io))

(define-public crate-ridicule-0.1 (crate (name "ridicule") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "155b6639iwzqxcbp80a1w2j56aivc3hmxqbrhfms12ly8c9vn1m9") (yanked #t)))

(define-public crate-ridicule-0.2 (crate (name "ridicule") (vers "0.2.0") (deps (list (crate-dep (name "predicates") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "ridicule-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0lc4zqbn06187ndqc677ic07kn5hysd8lhlxxbikvwn20yzkq135") (yanked #t)))

(define-public crate-ridicule-0.3 (crate (name "ridicule") (vers "0.3.0") (deps (list (crate-dep (name "predicates") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "ridicule-macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19a6qza7hjq5b12k6n79mdnng53jbd3vmg2j75xxa8mpmy4d602c")))

(define-public crate-ridicule-macros-0.2 (crate (name "ridicule-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "printing" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "ridicule") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0fgjyl1z934nkpb5xqcy7svk98hw2hp6gvlric9vjwnqgh4z26nh") (yanked #t)))

(define-public crate-ridicule-macros-0.3 (crate (name "ridicule-macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "printing" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "ridicule") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1symz1apxn4qbry30dfbhmqmvrn5ydr72lnl28p46mfbkwf13lz6")))

