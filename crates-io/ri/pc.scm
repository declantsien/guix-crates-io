(define-module (crates-io ri pc) #:use-module (crates-io))

(define-public crate-ripc-0.1 (crate (name "ripc") (vers "0.1.0") (hash "1ipnhrixzndkpi0z95m49kv2l4w7v822p3xq3v5ih202ml0mh8dw")))

(define-public crate-ripcal-0.1 (crate (name "ripcal") (vers "0.1.0") (hash "1r7gri1i5iipzi6zbyiy4z7hivqn1kfrahm2n3hwxfcixawlsj2r") (yanked #t)))

(define-public crate-ripcal-0.2 (crate (name "ripcal") (vers "0.2.0") (hash "1h02dcg17lx3jfakjsqxnwani450i6da22hsfaybk2acq20q2kbz") (yanked #t)))

(define-public crate-ripcal-0.2 (crate (name "ripcal") (vers "0.2.1") (hash "1ljmv9nmabygdybh5n6nabddyhj4pp3hmspp387h2nklm4b2fpvp") (yanked #t)))

(define-public crate-ripcal-0.3 (crate (name "ripcal") (vers "0.3.0") (hash "1r5gichms65q1v6cjp21y7k8i7kxaqlg0nxmfzi79vnzww368mxa") (yanked #t)))

(define-public crate-ripcal-0.4 (crate (name "ripcal") (vers "0.4.0") (hash "1ppvnx1grk4q1d3hrm2amz3ryrnf31mbfpsvmvfh8yhk3ymwprr9")))

(define-public crate-ripcal-0.5 (crate (name "ripcal") (vers "0.5.0") (hash "0387h4dzrg8mga0lnsdbkyid44fdv3gw3ba4z2348zbrrfj0jyil") (yanked #t)))

(define-public crate-ripcal-0.5 (crate (name "ripcal") (vers "0.5.1") (hash "1xi49h1sjrr2mm332mcs1ly941rpb9ah0j91iy1qdad1w3ssnz43") (yanked #t)))

(define-public crate-ripcal-0.5 (crate (name "ripcal") (vers "0.5.2") (hash "1832clx1q4a5ilk8z0lfxqd0j960wj7l371pgqb18jsk225dkvnp")))

(define-public crate-ripcal-0.6 (crate (name "ripcal") (vers "0.6.0") (hash "0rf4qrwn7205bcf05mv7svrlnchjk67z5r3xqcgxv44phs122zp9")))

(define-public crate-ripcalc-0.1 (crate (name "ripcalc") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "1dn78rbgzn8ni7b72ci97dvfs32l3fa93zzyz7b97amyhdjh92fj")))

(define-public crate-ripcalc-0.1 (crate (name "ripcalc") (vers "0.1.5") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "16036xpfqcm1arz2fxszfa52r7vf1dip82j34ivic0k96yjj3f57")))

(define-public crate-ripcalc-0.1 (crate (name "ripcalc") (vers "0.1.6") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "13gy1s6f1vp7lgx25zs8vdji3ccrd6xjqby35sf436j9lnq17c4f")))

(define-public crate-ripcalc-0.1 (crate (name "ripcalc") (vers "0.1.7") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nyrsbpb9fixlyd726k5jgcx3s6fdi7xwmihzn8k8f7yy62mimsv")))

(define-public crate-ripcalc-0.1 (crate (name "ripcalc") (vers "0.1.9") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (default-features #t) (kind 0)))) (hash "1zdy14jzvpic3wqwvxli76vy0zgkqi2f2nq0chrcwi4f1xmcclwh")))

(define-public crate-ripcalc-0.1 (crate (name "ripcalc") (vers "0.1.10") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (default-features #t) (kind 0)))) (hash "1hpcb2rkpjwpfnhavibnjfm5l577cmcpy11fyvafs2l7brvfrlvc")))

(define-public crate-ripcalc-0.1 (crate (name "ripcalc") (vers "0.1.11") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 0)))) (hash "0hxqpc10cc8qpc38y273bs0wbgilp5l3j1bjav160rvgs6v46vwv")))

(define-public crate-ripcd-0.1 (crate (name "ripcd") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1igqsyvdikzx3ffx723nl7agnka5chi2115md01sjiliax34qp5x")))

(define-public crate-ripclip-0.4 (crate (name "ripclip") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("commctrl" "winuser" "errhandlingapi" "winbase" "shellapi" "windowsx"))) (default-features #t) (kind 0)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "02yl70das6x3xb21z43vm41k6lsfh18h885dkgkcknwqanjxpnd3")))

(define-public crate-ripclip-0.5 (crate (name "ripclip") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("commctrl" "winuser" "errhandlingapi" "winbase" "shellapi" "windowsx"))) (default-features #t) (kind 0)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0xihk7bdi84xgbf9km4acqfx2a0f6h94vq9i5kb7sq6075d4jr9j")))

(define-public crate-ripclip-0.7 (crate (name "ripclip") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("commctrl" "winuser" "errhandlingapi" "winbase" "shellapi" "windowsx"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0lk2jb336sribmgz1d2md42kw36qi3wwc6s1kvyx03l0bavd901q")))

