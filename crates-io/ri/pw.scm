(define-module (crates-io ri pw) #:use-module (crates-io))

(define-public crate-ripwatch-0.1 (crate (name "ripwatch") (vers "0.1.0") (hash "0lkikiyhssbfzghhlfxnc9lv8frb3z22byh02y8g6n8bp6wbsd4i")))

(define-public crate-ripwatch-0.1 (crate (name "ripwatch") (vers "0.1.1") (hash "092zfq3w47wg1548lgbf68ypnyqcipafmsgaknc7587mdbzmx2yy")))

(define-public crate-ripwatch-0.1 (crate (name "ripwatch") (vers "0.1.2") (hash "0zmh9phw1cfvjbshmq7k0v2h869drvmdpiac8dxw74hn9rmxmicz")))

(define-public crate-ripwc-0.1 (crate (name "ripwc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1i31l6d7lin2fdjlapq992zca8izh254cspj6vq5zwpw9zlnc282")))

(define-public crate-ripwc-0.1 (crate (name "ripwc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0x3xwf080dz03a44a76h18ilh95i56zhrjnvgj9i87v82i47mppx")))

