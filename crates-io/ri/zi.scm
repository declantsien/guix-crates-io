(define-module (crates-io ri zi) #:use-module (crates-io))

(define-public crate-rizin-rs-0.1 (crate (name "rizin-rs") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "1wa9a4j7jg8mnada2wva3y3wq6h5wpp34f3pryxpi5qrmmcygdb4") (yanked #t)))

(define-public crate-rizin-rs-0.1 (crate (name "rizin-rs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "1k3c33afgfb10mya2ykz6v7bwxhp2vkfm5ksb8n4khgnp2xkx21y")))

(define-public crate-rizin-rs-0.1 (crate (name "rizin-rs") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "0l01hvs7rgih3gjc997ij70iidfpg5rwzgwdmg5mnllv3i8j7ddy")))

