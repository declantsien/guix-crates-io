(define-module (crates-io ri ce) #:use-module (crates-io))

(define-public crate-rice-0.0.1 (crate (name "rice") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "129x2fqs8z89s0cy3fcj9flhnpirhhilz9j8v678bkk1c60ca6fi") (yanked #t)))

(define-public crate-ricecomp-0.1 (crate (name "ricecomp") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1az7bk1saxv7gp99ab4ijgx9mbg89w9rqxkijhn2kmwmpsdxq9r2")))

(define-public crate-ricecomp-0.1 (crate (name "ricecomp") (vers "0.1.1") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vm8fa8qwjcq76bmcl41il663s76h9nsj4mb0hl0qvpzbp3j76mz")))

(define-public crate-ricecomp-0.1 (crate (name "ricecomp") (vers "0.1.2") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1p4wwwpqq8i1rhqgfajx25qij89y1s37k1d86k8dw6w9vjp2nyr2")))

