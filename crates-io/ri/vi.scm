(define-module (crates-io ri vi) #:use-module (crates-io))

(define-public crate-rivi-loader-0.1 (crate (name "rivi-loader") (vers "0.1.0") (deps (list (crate-dep (name "ash") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 2)) (crate-dep (name "gpu-allocator") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "metal") (req "^0.23.1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "rspirv") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1k96yaam52nkzld7p64hr8qwk6va606fj84hnv37r7rl5frsm7v5")))

(define-public crate-rivi-loader-0.1 (crate (name "rivi-loader") (vers "0.1.1") (deps (list (crate-dep (name "ash") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 2)) (crate-dep (name "gpu-allocator") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "metal") (req "^0.23.1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "rspirv") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "07zir87wc5lfbkx87g6308nmm8h0bldwl0c44jg45bhilrndaxni")))

(define-public crate-rivi-loader-0.1 (crate (name "rivi-loader") (vers "0.1.2") (deps (list (crate-dep (name "ash") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 2)) (crate-dep (name "gpu-allocator") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "rspirv") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0vyl31gx54kn4dmwwk6096fdv5pd7mwhrixznqm8kiwas2b7zz12")))

(define-public crate-rivi-loader-0.1 (crate (name "rivi-loader") (vers "0.1.3") (deps (list (crate-dep (name "ash") (req "^0.33.3") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 2)) (crate-dep (name "gpu-allocator") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "rspirv") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1hchcwp15jls42284bsbc8wpk6ccnimvfyk27w7acfn8mhank85c")))

(define-public crate-rivi-loader-0.1 (crate (name "rivi-loader") (vers "0.1.4") (deps (list (crate-dep (name "ash") (req "^0.35.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 2)) (crate-dep (name "gpu-allocator") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "rspirv") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0a8xqnfaxcmwx9278hcascyb3wkgkrfjf6bp2g7kmjy9vlm87a8c")))

(define-public crate-rivi-loader-0.1 (crate (name "rivi-loader") (vers "0.1.5") (deps (list (crate-dep (name "ash") (req "^0.36.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 2)) (crate-dep (name "gpu-allocator") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "rspirv") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "05520vksgvly7w1sc15gw5958c906gm2i1s941wbvddq3sxzynpy")))

(define-public crate-rivia-0.0.1 (crate (name "rivia") (vers "0.0.1") (hash "0mlkh9bn74h457cpl4mm7zbjssi69p9wp5bjxq0k88j4cys46i4k")))

(define-public crate-rivia-0.1 (crate (name "rivia") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "01kyhwmx3gpc71lk24hfxkaf8418srik6drfy4ni72fmv3gjdzw7")))

(define-public crate-rivia-0.1 (crate (name "rivia") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ij5zpnzxz8b6ppyg7laxcv83mwxx59fi6bz03b8amkk48g2icxh")))

(define-public crate-rivia-0.1 (crate (name "rivia") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "0xxgrqz1i3a1p0al33rlvj9xld1bs21238xz28s2qigj1pma3n0a")))

(define-public crate-rivia-0.1 (crate (name "rivia") (vers "0.1.4") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "18gvi98y7sw3fn773cdbd6pa0xnhgkz9rsrrnw7x5wkipx56idp0")))

(define-public crate-rivia-0.1 (crate (name "rivia") (vers "0.1.5") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "0minixyv163f53lhqqgqk5avpsblhxlm6frh5cbv5q7j4wzlcx3v")))

(define-public crate-rivia-0.2 (crate (name "rivia") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "1vd2n2j4nyrk7dnh1npnrd20465dl7lfdzs4mszkqn8kr6fyr9s5")))

(define-public crate-rivia-0.2 (crate (name "rivia") (vers "0.2.2") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "03p0b771x27lrja3vgdsrv10xniij72x8daqh2agdnwp811fq83a")))

(define-public crate-rivia-0.2 (crate (name "rivia") (vers "0.2.4") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "1zy89mv007zcxk8c71fhs64ybzmzgp7lxfqxawghm0v61hg2rqdl")))

(define-public crate-rivia-0.2 (crate (name "rivia") (vers "0.2.5") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "12725bv9ky4ihkcx8kizyhx57f831v9lq7xqqc20wcb8gh384ys4")))

(define-public crate-rivia-0.2 (crate (name "rivia") (vers "0.2.6") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "0hsn2shphswcysl61zgrwf7m8hxab2vvhicsbiyy6m05cm83vhnk")))

(define-public crate-rivia-vfs-0.0.1 (crate (name "rivia-vfs") (vers "0.0.1") (hash "08zbiz7y542h3iy85kgn6738pknrx0rdmylgikvl14qc3md90pk7")))

(define-public crate-rivia-vfs-0.1 (crate (name "rivia-vfs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wfn4w0wgsw59w2019ah3agphr8x1j8vbk72608905y08i11mbh3") (yanked #t)))

(define-public crate-rivia-vfs-0.1 (crate (name "rivia-vfs") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fqlchkpkka78hmi9385x9nhx5fs9hyfvx1l09kz7rygjllcf4bk") (yanked #t)))

(define-public crate-rivia-vfs-0.1 (crate (name "rivia-vfs") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w0zb2gb9wc0jdjayxrp5s95jm4nxp61xjvqfd38spjb4p5rwlkx") (yanked #t)))

(define-public crate-rivia-vfs-0.1 (crate (name "rivia-vfs") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1givx1whffal6va2b1wfpxndsg0zkpfwc627b3lxmjda245pciwx")))

(define-public crate-rivia-vfs-0.1 (crate (name "rivia-vfs") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1chis6fg9qsabch0nh6l0h2mq5y9m4g7km6y81a0ww2p2zk45in4")))

(define-public crate-rivia-vfs-0.2 (crate (name "rivia-vfs") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1dyj4h34ybafnx4lcf1h4j2lxkqr60kzpd6h61rxxdyaqf1b1zay")))

(define-public crate-rivia-vfs-0.2 (crate (name "rivia-vfs") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0yn36igqxvwhpqjsslj9nrb94cjvj4zy0ma5dh96g5411ki5lf43")))

(define-public crate-rivia-vfs-0.2 (crate (name "rivia-vfs") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1gg9b3n7bh956jffpwnx859ikawrwpql9b84q4vy6bvxsi5999dm")))

(define-public crate-rivia-vfs-0.2 (crate (name "rivia-vfs") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0r9vg3c1k0kfx1cbr9g0qyqyx5p8havw22pjsiar0iwwya46hv4k")))

(define-public crate-rivia-vfs-0.2 (crate (name "rivia-vfs") (vers "0.2.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rivia") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "04fjdi9pd419z8nylylb78a14kmnklhzzdv5wvmzy7br9k87hd12")))

(define-public crate-rivian-0.1 (crate (name "rivian") (vers "0.1.0") (hash "0x0ghiddnvb6kysy0a9pv71pn611qgdybp46bx8538gld9k9vc2m")))

