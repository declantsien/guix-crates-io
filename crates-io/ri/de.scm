(define-module (crates-io ri de) #:use-module (crates-io))

(define-public crate-rider-0.1 (crate (name "rider") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync"))) (default-features #t) (kind 0)))) (hash "0grxl2vgd5xd1n5a3cnn7g5izyjamaazllzkhjjyya31c4shkcri")))

(define-public crate-rider-0.1 (crate (name "rider") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "rt-multi-thread" "sync"))) (default-features #t) (kind 2)))) (hash "1fc7nmd4z300lpylqv12fc3vyf51sq4kdq4w2slma5h9i5mn6c7r")))

(define-public crate-rider-0.1 (crate (name "rider") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "rt-multi-thread" "sync"))) (default-features #t) (kind 2)))) (hash "0mw30vvglm8mqhsr1yjkhfi5cq5995xnfjb4rp78x5db9zf553pa")))

(define-public crate-rider2emacs-0.1 (crate (name "rider2emacs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "shell-escape") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "12z58nfdk5jcv06jbx3is4mszyfxx7r25xdh5jgxw6yfzgyr3m4a")))

(define-public crate-rider2emacs-0.1 (crate (name "rider2emacs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "shell-escape") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1x2g6rqqzwd672409yyc69kx6nj8w3hzd29qqrxyn4hl0nl23w5w")))

