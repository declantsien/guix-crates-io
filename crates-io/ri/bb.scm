(define-module (crates-io ri bb) #:use-module (crates-io))

(define-public crate-ribbon-0.1 (crate (name "ribbon") (vers "0.1.0") (hash "0gkzp065mj8djnp5jga0xiyv63ikcflmr3cq5ywagbgy0sa4j4ha")))

(define-public crate-ribbon-0.2 (crate (name "ribbon") (vers "0.2.0") (hash "0jc0b7fn3j5gcd6i07pz43p1w4aik7dn1cwxxab08b66nggd14zj")))

(define-public crate-ribbon-0.2 (crate (name "ribbon") (vers "0.2.1") (hash "16ra9xrp0fnx4jmi3g04hcbrv97xj2ch7jhk02x8chc36nmr3xca")))

(define-public crate-ribbon-0.2 (crate (name "ribbon") (vers "0.2.2") (hash "0nsd9gs3shzvkh4g1hnby2xpyw918q7idd4rv01d1vlj8yj51f2l")))

(define-public crate-ribbon-0.2 (crate (name "ribbon") (vers "0.2.3") (hash "0vjv3lwnib98bbm1xy434q7anhvnsyfvg0s7l1fkk6i6s8w5caqm")))

(define-public crate-ribbon-0.3 (crate (name "ribbon") (vers "0.3.0") (hash "0qhq2117cs2jm5s84ag7gn53zi8ssg32ysc257p6nkn17ynz4m0p")))

(define-public crate-ribbon-0.4 (crate (name "ribbon") (vers "0.4.0") (hash "0qpa4jgh9wl7s3mgj6sjdcck5b5sfzng2kn82s2z7fxfhym4cwr0")))

(define-public crate-ribbon-0.5 (crate (name "ribbon") (vers "0.5.0") (hash "1y0si1k3x4yc5wdjk0kdyxkn1w89glm6pway3xn6lrnahh4g14a0")))

(define-public crate-ribbon-0.6 (crate (name "ribbon") (vers "0.6.0") (hash "0lf20inxwikm038zcdvbijy67mh7qnnq7jgwa4lhwcxld31ff42x")))

(define-public crate-ribbon-0.7 (crate (name "ribbon") (vers "0.7.0") (hash "1ymlzmcf1wax9js2l3h6im10g9pfbkl5ji4d39ym29xfp5yp8l1w")))

(define-public crate-ribboncurls-0.1 (crate (name "ribboncurls") (vers "0.1.0") (deps (list (crate-dep (name "html-escape") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9.32") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.32") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1b87yhk9hwsdavqfad00rczmrdp47vqf2qcp7qhd5g3s3n7i79mm") (rust-version "1.64.0")))

(define-public crate-ribboncurls-cli-0.1 (crate (name "ribboncurls-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "ribboncurls") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.33") (default-features #t) (kind 0)))) (hash "1rkyc3ga71gsyxy970jh41r9jinfpkqyw2wa9srq3k625vfrnb3v")))

(define-public crate-ribboned-0.1 (crate (name "ribboned") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "svg2appicon") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "126wij9pf2qxpzpzcjq0s158mxsw6igp44sn05ckywfz8nrpwmdj")))

(define-public crate-ribbons-0.1 (crate (name "ribbons") (vers "0.1.0") (hash "150xs3iccfcqh9yjfl6illaqg3lb47bkqc8l2p9arqx8k8m6q2kr")))

(define-public crate-ribbons-0.1 (crate (name "ribbons") (vers "0.1.1") (hash "0n57q5qf0kxkir8b8w7x3ngazp7j53ap2865wqlawzs8z2wdlp2p")))

(define-public crate-ribbons-0.1 (crate (name "ribbons") (vers "0.1.2") (hash "0j8iggnvw7vfwyz9m7mj9xvbdp49g39smnang6wkq5qaxbwf2xaz")))

