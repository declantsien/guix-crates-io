(define-module (crates-io ri dd) #:use-module (crates-io))

(define-public crate-riddance-0.0.0 (crate (name "riddance") (vers "0.0.0") (hash "1m7jjwrkhlap605bpnjr37afrli8v9sj6dp2xr3rjy20g1cmhpqd")))

(define-public crate-riddance-0.1 (crate (name "riddance") (vers "0.1.0") (deps (list (crate-dep (name "typenum") (req "^1.17.0") (features (quote ("const-generics"))) (default-features #t) (kind 0)))) (hash "15h9vyiyxqwspqq1xg97r9hbypapsf8lxfrmxdq4rp8y9kw9pwx7")))

(define-public crate-riddance-0.1 (crate (name "riddance") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.17.0") (features (quote ("const-generics"))) (default-features #t) (kind 0)))) (hash "1kg76kka50h82j9ywrc3kdcyc7r2j3kzh5ci45g0j1vmy01l10vl")))

(define-public crate-riddle-0.0.0 (crate (name "riddle") (vers "0.0.0") (hash "1zc603w1bz3liwfqmm0w8w7sjqlivvr2cnji5z3a4zmy1ix20jhw")))

(define-public crate-riddle-0.0.1 (crate (name "riddle") (vers "0.0.1") (deps (list (crate-dep (name "metadata") (req "^0.44.0") (default-features #t) (kind 0) (package "windows-metadata")) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing" "printing" "full"))) (kind 0)))) (hash "1xz2yy3bj3brhj5hfx20b9ks5ih84mgspljxj9sk4jzi175wy9y1")))

(define-public crate-riddle-0.1 (crate (name "riddle") (vers "0.1.0") (deps (list (crate-dep (name "windows-bindgen") (req "^0.52.0") (kind 0)))) (hash "1gy0m6phsdx7yb9caimmwg89rhv4f1lfhx4j7z211r24p1qs5m87") (rust-version "1.56")))

(define-public crate-riddle-0.2 (crate (name "riddle") (vers "0.2.0") (deps (list (crate-dep (name "windows-bindgen") (req "^0.53.0") (kind 0)))) (hash "10fpg3h6xbcwyyhc8ghqg8mj7q1dplxzq59zh71lcmj431irijir") (rust-version "1.60")))

(define-public crate-riddle-0.3 (crate (name "riddle") (vers "0.3.0") (deps (list (crate-dep (name "windows-bindgen") (req "^0.55") (kind 0)))) (hash "0bd0g8xqmjgfa79y836dlq030a6r2pk68r8a4nasmzwj0s8crzf2") (rust-version "1.60")))

(define-public crate-riddle-0.56 (crate (name "riddle") (vers "0.56.0") (deps (list (crate-dep (name "windows-bindgen") (req "^0.56.0") (kind 0)))) (hash "0hanph3krc7dml085kciic95j67bz3rmpvc1n0k4hyj02jhbpw0d") (rust-version "1.60")))

(define-public crate-riddle-core-0.0.0 (crate (name "riddle-core") (vers "0.0.0") (hash "19m50qwljijbl23fa6s9ci9w2hllbq7ha3n1bggygslmdlq8phpv")))

(define-public crate-riddle-lib-0.0.0 (crate (name "riddle-lib") (vers "0.0.0") (hash "06yik0zkyjsvmzlh391f02h7xy9rzmwxdcpn94rfvw78xpj2j3hp")))

