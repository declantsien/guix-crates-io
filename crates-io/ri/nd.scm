(define-module (crates-io ri nd) #:use-module (crates-io))

(define-public crate-rindex-0.1 (crate (name "rindex") (vers "0.1.0") (deps (list (crate-dep (name "conv") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "ordered-float") (req "^4.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1vadkrk48h0sr238b59rg7ywc30bpxwip0pvi4n2vq6hn127p38f") (rust-version "1.63")))

