(define-module (crates-io ri dg) #:use-module (crates-io))

(define-public crate-ridge-0.1 (crate (name "ridge") (vers "0.1.0") (hash "06ni3gp1fxzj7q6bj6xm53wshjzqkhpmj0zr1wjiwc7gak5zyxms")))

(define-public crate-ridgeline-0.1 (crate (name "ridgeline") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1yyslng4hb1b6f1gqgzh6xxrq86ma8yckb8qck1z6ri9x4mvw5vm") (features (quote (("input" "cpal" "thiserror") ("default" "input"))))))

(define-public crate-ridgeline-0.2 (crate (name "ridgeline") (vers "0.2.0") (deps (list (crate-dep (name "cpal") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ak5in7ldllsp88zrhd2zg0ygf9p037cgk6hxg303h5qkw4rjk0p") (features (quote (("input" "cpal" "thiserror") ("default" "input"))))))

