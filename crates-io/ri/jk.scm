(define-module (crates-io ri jk) #:use-module (crates-io))

(define-public crate-rijksdriehoek-0.1 (crate (name "rijksdriehoek") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j518bgwv6zich2an7nqcc950kx2x8aif00qf1cq9sc45jhcjnzw")))

