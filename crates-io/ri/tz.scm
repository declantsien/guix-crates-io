(define-module (crates-io ri tz) #:use-module (crates-io))

(define-public crate-ritz-0.1 (crate (name "ritz") (vers "0.1.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-nested") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ritz_impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bg5px7y64bv5n5nm85h7vqbaii1avrk57c14k6xl244l9wj9dg7")))

(define-public crate-ritz_impl-0.1 (crate (name "ritz_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "snax") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ysx9nr19r00jx3pzbd5pyrn1f9qcd7qqjq9h3jsfk797hablyzs")))

