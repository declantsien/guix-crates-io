(define-module (crates-io ri qu) #:use-module (crates-io))

(define-public crate-riquid-0.0.1 (crate (name "riquid") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "0v18rr9lnpdqmvywihxa25g0jgx7lzfnm2syzfbch0sfflszz609")))

