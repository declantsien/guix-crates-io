(define-module (crates-io ri pr) #:use-module (crates-io))

(define-public crate-riprop-2 (crate (name "riprop") (vers "2.0.1") (deps (list (crate-dep (name "anyhow") (req ">=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req ">=0.10") (default-features #t) (kind 0)))) (hash "0616479h4hfjqkwyzqng1i1jh6icg6fbpdf039cfp992kk8hfhf9")))

(define-public crate-riprop-3 (crate (name "riprop") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req ">=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req ">=0.10") (default-features #t) (kind 0)))) (hash "0jn4m1kn25nxx7cynigb8fqkib4hc5lbs619m85nb99x96kqrg9v")))

