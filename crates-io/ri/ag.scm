(define-module (crates-io ri ag) #:use-module (crates-io))

(define-public crate-riag-0.1 (crate (name "riag") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0z7nr0iq9zbqd00pavbnpkvchjcnbm8va4w71r23dqh3i4ajddrr")))

