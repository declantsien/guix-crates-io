(define-module (crates-io ri do) #:use-module (crates-io))

(define-public crate-rido-0.1 (crate (name "rido") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1gnrwf53sgc64xs6f63q2j3icncnx5k0rwnp6i9vzi89bk90w963") (features (quote (("consumer"))))))

(define-public crate-rido-0.1 (crate (name "rido") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1hvzbp249dap65wnrs56x1f767gdfgnri3cacrr5lpid798pw7jx") (features (quote (("consumer"))))))

(define-public crate-rido-0.2 (crate (name "rido") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1wnlbjav5cgygy27l8gad5yi6383vli4n32v78b857rk0plgi3ih") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

(define-public crate-rido-0.3 (crate (name "rido") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "089vdg376anjnffidpl3pbbmxpvv13cmxc94bljp868dw69c9ssh") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

(define-public crate-rido-0.3 (crate (name "rido") (vers "0.3.1") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0nl9d8ibprxbv6kxkkp8lwzxzs5ry7k2mrnslmjk8pgw1l3dapl6") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

(define-public crate-rido-0.3 (crate (name "rido") (vers "0.3.2") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "12zrrc6kb69mbm7pkpxqqd0kjywn9wvygcaplsk56ias6ixqrlda") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

(define-public crate-rido-0.4 (crate (name "rido") (vers "0.4.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1bkfp33qvjlzrq19r8ca284jx7siddcyv3gjm8cnc995y28frsc0") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

(define-public crate-rido-0.4 (crate (name "rido") (vers "0.4.1") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "183j4aw3xapsn12l1fl0g5r9h3cfsfhysmkfhc2i80920bwbpb21") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

(define-public crate-rido-0.4 (crate (name "rido") (vers "0.4.2") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0dyrmf07syzk1p539iwbraxygn3q3whknai88jq9jpiia8kibp97") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

(define-public crate-rido-0.4 (crate (name "rido") (vers "0.4.3") (deps (list (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1x7fzmjpmvaciy4v8c34lqr18a1hz8rb6h66l85ihq2y56wad3k5") (features (quote (("enterprise") ("default" "consumer") ("consumer"))))))

