(define-module (crates-io ri #{2p}#) #:use-module (crates-io))

(define-public crate-ri2p-1 (crate (name "ri2p") (vers "1.0.0") (deps (list (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "nom-unicode") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "10r70ql40njyza1527s57yzaswr1qvx75yng0d2ddmg44ydmk4zy")))

