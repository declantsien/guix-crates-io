(define-module (crates-io ri gi) #:use-module (crates-io))

(define-public crate-rigid-0.0.0 (crate (name "rigid") (vers "0.0.0") (hash "103dqm7xj0qh5an8y7wr56xidil9d6yvpamlhs2qjwpzkdq6qm0k")))

(define-public crate-rigit-0.1 (crate (name "rigit") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1v61wnq04n9w3wxsc182arljdxgi4p7mhma35a0q1camnlnx40l4") (yanked #t)))

(define-public crate-rigit-0.1 (crate (name "rigit") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "13v2dlnk2lbghh4pzj2i4r04zdrflqsrxw4d31825hjhccj33201") (yanked #t)))

(define-public crate-rigit-0.1 (crate (name "rigit") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0d8rrn3pzc24xgx6b77nqhj62mbhbhlhkzr3aha5f1w0a6hdi7w8")))

(define-public crate-rigit-0.1 (crate (name "rigit") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "04j89b557man5phah3l2v7csqj79sp0x8m04qx348d4zbcbfi5sa")))

(define-public crate-rigit-0.1 (crate (name "rigit") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.5.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1x3h9l770qmmb1zzxhcnz66pc735722nsm1l4pakmb334fy07c0x")))

(define-public crate-rigit-0.2 (crate (name "rigit") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.5.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0a67dszxb1y3n49lrn0x2b24bsqzprx9q5krdc7y45g4wl9s1pjs")))

