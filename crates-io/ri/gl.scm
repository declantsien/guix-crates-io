(define-module (crates-io ri gl) #:use-module (crates-io))

(define-public crate-riglet-0.1 (crate (name "riglet") (vers "0.1.0") (hash "1s3nadz8703zf5n8r817zdk1x23wv4pbr14g4qnpzz7l8ycxm6zl") (yanked #t)))

(define-public crate-riglet-0.1 (crate (name "riglet") (vers "0.1.1") (hash "1w2c0lfgk8qzvi9bbzgbhs1ajj0wzfxbr3jmkzsz0snk0cyqynha") (yanked #t)))

(define-public crate-riglet-0.1 (crate (name "riglet") (vers "0.1.2") (hash "0x80b048s57yrl6y7wnrj6bvxyd09rvcyipgkk39kc6h9gx0kp8x")))

(define-public crate-riglet-0.1 (crate (name "riglet") (vers "0.1.3") (hash "0cfdh3z69r82magksb8m77v7da797mf240mz9lv3nykdgk7jr1s1")))

