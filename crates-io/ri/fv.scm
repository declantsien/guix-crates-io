(define-module (crates-io ri fv) #:use-module (crates-io))

(define-public crate-rifven-0.1 (crate (name "rifven") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "04hi412iapgxs2qpz5s11l3gqjcczjaj5ch86hk8dp284bnxj9in")))

