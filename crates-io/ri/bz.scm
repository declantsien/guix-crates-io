(define-module (crates-io ri bz) #:use-module (crates-io))

(define-public crate-ribzip2-0.1 (crate (name "ribzip2") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "1a01pylhlkk8afqhczq7gpdgjcqms7nv3wgdpasp7cyhjllwnypq")))

(define-public crate-ribzip2-0.1 (crate (name "ribzip2") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "0n4j769k42j54vmdsjqa5kg1bb0gkmjll53kzblchpp4armab2ir")))

(define-public crate-ribzip2-0.1 (crate (name "ribzip2") (vers "0.1.3") (deps (list (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "02nz3jrqn6ma350zh2461qfybi916514awnxgrcksglygr8xvwqk")))

(define-public crate-ribzip2-0.2 (crate (name "ribzip2") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "11vylmj2yilcwqdgqfayqgcha8v8iffpn94bf65xgnd14scc27k9")))

(define-public crate-ribzip2-0.3 (crate (name "ribzip2") (vers "0.3.1") (deps (list (crate-dep (name "libribzip2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "130yqadyyc5bci546a4mcmdqndjqzlis9wq12hba1blszrn6j34h")))

(define-public crate-ribzip2-0.3 (crate (name "ribzip2") (vers "0.3.2") (deps (list (crate-dep (name "libribzip2") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "1yayw63plac6p0f0x5m471mb7ibqas72hxnqdmdydmam08dl5gm8")))

(define-public crate-ribzip2-0.4 (crate (name "ribzip2") (vers "0.4.0") (deps (list (crate-dep (name "libribzip2") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "1abqi2nif573r015jxf9ph844xihf31206mvn4pmn5r023wcn9yq")))

(define-public crate-ribzip2-0.5 (crate (name "ribzip2") (vers "0.5.0") (deps (list (crate-dep (name "libribzip2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "0ymiigahb0vdzjhnvfi6nk4g9yr5d7i1mj7lk4rzil89cqs19arr")))

