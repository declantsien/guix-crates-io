(define-module (crates-io ri gc) #:use-module (crates-io))

(define-public crate-rigctld-0.1 (crate (name "rigctld") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("net" "io-util" "time" "process"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0rj487brkbzd4gfpkrvqxzqmhxh97gfq3hdkkvwcyxxq6zp3fnxv")))

