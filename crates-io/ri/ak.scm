(define-module (crates-io ri ak) #:use-module (crates-io))

(define-public crate-riak-0.1 (crate (name "riak") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1j9yym2wyhpy8f5i8n5f3ypfa7s3qflxkbjc4lpdvpvfbdckbdg7") (yanked #t)))

(define-public crate-riak-0.1 (crate (name "riak") (vers "0.1.7") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0b6yjrdcnajk8m58bicw52gmcjqrcwjm25aznvi0y02sabmvy1rc") (yanked #t)))

(define-public crate-riak-0.2 (crate (name "riak") (vers "0.2.4") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0nxxgfsdw6y24j5m3lhfz3smqb690g0yb2h6nlwnwx5n6rvbnwn9") (yanked #t)))

(define-public crate-riak-0.2 (crate (name "riak") (vers "0.2.5") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1w0lmsd29fi48ih7pkbx6w9d6mvk2j307n71y9fmc7lqyrli5257") (yanked #t)))

(define-public crate-riak-0.2 (crate (name "riak") (vers "0.2.7") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1j687ir4bkiijyjcpn6vzv6zmvv21b12nhkdmwbipimccx7p06md") (yanked #t)))

(define-public crate-riak-0.3 (crate (name "riak") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "12dsamb30qwdqbxc61faqp2qm1r9nq6cz3zb6p1gk323f650v7d5") (yanked #t)))

(define-public crate-riak-0.3 (crate (name "riak") (vers "0.3.2") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "07zm0562m67cj0r48i99clp45999zw83h42jab7qf35sxbgjwh2a") (yanked #t)))

(define-public crate-riak-0.3 (crate (name "riak") (vers "0.3.3") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0l17zgs1sdd027d4gb56pvbqxgby0fyd9l1nvg3xjlj33c5p12y8") (yanked #t)))

(define-public crate-riak-0.3 (crate (name "riak") (vers "0.3.4") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1n4f2qgnv9b725s9ja1yin4slbav2799dci5yzsrfmi4hvhdrz6d")))

