(define-module (crates-io ri mc) #:use-module (crates-io))

(define-public crate-rimcol-0.1 (crate (name "rimcol") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "lab") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "tensorflow") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1kk61c06a89ixn54n6z6kw3mq43s2l4rzwj9k0yyfk3v6jp82wh9")))

(define-public crate-rimcol-0.1 (crate (name "rimcol") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "lab") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "tensorflow") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0jhk8iwnh6v038vdxqzkf1wrf6srxc3rkna2b3kfdf0irjw8frxa")))

