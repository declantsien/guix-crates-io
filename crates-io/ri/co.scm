(define-module (crates-io ri co) #:use-module (crates-io))

(define-public crate-ricochet_robots-0.1 (crate (name "ricochet_robots") (vers "0.1.0") (deps (list (crate-dep (name "macroquad") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1a1xw0wv6vmzdxxgbcbyandb62fl63b5zvf92f3p86qk5mz4avx2")))

(define-public crate-ricochet_robots-0.2 (crate (name "ricochet_robots") (vers "0.2.0") (deps (list (crate-dep (name "macroquad") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0v7k5zrp1rzcaa1h4pfy0a1ggipbfjhvpla9x5cls1g0bx8a063l")))

(define-public crate-ricochetrobots-0.0.0 (crate (name "ricochetrobots") (vers "0.0.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde-wasm-bindgen") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "0fcab57mnlas55lj6zlh8clywd6i8dc7jn52f1rn6ikchprvgb5s")))

(define-public crate-ricom-0.1 (crate (name "ricom") (vers "0.1.0") (hash "0yjv3s7d318nqvh1d7x0qvnqq708fzgh8ybiiqfxv7imphj6kk1d")))

(define-public crate-ricom-0.1 (crate (name "ricom") (vers "0.1.1") (hash "0v650c8dp9cdnndh39z4zm2kbm03qkm7jdkcl14996jjk9ha7b5d")))

(define-public crate-ricom-0.1 (crate (name "ricom") (vers "0.1.2") (hash "05wcxaclh54j282krsd59snv2aywwrm8y3q9l1jxs4b83720z1n5")))

