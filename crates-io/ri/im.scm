(define-module (crates-io ri im) #:use-module (crates-io))

(define-public crate-riimote-0.1 (crate (name "riimote") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "bluez-async") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.2") (features (quote ("linux-static-hidraw"))) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 2)))) (hash "0fznii435v60n0p2zjlxplliap9px0h113sljkj7fncc1a7n147i")))

(define-public crate-riimut-1 (crate (name "riimut") (vers "1.0.0") (hash "0zjlyx9z60wkaa7wm5gczw78y46r9gmrwmhjh6wj53py0rzlvxjw")))

(define-public crate-riimut-1 (crate (name "riimut") (vers "1.1.0") (hash "1jv89sl8i2lzn3ccxpm55661hmrl3381fv55kq3z62yvj661c57h")))

(define-public crate-riimut-1 (crate (name "riimut") (vers "1.2.0") (hash "1v34907cb18ij28brk26i3pqqpr2yic96y4axxkcvi54p4vp675f")))

(define-public crate-riimut-1 (crate (name "riimut") (vers "1.2.1") (hash "0gvdhrkw87cadanhr3z0k6430w9wxzvlqgh4pdybn2hzk1asdiwi")))

