(define-module (crates-io ri tm) #:use-module (crates-io))

(define-public crate-ritm-0.1 (crate (name "ritm") (vers "0.1.0") (hash "0mc24hp9i0r0lbb3hql52wj63if3g8bqf9kwp3q5lnvf23gn2iby")))

(define-public crate-ritm-tui-0.1 (crate (name "ritm-tui") (vers "0.1.0") (hash "0vfvynii7622fwxrhmnd1wmylry4mjrq22c1xp6a8bhh7b1sb16j")))

