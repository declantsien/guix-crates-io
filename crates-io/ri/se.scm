(define-module (crates-io ri se) #:use-module (crates-io))

(define-public crate-rise-0.1 (crate (name "rise") (vers "0.1.0") (hash "0wayl9kr88zkpgzs8sgv6gw4crc6phbqv33mvc341ryyg8bpnl9f")))

(define-public crate-risecode_functions-0.1 (crate (name "risecode_functions") (vers "0.1.0") (hash "11rgiidq5z2pfg5pkvczxxx3g4c98hnnahviylccjdhy9fqmm6yj")))

(define-public crate-risecode_functions-0.1 (crate (name "risecode_functions") (vers "0.1.1") (hash "1n172sz4v1lv2mi82svr1ivnpkmdy76jlc922a1z76vym59z8lnl")))

(define-public crate-risect-0.1 (crate (name "risect") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "1j1lzs3akkx7apiwb1qydlkfwzl9wcqhhs5qdydr32wagffam2n1")))

