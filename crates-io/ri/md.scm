(define-module (crates-io ri md) #:use-module (crates-io))

(define-public crate-rimd-0.0.1 (crate (name "rimd") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "0sl4z3ysdqfnz11yy7sahjh46yq8s0dq7r1kcfr6qgpry7xya6f3")))

