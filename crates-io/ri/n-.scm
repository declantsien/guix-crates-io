(define-module (crates-io ri n-) #:use-module (crates-io))

(define-public crate-rin-rs-0.1 (crate (name "rin-rs") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)))) (hash "079r4wf7fi3w7lb65y2vcnxnwb0j3y8acr1is1zvgmihwbdixf99")))

(define-public crate-rin-sys-0.1 (crate (name "rin-sys") (vers "0.1.0") (hash "1rmx0hr7x2angxjfb7nwci0z51drbvl8zs78lnwd9g8ds82qm6yd")))

(define-public crate-rin-sys-0.1 (crate (name "rin-sys") (vers "0.1.1") (hash "04945gqsdvnd7maa7apxw2p3bvxl9rwxwwwrn19wbbib2rsgag6q")))

(define-public crate-rin-sys-0.1 (crate (name "rin-sys") (vers "0.1.2") (hash "182b870ymlh3sjvmha5n0yhckrajam45d22xlw0sa5d3vb77mhq2")))

(define-public crate-rin-sys-0.1 (crate (name "rin-sys") (vers "0.1.3") (hash "0p8hbhwzy6s0vxix5r5f5pjaxmac4m67251742fmp1n5v2gxn8h6")))

