(define-module (crates-io ri pb) #:use-module (crates-io))

(define-public crate-ripb-0.2 (crate (name "ripb") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 0)))) (hash "1gdbdg53wlxcsrixs8py66nvhvqr4ws222nvx474dagag6cxrz80")))

(define-public crate-ripb-0.3 (crate (name "ripb") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 0)))) (hash "03i74mgjg3ldvwcr5pd3y10rrxi1wiw1yivx6khpyy392nazhhps")))

(define-public crate-ripb-0.3 (crate (name "ripb") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 0)))) (hash "0rsc6rdbqcxqnx8ngvp5bk67z3irm4lk2qzj1jv49db52rrxkn5s")))

