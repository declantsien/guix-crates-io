(define-module (crates-io ri p-) #:use-module (crates-io))

(define-public crate-rip-rs-0.0.1 (crate (name "rip-rs") (vers "0.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "10k2vi24yhxrjdwfvnmj7ckgz0sdgq3fxl2iqip48kp01dnv8207")))

