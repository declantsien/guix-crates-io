(define-module (crates-io ri so) #:use-module (crates-io))

(define-public crate-rison-0.1 (crate (name "rison") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "10ckrwxrvfs22vvfh2j7cgk92wkfkzrpvn0cb4gdrfgamjpnk65y")))

(define-public crate-risotto-0.0.1 (crate (name "risotto") (vers "0.0.1") (hash "1ggiywawiinllsvjs76560ia3q56vn0hsl3nbhiy1zaackr6ds50")))

