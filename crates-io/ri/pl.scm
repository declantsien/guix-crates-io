(define-module (crates-io ri pl) #:use-module (crates-io))

(define-public crate-ripl-0.0.1 (crate (name "ripl") (vers "0.0.1-placeholder") (hash "16dhpsl2j6djnyw0kqhgbxln6zqldllq4nzam4ps5fga0ai5qjfb")))

(define-public crate-ripline-0.1 (crate (name "ripline") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (kind 0)) (crate-dep (name "bytecount") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "grep-cli") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "linereader") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "memmap2") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "termcolor") (req "^1.1.2") (default-features #t) (kind 2)))) (hash "1y3b1nyl1cvs2m5q7dkq9jkg55znybgvarl7rqm6s12nqz9s6dml") (features (quote (("default" "bytecount/runtime-dispatch-simd"))))))

