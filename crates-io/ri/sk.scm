(define-module (crates-io ri sk) #:use-module (crates-io))

(define-public crate-risk_and_safety_borat-0.1 (crate (name "risk_and_safety_borat") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0776484m7riiz087l7g610yixl97r2a72mmjwl5hp53f4axyiwbi")))

(define-public crate-risk_and_safety_borat-0.1 (crate (name "risk_and_safety_borat") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1xd808wzk1qf2cbfg0nz7v2kmmq5giszaahl9598d7w4gsn0ni5g")))

(define-public crate-risk_and_safety_borat-0.2 (crate (name "risk_and_safety_borat") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1kfph0dvvi2q0996p138gp7sq365b8r2dc48vwab7dcmgkjqk14a")))

(define-public crate-risk_and_safety_borat-0.3 (crate (name "risk_and_safety_borat") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0ia182kir8brvr25js86izh2kchyfyr3hkfpxbj8z09i3pjqy78g")))

(define-public crate-risk_management_network-0.0.0 (crate (name "risk_management_network") (vers "0.0.0") (hash "1548p8g3wry6i3xyk93mv4sx71zpcg25b87r3ls01hwnx9y9r30k")))

(define-public crate-riskparity-0.1 (crate (name "riskparity") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0aj64cz7fw2iy4bcxkvlkx28wdfkfa0sf6l7q7216jhj7pqwhyyp")))

(define-public crate-risky-0.1 (crate (name "risky") (vers "0.1.0") (hash "05pblramh25c2gs2pw5i65b5yb851hd841yqx58b7v2f29ykzvxf")))

(define-public crate-risky-0.1 (crate (name "risky") (vers "0.1.1") (hash "1agjmwmqq8ik0zcv6w56vzl037jvfyawdqbmhcy9aqk4f9aqi5vc")))

(define-public crate-risky-0.1 (crate (name "risky") (vers "0.1.2") (hash "06vsmm50h3jbv7fmyzy58b1cmbiz0fh6zxlpqpaknkp2f18yw8mb")))

(define-public crate-risky-0.2 (crate (name "risky") (vers "0.2.0") (hash "0a8db84r9lsjfr5g2i31s8whp0c9qbjfj8m03hrhb02q4w4rcfmn")))

(define-public crate-risky-0.2 (crate (name "risky") (vers "0.2.1") (hash "1id199bm9751z8r165jf2m17bz6n7rcxl0a5kqlqbxfcqnhlw154")))

(define-public crate-risky-0.2 (crate (name "risky") (vers "0.2.2") (hash "1nz2mr7gk2gn2ilixxmc6zwk08cjscxyqn44hg5m7a86dmfff418")))

(define-public crate-risky-0.2 (crate (name "risky") (vers "0.2.3") (hash "12cb4yfgsvcd50c8svm8cc21shskvj1xli07ybmah903qyzjc4z2")))

(define-public crate-risky-0.3 (crate (name "risky") (vers "0.3.0") (hash "1jd2lr3h552r9kaw1zz67m735br8zfvdh09wwrbimq75zljj1znj")))

