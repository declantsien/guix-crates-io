(define-module (crates-io ri ni) #:use-module (crates-io))

(define-public crate-rinimp3-0.1 (crate (name "rinimp3") (vers "0.1.0") (deps (list (crate-dep (name "minimp3") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "13z5mria94k0fl3fcjqhqvxmya35kjiggpyyh46rsqw61z2bl6d8")))

(define-public crate-rinit-0.1 (crate (name "rinit") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)))) (hash "0610cfwa1kgmvymyrffcrsgdnx5af603kbfxivwyrkjxdfxblz7f")))

(define-public crate-rinit-0.1 (crate (name "rinit") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)))) (hash "04lavaa7hf22zafz8m0hmgms5hblmhvk3fn6zmssxfp77553mgm7")))

