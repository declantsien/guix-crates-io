(define-module (crates-io ri go) #:use-module (crates-io))

(define-public crate-rigolds1000z-0.0.1 (crate (name "rigolds1000z") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qca6qjc9kfrqq8k1f5zbxnl5fg40a51yxzx17fc7gisnpd7yh71")))

(define-public crate-rigolds1000z-0.0.2 (crate (name "rigolds1000z") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wbx1kmjgwdarv5wfp97g8glm3j5lqgpbq6qzblh2c5f8yw6b7fp")))

