(define-module (crates-io ri na) #:use-module (crates-io))

(define-public crate-rinara-0.1 (crate (name "rinara") (vers "0.1.0") (deps (list (crate-dep (name "gfx-hal") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.18") (default-features #t) (kind 0)))) (hash "1fj0s6am3wig422c8d37cr46x7ixckm7ysw78lqf0ycvc7b0ibmz") (yanked #t)))

