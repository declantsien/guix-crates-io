(define-module (crates-io ri mp) #:use-module (crates-io))

(define-public crate-rimp-0.1 (crate (name "rimp") (vers "0.1.0") (hash "1327scax44lc0m7dcc4kqqipzbdn2rh1013y7gzz6k0fi0b4g2qq")))

(define-public crate-rimpiazza-0.1 (crate (name "rimpiazza") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "040gnbv02ciqprnwb1bs0mnamrqlp15vx05zm8nyn412ng1if74i")))

(define-public crate-rimpiazza-0.1 (crate (name "rimpiazza") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0c9f7hqsynpafx65wby9k4f54s7l9yxnqp4153lixngdhyk0lwav")))

(define-public crate-rimpiazza-0.2 (crate (name "rimpiazza") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "17l7w9pp16jy08sm23dxiks1izym8d59drzs7kv01npqddp846ww")))

