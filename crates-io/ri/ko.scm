(define-module (crates-io ri ko) #:use-module (crates-io))

(define-public crate-rikopnet-0.34 (crate (name "rikopnet") (vers "0.34.0") (deps (list (crate-dep (name "ipnetwork") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pnet_base") (req "^0.34.0") (kind 0)) (crate-dep (name "pnet_datalink") (req "^0.34.0") (optional #t) (kind 0)) (crate-dep (name "pnet_packet") (req "^0.34.0") (kind 0)) (crate-dep (name "pnet_sys") (req "^0.34.0") (optional #t) (kind 0)) (crate-dep (name "pnet_transport") (req "^0.34.0") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.3.23") (default-features #t) (kind 2)))) (hash "0hlbsxvvwidgrpw38w6niq0zy7b6ahd5r6izwypvzkgqvav77vsx") (features (quote (("travis") ("std" "pnet_base/std" "pnet_sys" "pnet_datalink" "pnet_transport" "ipnetwork") ("pcap" "pnet_datalink/pcap") ("netmap" "pnet_datalink/netmap_sys" "pnet_datalink/netmap") ("default" "std") ("benchmark") ("appveyor")))) (v 2) (features2 (quote (("serde" "pnet_base/serde" "pnet_datalink?/serde"))))))

(define-public crate-rikotsu-0.1 (crate (name "rikotsu") (vers "0.1.0") (hash "177fw2rdzb3bjclfax0cpfc50hg4ra3ypn4d3nsa41r3kwvskzm1")))

