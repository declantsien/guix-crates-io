(define-module (crates-io ri am) #:use-module (crates-io))

(define-public crate-riam-0.1 (crate (name "riam") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1b5pi4nvrh7ik3dn5x21sfc0jm4v5xhf3cyiijlpj9wfgfyhfs8h")))

