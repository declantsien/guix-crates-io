(define-module (crates-io ri fs) #:use-module (crates-io))

(define-public crate-rifs-3 (crate (name "rifs") (vers "3.7.0") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "1h6p71dyb6wm08s04s95az2c9k0jmx19ff85v468ybsw52iv3x9a")))

(define-public crate-rifs-3 (crate (name "rifs") (vers "3.7.1") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "0lv3mbk4x5h1j8xaj01pd2macs2sij3cgh9dh8g6qmfc448mc4p8")))

(define-public crate-rifs-3 (crate (name "rifs") (vers "3.7.2") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "16vjid5x4kf63gw6mrvgzisq0crbs3i7hcjbmyqya6917gpw6gvb")))

(define-public crate-rifs-3 (crate (name "rifs") (vers "3.7.3") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "1n4m6ivk4c6mapvdrrdkxhk5d9xi1v6s7iyxrafwinv7nkwf2w4c")))

(define-public crate-rifs-3 (crate (name "rifs") (vers "3.7.4") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "05vmsmrh2djg7vkrlg7pkwsjvg2wx8scsf392l8rgamqb8alacbp")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.0.0") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "1zx1a0vhdszkhxcbrd58zlnps72vq0asdq5q1pyzk48023icjy04")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.0.1") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "1mga7xyn70vicshhksmyg935fqswh8p5djv8fj1zsffr7m52y49q")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.0.2") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "0v9aa06lfhm5xadsm38lc6a9sp9h6l0adxxc556fw54j65zs80sp")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.0.3") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "1b1ri493jn4yfnnh59c30vygcmh3lgilgx7vsx2p98v7bl8037m2")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.1.0") (deps (list (crate-dep (name "which") (req "^4.4.2") (default-features #t) (kind 0)))) (hash "1yb1zj1ajb1a4byb22968v0965yh8p7n94z3pdcicd7vh82abccq")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.1.1") (hash "0ncv0gs5rwa8kc39iqi52bkxb2cdhsv7brx167j9jx2rvy8l171r")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.1.2") (hash "0aysnsjmmnh7ch9ww1ki4llzw2nlklrngak8p7xydckgn3jxqz42")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.1.4") (hash "08hmjx2pai04d0f0gh8kvdsa2v540754yj7sjxxsahicvv1crmab")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.2.0") (hash "0bv2f81kwbxf4j41v9f3mkw1y5xrpjnhvsry1wlcj4a4kxg3a7lv")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.2.1") (hash "1dbpld92bi5j1vbi2drrmg4ph8dd96rzzl0aaaslqpvk5ri5bf9c")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.3.0") (hash "0xgvx8ly7mrn593ksjh8k8g5ziw6v9fw93cl802ch6pmq9vsc1zh")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.4.0") (hash "0qskjyylqsfi1irw06ai892yf3gddnqdzrjz22azg7vpnf3klsgw")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.4.2") (hash "03271cq8hxa26b3l3zd6azpr56r0v5j1w7wg9hr3fz1099qrc22m")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.5.0") (hash "1mk1jvhjrv7ymikxg3y5kjv64jkzx5796i4gyzjsblpa1ar4bpnz")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.6.0") (hash "0bwbx620nyyvw55ic99pf7rwi38crja4gs6wxwafcxzdq665ch7h")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.6.1") (hash "1h61daq5c2j5954aqdfjqsgsf30hr4ybvbb6wwc8ns9klfsd5w4n")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.6.2") (hash "1nkdsh5j7qbfw0y56ywdjm8172mm5hwjw1pqazjay0c671kq7l85")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.7.0") (hash "1yqsv8jjdws9ll1mh5vm3b93rgnzy83h836ia1476nsnwzscg6dx")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.8.0") (hash "1k63k7y3irnjaclkd52dn0gyh07l06xzpp79z8zwbnjzbqkdfh4q")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.8.1") (hash "04xd49zwvg0878157645qs9qmid23jlpsksfp7n785bkx6rp4hpk")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.8.2") (hash "14m2xwd88dj6zqibqs1lbjbaw673qj57l68ap26x1v5qfxy94p9a")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.9.0") (hash "1bjify1nwxkhwg1l387grj5g55458gxayhiszvx2vgvx4hn8333c")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.10.0") (hash "1b9fyqiwid6856whk5hm3jjz93s2j16865i8a1ixj6yi4yizwg2x")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.0") (hash "11vl2dlqscllalpa3ghyr49mx27n9wxnpp6pc0ic8nx7c7x4k06y")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.1") (hash "1304wlnpj4b72bmjx950i7mnc12iycr8dx0v5fx9fdxp9w7ja0sz")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.2") (hash "072lknp1czh760n96b9350052ycvshnwprp6y5d5g5shpih06hfk")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.3") (hash "1n9s5sgj4cif61xxm9x6bxbzdlhf5yg1qlazpk9x8vlqicrm7971")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.4") (hash "0282wz70sy4h0y4ym2baz86h9lz49zzwgnn339cp3nmnibb9hzp5")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.5") (hash "0jp2235mc5z2qv3s4yp6w7j547n12ay7gcpsjqrj3m02n9c0gmgn")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.6") (hash "0mnsdgk40knf47aqf22899x5y3r16wfgmga36i6vj512jjr3cac6")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.7") (hash "0krnfqaabqxwi886rrzrlw7xxfkpyvbbwpnskz1bqpknzfnf2jcs")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.11.8") (hash "0jsaw7l5911n81yf6dgbymkzmvv8rpkv8mwxgp1wmwrf0nvq80k3")))

(define-public crate-rifs-4 (crate (name "rifs") (vers "4.12.0") (hash "0jgw9qlnrpfyyibdbb0crxd2iyf8cq5p0lra7div6s4x8fvnb2va")))

