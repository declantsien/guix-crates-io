(define-module (crates-io ri c-) #:use-module (crates-io))

(define-public crate-ric-engine-0.1 (crate (name "ric-engine") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "14lk8r8slhyv1h2d348v9zg6d98b470wzlhr0zg08ac0sp6l006w")))

(define-public crate-ric-repl-0.1 (crate (name "ric-repl") (vers "0.1.0") (deps (list (crate-dep (name "ric-engine") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-log") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1iaxzv6d9c5ybgb75hvghl0z32166adlzw3x0ryvgpyjyiyayp1m")))

