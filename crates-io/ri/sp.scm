(define-module (crates-io ri sp) #:use-module (crates-io))

(define-public crate-risp-0.1 (crate (name "risp") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kdglqsds2x087h6vpspavplcfmvabcp48q2l2ipkwqlq2gxil2r")))

(define-public crate-risp-0.2 (crate (name "risp") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "03a6ifff5yagdv6b8pxdb6lpzhci8hpxylay5h385l1k9nqymgzf")))

(define-public crate-risp-0.2 (crate (name "risp") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "09sszfb21i9mv67wdxija2s85clgl7f37nija1nq17mjjislfqnm")))

(define-public crate-risp-0.3 (crate (name "risp") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "17q39xswvd44dmif5rwrkiddg9byw0ahckrmv1a061x339hp20cn")))

(define-public crate-risp-0.4 (crate (name "risp") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vd4zlb1h2rzr5snxv5lh45xkxi4rh62582gdnqwiwd63xjib3jy")))

(define-public crate-risp-0.5 (crate (name "risp") (vers "0.5.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "05g14nlp4kpakij3hgabgwabfwxw6bdrcz6zmjs9bs31y19nacka")))

(define-public crate-risp-0.6 (crate (name "risp") (vers "0.6.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "10kkmcyyxh5jcjk4l2rwgqnmj5rd4xqv3jbnmyj5v9sy6myi7wd6")))

(define-public crate-risp-0.6 (crate (name "risp") (vers "0.6.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ysfj3lyhg8mn4xj5zdxs145x7qgwkkbcsnd5p1qnal7y84g1gi4")))

(define-public crate-risp-0.7 (crate (name "risp") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "17d6f6vd86iqxarvnrn0csczh614fv65mp73q0b9izk230wc8xv9")))

(define-public crate-risp_macro-0.1 (crate (name "risp_macro") (vers "0.1.0") (hash "18z5s1bgn1lqypmd9qh13dhsnbq6pa34fy7ni07v5rj9nhrpmi17")))

(define-public crate-rispc-0.1 (crate (name "rispc") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 0)))) (hash "0i98ra4asxcbl2x149w6qnnlggxnli2hbjcxkmlwg4cn6f524qhw")))

(define-public crate-rispcrt-0.1 (crate (name "rispcrt") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "10dz3c9nmafv7q5vjz32nkddpjs9slvm6zn023h9av495mjrbgy7")))

(define-public crate-rispy-0.0.1 (crate (name "rispy") (vers "0.0.1") (deps (list (crate-dep (name "man") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.0.0") (default-features #t) (kind 0)))) (hash "1av0gg4rhyysh7fw4kckvw0kdsfvaw6pzjvhz3h47yb445dn3lg7") (features (quote (("build_deps" "man"))))))

(define-public crate-rispy-0.0.2 (crate (name "rispy") (vers "0.0.2") (deps (list (crate-dep (name "man") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.0.0") (default-features #t) (kind 0)))) (hash "1gv02kwd3cg9bq8q1w8mnsdzi9gndra5f02r353ag603xly6kdzw") (features (quote (("build_deps" "man"))))))

(define-public crate-rispy-0.0.3 (crate (name "rispy") (vers "0.0.3") (hash "0pnkljs55q11sssm4p7wzwc6nx6bh18zny85wlc8hfrvcgf49xgh")))

(define-public crate-rispy-0.0.4 (crate (name "rispy") (vers "0.0.4") (hash "02f2qndpk210kihs8ihhca428fffn9bi8ywb5c6l20cnhl3dd2i2")))

(define-public crate-rispy-0.0.6 (crate (name "rispy") (vers "0.0.6") (hash "1wnv02rghm5bzx9w4hfkaqifwvjax282mdcf0dwjp8jznaxdmvcp")))

