(define-module (crates-io ri sh) #:use-module (crates-io))

(define-public crate-rish-0.1 (crate (name "rish") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0zyahix3dqw2f4cmyh9bzm64m5v0p1b8hbix5adr71h9r847n8n1")))

(define-public crate-rishyhll-0.1 (crate (name "rishyhll") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jpxlpcbmxcapb297a760nryrildnicljrd4idi3x973893a31v7") (yanked #t)))

(define-public crate-rishyhll-0.1 (crate (name "rishyhll") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1s67s2sg9wx9nik4wqq4x1iqmxahqy61v99cqln8bmpw2qn84br3") (yanked #t)))

