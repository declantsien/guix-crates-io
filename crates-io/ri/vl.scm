(define-module (crates-io ri vl) #:use-module (crates-io))

(define-public crate-rivlib-0.1 (crate (name "rivlib") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0rvv84mgcnr8d0b7rgd3620rky0vxbaighxxnldczrbs620lrsaz")))

(define-public crate-rivlib-0.1 (crate (name "rivlib") (vers "0.1.1") (hash "0fylnr45ngnsfv2wv8h3l3v9lvz9zhh7hr6qj49a8lr3iwfcqpr5")))

(define-public crate-rivlib-0.3 (crate (name "rivlib") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "scanifc-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "scanlib") (req "^0.1") (default-features #t) (kind 0)))) (hash "1r02qzqssb8dqzxqy03an813wf3da2wfipb0fyiq0q5kvl3dfapp")))

