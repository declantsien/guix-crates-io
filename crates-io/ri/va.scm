(define-module (crates-io ri va) #:use-module (crates-io))

(define-public crate-rival-0.1 (crate (name "rival") (vers "0.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0iffvn3xnfrwi693bfcrv3gmf1i1n0j91f834c7aqy8x2zpz6yyj")))

