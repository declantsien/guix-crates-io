(define-module (crates-io ej ni) #:use-module (crates-io))

(define-public crate-ejni-0.0.1 (crate (name "ejni") (vers "0.0.1") (deps (list (crate-dep (name "jni") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "jni") (req "^0.19.0") (features (quote ("invocation"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0hq23mi5gnjbl5x31cb2kqymc5v0bs5lyaywngxzlpxdpp02qlcq")))

(define-public crate-ejni-0.1 (crate (name "ejni") (vers "0.1.0") (deps (list (crate-dep (name "jni") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "jni") (req "^0.19.0") (features (quote ("invocation"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jaxnnhk5vhdwfplnxbrbrz45k9f6b4af6ga4gwrxfzsmdf8q5vi")))

