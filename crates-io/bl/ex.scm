(define-module (crates-io bl ex) #:use-module (crates-io))

(define-public crate-blex-0.1 (crate (name "blex") (vers "0.1.0") (hash "044c3m1lgkrfzc93zfynxj6hizjiys61c2s0h1b0dnlf95h0w17b")))

(define-public crate-blex-0.2 (crate (name "blex") (vers "0.2.0") (hash "0vfx43cycnd0s8nhp5d4vr1ikl3xgzg3n6m9rv0yxc515d1ffrrx")))

(define-public crate-blex-0.2 (crate (name "blex") (vers "0.2.1") (hash "1fjj26d7958aildpmjfc6mb90mr9h1hipk0yyivy1z65pl86lls8")))

(define-public crate-blex-0.2 (crate (name "blex") (vers "0.2.2") (hash "0i7ykkd2r468ms4n12yvq3fb1w42diy8hm952cf9h8r7avw5dff0")))

