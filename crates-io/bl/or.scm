(define-module (crates-io bl or) #:use-module (crates-io))

(define-public crate-blorb-0.1 (crate (name "blorb") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "120a7cvfbdlcvwxl2xjgqk37l4slrp2b24a7xzwfjv46cnwy5wa9")))

