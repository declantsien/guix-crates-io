(define-module (crates-io bl kt) #:use-module (crates-io))

(define-public crate-blktrace-0.1 (crate (name "blktrace") (vers "0.1.0") (hash "0rllmb69pwh809wq2pfrl5098ks4x32245m5shzz4ikgjgbnygjn") (yanked #t)))

(define-public crate-blktrace-0.1 (crate (name "blktrace") (vers "0.1.1") (hash "0rsy5jynri121lfimiswl8rajakl101yh7xl8wgcqa2rrhjjlibk")))

(define-public crate-blktrace-0.1 (crate (name "blktrace") (vers "0.1.2") (hash "0iddifa7l4wh1n8c1936f71hdy9f8v8kcwhqq85gmd8nd5i017yg")))

