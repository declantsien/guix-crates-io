(define-module (crates-io bl ig) #:use-module (crates-io))

(define-public crate-blight-0.1 (crate (name "blight") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1y7wisgnzfr9330d15f2zjgv8i633y827majs64ffh4yham0fybx")))

(define-public crate-blight-0.2 (crate (name "blight") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1svq1ag0y4a8gh4b034n23vyfqmj168xv0rd3m7n523qcjfpabii")))

(define-public crate-blight-0.3 (crate (name "blight") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "19jbb50qndzd053hk90gxcz62ss6gpx80lc874adgsphyi8kakn5")))

(define-public crate-blight-0.3 (crate (name "blight") (vers "0.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "17qb2x7s16rl03m6hifxsgkw4kbr0xn78fcdb7m6v9pyix5ds4pq")))

(define-public crate-blight-0.4 (crate (name "blight") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1fn4rjjzg5b4hx4cyf1shnwwgnsrfzcvwhrs330skhkrbnkhjcw7")))

(define-public crate-blight-0.4 (crate (name "blight") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1bhy2p9r4f5dj8qv6ikggp1zxyn2wks9csn2x523y2dl333ighk0")))

(define-public crate-blight-0.5 (crate (name "blight") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1c64mwprq8zs4qbxlbyfv0dqi0zpi63bzp2gxms5rqfhr85ynpyv")))

(define-public crate-blight-0.6 (crate (name "blight") (vers "0.6.0") (deps (list (crate-dep (name "colored") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "fs4") (req "^0.6.6") (features (quote ("sync"))) (default-features #t) (kind 0)))) (hash "04lm8a6q5g92qiwml7dbrqa9yz13kgv7fgryw5k4p5lb2x08v5x1")))

(define-public crate-blight-0.7 (crate (name "blight") (vers "0.7.0") (deps (list (crate-dep (name "colored") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "fs4") (req "^0.6.6") (features (quote ("sync"))) (default-features #t) (kind 0)))) (hash "108zpfa6xhz8qfkgphd4nrrvq3pwlalr8dcq2vakr8i26c3qsicp")))

(define-public crate-blight-0.7 (crate (name "blight") (vers "0.7.1") (deps (list (crate-dep (name "colored") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "fs4") (req "^0.6.6") (features (quote ("sync"))) (default-features #t) (kind 0)))) (hash "0wk45b99zzz3x6c3x0w6z5ki1ik8cs5rivpvha0l26g0ixhx4g2s")))

