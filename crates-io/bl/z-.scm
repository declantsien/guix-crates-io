(define-module (crates-io bl z-) #:use-module (crates-io))

(define-public crate-blz-nx-1 (crate (name "blz-nx") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (kind 0)))) (hash "1xz52vk1f95sdls7v0dr3hfch1jpza4zjvj96cgxjbf6mywb3zf7")))

