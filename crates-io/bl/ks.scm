(define-module (crates-io bl ks) #:use-module (crates-io))

(define-public crate-blksocks-1 (crate (name "blksocks") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.28") (features (quote ("socket" "net"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.3") (features (quote ("timestamps"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0dpkqspy4q5syx0vhkvmks9im9r1my2hmpz3ggj1svbmy1qskidx")))

