(define-module (crates-io bl es) #:use-module (crates-io))

(define-public crate-bless-0.0.0 (crate (name "bless") (vers "0.0.0") (hash "1lb5k4xzyvddssnln8fjybpy0bb6n89rpx38zci0k18x4fw3fhdr")))

(define-public crate-blessing-0.1 (crate (name "blessing") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)))) (hash "1mm6l43am2plvd4f0flcvrxwn3v7h1h0h5dj5aj973ckxcfysn84") (yanked #t)))

(define-public crate-blessing-0.2 (crate (name "blessing") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)))) (hash "193cs14wfq56gslvsqpgqld0n78r2b7nmmh6apy2pdhjqimdf7cd") (yanked #t)))

(define-public crate-blessing-0.1 (crate (name "blessing") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)))) (hash "1qcj5hchhix7cyhlaym5ki9glppss3inhb4h3pki38rxdx7cr7gj")))

(define-public crate-blessing-0.1 (crate (name "blessing") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)))) (hash "0a87ddj0bli5mhwlzzdsq5m0cvq2sx52lh5p5ra75jdy5csspm25")))

(define-public crate-blessing-0.1 (crate (name "blessing") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)))) (hash "0hdkzz5hddiakanj01rwj2c5lvl9yjz612bw6kjcq0rnqvbi5jk4")))

