(define-module (crates-io bl ur) #:use-module (crates-io))

(define-public crate-blur-0.0.1 (crate (name "blur") (vers "0.0.1") (hash "1nc2hj1dqmxrfnda5y6izpvjwg0x5zh1bmzy43b0xlsw3fa5xbr7") (yanked #t)))

(define-public crate-blur-0.0.4 (crate (name "blur") (vers "0.0.4") (hash "1nh5ip8ghfnc50x6ghbcmxfihb93g5lx9k6amcbn5w66g3irjhf7") (yanked #t)))

(define-public crate-blurdroid-0.1 (crate (name "blurdroid") (vers "0.1.0") (hash "02p7bswyladxg5b1qmqi506qw33b5rn4qmhyqqbk8hvndjbzv4vn")))

(define-public crate-blurdroid-0.1 (crate (name "blurdroid") (vers "0.1.1") (hash "17n7ry6c909y3krhdkdw24lv8lrf4m13map196fkwn3b6silxkjz")))

(define-public crate-blurdroid-0.1 (crate (name "blurdroid") (vers "0.1.2") (hash "1f3y3wnv6di9fpslkksapxnfhbvlqzx6n0hf858girqq72xnza7l")))

(define-public crate-blurdroid-0.1 (crate (name "blurdroid") (vers "0.1.3") (hash "0r5wyilka1m43c3c9fhhgp6r35025a772vqpzkllm0lp3bmdv04q")))

(define-public crate-blurdroid-0.1 (crate (name "blurdroid") (vers "0.1.4") (hash "0s2scp663b16d5c6icmr67s46sx1i2pra9rhgpyfpgi9km8vmnnp")))

(define-public crate-blurdroid-0.1 (crate (name "blurdroid") (vers "0.1.5") (hash "0qzkmhw4zq4q1frbwvxq155gnx1ih8wgspxjbp37z5zhv8ycaxim")))

(define-public crate-blurdroid-0.1 (crate (name "blurdroid") (vers "0.1.6") (hash "15nj0xgvdvhlhxvakm32ml6z5ghncj0v57cg2ablfw17vmbkbchr")))

(define-public crate-blurhash-0.1 (crate (name "blurhash") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.21.2") (default-features #t) (kind 2)))) (hash "01jypnzrxla2c5dzpw8pq971pcv31x38fbzh67hdd5ixxvd0zgjy")))

(define-public crate-blurhash-0.1 (crate (name "blurhash") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.21.2") (default-features #t) (kind 2)))) (hash "1d2b958s816iwlad2lhsp7z5v97jabhwir3zl957iy2rpz4f8wc6")))

(define-public crate-blurhash-0.2 (crate (name "blurhash") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gdk-pixbuf") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 2)))) (hash "1pjd2qjv4x8khrb9ac9dv4zicc67a306ql4kbanf5qyabgrdypda") (features (quote (("default")))) (v 2) (features2 (quote (("image" "dep:image") ("gdk-pixbuf" "dep:gdk-pixbuf"))))))

(define-public crate-blurhash-0.2 (crate (name "blurhash") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gdk-pixbuf") (req ">=0.18, <=0.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req ">=0.23, <=0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req ">=0.23, <=0.24") (default-features #t) (kind 2)))) (hash "178w00qfdhibnx8liyjrzylrigl775wzz1xwqna4kwir63h7hnsn") (features (quote (("default")))) (v 2) (features2 (quote (("image" "dep:image") ("gdk-pixbuf" "dep:gdk-pixbuf"))))))

(define-public crate-blurhash-cli-0.1 (crate (name "blurhash-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "blurhash") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "0p9awzv1za8rd2pzifspjypcv5x1z5r7i7nd6pmx4c166wqvx5v4")))

(define-public crate-blurhash-cli-0.1 (crate (name "blurhash-cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "blurhash") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "1aaq3bn9za103ys33jaz3mykhzbqk6w5ilf4bchk6hs3p4hvapfj")))

(define-public crate-blurhash-fast-0.1 (crate (name "blurhash-fast") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0fzjksb5bl8ci6x7jxcwha25kkzvr5mdymk33f5ndac79g8j4dn6")))

(define-public crate-blurhash-ng-0.1 (crate (name "blurhash-ng") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.23.13") (default-features #t) (kind 2)))) (hash "0wq516fn2nnys8ysg50jam9kwd4hj7zif6z5ycjal5inh68hjax0")))

(define-public crate-blurhash-update-0.1 (crate (name "blurhash-update") (vers "0.1.0") (deps (list (crate-dep (name "blurhash") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 2)))) (hash "1s4fz6dqnag05p8yjwvwsmrncjk7rx7fcd286gfz44mrmr2ymddz")))

(define-public crate-blurhash-wasm-0.1 (crate (name "blurhash-wasm") (vers "0.1.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "13in7mdg8qprr9n4n2rxmmmm40wv3zcfnd2inrb50sb3pr2qgv3i") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-blurhash-wasm-0.2 (crate (name "blurhash-wasm") (vers "0.2.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "19grd6m7y0680yql7bv8cps0ms66wdl08xjn3fz4zrqd352966iv") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-blurit-0.1 (crate (name "blurit") (vers "0.1.0") (hash "15bgpgni27m65kbhaj4kx35xbgdvnxlpzk2diaxcm984wn4idqgf")))

(define-public crate-blurmac-0.0.1 (crate (name "blurmac") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wh1pdng3zq37ws3qa44g0lcsci3z7arv7dz4qhgnlpnncc3gbvj")))

(define-public crate-blurmock-0.1 (crate (name "blurmock") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1932afv4vxnkrsja65b4rlk6f3yjj4a7y4amp6q4c4jcapxjkj9w")))

(define-public crate-blurmock-0.1 (crate (name "blurmock") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cf20n771qgh4w4d6b0inp6d8g04n19fkdqnm7h52ydwf9rlq0y3")))

(define-public crate-blurmock-0.1 (crate (name "blurmock") (vers "0.1.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1nm8dh9gikl05hw4yc36qc4d5cg2z166qdnx7cyhzd1v7bd75pb8")))

(define-public crate-blurmock-0.1 (crate (name "blurmock") (vers "0.1.3") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "1m3mqdjiqnc7ypy4wx5f3fpyh1s5g0551gcvj4hx23w32zb0y5cw")))

(define-public crate-blurple_hook-0.1 (crate (name "blurple_hook") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)))) (hash "0h6alsrxbxx73xzimni3n54w0xiga8h4p2i3sbywn520qd64lxc0")))

(define-public crate-blurple_hook-0.1 (crate (name "blurple_hook") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)))) (hash "01zwka1ah6j9rx2jynqvc9af60l9lxv00272zws4s5nprsgwafac")))

(define-public crate-blurple_hook-0.2 (crate (name "blurple_hook") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11b10lhhnw9b42j0n3kkgk4v4npiqds6aq1pbkaw8wh7axk7pnwh")))

(define-public crate-blurple_hook-0.3 (crate (name "blurple_hook") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "0kmhlw71cx7i78s8nmrdgpvgw2y8z66z91ma9a23g5zby5r1409n") (v 2) (features2 (quote (("queue" "dep:tokio"))))))

(define-public crate-blurple_hook-0.3 (crate (name "blurple_hook") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "03kkh35x29gp84z2cr9v88clk8wk70prpza10pxlydzynzxw3w0a") (v 2) (features2 (quote (("queue" "dep:tokio"))))))

(define-public crate-blurple_hook-0.3 (crate (name "blurple_hook") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "0lpyaamfma8gymhd819szddh3h1xhzksj1fvl76hfgamxkyqap32") (v 2) (features2 (quote (("queue" "dep:tokio"))))))

(define-public crate-blurple_hook-0.3 (crate (name "blurple_hook") (vers "0.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "0c5ypplj9lnbpiy90jkg262psh1bga8ifhppk8269zfmcknk1ysa") (v 2) (features2 (quote (("queue" "dep:tokio"))))))

(define-public crate-blurry-0.1 (crate (name "blurry") (vers "0.1.0") (deps (list (crate-dep (name "crunch") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "glow") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.29.1") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "ttf-parser") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "0fi0k2dsfaa1gav11mpvx5l0nah8szb2n4m3s0j6xh6qj1x27q2m")))

(define-public crate-blurry-0.2 (crate (name "blurry") (vers "0.2.0") (deps (list (crate-dep (name "crunch") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "glow") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.29.1") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)) (crate-dep (name "ttf-parser") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "1cqk8vcnl43pnnpmadmw5nfb92nlimbh2d7zxdg7my60parljbp6")))

(define-public crate-blurslice-0.1 (crate (name "blurslice") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0") (default-features #t) (kind 2)))) (hash "04qg8dxq06ik19h2dx1x58w1ix1w4xs68qbh388dlcc4dm2w2wxd")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.0") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0fjkdpwdyp8qmpbp9igi2z411pdxvccbyxiwbgd7klvam31qvlsi")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.1") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x5izay2fy3d163nh2a7yzasf676prvyr4z80gxcrmsq9f7inlak")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.2") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wx9h1wcn245skydcyk6hxphxij9f55vnfx4ay0p0wbm2kjwpqwl") (yanked #t)))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.3") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "07xyx3srq8s6pz3vjmhxpgspa3jh4fc9y72im4nm02ck8vzyqa0g")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.4") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1illakfga0svc4wln5d2sb4knjmzflzpr6kxvinkxmcxl2pglwwp")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.5") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "14zfs6bmq667d7sarpfiah8ak9ab9f5ya99djpdgxazlq77b58xc")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.6") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lk919x4cjqadmffy4wwy5fzpff4x1lf6gw4v6yqi1xbk916bc1a")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.7") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1v84h1agak3i48sc668nr44whzwviwg5dbbpbw9qvxdpdppvr3x1")))

(define-public crate-blurz-0.1 (crate (name "blurz") (vers "0.1.8") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "06ka35ahqhz7v8wfdsjk91a0s8r2zrr7qqc69dibqii83n938sqp") (yanked #t)))

(define-public crate-blurz-0.2 (crate (name "blurz") (vers "0.2.0") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1p3kywcrjj2c5mf0xnc3sg0xfrbxc6b41nyvc5br59igqrm4y4wn")))

(define-public crate-blurz-0.2 (crate (name "blurz") (vers "0.2.1") (deps (list (crate-dep (name "dbus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "02160zv8sghxkyv6bxvpqj3jk15bsipf91l65dmmy6ssimn7jjad")))

(define-public crate-blurz-0.2 (crate (name "blurz") (vers "0.2.2") (deps (list (crate-dep (name "dbus") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wsbp7viwdvka59fxfc6irqyc1zmyfbh01sifc23miki9h7xlfz7")))

(define-public crate-blurz-0.2 (crate (name "blurz") (vers "0.2.3") (deps (list (crate-dep (name "dbus") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02s77mf63g9ajx8095s0whvnkmw9srp3jr4zl0bpcxzzmg2sysp4") (yanked #t)))

(define-public crate-blurz-0.2 (crate (name "blurz") (vers "0.2.4") (deps (list (crate-dep (name "dbus") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "1j0967d1i7ij1n1jncjgizyv94b4vpzhjj1nsk3bk49cfh973d1s") (yanked #t)))

(define-public crate-blurz-0.3 (crate (name "blurz") (vers "0.3.0") (deps (list (crate-dep (name "dbus") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "02n5qfcg2hlzgq0vdm05a9m3wx85aq8s0a4ssbmfhzzngwryinpn")))

(define-public crate-blurz-0.4 (crate (name "blurz") (vers "0.4.0") (deps (list (crate-dep (name "dbus") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nga752pfi014kbdm7nn0cgvmxd3mik9jbb74jrp7lzis3s2vg5i")))

