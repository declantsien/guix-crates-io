(define-module (crates-io bl ee) #:use-module (crates-io))

(define-public crate-bleed-0.0.1 (crate (name "bleed") (vers "0.0.1-alpha.1") (deps (list (crate-dep (name "serialport") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1yxnih4d1mwijsmlbbdl2qr12iqc9j4cran3rvwrnilwji4zx38q")))

