(define-module (crates-io bl #{80}#) #:use-module (crates-io))

(define-public crate-bl808-pac-0.0.0 (crate (name "bl808-pac") (vers "0.0.0") (deps (list (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0ndf57kaz5dxb4mjlrqqpnq7sks91xda7rpjlqysmbh5wpng1ybq")))

