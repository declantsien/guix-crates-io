(define-module (crates-io bl eh) #:use-module (crates-io))

(define-public crate-blehr-0.1 (crate (name "blehr") (vers "0.1.0") (deps (list (crate-dep (name "bleasy") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "15v3jx7ijq1ylz7qyfdyjc4lyp3rx5v7s74nil0d9ig8mhimmpl8")))

