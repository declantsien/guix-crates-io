(define-module (crates-io bl ox) #:use-module (crates-io))

(define-public crate-blox-0.0.0 (crate (name "blox") (vers "0.0.0") (hash "01hik7b944g6qfnm68pday0h5wz4kr3l3y01xaz1fksnhl5128ih") (yanked #t)))

(define-public crate-bloxberg-0.1 (crate (name "bloxberg") (vers "0.1.0") (deps (list (crate-dep (name "capstone") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0qmiwldza1zdh84r0x2skrc679fcm25aq8h2pf6vbdvkxqfq202f")))

