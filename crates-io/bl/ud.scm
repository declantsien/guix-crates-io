(define-module (crates-io bl ud) #:use-module (crates-io))

(define-public crate-blud-0.0.0 (crate (name "blud") (vers "0.0.0") (deps (list (crate-dep (name "fimg") (req "^0.4.22") (kind 0)) (crate-dep (name "iai") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "umath") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "0j3g5g8qxbjp7m6dl9gxyqyylm6zypcmgpw3hdvj84ns1l0qxfkj")))

(define-public crate-bludev-0.1 (crate (name "bludev") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0mmwh3lj0yxnax0h567mvrgm3s5v2l5ii57ppafc77bqysmbqjjg")))

