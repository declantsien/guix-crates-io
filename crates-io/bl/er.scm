(define-module (crates-io bl er) #:use-module (crates-io))

(define-public crate-blerp-0.1 (crate (name "blerp") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0b10j846ygj2hj9k66y9c7jzycjc94lh5rdffx5bii44312krdy3")))

