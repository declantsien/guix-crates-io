(define-module (crates-io bl im) #:use-module (crates-io))

(define-public crate-blimp-0.1 (crate (name "blimp") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.13.2") (features (quote ("toml"))) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1dyn3233qk5g1kbrh2qhy0s51wlp6yrgvdmmr1fas1ki2ghkld9j")))

(define-public crate-blimp-0.1 (crate (name "blimp") (vers "0.1.1") (deps (list (crate-dep (name "config") (req "^0.13.2") (features (quote ("toml"))) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1p2naagvcjcwy9xcklf499wwlhq0h9i0dygvcmdll6s9f3rgqz86")))

(define-public crate-blimp-0.1 (crate (name "blimp") (vers "0.1.2") (deps (list (crate-dep (name "config") (req "^0.13.2") (features (quote ("toml"))) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0m8hp299ggishcy5x320hhm9jlc5xw2443f4hnvh4l0fdw6yzp0f")))

(define-public crate-blimp-0.1 (crate (name "blimp") (vers "0.1.3") (deps (list (crate-dep (name "config") (req "^0.13.2") (features (quote ("toml"))) (kind 0)))) (hash "0kyx5lslyn7s3l4xpnph27l8avkk115b8xsvrasd96ynil59hm3b")))

(define-public crate-blimp-0.1 (crate (name "blimp") (vers "0.1.4") (deps (list (crate-dep (name "config") (req "^0.13.2") (features (quote ("toml"))) (kind 0)))) (hash "0br0hsyc6h0h21hszfbb2mdh4fgc2dq2a6b5nar18lg2qkjm308y")))

(define-public crate-blimp-0.1 (crate (name "blimp") (vers "0.1.5") (deps (list (crate-dep (name "config") (req "^0.13.2") (features (quote ("toml"))) (kind 0)))) (hash "0hf5fz5bg5hcd6cd6smm7fa1a4pz2nyvzbxjf819k7a3219vw3gw")))

(define-public crate-blimp-0.1 (crate (name "blimp") (vers "0.1.6") (deps (list (crate-dep (name "config") (req "^0.13.2") (features (quote ("toml"))) (kind 0)))) (hash "1qcamy71blw512jvrgj9phyx7xgkdirizgkwn1zxcjfi9nvy75g0")))

