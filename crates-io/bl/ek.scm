(define-module (crates-io bl ek) #:use-module (crates-io))

(define-public crate-blek-0.1 (crate (name "blek") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "tera") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0az1aig7hsmxay5b2zf12b6w18sqzmac4nw1japjjyaaq9v5iqqf")))

