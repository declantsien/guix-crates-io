(define-module (crates-io bl ai) #:use-module (crates-io))

(define-public crate-blair_mountain-0.1 (crate (name "blair_mountain") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xc3nbyld481sxlmvi8ifyra2wxjl4b1sd5n2lwwzl5x22hcs1r9")))

(define-public crate-blair_mountain-0.1 (crate (name "blair_mountain") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "12r44xxan3y2r20nc1xb6kg1bplhpscjjlcr235jjq0q0n3hkvxb")))

(define-public crate-blair_mountain-0.2 (crate (name "blair_mountain") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "11fjjf69g31xl2f8ndgg70l6z6dwpc3jk35kpbdqqd4kxa3kj69y")))

(define-public crate-blair_mountain-0.3 (crate (name "blair_mountain") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "00hlb9m3i5d1qin5snj4baxa4avd9443iqh2i6vmlf0b8swr8fgj")))

