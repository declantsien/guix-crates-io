(define-module (crates-io bl e-) #:use-module (crates-io))

(define-public crate-ble-central-0.1 (crate (name "ble-central") (vers "0.1.0") (hash "1qmvffkmkprr8glpcsli8fn5p5mzgdxk92jh2kplssxci28dn15v")))

(define-public crate-ble-data-struct-0.1 (crate (name "ble-data-struct") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_System_Threading" "Storage_Streams" "Foundation" "Devices_Bluetooth" "Devices_Bluetooth_Advertisement" "Foundation_Collections"))) (default-features #t) (kind 0)))) (hash "1xyxr799aj8a6rdmxjmd0zfvkwggbnhk8m9yfkcwrvcwaii0jrjq")))

(define-public crate-ble-data-struct-0.2 (crate (name "ble-data-struct") (vers "0.2.0") (deps (list (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_System_Threading" "Storage_Streams" "Foundation" "Devices_Bluetooth" "Devices_Bluetooth_Advertisement" "Foundation_Collections"))) (default-features #t) (kind 0)))) (hash "098dzg533ilf1arpxrnlaz2jmb87qk1za9wkxk2gwkhh9js4kfcx")))

(define-public crate-ble-data-struct-0.2 (crate (name "ble-data-struct") (vers "0.2.1") (deps (list (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.54.0") (features (quote ("Win32_Foundation" "Win32_System_Threading" "Storage_Streams" "Foundation" "Devices_Bluetooth" "Devices_Bluetooth_Advertisement" "Devices_Bluetooth_GenericAttributeProfile" "Foundation_Collections"))) (default-features #t) (kind 0)))) (hash "0b6bs6vs04zrhj3a5a1520bx7wp6fcc0kpm2qj3mnfvycphbnpzj")))

(define-public crate-ble-ledly-0.1 (crate (name "ble-ledly") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "btleplug") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0f736cfcrhci3h3kl2a4dl8p7a172912gn664aqxbrrkg19pq3r4")))

(define-public crate-ble-ledly-0.1 (crate (name "ble-ledly") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "btleplug") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0qm70ydarf66rhppycixzdf13f9lq2mljh6aizbd3zx5y2bxx8rd")))

(define-public crate-ble-ledly-0.2 (crate (name "ble-ledly") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "btleplug") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0wfjad61ppdmpw24zsa9xpkyx2g8zqw7n2cx3xid7cg83npbazg5") (features (quote (("sw_animate") ("light") ("hw_animate") ("default" "all") ("color") ("brightness") ("all" "light" "color" "brightness" "hw_animate" "sw_animate"))))))

(define-public crate-ble-ledly-0.3 (crate (name "ble-ledly") (vers "0.3.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "btleplug") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "enumflags2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0p10xbwp4j6ca5c134awa3d447mmql25i8xhh5ziqrkl8m7bz6wx") (features (quote (("sw_animate") ("light") ("hw_animate") ("default" "all") ("color") ("brightness") ("all" "light" "color" "brightness" "hw_animate" "sw_animate"))))))

(define-public crate-ble-mesh-0.1 (crate (name "ble-mesh") (vers "0.1.0") (hash "12qsr0z7xgca7ycyij3an3x7sy01ch7ga2f22l24abk187v168d2")))

(define-public crate-ble-mesh-0.1 (crate (name "ble-mesh") (vers "0.1.1") (hash "0dx7lcpc43270h0l7qqfrasv420ghrcsd65r6zqqlls17xlxix7n")))

(define-public crate-ble-peripheral-0.1 (crate (name "ble-peripheral") (vers "0.1.0") (hash "0389sys1ykj1gvj84irgy57a8gfbynq7xpfb2zi1wkqs4ni364q8")))

