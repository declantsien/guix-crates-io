(define-module (crates-io bl ab) #:use-module (crates-io))

(define-public crate-blabber-0.1 (crate (name "blabber") (vers "0.1.0") (hash "1mni4x1p9qa05s0f1vhf8g0jxnxalm1xajaljbqdsa9a2hg4x95g")))

(define-public crate-blablabla-0.1 (crate (name "blablabla") (vers "0.1.0") (hash "0nhvhifc02x9bg3c9myz379wa35vcq7mn85igb4zhw1vddqikxvc")))

