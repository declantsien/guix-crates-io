(define-module (crates-io bl iz) #:use-module (crates-io))

(define-public crate-blizzard-0.0.1 (crate (name "blizzard") (vers "0.0.1") (hash "1r9vm1rv4qadplcjbxxm8ds03svs2zigwr567vnvk2kv9lr6mh8y")))

(define-public crate-blizzard-engine-0.1 (crate (name "blizzard-engine") (vers "0.1.0") (deps (list (crate-dep (name "blizzard-engine_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "blizzard-id") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0nxkasfm2azdy3rb6ic5lzwy5lb0vjwqyk6cpqi4wcpf09ln46d2")))

(define-public crate-blizzard-engine_derive-0.1 (crate (name "blizzard-engine_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19aw3smsddzn3j4syr36jh00m5gv3dbvja0fa9j6ysyjnscnkpkx")))

(define-public crate-blizzard-id-0.1 (crate (name "blizzard-id") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1q9ja4hap4p0asiiv6jizz96fwlh0dx13jiicihhcxm7mln91lg7")))

(define-public crate-blizzard-server-0.1 (crate (name "blizzard-server") (vers "0.1.0") (deps (list (crate-dep (name "blizzard-engine") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "blizzard-engine_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "blizzard-id") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f050c0azmjn00kl9wxw7afai1y02yn1aswjvdyqf4nxrrgdsfl0")))

