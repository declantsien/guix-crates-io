(define-module (crates-io bl t-) #:use-module (crates-io))

(define-public crate-blt-utils-0.1 (crate (name "blt-utils") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "14bp6gp1lwprwsvip6w50ngr3jkb9hvv0baylwpqfji3n19p1fsn")))

