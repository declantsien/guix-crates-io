(define-module (crates-io bl pa) #:use-module (crates-io))

(define-public crate-blpapi-0.0.1 (crate (name "blpapi") (vers "0.0.1") (deps (list (crate-dep (name "blpapi-derive") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blpapi-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0yngcddicg6jjyhhl0g0gl1y35zylnmgal11rlhp11bzh87fkbx0") (features (quote (("full" "blpapi-derive" "chrono") ("derive" "blpapi-derive") ("default") ("dates" "chrono"))))))

(define-public crate-blpapi-derive-0.0.1 (crate (name "blpapi-derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "087fb9l9q2xy3njvdz83gclqr8fr88xa4vqvf653vgnlqfw1kadh")))

(define-public crate-blpapi-sys-0.0.1 (crate (name "blpapi-sys") (vers "0.0.1") (hash "0icvi5dimwfbp002vl6ca8pf2dhidprhzi1lk2gl93ld45y33d7w")))

