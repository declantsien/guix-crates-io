(define-module (crates-io bl y-) #:use-module (crates-io))

(define-public crate-bly-ac-0.1 (crate (name "bly-ac") (vers "0.1.0") (hash "0jq3nrlns5w974b9yp1izwjxcpzil5kdslg8wcycn42yjaapmfz0")))

(define-public crate-bly-cairo-0.1 (crate (name "bly-cairo") (vers "0.1.0") (deps (list (crate-dep (name "bly-ac") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "cairo-sys-rs") (req "^0.17.0") (features (quote ("xlib"))) (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21.0") (default-features #t) (kind 0)))) (hash "12jalg9r17y1ibcz9gqal5qc3x83q750j1xax9kpxjcina5hyvfl")))

(define-public crate-bly-dx2d-0.1 (crate (name "bly-dx2d") (vers "0.1.0") (deps (list (crate-dep (name "bly-ac") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.28.1") (default-features #t) (kind 2)) (crate-dep (name "windows") (req "^0.38.0") (features (quote ("Foundation_Numerics" "Win32_Foundation" "Win32_Graphics_Direct2D_Common" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D11" "Win32_Graphics_Dxgi_Common" "Win32_System_Performance" "Win32_System_SystemInformation" "Win32_UI_Animation" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1vdya29n8wr89anz8s43nhzilzmpdisdi835rppjlmy982rlq187")))

