(define-module (crates-io bl sc) #:use-module (crates-io))

(define-public crate-blsctl-0.2 (crate (name "blsctl") (vers "0.2.0") (deps (list (crate-dep (name "serial_test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serial_test_derive") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "0s175isjlmxihc8ia5msfd74ykx39ngdhx629m43cbj5609d4wyl")))

(define-public crate-blsctl-0.2 (crate (name "blsctl") (vers "0.2.1") (deps (list (crate-dep (name "serial_test") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test_derive") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "085jmq5dkas9qnd6qxkkbfhl2mr04mjihvwlpni9si7cyz9664nx")))

(define-public crate-blsctl-0.2 (crate (name "blsctl") (vers "0.2.2") (deps (list (crate-dep (name "serial_test") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test_derive") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0pcx0agj4sl7q97dy5f9hrfwymznwkh31xqjkwddv6dn5ac8xp49")))

(define-public crate-blsctl-0.2 (crate (name "blsctl") (vers "0.2.3") (deps (list (crate-dep (name "serial_test") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serial_test_derive") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0i75gp72ib1qw479y107x700p9vafmafmy1xvykyfryra90m1yv5")))

