(define-module (crates-io bl fi) #:use-module (crates-io))

(define-public crate-blfilter-0.1 (crate (name "blfilter") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (kind 0)) (crate-dep (name "farmhash") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (optional #t) (kind 0)))) (hash "135ffd74bn8chrv811yq3qlsi9cknz4c7m5rg49ghcx3iccx5waf") (features (quote (("std" "bit-vec/std" "bit-vec/serde" "serde/std") ("default" "std"))))))

(define-public crate-blfilter-0.2 (crate (name "blfilter") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "farmhash") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (optional #t) (kind 0)))) (hash "0c8j483v46plcid0azxln2df0dvv0j0334ns5ikr2vmx9mp4rpdp") (features (quote (("std" "serde/std" "serde/derive") ("default" "std"))))))

