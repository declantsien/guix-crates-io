(define-module (crates-io bl ct) #:use-module (crates-io))

(define-public crate-blctl-0.1 (crate (name "blctl") (vers "0.1.0") (deps (list (crate-dep (name "rust-ini") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serial_test_derive") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "0hbbnqfrl6b5jp11crksdp30gz54sqqdwh5xr1zi3rvkaixp0bgr")))

(define-public crate-blctl-0.1 (crate (name "blctl") (vers "0.1.1") (deps (list (crate-dep (name "rust-ini") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serial_test_derive") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "03ciygyl73fwzf2igwhwhw6ry1zv4rzmgdzisl3bps5n460ykdh9")))

(define-public crate-blctl-0.1 (crate (name "blctl") (vers "0.1.2") (deps (list (crate-dep (name "rust-ini") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serial_test_derive") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "1jnz9hvp9ddn2q05s39j2dba6h29xa8ixj5bx424w228dwj2z2qp")))

