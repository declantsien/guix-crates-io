(define-module (crates-io od ys) #:use-module (crates-io))

(define-public crate-odysseus-0.0.0 (crate (name "odysseus") (vers "0.0.0") (hash "0spgzfnp5qdlmsb8wgw2m8ljqvf8xfr1yr25qfg9i6cvllp3kady") (yanked #t)))

(define-public crate-odysseus-0.0.1 (crate (name "odysseus") (vers "0.0.1") (hash "10wz74ifgrpgl9v4smgdxlir8vssi0izl5clx620wl65739pyjw4") (yanked #t)))

(define-public crate-odysseus_codegen-0.0.0 (crate (name "odysseus_codegen") (vers "0.0.0") (hash "0mm1zvns9ldfk205s263s3mgwfprqp9hi9alqckpsr3jczwgcm84") (yanked #t)))

(define-public crate-odysseus_codegen-0.0.1 (crate (name "odysseus_codegen") (vers "0.0.1") (hash "14h6lalaa5jh014vc5vy2g60pmpyfgp60zr8qks4qy2cx2l183c1") (yanked #t)))

(define-public crate-odysseus_codegen_impl-0.0.0 (crate (name "odysseus_codegen_impl") (vers "0.0.0") (hash "0sbl525gf8avh8j0pilfajnas77s3ym5x0b42ypcw9q52bcwppqc") (yanked #t)))

(define-public crate-odysseus_codegen_impl-0.0.1 (crate (name "odysseus_codegen_impl") (vers "0.0.1") (hash "1bawdx4c9dzcmjlygcm6dp4xmqp39sd47a81ldg1fbrmlghylrzk") (yanked #t)))

(define-public crate-odyssey-0.0.0 (crate (name "odyssey") (vers "0.0.0") (hash "04izhap7n4yz560b1dvgy51vy3qzjk912xm5fhjw3ra5y4k34yzp") (yanked #t)))

