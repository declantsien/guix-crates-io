(define-module (crates-io od _o) #:use-module (crates-io))

(define-public crate-od_opencv-0.1 (crate (name "od_opencv") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.66") (features (quote ("dnn" "imgcodecs" "imgproc"))) (kind 0)))) (hash "11n401qjvf1rz9mabp4nj1j3h65jj2dqlr92g0v6i1b9d5wf9l4k")))

(define-public crate-od_opencv-0.1 (crate (name "od_opencv") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.66") (features (quote ("dnn" "imgcodecs" "imgproc"))) (kind 0)))) (hash "13k62r0dz61l0scpxi5ri7i8c9v7jz1yqs49g0y30q7l5627fynn") (rust-version "1.73")))

(define-public crate-od_opencv-0.1 (crate (name "od_opencv") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.66") (features (quote ("dnn" "imgcodecs" "imgproc"))) (kind 0)))) (hash "10zv1gq1lc9ssbc3f6sp5y9s1mxjlxd4qpffvky3ln3ki2a1hhkq") (rust-version "1.73")))

(define-public crate-od_opencv-0.1 (crate (name "od_opencv") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.66") (features (quote ("dnn" "imgcodecs" "imgproc"))) (kind 0)))) (hash "1lwiz3fkj3jczfvk26k82ycpkhldvkhyibsyal0kdzmahdqz4n67") (rust-version "1.73")))

(define-public crate-od_opencv-0.1 (crate (name "od_opencv") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.88.5") (features (quote ("dnn" "imgcodecs" "imgproc"))) (kind 0)))) (hash "1c8vw0p2zykjpli578cmpjzg53dcgwg29v6c84g9qdjfmn6b3wgy") (rust-version "1.73")))

(define-public crate-od_opencv-0.1 (crate (name "od_opencv") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.91.3") (features (quote ("dnn" "imgcodecs" "imgproc"))) (kind 0)))) (hash "1y6slvjgpymxxmnx0kianz25k1vgsj30p8489i7krhd0bc0jgsac") (rust-version "1.73")))

