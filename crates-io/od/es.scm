(define-module (crates-io od es) #:use-module (crates-io))

(define-public crate-odesli-rs-1 (crate (name "odesli-rs") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("gzip" "json" "deflate"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00yfvlzmmz3qvx7jlkq9bvpmah36bzx6kcv68jrsydrcydww6k4n")))

(define-public crate-odesli-rs-2 (crate (name "odesli-rs") (vers "2.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("gzip" "json" "deflate"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jw8fbwajb51xfdwadxz1ih272f596q0vq1y2jslypmbs3r1f8x0")))

(define-public crate-odesli-rs-3 (crate (name "odesli-rs") (vers "3.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("gzip" "json" "deflate"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hi67pwfnhskqbzmm9w9296bwdy3vx6f76xsnxd8091bbwzl1rx2")))

(define-public crate-odesli-rs-4 (crate (name "odesli-rs") (vers "4.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("gzip" "json" "deflate"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "13g65xxhrk2y28ihdn47l2k616fxgk87cd4d9s5sncigqs90kxqw")))

(define-public crate-odesli-rs-4 (crate (name "odesli-rs") (vers "4.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("gzip" "json" "deflate"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "09bw0r1zva0vjq7nkz19p1bpcgs8sqliy3rxppw6lb9ykgl0zyyd")))

(define-public crate-odesli-rs-4 (crate (name "odesli-rs") (vers "4.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("gzip" "json" "deflate"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zaiac3vivla17030vxcqqprlswby6ldb2933h31ig2gy4lqhj65")))

(define-public crate-odesli-rs-5 (crate (name "odesli-rs") (vers "5.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("gzip" "json" "deflate"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16yi5m1a7p0sd72ykgdjr39p1srbdacnras1d3cwy13yii218ipc")))

(define-public crate-odesolver-0.1 (crate (name "odesolver") (vers "0.1.0") (hash "0n8vsisp382nrzpp13496zgamvlagzsk14imnc2jirz6fff52nf6")))

(define-public crate-odesolver-0.1 (crate (name "odesolver") (vers "0.1.1") (hash "0qlkrk3nl8ddychpmha9avak0h6yp6257m1ggv5drh4mnamasgzb")))

(define-public crate-odesolver-0.1 (crate (name "odesolver") (vers "0.1.2") (hash "16kankyl5ra4b3lbsafm17nch3r3galnfwm5z792d61dc7mqcsjm")))

(define-public crate-odesolver-0.1 (crate (name "odesolver") (vers "0.1.3") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "0ndgn640v4gfpq99p6d73lbrsvi8737nhyvki07r48lp2q4pm94m")))

(define-public crate-odesolver-0.1 (crate (name "odesolver") (vers "0.1.4") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "11cc9ggfzjhwbhjwp5w8xyiix6bi8qw3cv2v991x5djp70v2jnd9")))

(define-public crate-odesolver-0.1 (crate (name "odesolver") (vers "0.1.5") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "1b88kyylrzc8sh2yy93wpg6md1ikc6ypmnw7dpkjqdca1yd0z6dw")))

