(define-module (crates-io od di) #:use-module (crates-io))

(define-public crate-oddio-0.1 (crate (name "oddio") (vers "0.1.0") (hash "12sy229kv5lqhxl7gkm8l7xicqyn7i9m95rv61bjb83mx5m2szy0")))

(define-public crate-oddio-0.1 (crate (name "oddio") (vers "0.1.1") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^0.4") (default-features #t) (kind 2)))) (hash "0jmr7drxavhnh3abzgaxpk4knyn1i24ps4fybidcj4jzg7yzjqx1")))

(define-public crate-oddio-0.2 (crate (name "oddio") (vers "0.2.0") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^0.4") (default-features #t) (kind 2)))) (hash "1vk5a6b2n0mm78lmkh6qxm7gsg3h3wg6a4rhgsdy92armnaq174b")))

(define-public crate-oddio-0.3 (crate (name "oddio") (vers "0.3.0") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^0.4") (default-features #t) (kind 2)))) (hash "1i2ijqr5mm7w2mi28qmf04w4vsy8cjv5fggnypd6l0a0msd7s7pp")))

(define-public crate-oddio-0.4 (crate (name "oddio") (vers "0.4.0") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "11drb1lyh8rxlv8mwwkd8vsm9yqrdiak33aa7kh7fsj2kxzzwn2k") (yanked #t)))

(define-public crate-oddio-0.4 (crate (name "oddio") (vers "0.4.1") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0yiq934alww5vpg830d2fvbfhwl7083sgb4rp3f56bssp2314r7f")))

(define-public crate-oddio-0.5 (crate (name "oddio") (vers "0.5.0") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "081ydx0mqgc9zlakpnspwwqi8kzir6yiqx525fq24qzj52hxbxik") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.6 (crate (name "oddio") (vers "0.6.0") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0k07px8dl025swd8qs1vkdlk2jnqary3z03d5x9xgmkjc2irazzj") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.6 (crate (name "oddio") (vers "0.6.1") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1wlg08mvqa1sqsv23gsn59sa3g1h0wx3039ji9yr155sdsprcbcl") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.6 (crate (name "oddio") (vers "0.6.2") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0d67n2hzlij6zq774fw4jrlc9bzsfvr11qx0sjqd0m78qyf3gwzh") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7 (crate (name "oddio") (vers "0.7.0") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1aiq9qfyfffyh5a7qlk8psv4cb6nmd4w5fnlxi7k73vm53lhv6x8") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7 (crate (name "oddio") (vers "0.7.1") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1ymbdzk5vlxkvmwr2wy1pd5ky0kfick432mqaq0c11y8mw4zvdpm") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7 (crate (name "oddio") (vers "0.7.2") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1zn83kq0dzzcq5d6nb4zzk8lr8gi074rwiqs4xhpxms0xyk865d6") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7 (crate (name "oddio") (vers "0.7.3") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1b4ancjab4rqnbkyr56i1xgqy4fhldgf42cslskbihjm5xw0i22s") (features (quote (("no_std" "libm"))))))

(define-public crate-oddio-0.7 (crate (name "oddio") (vers "0.7.4") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1wjc6k5gy6ski7x5rb8ndzgbvm26gaccmbargvc3px8i0nvb51y2") (features (quote (("no_std" "libm"))))))

(define-public crate-oddits-0.0.0 (crate (name "oddits") (vers "0.0.0") (hash "1fgx5w0s90wi3ky70vb1d9zchild7qhk4abrilgv8ayw8sgjzq4v")))

