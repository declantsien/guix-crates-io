(define-module (crates-io od ds) #:use-module (crates-io))

(define-public crate-odds-0.1 (crate (name "odds") (vers "0.1.0") (hash "1k20rzn0a8fqhbfr454l51ykaifjd6ivbx341ix3v9anq1m2h6a5")))

(define-public crate-odds-0.1 (crate (name "odds") (vers "0.1.1") (hash "1qa5qgswfd323dvx675h3g1rydfwp3vcw0hyyh0rdmznrfddgv5j") (features (quote (("unstable"))))))

(define-public crate-odds-0.1 (crate (name "odds") (vers "0.1.2") (hash "1whcqzhi9bbnck58nqrfwnqqhpldhvcql4jc4zn8wxxgj4rcbypz") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.0") (deps (list (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1flj6vx197dmiwyqdjhjnzwi08lxhyc5pfy6iw06h4352h34l5zq") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.1") (deps (list (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0a905180c948n9kbs2sjrdaj6in21idbdxja80wm38i7baw41lbk") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.2") (deps (list (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0zadhkrs2rr02rkczvb3c9fvdg3dqnv3p0sxsgklmpkhrwmi0cc3") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.3") (deps (list (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1pdff5l12yj74g8cm76nk6dcw4pl6x032hn9pvkz7nwvgkcsabff") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.4") (deps (list (crate-dep (name "itertools") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "08i1z99y2526p82jsnc03lg62wjqydjd6akf8is32f7i5qzz9ija") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.5") (deps (list (crate-dep (name "itertools") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0das06adsylhzmc2k9kr2s42zih799c88mr1m22rx5fliy6vf0ca") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.6") (deps (list (crate-dep (name "itertools") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "18wca2kw661cqrr268kwcndynzczg55llga1qxzflqbbzanqh251") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.7") (deps (list (crate-dep (name "itertools") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1cs3s9h6yycgsikjw9rxhvk966mkb0bac8nsgw4xhs0hb1chpkfl") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.8") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "19vhclxkjzr68z46x4ndi4xg07v0r9a0hpa4abln98j6hn425l7n") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.9") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1k48x0gr4kxc7c5cn769g4d31mc3g04b8sp9xg410qa6iqx1qhl9") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.10") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1gh7l1x5hyq8ws9jkbycmcb3z1afr1lwdxa4pcxs6n1gqfd2hjsf") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.11") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)))) (hash "0zsdninapmx1m4nslklf39n2dh6m71bpvnv8v66zbwgk1spfhs0g") (features (quote (("unstable"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.12") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)))) (hash "1hcfqw8dqg0dk31g8sbl0vlbsjg8bx5kkmhmshi92y0g3gl0d35j") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.13") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)))) (hash "1rpgznqjw8hy79az061k5x22kw4wimqzw4q8wfvgk4rspmyah5cm") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.14") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)))) (hash "0j7ad14i6cbxz97yj3jrr06a3m8vmsvnvyc9kwgllqq44rks47xy") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.15") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)))) (hash "0sgjn6kvh6bv46a4k94w72iwfpw8zp23x9sghyccks1s9r6b9bg2") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.16") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)))) (hash "0h5zjckgarik2mv2j0q6434zi7g33p1rdzrpmlcy6xhnxkyiqw7k") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.17") (deps (list (crate-dep (name "itertools") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "03kvsd7iykjclwwk252rxsrxj6cd7lz6d5nig94wih0d5m192c5c") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.18") (deps (list (crate-dep (name "itertools") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "0qjwdpywi9ak6487drgz4l30pbpjb4pc2k59jjn6k9klwa5rnl4m") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.19") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "1rz3f9z3j491w7h1468y9il7xqin85kbi3vac8xszfi7biqjxc98") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.20") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "1ilj97jyv3s2hf1262p82gpgn88fizk3sl255j3jz8l36bbmxdfw") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.21") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "1vaa6zfkcn8ds23swsjgslb8aks8s0n4kdhaggpfryp1xy97h3dj") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.22") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "1c7g0lcnn8vjq47zffdk9924pz12g3wan140pv2a4rd1r8ibk3ih") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.23") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "181k5nkqdwkfjd18458f88mz0dh1liajb3sdig2wh71z5fk30ip0") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.24") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "03gw4hqsy6kar8wanw37y4ydb3nxv0infyjgs505g59l4z0xgclp") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.25") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "1fpwxqs5f5p62mh6v4yh032akwwx5xz0pagsqfhs7klq09rrppy3") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.2 (crate (name "odds") (vers "0.2.26") (deps (list (crate-dep (name "itertools") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "08pvngx0nf7yl9cgk4bahn1a0s8na5g9knbhq7y29kysp58h3bjf") (features (quote (("unstable") ("std") ("default" "std"))))))

(define-public crate-odds-0.3 (crate (name "odds") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (kind 2)) (crate-dep (name "rawpointer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rawslice") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unchecked-index") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1560gcb7bg95g53naha7c9xal72jicl1i4sx9p5h3xzi8c31lr7p") (features (quote (("unstable") ("std-vec" "std") ("std-string" "std") ("std") ("docs" "std-string" "std-vec") ("default"))))))

(define-public crate-odds-0.3 (crate (name "odds") (vers "0.3.1") (deps (list (crate-dep (name "itertools") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (kind 2)) (crate-dep (name "rawpointer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rawslice") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unchecked-index") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0rdnxa0na4897yb0svb3figz35g4imxjv61yfm2j21gbh5q8v8d9") (features (quote (("unstable") ("std-vec" "std") ("std-string" "std") ("std") ("docs" "std-string" "std-vec") ("default"))))))

(define-public crate-odds-0.4 (crate (name "odds") (vers "0.4.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rawpointer") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rawslice") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unchecked-index") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "17jd5fjcrlya7dbnnj0v8s83l3jhlajyljmkcy49pxsvxj9zdsdz") (features (quote (("unstable") ("std-vec" "std") ("std-string" "std") ("std") ("docs" "std-string" "std-vec") ("default"))))))

