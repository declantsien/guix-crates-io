(define-module (crates-io ns vg) #:use-module (crates-io))

(define-public crate-nsvg-0.1 (crate (name "nsvg") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0qg52xv66iwyy71x3xaziwjv5lzrhjk171ndwx95mc3vqsabxb7a")))

(define-public crate-nsvg-0.2 (crate (name "nsvg") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "02i3f935ndcxc0g2lm1i6fnlxbl54z48x379xlc4cp58ihv925hq")))

(define-public crate-nsvg-0.2 (crate (name "nsvg") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0s3sj2dnvj7mk5cvnb9rz4fj7k9f3b7cd56rm9wrnswagzl0g97y")))

(define-public crate-nsvg-0.3 (crate (name "nsvg") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1d4jkpf6jqkijjv7fbp62jz9jx8v96zkxfhass782d35aqk126ks")))

(define-public crate-nsvg-0.3 (crate (name "nsvg") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0ky7vkl7cc75bdvkxmmia00mmccpqd5djnbc59vamd4ng9pj5nrg")))

(define-public crate-nsvg-0.3 (crate (name "nsvg") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "008531z7pf4a5lrc1i5vpcs6hv50swgjbq594bs0i68kzn1mkii5")))

(define-public crate-nsvg-0.4 (crate (name "nsvg") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1lfyi1d2kyp8cbk9sxqig6v2j4ym2zh2yskwy75abicwvhsf5d67")))

(define-public crate-nsvg-0.5 (crate (name "nsvg") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "09gfqccm3ciq7h3y405dkqi28la15ip3ll6x6mkgi5fb8v6pqpjk") (features (quote (("default" "image"))))))

(define-public crate-nsvg-0.5 (crate (name "nsvg") (vers "0.5.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0861ih38pl8bhjvgh7cikwkrd3d9hhh4ac3a3jq81jh5kha51yhb") (features (quote (("default" "image"))))))

