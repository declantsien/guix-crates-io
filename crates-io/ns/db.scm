(define-module (crates-io ns db) #:use-module (crates-io))

(define-public crate-nsdb_secret-0.1 (crate (name "nsdb_secret") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "0a7by6w22m1sc6hynj4j5ia5014nqpvm3dqw0fbg8dcx7x3hmy33")))

(define-public crate-nsdb_secret-0.1 (crate (name "nsdb_secret") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "1ys28r3vzwslcy9s7739kms1zmxwzzkgyfil9h93qf0d4wqwxp3z")))

(define-public crate-nsdb_secret-0.1 (crate (name "nsdb_secret") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "1bk00yv2s4409mjhxbarha0vqsbixq16kavq5013p88xhd66wra8")))

(define-public crate-nsdb_secret-0.1 (crate (name "nsdb_secret") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "0wi5nfx5bxyn1n49yprlr37xjny8i1lb5la3r70ng3b8pq38m3np")))

(define-public crate-nsdb_secret-0.2 (crate (name "nsdb_secret") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "16lh00fxjg696r4drpl4wwwdqkacmgh0rv4cbcqvii74z7k61fi6")))

