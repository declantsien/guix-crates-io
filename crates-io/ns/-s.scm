(define-module (crates-io ns -s) #:use-module (crates-io))

(define-public crate-ns-scylla-orm-0.1 (crate (name "ns-scylla-orm") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ciborium") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "scylla") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)))) (hash "0vwn3709nb7b4cn6j78gih9nqwgd9f5d8yqvgmn3ysqy5rvjhb1g") (rust-version "1.64")))

(define-public crate-ns-scylla-orm-macros-0.1 (crate (name "ns-scylla-orm-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "2.0.*") (default-features #t) (kind 0)))) (hash "19disf86dks8qmjz4vqdhcri9c4d0sa01sb62bz318rsiz7rr8xm") (rust-version "1.64")))

(define-public crate-ns-std-threaded-0.1 (crate (name "ns-std-threaded") (vers "0.1.0") (deps (list (crate-dep (name "abstract-ns") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1dnrfr9by39iffya3f58wlz3ab2mjfbs30pn3pc7xv6yv96b4apv")))

(define-public crate-ns-std-threaded-0.2 (crate (name "ns-std-threaded") (vers "0.2.0") (deps (list (crate-dep (name "abstract-ns") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0pg95lfqzzd2fqhgsqpjz9rfmiw62zrhawzkwpi8xdwqv2fy9qpw")))

(define-public crate-ns-std-threaded-0.3 (crate (name "ns-std-threaded") (vers "0.3.0") (deps (list (crate-dep (name "abstract-ns") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "01p7r2qiqmwrmq7wysm85bb9slv84gyr83h73vqdp2szn3kwsax7")))

