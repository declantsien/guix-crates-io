(define-module (crates-io ns f-) #:use-module (crates-io))

(define-public crate-nsf-imgui-0.1 (crate (name "nsf-imgui") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nsf-imgui-raw") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0iwdl50h4qvvg01cr0c2cha5bmjpmfsyzaw5mi3x30hvhqrzfbyc")))

(define-public crate-nsf-imgui-0.1 (crate (name "nsf-imgui") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nsf-imgui-raw") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0nl0asyqxfkw5rz06balpyixssc704zhhddw031f6dvq8x70ds6k")))

(define-public crate-nsf-imgui-0.1 (crate (name "nsf-imgui") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nsf-imgui-raw") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1aiqnqkpf70gf11p7s79slzvcfq3xfw6ysjairs74iq08n4hi1ln")))

(define-public crate-nsf-imgui-0.1 (crate (name "nsf-imgui") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nsf-imgui-raw") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "046kyb1ikck7wrzmxpdh4ljh2pkiccbia6502dxyc97qs8qljaaf")))

(define-public crate-nsf-imgui-raw-0.1 (crate (name "nsf-imgui-raw") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)))) (hash "1jxkpkwdyk0z18scby7rsiapx9ylbdm1d7gkskx23m9may2jqkmr")))

