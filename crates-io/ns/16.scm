(define-module (crates-io ns #{16}#) #:use-module (crates-io))

(define-public crate-ns16550a-0.1 (crate (name "ns16550a") (vers "0.1.0") (hash "1knjbf20ap2cv8bhnq134z4rwndpl0kz3xlhcy3lyppqqmvzlp69")))

(define-public crate-ns16550a-0.2 (crate (name "ns16550a") (vers "0.2.0") (hash "13a50lm1jm1j7gv0s9y3kbkvb0w3g772kh0igi2f0wg5h6vn9153")))

(define-public crate-ns16550a-0.3 (crate (name "ns16550a") (vers "0.3.0") (hash "1b9j1gnr8knjxsx5b0pqvb1xg76q3dy4kmw6zazcj479hvr920z3")))

(define-public crate-ns16550a-0.4 (crate (name "ns16550a") (vers "0.4.0") (hash "19j54n1sjm8ajqfc8jrchk08m28vlmngy5k5c0c5jxzlrzybszv6")))

