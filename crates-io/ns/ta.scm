(define-module (crates-io ns ta) #:use-module (crates-io))

(define-public crate-nstack-0.1 (crate (name "nstack") (vers "0.1.0") (deps (list (crate-dep (name "kelvin") (req "^0.15") (default-features #t) (kind 0)))) (hash "1b9h0n14jvhdlnghlckibkhfndv3iq6bqlxnlj1ivl6798zhdj76")))

(define-public crate-nstack-0.2 (crate (name "nstack") (vers "0.2.0") (deps (list (crate-dep (name "kelvin") (req "^0.16") (default-features #t) (kind 0)))) (hash "1896c98z50h5p1g7g7bz2j35lx6lgch3jnl4lyy4pjn0kndmgms2")))

(define-public crate-nstack-0.3 (crate (name "nstack") (vers "0.3.0") (deps (list (crate-dep (name "kelvin") (req "^0.17") (default-features #t) (kind 0)))) (hash "1q1k83ps48mc1ybwlmwi9swf8pnkb4qyb78nql7l73rizj8sknj5")))

(define-public crate-nstack-0.4 (crate (name "nstack") (vers "0.4.0") (deps (list (crate-dep (name "kelvin") (req "^0.18") (default-features #t) (kind 0)))) (hash "124yk9a85m1d427msq6pvjxk0xc0ndhy1a1akpx5r23vmnpx4isi")))

(define-public crate-nstack-0.5 (crate (name "nstack") (vers "0.5.0") (deps (list (crate-dep (name "kelvin") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1065ngvn7srq9crp00qh7pypy60ia8g1x5agghi99hzs83a6bfj8")))

(define-public crate-nstack-0.6 (crate (name "nstack") (vers "0.6.0") (deps (list (crate-dep (name "canonical") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "canonical_host") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.4") (default-features #t) (kind 0)))) (hash "002i53125d760s61iwfv6mv4544z2xb3k3i7z50sc68db2l5m9ph")))

(define-public crate-nstack-0.6 (crate (name "nstack") (vers "0.6.1") (deps (list (crate-dep (name "canonical") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "canonical_host") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "microkelvin") (req "^0.5") (default-features #t) (kind 0)))) (hash "1l64p6lqi1qdgi8dxpf0cakfn01966l7qh32mjy36spc4nww6s8a")))

(define-public crate-nstack-0.6 (crate (name "nstack") (vers "0.6.2") (deps (list (crate-dep (name "canonical") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "canonical_host") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "microkelvin") (req "^0.5") (default-features #t) (kind 0)))) (hash "0vwd5yhgljcxxnl7z4msxkj0781cl47yqm8fdaz7ivxvzxsgzf50")))

(define-public crate-nstack-0.6 (crate (name "nstack") (vers "0.6.3") (deps (list (crate-dep (name "canonical") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "canonical_host") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "microkelvin") (req "^0.5") (default-features #t) (kind 0)))) (hash "15vca225kywr0dk80dxms3z0s8xnif003g2x4gcp34jfkgh25ga2") (yanked #t)))

(define-public crate-nstack-0.7 (crate (name "nstack") (vers "0.7.0") (deps (list (crate-dep (name "canonical") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "canonical_host") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "microkelvin") (req "^0.6") (default-features #t) (kind 0)))) (hash "0mrp0va59d16lb98xj1pl96vj52rfsyr76fq0v83zppqq0ab8j9w")))

(define-public crate-nstack-0.8 (crate (name "nstack") (vers "0.8.0") (deps (list (crate-dep (name "canonical") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.7") (default-features #t) (kind 0)))) (hash "0ja8hhskkayd0f0ccyn3bx63hspr5mqzzcjjclicbmak2b1r20mw")))

(define-public crate-nstack-0.8 (crate (name "nstack") (vers "0.8.1") (deps (list (crate-dep (name "canonical") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.9.0-rc") (default-features #t) (kind 0)))) (hash "1ylynknfxd3rwvnig2c9gpi7lzzi1c0camw657hws229246w0428") (features (quote (("persistance" "microkelvin/persistance")))) (yanked #t)))

(define-public crate-nstack-0.9 (crate (name "nstack") (vers "0.9.0") (deps (list (crate-dep (name "canonical") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.9") (default-features #t) (kind 0)))) (hash "1vgdbc99hsm6wk09arwsdhfwssac29v7dqyv83pq6bd3mjic115f") (features (quote (("persistance" "microkelvin/persistance"))))))

(define-public crate-nstack-0.10 (crate (name "nstack") (vers "0.10.0-rc.0") (deps (list (crate-dep (name "canonical") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.10.0-rc") (default-features #t) (kind 0)))) (hash "18wwmbh57pn3nw5hf3jimcl4glh8ln0654zgdxq95j0jmp5b1qzl") (features (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.10 (crate (name "nstack") (vers "0.10.0") (deps (list (crate-dep (name "canonical") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.10") (default-features #t) (kind 0)))) (hash "1ngx7ax6i66annm060202a3m9sxhih4bv0mdp05c70plmlvwwp0x") (features (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.11 (crate (name "nstack") (vers "0.11.0-rc.0") (deps (list (crate-dep (name "microkelvin") (req "^0.11.0-rc.0") (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7.20") (default-features #t) (kind 0)))) (hash "1hiwx2va194bdv39niax2h4q61dfbzfhzmappbs1hqcskz7dwzyz")))

(define-public crate-nstack-0.12 (crate (name "nstack") (vers "0.12.0-rc.0") (deps (list (crate-dep (name "bytecheck") (req "^0.6.7") (kind 0)) (crate-dep (name "microkelvin") (req "^0.13.0-rc.0") (kind 0)) (crate-dep (name "microkelvin") (req "^0.13.0-rc.0") (default-features #t) (kind 2)) (crate-dep (name "rkyv") (req "^0.7.29") (features (quote ("validation"))) (kind 0)))) (hash "0ji6r1r5mz3kxyszyc14gfxa4rf4bpcciyfh1rha022crf7h7l2w")))

(define-public crate-nstack-0.13 (crate (name "nstack") (vers "0.13.0") (deps (list (crate-dep (name "canonical") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.14") (default-features #t) (kind 0)))) (hash "028lcbkkv2zyrk3dfr88maxj7qrrnbwwjflc3mvp7zh3n6j8zwbk") (features (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.14 (crate (name "nstack") (vers "0.14.0-rc.0") (deps (list (crate-dep (name "canonical") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "canonical_derive") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "microkelvin") (req "^0.15.0-rc") (default-features #t) (kind 0)))) (hash "06ikindr3fnqizhssc9bpnlvbrsnm8r8xkl83zvhjyyygj94hjva") (features (quote (("persistence" "microkelvin/persistence"))))))

(define-public crate-nstack-0.15 (crate (name "nstack") (vers "0.15.0-rkyv.0") (deps (list (crate-dep (name "bytecheck") (req "^0.6.7") (kind 0)) (crate-dep (name "microkelvin") (req "^0.16.0-rkyv") (kind 0)) (crate-dep (name "microkelvin") (req "^0.16.0-rkyv") (default-features #t) (kind 2)) (crate-dep (name "rkyv") (req "^0.7.29") (features (quote ("validation"))) (kind 0)))) (hash "0sra884x7pypqbvsr1r5glmdnkqn1i4wx9x7yl96c4jcl7ffgids")))

(define-public crate-nstack-0.16 (crate (name "nstack") (vers "0.16.0-rc.0") (deps (list (crate-dep (name "microkelvin") (req "^0.17.0-rc") (default-features #t) (kind 0)) (crate-dep (name "ranno") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hf2qv2dwv23in7v06yydrwcsgw7h2gkz98jpmjas6a52wpi82wa")))

(define-public crate-nstack-0.16 (crate (name "nstack") (vers "0.16.0") (deps (list (crate-dep (name "microkelvin") (req "^0.17.0-rc") (default-features #t) (kind 0)) (crate-dep (name "ranno") (req "^0.1") (default-features #t) (kind 0)))) (hash "097jr2n5kss9ni11av6xwv6y8vp998xmrfwsrbdmbaq9ip76w48g")))

