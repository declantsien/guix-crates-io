(define-module (crates-io ns ga) #:use-module (crates-io))

(define-public crate-nsga-0.1 (crate (name "nsga") (vers "0.1.0") (deps (list (crate-dep (name "peeking_take_while") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1hn9b9dd9100j9pnvrvqig1pwndb0l9mwp9mb9hxv57zfpc9sgzy")))

(define-public crate-nsga-0.1 (crate (name "nsga") (vers "0.1.1") (deps (list (crate-dep (name "peeking_take_while") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1crfdlpjj3i5in68ck4ax3g9b7r03hk32gbiqgk4yx8g55rj55ss")))

(define-public crate-nsga-0.1 (crate (name "nsga") (vers "0.1.2") (deps (list (crate-dep (name "peeking_take_while") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0k9knf0290g7swqazvggmc8vn28yssh5bg8rl9b98i1k6f867780")))

