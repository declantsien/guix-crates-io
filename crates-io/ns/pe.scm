(define-module (crates-io ns pe) #:use-module (crates-io))

(define-public crate-nspector-0.1 (crate (name "nspector") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "19kwsa8dbagflcfgdam121943x4r5fym3kqlvmj5pnidr5g04asz")))

