(define-module (crates-io ns id) #:use-module (crates-io))

(define-public crate-nsid-0.0.1 (crate (name "nsid") (vers "0.0.1") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1ra7cjdv17v68l7k99vrrva0885sx9gpqdh1ikccwp1nn3rcnbmn")))

