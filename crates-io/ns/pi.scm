(define-module (crates-io ns pi) #:use-module (crates-io))

(define-public crate-nspire-0.1 (crate (name "nspire") (vers "0.1.0") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0dljcdvzy4j4j95005d2ssbs8h1qin4k09s0pgynsg98ksi7gdjp")))

(define-public crate-nspire-0.2 (crate (name "nspire") (vers "0.2.0") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1c6wwyg2fsq34jr96xwm0cxxsryh03z63iprlv0bwyf6hvl7lzb7") (features (quote (("disable-panic-handler") ("disable-oom-handler") ("disable-allocator"))))))

(define-public crate-nspire-0.2 (crate (name "nspire") (vers "0.2.1") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "12x1yyjvwrr7pjkxvqxrns8b12drby5zpn06342jl5kszx391y9y") (features (quote (("disable-panic-handler") ("disable-oom-handler") ("disable-eh-personality") ("disable-allocator"))))))

(define-public crate-nspire-0.3 (crate (name "nspire") (vers "0.3.0") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1fb0v4xyzg3kdiyg42i3wwqvagy5lsnbldk7zpgmgqhmbqb1zqha")))

(define-public crate-nspire-0.3 (crate (name "nspire") (vers "0.3.1") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "09khwr8ql2xx6hd3w641x3jgvhky3dgch0akp0xd0i05va9j3w40")))

(define-public crate-nspire-0.3 (crate (name "nspire") (vers "0.3.2") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0nz8dy0v0lrhqjk72vqkp2kfx1wschnixrr7znpc4h6jmp48ws7c")))

(define-public crate-nspire-0.4 (crate (name "nspire") (vers "0.4.0") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0g8v55x6w6bss2i1x01xi2pb05fxbjkdgbf6v54xddnwbcwxhla5")))

(define-public crate-nspire-0.5 (crate (name "nspire") (vers "0.5.0") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "048bfapllhi2mx8fh4hlgqb82bbf9lxyrb2yydc1y5g425s10rmx")))

(define-public crate-nspire-0.5 (crate (name "nspire") (vers "0.5.1") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1g1r3rp3cqa69shwxfqc61m3z82dll1x7ymlzghpqmb38g7ikywc")))

(define-public crate-nspire-0.5 (crate (name "nspire") (vers "0.5.2") (deps (list (crate-dep (name "cstr_core") (req "^0.1.2") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "ndless-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "15434chbhwd0sspyb5kpymb0zg8ypxvkrqdlsfzj5xbv3vmklvrk")))

