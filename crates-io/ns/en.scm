(define-module (crates-io ns en) #:use-module (crates-io))

(define-public crate-nsenter-0.0.1 (crate (name "nsenter") (vers "0.0.1") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nsutils") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1c7zjq5vzyfgl70qdv7qbhnyl8ybcgjjdyl2fm586r4f46j0fx0k")))

