(define-module (crates-io ns pa) #:use-module (crates-io))

(define-public crate-nspawn-lite-0.2 (crate (name "nspawn-lite") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "myutil") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.17") (default-features #t) (kind 0)))) (hash "09b3xp5pqi3rjgryds56wn9jkybzr6mzp2qbkp5fpjssrl1vxkr9")))

