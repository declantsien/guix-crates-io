(define-module (crates-io ns ut) #:use-module (crates-io))

(define-public crate-nsutils-0.0.1 (crate (name "nsutils") (vers "0.0.1") (deps (list (crate-dep (name "lsns") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1acky10q44b6bkd196is120lbcl51zwqqqk8kkmjcfqp30d054iq")))

(define-public crate-nsutils-0.0.2 (crate (name "nsutils") (vers "0.0.2") (deps (list (crate-dep (name "lsns") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1w34njs1y9cpxp1nf2lamz0d9pcb7db0xhx24xcmzm8s53vilj5f")))

(define-public crate-nsutils-0.0.3 (crate (name "nsutils") (vers "0.0.3") (deps (list (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0dlcjxsfkp9f65lw3zcwrka944bbblis28b02d4zh87vhd7syrqj")))

(define-public crate-nsutils-0.0.4 (crate (name "nsutils") (vers "0.0.4") (deps (list (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "12ijsqw77m3v48rcx53pqgla23za5f1ww61vl677mam0fnd1ji7f")))

(define-public crate-nsutils-0.0.5 (crate (name "nsutils") (vers "0.0.5") (deps (list (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "04kcwvwvcc51ykcylqzq5j641bjwxzv0m8hry7b4g01gfh4mqml8")))

