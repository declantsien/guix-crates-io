(define-module (crates-io ns li) #:use-module (crates-io))

(define-public crate-nslice-0.1 (crate (name "nslice") (vers "0.1.0") (hash "13qha9z99lr3saaxabqrjxw54gdij3mc83nnfq85fzn58ihsb6cb")))

(define-public crate-nslice-0.2 (crate (name "nslice") (vers "0.2.0") (hash "0k44198vmijrc5rscax6yjwl4388wn7jf3y5klrpdk8as855ynn1")))

(define-public crate-nslice-0.2 (crate (name "nslice") (vers "0.2.1") (hash "1dkplq119axiq60kr7nw8xs53fxs2f56gr6hms5vf980syw22yz0") (rust-version "1.56.1")))

