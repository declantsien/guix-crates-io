(define-module (crates-io ns tr) #:use-module (crates-io))

(define-public crate-nstr-0.1 (crate (name "nstr") (vers "0.1.0") (hash "1x1mkfi21gzz9nqh1jw42n5znl0zzl0xk7mkxqlyk7pffvnivz6h")))

(define-public crate-nstr-0.2 (crate (name "nstr") (vers "0.2.0") (hash "1ds20xarwpjcrab64jnc8fqjl4lyww5860pfwjs9qcx5m441w815")))

(define-public crate-nstr-0.3 (crate (name "nstr") (vers "0.3.0") (hash "07xb4y0g1d7d58vv9va5vmph1rq4x1q873vsxrj7x17sdfzms6x5")))

(define-public crate-nstr-0.3 (crate (name "nstr") (vers "0.3.1") (hash "0gbv0cc6nraq63sr2vga5kpqf980s85llj97r8n78dxgvr9cl1r1")))

(define-public crate-nstr-0.3 (crate (name "nstr") (vers "0.3.2") (hash "0pizzbl5pvgbsqcmp0mrn5yaa9nmpdw869r8bfvwpnlg4dw2vc4v")))

