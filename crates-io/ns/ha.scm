(define-module (crates-io ns ha) #:use-module (crates-io))

(define-public crate-nshare-0.1 (crate (name "nshare") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.2") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.20.0") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (optional #t) (kind 0)))) (hash "1ks7lazv4qc4y1d69aylk1sa2qapil61kfygf54hcnvj8znv0qda") (features (quote (("std" "nalgebra/std") ("defalut"))))))

(define-public crate-nshare-0.1 (crate (name "nshare") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.2") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.20.0") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (optional #t) (kind 0)))) (hash "1gr47849c29nwdi86pv196gfr6ad2lsddnwxfysf9a10fr7fvmm4") (features (quote (("std" "nalgebra/std") ("defalut"))))))

(define-public crate-nshare-0.2 (crate (name "nshare") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.12") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.23.1") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (optional #t) (kind 0)))) (hash "1wlzrvlaaafp6yz404fbrx082kna3wy8ml59zvhygbbqf6cvanzl") (features (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.3 (crate (name "nshare") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.23.12") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.23.1") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.0") (optional #t) (kind 0)))) (hash "0gh3nf1q3ib19cxc0wjdgqdq2nbjmda4l4ffi9jzyr12lcfbkija") (features (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.4 (crate (name "nshare") (vers "0.4.0") (deps (list (crate-dep (name "image") (req "^0.23.13") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.24.1") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (optional #t) (kind 0)))) (hash "1hj8jffkqcpimz90grmvnmjzfpi6y2bz4ls3xm3l9yyxhsc3rnzf") (features (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.5 (crate (name "nshare") (vers "0.5.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.26.1") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.1") (optional #t) (kind 0)))) (hash "1awdgl76kqpxd3d38lzs54jw20frzr6b7ab43zbkl5426k16ysk2") (features (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.6 (crate (name "nshare") (vers "0.6.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.26.2") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.1") (optional #t) (kind 0)))) (hash "1mrkljnns7qssza65a6rmibdnim1k7jsidc5aw859dcal62gmmi6") (features (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.7 (crate (name "nshare") (vers "0.7.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.27.1") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.3") (optional #t) (kind 0)))) (hash "00bq0rrx3284hjlkp7v9yafn8n1hnca3g3wlkq4j2vm0fsq2j8gy") (features (quote (("std" "nalgebra/std") ("default"))))))

(define-public crate-nshare-0.8 (crate (name "nshare") (vers "0.8.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29.0") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.3") (optional #t) (kind 0)))) (hash "1kn1l4h9aj59nchb83c9mqsj07wzkwvqkp8gvp8wnr8d2k9zhwib") (features (quote (("nalgebra_std" "nalgebra/std") ("default" "nalgebra" "nalgebra_std" "ndarray" "image"))))))

(define-public crate-nshare-0.9 (crate (name "nshare") (vers "0.9.0") (deps (list (crate-dep (name "image") (req "^0.24.0") (optional #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.30.1") (optional #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (optional #t) (kind 0)))) (hash "1in44v5vrg2biwxrwy3102q831x1c4zarwn4dr0hfc8fsiy6ais4") (features (quote (("nalgebra_std" "nalgebra/std") ("default" "nalgebra" "nalgebra_std" "ndarray" "image"))))))

