(define-module (crates-io pd fc) #:use-module (crates-io))

(define-public crate-pdfcr-1 (crate (name "pdfcr") (vers "1.0.0") (deps (list (crate-dep (name "printpdf") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0b4236qaa0ly998kfywi1dqmdhb94s4z63310x4hh94lza9bs6ip")))

(define-public crate-pdfcr-1 (crate (name "pdfcr") (vers "1.1.0") (deps (list (crate-dep (name "printpdf") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0zjirs3wxjq31q3bap94rwzc5dnd9jhk92gb0dp1w6p8nf00hdd6")))

(define-public crate-pdfcr-1 (crate (name "pdfcr") (vers "1.2.0") (deps (list (crate-dep (name "printpdf") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "191hdg2xibnxf18yk5p5iczglvpwl36wsyyl22hb5450cpph7xbr")))

