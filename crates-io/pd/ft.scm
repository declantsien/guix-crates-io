(define-module (crates-io pd ft) #:use-module (crates-io))

(define-public crate-pdftotext-0.1 (crate (name "pdftotext") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0adi3ljs1q1gvm00wz9fwpfyx7gm2c7mhllzcaqp8wrgcg6dhkfi")))

(define-public crate-pdftotext-0.1 (crate (name "pdftotext") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "02lnnljim6d3gsjb2cfd2q58ccswnz2mqsklfvypdw7szkir05hw")))

(define-public crate-pdftotext-0.1 (crate (name "pdftotext") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1nya90x94l6m6b3z1n6cibi155m0pd5nl440nval8v5kv819brg9")))

(define-public crate-pdftotext-0.1 (crate (name "pdftotext") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0qyd2m5q9ligzzwhyilac1kb22qsh5wpkzk3ddv1xgbyl2jndb85")))

(define-public crate-pdftotext-0.1 (crate (name "pdftotext") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0dg6438miaybza5jqqghlznd6al2xwv4bzjd0mbf4ipa7zmy9gsk") (features (quote (("static-poppler" "cmake") ("default"))))))

(define-public crate-pdftotext-0.1 (crate (name "pdftotext") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0zbpskmm63c05rf0c1d2p9bx2y6k7bsw7i4s2zh9gkbvwfas6dnq") (features (quote (("static-poppler" "cmake") ("default"))))))

