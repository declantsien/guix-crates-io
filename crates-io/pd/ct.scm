(define-module (crates-io pd ct) #:use-module (crates-io))

(define-public crate-pdctl-0.1 (crate (name "pdctl") (vers "0.1.0") (deps (list (crate-dep (name "pagersduty") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pb5xcxjn42gxqcf0hfdvx6dyighmmrdy2nlrh8d7mxydvfanz01")))

(define-public crate-pdctl-0.1 (crate (name "pdctl") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pagersduty") (req "^0.2") (default-features #t) (kind 0)))) (hash "14pj70nwwm4fqcfgs5p81zbvpf2y7xnf8zmw92h95as2kqi631c7")))

