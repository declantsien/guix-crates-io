(define-module (crates-io pd _a) #:use-module (crates-io))

(define-public crate-pd_auth_service-0.1 (crate (name "pd_auth_service") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0m1qwq2v241dh3s87w3bp0w0qrkhc1ii7h21kafkqfr66wqbakpv") (yanked #t)))

