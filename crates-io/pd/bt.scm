(define-module (crates-io pd bt) #:use-module (crates-io))

(define-public crate-pdbtbx-0.1 (crate (name "pdbtbx") (vers "0.1.0") (hash "1dyxfxgiqnaz013xz70c55mfbjkgvshhzzn2q15ddh0xsssb6lqw")))

(define-public crate-pdbtbx-0.1 (crate (name "pdbtbx") (vers "0.1.1") (hash "1j37akl3d3cik0dpaz6bwz8bgwx1mi4drr6qz23cln669d8342rk")))

(define-public crate-pdbtbx-0.1 (crate (name "pdbtbx") (vers "0.1.2") (hash "1ks1r5ng91may2v48ww7h0qwhky96ilw8zqs3wg0sm1szcys4jpi")))

(define-public crate-pdbtbx-0.1 (crate (name "pdbtbx") (vers "0.1.3") (hash "0v8x0s44xzrd29rzc05m2pn3rk6fjqkv94prqlz78c9pn9fp1xbb") (yanked #t)))

(define-public crate-pdbtbx-0.1 (crate (name "pdbtbx") (vers "0.1.4") (hash "19ryg29n2jrgi9a9c5sja3f48mkpwjp84vnrzbphmc4yv6y1zfsz") (yanked #t)))

(define-public crate-pdbtbx-0.1 (crate (name "pdbtbx") (vers "0.1.5") (hash "0h06hfrh8nfybp6c92kv331c993y0ml6dwf00nlsmydf3d5l2p2j")))

(define-public crate-pdbtbx-0.2 (crate (name "pdbtbx") (vers "0.2.0") (hash "1mv6dymyhak1plh4yj85dc4961l3dm5x6ji4ij7wzssja3qswp15")))

(define-public crate-pdbtbx-0.2 (crate (name "pdbtbx") (vers "0.2.1") (hash "07ys214z575ais543xshmwrpcv1py75rfza8nksfs3jyndbplliw")))

(define-public crate-pdbtbx-0.3 (crate (name "pdbtbx") (vers "0.3.0") (hash "0q7y12c8r993fsznfrxkgfdidjw1hqqpaz52ndycgvq637vh5291")))

(define-public crate-pdbtbx-0.3 (crate (name "pdbtbx") (vers "0.3.1") (hash "1dk860y2s1g4w2fxkzr0s6gf3lzinvwp7v8hl1p69sav6mc9am3f")))

(define-public crate-pdbtbx-0.3 (crate (name "pdbtbx") (vers "0.3.2") (hash "1iql8vcy6x9608h7mamviyx306qnd5dvqhwm2rxqqcs0z7ppg3yx")))

(define-public crate-pdbtbx-0.3 (crate (name "pdbtbx") (vers "0.3.3") (hash "1kplbpl4fn89nhl10lc9j12dbsbwdw8y5blrzkzk70nrnmk70h46")))

(define-public crate-pdbtbx-0.4 (crate (name "pdbtbx") (vers "0.4.0") (hash "0md5c09cps0b943gcdhgpalmcaqaj6rgc7jsrqaizgn8qm3zq7hy")))

(define-public crate-pdbtbx-0.4 (crate (name "pdbtbx") (vers "0.4.1") (hash "1p6a3rbx9n74s63zvzzzph25jpyf8sax7x2nrx4m3c0pwbacni7f")))

(define-public crate-pdbtbx-0.5 (crate (name "pdbtbx") (vers "0.5.0") (hash "158h4v5jrsc8khgxs8xhnil5qsbwqa8k141p9cq6rp0bjfw802v0")))

(define-public crate-pdbtbx-0.5 (crate (name "pdbtbx") (vers "0.5.1") (hash "1ihwjyj0kq98qs5bvvkkcfhwm0y514bjxngk5fplxhqgi68wflfh")))

(define-public crate-pdbtbx-0.6 (crate (name "pdbtbx") (vers "0.6.0") (hash "1ijlf9hxkbrdspqc1x9qvbj0s675cag71166llwqj7ybg3ywahqk")))

(define-public crate-pdbtbx-0.6 (crate (name "pdbtbx") (vers "0.6.1") (hash "1a4dncyc4h830gvy5nar6a92wx119q38fimf9i3hjkcy5wm879kr")))

(define-public crate-pdbtbx-0.6 (crate (name "pdbtbx") (vers "0.6.2") (hash "0ih4hb2li6c742np59zd2a73qkhxjb4xngiwghcj9645znz4a40w")))

(define-public crate-pdbtbx-0.6 (crate (name "pdbtbx") (vers "0.6.3") (hash "11aqjj7gvwc66zihdlwx31qq5abgv3q91np0sn9jbb37j8w5a4f6")))

(define-public crate-pdbtbx-0.7 (crate (name "pdbtbx") (vers "0.7.0") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0r52fvsm18s34ws7p6iljvjmq52mnw2i6dlcq2xabvlqrgmdq5d6") (features (quote (("unstable-doc-cfg") ("default" "rayon"))))))

(define-public crate-pdbtbx-0.8 (crate (name "pdbtbx") (vers "0.8.0") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0bx2ci7mxpr0wzp9yxskv75ilm9r0d9dvrc8w9bffml7h50r9fl1") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.9 (crate (name "pdbtbx") (vers "0.9.0") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 2)))) (hash "00dpxxafmkzkh65hgdvpzdnzvpird8mmkdm9w1xdijm1bax8rfsv") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.9 (crate (name "pdbtbx") (vers "0.9.1") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 2)))) (hash "0dz7906plf0z1ssjwmq4ssiqzbj2d7bnh69wc88a49a552cbywas") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.9 (crate (name "pdbtbx") (vers "0.9.2") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 2)))) (hash "19vjl42wnlxxd3zkan2iacjscglsnpld8yycz025acpryvr2kn5a") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.10 (crate (name "pdbtbx") (vers "0.10.0") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 2)))) (hash "1v1psds2rni011wl4p9gbmzbv1fhgs9c6ghs8fmfcqvazrmfz0gz") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.10 (crate (name "pdbtbx") (vers "0.10.1") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 2)))) (hash "0jm38lw87iyh6y1h5j8rx59wwvsb5169rcdp1jcyaybfqmx5nf8r") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.10 (crate (name "pdbtbx") (vers "0.10.2") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 2)))) (hash "130z9k4xx0rg06bj059qbf3xrd3yarjcnfkclh8vrqabg25040bl") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde"))))))

(define-public crate-pdbtbx-0.11 (crate (name "pdbtbx") (vers "0.11.0") (deps (list (crate-dep (name "doc-cfg") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstar") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 2)))) (hash "0jzb9c7qwph1sla49md7mz5m9l5gwa7zs7izdqm10bjss89dmbnq") (features (quote (("unstable-doc-cfg") ("default" "rayon" "rstar" "serde" "compression") ("compression" "flate2")))) (rust-version "1.56")))

