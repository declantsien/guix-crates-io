(define-module (crates-io pd r-) #:use-module (crates-io))

(define-public crate-pdr-build-0.0.0 (crate (name "pdr-build") (vers "0.0.0") (hash "0ny75kh5n1j2aqn4c37qmlzsxa2a5dv87vj64m3z1771vjsk4dqw")))

(define-public crate-pdr-build-support-0.0.0 (crate (name "pdr-build-support") (vers "0.0.0") (hash "0i0n49dyw0k6ggdrskx805bcqdjvm1yic1g620r8kmzr54mmj7lk")))

(define-public crate-pdr-std-0.0.0 (crate (name "pdr-std") (vers "0.0.0") (hash "1xcdrsl8qpblrgyii1bslqj3iipqyipw9psns2v13chp0nn1hhsi")))

(define-public crate-pdr-sys-0.0.0 (crate (name "pdr-sys") (vers "0.0.0") (hash "1hvglqpxcjamr9vbga2h5ypsc6r5cwg6v29n5xz0gkqfp8n6dqjf")))

