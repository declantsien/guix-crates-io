(define-module (crates-io pd fo) #:use-module (crates-io))

(define-public crate-pdfork-0.1 (crate (name "pdfork") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ha1j2mjhzfyn2s1sqyyc51lm43j68c2isqpzgrxx296wan9rldn")))

(define-public crate-pdfork-0.1 (crate (name "pdfork") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "09mv9k61myxqs9hxdnmhhb2brcpdnin8llq74samh29qq584p8d7")))

