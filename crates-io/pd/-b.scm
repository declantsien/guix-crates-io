(define-module (crates-io pd -b) #:use-module (crates-io))

(define-public crate-pd-build-0.0.0 (crate (name "pd-build") (vers "0.0.0") (hash "02j3kzl30qhkj6xq405v977zwqim8pf83maac6fg7iq0qbhrh0ic")))

(define-public crate-pd-build-support-0.0.0 (crate (name "pd-build-support") (vers "0.0.0") (hash "1nbi8c71ibmq6p3bzswcgy77mkn745xh03vimnxmypghh8miiwg5")))

