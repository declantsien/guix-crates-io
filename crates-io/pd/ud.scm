(define-module (crates-io pd ud) #:use-module (crates-io))

(define-public crate-pdudaemon-client-0.1 (crate (name "pdudaemon-client") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1zf1fx8qdwvpcqk4djfmqzhhaay1krsmhz4mi3z2wn4iwhc0js7z")))

