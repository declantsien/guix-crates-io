(define-module (crates-io pd g-) #:use-module (crates-io))

(define-public crate-pdg-build-0.0.0 (crate (name "pdg-build") (vers "0.0.0") (hash "0iwf706hqbbr0y44qniqnv3gm0hf921fa23ri15xfhm2wy6sfj1j")))

(define-public crate-pdg-build-support-0.0.0 (crate (name "pdg-build-support") (vers "0.0.0") (hash "1sjc3764zccpmlbwcdyzf995w9nwngb1p4x9n0lczlbsvd4ipyh7")))

(define-public crate-pdg-std-0.0.0 (crate (name "pdg-std") (vers "0.0.0") (hash "0d5wx7mq4immqaglqylljcdn5qsmlpkm7w092xc9bk7c2yh74cj4")))

(define-public crate-pdg-sys-0.0.0 (crate (name "pdg-sys") (vers "0.0.0") (hash "02ndmpik86v736mnd3bjjizdccb4007rgka5xqg3i1lkydavhygh")))

