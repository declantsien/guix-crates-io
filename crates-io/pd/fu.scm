(define-module (crates-io pd fu) #:use-module (crates-io))

(define-public crate-pdfutil-0.1 (crate (name "pdfutil") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0msi75ppdwgzkv6wbh1wzgl6ap4ja28axzgq3kjmxyrq3iilrk54")))

(define-public crate-pdfutil-0.2 (crate (name "pdfutil") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1zqc7cv5lq4wfcpbkcpvi9mvgkncda046xq12z1nzzpa5v5bnh22")))

(define-public crate-pdfutil-0.4 (crate (name "pdfutil") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "lopdf") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "1n4b3nq8dh2kqb7n1vr7hlxfadqhzd7p3sjyzb01wndsw84z3d4d")))

