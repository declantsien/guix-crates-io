(define-module (crates-io pd a_) #:use-module (crates-io))

(define-public crate-pda_parser-0.1 (crate (name "pda_parser") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "fi-night") (req "^0.1.6") (features (quote ("fsm_gen_code"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 0)))) (hash "06993fdv9f4zsbxq2nmirzxhdhg0zxyr3vrh0vgmi36fs0yx0qh8") (yanked #t)))

