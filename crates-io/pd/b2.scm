(define-module (crates-io pd b2) #:use-module (crates-io))

(define-public crate-pdb2-0.9 (crate (name "pdb2") (vers "0.9.0") (deps (list (crate-dep (name "fallible-iterator") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 2)) (crate-dep (name "scroll") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0p8p1ldsn7i5h0bwyjvnl3r0454vx9vzf7275qdd9c6a3c9hxqq0")))

