(define-module (crates-io pd o3) #:use-module (crates-io))

(define-public crate-pdo3-0.0.0 (crate (name "pdo3") (vers "0.0.0") (hash "1jl4gn4bgf45k4xx3plv8fxpi4rzyzr1c037m017pagmi0ab8spq")))

(define-public crate-pdo3-build-0.0.0 (crate (name "pdo3-build") (vers "0.0.0") (hash "1fsp1q5ywhs3504qrr8fdg9prssigslzz7588byhiimlz7bjl7rv")))

(define-public crate-pdo3-build-support-0.0.0 (crate (name "pdo3-build-support") (vers "0.0.0") (hash "068ic0nywn9nr5z1hmcfy6p552r07ppdg58zc5djpb5fa65a6ryf")))

(define-public crate-pdo3-std-0.0.0 (crate (name "pdo3-std") (vers "0.0.0") (hash "0478jbbz140lpjjll4nd9vw5rczi521jgmrjdqf5509jrw3g236w")))

(define-public crate-pdo3-sys-0.0.0 (crate (name "pdo3-sys") (vers "0.0.0") (hash "06i1dsqb7bl7a2252qdmfy3g5290wws0x5dxayxpshynryvpyzpg")))

