(define-module (crates-io pd x-) #:use-module (crates-io))

(define-public crate-pdx-build-0.0.0 (crate (name "pdx-build") (vers "0.0.0") (hash "0whr5d2xhfnmpjr4lf5nl57386rvax6d95sb3dq41anxhrccjcc5")))

(define-public crate-pdx-manifest-0.0.0 (crate (name "pdx-manifest") (vers "0.0.0") (hash "1wlgca9cpgc9p999sgph94c6z3p11imw4gdx7rkpmf60r2n772c6")))

(define-public crate-pdx-syntax-0.1 (crate (name "pdx-syntax") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strip_bom") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h2gq9899an70rq44ags0skhz54pqrmrkm5jzpfs4w9whhj29g93")))

(define-public crate-pdx-syntax-0.2 (crate (name "pdx-syntax") (vers "0.2.0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strip_bom") (req "^1.0") (default-features #t) (kind 0)))) (hash "19sngzanadsz6gizpad2jyy42y5x9grnf17a8qhrl8r2n7xsflvq")))

