(define-module (crates-io pd cm) #:use-module (crates-io))

(define-public crate-pdcm-linkify-0.1 (crate (name "pdcm-linkify") (vers "0.1.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1jq610ib5nrry47h33yf7bbkvpv86xynqzj3qvc3h74nmpihd6l5")))

