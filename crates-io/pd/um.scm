(define-module (crates-io pd um) #:use-module (crates-io))

(define-public crate-pdump-0.2 (crate (name "pdump") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "default-net") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.33.0") (default-features #t) (kind 0)))) (hash "0jh9ywgwk6r5bgy3hy7wlsyv8r27061fjk61xbd1iiwhgb972hlw")))

(define-public crate-pdump-0.2 (crate (name "pdump") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "default-net") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.33.0") (default-features #t) (kind 0)))) (hash "1fhw0p3qzni1p9cjij10a9vd5iracdqxmy12rpav0h1dnxzn7v2r")))

(define-public crate-pdump-0.2 (crate (name "pdump") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "default-net") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.33.0") (default-features #t) (kind 0)))) (hash "0spc1r9dalx23yqr5sqxl7vhgwnxv17ky29f1zg4653jgwyqkj0b")))

