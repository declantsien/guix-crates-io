(define-module (crates-io pd sl) #:use-module (crates-io))

(define-public crate-pdsl-0.1 (crate (name "pdsl") (vers "0.1.0") (hash "16p1v3c15a5l72hcs8xh2fsk9q39i9mx31aazvy2rayhn2l8c85h")))

(define-public crate-pdsl_core-0.1 (crate (name "pdsl_core") (vers "0.1.0") (hash "0wvd4gqfp965cc7l3fxx5vgyc4dw1qgg58a7v3467vnl98k6rwba")))

(define-public crate-pdsl_derive-0.1 (crate (name "pdsl_derive") (vers "0.1.0") (hash "0q57gp7b1pcl8czyzly7fxbzigf9kpx7hypcmg889kxrjd2zzvwg")))

