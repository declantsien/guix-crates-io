(define-module (crates-io pd qs) #:use-module (crates-io))

(define-public crate-pdqselect-0.1 (crate (name "pdqselect") (vers "0.1.0") (hash "09vwywzavhgqgr3vi2ycgv2nd0067jirv36fb3jvp860xikigjaf")))

(define-public crate-pdqselect-0.1 (crate (name "pdqselect") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 2)))) (hash "1vis37a2ppvsf9i6ydkdv9gwz7g5ffdadzyiw76mdp91jdnr0y3p")))

(define-public crate-pdqsort-0.1 (crate (name "pdqsort") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "120mi95majh5jjnfv8qcgr4d0cxc1fq4krk8qy8yd6jg7y8dvsdg")))

(define-public crate-pdqsort-0.1 (crate (name "pdqsort") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0m5snnz9xqm2mr8cnwlkb1q9md0jgd3jl4g34q7k679gcl2q8j4v")))

(define-public crate-pdqsort-0.1 (crate (name "pdqsort") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0z18sq4pp7zljm77r464bwsyzaw3qfh7biqwc42wlj4ir111djnf")))

(define-public crate-pdqsort-1 (crate (name "pdqsort") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "123p9zmj0pkzyj1xf224wvxaxy9v6p9yv5j9chclv8li4dbwa5s4")))

(define-public crate-pdqsort-1 (crate (name "pdqsort") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1c4yc6zwcl6bpjkxk8f4a0m0idwfy04zz1yx8appz1xi266jflg6")))

(define-public crate-pdqsort-1 (crate (name "pdqsort") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "161simf9pxpg0f2i3rb02b3mncpr8ysj10453jnw1vcwrzb1ap5y")))

(define-public crate-pdqsort-1 (crate (name "pdqsort") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1rf5qlgkr0l0fk3y56s2pxsc615gnnqz6kxniccgly1h11q1glcj")))

