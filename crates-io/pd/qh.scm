(define-module (crates-io pd qh) #:use-module (crates-io))

(define-public crate-pdqhash-0.1 (crate (name "pdqhash") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "1rm0sbnjycn8qpdn9aygbihn9ik8qk0x1g2mpfcc5dsijvs2nh9n")))

(define-public crate-pdqhash-0.1 (crate (name "pdqhash") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "1vjrcwkyflqn5m465wdj78sx3ls8rnv8fhv42bsfqi75hmaxcx5x")))

