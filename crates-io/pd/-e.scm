(define-module (crates-io pd -e) #:use-module (crates-io))

(define-public crate-pd-external-rs-0.1 (crate (name "pd-external-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)))) (hash "0ld8vjv8lymigv854xyz2yfk95icih3m3h3chwy25dvpm4imljpi")))

