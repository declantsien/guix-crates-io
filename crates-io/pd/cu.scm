(define-module (crates-io pd cu) #:use-module (crates-io))

(define-public crate-pdcurses-sys-0.1 (crate (name "pdcurses-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03zky1vlvgjh0bxabwiqhc9csxa6yzgpyb91j28fpl38w2vl7j6a")))

(define-public crate-pdcurses-sys-0.2 (crate (name "pdcurses-sys") (vers "0.2.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l0bakcff1dirns2jcsckh3cnaqwdcgna4m92kw42qrsaqpwmc3i")))

(define-public crate-pdcurses-sys-0.2 (crate (name "pdcurses-sys") (vers "0.2.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kgzvnq7igchq05zdcfv32wdgbmkc5ias7cx8k9gas9blmwkc2sz")))

(define-public crate-pdcurses-sys-0.2 (crate (name "pdcurses-sys") (vers "0.2.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xh07ba7a86lmbi72a2qp1zg2rcnxfz5ad8plqyq7ccmbrffxisq")))

(define-public crate-pdcurses-sys-0.3 (crate (name "pdcurses-sys") (vers "0.3.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wfnyz7ysxhgk14vnxl00i29slkx9zcfmr956a6b7r7h087br8r7")))

(define-public crate-pdcurses-sys-0.4 (crate (name "pdcurses-sys") (vers "0.4.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cbka55g6m1kbdmbhaxv37rflnw6zxdm5c9nwx6a1njxqadglapw")))

(define-public crate-pdcurses-sys-0.5 (crate (name "pdcurses-sys") (vers "0.5.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00c62bffx3gnc0jx3ycbnr0cm2vz1zg6fkpvxhccrqvzls0nbkx3")))

(define-public crate-pdcurses-sys-0.5 (crate (name "pdcurses-sys") (vers "0.5.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wjwf8kmmj5j7ag2l7db85ycmqx9x0fjqvg9wqls0gd897q2jflj")))

(define-public crate-pdcurses-sys-0.6 (crate (name "pdcurses-sys") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05wlm0jnf47qfpkb6w8691hm31nrb9nhgbm229dxci054nh3ai7k")))

(define-public crate-pdcurses-sys-0.7 (crate (name "pdcurses-sys") (vers "0.7.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0apshhh3m10wp3k4l1naa2i1spbxrqi7lbvll3ghy25papz2pqch") (features (quote (("win32a") ("win32"))))))

(define-public crate-pdcurses-sys-0.7 (crate (name "pdcurses-sys") (vers "0.7.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sqn552nz33bmd0d8lcx862lrbxg6fgk5djfblig2q7zjqkx4k88") (features (quote (("win32a") ("win32")))) (links "pdcurses")))

