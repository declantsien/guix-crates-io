(define-module (crates-io pd fp) #:use-module (crates-io))

(define-public crate-pdfpdf-0.1 (crate (name "pdfpdf") (vers "0.1.0") (deps (list (crate-dep (name "deflate") (req "^0.7") (default-features #t) (kind 0)))) (hash "1kibm827hm3x1mxrry1diz0l4815pghq6xj06nx2a8am210a5b45")))

(define-public crate-pdfpdf-0.2 (crate (name "pdfpdf") (vers "0.2.0") (deps (list (crate-dep (name "deflate") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1k0kxm8mfs7f71kfndwasxyjlb582lpjn1g5k6jdgipp9sgwajf9")))

