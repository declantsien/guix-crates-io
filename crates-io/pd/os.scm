(define-module (crates-io pd os) #:use-module (crates-io))

(define-public crate-pdos-0.0.0 (crate (name "pdos") (vers "0.0.0") (hash "011jnryylnw27x3hyjvhg4jk024l7n8ralpqvcl4pfmsdig9cgi2")))

(define-public crate-pdos-build-0.0.0 (crate (name "pdos-build") (vers "0.0.0") (hash "1mrq65saxn85dnvim31ysi7901ridslyc08jgmdhf6z4wi5ydjnw")))

(define-public crate-pdos-build-support-0.0.0 (crate (name "pdos-build-support") (vers "0.0.0") (hash "1cid3m81xxag6j0vq1vb4iqah2aifqawpfrwr1jiwa84ai179cwy")))

(define-public crate-pdos-std-0.0.0 (crate (name "pdos-std") (vers "0.0.0") (hash "1j39gjj33y7kgv3yqb05s3wmfmcf6pf2xz10clascczy4894ly3h")))

(define-public crate-pdos-sys-0.0.0 (crate (name "pdos-sys") (vers "0.0.0") (hash "0hx13wmcr0r538v4k68lbsvjwwyc8xs62ang4kh8n1akayr1sx6x")))

