(define-module (crates-io pd bs) #:use-module (crates-io))

(define-public crate-pdbs_benchmark-0.1 (crate (name "pdbs_benchmark") (vers "0.1.0") (deps (list (crate-dep (name "cpu-time") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "csf") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fsum") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ph") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "puzzles") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 0)))) (hash "1s99nnjfirz976fszpdalj1m2xqz740syv65waniml8f87g9dink")))

