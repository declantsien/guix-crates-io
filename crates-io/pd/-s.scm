(define-module (crates-io pd -s) #:use-module (crates-io))

(define-public crate-pd-std-0.0.0 (crate (name "pd-std") (vers "0.0.0") (hash "0hppdq5dsicmcn2zhw9q1afb7qjglcgxb80hp2d1pqmrljg8rl9n")))

(define-public crate-pd-sys-0.1 (crate (name "pd-sys") (vers "0.1.0") (hash "1p7yag1x08n017jw5rf6g37x1g2zyp02wwf0sqyh3pxrjy87q9rd") (features (quote (("instance") ("doubleprecision") ("default"))))))

(define-public crate-pd-sys-0.1 (crate (name "pd-sys") (vers "0.1.1") (hash "1gvc37q9aw90y2ajvl16006nh09jyykkcxin7dvm5rw3ljppi8jm") (features (quote (("instance") ("doubleprecision") ("default"))))))

