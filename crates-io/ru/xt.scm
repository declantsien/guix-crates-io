(define-module (crates-io ru xt) #:use-module (crates-io))

(define-public crate-ruxt-0.1 (crate (name "ruxt") (vers "0.1.0") (deps (list (crate-dep (name "ruxt_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1lmrp40hdrpy6fcsbb1vv3j1cisp72krijhpch1fqrp0sclc8rr6")))

(define-public crate-ruxt-0.1 (crate (name "ruxt") (vers "0.1.1") (deps (list (crate-dep (name "ruxt_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ry9idk3hydn8ksifgxa8rhdvi4q65jy1z7gwxw2aqb071dxq2hy")))

(define-public crate-ruxt-0.1 (crate (name "ruxt") (vers "0.1.2") (deps (list (crate-dep (name "ruxt_macros") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0p915vsy25w754lw1w028hjzhqj65x7mffkypngkw638n2dqm7bc")))

(define-public crate-ruxt_macros-0.1 (crate (name "ruxt_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1y125rnhv3pg7jlmkb3n8z9qwpmawzc2sjyyyrpr70ybj6hyaiw4")))

(define-public crate-ruxt_macros-0.1 (crate (name "ruxt_macros") (vers "0.1.1") (deps (list (crate-dep (name "actix-web") (req "^4.5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "072lkxzji5rilppk6d24isfdv9ch28gydbgmq8akdlqipg915qqr")))

(define-public crate-ruxt_macros-0.1 (crate (name "ruxt_macros") (vers "0.1.2") (deps (list (crate-dep (name "actix-web") (req "^4.5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "12nmmfj698k3a5vhq6y5f7p4sdjw7mnynbhd8x55v3607pqrks56")))

(define-public crate-ruxt_macros-0.1 (crate (name "ruxt_macros") (vers "0.1.3") (deps (list (crate-dep (name "actix-web") (req "^4.5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0gkvq9brb4a89jghzwk6520d2lw6s9dajq46bfyqs01aihcr9y8l")))

(define-public crate-ruxt_macros-0.1 (crate (name "ruxt_macros") (vers "0.1.4") (deps (list (crate-dep (name "actix-web") (req "^4.5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "01kf3ii4d4fd3xqq9kmjzz9brx6kjcgkr71z9rrpb2bahc6v7zi2")))

