(define-module (crates-io ru tc) #:use-module (crates-io))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "175z001hy0j0g746xcdq7703hr0rchjzn2c01m28iyc43x5gyh38")))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0l24f9aih6cj031l1bmic7vs65rdf14irrnfsla6sggpn9z59i2j")))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0cmvb5i3x110isbw9ppanq0qxfd80ssz009iswn1ignp004jg747")))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "12fr5knx7mv7msh03y91fp2ab4sicvf846d1pbvnfvr3mj5wwf35")))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.4") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1lxx53l4775114qgyjpajnfnqy8rykxcvcs8l55yb1gqn447rsbd")))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.5") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1qyv610qvqxpyvs9ahlcghkjd4lip94nkxg5cr1pfy2bh0lj0rgk")))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.6") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0awy9y1iq4bngsfzrsz5j3vlcpnf5717075q7ls9bnpsqn22fssw")))

(define-public crate-rutcl-0.1 (crate (name "rutcl") (vers "0.1.7") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.176") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "00z102bikdphxsfkv0wai610ix4g37i923fcyl7cj27500cyqp8y") (v 2) (features2 (quote (("serde" "dep:serde"))))))

