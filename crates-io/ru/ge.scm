(define-module (crates-io ru ge) #:use-module (crates-io))

(define-public crate-rugenere-0.2 (crate (name "rugenere") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0jifjpslrb016dh7ai97l7d5xfb5p6zrr77xnngr2jdzm7sd1512")))

(define-public crate-rugenere-0.2 (crate (name "rugenere") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "16i6rbv9vmnrv3dbha7n2lnnnrrwpsy7r7qnwi9031xj5qxmm9yd")))

(define-public crate-ruget-0.1 (crate (name "ruget") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "146ffscv977r31gib365rss0k6jhpwrgrwv214xq32yi26ir1zlq")))

(define-public crate-ruget-0.1 (crate (name "ruget") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "03xvb04kcnbb59f1hk5mnrmsi1sirljy88b59jzac9i1lx6p8jmg")))

(define-public crate-ruget-0.1 (crate (name "ruget") (vers "0.1.2") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "1xj29if8i68253qxs5p06ym1ify0fbz78ihdy3vcsp9ydy21hp7x")))

(define-public crate-ruget-0.1 (crate (name "ruget") (vers "0.1.3") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "05r0ac7l9zasqd8zqqfk2gx5r93lpl7w753c8mnym8hl7fzsfhqy")))

(define-public crate-ruget-0.2 (crate (name "ruget") (vers "0.2.0") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1slaamgj0rs42fvyiwqbz8fy6lh2d1rdy5zjzn5kwqbrgbq161j6")))

(define-public crate-ruget-0.2 (crate (name "ruget") (vers "0.2.2") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0h8wvdcwr6hp1pq8lnda27y88a818n8n0rzlkjmmvrycawkhzs3s")))

(define-public crate-ruget-0.3 (crate (name "ruget") (vers "0.3.1") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0s7j8k2lj3m2x77p0izy4dw223p558wsyzpc5c0z2vicqkh0bqq0")))

(define-public crate-ruget-0.4 (crate (name "ruget") (vers "0.4.0") (deps (list (crate-dep (name "async-std") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0vzknnjp508ak3mzxlddrbkkrkd8szbjimrjgwfmp4jda4nji24g")))

(define-public crate-ruget-0.4 (crate (name "ruget") (vers "0.4.1") (deps (list (crate-dep (name "async-std") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1ckcpxszzmmiwabf8dr8aslgqzzlfmwfsixvab3ickm16mrqb2ck")))

(define-public crate-ruget-0.4 (crate (name "ruget") (vers "0.4.2") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10jrx8ygh01v22sgw5b9pq71wr23k08jln3wc0gvgvmsfqkidcx8")))

(define-public crate-ruget-0.4 (crate (name "ruget") (vers "0.4.3") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0wak5byqx0xp1gsw88yzk5qql57dwx595rbyalrsidiy28ywm3sx")))

(define-public crate-ruget-0.5 (crate (name "ruget") (vers "0.5.0") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^2.0") (default-features #t) (kind 0)))) (hash "1ng1m4syc1kskz2ha1ink853g5nf4gp03j5r89kpqlv9nzprnm8k")))

