(define-module (crates-io ru by) #:use-module (crates-io))

(define-public crate-ruby-0.0.1 (crate (name "ruby") (vers "0.0.1") (hash "1bj4pcgayvab50vc3q6c40vp9i4pxn7vhl90yswsz6zqrnm3cq2c") (yanked #t)))

(define-public crate-ruby-0.0.2 (crate (name "ruby") (vers "0.0.2") (hash "0bx7wwr7ky11m410r8cdwk0ayyhclnb1yckpjk1c5c6rymx09zsw") (yanked #t)))

(define-public crate-ruby-0.1 (crate (name "ruby") (vers "0.1.0") (hash "1iji5y3lixp5g7qc96aa8zfnwcc5xw077l2kwzgig7c734vnpmpb") (yanked #t)))

(define-public crate-ruby-0.1 (crate (name "ruby") (vers "0.1.1") (hash "0vi3mj3j2paa4vfx5xr07k6vb9ahlxyxbf2nmv9n08fk554i7aq4") (yanked #t)))

(define-public crate-ruby-0.1 (crate (name "ruby") (vers "0.1.2") (hash "1x44ik1dc0bk5qhg98ikhya5ixhgqxrh7aymbwyj4myxs91gry2g") (yanked #t)))

(define-public crate-ruby-lexer-0.0.0 (crate (name "ruby-lexer") (vers "0.0.0-dev1") (deps (list (crate-dep (name "memchr") (req "^2.2.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0.0-alpha1") (default-features #t) (kind 0)))) (hash "03cjijab8mkcxhxi9jski75vfvfsv4fbibiqaj5iv8g9j9whhcmn") (yanked #t)))

(define-public crate-ruby-marshal-0.0.1 (crate (name "ruby-marshal") (vers "0.0.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1kz3qa8hsf3sf9v58r2d5hdxg7nsp0f0a5gn8b22wjfq29w12c9z") (yanked #t)))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.0") (hash "1ijmirh96z3ppbh55fg8wnvik3xlhr97ywq3h025ikg02974b1gi") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert")))) (yanked #t)))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.1") (hash "1salaxyjkm5ixzdgvf2jdwyc6vpgmj8l5iyggqjcp9z7q1nvksf5") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert")))) (yanked #t)))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.2") (hash "1hz87976bjd1r679ihc8sz20hfwxrqzgad65hszv55mjq1k7wyz1") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.3") (hash "1ggxqlf5wh9zrd2pvapwchxvsfjz47xdhdxl6ibvsilcx8929vs1") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.4") (hash "1wdlczsdn550f70xrpxzkx6vkwxmcg6100dg14zg9lv9gi2sr5nf") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "174d148nc6c57rwfdc0r3q5zmfdx1v97jdhgb07yn5n6dc2mzk03") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1aihvnhf4dms5cdpfhalx0lyin9q3jpr84g62bqzysn63s1g24qg") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.1 (crate (name "ruby-math") (vers "0.1.7") (hash "112h9dwwggrcwsh1lh62gxi46jpkdpbh8nvdf5hfq02z234p078q") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.2 (crate (name "ruby-math") (vers "0.2.0") (hash "1i5k84jaff0n5gn87c2w8kpqjk9dqhf01i6sg5wna6h2qmx77ji1") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.2 (crate (name "ruby-math") (vers "0.2.1") (hash "1jb1cw5n9n3i2d2azgb4b8cskw1k9hypy7nrw712d1xzfwal37n1") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-math-0.2 (crate (name "ruby-math") (vers "0.2.2") (hash "11cxnq9pg8a17hqdq0gix8wwaa0skdwscjihfrbzc97dzs9vskqp") (features (quote (("ruby-assert") ("default" "ruby-assert") ("debug-ruby-assert"))))))

(define-public crate-ruby-mri-0.0.1 (crate (name "ruby-mri") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1g8hw6qy8f6czh46a94jfk3sf1sa29gfklmwp8c2fsy5llgirzns")))

(define-public crate-ruby-parser-0.0.0 (crate (name "ruby-parser") (vers "0.0.0-dev1") (deps (list (crate-dep (name "codemap") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.2.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0.0-alpha1") (default-features #t) (kind 0)))) (hash "16s1alg5q7x7p89v3ca1hvmw83j50na22s444d4ky4rxm0lwqhv5")))

(define-public crate-ruby-prism-0.19 (crate (name "ruby-prism") (vers "0.19.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "1rvi7bl6rq9ciz78c6w0z8p73prxqjiglak55vbz9y8qq85widab") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.20 (crate (name "ruby-prism") (vers "0.20.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "08sp8d9h2qczy9wlmr9ydzqc4ndckbaspk4vw17q1zif8a6cspl8") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.21 (crate (name "ruby-prism") (vers "0.21.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "1ii5as4q9nwpk7j4y5vlfsryqpiy2fdjn09wziixhyfdqinjfbh6") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.22 (crate (name "ruby-prism") (vers "0.22.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "1asdjqfkmfrl084545c4qpawjzp87w4v8h6jsg29w2vs8jcqbixf") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.23 (crate (name "ruby-prism") (vers "0.23.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "08vy6hr2c78bivrg96rmq09d1ldmnsra6jzbglg6645ssak24p4l") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.24 (crate (name "ruby-prism") (vers "0.24.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "18jx0j30yvfvcdqjywb79p1q766df6k4y9n1p9nfrnkq22krqnc0") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.25 (crate (name "ruby-prism") (vers "0.25.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "1nhi4mcmacjqn9p51dkgijwpyxh40341fnifrwpllmf7damx8342") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.26 (crate (name "ruby-prism") (vers "0.26.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "0767h6vg0qi0ivg930rplxdygh92gyhf0fhn14479h173apbkpgk") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.27 (crate (name "ruby-prism") (vers "0.27.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "0jxy52alz6g2lzh5dw22h579rwnbalvfg3g8a2snzp09famaq30j") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.28 (crate (name "ruby-prism") (vers "0.28.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "0wn0p3j13bdb6wkw4q8gq17w9awmyjb2y2rnzbqs46pdh6f97i98") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-0.29 (crate (name "ruby-prism") (vers "0.29.0") (deps (list (crate-dep (name "ruby-prism-sys") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 1)))) (hash "1pghvf0j62i7wpd7a988w0679m2iy1y5ba11rh141z5snma501w6") (features (quote (("vendored" "ruby-prism-sys/vendored") ("default" "vendored"))))))

(define-public crate-ruby-prism-sys-0.19 (crate (name "ruby-prism-sys") (vers "0.19.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "1kx07jw5fnq5iq63bdmnik7q6v9wppvp56sc24avmx35h2n5mb6f") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.20 (crate (name "ruby-prism-sys") (vers "0.20.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "1mdqfv9vlsf76rinqxwjg6hc4szj5xvdp5q7xkvagm6q4zqi8lb2") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.21 (crate (name "ruby-prism-sys") (vers "0.21.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "1a3jif9s62p1q2vralgg3mmy7vrmbagf826mf15dfqxis33g9wnf") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.22 (crate (name "ruby-prism-sys") (vers "0.22.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "10h0v5wwd728drc59ii1vkrqrizjqp4rc5bd1gxib09jsxfqxp0p") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.23 (crate (name "ruby-prism-sys") (vers "0.23.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "02521pgkcackc0wbapgyqfp3vzm5zxl2z7b9yyax5g4cm2i19l9v") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.24 (crate (name "ruby-prism-sys") (vers "0.24.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "0lgajj80qxx2d11gy2ywa8kw3v3g37lv9prv1r8bh7yd6msw2flq") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.25 (crate (name "ruby-prism-sys") (vers "0.25.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "0ci85gy0qxb62g1c8217y43vb7ghiyzfisicg97s8nsbz2mljgch") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.26 (crate (name "ruby-prism-sys") (vers "0.26.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "0yasp0w49wlip2kxyzcy6f1wgvqqczz2fgjcbqwl9dn2mr5mmvcb") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.27 (crate (name "ruby-prism-sys") (vers "0.27.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "0i9myv56x0qymvkh6r9ihbi464xcpj048100lsxfbrg8v8y7mvl4") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.28 (crate (name "ruby-prism-sys") (vers "0.28.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "0kvp5grfilrd7an0a7yfzvyj576fa83ybslwgcgfxb8k1w19iscr") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-prism-sys-0.29 (crate (name "ruby-prism-sys") (vers "0.29.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "1k73qmfm3z5yx153z8x2jjws93nspyy45mkdh311n8gxvamnnfbp") (features (quote (("default" "vendored")))) (links "prism") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-ruby-string-0.1 (crate (name "ruby-string") (vers "0.1.0") (hash "1kvs5bybq78nyx3amlb9rad5lamr3d7nvw1x6zpbyhyygaachnv0")))

(define-public crate-ruby-sys-0.1 (crate (name "ruby-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "088mh18r9zckdjy53776x219a9xy2vs9qa10lj6ba1fddq928d1c")))

(define-public crate-ruby-sys-0.1 (crate (name "ruby-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "186knb040i68xsspm4p5pmd9zsmawkiwwq5zxnjx2akwgn4fg8i1")))

(define-public crate-ruby-sys-0.1 (crate (name "ruby-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1r3cksl3p1jgczq55mxwbn1nlmsmfr79a9mg2y068yzddl6fgjph")))

(define-public crate-ruby-sys-0.1 (crate (name "ruby-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "16gs9xg026j73ys5qfgcglfsxx29c6nhz8gxi22fp8x8pg4k4mzc")))

(define-public crate-ruby-sys-0.1 (crate (name "ruby-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "11n3lfpp1gphmh2m9gj2rvvsi68mmaylgr5xbz85qp0amrgclw21")))

(define-public crate-ruby-sys-0.1 (crate (name "ruby-sys") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "04iwcmz3142gs678y2d4aigs4xmp1z0fdhb4y9mksh6q6pna6zf2") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1flx3gn73vrrsj2jjyw9kjg0yjwhlf0aqdrs95sambffwp311xcl") (features (quote (("unstable") ("default")))) (yanked #t)))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1zdik07z9v92vsxxij3wnfw8hdr7b78gyfxcch5d6hjdkyhvic7y") (features (quote (("unstable") ("default")))) (yanked #t)))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05qkss4ip10614w0dgp8zyy3x7r0f6kw7x4hj3yak6bd8lqigdpc") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1g8y227mjhqzdnacncixw7c48jab6p7lds06gl7s83fyqcw5xryq") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0bvs29hx3lknk6f1f988qwfdsh0ppjjrsir9a403dg1s8w6cy07m") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ndb8igs2k2cf2m15qjxqqffan1wrb5614ansl2s1isi815ifcph") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.6") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0a27km0a0q04188h7ndfygrw02fnm1br6rlisvgsl5ga1dwhmryq") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.7") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lja32xw99470c6nm2xxm7xs8y4jvfnya4vnc8ndd23f8mnlxkbn") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.8") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0h02l2yrph0mfqvw75qamw82i3mcg5hmjrq8xpga6rpg5c3yb663") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.9") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1755qd4rrld6j1ii9892dfifdar94w6fm3x08jbzdd1d4mzbzbd4") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.10") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1fykyh9nfmpn14b8nq78rlzwqpdhqhi5qsvvd9f041g34v009kvq") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.11") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vdfds4ji9gjy9xh4aspgcrax646m75741sgagwxzrym5qbk8c9q") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.12") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vhzhd670hgfa63ji3knnym1m4vvkaklckjxgda12l6j558pqk2h") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.13") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0i0bv779ar5vw4gwl9l2b19ilnvkjw96apvchpylaaiddyqcy558") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.14") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "17aqn9s4rm4icypkj0p89i8lwhlay8m789lk9dkm5dm8sgzhwl4w") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.15") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1mnbn73qvvd00dl8xf1hlgj3823s8bfmrn3iv817x5s4hlkvxrlv") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.16") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ay4hvk73vwq8r9h0aiwgn7lafqv18qiwvxkpq0l1243iwvik8ca") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.17") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1i0my1ay629iccsbgksyj9g0igmbmx5dp64fnzlzygybb4ly3n9p") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.18") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05qvz8yfxa94y63fnfh03k8d5i2308klnjxjw54ak885c12k7jvg") (features (quote (("unstable") ("default")))) (yanked #t)))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.19") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "13rcyzxssxh9vaq9dvxk6xq7pgnyccpc4wik619svdhb1c9zl8xm") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.2 (crate (name "ruby-sys") (vers "0.2.20") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1iq4p9s0l4m82qw5c13b2l54jl40nmlf2n3237f36dz9gkqhj5g7") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby-sys-0.3 (crate (name "ruby-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "07hw7b676jxv5xz9r8l6qmgfrd0vm6wxzcrc57vp4daaj1miggvv") (features (quote (("unstable") ("default"))))))

(define-public crate-ruby_inflector-0.0.5 (crate (name "ruby_inflector") (vers "0.0.5") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0mba6mvp1xj6aqgpdqj1y63fncg0w7w8nji99hi1msinmzip2n9d")))

(define-public crate-ruby_inflector-0.0.6 (crate (name "ruby_inflector") (vers "0.0.6") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0ycgd7ym1bdlfwmwhfi94abzfzjvlq9l197lyiz7v3kk9fbwk2k3")))

(define-public crate-ruby_inflector-0.0.7 (crate (name "ruby_inflector") (vers "0.0.7") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1by7vwfskczv5wr2zxfrbcy95mj8yfcnhwrqn8s898v4s1ms3g9z")))

(define-public crate-ruby_inflector-0.0.8 (crate (name "ruby_inflector") (vers "0.0.8") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02q0f24gcq7mx152vqyg1748c1k7xzqffjcsanbh03y49v4gsl4g")))

(define-public crate-ruby_inflector-0.0.10 (crate (name "ruby_inflector") (vers "0.0.10") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1j9zqdr9bclhq7j58lnmpw021bdikp199bs7hnzahl0mk8ap68cv")))

(define-public crate-ruby_inflector-0.0.9 (crate (name "ruby_inflector") (vers "0.0.9") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0wmbag0rjcxb10ws7xprkmqxcxl8fwi0jwlcyhin6v03ray0lw90")))

(define-public crate-rubygems_api-0.1 (crate (name "rubygems_api") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0wfys8aaj6wpw5fm27yarqnsrvndk651yrbk9ah06sb4rby31qzi")))

(define-public crate-rubygems_api-0.1 (crate (name "rubygems_api") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "19vjszi7hqzzz1877j8vgcc6gcqzxrc8xvy1l8xwdm6y2x3falwp")))

(define-public crate-rubygems_api-0.1 (crate (name "rubygems_api") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0c1j17ib47zqxjwbx8vbizcphz9zq5v7kaivwdch1b8y376n7b9i")))

(define-public crate-rubygems_api-0.1 (crate (name "rubygems_api") (vers "0.1.3") (deps (list (crate-dep (name "cargo-husky") (req "^1.2.0") (features (quote ("precommit-hook" "run-cargo-test"))) (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1j5rx9nla0iia2hmdx3ibsjgx0cf20shlh47yladv3l79vfm2p03")))

(define-public crate-rubygems_api-0.2 (crate (name "rubygems_api") (vers "0.2.0") (deps (list (crate-dep (name "cargo-husky") (req "^1.2.0") (features (quote ("precommit-hook" "run-cargo-test"))) (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0p8zrqqsg8z945ywqn751k8iz6lj92bs48n23k4w25qzm2gi2yc1")))

(define-public crate-rubygems_api-0.3 (crate (name "rubygems_api") (vers "0.3.0") (deps (list (crate-dep (name "cargo-husky") (req "^1.2.0") (features (quote ("precommit-hook" "run-cargo-test"))) (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1d9r9cnaz4sw2d0gjp97gsmkdxr0vbcwfw8sq1rsyr3lxx6nnarn")))

