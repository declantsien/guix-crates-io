(define-module (crates-io ru _a) #:use-module (crates-io))

(define-public crate-ru_annoy-0.1 (crate (name "ru_annoy") (vers "0.1.0-dev1") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "1a56mpp09j3r7dvbqddbyna9ba8i2k6vyzwspj82kfhn0pksmhrr") (features (quote (("cffi" "libc"))))))

(define-public crate-ru_annoy-0.1 (crate (name "ru_annoy") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0") (default-features #t) (kind 0)))) (hash "185fyl6sy3i5knz5s59glwnydjbmk9q6b3kkaw0z1h8w6h49l9v0") (features (quote (("cffi" "libc"))))))

(define-public crate-ru_annoy-0.1 (crate (name "ru_annoy") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0") (default-features #t) (kind 0)))) (hash "14jnbsadh1rr3j5nhf4al4isckhzibhjblb249jl51dim4fvk8sx") (features (quote (("cffi" "libc"))))))

(define-public crate-ru_annoy-0.1 (crate (name "ru_annoy") (vers "0.1.3") (deps (list (crate-dep (name "hashbrown") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0") (default-features #t) (kind 0)))) (hash "0vi5g6myz5vp0bwbd4qshgbj4qzwqbq47r181wi0ypadk3krbqns") (features (quote (("cffi" "libc"))))))

