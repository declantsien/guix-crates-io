(define-module (crates-io ru gs) #:use-module (crates-io))

(define-public crate-rugs-0.0.1 (crate (name "rugs") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1dh1l719id38nydha7msmw1bxgl4g09c2nmb0dx3namhqbbg7z7a")))

