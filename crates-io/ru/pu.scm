(define-module (crates-io ru pu) #:use-module (crates-io))

(define-public crate-rupushdeer-0.1 (crate (name "rupushdeer") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wq0gfd65gp4h96nlj5amgxlw6p7wi404bb7acynqna71a9v4bnj")))

