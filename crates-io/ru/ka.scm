(define-module (crates-io ru ka) #:use-module (crates-io))

(define-public crate-rukako-0.1 (crate (name "rukako") (vers "0.1.0") (hash "072q1n30rphn5971rlba6n4prcbv9dfn1l84qsyw52a1ramv1lxl")))

(define-public crate-rukako-shader-0.0.0 (crate (name "rukako-shader") (vers "0.0.0") (deps (list (crate-dep (name "bytemuck") (req "^1.6.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "float-ord") (req "^0.3.1") (default-features #t) (target "cfg(not(target_arch = \"spirv\"))") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (target "cfg(not(target_arch = \"spirv\"))") (kind 0)) (crate-dep (name "spirv-std") (req "^0.4.0-alpha.10") (features (quote ("glam"))) (default-features #t) (kind 0)))) (hash "0slcm8m9bcyrs7kzr6k5xxskdjaacddwhs9m5qby31s3lrn4j2hx")))

