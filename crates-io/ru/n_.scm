(define-module (crates-io ru n_) #:use-module (crates-io))

(define-public crate-run_cmd-0.1 (crate (name "run_cmd") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0a63n8b1ilzmgxhg45gillf4c3d2ncpxak8cf4w3zi32a4ir545l") (yanked #t)))

(define-public crate-run_cmd-0.1 (crate (name "run_cmd") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kbwl9zqkh7wmb0l3319mbqn8v65417g5kdd81gkfiwwad0cpf69") (yanked #t)))

(define-public crate-run_cmd-0.1 (crate (name "run_cmd") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zddbglrzgbqvgsx61i3dh0m8a9p4nq2svmjjiz8p8pcpcf8pggg") (yanked #t)))

(define-public crate-run_cmd-0.1 (crate (name "run_cmd") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nfvaq1zll00a2igczpm3hvfg5343l3ar8fjhafhbgjig87ls085") (yanked #t)))

(define-public crate-run_command-0.0.1 (crate (name "run_command") (vers "0.0.1") (hash "12x745nad0im4cjys50ya6ccignz40bcnzzc52y0g710kz45al8d")))

(define-public crate-run_command-0.0.2 (crate (name "run_command") (vers "0.0.2") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "1g25vw60sxaw23lz2bzsgwrcymya8pnkasnlbd8lz6ihhzif9sx0") (v 2) (features2 (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.3 (crate (name "run_command") (vers "0.0.3") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hjk0fxa64x74ssr643sqv004qkfj252lfns6vzv4hpcx7gl49y7") (v 2) (features2 (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.4 (crate (name "run_command") (vers "0.0.4") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "01k17c8kw43a28bn3g9d4y0yy6kq68a99ks6m3m5bqmv91va79hc") (v 2) (features2 (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.5 (crate (name "run_command") (vers "0.0.5") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "03kv4nli7cqvg6w6anqwmh94b0b84fjslqiym15la1krqv4xj06b") (v 2) (features2 (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_command-0.0.6 (crate (name "run_command") (vers "0.0.6") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "01skcwlmlz4ag7ap99q6rncna2y6xyrviv5gayx0kn2bm83133iq") (v 2) (features2 (quote (("async_tokio" "dep:tokio"))))))

(define-public crate-run_it-0.1 (crate (name "run_it") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0p5gxnfb4525j8yin603ln0rbz38m7vy0yg8issg7azw37i5rhi5")))

(define-public crate-run_it-0.2 (crate (name "run_it") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1wiaabmcrbqswl1ninp26zjgnnm5a0lkff6cvj2qyl593faxcm5r")))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1xmb4q6szjp0spkxgzfdcxjygxlkiwamjmaiakp1ha16827pdz09") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1528rbqjra9m2aznkjs1xgz25z8wshr9yk9a4151sapn0by644ss") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "04pb1vxh64ch0xxw0748in35kyj6w2q81incmshlbzqk3xcqqybf") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "16aww3sq6nivgg8byzpx1hq1psh6amqzs4xs7bs7s0dhzbqg90z3") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0wl4k157q7bw849rrqldwsxh259qkjfzan2x1m3snfz359ivk0wi") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "13xiympykaqksfq5z1m3dv7pb0m75hga590v1f8dp30cgsx4japk") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1dswm5iziy34v2ml96s89h4r0201cf195vs213wlwj55mdlhjyvg") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0250xrgq31zf81v8bh1l8zzp54c540g5y2baf9qfssa513xiw32c") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.9") (deps (list (crate-dep (name "rand") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0rrkag3lk80fi2nsg9sybmdckc4kckhnszxjg7x7byvr53an6fvb") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.10") (deps (list (crate-dep (name "rand") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1idffidxidgrfl6b1v5n0pl4skzl9xr2n6nbrm6vm0bvdl0b0zrw") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.11") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1ys6vv85lpb8xnk1mjpdvwzqz986gqdx7bi8hpyx11w9wva9pzvr") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.12") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "07lz5rc6zg3l4i1mjgl3csprjcv9jnmhlp85k9xlp0k4bkdhbnd2") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.13") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1qd5pzvpqmsrlpklspvvngmllib2l6cf7qm8hngf2r2iilzgd3vq") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.14") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.6.1") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "13liip3i20d7y4fbj3w5vy4c051qgjvbfwnsvlv19750k8w7sgiw") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.15") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.6.1") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1jna9i7rpr0d3m7qa8x4j80izs0vkmnddx52sf9hlagcshy8yzk7") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.16") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0vsrj874srrcac8npai46b1ljwfcy3w38qbs706kyby3s512xfzy") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.17") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0y12x92j3vf2m2vy095jzpq3bllp7zhf08cq3im164271rkd432w") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.18") (deps (list (crate-dep (name "rand") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "18dvw7152l7j8z40rikd4ifsd93gh32jw7z61jh4h3ac3w56z6ln") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.19") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "08yyiil4i7jw0n65svbqfz6abdc2yd0xbbpwmvcb1i13q769g432") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.20") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1v9xdxg5kjj8rxj2cdniqjfnh1x79h4w9blzkjiy4k0gfkim9gg5") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.21") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0l974zgl1gpq6pqxwgxxpqrbvsc9hn34kmhif9sdbblmdddnpbjx") (features (quote (("default"))))))

(define-public crate-run_script-0.1 (crate (name "run_script") (vers "0.1.22") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1g9k3vi9p2ljnkzwk65pca19sqlg0y91qnc27g118kgrdxjjf2jv") (features (quote (("default"))))))

(define-public crate-run_script-0.2 (crate (name "run_script") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1w3rrnri31n1k1arxn36n4avsjf8530icklh884kyri9wl15xn4x") (features (quote (("default"))))))

(define-public crate-run_script-0.2 (crate (name "run_script") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0g8psli1dzxrhkdlvg82jrr5anwabs38lqx3dwr96yk8q4kjb09r") (features (quote (("default"))))))

(define-public crate-run_script-0.2 (crate (name "run_script") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "01cvnihbaxxrll4vjg5w9d29iyvasgq5lpaa5m5n3y07nf0280ch")))

(define-public crate-run_script-0.2 (crate (name "run_script") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8.1") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0xjzzcza8ammirpxk83rywrq4ygd47x697fzhf3w9ds7snadql2x")))

(define-public crate-run_script-0.2 (crate (name "run_script") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8.1") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1z8c9wafvycv8myrlz9f6s8m30ppx5kjjnjqn49n3ar5s43hyi7p")))

(define-public crate-run_script-0.2 (crate (name "run_script") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.8") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0lis2ck2f86552k09nnmdzn5sm430cdzb4jdb6dxsdl6nqid4s04")))

(define-public crate-run_script-0.3 (crate (name "run_script") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1y69p9rghlrmlsjdqzyyq05204wfjbcsj1ymdns15a0fxqq6pl5h")))

(define-public crate-run_script-0.3 (crate (name "run_script") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0iwcw7dz3wjxwrxlhv3i3yh9b5mwfa4l0zyxbyqwf0p2n2b4hh5h")))

(define-public crate-run_script-0.3 (crate (name "run_script") (vers "0.3.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0zwkaf44f9ghk52a0r5xi2n6kbqk479f4y414czgqnp1hbnsbcv1")))

(define-public crate-run_script-0.4 (crate (name "run_script") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1p3qk2qz8ibjfj6jdiqbb4ykrwc7x621m9680s0dbwxz1y8cqznc")))

(define-public crate-run_script-0.5 (crate (name "run_script") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0zya82dhjgigjx1qwn0y175anl0xhdmva9ljxy9fc809hyp5j0bw")))

(define-public crate-run_script-0.6 (crate (name "run_script") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1b3m6a9sd5nyxr0r80m2q4qcjjyah47n8s6rmnx73vn7p54bpyrd")))

(define-public crate-run_script-0.6 (crate (name "run_script") (vers "0.6.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "08b6pmsmdz1ik995fbx8ymchamfwxlmdb6qjsdbwryia3saig8l0")))

(define-public crate-run_script-0.6 (crate (name "run_script") (vers "0.6.2") (deps (list (crate-dep (name "fsio") (req "^0.1") (features (quote ("temp-path"))) (default-features #t) (kind 0)))) (hash "1hdcilnj3rd2yhyzvahhp0isrxl05ld4gmwa7qx28agrisvbh9wm")))

(define-public crate-run_script-0.6 (crate (name "run_script") (vers "0.6.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fsio") (req "^0.1") (features (quote ("temp-path"))) (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)))) (hash "1d1wlz8l93pkchv4238z0kqv0mqj3qv488gy6m5a05bq0qszrs58")))

(define-public crate-run_script-0.6 (crate (name "run_script") (vers "0.6.4") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fsio") (req "^0.1") (features (quote ("temp-path"))) (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)))) (hash "09m63b7wcg50s7kh8lxii5lqk793mmfm99djnp5az31dfsvfzxbh")))

(define-public crate-run_script-0.7 (crate (name "run_script") (vers "0.7.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fsio") (req "^0.2") (features (quote ("temp-path"))) (default-features #t) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)))) (hash "0v6f26aprbipq19pqsn61b08if7114n6qwamcvzh8qv9dlbj5h71")))

(define-public crate-run_script-0.8 (crate (name "run_script") (vers "0.8.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fsio") (req "^0.2") (features (quote ("temp-path"))) (default-features #t) (kind 0)))) (hash "1vcds46fn0ca9kaa64fb88yb8qpk04vp959nshvwahj2d76bx93z")))

(define-public crate-run_script-0.9 (crate (name "run_script") (vers "0.9.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fsio") (req "^0.3") (features (quote ("temp-path"))) (default-features #t) (kind 0)))) (hash "1vz4v1rwaygnmj3wj0iiii8mrc49lvriqy7fhq0v8xkzwc9m5n2x")))

(define-public crate-run_script-0.10 (crate (name "run_script") (vers "0.10.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fsio") (req "^0.4") (features (quote ("temp-path"))) (default-features #t) (kind 0)))) (hash "0c7jx8xpscajq6bg53x43m1ci4b79j2pmbvywhny0n5dlyrmbp3z")))

(define-public crate-run_script-0.10 (crate (name "run_script") (vers "0.10.1") (deps (list (crate-dep (name "fsio") (req "^0.4") (features (quote ("temp-path"))) (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ix9kf1b3h5vmdadpv7rfxylmj8mphlbx0xgv6frhy4dqpyri7w2")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.4") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0clpja54pmf3xz24maxs943ac4mvw8pbm4jh0yiispbr5jlr6k7z")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.5") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ypv48ph0xv4fjhaqczapy6835mrj4j864cpzw9q2vblkgy16qdc")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.6") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r2sr4p11kg6rsy6040x5y4hl62ss0n2h1v49p27dvavnm5d5cwr")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.7") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "06n1ljf4wf65j5rj8cvkcawx4d78klgzdd7p2qnqigfm40wpcvan")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.8") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1aa0ckdkpbspqfwrs9ng3ichbzw6n3ghfpv8wp21i4n3i43v0f3d")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.9") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f2dn50gzmrn1ys55lxg4483sgyjqlv9r05aw2118rs05vw86yya")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.10") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wgmhn7bpxfx3ng31nfl71cbnx13bmmqfyv7ri10lc3h7rp20gcp")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.11") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "103jxkpbh4wgk6y63wcdvg829sh925rlm1367v31y5l54kwhxdq0")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.12") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jda386m70hfvx18y4kvhn34crzz2jb05d303f9wii6w5h3m9hrr")))

(define-public crate-run_shell-0.1 (crate (name "run_shell") (vers "0.1.13") (deps (list (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "07i5r1qx4znfgg4cz2hd43fpn0wnfiqcgagsyrzj28ibj7sdl14m")))

