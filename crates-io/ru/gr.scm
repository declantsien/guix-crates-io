(define-module (crates-io ru gr) #:use-module (crates-io))

(define-public crate-rugra-0.1 (crate (name "rugra") (vers "0.1.0") (deps (list (crate-dep (name "sfml") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "15rwky7zy8k3w87bky7snkbkm7asmrc7cf3ggy4d84b2qiql7amr")))

(define-public crate-rugraph-0.2 (crate (name "rugraph") (vers "0.2.0") (hash "1bi5hz123kw3akj9x6fns971fywzvwgndvqqi2j9r8s1cvyrfrjb")))

(define-public crate-rugraph-0.2 (crate (name "rugraph") (vers "0.2.1") (hash "0xs0qy0671d81697nkfy0w6h3ak1qk7s61lpj57mvsmgdl40p1d5")))

(define-public crate-rugraph-0.2 (crate (name "rugraph") (vers "0.2.2") (hash "0i43a0dh0626ymlqzs7vc1igsfbpcbxkz0iq1k9mbhxbiahsg8fk")))

(define-public crate-rugraph-0.3 (crate (name "rugraph") (vers "0.3.0") (hash "0lkfdpn4ncfnp9frkz7dkgszki94l0h6v784yzpn6f23j1vxz67l")))

(define-public crate-rugraph-1 (crate (name "rugraph") (vers "1.0.0") (hash "1k902h62z9hkab1jnc5pc61l3bhcral0vvsmyzwqggjgk8gxdxi1")))

(define-public crate-rugraph-1 (crate (name "rugraph") (vers "1.1.0") (hash "0y0qz5xq9z9bdnzg7zx07wk5rj0b6qa2gdvy6d1bg0gyi2cqrnkz")))

(define-public crate-rugraph-1 (crate (name "rugraph") (vers "1.1.1") (hash "1njjkynr6bimmkx7ig4awzp9kk9vgp1hq071n3gwn768lcm8wl6l")))

(define-public crate-rugraph-1 (crate (name "rugraph") (vers "1.2.0") (hash "16gciqdh1rwk2c87dsryn9jmd2sl4g818wjb39ajdbynfzinsryx")))

(define-public crate-rugraph-1 (crate (name "rugraph") (vers "1.2.1") (hash "07h0x3s0lhs54d00lf80pizq1ica7wc4gqmnngphqgcq2mafj5hi")))

(define-public crate-rugrat-0.1 (crate (name "rugrat") (vers "0.1.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rugint") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w378zchzcyafvvg3nzf4ccsy60pmgviizlkd1cdh5jfdxiib0b6")))

(define-public crate-rugrat-0.1 (crate (name "rugrat") (vers "0.1.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rugint") (req "^0.1") (kind 0)))) (hash "1vp351khriwld8750q8d2fyb04f59y61sknm947i2cfkb6df6w2m")))

(define-public crate-rugrat-0.1 (crate (name "rugrat") (vers "0.1.2") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rugint") (req "^0.1.2") (kind 0)))) (hash "0yqc3wdk5lmdc5bdbniq2z35jw3r16m6wqcia5c1p8kdw3f0fv2v")))

(define-public crate-rugrat-0.1 (crate (name "rugrat") (vers "0.1.3") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rugint") (req "^0.1.3") (kind 0)))) (hash "1lfc424a4yj44y7k5as1df4gnbmbmiqa4pxs2k3mgkdd2w0fqmxz")))

(define-public crate-rugrat-0.2 (crate (name "rugrat") (vers "0.2.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rugint") (req "^0.2.0") (kind 0)))) (hash "025xgvqs9y8x33j08w5a6aix4blswm5dkwj974ds0yjixsxjjg9g")))

(define-public crate-rugrat-0.2 (crate (name "rugrat") (vers "0.2.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rugint") (req "^0.2.1") (kind 0)))) (hash "003510df8bwjr6pkwr40q7b8m1x8rnbyml5ac48wm4rx6x68mjmj")))

(define-public crate-rugrat-0.2 (crate (name "rugrat") (vers "0.2.2") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.0") (kind 0)) (crate-dep (name "rugint") (req "^0.2.2") (kind 0)))) (hash "05i1kgi9gfw9dx3dvw58l8jzsd36y5hybr9y68b0fq7wj16b0kid")))

(define-public crate-rugrat-0.3 (crate (name "rugrat") (vers "0.3.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.0") (kind 0)) (crate-dep (name "rugint") (req "^0.3.0") (kind 0)))) (hash "06p3yksgdpk28pz7w28gcyjw5r77jn9ll4gmy6m7j9pq8cq74dqr")))

(define-public crate-rugrat-0.4 (crate (name "rugrat") (vers "0.4.0") (hash "17xhh56b7rc6yy1qhbyi0xkymvxs1xfdsnsqlsbd58bamr0g73xl")))

(define-public crate-rugrat-0.4 (crate (name "rugrat") (vers "0.4.1") (hash "1vjqn6bsh43kscjqmz7h7lfvwf50x3pfclxzd06vcyzrh6zipy3s")))

(define-public crate-rugrep-0.1 (crate (name "rugrep") (vers "0.1.0") (hash "0b4z67ma89x301d235yq5n4bsax1p3faals83pficrp6iwbsmwr4") (yanked #t)))

