(define-module (crates-io ru at) #:use-module (crates-io))

(define-public crate-ruatom-0.1 (crate (name "ruatom") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1ra65gjfvnsy5rc1jlv7579mv5bsn41hc4mh3i9n0ryp5ayk3f0k")))

