(define-module (crates-io ru sw) #:use-module (crates-io))

(define-public crate-ruswords-0.1 (crate (name "ruswords") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "xvii") (req "^0.3") (default-features #t) (kind 0)))) (hash "16xbwpxw0crmpr8dcayb2gxc5l03mhq8c6hy9rwijcw1lm62lshy")))

