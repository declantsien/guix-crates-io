(define-module (crates-io ru sl) #:use-module (crates-io))

(define-public crate-rusl-0.1 (crate (name "rusl") (vers "0.1.0") (deps (list (crate-dep (name "linux-rust-bindings") (req "^0.1.1") (features (quote ("all"))) (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "02aas7wfj0p3545h145aa3vzpzcrfr2nkimx7vx8zr0mvf22h0yi") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-rusl-0.2 (crate (name "rusl") (vers "0.2.0") (deps (list (crate-dep (name "linux-rust-bindings") (req "^0.1.1") (features (quote ("all"))) (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1y2xynnv45c1833wigc1zyalybm0djalild10qxy5qilbc7sq16j") (features (quote (("integration-test") ("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-rusl-0.2 (crate (name "rusl") (vers "0.2.1") (deps (list (crate-dep (name "linux-rust-bindings") (req "^0.1.1") (features (quote ("all"))) (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "140ynj51kk5xgy111l8vc2r851k9j18lmsqpk0v1yrjr54fx4qhv") (features (quote (("integration-test") ("default" "alloc") ("alloc"))))))

(define-public crate-rusl-0.2 (crate (name "rusl") (vers "0.2.2") (deps (list (crate-dep (name "linux-rust-bindings") (req "^0.1.1") (features (quote ("all"))) (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0mavkjhhkcz4qz1grz580bn25q5jshw18jlkpwpw7knwlp9vx07z") (features (quote (("integration-test") ("default" "alloc") ("alloc"))))))

(define-public crate-rusl-0.3 (crate (name "rusl") (vers "0.3.0") (deps (list (crate-dep (name "linux-rust-bindings") (req "^0.1.3") (features (quote ("all"))) (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1apqc8ydchdbii1dhlji5hnv2l93d2f6cqnyxs9ahig1z6vz7kip") (features (quote (("integration-test") ("default" "alloc") ("alloc"))))))

