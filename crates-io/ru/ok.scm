(define-module (crates-io ru ok) #:use-module (crates-io))

(define-public crate-ruok-0.1 (crate (name "ruok") (vers "0.1.0") (hash "11qjisa4wxsnjc7j75pxxhd76kmcx2bfd5d80xj29gv5v5yd0lli")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0pclc0vsl4xksxiqnq78vvqaz327h6xmzxd7jfnj21s1bh5q4aqp")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1vzs4r3bvnzlqr80g5d354da3dskqydqxkfsd4g5xvqq58zj0fza")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1i7q22kn6qgbcpxvbr44va5kasxbfs66i3bba2ci34p4229kw10i")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0pb49yl029jkly6k3b8gi2vk0fwdvv5xj0v3m550xmvnj98mvpj2")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0qk7lvzcr0wv3rjipb71dv4rhx29clr812zmz9v8pckgwmj0a38l")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "16pmpnfsq7jg1nqdf4wbyrzqiq4acylm2fjla3271clafz687y5m")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0n1afqwav81v31w2ji63accfzicqay4kc599c0043p91aqfs4793")))

(define-public crate-ruoka-rs-1 (crate (name "ruoka-rs") (vers "1.0.7") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.43") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "03cnjkhq3vvscayadkfdfinrm54lizy2m9fymw1ra8zynn4pxvif")))

