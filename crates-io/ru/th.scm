(define-module (crates-io ru th) #:use-module (crates-io))

(define-public crate-ruth-0.0.1 (crate (name "ruth") (vers "0.0.1") (hash "0hzlc9mw43l4k6f42h3v8ada9l536b3n4y66pkdimm5fl13ph2v1")))

(define-public crate-ruthen-0.0.0 (crate (name "ruthen") (vers "0.0.0-alpha") (hash "0mlckq00vz1sf8xp5zmsg9q2ifjnhajib6fqqgknmv9rfyj1wz9s") (yanked #t)))

