(define-module (crates-io ru g2) #:use-module (crates-io))

(define-public crate-rug2d-0.1 (crate (name "rug2d") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0y1w3i3wm2rq1xv1ir2mjgz6xncf3fy971560zrn3pahasa7ii3m")))

(define-public crate-rug2d-0.1 (crate (name "rug2d") (vers "0.1.1") (deps (list (crate-dep (name "gl") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "16afmxnmi007mhcllx2474k13zlvl08l21q8644vlwlsihn14pnl")))

