(define-module (crates-io ru li) #:use-module (crates-io))

(define-public crate-rulid-0.3 (crate (name "rulid") (vers "0.3.0") (deps (list (crate-dep (name "base32") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "~0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "01p79w98v8k140i3q2g7sqj1vhh7hrz86gq6vhrjjgvss2psd8rm")))

(define-public crate-rulid-0.3 (crate (name "rulid") (vers "0.3.1") (deps (list (crate-dep (name "base32") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1038k1kymhin7jf3b5ng43pyn6kmmak7gzwixj01jmifbr38jppl")))

(define-public crate-rulinalg-0.0.1 (crate (name "rulinalg") (vers "0.0.1") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.31") (kind 0)))) (hash "1mgkzymy7ivpq5yrsscnvkl3kqlan3w03mnvgbi1i1hbipgajv38")))

(define-public crate-rulinalg-0.0.2 (crate (name "rulinalg") (vers "0.0.2") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.31") (kind 0)))) (hash "1knx8pddapsq9cqgwl4sgl0ymk2msiwni5lnzldr5i241jmyp9bp")))

(define-public crate-rulinalg-0.1 (crate (name "rulinalg") (vers "0.1.0") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.31") (kind 0)))) (hash "1fz974kifay203bp5hnhxbj9ykwnhiibbc8pc2dkj4p1129zxkg8")))

(define-public crate-rulinalg-0.2 (crate (name "rulinalg") (vers "0.2.0") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.31") (kind 0)))) (hash "0fk35qx1k690dlrkcxwj58hw6ki70fblbizks1lr4vvq4ji0dl6k")))

(define-public crate-rulinalg-0.2 (crate (name "rulinalg") (vers "0.2.1") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.31") (kind 0)))) (hash "1iwvk13hdi2wjypc2n7bffqid8vx6ralr1by3df98d1yrd6jldbj")))

(define-public crate-rulinalg-0.2 (crate (name "rulinalg") (vers "0.2.2") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.34") (kind 0)))) (hash "135hd5jk1nz4h6xlh25zrxaldgrcjzr6zzb58xlinwbk3yyjd249")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.0") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.34") (kind 0)))) (hash "1wilx6kgpx2x3gkqvph3nj54rnsrwj44g72wxcv1wxaj11jg32kv")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.1") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.34") (kind 0)))) (hash "1fb3mw8ghfg83zv3g0x5l074ka43kxdl5k7aw9kjrzmxjlfc4qq1")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.2") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.34") (kind 0)))) (hash "1q4gyxa9bsk9g4x0q2awf6acxpzj70mc7vvl9di2vdcai50rnlbf")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.3") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.34") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "19jrsfrkksyfkj078ycj5jha5mw37yz8nkmzli11ck9l5li1jmj7")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.4") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0jc1plkjzsdz3f80plhil5hlniz611b8ah2yhv7kjaicq5cixvs9")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.5") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "15h65274zc2ayyq4n41ihjbxpchf4q544pas5bdkqad42h4l015g")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.6") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1dgk9kwfgsvqpxfi37bbyrinzqz5l2wcgx19pvk1v9rxvm59mr1z")))

(define-public crate-rulinalg-0.3 (crate (name "rulinalg") (vers "0.3.7") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "11kzqld1n71gd2j1rbyq3xgmjmlvaiyg0afvdd1x9cny2wvwww2n")))

(define-public crate-rulinalg-0.4 (crate (name "rulinalg") (vers "0.4.0") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1z5v8h2l08a4pyjq95x79z7pgnskfai4xgy53z97w7pc955jkhmx")))

(define-public crate-rulinalg-0.4 (crate (name "rulinalg") (vers "0.4.1") (deps (list (crate-dep (name "matrixmultiply") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0dz1yzs4zal9hjjr02ykp0x29n6avya6wxlwjqz1pz0xbjymkc4r")))

(define-public crate-rulinalg-0.4 (crate (name "rulinalg") (vers "0.4.2") (deps (list (crate-dep (name "csv") (req "^0.14.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.5.9") (default-features #t) (kind 2)) (crate-dep (name "matrixmultiply") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0bxi7g6bknm3fsvappyk0zwdnn5kja75f322lxr1spk8r41a5b84") (features (quote (("io" "csv" "rustc-serialize"))))))

