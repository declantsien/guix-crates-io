(define-module (crates-io ru wr) #:use-module (crates-io))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.0") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0vqpbn8867qcnghp59s7makrp0dy67kd5p6infarjfc05xwz98hb")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.1") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ncwc6wnd9617f8gd4aqjvjp7z6g56jmpql85wy2vzygqf7r56ni")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.2") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r99sa83qdmsvjpdmgc730ksz895wyzzwg2w0s8cfjpgarqhrq5s")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.3") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wfnvldiqrdk01asixzs134ibfxqhsf9iiscbg6d3wd2kpa0369c")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.4") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0szgjk1szs7cllcfh1nvla6hgi923kyicvc2af2m4gn0kg28xg6c")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.5") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rrgibhai2d58h9zqpm2i38iy2szv0rz78d2d1m4bb7ifq468b57")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.6") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "10qbwfrfg1aiwkbbs9xy0rzs38n9ckjrwcb20j2lwcdj566l05sk")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.7") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1alfdjzq9ngbi7hd4g70c28dvap0slzqvb3rf4sf1shi2bya5lh3")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.8") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1gmr7nbi8a3mbx23wbxh66f5khx5pb5xvchn8ww7ai1wjcpyzw24")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.9") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0gn0n62krg0jqj84nkps4zzma9f4w6fc2jyxp9gj5nf8w2z9pm69")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.10") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14dix08kc4x5sw8anzsh1xjjk5d1f176vzm29n8znci4drm0hkkq")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.11") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ziz3nd8dyv8qnhrdrrmwp86bxi0aq9z3q5wm808a3m6d7idz5kd")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.12") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04w33x0ry66711zaz2f9zila890vybjbi831vwlr4zkgi6z8axh8")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.13") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "178cjs7gl3imxkkai8m06x67f8m1yjq42drlfk4swks8p2ik0kc0")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.14") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1f3cl5s9cidbfj6dlm9ki4j14l41awq160npiz95dk0cp0bsw2ql") (yanked #t)))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.15") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05sx442zfl4pmxli6lj3b1grs1s2852f9b12lvsi8cyfsz73032q")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.16") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0bfq1696s8yw0c1wx5rys3id8lp054kifz3jkrks6d2qcm6nif6q")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.17") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r25napaaw3dckhsvrbd5d7vrm7nzli1yx6pn5yanvcs29cv5s8j")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.18") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15jm6shnwzf9i8hn39x90gphzqr39nmqrll2whgpqfpa8hpy459f") (yanked #t)))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.19") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0j7i6xpfhvn2inc35ni12q43jww0zn1pg0zz8f5q2xllzifqca38")))

(define-public crate-ruwren-0.2 (crate (name "ruwren") (vers "0.2.20") (deps (list (crate-dep (name "ruwren-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0s316x78d1mf4n82934k1y37x39l715lzyphyq7mblarhfwmbz62")))

(define-public crate-ruwren-0.3 (crate (name "ruwren") (vers "0.3.0") (deps (list (crate-dep (name "ruwren-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ylf7m8qd7z547znw4r1i9kymq1bsc5cxq47apbcfb6x9cjbkqw6")))

(define-public crate-ruwren-0.3 (crate (name "ruwren") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v0mj9nsnfx7g2307nymppqxhpj275vy5nyp6vb8vv35xlr2fcl5")))

(define-public crate-ruwren-0.3 (crate (name "ruwren") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kikmw1p1nfwb9qkvfwpbcf4a323sdk37d5p5aw1p3lk9z5m1z75")))

(define-public crate-ruwren-0.3 (crate (name "ruwren") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "07hl3ghniqyjvb4clmq1f52y5h8vzsncg8w85y41ws0arq4sn262")))

(define-public crate-ruwren-0.3 (crate (name "ruwren") (vers "0.3.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cssgjsy7znr2swizlh1h1y36ssn5zqpyxpljqvhcr0f17jg8x4k")))

(define-public crate-ruwren-0.3 (crate (name "ruwren") (vers "0.3.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0p556hj3zhkfq746kagmh5ri2i0p74zsvhiyzhg4licyrrrbvcny")))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.4") (default-features #t) (kind 0)))) (hash "0g6jybb8x0lgw688z4s1zxwdf6sd4wshnr0w4l2s1hh7gn592aq5") (yanked #t)))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.4") (default-features #t) (kind 0)))) (hash "0f27402l5aj3yr1bx5vf8zb91q44j4lzq9a1963zpxrc5dvhn5fk") (yanked #t)))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sbyysv7xrckgp9q7qkv6hm1nw1v07an7s99fjirlfg6gzgayclq")))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.4") (default-features #t) (kind 0)))) (hash "0kz4zyha810g1sk3ljsyqddqvljkwi63jbfa3nij28nkgikm5g87") (yanked #t)))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-sys") (req "^0.4") (default-features #t) (kind 0)))) (hash "02wraa2gkjqwpk7c4r0q4hprjpvrzz4w3dds2i5jmraic8hmhxz6")))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-macros") (req "^0.4.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ruwren-sys") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0n3b3c3ccr4w09hwv75sp9p227gd098kfbqddp1r0g9sc7pbfln8") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-macros") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ruwren-sys") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0jbhxiyas296blzx3h54k2nd08skan07a1mnk5rckwc30mjqawkk") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-macros") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ruwren-sys") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0ylffsgvf2r1xyp0hn7pgsif182f0ir10b25s9m64y4wpkbgzfx2") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-macros") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ruwren-sys") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1h56rxhfdr2b6k8szs5aq1b2z6zkf7l1k5alcyxzzac9kc2y2rsl") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-macros") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ruwren-sys") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0abj86pm7d5gjlba7v6k08pkx2fxnx7rkmcj16w5a2nn8dwjxfgg") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-0.4 (crate (name "ruwren") (vers "0.4.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ruwren-macros") (req "^0.4.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ruwren-sys") (req "^0.4.10") (default-features #t) (kind 0)))) (hash "1azwrn88w3x3fdd9j336vayc8zjl45q62655pawwk6jyd8lsyi7d") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:ruwren-macros"))))))

(define-public crate-ruwren-macros-0.4 (crate (name "ruwren-macros") (vers "0.4.5") (deps (list (crate-dep (name "deluxe") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jszmavh5dj9d6ik427d9qwcpqkaqv9n8q851x95gz68hkcjhj2k")))

(define-public crate-ruwren-macros-0.4 (crate (name "ruwren-macros") (vers "0.4.6") (deps (list (crate-dep (name "deluxe") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08x51xj961l2mw74n8792rx52xg0smf07221g5gml38brly74j3p")))

(define-public crate-ruwren-macros-0.4 (crate (name "ruwren-macros") (vers "0.4.7") (deps (list (crate-dep (name "deluxe") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0xpzdnnb3cfh1yyln8d6mrdcny4jjgr18p6gb7wmw31li0qypyqi")))

(define-public crate-ruwren-macros-0.4 (crate (name "ruwren-macros") (vers "0.4.10") (deps (list (crate-dep (name "deluxe") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0aw5z4ynpcr4p73ngnvfhszbxg83l000hyihzs9m2746b74j9bln")))

(define-public crate-ruwren-sys-0.2 (crate (name "ruwren-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yi9ibh0wjnxy3vh1r4ncbjb6721vcim8n9imhzk2bzy11har7zb") (links "wren")))

(define-public crate-ruwren-sys-0.2 (crate (name "ruwren-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r2jh8fyaxha1xw35m49jvahvwq0i0hzwvqphfjmy1vqqi0x362z") (links "wren")))

(define-public crate-ruwren-sys-0.3 (crate (name "ruwren-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "02qjqc088ilkr49bzrb1ixd3z19gsbjrsrgspj02lrlvz5fyk904") (links "wren")))

(define-public crate-ruwren-sys-0.4 (crate (name "ruwren-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "07659wiyhdj49wdznkgdwv67fjxjijlw9aacbpgqpbp5j88c3ny3") (links "wren")))

(define-public crate-ruwren-sys-0.4 (crate (name "ruwren-sys") (vers "0.4.5") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zwvnk47zkvsy1g4faz7hr7mqmvpbmzm0ipk4r329mdksfym4djf") (links "wren")))

(define-public crate-ruwren-sys-0.4 (crate (name "ruwren-sys") (vers "0.4.10") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03hnvqcxhmc8dw83d02yzqj2y20fs4pyv8182xfcdi3a1f0s303n") (links "wren")))

