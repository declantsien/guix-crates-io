(define-module (crates-io ru ti) #:use-module (crates-io))

(define-public crate-rutie-0.0.1 (crate (name "rutie") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "188rfixfp67z3yq2c4wwq9b825196v3yl8d9bf240izxdw02npz9") (features (quote (("unstable") ("test") ("default")))) (links "ruby")))

(define-public crate-rutie-0.0.2 (crate (name "rutie") (vers "0.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "190gdascbkqm48jjranbwkx1acmr026ncql0j9bzpbb89dj5pjjc") (features (quote (("unstable") ("test") ("default")))) (links "ruby")))

(define-public crate-rutie-0.0.3 (crate (name "rutie") (vers "0.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0ng922f24s7jlflr6vwbvzcldnfz1p97m0k3b3s4j5parmaznyfp") (features (quote (("unstable") ("test") ("default")))) (links "ruby")))

(define-public crate-rutie-0.1 (crate (name "rutie") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0vziv4a3ygv2882n3r6nwkgd079d1z9d4m7g11jgzl6zzh1mlsqn") (features (quote (("unstable") ("test") ("default")))) (links "ruby")))

(define-public crate-rutie-0.1 (crate (name "rutie") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "00rdgfk0z4qssfp12sdf8f3x0i3cabn6b35alpz0yy785r13gf2a") (features (quote (("unstable") ("test") ("default")))) (yanked #t) (links "ruby")))

(define-public crate-rutie-0.1 (crate (name "rutie") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "0796cgrcsw73fsb1d47nb74k801vk7i5idplpv0yd4nxk62nx1lg") (links "ruby")))

(define-public crate-rutie-0.1 (crate (name "rutie") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "01anyazz0v7fwfwavpwlwbaiicwfl3acs4k3qpgbypxfl2c0zpls") (links "ruby")))

(define-public crate-rutie-0.1 (crate (name "rutie") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "1gz64zr29mq1j8321fm08qvcj6q645yzxgq6cg7015z9hbxpcjbm") (links "ruby")))

(define-public crate-rutie-0.2 (crate (name "rutie") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "0x6pvpfzvl0x7d7li1ffbxmkgvbfz1kpsw40yks1b1ab24q54qas") (links "ruby")))

(define-public crate-rutie-0.2 (crate (name "rutie") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "1qakyc93hbnzkppnjkrvb5m1nbfhi38h113gaqxkxvd1iprgc420") (links "ruby")))

(define-public crate-rutie-0.2 (crate (name "rutie") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "1s24ghhpsz6sq1crzswy5zlrswam6i39a2cqj8h1q7wz47cbfrri") (links "ruby")))

(define-public crate-rutie-0.3 (crate (name "rutie") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "0sr23y4k36vij3z4k35k7nrpl07q9grcgrzrkww512sa4hl2azyv") (links "ruby")))

(define-public crate-rutie-0.3 (crate (name "rutie") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "0q1iii39f0j3r6dwaqwg743ag7jiwz12bzaqfbncsq3wv5i1npdd")))

(define-public crate-rutie-0.3 (crate (name "rutie") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "1f3q0wmaacw5p3c2824kxg2llwpb00bnz3ril5jrdx7sd05fkr3k") (links "ruby")))

(define-public crate-rutie-0.3 (crate (name "rutie") (vers "0.3.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "0b19c9v5ynm4qnckj13rklvxjh79m0iqswqq38ccwpq5lxbjxk18") (links "ruby")))

(define-public crate-rutie-0.3 (crate (name "rutie") (vers "0.3.4") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "1fva7myf270fk2hzr84x7xi84javrdp4v54saggqrc1pfc1yshmk") (links "ruby")))

(define-public crate-rutie-0.4 (crate (name "rutie") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "0iq4sf8cail8n5fs3vzp30rxx82r9j51ys2i39hr41bmkhy7g9vl") (links "ruby")))

(define-public crate-rutie-0.4 (crate (name "rutie") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "08cdm90dcqj14pbhv1wbas43dxxl30n5zv7gyhqlb6m8hxkk1l56") (links "ruby")))

(define-public crate-rutie-0.4 (crate (name "rutie") (vers "0.4.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "04m15brbwzryqsgd1nb6lbrsa3iyfv217fnr31fbbifcyvljgapj") (links "ruby")))

(define-public crate-rutie-0.4 (crate (name "rutie") (vers "0.4.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "1l7wvw791qrb2lygg19ip1n745zqla9vh2fbgawfbgx3dkvk3kk4") (links "ruby")))

(define-public crate-rutie-0.5 (crate (name "rutie") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "17wiwr7z5646y08f5rb7ibjkxm9813acn9fjn31s2nar6iz5m71q") (links "ruby")))

(define-public crate-rutie-0.5 (crate (name "rutie") (vers "0.5.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "1dg31rv4sk3di2f6g2xcp4lqhix6cdk4zl2v18vbfr5pwiqsvg16") (links "ruby")))

(define-public crate-rutie-0.5 (crate (name "rutie") (vers "0.5.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.10") (default-features #t) (kind 1)))) (hash "0bzpz3qrdq3gzsc2r8qarl04g3a8lbvhdidcmg2mxm7dsy5c8m1g") (links "ruby")))

(define-public crate-rutie-0.5 (crate (name "rutie") (vers "0.5.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "02mwia4xfpvxsjni7j000snakw03ciq33bvwwbfcq6xa36n7z22g") (links "ruby")))

(define-public crate-rutie-0.5 (crate (name "rutie") (vers "0.5.4") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "0bnilc88yq9qa6k09nqhar8sgq4kf81jjzrw6gmr57sfa7n6vqbi") (links "ruby")))

(define-public crate-rutie-0.5 (crate (name "rutie") (vers "0.5.5") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "1a5kjxcwvs46rfksqzm430rapmjqqgv3cfy7lv5gksflllhxgh05") (links "ruby")))

(define-public crate-rutie-0.6 (crate (name "rutie") (vers "0.6.0-rc.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "1if98hxrsdj1rw4mkzlsagnq1cpl16k8dj92m6w5s8f82761fcbf") (links "ruby")))

(define-public crate-rutie-0.5 (crate (name "rutie") (vers "0.5.6") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "1102j1cq3wvsh1wbfcpa1lj70q6j7s1ns1zmyvqd41a7sw6211jk") (links "ruby")))

(define-public crate-rutie-0.6 (crate (name "rutie") (vers "0.6.0-rc.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (default-features #t) (kind 0)))) (hash "1q6svwmp05wbyy80vfs8ziyls1l4bp01w9gxhxy92ingx8mqn7s7") (links "ruby")))

(define-public crate-rutie-0.6 (crate (name "rutie") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)))) (hash "0d7a6b5h6kdrkshhgrh3nbl6g4sfd8xnpgjp7mrdaq1fq8sbapk9") (links "ruby")))

(define-public crate-rutie-0.6 (crate (name "rutie") (vers "0.6.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)))) (hash "0l3w06vdp2yiy18rfxz51ff5sg5jiy5dsms6ksykybnq0hvdzm4f") (links "ruby")))

(define-public crate-rutie-0.7 (crate (name "rutie") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)))) (hash "1cg5p2jib7vbpkwfqcp3qbc01agcgrv3kn7z4k2nd7k70i2ig94r") (links "ruby")))

(define-public crate-rutie-0.8 (crate (name "rutie") (vers "0.8.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.72") (default-features #t) (kind 0)))) (hash "0pzjn9k6dds9f7b95jkja58x7f0qb7j8dcbynzfvf0h45vzky79k") (features (quote (("no-link")))) (links "ruby")))

(define-public crate-rutie-0.8 (crate (name "rutie") (vers "0.8.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)))) (hash "154pj8lvqw8rpfgrgzvxzg3ff9j8ywpwz21iw1dg6hz18268k0kf") (features (quote (("no-link")))) (links "ruby")))

(define-public crate-rutie-0.8 (crate (name "rutie") (vers "0.8.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.86") (default-features #t) (kind 0)))) (hash "1362nz5ac464hc1fp5l8ngxql9yi0dki9h3rg82ika1cfknig0k7") (features (quote (("no-link")))) (links "ruby")))

(define-public crate-rutie-0.8 (crate (name "rutie") (vers "0.8.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.86") (default-features #t) (kind 0)))) (hash "0ii3wl95z4yk81pfc5c9rqsm39ir5a2fm385x01652y7vsid508d") (features (quote (("no-link")))) (links "ruby")))

(define-public crate-rutie-0.8 (crate (name "rutie") (vers "0.8.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "1b5jxgm2j7wf8jmn0yyq5fs0gp5bxfkck363cj1v8fcppd6dp5sx") (features (quote (("no-link")))) (links "ruby")))

(define-public crate-rutie-0.9 (crate (name "rutie") (vers "0.9.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)))) (hash "12k9gfdbq7rpigwwm895a9994iwdgfwgmbc06jg60c0c93vf9s75") (features (quote (("no-link")))) (links "ruby")))

(define-public crate-rutie-serde-0.1 (crate (name "rutie-serde") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rutie") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b56qig01nscklqwrgdy8gazb2i5dgn46gplkivcqq1fgxxnmb03")))

(define-public crate-rutie-serde-0.1 (crate (name "rutie-serde") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rutie") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09sbr7f3jb2mf39fr0f14wlp0dkwzbhd5bhdhq93ws0707m8lgfl")))

(define-public crate-rutie-serde-0.2 (crate (name "rutie-serde") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "rutie") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (default-features #t) (kind 0)))) (hash "02g0siprw0sb0nk2jcxqiw4h3g4gzvyi1k5c4spyxclj00aa7z3m")))

(define-public crate-rutie-serde-0.3 (crate (name "rutie-serde") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "rutie") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (default-features #t) (kind 0)))) (hash "09g462maksj6bycp0bin95v1fzbz0rf5bjbksxjqcwrijj970g36")))

(define-public crate-rutil-0.1 (crate (name "rutil") (vers "0.1.0") (hash "05ws8bhpisahrziamsn55aby423xpc35hhbd5x3b64w6yr91nimz")))

(define-public crate-rutil-0.2 (crate (name "rutil") (vers "0.2.0") (hash "19n8m17k76j9d234wnfkjjkvx3lv417jv5qmg8fz4n4xn8blaizq")))

(define-public crate-rutile-0.1 (crate (name "rutile") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "17qk1y09iakf0sl3yh6rk917is06ral9ljh2gvklrbgypdmnhkm5")))

(define-public crate-rutile-0.1 (crate (name "rutile") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "13w7ai7kvd98rxrf1akqpahf7wnwszln9dk3dvvkhms90nn19q95")))

(define-public crate-rutilities-0.1 (crate (name "rutilities") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1p8jd1ksf7lxzk1xlprk3n734mmv23n1cysmij5xzlhs5nimzjn4") (yanked #t)))

(define-public crate-rutilities-0.2 (crate (name "rutilities") (vers "0.2.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cha7mshk7waavamz5ygvyc2agxbmc7z1k3lli9jljibx48577wa") (yanked #t)))

(define-public crate-rutilities-0.2 (crate (name "rutilities") (vers "0.2.2") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ffwp82n2d7z4ir8lmw2mmn4b5aldrd02c45hf36ijqlih6cibiy") (yanked #t)))

(define-public crate-rutilities-0.2 (crate (name "rutilities") (vers "0.2.3") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "0aqsy7gw3d4dl4nikksv0ix467ncvnhdj3dnbph9sf6j9cfnqn8w") (yanked #t)))

(define-public crate-rutilities-0.2 (crate (name "rutilities") (vers "0.2.4") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ghis7n2d88a2lvfhpa1dzl23fgsvadw03mybh71d278k9wjvixx") (yanked #t)))

(define-public crate-rutilities-0.2 (crate (name "rutilities") (vers "0.2.5") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "0gnhkwrnqrxgajhpv1yf5rhh3lw9jbya0vxcfrrw1wy8ih0irk6n")))

(define-public crate-rutils-0.0.1 (crate (name "rutils") (vers "0.0.1") (hash "0bavvv3d114963a9faiy0cjv4939i6r9c863j7sxjsiv8zm28sla")))

(define-public crate-rutin-tizen-0.1 (crate (name "rutin-tizen") (vers "0.1.0") (deps (list (crate-dep (name "rutin-tizen-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "155pary7cbxyzb2dkkg07h2y5yi076ij5kryc006yghcn8gncy68")))

(define-public crate-rutin-tizen-sys-0.1 (crate (name "rutin-tizen-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)))) (hash "0m6yyq7hr2z6z9db3cnnclp1rr14ylx7fd248s5g9krdvzxhdab7")))

