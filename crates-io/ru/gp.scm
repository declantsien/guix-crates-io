(define-module (crates-io ru gp) #:use-module (crates-io))

(define-public crate-rugpi-0.0.0 (crate (name "rugpi") (vers "0.0.0") (hash "0wdx5i6qn3yd8g2q1wsx37pwffs75cld8x27wxrss6kpjv1lhlcc")))

(define-public crate-rugpi-admin-0.0.0 (crate (name "rugpi-admin") (vers "0.0.0") (hash "16vw2hckfj3nzy2aiz3bnwi3d3yx1f2vi5b4r4r25kw4igfxd94j")))

(define-public crate-rugpi-bakery-0.0.0 (crate (name "rugpi-bakery") (vers "0.0.0") (hash "0qxkj6524g2rag61di5lnp4gmbsqvw4pzq6ncj4fsd3qbvap817y")))

(define-public crate-rugpi-core-0.0.0 (crate (name "rugpi-core") (vers "0.0.0") (hash "0b1428hjffj39d4ma8889s5cih08y5m1ybdm8q913r96hyqpni0h")))

(define-public crate-rugpi-ctrl-0.0.0 (crate (name "rugpi-ctrl") (vers "0.0.0") (hash "0qckympzi3m9315h1rz7648sb2rwa901ll2c8vrnj91l277k684d")))

