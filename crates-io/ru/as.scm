(define-module (crates-io ru as) #:use-module (crates-io))

(define-public crate-ruast-0.0.1 (crate (name "ruast") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "15im3bnazdy8hs5jbwbvypw0n438bkrmhy6slwjy0rmqzhqlddd9") (v 2) (features2 (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.2 (crate (name "ruast") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rxyhk6wyk07ifmhpsnaf3hradhqmldi69av6081x1irv9vd3b9x") (v 2) (features2 (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.3 (crate (name "ruast") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "057c8avw98s49pkiy6byj7ww75q7w5dlr8f6nc8w1by8p8asxg8z") (v 2) (features2 (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.4 (crate (name "ruast") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gh4f4p40dypgasg05yra661czq5zgag01pd0xjhcsg47z3y27cv") (v 2) (features2 (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

(define-public crate-ruast-0.0.5 (crate (name "ruast") (vers "0.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "136536cr48am3w6f236qv1cm2s1sw9w4lxq55wmj3ngzsii0clws") (v 2) (features2 (quote (("tokenize" "dep:proc-macro2" "dep:quote"))))))

