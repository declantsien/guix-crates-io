(define-module (crates-io ru au) #:use-module (crates-io))

(define-public crate-ruautd-framework-4 (crate (name "ruautd-framework") (vers "4.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "na") (req "^0.20") (default-features #t) (kind 0)))) (hash "13mghr9bhc4f0ji9rhcrn2ird7s2ckggj96f3i2l4a9i4i2kjmjp") (yanked #t)))

