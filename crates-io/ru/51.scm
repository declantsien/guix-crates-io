(define-module (crates-io ru #{51}#) #:use-module (crates-io))

(define-public crate-ru5102-0.1 (crate (name "ru5102") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0i9vg5zh5bkhjvvsy42bnc6pjg6271rpp15v8qr1q6nk9kncwdax")))

(define-public crate-ru5102-0.1 (crate (name "ru5102") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1d5ckyjly8vy734di7020z09fkvh02hhnjiajbgi6p496xc9fvk9")))

(define-public crate-ru5102-0.1 (crate (name "ru5102") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0z52hj4gdqmb0kcdr1nwyjkf64lgiq0c3f8a7pbwjwwd40d6c077")))

