(define-module (crates-io ru fi) #:use-module (crates-io))

(define-public crate-rufi-0.1 (crate (name "rufi") (vers "0.1.0") (deps (list (crate-dep (name "rf-core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sal63x90g9kkd8ibknlsinij0fc0zkd3d8rr4lhsq9i7r4l3891") (features (quote (("full" "core" "distributed") ("distributed") ("default" "core" "distributed") ("core"))))))

(define-public crate-rufi-0.2 (crate (name "rufi") (vers "0.2.0") (deps (list (crate-dep (name "rf-core") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "1ph4bph1r7zf80f9ki5j8i94mpwxm2dybczzbn0zh8ganpq5g94x") (features (quote (("programs") ("full" "core" "distributed" "programs") ("distributed") ("default" "core" "distributed") ("core"))))))

(define-public crate-rufi-0.2 (crate (name "rufi") (vers "0.2.1") (deps (list (crate-dep (name "rf-core") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "1znk09ch0rvh41i4p2k9h63gn09jf5pimr4lqj86ibf6cgm2nswa") (features (quote (("programs") ("full" "core" "distributed" "programs") ("distributed") ("default" "core" "distributed") ("core"))))))

(define-public crate-rufi-0.4 (crate (name "rufi") (vers "0.4.0") (deps (list (crate-dep (name "rf-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed-impl") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "16p2yf4bsx8gsyipavq2913fjvx9c51d501x80avqhf23ja3i9wp") (features (quote (("programs") ("impls") ("full" "core" "distributed" "impls" "programs") ("distributed") ("default" "core" "distributed") ("core"))))))

(define-public crate-rufi-0.4 (crate (name "rufi") (vers "0.4.2") (deps (list (crate-dep (name "rf-core") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed-impl") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.7") (default-features #t) (kind 0)))) (hash "12ddddi34dgd63dkpxn9h62nipwh7v1lb1bbhk0i6ccrjd5wdik3") (features (quote (("programs") ("impls") ("full" "core" "distributed" "impls" "programs") ("distributed") ("default" "core" "distributed") ("core"))))))

(define-public crate-rufi-0.4 (crate (name "rufi") (vers "0.4.3") (deps (list (crate-dep (name "rf-core") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed-impl") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.8") (default-features #t) (kind 0)))) (hash "0cqqkpj62lalf2ygdf8wzd2qz997714lx5swcxjy0zrsrz4dibpl") (features (quote (("programs") ("impls") ("full" "core" "distributed" "impls" "programs") ("distributed") ("default") ("core"))))))

(define-public crate-rufi-0.5 (crate (name "rufi") (vers "0.5.0") (deps (list (crate-dep (name "rf-core") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed-impl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.9") (default-features #t) (kind 0)))) (hash "1ghkyiywfaxf8d05i0hsl99bz3x521477jmx45bhgcsq86fa54mm") (features (quote (("programs") ("impls") ("full" "core" "distributed" "impls" "programs") ("distributed") ("default") ("core"))))))

(define-public crate-rufi-0.5 (crate (name "rufi") (vers "0.5.1") (deps (list (crate-dep (name "rf-core") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed-impl") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.10") (default-features #t) (kind 0)))) (hash "18sbimalajkwj0kdnajydj6zdc0fig625m3l10y4lbn0msgzxw4a") (features (quote (("programs") ("impls") ("full" "core" "distributed" "impls" "programs") ("distributed") ("default") ("core"))))))

(define-public crate-rufi-0.5 (crate (name "rufi") (vers "0.5.2") (deps (list (crate-dep (name "rf-core") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed-impl") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.11") (default-features #t) (kind 0)))) (hash "13c34wfsfwn28kjcdbw8c17fsr9zns8ibwd7sp9h58x5by6fmff0") (features (quote (("programs") ("impls") ("full" "core" "distributed" "impls" "programs") ("distributed") ("default") ("core"))))))

(define-public crate-rufi-0.6 (crate (name "rufi") (vers "0.6.0") (deps (list (crate-dep (name "rf-core") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rf-distributed-impl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rufi_gradient") (req "^2.0.12") (default-features #t) (kind 0)))) (hash "0wd10c58wajys5892vipxz7cm44j1yljgl5m7f2r2dfn00q9ap13") (features (quote (("programs") ("impls") ("full" "core" "distributed" "impls" "programs") ("distributed") ("default") ("core"))))))

(define-public crate-rufi_core-1 (crate (name "rufi_core") (vers "1.0.0") (hash "01szyji18jmnm6ww7lwwkd9p97hfwkfk8lnnrvir2474kdwfifs4")))

(define-public crate-rufi_core-1 (crate (name "rufi_core") (vers "1.1.0") (hash "16hix9g2kra3cng09bc1csb99lhhfy9p3dv8x1vsrl0rmkcfhflh")))

(define-public crate-rufi_core-1 (crate (name "rufi_core") (vers "1.2.0") (hash "00p8v2dvh17x345wzlkq9ffcjddysvn0k996d1562f4bpsp3bv6m")))

(define-public crate-rufi_core-2 (crate (name "rufi_core") (vers "2.0.0") (hash "1nh70vk0zcp9h0vbjc3hy5j0vzng37h7c6r63bkm8fq9xn90kbrg")))

(define-public crate-rufi_core-3 (crate (name "rufi_core") (vers "3.0.0") (hash "1kqgw0ix25pr1m75cbcgj53f3vm5p8l50pmw27hx0gmhb418zmfy")))

(define-public crate-rufi_core-4 (crate (name "rufi_core") (vers "4.0.0") (hash "1z5p1igr31f9qqpg5pq17h1rxbi68cwy6a42mvpg5kzi6bj7yy8b")))

(define-public crate-rufi_core-5 (crate (name "rufi_core") (vers "5.0.0") (hash "1j7k1dy7zc8nda92fgsgl8sxv77i9v11mjjrmxyqxbphlxjjj0z5")))

(define-public crate-rufi_core-5 (crate (name "rufi_core") (vers "5.1.0") (hash "1f67i9rwrn9fjam276m5nlxzmq4jgpg2xp5rmlhkp30fg2x3337k")))

(define-public crate-rufi_core-6 (crate (name "rufi_core") (vers "6.0.0") (hash "0a2bwhsggz21bhynl56alyarsg2nsqjpa18j8781jvmj53bf7nn6")))

(define-public crate-rufi_core-6 (crate (name "rufi_core") (vers "6.0.1") (hash "0jsc7812z2cvsa0jcxprnwlv35i2llwpryhzw8rnmfcbbbbv1k49")))

(define-public crate-rufi_core-6 (crate (name "rufi_core") (vers "6.1.0") (hash "0dxsd93jfsc98f68m8vy290sb5fnmn09jh6pzxq03a3l97rwl50h")))

(define-public crate-rufi_core-7 (crate (name "rufi_core") (vers "7.0.0") (hash "1waflc93frd907ahsny94qcmhj6rh7qprmnjll2x6n7nvlfhl2jc")))

(define-public crate-rufi_core-8 (crate (name "rufi_core") (vers "8.0.0") (hash "05zky04hk5zavvh7srkvfpg4bzsxr14h75bkl1rzlrf5kiissq4b")))

(define-public crate-rufi_core-9 (crate (name "rufi_core") (vers "9.0.0") (hash "0hmh8fa5by346biznwv8l2h79rp8h0xd2ssqzspkdbk481y45lgl")))

(define-public crate-rufi_core-9 (crate (name "rufi_core") (vers "9.0.1") (hash "1kvwxxmyi6a16icpnqgxgl3py3ncsva6q8j6h1f26ig62wg9pk05")))

(define-public crate-rufi_core-9 (crate (name "rufi_core") (vers "9.1.0") (hash "1c2wyv5xd17p949hvidrb247589ym9y6iyn2m16g7mjs9f8ki3r1")))

(define-public crate-rufi_core-10 (crate (name "rufi_core") (vers "10.0.0") (hash "1fmc3qw71jgp3s08x1rmni4pxrrjy7l2ak1xn23nxarhr28jihsb")))

(define-public crate-rufi_core-10 (crate (name "rufi_core") (vers "10.1.0") (hash "0h603k1lxzjx344k2kkgdmc214mi5690wqx88rqhkpqrv7njbzsi")))

(define-public crate-rufi_core-11 (crate (name "rufi_core") (vers "11.0.0") (hash "0dlgcpyyjs99dqngq5dy65v85s9iw4wkks8z0mscc3m7hpxlnjgj")))

(define-public crate-rufi_core-11 (crate (name "rufi_core") (vers "11.1.0") (hash "0hxdgdjr8vl65y0c21c9pz7niz129z4zaw2ayrhyrxccq1cjc8y3")))

(define-public crate-rufi_core-12 (crate (name "rufi_core") (vers "12.0.0") (hash "0av0lb63522pj0jb540qnqmz54gdad4z2wsakz5d26xvnnv1ksk6")))

(define-public crate-rufi_core-12 (crate (name "rufi_core") (vers "12.0.1") (hash "0c7l10ffyqxl74q2cpc8j830cwvn14yin9xx111261v2ivdjq9j1")))

(define-public crate-rufi_core-12 (crate (name "rufi_core") (vers "12.0.2") (hash "11fp8xv7hk2097xd124vaf8nakyy5aa1ybjm7zz9282j04516hid")))

(define-public crate-rufi_core-13 (crate (name "rufi_core") (vers "13.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m9y1dryll7p1bpw4h12f0jrf2rjijg3x8s95fsl3adigfifd3am")))

(define-public crate-rufi_core-13 (crate (name "rufi_core") (vers "13.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1n4ynx57i7zy2dnarl1szw6g90m88p0acg9qhxafvaddzh1fmh39")))

(define-public crate-rufi_core-13 (crate (name "rufi_core") (vers "13.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1941a9mv3smpms4n33a3nj9bbryfs61pp8xhwmisi1z6qk71l0qn")))

(define-public crate-rufi_core-14 (crate (name "rufi_core") (vers "14.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0kjkjg84nbc133xlcnd7n0xbzgvqk0x4mlf1jila10xhiycj2wrx")))

(define-public crate-rufi_core-15 (crate (name "rufi_core") (vers "15.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "06q99s983y6l2sl563k0b3a9lqvcbhr4g58xg40cm0amnl8s6ih6")))

(define-public crate-rufi_core-15 (crate (name "rufi_core") (vers "15.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0354qvh6yq2zbzh85gqzif5fg1rsq7hlnk5h71vgf7kxilh6kx11")))

(define-public crate-rufi_core-15 (crate (name "rufi_core") (vers "15.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "01kq8m7s2lg81fmdfcxw84myrnb0b8nwmakqx67xa10sqbxwd69i")))

(define-public crate-rufi_gradient-1 (crate (name "rufi_gradient") (vers "1.0.0") (deps (list (crate-dep (name "rufi_core") (req "^15.1.0") (default-features #t) (kind 0)))) (hash "0kdzmax7js99c61a15826vf7r13n63g7wr3wwql79ddyimpr8r9g")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.0") (deps (list (crate-dep (name "rf-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a1963kj2nzi39minpfhk58l3f5827zkiaxrsrw8a7wchjsz0v8f")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.1") (deps (list (crate-dep (name "rf-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1i7i1i2b2szna5q4vv6z4k90xavr3mwkvchkklvbirzj0k0k1ac6")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.2") (deps (list (crate-dep (name "rf-core") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0irdsxlbhccx76an1al0ag8jrmnpxl4ji0h1fk1id6gzc4qlf4xk")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.3") (deps (list (crate-dep (name "rf-core") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0fp69xp1j3ll8yasgfc39ih89g19hymczw7mznv0ajqmxci2kknl")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.4") (deps (list (crate-dep (name "rf-core") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0mwl9j3mxfwn4h28yk4bzp2860kd1bmpyzsh6caxfzgzb2ccdxq4")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.5") (deps (list (crate-dep (name "rf-core") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0wn0hmg95k4fj31kvsv334rn63pka2gkqqx727bg470xfal4v3a9")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.7") (deps (list (crate-dep (name "rf-core") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "03281gm6fs85drikrk1y5qa7dz146ndd579wqvr96hrgmckpl86r")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.8") (deps (list (crate-dep (name "rf-core") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0y4nad92z6vjjnmi2h0wffalw0gpcgjcy1hnygnj92i1yz09750v")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.9") (deps (list (crate-dep (name "rf-core") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1z5i0kpjh4bvplg4hqfkjgfh1v9mvg9li0z9nyqsyf5qr6hr4bcb")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.10") (deps (list (crate-dep (name "rf-core") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "141dwx0bl79g6kpb2v9gi4pi8qgdc6ik4pxzcadp0sj3nyi14sm6")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.11") (deps (list (crate-dep (name "rf-core") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0hfajk2zlgrramf290b74j80dy05gp8a76lirq7rhfl1qj1q4d5l")))

(define-public crate-rufi_gradient-2 (crate (name "rufi_gradient") (vers "2.0.12") (deps (list (crate-dep (name "rf-core") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1xwarpmp34jp2agi4fppnjbf2a2r4ck1sg1ssq5d9yn1a1259jv9")))

