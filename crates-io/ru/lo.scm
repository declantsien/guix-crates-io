(define-module (crates-io ru lo) #:use-module (crates-io))

(define-public crate-rulox-0.1 (crate (name "rulox") (vers "0.1.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dp7zdqpn8anqnxgnp9ljgx6idpwhj47fri3ybslj99xafinjmh0")))

(define-public crate-rulox-0.2 (crate (name "rulox") (vers "0.2.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qpqljkccp7h6y3q94az8y3hcg59azr3pfb8yz1qrhgc9xfckp36")))

(define-public crate-rulox-0.2 (crate (name "rulox") (vers "0.2.1") (deps (list (crate-dep (name "rulox_macro") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "02fbs9nahpp2x3mzjqv63jvcswxa5ykwhyhc14p5bzvkv4f1nwpg")))

(define-public crate-rulox-0.3 (crate (name "rulox") (vers "0.3.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v6kpymznlwf2nhqb428if8v569c5ydrg4piy8v5xjxjyv87k2qf")))

(define-public crate-rulox-0.4 (crate (name "rulox") (vers "0.4.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gbrxcmc29f49r0lmwb93pqzw0gx89jal9d0l7hinzifc16f67qv")))

(define-public crate-rulox-0.4 (crate (name "rulox") (vers "0.4.1") (deps (list (crate-dep (name "rulox_macro") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "0sgzahak7v6bw32jdddvmw2aiflqh3vszrr5r550y9w33wdlkq85")))

(define-public crate-rulox-0.4 (crate (name "rulox") (vers "0.4.2") (deps (list (crate-dep (name "rulox_macro") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kzq12ms4chsdw56vbajw7rgkkpa6fbpc9i25vcldgbz6jpvvkc2")))

(define-public crate-rulox-0.4 (crate (name "rulox") (vers "0.4.3") (deps (list (crate-dep (name "rulox_macro") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "1snxq6wlf8vj037iiqzw4cwby3cdmw85did0z9hxq7a80m183kxd")))

(define-public crate-rulox-0.5 (crate (name "rulox") (vers "0.5.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cnwylq6qwchg9470829d98l5frjxwvirbd6kq1l3m0wk572cgn0")))

(define-public crate-rulox-0.5 (crate (name "rulox") (vers "0.5.1") (deps (list (crate-dep (name "rulox_macro") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vd4qzldz0w45zcgl90zfp55h3d5667fcldqlsx1azapy55hn6fg")))

(define-public crate-rulox-0.6 (crate (name "rulox") (vers "0.6.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jj56fgnzbm5c3a57flpspwbw89y26qwfiy0w9p9rmmp4yxnbnqr")))

(define-public crate-rulox-0.6 (crate (name "rulox") (vers "0.6.1") (deps (list (crate-dep (name "rulox_macro") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.3") (default-features #t) (kind 0)))) (hash "17d7wm30qldgcvdfv15n0gq2cl6w3hzz637nsbwmgqaypb5bznvd")))

(define-public crate-rulox-0.7 (crate (name "rulox") (vers "0.7.1") (deps (list (crate-dep (name "rulox_macro") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ixlrblrx3g7q8r2p8bvp77ks8gs9y3x2d55mv6f5gb7klnk5xl0")))

(define-public crate-rulox-0.7 (crate (name "rulox") (vers "0.7.2") (deps (list (crate-dep (name "rulox_macro") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.4") (default-features #t) (kind 0)))) (hash "09gvii9vij67iv7gakbfbs2lz92qcqw2dwb9jw622fxw9s2r8n92")))

(define-public crate-rulox-0.8 (crate (name "rulox") (vers "0.8.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.5") (default-features #t) (kind 0)))) (hash "1psy207iw5h11ar4zwf2y2xn3pi78zvpr7y64rns96p6310m07p2") (features (quote (("sync" "rulox_types/sync") ("default" "sync"))))))

(define-public crate-rulox-0.8 (crate (name "rulox") (vers "0.8.1") (deps (list (crate-dep (name "rulox_macro") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.5") (default-features #t) (kind 0)))) (hash "1qk3k1qwgn7mpq4iyfpa4q33vd9ayz2bcfmnv8h0d42hvfy65r1i") (features (quote (("sync" "rulox_types/sync") ("default" "sync"))))))

(define-public crate-rulox-0.9 (crate (name "rulox") (vers "0.9.0") (deps (list (crate-dep (name "rulox_macro") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.6") (default-features #t) (kind 0)))) (hash "18565rhwj9pxr0y1x1asl30sskcyy94bl45nzr7xzgqpbncp2jis") (features (quote (("sync" "rulox_types/sync") ("serialise" "rulox_types/serialise") ("default" "sync" "async") ("async" "sync" "rulox_types/async")))) (rust-version "1.70")))

(define-public crate-rulox_macro-0.1 (crate (name "rulox_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0isvnrfh17llh6s54d7m10pmgxknkm04wjqcrdkdzf20bszqx4k0")))

(define-public crate-rulox_macro-0.2 (crate (name "rulox_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0iwnlk7hlpjypiiidv6fpd6i6brzymkv37350qnpph2hg1id31ag")))

(define-public crate-rulox_macro-0.3 (crate (name "rulox_macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yzi5p5bn559s54171gndazd3a7xybbq4aixwl4sdsdlgc75mmp2")))

(define-public crate-rulox_macro-0.4 (crate (name "rulox_macro") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cd4yx5gwm4v8k51skfmh79xz5h7cbg79ch55s4kg6bpm12a35j1")))

(define-public crate-rulox_macro-0.5 (crate (name "rulox_macro") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ky3159pa87zdk76vkzrdhf5g5p6rxd074c54zasrgny88sls60w")))

(define-public crate-rulox_macro-0.6 (crate (name "rulox_macro") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "03dgjs91gwij10f0vzwhwly39898j7vakvjm26g3ah95in38fnhw")))

(define-public crate-rulox_macro-0.7 (crate (name "rulox_macro") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mf1w3f1p2w74a9q1x6mlflnp5vr294dv0zswg4gfy0bqvq2w6nb")))

(define-public crate-rulox_macro-0.7 (crate (name "rulox_macro") (vers "0.7.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zmhz2y0n6rdxs4ji3wni8158fwa4h8y71s0b2j8sr1dmky3ii31")))

(define-public crate-rulox_macro-0.8 (crate (name "rulox_macro") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0q9iyc8hplbmz860d0y9w81cvpr31jrnwgkxdips1q11nr1xlmsx")))

(define-public crate-rulox_macro-0.9 (crate (name "rulox_macro") (vers "0.9.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rulox_types") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0z1qx5g49l0bkjpqq22n4c3hf3vzjzsrrpaba7bmdijqca6hm2pl")))

(define-public crate-rulox_types-0.1 (crate (name "rulox_types") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x9a6a8y49cmxv88vfbccpmyww78r73qia11hvhsf4bwj9fyjad2")))

(define-public crate-rulox_types-0.1 (crate (name "rulox_types") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hhnmasdpypiavhcagvarsyllh3j645x5g5p46dg4vhngwjmzrky")))

(define-public crate-rulox_types-0.1 (crate (name "rulox_types") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "00i96qblvirkm1876l5ihcx6gj05z0xrn2d56xh44jai1hlggskq")))

(define-public crate-rulox_types-0.2 (crate (name "rulox_types") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z7immjbf7dgqkd3rpmijkfcs514cd3x24kfzzcrh45zp4khrl3w")))

(define-public crate-rulox_types-0.3 (crate (name "rulox_types") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dxqbxhbbg6r8r3jyiyhwp98ydqa2myq8xz1d9fys75cfvpim52i")))

(define-public crate-rulox_types-0.4 (crate (name "rulox_types") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "19qlhasd5hhbyf58j4n4rhbhl3phbhvvyhxbni6j96vacl1m3370")))

(define-public crate-rulox_types-0.4 (crate (name "rulox_types") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0l14pzcy6fklhbq27i3r71y3s7b3jadwsig6v9l16k3px8ycv47y")))

(define-public crate-rulox_types-0.5 (crate (name "rulox_types") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fpnig288lx7cjrcv0d7kz2nj9j85xbbv46sf62lqvxfyf9vxwpf") (features (quote (("sync"))))))

(define-public crate-rulox_types-0.6 (crate (name "rulox_types") (vers "0.6.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "castaway") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive" "rc"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "16mpmkv5002fdgj1a6n6k1c2ll13n0whny0wvljv3mab0y5sm4kz") (features (quote (("sync") ("async" "sync")))) (v 2) (features2 (quote (("serialise" "dep:serde" "dep:serde_json" "bytes/serde")))) (rust-version "1.70")))

