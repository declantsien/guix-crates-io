(define-module (crates-io ru xn) #:use-module (crates-io))

(define-public crate-ruxn-0.1 (crate (name "ruxn") (vers "0.1.0") (hash "00i14q1r17hzgidcmmg4qq7wi720fdqsc1a88r7nzfnmfw3iy464")))

(define-public crate-ruxnasm-0.1 (crate (name "ruxnasm") (vers "0.1.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)))) (hash "09qmp589b4r9s6bjm8i29cvqz72jqidv41snxh5k4hbixgyzjjqz") (features (quote (("default" "codespan-reporting"))))))

(define-public crate-ruxnasm-0.1 (crate (name "ruxnasm") (vers "0.1.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)))) (hash "167lfajipl886hss1ix3h8r3qlkg0c2sq8shl0nakdhmwhxpwcax") (features (quote (("default" "codespan-reporting"))))))

(define-public crate-ruxnasm-0.1 (crate (name "ruxnasm") (vers "0.1.2") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "11ssh90fpl463r2rfgdikakl7gra3q73ppxbp71hysk9npif5ba5") (features (quote (("default" "codespan-reporting"))))))

(define-public crate-ruxnasm-0.2 (crate (name "ruxnasm") (vers "0.2.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1a7d890nc4vn5pi7319zhdmddm16ix5fxy1l4pdpv8gyj699dxfd") (features (quote (("default" "bin") ("bin" "codespan-reporting"))))))

