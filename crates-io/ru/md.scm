(define-module (crates-io ru md) #:use-module (crates-io))

(define-public crate-rumdb-0.1 (crate (name "rumdb") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x7k7siqx5bf3lnflx02b6sngl30jpxk60r85qr9rsaydk6j43dp")))

(define-public crate-rumdb-0.2 (crate (name "rumdb") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "10rn1j6jyf0l8plkp4cpil0ksx6liisgm7rqk1kb3svacwkb0r6i")))

(define-public crate-rumdump-0.1 (crate (name "rumdump") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fc85d8cqgswlzcr1rp3pm6784w6725pbiwq7c7jn00xm0wfp6ys")))

(define-public crate-rumdump-0.2 (crate (name "rumdump") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vcrgjaafq9358v1fqfi879zxjhamaws2xhsxki4g4s9pqbih3ac")))

(define-public crate-rumdump-1 (crate (name "rumdump") (vers "1.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vdddnjbj7s406h0nb8rkcprgbp410di3yjqnmn6yfs0q1hjg54g")))

