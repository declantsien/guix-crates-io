(define-module (crates-io ru nd) #:use-module (crates-io))

(define-public crate-rundeck-0.1 (crate (name "rundeck") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.25") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0f17jzji644mqvwbxi33p59rm3rcs5zfq94q9kb6i3s753gfy1yz")))

(define-public crate-rundeck-api-0.1 (crate (name "rundeck-api") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0h0ydgvg7vihnlh6vbmcmjars8x8yfzvqx5p3fgyv7pfnn9arqk4")))

(define-public crate-rundlet-0.0.1 (crate (name "rundlet") (vers "0.0.1") (hash "08g60vj10sg817k908ynhsj10lf480v5yzard7bnf08mf6qw69ac")))

(define-public crate-rundo-0.1 (crate (name "rundo") (vers "0.1.1") (deps (list (crate-dep (name "rundo_attrs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.1") (default-features #t) (kind 0)))) (hash "0x4qwq7h1a7r7nsjm0qnpkgnwd1i9mwkbg91z3s2djxnr3szxqm3")))

(define-public crate-rundo-0.2 (crate (name "rundo") (vers "0.2.0") (deps (list (crate-dep (name "rundo_attrs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.2") (default-features #t) (kind 0)))) (hash "14hyizz3984glnlq01y7w39j673pgsgxlvafg927iyvw1iskjp9h")))

(define-public crate-rundo-0.3 (crate (name "rundo") (vers "0.3.2") (deps (list (crate-dep (name "bson") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rundo_attrs") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kqnny1xs6l6d9cgyvjzjg8w99f0g0ancagg7zvii1aaigmdmpk0")))

(define-public crate-rundo-0.4 (crate (name "rundo") (vers "0.4.0") (deps (list (crate-dep (name "bson") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rundo_attrs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.4") (default-features #t) (kind 0)))) (hash "07wmlsdp1wkhpmh1cx1ryp894zgnrxd6gxj2jag4gdik5xldg9p6")))

(define-public crate-rundo_attrs-0.1 (crate (name "rundo_attrs") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0sssnkjsvxq2wra38sqkkx9lzs0z48aj04i1nxcmz12byw1n5x4s")))

(define-public crate-rundo_attrs-0.1 (crate (name "rundo_attrs") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1ibhwpr78hmwr5irwgifdrpd668p0am1qydrijvchay1sqm4w0dr")))

(define-public crate-rundo_attrs-0.2 (crate (name "rundo_attrs") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "03j3smq7jnxdnax53c3pli93r4rja4ximbjqak9ilmylfmqgzr7f")))

(define-public crate-rundo_attrs-0.2 (crate (name "rundo_attrs") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0g83bwmlx0fmxva6p8g7y6s0h549n5vza8m1p20myjxv4p0fy3as")))

(define-public crate-rundo_attrs-0.2 (crate (name "rundo_attrs") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "11fvygxnpgm9k23d5c2pd9ldf8chb5nvxkb3b20ywxkg0ld5gprx")))

(define-public crate-rundo_attrs-0.3 (crate (name "rundo_attrs") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0aphaikjl16m7743kwxvq9g1r60609kydjr5iwnczijycbx987bk")))

(define-public crate-rundo_attrs-0.3 (crate (name "rundo_attrs") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1frcv76sqd6xa59lgb4c7sx3cwbjvbd2xrgnxikcx4q60nhsfjh6")))

(define-public crate-rundo_attrs-0.4 (crate (name "rundo_attrs") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0pnb94s62zd9gnr7z9v1asbvkibwvba8bc3kh37vkyik39hf3wbk")))

(define-public crate-rundo_attrsca-0.1 (crate (name "rundo_attrsca") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rundo_types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.10") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1fh8yqhpq7r4sz9d38i9s195nn132vrma2adwgskr5flwjj4adm0")))

(define-public crate-rundo_types-0.1 (crate (name "rundo_types") (vers "0.1.0") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "1n7f26sxrghyjxrfkvv3vnqjb1nqqxs1qmm68vk5wvvczxrh96wz")))

(define-public crate-rundo_types-0.1 (crate (name "rundo_types") (vers "0.1.1") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "0glw3c7qc7rsc26k201qlqix8chjmyp24q8hq4w0fz85ifsxzdqp")))

(define-public crate-rundo_types-0.2 (crate (name "rundo_types") (vers "0.2.0") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "0s5dbgvk8r15km4zhczqaij9v02ak5dwwacz92g5kmdrl1x0l12z")))

(define-public crate-rundo_types-0.2 (crate (name "rundo_types") (vers "0.2.1") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "1hd2jbcslb0jxz6gsrzi75g0b4c18cg9a2a9c2fkkr98w1hd5499")))

(define-public crate-rundo_types-0.2 (crate (name "rundo_types") (vers "0.2.2") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "05a0273fi76spyk3mcfrx4w8qijhr4dfd6ln1ca795fg2pn0iq4v")))

(define-public crate-rundo_types-0.3 (crate (name "rundo_types") (vers "0.3.1") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "0p10f9kggycab3xyggdi83p5k3wqyih9xd03dgas0ycg40mrld0n")))

(define-public crate-rundo_types-0.3 (crate (name "rundo_types") (vers "0.3.2") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "0fikaw14q3ggjashvixl539yr14sd2ry07myy56fygcyajh5y5n1")))

(define-public crate-rundo_types-0.4 (crate (name "rundo_types") (vers "0.4.0") (deps (list (crate-dep (name "difference") (req "^2.0") (default-features #t) (kind 0)))) (hash "0sh7hbi2jr8ffcrjmyqhcfafdx8l5wkc4nspj0rcknf6yyrn3hjk")))

