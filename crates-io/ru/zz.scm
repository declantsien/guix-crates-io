(define-module (crates-io ru zz) #:use-module (crates-io))

(define-public crate-ruzzzt-0.0.1 (crate (name "ruzzzt") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11b4gghvaw3ckilamkmbgglnsyivj0rpwbx793xws8h3m8j1izzw")))

(define-public crate-ruzzzt-0.0.2 (crate (name "ruzzzt") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12q8hcm02sd4gc1xnxbxcmlk0q5j5q90aafy30252jdfa6hwkvi0")))

