(define-module (crates-io ru rd) #:use-module (crates-io))

(define-public crate-rurdkit-0.1 (crate (name "rurdkit") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1q9w0m2nlsiyzr08c6029xzxf8ar9vyvvn7jagkwz76j8bx7nkag") (yanked #t)))

(define-public crate-rurdkit-0.1 (crate (name "rurdkit") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "08rpmfzh8vmypns7qkp7kfp1nq94rwfj6fbg377vki5n2grj43mh")))

