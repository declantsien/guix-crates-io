(define-module (crates-io ru ly) #:use-module (crates-io))

(define-public crate-ruly-0.1 (crate (name "ruly") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "124n36q2msvlph34d778pw99g93mnarf4i8ymp4r142k0qrslymb")))

(define-public crate-ruly-0.1 (crate (name "ruly") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1q71aakfn3ji3k2a9r35gnbli7yy4vbwa1pfi8dffa7fa1z2vvqy")))

(define-public crate-ruly-0.1 (crate (name "ruly") (vers "0.1.11") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0rcxc3gryqlv1b1lj11mmg2yv8wdnpysamyd98inh9wd8wpwx4ws")))

(define-public crate-ruly-1 (crate (name "ruly") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "146c73vj0381qpb1v4nsz75dqs7r9lrpk0952vm5575mghnj6d56")))

(define-public crate-ruly-1 (crate (name "ruly") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0wvpk3p4iac8f5zfb8nz3v2k8dhzxqy9gqp1bq4clw8941nnfzm0")))

(define-public crate-ruly-1 (crate (name "ruly") (vers "1.1.1") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0clh1kmjg7bjy17h9avdfwa61sj78irxmf9gff7lp8gik52swrlk")))

(define-public crate-ruly-1 (crate (name "ruly") (vers "1.2.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "00b7m1vilv6xh0w1v1hgj1dwcaq44ly72da7vif3g4cqdcswxkix")))

(define-public crate-ruly-1 (crate (name "ruly") (vers "1.2.1") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "05jp14pl81a7vwzjbhliwpicbi9kwc8lkg55fybwh5dgr55drkzl")))

(define-public crate-ruly-2 (crate (name "ruly") (vers "2.0.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0yjgjjm5jdvwji4y8bjck8yc0281x7hs8j6fnsd48fsg6mvr2lq5")))

(define-public crate-ruly-2 (crate (name "ruly") (vers "2.0.1") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0875mi7lhl0djvl9clblkbyw2yv2s8ri8ccb0mq6gazhrmd9q4mm")))

(define-public crate-ruly-2 (crate (name "ruly") (vers "2.0.2") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "049bjl0gm8fbgcif1lh62shpd2rfxm3g7z2fzg869h21g9qa1n0w")))

(define-public crate-ruly-2 (crate (name "ruly") (vers "2.1.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1m3nxm1nd3r71mk4r0in38f8l9vhrnpvq2h2nckzxncx52i52fwz")))

(define-public crate-ruly-2 (crate (name "ruly") (vers "2.2.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0cmii2micjh992ilgzxrfvdysag1x6fk9v5vqbavzn1zd3gy1d28")))

(define-public crate-ruly-2 (crate (name "ruly") (vers "2.2.1") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0x9lyzrqdiyv3shdlj55xqld1lc46ym2zdh63idgddz6nfcvqmn6")))

(define-public crate-ruly-2 (crate (name "ruly") (vers "2.3.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0550kkp9fs3ql10idlp9yc42d84ws70j3hxghrfvd8vhjipkn5kg")))

(define-public crate-ruly-3 (crate (name "ruly") (vers "3.0.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0q6jxnbn3c9sg3ig7193g2sa9qsi7hc8ayx46lbxh8mac7waqjf0")))

(define-public crate-ruly-4 (crate (name "ruly") (vers "4.0.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0yzdn1xsqknh2g6i203nvd48d5cj2k8w8gb53lrb1gdyj4nsl4z9") (yanked #t)))

(define-public crate-ruly-4 (crate (name "ruly") (vers "4.1.0") (deps (list (crate-dep (name "pmacro_ruly") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0g0xiiv6hb2d0wh859ahamykwhbhgbknsg7xljn43g86a8h0gwbr")))

(define-public crate-ruly-4 (crate (name "ruly") (vers "4.1.1") (deps (list (crate-dep (name "pmacro_ruly") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0hb745bjgvfmdj39bs30bh3qp7ilh6g5g2b9ns36szwxb4daqppi")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.0") (deps (list (crate-dep (name "lr_parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0la8ccv8j2d1rpypwky6ymczh1a6nll645h2jh7mnf6ym45klf3n")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.1") (deps (list (crate-dep (name "lr_parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0fa51nkmazkl0s0r3sqqi2xrmjrsx5f6fs6ljnhi6s7l4ncrjd5v")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.2") (deps (list (crate-dep (name "lr_parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1k9zvd2g0bkl7x5cqzny09g7zslsk8q4nb83dy096y24flz8wb1q")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.3") (deps (list (crate-dep (name "lr_parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0igcv2r71n85naipvbrgr0x8m5pp2ny11y5nyvf18fjkk0s7j1q1")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.4") (deps (list (crate-dep (name "lr_parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0l0vv7p7wljfcs6477hqz15h1qlzp82pwqi7a6qz3q4kawm76nqr")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.5") (deps (list (crate-dep (name "lr_parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "177kfi50n3qsw8y5gcw38w7f8125781da2l61lvy4h6nmndw0a3q")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.6") (deps (list (crate-dep (name "lr_parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0nfswrj73k8vzj6cykqv23ghfgql26asqxjl362xg6wc9i4ir2p9")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.7") (deps (list (crate-dep (name "lr_parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "05z5mxxxa865f7wca81x4yyxyyjzw1i2nfyhqlx1pzl721zid293")))

(define-public crate-ruly2-0.1 (crate (name "ruly2") (vers "0.1.8") (deps (list (crate-dep (name "lr_parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1n1qws2mnzahyw32vzmczm10kx3b88h5xjl3i0ihwqi49nr2vijn")))

