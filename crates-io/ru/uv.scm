(define-module (crates-io ru uv) #:use-module (crates-io))

(define-public crate-ruuvi-sensor-protocol-0.1 (crate (name "ruuvi-sensor-protocol") (vers "0.1.0") (hash "1hsz0z7r23i8xkiaz47f1laavjwyj3milci6zqxk4craa0v2wpdn")))

(define-public crate-ruuvi-sensor-protocol-0.1 (crate (name "ruuvi-sensor-protocol") (vers "0.1.1") (hash "07lll76vf2nv7cx2jdnmp479bdwvirs7bsyjvvimhzsli1ylysg2")))

(define-public crate-ruuvi-sensor-protocol-0.2 (crate (name "ruuvi-sensor-protocol") (vers "0.2.0") (hash "1vg0q5z7pyd8mwaqk1xzkb5hif3dn2ym70q4c62109h07fvk12mk")))

(define-public crate-ruuvi-sensor-protocol-0.3 (crate (name "ruuvi-sensor-protocol") (vers "0.3.0") (hash "00phh9vcpj4aj4a5n1k800bnwn5477d1i3h8l1pfs843n9y37ffi") (features (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.4 (crate (name "ruuvi-sensor-protocol") (vers "0.4.0") (hash "17lz3z3vg71431hn2py6vg3gxjd28hq13qqi173sb3nzv1fzpa8w") (features (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.4 (crate (name "ruuvi-sensor-protocol") (vers "0.4.1") (hash "17cqwks85l61rcz9nvk1qm3bh6b4x830lkk17755smjmqgdla5cv") (features (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.5 (crate (name "ruuvi-sensor-protocol") (vers "0.5.0") (hash "18cr3sgpkxvybvw1ibri7nv95lqn0r63mjww0klkcgm03kq4rgdn") (features (quote (("std") ("default" "std"))))))

(define-public crate-ruuvi-sensor-protocol-0.6 (crate (name "ruuvi-sensor-protocol") (vers "0.6.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (features (quote ("alloc"))) (optional #t) (kind 0)))) (hash "01d4zhlnxjfgfxyy7fjm7qibbpjp46i1m221v1i4lva6jibb2qp1") (features (quote (("gateway" "hex" "serde" "serde_json") ("default" "std")))) (v 2) (features2 (quote (("std" "serde_json?/std")))) (rust-version "1.60")))

(define-public crate-ruuvi-sensor-protocol-0.6 (crate (name "ruuvi-sensor-protocol") (vers "0.6.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (features (quote ("alloc"))) (optional #t) (kind 0)))) (hash "0yhapjvj5v13pi2m5kp5hkd0bc76crhcq5i4h34izynvxy9cdmgd") (features (quote (("gateway" "hex" "serde" "serde_json") ("default" "std")))) (v 2) (features2 (quote (("std" "serde_json?/std")))) (rust-version "1.60")))

(define-public crate-ruuvi_reader-0.1 (crate (name "ruuvi_reader") (vers "0.1.0") (deps (list (crate-dep (name "btleplug") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1ysj8ngf9mbilfj49y56vxmmnkj2z4wfp5lym7230j1vqr6b20f3")))

(define-public crate-ruuvitag-listener-0.1 (crate (name "ruuvitag-listener") (vers "0.1.0") (deps (list (crate-dep (name "rumble") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0nxw2z7c2vacn9da9ja5aya42qcyj2hpwr2ibyb3h6jccn9m89xk")))

(define-public crate-ruuvitag-listener-0.1 (crate (name "ruuvitag-listener") (vers "0.1.1") (deps (list (crate-dep (name "rumble") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0vlgffah8fam5zii5ih1mg155c0pa6xkdxzxj02cazny9835vf74")))

(define-public crate-ruuvitag-listener-0.2 (crate (name "ruuvitag-listener") (vers "0.2.0") (deps (list (crate-dep (name "rumble") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.2.*") (default-features #t) (kind 0)))) (hash "02i7c6dnmq52hsf8ykbgsz6a2j8pfhlgvpg0qk7b3dk2d59sfjis")))

(define-public crate-ruuvitag-listener-0.3 (crate (name "ruuvitag-listener") (vers "0.3.0") (deps (list (crate-dep (name "rumble") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (kind 0)))) (hash "0vmlvx6ipfq5352z0j8865xf4c74hzfrk81rha0x73qjrs7nb9wq")))

(define-public crate-ruuvitag-listener-0.3 (crate (name "ruuvitag-listener") (vers "0.3.1") (deps (list (crate-dep (name "rumble") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (kind 0)))) (hash "0a061vjzyd8hhwvi2k5hkp52hcgcx99jw6hj94d1n6nhhc4q72vf")))

(define-public crate-ruuvitag-listener-0.4 (crate (name "ruuvitag-listener") (vers "0.4.1") (deps (list (crate-dep (name "btleplug") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "0.3.*") (kind 0)))) (hash "1mvcw246akn06cbfp7zlvws43yhd4rkswghkn6z31hhar3ng0sqr")))

(define-public crate-ruuvitag-listener-0.5 (crate (name "ruuvitag-listener") (vers "0.5.1") (deps (list (crate-dep (name "btleplug") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "0.3.*") (kind 0)))) (hash "1b8iirg2nqhd2gn9paf3xig6ikih7igvdc4p5karaa8pxdd5fhqr")))

(define-public crate-ruuvitag-listener-0.5 (crate (name "ruuvitag-listener") (vers "0.5.2") (deps (list (crate-dep (name "btleplug") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "0.3.*") (kind 0)))) (hash "02wqgfksbszwsbifcmx38y9gi7jn0rfb6rpkiqa1yz4ahhvw1cyd")))

(define-public crate-ruuvitag-listener-0.5 (crate (name "ruuvitag-listener") (vers "0.5.3") (deps (list (crate-dep (name "btleplug") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "0.3.*") (kind 0)))) (hash "1j1g8n1y3zimvxhy6l2s10v07rx7lm2zpda9mc2lnhs7ak9iw77a")))

(define-public crate-ruuvitag-listener-0.5 (crate (name "ruuvitag-listener") (vers "0.5.4") (deps (list (crate-dep (name "btleplug") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "0.3.*") (kind 0)))) (hash "1xva2wrxhb83slalyh5blf46w8x2qv119xfg4dfvxhsx0mwmymkz")))

(define-public crate-ruuvitag-listener-0.5 (crate (name "ruuvitag-listener") (vers "0.5.5") (deps (list (crate-dep (name "btleplug") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "0.5.*") (default-features #t) (kind 0)))) (hash "0ldgmmdklg56fji3krrlq35fij47pkv2p8h21if0vzbqa806i8n8")))

(define-public crate-ruuvitag-upload-0.2 (crate (name "ruuvitag-upload") (vers "0.2.0") (deps (list (crate-dep (name "docopt") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.14") (default-features #t) (kind 0)) (crate-dep (name "rumble") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0civjzm8zfpfp6k9vav2hvs8i5sx2bcl7kp52q6w2i1m12zbndnc")))

(define-public crate-ruuvitag-upload-0.3 (crate (name "ruuvitag-upload") (vers "0.3.0") (deps (list (crate-dep (name "assert_fs") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "directories") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.14") (default-features #t) (kind 0)) (crate-dep (name "rumble") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ruuvi-sensor-protocol") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "094glza09zi61a3l5dg7w3x0qk1ggn6q7xy80q9s24krv7508h37")))

