(define-module (crates-io ru si) #:use-module (crates-io))

(define-public crate-rusic-0.0.2 (crate (name "rusic") (vers "0.0.2") (deps (list (crate-dep (name "async_command") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusting") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "17in6wzdpb2jwl9l8w4653304pdyisx8wp7avhf7408jskjzyr5s")))

(define-public crate-rusic-0.0.3 (crate (name "rusic") (vers "0.0.3") (deps (list (crate-dep (name "async_command") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusting") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1vvl2w842yzj9ngvdrq12rq4nnvsnhkqkqjm4hbc8qsqdqvydvsj")))

(define-public crate-rusic-0.0.4 (crate (name "rusic") (vers "0.0.4") (deps (list (crate-dep (name "async_command") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusting") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0a5v86vgqggl52g8zwdvzrknc3x4cbqfvfj6safwrk67y99qmw83")))

(define-public crate-rusic-0.0.5 (crate (name "rusic") (vers "0.0.5") (deps (list (crate-dep (name "async_command") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusting") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1wpd8ir5kl9w8913nslkn041a1zkr9bdk9vnsl33g6pkpg7gxsvr")))

