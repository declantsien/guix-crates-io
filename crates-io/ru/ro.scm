(define-module (crates-io ru ro) #:use-module (crates-io))

(define-public crate-ruroonga-0.1 (crate (name "ruroonga") (vers "0.1.0") (hash "1x3cwj70h2saspjy2b7as955x2ah4r7axw2hha4gbw0787k6xa16")))

(define-public crate-ruroonga-0.2 (crate (name "ruroonga") (vers "0.2.0") (hash "06raaqv48lzbxisb0l8314b9dlibc1cr9fn0rrnnnsvdmc7sm0gp")))

(define-public crate-ruroonga-0.2 (crate (name "ruroonga") (vers "0.2.1") (hash "0ljr61lsmyjd8i1jmm0yvvl67zaj29favcqacc1ff2nf4a87mz08")))

(define-public crate-ruroonga-0.3 (crate (name "ruroonga") (vers "0.3.0") (hash "1bmprzp73078rcjpyhkxmdl8fxc6ykbjns9h63wh6na6bnmjzvc2")))

(define-public crate-ruroonga-0.4 (crate (name "ruroonga") (vers "0.4.0") (deps (list (crate-dep (name "groonga-sys") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "0aw13bqyn8dpyd87dl0jhz4jw8dhb6hwbcjcci0ivv27gwdrmzqa")))

(define-public crate-ruroonga-0.4 (crate (name "ruroonga") (vers "0.4.1") (deps (list (crate-dep (name "groonga-sys") (req "~0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3") (default-features #t) (kind 0)))) (hash "0y61kigl2ayp8kwy6m3krpxkhzk8ccrv59kk126zaprsdidna3nc")))

(define-public crate-ruroonga-0.5 (crate (name "ruroonga") (vers "0.5.0") (deps (list (crate-dep (name "groonga-sys") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3") (default-features #t) (kind 0)))) (hash "1is5k7i8safxaf2dflliplhxjmrk3ff1w4srr04swsy14zf6yvc2")))

(define-public crate-ruroonga-0.6 (crate (name "ruroonga") (vers "0.6.0") (deps (list (crate-dep (name "groonga-sys") (req "~0.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3") (default-features #t) (kind 0)))) (hash "0fc4i7zwdshghkrg002023q0dr3qz8c55dkq1dirxk08d1hgidpv")))

(define-public crate-ruroonga_client-0.1 (crate (name "ruroonga_client") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "11zz8m0mqcz6aayhh9p35mdkcj1qfvl92gs6979q56li21hq38w7")))

(define-public crate-ruroonga_client-0.1 (crate (name "ruroonga_client") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0999cs9ilcw5z12rq2w1a621mrcxkxz2ydknwa8yvbfgwzqkd8hp")))

(define-public crate-ruroonga_client-0.2 (crate (name "ruroonga_client") (vers "0.2.0") (deps (list (crate-dep (name "hyper") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0j05ivf9q2grqh3q1sbrwyrki94psw636ndpjwb42i8xhd8w3dsd")))

(define-public crate-ruroonga_client-0.3 (crate (name "ruroonga_client") (vers "0.3.0") (deps (list (crate-dep (name "hyper") (req "~0.7.2") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~0.5.2") (default-features #t) (kind 0)))) (hash "1z5w9xs4qamixs36ajr4a5jqasq63czdxxa16jprgyia7yfbvcmy")))

(define-public crate-ruroonga_client-0.4 (crate (name "ruroonga_client") (vers "0.4.0") (deps (list (crate-dep (name "hyper") (req "~0.7.2") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~0.5.2") (default-features #t) (kind 0)))) (hash "0jl7mfmx2sm6xhyjqd0m3861fh5gq93jqlg8cmrgdvbff0wgsimn")))

(define-public crate-ruroonga_client-0.4 (crate (name "ruroonga_client") (vers "0.4.1") (deps (list (crate-dep (name "hyper") (req "~0.8.0") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~0.5.2") (default-features #t) (kind 0)))) (hash "1y6mr06pr6wkjkj4b7gdh0zddcrl8j0mns1iawrw37jawsa9x980")))

(define-public crate-ruroonga_client-0.4 (crate (name "ruroonga_client") (vers "0.4.2") (deps (list (crate-dep (name "hyper") (req "~0.8.0") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~1.0.0") (default-features #t) (kind 0)))) (hash "01qajy1bhfnyr3kzfd3g55qr6d4a4agx3lil24whs3dzz112xncb")))

(define-public crate-ruroonga_client-0.4 (crate (name "ruroonga_client") (vers "0.4.3") (deps (list (crate-dep (name "hyper") (req "~0.9.0") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~1.0.0") (default-features #t) (kind 0)))) (hash "05d5qj70698kva7r588gfld7jnm8cq2zcl1k0w0sqr82cvrhcmsp")))

(define-public crate-ruroonga_client-0.5 (crate (name "ruroonga_client") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~0.9.0") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~1.2.0") (default-features #t) (kind 0)))) (hash "0pckpaa7ivf6r74b53ydsd4dgj6a3jv7gcqp6i374s1akm1zhm24") (features (quote (("gqtp" "byteorder"))))))

(define-public crate-ruroonga_client-0.5 (crate (name "ruroonga_client") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~0.10.0") (default-features #t) (kind 0)) (crate-dep (name "json_flex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~1.2.0") (default-features #t) (kind 0)))) (hash "1n4n2zjjw8dqr32d4ihx7p43hdq5xgzhw1p01hq219pshiv3mypp") (features (quote (("gqtp" "byteorder"))))))

(define-public crate-ruroonga_command-0.1 (crate (name "ruroonga_command") (vers "0.1.0") (deps (list (crate-dep (name "url") (req "~0.5.2") (default-features #t) (kind 0)))) (hash "10lk2grl6y1f8zl7sa1jmrah5qn6an4za0qrwl1a4h2hh6aa1sxd") (features (quote (("unstable") ("normalizer_mysql"))))))

(define-public crate-ruroonga_command-0.2 (crate (name "ruroonga_command") (vers "0.2.0") (deps (list (crate-dep (name "skeptic") (req "~0.4") (kind 1)) (crate-dep (name "skeptic") (req "~0.4") (kind 2)) (crate-dep (name "url") (req "~0.5.2") (default-features #t) (kind 0)))) (hash "1x80hih4kqa445jzfdin64jyk4amcnp9rma6sncadxz4n56rqrkl") (features (quote (("unstable") ("normalizer_mysql"))))))

(define-public crate-ruroonga_command-0.2 (crate (name "ruroonga_command") (vers "0.2.1") (deps (list (crate-dep (name "skeptic") (req "~0.4") (kind 1)) (crate-dep (name "skeptic") (req "~0.4") (kind 2)) (crate-dep (name "url") (req "~1.0.0") (default-features #t) (kind 0)))) (hash "0wm23y21sk6cmlgvim2rp0x9y0d5jh20ywi79f5ma9l7qxgbhvqp") (features (quote (("unstable") ("normalizer_mysql"))))))

(define-public crate-ruroonga_command-0.2 (crate (name "ruroonga_command") (vers "0.2.2") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "~0.4") (kind 1)) (crate-dep (name "skeptic") (req "~0.4") (kind 2)) (crate-dep (name "url") (req "~1.1.0") (default-features #t) (kind 0)))) (hash "0vadlfjz3anc5ysyck834rlhjbdly9pxqv0qyblqqm60vjjmwx56") (features (quote (("unstable") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.2 (crate (name "ruroonga_command") (vers "0.2.3") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "~0.4") (kind 1)) (crate-dep (name "skeptic") (req "~0.4") (kind 2)) (crate-dep (name "url") (req "~1.1.0") (default-features #t) (kind 0)))) (hash "1z2rikwq0nc6j7kf00sz5nr6jqxa0cjlxh11yss67yq5csld92j6") (features (quote (("unstable") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3 (crate (name "ruroonga_command") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "~0.4") (kind 1)) (crate-dep (name "skeptic") (req "~0.4") (kind 2)) (crate-dep (name "url") (req "~1.1.0") (default-features #t) (kind 0)))) (hash "143i1kzgc09kwmi9b838hj7ib4xi8g59d2w5nmmc4xpkiv3x3dbs") (features (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3 (crate (name "ruroonga_command") (vers "0.3.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "~0.4") (kind 1)) (crate-dep (name "skeptic") (req "~0.4") (kind 2)) (crate-dep (name "url") (req "~1.2.0") (default-features #t) (kind 0)))) (hash "1gx7v9i7xsn7hrlsbdd5h3q3k2vns7s2hxvryawghwp3416mc45p") (features (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3 (crate (name "ruroonga_command") (vers "0.3.2") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "~0.6") (kind 1)) (crate-dep (name "skeptic") (req "~0.6") (kind 2)) (crate-dep (name "url") (req "~1.2.0") (default-features #t) (kind 0)))) (hash "16pyxnx5x39b48gx8vikfsms27nxlrgdkh9knncn97rs4a7f062p") (features (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3 (crate (name "ruroonga_command") (vers "0.3.3") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "~0.6") (kind 1)) (crate-dep (name "skeptic") (req "~0.6") (kind 2)) (crate-dep (name "url") (req "~1.2.0") (default-features #t) (kind 0)))) (hash "18sz71p06a4bf30wjksr2xhkb6dyjk6jsfwhndpfn28dpwps3k6r") (features (quote (("unstable") ("sharding") ("normalizer_mysql") ("dev" "clippy"))))))

(define-public crate-ruroonga_command-0.3 (crate (name "ruroonga_command") (vers "0.3.4") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "~0.6") (kind 1)) (crate-dep (name "skeptic") (req "~0.6") (kind 2)) (crate-dep (name "url") (req "~1.2.0") (default-features #t) (kind 0)))) (hash "1if0gk5pf0b469qyxy6hf3mh9qbr6bwk29zfrs032axx9s19cbcg") (features (quote (("unstable") ("sharding") ("normalizer_mysql") ("groonga_611") ("dev" "clippy"))))))

(define-public crate-ruroonga_expr-0.1 (crate (name "ruroonga_expr") (vers "0.1.0") (deps (list (crate-dep (name "regex-syntax") (req "~0.3") (default-features #t) (kind 0)))) (hash "1hy8pw4mckfgjxrqnv9mkaayfxzkmp0f4hjmlnjzdr7nz3j3cmdw")))

(define-public crate-ruroonga_expr-0.2 (crate (name "ruroonga_expr") (vers "0.2.0") (deps (list (crate-dep (name "regex-syntax") (req "~0.3") (default-features #t) (kind 0)))) (hash "0zn6m6h42ysdqnma0zbpfybbq57rpayhy49xfz3fzzhckprli88g")))

