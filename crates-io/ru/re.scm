(define-module (crates-io ru re) #:use-module (crates-io))

(define-public crate-rure-0.1 (crate (name "rure") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)))) (hash "07bllrdp71aplylgpdmk0pbm2rrwpjmqawnkl83y3aajj1qzn912")))

(define-public crate-rure-0.1 (crate (name "rure") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)))) (hash "0gcjl2dv6d642z09c5ldn24v1z5n9321cg5ax1iwkp8mvvlvg1bd")))

(define-public crate-rure-0.2 (crate (name "rure") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1x9dflxx7mnx97vscn0yb5rpbrfhy84y4rjnfv9xaj2nfvby97x7")))

(define-public crate-rure-0.2 (crate (name "rure") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0m768pkh8dy305h287k1qag6xk16wd5wbklq44q10krxc413n2bb")))

(define-public crate-rure-0.2 (crate (name "rure") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0k529aly7knhfkxrvmiil0s47l2hzfniz2ipv88fxfkmbrchkppk")))

(define-public crate-rurel-0.1 (crate (name "rurel") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ik0854141ykhdvqarlpyfkdwb7saymxwp9khn653aagl0vwvagh")))

(define-public crate-rurel-0.1 (crate (name "rurel") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "16vly8xgsmdc91jpnwsy56plaxwl2zbfpk0hny6qdpifqkh895xg")))

(define-public crate-rurel-0.1 (crate (name "rurel") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0smmwzhcdpd8w9mqc4liha3nh5hxsbagz14xgbd2bk3kbnfz5gwv")))

(define-public crate-rurel-0.2 (crate (name "rurel") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1h0shg5sbzaw3mz0mhc3gn9m2w5nqlknkxsvq611zkmf5m3h4gp1")))

(define-public crate-rurel-0.2 (crate (name "rurel") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0zsmpvl2xmm8s697pdi4v60mfaigsg1kch4kywgydklzm9p4q7z6")))

(define-public crate-rurel-0.3 (crate (name "rurel") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1x6ckx0yvmdn9sm8nc8jdcm1mrvn29yv2aijk0inicnnm1i5x6c7")))

(define-public crate-rurel-0.3 (crate (name "rurel") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "01b7cl5b5ilb1snzfp0w894yrmq27llii57mla86cnhah1lyjkj4")))

(define-public crate-rurel-0.4 (crate (name "rurel") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "120y4b7rh5f97w0nkw1fxas38935ggnpcnpcgyrnwr3mxfqd94fr")))

(define-public crate-rurel-0.5 (crate (name "rurel") (vers "0.5.0") (deps (list (crate-dep (name "dfdx") (req "^0.11.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0s7ky8r4yb5wwzx7ylinfqpwxq0n87qj63ffpx7b63vmnryzk1my") (features (quote (("dqn" "dfdx") ("default"))))))

(define-public crate-rurel-0.5 (crate (name "rurel") (vers "0.5.1") (deps (list (crate-dep (name "dfdx") (req "^0.11.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "176nxhpgvy7c80pb9mxv80vwrfmryq178b905zd8z4sfb7g68lhd") (features (quote (("dqn" "dfdx") ("default"))))))

(define-public crate-rurep-0.1 (crate (name "rurep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1nl9y9ym7lv0vp2xqjs4mxrrp6aj89b7vmq2m89sqql6xkfzz770")))

