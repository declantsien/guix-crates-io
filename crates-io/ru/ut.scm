(define-module (crates-io ru ut) #:use-module (crates-io))

(define-public crate-ruut-0.1 (crate (name "ruut") (vers "0.1.0") (deps (list (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1flcgq9nicd459afvz42irb8qxi126k5brxrin8zp5p1b6dwmk7k")))

(define-public crate-ruut-0.2 (crate (name "ruut") (vers "0.2.0") (deps (list (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1rvzmkgh7hpnmhd7c2114zy2wrvbxz0n85427ainqiglgb623xlp")))

(define-public crate-ruut-0.3 (crate (name "ruut") (vers "0.3.0") (deps (list (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "05bm4ksv27kai3jdf9say412a4kf1klikxazjsl5073iwkny0v1z")))

(define-public crate-ruut-0.4 (crate (name "ruut") (vers "0.4.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0zx2sg7aj0xf44405jj4vbg16rs89zcdqxg29nvsf3r5zbirmb6g")))

(define-public crate-ruut-0.5 (crate (name "ruut") (vers "0.5.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0xx257pagf8a7c20ann15fv54qwdyfjc68pfkrlxdj9x09jfa7gm")))

(define-public crate-ruut-0.5 (crate (name "ruut") (vers "0.5.1") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0fsvy29ycr3wdpvyyadxkh7pz9fhqyy17zkpfhffa7p7x3fc016y")))

(define-public crate-ruut-0.6 (crate (name "ruut") (vers "0.6.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "json5") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "09x6sq04gx8w6x4pgagy7is7jzw8ck9i8kndbqvz1cj2c11hady9")))

(define-public crate-ruut-0.6 (crate (name "ruut") (vers "0.6.1") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "json5") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "04n3icskjk5p2sr6i38pc6gsvyh2qpisgl3lywx9xxbrsf38fiy4")))

(define-public crate-ruut-0.7 (crate (name "ruut") (vers "0.7.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "json5") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0ipapq354m92fbarnsh6f7fsas48w155b28qfkf56rj34xhp0v8v")))

(define-public crate-ruut-0.8 (crate (name "ruut") (vers "0.8.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "json5") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "render_as_tree") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0yf7d1z8w9rccy853g63mgj60hsjd3w20fi8lvml5h6in0lm92s5")))

(define-public crate-ruut-algebra-0.1 (crate (name "ruut-algebra") (vers "0.1.0") (hash "0r5h02lngz3w2hhwrqqkk5qcx2l4ava5nvj2g3qfallaa179c8ms")))

(define-public crate-ruut-functions-0.0.1 (crate (name "ruut-functions") (vers "0.0.1") (deps (list (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 0)))) (hash "0jx75ij32xyh344rbcap7vgm5b6sgr78rrz92qw6dm5x20vxgip1")))

(define-public crate-ruut-functions-0.0.2 (crate (name "ruut-functions") (vers "0.0.2") (deps (list (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 0)))) (hash "013brcwblnyll187nxg8z01zkm6q7ikvbi5i964yxvlln40yzmw1")))

(define-public crate-ruut-functions-0.0.3 (crate (name "ruut-functions") (vers "0.0.3") (deps (list (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 0)))) (hash "0gr406mfmncx83dj7gqjdljhdarycwbrksagahr84dw10i73mxyl")))

(define-public crate-ruut-functions-0.0.4 (crate (name "ruut-functions") (vers "0.0.4") (hash "04xdyk9s8jg6k9h7rmi88pd9jm00gxlcp96jahqigvzissvh9yr2")))

(define-public crate-ruut-functions-0.0.5 (crate (name "ruut-functions") (vers "0.0.5") (hash "1xx8c7wr26av6gx80nzafzgaq3ikx6kbnaysrv1712br0gr8irag")))

(define-public crate-ruut-functions-0.0.6 (crate (name "ruut-functions") (vers "0.0.6") (hash "1zwwh4cla8j24q3ncwjvsw6kh2x23c21dsi639axzb3cp0831vnd")))

(define-public crate-ruut-functions-0.0.61 (crate (name "ruut-functions") (vers "0.0.61") (hash "0wa4dkbss4p51qcshb3lvcrc45g8pyg3n9vyr5lbbqffrvrli32g")))

(define-public crate-ruut-functions-0.1 (crate (name "ruut-functions") (vers "0.1.0") (hash "1wfawqqr6dnsviy7dk9za8fnyajzg9is75brn7dfp51b297mpr1p")))

(define-public crate-ruut-functions-0.1 (crate (name "ruut-functions") (vers "0.1.1") (hash "100pmgf266bldg80fh2y92lm33cgrh01l9lak4ifjd0sd8pkap1k")))

(define-public crate-ruut-functions-0.1 (crate (name "ruut-functions") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "ruut-algebra") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0d0hz7r7ia0ifk6in9g8bd6h9jwpp2acbpyp8p9r54j3wm5zighh")))

