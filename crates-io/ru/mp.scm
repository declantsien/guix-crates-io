(define-module (crates-io ru mp) #:use-module (crates-io))

(define-public crate-rump-0.0.1 (crate (name "rump") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0kfz7qcsswi2cdwjfshg3j2vm182sav1jb4y0q7f8j9n8nvvlckw")))

(define-public crate-rump-0.1 (crate (name "rump") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req ">= 0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req ">= 0.2.12") (default-features #t) (kind 0)))) (hash "1npgy0yj8fc00y8x8vfy9jc0gma0dbsj1b5pfdg8dj0gc4nhrmay")))

(define-public crate-rumpkernel-0.0.2 (crate (name "rumpkernel") (vers "0.0.2") (deps (list (crate-dep (name "num_cpus") (req "^1.9") (default-features #t) (kind 1)))) (hash "0bxdv6wlm59zmynycji0iby169k2y4zan9dzwfvhigadf6jvldcp") (links "rkapps")))

(define-public crate-rumpkernel-0.0.3 (crate (name "rumpkernel") (vers "0.0.3") (deps (list (crate-dep (name "num_cpus") (req "^1.9") (default-features #t) (kind 1)))) (hash "098rf0p51lshv57phbhss9b81a2yikngl3jk2x8n4zwbqyph772v") (links "rkapps")))

(define-public crate-rumpkernel-0.0.4 (crate (name "rumpkernel") (vers "0.0.4") (deps (list (crate-dep (name "num_cpus") (req "^1.9") (default-features #t) (kind 1)))) (hash "0a1d5xxk5sxkygbskvzvbb9agx05qqjnjsbvg87nhsnyb1ya2bnv") (links "rkapps")))

(define-public crate-rumpkernel-0.0.5 (crate (name "rumpkernel") (vers "0.0.5") (deps (list (crate-dep (name "num_cpus") (req "^1.9") (default-features #t) (kind 1)))) (hash "087335c63nf849kf7clffh8akimm0w6xbmajvj64djip8rvzr25l") (links "rkapps")))

(define-public crate-rumpsteak-0.1 (crate (name "rumpsteak") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rumpsteak-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("macros" "rt" "time"))) (default-features #t) (kind 2)))) (hash "0wll7ar0yx3ikaq3w0pdgy2a5wyjdynpy7vpwfk3akfdjaz6g8f2")))

(define-public crate-rumpsteak-macros-0.1 (crate (name "rumpsteak-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dzahikc8b8vmczn78bslwpv7il2p6l3byqgp16c96llbczpbwys")))

(define-public crate-rumpunch-0.0.0 (crate (name "rumpunch") (vers "0.0.0") (hash "193w1xbvb7w9b6cwviazgw6kb08mx3fmzzrhdnvg87jjvx089wy7")))

(define-public crate-rumpunch-0.0.1 (crate (name "rumpunch") (vers "0.0.1") (hash "0a2dkqnn3v6sd6794jqxkd7g4c4869y5m6li1y9xk1q7bz9n0f97")))

(define-public crate-rumpunch-0.0.2 (crate (name "rumpunch") (vers "0.0.2") (hash "1qkxm1mhj4fhkm8m2q4j7pb6axxm7lkxd3jwdjl008p641dvdx0r")))

