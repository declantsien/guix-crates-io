(define-module (crates-io ru dg) #:use-module (crates-io))

(define-public crate-rudg-0.1 (crate (name "rudg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dot_graph") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ra_ap_syntax") (req "^0.0.104") (default-features #t) (kind 0)))) (hash "0h28hakman80rkk3fl127xc476d0g96ii2w0bgxdqrk50pq50wf6")))

(define-public crate-rudg-0.1 (crate (name "rudg") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dot_graph") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ra_ap_syntax") (req "^0.0.104") (default-features #t) (kind 0)))) (hash "18wma4yg2qpvbyz5p2bj89vw55js1zhlx5ddlkqfwzh5sr5p24fy")))

