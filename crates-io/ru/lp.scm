(define-module (crates-io ru lp) #:use-module (crates-io))

(define-public crate-rulp-0.1 (crate (name "rulp") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "assert_approx_eq") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0y9ix0177867ig8zmf57j1ir8d03li441gs4124hymw21mdxdhbg")))

