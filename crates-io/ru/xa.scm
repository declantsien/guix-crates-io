(define-module (crates-io ru xa) #:use-module (crates-io))

(define-public crate-ruxafor-0.1 (crate (name "ruxafor") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0dwhiidzvp3ikfwj3snyfii4qkghxjpjq37ylx5gs09vxd5xfwqd")))

(define-public crate-ruxafor-1 (crate (name "ruxafor") (vers "1.0.0") (deps (list (crate-dep (name "hidapi") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0wzrp0ci80rbzw4s61liiaj45f035b05i6xqs9sm1ifq4a5i43lb")))

