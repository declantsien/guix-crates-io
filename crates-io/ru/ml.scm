(define-module (crates-io ru ml) #:use-module (crates-io))

(define-public crate-ruml-0.1 (crate (name "ruml") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("extra-traits" "full" "parsing" "printing"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "0hqa8ki4wyn4h97rvqprh7j7fa1dfl99lq9izw1mq2dyhhsyxbx9")))

(define-public crate-ruml-ox-0.1 (crate (name "ruml-ox") (vers "0.1.0") (hash "0nr3rybhiv124a233n77r5q16a3larhidnqgs8fy0v5klrhfkmxw")))

(define-public crate-ruml-ox-0.1 (crate (name "ruml-ox") (vers "0.1.1") (hash "0vi8g510hdbd8khzn9ff5yjg326f45xzifb1a5ys7rq0wd9f12dw")))

