(define-module (crates-io ru ve) #:use-module (crates-io))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.0") (hash "07xw0hwpxz9rdjiwcb1zdxmby15r6cj9m8pn1qc1qlhiiq30mhfl")))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.1") (hash "1kyn9zqj29p3j02da95bj992j0vppk39524yrb9l9zcjigw99zsv")))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.2") (hash "1bk6nn4pjkq0mkq4kawc3ikrgzjmjwjlmysckmqhfnsf8l3f7nih")))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.3") (hash "1jpmpdij1idmpmlfx1n6fx5sjh1j32pnq13vjz1z1ickn52sxh94")))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.4") (hash "1zq7ps5pcgflpkmrkmvs192la0i8ss9r3qyizfkb72v84hr1cfc6")))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.5") (hash "1gc0m6fk4cw2bknv2plxlmv9gzrqzbmgfhhmhsk4hay3pr2xfz5j")))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.6") (hash "1k55y83q0lqv7k63lni9wyzlw44rxg27rm07qhfbljig9b58rf0w")))

(define-public crate-ruver-0.1 (crate (name "ruver") (vers "0.1.7") (hash "0957dk4dr34254fc04cj5fca60fjrx1ncb5q8hnrwfbldq059rsb")))

