(define-module (crates-io ru go) #:use-module (crates-io))

(define-public crate-rugo-0.1 (crate (name "rugo") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)))) (hash "02s1lj64f4vp8ly2knncv9dnxnzbrx626r127nb0csbd6iixhcds")))

(define-public crate-rugo-0.1 (crate (name "rugo") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)))) (hash "072pgxyvqypijwyajy1yg51x46fsyimrvyz0840nhj1rm1bvihkl")))

