(define-module (crates-io ru mi) #:use-module (crates-io))

(define-public crate-rumi-0.2 (crate (name "rumi") (vers "0.2.0") (deps (list (crate-dep (name "basebits") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.26") (default-features #t) (kind 0)))) (hash "1iwx10b4axnhp9l8zfzaa9s3bysifk8pxpjwpcbw7vmi46w551m2")))

(define-public crate-rumi-0.2 (crate (name "rumi") (vers "0.2.1") (deps (list (crate-dep (name "basebits") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.26") (default-features #t) (kind 0)))) (hash "17kijnvgl988bcqvk6cx6akjq4lb2fp1f3bvbaf4i972rqi9gfh7")))

(define-public crate-ruminant-0.1 (crate (name "ruminant") (vers "0.1.0") (hash "12csip2krl60frnp4sgypbrcz4blmn2sn8flw24w4ln46xm7rqi9")))

(define-public crate-ruminant-0.2 (crate (name "ruminant") (vers "0.2.0") (hash "06m5rljh440mk0sgff2mpgcv2afx94midivygif594pkda841a7k")))

(define-public crate-ruminant-0.3 (crate (name "ruminant") (vers "0.3.0") (hash "0vkdzb5mv8bcc3ncwqv2jlk80mzm02mnzq1c9y7qlgxrdgjz2a3s")))

(define-public crate-ruminant-0.4 (crate (name "ruminant") (vers "0.4.0") (hash "1gj52hjpxyxg5wllmgsi0c5rrnw7inr2bvqhka1i9gg14gl8vbg2")))

(define-public crate-ruminant-0.5 (crate (name "ruminant") (vers "0.5.0") (hash "19nbiw8hgdcbblwza5icqnfqyzrrx7dsp2jf6la7khgd1dc43pwc")))

(define-public crate-ruminant-0.6 (crate (name "ruminant") (vers "0.6.0") (hash "1m17v70r4kgl573p70bnsfjn93lxll6ilirm1f0snp8wx6v468cw")))

(define-public crate-ruminant-0.7 (crate (name "ruminant") (vers "0.7.0") (hash "0hsbakj7kq80jkfylsix5pz9y5ciyvikm3y9f6sz7zyv3z3pykxb")))

(define-public crate-ruminant-0.8 (crate (name "ruminant") (vers "0.8.0") (hash "1086dvffafv2cxzl448sf9p5r4nzvqi0b4fjpq2ybf7xhfds3br3")))

(define-public crate-ruminant-0.9 (crate (name "ruminant") (vers "0.9.0") (hash "1hsgf8jwjqwkmcwgb2scp1pn0q2pdgdls72bkjfsk8yki76gywf0")))

(define-public crate-ruminant-0.10 (crate (name "ruminant") (vers "0.10.0") (hash "03hcqr3pihhri3hhaim46iaacc1r6v853qcg0rlw02bgq43n2ipy")))

(define-public crate-ruminant-0.11 (crate (name "ruminant") (vers "0.11.0") (hash "1wwkwsl0rl7ahqq2h43p1liscjdilsrzc7b7njh7jd8sfvw5fqw9")))

(define-public crate-ruminant-0.12 (crate (name "ruminant") (vers "0.12.0") (hash "1s4qziv6vbp5l9qnls8xjaj7z4igyi9dkhm88va6aspvvfs4cnd5")))

(define-public crate-ruminant-0.13 (crate (name "ruminant") (vers "0.13.0") (hash "0fr70jfpsmi8mi1alhnnjbi8kpd0vvl35nwsmf8r65ys1wbiv0fk")))

(define-public crate-ruminant-0.14 (crate (name "ruminant") (vers "0.14.0") (hash "1sqynaj6v3m5ppammgj6zdaqmlhiclc112lf9vhxy8gasrfd1inf")))

(define-public crate-ruminant-0.15 (crate (name "ruminant") (vers "0.15.0") (hash "06m19lpp5vn8zin570y2bh2yb5dpvaj9j58a73xwlggqcqmdjicw")))

(define-public crate-ruminant-0.16 (crate (name "ruminant") (vers "0.16.0") (hash "1k04y8jlw40iyzc5rhh8ymcilpwahbbjagaa1yjh60b9575bnxl5")))

(define-public crate-ruminant-0.17 (crate (name "ruminant") (vers "0.17.0") (hash "0zjphyp82jj19b9dl6p4jgpk7z250s49yz3677c4wa6ga4vhm02w")))

(define-public crate-rumio-0.1 (crate (name "rumio") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "0vq83hp375gyfjx1gw9qxkl9iasf15k2492wbr15ky6vjs87zl11") (features (quote (("example_generated")))) (yanked #t)))

(define-public crate-rumio-0.1 (crate (name "rumio") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "0l81mczv4wcr45m2vnhagz0cf5dnqg1iinqfbpm2l9q9mm133awk") (features (quote (("example_generated"))))))

(define-public crate-rumio-0.1 (crate (name "rumio") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "defile") (req "^0.1") (default-features #t) (kind 0)))) (hash "128d1aaxjd7xvdgqvrvc6ky0xxbxgznmgiip98kqg8kcqbib50il") (features (quote (("example_generated"))))))

(define-public crate-rumio-0.1 (crate (name "rumio") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "defile") (req "^0.1") (default-features #t) (kind 0)))) (hash "15d2p8vkqxcxz1ind3i909714l9ki1zgyg7qnbd1zcklcn3xg6rl") (features (quote (("example_generated")))) (yanked #t)))

(define-public crate-rumio-0.1 (crate (name "rumio") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "defile") (req "^0.1") (default-features #t) (kind 0)))) (hash "09jkw7rxqaj760s6n301pgz9rsv6iqs5hrlvzg2ni5scqvazq2dw") (features (quote (("example_generated"))))))

(define-public crate-rumio-0.2 (crate (name "rumio") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "defile") (req "^0.1") (default-features #t) (kind 0)))) (hash "0al118vffk92p7cl9y1y5ksds7pi1pjgbr8fa4pafifjdi7l7pj9") (features (quote (("example_generated"))))))

