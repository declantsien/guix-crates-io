(define-module (crates-io ru bu) #:use-module (crates-io))

(define-public crate-rubullet-0.1 (crate (name "rubullet") (vers "0.1.0-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "rubullet-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "1fxn2q1vigycwsx09pfx978knzw7wbayabi4ymk1xd5xabcjigmw")))

(define-public crate-rubullet-0.1 (crate (name "rubullet") (vers "0.1.0-alpha-2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "rubullet-sys") (req "^0.1.0-alpha-2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "1k36wrlw9r15dl6qzbzh3ym2ram8k2xambb27p0p7p27lm4g0kpm")))

(define-public crate-rubullet-0.1 (crate (name "rubullet") (vers "0.1.0-alpha-3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "rubullet-sys") (req "^0.1.0-alpha-2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mh04515x6hjkxb6a6w89wrm1ny6vvwzx5q93lrkv6h4niz77q7i")))

(define-public crate-rubullet-sys-0.1 (crate (name "rubullet-sys") (vers "0.1.0-alpha") (deps (list (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0gbm8nlf4g96m30186ya2j4fq93r3llzrzlxcj0vk20s4a368m9l")))

(define-public crate-rubullet-sys-0.1 (crate (name "rubullet-sys") (vers "0.1.0-alpha-2") (deps (list (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0a1finzbdmjqfmvm1y61nzp9xlmygaq5mk8vf8rpmi4fk22zhvxy")))

(define-public crate-rubus-0.1 (crate (name "rubus") (vers "0.1.0") (hash "06f7zf08x8rvdclk09kbmbiniakhmr0x2zybz4yj8fdj4y2flyxy")))

