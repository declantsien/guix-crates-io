(define-module (crates-io ru m_) #:use-module (crates-io))

(define-public crate-rum_framework-0.0.1 (crate (name "rum_framework") (vers "0.0.1") (deps (list (crate-dep (name "mime") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "mime_guess") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)))) (hash "145fz17a28j62q4viz35bir6g4iyj0hfmjq7h6vm9mjfhbvp8z4g")))

