(define-module (crates-io ru fl) #:use-module (crates-io))

(define-public crate-rufl-0.1 (crate (name "rufl") (vers "0.1.1") (deps (list (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0jqmpvjhfq93dk94rdlywfacl1s6hf3lh7c285wjiz69z3fj9acm") (features (quote (("string") ("random") ("math") ("file") ("eventbus") ("default" "collection" "eventbus" "file" "math" "random" "string") ("collection"))))))

(define-public crate-rufl-0.1 (crate (name "rufl") (vers "0.1.2") (deps (list (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0p3wqjn54zk29bz5yj75y3wm3bgjvm1lcz3ksq39mrysqyf393b8") (features (quote (("string") ("random") ("math") ("file") ("eventbus") ("default" "collection" "eventbus" "file" "math" "random" "string") ("collection"))))))

(define-public crate-rufl-0.1 (crate (name "rufl") (vers "0.1.3") (deps (list (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1ynq2zmyj4a53hfj9j4ha09x3gh2550xpnnaz5iai8cfw1kxp1g7") (features (quote (("string") ("random") ("math") ("file") ("eventbus") ("default" "collection" "eventbus" "file" "math" "random" "string") ("collection"))))))

(define-public crate-ruflex-0.1 (crate (name "ruflex") (vers "0.1.0") (hash "128mady7njpwjibhv25gsi1slpczahvrc7rlz9kqrqab3mjrx32r") (yanked #t)))

(define-public crate-ruflex-0.1 (crate (name "ruflex") (vers "0.1.1") (hash "0dk7mhyglinmfp09ar1z4y2x863prcpf2j5lps9sv4f9jm2pi0q4") (yanked #t)))

(define-public crate-ruflex-0.1 (crate (name "ruflex") (vers "0.1.2") (hash "0s7f6k75m7h9mqh5crq5vkjnwm2zifkx6b41pn9ylk33jndhis44") (yanked #t)))

