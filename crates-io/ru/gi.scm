(define-module (crates-io ru gi) #:use-module (crates-io))

(define-public crate-rugint-0.1 (crate (name "rugint") (vers "0.1.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "11n003v7vkbhp0mawf6zvsscslxbz0mfrg7w3rkb5hafmpm0zdgw")))

(define-public crate-rugint-0.1 (crate (name "rugint") (vers "0.1.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "10sw4x85l36vrcapnw10ganra7jzy9rd5dcgaqfrp31y5ij7c248") (features (quote (("default" "rand"))))))

(define-public crate-rugint-0.1 (crate (name "rugint") (vers "0.1.2") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "155whk068vdsv958iwixy2ls5nlicrijqpy6fsqf06v5mgg75989") (features (quote (("default" "rand"))))))

(define-public crate-rugint-0.1 (crate (name "rugint") (vers "0.1.3") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "08yzyx0s7vlvzsi81jr602xq1c3dhpb1ln4kdf8lk6d0wfpq5zp2") (features (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugint-0.2 (crate (name "rugint") (vers "0.2.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1j55wy54nnzcqnk8yai80rlywckjf1mw15xsnchmchz2xr7b3d5d") (features (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugint-0.2 (crate (name "rugint") (vers "0.2.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0j376v1irn5rmm7z46mmq61a68r3whc2wn8hmzzm9xqzmp1x66l0") (features (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugint-0.2 (crate (name "rugint") (vers "0.2.2") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.0") (kind 0)) (crate-dep (name "rand") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "043f0gvphnvil3ypg29d3msvgipiiri9p7j15qyg064bglnni0r1") (features (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugint-0.3 (crate (name "rugint") (vers "0.3.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.0") (kind 0)) (crate-dep (name "rand") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "176z0hyrrlghxyvrb42nc1miiw9p9ch12ajfphbavmabvd8wmcdl") (features (quote (("random" "rand") ("default" "random"))))))

(define-public crate-rugint-0.4 (crate (name "rugint") (vers "0.4.0") (hash "1bh1jf5f9d9crybccxn7gj484rczaj6zkmmv6llcxq4ck85lj8d0")))

(define-public crate-rugint-0.4 (crate (name "rugint") (vers "0.4.1") (hash "1hnkrmk8xsfp3lw23sii6i8ani6zbviymidn0qq1y3xvna7qkkkq")))

(define-public crate-rugistry-0.0.0 (crate (name "rugistry") (vers "0.0.0") (hash "0fibv0r813rgyazkhq8a29cb85zg8z5szsky8h854lxa2jlqbclr")))

