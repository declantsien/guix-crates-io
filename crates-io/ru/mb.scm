(define-module (crates-io ru mb) #:use-module (crates-io))

(define-public crate-rumble-0.0.1 (crate (name "rumble") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "1fczrchjdd6bbbr5zxnkp4kmhnpj35jhnizci0c29w02a81mbm1a")))

(define-public crate-rumble-0.2 (crate (name "rumble") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1v76fg7334x7clclvccwklqair6fx8arnasjdihhrncr0cyxvcx4")))

(define-public crate-rumble-0.2 (crate (name "rumble") (vers "0.2.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1j61yxqii77a54v6xy0s889n5gn19rlb5sl9dd6bi6pdasy553pd")))

(define-public crate-rumble-0.3 (crate (name "rumble") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.0") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0j0akjm0yihvznl9i4f5zi025pywbbfd8jldgwm4vm2xsnq8mf04")))

(define-public crate-rumblebars-0.1 (crate (name "rumblebars") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0iq11wn3912prcpdqzgpvmzbrmjn37bg1lllppijhwizziisii6s")))

(define-public crate-rumblebars-0.1 (crate (name "rumblebars") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d1rayrrh5fpy8y3l7b9dfmcn8w4yrdclyrk7hdlhjzhs1j62yv2")))

(define-public crate-rumblebars-0.1 (crate (name "rumblebars") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rustlex_codegen") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustlex_codegen") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 1)))) (hash "1jnm9qipkv34v90di0kjjgjnkn2ff5p8f3wmkl8b0a1vd4n72hkr") (features (quote (("with-syntex" "syntex" "rustlex_codegen/with-syntex") ("stable" "with-syntex") ("nightly" "rustlex") ("default" "stable"))))))

(define-public crate-rumblebars-0.2 (crate (name "rumblebars") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rustlex_codegen") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustlex_codegen") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 1)))) (hash "0l2qi4xmlkqiiwa6bibv0wg587i1d6fhlf0r8r9wihrz017hgsql") (features (quote (("with-syntex" "syntex" "rustlex_codegen/with-syntex") ("stable" "with-syntex") ("nightly" "rustlex") ("default" "stable"))))))

(define-public crate-rumblebars-0.3 (crate (name "rumblebars") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustlex") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "rustlex_codegen") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustlex_codegen") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 1)))) (hash "1hzkigsrxfjk8bwf7givzkgbhr9ad3j9fcbg4lq6w36iyrxxnhd9") (features (quote (("with-syntex" "syntex" "rustlex_codegen/with-syntex") ("stable" "with-syntex") ("nightly" "rustlex") ("default" "stable"))))))

(define-public crate-rumblebars-rustlex-0.3 (crate (name "rumblebars-rustlex") (vers "0.3.1") (deps (list (crate-dep (name "rustlex_codegen") (req "*") (default-features #t) (kind 0)))) (hash "1qrmswkpnx9dqp4rgy4nb0pxrdr2la7xw8q033c94lfnj865hngl") (yanked #t)))

(define-public crate-rumblebars-rustlex-0.3 (crate (name "rumblebars-rustlex") (vers "0.3.2") (deps (list (crate-dep (name "rumblebars-rustlex_codegen") (req "*") (default-features #t) (kind 0)))) (hash "15hl02xh41fk981hwv2xb60gfqlxkb83mlhhc7ivzlw10cch6lzp") (yanked #t)))

(define-public crate-rumblebars-rustlex_codegen-0.3 (crate (name "rumblebars-rustlex_codegen") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quasi") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quasi_codegen") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syntex") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "syntex_syntax") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1jglqldwp9a86qmrcfqfvzqj3h4hdqcv4r6khhz7jjmnihvrmgnc") (features (quote (("with-syntex" "quasi/with-syntex" "quasi_codegen/with-syntex" "syntex" "syntex_syntax")))) (yanked #t)))

(define-public crate-rumblr-1 (crate (name "rumblr") (vers "1.0.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0dg4hf0rycb4c99w9ya0iyw52yzlq3w19mx2x5i36r3xp43g1mwy")))

(define-public crate-rumblr-1 (crate (name "rumblr") (vers "1.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "021fs2q5mwvh1fa0vhfjddyndyicpf8q3rksg2m243k3y62dl798")))

(define-public crate-rumblr-1 (crate (name "rumblr") (vers "1.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "167k690xjkmfdarmfpa8ykmgzjz5yb8pxgwlywgma46ks9plgz90")))

(define-public crate-rumblr-1 (crate (name "rumblr") (vers "1.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0n3qcrm4d2l7lc1x4dixalksn52pr7gqaadksh6203vynzblcm2l")))

(define-public crate-rumblr-1 (crate (name "rumblr") (vers "1.4.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0x95a3s1gxxd3kx6sw0sda2layyympxy5f47nwfym3dznlmp3dqx")))

(define-public crate-rumblr-2 (crate (name "rumblr") (vers "2.0.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0rlga9s9cvx5f4pywjkj75s36j2n2i9f8rd734mwlrqg7ijqqsxi")))

(define-public crate-rumblr-2 (crate (name "rumblr") (vers "2.0.1") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0rmcyx5dn2yl85lrn3s080vyx45aajjlwy225bllczklx31ks2ny")))

(define-public crate-rumblr-2 (crate (name "rumblr") (vers "2.0.2") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "oauth-client") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "00akvl0a210mgqw36fyih5g83jgny012mify4nl5wyir259lcd4d")))

(define-public crate-rumbrella-0.1 (crate (name "rumbrella") (vers "0.1.0") (hash "00fnrm2ka1dn7qcpnaa0vafdz2fx43dbl3g18i3ii8v6xj6avawm")))

(define-public crate-rumbrella-cargo-0.1 (crate (name "rumbrella-cargo") (vers "0.1.0") (hash "117zyzfh1h0rj24wmyrm27g3jii2vvgn04sr1wh3b3naynd3rhdy")))

(define-public crate-rumbrella-cli-0.1 (crate (name "rumbrella-cli") (vers "0.1.0") (hash "18sxlpf15s6zszkfa12d14dyy4rkdxzpz842vdxhzqfrcs9zrhmg")))

(define-public crate-rumbrella-ide-0.1 (crate (name "rumbrella-ide") (vers "0.1.0") (hash "08r7c3znzydal0cyn0lrk4m4xc4ngv4hzj5q74abpqikc4jajk42")))

(define-public crate-rumbrella-introtoalgorithms-0.1 (crate (name "rumbrella-introtoalgorithms") (vers "0.1.0") (hash "1zxn7mxaar6rsvms2b3vkl2zj1cg23g6px16i77g9wd1540bqxim")))

(define-public crate-rumbrella-rdk-0.1 (crate (name "rumbrella-rdk") (vers "0.1.0") (hash "08sadwqlg2h6bb5lcskivdhj6j02h1p4ss57qas0w9yjvj2zzpli")))

(define-public crate-rumbrella-rdk-a11y-0.1 (crate (name "rumbrella-rdk-a11y") (vers "0.1.0") (hash "0bcisjydmmqdxk4nv5h1y9lhk7mvb95mac5zj4bc6pk78qz209vq")))

(define-public crate-rumbrella-rdk-arch-0.1 (crate (name "rumbrella-rdk-arch") (vers "0.1.0") (hash "08yvlpavraxw4cvlg499293rrvi8gy1g7wlx36rksnzv958hdhbq")))

(define-public crate-rumbrella-rdk-chrono-0.1 (crate (name "rumbrella-rdk-chrono") (vers "0.1.0") (hash "1n0l81h0cllfvykkprylpa2rzq866rj9n0ajxy181a9v91pz5hyc")))

(define-public crate-rumbrella-rdk-cli-0.1 (crate (name "rumbrella-rdk-cli") (vers "0.1.0") (hash "1fssvaz25nq06l50bcggb3iflzm35skhlxxny4iq04hy4qw045dh")))

(define-public crate-rumbrella-rdk-core-0.1 (crate (name "rumbrella-rdk-core") (vers "0.1.0") (hash "0ss75l9fq49vii5ixhh3gi0hh2rj3hg8195zplgm35jgw7s67v9p")))

(define-public crate-rumbrella-rdk-crypto-0.1 (crate (name "rumbrella-rdk-crypto") (vers "0.1.0") (hash "1jdz56h69hwd8s81lq4116dndn2cc1fx432yxvibj4ccp9r3m71m")))

(define-public crate-rumbrella-rdk-db-0.1 (crate (name "rumbrella-rdk-db") (vers "0.1.0") (hash "0hccqjbriamaaraww1lpkj7csv94g574lz1nvcwiw2hv2d099wm1")))

(define-public crate-rumbrella-rdk-db-kv-0.1 (crate (name "rumbrella-rdk-db-kv") (vers "0.1.0") (hash "1fyrqzsgiwb54b87m3a8616njak6i7if2jkgmq3yyl6zma0dnc6b")))

(define-public crate-rumbrella-rdk-db-or-0.1 (crate (name "rumbrella-rdk-db-or") (vers "0.1.0") (hash "0pfb8vsz8g8z9rvqj5bbfj6wylkh30wzp1300dq79k7mhd3g8ry8")))

(define-public crate-rumbrella-rdk-db-rel-0.1 (crate (name "rumbrella-rdk-db-rel") (vers "0.1.0") (hash "016pmkyygc3r7r7lnzs7by32hpykhj44rsw5b6a4ysdbhzmqahz4")))

(define-public crate-rumbrella-rdk-doc-0.1 (crate (name "rumbrella-rdk-doc") (vers "0.1.0") (hash "0fsanajibfp53nx4ml6gx6fx81d1r9ba9ml743lpwdq7fcpih41f")))

(define-public crate-rumbrella-rdk-doc-msooxml-0.1 (crate (name "rumbrella-rdk-doc-msooxml") (vers "0.1.0") (hash "0ij192amlff3cz8p0ldcszcl3p2i6w7hg1bjsmb63n2y6npqaw18")))

(define-public crate-rumbrella-rdk-doc-odf-0.1 (crate (name "rumbrella-rdk-doc-odf") (vers "0.1.0") (hash "080zwf3ra2l12vqdqri27l6qc4ca9ha8lj9nykaip8rcydazx1fq")))

(define-public crate-rumbrella-rdk-ffi-0.1 (crate (name "rumbrella-rdk-ffi") (vers "0.1.0") (hash "0pvya6lp9d86yx3yb50rs457d527l3ha63mvzswazkjc9p87fpaq")))

(define-public crate-rumbrella-rdk-ffi-asm-0.1 (crate (name "rumbrella-rdk-ffi-asm") (vers "0.1.0") (hash "1f5gac2aq6qj97lsjlj1xhbw6s2wnnik5rgqbz9sqppvsdh1rnv7")))

(define-public crate-rumbrella-rdk-ffi-c-0.1 (crate (name "rumbrella-rdk-ffi-c") (vers "0.1.0") (hash "0yvay3mjhxqc82n13qabp9w5ap3fgmykh34ak9mgjh4ji414cnja")))

(define-public crate-rumbrella-rdk-ffi-cpp-0.1 (crate (name "rumbrella-rdk-ffi-cpp") (vers "0.1.0") (hash "1syl8k3ca4k7mmpz84zj1zrgvdssij6brn4cpg7vk5ffgg6m2wx2")))

(define-public crate-rumbrella-rdk-ffi-d-0.1 (crate (name "rumbrella-rdk-ffi-d") (vers "0.1.0") (hash "0fcpbxa3j9kccy1qvbnk0jr3q3spibianwh8mqh7z1i7z244jpm6")))

(define-public crate-rumbrella-rdk-ffi-pascal-0.1 (crate (name "rumbrella-rdk-ffi-pascal") (vers "0.1.0") (hash "0jmcvim7k7p2lb401mbdzhcpihiwg9rqk67bldhdwmh2pnj7y5iy")))

(define-public crate-rumbrella-rdk-gfx-0.1 (crate (name "rumbrella-rdk-gfx") (vers "0.1.0") (hash "0hx1ajhsrdg32c8v49v33zj0lg5safcfswghk9vd3qwj3lq2x5ra")))

(define-public crate-rumbrella-rdk-gfx-dx-0.1 (crate (name "rumbrella-rdk-gfx-dx") (vers "0.1.0") (hash "0ad6ll312bll69z3b5zkbyr2i0fa1agbgxkr4lqmg2xz5anc8jhg")))

(define-public crate-rumbrella-rdk-gfx-metal-0.1 (crate (name "rumbrella-rdk-gfx-metal") (vers "0.1.0") (hash "0h0zk1pvg87jzlh1x6diy02rih2fd3fp1vigws9cfb7fldq6fpri")))

(define-public crate-rumbrella-rdk-gfx-opengl-0.1 (crate (name "rumbrella-rdk-gfx-opengl") (vers "0.1.0") (hash "17dih3b451mzc7jasg2sfsxgkcyck1dhvrypsj3l0yjy8n0msp7y")))

(define-public crate-rumbrella-rdk-gfx-vulkan-0.1 (crate (name "rumbrella-rdk-gfx-vulkan") (vers "0.1.0") (hash "0fyxg906wkhqqywif57h1cc04b9av2wj2f7jw1ypa9jcifmd8wfb")))

(define-public crate-rumbrella-rdk-gui-0.1 (crate (name "rumbrella-rdk-gui") (vers "0.1.0") (hash "1i46z9gwwjlkz476zaywb946rbm6zcdlyr73anf3l8m2isygch8x")))

(define-public crate-rumbrella-rdk-i18n-0.1 (crate (name "rumbrella-rdk-i18n") (vers "0.1.0") (hash "1dg8bprvnnimv4ldb4zaivvdii2if344azcrkcca1imyhnawppl1")))

(define-public crate-rumbrella-rdk-img-0.1 (crate (name "rumbrella-rdk-img") (vers "0.1.0") (hash "0z554h3d3bc78mp0plivvrz9bdvgd1l3zwhmiyfy0anigz4cgbr7")))

(define-public crate-rumbrella-rdk-img-pix-0.1 (crate (name "rumbrella-rdk-img-pix") (vers "0.1.0") (hash "02nzlwq5baxrc4wwwiis6r6szkdxj7j0092x45lq1kbbyg6zwzj1")))

(define-public crate-rumbrella-rdk-img-pix-bmp-0.1 (crate (name "rumbrella-rdk-img-pix-bmp") (vers "0.1.0") (hash "17l54yfv3r444i7yqmk5g1d9n6qad9l8fay0gy6lvgc0dbhj04f7")))

(define-public crate-rumbrella-rdk-img-pix-gif-0.1 (crate (name "rumbrella-rdk-img-pix-gif") (vers "0.1.0") (hash "0cx81pjdl48kscx6p3waf8432wxpr97mc0yrw3zqhzcg5qhcxzl4")))

(define-public crate-rumbrella-rdk-img-pix-jpg-0.1 (crate (name "rumbrella-rdk-img-pix-jpg") (vers "0.1.0") (hash "1afizskx0l3qw3zqmgs2j4yi938qf84cj0md9kyyhj93bxsd041f")))

(define-public crate-rumbrella-rdk-img-pix-png-0.1 (crate (name "rumbrella-rdk-img-pix-png") (vers "0.1.0") (hash "0cbylgagmpis1cjzdcpvx4dqn0lnpbbwiwy9inzc36kmx541v8av")))

(define-public crate-rumbrella-rdk-img-pix-tiff-0.1 (crate (name "rumbrella-rdk-img-pix-tiff") (vers "0.1.0") (hash "05bn43d44brf6i2hfpb847kgkc9wakr9sll5p1qmnchiax2zyngf")))

(define-public crate-rumbrella-rdk-img-vec-0.1 (crate (name "rumbrella-rdk-img-vec") (vers "0.1.0") (hash "1asnxai033vyzlwx9mnlrz44xhs358yanifrzzf13anj563jwjd6")))

(define-public crate-rumbrella-rdk-img-vec-pdf-0.1 (crate (name "rumbrella-rdk-img-vec-pdf") (vers "0.1.0") (hash "17dn78cdqnw01rbjjfnji3y9r9bwpi0y01ka0x24jh6plcqh52nj")))

(define-public crate-rumbrella-rdk-img-vec-ps-0.1 (crate (name "rumbrella-rdk-img-vec-ps") (vers "0.1.0") (hash "0ciq2qvy1ghh7m6vrg6rj7n99kviv3n407kznaqlaalaaxl25g1d")))

(define-public crate-rumbrella-rdk-img-vec-svg-0.1 (crate (name "rumbrella-rdk-img-vec-svg") (vers "0.1.0") (hash "0iyajk666m4nhpbvyvgrkmzz8icri2rik5hawk13wr488qbj58p2")))

(define-public crate-rumbrella-rdk-io-0.1 (crate (name "rumbrella-rdk-io") (vers "0.1.0") (hash "08zmqma7msrhy8chdnim05kbdq1nc3cn94xqpzmn6v46pi49z1kq")))

(define-public crate-rumbrella-rdk-math-0.1 (crate (name "rumbrella-rdk-math") (vers "0.1.0") (hash "18a4a3z048wqshisaasrccng15vkxr00d2hfvla3jwd2v3pc675j")))

(define-public crate-rumbrella-rdk-mem-0.1 (crate (name "rumbrella-rdk-mem") (vers "0.1.0") (hash "1z2phnqll984yhy9xlf8g0g5gky7w125jnpbjq88m38mk674ny11")))

(define-public crate-rumbrella-rdk-multimedia-0.1 (crate (name "rumbrella-rdk-multimedia") (vers "0.1.0") (hash "09p8d5y415xg43641pp7d4gcrv4p1d55p9cdqz1mhn7lwjp5n7w6")))

(define-public crate-rumbrella-rdk-multimedia-audio-0.1 (crate (name "rumbrella-rdk-multimedia-audio") (vers "0.1.0") (hash "0a9i3as5x5rgm8m16p2zb6js3czh439ppqdw5z7n23nnya6yi9xn")))

(define-public crate-rumbrella-rdk-multimedia-video-0.1 (crate (name "rumbrella-rdk-multimedia-video") (vers "0.1.0") (hash "1ncgrfapfakjl7bq85ghps959vk7xnii235fbwxz8wfb3hd3bmyb")))

(define-public crate-rumbrella-rdk-name-0.1 (crate (name "rumbrella-rdk-name") (vers "0.1.0") (hash "0k6sss9w3h32liip36j76l9f2iqfys36ccmrhc1sjq5dj4cryjn6")))

(define-public crate-rumbrella-rdk-name-dns-0.1 (crate (name "rumbrella-rdk-name-dns") (vers "0.1.0") (hash "1cdv4v5cvb6sh258l9n0r5cxp3wg16lli6g9rhslmyh36s1l7vpv")))

(define-public crate-rumbrella-rdk-name-ldap-0.1 (crate (name "rumbrella-rdk-name-ldap") (vers "0.1.0") (hash "00qigir2a0sz74pfk0b7w5ab5d5pxfyszvs6306dzhyhhvwa8xv0")))

(define-public crate-rumbrella-rdk-net-0.1 (crate (name "rumbrella-rdk-net") (vers "0.1.0") (hash "0fch42wwl6m3hfnpipf70vhis2i1nn60564f7sfsccfwsy1d2n8k")))

(define-public crate-rumbrella-rdk-net-http-0.1 (crate (name "rumbrella-rdk-net-http") (vers "0.1.0") (hash "0sa218zl9rw7fkiisplj5qr84viyg3a2dsmi6yhxl1ddxlrwj8gf")))

(define-public crate-rumbrella-rdk-net-ip-0.1 (crate (name "rumbrella-rdk-net-ip") (vers "0.1.0") (hash "1vzc9yy2ks5g72k9357bq2rm5cspldf2fffdmm5sf4c0rcyldnls")))

(define-public crate-rumbrella-rdk-net-ip-icmp-0.1 (crate (name "rumbrella-rdk-net-ip-icmp") (vers "0.1.0") (hash "0f3z5cyrhw9apjda7wlbd69zf270kpv115p9iwzrwy1pvv3pf20a")))

(define-public crate-rumbrella-rdk-net-ip-tcp-0.1 (crate (name "rumbrella-rdk-net-ip-tcp") (vers "0.1.0") (hash "164dms6w37vmzxkxdmh8k57wf5rjhbqhz7plx6i24kvc13x9wqm2")))

(define-public crate-rumbrella-rdk-net-ip-udp-0.1 (crate (name "rumbrella-rdk-net-ip-udp") (vers "0.1.0") (hash "1wmz4zyg44isih0rwjs1sgvg1nja4gbpm8abr9z8h2sd5h07xyws")))

(define-public crate-rumbrella-rdk-net-smtp-0.1 (crate (name "rumbrella-rdk-net-smtp") (vers "0.1.0") (hash "16h9c7bixq0skr2byqmk8ah5k1j06cnhj67haxpqjgdgkr2srv20")))

(define-public crate-rumbrella-rdk-net-snmp-0.1 (crate (name "rumbrella-rdk-net-snmp") (vers "0.1.0") (hash "1mkgrl68ld2ngg6lib32vl5zbbr3fp9cv9bqpj4sa0xhm52vlbr4")))

(define-public crate-rumbrella-rdk-net-snmp-mib-0.1 (crate (name "rumbrella-rdk-net-snmp-mib") (vers "0.1.0") (hash "1x22fns30yqvxgwzxr6mnr3zbdldrb3zvc5gngdmggr11yqg3wg9")))

(define-public crate-rumbrella-rdk-orm-0.1 (crate (name "rumbrella-rdk-orm") (vers "0.1.0") (hash "1x25701ib17yq9wrxh230m0jpsygg0qnq529rs7zj92bqhfhp3g2")))

(define-public crate-rumbrella-rdk-os-0.1 (crate (name "rumbrella-rdk-os") (vers "0.1.0") (hash "108k00asnkvvbljlg2x82sxlwi2hjk80fax4h7iyq696752qklf7")))

(define-public crate-rumbrella-rdk-rand-0.1 (crate (name "rumbrella-rdk-rand") (vers "0.1.0") (hash "0y7y6kzly8m5lixn42fhjssmn4qrwzd1i5vh2vc0kcf3baxf3cjm")))

(define-public crate-rumbrella-rdk-rust-0.1 (crate (name "rumbrella-rdk-rust") (vers "0.1.0") (hash "1s14xcviy9svi5vrdyv8n938sl5cllxlh0s3r9d1nwispz7bkzs1")))

(define-public crate-rumbrella-rdk-serialization-0.1 (crate (name "rumbrella-rdk-serialization") (vers "0.1.0") (hash "1yddyy1979h0zvzyvlir9krfh2gpsl4xnqabhkafzrxi6h8qxgs6")))

(define-public crate-rumbrella-rdk-serialization-csv-0.1 (crate (name "rumbrella-rdk-serialization-csv") (vers "0.1.0") (hash "18z07mrq4x46j2x2hd2h3q1nh7nxfcmdsca5pmshxcrcxpm7p23s")))

(define-public crate-rumbrella-rdk-serialization-json-0.1 (crate (name "rumbrella-rdk-serialization-json") (vers "0.1.0") (hash "1lrfg67i93m73d4b6dn4v6csaxn5d24glb11qmix6x7grm7sal7g")))

(define-public crate-rumbrella-rdk-serialization-rson-0.1 (crate (name "rumbrella-rdk-serialization-rson") (vers "0.1.0") (hash "029ndd0ix5gckgvick7nmg2im64mrb7sxv4sr3y0pkdq56v1pvza")))

(define-public crate-rumbrella-rdk-serialization-toml-0.1 (crate (name "rumbrella-rdk-serialization-toml") (vers "0.1.0") (hash "09slib86m36dbwczhc24wgfscfzmnaafxdp3bry3wl81i1mvpkq4")))

(define-public crate-rumbrella-rdk-serialization-yaml-0.1 (crate (name "rumbrella-rdk-serialization-yaml") (vers "0.1.0") (hash "0w18q4qssdf2njl1sramphfkxd2zzlsd1mc5697g9m174zki7bdy")))

(define-public crate-rumbrella-rdk-std-0.1 (crate (name "rumbrella-rdk-std") (vers "0.1.0") (hash "089c1lg6cd5sv85yk0wcn6yj4rm1q3w1f042c9vf5g9x4k0lizjx")))

(define-public crate-rumbrella-rdk-string-0.1 (crate (name "rumbrella-rdk-string") (vers "0.1.0") (hash "0ghvah4qkgn7ryhwyckya1f3gbbl4bpakrsjbjvgi1in3mgnd23c")))

(define-public crate-rumbrella-rdk-tui-0.1 (crate (name "rumbrella-rdk-tui") (vers "0.1.0") (hash "17g2y9960k8svk00qrdsg581vsvwd6h1ykkz99dh6azs4xjpfcwl")))

(define-public crate-rumbrella-rdk-vcs-0.1 (crate (name "rumbrella-rdk-vcs") (vers "0.1.0") (hash "09gg2vn64gl66qpn9sljgqq0v0lhsfkzx4ypr1p6yrp2kdcs4xiw")))

(define-public crate-rumbrella-rdk-vcs-bzr-0.1 (crate (name "rumbrella-rdk-vcs-bzr") (vers "0.1.0") (hash "1849cykgdb381lzycv845c10iwjzc47gdbbdsn89x841p68yjqnf")))

(define-public crate-rumbrella-rdk-vcs-cvs-0.1 (crate (name "rumbrella-rdk-vcs-cvs") (vers "0.1.0") (hash "13ncjxss58gsikiws69xm6kyi5z15rv0vn4f3l1pbjpzkqdc3vn1")))

(define-public crate-rumbrella-rdk-vcs-git-0.1 (crate (name "rumbrella-rdk-vcs-git") (vers "0.1.0") (hash "19qrq9zabv7p74jc7fc98c23fj5pmdbi010i20wggalir3i20f6k")))

(define-public crate-rumbrella-rdk-vcs-hg-0.1 (crate (name "rumbrella-rdk-vcs-hg") (vers "0.1.0") (hash "0c0frd9i23qqihhxxir785p5ksf1wldfwfajxiazh96pwcc742hs")))

(define-public crate-rumbrella-rdk-vcs-svn-0.1 (crate (name "rumbrella-rdk-vcs-svn") (vers "0.1.0") (hash "0zp0blddg618dxbihiifrvby30klf7vggjrlgf5x2q7qhkm0nb0q")))

(define-public crate-rumbrella-rdk-vm-0.1 (crate (name "rumbrella-rdk-vm") (vers "0.1.0") (hash "15l2xk26zkibsbr65229f2x2qqirb9dxshfjx5k4adfm60idbkal")))

(define-public crate-rumbrella-rdk-vm-clr-0.1 (crate (name "rumbrella-rdk-vm-clr") (vers "0.1.0") (hash "1dn0bggl023j0mdg7s37mwzc1cdm6z4fbr34psnkv97sxwxynzga")))

(define-public crate-rumbrella-rdk-vm-js-0.1 (crate (name "rumbrella-rdk-vm-js") (vers "0.1.0") (hash "00d3x7camvnsw2gjmmgnxn6ivp7yxqbn71jjg02hqbcnv9s26g7z")))

(define-public crate-rumbrella-rdk-vm-jvm-0.1 (crate (name "rumbrella-rdk-vm-jvm") (vers "0.1.0") (hash "0zdn69bs3bzfhml6cf3y7bnin1vvlmgb4bgqkfcnjp4871mww5x8")))

(define-public crate-rumbrella-rdk-vm-llvm-0.1 (crate (name "rumbrella-rdk-vm-llvm") (vers "0.1.0") (hash "1s4n35mcibyl69p8kgd56mm102qy75p9602mnzsbh5largz4z4ss")))

(define-public crate-rumbrella-rdk-vm-perl-0.1 (crate (name "rumbrella-rdk-vm-perl") (vers "0.1.0") (hash "1h98611hpibl2yhflxj8g0ncmwa3x086knp8c7yr3qmwvc6i9raa")))

(define-public crate-rumbrella-rdk-vm-php-0.1 (crate (name "rumbrella-rdk-vm-php") (vers "0.1.0") (hash "1lr2xs7z90945n9nb15ngl67avwdiq2f3qhinnnx2xva7prpj3wv")))

(define-public crate-rumbrella-rdk-vm-python-0.1 (crate (name "rumbrella-rdk-vm-python") (vers "0.1.0") (hash "1cnn5n7c8ghdna27qjx60v2cq2cp45bfhzlml06qkgpyimp6nvzp")))

(define-public crate-rumbrella-rdk-vm-wasm-0.1 (crate (name "rumbrella-rdk-vm-wasm") (vers "0.1.0") (hash "0z7nvarczxdvk8a3vwwycl36n53v8pwswjvmpnm40jy0rx7zbvgk")))

(define-public crate-rumbrella-rustup-0.1 (crate (name "rumbrella-rustup") (vers "0.1.0") (hash "0k5a2gi5cfhjn0azdnq85sk9g7k70444ml00xbvbl0x97viwix03")))

