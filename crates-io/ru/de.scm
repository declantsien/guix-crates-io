(define-module (crates-io ru de) #:use-module (crates-io))

(define-public crate-rude-0.1 (crate (name "rude") (vers "0.1.0") (hash "1gqf0kcqfvqzzw1ikfid1nllrcdqgdb9nk2wjhlmqcr3jayv50yr")))

(define-public crate-rudeboy-0.1 (crate (name "rudeboy") (vers "0.1.0") (deps (list (crate-dep (name "rlua") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "rudeboy-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mkr4dfv3appl6f19nmk0s5cav7sfdgrq46dnmqq0ndma9zygakn")))

(define-public crate-rudeboy-0.2 (crate (name "rudeboy") (vers "0.2.0") (deps (list (crate-dep (name "rlua") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "rudeboy-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qpxk4acb6z8gm6806kg36gfvz3rqr1rrhzpcbxavly2kh6yq0ck")))

(define-public crate-rudeboy-derive-0.1 (crate (name "rudeboy-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03gyrnzqg0z50dzray6dvcanwc1rglpy4k921sj0d0g9hwgam35a")))

(define-public crate-rudeboy-derive-0.2 (crate (name "rudeboy-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "04mx4dq5rwskr9wssqn5ylwrjjp3iyzgb1j2kn0j518frw0qq6id")))

