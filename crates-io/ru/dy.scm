(define-module (crates-io ru dy) #:use-module (crates-io))

(define-public crate-rudy-0.0.1 (crate (name "rudy") (vers "0.0.1") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "12q28mw42mwyywla0j9jk8l69h31a4dw41bp3385b9r1jccdbdrl")))

(define-public crate-rudy-0.1 (crate (name "rudy") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hm0wy5x858xhbcvc4wm3yhwl10ksk7y89afyr1rhh4436ys3nbw")))

