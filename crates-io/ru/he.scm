(define-module (crates-io ru he) #:use-module (crates-io))

(define-public crate-ruhear-0.1 (crate (name "ruhear") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (target "cfg(not(target_os = \"macos\"))") (kind 0)) (crate-dep (name "screencapturekit") (req "^0.2") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "0hmp09v1s9d01bm5r26h2mpnfn4j973qsxc5sjv6ap3cr6zlhbfc")))

