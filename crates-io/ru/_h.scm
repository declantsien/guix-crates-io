(define-module (crates-io ru _h) #:use-module (crates-io))

(define-public crate-ru_history-0.1 (crate (name "ru_history") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "bogobble") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "err_tools") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "str_tools") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hggh0lkblmfrhja88v26qz0j86k9dscgfx75gyiqfzwjyq1h8iz")))

