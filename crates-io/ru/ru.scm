(define-module (crates-io ru ru) #:use-module (crates-io))

(define-public crate-ruru-0.5 (crate (name "ruru") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0bhb2lpiqyqbdn255mh0085dir93m3id57svi57lk6xfl4dviia2")))

(define-public crate-ruru-0.5 (crate (name "ruru") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0hvlxipbidmwsi2mx4d3cz2qjqmcnmy9d6f7vhh0jdfm80jgg6x4")))

(define-public crate-ruru-0.5 (crate (name "ruru") (vers "0.5.2") (deps (list (crate-dep (name "libc") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1rymkw5rwyi5yh75knd6ijdpsdfxgny8lx3qjbgr2wv9ymn5z04y")))

(define-public crate-ruru-0.5 (crate (name "ruru") (vers "0.5.3") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1hnilzcc3vwggh92m5irmz2icd0xxk0ddbrys077klblvnz5l3zc")))

(define-public crate-ruru-0.5 (crate (name "ruru") (vers "0.5.4") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1hipmkxwypzh4bh089j60wwb1601gx0qy29dhi2y70bff2w39q14")))

(define-public crate-ruru-0.5 (crate (name "ruru") (vers "0.5.5") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0msiqwp52a93cr0m93d1fncv9ihfzhadavbl0r6cg88zypaqg1d5")))

(define-public crate-ruru-0.6 (crate (name "ruru") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "182abmvs7v5rv7xgs1mvjigsp52y0x3vyja2z3p64bn5sa5xv4nm")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0n9qa6rna9192h1rxlvprcykacgj2c6iv478mjpww1jz1hzz20hr")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.1") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "080270frm0bx97xbidk1z4m892ymck20ii1xmb0jbh6gqnnmymh5")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.2") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0li47bj3dl5f19k70g46d1kvxiac86cya7yf48r151pf3r18fba3")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.3") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "12j1m5r0d756waisi7hbrbas75nr68kcjr1k5837d9xf46ap3jdc")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.4") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "17q5h7k7cd0nq8fj3jxlm19qyrdr2wi41qv2waibkra46jnld2sq")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.5") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1q6ccpzdsmcgy0x1dsv7l5jnp4kb5dznlbxxxd68gkjccwr15l0w")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.6") (deps (list (crate-dep (name "libc") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0a4h61zb0z0xs9a5in0b25901c71mzwd9xf4dnk6zbav5qznylz5")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.7") (deps (list (crate-dep (name "ruby-sys") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "12y3d96ha17plb9640yl09mwk0zss7c1wj37yix92n8msi6mgkz5")))

(define-public crate-ruru-0.7 (crate (name "ruru") (vers "0.7.8") (deps (list (crate-dep (name "ruby-sys") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0zc3yn4pdmc9s5q2bmlqf8wlffb7aq9w813axkwhyh8449j83gmv")))

(define-public crate-ruru-0.8 (crate (name "ruru") (vers "0.8.0") (deps (list (crate-dep (name "ruby-sys") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "12acc2wh0b4fyy8nmg84swhjfgp46kjwffpmq3rvc1w6inbdfdqr")))

(define-public crate-ruru-0.8 (crate (name "ruru") (vers "0.8.1") (deps (list (crate-dep (name "ruby-sys") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "0razsf856r1q00cmdjz38jqv2lvwhk68c66br8x4zwx160r989kk")))

(define-public crate-ruru-0.9 (crate (name "ruru") (vers "0.9.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "09wl6bzvf8ipggh84gnf6azvnz931m9lpj4ix377n3ncqd48dj7v")))

(define-public crate-ruru-0.9 (crate (name "ruru") (vers "0.9.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0n3b2cmr2k2zqzv60mbaw9s3lhm31l5spxrzmv4jy1xcj8vm2zn7")))

(define-public crate-ruru-0.9 (crate (name "ruru") (vers "0.9.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.2.19") (default-features #t) (kind 0)))) (hash "1q7riimllqa4qgv3lka3swyq8rsi9knnxhb1bpfic0bg11bclm9b")))

(define-public crate-ruru-0.9 (crate (name "ruru") (vers "0.9.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ruby-sys") (req "^0.2.20") (default-features #t) (kind 0)))) (hash "0wq2iqlzc0xn7r21s11qq50pv5y16lv8jq9a95m2823s1g2xd1k4")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req ">= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req ">= 0.1.4") (default-features #t) (kind 2)))) (hash "1lxh6vwpqrch00pgn7saxzangwm7njf84387ym3kb4lrj8cw1038")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req ">= 0.1.7") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req ">= 0.1.4") (default-features #t) (kind 2)))) (hash "07w4c0z6iyrvm0lx6ijvmj3i7ahjlh6fmabikzzsfrx7hcxgayk5")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req ">= 0.1.8") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req ">= 0.1.4") (default-features #t) (kind 2)))) (hash "1hlc9bns1qsrz88kavkpxg2q3avk3l1qf756ic6c43wr5msfy26y")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req ">= 0.1.9") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req ">= 0.1.4") (default-features #t) (kind 2)))) (hash "0ckk9g3vyrh28fv3hiavy2hpmrhslqzidfh5k3hn4lhgnvlnfmm6")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req ">= 0.1.9") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req ">= 0.1.4") (default-features #t) (kind 2)))) (hash "1kxlz3sfd2lcwvajvcv43l5rpy786z95i5y6iz54am240n1lhr4a")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0c82v52dvgjyc0hgxwdcv0akbwl879xsk8s9z24g7s7dzg1917yz")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1izz1d2c65a8mmbcwawv6i0i35hwj29awd6gkwfv3xc07ps54dfj")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i7igplcdziqhajq1ydxllfssnlw6mgpcdmc8y6lvnbwx4kisdvi")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qq2g6jkkm5nj1q00zi1a0vmv3vprsbjj10p5z0rpci90vl6q3sc")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hqh7ias4f8s7p6vk8fx8d9v7x19zxcxb6dgkbfr5p2f23cl1apc")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.10") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kd3kn67a71p0qaas8gjn7816ny1b70akrmd39mrlnrxq8aasw0y")))

(define-public crate-rurust-0.1 (crate (name "rurust") (vers "0.1.11") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "09fp2250ag2rfxgk22llj1w8qckvjzx43zil3c48h8p3zgjfr8p9")))

(define-public crate-rurust-0.2 (crate (name "rurust") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">=0.2.86") (default-features #t) (kind 0)) (crate-dep (name "mri-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "07kzkf6fkyiyq52gng9dxc1rk1dlbbnbqqfg3shkhhn60qwbnh0g")))

