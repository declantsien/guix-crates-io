(define-module (crates-io ru ic) #:use-module (crates-io))

(define-public crate-ruic-0.1 (crate (name "ruic") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1s6x4g2z4k8kawbmm40d2mk0gpk7zb91dlxkgh86ji37mhf6mwr0")))

(define-public crate-ruice-0.1 (crate (name "ruice") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.64") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "02029gk3xqp4ap1xw1sq2rnc7pkx9qnmxqj1gpd9xr5f6kda5lcc") (rust-version "1.67.1")))

(define-public crate-ruice-0.1 (crate (name "ruice") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.64") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "1r8drx496h6ah5d42wq4qyqvhf6z4dk8ckkamm3jskdk27djyn9h") (rust-version "1.67.1")))

(define-public crate-ruice-0.1 (crate (name "ruice") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0wl6c1k44rvglnf3kxwv4c65g8cxvd2s2i23alh177anm475pc3b") (rust-version "1.67.1")))

(define-public crate-ruice-0.2 (crate (name "ruice") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "05csbsz9y7fgvfzp44vp90vfqlyx0i0yys8s8w6i89gyz69wfcx2") (rust-version "1.67.1")))

(define-public crate-ruice-axum-0.1 (crate (name "ruice-axum") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.64") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "ruice") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lnr2wwjkpvmig7nj9mxv7xc4dfdnzcrn5x3i6p34g0qk1x0kwi2") (rust-version "1.67.1")))

(define-public crate-ruice-axum-0.1 (crate (name "ruice-axum") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.64") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "ruice") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "12sb5m1nh77myxzkzj3f0svg571sz2kgyimwwgavsv2s4p4n5a8g") (rust-version "1.67.1")))

(define-public crate-ruice-axum-0.1 (crate (name "ruice-axum") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.18") (default-features #t) (kind 0)) (crate-dep (name "ruice") (req "=0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hv607jj1jrjc8xydcydciyaq134h76xzn56hm1lc3hdn901743p") (rust-version "1.67.1")))

(define-public crate-ruice-axum-0.2 (crate (name "ruice-axum") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "ruice") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "16yy7gzkrypylrrwf98sasl743gc0ikrnm3z60z25vzmqriy060g") (rust-version "1.67.1")))

