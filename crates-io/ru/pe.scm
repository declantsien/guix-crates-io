(define-module (crates-io ru pe) #:use-module (crates-io))

(define-public crate-ruperl-0.0.1 (crate (name "ruperl") (vers "0.0.1-alpha.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.95") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)))) (hash "1fqjkj49gs7lvs5z8sn4w3amnndw34v8x2sbfr4ra0769pbn7whc")))

