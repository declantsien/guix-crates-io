(define-module (crates-io ru ll) #:use-module (crates-io))

(define-public crate-rulloc-0.1 (crate (name "rulloc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_System_Memory" "Win32_Foundation" "Win32_System_SystemInformation" "Win32_System_Diagnostics_Debug"))) (target "cfg(windows)") (kind 0)))) (hash "19cj9cad9nzxdq06gd3j2zcyhzg1qvhd35c6r1a4nzmb8klg318s")))

