(define-module (crates-io su ml) #:use-module (crates-io))

(define-public crate-sumlib-0.0.0 (crate (name "sumlib") (vers "0.0.0") (hash "1cjd1n6h9xii3aq3hhqf1h43r463dzlwmbb2zl113l2mijlnyiz9")))

(define-public crate-sumlib-0.0.3 (crate (name "sumlib") (vers "0.0.3") (hash "12cc6hlbpq01wx6zzn6jgq427cz0ag9bxk6pkgnqxblfkwbv1yff")))

(define-public crate-sumlib-0.0.4 (crate (name "sumlib") (vers "0.0.4") (hash "1lnr5hybr4jd342glyqb0chwnm73gznjanfrhvj3lbabyri5wlnh")))

(define-public crate-sumlibrary-0.1 (crate (name "sumlibrary") (vers "0.1.1") (hash "0whzy1k9idlkp5w1g0qxv4s178yimdisk1x3f4xx02cvm5xa53kw") (yanked #t)))

(define-public crate-sumlibrary-0.1 (crate (name "sumlibrary") (vers "0.1.2") (hash "02hdsl4wyj1hxsf5d0azwcnyxrd68w0z4m03i93560rnq6qq16jv")))

