(define-module (crates-io su bv) #:use-module (crates-io))

(define-public crate-subversion-0.0.1 (crate (name "subversion") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.2") (default-features #t) (kind 1)))) (hash "1bbvzshr05mybsgjgjp8nrbl31xyyw21vr6m10rbbjlqj1wi86rf")))

(define-public crate-subversion-0.0.2 (crate (name "subversion") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.2") (default-features #t) (kind 1)))) (hash "0wrhc2jlgxppj8j3ibrwzk89j2jwkl8p58yxfkpir4gbsyyi72h7")))

(define-public crate-subversion-0.0.3 (crate (name "subversion") (vers "0.0.3") (deps (list (crate-dep (name "apr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "pyo3") (req ">=0.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.2") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1yw1h0mv84hb298b8a6ifpr43cm427l6gc46sgib0fcz1ig3g9q3") (v 2) (features2 (quote (("url" "dep:url") ("pyo3" "dep:pyo3"))))))

(define-public crate-subversion-0.0.4 (crate (name "subversion") (vers "0.0.4") (deps (list (crate-dep (name "apr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "ctor") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req ">=0.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.2") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0y4gsd59mpbp3wbl2cvhsnmnh0vchq7js2w42jchkpjxqig2afab") (v 2) (features2 (quote (("url" "dep:url") ("pyo3" "dep:pyo3"))))))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc13") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1h5b2x2i9v9ch1n60pjm4ilif3d38qrgna1js2pzxyrxgp570xd9")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc14") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0kjnjhk1c5m9vwj8sjmbpaxzmdcv6hhm9c9q00cqvkf593h44cx2")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc15") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "17hdsqpad85yggv2kaaw1diwg3azbwpywlwj84rkal2j5k6qbrvc")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc16") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "05g2yy81900ycjpyjv27p9l227m9lfmadg86hhygn8vm28p74in5")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc17") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0jj569livlfl0svdpnyrpivcl9wk9qp5z126r8h17shx3y3wpyqf")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc18") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0z7qmx11d479qd39klp3am3gzyhclpf5sdfcvrrisi59f0xw97f3")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc19") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "18fr4scw4fcgir35wzgh71jxpsvk05w9qcbpl8kbqmx0iii3kq19")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0-rc20") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "064bd18rw1fnjykdmng4ghh1dlsr724h52a330yrfvpnlk9rhz7d")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1r3xzch6ln33s3mjm9sz2c09bba5qyhj4xlxy1100kl6la4fvw1y")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vkrll8cfajawq0qm58f1ag7jsd49nxf4nsaddrgpwkixhgky27p")))

(define-public crate-subversioner-0.9 (crate (name "subversioner") (vers "0.9.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "07dczlmqpnfpalkl396ggq9hqhlajvbfwjl0zjv6f4bd8v5j9a98")))

(define-public crate-subversioner-0.10 (crate (name "subversioner") (vers "0.10.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0m7l60llamvjlv5cdk3cdrqpmdp0jwjnylqiq9a036lk87rh248f")))

(define-public crate-subversioner-0.10 (crate (name "subversioner") (vers "0.10.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16p1x3b6fwjhjp6dy13scbm85i0qyqqifj2xwrl55x7zd030m199")))

(define-public crate-subversioner-0.11 (crate (name "subversioner") (vers "0.11.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bmr3jcd334hq4qc4vjnm6bc8c5yjvyasvsvr3ill2ih2s20mhsq")))

