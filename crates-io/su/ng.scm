(define-module (crates-io su ng) #:use-module (crates-io))

(define-public crate-sungod-0.1 (crate (name "sungod") (vers "0.1.0") (hash "0y4c6cc5psh635qwcbsfjzcxwzin3g4n8as1dw8fb8f2i4yxklzm")))

(define-public crate-sungod-0.1 (crate (name "sungod") (vers "0.1.1") (hash "0gd9rkwk4gi0pn2bgigybmxwgfaifn55mapd1apkrczjxai5pq3c")))

(define-public crate-sungod-0.1 (crate (name "sungod") (vers "0.1.2") (hash "1rypwwmmkrcndclqdfak6d8b0pb538hpb70q31nhh3qxnl09n1g6")))

(define-public crate-sungod-0.2 (crate (name "sungod") (vers "0.2.0") (hash "0yqmzqsfk763214hz5vprsys4gix44rl1yai3cw9z3zw5s5i4z5z") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-sungod-0.2 (crate (name "sungod") (vers "0.2.1") (hash "0fqkb1q3yq41spmksqcrxkfmmdxzf70iph6ily2jz8n9k1j3p089") (features (quote (("std") ("default" "std"))))))

(define-public crate-sungod-0.2 (crate (name "sungod") (vers "0.2.2") (hash "0q8gmpfxhc20kmmpq65ip4hr3iggi6fy6wkxs3yf2304x2v8xvif") (features (quote (("std") ("default" "std"))))))

(define-public crate-sungod-0.3 (crate (name "sungod") (vers "0.3.0") (hash "08b9bazk2x0jgci8ck1mx0ml0310ig2dhz15zchvchzlvnpjh13y") (features (quote (("std") ("default_is_random" "std") ("default" "std"))))))

(define-public crate-sungod-0.3 (crate (name "sungod") (vers "0.3.1") (hash "1x66d4i082mfx9dfgmjk38bl3rijlzznij3vmdpm1jjf2p8wlldk") (features (quote (("std") ("default_is_random" "std") ("default" "std"))))))

(define-public crate-sungrow-winets-0.1 (crate (name "sungrow-winets") (vers "0.1.0") (deps (list (crate-dep (name "bitmask-enum") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "if_chain") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.139") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-aux") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "tungstenite") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "02h3qhrms3s6igzi4vbly4zkghv0g3c31lfyamjfs6nyq5i63dij")))

(define-public crate-sungrow-winets-0.2 (crate (name "sungrow-winets") (vers "0.2.0") (deps (list (crate-dep (name "bitmask-enum") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "if_chain") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.139") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-aux") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "tungstenite") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "0nliqai136cflpmln39y46r602k2wbaqh299kqwnhr8hsgsdyb33")))

