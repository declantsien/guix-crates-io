(define-module (crates-io su bz) #:use-module (crates-io))

(define-public crate-subzero-0.1 (crate (name "subzero") (vers "0.1.0") (deps (list (crate-dep (name "curve25519-dalek-ng") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "schnorrkel") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (default-features #t) (kind 0)))) (hash "1j38l3pg4kx15ncqn9lrwhw5q9ads5iagdc2iar957vply3wwaa0")))

