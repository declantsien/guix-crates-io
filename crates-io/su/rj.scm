(define-module (crates-io su rj) #:use-module (crates-io))

(define-public crate-surjective-enum-0.1 (crate (name "surjective-enum") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01z4s35acpc9p7p54q2560xn7ypanp9jh6clzszfdwqnzn1jrgiv")))

