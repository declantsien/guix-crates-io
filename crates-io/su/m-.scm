(define-module (crates-io su m-) #:use-module (crates-io))

(define-public crate-sum-calc-0.0.0 (crate (name "sum-calc") (vers "0.0.0") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)))) (hash "1dhniy9kkj5s1swh7q16rfaivy9jfdr9q46gmh0vw1c1rsm63yxc")))

(define-public crate-sum-calc-0.0.1 (crate (name "sum-calc") (vers "0.0.1") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)))) (hash "1qfakvlfvbyd9fqwqfjd996d045y4l6789r5qgzmsvql1siphrmi")))

(define-public crate-sum-calc-0.0.2 (crate (name "sum-calc") (vers "0.0.2") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)))) (hash "1q2kmm4sm0qyqbm6x7hcaqg1lg1si8zipwp2ndxk3495ypqxf707")))

(define-public crate-sum-calc-0.0.3 (crate (name "sum-calc") (vers "0.0.3") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "0why56w94hn9jbrp9aj32ah0kf4daxqh67n0r26gmvij33gj14jc")))

(define-public crate-sum-calc-0.0.4 (crate (name "sum-calc") (vers "0.0.4") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "1i8sfi6q2g4lnlaz6akaqpjfbfspv1is5wgx3pk6cnixv4in4fv2")))

(define-public crate-sum-calc-0.0.5 (crate (name "sum-calc") (vers "0.0.5") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "0yhgsr8jq64c1szw0bzjjkr0v43yfd3jv1j7nyc9gkmi4zwbswjm")))

(define-public crate-sum-calc-0.0.6 (crate (name "sum-calc") (vers "0.0.6") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "16mqaycspjqwksi0yf2n20fn3yw68fjbpdpsy088yj04xbxsc86f")))

(define-public crate-sum-calc-0.0.7 (crate (name "sum-calc") (vers "0.0.7") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "16dj8s249kwwpz67yax07ybhx0a78dlg13lzxwzyvnjqglgw480y")))

(define-public crate-sum-calc-0.0.8 (crate (name "sum-calc") (vers "0.0.8") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "0c3x8xamcq8g9ci2d3x0l5vx0hl3p6vl58744xln65ysd3pxvq93")))

(define-public crate-sum-calc-0.0.9 (crate (name "sum-calc") (vers "0.0.9") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "0kj7nxvlnrgxklrpf4frfvida2c7r94miwxi0k554bg5q20s3fpq")))

(define-public crate-sum-calc-0.0.10 (crate (name "sum-calc") (vers "0.0.10") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "1blg1pmvmgmgri6s6nz23jx5wyc9w50x74ffs6mhyf2bpf1rh2zw")))

(define-public crate-sum-calc-0.0.11 (crate (name "sum-calc") (vers "0.0.11") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7") (default-features #t) (kind 0)))) (hash "14dfiriblxdv69y9pl13adqxkvx4scm8flgm8jzh74hvhxd27vbd")))

(define-public crate-sum-calc-0.0.12 (crate (name "sum-calc") (vers "0.0.12") (deps (list (crate-dep (name "aoko") (req "=0.3.0-alpha.19") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7") (default-features #t) (kind 0)))) (hash "1ljsyz4i3492ah989dxr8iay5bdihxdpl5azlwdbc554z5vb8zkf")))

(define-public crate-sum-calc-0.0.13 (crate (name "sum-calc") (vers "0.0.13") (deps (list (crate-dep (name "aoko") (req "^0.3.0-alpha.25") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8") (default-features #t) (kind 0)))) (hash "14q66j4f4xn37ylg5nl125yjy2wbr4cyh3a9fnmnzr8qcar77m47")))

(define-public crate-sum-calc-0.2 (crate (name "sum-calc") (vers "0.2.0") (hash "1z8083z63rqjljb632aj49gid24r60vv1qv5id9p25vs6y82yig3")))

(define-public crate-sum-calc-0.2 (crate (name "sum-calc") (vers "0.2.1") (hash "15snm0fc6n98fqnqj2l2njbb55i4xmzyf34p3kmkq4sk2czwip5x")))

(define-public crate-sum-calc-0.2 (crate (name "sum-calc") (vers "0.2.2") (hash "15pdbh444rvp1n48mjpjj2v98kfrg3jdcw8b7i8dwlkncjgnnmfb")))

(define-public crate-sum-calc-0.2 (crate (name "sum-calc") (vers "0.2.3") (hash "0zrxbgqhhqdjpbf6bsl0ix9sa14y4sjf2lpn7bi0jzllavhn16zj")))

(define-public crate-sum-queue-0.1 (crate (name "sum-queue") (vers "0.1.0") (hash "10873k53kfw8328d37gzr5rg249qy7df1h2w1fxm2fnp9xvglhwp")))

(define-public crate-sum-queue-0.2 (crate (name "sum-queue") (vers "0.2.0") (hash "0m1amdx44s5r75im7p6xcrw72firp7jdl7h7sm15h56ljyiqhc7p")))

(define-public crate-sum-queue-1 (crate (name "sum-queue") (vers "1.0.0") (hash "17yl942a71x789mnprw8f6m74ggcnpi7jj5gm3az5dqricfad1sr")))

(define-public crate-sum-storage-2 (crate (name "sum-storage") (vers "2.0.0-alpha.6") (deps (list (crate-dep (name "codec") (req "^1.3.0") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "frame-support") (req "^2.0.0-alpha.6") (kind 0)) (crate-dep (name "frame-system") (req "^2.0.0-alpha.6") (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.6") (kind 2)) (crate-dep (name "sp-io") (req "^2.0.0-alpha.6") (kind 2)) (crate-dep (name "sp-runtime") (req "^2.0.0-alpha.6") (kind 0)) (crate-dep (name "sp-std") (req "^2.0.0-alpha.6") (kind 0)))) (hash "07lng1l3ks55ia5q8rca8q7nkya45rlzx99mnfv647pf5d77m1pd") (features (quote (("std" "codec/std" "sp-std/std" "sp-runtime/std" "frame-support/std" "frame-system/std") ("default" "std"))))))

