(define-module (crates-io su rp) #:use-module (crates-io))

(define-public crate-surplus-compiler-0.1 (crate (name "surplus-compiler") (vers "0.1.0") (deps (list (crate-dep (name "oxc") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "oxc_codegen") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1ffh9xhknfwglz2wn2s3rwi8p0hz1wjj2h7lp1128wp7wprr18wf")))

(define-public crate-surplus-compiler-0.2 (crate (name "surplus-compiler") (vers "0.2.0") (deps (list (crate-dep (name "oxc") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "oxc_codegen") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0hb8gg6ql8npn2v135n0fh7qwyq2cym0283m2hz83m9p3pkf1fx2")))

(define-public crate-surprise-me-0.1 (crate (name "surprise-me") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "surprise-me-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xb02yjpnd55ll8q718rpybrg54s53pghd1x5hnxsqg1xqk7acx5")))

(define-public crate-surprise-me-derive-0.1 (crate (name "surprise-me-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zfxhsbszdjsqswa61f3ak1xq2nxahpqqvyxsgfn2kz34xa6sghs")))

