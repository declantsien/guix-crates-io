(define-module (crates-io su id) #:use-module (crates-io))

(define-public crate-suid-1 (crate (name "suid") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1fkawjz23jqmmpalaifwpdyg16dd7zqxwhbrwaqfs0yk0vnfpfsz")))

