(define-module (crates-io su cc) #:use-module (crates-io))

(define-public crate-succ-0.1 (crate (name "succ") (vers "0.1.0") (hash "0dq1v4161jv1002xwpq9b4iz582hjdsrgcrzf3fmybd9frp2d933")))

(define-public crate-success-0.1 (crate (name "success") (vers "0.1.0") (hash "088asnn14j22aa8m1fjb3kngia0m2b9rhb41kkd0mj12cirhspk8")))

(define-public crate-succinct-0.0.1 (crate (name "succinct") (vers "0.0.1") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.3") (default-features #t) (kind 0)))) (hash "1h199jns3bjrd2p8lxgflhzxcb7z5wf033w2v4q35s7g7c6g3p7h")))

(define-public crate-succinct-0.1 (crate (name "succinct") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yybwa4zyy890ax5dkll9p3jn0gjs3babl2cc8cik2i4q06x3plb")))

(define-public crate-succinct-0.1 (crate (name "succinct") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p90kaczgij35q9lf20d90rrs0kw73mcz2xch7bya8rdr541l6v4")))

(define-public crate-succinct-0.2 (crate (name "succinct") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "08r9wnqb18v0a1vj32dbay61i92hxf4xh34ai3gv24vbxr36ab5q")))

(define-public crate-succinct-0.2 (crate (name "succinct") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ms3h7833qvj16kwb2gz5c6pmgw4nqayqgqiwjs045d5n7a4pwi6")))

(define-public crate-succinct-0.2 (crate (name "succinct") (vers "0.2.2") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1c4bbnyj3qlamk53d6ghn9zn1gpmnkxfhkgf3qbwbwrpb54kal5l")))

(define-public crate-succinct-0.3 (crate (name "succinct") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1kpw4jz1y82gbkrcvr4q7qqm5s1v5nahhr3rq987268cmqmj09ar")))

(define-public crate-succinct-0.3 (crate (name "succinct") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "130cssp12zqcc05pccgbyqj3s6vlyzlv0z4rs6plz1d8az4b920g")))

(define-public crate-succinct-0.3 (crate (name "succinct") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1mhrsr074i43y8sgb31g323ijj566kjjij72l6644csjah8fasgc")))

(define-public crate-succinct-0.4 (crate (name "succinct") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ix7rsipx3i1xw5n2c5348s5slysp3pphz26g7aknwgs72qlkdg0")))

(define-public crate-succinct-0.4 (crate (name "succinct") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "13ywvvzi8jbf2mbnbk1850glwhwbd58flxmrm2j8sx53a6djm7dp")))

(define-public crate-succinct-0.4 (crate (name "succinct") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1fibffidk7ivcvfhapp2f4x92fjvw9dvym071ix91xzc1wf19jsh")))

(define-public crate-succinct-0.4 (crate (name "succinct") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1flcjxjv947l739z9jrkya6fhp6g9sj4s8ngc37wq31di45g1q3q")))

(define-public crate-succinct-0.4 (crate (name "succinct") (vers "0.4.4") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.2") (default-features #t) (kind 2)))) (hash "1nk4q58msmbsqknyv0rsjz15axqyxcyknrigap6qcx840k2i976b")))

(define-public crate-succinct-0.5 (crate (name "succinct") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0xc0viyd5v2gk95k380npssaakr5x8vbjym367488cnc47i2cdws")))

(define-public crate-succinct-0.5 (crate (name "succinct") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "10zq6rn50b1q84lblk39qbav4p76lr91vjjia5gk4ihc0dvqfjmy")))

(define-public crate-succinct-0.5 (crate (name "succinct") (vers "0.5.2") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0654c9gq50x7djyf25zbzz3d2pc4x3z21wmjj3qbr6d9h4hbd63p")))

(define-public crate-succinct_rs-0.1 (crate (name "succinct_rs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "07myrxf0m0hldv6i430l7i7hkknf0sirc2nnvcf9w2k6a9mri0js")))

(define-public crate-succinct_rs-0.1 (crate (name "succinct_rs") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1dabf4z72jasw2qjn7cx3blz0kz96h1lgsz2csw4vf2bzgfa63xm")))

(define-public crate-succinct_rs-0.2 (crate (name "succinct_rs") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "17cv0zcr693aaff0ma1hrb1z7mznzkg2xg48cd3i1vsishikpx80")))

(define-public crate-succinct_rs-0.3 (crate (name "succinct_rs") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "015kfid9g775y0cx20si273485razk9snc0k6iny1150zp4v3q78")))

(define-public crate-succinct_rs-0.4 (crate (name "succinct_rs") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "070d7vfs5nxrylwsx7v0n0xqhnbbvph1fwjggpcxqgkfmmrqkc4x")))

(define-public crate-succinct_rs-0.5 (crate (name "succinct_rs") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "16j85n8ch443qhibaggkc4lwrz9jscmr36lqgplja9g42fxn8wj5")))

(define-public crate-succinct_rs-0.6 (crate (name "succinct_rs") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1aw008qp7gqaql18m96va56mcrbdzvfqyhbwb47x2z0sifdgfxl8")))

(define-public crate-succinct_rs-0.6 (crate (name "succinct_rs") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0l2i93lk2jgzzr8hz483sh51sc5j8gy14m9vvavkwjvkdnjrs03p")))

(define-public crate-succinct_vec-0.1 (crate (name "succinct_vec") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1d3j13yga78clzs4vrwp8qn210mjmbjlnkhvyxhbx4mxi1kg1v95")))

(define-public crate-succtree-0.5 (crate (name "succtree") (vers "0.5.0") (hash "0h20dqh1x2qqijw0zi6bf5w64l1l7xyjs7rqql59clm8bhdg5qhw")))

