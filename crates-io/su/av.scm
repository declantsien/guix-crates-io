(define-module (crates-io su av) #:use-module (crates-io))

(define-public crate-suave-0.1 (crate (name "suave") (vers "0.1.0") (hash "0qjnqwkhb0zm9xbpdy85s1g0l4x9yl08mxm43sq51xmxkljm5j7i") (rust-version "1.73.0")))

(define-public crate-suave-0.1 (crate (name "suave") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (default-features #t) (kind 0)))) (hash "1islxh4frzd39xmdwwfxnzpnrrsyd4fp0ri7wkj8bz61k0r9ff2k") (rust-version "1.73.0")))

(define-public crate-suave-0.1 (crate (name "suave") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "18szalpfcha9zy4kfy4vb6phz77llmjbn35x4ax5fvcg81dckgl3") (rust-version "1.73.0")))

(define-public crate-suave-0.1 (crate (name "suave") (vers "0.1.3") (deps (list (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "0zshqi3a7jcykg63mywjfh15i20n5yzd8vg2iv5bawihdvx8yq8k") (rust-version "1.73.0")))

(define-public crate-suave-0.1 (crate (name "suave") (vers "0.1.4") (deps (list (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs" "rt" "macros" "io-util" "time"))) (default-features #t) (kind 0)))) (hash "1lmj9a7s4ignh9iq8pznd55k3i17a4cblyxrz6kvkqggclw88mkv") (rust-version "1.73.0")))

(define-public crate-suave-0.1 (crate (name "suave") (vers "0.1.5") (deps (list (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs" "rt" "macros" "io-util" "time"))) (default-features #t) (kind 0)))) (hash "172kamvb7ijkqi3aaydlzcvqj3m3vkxz944gjz90wv35cv09w7lp") (rust-version "1.73.0")))

(define-public crate-suave-0.2 (crate (name "suave") (vers "0.2.0") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs" "rt" "macros" "io-util" "time"))) (default-features #t) (kind 0)))) (hash "1hswbhf23zld85zdh6flib68aicz78lqjxz90ljf130rn97hkl2c") (rust-version "1.73.0")))

(define-public crate-suave-0.2 (crate (name "suave") (vers "0.2.1") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs" "rt" "macros" "io-util" "time"))) (default-features #t) (kind 0)))) (hash "1v3523snc497l4b38cagkcjsjk08s2lh3rh1gpw1rf7irk0bmqlm") (rust-version "1.73.0")))

(define-public crate-suave-0.2 (crate (name "suave") (vers "0.2.2") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs" "rt" "macros" "io-util" "time"))) (default-features #t) (kind 0)))) (hash "0w6m3wr4gyw5yd9l7y674kljfgfvf3d6zcmw132b880yf8lin0ca") (rust-version "1.73.0")))

(define-public crate-suave-0.2 (crate (name "suave") (vers "0.2.3") (deps (list (crate-dep (name "arboard") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("fs" "rt" "macros" "io-util" "time"))) (default-features #t) (kind 0)))) (hash "1gdilny86bxsl4vsaf659dd6asc118y0kwjsydxgbf3mh7f5qzrd") (features (quote (("queue")))) (rust-version "1.73.0")))

