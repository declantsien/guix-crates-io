(define-module (crates-io su bk) #:use-module (crates-io))

(define-public crate-subkey-2 (crate (name "subkey") (vers "2.0.1") (deps (list (crate-dep (name "sc-cli") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1j05w3c8j5gyjvxp3iks6imvw6n9k248kz97apw54bnxgkik2plc") (yanked #t)))

(define-public crate-subkey-3 (crate (name "subkey") (vers "3.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "0qnmd70p2ys4mvbnrsgi06n9x21aaz1kslihxqq8hvizq9fxj42l")))

(define-public crate-subkey-4 (crate (name "subkey") (vers "4.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "0vl1d3jz104zmriwnln2w58jk7ghi0smvry0m1rqg43kcm9wlcca")))

(define-public crate-subkey-5 (crate (name "subkey") (vers "5.0.0-dev.1") (deps (list (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "=0.32.0-dev.1") (default-features #t) (kind 0)))) (hash "1b20887v12w6h8nbgn384ld4rgb1bj3nkizmyvb5j4l3m3lzlb9j")))

(define-public crate-subkey-5 (crate (name "subkey") (vers "5.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.32.0") (default-features #t) (kind 0)))) (hash "16hqk5w7yhsxzq3vvqcgj2f53aq2f6d75h5m4lr97vz59yp69s00")))

(define-public crate-subkey-6 (crate (name "subkey") (vers "6.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.33.0") (default-features #t) (kind 0)))) (hash "0xjjx9p6ln1mycbjhzdsyrxgdfksnhqwdvysi47apiqan2kq8fcd")))

(define-public crate-subkey-7 (crate (name "subkey") (vers "7.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.34.0") (default-features #t) (kind 0)))) (hash "0kdk8p1l1zh1w47g0s2irz8ivjs4yfq2ry9slnicfir80vr7m2bi")))

(define-public crate-subkey-8 (crate (name "subkey") (vers "8.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "0a8244xq1wf69mj6fkgp8l9yyv6nrn4vkbrj8c79y4n6bgqkdrl3")))

(define-public crate-subkey-9 (crate (name "subkey") (vers "9.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.36.0") (default-features #t) (kind 0)))) (hash "08d2nis3lckc0hr6vq06dfk6dhqx4vdbkc7psxbldckm1dkl90k9")))

(define-public crate-subkey-10 (crate (name "subkey") (vers "10.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.37.0") (default-features #t) (kind 0)))) (hash "00lm066p1v0np0ghljwv8vnw8d123fxbnfiw45zmw34f53y0nnmm")))

(define-public crate-subkey-11 (crate (name "subkey") (vers "11.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.38.0") (default-features #t) (kind 0)))) (hash "00513kq8r8ncn4hljq6kk4lar1cgh6bzqwgymhli8ygck42rk0i7")))

(define-public crate-subkey-12 (crate (name "subkey") (vers "12.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.39.0") (default-features #t) (kind 0)))) (hash "0gaflpdba0p7j996q2xi2fx2bhr6yv9kf3agfqa43nzhfc6jbbls")))

(define-public crate-subkey-13 (crate (name "subkey") (vers "13.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.40.0") (default-features #t) (kind 0)))) (hash "07q29da5py2jg3qm6w694xw2f48s5xp4nsbd0wkbmkivfbvs17vf")))

(define-public crate-subkey-14 (crate (name "subkey") (vers "14.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.41.0") (default-features #t) (kind 0)))) (hash "0rq1hidby8rlrb8634dsnbyrqsdhadk59aqmdid5vc9kr0gxqnh4")))

(define-public crate-subkey-15 (crate (name "subkey") (vers "15.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sc-cli") (req "^0.42.0") (default-features #t) (kind 0)))) (hash "1qy8kz8l747mx3yzr6m8c08rjmls7s2gviw16xgfx1jwpqbk2wxb")))

