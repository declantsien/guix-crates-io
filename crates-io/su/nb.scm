(define-module (crates-io su nb) #:use-module (crates-io))

(define-public crate-sunbeam-0.0.1 (crate (name "sunbeam") (vers "0.0.1-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.1-alpha") (default-features #t) (kind 0)))) (hash "1p89r7qlpkzkdf9c86qm18774i1hi8pymbp67nsnp44r5f5nywz8")))

(define-public crate-sunbeam-0.0.2 (crate (name "sunbeam") (vers "0.0.2-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.2-alpha") (default-features #t) (kind 0)))) (hash "0spmnxkx3dm8lz2908nwm57yfdyqg1ldlibrfjfiq6fxnsnhmf3v")))

(define-public crate-sunbeam-0.0.3 (crate (name "sunbeam") (vers "0.0.3-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.3-alpha") (default-features #t) (kind 0)))) (hash "0l6aqy9arc7zvxpwz6q5bza247bcn0s5s4bmsmj5djz71szjx8kc")))

(define-public crate-sunbeam-0.0.4 (crate (name "sunbeam") (vers "0.0.4-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.4-alpha") (default-features #t) (kind 0)))) (hash "1ibary8qf43zjwkrilrwxn5p4mivagb888c7z8y1nq01s39ki8q1")))

(define-public crate-sunbeam-0.0.5 (crate (name "sunbeam") (vers "0.0.5-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.5-alpha") (default-features #t) (kind 0)))) (hash "1rgkzdk2gjdxcghck9wj9kqxx0k3kyaqkagm1apxhqh9hmkydd39")))

(define-public crate-sunbeam-0.0.6 (crate (name "sunbeam") (vers "0.0.6-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.6-alpha") (default-features #t) (kind 0)))) (hash "02xlazj6qqj8canng5hwpq9ry0gcpbm3kq15gfwa3r2lby1b7c6c")))

(define-public crate-sunbeam-0.0.7 (crate (name "sunbeam") (vers "0.0.7-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.7-alpha") (default-features #t) (kind 0)))) (hash "0sqv6ywqshgwi7w54iisyidxpzj63bf650nqqdiprfzx9wjsbqj3")))

(define-public crate-sunbeam-0.0.8 (crate (name "sunbeam") (vers "0.0.8-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.8-alpha") (default-features #t) (kind 0)))) (hash "1s8pgx85ycwz25fg9ks38k8i29cwabl5qlrklji89bzhzkd34a1q")))

(define-public crate-sunbeam-0.0.9 (crate (name "sunbeam") (vers "0.0.9-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.9-alpha") (default-features #t) (kind 0)))) (hash "11ifla83n16b5pzx45c9y7lmw2fkga1rhfb0nwlkf97s3z9ig197")))

(define-public crate-sunbeam-0.0.10 (crate (name "sunbeam") (vers "0.0.10-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.10-alpha") (default-features #t) (kind 0)))) (hash "0gjdx0aaigncvpljr4wavl66cv3jzmk3r0xkbgyk5fgbzglxa1bc")))

(define-public crate-sunbeam-0.0.11 (crate (name "sunbeam") (vers "0.0.11-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.11-alpha") (default-features #t) (kind 0)))) (hash "14cirg304s9hsiz37pbbmkqd9waiwr2cnlk4bmnq1kbc2ba327aw")))

(define-public crate-sunbeam-0.0.12 (crate (name "sunbeam") (vers "0.0.12-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.12-alpha") (default-features #t) (kind 0)))) (hash "0834az4y70prr1lypcddpqr65wnwypr1m25dw2rc1wa4mpjxwjkk")))

(define-public crate-sunbeam-0.0.13 (crate (name "sunbeam") (vers "0.0.13-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.13-alpha") (default-features #t) (kind 0)))) (hash "0g7m25vxhnhm8919zdnvjn2rli3wqjc1nwlf9pdl24hq1l2dg6dp")))

(define-public crate-sunbeam-0.0.14 (crate (name "sunbeam") (vers "0.0.14-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.14-alpha") (default-features #t) (kind 0)))) (hash "14y8av7h25amvd453kf72av1ib292nyvklbr5yq2mbhxdlvb8qc0")))

(define-public crate-sunbeam-0.0.15 (crate (name "sunbeam") (vers "0.0.15-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.15-alpha") (default-features #t) (kind 0)))) (hash "0kn7yjkgc5w6hgh3xlz2wv0383c75kgkb52s2bc53awa625s2cy5")))

(define-public crate-sunbeam-0.0.16 (crate (name "sunbeam") (vers "0.0.16-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.16-alpha") (default-features #t) (kind 0)))) (hash "19b0h2r1g6i1idqi826aj5j2acszfdkrnh553vaisaxf6bwpxbd6")))

(define-public crate-sunbeam-0.0.17 (crate (name "sunbeam") (vers "0.0.17-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.17-alpha") (default-features #t) (kind 0)))) (hash "1knxcsfhdg1fvc4wpni8ir4v687i1fhly0v0z083agbdzgf78bj5")))

(define-public crate-sunbeam-0.0.18 (crate (name "sunbeam") (vers "0.0.18-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.18-alpha") (default-features #t) (kind 0)))) (hash "1rfnbzx0430s94x61x2niwi586j881r42xx2wxndb6lb0rlpvbqx")))

(define-public crate-sunbeam-0.0.19 (crate (name "sunbeam") (vers "0.0.19-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.19-alpha") (default-features #t) (kind 0)))) (hash "0g62apd163711m2dh3kw48w0nrzdj8wpi8ilhy4j99zf13y04f30")))

(define-public crate-sunbeam-0.0.20 (crate (name "sunbeam") (vers "0.0.20-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.20-alpha") (default-features #t) (kind 0)))) (hash "17my2q57b83dy3fwkbj202czp78km8xh7q83aqv3xd409v18gij9")))

(define-public crate-sunbeam-0.0.21 (crate (name "sunbeam") (vers "0.0.21-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.21-alpha") (default-features #t) (kind 0)))) (hash "03s5scs1jwvchqqf0ardv01ifh671xm258bfx1ds62yjlhxjbprl")))

(define-public crate-sunbeam-0.0.22 (crate (name "sunbeam") (vers "0.0.22-alpha") (deps (list (crate-dep (name "sunbeam-macro") (req "^0.0.22-alpha") (default-features #t) (kind 0)))) (hash "1w7cfr0kizqwqq8zzs7jk02433x5khqn1zv8746dp0nmavcz1p5c")))

(define-public crate-sunbeam-build-0.0.1 (crate (name "sunbeam-build") (vers "0.0.1-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.1-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0lcbaklhzkkvy7qxfk79aw8rdli3gs4mgwv51zvb89myqaym9xim") (features (quote (("serde" "sunbeam-ir/serde"))))))

(define-public crate-sunbeam-build-0.0.2 (crate (name "sunbeam-build") (vers "0.0.2-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.2-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "162bfs31i9dirpj7jkcpsnbd3x2af29jll522lsjpxq9h1b40ikh")))

(define-public crate-sunbeam-build-0.0.3 (crate (name "sunbeam-build") (vers "0.0.3-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.3-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0ibxskdnfpmi06jmlr0s1mz1b5hq2g6ww0rj798zzyfrlfwrzvc2")))

(define-public crate-sunbeam-build-0.0.4 (crate (name "sunbeam-build") (vers "0.0.4-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.4-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0j5h9l4kg82r2klkzn49xpa2agzgz7216220frf5ymwl64fl1zn2")))

(define-public crate-sunbeam-build-0.0.5 (crate (name "sunbeam-build") (vers "0.0.5-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.5-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ky1hgbiy3n7nsx1h54ddqj1k81bqrxp5f3y0dfp9a0hi8198ah9")))

(define-public crate-sunbeam-build-0.0.6 (crate (name "sunbeam-build") (vers "0.0.6-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.6-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1z31pn7ac0canyay5jpswl4smrpgw6f2hfr0d358m4yx1y7asqgh")))

(define-public crate-sunbeam-build-0.0.7 (crate (name "sunbeam-build") (vers "0.0.7-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.7-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "18dwp6yg29gay22f7q7v55v181gw6rbs9graz8jbjrad2kvsm6x8")))

(define-public crate-sunbeam-build-0.0.8 (crate (name "sunbeam-build") (vers "0.0.8-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.8-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1sgxcwdla992yvhpkmxj5bhcq58apflfyb1vpc1a16671w60h84n")))

(define-public crate-sunbeam-build-0.0.9 (crate (name "sunbeam-build") (vers "0.0.9-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.9-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0y9k0szk6f4d3i49ddirahfgj28kmqcq7fcik26ifbvazq01yacc")))

(define-public crate-sunbeam-build-0.0.10 (crate (name "sunbeam-build") (vers "0.0.10-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.10-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "03q6nfblnmbrxaq206f2qmaz0v7prp6jwbqwjd5w4chm0h24d8q1")))

(define-public crate-sunbeam-build-0.0.11 (crate (name "sunbeam-build") (vers "0.0.11-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.11-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0lwkhsl6rw1r6p1fh79vgrpvpg64gv5c6jf5fzjd471alhb78m03")))

(define-public crate-sunbeam-build-0.0.12 (crate (name "sunbeam-build") (vers "0.0.12-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.12-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1r3niih5zkbbarjxpmjrkpdk416ghzi1i33zm15kbqzvxxzbc5hw")))

(define-public crate-sunbeam-build-0.0.13 (crate (name "sunbeam-build") (vers "0.0.13-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.13-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1hv3nhsq6l8fvlbdm0ijaw040dh0mri1icqjrq45q79djivkjry3")))

(define-public crate-sunbeam-build-0.0.14 (crate (name "sunbeam-build") (vers "0.0.14-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.14-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1qmlwy0amlpsrrn19xv1wp5wm5i6b9k72g9fvdjiwj6lhkmbjfcm")))

(define-public crate-sunbeam-build-0.0.15 (crate (name "sunbeam-build") (vers "0.0.15-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.15-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "108ik0zag4rzcxjv30fyry26c21bq8lckxd0ampy7fj8g8mwy0d9")))

(define-public crate-sunbeam-build-0.0.16 (crate (name "sunbeam-build") (vers "0.0.16-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.16-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "18wllbwnvc03n91v5rhvb2izy7m8lzrby9g7raz90iazziz9xj6d")))

(define-public crate-sunbeam-build-0.0.17 (crate (name "sunbeam-build") (vers "0.0.17-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.17-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "10bz8vxak324blfybg1plip21xp9ls495yjd138g2bn4a397jwg4")))

(define-public crate-sunbeam-build-0.0.18 (crate (name "sunbeam-build") (vers "0.0.18-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.18-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1fl59bifs0cajgbkn1yyxqzdq213s6amlsxchdk5bwh4apzlhfd1")))

(define-public crate-sunbeam-build-0.0.19 (crate (name "sunbeam-build") (vers "0.0.19-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.19-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0s0xh42hgy1zvhqa37s12yza3lr5nxhxqg3bh8sm08c64h1bk4wv")))

(define-public crate-sunbeam-build-0.0.20 (crate (name "sunbeam-build") (vers "0.0.20-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.20-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0hrm02117zfklckp8psil2f28igpsb6r7dj3a9gbqrz8l6spnb7c")))

(define-public crate-sunbeam-build-0.0.21 (crate (name "sunbeam-build") (vers "0.0.21-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.21-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0m0f4rkshnammm989rpd4iir96qxi8ky4m42s1r4n80c52ip8g1c")))

(define-public crate-sunbeam-build-0.0.22 (crate (name "sunbeam-build") (vers "0.0.22-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sunbeam-ir") (req "^0.0.22-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1cwa7g4g9zkrahrh6spps3j4g7bwibskn201g2i962qa76dxhpim")))

(define-public crate-sunbeam-cli-0.0.1 (crate (name "sunbeam-cli") (vers "0.0.1-alpha") (hash "17j9pxjjm2rvxh249ffqmpx4za3bxqvs7fc1js0g2f4wgr9gq1b4")))

(define-public crate-sunbeam-cli-0.0.3 (crate (name "sunbeam-cli") (vers "0.0.3-alpha") (hash "01v5d1nhx8ayjw57qm93rfn4d9a3qf2wnc6lq9ggirlsla2canlr")))

(define-public crate-sunbeam-cli-0.0.4 (crate (name "sunbeam-cli") (vers "0.0.4-alpha") (hash "1462m9anls0k2n73qdhiabdfy7jk0bvz63m25vwkl83h3fnagidy")))

(define-public crate-sunbeam-cli-0.0.5 (crate (name "sunbeam-cli") (vers "0.0.5-alpha") (hash "1bkvlq6p5jipr3skczkmqj4dg1rb8nijjcgc8xpimxvagwic81rp")))

(define-public crate-sunbeam-cli-0.0.6 (crate (name "sunbeam-cli") (vers "0.0.6-alpha") (hash "0n548hhf3i7yi5gcvbn4i0x1z0ii9gvavs602v7q93giydy8mnd7")))

(define-public crate-sunbeam-cli-0.0.7 (crate (name "sunbeam-cli") (vers "0.0.7-alpha") (hash "07pgwimaf9pkn7r2vcsx9kpj0vdky9akvvinh6x4a2npy43gg4ry")))

(define-public crate-sunbeam-cli-0.0.8 (crate (name "sunbeam-cli") (vers "0.0.8-alpha") (hash "0n7dzqazzm3idzwijw980qqn0fa9fk3wnd46353jry5zhbn8v91b")))

(define-public crate-sunbeam-cli-0.0.9 (crate (name "sunbeam-cli") (vers "0.0.9-alpha") (hash "11zr5b496hihzrmw8ygiynp2srbykyn1y70x00zl6vj958c71gkw")))

(define-public crate-sunbeam-cli-0.0.10 (crate (name "sunbeam-cli") (vers "0.0.10-alpha") (hash "1158la24nx7lj85fskj99wsmbib3sdviw455qhjg7wbh9n8f3bgj")))

(define-public crate-sunbeam-cli-0.0.11 (crate (name "sunbeam-cli") (vers "0.0.11-alpha") (hash "1r494gj03p4g93s5av23iv6m7hhz8y77vjb0qwzkzgcxxsyng75g")))

(define-public crate-sunbeam-cli-0.0.12 (crate (name "sunbeam-cli") (vers "0.0.12-alpha") (hash "08hcz7dfbqflv3gav8ynxp47gsqbgppwdany6nd8dkm3hi7iig0r")))

(define-public crate-sunbeam-cli-0.0.13 (crate (name "sunbeam-cli") (vers "0.0.13-alpha") (hash "0d84p7dqy3awwfy862x5k6g52kr7p23yd0ssk0qpnafpdx30p3hw")))

(define-public crate-sunbeam-cli-0.0.14 (crate (name "sunbeam-cli") (vers "0.0.14-alpha") (hash "1nlifcri74fgyb1w6svvim8123vj7nw9sminqzgskzgiqpkw17s4")))

(define-public crate-sunbeam-cli-0.0.15 (crate (name "sunbeam-cli") (vers "0.0.15-alpha") (hash "15hj22q23bjvb9wl91w0y6jp1xhq0a0jjkydshdv463gjqvlmcsa")))

(define-public crate-sunbeam-cli-0.0.16 (crate (name "sunbeam-cli") (vers "0.0.16-alpha") (hash "1pnhglm7qmk1121rm30lixyd4dr2a214i707p63ap6srhl6ckcs1")))

(define-public crate-sunbeam-cli-0.0.17 (crate (name "sunbeam-cli") (vers "0.0.17-alpha") (hash "04mpnqfcpdf1ssx9ck4qb8kzchlfj5hikn8yd1axg27ypcnx6vyz")))

(define-public crate-sunbeam-cli-0.0.18 (crate (name "sunbeam-cli") (vers "0.0.18-alpha") (hash "0lmcpbjccmh14b2mqhjc7jn6mckfzi6a45x8k9w2w0aj2ci9bgzn")))

(define-public crate-sunbeam-cli-0.0.19 (crate (name "sunbeam-cli") (vers "0.0.19-alpha") (hash "0x8zpg4a363vw9ld7vrxbp3w3ll5dk7gf3gh1qfxi6wdiwq5028s")))

(define-public crate-sunbeam-cli-0.0.20 (crate (name "sunbeam-cli") (vers "0.0.20-alpha") (hash "1p7vgdvwzljgw6srx3v4ddvpdbq8gp3xggyvhj5f2bhy0sss15c0")))

(define-public crate-sunbeam-cli-0.0.21 (crate (name "sunbeam-cli") (vers "0.0.21-alpha") (hash "0zc15vgmnqq3hkvrsnqkwivzyylyj2q4mzzgi879kbnwgc3cb77x")))

(define-public crate-sunbeam-cli-0.0.22 (crate (name "sunbeam-cli") (vers "0.0.22-alpha") (hash "0y6zdd5gzqna6hqy9j1pi4gjppnd0kh98yvx8is910kc5j76a9hv")))

(define-public crate-sunbeam-ir-0.0.1 (crate (name "sunbeam-ir") (vers "0.0.1-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "08jx7wdfgr0q6v3npllh2mzjvyp9ac8phxf9v8wpcbqpgavh9sc9")))

(define-public crate-sunbeam-ir-0.0.2 (crate (name "sunbeam-ir") (vers "0.0.2-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0p34d74b4mrs6928zni9lzi81icbrc8wvdcmghfcyhqf38ljpg3g")))

(define-public crate-sunbeam-ir-0.0.3 (crate (name "sunbeam-ir") (vers "0.0.3-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1lwmvnn25g3hrin02yjqbgqpk9vvjwih1xnyas29i51haxblp7j7")))

(define-public crate-sunbeam-ir-0.0.4 (crate (name "sunbeam-ir") (vers "0.0.4-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "027hb0qjxyjs1ivmyyx4dh45j6x54x61df1ccn81midw3fjjf022")))

(define-public crate-sunbeam-ir-0.0.5 (crate (name "sunbeam-ir") (vers "0.0.5-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0yii85h6qhzn52p8y47jxm3lqzv4ky3wnag8mzqf0mq6qsk8gy1r")))

(define-public crate-sunbeam-ir-0.0.6 (crate (name "sunbeam-ir") (vers "0.0.6-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1jzj4d6hbywkn347fd7w9qkn5yadyz267l9li7yv1dmri92hvw78")))

(define-public crate-sunbeam-ir-0.0.7 (crate (name "sunbeam-ir") (vers "0.0.7-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1blkrb5v4gad1nk3x9jk1br2wpv4g6qpd0pbcqpz0hzsdl05kz96")))

(define-public crate-sunbeam-ir-0.0.8 (crate (name "sunbeam-ir") (vers "0.0.8-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1hjk38d8p0f210hm9knzfrdccg6rc3ksi66wwxflrgvaqk552c75")))

(define-public crate-sunbeam-ir-0.0.9 (crate (name "sunbeam-ir") (vers "0.0.9-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1sn7s0ig83vi4w3y51mwgnqcszzj54icy7bx1wlpc8skyv666dgr")))

(define-public crate-sunbeam-ir-0.0.10 (crate (name "sunbeam-ir") (vers "0.0.10-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1dh8h89a9s3n79z3lvga67rh4j1kflabmyhwzjfihd3c4bfcb0dy")))

(define-public crate-sunbeam-ir-0.0.11 (crate (name "sunbeam-ir") (vers "0.0.11-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "06kbzl0kr40zq84iwqw1cjrpmzgn4hpn3g80a8mcx3d275mfm8cv")))

(define-public crate-sunbeam-ir-0.0.12 (crate (name "sunbeam-ir") (vers "0.0.12-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1pc9pka0qwd7rwhz8spcy572i6qhhnrjfm2qqjjawkpzw726h8l0")))

(define-public crate-sunbeam-ir-0.0.13 (crate (name "sunbeam-ir") (vers "0.0.13-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0s8flics5bd1wpb7hxs0rywyqwm8xvj58pzyyyfl9qsi4kz63xda")))

(define-public crate-sunbeam-ir-0.0.14 (crate (name "sunbeam-ir") (vers "0.0.14-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0j5s2qqzqfplwcr23d3frkgb9cdkpfpjnjjacawrndd493xbly16")))

(define-public crate-sunbeam-ir-0.0.15 (crate (name "sunbeam-ir") (vers "0.0.15-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1dq9c1kniaxly0y0qk1j33df4xlmi6vflkiky7xzlaax9z9mdja2")))

(define-public crate-sunbeam-ir-0.0.16 (crate (name "sunbeam-ir") (vers "0.0.16-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0l4vggcvdn1z0kl22vvrd49fnc166gflgrg0b1c3bph0x39qc6v5")))

(define-public crate-sunbeam-ir-0.0.17 (crate (name "sunbeam-ir") (vers "0.0.17-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "166d691jw0arkgbnibf6k0mkc4lg763wfxvwgm3qrmijsipc43k6")))

(define-public crate-sunbeam-ir-0.0.18 (crate (name "sunbeam-ir") (vers "0.0.18-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "043bvhdwvk63bgkhhflh79bqwdmal5bryqj5pj0rpj6rfyv26c6s")))

(define-public crate-sunbeam-ir-0.0.19 (crate (name "sunbeam-ir") (vers "0.0.19-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0r1rp5hq37f2322qy15lc82ksbaqq8yr35ic74as66mjzsf4va78")))

(define-public crate-sunbeam-ir-0.0.20 (crate (name "sunbeam-ir") (vers "0.0.20-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0c0wqv5vv3cifvfak1blqzg70y1xayhk0m92g0anqccfxfd90fnm")))

(define-public crate-sunbeam-ir-0.0.21 (crate (name "sunbeam-ir") (vers "0.0.21-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "183dx4j07ap5yw097qwhddxkw8xqya6b8xwpcvbrglakb14mxhn2")))

(define-public crate-sunbeam-ir-0.0.22 (crate (name "sunbeam-ir") (vers "0.0.22-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "19848nx88q8wx9m8z1hm80a274g854msjifl2q0ywkpf086wbxbw")))

(define-public crate-sunbeam-macro-0.0.1 (crate (name "sunbeam-macro") (vers "0.0.1-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.1-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "05wqbmic0slfj441mr5ni9hq9j5309md76jnqxsa3vbp68i8aqkj")))

(define-public crate-sunbeam-macro-0.0.2 (crate (name "sunbeam-macro") (vers "0.0.2-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.2-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "057y1v7frnvyqdrrm8h3hzr7cnr5n4m28827pnsmpabyx5i3wixa")))

(define-public crate-sunbeam-macro-0.0.3 (crate (name "sunbeam-macro") (vers "0.0.3-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.3-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0zh7m1xz586y7kgp7q3ag7qs5dq3i1adx1kz42lw5vgrrjrzn801")))

(define-public crate-sunbeam-macro-0.0.4 (crate (name "sunbeam-macro") (vers "0.0.4-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.4-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "19k6q41q9vawqba51nhxhdm8b08s8wn8kb128sy29ig4gqy9lxsm")))

(define-public crate-sunbeam-macro-0.0.5 (crate (name "sunbeam-macro") (vers "0.0.5-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.5-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0gncv6c6v8i967jn6kq84wclw5wvrqil7lncag34m8nr0d4av4k0")))

(define-public crate-sunbeam-macro-0.0.6 (crate (name "sunbeam-macro") (vers "0.0.6-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.6-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1h6dj5ffrf5x89grkh889896wfc1550sbgcnx7m69b7j33bl5w5m")))

(define-public crate-sunbeam-macro-0.0.7 (crate (name "sunbeam-macro") (vers "0.0.7-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.7-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "03npr6kyh3rchx5spv8dvffdfdkkfqhlqwj7q04r7gzq3f3w23m4")))

(define-public crate-sunbeam-macro-0.0.8 (crate (name "sunbeam-macro") (vers "0.0.8-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.8-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "08lbhv7gzrs96lnkgl4igxif1963d0ql045yhhkzsxsqkzfm564r")))

(define-public crate-sunbeam-macro-0.0.9 (crate (name "sunbeam-macro") (vers "0.0.9-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.9-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1j69v227xzszs06x0hly6b5wvmygcqyh9angkz80rga1n8y9431w")))

(define-public crate-sunbeam-macro-0.0.10 (crate (name "sunbeam-macro") (vers "0.0.10-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.10-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1sxgdhk4rdplb0gxflb6nf93r8jqc6nqiwa76hhl1r9pdc6pk6rs")))

(define-public crate-sunbeam-macro-0.0.11 (crate (name "sunbeam-macro") (vers "0.0.11-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.11-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1bg8yfbi6m0xi68id1d75hyjr4yb3c6gd7h0bhakwic659gvmxnw")))

(define-public crate-sunbeam-macro-0.0.12 (crate (name "sunbeam-macro") (vers "0.0.12-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.12-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "12ff98zjd7vdl90g5zp3ki42ywx5lihbwvplbmjm26pnfjlh5a78")))

(define-public crate-sunbeam-macro-0.0.13 (crate (name "sunbeam-macro") (vers "0.0.13-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.13-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1xafncz8bcdyakql0kzzl0wiafc2p7akq7dadidnpbpxdb9x1ca5")))

(define-public crate-sunbeam-macro-0.0.14 (crate (name "sunbeam-macro") (vers "0.0.14-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.14-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1szc8jqg7z2zkdp2qz0dxh70r3xl6kkgqkk1s2rccp1vfd6kjbaq")))

(define-public crate-sunbeam-macro-0.0.15 (crate (name "sunbeam-macro") (vers "0.0.15-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.15-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "13yhq8mjizxl8xjb3gh80g049yb7grv96352jqsr454kv99wzwpz")))

(define-public crate-sunbeam-macro-0.0.16 (crate (name "sunbeam-macro") (vers "0.0.16-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.16-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1jsfly1cg9mjahjk6vjh6s8kyny8b57if68plpcvnc56g7ji8r7i")))

(define-public crate-sunbeam-macro-0.0.17 (crate (name "sunbeam-macro") (vers "0.0.17-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.17-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "14jlbwl2221n7r38xwlrggllz3qcpk6dgrwky02jfz5i9scrm6aw")))

(define-public crate-sunbeam-macro-0.0.18 (crate (name "sunbeam-macro") (vers "0.0.18-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.18-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0akv5hfh0r0frmlv69jsvn554lyx7b5i5ylkp1hj7p1bg1rawmrm")))

(define-public crate-sunbeam-macro-0.0.19 (crate (name "sunbeam-macro") (vers "0.0.19-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.19-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "185ixbssppccs8rwza8mfp87n9nhn7fxymmpjkfsh871mh1z71n3")))

(define-public crate-sunbeam-macro-0.0.20 (crate (name "sunbeam-macro") (vers "0.0.20-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.20-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1k0kr3z0iabjz3mlkzzm6bk8qm68rrh4g52gysk37n100g18bi47")))

(define-public crate-sunbeam-macro-0.0.21 (crate (name "sunbeam-macro") (vers "0.0.21-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.21-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1lhmqmx8n6hlqzkygs3sfzghjdrsh2kzj07j7d2pn9mlag2xs0x5")))

(define-public crate-sunbeam-macro-0.0.22 (crate (name "sunbeam-macro") (vers "0.0.22-alpha") (deps (list (crate-dep (name "sunbeam-ir") (req "^0.0.22-alpha") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1b604fb4ibz9qfvbrasfdirwa1l3yqgaazaah0jclp4angpnw1ly")))

(define-public crate-sunburn-0.0.0 (crate (name "sunburn") (vers "0.0.0-reserved") (hash "10rh6vndcqdmr9c07yz6gf8khga69hxv85jv5hwngam7rm5g8a7k")))

(define-public crate-sunburn-0.1 (crate (name "sunburn") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "solana-bpf-loader-program") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-logger") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-program-test") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-runtime") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-sdk") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "spl-associated-token-account") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "spl-token") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "15q86dzmqmka212l5xrh4n5a0f8p8qbqb8a3sazm88hrnq2hlprm")))

(define-public crate-sunburn-0.2 (crate (name "sunburn") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "solana-bpf-loader-program") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-client") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-logger") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-program-test") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-runtime") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-sdk") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "solana-transaction-status") (req "~1.10.10") (default-features #t) (kind 0)) (crate-dep (name "spl-associated-token-account") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "spl-token") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ncplxa457zykwywpxf38clbadb0s0yq912mxm3h2naz2dslxash")))

(define-public crate-sunburst-0.1 (crate (name "sunburst") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "noto-sans-mono-bitmap") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "1lwhrq74bg8w5xr985r21q60daffwl8s26dq71z542x63jd49g58")))

(define-public crate-sunburst-0.2 (crate (name "sunburst") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "noto-sans-mono-bitmap") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "0crsvn9xrzf7c4agy4v5iskpdqws81kzaidsgnx4csgrwy72calr")))

