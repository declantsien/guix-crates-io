(define-module (crates-io su n-) #:use-module (crates-io))

(define-public crate-sun-times-0.1 (crate (name "sun-times") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ii7zyp6haxnzcjxyg8ympi4m2qg2491xcxs9632vvg365q1fi84")))

(define-public crate-sun-times-0.1 (crate (name "sun-times") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0fh70ngddbymp80ms51b32dy2rpn85d75a741yxqqjqdbrzg8igf") (yanked #t)))

(define-public crate-sun-times-0.1 (crate (name "sun-times") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1cgf8dk9y91ipfkny1zajdv6arf5wqasdym9d81ywck40wjfmfha")))

(define-public crate-sun-times-0.2 (crate (name "sun-times") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 2)))) (hash "1j45lx0crf54pxx0j6mzaz27ls5hipajz1dlx9wlm2iqwiadcdnw")))

