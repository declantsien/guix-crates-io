(define-module (crates-io su nx) #:use-module (crates-io))

(define-public crate-sunxdcc-0.1 (crate (name "sunxdcc") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.0") (default-features #t) (kind 0)))) (hash "1q9292jfaydlzbxcv4bjnh3wiz7ywrwg3c52w26q1n6mrrg6jvj2")))

