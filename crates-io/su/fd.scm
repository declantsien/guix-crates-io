(define-module (crates-io su fd) #:use-module (crates-io))

(define-public crate-sufdb-0.1 (crate (name "sufdb") (vers "0.1.0") (deps (list (crate-dep (name "cbor") (req "*") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "0dxsacgd7g9frpyiw4calngf07ybzndwvbag24pqcrzs1vybpdcr")))

(define-public crate-sufdb-0.1 (crate (name "sufdb") (vers "0.1.1") (deps (list (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "0bjg03vlx7ia8r0ypnx7q5i81w0xm1j8jq6yzaalx6l4i5xjbby0")))

(define-public crate-sufdb-0.1 (crate (name "sufdb") (vers "0.1.2") (deps (list (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "0pzqxvdd96m1nqzbkmhzl61g52r7w135pnhasg4dyivwp1rbkr8c")))

(define-public crate-sufdb-0.1 (crate (name "sufdb") (vers "0.1.3") (deps (list (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "12vvz72nm0k2nh55s0xvf292n1aap51wb3jmnpds867zldwvqhkn")))

(define-public crate-sufdb-0.1 (crate (name "sufdb") (vers "0.1.4") (deps (list (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "0fs0r57sa3az0l9imvbnh4cmzmr6xarpy3cz0fksf32dwbvr643q")))

(define-public crate-sufdb-0.1 (crate (name "sufdb") (vers "0.1.5") (deps (list (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "03qjw5jlsb53qr30g7binkcb5cpwrqd6bi0x9fxl9hcwk25ghmwx")))

(define-public crate-sufdb-0.1 (crate (name "sufdb") (vers "0.1.7") (deps (list (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "156fi2gss13pgdy3c8013c4jnp2y6swp1fqjj4wxp9xi6ssh2vfi")))

