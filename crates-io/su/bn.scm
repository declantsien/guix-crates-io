(define-module (crates-io su bn) #:use-module (crates-io))

(define-public crate-subnet-0.1 (crate (name "subnet") (vers "0.1.0") (hash "1v2ssscxllarb73640m1x7bb235kkljcj4s5r5m11x1kdak0c5dk")))

(define-public crate-subnet-evm-0.0.0 (crate (name "subnet-evm") (vers "0.0.0") (deps (list (crate-dep (name "big-num-manager") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.139") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "0wbcqks03m21hs2kq77iwh3p25nk6p4w1c2lbm76sfb7kzxbq6rg") (rust-version "1.62")))

(define-public crate-subnet-garden-core-0.1 (crate (name "subnet-garden-core") (vers "0.1.0") (deps (list (crate-dep (name "cidr") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cidr-utils") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "grcov") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1xn4n62c0d15dixy303dk73chcnyghzrbsjjbscvjqj3pdry12xy")))

(define-public crate-subnet-garden-core-0.2 (crate (name "subnet-garden-core") (vers "0.2.0") (deps (list (crate-dep (name "cidr") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cidr-utils") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "grcov") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "13ry2jh8gwkyycd8fkcw644i3470qx7jhlfqwrfr4vag4y0688vh") (yanked #t)))

(define-public crate-subnet-garden-core-0.2 (crate (name "subnet-garden-core") (vers "0.2.1") (deps (list (crate-dep (name "cidr") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cidr-utils") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "grcov") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1dc6lm06q1cvs62qw4q58hm9dzjzlqdapyx3228m5448d4is013r")))

(define-public crate-subnet-garden-core-0.2 (crate (name "subnet-garden-core") (vers "0.2.2") (deps (list (crate-dep (name "cidr") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cidr-utils") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "grcov") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1nsg7qah31rwmpamfxr6l8c32qbhkhqh6fbvvgb5i3i2gwvsvp1j")))

(define-public crate-subnet-garden-core-0.3 (crate (name "subnet-garden-core") (vers "0.3.0") (deps (list (crate-dep (name "cidr") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cidr-utils") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "grcov") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "postcard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "10f57759asr4q60l0j69vsq3hsnwx5712fpyxl2i7gdq6nbq114y")))

(define-public crate-subnet_calculator-0.1 (crate (name "subnet_calculator") (vers "0.1.0") (hash "0khi91p4hd3jsnsy0qs5n5n2vjbsq5n25gvzvy7705n18jdmjgj4")))

(define-public crate-subnet_calculator-0.2 (crate (name "subnet_calculator") (vers "0.2.0") (hash "1v9xq0xl3g86c0y0lc2235f6c3q9dp27v25w9pl7lgdpkf3sj9q8")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.0") (hash "1pdm8ilzccmhgjswhlm5zphr44j3x0cvbx828lpc9c61k2h70d0p")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.1") (hash "0fs3hi8gvz2fx7w542ib5d0jhm157g3djs32plxgb8xfajrxx24q")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.2") (hash "06rjxyqwswah6y7d0pjv9fgvmd4lxghk8vvcjzy507fvfr0jc4hp")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.3") (hash "1drpdm0r1lkr1724saf139id2vd3ja6dbwnixl0r1625ipkg520p")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.4") (hash "0v19qs8brp4dsk66lgza5qpbl7i7k20niw3m2ypwn8x53ccczqgb")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.5") (hash "092k8nbrr2g17dswjxnz50psqylphdlirx5dwkpqaal29hbbk8gg")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.6") (hash "0wfbjwrrh3gs1ydcjm7fnx7madn2hvhlpm2g5fyln0jcd9a3n0zy")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.7") (hash "08f0m0gmyyriq97jwh1q95g3cna2j0wqllb7ldbp5qlqpi0cjr0f")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.8") (hash "1kh1x7jsq15cn8f9ivax5f8hqvjn1kpc1xqqi251qwq3zfd3kl1b")))

(define-public crate-subnetwork-0.1 (crate (name "subnetwork") (vers "0.1.9") (hash "0dgbvndzcq0wfp7y9iicnv1s7h7qs522qcybb4dd7x62vqy33nbv")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.0") (hash "1m2nb25d6gbhmy58cvwncmn3pphxfqg5vl6q4rafj97gd4hd6jps")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.1") (hash "14dg3hd46gqrp5i7v8wip1yk7cvv89aqvp3cgdpvv63bxllp65qf")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.2") (hash "041z3ijrnjcpnghn27h6drvs6hp4rss4q22znb6437ahvp81h1vf")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.3") (hash "1j7akrvap7038pcpbiv9nwhvgr6j623cbzypaxkxgzwanh0axwbk")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.4") (hash "1ix37mw9mcx0fz6gh8if34lg3gv6wyc9dq0rr8278nlyab1hz2f8")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.5") (hash "18z4cqwaqfwl5za9r5ijnyd6mha3g7qbxkk89b558i2zjh4987nj")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.6") (hash "15is98jksgwyv07yd7ga3slazyp7l2raclhig2l0r4xk831av3my")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.7") (hash "1gqa5g22x8vr6r8i1fmg594wfng3vh5s86jr8vqwfg8x2fj30bbz")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.8") (hash "0h0gipyakbamkvzl5plp3zrbyk0p7j2ff33ibgiw5vj438wnx0ap")))

(define-public crate-subnetwork-0.2 (crate (name "subnetwork") (vers "0.2.9") (hash "1yr25518bagdinpvwhaqga1y7gqv860ghs1q4d7qc5qvpq8r5gi9")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.0") (hash "0bwdxjprfbss1xsgbjqv5g6drbz7gpkq4z4izb231mdynhvdxkh1")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.1") (hash "00a0566wnf403k2y6fpjzn7ni4hn52b53f3r71gcga0xl9vkz1fa")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.2") (hash "0yp8kp154z8ri3fxpdg52szscfpsps50d187hlz1f9spnaij7aq2")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.3") (hash "1bvbvjscvnkxb10hv4fc53db32d3vpj8fjv20p4f0fcrbkpgm1p0")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.4") (hash "09m1k4xyk8mf75y2bnds1izlbnx16c4i6a18ks2dazinyfb2lwbx")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.5") (hash "1by0ny811sqvy8296bb5jga6yrpr5nr4i2xxa4cx8hpcscqwrnad")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.6") (hash "1amw15g2fm9mqb0bnb6m3x0mj40vq22jpzjx2s9vh8i8s2vb6pia")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.7") (hash "0b5r9f6xg0zz6ca5w2mzm6z35w6djxw8fzzjk74hihy5mai935cy")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.8") (hash "0xynhbqfjhcgiw9i6vizr6kan7r1wwhjw9ys53clqc53ll5c4hh1")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.9") (hash "0m5wdhdar0w05i3hbqzzsk22mkrrf2jfqd4ibfjr23a8y63cy2w6")))

(define-public crate-subnetwork-0.3 (crate (name "subnetwork") (vers "0.3.10") (hash "0avkrysph9pcbpx4hfq8bpfij5k2asv69wwpv8zf22977w6g7kvp")))

(define-public crate-subnetwork-0.4 (crate (name "subnetwork") (vers "0.4.0") (hash "1bhrh4j1zyr7wi5mb0rnxx5ppf35ahrllsjc3yghps2k31mw2gk6")))

(define-public crate-subnetwork-0.4 (crate (name "subnetwork") (vers "0.4.1") (hash "1v3yzad7qcw5wkxwpj0sviw5r06161qr21kzga6j3dgjw2ipza0h")))

