(define-module (crates-io su n_) #:use-module (crates-io))

(define-public crate-sun_rpc-0.1 (crate (name "sun_rpc") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xdr") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "xdr_extras") (req "^0.1") (default-features #t) (kind 0)))) (hash "1r0k4ngarx0mrgaiwybc2w8p9241sbq0j9816bq26gzw2z261c7b")))

(define-public crate-sun_rpc_client-0.1 (crate (name "sun_rpc_client") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde-xdr") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "sun_rpc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vm_test_fixture") (req "^0.1") (default-features #t) (kind 2)))) (hash "07hvsdlf9yb98l1979ppyh0pl2wdv65n5nhp3kpsr3ayza2cs5jr")))

