(define-module (crates-io su bo) #:use-module (crates-io))

(define-public crate-subor-rs-0.1 (crate (name "subor-rs") (vers "0.1.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thrift") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.2") (default-features #t) (kind 0)))) (hash "05y6ghr3pw4wbwgnzv5jxdqy046qaxpb9947s2mfn207g55c1in8")))

(define-public crate-subor-rs-0.9 (crate (name "subor-rs") (vers "0.9.2") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thrift") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fdnqv0vymr6lbf56cy90i87in94385ay1lw36jkwd1anzsnzp2i")))

(define-public crate-subor-rs-0.9 (crate (name "subor-rs") (vers "0.9.3") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thrift") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x94703p0s8dlrvi9fv47ldvdg01lkyiix63x9hlfhinx283gi4m")))

(define-public crate-suborbital-0.0.1 (crate (name "suborbital") (vers "0.0.1") (deps (list (crate-dep (name "cargo_toml") (req ">=0.6.3, <0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">=1.0.0, <2.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req ">=0.1.9, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req ">=0.5.6, <0.6.0") (default-features #t) (kind 0)))) (hash "18bk9b96njqj0fjmc3jyds87v5skv801p8764mn3fwf814nl3swm")))

(define-public crate-suborbital-0.2 (crate (name "suborbital") (vers "0.2.2") (deps (list (crate-dep (name "cargo_toml") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0cn9d3crh4b3zpw8jlcnfn942f60x5qby67b970a1q3kmlr6j9gl")))

(define-public crate-suborbital-0.2 (crate (name "suborbital") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0wgpmxyacgvvhk3c5fr57qywkgyn9pqxn98a9h9spjr5vpk1ccvm")))

(define-public crate-suborbital-0.2 (crate (name "suborbital") (vers "0.2.5") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1c1cibxpnv3irwk4a2zszp8xfjwcwhbgxanpn95qbc8sqnszcsvk")))

(define-public crate-suborbital-0.3 (crate (name "suborbital") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0pdax1s7j4g5ai5nac8i6d5army9hsra3krxnasw03j0rw3hbyk3")))

(define-public crate-suborbital-0.4 (crate (name "suborbital") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0v9z58xrzxnar6h1xls0m3dhcrq6i5d6bpbfpmmr5xfjdbs6pw7f")))

(define-public crate-suborbital-0.4 (crate (name "suborbital") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1l66pjqdb358h1n3h852lp6xd3vzwa0pg6c6k685prq0130qjz7k")))

(define-public crate-suborbital-0.4 (crate (name "suborbital") (vers "0.4.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "03n8f47xmrjkqca68fw59y9jp1c682gvar5anffp5jix7gdz2s9b")))

(define-public crate-suborbital-0.4 (crate (name "suborbital") (vers "0.4.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0gbrsw5233ap4i0v87pj8sziimaa6z0qlc4qizpgr564n9bcjg0b")))

(define-public crate-suborbital-0.5 (crate (name "suborbital") (vers "0.5.0") (hash "1i0prcsp3qn2dfzlnb5rjh8xkwb5ygwqpry8afj67saj5jmr681p")))

(define-public crate-suborbital-0.6 (crate (name "suborbital") (vers "0.6.0") (hash "1j7hvl8xa8ympsiyph2k3sz0nph0x0f3s3mr0sp73ziks950b5bh")))

(define-public crate-suborbital-0.6 (crate (name "suborbital") (vers "0.6.3") (hash "06dxhg129qib17q7s6rxlpp6pg0lvf86c0ppg56dywws56gvqbf3")))

(define-public crate-suborbital-0.8 (crate (name "suborbital") (vers "0.8.0") (hash "00175k5w1wbj7j6iaz1mzvviswpsglm3p3p2z8jxffgqywagvp7f")))

(define-public crate-suborbital-0.8 (crate (name "suborbital") (vers "0.8.1") (hash "1a43c2wwf79wyjvcbdvpin2wfnbm1cfzrksd9gkanpaxn1i7s8qb")))

(define-public crate-suborbital-0.8 (crate (name "suborbital") (vers "0.8.2") (hash "1wmi76y28cvxdf5m4i2i6vw8ihwq27p2g5sqwnrnmzrrp4455r28")))

(define-public crate-suborbital-0.9 (crate (name "suborbital") (vers "0.9.0") (hash "17pygni8lk2q07bl3lgn9vbj8idfdasbzwy1lcvi6l90ar9544l8")))

(define-public crate-suborbital-0.9 (crate (name "suborbital") (vers "0.9.1") (hash "12r0i81n7mp47bx89hnm4i34w2ckvgiq6n7zf1qz9s1zw6h0pf83")))

(define-public crate-suborbital-0.9 (crate (name "suborbital") (vers "0.9.2") (hash "1rzjbpls3d3skfbf4af6pkhkjbkx9vx5an6rh8rn030nmrgz7w9f")))

(define-public crate-suborbital-0.10 (crate (name "suborbital") (vers "0.10.0") (hash "168bzylncnyxpg7qi43dlxdagvm6d9daj6brqzs64k3f8bx0927k")))

(define-public crate-suborbital-0.11 (crate (name "suborbital") (vers "0.11.0") (hash "09ldpbdc3vilj21b76dmz8hay0pyimv063ss5jbhjzjml50l609g")))

(define-public crate-suborbital-0.11 (crate (name "suborbital") (vers "0.11.1") (hash "1g27ssfq1qi0c108lxacll8rzkw1sxw5lnfs4kvcx9jqln689nb3")))

(define-public crate-suborbital-0.12 (crate (name "suborbital") (vers "0.12.0") (hash "1585nvxmnfvglqd6y6qiq6w47k96zk6z9jaynql5dkkj5vjzimgq")))

(define-public crate-suborbital-0.13 (crate (name "suborbital") (vers "0.13.0") (deps (list (crate-dep (name "suborbital-macro") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1vcpf1cmyxd6pgymvmax8z3kf1d01qhg0bmz75fflnwrfa44mjs5")))

(define-public crate-suborbital-0.13 (crate (name "suborbital") (vers "0.13.1") (deps (list (crate-dep (name "suborbital-macro") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "10031kxsimb0hka2mrj7fl06c12p7k7pb8ri2aqjxwm19vvvq57m")))

(define-public crate-suborbital-0.14 (crate (name "suborbital") (vers "0.14.0") (deps (list (crate-dep (name "suborbital-macro") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0p93pyb66v7ckpd0zcgm7kbp7mq7rcip1kqa8d8g7dw0znjms19a")))

(define-public crate-suborbital-0.15 (crate (name "suborbital") (vers "0.15.0") (deps (list (crate-dep (name "suborbital-macro") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "02q7h2znw51zk3sja4yzniy47q5dmny25byimbimaq6limi944ib")))

(define-public crate-suborbital-0.15 (crate (name "suborbital") (vers "0.15.1") (deps (list (crate-dep (name "suborbital-macro") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "0kd7mgzpzxk8khbif3g9bidxnw6n5gl8bdf9bg2kz9kp3mwyglsx")))

(define-public crate-suborbital-0.16 (crate (name "suborbital") (vers "0.16.0") (deps (list (crate-dep (name "suborbital-macro") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1lq2rj0zhkzhwwwc8v4bi1zyhxa5y6msjbbxdxfgmmydc9xmbh0m")))

(define-public crate-suborbital-0.16 (crate (name "suborbital") (vers "0.16.1") (deps (list (crate-dep (name "suborbital-macro") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "1nq324657h50qagv5yip4smqf96rk2lcl07f301wjmz19i3j3fqm")))

(define-public crate-suborbital-0.16 (crate (name "suborbital") (vers "0.16.2") (deps (list (crate-dep (name "suborbital-macro") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "1rb13c2yhbdpjdqgi0rrd6p353dpb0m7kma1zr4as15s083sv1l5")))

(define-public crate-suborbital-0.16 (crate (name "suborbital") (vers "0.16.3") (deps (list (crate-dep (name "suborbital-macro") (req "^0.16.3") (default-features #t) (kind 0)))) (hash "1r6j2ff4qd34nbwdq08rkr36nhhgqcaayn0gqv48bfjipbgd58c6")))

(define-public crate-suborbital-macro-0.13 (crate (name "suborbital-macro") (vers "0.13.0") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0pcg7hf8mmmlrq169dp9pzf3ynpxm9q3k3h6f1h968dijxyi217c")))

(define-public crate-suborbital-macro-0.13 (crate (name "suborbital-macro") (vers "0.13.1") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0i0vlnbyf9b2iawb530l0b43dwj5fc0196g32h6nd093c1k0n1fr")))

(define-public crate-suborbital-macro-0.14 (crate (name "suborbital-macro") (vers "0.14.0") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0451gassfzxl0zd3mq9g221cmpa3i9wvhpyxabl7vq2wwrhkycpr")))

(define-public crate-suborbital-macro-0.15 (crate (name "suborbital-macro") (vers "0.15.0") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "1d6bmh63c3f1qfs8qqqh6dcwwqjn74cqwr7mjxqh3hiildlvg99m")))

(define-public crate-suborbital-macro-0.15 (crate (name "suborbital-macro") (vers "0.15.1") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0y4ray0rpw9w3gdjaj1w7hrp6wd917yg30v77llp6wnq7vr68j27")))

(define-public crate-suborbital-macro-0.16 (crate (name "suborbital-macro") (vers "0.16.0") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0fn7ykmjdnli7kyab09dmivgsgy61fb53nzpb20bfzyqzbd2jvnq")))

(define-public crate-suborbital-macro-0.16 (crate (name "suborbital-macro") (vers "0.16.1") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0jhwi3j1dhqa32g9gwj5a4gqk0ci7nrayf8331rv7a6acjlfrc71")))

(define-public crate-suborbital-macro-0.16 (crate (name "suborbital-macro") (vers "0.16.2") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0jr7q10v7w6mgbxlnxn92kincrfzr1r9yfzsh437xpsz9idy4pqh")))

(define-public crate-suborbital-macro-0.16 (crate (name "suborbital-macro") (vers "0.16.3") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0v6j36jpl27vnw585561p662535whmqpq9lslwx40kdx668j2nff")))

(define-public crate-subotai-1 (crate (name "subotai") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "bus") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "0.*") (default-features #t) (kind 0)))) (hash "09vvqrlhwb64p9mrvr8vrvgfi7wfz815gdz1r4b8d6qrih4kw49c")))

(define-public crate-subotai-1 (crate (name "subotai") (vers "1.0.1") (deps (list (crate-dep (name "bincode") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "bus") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "0.*") (default-features #t) (kind 0)))) (hash "0jdlfhzp8jwcca7mda4y3z1gbwf1b77mpxfhjqr0z5i0jgx3pagq")))

(define-public crate-subotai-1 (crate (name "subotai") (vers "1.0.2") (deps (list (crate-dep (name "bincode") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "bus") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "0.*") (default-features #t) (kind 0)))) (hash "0514zyc207cp5jzbm590wkk7cfxavd3ffl6ri617cjhm1j7kf0yx")))

(define-public crate-subotai-1 (crate (name "subotai") (vers "1.0.3") (deps (list (crate-dep (name "bincode") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "bus") (req "1.*") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "0.*") (default-features #t) (kind 0)))) (hash "09kxsikiirqad5pg6w7rd0ac06xx5shdygqzy71v6hdfsqs2l6ym")))

