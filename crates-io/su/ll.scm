(define-module (crates-io su ll) #:use-module (crates-io))

(define-public crate-sully_graph-0.1 (crate (name "sully_graph") (vers "0.1.0") (hash "0gpbxski6vw8xk66wbhbinla0n51036x1n8gykxpif714y8yjzvz")))

(define-public crate-sully_input-0.1 (crate (name "sully_input") (vers "0.1.0") (hash "094x2449rrvdxc10w1dyd9313rgiqyb537dscxgw8qkj33z7c74n")))

(define-public crate-sully_input-0.1 (crate (name "sully_input") (vers "0.1.1") (hash "1pl6sj87zrk8pnksmcmgvpd5a9v067iy5svvyklqip7j42rlw96v")))

(define-public crate-sully_input-0.1 (crate (name "sully_input") (vers "0.1.11") (hash "0n7jcblhbcs8mccvnn2dzl5lwmisbf66qs62h0m86dvpdrahimaf")))

(define-public crate-sully_input-0.1 (crate (name "sully_input") (vers "0.1.12") (hash "0606hkpqrc577ndq8qrhld49ffrlm0rzscgbi55v5s5said56lqj")))

(define-public crate-sully_input-0.1 (crate (name "sully_input") (vers "0.1.13") (hash "14vgjs9yxwfwv5d5km0md13hsr1p4frh4l7sq5kih2kyykl9rmij")))

(define-public crate-sully_peg-0.1 (crate (name "sully_peg") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sully_graph") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "parsing" "full"))) (default-features #t) (kind 0)))) (hash "14xzinxrk4bch6zhl0fivy3jrw8k61pp5j0xkwivlvclhmfsbgi9")))

(define-public crate-sully_peg-0.1 (crate (name "sully_peg") (vers "0.1.11") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sully_graph") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sully_input") (req "^0.1.13") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "parsing" "full"))) (default-features #t) (kind 0)))) (hash "0glz1abgqcsavffrvf68pjxz2mc024fj9hgxcb2hhs12ib2drh3k")))

