(define-module (crates-io su su) #:use-module (crates-io))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "1w8w1r4hvifh2pcsv12z2f6wvi9xng63k4mak75yp8jajskpv5p5")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190409163015") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "1y6nfdxdfvr4c183vbr797b52yzwl72cyv3bd53fp4dswn347axn")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190409173253") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "0h5dgp93llhqfapi5dxk4c50c4p1d57jr23m27r10aii9fv9pgbn")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190409214629") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "0wfdrh3s8g77ri3v2kb1ykgiscyab4bqycv9f2slr9g5nvi6dxpd")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190409215312") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "1ikn9h5jpv3q3gs8sdi5kjn3mjp3cawqghhi8i8v6bv8khdnzbws")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190409225553") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "05dy8jizv2bs7j948srhlbqypwih90npcw8agg7lxf8hpidkfn2a")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190509101552") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0xk0xm666mhgzzjvch9lp81l51h3fgzpggszjmaizqnzlbxpvhqz")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190509145508") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0qcg952a50njr9ygjcprbrsglv7zbpycbf9rr1sik4sniq65gzgh")))

(define-public crate-susu-0.1 (crate (name "susu") (vers "0.1.20190509190436") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0vr8l31a2apvr258pr6vxagngag0z3hm9pqhr7j2ad3a6ayd0krp")))

(define-public crate-susudb-1 (crate (name "susudb") (vers "1.0.0") (deps (list (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0ncx5vw390bkfkc5dfnw6z0b30lxjbwj2pi107fw991wcl48ppw1") (yanked #t)))

(define-public crate-susudb-1 (crate (name "susudb") (vers "1.0.20190609230450") (deps (list (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0l1bjv5iph8cqpkn0y6s8csav8ij0idbf31d95fwvmk104p6awcp")))

(define-public crate-susudb-1 (crate (name "susudb") (vers "1.0.20190709015154") (deps (list (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "10732kl60slari6kgpym6iay64m57apjg51yfgg1aghb9rvmkr4n")))

(define-public crate-susudb-1 (crate (name "susudb") (vers "1.0.20191009134440") (deps (list (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0vjzg770z379qkm0nms5bs4if6a888gi3a7zpycjsf2ganklxffa")))

(define-public crate-susudb-1 (crate (name "susudb") (vers "1.0.20191309225933") (deps (list (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0dh8k4g8zw6x1z7w2qqdl8whqv5922fkinz084i516i4abxpz71y")))

(define-public crate-susurrus-0.1 (crate (name "susurrus") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "*") (default-features #t) (kind 0)))) (hash "05h720igi03whj9g353f6cfv3wwcgqnzphqr7krkdzrz64384707")))

