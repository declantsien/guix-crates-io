(define-module (crates-io su gi) #:use-module (crates-io))

(define-public crate-sugiura-hiromichi_dot-0.1 (crate (name "sugiura-hiromichi_dot") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sugiura-hiromichi_mylibrary") (req "^1") (default-features #t) (kind 0)))) (hash "0v6pz35y5bbbyia7a0dmdlmwgx0m8s7wfx8z8s5xj8rc40hy36p1")))

(define-public crate-sugiura-hiromichi_gc-0.2 (crate (name "sugiura-hiromichi_gc") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sugiura-hiromichi_mylibrary") (req "^1") (default-features #t) (kind 0)))) (hash "1lp7k00svpxl1vairlgq79fz30g1x2yq9ajssp0grrjqvhf8rpww")))

(define-public crate-sugiura-hiromichi_mylibrary-1 (crate (name "sugiura-hiromichi_mylibrary") (vers "1.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yq5j0cxsj5ghlsmjf5fcjmxym42rdm1aq2riyhyggm56lqrv4jd")))

(define-public crate-sugiura-hiromichi_mylibrary-1 (crate (name "sugiura-hiromichi_mylibrary") (vers "1.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "06ldaq41cvm04cvrj1nzbkhlh1fdiajz4x4dva1z1vz4nf9fnr2i")))

(define-public crate-sugiura-hiromichi_tp-0.2 (crate (name "sugiura-hiromichi_tp") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sugiura-hiromichi_mylibrary") (req "^1") (default-features #t) (kind 0)))) (hash "10dmhwxbzrmd0p9qmw2j0h1ngpw0f8rh81lbvqy4pg5999pwr46n")))

