(define-module (crates-io su lt) #:use-module (crates-io))

(define-public crate-sultan_function-0.1 (crate (name "sultan_function") (vers "0.1.0") (hash "0pfjygf6r3mhl8z9gj5q35h6brg6vq4q8xz5qpx53axcmspxjilk")))

(define-public crate-sultan_function_2-0.1 (crate (name "sultan_function_2") (vers "0.1.0") (hash "08dj9qwnic6kmwpcyl41hjsqcbx84592z2mqdy47vzs556bhy718")))

