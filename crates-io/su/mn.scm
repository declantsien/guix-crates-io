(define-module (crates-io su mn) #:use-module (crates-io))

(define-public crate-sumno-0.1 (crate (name "sumno") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "065kfsml8wdx1p6fc7mcm8szghbq8mim2m5rvbcw7q541q4dknhf")))

