(define-module (crates-io su rt) #:use-module (crates-io))

(define-public crate-surt-0.1 (crate (name "surt") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wn83r10bpk45i4d1184fvmcvfhgqz45gy2vza43v44i1dmlr1ys")))

(define-public crate-surt-rs-0.1 (crate (name "surt-rs") (vers "0.1.0") (deps (list (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "01n85y2rdc5g4cf6pyyifr46rq6x40yg9bcz8qdaylaz8dhxqbiy")))

(define-public crate-surt-rs-0.1 (crate (name "surt-rs") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "url-escape") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1xrcg3piqc5z0l1x7315b7i3qdvqfi2hhq205hhlfm9lgawf1fc4")))

