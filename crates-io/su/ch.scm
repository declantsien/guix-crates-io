(define-module (crates-io su ch) #:use-module (crates-io))

(define-public crate-suchbar-0.5 (crate (name "suchbar") (vers "0.5.0") (deps (list (crate-dep (name "permeable") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "timewarp") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "05zrqcgcpca4aak335f28rhjiirfgsjqq4zcljgc471pb0pw3v07")))

(define-public crate-suchbar-1 (crate (name "suchbar") (vers "1.0.0") (deps (list (crate-dep (name "permeable") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "timewarp") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1gcmx852mvzh4qrab6f97k6lyq96j4i9agh25h65iic7p6c214vx")))

