(define-module (crates-io su s-) #:use-module (crates-io))

(define-public crate-sus-impls-0.1 (crate (name "sus-impls") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)))) (hash "0anzc070ws7kb01g0wl9fdiizxzxrk93i922si3w3cvr0y8an5q3")))

(define-public crate-sus-impls-0.2 (crate (name "sus-impls") (vers "0.2.0") (deps (list (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)))) (hash "18kc3wrp6r3xqr3dh1l6iwv7z6p2ywsls6hblq91h0nw6ahqfrp0")))

(define-public crate-sus-impls-0.2 (crate (name "sus-impls") (vers "0.2.1") (deps (list (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)))) (hash "03xxjgydhvcjqckc5fipfnpp8j0kc8y19rs13q2wcpzs97x4dyvx")))

(define-public crate-sus-rs-0.1 (crate (name "sus-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zb2w160brwrdi2k8jcqgq008pvk1swjbrpkwvrpirhjv4l16bsy")))

