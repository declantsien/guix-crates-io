(define-module (crates-io su gg) #:use-module (crates-io))

(define-public crate-suggest-0.4 (crate (name "suggest") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0nb6axl4i58g7k0q3p3bg6m363aw6qnqdg31y5c8b43x6bbd0n15")))

(define-public crate-suggest-0.5 (crate (name "suggest") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.2") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ag8zz53fp54w9jzpc4m7diw1xcw9l74afa6371hf2f3r33bcdyz")))

(define-public crate-suggest-0.5 (crate (name "suggest") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1f4zjv9n12bcwp94dl76150rcg45kry3i7gsb56bxz1hbzny182f")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.4") (default-features #t) (kind 0)))) (hash "17gk3h5kfs264rzw5jvqdp5ww2979bjb58nmrbygm5li9m1rldbv")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pdcr3g7irh1w6d3qwrax3l4dn390jcp7hsqikgwnv45a7abal16")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.4") (default-features #t) (kind 0)))) (hash "0q83ligjy3jq4hbwlyjzssp0isxan4633dxpzqc8rr3lhwmb0j0x")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.4") (default-features #t) (kind 0)))) (hash "063yyr7qpyjfq8qsg0k6s4vrprf53lx36x8g7507sgzg9akfxras")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.4") (default-features #t) (kind 0)))) (hash "0srm07hqy2nxnj6rml7a4dl472jvcdc2qbpwix7cfl62ls8ah068")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.4") (default-features #t) (kind 0)))) (hash "1vr64jigbqxfqh93j9hcrb2mkzbmfz4j8v8c2b5z854k82g7lr6d")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ivwl9vkyikm2y3x5f2cc95hkvdzqsw97fblv6kf8qik4xmb5mdd")))

(define-public crate-suggest-command-not-found-0.1 (crate (name "suggest-command-not-found") (vers "0.1.7") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "suggest") (req "^0.5") (default-features #t) (kind 0)))) (hash "0625948015zdb2vmhm9zgza0k7lz01cch3pm1chv7drpw7ifazjc")))

(define-public crate-suggestion-0.1 (crate (name "suggestion") (vers "0.1.0") (deps (list (crate-dep (name "lev_distance") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jz5asd940v1przagki65d23bzxkhkdzq4jh9whbwq3ngch3clq1") (yanked #t)))

(define-public crate-suggestion-0.1 (crate (name "suggestion") (vers "0.1.1") (deps (list (crate-dep (name "lev_distance") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "177vpg4mvvmg4aacv558iipvwjqhb3xyp2y6l553dv5nvlv0b98z") (yanked #t)))

(define-public crate-suggestion-0.1 (crate (name "suggestion") (vers "0.1.2") (deps (list (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zqsabda6ya6aqzd9iw7jjvfrs83758wvkj0i0nc0j9ancjp89np") (yanked #t)))

(define-public crate-suggestion-0.2 (crate (name "suggestion") (vers "0.2.0") (deps (list (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "112pr82qjxwvxy8bzcnfa12gl701sdwnv6s3nmnx37z1ib18srbk") (yanked #t)))

(define-public crate-suggestion-0.3 (crate (name "suggestion") (vers "0.3.0") (deps (list (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1qrn644ccp2p2yk7k4c2jcfry3k9r2pn3p3zhqgmm9gy269ndmk4") (yanked #t)))

(define-public crate-suggestion-0.3 (crate (name "suggestion") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ql6954krrz581d5p8dkcvy7p5p8fgjznd3qs1n4ishafjvjk2d1") (yanked #t)))

(define-public crate-suggestion-0.3 (crate (name "suggestion") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09z90kzyfs6wd5j38wr1nz3x323nkvrsbndgz8nhpi410ym8qsny") (yanked #t)))

(define-public crate-suggestion-0.3 (crate (name "suggestion") (vers "0.3.4") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lev_distance") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gqdz2llj58bx420i1rlg9fx9qs655w5p7bz9xzpbblr2z27fb23") (yanked #t)))

(define-public crate-suggestion-cli-0.2 (crate (name "suggestion-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "suggestion") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1dws4jqn6dq5m9azz2xsmy1wj8i9whq2525030qmcrwkmgk8hszh") (yanked #t)))

(define-public crate-suggestion-cli-0.3 (crate (name "suggestion-cli") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "suggestion") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1dmcw4rfx5k8sdk766c4jz20p9n61b89ndh7k4kzw0v2842myfc2") (yanked #t)))

(define-public crate-suggestion-cli-0.3 (crate (name "suggestion-cli") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "suggestion") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "13fp2vz4d0g4bwk4razgz6dksyzlx4viz94w9k2m0bwkm0jaaci9") (yanked #t)))

(define-public crate-suggestion_trie-0.1 (crate (name "suggestion_trie") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "19f4wm8qxl000bzgc44xrmfmks8f3bnv239dlz92gd42n4fi09yz")))

(define-public crate-suggestion_trie-0.1 (crate (name "suggestion_trie") (vers "0.1.1") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1aagjkipj6xixxk1pr2x7fp2zdv36v8dx58zzdd7hfrbmc5sa0f7")))

(define-public crate-suggestion_trie-0.1 (crate (name "suggestion_trie") (vers "0.1.2") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "01yvkg0j37q0f2413dzlnskh8vgv0f0x6bbrmj1cv7s8nbyc9l0w")))

(define-public crate-suggestion_trie-0.1 (crate (name "suggestion_trie") (vers "0.1.3") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1jwqaxi0fhsksmihnxyligas9c02vlsxkyr2kic68y8l3j65xvya")))

(define-public crate-suggestion_trie-0.1 (crate (name "suggestion_trie") (vers "0.1.4") (deps (list (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "04xjnarg2p71h2f79xlzk86wpjlndnvff261spg4xk195v92x5ja")))

(define-public crate-suggestions-0.1 (crate (name "suggestions") (vers "0.1.0") (deps (list (crate-dep (name "strsim") (req "^0.10") (default-features #t) (kind 0)))) (hash "16z9qp9qsxrhwyszvr4p47iw1hgym2yhzp1983nxxb8mvq89mwaf") (yanked #t)))

(define-public crate-suggestions-0.1 (crate (name "suggestions") (vers "0.1.1") (deps (list (crate-dep (name "strsim") (req "^0.10") (default-features #t) (kind 0)))) (hash "100yy52b3i6zym5fk7l7dqj321994k6wpa9y5jnalj9f921c6hal")))

