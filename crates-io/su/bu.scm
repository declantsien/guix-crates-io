(define-module (crates-io su bu) #:use-module (crates-io))

(define-public crate-subunit-0.2 (crate (name "subunit") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3") (default-features #t) (kind 0)))) (hash "0sv83d9h0h6f86fpycnx8kjgnm2k7lh6lvrlvvvnk46hr09bhwiq")))

(define-public crate-subunit-rust-0.1 (crate (name "subunit-rust") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1x6g671wvwabxn8j8djg1dfg5xdrigmach0yp5izas7wvcv3p024")))

(define-public crate-subunit-rust-0.1 (crate (name "subunit-rust") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "17b19v4l321jp2nz0rqizqy9c9cbp97clfq1i80fw5sd4wi31g02")))

(define-public crate-subunit-rust-0.1 (crate (name "subunit-rust") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1p3cyfzgb66acpbwymik3378r482w8v944p89pnn4s0adwm4l8ic")))

(define-public crate-subunit-rust-0.1 (crate (name "subunit-rust") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12ms0g8xyc6vdhnlx4x6agcrnybn2r0wf143n9bmb4014jxlxh8d")))

(define-public crate-subunit-rust-0.2 (crate (name "subunit-rust") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3") (default-features #t) (kind 0)))) (hash "1rqmrjq1ls4v5hndfcjpyj9y26vqnbn0vb2xz232kz86zr4vz36r")))

(define-public crate-subup-0.0.0 (crate (name "subup") (vers "0.0.0") (hash "187wbx1wz41jwa3znvh447gif4zavjgcjy1db35a4gdb3sbbxc14")))

