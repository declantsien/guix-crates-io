(define-module (crates-io su nc) #:use-module (crates-io))

(define-public crate-suncalc-0.3 (crate (name "suncalc") (vers "0.3.0") (hash "0sg6755bvjpgm3hmhw4jcqxvs0ad9k8w905611v7pyf1k21ixzzi")))

(define-public crate-suncalc-0.3 (crate (name "suncalc") (vers "0.3.1") (hash "1nv4lil98jzqh0vzsc8kdi2jnv5fb6wyvyqbysnb34j67l7la1m4")))

(define-public crate-suncalc-0.4 (crate (name "suncalc") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "0ic0wvl11mgphx589rnpwmc3wk015ri64i2imn9zlbgxjv2l8343")))

