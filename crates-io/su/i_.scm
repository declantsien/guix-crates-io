(define-module (crates-io su i_) #:use-module (crates-io))

(define-public crate-sui_authority-0.0.1 (crate (name "sui_authority") (vers "0.0.1") (hash "120vzz47l6g06fwkpgw2amrqzn3i3w1xz5mc1vs87xr0djlymnc4")))

(define-public crate-sui_cli-0.0.1 (crate (name "sui_cli") (vers "0.0.1") (hash "18hdgwx3lxzvxrzr8a5vmcmyz953f18jfrc697pvi71w7whrg6hh")))

(define-public crate-sui_client-0.0.1 (crate (name "sui_client") (vers "0.0.1") (hash "18kayp3px51m603vf8cqmsbkw4gmjn79zad9kxixk18s9cjmk78p")))

(define-public crate-sui_core-0.0.1 (crate (name "sui_core") (vers "0.0.1") (hash "19a0kb52as553ykwywdiqzhwqcmjhsl7xka1nhgrbw9y0zi5z85c")))

(define-public crate-sui_explorer-0.0.1 (crate (name "sui_explorer") (vers "0.0.1") (hash "13qv25c8qjl1cz6fkfizphzv83fl13qhb45xnyg76nswgnna0qgk")))

(define-public crate-sui_gateway-0.0.1 (crate (name "sui_gateway") (vers "0.0.1") (hash "1wdplfvv7prsxn91dpgaij3kamh4kzcyp686f0kdbzi1ylq0h612")))

(define-public crate-sui_move-0.0.1 (crate (name "sui_move") (vers "0.0.1") (hash "1w5mgl393s4i7hy3i2ffxws91six40cw0safpv79fbz8hcf3hc50")))

(define-public crate-sui_network-0.0.1 (crate (name "sui_network") (vers "0.0.1") (hash "0mkdj53lvqgah2wnwpgbzs4vb3imdhhbwsva5acxpajaz88z41p7")))

(define-public crate-sui_node-0.0.1 (crate (name "sui_node") (vers "0.0.1") (hash "02f5m8ipix2wmh7b90b839lxlkxnl8z4yp42n6mygbggqw9mshdz")))

(define-public crate-sui_prover-0.0.1 (crate (name "sui_prover") (vers "0.0.1") (hash "1c5ncwcxxkvp416rsvalgrlv6kc198bxj1nyfv4rzwr99ni8a5g4")))

(define-public crate-sui_replica-0.0.1 (crate (name "sui_replica") (vers "0.0.1") (hash "0w61mq55vw274db7dizhfglzhijmxbgg3l02k0bv19118hxavl4r")))

(define-public crate-sui_storage-0.0.1 (crate (name "sui_storage") (vers "0.0.1") (hash "16hmr0nfnx2kl4b01w3kfc1j09982x5za9m3v40k77j9r2ld2xm5")))

(define-public crate-sui_test-0.0.1 (crate (name "sui_test") (vers "0.0.1") (hash "0rpyzr56h7krnjxqkhyrky6i6fc4c29wda3c2haglqi9xcf81dhr")))

(define-public crate-sui_types-0.0.1 (crate (name "sui_types") (vers "0.0.1") (hash "0a22z8yrq7if8jvi1rds1gz24kmygwjrjghqq323h4z7xp6fs67q")))

(define-public crate-sui_validator-0.0.1 (crate (name "sui_validator") (vers "0.0.1") (hash "1v4q5b9zy6xxhvy79l2nmaisnqshnypahk1qib2rayfkvqmm65y3")))

(define-public crate-sui_verifier-0.0.1 (crate (name "sui_verifier") (vers "0.0.1") (hash "115m9psdmgvnr63prardi9bndr8zjgbb0pg7dg0kbn3bgcp5hg35")))

(define-public crate-sui_vm-0.0.1 (crate (name "sui_vm") (vers "0.0.1") (hash "0b9z6708qxswzp7gmrawpri6xkw3pig87q0n1mcipss0bsqq9x92")))

(define-public crate-sui_wallet-0.0.1 (crate (name "sui_wallet") (vers "0.0.1") (hash "0s5hvrf287fwgnnh9w5i0vhns103jr0hzj2iblrq2bfg531slq7i")))

