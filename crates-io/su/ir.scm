(define-module (crates-io su ir) #:use-module (crates-io))

(define-public crate-suiron-rust-0.1 (crate (name "suiron-rust") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thread_timer") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0hms79705mc2gjm9x3sva33nf1nk577hxwrzvvja52n71yh0ls2k")))

