(define-module (crates-io su si) #:use-module (crates-io))

(define-public crate-susie-0.1 (crate (name "susie") (vers "0.1.0") (deps (list (crate-dep (name "cranelift") (req "^0.50.0") (default-features #t) (kind 0)) (crate-dep (name "scryer-prolog") (req "^0.8.115") (default-features #t) (kind 0)))) (hash "1k775v7lws2ggbcqc9k3jcx554jslrd7jq0bfxcvsdfjqvq9nirv")))

