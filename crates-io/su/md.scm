(define-module (crates-io su md) #:use-module (crates-io))

(define-public crate-sumdir-0.1 (crate (name "sumdir") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0zhxcgc9khxl5kv9s1xd0l7rk0bdwld254vm42lgicbw4fq4c0xx")))

(define-public crate-sumdir-0.2 (crate (name "sumdir") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0rsjgbi14ckzkibzsnfflwshxikd1i3pgmqk2frlmyfqn4a3937v")))

