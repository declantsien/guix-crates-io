(define-module (crates-io su bf) #:use-module (crates-io))

(define-public crate-subfilter-0.1 (crate (name "subfilter") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "subparse") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (kind 0)))) (hash "1k0n1kfdjzmw0w7bmfmdpz3dxh79l9pqf4c8bkqnz8byhx1hq6i1")))

(define-public crate-subfilter-0.2 (crate (name "subfilter") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "subparse") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (kind 0)))) (hash "14436vph7rg9sq6zh8k5n9a5csww7ahr2hig1lmkrfi3hqa1ha3s")))

(define-public crate-subfilter-0.3 (crate (name "subfilter") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "subparse") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (kind 0)))) (hash "1fk6wsf6qypw7v7yxm2m0r1z27bpfb2dwgrv22nym3w9fmpl8irc")))

