(define-module (crates-io su ma) #:use-module (crates-io))

(define-public crate-suma-0.0.1 (crate (name "suma") (vers "0.0.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "subgraph-matching") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1aycdgrr9knpsf56zwg1rdxfgx65831flhwkq2fj9bzjrzpjb0bp")))

(define-public crate-suma-0.0.2 (crate (name "suma") (vers "0.0.2") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "subgraph-matching") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "11zhczij51lf90rwfs1rcs5fmv00rj2a5vlmy3n2ac5342mjkypr")))

(define-public crate-suma-0.0.3 (crate (name "suma") (vers "0.0.3") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "subgraph-matching") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1f2vd62i3pb2mpyghph72k7yzmbwia9n2ssrf7g64s7iki27yd07")))

(define-public crate-sumatra-pdf-0.1 (crate (name "sumatra-pdf") (vers "0.1.0") (hash "1xr694m0ccn7gcqdzdh38x7lhq79abkvpb27i7kzhnp5as2rbyc9")))

