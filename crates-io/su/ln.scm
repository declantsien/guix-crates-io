(define-module (crates-io su ln) #:use-module (crates-io))

(define-public crate-suln-0.1 (crate (name "suln") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1miz5d4mzrhpv1xjxzx2nc15fz85c4b6z0lryi9i6azycczkdamd")))

(define-public crate-suln-0.1 (crate (name "suln") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1k462qdqccmh841jgifk652bs113kbsgp7i9yrhbmwr6wiyryxnj")))

(define-public crate-suln-0.1 (crate (name "suln") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1hpqd571xzynm0vzhsrmrsc316bs0wa3h3wd729398d1m16wfirp")))

