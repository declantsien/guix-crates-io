(define-module (crates-io su lf) #:use-module (crates-io))

(define-public crate-sulfa-0.1 (crate (name "sulfa") (vers "0.1.0") (hash "0lllpjihi38iz12dwqwcjac3lycfag4wh1nz28p11a61gagzz39p")))

(define-public crate-sulfa-0.1 (crate (name "sulfa") (vers "0.1.1") (hash "1yv2rrglhcmhiv12ngbk69hmycgl3bkvzh3vwk43zzgg12dd9x7z")))

(define-public crate-sulfa-0.0.0 (crate (name "sulfa") (vers "0.0.0") (hash "12g8bmg7rb7kz3ax1siyqyip3yp3z81p7q511lp50228ck0qdmdf") (yanked #t)))

(define-public crate-sulfur-0.1 (crate (name "sulfur") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.5.13") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 2)) (crate-dep (name "hyper") (req "^0.12.13") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.11") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.1.9") (default-features #t) (kind 2)))) (hash "1zf032p93ni0nrc3c0x4r9pln1m8slbapwx4p4wjsk4v304d632h")))

