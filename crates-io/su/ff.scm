(define-module (crates-io su ff) #:use-module (crates-io))

(define-public crate-suff_collections-0.1 (crate (name "suff_collections") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1a2axd72032hncg6hmyzxyk9lyjsp8fb6l072pccnig1jfg89dw0")))

(define-public crate-suff_collections-0.1 (crate (name "suff_collections") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "12crgy4isp0v1i26z9nj8dgy857p0vbirfkbys01nb7i1y6508w7")))

(define-public crate-suff_collections-0.1 (crate (name "suff_collections") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1riwq83hg3fw1bm2y2wj17ksphg6r2airivxx8q8f6rabh7imhaj")))

(define-public crate-suff_collections-1 (crate (name "suff_collections") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "0a0xx853zhx5asm0bq6xabp1xcdmxh53lg11dfyn2fiqir1yp386")))

(define-public crate-suff_collections-1 (crate (name "suff_collections") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1gklw3n5w3nmjk95dsw977zdrb7qivjgnmmfksd5mymkg3mnhf11")))

(define-public crate-suff_collections-1 (crate (name "suff_collections") (vers "1.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "0q5zph7ywm36blkar9iw2gsav3pxaqq2xfm8dcy0mvjl19pf1lbg")))

(define-public crate-suff_collections-1 (crate (name "suff_collections") (vers "1.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1awfn3brz80ajdd16dgjpjmlpqzzw1r1p6l9hwkqsp30fb3pxnl5")))

(define-public crate-suff_collections-1 (crate (name "suff_collections") (vers "1.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "121mr8kiy1xap4g746hi4347qdrxnr61zgn2j1m6a219a0j485wx")))

(define-public crate-suff_collections-2 (crate (name "suff_collections") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "00h6ligh6873pwd4fq4yhwgkff7f1r611k7rvz4smvsvfk3fxarn")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.1") (hash "028hs7r2wjfsxp4sm4jz1m1dnd69krsvv92jmwc5s56p8wvz3zmi")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "153l89zpm7ixy19wqyg8ay7vqi79f4p6dl7ifr0jk2hs76pyvzya")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "05w1f6vmk4gv96zy69ya7cyvd8imqk2kfb6wra9k58d714p9fjbf")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.4") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "06ci51sd9bl6bc2gkbnrhklzs6afj44vm6c97shabyzyhxha56hf")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.5") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0fy1ck468pvby4gafcmgz8da7vwyj5lay51ybhqf02kvr5idh4a6")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.6") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0bxf2r3bq00xhl2xq5886nkjzm520x1jchx71mfw6k55kk358n7x")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.7") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0hd8m870hbqyxh1x8sq5qmbn01zql0iyjj2rj450205mvi1fws64")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.8") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "19fvzmr3qj3cxihlh1rcfz1q78ay6ahy6b96x12wdgzdx5mgd2dq")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.9") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "15y405lzda88wc6bscafajjgidag577xgabdf6wr23i4kkw5jpmf")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.10") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "17ysabvlakjlilpvcragl1c6jn6mkgc3ha894d6kphawycqdibgj")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.11") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1q615rfr1qn1nhg0n25aq9i1y9145pzvdkdc24phv0lz4q1jgscj")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.12") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0h2gv2hmf9vmylx3wnn3nwqf35dc03swxih7hvfsb12jc1phgl2z")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.13") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "03mv71ann2grcs7lpxf2wcdib2mjifas7bpjvzh6hnzsn97j283j")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.14") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0wbbrkd8jsibap8hz76rwlqrn2sjljpk83r8ng7rbqxj96cxm147")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.15") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1izhnf2xw93kyqn4k6q5l4f0na9gnz6a9n9czqvk3mikvq72jjls")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.16") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "12awwj1p37xh2lwfhj16xh4zwani5zascy01bf310v04k3kfj1z2")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.17") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0xab5hnxahkv3xyxdldswk1zf50y9pyf8akjhd9lr0i5wgr6qjch")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.18") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "19xm5lyzasbg8f1zznarpznrmzkpzaj4prk9hmbhjm46zia68rcy")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.19") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1s2ahgl150rm6ph5hl9z4nxysr0ab654p4psy8pkhc3n5081jl38")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.20") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1lq77gz72n8zr0h167r03i62rdj1d77fzn8zpzf54k23iqkr7zk3")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.21") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "12l3i6ydp5iag6cri7rg5h38d0cykkj9jsb3amnqgsbk27r6zqws")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.22") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1lm6fjxnxzdfj2xqpbmyjdb1qn4slzlakqvmzv6ab364r4d8qdsm")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.25") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0rfsq0qlgdhc2x4c08fqyxh4iqq8d8ygm630vhjv97nn8b7gij1q")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.26") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0g4fmbi36xwgryyvs0cx2k14am8inc5vbbxrz0b9n8mqf0lax6qm")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.27") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0hsk7bbmrjn821li15slbwlkppbnnsgmlw5myr09a0y99as2ijm6")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.28") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1zf2ax5sbsipwsjfhisnl6dhrbwqq5lr0837356qbswpjlgsy7zw")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.29") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "19sdsnibb0jxg5k0px85vwvk03lkykjwlsz8915yc3wvq211nckg")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.30") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "11h5wqmj3b7i7xffwcx7pkh1in8jyz3qfaz3l3p31s9gqsh7f0r9")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.31") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0lcwd84mrysnl3krf5qhqw1aflywz87p6kdj06n9g5mg8pg0vyw3")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.32") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0mc1wnyqgziywgagdg43hdhzlg8a3850yh2c87pb5qw7iscnqmld")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.33") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "18fr2j9nrla16p6ydd5v6i7y5kjmpkxvyfm44f8fqlw0pk1j82s5")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.34") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "12vabl5dd33v3ajknrqfdvs5qcshv9i53nrfl1jgi5ddb6fix98g")))

(define-public crate-suffix-0.1 (crate (name "suffix") (vers "0.1.35") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1vy741l18bcn16abkmd275v84da4dkk6fbx0v5cg2cvhpal93k8l")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0hbh0bj7m7bjjaz0mg334jv1rafg74l134x41k0s3rdq442bxn2j")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1wx3aas5270c6llx0chd7r6iyifx7i13csa67lp0bpjl67wfr27v")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0idzkwk3709shd2j5zlfs5nbl8r30jy52n70lihszmwgpynhqswq")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.3") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1zwjppzg20c3f1xa1lg5g5hi8x0dwhdwc03sdwqfxg117alvrf7i")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.4") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1r83ivbjhk7s7q25hbz1i3f05yyv8x70m42ppr239ys70jmi2svn")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.5") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1rl2xfqd63law4s2ignbzlmmcpr07zhjj7hkg5xrkkysnyyp2wr4")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.6") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1ykbdiy4hmc1k14d5y78g9ipqpwrvlvyn9xkmqaw2ma05pdzah6s")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.7") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0m1hnskdjk80vbwpsrjisjd85nnc01n4vf00746w225g4rkbbjs6")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.8") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0j4pxxw79mzf033nflim9gxql8c112a38dh34x907x4x7y2szwlf")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.9") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0y2hjbn4vq8pvw5pza3ab80xllbjfp6vzjwnb76hjbhnf8pgcy1y")))

(define-public crate-suffix-0.2 (crate (name "suffix") (vers "0.2.10") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "134fb3sl3337ij3v04lbgy1mn865bhgywr2hja1xr40mgaii3ipf")))

(define-public crate-suffix-0.3 (crate (name "suffix") (vers "0.3.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1zy6sdn0my0jbzzab8iiqqaii49hqapv0s7lrnb1bsvqcx231xhs")))

(define-public crate-suffix-0.3 (crate (name "suffix") (vers "0.3.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "118ji98bl9ic85ajadfyxq12rxq2rx15m29bd1v3k3xyhfd8cny8")))

(define-public crate-suffix-0.3 (crate (name "suffix") (vers "0.3.2") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0la4qxaf8hinz4ypg14kjangz381crgfwls4mh7lb5zj051ff83f")))

(define-public crate-suffix-0.4 (crate (name "suffix") (vers "0.4.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "005qvgs4m4b0yyf7kryxc663gcrjkhyiz8y6wr1s8gbi1h1rfndi")))

(define-public crate-suffix-0.4 (crate (name "suffix") (vers "0.4.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "00c0xch8bx9w6922kwj39agdaqdykrazysll3m1bw16zpdzf0jrh")))

(define-public crate-suffix-1 (crate (name "suffix") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0fgr6i01l86nn1q6zhgc4hvkx84xg80prbmg71j814g184sb8w04")))

(define-public crate-suffix-1 (crate (name "suffix") (vers "1.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (kind 2)))) (hash "08fk2x8ykbgib0fb5qxzra7mdrp2lzspbwjdd343nmydxiqq1ksv")))

(define-public crate-suffix-1 (crate (name "suffix") (vers "1.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "0k91a8ng2sdis638v2kkq3myyjwg13mzzikyxf1k822s0ks4nb45")))

(define-public crate-suffix-1 (crate (name "suffix") (vers "1.3.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "16a10mhkcqv1pykwbbjgiyxr56s90390wsf6v454jrjbp2wk91w8")))

(define-public crate-suffix-array-rust-0.1 (crate (name "suffix-array-rust") (vers "0.1.0") (hash "0nmb1nkkb7f0wwyyl9pkfijvkvw1r9r1jgh56nzxj0mnjcsziqbf")))

(define-public crate-suffix-rs-0.1 (crate (name "suffix-rs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1nl7g3xj27pqsk81fk28025ha5qqjv7gdnlrxc6ggs1nzc4a9nj9")))

(define-public crate-suffix_array-0.1 (crate (name "suffix_array") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "10drwvv6z7xk63l9f05v0nciyjjgv41y5ji5imsbahzhv34ixy1q")))

(define-public crate-suffix_array-0.1 (crate (name "suffix_array") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0pplvwfz33appk6kh578y228gs39i6csra68q5qvfrpyr5wvqjqm")))

(define-public crate-suffix_array-0.1 (crate (name "suffix_array") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0x8165yqirrkjnl4g9i607qdjw61xqb0g735bf9yhn3khls1b817")))

(define-public crate-suffix_array-0.2 (crate (name "suffix_array") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0jglahfqncfsngni0mx1w72drrd2466ycgka457g918khj8w79lh")))

(define-public crate-suffix_array-0.3 (crate (name "suffix_array") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 2)))) (hash "15wdwhj8808z2haj46j754zans02gvfxnczzgnr8cy7nb0nwswix")))

(define-public crate-suffix_array-0.3 (crate (name "suffix_array") (vers "0.3.1") (deps (list (crate-dep (name "bincode") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitpacking") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1p5ak0b4wp50wxbfgv65pbq5hm80g6y2i3h7rhh2jkxzqzablflb") (features (quote (("pack" "bitpacking" "serde" "bincode") ("default" "rayon"))))))

(define-public crate-suffix_array-0.4 (crate (name "suffix_array") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitpacking") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ki1cfc624snrv5fgfvabgrqs965gvpjksafs09xvq12v4pg4495") (features (quote (("parallel" "rayon") ("pack" "bitpacking" "serde" "bincode") ("default" "parallel" "pack"))))))

(define-public crate-suffix_array-0.4 (crate (name "suffix_array") (vers "0.4.1") (deps (list (crate-dep (name "bincode") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitpacking") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cdivsufsort") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wx2g70713bqvw9mq7xv5kc5b1bl8gqjpncmqk66xq0yq6bjysjs") (features (quote (("pack" "bitpacking" "serde" "bincode") ("default")))) (yanked #t)))

(define-public crate-suffix_array-0.4 (crate (name "suffix_array") (vers "0.4.2") (deps (list (crate-dep (name "bincode") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitpacking") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cdivsufsort") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0z4gps1hh93zqkvs8c4vc17yz0mbfdcgjxjqdiraa1xibxiif1xm") (features (quote (("pack" "bitpacking" "serde" "bincode") ("default")))) (yanked #t)))

(define-public crate-suffix_array-0.5 (crate (name "suffix_array") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitpacking") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cdivsufsort") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0gzcp626d62x6b9pl64y6mbqi2d70mhqz0f7syky68kscflrqzch") (features (quote (("pack" "bitpacking" "serde" "bincode") ("default"))))))

(define-public crate-suffix_cmd-0.1 (crate (name "suffix_cmd") (vers "0.1.33") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "= 0.2.13") (default-features #t) (kind 0)) (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "09zgkjd9l34c4rkpybbyvkam1ndspfx9xq75x72sbgkr8br0w9z4")))

(define-public crate-suffix_tree-0.1 (crate (name "suffix_tree") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "1sdddphzaj5ppwph14aqj7h5m85wy2bi12d0cnsg0b2ipx8npka4")))

(define-public crate-suffix_tree-0.2 (crate (name "suffix_tree") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "suffix") (req "*") (default-features #t) (kind 0)))) (hash "06si9rfffybhlisv2yfwh74h63qwg6fra49j2k4jpsbzvz1faymj")))

(define-public crate-suffix_tree-0.2 (crate (name "suffix_tree") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "suffix") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pzd3vw1ihfm5qiblcml0ccpibq2hmh7ja42jbn092ph5f2vmfsf")))

(define-public crate-suffix_tree-0.2 (crate (name "suffix_tree") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "suffix") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x3i56f8xwqplcf7h2pzzcyf6jscxyhwp2mlzq1pk0cggr1dnvl2")))

(define-public crate-suffix_trie-0.1 (crate (name "suffix_trie") (vers "0.1.0") (hash "09psar4sqd467n5bh8jxln1nkndckmlch11mb0iq6m4mw3f354jm")))

