(define-module (crates-io su to) #:use-module (crates-io))

(define-public crate-sutom-rules-0.1 (crate (name "sutom-rules") (vers "0.1.0") (hash "025wzvsnxsk9qp89fnscgkg2nllpmmsd77bjins54d9q7jnn8yzy")))

(define-public crate-sutom-rules-1 (crate (name "sutom-rules") (vers "1.0.0") (hash "0yx88nq8svn7flpcl87rigywdnzjgm4fj3z78xyza39nhlw1bcic")))

(define-public crate-sutom-rules-1 (crate (name "sutom-rules") (vers "1.0.1") (hash "1yqxc93r74nx70ljl5v0ynymyv0vm8gd9c3g9zc1cwk9bfhn9540")))

