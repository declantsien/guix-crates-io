(define-module (crates-io su uk) #:use-module (crates-io))

(define-public crate-suukon-0.1 (crate (name "suukon") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08wb09hlgraaqxmyjbx74yylv92dxbjr04na1xm2snfz1w7mvsj0") (features (quote (("default" "build-binary") ("build-binary" "clap"))))))

(define-public crate-suukon-0.2 (crate (name "suukon") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req ">=4.3, <4.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1cbwmbyddxb8w5glf6xwmj742xid9kglzcs420rcvv1gpmabd52l") (features (quote (("default" "build-binary") ("build-binary" "clap"))))))

