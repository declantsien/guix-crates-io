(define-module (crates-io su m_) #:use-module (crates-io))

(define-public crate-sum_error-0.1 (crate (name "sum_error") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "05gz47qq5vq0nkninzrfg0qbsfnkshmzarjw6143dx140y8j4fn0")))

(define-public crate-sum_error-0.1 (crate (name "sum_error") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0i1wprnj69mvmgg1ql6glimbjw25kw0pn001x15pkbmgdih7i5r3") (yanked #t)))

(define-public crate-sum_error-0.1 (crate (name "sum_error") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0wiidrxrrdzxcyrxwrpj9mifgnbbrn03yc3gv1zdxlzy93hxi7ih")))

(define-public crate-sum_type-0.1 (crate (name "sum_type") (vers "0.1.0") (hash "08svsa4prf7gl3bshz3xryy22h2jp8iygx2hjx8yfp4gyz4al8ar") (features (quote (("try_from") ("generated_example") ("default"))))))

(define-public crate-sum_type-0.1 (crate (name "sum_type") (vers "0.1.1") (hash "1yln7bwv3whk785l9hbw9f56m1dibykblifirjly3rnghsnz1jlz") (features (quote (("try_from") ("generated_example") ("default"))))))

(define-public crate-sum_type-0.2 (crate (name "sum_type") (vers "0.2.0") (hash "05ba7qldsrl0g60bcfm2pgl0iqi7crvpqnj43s4qwz1wkw64lnys") (features (quote (("generated_example") ("default"))))))

