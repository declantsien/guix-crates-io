(define-module (crates-io qn #{90}#) #:use-module (crates-io))

(define-public crate-qn908x-rs-0.0.1 (crate (name "qn908x-rs") (vers "0.0.1") (deps (list (crate-dep (name "bare-metal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p3pkbawzf22ismzdaiwwv1rd8qch782fxsll8d96g6kqckgb37s") (features (quote (("rt" "cortex-m-rt"))))))

