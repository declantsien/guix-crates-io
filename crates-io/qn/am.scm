(define-module (crates-io qn am) #:use-module (crates-io))

(define-public crate-qname-0.1 (crate (name "qname") (vers "0.1.0") (deps (list (crate-dep (name "qname-impl") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "qname-macro") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0a9bnlf9acdrwb8s52zqb35d3cqygcakvndmxgca4a6yqlmmbb4z")))

(define-public crate-qname-impl-0.1 (crate (name "qname-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0143g3nc6y8zi7ic664swrwzwj76krdx8h6ybh3wpl0y9lrx1362")))

(define-public crate-qname-macro-0.1 (crate (name "qname-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "qname-impl") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0g1k73fvhszp5qswbkcj8lqb4d96rihs7i23qkx1kzfwlbd3yrqy")))

