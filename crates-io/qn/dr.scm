(define-module (crates-io qn dr) #:use-module (crates-io))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.0") (hash "0g2ifi4l602x4lxqxilgvipqa455y13vr0haz1mnn7jv91zv730j") (yanked #t)))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.1") (hash "0i9n75pc320vnias7rxp1af2yr2basrb3f5aq6d6hhrgvna97qz8") (yanked #t)))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.2") (hash "1b9spnj6n8lpssdsxf0b5z9nvyijxnpi2whj8cang25wk52wazjw") (yanked #t)))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.3") (hash "12ddlipyflba8pjrq8ysiy20q8fc6cw6vifnrmkwvhrwcny321fa") (yanked #t)))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.4") (hash "00sys93p5m07nxizx3v3is6h8jrnwrq89q5aav2n677nid2i5l00") (yanked #t)))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.5") (hash "1300w9xxf3iyxdmihhxhkpzxrv1wki080zzbxlhrynkd7ik5bfiq") (yanked #t)))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.6") (hash "1yjz735g5ssa063xq2z45zp2wf1z35zm66w478ksqkbasanmyrml") (yanked #t)))

(define-public crate-qndr-0.1 (crate (name "qndr") (vers "0.1.7") (hash "0z9y4ms0vm6fgnw4qpx6m9c26c7kwhxs8jsbhvrlx0lw2chl3ybp")))

