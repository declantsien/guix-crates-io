(define-module (crates-io qn ic) #:use-module (crates-io))

(define-public crate-qnicorn-1 (crate (name "qnicorn") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "build-helper") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4.37") (default-features #t) (kind 1)))) (hash "05r2hg3hds2vqp3q6a72k2ialn1im39fl683cg5m163admw8sf8j") (links "qnicorn")))

