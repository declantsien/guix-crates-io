(define-module (crates-io qn ip) #:use-module (crates-io))

(define-public crate-qnip-0.1 (crate (name "qnip") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "05z0yqc6h5k1p1rcw8dp9ylmh0dfpi9qzc9hx6gx3ngxffxg9ky2") (rust-version "1.60.0")))

