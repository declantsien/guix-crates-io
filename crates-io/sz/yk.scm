(define-module (crates-io sz yk) #:use-module (crates-io))

(define-public crate-szyk-0.1 (crate (name "szyk") (vers "0.1.0") (hash "1xirgqqhbkipkcy2ibf8ibzix7jan8mgqd29jwkqalwz3mqqgyni")))

(define-public crate-szyk-0.1 (crate (name "szyk") (vers "0.1.1") (hash "1jxlj447glwy3h0mw71biqzxsvbq7miwr1pnjna3cz1p71h5x8sy")))

(define-public crate-szyk-0.1 (crate (name "szyk") (vers "0.1.2") (hash "0gc8i4h89q34mgan8g920wdmjsy0xpa6cklizmwq0bg2z7sfwyki")))

(define-public crate-szyk-1 (crate (name "szyk") (vers "1.0.0") (hash "1g4ypvsx8zqn4q5my98pp2zafls81kvq5n2iq1qw0qxy64zc2jcw")))

(define-public crate-szyk-1 (crate (name "szyk") (vers "1.0.1") (hash "1gfph32hvcw6ycc6s3nrzgyza8f1xh2awy0ridy884a243a9pc2g")))

(define-public crate-szyk-1 (crate (name "szyk") (vers "1.0.2") (hash "0f645vi1w2f0z9i0p6zfc7gs7whr4azfs9k8scyyz7i838j4m0wq")))

