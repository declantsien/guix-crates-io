(define-module (crates-io sz -c) #:use-module (crates-io))

(define-public crate-sz-cli-0.1 (crate (name "sz-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clearscreen") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1dw5jbvhb96f2rl2a6lndlwrnwhg8nvdf67s91p2m02qix91wsv2")))

