(define-module (crates-io sz ip) #:use-module (crates-io))

(define-public crate-szip-0.1 (crate (name "szip") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fr67mfhsjbpa3sjb1sg49ymaj180gab1xv9hrv1daamanvi8jwh")))

(define-public crate-szip-0.1 (crate (name "szip") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^0.2") (default-features #t) (kind 0)))) (hash "100wsfv7qngfvabzpiy3xrrz8xh95prl14wzn74g6lmjdygygv0i")))

(define-public crate-szip-1 (crate (name "szip") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (kind 0)) (crate-dep (name "filetime") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "snap") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "02wsf3v4zmqwl63nvprx0w8y40i42iryl64ax81ahvqgkiviry8b")))

