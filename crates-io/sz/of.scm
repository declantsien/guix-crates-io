(define-module (crates-io sz of) #:use-module (crates-io))

(define-public crate-szof-0.1 (crate (name "szof") (vers "0.1.0") (hash "087ahygcnqbf4h8ygavpswn0dws46wh4zs3zzg019ydbrg7zsmi8")))

(define-public crate-szof-0.2 (crate (name "szof") (vers "0.2.0") (hash "0m35hgl5mpq8sahb19hikhqpjxll1v894f1gyfcxlasnb0m3srj8")))

