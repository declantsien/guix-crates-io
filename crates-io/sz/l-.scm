(define-module (crates-io sz l-) #:use-module (crates-io))

(define-public crate-szl-simple-xml-0.1 (crate (name "szl-simple-xml") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "15xsz1v8mk282qychzv3w2lqvbljg11lhcpir8yhfrwk4rk0j4am") (yanked #t)))

(define-public crate-szl-simple-xml-0.1 (crate (name "szl-simple-xml") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0zp907jk4x4n3lyx1mcnnxzdvcvmwll6ip85lg3sq0qgfzwipkh5") (yanked #t)))

(define-public crate-szl-simple-xml-0.1 (crate (name "szl-simple-xml") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0lclw26hhiwb8flcqm1znggaflgjb36fc388mn8s7gg4s34jz1gb") (yanked #t)))

(define-public crate-szl-simple-xml-0.1 (crate (name "szl-simple-xml") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0i9jjc91nszqhwwg9nalinxyyf4zh3zz158hbkr5pi3qzlbnbx8l")))

