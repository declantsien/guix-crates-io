(define-module (crates-io c2 bp) #:use-module (crates-io))

(define-public crate-c2bpp-1 (crate (name "c2bpp") (vers "1.0.0") (deps (list (crate-dep (name "image") (req "^0.22") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (kind 0)))) (hash "18fqvvx2wbnvs3rp7ymwqnyk2y0mlqm3lvvl96m217bwfw2xdsn6") (yanked #t)))

(define-public crate-c2bpp-1 (crate (name "c2bpp") (vers "1.0.1") (deps (list (crate-dep (name "image") (req "^0.22") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (kind 0)))) (hash "12w27k7f4yh78jf85din277sxlpy5r80jdwlln9x9afa5f8am8rb") (yanked #t)))

(define-public crate-c2bpp-1 (crate (name "c2bpp") (vers "1.0.3") (deps (list (crate-dep (name "image") (req "^0.22") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (kind 0)))) (hash "1ya2vg2r1zkpyblyy8invjzhmxsvwy7ix03mps5p3p811ipqwgim")))

