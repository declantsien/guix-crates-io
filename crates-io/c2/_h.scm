(define-module (crates-io c2 _h) #:use-module (crates-io))

(define-public crate-c2_histograms-0.1 (crate (name "c2_histograms") (vers "0.1.0") (hash "16hxpn52rvkxrmvlza8by0d7y1cayby65admdfzwa2gka7zjq8k5")))

(define-public crate-c2_histograms-0.2 (crate (name "c2_histograms") (vers "0.2.0") (hash "1msvqdsd81nkkmvrd1h1r5a123y2pzjvzwn4p4ximf3fmzwgnjc3")))

(define-public crate-c2_histograms-0.2 (crate (name "c2_histograms") (vers "0.2.1") (hash "11digy55b2d95976sr34ddvgivw4r0kw5z62s58vmjd58ns1i3ng")))

(define-public crate-c2_histograms-0.2 (crate (name "c2_histograms") (vers "0.2.2") (hash "1a68n8spz2ac66rzzrxxkfnj4isx691d7k2f1x0wpid115z3ivfj")))

(define-public crate-c2_histograms-0.2 (crate (name "c2_histograms") (vers "0.2.3") (hash "1wjf3ydh7kbkml52ids0wq233s95v7ldwf80nbrh2fcss71zx94b")))

(define-public crate-c2_histograms-0.2 (crate (name "c2_histograms") (vers "0.2.4") (hash "0vr0b3ylpig6ys4m2g99hmi4r7g2cxhwp3q8vw49pcgy6zzxp5k9")))

