(define-module (crates-io c2 -s) #:use-module (crates-io))

(define-public crate-c2-sys-0.1 (crate (name "c2-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1xyshd59jljljckbdrpib8lka7h04vm4d5hixpj9cwl117npa9ms")))

(define-public crate-c2-sys-0.2 (crate (name "c2-sys") (vers "0.2.0") (hash "037hi10aq98c0jhbs766sv4hqr34zffdqyihafi1y5a3qzmrcm0d")))

