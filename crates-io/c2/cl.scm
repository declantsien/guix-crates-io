(define-module (crates-io c2 cl) #:use-module (crates-io))

(define-public crate-c2clat-1 (crate (name "c2clat") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8.16") (default-features #t) (kind 0)) (crate-dep (name "lexopt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "0wsmyfacbwnjz287sagwg42zzg8rcwa0igd767lxzbb0mb7xsk6k")))

