(define-module (crates-io c2 rs) #:use-module (crates-io))

(define-public crate-c2rs-0.1 (crate (name "c2rs") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("derive" "fold"))) (default-features #t) (kind 0)))) (hash "165fklhq4qi72mzg2dhs8mc7wsdp8b1zm1qm87s2ggsrxkk9mn2a") (yanked #t)))

(define-public crate-c2rs-0.1 (crate (name "c2rs") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("derive" "fold"))) (default-features #t) (kind 0)))) (hash "1ldxdixp3l78bad18746kdqxvn45c0hmcwya4q80jjqbd441hfr0")))

(define-public crate-c2rs-0.1 (crate (name "c2rs") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("derive" "fold"))) (default-features #t) (kind 0)))) (hash "0gqcpj49fhnwyclvhc1sg7qs6rbdmqcnharam7ba9jrn3nzviqhz")))

