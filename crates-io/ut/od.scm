(define-module (crates-io ut od) #:use-module (crates-io))

(define-public crate-utodo-0.0.0 (crate (name "utodo") (vers "0.0.0") (hash "0dxb8b8dl1926avai1z7lw9djxjs3ramq6ibfg0a1lrj1mhd32p2") (yanked #t)))

(define-public crate-utodo-1 (crate (name "utodo") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "09ppkfm91kcsaxnn8njn90z7p7q6gjqrwzcd8miwk5w5nk191x04")))

(define-public crate-utodo-2 (crate (name "utodo") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (optional #t) (default-features #t) (kind 0)))) (hash "0j31r2jq1qdn06rr8gdarvfizlwn04a56fixy4vx4mk001hlqsdj") (features (quote (("full" "serde" "toml"))))))

(define-public crate-utodo-2 (crate (name "utodo") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (optional #t) (default-features #t) (kind 0)))) (hash "0nvwczfk2i5m43brph0gv956hm65db0wh3260qk8hkz4q7hpwav5") (features (quote (("full" "serde" "toml"))))))

