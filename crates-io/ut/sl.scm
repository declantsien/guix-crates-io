(define-module (crates-io ut sl) #:use-module (crates-io))

(define-public crate-utsl-hybrid-clocks-0.1 (crate (name "utsl-hybrid-clocks") (vers "0.1.0") (deps (list (crate-dep (name "suppositions") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0kslwzsp3hn7n01fpjzyq82w4w7hx42snk5rkv8060aj2yqywka7") (features (quote (("pretty-print" "time"))))))

