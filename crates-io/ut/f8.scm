(define-module (crates-io ut f8) #:use-module (crates-io))

(define-public crate-utf8-bufread-0.1 (crate (name "utf8-bufread") (vers "0.1.1") (hash "1fbwpzvf7kbhg8bn9gwzj1iy8wcxm1s23wblqb8qy3wy49dkrwxi") (yanked #t)))

(define-public crate-utf8-bufread-0.1 (crate (name "utf8-bufread") (vers "0.1.2") (hash "0mihc6rpp0yygscczmq60fxdhaia5h7pwbg7w6qffk673yqzzldn") (yanked #t)))

(define-public crate-utf8-bufread-0.1 (crate (name "utf8-bufread") (vers "0.1.3") (hash "1ivd18dhnk7a52n2nly6lbwy466qhmj6ap9kzpmkh173avpwwsyk") (yanked #t)))

(define-public crate-utf8-bufread-0.1 (crate (name "utf8-bufread") (vers "0.1.4") (hash "1r69881c7byldmhg1dha66ri36pxlj54ihly0m12bjkdianqx5sx") (yanked #t)))

(define-public crate-utf8-bufread-0.1 (crate (name "utf8-bufread") (vers "0.1.5") (hash "1rr7d9gsnhshqmksngszmrr1g7nidpkygsm283h3bphhzin1cj0a") (yanked #t)))

(define-public crate-utf8-bufread-1 (crate (name "utf8-bufread") (vers "1.0.0") (deps (list (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "040cir45ki9n3xs138xqb5aiay6znsz6p405f9iviw6n0qxi7jbf") (yanked #t)))

(define-public crate-utf8-builder-0.1 (crate (name "utf8-builder") (vers "0.1.0") (deps (list (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "01b62pahqh8xrv6nm0va64r0k30zvy8sg5ksv2lcabcaadkvgbma") (features (quote (("std") ("default" "std"))))))

(define-public crate-utf8-builder-0.1 (crate (name "utf8-builder") (vers "0.1.1") (deps (list (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bp3yig9qksrfjdj2pqdmsf7jpv4kimsm64gya6i3wzpvkmw1lrs") (features (quote (("std") ("default" "std"))))))

(define-public crate-utf8-builder-0.1 (crate (name "utf8-builder") (vers "0.1.2") (deps (list (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wfb2s22lan2fhcknhx5d48mlym8fqagcr9zp6gacigx88bv545y") (features (quote (("std") ("default" "std"))))))

(define-public crate-utf8-chars-0.0.1 (crate (name "utf8-chars") (vers "0.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1fn195nyc21a6p7gnnhpff1m4mgf9gl7x3y2wm4w2ibarzrfrnpw")))

(define-public crate-utf8-chars-0.0.2 (crate (name "utf8-chars") (vers "0.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0izqn8jqm5jlkcf3clyayfryn3qgx6wbmdjsrnj1gd1w6bifhkcl") (features (quote (("docs-rs"))))))

(define-public crate-utf8-chars-0.0.3 (crate (name "utf8-chars") (vers "0.0.3") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "067zd04lxrbfj00r4yycjd7k8bk23zblk17llbnkpc3k22y520jw")))

(define-public crate-utf8-chars-0.0.4 (crate (name "utf8-chars") (vers "0.0.4") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "19rk4spinpnyymxwndxb9jx9rbc4qfzf6wv3z62v2ig6gx946yd8") (features (quote (("docs-rs"))))))

(define-public crate-utf8-chars-0.0.5 (crate (name "utf8-chars") (vers "0.0.5") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1c8crjbx7wr3idma2hfpbmaw09gxxj31na3mrix5335rjgnb0p37")))

(define-public crate-utf8-chars-0.0.6 (crate (name "utf8-chars") (vers "0.0.6") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "00yf6sii6h0a8v5zj7fqjq6wfjrx39z7fxhpzk9i6xg84ldsmqsw")))

(define-public crate-utf8-chars-0.0.7 (crate (name "utf8-chars") (vers "0.0.7") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0sq05fz843h8znrcnrgbvpnkf7kspp1ykw4y7hhzmrh0sgymqnfz")))

(define-public crate-utf8-chars-0.0.8 (crate (name "utf8-chars") (vers "0.0.8") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "1qkcrbk4nkkkgzs071d992l0gmfii4a2g06hvp928pjb8sv6irvp")))

(define-public crate-utf8-chars-0.0.9 (crate (name "utf8-chars") (vers "0.0.9") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0zbxg9bcgvnxrnn1svk1dlazl45h5h8lkjf4fz4gb7k7pyn3shn6")))

(define-public crate-utf8-chars-0.0.10 (crate (name "utf8-chars") (vers "0.0.10") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "08961hbcfsvi4arfrcqq5sb6cjxxxk092p27vk64f1r5zc8y3n0n")))

(define-public crate-utf8-chars-0.1 (crate (name "utf8-chars") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "177lkvz1mmf4f7r3gzyqkvi75v8xp0bar84fvsxzgzcrvl5bdyp4")))

(define-public crate-utf8-chars-0.1 (crate (name "utf8-chars") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1yigpws25n4z8f189bmq9r3rm1i9p92y7xsy8kmpq2164d8x881k")))

(define-public crate-utf8-chars-0.1 (crate (name "utf8-chars") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0wnckqzgmmzz9z10yxpfig4kn0cbh50xy5qhggkwid4bvskm7w04")))

(define-public crate-utf8-chars-0.1 (crate (name "utf8-chars") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1hh507syf0l44cx4bs416j0zw9x93nzw6gdwpm236xhrvk3h05fx")))

(define-public crate-utf8-chars-0.1 (crate (name "utf8-chars") (vers "0.1.4") (deps (list (crate-dep (name "arrayvec") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "11pjij0n20jknzk08wdg7h5ir18pwwwsavq75i5r0ya7jfdyr2lp")))

(define-public crate-utf8-chars-0.2 (crate (name "utf8-chars") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0dk87jmpjzj8qz24yz6f3znrxxg1qzlj4s97brjn2zi888sfqnzj") (yanked #t)))

(define-public crate-utf8-chars-0.2 (crate (name "utf8-chars") (vers "0.2.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0r5fksmsfn1zw5jbildh9yc5v3amxbb11wxp8vcipjcwxi4qnacc") (yanked #t)))

(define-public crate-utf8-chars-0.2 (crate (name "utf8-chars") (vers "0.2.2") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0gavlia3jq8sy6vhxhr5m4zyrlp56vjrh3z2z98xbikq2937n4xd") (yanked #t)))

(define-public crate-utf8-chars-0.2 (crate (name "utf8-chars") (vers "0.2.3") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "066d6zmbwcc88wpiz7gy3c7qr5h7mp0b21kfazj23x5rg07d2mdj")))

(define-public crate-utf8-chars-0.3 (crate (name "utf8-chars") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0c09rw5mpwlsfx8g54vs4hwafrjmi9wjaxmwjp6z06zayafcz9x0")))

(define-public crate-utf8-chars-0.4 (crate (name "utf8-chars") (vers "0.4.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1na771kgxlh9i2x1f7q53c7vwjawvwdfgndkk1i0cgh7v9svz478")))

(define-public crate-utf8-chars-0.5 (crate (name "utf8-chars") (vers "0.5.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1i6acpan7cwrpvrq7s81r07nsfx8k5bdjf0xhrrk1y0c4yy3bq84")))

(define-public crate-utf8-chars-0.5 (crate (name "utf8-chars") (vers "0.5.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1f9dfivfi5kg1ii6rgy59lhalz6ri9al77c691cbfkmkpnbb7557")))

(define-public crate-utf8-chars-1 (crate (name "utf8-chars") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1p59yq113py5w36fkhw19x1kw8lbqns1rw0igvw0hd7dpbc5qzh9")))

(define-public crate-utf8-chars-1 (crate (name "utf8-chars") (vers "1.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0s4m3ag4k80k3izx6z52pfhqk2p9h48d4wlzynf5gq4qbfnhla3x")))

(define-public crate-utf8-chars-1 (crate (name "utf8-chars") (vers "1.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0hf8krxn54lm9jsppamzvmhf34xz6s71kh6bwydh37g7mj7qsd61")))

(define-public crate-utf8-chars-2 (crate (name "utf8-chars") (vers "2.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0awq3i04hn77lyvj82mqvxa5pgx6fn475in7missn22qnq8zz3b7") (rust-version "1.59")))

(define-public crate-utf8-chars-2 (crate (name "utf8-chars") (vers "2.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "06638bmc6psy32vqfvfh434rls4bqpxhi7v45nm1sih6jdr3grzs") (rust-version "1.59")))

(define-public crate-utf8-chars-2 (crate (name "utf8-chars") (vers "2.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1bvqq8qx2m1gwvkm8x252kbvgif5kmc50xs8zsh96gbp9c0s9w3f") (rust-version "1.59")))

(define-public crate-utf8-chars-2 (crate (name "utf8-chars") (vers "2.0.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0fpw9mgrwp7c84ii3nnpy7ckd99i44ffq68v96cjmnxclfkc8ys3") (rust-version "1.59")))

(define-public crate-utf8-chars-2 (crate (name "utf8-chars") (vers "2.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1azamg1y7q9q3vmsr00zkdvicr891kcl5bynh2mwzzxaqpqncpms") (features (quote (("default") ("bench")))) (rust-version "1.59")))

(define-public crate-utf8-chars-3 (crate (name "utf8-chars") (vers "3.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0qmaqa2c8wi5hk2b4p666siv666252jsdjgdh0awmw3ac9sxhr02") (features (quote (("default") ("bench")))) (rust-version "1.70")))

(define-public crate-utf8-chars-3 (crate (name "utf8-chars") (vers "3.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0xy2qa4xdk2qls6p7zl2iw3sj9zc8r85crq9pkc35b7r1575wih8") (features (quote (("default") ("bench")))) (rust-version "1.70")))

(define-public crate-utf8-chars-3 (crate (name "utf8-chars") (vers "3.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0kivq9pyxp84ap97yd2q0c48dv6krrhxmlp96farpcxgrazxka2j") (features (quote (("default") ("bench")))) (rust-version "1.70")))

(define-public crate-utf8-chars-3 (crate (name "utf8-chars") (vers "3.0.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1v2wyj6s06hy0x0r5r3b55bciww0zzd6mghbf407rxrlij7632xv") (features (quote (("default") ("bench")))) (rust-version "1.70")))

(define-public crate-utf8-command-1 (crate (name "utf8-command") (vers "1.0.0") (hash "0c29dcz3k7yy5im458dkzicg2i11ddlwhj9d1b2408f4iwy0a8p8")))

(define-public crate-utf8-command-1 (crate (name "utf8-command") (vers "1.0.1") (hash "0ny5204sckcrycdwqamhcf9zz4ihn0h6v7mj8s8a9kcl9i953cg4")))

(define-public crate-utf8-cstr-0.1 (crate (name "utf8-cstr") (vers "0.1.0") (hash "1zwhc5xy4dhjjvw995ysv02imshs47n9cvvmnns3hfm2kyymdgac")))

(define-public crate-utf8-cstr-0.1 (crate (name "utf8-cstr") (vers "0.1.1") (hash "12sfgnx97cand56h0rq97b1s7jv28lg42bnkmbk97p60xgqbskw7")))

(define-public crate-utf8-cstr-0.1 (crate (name "utf8-cstr") (vers "0.1.2") (deps (list (crate-dep (name "assert_matches") (req "^1") (default-features #t) (kind 2)))) (hash "1pzjsmqgp2y0wlixjqrdzivc9lick4isgndgzbcg0yg6c5h0np4z")))

(define-public crate-utf8-cstr-0.1 (crate (name "utf8-cstr") (vers "0.1.3") (deps (list (crate-dep (name "assert_matches") (req "^1") (default-features #t) (kind 2)))) (hash "1slbxcw0jrmnihxmiwd6h42c8aafm97kz6c0fhrka7h2rwjdvyfl")))

(define-public crate-utf8-cstr-0.1 (crate (name "utf8-cstr") (vers "0.1.4") (deps (list (crate-dep (name "assert_matches") (req "^1") (default-features #t) (kind 2)))) (hash "1b1ranziwx8ziv3l7krhk32shipkiak9m0vh941cmpffrj0srqdb")))

(define-public crate-utf8-cstr-0.1 (crate (name "utf8-cstr") (vers "0.1.5") (deps (list (crate-dep (name "assert_matches") (req "^1") (default-features #t) (kind 2)))) (hash "16pqcgirw6129q3mck396aps0c9qkbssa5zxis5011970nv1j6x5")))

(define-public crate-utf8-cstr-0.1 (crate (name "utf8-cstr") (vers "0.1.6") (deps (list (crate-dep (name "assert_matches") (req "^1") (default-features #t) (kind 2)))) (hash "0a5n94agdn7z54lzqqwk05fp9hsi1farac39sl82n5a1a51bpg2m")))

(define-public crate-utf8-decode-0.1 (crate (name "utf8-decode") (vers "0.1.0") (hash "1hiwlb3cp47c00lj12pkwan0g6amkxg6najqbk6ff3bwqzla7vgc")))

(define-public crate-utf8-decode-1 (crate (name "utf8-decode") (vers "1.0.0") (hash "1ymbkqanqaja7sngqcibgyfrckrm118k4kzmz3h26311s2a7ws7w")))

(define-public crate-utf8-decode-1 (crate (name "utf8-decode") (vers "1.0.1") (hash "1674kmm1pfv6s9pk0m92zkqdid4rggl077x24s4a16ikz8kynqfa")))

(define-public crate-utf8-io-0.0.0 (crate (name "utf8-io") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1wsyg3z3xxq6ysm7xkqawcx9ng2k2hv0q8ca7dnr9p0g6hkldxcn")))

(define-public crate-utf8-io-0.1 (crate (name "utf8-io") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1na743d9jamps07mca4nk2vs1gn06rwpqjkgjbjv3kwc2fix1m94")))

(define-public crate-utf8-io-0.2 (crate (name "utf8-io") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0zvy5mfvlcqqj60whhvplxmg490p4gnchnypcnqnlmjjsqh2cmr6")))

(define-public crate-utf8-io-0.3 (crate (name "utf8-io") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "08ni4v03j2vdhs3jm4vid81p4zwsy8yim4sdac4vkyv0xvsmxmll")))

(define-public crate-utf8-io-0.3 (crate (name "utf8-io") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1vphkqv0sxfxkamdddcx6m7ds2rlj7wbq3aiwi751jq1ipin19x3")))

(define-public crate-utf8-io-0.4 (crate (name "utf8-io") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0637686wnfz2jp6zgbsj5sl9z361wfxigf83404d47p84h6f9q2k")))

(define-public crate-utf8-io-0.4 (crate (name "utf8-io") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.5.2") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0x28hpb0m9q5i2wj95mwg076915yd72xwmi7cj4isc2rwhw7vl09")))

(define-public crate-utf8-io-0.5 (crate (name "utf8-io") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1wz0ayhbby9pn45c0cmpw6ljpbl08ixmi0d6ajmvhfmyg1524kfk")))

(define-public crate-utf8-io-0.6 (crate (name "utf8-io") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.2.3") (kind 0)) (crate-dep (name "layered-io") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0776k887xpswhjyjd7gks1gp9k74nmd5j82f1ld11haz3n6ygfm3")))

(define-public crate-utf8-io-0.7 (crate (name "utf8-io") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.3.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsafe-io") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "075bd6wd7k55ggidmf5chqlha3p43xl1vzri0bj2p3231xrv98c6")))

(define-public crate-utf8-io-0.8 (crate (name "utf8-io") (vers "0.8.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.3.3") (kind 0)) (crate-dep (name "layered-io") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zzy9f9fa1f78x475j0zcm4insfibbk4yi6zx7b52c041nhww886")))

(define-public crate-utf8-io-0.9 (crate (name "utf8-io") (vers "0.9.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.4.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "terminal-io") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "00ww040h4mrp724p7ixrx7rj5rhqjgg474lav9x3n4p2kwylk2fx")))

(define-public crate-utf8-io-0.9 (crate (name "utf8-io") (vers "0.9.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.4.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ypdci4kbqz051v5g142yif5lw05gk3jrcd9wsq7wxw30jx0jkch")))

(define-public crate-utf8-io-0.10 (crate (name "utf8-io") (vers "0.10.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.4.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "1rp7wi090j7fakid1l1ah13cnrdd3g1d8hn9rrvhsqigzmj0kxi1")))

(define-public crate-utf8-io-0.11 (crate (name "utf8-io") (vers "0.11.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.5.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)))) (hash "02a0vhm7dr0yl7yh4gl0irfcjhfzgixkg5xr451rmdrw5092is0a")))

(define-public crate-utf8-io-0.12 (crate (name "utf8-io") (vers "0.12.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.6.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.13.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.13.0") (optional #t) (default-features #t) (kind 0)))) (hash "008npyq539h6fs81r0lynxwvn9qmph4j9xnhl8sfrz8m4a7qij39")))

(define-public crate-utf8-io-0.13 (crate (name "utf8-io") (vers "0.13.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^0.7.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.14.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.14.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vy8vqmka1l4yr207yslb8lb6f2k3r77hq7kpyhl5vwfjc6xsx1l")))

(define-public crate-utf8-io-0.14 (crate (name "utf8-io") (vers "0.14.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.17.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)))) (hash "1wdcxbiwrq7raiwxqkg78dm870yq9wi3pn8c37llhgr0yqfp9yvc")))

(define-public crate-utf8-io-0.15 (crate (name "utf8-io") (vers "0.15.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.18.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.18.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.16.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gfa4vzyjpl81pnrmj3blwvqxwflnnx8sdq10h19rwk4py9dcnln")))

(define-public crate-utf8-io-0.16 (crate (name "utf8-io") (vers "0.16.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.20.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.17.0") (optional #t) (default-features #t) (kind 0)))) (hash "007hf06k7ph08nbki8rrd9sar3b4mmcv7l1g3cmw4jls5b6cb342")))

(define-public crate-utf8-io-0.17 (crate (name "utf8-io") (vers "0.17.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "io-lifetimes") (req "^1.0.0") (kind 0)) (crate-dep (name "layered-io") (req "^0.21.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.21.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.18.0") (optional #t) (default-features #t) (kind 0)))) (hash "1m9q5kpkq1byv78mj18bqyqyr1j2jpys4aaaka4057vhh1jbgwsx")))

(define-public crate-utf8-io-0.18 (crate (name "utf8-io") (vers "0.18.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.22.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mjdyh6iy4hv0p84pzh17lkldi85pakkl2ypdw7fps6acd7c12h8")))

(define-public crate-utf8-io-0.19 (crate (name "utf8-io") (vers "0.19.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.23.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "0isb2bn58a7izfqqfhvmbzq92hb0irlc6l4q9aspbkz0b2kbp92d")))

(define-public crate-utf8-io-0.19 (crate (name "utf8-io") (vers "0.19.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "duplex") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "io-extras") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.23.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "layered-io") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "terminal-io") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "0sgc12xyqbx4apvbinyxh7sdmkfy4fzxpi509jxrs97asn8rcx1i")))

(define-public crate-utf8-locale-0.2 (crate (name "utf8-locale") (vers "0.2.0") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req ">=1, <3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0kc1dyhhihrkw21n70rnj66qchvndjz4b22yk35nf897mllygpfa")))

(define-public crate-utf8-locale-0.3 (crate (name "utf8-locale") (vers "0.3.0") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req ">=1, <3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0myjbc9fj96vjv9hjn0p4m6krnid2wqh1mbczjnl1h40f6kli07s")))

(define-public crate-utf8-locale-1 (crate (name "utf8-locale") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "16zbs9wbrv873sscs4fj4n4yz8w2dqdglwr81fbngbcmlmd5k1ay")))

(define-public crate-utf8-locale-1 (crate (name "utf8-locale") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "05zjz8w5l5avw5zj421g296klppyd40qrp945q2fmdq2rxx1k29f") (rust-version "1.58")))

(define-public crate-utf8-locale-1 (crate (name "utf8-locale") (vers "1.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "02nrdp0yv205kld6f7gvwx7dynwxv5mhl776zlk7k2239sssdbic") (rust-version "1.58")))

(define-public crate-utf8-norm-1 (crate (name "utf8-norm") (vers "1.0.0") (deps (list (crate-dep (name "lapp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zmdc904j3cvd137j3dz7r1s2lk8bi99sbgpmwlw4sq5l5kblcar")))

(define-public crate-utf8-norm-1 (crate (name "utf8-norm") (vers "1.0.1") (deps (list (crate-dep (name "lapp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lvkw7q23xal4acp48493gz0zdfgx20nm01g8syp098nfgdpsghs")))

(define-public crate-utf8-norm-1 (crate (name "utf8-norm") (vers "1.1.1") (deps (list (crate-dep (name "lapp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g51gz2sy4p8j06bcra8rcjrkv7c2rj3lg7cy5vpqynwjy0w72zx")))

(define-public crate-utf8-parser-0.0.3 (crate (name "utf8-parser") (vers "0.0.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "01nsf6fwbcm9mbx3qigdax6j16mv991dia6cm9bxx3l0kinjbxn8") (features (quote (("std") ("default-feature" "std")))) (rust-version "1.60")))

(define-public crate-utf8-parser-0.0.4 (crate (name "utf8-parser") (vers "0.0.4") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0bj8hhkf6l0ww5ki0rrzylnrky4kgwmpschg0gglahkshrg3rbi1") (features (quote (("std") ("error_in_core") ("default-feature" "std")))) (rust-version "1.60")))

(define-public crate-utf8-ranges-0.1 (crate (name "utf8-ranges") (vers "0.1.0") (hash "1qfsl396n06jzvnsxqirk96gv0jgl5xj30xdgr22rcz6j2spviap")))

(define-public crate-utf8-ranges-0.1 (crate (name "utf8-ranges") (vers "0.1.1") (hash "0ibszmxc75n0w0lcqbylaclqhkv4pcg7ckhfy7lnc4jbj8mlnkc3")))

(define-public crate-utf8-ranges-0.1 (crate (name "utf8-ranges") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1dlf2mn6x3z8n4mvy9cai3v91rwcnpqa9phccgv4af4kkniq1m0m")))

(define-public crate-utf8-ranges-0.1 (crate (name "utf8-ranges") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "03xf604b2v51ag3jgzw92l97xnb10kw9zv948bhc7ja1ik017jm1")))

(define-public crate-utf8-ranges-1 (crate (name "utf8-ranges") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "08j129anwcbdwvcx1izb4nsf0nbxksin2zqxjbrfz2x94mjsnbv6")))

(define-public crate-utf8-ranges-1 (crate (name "utf8-ranges") (vers "1.0.1") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (kind 2)))) (hash "1r7gp4jmxkrv1266l0pbmibp3s6h3gpf1z72d14hj438vxkz8w7x")))

(define-public crate-utf8-ranges-1 (crate (name "utf8-ranges") (vers "1.0.2") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (kind 2)))) (hash "0drp4j55bz7gzk5sbrk6g1nd0p3xm2an9q77mpvhjxpqpr47wvvr")))

(define-public crate-utf8-ranges-1 (crate (name "utf8-ranges") (vers "1.0.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)))) (hash "1ppzjsxmv1p1xfid8wwn07ciikk84k30frl28bwsny6za1vall4x")))

(define-public crate-utf8-ranges-1 (crate (name "utf8-ranges") (vers "1.0.4") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)))) (hash "1fpc32znar5v02nwsw7icl41jzzzzhy0si6ngqjylzrbxxpi3bml")))

(define-public crate-utf8-ranges-1 (crate (name "utf8-ranges") (vers "1.0.5") (deps (list (crate-dep (name "quickcheck") (req "^1") (kind 2)))) (hash "1fk46654sqis2dqamihlj9b1sv162kp3brgmmqpa0lqfz4kwikvz")))

(define-public crate-utf8-read-0.4 (crate (name "utf8-read") (vers "0.4.0") (hash "0640qbnj3smds1451x0nbbvg15szx9r0xjzbf8wg3hqidv06lmpv")))

(define-public crate-utf8-rfc2279-0.1 (crate (name "utf8-rfc2279") (vers "0.1.0") (deps (list (crate-dep (name "tinyvec") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1hqc3cnpir4jnvlg31rc1jzsra3b8zs79x88cin9zgp0z75krcwk")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0is2bhwnshfyv07gniyn5pxpbks5qczsl5kc1qqf5d6fbi4rka0x")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1zmq1hp8n02h9xhg7zjl6pnk5jaxymg8fx9nbns5wvlfpcjf9rry")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0bri0rnaw49al897ardzbwxsy3354nhh6s5pa47xhhl4yrj6f6hc")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "192b27g65d0c975jckpzjxfj8ac5rhrabyy61p30fscdbvz58b3g")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.4") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1ylf5mvzck81iszchxyqmhwimkcdqv7jhazvd454g911cchsqwch")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.5") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0ax3y087ga8r2aap2h36jhmcapfvbly97mac3shxzy3y8mzxgxvw")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.6") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "18fbr6bbkfprs0hb2pdz3ckfb6i1gm0j0x7ka3fhvbyd5m2ck42i")))

(define-public crate-utf8-width-0.1 (crate (name "utf8-width") (vers "0.1.7") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1qwj8c0fg8cpn8hq7c9xzz26kdz6ci32bf0madz57a2xi578vgc6") (rust-version "1.56")))

(define-public crate-utf8_iter-1 (crate (name "utf8_iter") (vers "1.0.0") (hash "1nj1ghjyw9qq0ig9qggi0mnvfcm2n1w012mj8vfjzvnh6q300105")))

(define-public crate-utf8_iter-1 (crate (name "utf8_iter") (vers "1.0.1") (hash "120ijgdin1w08n8v8wd604k21zc9wg8011f7i1xksniyjb2dprjr")))

(define-public crate-utf8_iter-1 (crate (name "utf8_iter") (vers "1.0.2") (hash "0rnj1nwwc07lmchd1304dnjjlddyvbjl54cm7j6rgph0sgamqw7q")))

(define-public crate-utf8_iter-1 (crate (name "utf8_iter") (vers "1.0.3") (hash "0cwzrh2w7n0i6cd3y842pxydcb0p629x3bjwhqyhwl5raljr5a34")))

(define-public crate-utf8_iter-1 (crate (name "utf8_iter") (vers "1.0.4") (hash "1gmna9flnj8dbyd8ba17zigrp9c4c3zclngf5lnb5yvz1ri41hdn")))

(define-public crate-utf8_reader-0.1 (crate (name "utf8_reader") (vers "0.1.0") (hash "180jbjs98xlwarzzwc43qy5zk9zq0lkiay0p2raszl19wsk5ap6j")))

(define-public crate-utf8_reader-0.2 (crate (name "utf8_reader") (vers "0.2.0") (hash "1rlc0iw2116fax9g314g3sg7yjr2xr6gir36fjnix89mrsalw8cf")))

(define-public crate-utf8_reader-0.3 (crate (name "utf8_reader") (vers "0.3.0") (hash "1yj2q2a49r5a8wa93vvq68nl4wmbz9x445ymvjcbcm7vxmlwb14q")))

(define-public crate-utf8_reader-0.4 (crate (name "utf8_reader") (vers "0.4.0") (hash "09fnfz42r83xl2ghr67cis71fj9qnl7xpi39mbw1y72vlwmq2fnd")))

(define-public crate-utf8_reader-0.5 (crate (name "utf8_reader") (vers "0.5.0") (hash "0k2ipkdgnznkqmkwxg4r8jflsk8f488lgvxjdxs5qpmbdkn19ydm")))

(define-public crate-utf8_reader-0.6 (crate (name "utf8_reader") (vers "0.6.0") (hash "0lcgakphd9al5n6f5hqfpxk4g94ny8c8shcjxqghvnfggnpiafin")))

(define-public crate-utf8_reader-0.7 (crate (name "utf8_reader") (vers "0.7.0") (hash "094i792985bdclqpi8fni3k7darqkr19ak5w51jbn0ky4p6hqs19")))

(define-public crate-utf8_slice-1 (crate (name "utf8_slice") (vers "1.0.0") (hash "022lg2fv67yimm5lk7nwa5f2f200cppc0wg6ds75s3yg1ql9w424")))

(define-public crate-utf8_utils-0.1 (crate (name "utf8_utils") (vers "0.1.0") (hash "17bmb74vh5j3ibg61lbvs6fx59drm9z74rc1gh78x3cv7qr2cmgg")))

(define-public crate-utf8conv-0.0.7 (crate (name "utf8conv") (vers "0.0.7") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (features (quote ("small_rng"))) (kind 2)) (crate-dep (name "stackfmt") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "0zqlm19c1w3xi3193c43m81nr1i9asanb68zmv4j11vcd4zrp8qg") (features (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

(define-public crate-utf8conv-0.0.8 (crate (name "utf8conv") (vers "0.0.8") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (features (quote ("small_rng"))) (kind 2)) (crate-dep (name "stackfmt") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1npjgshfwvsabvgha1chypvxkygbjmvagh2cjy2jhxcv7qwx843h") (features (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

(define-public crate-utf8conv-0.0.9 (crate (name "utf8conv") (vers "0.0.9") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (features (quote ("small_rng"))) (kind 2)) (crate-dep (name "stackfmt") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1mspnfg01za0yfv4058x537l1s38pr22w4gb3zic8kf0jnvqpzg3") (features (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

(define-public crate-utf8conv-0.1 (crate (name "utf8conv") (vers "0.1.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (features (quote ("small_rng"))) (kind 2)) (crate-dep (name "stackfmt") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1nsf4vrb8glab8796lhjx9wdczyffc6n0jq5zi980viqx6r7hdrk") (features (quote (("std" "alloc" "core") ("docsrs") ("default" "core") ("core") ("alloc" "core"))))))

(define-public crate-utf8mb3-0.1 (crate (name "utf8mb3") (vers "0.1.1") (hash "0y8hsgbvyc3ck2c5gvqlrqbzbza21hqkb5iq8lncaxd0d77qdh7s")))

(define-public crate-utf8mb3-0.1 (crate (name "utf8mb3") (vers "0.1.2") (hash "1qdp6cyqwf877yigrm17bwabrw7nzzh314sfbywkaw8xzf40ff15")))

(define-public crate-utf8parse-0.1 (crate (name "utf8parse") (vers "0.1.0") (hash "08q37jm94j5ypxmvhv6vdnazd1ay9wmhhycdqxaa98wl65zshpm1")))

(define-public crate-utf8parse-0.1 (crate (name "utf8parse") (vers "0.1.1") (hash "0zamsj2986shm4x9zncjf2m5qy9scaw7qnxw4f89b2afpg6a8wl7")))

(define-public crate-utf8parse-0.2 (crate (name "utf8parse") (vers "0.2.0") (hash "0wjkvy22cxg023vkmvq2wwkgqyqam0d4pjld3m13blfg594lnvlk") (features (quote (("nightly") ("default"))))))

(define-public crate-utf8parse-0.2 (crate (name "utf8parse") (vers "0.2.1") (hash "02ip1a0az0qmc2786vxk2nqwsgcwf17d3a38fkf0q7hrmwh9c6vi") (features (quote (("nightly") ("default"))))))

(define-public crate-utf8path-0.1 (crate (name "utf8path") (vers "0.1.0") (hash "1qy8nrqgmd78brf240a3c6z4q7x8fyqzkrhq07yrzp2cnj0ar0zc")))

(define-public crate-utf8reader-0.0.1 (crate (name "utf8reader") (vers "0.0.1") (hash "02s31zf0dzf97d0ld04n6my5725rfh7yl825izj9bvqrc9wvi6qg")))

(define-public crate-utf8reader-0.0.2 (crate (name "utf8reader") (vers "0.0.2") (hash "06kd41l5nrnwzp3y8m7m5zfjza13j53a4x3qn8cx730l86ybmywd")))

(define-public crate-utf8reader-0.0.3 (crate (name "utf8reader") (vers "0.0.3") (hash "0winrmvd93pxih9kx1njkq233pia5lz8ydsxdzx1il2wb5mb3d0y")))

(define-public crate-utf8reader-0.0.4 (crate (name "utf8reader") (vers "0.0.4") (hash "0zmrv4iiisa1j6vsvkwb9bpk1ixxxi8a36rn3rmb01nng3iwsnh8")))

(define-public crate-utf8reader-0.1 (crate (name "utf8reader") (vers "0.1.0") (hash "036gcznivbm7xgqnxspd8xy17snfnvkx1k8zwdifjsmxfsrxgb9i")))

