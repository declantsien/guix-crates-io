(define-module (crates-io ut e2) #:use-module (crates-io))

(define-public crate-ute2-0.1 (crate (name "ute2") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10xcaxdc2nl5km2ngwpmrlvjbx5w6zfy5lmjhgq5rip9w0r4h0h7")))

