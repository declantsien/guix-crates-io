(define-module (crates-io ut oo) #:use-module (crates-io))

(define-public crate-utools-0.1 (crate (name "utools") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jf74w89zmqxiwjndsigdmizb4cywxz02ycsm8llsir0lzl7asdg")))

(define-public crate-utools-0.1 (crate (name "utools") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g65y0fq9k4cz5kxcf062mafill70i3058pdzz89mgc97g6flab4")))

(define-public crate-utools-0.1 (crate (name "utools") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0iidlcq5sj8bg08861jyg9959j51pnq06r7wrmvl9dry7rxw86wz")))

