(define-module (crates-io ut f6) #:use-module (crates-io))

(define-public crate-utf64-0.1 (crate (name "utf64") (vers "0.1.0") (hash "0ahv42nmgw596x52fvjh9slia16ghz51yyl0myyng4qjpimsjls6")))

(define-public crate-utf64-1 (crate (name "utf64") (vers "1.0.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1x8q6fa7fbk1rs6a1rcyzp6qccg1p3lzllzm2qaa1c2dffs050ih")))

(define-public crate-utf64-1 (crate (name "utf64") (vers "1.0.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1nm4nn97r2g6rm4d8fjf75pvjdh1diib77mbqaq6rjpw68fvr5ix")))

