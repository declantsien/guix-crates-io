(define-module (crates-io ut s2) #:use-module (crates-io))

(define-public crate-uts2ts-0.1 (crate (name "uts2ts") (vers "0.1.0") (hash "1pf5xc8yxz55lj2k8xn4rlzc7bdn2cadx6j80zfpv8h306d3pk6g")))

(define-public crate-uts2ts-0.2 (crate (name "uts2ts") (vers "0.2.0") (hash "0v38lasxs7qcv24ajhjdpbdlrhmk52h4nghv7qllxnbivh7ajq9i")))

(define-public crate-uts2ts-0.2 (crate (name "uts2ts") (vers "0.2.1") (hash "1qqhx2001mqk75d44ggpkzia4wiiq74s2diqggzysi5c7x5wgchl")))

(define-public crate-uts2ts-0.2 (crate (name "uts2ts") (vers "0.2.2") (hash "1ls9zm72f8xn1a1rk906pbz5wkv8ngchnzd1amq6sblgk0k7sjyd")))

(define-public crate-uts2ts-0.3 (crate (name "uts2ts") (vers "0.3.0") (hash "12nil2wnzplfwpz02fmcqy5bi8xsiz0hrpcmfzv6bgil1rhvim9i")))

(define-public crate-uts2ts-0.4 (crate (name "uts2ts") (vers "0.4.0") (hash "0kxlh2chn9sip3ch19hcvv3r35gq4qgwf8s7cf00h22qr82s3301")))

(define-public crate-uts2ts-0.4 (crate (name "uts2ts") (vers "0.4.1") (hash "1a063wh09dik0lflbrgf6rcqglwzzprmss1mdq38vn2jahiig6r5")))

