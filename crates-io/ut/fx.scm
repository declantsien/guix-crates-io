(define-module (crates-io ut fx) #:use-module (crates-io))

(define-public crate-utfx-0.1 (crate (name "utfx") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (kind 2)))) (hash "1pl1dacyz2vibmz7kav87g636a8dwbifikfzglqp6rs8057zffqk") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

