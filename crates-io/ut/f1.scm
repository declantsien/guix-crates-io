(define-module (crates-io ut f1) #:use-module (crates-io))

(define-public crate-utf16-0.0.0 (crate (name "utf16") (vers "0.0.0") (hash "0fy6m80q2c8h95gx36mfb1s3gr7vxb6z14jnwsli01vlh2sfl01n") (yanked #t)))

(define-public crate-utf16-ext-0.0.1 (crate (name "utf16-ext") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "~1") (default-features #t) (kind 0)))) (hash "0svi007sfh943flg8hhg0ygl33r1vawq3xmkkx4dzlncxq386s1j")))

(define-public crate-utf16-ext-0.0.2 (crate (name "utf16-ext") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "~1") (default-features #t) (kind 0)))) (hash "0ks81nl0n412x49nni4zs1irvvnrfh3ail22rr1nymsh5mldlmcp")))

(define-public crate-utf16-ext-0.0.3 (crate (name "utf16-ext") (vers "0.0.3") (deps (list (crate-dep (name "byteorder") (req "~1") (default-features #t) (kind 0)))) (hash "1vkxhal2qspi2838dwdqyj5xk3c8h7qny0kxwws823pazawzmddv")))

(define-public crate-utf16-ext-0.1 (crate (name "utf16-ext") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "~1") (default-features #t) (kind 0)))) (hash "0xnsg8f477r90d04ycpnih05zywnms08v3pw6bx5di30mrv9qzxr")))

(define-public crate-utf16_iter-1 (crate (name "utf16_iter") (vers "1.0.0") (hash "08gnxq0651i7p5dscmlalv07kqs6wgqmcpqlc70x9iirbc5nkzdq")))

(define-public crate-utf16_iter-1 (crate (name "utf16_iter") (vers "1.0.1") (hash "0a8y1rwk2rrm9gj2px7r6nbshs3145rqrqnhs3lzsw02xw1vbnm0")))

(define-public crate-utf16_iter-1 (crate (name "utf16_iter") (vers "1.0.2") (hash "00aqjxflzh2y3ckpqklv4zfa269riwv2wxpmdk2crqmh4mckdwvx")))

(define-public crate-utf16_iter-1 (crate (name "utf16_iter") (vers "1.0.3") (hash "1da72yp5y9dbx4zz0y71h5vmswy0xdg1d9x5phx9j0v4wrcnf93a")))

(define-public crate-utf16_iter-1 (crate (name "utf16_iter") (vers "1.0.4") (hash "14m6v10i9jw3s7dv4zlfbvah8dmgrr12xkzwfvbi0ycfnxzqppsj")))

(define-public crate-utf16_iter-1 (crate (name "utf16_iter") (vers "1.0.5") (hash "0ik2krdr73hfgsdzw0218fn35fa09dg2hvbi1xp3bmdfrp9js8y8")))

(define-public crate-utf16_lit-0.0.1 (crate (name "utf16_lit") (vers "0.0.1") (hash "0gxcvlab52h415q5wsahhhrb985rklyc6ivcf6kassc4y2ymafyw")))

(define-public crate-utf16_lit-0.0.2 (crate (name "utf16_lit") (vers "0.0.2") (hash "0007qpyqi9qy1h7z1gb017d6xknmsx5mgp7zz3dv9zgb5lbjrir4")))

(define-public crate-utf16_lit-0.0.3 (crate (name "utf16_lit") (vers "0.0.3") (hash "1w2hlv55n0c8ri0n35kbllf3gn5v9mdxm1xh04xxahmw4dnn11nx")))

(define-public crate-utf16_lit-1 (crate (name "utf16_lit") (vers "1.0.0") (hash "0wgsgang5a7xczb94942fndddgv99n7d2phi376dh5h929gaghhp")))

(define-public crate-utf16_lit-1 (crate (name "utf16_lit") (vers "1.0.1") (hash "01vsgvzzdqxs5f1w367sw1bbh6zwyiqkbl2cybd5a3wgzg932cj0")))

(define-public crate-utf16_lit-2 (crate (name "utf16_lit") (vers "2.0.0") (hash "1d8h62sa9qhgnq02lqzbhgswhy0xchdmi6i2im9cyipkmr4nsmd8")))

(define-public crate-utf16_lit-2 (crate (name "utf16_lit") (vers "2.0.1") (hash "10f475pipkf7jkdd7kk8isg005k8z820sicz20zpy2z4k8mx1778")))

(define-public crate-utf16_lit-2 (crate (name "utf16_lit") (vers "2.0.2") (hash "1bm10sdzakmldc20sz7bb7m72sb1rmrvivfkq4wgzs0fh0m6sw0l")))

(define-public crate-utf16_literal-0.1 (crate (name "utf16_literal") (vers "0.1.0") (hash "0db883lvppyliyzkp4np7b312b5l4bmnva0d6zbl875v0vw99jjr")))

(define-public crate-utf16_literal-0.2 (crate (name "utf16_literal") (vers "0.2.0") (hash "1qks2hbvflcyyacxydv6vhxd0vzqjabkgha4wc59vprv6r466ziy")))

(define-public crate-utf16_literal-0.2 (crate (name "utf16_literal") (vers "0.2.1") (hash "0j1z5qirlgszh9qyybd4am8m26kg72knp05nw0f99svv9bz90vri")))

(define-public crate-utf16_reader-0.1 (crate (name "utf16_reader") (vers "0.1.0") (hash "1vj0fcznwbm4jxz7jdwpjqxizpamzy0q5anhdig0xf2c10jipyk4")))

(define-public crate-utf16string-0.1 (crate (name "utf16string") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "17py1wwyf34ws6d68ajraj2arimvf6798r60vn6bkdaqf458ybbk")))

(define-public crate-utf16string-0.2 (crate (name "utf16string") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "05njx6b3bpq461gxx40as3i07lvkdd15za27pw9dgm8jbvla2qhb")))

(define-public crate-utf16strings-0.1 (crate (name "utf16strings") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.42") (default-features #t) (kind 0)))) (hash "1ya47qdaz8ahx810f98zq3milpp0la2x2ffq68103w33207a5r15") (yanked #t)))

