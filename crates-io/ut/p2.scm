(define-module (crates-io ut p2) #:use-module (crates-io))

(define-public crate-utp2-0.1 (crate (name "utp2") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0ijixvm63baijwzgl8ydp9r9999bg1fbylib4yb5qm890x5006q9")))

