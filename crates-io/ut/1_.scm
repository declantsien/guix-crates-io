(define-module (crates-io ut #{1_}#) #:use-module (crates-io))

(define-public crate-ut1_blocklist-0.1 (crate (name "ut1_blocklist") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1nayzkn5ybi26j1yxm5c2hd4qlyda3vb9fpadba1v5gafxbpn90f")))

(define-public crate-ut1_blocklist-0.1 (crate (name "ut1_blocklist") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1ggcsb5zaijcgbk5vslq6khxkqaaxa2yx4l0j9r694bpbbdz8rw7") (yanked #t)))

(define-public crate-ut1_blocklist-0.2 (crate (name "ut1_blocklist") (vers "0.2.0") (deps (list (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0r2hc7cq2d3747fbapd5y7l8n1g1hdy2j51hgrm2nglx7zb54j4v")))

(define-public crate-ut1_blocklist-0.3 (crate (name "ut1_blocklist") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1q2awjyg5ymm1lpm2famkzlm6g298jq7lc8cgq0davddcvki4fyf")))

(define-public crate-ut1_blocklist-0.3 (crate (name "ut1_blocklist") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1k63p23mfdb6f69ijbbqk8crwcl21fmqhcffcra1drjm1l247y6w")))

(define-public crate-ut1_blocklist-0.3 (crate (name "ut1_blocklist") (vers "0.3.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0z2hs29nl14xv176dwg7b9pcaha2f9y2d9lm4gq04vpf87hn7bbk")))

