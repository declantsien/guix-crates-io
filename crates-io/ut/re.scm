(define-module (crates-io ut re) #:use-module (crates-io))

(define-public crate-utreexo-0.0.0 (crate (name "utreexo") (vers "0.0.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "blake2b_simd") (req "^0.5") (kind 0)))) (hash "1kybjg2h49svnvkww93spxcmlflhfrrssds0m93r467wzzsnl5k5") (features (quote (("std" "blake2b_simd/std" "bit-vec/std") ("default"))))))

