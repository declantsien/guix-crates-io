(define-module (crates-io ut op) #:use-module (crates-io))

(define-public crate-utop-0.1 (crate (name "utop") (vers "0.1.0") (hash "18mq5jksdc631ba588crjab02dvh2jv4zd6r9xf0ajcr7n64f46p")))

(define-public crate-utopia-0.0.1 (crate (name "utopia") (vers "0.0.1") (hash "0asdd05izsyqx9wy232mwvl986ihmyppiyis6xxyvzplggnvjdix")))

