(define-module (crates-io ut -d) #:use-module (crates-io))

(define-public crate-ut-dialog-0.1 (crate (name "ut-dialog") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "1a2hv1lgz708bx3b4zngj8zzrll06n2kdnd2cba7nhs2pf325gv1")))

