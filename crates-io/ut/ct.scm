(define-module (crates-io ut ct) #:use-module (crates-io))

(define-public crate-utctimestamp-0.1 (crate (name "utctimestamp") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gy8q42hhkvv472601w69752pbvibc5pw65zjp0q05v6yikbncb1") (features (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1 (crate (name "utctimestamp") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "03rr3plfcvzw5d6kxm4q5cj4a9jnfn6hgkwsqf4asrrf7y11cwpa") (features (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1 (crate (name "utctimestamp") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1zd6pf118bl8x9imi602q02z5bdh0i4dysjxf3b02gsnnl2023qp") (features (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1 (crate (name "utctimestamp") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0hhxdk0cgp5mf1nr8j4i2y2iyqdgs7j6bmnywg1cyxs93lxfn6qa") (features (quote (("serde-support" "serde") ("default"))))))

(define-public crate-utctimestamp-0.1 (crate (name "utctimestamp") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mw1l8jz5lwj4iwbk3gjp1v0zkd0ilj2c1xwgnh39d0i1nkfngff") (features (quote (("serde-support" "serde") ("default"))))))

