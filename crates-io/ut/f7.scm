(define-module (crates-io ut f7) #:use-module (crates-io))

(define-public crate-utf7-imap-0.1 (crate (name "utf7-imap") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0mc1gm6c58bhw1vqyxl8k7iilppi32m91x3iw4iv20407m77pk9n")))

(define-public crate-utf7-imap-0.2 (crate (name "utf7-imap") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0s7fh6mjwnfngy6jqnib37841bnxm9snpmlih0f9n6ppil5fz76p")))

(define-public crate-utf7-imap-0.3 (crate (name "utf7-imap") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1spzf5m7rwmg0s41qmqi73xdkwnnycwdlpsmy2lzz6qzrjms79s5")))

(define-public crate-utf7-imap-0.3 (crate (name "utf7-imap") (vers "0.3.1") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1jlpyc2zp7wxz3c87p2flaljyp2flhc260n1fncxxl0njgbn65z8")))

(define-public crate-utf7-imap-0.3 (crate (name "utf7-imap") (vers "0.3.2") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "0pczz3pazxj6ypnz1n5v4w24llqh501vdpq910gpdhhz4rjn6cjy")))

