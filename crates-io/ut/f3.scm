(define-module (crates-io ut f3) #:use-module (crates-io))

(define-public crate-utf32-lit-1 (crate (name "utf32-lit") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)))) (hash "00h932p5wwwdkc52vk6jazzr0h23hfap7yspr04hd6733i3964v7")))

(define-public crate-utf32-lit-1 (crate (name "utf32-lit") (vers "1.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0y1s26kkcbdwsxsj3cip8vn57j4qlcqisnzw20lajxr07nxivy8j")))

(define-public crate-utf32-lit-1 (crate (name "utf32-lit") (vers "1.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1zw9ik4crj2nyjgmljw6vz7v6p39lpnc71662qmwwkyrb71bidid")))

(define-public crate-utf32-lit-1 (crate (name "utf32-lit") (vers "1.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0vykydsrgjiha9yy1cmplgqw08sxbzwb9jnx8bljgwmdw3s2gdyw")))

