(define-module (crates-io ut f-) #:use-module (crates-io))

(define-public crate-utf-16-to-utf-32-0.0.1 (crate (name "utf-16-to-utf-32") (vers "0.0.1") (hash "1zsh2ndyihng0vq2d2rkqzigdflb7d7rj5gf0biv345sf8h99ybr")))

(define-public crate-utf-16-to-utf-8-0.0.0 (crate (name "utf-16-to-utf-8") (vers "0.0.0") (hash "0zjxqh2whnypqgjimrvc8a88jm3pj7drr6qj94hi4r5q72b63sk6")))

(define-public crate-utf-16-to-utf-8-1 (crate (name "utf-16-to-utf-8") (vers "1.0.0") (hash "1pv6zsdgikj0y6l2nj910lr82rd0dw6fyq4439sy3290fdif3jff")))

(define-public crate-utf-16-validate-0.0.0 (crate (name "utf-16-validate") (vers "0.0.0") (hash "1j8jjlx4352jq3byr8kx3a5gvryf62s6289a3ig9hd2li7hmg6jd")))

(define-public crate-utf-32-to-utf-16-0.0.0 (crate (name "utf-32-to-utf-16") (vers "0.0.0") (hash "01pg8qi66n186fnps2kffnx2riximd8ayjsi9i0jjhv5622qvx5j")))

(define-public crate-utf-32-to-utf-8-1 (crate (name "utf-32-to-utf-8") (vers "1.0.0") (hash "05wpw8xhqm72scpizwkw700vzc1inhc57p62b0dgf59vl2ag8020")))

(define-public crate-utf-32-to-utf-8-1 (crate (name "utf-32-to-utf-8") (vers "1.0.1") (hash "12dkpklh9hwbqzlq66l1s47072r0mqlgbqfm9cdfxm5d97l6rjdh")))

(define-public crate-utf-32-to-utf-8-1 (crate (name "utf-32-to-utf-8") (vers "1.0.2") (hash "09a673f9n2ymhvjdff0xr16dgnd76akymkqrf15pamf46jmvpkqv")))

(define-public crate-utf-32-to-utf-8-1 (crate (name "utf-32-to-utf-8") (vers "1.0.3") (hash "1nh1hmkk7h493qgwj2r60hg49v1cacx7fyapy9rqq4msmlxxwifk")))

(define-public crate-utf-32-to-utf-8-1 (crate (name "utf-32-to-utf-8") (vers "1.0.4") (hash "1yxdw4nvnygwxa34kdfnlz9vlq5zpz5pl03jcdasf5yhmfxkyd4x")))

(define-public crate-utf-32-validate-0.0.0 (crate (name "utf-32-validate") (vers "0.0.0") (hash "1dvbvfhjxkj34a84mc8ss4924hgizn8m4gya6314hnkw464202hx")))

(define-public crate-utf-8-0.1 (crate (name "utf-8") (vers "0.1.0") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "string-wrapper") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1j6ybsb89dqy2wahdvj3ibjy5njskg93p144dw9wrck3x3hqi5k9")))

(define-public crate-utf-8-0.2 (crate (name "utf-8") (vers "0.2.0") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "string-wrapper") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kikn027547fvmyqwnqhidngpc8qcb9jm70zw568jbsakwag9y0z")))

(define-public crate-utf-8-0.3 (crate (name "utf-8") (vers "0.3.0") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "string-wrapper") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0k1np73cy6ll3m9wlsppn1sk9zpfpb834jgmihrfv5n4f0dp3vh6")))

(define-public crate-utf-8-0.4 (crate (name "utf-8") (vers "0.4.0") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "string-wrapper") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0l0xy7scxncabnkwbim4a6vwwkkbc3vil8h9lnvfcccbyfw4xnw7")))

(define-public crate-utf-8-0.5 (crate (name "utf-8") (vers "0.5.0") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "string-wrapper") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "17kmql0w8vwaig1jl7c4fy0j34dg785qz6cs85y5bq1izhdvv9c0")))

(define-public crate-utf-8-0.6 (crate (name "utf-8") (vers "0.6.0") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "005lknlmmmvqnbqfzvhiz1bz83sg17r2jlxw3rnvaf0452xfkbm9")))

(define-public crate-utf-8-0.7 (crate (name "utf-8") (vers "0.7.0") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "02jpn2gm75f0byb02502ddqr6gd0x7mchqhghyxg4vvig4a4cn4x")))

(define-public crate-utf-8-0.7 (crate (name "utf-8") (vers "0.7.1") (deps (list (crate-dep (name "matches") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0mjs304h73fz7mdbffika6fbm9yrnasxbxv63pplib6707327ydn")))

(define-public crate-utf-8-0.7 (crate (name "utf-8") (vers "0.7.2") (hash "0p5pbm73a3qnm4203p3z0bsnqfilxq0bw9khq2vmq3f3nkx2s9pi")))

(define-public crate-utf-8-0.7 (crate (name "utf-8") (vers "0.7.3") (hash "0y2ibazjpqp8mb5npcd9wr2izp618q4fm111cf97dqzhb1xm0a1z")))

(define-public crate-utf-8-0.7 (crate (name "utf-8") (vers "0.7.4") (hash "0hail9ds825c0ai28w824phcvrw0syyg5q6waccvnc1nd5qmzcxs")))

(define-public crate-utf-8-0.7 (crate (name "utf-8") (vers "0.7.5") (hash "1iw5rp4i3mfi9k51picbr5bgjqhjcmnxx7001clh5ydq31y2zr05")))

(define-public crate-utf-8-0.7 (crate (name "utf-8") (vers "0.7.6") (hash "1a9ns3fvgird0snjkd3wbdhwd3zdpc2h5gpyybrfr6ra5pkqxk09")))

(define-public crate-utf-8-to-utf-16-0.0.0 (crate (name "utf-8-to-utf-16") (vers "0.0.0") (hash "15n1wzhkrrldw0d61340yzbp7ak83pvg5wfjbc95ikm89n74wj8r")))

(define-public crate-utf-8-to-utf-32-0.0.0 (crate (name "utf-8-to-utf-32") (vers "0.0.0") (hash "1srw6f3f561d5zg2v5k5bdkw4hl22q7mvxb9vdxsd1p6778426c8")))

(define-public crate-utf-8-validate-0.0.0 (crate (name "utf-8-validate") (vers "0.0.0") (hash "0x11zb402zpxzap0j2xpql886c17ni3iid68ga2krr81f99d72k1")))

(define-public crate-utf-cli-0.1 (crate (name "utf-cli") (vers "0.1.0") (hash "0whdmd36gi06pll2frlpvakn3b6al87v6kgr3ipbdz6n5541lnq3")))

(define-public crate-utf-crawler-0.1 (crate (name "utf-crawler") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1dbklp5m15mwv87alr9lq0jfhvw61jg2nr7wfslh82ipm7a000kq")))

(define-public crate-utf-crawler-0.1 (crate (name "utf-crawler") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "14h9v57r0zk1xjmayks5pj275wjv8g2h0rgm1jwwywpc775292wg")))

(define-public crate-utf-crawler-0.1 (crate (name "utf-crawler") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "11dhyi6m5cvawb9mr5lakk7qaqys555wppy53h7r5dy0w0fjd7s9")))

(define-public crate-utf-railroad-1 (crate (name "utf-railroad") (vers "1.0.0") (hash "1r9y3nr5r9799d3y3wjaylca5fzqk9iv7rykh809vwcipjxm8kdi")))

(define-public crate-utf-railroad-1 (crate (name "utf-railroad") (vers "1.1.0") (hash "07ksbaawgc0ja7zrxqcvqha29d9xqiiq5w5yqmdgh9lq3xjvkqxl")))

