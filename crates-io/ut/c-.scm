(define-module (crates-io ut c-) #:use-module (crates-io))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.0") (hash "1jlmsz7xyicwa57b34ikhcg6qlzpsjzk9mr0h390m88ws5f5xbkm") (yanked #t)))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.1") (hash "121lfw8vzm206y0sy6zdyhbhcr2c6kn5kiw79bvma9chqcjb6p8p") (yanked #t)))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.2") (hash "1hx94dzi1slpd57wh84fw84lgmi1rkkk06wkrbxv0nzvk202gix9")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.3") (hash "0n8ljl5zgjvgql3jig6vcmh8jjnmxn6y7m0ijvbi2q4c9dpkvxx8")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.4") (hash "0qpq16hsjypgc0bhhngz32zdws8gs4rgc90gwg2a3sanrvqgl2zc")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.5") (hash "1hh05mfkdxssr2vycr1w5pqvvny0fwx5n4xxbwnhdhvrjid8z5lw")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.6") (hash "0phf6qg91i42dccv2chplbx6rwkv71x0zpln7mp4lbq810i60hag")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.7") (hash "0pi5nnlr3dhrjbl3xfmj16v99qp3i62lqx88gihzychzbgfv6qn3")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.8") (hash "031qxzd84xm9g2n5g8bb5b27h12msli90kd9v8dbd0ybgbpirazs") (yanked #t)))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.9") (hash "1hcxli3q429bph4s3fljny0d1za2r6bagf0izi198af0l98v4bnd")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.10") (hash "0nm6486gwnwzplqa8p7dnm0arc46z78pnmyrj72z84j4wf47z1xf")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.11") (hash "1xh4j9zyjypjdddwx32mp4bs0k8crddbh02zsdr1jz3fp460n980")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.12") (hash "076zzmbhn64ddwxw93bysw0mgf5qai6jbsxz40n9lw0sf6yqj0fx")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.13") (hash "0lisn92rb4bi94q7r5k31h344hdqfql5sk4ch6yv7hii5gk1m3m2")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.14") (hash "00cs6md4wcpy3s0kkn8af3l42a5g7l4gfvy8671jwgjwhygsxm8d")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.15") (hash "0c23hmzfyjg7kxsb4rbiw2wmksrsrzqdynvq5xw631qvb8cam0xd")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.16") (hash "13m97sjlak4pq3xrmwhx840ljrvn8slrh57a090bxnfh77cmvlzm")))

(define-public crate-utc-datetime-0.1 (crate (name "utc-datetime") (vers "0.1.17") (hash "0533bda9xp2i0mljs0jhlb3b1s0cvns647rcmv1zdz318rkkijrx")))

(define-public crate-utc-dt-0.1 (crate (name "utc-dt") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (features (quote ("add" "mul" "from" "into"))) (default-features #t) (kind 0)))) (hash "16z26gykisb65j6i5zhw71pkgpb2jmf71im9sfrb0jp3vim151rl") (features (quote (("std" "anyhow/std") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-utc-dt-0.1 (crate (name "utc-dt") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (features (quote ("add" "mul" "from" "into"))) (default-features #t) (kind 0)))) (hash "02xwcs1vg95r8bzjhdxq5sixnqcfkdhwfylgfvcxg9dy3nvc6bba") (features (quote (("std" "anyhow/std") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-utc-dt-0.1 (crate (name "utc-dt") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (features (quote ("add" "mul" "from" "into"))) (default-features #t) (kind 0)))) (hash "10g4b9xd8axxvkds4syvvy6z2v8s7s5c1wmlqzvdzwn2998lv57c") (features (quote (("std" "anyhow/std") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-utc-dt-0.1 (crate (name "utc-dt") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1") (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (features (quote ("add" "mul" "from" "into"))) (default-features #t) (kind 0)))) (hash "0072wzjgfvkyl0g431x0qkrvj6i2chaxdl0qyq3ipg9n79szqpw4") (features (quote (("std" "anyhow/std") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-utc-dt-0.2 (crate (name "utc-dt") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (kind 0)))) (hash "1137kl47g5xxrqbpxn7d2fbfrdbpc63m87f8kc8adidisdya35bw") (features (quote (("std" "anyhow/std") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-utc-dt-0.2 (crate (name "utc-dt") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (kind 0)))) (hash "029wy7ryharccic8wsjicjsxmnw3kwiycrhc3y8q3f95gf1h0a2i") (features (quote (("std" "anyhow/std") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-utc-offset-0.1 (crate (name "utc-offset") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.5") (features (quote ("formatting" "local-offset" "parsing"))) (default-features #t) (kind 0)))) (hash "0amnbc3qsvmp0gg531qcr7p484kassp55ddapfcbxz39qdkpcw5h")))

(define-public crate-utc-offset-0.2 (crate (name "utc-offset") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.5") (features (quote ("formatting" "local-offset" "parsing"))) (default-features #t) (kind 0)))) (hash "045d93dhfk62l29ihnm5fx7zpzadp3rrs1yva24dbd9wcfjrf2zj")))

(define-public crate-utc-offset-0.3 (crate (name "utc-offset") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "time") (req ">=0.3.20, <0.3.24") (features (quote ("formatting" "local-offset" "parsing" "macros"))) (default-features #t) (kind 0)))) (hash "0lfdinwbrirnphmc2zwlpy3s3mgvmndv1p3crgz0hgc9srziixfp")))

(define-public crate-utc-offset-0.4 (crate (name "utc-offset") (vers "0.4.0") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.20") (features (quote ("formatting" "local-offset" "parsing" "macros"))) (default-features #t) (kind 0)))) (hash "1zi863nj29p8d1qm9q0ln84vhpvi42ljdjsa0ka4ly6lw7nvwxxz")))

