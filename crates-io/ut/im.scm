(define-module (crates-io ut im) #:use-module (crates-io))

(define-public crate-utime-0.1 (crate (name "utime") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hc5mr29avgwhaymb0474aspd1bh0wm2521q21xj4s32a0y8ssam")))

(define-public crate-utime-0.0.0 (crate (name "utime") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "00h2d5dj88wyzqqarcd6mm4gghvknbyn2s93iq7yknc6yxxf2ykp")))

(define-public crate-utime-0.0.1 (crate (name "utime") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nlv3giqnx7p42n4346kqr990czvc3p1wlwnr6dnc92l81rdsmgi")))

(define-public crate-utime-0.1 (crate (name "utime") (vers "0.1.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hn0zln84jhcszsp700a9yx8cz5idfp3cd27fnaxrx33his3a4yp")))

(define-public crate-utime-0.1 (crate (name "utime") (vers "0.1.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jp0m0ka2wjlar47yskv54p6kbnckqpap49sbvs5d4fv8lpw50sg")))

(define-public crate-utime-0.1 (crate (name "utime") (vers "0.1.3") (deps (list (crate-dep (name "kernel32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i6g8kakdgzv8njga8zyg7863q1i1k5xracn2r4p2s6hh6c56yk3")))

(define-public crate-utime-0.2 (crate (name "utime") (vers "0.2.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "16xxyz3f3qrvdia2fw30z2bs27yp0d1i5b3d646csfasgbghv72a")))

(define-public crate-utime-0.2 (crate (name "utime") (vers "0.2.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0akr32rd2qiiwdbnkwd6hs4zfy76791xlqgwh9h5cp515iamhl05")))

(define-public crate-utime-0.2 (crate (name "utime") (vers "0.2.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (kind 0)))) (hash "12xpv6dqi53ng3q59q9mf8zrs278haipm668fdadcax3n6xrpczx") (yanked #t)))

(define-public crate-utime-0.3 (crate (name "utime") (vers "0.3.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "151l62dbhxkfkvxmi5r5clii7zfsp5q1ia4g0m26c515v5w4bayg")))

(define-public crate-utime-0.3 (crate (name "utime") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fileapi" "minwindef" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1vrw36v1s9yhsqi04vg9raasp70m2vzkbk68vb5jzldbbv3a1fli")))

(define-public crate-utimeseries-0.1 (crate (name "utimeseries") (vers "0.1.0") (deps (list (crate-dep (name "byte_conv") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cast") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.1") (default-features #t) (kind 2)))) (hash "02cd8cmyh99zx5981h0swhx7dgmk6waimsjczqg0wwrq3i2rj53r") (yanked #t)))

(define-public crate-utimeseries-0.1 (crate (name "utimeseries") (vers "0.1.1") (deps (list (crate-dep (name "byte_conv") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cast") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.1") (default-features #t) (kind 2)))) (hash "0cz0llm6mj5cnbvp8mr8dsmkv9bxy1cv7g21ddwdcay0j8p3n43p")))

