(define-module (crates-io ut wt) #:use-module (crates-io))

(define-public crate-utwt-0.4 (crate (name "utwt") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "utmp-raw") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.6") (default-features #t) (kind 0)))) (hash "1sia8w6x8vc8ibd397g4fspjqw6vqcy25vnb1wb1pwb394d1qdd4")))

