(define-module (crates-io fw if) #:use-module (crates-io))

(define-public crate-fwif-0.1 (crate (name "fwif") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "19adx4gvygg93kz6gkb6zyxmm0np019szbqy1fxq61249y8fs2ll")))

