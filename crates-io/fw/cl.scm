(define-module (crates-io fw cl) #:use-module (crates-io))

(define-public crate-fwcl-0.1 (crate (name "fwcl") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0fvmsp95kqk4x6c5kh2lfz24y4m9czwb0rz6dbbbbab9aivcr5x9")))

(define-public crate-fwcl-0.1 (crate (name "fwcl") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0s00bakc1rym9l2ad6paizaq4ky8k22yj9nlsnpf011ihd7mfg2y")))

