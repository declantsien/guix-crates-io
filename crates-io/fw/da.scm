(define-module (crates-io fw da) #:use-module (crates-io))

(define-public crate-fwdansi-1 (crate (name "fwdansi") (vers "1.0.0") (deps (list (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "0c3lkbc4m413ainw0szxq1j8vwn85wrl33vrpxcqmnqmvzdy1bz7") (yanked #t)))

(define-public crate-fwdansi-1 (crate (name "fwdansi") (vers "1.0.1") (deps (list (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1hqhhq4bxnmip8yh90jv9pfy0k4ll7gn684nxzzkg3gng984rp9l")))

(define-public crate-fwdansi-1 (crate (name "fwdansi") (vers "1.1.0") (deps (list (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "027jz2x5fbi6rskic8sd6xx0mn03a7dnhwkpyz8hamg8gxwgbh88")))

