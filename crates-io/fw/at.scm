(define-module (crates-io fw at) #:use-module (crates-io))

(define-public crate-fwatch-0.1 (crate (name "fwatch") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1385s5jz22g8khi2kxwi6l7l3d9y4kdh3ll8hscikbqnbzdc3bir")))

(define-public crate-fwatch-0.1 (crate (name "fwatch") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "162pakiqm1pyw52p8nsn1x5diwzgb6axb04sw0sz36b7skcqqy2x")))

(define-public crate-fwatch-0.1 (crate (name "fwatch") (vers "0.1.2") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1nsp43fbbi39fhxdqcpjr8mg3bykmcyafc56dw8j2p517c0pmcsy")))

(define-public crate-fwatch-0.1 (crate (name "fwatch") (vers "0.1.3") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0jiwvyi3bir0gshkmblp9pnk7ay7p5cpdhg0vb192ffd9jg2bpdr")))

(define-public crate-fwatch-0.1 (crate (name "fwatch") (vers "0.1.4") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1y5156niz0pxn796hvqzh31421l3q1qzb1cx4hys0lfcv3cmxx5s")))

(define-public crate-fwatch-0.1 (crate (name "fwatch") (vers "0.1.5") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1p4nj3bcwx8pvpwgmjk15ri7vhz28lwcmkcdvp2cj5jypixak320")))

(define-public crate-fwatchd-0.1 (crate (name "fwatchd") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.117") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "syslog") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "0wbxndlh77pyv4bddhslzmf5if8fr5m6p3h345519ffzsnrnmiag")))

(define-public crate-fwatcher-0.0.1 (crate (name "fwatcher") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "0vizla8gnrhxgy4xdcvw17mlw0ijfkzgqgsx07lgs7xcda04gpfy") (yanked #t)))

(define-public crate-fwatcher-0.0.2 (crate (name "fwatcher") (vers "0.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "0m5msqi4ch795a38anziihg64azn9l3d5b4a7yck9phrdjllrd41") (yanked #t)))

(define-public crate-fwatcher-0.0.3 (crate (name "fwatcher") (vers "0.0.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "1msh8yn1vzb2srky5r3q1ylp52ywfiqayzywh9qj9pjpjc8bq263") (yanked #t)))

(define-public crate-fwatcher-0.0.4 (crate (name "fwatcher") (vers "0.0.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "0q0fzc2c25h1g96bhmi6ja1j47pdhh3ffzlhjr0wfpxnidfaf9hi") (yanked #t)))

(define-public crate-fwatcher-0.1 (crate (name "fwatcher") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "0701gpjadaznsj76xb6pp01j88rzx0cvmq8vg6wa1h6vrz1bh3r6") (yanked #t)))

(define-public crate-fwatcher-0.2 (crate (name "fwatcher") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "1ssq2d8varzd9rqfzn55hjk4l1il7ywqqmqdqcvvkxgk8qzxgv2b")))

(define-public crate-fwatcher-0.3 (crate (name "fwatcher") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "171qp53rhynfzpc28g1jg558252jbs4wcif0d2pgppi7bjhbnkds")))

(define-public crate-fwatcher-0.3 (crate (name "fwatcher") (vers "0.3.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "1vq0rfay2f61m6d3c2fzqkvmhc0ad9zizfqwi4c6i1v9q1053dnb")))

(define-public crate-fwatcher-0.3 (crate (name "fwatcher") (vers "0.3.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "05wf7nax91lz1iz47i2gs87zyg7pnkmbn21w1rrp9v2a357fk796")))

(define-public crate-fwatcher-0.4 (crate (name "fwatcher") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "0sfdmlna7cm1h4c8717kbh0c4xpv04k4h6d81axc5sz15id1ai2i")))

(define-public crate-fwatcher-0.4 (crate (name "fwatcher") (vers "0.4.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "1fdrzn7lanmlqmy033q14gm7ngw32p7309k7692azmgxnw4mzq40")))

(define-public crate-fwatcher-0.4 (crate (name "fwatcher") (vers "0.4.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)))) (hash "16n016zlrj8wxcvaf7i745izd182yd95brsf3d8568pgvfwa1fk9")))

