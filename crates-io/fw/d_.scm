(define-module (crates-io fw d_) #:use-module (crates-io))

(define-public crate-fwd_ad-0.1 (crate (name "fwd_ad") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0d1i8qp1g7g6p8h95iwsnzpfmqzcwzbhzi6v067dwka472x3vdx9") (features (quote (("implicit-clone") ("default") ("bench" "criterion"))))))

(define-public crate-fwd_ad-0.2 (crate (name "fwd_ad") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1zkvsdjb6rnk7lqiihrzfmcx991f6k0ig1666gr6q2rsgdbkp249") (features (quote (("implicit-clone") ("default") ("bench" "criterion"))))))

