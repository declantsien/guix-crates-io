(define-module (crates-io fw uf) #:use-module (crates-io))

(define-public crate-fwuffgrep-1 (crate (name "fwuffgrep") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "164av5byw8jb57ljmwd8snblrr3xn1xcazn7zmcfyfkw6hkvjwj7")))

