(define-module (crates-io fw al) #:use-module (crates-io))

(define-public crate-fwalker-0.1 (crate (name "fwalker") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "14pniswkc1jgqrw7sk7mg6f7vn54prb8vbkdsxcz9sni9lhl4mx4")))

(define-public crate-fwalker-0.2 (crate (name "fwalker") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "06qwzyd6ngsk70cygyrr4n1qmpn3sa6xjfvw4p8qz9h487rhj3wd")))

(define-public crate-fwalker-0.3 (crate (name "fwalker") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.14") (default-features #t) (kind 0)))) (hash "056b7wvz343pfa8hagyadmhfjxfizq0zjlhxrpjrp4y5b3qym0i1")))

(define-public crate-fwalker-0.3 (crate (name "fwalker") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15") (default-features #t) (kind 0)))) (hash "0cvnvrbs7smiagsprcdny6pqqbmhpj2zkaily49sl9lgvc9xg07m")))

(define-public crate-fwalker-0.3 (crate (name "fwalker") (vers "0.3.2") (deps (list (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req ">=0.15.0, <0.16.0") (default-features #t) (kind 0)))) (hash "0f6wz6qjghi481z5gqvssbhqf2c1svxgmk54r73ggaqvk27zk8a9")))

(define-public crate-fwalker-0.3 (crate (name "fwalker") (vers "0.3.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15") (default-features #t) (kind 0)))) (hash "0h78hmbsy0d14awgdhdf6qy68wn1gsxg2gvlmyk2hqr2622y9bwh")))

(define-public crate-fwalker-0.3 (crate (name "fwalker") (vers "0.3.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15") (default-features #t) (kind 0)))) (hash "1wbwgaw6zy9f59vqawizhn92vd1vdjm5v6h8jmg5lvw0bvqv2i6p")))

(define-public crate-fwalker-0.4 (crate (name "fwalker") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.15") (default-features #t) (kind 0)))) (hash "1kxb33glcpp2vh9n28n5lqmq2p8rmdmvsjybbqb8r862i7xz0mnh")))

(define-public crate-fwalker-0.4 (crate (name "fwalker") (vers "0.4.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.16") (default-features #t) (kind 0)))) (hash "17q5p6pnfb9rr4zsll2l0q9g2v2y62dlnqfh6pbhg3jq7b941ag4")))

