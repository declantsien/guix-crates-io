(define-module (crates-io fw _e) #:use-module (crates-io))

(define-public crate-fw_env-0.1 (crate (name "fw_env") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "scan_fmt") (req "^0.1") (default-features #t) (kind 0)))) (hash "0v6c8j4dj6gsx76a6wj8lxf70q4yypvavcjf48lyj9rhymigg1b3")))

