(define-module (crates-io fw dt) #:use-module (crates-io))

(define-public crate-fwdt-0.1 (crate (name "fwdt") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1jzai0yfjjj5vxy5s8ypvjh3h0cmd9rrz8h1436jaycrp5a60ik9")))

(define-public crate-fwdt-0.1 (crate (name "fwdt") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "023cr81ahpqamqca84j1lap820b87jvfh55v0y29qyd3yqh8gk8z")))

