(define-module (crates-io op ma) #:use-module (crates-io))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.0") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0wd2lj3aqip7wa8k1cmrj2bfzzc41hchn13fdrn9w27xzb1nlvvc")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.1") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1apad6r29zdn8nc560ipm3l2922nbbwhlgdprw52g8q755kir3jr")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.2") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0in48ls6rnxcdij4mdprmrpzjav6a3jxfjpccx8c1k7d8hx928w2")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.3") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1g3lidyb0liwajhz0ywrshk2d9azszgm5ncd32yr4mz7sikp1nxc")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.31") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0pkpq02rgi3cdl1z0zgixchs48gfxi3db32mfrh6cgjz4f7zidii")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.32") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "06pr9bxwmxsvnkr4ah92na97bwvnkn4d5ydrzvyflhkizmp9r71z")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.33") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "01xrh379hmn80ckp6q1wxg915a3w5w2mc8qmhxvrlhamkzsgdcy7")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.34") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "176jx5qcpssk631204q0d389k0dspc38mwhzr2vd8vigl0zrr0mm")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.35") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "046zr7gnn057cibdngqsws365x8ihk3b83rafhfii4cjgvwnx3s4")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.36") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1jdwa86p0j2wjflsqz455d4h0hkzp90mplnqhyg5cm3521p9qcsw")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.37") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0bf2vjqab9jia5skkrqzjwhb6dwhp0vmzbmaijpsnrj6s6dzhw64")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.38") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1dqwqmrx625jga561z8g0l45k79c30hyfrs97hrwkp2airdznshd")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.39") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1xhhqq2qjxybqjm6n9jwxpipz8wyz1czz8n0342jbjihziqj29yk")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.40") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1whqq93x6agdd4wrfv9lr4nzwqj1rkn0gdyzr9chm3mlxxbw90bk")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.41") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "02d29mqcaifgwd9icqw7sz8alnfn4yir9in1j4zg3v0nqy8047g2")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.42") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0jjbr29n7k90aa0ihw3n951jg8d0fns7nw78z5jl0dm0jl4adr9x")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.43") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0ysyh8icsgknvj3vax2crglkwrpqczj5psv3jnm6k614vskwkysg")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.44") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "003xiaxgb9478ychxp4fr1wxvbwxph5mzvg6raa7i129yxgcpd5z")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.45") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0x98agsyiiqwacx0sfn7hsz0bq63wkwfwiva12jxgn0d8ri1lr7s")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.46") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0scp5ajf6lc8mdpq6hddm0c1iizys71wzqc6hp0vxlnk8cqsqbmg")))

(define-public crate-opmap-1 (crate (name "opmap") (vers "1.0.47") (deps (list (crate-dep (name "drain-while") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hamcrest2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0wrc7wzhyjvvjhv2fx244070zlgvflf3fibhpf66wrc7kqzs00p0")))

(define-public crate-opmark-0.0.1 (crate (name "opmark") (vers "0.0.1") (hash "12k9bm5ksf6vf2vifj6cywjifrqvn33j1qahr1scrvmhg371fj49")))

(define-public crate-opmark-0.0.2 (crate (name "opmark") (vers "0.0.2") (hash "1cplg2zq28gk9y57hlqdzq6mk5hdz58xpbm07vn8f58rh8dwh727")))

(define-public crate-opmark-0.0.3 (crate (name "opmark") (vers "0.0.3") (hash "1371xv4gw12rbqkbdc8vw5mghxpw2xnp6nab5irjfygbw3x67qj8")))

(define-public crate-opmark-egui-0.0.1 (crate (name "opmark-egui") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "opmark") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0mhssp1l7jv6zcqfzircxf1dfjjdwvqgvrw66yll7mf9npyfxbiz")))

(define-public crate-opmark-egui-0.0.2 (crate (name "opmark-egui") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "opmark") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "10z4y0m60j8m56gd6d2xgywv6xg2l2s2qisav8g8lmd5jq02hfwp")))

(define-public crate-opmark-egui-0.0.3 (crate (name "opmark-egui") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "opmark") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1sfppqx9k9i6c9mcdg4fc1q6msb40hq74sfpah8i2jyv8r50bcyx")))

(define-public crate-opmark-egui-0.0.4 (crate (name "opmark-egui") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "opmark") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0yiwry7y7w5ngw78ga9q61qwqcd11kzl2npplqq9b5glwy8n2r09")))

(define-public crate-opmark-egui-0.0.5 (crate (name "opmark-egui") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "opmark") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0434r21xwb9b2q0r09831sjp4h183xvysapzp1srsxab804qbv7v")))

(define-public crate-opmark-egui-0.0.6 (crate (name "opmark-egui") (vers "0.0.6") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "opmark") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1yksnbbgin1pwss0p1b0smhdc1w6zzq67scz206jzvk085l2gm1f")))

(define-public crate-opmark-egui-0.0.7 (crate (name "opmark-egui") (vers "0.0.7") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "font-kit") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "opmark") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "19wljyrdsgf9lnx2zss7jbb9d5dv8wnv8lhkar4a4xr1kazqpksp")))

