(define-module (crates-io op oo) #:use-module (crates-io))

(define-public crate-opool-0.1 (crate (name "opool") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "1z9vm4dfqp2sw9pkrwpcg5zr41k9352yldklndld2w9bqyvlpcr5")))

(define-public crate-opool-0.1 (crate (name "opool") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "1gp2qjvjlfgxgjc0qcj9vri76zim9q7i9rdq4dpmkirmhhfvd3f3")))

