(define-module (crates-io op tb) #:use-module (crates-io))

(define-public crate-optbuilder-0.1 (crate (name "optbuilder") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.13") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.39") (default-features #t) (kind 0)))) (hash "13ii55mw24l0al13y3npdxhh9f7md23b7masszmapa26xaphd23n")))

(define-public crate-optbuilder-0.1 (crate (name "optbuilder") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.13") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.39") (default-features #t) (kind 0)))) (hash "05ga6wwybbi5nfwhdxa5b7j2w3891wszfy99pp988m7hfd2wv6wf")))

(define-public crate-optbuilder-0.1 (crate (name "optbuilder") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1wgwlg8ga7bzjqxfb6b3sy4gs3zskjrr0d2xp1bip96ms2308nvd")))

