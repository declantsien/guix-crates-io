(define-module (crates-io op do) #:use-module (crates-io))

(define-public crate-opdot-0.1 (crate (name "opdot") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0qf00nf6q680rq7g0bp0b73cwfz3r2cxq1j0jfrks793w2c2nv10") (yanked #t)))

