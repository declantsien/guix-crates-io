(define-module (crates-io op tp) #:use-module (crates-io))

(define-public crate-optparse-0.1 (crate (name "optparse") (vers "0.1.0") (hash "0apv7nyf958516s4f2fkd6lhkcrq9wr6vn71302a87ji34w4zm8x")))

(define-public crate-optparse-0.1 (crate (name "optparse") (vers "0.1.1") (hash "0adkc3n2sgg57hvcz2g515siyr84qrsbk8hvkjw5s92qf1bdm501")))

(define-public crate-optparse-0.1 (crate (name "optparse") (vers "0.1.2") (hash "17qmkq7knpcqga21nd81yd7zp511m1iv0z6m5nk2ammyca0hs6ny")))

(define-public crate-optparse-0.1 (crate (name "optparse") (vers "0.1.3") (hash "0blf9xs83y84ssvbb07w72drdfl50dsr9w7fjpn3rahnk0l0ccf4")))

(define-public crate-optparse-0.1 (crate (name "optparse") (vers "0.1.4") (hash "0fn3ivs227yypmrvzb8dy2rankfv5cx39lfz2bf0svd2dxwqn1yz")))

(define-public crate-optparse-0.1 (crate (name "optparse") (vers "0.1.5") (hash "1l48x5waxdgy6f1kv9mcxwzvflmmqh98w4nxpk7r479y8ign5h1n")))

