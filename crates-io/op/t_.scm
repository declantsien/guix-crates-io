(define-module (crates-io op t_) #:use-module (crates-io))

(define-public crate-opt_args-0.0.1 (crate (name "opt_args") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09jc0j6d99xk8h5x5h40r98xxx6s6az0bad266kz5aprvnjfb8nh")))

(define-public crate-opt_args-0.1 (crate (name "opt_args") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17pn54z7x878v6nsn1xip2w52zsmzcbi4bbhlsp8411bh2q554ww")))

(define-public crate-opt_args-1 (crate (name "opt_args") (vers "1.0.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bpn5wzwgcxfnwninhfi9mxkxsqyy0834fhdal5rw10cw4l9w4np")))

(define-public crate-opt_args-2 (crate (name "opt_args") (vers "2.0.0") (deps (list (crate-dep (name "deluxe") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "derive-syn-parse") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gaf85ysrjb02a72wd5xs6bn2s5c842iwpprni1r68wyfbf2rinm")))

(define-public crate-opt_reduce-1 (crate (name "opt_reduce") (vers "1.0.0") (hash "13iv9mzvg7m17q77byjjsfqpxhl5g236rmv1awrl0czqkc5dsw9l")))

(define-public crate-opt_solver-0.1 (crate (name "opt_solver") (vers "0.1.1") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (features (quote ("rand" "std"))) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.3") (features (quote ("rand" "std"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("num-complex" "rust_decimal" "auto-initialize" "num-bigint"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.29.1") (features (quote ("rand" "std"))) (default-features #t) (kind 0)))) (hash "1j81m0vhdwpfgjqrli0jf9yrl2vmxicq0rk9p2lhv494d3rzy7sz")))

(define-public crate-opt_struct-0.1 (crate (name "opt_struct") (vers "0.1.1") (hash "1apddjpwnpmjlkb0fc26701d32rmaw2alsxn07pvqmpf8v922a8c")))

(define-public crate-opt_struct-0.1 (crate (name "opt_struct") (vers "0.1.2") (hash "14cm9lmxvyzfl96m34hj9s90wd65bvi4jksks9mn15i2z57nygp6")))

(define-public crate-opt_vec-0.1 (crate (name "opt_vec") (vers "0.1.0") (hash "0ns52175a7gwj2843g39lr0dykn9yp1rd4w66c6fswrmszz9nizk") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.56")))

(define-public crate-opt_vec-0.1 (crate (name "opt_vec") (vers "0.1.1") (hash "1mym014jz6qkmdf2x5a762vfw5pncnznblxdnr206c2ydi6y1j02") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.56")))

(define-public crate-opt_vec-0.1 (crate (name "opt_vec") (vers "0.1.2") (hash "1v7zfgnf8y3idqlgigdg55ig6bcf7lwx69nj03kqbycsflh4iawj") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.56")))

