(define-module (crates-io op bo) #:use-module (crates-io))

(define-public crate-opbox-0.1 (crate (name "opbox") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0p6vskqysvfxhkmyira4sq3k1d1hprd6kqnnp4n8dflp5g5hy658")))

(define-public crate-opbox-0.1 (crate (name "opbox") (vers "0.1.1") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1cd1q5yxfdrfkf79d71rn4cg1lfxylacrr8ccxgm9bbxaxl3k3pl")))

