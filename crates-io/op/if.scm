(define-module (crates-io op if) #:use-module (crates-io))

(define-public crate-opifex-0.1 (crate (name "opifex") (vers "0.1.0") (hash "0cgqgmz73jx2qd68r6hrz7c4wp7v0a7jz6q6cy9d5ngp1zmxih1d") (yanked #t)))

(define-public crate-opifex-0.3 (crate (name "opifex") (vers "0.3.0") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "14r7jazv0a6ygcmrpgak2bqxn1cj3pfprjna4f0cqdzj21hkrhnb")))

