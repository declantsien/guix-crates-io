(define-module (crates-io op #{8d}#) #:use-module (crates-io))

(define-public crate-op8d_lexemizer-0.1 (crate (name "op8d_lexemizer") (vers "0.1.0") (hash "1bjnhdad80lgkn0mj79i1zdxp87ssaxc82xvzv9rqn69g6dv5zqr")))

(define-public crate-op8d_lexemizer-0.1 (crate (name "op8d_lexemizer") (vers "0.1.1") (hash "0a45diw5iapwihv48cmb5qc8pwzxzks7v4wn5w5jg0rz6wvsnkk3")))

