(define-module (crates-io op em) #:use-module (crates-io))

(define-public crate-opemssh-0.1 (crate (name "opemssh") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yasna") (req "^0.1") (default-features #t) (kind 0)))) (hash "10v695l4mynmbb0awcl5csg320x6a0qb3rcm6alfsk4gnni20z3f")))

(define-public crate-opemssh-0.1 (crate (name "opemssh") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yasna") (req "^0.1") (default-features #t) (kind 0)))) (hash "0m9pbm5ihsyf0i32r2n5xzpr2n7v6d3d7vsx4d1a3i8858ldhhj4")))

