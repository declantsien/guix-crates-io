(define-module (crates-io op tc) #:use-module (crates-io))

(define-public crate-optcollection-0.1 (crate (name "optcollection") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "10s7g4c35fwq2m1xkfsm185dna6ry4hmi0lb4arlv91digqm2x70")))

(define-public crate-optcollection-0.1 (crate (name "optcollection") (vers "0.1.1") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "1v9lxip9kswywvaxvjligzy3bnvmp56cpf3dxhapaslz73p19ph0")))

(define-public crate-optcollection-0.1 (crate (name "optcollection") (vers "0.1.2") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "19w7lrfqazjgcdac7rn1z4qld1c0p1crvazfqm324hs6his7gjfa")))

(define-public crate-optcollection-0.1 (crate (name "optcollection") (vers "0.1.3") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "1ij23476afkja5hdfswqcqmi3dwcjxs4157ia4y4yqxv2y91n5km")))

(define-public crate-optcollection-0.1 (crate (name "optcollection") (vers "0.1.4") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "1wimngirxjvfpg1n0za85vgf6lagpcrkmgi3985qyhvj4hv0lfca")))

(define-public crate-optcollection-0.1 (crate (name "optcollection") (vers "0.1.5") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "1splgccaxq34llx3knxmwc7mb7qs4rc8alyx5d93a0ci8xp0pnl1")))

(define-public crate-optcollection-0.1 (crate (name "optcollection") (vers "0.1.6") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "18h7hr29wywqvvhkzn5rd04p6pqy9z6phnyv9i3yirgz7g9ainwr")))

