(define-module (crates-io op co) #:use-module (crates-io))

(define-public crate-opcode-0.0.0 (crate (name "opcode") (vers "0.0.0") (hash "1jalkxnncmqv3vx62f6xa20yj3l21qsh78zk0hahlmvv78q7a6lq")))

(define-public crate-opcode-macros-0.1 (crate (name "opcode-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.46") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "0l2kji4sghgbzhk73sa5ickmnv85pb007xgndk1jxkl3r2fz1y13")))

(define-public crate-opcodes-0.0.0 (crate (name "opcodes") (vers "0.0.0") (hash "1nnrwzmywbklm4ky7zryhv26vva618v655ai5q46lsfk1zgzycyw")))

