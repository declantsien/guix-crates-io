(define-module (crates-io op re) #:use-module (crates-io))

(define-public crate-oprec-0.1 (crate (name "oprec") (vers "0.1.0") (deps (list (crate-dep (name "petgraph") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sdpp27day5ipliwfq2xa0mvhisdl96a0yk0lggp09m9jlaw1chv")))

