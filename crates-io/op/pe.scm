(define-module (crates-io op pe) #:use-module (crates-io))

(define-public crate-opper-0.0.0 (crate (name "opper") (vers "0.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vhlh31frqnkvw79l1blrbkjrv9s4x4h2qa3fr90g2m16zcq5lvx") (features (quote (("std") ("full" "default" "serde") ("default" "std")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

