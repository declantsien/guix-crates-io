(define-module (crates-io op t3) #:use-module (crates-io))

(define-public crate-opt300x-0.1 (crate (name "opt300x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "111jqrraaixjmlgwh04h25rarcrlk83j436y9xb2wigpz1kczflg")))

(define-public crate-opt300x-0.1 (crate (name "opt300x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1") (default-features #t) (kind 0)))) (hash "01vg3xjc5cflc74k4nqi7ag4sx6xcwgms4prillh2nqzvly5fj53")))

