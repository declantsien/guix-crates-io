(define-module (crates-io op d-) #:use-module (crates-io))

(define-public crate-opd-parser-0.1 (crate (name "opd-parser") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.22") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "125yv4kbas730m83v1wv1pmzyid7hhm3s50mw75z1agn09cypybb")))

(define-public crate-opd-parser-0.1 (crate (name "opd-parser") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.22") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1wva4nf8clkiilv2p2wc79kkpwxlp8g1787wjly5zd5azpcsalqb")))

(define-public crate-opd-parser-0.2 (crate (name "opd-parser") (vers "0.2.0") (deps (list (crate-dep (name "glam") (req "^0.22") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "02dii2a1a05cq6a8lk0c0ypiahi1mdyn53a549dvmc40fhqp07f0")))

(define-public crate-opd-parser-0.3 (crate (name "opd-parser") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1slmhgph6lvq8z7385rg11ag5lp9d4vcrhfz81x3sdmxvapmqkh7")))

(define-public crate-opd-parser-0.4 (crate (name "opd-parser") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0w0p3c75wfvg188y6mpdvv1p7qix51vqm2zs6n5fjgxjqjzlg3f2")))

(define-public crate-opd-parser-0.5 (crate (name "opd-parser") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "17pyglcx3iw551216x87jfzc5364c294243x2crbvxa3rr4afkf2")))

