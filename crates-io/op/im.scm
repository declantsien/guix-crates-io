(define-module (crates-io op im) #:use-module (crates-io))

(define-public crate-opimps-0.1 (crate (name "opimps") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lc5hmiapp2z8fq4viwm4w84wd6k7i1zy3df5sz8mibjg5g3p1cb")))

(define-public crate-opimps-0.1 (crate (name "opimps") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cka7lgb89jzzc2790irl09v3v49v2v5wlw9cw625iyhay5cqrpm")))

(define-public crate-opimps-0.1 (crate (name "opimps") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1av886xpmwgdw0ph1ayzziks4k8mp8xf601nr0zz69n3vaid3g7a")))

(define-public crate-opimps-0.1 (crate (name "opimps") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07qbw3y8dpbc4rpwsrh7i935s3rqcndd0g0rfq2bm1nrbmdhl0c8")))

(define-public crate-opimps-0.1 (crate (name "opimps") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gld4sfg9wgdzb94gs65s1xhwalrxzmk3xhgdmy3icn7nbsgmsb8")))

(define-public crate-opimps-0.2 (crate (name "opimps") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j74y0iys7jpcpp2xgwhnnfillpr9ydcpi4l7ylw745zgv8dxy3g") (yanked #t)))

(define-public crate-opimps-0.2 (crate (name "opimps") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xvrdwyvbw80n0fx2ajqc86jx0ik15x8dcs9xx6p4kr6n1lsg1qg") (yanked #t)))

(define-public crate-opimps-0.2 (crate (name "opimps") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0a82gak6kz5iwykj2znl1l2swx4f00ydqqlrmi8jxzks9bkanzc5")))

(define-public crate-opimus-0.1 (crate (name "opimus") (vers "0.1.0") (hash "1yn5nvwialnm6r9p13gq3cmnrb1c3fsri5iy3a3q1s66qns3yjnn") (yanked #t)))

(define-public crate-opimus-0.1 (crate (name "opimus") (vers "0.1.1") (hash "1vgg6ci0n2mfl1jz32zyalcqh5003kcwf8y6zg1ns2nwjw8azl36") (yanked #t)))

