(define-module (crates-io op a-) #:use-module (crates-io))

(define-public crate-opa-macros-0.1 (crate (name "opa-macros") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.2.4") (default-features #t) (kind 0)))) (hash "04p2czij3siraqybfylg139prr990cg3v42xzrcq6v5zwm4kgqcq") (yanked #t)))

