(define-module (crates-io op ca) #:use-module (crates-io))

(define-public crate-opcalc-0.2 (crate (name "opcalc") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.69") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.13") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("console"))) (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4.5") (optional #t) (default-features #t) (kind 0)))) (hash "10vhwp5rlg8ax2rnm7njnmxws7r6kfnkdnpa5xm2h8lkfykdxv56") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-opcard-0.0.0 (crate (name "opcard") (vers "0.0.0") (hash "0r1xbraarzhj791vspxf6ymg62s739kwf2snx50z02gyaai17gxa")))

