(define-module (crates-io op am) #:use-module (crates-io))

(define-public crate-opam-file-parser-0.1 (crate (name "opam-file-parser") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19.4") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0ca1qs1v4b7j6yjfvaxyvs3hgkwn0p0x7va4drina0wcgxagzg47") (yanked #t)))

(define-public crate-opam-file-rs-0.1 (crate (name "opam-file-rs") (vers "0.1.1") (deps (list (crate-dep (name "lalrpop") (req "^0.19.4") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0d4z0x03f6n5yn6yqb97fdbpz6rkx3545n2815n48bp00kpawgyq")))

(define-public crate-opam-file-rs-0.1 (crate (name "opam-file-rs") (vers "0.1.2") (deps (list (crate-dep (name "lalrpop") (req "^0.19.4") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0yqqzwpybk5yvhd2mxms9r90hdlzj60bs8galbpild7bvmai6cpi")))

(define-public crate-opam-file-rs-0.1 (crate (name "opam-file-rs") (vers "0.1.4") (deps (list (crate-dep (name "lalrpop") (req "^0.19.4") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0cxakhmc9bd4bn9gf2gfxg91xdqj298i9wyn0aw0qwmv68brjkg8")))

(define-public crate-opam-file-rs-0.1 (crate (name "opam-file-rs") (vers "0.1.5") (deps (list (crate-dep (name "lalrpop") (req "^0.19.4") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0qs81i727vp12745di3w22iyjx6qh5lxhbbp6031gj86czigvjad")))

