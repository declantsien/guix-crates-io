(define-module (crates-io op e-) #:use-module (crates-io))

(define-public crate-ope-rust-web-server-0.1 (crate (name "ope-rust-web-server") (vers "0.1.0") (hash "09jmha5j2b93bvx20rvv4kc1lhv2arbisx85wg9cyh01429znrz5")))

(define-public crate-ope-rust-web-server-0.1 (crate (name "ope-rust-web-server") (vers "0.1.1") (hash "1ibl47jqpd678xghrrj6vdlj3mpzmip3bqfr7qdz786lmi905cbh")))

(define-public crate-ope-simplified-0.1 (crate (name "ope-simplified") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "block-modes") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "ctr") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.55") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 0)))) (hash "0vbnydkrhz1frchz09wnigw3p32srsnlk9qgni854w7lx9xv69zz")))

