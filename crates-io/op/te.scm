(define-module (crates-io op te) #:use-module (crates-io))

(define-public crate-optee-teec-0.0.1 (crate (name "optee-teec") (vers "0.0.1") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "optee-teec-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ichfpc9z3i9sl00k34p83fkcqs0b509i1crva04v3kzxq5d9y71")))

(define-public crate-optee-teec-0.1 (crate (name "optee-teec") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "optee-teec-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (default-features #t) (kind 0)))) (hash "0rgds8w1py6ml4yka1ridzzyrz8cc5j1l4vkakdfj8hh4fd74s30") (yanked #t)))

(define-public crate-optee-teec-sys-0.0.0 (crate (name "optee-teec-sys") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)))) (hash "12y8fbnv9x1sassh2dx35ay0jkqdj2qjip0kpawn2q6mb9ymi5q8")))

(define-public crate-optee-teec-sys-0.0.1 (crate (name "optee-teec-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)))) (hash "04a5bc5g203g5y9kkr3vaq4d8c0f227ambvwkaqdmbgyqw8lp7c4")))

(define-public crate-optee-utee-0.0.0 (crate (name "optee-utee") (vers "0.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.49") (default-features #t) (kind 0)) (crate-dep (name "optee-utee-macros") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "optee-utee-sys") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0x97zlbv0gdd3sl9jv25ggp2vswnxa4pizpv1x3x77si5k2k50hm")))

(define-public crate-optee-utee-0.0.1 (crate (name "optee-utee") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.49") (default-features #t) (kind 0)) (crate-dep (name "optee-utee-macros") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "optee-utee-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0gydl5z8ds4nmaaa2m7wr2bxw1awr1i22ygfda7gvixa21l29nm1")))

(define-public crate-optee-utee-macros-0.0.0 (crate (name "optee-utee-macros") (vers "0.0.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r42bgvr9slh4i4hhm9qllzwjiw3a5q4z5hqcgm8gsp5yc66dk6k")))

(define-public crate-optee-utee-macros-0.0.1 (crate (name "optee-utee-macros") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1l2qai67pwwvgm9c5s8c0xi7mbh6sky968vvpppi56qkb5dk0jsv")))

(define-public crate-optee-utee-sys-0.0.0 (crate (name "optee-utee-sys") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.49") (default-features #t) (kind 0)))) (hash "00l0bn409p4jzdp2dz9j7pnkwmdzdf35rybsjpggxk2a6g901v01")))

(define-public crate-optee-utee-sys-0.0.1 (crate (name "optee-utee-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.49") (default-features #t) (kind 0)))) (hash "0nbwph91x4phsrfqmsvsdk4p9lp2yxcnjlhhq9hl71s0mjiynpjg")))

(define-public crate-optempty-0.1 (crate (name "optempty") (vers "0.1.7") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "1v4nxljygysk29713armrim03dbq353vgnxqigipi2544n5x3sd4")))

(define-public crate-optempty-0.1 (crate (name "optempty") (vers "0.1.8") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "1i94yl57rzfkxsdzryxr3v087h0d9qbff3ldr7p8jws7ac9yg4zx")))

(define-public crate-optempty-0.1 (crate (name "optempty") (vers "0.1.9") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0gskfdr5jklk39vzc5dc6qxi38xzhmkszqwkn2jqa4dbgddn2qvl") (features (quote (("serdejson" "serde_json") ("default"))))))

(define-public crate-optempty-0.1 (crate (name "optempty") (vers "0.1.10") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0wkdb18vzyj4fazwvmr6snkmdyj1mwq47lhvxxy7pk0jlipwzqw0") (features (quote (("std") ("serdejson" "serde_json") ("default" "std"))))))

(define-public crate-optempty-0.1 (crate (name "optempty") (vers "0.1.11") (deps (list (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1zdypkkcwhl0l2lx4lac42mzn8inqws2v60z68ncflx00x4q5mpk") (features (quote (("std") ("serdejson" "serde_json") ("default" "std"))))))

(define-public crate-optempty-0.1 (crate (name "optempty") (vers "0.1.12") (deps (list (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)))) (hash "1a4brck2r1qbmdvp4qxcfszapzd604vrjswif4bmxg86jnff0961") (features (quote (("std") ("serdejson" "serde_json") ("default" "std"))))))

(define-public crate-optempty-0.1 (crate (name "optempty") (vers "0.1.13") (deps (list (crate-dep (name "query_map") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)))) (hash "0cc55n3j046i2bvy1xdc59axxqm1p1kkf799m0iqb9d77mbm7gf7") (features (quote (("std") ("serdejson" "serde_json") ("querymap" "query_map" "std") ("default" "std"))))))

(define-public crate-opter-0.1 (crate (name "opter") (vers "0.1.0") (hash "1dmfs55achmr1kndkwv32b7iv55sdd931g6bacrc1q2y3kvcbzw3")))

(define-public crate-opter-0.2 (crate (name "opter") (vers "0.2.0") (hash "14h43y2rs9x4i6smshxa3jhj3di29d37qa0g49x9jzixwp91afcl")))

