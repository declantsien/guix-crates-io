(define-module (crates-io op -a) #:use-module (crates-io))

(define-public crate-op-api-sdk-0.1 (crate (name "op-api-sdk") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.8") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "0x76c5ai87kj34mxcrjgv2m9axr945ba8d6j110m349jzfhf10jx")))

