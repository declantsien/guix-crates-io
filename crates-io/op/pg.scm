(define-module (crates-io op pg) #:use-module (crates-io))

(define-public crate-oppgave-0.1 (crate (name "oppgave") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0978k8f9m3na8x7bvcjq1kzf883pki0mvm62ab9sks8infw0a6lp")))

