(define-module (crates-io op us) #:use-module (crates-io))

(define-public crate-opus-0.1 (crate (name "opus") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "opus-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1cmx765mwvrv65a555q5wdggz2j9w3zq6c2lqs10xxgsz7ha1ljr")))

(define-public crate-opus-0.1 (crate (name "opus") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opus-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0whw1hxhly6yh30g8plz9zp78qmxgd6qx4j0jcnvx094hhia5pn6")))

(define-public crate-opus-0.2 (crate (name "opus") (vers "0.2.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opus-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1nyhx841v0s2shpc8f7rham32gwfzal9y74r3lm8bl1rnjda47jw")))

(define-public crate-opus-0.2 (crate (name "opus") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opus-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "12y1brz0sa4ia7cfjrpxfv9ad4zkkz59ajlcp1g6drpivakmk42y")))

(define-public crate-opus-0.3 (crate (name "opus") (vers "0.3.0") (deps (list (crate-dep (name "audiopus_sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13jyszydy9p15k1vnzbbshscvriqznm9d7gmbzjrhzja4ydl09k5")))

(define-public crate-opus-cmake-sys-1 (crate (name "opus-cmake-sys") (vers "1.0.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "0g91ff6pnjwlwnls9g1by8q30i4fgnb63kl6pq3jmdqq98q238cc") (links "opus")))

(define-public crate-opus-cmake-sys-1 (crate (name "opus-cmake-sys") (vers "1.0.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "13a8mxzzi7g3w3wn0yrpiigcgza7szbkxcfsn4q6nj1bn1j5sfzn") (links "opus")))

(define-public crate-opus-cmake-sys-1 (crate (name "opus-cmake-sys") (vers "1.0.2") (deps (list (crate-dep (name "cc") (req "^1.0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "1xf4x1avl5nwqracsn45z8f62jwmdzddvc4x83z2rzfpyvycswdg") (links "opus")))

(define-public crate-opus-cmake-sys-1 (crate (name "opus-cmake-sys") (vers "1.0.3") (deps (list (crate-dep (name "cc") (req "^1.0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "10s43qqqggrfvr3cf6j92blyhd1qs9caf9xy32zwndw34lmrsxp9") (links "opus")))

(define-public crate-opus-cmake-sys-1 (crate (name "opus-cmake-sys") (vers "1.0.4") (deps (list (crate-dep (name "cc") (req "^1.0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "0505rj0am8h86i9vwhbcfq41xwm87ll23zglc47jzdzjgsx5jzp2") (links "opus")))

(define-public crate-opus-cmake-sys-1 (crate (name "opus-cmake-sys") (vers "1.0.5") (deps (list (crate-dep (name "cc") (req "^1.0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "1qma6yn7ypw0vsvfvhpbfzhiaf54ls8q8iz971h423yqk0y0p80j") (links "opus")))

(define-public crate-opus-cmake-sys-1 (crate (name "opus-cmake-sys") (vers "1.0.6") (deps (list (crate-dep (name "cc") (req "^1.0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "1bxdrlj69iljx2n1pknh1w82mxlhqgca1r4p4i5v41xxzqxc7a13") (links "opus")))

(define-public crate-opus-db-0.1 (crate (name "opus-db") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1id6nzyj2x7ywm8vxa8x6mc10j03rac4nkv1nakxvf0ff1chxhvc")))

(define-public crate-opus-mux-0.0.0 (crate (name "opus-mux") (vers "0.0.0") (deps (list (crate-dep (name "ogg") (req "^0.8") (default-features #t) (kind 0)))) (hash "0brpkd7jq1bn53ahmi7jca8lhi6m6xhi89wlq8i6chpq5l73cvc2") (features (quote (("std") ("default" "std"))))))

(define-public crate-opus-mux-0.1 (crate (name "opus-mux") (vers "0.1.0") (deps (list (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "magnum-opus") (req "^0.3") (default-features #t) (kind 2)))) (hash "02a9hda6xb55np9z66vrfni1hah4c4cvvvpgnh882akxx46dma34") (features (quote (("std") ("default" "std"))))))

(define-public crate-opus-mux-0.1 (crate (name "opus-mux") (vers "0.1.1") (deps (list (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "magnum-opus") (req "^0.3") (default-features #t) (kind 2)))) (hash "11qwjcwinh8xbj89gd4xph7xci1sz9sdc3czlv43shfp2msgzhn8") (features (quote (("std") ("default" "std"))))))

(define-public crate-opus-oxide-0.0.1 (crate (name "opus-oxide") (vers "0.0.1") (hash "1ymfivl8blirs5wprlphh75jjm4xarm70rg61slbx5sh5s8rhgcp")))

(define-public crate-opus-parse-0.0.1 (crate (name "opus-parse") (vers "0.0.1") (deps (list (crate-dep (name "derive-error-chain") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1xry6x8k8lz6afs22c27j9msdsgf42sqldiv2q7sjnhzy5v1xcfc")))

(define-public crate-opus-parse-0.0.2 (crate (name "opus-parse") (vers "0.0.2") (deps (list (crate-dep (name "derive-error-chain") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1x6k1hxm3pcvzkbwpzmcgrh441dkx0hgqs8d2fdk7r81ph9rrinq")))

(define-public crate-opus-parse-0.0.3 (crate (name "opus-parse") (vers "0.0.3") (deps (list (crate-dep (name "derive-error-chain") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0hz0haq638zvak3c8aag0aa77wzwrg6xpvx14lmxx2ng97cf4fnk")))

(define-public crate-opus-rust-0.1 (crate (name "opus-rust") (vers "0.1.0") (deps (list (crate-dep (name "opus-cmake-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0015kfvy854yv8817pbjz9dk0wi0pyps0p64vkg0ig9cccyc2p3q")))

(define-public crate-opus-rust-0.1 (crate (name "opus-rust") (vers "0.1.1") (deps (list (crate-dep (name "opus-cmake-sys") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "16m6ylr9wgh5glav26ag87jpyp523qp9kzqycrf2ma0x932c9vrx")))

(define-public crate-opus-sys-0.1 (crate (name "opus-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1cyh9akm30z29vw2g5clff7f83sjw3qz2qf0hm4nybh6ycvxn2rb")))

(define-public crate-opus-sys-0.1 (crate (name "opus-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "03dihjb0pci6vnal2rq50bpmfqj8dcdancy7mhldpm4mng7q09w7")))

(define-public crate-opus-sys-0.1 (crate (name "opus-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "08z6lnjdb3wzhk8lb3jziinjdryvi34p23cjzw69xs8iyqz6iwsr")))

(define-public crate-opus-sys-0.2 (crate (name "opus-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "0r9fbzlkf04mxcwwywfv5h82xyqyx25qnsjrqfqwprl6682pza82")))

(define-public crate-opus-sys-0.2 (crate (name "opus-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "1v5n4lzshkzfhc2di6amjzk4wkjnb96wdcb6lhprgxw2yjab5n7s")))

(define-public crate-opus-sys2-0.1 (crate (name "opus-sys2") (vers "0.1.0") (hash "069r6r2ca7y0h9xhhs3m5faz5kkj751hhpqh7b0yscginmp1npyv")))

(define-public crate-opus_headers-0.1 (crate (name "opus_headers") (vers "0.1.0") (hash "1yb759ahj4kkga4pw207isggaynf55d81w7ynp9j9217ajbdh9n1")))

(define-public crate-opus_headers-0.1 (crate (name "opus_headers") (vers "0.1.1") (hash "025c5w663rncxq42qzsdjdr4jk52gzghp0060k3yzqjmf03qhb37")))

(define-public crate-opus_headers-0.1 (crate (name "opus_headers") (vers "0.1.2") (hash "0q2b81b0p6g1xxwd90rkwyj58zxcim7r8dn55dy3j4gi8wwrkfxg")))

(define-public crate-opus_tools-0.1 (crate (name "opus_tools") (vers "0.1.0") (deps (list (crate-dep (name "cli_test_dir") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "docopt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pbdm7ff7mxmxspgyg46z8jzphkra1j37b666m37frxadn5spk69")))

(define-public crate-opus_tools-0.1 (crate (name "opus_tools") (vers "0.1.1") (deps (list (crate-dep (name "cli_test_dir") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "docopt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dy6rxwn457zv0kddrywan938mvscwq0x9hqzsfd0fgrn5wmc7xf")))

(define-public crate-opus_tools-0.1 (crate (name "opus_tools") (vers "0.1.2") (deps (list (crate-dep (name "cli_test_dir") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "docopt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)))) (hash "0h7w9hq9bc140gfv4sx4xg1c6x3zfbsd4ak1iqb32a8c0rn4bq5q")))

(define-public crate-opus_tools-0.1 (crate (name "opus_tools") (vers "0.1.3") (deps (list (crate-dep (name "cli_test_dir") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "docopt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)))) (hash "19sibvfpv144lphjxysw0wjj2xmz7yy5rg42hn2fqv2cia7mmgra")))

(define-public crate-opusenc-0.1 (crate (name "opusenc") (vers "0.1.0") (deps (list (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "opusenc-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04qm9y0vds4cg5x370yzszhha8mawr8pgsl485n3jb6mpzbdni4c") (features (quote (("encoder-options") ("default"))))))

(define-public crate-opusenc-0.2 (crate (name "opusenc") (vers "0.2.0") (deps (list (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "opusenc-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "074yyxjb520fnkpklng9bynvdivkzdl3262l376q9gv9ck29zx79") (features (quote (("encoder-options") ("default")))) (yanked #t)))

(define-public crate-opusenc-0.2 (crate (name "opusenc") (vers "0.2.1") (deps (list (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "opusenc-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0bk9v81bfv3i2kx615anb21kf2cq7592lj8dvciacacbginmyp4l") (features (quote (("encoder-options") ("default"))))))

(define-public crate-opusenc-sys-0.1 (crate (name "opusenc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "0i4sizqxbmwfzzprcmigm2szjdai47wd85iizm8k20ic3ilz1pi8")))

(define-public crate-opusenc-sys-0.2 (crate (name "opusenc-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)))) (hash "06yqz8di6b6qwmkprxc339q1bf3j282c82abc90plakcqmssr493") (yanked #t) (links "opusenc")))

(define-public crate-opusenc-sys-0.2 (crate (name "opusenc-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)))) (hash "022azvv8hxlvbn64am2rann2gpam4mnd3x78nx15in4lgg1g1kai") (links "opusenc")))

(define-public crate-opusfile-ng-0.1 (crate (name "opusfile-ng") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "num-derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1if0pgv7am04b9dvs8ak6zymgsggahq5s0f8bl2vd1qx6w9c8h3z")))

(define-public crate-opusfile-rs-0.1 (crate (name "opusfile-rs") (vers "0.1.0") (deps (list (crate-dep (name "enum_primitive") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "opusfile-sys") (req "*") (default-features #t) (kind 0)))) (hash "10drx1plkxkklhvbgjgqy6vw0vm9rcn6gm2601grycp9h0f8a38c")))

(define-public crate-opusfile-rs-0.1 (crate (name "opusfile-rs") (vers "0.1.1") (deps (list (crate-dep (name "enum_primitive") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "opusfile-sys") (req "*") (default-features #t) (kind 0)))) (hash "1s31yxni58lq018mynqfnb88ijzvsvdiy3ss2y0g6swbj3wg3339")))

(define-public crate-opusfile-rs-0.1 (crate (name "opusfile-rs") (vers "0.1.2") (deps (list (crate-dep (name "enum_primitive") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "opusfile-sys") (req "*") (default-features #t) (kind 0)))) (hash "1z0l7ci6fxpdrr7a4yc3q3g7wwl4xvy1di65nsvg1x120fjswll2")))

(define-public crate-opusfile-rs-0.1 (crate (name "opusfile-rs") (vers "0.1.3") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "opusfile-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jknyy9yfd10mvw5yv0h5q986wmx4avz1qb7y9xqsd78800q32pa")))

(define-public crate-opusfile-rs-0.1 (crate (name "opusfile-rs") (vers "0.1.4") (deps (list (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opusfile-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wwgymr7mdp3zy33kgh4j7m4sfifqvbb894kv0hi0sm79mhl68qx")))

(define-public crate-opusfile-sys-0.1 (crate (name "opusfile-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ogg-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "opus-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "15aqnyz4k9akc3gh7bfh27rvakz5rfy6979g81llsx1dk6cdc9mg")))

(define-public crate-opusfile-sys-0.1 (crate (name "opusfile-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ogg-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "opus-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1a9zhdymjn5yyqbdkd73427m9x53d4i2j06kwawjk9nkvwzaqz2h")))

(define-public crate-opusfile-sys-0.2 (crate (name "opusfile-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ogg-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "opus-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.8") (default-features #t) (kind 1)))) (hash "1dvwv40rp6pgzd3pa8zdjxla07823xqmj7yd38zfy8rd7h7ndy4r")))

(define-public crate-opusic-sys-0.1 (crate (name "opusic-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.43") (optional #t) (default-features #t) (kind 1)))) (hash "0g15i76p2mf9xjv387lgf8y429cl9djb6qha11hrhdjal1l18lya") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.2 (crate (name "opusic-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.43") (optional #t) (default-features #t) (kind 1)))) (hash "0mgs89fykd9mz76lm8y38wmkyv01dk53vddlni0g89lv39q6y9ck") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.2 (crate (name "opusic-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.43") (optional #t) (default-features #t) (kind 1)))) (hash "0rikmm8zkzvyahpr3wl1in9s8sn0ddm35w0nqz3x0a49pm6mjc96") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.2 (crate (name "opusic-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.43") (optional #t) (default-features #t) (kind 1)))) (hash "1wcznjy85jicz51pazlr00fz5wvmnycl4xinw6ilqp5zp5ragvi3") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.2 (crate (name "opusic-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.43") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "00r1g0pxwnal5rm0zs70gdlnm9h224r81kzy41q16lzcdj21230h") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.3 (crate (name "opusic-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.54") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1gz97r17wnr1lr23a7qm6gxl4gyps60bkpwd84lhqghj6wsnzyd2") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.3 (crate (name "opusic-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.54") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1hwf6w7my9v46446mc6bip77wbwhcggsxl6ghq5pnc95nlg8a0ja") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.3 (crate (name "opusic-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.54") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0cwqpn9113ajv3mc8s985ih2y1p5w9sb2n04cxcwxv3hv9zj3km9") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.3 (crate (name "opusic-sys") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.54") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "18xnghshrr7mh481m9k8bi2qgqlbg8bbmpl0g9x9nhrimjn72asa") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.3 (crate (name "opusic-sys") (vers "0.3.4") (deps (list (crate-dep (name "bindgen") (req "^0.54") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "107mrqrr30kqz0w24j4yqkamg22p8jbriqxaiyk3d9sx49411593") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.3 (crate (name "opusic-sys") (vers "0.3.5") (deps (list (crate-dep (name "bindgen") (req "^0.57") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "06jzm5nh32jmcg5scnzvsvhdjwa99m5lddwq28cmqhx68a0a7bxk") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.3 (crate (name "opusic-sys") (vers "0.3.6") (deps (list (crate-dep (name "bindgen") (req "^0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1xal7gl81d5was0049qv8cg0b7vrak3053xs3lj3g807rr9fgb2y") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.4 (crate (name "opusic-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0i7q87439f89yzz1np3wi9wjh886yf1988zlc63qivf2147y6wv9") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.4 (crate (name "opusic-sys") (vers "0.4.1") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "07kc7mrykgrjd02b9x9hqygd0anz67zgpvksy448ylv5yy2b8gwz") (features (quote (("build-bindgen" "bindgen"))))))

(define-public crate-opusic-sys-0.5 (crate (name "opusic-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "044glgfl933y05x2yl34parqsz69ca7jl3k8gy95sc5cimnjchg9") (features (quote (("build-bindgen" "bindgen"))))))

