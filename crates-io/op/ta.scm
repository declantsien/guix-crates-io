(define-module (crates-io op ta) #:use-module (crates-io))

(define-public crate-optarg2chain-0.1 (crate (name "optarg2chain") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1xkwllhpx0d0fs4q6sd1d9irqlgywmqimm298vz8m5nvs75vhpr3")))

(define-public crate-optargs-0.1 (crate (name "optargs") (vers "0.1.0") (deps (list (crate-dep (name "optargs-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pis1g8zzw7fnmw2prrkl63af3jq5vg2bjrqgzak501ml8ll74k7")))

(define-public crate-optargs-0.1 (crate (name "optargs") (vers "0.1.1") (deps (list (crate-dep (name "optargs-macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0zifgr4h9y35902v63xyi029w6q8p31nr1r2krph1gsd6155rziz")))

(define-public crate-optargs-0.1 (crate (name "optargs") (vers "0.1.2") (deps (list (crate-dep (name "optargs-macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "059l631lgdky3ffmrpyyxvavrdc1d0bslhik12crndwarb2kkivw")))

(define-public crate-optargs-macro-0.1 (crate (name "optargs-macro") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.19") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.11") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1g3dj16cc4ym578nswxibwy2km5d3dq5byibpgfgdf2vf2nfqvpd")))

(define-public crate-optargs-macro-0.1 (crate (name "optargs-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.11") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "192z3g5dc1jpvzyrqgdx5rz3cnlaydkjxl2b4cwhq6alr6knw3qj")))

(define-public crate-optargs-macro-0.1 (crate (name "optargs-macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.11") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0s9rjysz9n2c86iiymnfybwbhdzrqrzjhb42m3kjxz8ridiidihm")))

