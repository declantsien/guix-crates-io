(define-module (crates-io op s-) #:use-module (crates-io))

(define-public crate-ops-core-0.1 (crate (name "ops-core") (vers "0.1.0") (hash "0shrx2qk3nxdprhc7bvk2vy39nb2ymw7iciwbw8d61c0b6nr4j3s")))

(define-public crate-ops-core-0.2 (crate (name "ops-core") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mxgqy5wg0d4rw2cp1lf3cbvds99818j3j63512y44wawddlcmqd")))

(define-public crate-ops-derive-0.1 (crate (name "ops-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0wkai92ywl1mvydvbd6rjjv1f10679yrrqm6l6n8ig43bl9r9qvi")))

(define-public crate-ops-derive-0.1 (crate (name "ops-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0h9rjh2c319vcrp5z7qf2cny2fqxn41kmvi1nflhm946baw1zyz1")))

(define-public crate-ops-grpc-0.1 (crate (name "ops-grpc") (vers "0.1.0") (deps (list (crate-dep (name "ops-core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.2") (default-features #t) (kind 1)))) (hash "1pfvs26gn2p0j514k9q9wsjyk6nramgx4bfja9b2hrgffiydp1ij")))

(define-public crate-ops-janus-0.1 (crate (name "ops-janus") (vers "0.1.0") (deps (list (crate-dep (name "janus") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ops-core") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wgni980cadsph7g6raif3c7wi27s7m2jbzhnlsyxffhimfifwiy")))

(define-public crate-ops-mel-0.6 (crate (name "ops-mel") (vers "0.6.0") (deps (list (crate-dep (name "melodium-core") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0nr203m6gbq6dlm1pc0czc33nn6s3ckyfrnsnhlkb8plp0kg94i7") (rust-version "1.60")))

(define-public crate-ops-mel-0.7 (crate (name "ops-mel") (vers "0.7.0-rc1") (deps (list (crate-dep (name "melodium-core") (req "=0.7.0-rc1") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "=0.7.0-rc1") (default-features #t) (kind 0)))) (hash "0plrdkbm85hakvjsifg9ax143aaacqda5i7894j20n0bpxdpk8w6") (features (quote (("real") ("plugin") ("mock")))) (rust-version "1.60")))

(define-public crate-ops-mel-0.7 (crate (name "ops-mel") (vers "0.7.0") (deps (list (crate-dep (name "melodium-core") (req "=0.7.0") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "=0.7.0") (default-features #t) (kind 0)))) (hash "0vk4frgnvfbmvsw2xl1qjfa2nx9vcm1cx4h03lxwvsda2d06qcsf") (features (quote (("real") ("plugin") ("mock")))) (rust-version "1.60")))

(define-public crate-ops-mel-0.7 (crate (name "ops-mel") (vers "0.7.1") (deps (list (crate-dep (name "melodium-core") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "melodium-macro") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1fx00v945cyhcvlsnjq8ih1622ybxfir5mfvs0ik07j3r7nrrpps") (features (quote (("real") ("plugin") ("mock")))) (rust-version "1.60")))

(define-public crate-ops-mongodb-0.1 (crate (name "ops-mongodb") (vers "0.1.0") (deps (list (crate-dep (name "mongodb") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ops-core") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f7mx6b40ji8q5g08438dylnj9dz6nisip90m7jmbf896xsxd69y")))

(define-public crate-ops-on-fn-0.0.1 (crate (name "ops-on-fn") (vers "0.0.1") (hash "1fh2fk5i3frrx2mdlsc18chzi1y3kb48l8nym2nkiygad73h0wc3")))

