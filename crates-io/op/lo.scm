(define-module (crates-io op lo) #:use-module (crates-io))

(define-public crate-oplog-0.1 (crate (name "oplog") (vers "0.1.0") (deps (list (crate-dep (name "bson") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mongodb") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "192hmzrcyn63rr35z86q8fq9d4f2rrm5d0zzyh69sskfn43c9vbf")))

(define-public crate-oplog-0.2 (crate (name "oplog") (vers "0.2.0") (deps (list (crate-dep (name "bson") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mongodb") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0ihsfcnpjmivjz92nr7d1kc32qx4qf12r0sz121x5l10pyzz4c50")))

(define-public crate-oplog-0.3 (crate (name "oplog") (vers "0.3.0") (deps (list (crate-dep (name "bson") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mongodb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "10g27ffpwhgfnc1yl6dqyj2k2lrpx3kzd0234px7byrvbsnza556")))

