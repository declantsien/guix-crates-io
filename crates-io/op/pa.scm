(define-module (crates-io op pa) #:use-module (crates-io))

(define-public crate-oppai-0.0.1 (crate (name "oppai") (vers "0.0.1") (hash "1lckay860yasyamdhmpn1fnarmin154vkqimp4yg8w3jf2wbczia")))

(define-public crate-oppai-rs-0.1 (crate (name "oppai-rs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "1l6av7r2qzxp3nshg9fsqclslpx9b3qcx4n6394186j9b4khqgma")))

(define-public crate-oppai-rs-0.2 (crate (name "oppai-rs") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "0nqh4fpkq23s142sz99084jybjqg8kghjd9k014c198dambk652g")))

(define-public crate-oppai-rs-0.2 (crate (name "oppai-rs") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "1cqb8yb97hc8fckkh5n2r4rcq3hdygyjz0g36fd6hp1sjan8r1kf") (yanked #t)))

(define-public crate-oppai-rs-0.2 (crate (name "oppai-rs") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "08g6fmi3xirfs0jfvb919wmyvfwy6myqmz8y31wdsn57dqx5h222")))

(define-public crate-oppai-rs-0.2 (crate (name "oppai-rs") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "0zl73v628zm8z9pccbjqblc6xh09zshg0id00c0mkmjyxhylhq70")))

(define-public crate-oppai-sys-0.1 (crate (name "oppai-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1201avxlq07ir9wzfl17gj3v3v9fjn7snaj4cw9fmwmmr82fgx7x")))

