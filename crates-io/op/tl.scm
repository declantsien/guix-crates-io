(define-module (crates-io op tl) #:use-module (crates-io))

(define-public crate-optlib-0.1 (crate (name "optlib") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1njjfsnqyg5j2yzdxaq32x5csi3kac5xjxnpns7v627ijszfy0s4")))

(define-public crate-optlib-0.2 (crate (name "optlib") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "optlib-testfunc") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1m9hrh8faqmp9cgrx5gp688wn0bggkn5dx0d36xiplizhp7038lf")))

(define-public crate-optlib-0.3 (crate (name "optlib") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "optlib-testfunc") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1iclbs4nm8bmq5zdzfxs9jzwgcgr9abcjf0bw85ah19lyxi8pqy2")))

(define-public crate-optlib-0.4 (crate (name "optlib") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.12.0") (default-features #t) (kind 2)) (crate-dep (name "optlib-testfunc") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "13jx89yfz9qgy1h7345ik2r9i6ynn4dghlnd5j81x21ky1a68g0k")))

(define-public crate-optlib-testfunc-0.1 (crate (name "optlib-testfunc") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "06ppzvs78qn5srj51cd513h4g269m6x21dqxdsj88gv90hr5pman")))

