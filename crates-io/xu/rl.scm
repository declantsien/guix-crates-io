(define-module (crates-io xu rl) #:use-module (crates-io))

(define-public crate-xurl-0.1 (crate (name "xurl") (vers "0.1.0") (deps (list (crate-dep (name "idna") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.3.3") (default-features #t) (kind 0)))) (hash "06yc63sv39ap3qnxfw8grdf33yqvsjqhsvnmsar9aj6cb92rl7yg")))

