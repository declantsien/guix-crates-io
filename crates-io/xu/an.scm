(define-module (crates-io xu an) #:use-module (crates-io))

(define-public crate-xuantie-0.0.1 (crate (name "xuantie") (vers "0.0.1") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)))) (hash "1v18ica2nidqg93cb4sz4jngs0ab4xm7rim2jgqibcq9421nv3cq")))

(define-public crate-xuantie-0.0.2 (crate (name "xuantie") (vers "0.0.2") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "11ggnk3mfh9salbrm1zc7n113ml2fi3zjkzdznwcqkklz75nrca6")))

(define-public crate-xuantie-0.0.3 (crate (name "xuantie") (vers "0.0.3") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "06fnczh4lcbviiiswlqjzw7qcilz0dv0ghmvbqi8rk25ysz7z71x")))

(define-public crate-xuantie-0.0.4 (crate (name "xuantie") (vers "0.0.4") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1xf4g354ljg5zydnbfbpm798b9zcdvpinxj0ia1pnr84wyi6hkq3")))

(define-public crate-xuantie-0.0.5 (crate (name "xuantie") (vers "0.0.5") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "0xd9bpxp5jd84cl4y2699qb1vd5l5blgiw5z8bxwrngvbm9qhnb6")))

