(define-module (crates-io xu #{4-}#) #:use-module (crates-io))

(define-public crate-xu4-hal-0.1 (crate (name "xu4-hal") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wjd52mx1i8l44njp1gz51vgri2l3yzs7dr38x4zkzgybxgacwhr") (yanked #t)))

(define-public crate-xu4-hal-0.1 (crate (name "xu4-hal") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rsfagn1vhipzygc79p0nnazwbs39rnd3kgsp7xhhlbrgzaj08zn") (yanked #t)))

(define-public crate-xu4-hal-0.2 (crate (name "xu4-hal") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0a94mprvwr0xw9wg4jrm59mxj3c5i8hkcs54b22gkhfz510ha778") (yanked #t)))

(define-public crate-xu4-hal-0.2 (crate (name "xu4-hal") (vers "0.2.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "gpio-cdev") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "088yy6yyab8lbv39wldr783q30cmybgcxrzmibnzacxl614vpibd") (yanked #t)))

