(define-module (crates-io xu i-) #:use-module (crates-io))

(define-public crate-xui-core-0.0.0 (crate (name "xui-core") (vers "0.0.0") (hash "1kigc4v2bx1rvyi6fl83gnhaqa0rinpyczrlj5ia237qxrkwscz2")))

(define-public crate-xui-css-0.0.0 (crate (name "xui-css") (vers "0.0.0") (hash "1kd24mh2ll5svn139w3cj5865z00k9iwqg8wdgnrzb6hglgi91s5")))

(define-public crate-xui-dom-0.0.0 (crate (name "xui-dom") (vers "0.0.0") (hash "1qcylpq6ydryl0hgvdpryhp1qw9w40h9gfx2jhrv9l173lykyfpi")))

(define-public crate-xui-macros-0.0.0 (crate (name "xui-macros") (vers "0.0.0") (hash "0yc55sqlm650zj7dhp2my7fmy2s8d3wqqd5zisx9mycm3vcssjy0")))

(define-public crate-xui-widgets-0.0.0 (crate (name "xui-widgets") (vers "0.0.0") (hash "03q35piqdilihg084v1ky7927jbizp78cnys0y7swv0vlgw31cr7")))

