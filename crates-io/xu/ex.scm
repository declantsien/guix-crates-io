(define-module (crates-io xu ex) #:use-module (crates-io))

(define-public crate-xuexi_000115-0.1 (crate (name "xuexi_000115") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m52s4bhimvmabyc7l6v0ldqpia5lw8db6k18rdbs8imv6352djg") (yanked #t)))

(define-public crate-xuexi_000115-0.1 (crate (name "xuexi_000115") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0s2xnm94njwligvyp33gfxyq6vqwm2wpw3r4y47pqya0lsh85cl9") (yanked #t)))

