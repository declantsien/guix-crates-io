(define-module (crates-io i3 sw) #:use-module (crates-io))

(define-public crate-i3switcher-0.1 (crate (name "i3switcher") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "045k4nw92pjd71s7jmgp8mjl47sbhiy2cmcba04nh4i7d99ymnn6")))

(define-public crate-i3switcher-0.2 (crate (name "i3switcher") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0dg2qk4jmbhf9z3ncqrrrm8yzh3k04s7vhk4gcvxd6iw1rdcjcbf")))

(define-public crate-i3switcher-0.3 (crate (name "i3switcher") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.8") (default-features #t) (kind 0)))) (hash "0hwzqaj8w3pg8dg4ad2wc2mfa8lrnhccm5bn9gb6ks983r7pr4sb")))

(define-public crate-i3switcher-0.3 (crate (name "i3switcher") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.8") (default-features #t) (kind 0)))) (hash "09n2p3hjs75gjkdkgc14ajif59341yss0bhrns824qacxw3b4ncr")))

