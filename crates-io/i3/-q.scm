(define-module (crates-io i3 -q) #:use-module (crates-io))

(define-public crate-i3-quick-bind-0.1 (crate (name "i3-quick-bind") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "197wvvm7wf3b51bkb4vz2d430kzq95z1ychn148ba91wx2b49z3s")))

