(define-module (crates-io i3 -w) #:use-module (crates-io))

(define-public crate-i3-workspace-brightness-1 (crate (name "i3-workspace-brightness") (vers "1.0.0") (deps (list (crate-dep (name "hashbrown") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0phb1pkgjndfy1cxz377hnryz4j9v5fx0vc21zx94l8lw3yk8whb")))

(define-public crate-i3-workspace-brightness-1 (crate (name "i3-workspace-brightness") (vers "1.0.1") (deps (list (crate-dep (name "hashbrown") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "007q2mh5xn4b041s8cq1q5fmw1xz6xqpmip966h7gmjrc6bbra20")))

(define-public crate-i3-workspace-brightness-1 (crate (name "i3-workspace-brightness") (vers "1.0.2") (deps (list (crate-dep (name "hashbrown") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1zgk97r51b87994xsy73bpbxzdg20m995fynkw6h0gasgbl18l97")))

(define-public crate-i3-workspace-brightness-1 (crate (name "i3-workspace-brightness") (vers "1.0.3") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "18ifmzr3l2ak76a7w47lv1dly7pbsqmk1x419n3507ld583g5rbb")))

(define-public crate-i3-workspace-groups-0.1 (crate (name "i3-workspace-groups") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "15f8al3hghz3fns0gln4arhiwiwkj8nf55wak4ff55qcz3c1zqbc")))

(define-public crate-i3-workspace-groups-0.2 (crate (name "i3-workspace-groups") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1csbh1v14zwn1xvwv9x2xzrlds2jcvckpmhf1j2by2dfzyy1wixs")))

(define-public crate-i3-workspace-groups-0.2 (crate (name "i3-workspace-groups") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1m531s5mmvyf6pyjgx178vbw5wnwf0jjxwmjsrgr81ywckr3dhli")))

(define-public crate-i3-workspace-groups-0.2 (crate (name "i3-workspace-groups") (vers "0.2.2") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0nvrxa17kc1bnxiq032v305wpbakd858f31c969a4a2h3l1g1g5q")))

(define-public crate-i3-workspace-scroll-0.1 (crate (name "i3-workspace-scroll") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1m7zh1yca9z4r7m377ysyr96g35610ib021i0q3v47npd0va6nha")))

