(define-module (crates-io i3 re) #:use-module (crates-io))

(define-public crate-i3ref-0.1 (crate (name "i3ref") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.10") (features (quote ("env" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "1d2km6n3c5aw2slymqy2631lq0565qfrvpy2xgvpwqj8jdhmw9g2")))

