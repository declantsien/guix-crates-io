(define-module (crates-io i3 ga) #:use-module (crates-io))

(define-public crate-i3gamma-0.1 (crate (name "i3gamma") (vers "0.1.0") (deps (list (crate-dep (name "i3ipc") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "0plc4nqvnmhchyh44s4asanj4nryvp98kq4j48p4dy1hm5xz9yij")))

