(define-module (crates-io i3 _i) #:use-module (crates-io))

(define-public crate-i3_ipc-0.3 (crate (name "i3_ipc") (vers "0.3.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wckvz8f2r57y8abga0b61sp1f8z8za63wdb62na0izhnbnsipam")))

(define-public crate-i3_ipc-0.3 (crate (name "i3_ipc") (vers "0.3.1") (deps (list (crate-dep (name "i3ipc-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qv1ysh5qy95jgf27ymh39dx2zy93m5jhsb0r6q3gx2qbhv3p0in")))

(define-public crate-i3_ipc-0.4 (crate (name "i3_ipc") (vers "0.4.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kx0kbrwmx39x7cwb8p7560w2442igks65nl9z1fk8wyxjhpdiwl")))

(define-public crate-i3_ipc-0.5 (crate (name "i3_ipc") (vers "0.5.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kqb086yr2nqbi9a26swppr65s3yi8xnys83pcmnclzrgk5s0fxn")))

(define-public crate-i3_ipc-0.6 (crate (name "i3_ipc") (vers "0.6.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v83c7vfa6k4cn8x45xdx6rgl2jm26qc74pwi6v6lmjv915xx6zk")))

(define-public crate-i3_ipc-0.8 (crate (name "i3_ipc") (vers "0.8.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fg7z93k3rp700li4risc576rvm4708qdpl6ddrmp1fgfqfdwiaj")))

(define-public crate-i3_ipc-0.9 (crate (name "i3_ipc") (vers "0.9.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q5jqxb1n3a6a86542fh53s8zbqr3b8lbrplr6fdn3gc9apf2pn4")))

(define-public crate-i3_ipc-0.10 (crate (name "i3_ipc") (vers "0.10.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c618n9dzk3b3rgr5c94b22v1a5wddhqhs6x7n80z6gbw3k41sva")))

(define-public crate-i3_ipc-0.11 (crate (name "i3_ipc") (vers "0.11.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vsh7pbrnpv2w2b79ffy9l6323s3c8hims642pp7jvaw0a559ddk")))

(define-public crate-i3_ipc-0.12 (crate (name "i3_ipc") (vers "0.12.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "15710d2a26i1y5ma82p9hn5lyxn0h5xfx6g9wkas44akdwgb8mnl")))

(define-public crate-i3_ipc-0.13 (crate (name "i3_ipc") (vers "0.13.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1my8w2rh4p1z085hrswjc31czygv5j5s6idak78kfbsvkrfxjifx") (yanked #t)))

(define-public crate-i3_ipc-0.14 (crate (name "i3_ipc") (vers "0.14.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xhrwcmm2sfac98b7gknhhaal26kp3yy5wrnrri3k4h7cj6764z1")))

(define-public crate-i3_ipc-0.15 (crate (name "i3_ipc") (vers "0.15.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1616zkbk2ca7swn8l987yhqm353gz4ddkm3m3cqcx5rlw27ch9r5")))

(define-public crate-i3_ipc-0.16 (crate (name "i3_ipc") (vers "0.16.0") (deps (list (crate-dep (name "i3ipc-types") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ynz66jssprpv1c70dk21bvmad5q7973i4fpa8zvd42zg7v9aahw")))

