(define-module (crates-io i3 g4) #:use-module (crates-io))

(define-public crate-i3g4250d-0.3 (crate (name "i3g4250d") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0mn4lgdcpb1arf7fkzcnq63bigw69ypia00nra63zrmrx7zfciqy")))

