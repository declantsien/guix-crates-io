(define-module (crates-io i3 #{-i}#) #:use-module (crates-io))

(define-public crate-i3-insert-workspace-1 (crate (name "i3-insert-workspace") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0cg7zqsa480q6ijqpnd7dxhzn7j6wdf9wzs9raq8icz9rp2n93pb")))

(define-public crate-i3-insert-workspace-1 (crate (name "i3-insert-workspace") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "19n65mgis4vmgad5bmrjinh78zisdafdzdc2giyd547y1w8s6fs2")))

(define-public crate-i3-insert-workspace-1 (crate (name "i3-insert-workspace") (vers "1.3.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "04sqgd46q5ri5j7arz303nn3v8kar9f5v0064xyn5nwkyal26hi7")))

(define-public crate-i3-insert-workspace-1 (crate (name "i3-insert-workspace") (vers "1.3.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "179kxbdnkvzpr9v3b3y5gjsznzpp4d3mb2x3kc3fn434slyq9syx")))

