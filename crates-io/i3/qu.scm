(define-module (crates-io i3 qu) #:use-module (crates-io))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.0") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)))) (hash "1q2rd9ccgxg31py9q8wsw3ggh4xnzpn9x8nwg807r93d7scji6hs") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.1") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)))) (hash "14391sz4zh4bgz9ya0az4kbrxkbg43ki7k19rlb6dzypw0xs3lv5") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.2") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)))) (hash "0chvn6ad69xb42fbdrx8jvlagy7gsb9h17ijp25qry2sdgh4d7h9") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.3") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)))) (hash "1ncgnjm7hx5na4pflqph996xm3d0dhwad8maqqcawqkdl2rac16g") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.4") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)))) (hash "099a4r3xmlvvp59fv52j12x0ryn22jj6d1s39nrz3n8qbvip65dc") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.5") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)))) (hash "05a7i7h9s2lg9ddfda0vi6zxbsbnsyn9g55140lyrq7hlfwj7gnx") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.6") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "1a5a7vqyfd1w28wpyyxxmmnpzhdszmwq8i2kss9shmlr3rgp12zv") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.7") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "1gqz6a45rjh0d7idbszbcysfqvwis16fi8l3rd9yfgwcpi9jqmhb") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.8") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "01zs7mrawp0p9zhmpln1fllx32n2zrzv9zhf9lkqvdrqzd2wr9va") (yanked #t)))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.9") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "0sd8r5l6hkw2r1b396dak3qh0knx2rh8f3x763awqxn3kggyszfp")))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.10") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "0f73c5dcqxbf1k95vm74lj4skqxsw9mf7sb4dp8zrdja6mhycqja")))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.11") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "0nh3r7g3s9i5lmv5wdjzsvyr1rmk8vyh2yq9qjlmyayw9p4w0hcg")))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.12") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "1v38jdgmv1achlqrpk94195hxmliv7nmjsnq1i8f0lg3q0cxppdw")))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.13") (deps (list (crate-dep (name "fltk") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "1fxrgdw1w8c4hghr0dn6gjd4sh3ps129nqnp7swdsf89mdiq5xbf")))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.14") (deps (list (crate-dep (name "eframe") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "1iclfy588136s46qkkr370xhf0scsg2v0dqca5p796ljhwfv8754")))

(define-public crate-i3quitdialog-0.1 (crate (name "i3quitdialog") (vers "0.1.15") (deps (list (crate-dep (name "eframe") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "06fqs3d1j7r364xfwlc1400l2854yflascxdyq742jd8m6vg81jb")))

(define-public crate-i3quitdialog-1 (crate (name "i3quitdialog") (vers "1.0.0") (deps (list (crate-dep (name "eframe") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "static_init") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "1792aa9qdm13qm4qsxp5fgx9i98pm8rlgk0h5ifvlph42a3jzmfw")))

