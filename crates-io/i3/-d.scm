(define-module (crates-io i3 -d) #:use-module (crates-io))

(define-public crate-i3-dws-0.1 (crate (name "i3-dws") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (features (quote ("i3-4-14"))) (default-features #t) (kind 0)))) (hash "0vkgrs2yfxkl1ldapzzjs9qz3sq6rhampb7j5cbx34f83yfrp105")))

(define-public crate-i3-dws-0.1 (crate (name "i3-dws") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (features (quote ("i3-4-14"))) (default-features #t) (kind 0)))) (hash "04zkkcd4vp411vd2x3rcbkw366db5j6r0cwks19l0gxym4cxnh6d")))

