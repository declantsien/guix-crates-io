(define-module (crates-io i3 sp) #:use-module (crates-io))

(define-public crate-i3spin-0.2 (crate (name "i3spin") (vers "0.2.0") (deps (list (crate-dep (name "i3ipc") (req "^0.10") (default-features #t) (kind 0)))) (hash "0zgchd3zp23d62qhff19ya3phg6bl8fvwb1q5dr765myan4nkwjh")))

(define-public crate-i3spotify-0.1 (crate (name "i3spotify") (vers "0.1.0") (deps (list (crate-dep (name "dbus") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "16w83609dgm9i2x2mdyh9nsx3anfs5bpvisz6i2lk8scyrqs7jia")))

(define-public crate-i3spotify-0.1 (crate (name "i3spotify") (vers "0.1.1") (deps (list (crate-dep (name "dbus") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0x1zvwvnp5pmblggyahp9lln9dq2jzyvznpp6acfbd6bb20dl77q")))

