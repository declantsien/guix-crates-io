(define-module (crates-io i3 wm) #:use-module (crates-io))

(define-public crate-i3wm-0.1 (crate (name "i3wm") (vers "0.1.0") (hash "00fcg6nbr2rzfiw9p018fdipfcgk2dx8rz17iaksv4y3ikisabrm")))

(define-public crate-i3wm_switch_windows-0.1 (crate (name "i3wm_switch_windows") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-i3ipc") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1qwl8ywnarsihjnnr2r1yxfpbrj5gardkv6vdxs6rs7cr9xldnqm")))

(define-public crate-i3wm_switch_windows-0.1 (crate (name "i3wm_switch_windows") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-i3ipc") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0050w28b13s4p9nr7j9wd9ik9kz7abklcmdsdnxziqkvfx717v59")))

(define-public crate-i3wm_switch_windows-0.2 (crate (name "i3wm_switch_windows") (vers "0.2.0") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-i3ipc") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0ji4c75zpkgh2zwszwnmzqai6fhg6p64ksmg1hxa3vxysr7q3rq8")))

(define-public crate-i3wm_switch_windows-0.3 (crate (name "i3wm_switch_windows") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-i3ipc") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "04igjs2zxhijk6x4mjb3fn5fki277yj94lwmkww6i26lskqjznp2")))

