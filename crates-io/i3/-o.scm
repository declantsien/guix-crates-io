(define-module (crates-io i3 -o) #:use-module (crates-io))

(define-public crate-i3-open-next-ws-0.1 (crate (name "i3-open-next-ws") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "swayipc") (req "^2.7.2") (default-features #t) (kind 0)))) (hash "0qnvlbyc45n0zw2n1q7119zg75syfr5m6wmzfhrfag36jqnr1i3g")))

(define-public crate-i3-open-next-ws-0.1 (crate (name "i3-open-next-ws") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "swayipc") (req "^2.7.2") (default-features #t) (kind 0)))) (hash "1lwm5q5rn12kmf1migzmppidx8v0zxadizr4p4cqjpnk21jrm8lb")))

(define-public crate-i3-open-next-ws-0.1 (crate (name "i3-open-next-ws") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "swayipc") (req "^2.7.2") (default-features #t) (kind 0)))) (hash "0rj5sdcarsjh0zf5rxpya36gsnvdpvw7h0pg6qsilhi5c6vi85zm")))

(define-public crate-i3-open-next-ws-0.1 (crate (name "i3-open-next-ws") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i3_ipc") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "swayipc") (req "^3.0") (default-features #t) (kind 0)))) (hash "07y77qvwcx9cavl5n957qbrw3h3b0nb5z7wp7w6jw68x3i4nagc6")))

