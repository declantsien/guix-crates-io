(define-module (crates-io kj ps) #:use-module (crates-io))

(define-public crate-kjps-0.1 (crate (name "kjps") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "04myryvb99bx4i0ghzzzyvpw1vdpnxbiw36jlkm5in830111pa8b")))

(define-public crate-kjps-0.1 (crate (name "kjps") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0y7k3h17a21isq35z705fsyg5lv287gsgjipx63rzghnyh5z09nb")))

(define-public crate-kjps-0.1 (crate (name "kjps") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1j7khj7mzyxs7qanr721clvffa8cclb4rpljmfikvl7w5qklr68f")))

