(define-module (crates-io w6 #{5c}#) #:use-module (crates-io))

(define-public crate-w65c02s-0.9 (crate (name "w65c02s") (vers "0.9.0") (hash "00bawnssxlm5ayq6waliw3djm0dm7gxglj8lzmpv3qpwxagya303")))

(define-public crate-w65c02s-0.9 (crate (name "w65c02s") (vers "0.9.1") (hash "17kz2a3389kbfg6m9cfiwf73r33dk26a638hblngrqs750l2ygr4")))

(define-public crate-w65c02s-0.9 (crate (name "w65c02s") (vers "0.9.2") (hash "10k3kxrniq2hd709ygpqlmdfqvlrccrkvapsnsc3l8xr92qigy8j")))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.0") (hash "1xi0jgpv9naprskjj34w5v3bhhjfhgw247bryr5sazjc8wxvf6v3") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.1") (hash "1pcz9cjbif2qwfmvsv3ph03n0cpa9lhs7yn5qaxygrz16vbp561r") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.2") (hash "0092v19igzc32dllpbbv26ig4ljqkr2izbg2p9axdxiggqxc3ayl") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.3") (hash "12dfap6lawz1nyvhfmrqhs10pgba7hz03crc98nlbiz1sggvb5a8") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.4") (hash "1kgkznlcdxbi4myjkwqy5qn743g0j2wy1fy6sahhh5yva93q8qgl") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.5") (hash "07n7halj8cryb6ga272kp0afx3gz5zgzay32kp1wwbpix9znssjd") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.6") (hash "0hagj9qvbq7m362c4c1hhjnp31y523869rnjzyj6m3h7cwq9ly5h") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.7") (hash "0zm5j6glhkb6ad6r10hhp84is5zbmgjy83m75ka9mivvnih76yd4") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.8") (hash "0xf5rgjrss8z8x1pqbdv5970bsyaydvb59zkqxbpcwfxp3yhjgbw") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.9") (hash "10vrb98j5gxyf5argz71p3gj5xlwq40d6843hd71caqng0fz6mg3") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.10") (hash "08mhvxw2v5mf880wzbmpzmw31vq6yzww2sxn1phqqqzkqvi1zkcs") (features (quote (("nightly") ("default" "nightly")))) (yanked #t)))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.11") (hash "1wfyhachhb4l687yyxl9yzmj670dk8nyyyckwcmfyavka1rlqbv5") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.12") (hash "0rwv9ksgj8hd0k396ah4vdzh3landaxmflsnq4clzidihn433648") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.13") (hash "1x34y4xv0n7hrdsbdrgxdzx8rbw9if5b2p32c8maivpk51kcml8y") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-w65c816-0.1 (crate (name "w65c816") (vers "0.1.14") (hash "1giy8zipw7l363bwmlsc1pph6qnpwm06sm8z7ximhla1q8cpdjl9") (features (quote (("nightly") ("default"))))))

