(define-module (crates-io fk -l) #:use-module (crates-io))

(define-public crate-fk-lis3dsh-0.1 (crate (name "fk-lis3dsh") (vers "0.1.0") (deps (list (crate-dep (name "accelerometer") (req "~0.12") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "~0.5") (kind 0)))) (hash "1ra73plgvay1m8f5ywipi05l6h6mkidppl441yr9cfdwydjqm0rl")))

