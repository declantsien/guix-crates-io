(define-module (crates-io fk _c) #:use-module (crates-io))

(define-public crate-fk_cli-0.1 (crate (name "fk_cli") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.3") (features (quote ("fuzzy-select"))) (default-features #t) (kind 0)))) (hash "0klaihw0npqwcyx5s1mdivhm1hmm1m7id492z4j5g65rwjvp9xsm")))

