(define-module (crates-io fk ys) #:use-module (crates-io))

(define-public crate-fkys-rs-0.0.0 (crate (name "fkys-rs") (vers "0.0.0") (hash "00q943nl9hm2qswqy41fkxbskxrfv03hrycjm887b4wjrzbbywbj")))

(define-public crate-fkys-rs-0.1 (crate (name "fkys-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "07cz8ldkrgcnnldn8klqhq8jnv98wmi5z8jn0zyck158kkvcqjs5")))

(define-public crate-fkys-rs-0.1 (crate (name "fkys-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0spn94izd272fvyj979096yz7zj4f78z232fh10i3bdjpf7jv22z")))

(define-public crate-fkys-rs-0.2 (crate (name "fkys-rs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vkhn35an9vxvwq0h2yinbrqji14viy76v268vb9y28qp7smfkf7")))

(define-public crate-fkys-rs-0.2 (crate (name "fkys-rs") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1358zrswmn682nqkm6kzvsqn4gg03csdam24i4s5kvwq2jjisy5d")))

(define-public crate-fkys-rs-0.2 (crate (name "fkys-rs") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bavwfyx7fpfqscgak43z92rhwvjmwfip18hddxm1a5fc11m1jd4")))

(define-public crate-fkys-rs-0.2 (crate (name "fkys-rs") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1iv1i7qkd9afjrq03s3rxk5yryaqgbfkmyzpj8npwwfl5c5z22p2")))

(define-public crate-fkys-rs-0.2 (crate (name "fkys-rs") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18fz328hz8isj224c7gypbgmds4nhbj8sac6dmzifqq8vrbbzkw2")))

(define-public crate-fkys-rs-0.2 (crate (name "fkys-rs") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0q2g0h4g58z9xd1zwp2128z35q3pq8m8h720rn9kwm1588q1bnr4")))

