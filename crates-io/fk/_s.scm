(define-module (crates-io fk _s) #:use-module (crates-io))

(define-public crate-fk_std-0.1 (crate (name "fk_std") (vers "0.1.0") (hash "0vygp6ajx79pkw8m3vv2hi6ys37mg8sdc0bjp3mlng8496lhrmvg") (yanked #t)))

(define-public crate-fk_std-0.1 (crate (name "fk_std") (vers "0.1.1") (hash "042ni5jvf55w3nvixcg4pmld3bdqqxhnpks20mvfgxpjn0b8yzcx") (yanked #t)))

(define-public crate-fk_std-0.1 (crate (name "fk_std") (vers "0.1.2") (hash "0xx7456vyr8ia8adphc963fmg9brd42s8jf7pba09hnrivhizi9l") (yanked #t)))

(define-public crate-fk_std-0.2 (crate (name "fk_std") (vers "0.2.2") (hash "139s1qmglgxmq14cgr4cbc54y14hs42pwwxi30br4fic1068axqh") (yanked #t)))

(define-public crate-fk_std-0.2 (crate (name "fk_std") (vers "0.2.3") (hash "17kvr7vcpxbby5xqc918y5ayyfl3pqb3kqgsm8aranw8mmfmxwd1") (yanked #t)))

(define-public crate-fk_std-0.3 (crate (name "fk_std") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0.2.85") (default-features #t) (kind 0)))) (hash "16d90cgzy56dcifsd8fm051xb999w4aiv6zalgnxr61davh7rqgn") (yanked #t)))

(define-public crate-fk_std-0.3 (crate (name "fk_std") (vers "0.3.4") (hash "12ixxvjv5sggw4sjm7ihs0g4w879dnvgqf5wac1ls3csnv2c4sln") (yanked #t)))

