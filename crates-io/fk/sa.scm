(define-module (crates-io fk sa) #:use-module (crates-io))

(define-public crate-fksainetwork-0.1 (crate (name "fksainetwork") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "bincode_derive") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0sxhha5xxccn01zqgsywcm1wcvwn7rly3a4mw779kg99cpkcx4la")))

(define-public crate-fksainetwork-0.1 (crate (name "fksainetwork") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "bincode_derive") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0va8nri7b0qv8yypiy6qi7zwn0s167zw8fxwfpi5qxp04mkmfbyj")))

(define-public crate-fksainetwork-0.1 (crate (name "fksainetwork") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "bincode_derive") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "103r7ypiiz27qsvc4aqw3x3vrmi9pd9kc26lfhpl9hn3z8za3hjq")))

(define-public crate-fksainetwork-0.1 (crate (name "fksainetwork") (vers "0.1.3") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "bincode_derive") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0fxi6mgs4hadlgnzxglib38cvr0b59sqy0n1bh2lhcmhbz40lj7b")))

(define-public crate-fksainetwork-0.2 (crate (name "fksainetwork") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "bincode_derive") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "bmp") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0472660na3wwsqw17sidjrkdfpc0615sf37l57kx8xbq72x8iadg")))

