(define-module (crates-io ju na) #:use-module (crates-io))

(define-public crate-juNa-0.1 (crate (name "juNa") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1jrxx6rairpwz4n0wx0hbpaaqkcgf482fcwr1h9jr13izyr8fimd") (yanked #t)))

(define-public crate-juNa-0.0.1 (crate (name "juNa") (vers "0.0.1") (deps (list (crate-dep (name "bytemuck") (req "^1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0ajg5sdk7wc4p64l6mvm94wkv59gs1rcw8k0nfqxy37pliwgxmjr")))

