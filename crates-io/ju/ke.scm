(define-module (crates-io ju ke) #:use-module (crates-io))

(define-public crate-juke-0.0.1 (crate (name "juke") (vers "0.0.1") (deps (list (crate-dep (name "minifb") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0nsnn2kdr6sjm48l5sqw5w6pc8ffc1vpj7g8qlc4agr1ldyzql90")))

(define-public crate-juke-0.0.11 (crate (name "juke") (vers "0.0.11") (deps (list (crate-dep (name "glam") (req "^0.20.5") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1n7d8idajijib2m167z15zp1n0jb4kad0sgajd92kx0c719l2l18")))

(define-public crate-juke-0.0.111 (crate (name "juke") (vers "0.0.111") (deps (list (crate-dep (name "glam") (req "^0.20.5") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qx7qgdma26p68scsy83hwbpn8307fshavqiaxsa577qajvzp66s") (yanked #t)))

(define-public crate-juke-0.0.12 (crate (name "juke") (vers "0.0.12") (deps (list (crate-dep (name "egui") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "egui-winit") (req "^0.17") (features (quote ("links"))) (kind 0)) (crate-dep (name "egui_wgpu_backend") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "winit_input_helper") (req "^0.12") (default-features #t) (kind 0)))) (hash "1mmspfmgyyzc5y4072kyswpsx4rlkyiag91vs3crv02virlk4hfj") (features (quote (("optimize" "log/release_max_level_warn") ("default" "optimize"))))))

(define-public crate-jukebox-0.1 (crate (name "jukebox") (vers "0.1.0") (hash "11hshvnxqsqq1fgry6npl87y821gbc0bs4h2jrlc2kbnpp029d5c")))

(define-public crate-jukeboxrhino-first-module-0.1 (crate (name "jukeboxrhino-first-module") (vers "0.1.0") (hash "115vms0ylbl66f75igqxa5jgwkp99yf1gzqnpqddh32irh2vb0ml")))

