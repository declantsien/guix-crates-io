(define-module (crates-io ju ng) #:use-module (crates-io))

(define-public crate-jungle-0.1 (crate (name "jungle") (vers "0.1.0") (hash "0fkwm26lxv6p4dy0wph8rdxz6fj37ivasw2cdh88y4lld2in4pis")))

(define-public crate-jungle-0.1 (crate (name "jungle") (vers "0.1.1") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1nj6wvagbcpf2qnzjpz38yrwr6mlkh3asijda8k7kdw2v679yvkk")))

(define-public crate-junglefowl-0.1 (crate (name "junglefowl") (vers "0.1.0") (deps (list (crate-dep (name "junglefowl-macros") (req ">0.0") (default-features #t) (kind 0)))) (hash "1jh8s19r0jwx1qkqf8bfqawilnaykn4qhzxbyy5qhjcmf15l4y0j")))

(define-public crate-junglefowl-0.1 (crate (name "junglefowl") (vers "0.1.1") (deps (list (crate-dep (name "junglefowl-macros") (req ">0.0") (default-features #t) (kind 0)))) (hash "1di4rkmv3x72fh38brll0dci2jfn8h54z8xhvww0h5xlxxadz8xf")))

(define-public crate-junglefowl-macros-0.1 (crate (name "junglefowl-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req ">=1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req ">=1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">=2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1absr0ry4w0xawkfdz1dbgbpvp80jyqa9nryy78s22lzclgj3is9")))

(define-public crate-junglefowl-macros-0.1 (crate (name "junglefowl-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req ">=1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req ">=1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">=2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0asl2srkmhfr04g2qqa91l6x03761bgb6sfm6a0w2y0z0zfgxvki")))

