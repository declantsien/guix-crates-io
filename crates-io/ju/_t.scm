(define-module (crates-io ju _t) #:use-module (crates-io))

(define-public crate-ju_tcs_rust_23_18-0.1 (crate (name "ju_tcs_rust_23_18") (vers "0.1.0") (hash "16i4n3h05gblpi5sgj2r5qwsviag7fd0w342hf8rjy16hwxcgddf")))

(define-public crate-ju_tcs_rust_23_19-0.1 (crate (name "ju_tcs_rust_23_19") (vers "0.1.0") (hash "0p7rfqqhi2wmkkpr93x4iscx6h4zqa9rhjzg5lnqhzxpmqsj3g1y")))

(define-public crate-ju_tcs_rust_23_19-0.1 (crate (name "ju_tcs_rust_23_19") (vers "0.1.1") (hash "0mmzqgmi4gq3f8gddyhkc3ns62sllbjqz187fznas3x4h0hvx2r8")))

