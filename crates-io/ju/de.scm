(define-module (crates-io ju de) #:use-module (crates-io))

(define-public crate-jude-0.1 (crate (name "jude") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.8.3") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "1wgacrbziscvp3r2lrrx4ap0sfly74pcvsry4fgmmpky0bqmjnxn")))

(define-public crate-jude-0.1 (crate (name "jude") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "0.8.*") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "02lrsyvcnyab9ffprg6hhfrz4w0k90nhzywcq3jsgwpgrh8232m9")))

(define-public crate-jude-0.1 (crate (name "jude") (vers "0.1.2") (deps (list (crate-dep (name "libloading") (req "0.8.*") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "1bvbskilvknlasasdfkff5kq1nib0771h9kwdhxc750cdddn8z7j")))

