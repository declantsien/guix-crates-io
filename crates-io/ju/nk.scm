(define-module (crates-io ju nk) #:use-module (crates-io))

(define-public crate-junk-0.1 (crate (name "junk") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.3.3") (default-features #t) (kind 0)))) (hash "05h37xp59zbzzys9mygx939dy31i4cdawgy5jx993fwzq6q41057")))

(define-public crate-junk-0.1 (crate (name "junk") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.3.3") (default-features #t) (kind 0)))) (hash "15vfzixvpy9nn24pk985aczrmynb3h90dv7940js0pfsc71xihz3")))

(define-public crate-junk-0.1 (crate (name "junk") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (default-features #t) (kind 0)))) (hash "17cmmi14460a5ppbib9j0s707vqnshikww1ksazc8hb9zw7js7ig")))

(define-public crate-junk_file-0.1 (crate (name "junk_file") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 1)))) (hash "0flzaalqr2b2ykgkwc1kwbx8fyhnq5bxg6ppfyzrlqsg45vxjm0j")))

(define-public crate-junk_file-0.1 (crate (name "junk_file") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 1)))) (hash "04pyjf813zfy6al0s9jnmdm5fnp7fkg21hmihg65kk773h207p4d")))

(define-public crate-junkbox-0.1 (crate (name "junkbox") (vers "0.1.0") (deps (list (crate-dep (name "ron") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "1351739sc3knf269377511glmm1ciy73pjxfwvgfsv28c5h9m8kh")))

(define-public crate-junkyard-0.0.0 (crate (name "junkyard") (vers "0.0.0") (hash "1vb4pnb8zdnl9kqx7q9fb9hl8l3kwwi5hrq020y51ji83xn8prh3")))

