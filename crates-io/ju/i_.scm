(define-module (crates-io ju i_) #:use-module (crates-io))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.0") (deps (list (crate-dep (name "cbsk_base") (req "^0.1.5") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "0k4nyrz4a31dd8igsgxss6dbg9kjzalv7vbz7iwq05609w8jaqa1") (yanked #t)))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.1") (deps (list (crate-dep (name "cbsk_base") (req "^0.1.5") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "04shmqpx4rxi85pzmdaih37dj4ivr48ajzbwl2hzcl3vcbqldp56") (yanked #t)))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.2") (deps (list (crate-dep (name "cbsk_base") (req "^0.1.5") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "0rvdpdr4vjvg98nmqqg9dxngvjy9cxg1grriwsdq9c736cg875vn")))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.3") (deps (list (crate-dep (name "cbsk_base") (req "^0.1.6") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "0wqxqx82v29ggaiizg1kl2w22hqgz5mzbi8sbnxfbyzsdknmr8bw")))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.4") (deps (list (crate-dep (name "cbsk_base") (req "^0.1.7") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "16pqdj89ap16jbfp3aykwvgp27f2yw1lb5mdx3awvmzkch6vlk1m")))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.5") (deps (list (crate-dep (name "cbsk_base") (req "^0.1.8") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "1s7v26c0d3khf1wnq5fa1x25aq7s5w7lkl19k58w6fbp53qypdn1")))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.23") (deps (list (crate-dep (name "cbsk_base") (req "^1.1.0") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "150wvb62h4l97wwjxibvf4ynl4gss43szc8d0j5lblbv8dn6dkp9")))

(define-public crate-jui_file-0.1 (crate (name "jui_file") (vers "0.1.24") (deps (list (crate-dep (name "cbsk_base") (req "^1.2.0") (features (quote ("macro" "log"))) (default-features #t) (kind 0)))) (hash "0vlqfpar2dzd1mgy7sbsvbvwf5cihadwvch8h6w7xz6hzzvl3qkn")))

