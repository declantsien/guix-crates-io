(define-module (crates-io ju dy) #:use-module (crates-io))

(define-public crate-judy-0.0.1 (crate (name "judy") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "111yalrpw30rfhmfilxsnaz0cx24nircpjy7hr9iknfqrjkqvsmh") (yanked #t)))

(define-public crate-judy-0.0.2 (crate (name "judy") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1x59nccyh2rcsxy4cb2p78f3aac0a0p5lr8300x9sa5m2xbcqwik")))

(define-public crate-judy-0.0.3 (crate (name "judy") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "~0.2.6") (default-features #t) (kind 0)))) (hash "1q8rzgz44pf38ddzddb9y67vxslp6xm7k426gamzym2xly2qwgj4")))

(define-public crate-judy-sys-0.0.1 (crate (name "judy-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08rvb126klnz4whhbaiq173b0kw9wqqgmd401akz4x2g16jpy99y") (yanked #t)))

(define-public crate-judy-sys-0.0.2 (crate (name "judy-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xsxxlgw0lqfp6cmzyrcll89xxhnwi7qyvpvdbqx249wv026j05a")))

(define-public crate-judy-sys-0.0.3 (crate (name "judy-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "065ssi898dhv44jmdjxiy8dl1pr6hnlk9kmjb2xa2r8ilvdiy6sv")))

(define-public crate-judy-sys-0.0.4 (crate (name "judy-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05w17s4b7nr3r5rdpn775asgjbjcixqjm9kw5gqxbfdrzm10gngv")))

(define-public crate-judy-wrap-0.0.1 (crate (name "judy-wrap") (vers "0.0.1") (deps (list (crate-dep (name "judy-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yrfam5fxlabscsj5a4gdygpbvys9k6kqnkq8ky620q2dkxzv0yv")))

(define-public crate-judy-wrap-0.0.2 (crate (name "judy-wrap") (vers "0.0.2") (deps (list (crate-dep (name "judy-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ibjwxg9150vkx0j0p2ny08fjhqskzssni2m7ng8ylwizlfy439l")))

(define-public crate-judy-wrap-0.0.3 (crate (name "judy-wrap") (vers "0.0.3") (deps (list (crate-dep (name "judy-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v92mrc902j0zf48v9yd34q1g4z5306srjq6q24ph0r57knjr4gj")))

(define-public crate-judy-wrap-0.0.4 (crate (name "judy-wrap") (vers "0.0.4") (deps (list (crate-dep (name "judy-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "035v8d2y2yarg53yalh7vwgivkq147hp4gizvbpjrd587bglvxfq")))

(define-public crate-judy-wrap-0.0.5 (crate (name "judy-wrap") (vers "0.0.5") (deps (list (crate-dep (name "judy-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1492k34c7msp9vcpdjixbagmvw2mcc9hfy42rwgdkim0bg3hb9px")))

(define-public crate-judy-wrap-0.0.6 (crate (name "judy-wrap") (vers "0.0.6") (deps (list (crate-dep (name "judy-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pdl38wspf0dyf5df4s584blsim1hx6jbs7dwa8z40h1hq2mrzvr")))

(define-public crate-judy-wrap-0.0.7 (crate (name "judy-wrap") (vers "0.0.7") (deps (list (crate-dep (name "judy-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "166m3l3pi7207mciz96yvnhjwmx4w1pz81l70nw4cng5h34iclix")))

