(define-module (crates-io ju xt) #:use-module (crates-io))

(define-public crate-juxt_adler32-0.1 (crate (name "juxt_adler32") (vers "0.1.0") (hash "1f12h8cjbn08fhwaicb307hl2f1yvzgn3jkzkw7c0996phygivyz")))

(define-public crate-juxt_adler32-0.1 (crate (name "juxt_adler32") (vers "0.1.1") (hash "1gbpapm8mzndzvrvh6lsx1q99y4jxdan85ksdx73rris74dc44c5")))

(define-public crate-juxt_basex-0.1 (crate (name "juxt_basex") (vers "0.1.0") (hash "0ibr0y92kpgqyxn0676641clmcc9fv134w6pfyi3ajfhb271h00s")))

(define-public crate-juxt_matrix-0.1 (crate (name "juxt_matrix") (vers "0.1.0") (hash "0v5bw5qplnx50bqk4d4b41blzcxnch54z6gmsj829rsmpbh7631w")))

(define-public crate-juxt_matrix-0.1 (crate (name "juxt_matrix") (vers "0.1.1") (hash "0w2vishk1m14j5n8whiw75ccd003l5wa8l9jbm81byp9kcwij2l0")))

(define-public crate-juxt_md5-0.1 (crate (name "juxt_md5") (vers "0.1.0") (hash "0l1ycyj19c2jh6i15cd8l47lc301ssbbm2fjpm7ck2kr6w06j0pq")))

(define-public crate-juxt_md5-0.1 (crate (name "juxt_md5") (vers "0.1.1") (hash "1a02f5qqm2d6j2pq5hy0rcd53rc557y901sq5yp3xjgv6nfs0dzw")))

