(define-module (crates-io ju -t) #:use-module (crates-io))

(define-public crate-ju-tcs-rust-2023-14-0.1 (crate (name "ju-tcs-rust-2023-14") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "134nllzk09f063c8dfpcqnb8v6imkwsyq6bhdx1gl09vqvgblszj")))

(define-public crate-ju-tcs-rust-2023-14-1 (crate (name "ju-tcs-rust-2023-14") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uj-tcs-rust-2023-13") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pr0fvlw1s2gh0k1xcxsrblqx9ffibhqzxkyh69dw5bn610q0141")))

(define-public crate-ju-tcs-rust-23-03-0.1 (crate (name "ju-tcs-rust-23-03") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-04") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0jw0lxfk9nh5cbw6kpm97qxib78jpqp0qgv0h4yiisjgqi26i2yf")))

(define-public crate-ju-tcs-rust-23-03-0.2 (crate (name "ju-tcs-rust-23-03") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-04") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0i9rwrl7k0ywx6sxfsbs74ym8zxz2j4d1nbr0kdgfrkrnn0747hn")))

(define-public crate-ju-tcs-rust-23-04-0.1 (crate (name "ju-tcs-rust-23-04") (vers "0.1.0") (hash "0giaac59qslpaknrm8hwnif46rnrbkmfm3f40qa1ydg6qcsq9hz8")))

(define-public crate-ju-tcs-rust-23-04-0.2 (crate (name "ju-tcs-rust-23-04") (vers "0.2.0") (hash "0jdknwnqp5yhyldm1f7n39d17zdrfpl0m4sha0hw8kil0fk8359f")))

(define-public crate-ju-tcs-rust-23-04-0.2 (crate (name "ju-tcs-rust-23-04") (vers "0.2.1") (hash "06rszlxbjl350qfq6qsak0b4dvavbs8gng3vz7x4is211iv82a0s") (yanked #t)))

(define-public crate-ju-tcs-rust-23-04-0.3 (crate (name "ju-tcs-rust-23-04") (vers "0.3.0") (hash "0virsk6108d141gxr861lzjrzcgnrri5wn1x2477340rps0817ib") (yanked #t)))

(define-public crate-ju-tcs-rust-23-04-1 (crate (name "ju-tcs-rust-23-04") (vers "1.0.0") (hash "0s9srkp705gwavfjc4lm99jnpikg31z29fcs88clr81nr09cym14")))

(define-public crate-ju-tcs-rust-23-1-0.1 (crate (name "ju-tcs-rust-23-1") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0p72w0aj0qqalkcfzrr1rf0kakynvdqqyr1qm985ff1kshq43b2l")))

(define-public crate-ju-tcs-rust-23-1-0.2 (crate (name "ju-tcs-rust-23-1") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-2") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bdk39vw1i4phjwhn2di5mmhy92k7k0kqf36si6xhm474mywmhc5")))

(define-public crate-ju-tcs-rust-23-11-0.1 (crate (name "ju-tcs-rust-23-11") (vers "0.1.0") (hash "0pvjxbvqfbgnmwllibk699x47bcvdc3f86r124xaj8swamh075vd")))

(define-public crate-ju-tcs-rust-23-11-1 (crate (name "ju-tcs-rust-23-11") (vers "1.1.0") (hash "0ahwvajdv2h79n39ibnj0hp3j2azmrih13mg7brc4avffpl80kwh")))

(define-public crate-ju-tcs-rust-23-12-0.1 (crate (name "ju-tcs-rust-23-12") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cycqshczdc0akx6d0hfz7iz8hh5yzw8gq29ww8y6qxy2f7i52xg")))

(define-public crate-ju-tcs-rust-23-12-1 (crate (name "ju-tcs-rust-23-12") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-11") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05nzzj4qv8s7lzbdq90ddbf5gcqcvi8f3wb9x1s03hw1msrhmd97")))

(define-public crate-ju-tcs-rust-23-12-1 (crate (name "ju-tcs-rust-23-12") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-11") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "06kx8r6jlvhnmx3phhhhryls938fvdbmnap68swsjki527x9g3r3")))

(define-public crate-ju-tcs-rust-23-12-1 (crate (name "ju-tcs-rust-23-12") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-11") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1f4a7jnjz5l8ynz24nwnq8ikxwinaafc6jv4qnxzh414rp33vvjg")))

(define-public crate-ju-tcs-rust-23-15-0.1 (crate (name "ju-tcs-rust-23-15") (vers "0.1.0") (hash "12x7iyy7ay77my5n4cjf9hiwrva7gf7376nc5nrkgz946jcvrjl3") (yanked #t)))

(define-public crate-ju-tcs-rust-23-15-0.1 (crate (name "ju-tcs-rust-23-15") (vers "0.1.1") (hash "0mvja4wc9n6v55hw7h2fdqmmld8qx6hw3dxzlax8k62irkra5xdd") (yanked #t)))

(define-public crate-ju-tcs-rust-23-15-0.1 (crate (name "ju-tcs-rust-23-15") (vers "0.1.2") (hash "192hnbc5vf0waqb1d1976rri8l5r669lwf23c5n0ilvifqccyj2l") (yanked #t)))

(define-public crate-ju-tcs-rust-23-15-0.1 (crate (name "ju-tcs-rust-23-15") (vers "0.1.3") (hash "10lgciacxwck7i4hrpqbaa5cgklpidz92813yjgaxzlc4bp1kri3") (yanked #t)))

(define-public crate-ju-tcs-rust-23-16-0.1 (crate (name "ju-tcs-rust-23-16") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "06i3lr26ag485af1blnw13g2qj50bn4ygybg432q5kif94xhll6b")))

(define-public crate-ju-tcs-rust-23-16-0.1 (crate (name "ju-tcs-rust-23-16") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-15") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08sz25wx954ffkx28r6iw3kphy7010i8kcmha159ql6461q4nh0v")))

(define-public crate-ju-tcs-rust-23-2-0.1 (crate (name "ju-tcs-rust-23-2") (vers "0.1.0") (hash "1j3p7r8bcgxyyr834b3g6mls2f304da1al52b6i3hfakv4n34364")))

(define-public crate-ju-tcs-rust-23-20-0.1 (crate (name "ju-tcs-rust-23-20") (vers "0.1.0") (hash "1gy496wk12lxq1z8yxw59h9994a2zq0cd3n8bhsglrfd8q756msr")))

(define-public crate-ju-tcs-rust-23-20-0.1 (crate (name "ju-tcs-rust-23-20") (vers "0.1.1") (hash "0562jlp1r21sdrv46kag4iyyc1c1w28dvan5q5kjbgjh8bizc3b0")))

(define-public crate-ju-tcs-rust-23-20-b-0.1 (crate (name "ju-tcs-rust-23-20-b") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-20") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "159zi95cg0x7q27afg7n9qh16hs1f9sgphb91k477rgn5i7ga3rr")))

(define-public crate-ju-tcs-rust-23-21-0.1 (crate (name "ju-tcs-rust-23-21") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wsvajq8m6iqf6717wxh8mr3rlyncqbhgzzklk3g5yiqy6fdbhny")))

(define-public crate-ju-tcs-rust-23-21-0.1 (crate (name "ju-tcs-rust-23-21") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-22") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03rrj3dlc242zd3d09kghj2c89l2fshnbzi4d5d3j9icyygyz821")))

(define-public crate-ju-tcs-rust-23-22-0.1 (crate (name "ju-tcs-rust-23-22") (vers "0.1.0") (hash "08hv33zr4qiyf1h7lp6cq3jcmg0k11j8isvs7jx933zcxl2n4900")))

(define-public crate-ju-tcs-rust-23-23-0.1 (crate (name "ju-tcs-rust-23-23") (vers "0.1.0") (hash "078r3qa3g2whha5iccn0aszvz9k660nrc54pwj6850i0wqg3kivc")))

(define-public crate-ju-tcs-rust-23-24-0.1 (crate (name "ju-tcs-rust-23-24") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1110yi5qadfza7s39pqdgqxjls01h59h7k9vi7zbh465925hvpim")))

(define-public crate-ju-tcs-rust-23-24-0.2 (crate (name "ju-tcs-rust-23-24") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0in6n6ad2clcmjvcwxvqpfk1bgcq4gyxsa230fz5dd7pa45adnhq")))

(define-public crate-ju-tcs-rust-23-24-0.2 (crate (name "ju-tcs-rust-23-24") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0x8chgnqhb4zxfk1kn6q4s05ikkr5jgcy9bcn592jsl1vmgsd46i")))

(define-public crate-ju-tcs-rust-23-24-0.2 (crate (name "ju-tcs-rust-23-24") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1zb0qv1fygg0wnph9dg1n0qb96b587z4s1g6xs8wzcjbgliwhx2h")))

(define-public crate-ju-tcs-rust-23-24-0.2 (crate (name "ju-tcs-rust-23-24") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sjbdca839b8d2c9l5c8fsynb3nygjw7s3ndhkwz0f59cjd1cpwq")))

(define-public crate-ju-tcs-rust-23-24-0.2 (crate (name "ju-tcs-rust-23-24") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-23") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15hbrszr7dzwzh1h0naj4hxbsma6g8wcmpcy3l7v1v5lpqxr9j82")))

(define-public crate-ju-tcs-rust-23-25-0.1 (crate (name "ju-tcs-rust-23-25") (vers "0.1.0") (hash "0mz0m4zksa3p8x7460y58fyhxmghyff53brpi46avs3q608mkr4g")))

(define-public crate-ju-tcs-rust-23-25-0.1 (crate (name "ju-tcs-rust-23-25") (vers "0.1.1") (hash "17ffxr6f6la43l52fnhyza191dajkh3dvwxccdr78xb3fdikbkh7")))

(define-public crate-ju-tcs-rust-23-25-0.1 (crate (name "ju-tcs-rust-23-25") (vers "0.1.2") (hash "1n0ipbgg76isksv04kxrgf4dgjib249mzz8zyf7v6zv9wc9dyd26")))

(define-public crate-ju-tcs-rust-23-27-0.1 (crate (name "ju-tcs-rust-23-27") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03cchk2y6c1afl2q10qrfsp6788f8pnjkk42d8brcq8c3y3r4c52")))

(define-public crate-ju-tcs-rust-23-27-0.1 (crate (name "ju-tcs-rust-23-27") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-28") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "119ja76w0x4ybgpsbln3vl6sg8llqkh53sjd9spsq7kqb2332mjx")))

(define-public crate-ju-tcs-rust-23-28-0.1 (crate (name "ju-tcs-rust-23-28") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09cvj6p6wxz1mw6iq3qy8b7mrmhy3m0drygfngdrm71ijyndvfwl")))

(define-public crate-ju-tcs-rust-23-29-0.1 (crate (name "ju-tcs-rust-23-29") (vers "0.1.0") (hash "0g0grn6mvzjbzv5p0s7bknq6vkgp6vr5xchv7l05fa0n1n49qvmy")))

(define-public crate-ju-tcs-rust-23-30-0.1 (crate (name "ju-tcs-rust-23-30") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lmqda3axmr7pjbpkw122ppfynllcv3h11snc29qhk0b140n3v6a")))

(define-public crate-ju-tcs-rust-23-30-0.2 (crate (name "ju-tcs-rust-23-30") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-29") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x10h6if50l3aml79knxicc273703syfxabdk7pfxsqjmg8cylh1")))

(define-public crate-ju-tcs-rust-23-30-0.2 (crate (name "ju-tcs-rust-23-30") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-29") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1j8pkxh6y0mv99p3gxlww7nifmvkfk2l2d7k09xw9hb1wir1f7rb")))

(define-public crate-ju-tcs-rust-23-5-0.1 (crate (name "ju-tcs-rust-23-5") (vers "0.1.0") (hash "1wik9x672g6l73l09dsm3dxajqpsg5isyxs7yi8qr8fvlywwaclm")))

(define-public crate-ju-tcs-rust-23-5-0.2 (crate (name "ju-tcs-rust-23-5") (vers "0.2.0") (hash "0w2k6dllrvqkl5klzz4d7s81n2vi2bbdnn05bbgxwpvml75m8rxs")))

(define-public crate-ju-tcs-rust-23-6-0.1 (crate (name "ju-tcs-rust-23-6") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cmszyy894qksaa4ryagaizlh1h2z7p6cc48m27z5jyf7gndx9b0")))

(define-public crate-ju-tcs-rust-23-6-0.2 (crate (name "ju-tcs-rust-23-6") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-5") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1b02ilz7ghqqpwr7y0gmzh5vad2b482vacvl4hg6zbz9g4npgnca")))

(define-public crate-ju-tcs-rust-23-7-0.1 (crate (name "ju-tcs-rust-23-7") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14fw91cdpzslkppzvmhnbkjkp98h1wv45pwxqv3anpbxvnmja2w5")))

(define-public crate-ju-tcs-rust-23-7-0.1 (crate (name "ju-tcs-rust-23-7") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-8") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "131r9cygfmhvhsx8rys1v1ifhm2kj7mdhwvvlkphfk50h532fzah")))

(define-public crate-ju-tcs-rust-23-8-0.1 (crate (name "ju-tcs-rust-23-8") (vers "0.1.0") (hash "0szwm3rkv3wh5ad0iwn0xblh7ga2rcgavvyqahk7f7v9q7xmclfx")))

(define-public crate-ju-tcs-rust-23-8-0.1 (crate (name "ju-tcs-rust-23-8") (vers "0.1.1") (hash "09g15bkzw1f51f60ji62z7rgjnqr9krcrr01xzn7hq21g6jc5i4c")))

(define-public crate-ju-tcs-rust-23-8-0.1 (crate (name "ju-tcs-rust-23-8") (vers "0.1.2") (deps (list (crate-dep (name "ju-tcs-rust-23-7") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1j93y9llc9bf1mkfc5hhgsmx47kfdf0gfz1igjfdb3g15m53i2pw")))

(define-public crate-ju-tcs-rust-23-9-0.1 (crate (name "ju-tcs-rust-23-9") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pih6201hywyx8ssb4xa83vj5lx8a7js2grdf579gjr31sa3yqj1")))

(define-public crate-ju-tcs-rust-23-9-0.2 (crate (name "ju-tcs-rust-23-9") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-rust-23-10") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1b2nqf2d2q9b1hhzssqaa8kvwraixz8ig0hk3s4wczh7rwf2031z")))

(define-public crate-ju-tcs-tbop-24-bb-0.1 (crate (name "ju-tcs-tbop-24-bb") (vers "0.1.0") (hash "02jy5dwy1nmkwckaapi2cfyg0s8pfh3qqa5kizr7lppv10af6qwv")))

(define-public crate-ju-tcs-tbop-24-bebidek-endpoints-0.1 (crate (name "ju-tcs-tbop-24-bebidek-endpoints") (vers "0.1.0") (hash "1qmwi7zmmyjqnf60pwycc0rdfj3ggav7x59kwk5rzgqq3mfq5az1") (yanked #t)))

(define-public crate-ju-tcs-tbop-24-bebidek-endpoints-1 (crate (name "ju-tcs-tbop-24-bebidek-endpoints") (vers "1.0.0") (hash "1n94an7z2y75j1bcf3pjy1xwcz4xpq2i4sv6y2nm1anmxh0qv52f")))

(define-public crate-ju-tcs-tbop-24-bebidek-endpoints-2 (crate (name "ju-tcs-tbop-24-bebidek-endpoints") (vers "2.0.0") (hash "0jc1jcjsj48h7wb4kp8wr9nc0xf1p8qcakxj39lbb8mbshh9kbv2")))

(define-public crate-ju-tcs-tbop-24-blueoctopus-0.1 (crate (name "ju-tcs-tbop-24-blueoctopus") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-littlesquirrel") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1nq90dqg6j9bywb2cigc0bjav2dkpk6c70y8f8ij0rxs5wqxb0va")))

(define-public crate-ju-tcs-tbop-24-clappy-0.1 (crate (name "ju-tcs-tbop-24-clappy") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (default-features #t) (kind 0)))) (hash "10s3chmnkadwk8hs0b7qnmafw40bsrgjavj16vddk2prj5dmhcf3")))

(define-public crate-ju-tcs-tbop-24-clappy-0.1 (crate (name "ju-tcs-tbop-24-clappy") (vers "0.1.1") (hash "13f563yqmw2r9hcaxn09gyixc521xqi69xlaz4igqds1lp77h5sw")))

(define-public crate-ju-tcs-tbop-24-clappy-0.1 (crate (name "ju-tcs-tbop-24-clappy") (vers "0.1.2") (hash "1rh447bmkq526mqajakxjvvimbgbqs834qpzd901ynii4jk8cv44")))

(define-public crate-ju-tcs-tbop-24-clappy-0.2 (crate (name "ju-tcs-tbop-24-clappy") (vers "0.2.2") (hash "1j5dqmjvc775lcdr4v9w7b2xscprpsyfj85gm42vi4vka3vd51vc")))

(define-public crate-ju-tcs-tbop-24-dcfk-0.1 (crate (name "ju-tcs-tbop-24-dcfk") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04636zjb26xk86nd4bsj9jkkzv9wp69zbw72nbwcab1zsx9bmh4c")))

(define-public crate-ju-tcs-tbop-24-dcfk-0.1 (crate (name "ju-tcs-tbop-24-dcfk") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-lib-dcfk") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0nlyic1dl3w6bnpd4nnqpv4d4kbqqbwy1mhv6rj49zm850dn1y35")))

(define-public crate-ju-tcs-tbop-24-dcfk-0.1 (crate (name "ju-tcs-tbop-24-dcfk") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-lib-dcfk") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0k8cr1b3a9jbphnn051jbib3baxfyqaqnn00m709dzc7il1blyfl")))

(define-public crate-ju-tcs-tbop-24-dcfk-1 (crate (name "ju-tcs-tbop-24-dcfk") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-lib-dcfk") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0vliybk0yqbb461sw3banlw3qndlh35l7795k13wlp8p0fg71m7s")))

(define-public crate-ju-tcs-tbop-24-ganacci-0.1 (crate (name "ju-tcs-tbop-24-ganacci") (vers "0.1.0") (hash "1yl5hw407irgg225rmg0s5jr59z6p9v2yy6rp3p4sd3lra748g7s")))

(define-public crate-ju-tcs-tbop-24-ganacci-0.2 (crate (name "ju-tcs-tbop-24-ganacci") (vers "0.2.0") (hash "1dkhdrzhl6a1kw0s5wlq1nr7ixv3mass5q5lrjmlql6lph3hmv2j")))

(define-public crate-ju-tcs-tbop-24-ganacci-0.3 (crate (name "ju-tcs-tbop-24-ganacci") (vers "0.3.0") (hash "09sgx6nw5p0mp9pxw887w7l1qix7wgq3s62zchx5x25w5278d5w6")))

(define-public crate-ju-tcs-tbop-24-jfk-0.1 (crate (name "ju-tcs-tbop-24-jfk") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ph5p8daw2sva361rdbka6xhzfiknsjigirhdz37vjpfydglqsri")))

(define-public crate-ju-tcs-tbop-24-jfk-0.1 (crate (name "ju-tcs-tbop-24-jfk") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-bb") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pwvmhjrc4p7mzbn0wzgqmi6qjvcl4lzqjp5scknb7rsqmllfys0")))

(define-public crate-ju-tcs-tbop-24-jfk-0.1 (crate (name "ju-tcs-tbop-24-jfk") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-bb") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "023dnhy86a2q6nlawdh969c8gfj6nl7vi463vmh2293b27zafnyv")))

(define-public crate-ju-tcs-tbop-24-jjaworska-0.1 (crate (name "ju-tcs-tbop-24-jjaworska") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0i0rqwysmxpnxrmr1jfn4fg0kw62p538bppwzx04hdgkzv7wv3p7")))

(define-public crate-ju-tcs-tbop-24-jjaworska-0.2 (crate (name "ju-tcs-tbop-24-jjaworska") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-kasiazboltaa") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qnjjgdccgif7csjpikwnp28q86a5d1qsfpfx7x0vdjhdvc70s77")))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.1 (crate (name "ju-tcs-tbop-24-kasiazboltaa") (vers "0.1.0") (hash "1c1k23ij8fhp9b29x61ymdxc0xzz48ixgamb6cav9d0ylscgi8s4")))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.2 (crate (name "ju-tcs-tbop-24-kasiazboltaa") (vers "0.2.0") (deps (list (crate-dep (name "ju-tcs-tbop-24-jjaworska") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "10dglm9nd9v9isi4y4c7zcis1m3vmzl7naygidykz5db8jd2iqzg")))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.3 (crate (name "ju-tcs-tbop-24-kasiazboltaa") (vers "0.3.0") (hash "054xkhl2v881v0xz4jal3qc4qgnar4dcmz2m94ws4vna6jxmwd55")))

(define-public crate-ju-tcs-tbop-24-kasiazboltaa-0.3 (crate (name "ju-tcs-tbop-24-kasiazboltaa") (vers "0.3.1") (hash "13ylja4zcgav2n07apvdzjhw6va1l9g4fqp0bkdd616xffs96c1p")))

(define-public crate-ju-tcs-tbop-24-lib-dcfk-0.1 (crate (name "ju-tcs-tbop-24-lib-dcfk") (vers "0.1.0") (hash "1mjh1ijlq4a1ldlb48m0wmbpk5g3qcxj1y9c9s66p4q89dvvhk99")))

(define-public crate-ju-tcs-tbop-24-lib-dcfk-1 (crate (name "ju-tcs-tbop-24-lib-dcfk") (vers "1.0.0") (hash "101pqccq1srvq9la3mapr9l0vdvz1iifqz64cwi7j55bgs9qlkn2")))

(define-public crate-ju-tcs-tbop-24-lib-dcfk-1 (crate (name "ju-tcs-tbop-24-lib-dcfk") (vers "1.0.1") (hash "1l0zrq6n7ncy86nj4qp0wrr8b8q3lhzh87p0miih4aw3qr5m5ia6")))

(define-public crate-ju-tcs-tbop-24-littlesquirrel-0.1 (crate (name "ju-tcs-tbop-24-littlesquirrel") (vers "0.1.0") (hash "1yf5apj3pyq731r3m8d4wd5d09z32a1ij3zgjbahjsipf9v97ds4")))

(define-public crate-ju-tcs-tbop-24-lukaszgniecki-endpoints-0.1 (crate (name "ju-tcs-tbop-24-lukaszgniecki-endpoints") (vers "0.1.0") (deps (list (crate-dep (name "ju-tcs-tbop-24-bebidek-endpoints") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01q7bhc3jksws7id058v6kvkmalvgyrj94f50yzl9zj7zjxqlqqy")))

(define-public crate-ju-tcs-tbop-24-lukaszgniecki-endpoints-1 (crate (name "ju-tcs-tbop-24-lukaszgniecki-endpoints") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-bebidek-endpoints") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0zjp8p6skkcphnxzz9y34vs37s7pf4wvisi6sjhmw2j2zz6xl5zp")))

(define-public crate-ju-tcs-tbop-24-lukaszgniecki-endpoints-2 (crate (name "ju-tcs-tbop-24-lukaszgniecki-endpoints") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-bebidek-endpoints") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1shj0rhb36cyb1l8ayz75c7418knczsqmj177f3jqsbpzbk09bnx")))

(define-public crate-ju-tcs-tbop-24-oloi-tailhead-0.1 (crate (name "ju-tcs-tbop-24-oloi-tailhead") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qncsax42d8n0iahmpifbz48aviqbmcw38r489ydgcqmv64cgwf3") (yanked #t)))

(define-public crate-ju-tcs-tbop-24-oloi-tailhead-0.1 (crate (name "ju-tcs-tbop-24-oloi-tailhead") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-clappy") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0qibs0jsdhnvpp8ziinjwsm4d0h86qksf6bkbppgcbk0k2964mhm") (yanked #t)))

(define-public crate-ju-tcs-tbop-24-oloi-tailhead-0.1 (crate (name "ju-tcs-tbop-24-oloi-tailhead") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-clappy") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "01a7dpbrzkzk0ag7q0yz5zf1q8ba4l288n9hygdkifvzgiay48s0")))

(define-public crate-ju-tcs-tbop-24-pyzik-0.1 (crate (name "ju-tcs-tbop-24-pyzik") (vers "0.1.0") (hash "0fk1j4vxn30bcqangzf0jaycrlk1hs6mm7c4a8nf6s7s09dyg41h")))

(define-public crate-ju-tcs-tbop-24-pyzik-0.2 (crate (name "ju-tcs-tbop-24-pyzik") (vers "0.2.0") (hash "11q34gcvdszqhw96rr7gky6cmii3a5108hs29w4i16snqwlhyjby")))

(define-public crate-ju-tcs-tbop-24-salvatore-0.1 (crate (name "ju-tcs-tbop-24-salvatore") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1yy52argn7b1g7mb1ksa9vfc2mz86sy2mjr4sd6gzamzmj5yq5sl")))

(define-public crate-ju-tcs-tbop-24-salvatore-0.2 (crate (name "ju-tcs-tbop-24-salvatore") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-ganacci") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hy0avavfrxggmgw6ibk44cdli34k3mfvngjbf6jdjzf6vx9wjrx")))

(define-public crate-ju-tcs-tbop-24-salvatore-1 (crate (name "ju-tcs-tbop-24-salvatore") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-ganacci") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qy253ifhqqcxc2c68113r8bqxj3v41609d9a985dg7zgqabs9ll")))

(define-public crate-ju-tcs-tbop-24-salvatore-1 (crate (name "ju-tcs-tbop-24-salvatore") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-ganacci") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "048ajn51c582ggv38m8npalw08yaxnwp39xp8igl10i64a9c07p9")))

(define-public crate-ju-tcs-tbop-24-spyrzewski-0.1 (crate (name "ju-tcs-tbop-24-spyrzewski") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0h5c0940ql0h3ng4x17c9vaxnbq6ymkffk3kgg0pndqnidad2ghp")))

(define-public crate-ju-tcs-tbop-24-spyrzewski-0.2 (crate (name "ju-tcs-tbop-24-spyrzewski") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-tbop-24-pyzik") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0as717wq2wb2w114fhwnqjlihv2rd6zv1xi9z79kpm0wrfag01p7")))

