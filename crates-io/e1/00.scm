(define-module (crates-io e1 #{00}#) #:use-module (crates-io))

(define-public crate-e1000-driver-0.1 (crate (name "e1000-driver") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "volatile") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1d5j3q1v4vppxis8hcn6zidcx5npx4iq7rvc0qs83r66kfv66c3p")))

