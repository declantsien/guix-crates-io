(define-module (crates-io e1 #{64}#) #:use-module (crates-io))

(define-public crate-e164-phones-countries-0.1 (crate (name "e164-phones-countries") (vers "0.1.0") (hash "0qncxwmcm2cdbpchgx16pcdxvl2m6z1wbk3z0y7h7iq2xngckxy0")))

(define-public crate-e164-phones-countries-0.1 (crate (name "e164-phones-countries") (vers "0.1.1") (hash "1svlzbgvngx6cnh58zq2frz134y6rlb2sy4idy7aa2n8d718lf84")))

(define-public crate-e164-phones-countries-0.1 (crate (name "e164-phones-countries") (vers "0.1.2") (hash "0mi4iz5m6683v70f8fi4pl2nr0zfznqbgka02ihf3a0kbaas53lc")))

