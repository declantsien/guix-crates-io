(define-module (crates-io sn bc) #:use-module (crates-io))

(define-public crate-snbclabelprint-0.1 (crate (name "snbclabelprint") (vers "0.1.0") (deps (list (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.135") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "135b9avgm7c654wr4a569aim3i9df1f8h2yp890vrjbfk33jgxr8")))

