(define-module (crates-io sn zi) #:use-module (crates-io))

(define-public crate-snzip-0.1 (crate (name "snzip") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "06gl2i0y6iig75rf0s58y0vhyb3hygz8yb244hsypa2chz64y57h") (yanked #t)))

