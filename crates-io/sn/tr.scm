(define-module (crates-io sn tr) #:use-module (crates-io))

(define-public crate-sntru-lprime-0.0.0 (crate (name "sntru-lprime") (vers "0.0.0") (hash "182c5nw375p8mwzwyxsgxfb7vslcydxcdb611m9v7ladmnjg3h0r") (yanked #t)))

(define-public crate-sntru-prime-0.0.0 (crate (name "sntru-prime") (vers "0.0.0") (hash "06wxw9bnzvbrjg3a7aqmzhrqyyk6fzg2hj3957i8cl97yqix3rbs")))

(define-public crate-sntrup-0.0.0 (crate (name "sntrup") (vers "0.0.0") (hash "01qi2ppzjdn0lqhd81w5s5yrahnqz28j2bd02il6850dv93aswzh")))

