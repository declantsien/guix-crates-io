(define-module (crates-io sn df) #:use-module (crates-io))

(define-public crate-sndfile-0.0.0 (crate (name "sndfile") (vers "0.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sndfile-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "13ig8vbx52hx45yvwl0gcmlkbq21zzl1y5a9rp96aybpd3ymskiy") (features (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.1 (crate (name "sndfile") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sndfile-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q2ja0ld3ikqzppw5nrby5sfx2nqyxczhjbma9vda4m4lhsp2x90") (features (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.2 (crate (name "sndfile") (vers "0.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sndfile-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "05ncpyrc786zp78l52ma6b62llprhvvzflmd9d5gbq4jmh2sx0gp") (features (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.3 (crate (name "sndfile") (vers "0.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sndfile-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1gkf4s4dw780qvhmyfr0bkhl04wz2akfjfkqckf72mn80f56anix") (features (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.0.4 (crate (name "sndfile") (vers "0.0.4") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sndfile-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0vc002hjshq89lq0s6pbgvzry5x7ds8n59rm506bmb340i8w61qa") (features (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.1 (crate (name "sndfile") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sndfile-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1gsqlz4fd0h78s69pll0pbfr53d1x84bkwvb4c3lph48kyq2s857") (features (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-0.1 (crate (name "sndfile") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sndfile-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1w8g53bmsyg31rvwxz518vybyv6xmwwigal2n65ardcdrrk45bz5") (features (quote (("ndarray_features" "ndarray") ("default"))))))

(define-public crate-sndfile-sys-0.1 (crate (name "sndfile-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18yj9phv0qbkkawg5csrlzkd3fd15c198rynqyy92ki3f2b8fl8x")))

(define-public crate-sndfile-sys-0.1 (crate (name "sndfile-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a3rib76ndj4m03hpc23k2lxqpbsryi7ss8n8nm5nmj4chmp50ym")))

(define-public crate-sndfile-sys-0.1 (crate (name "sndfile-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (kind 1)))) (hash "176pk3jq9fmwv2mb8a0vv9xh3n1jnhb3wfqk9xkkw7js3grrwq6w") (links "sndfile")))

(define-public crate-sndfile-sys-0.2 (crate (name "sndfile-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (kind 1)))) (hash "0d71i5gk2r606ml0pk14iaxhksisrk2hgziz7skiivz8fwnyp5jb") (links "sndfile")))

(define-public crate-sndfile-sys-0.2 (crate (name "sndfile-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (kind 1)))) (hash "1qv2k2dm9in5fm08z5m96dz9d6r3zkhn20fdw6kbx082yfazb9bs") (links "sndfile")))

(define-public crate-sndfile-sys-0.2 (crate (name "sndfile-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (kind 1)))) (hash "0nzdc27j4sb3l57djn8xqdlsy0jmdsj8z00wlvlkr0rs6vkd5g1v") (links "sndfile")))

(define-public crate-sndfile-sys-0.3 (crate (name "sndfile-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (kind 1)))) (hash "1wb6lznifdhr1pvmcn4bs3yiv7nhkw38b7n9xavrk6bsm94xk6dj") (links "sndfile")))

