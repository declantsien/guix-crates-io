(define-module (crates-io sn #{32}#) #:use-module (crates-io))

(define-public crate-sn3218-0.1 (crate (name "sn3218") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "11sdrcx3gfpsjiy0cs1lsh068l9rcmykxw7892ddq7snly6n528w")))

