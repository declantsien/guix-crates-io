(define-module (crates-io sn ed) #:use-module (crates-io))

(define-public crate-snedfile-0.1 (crate (name "snedfile") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tcp-test") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "1d496syfqzd06qw1xszi9y3f854dzwyf88ydyddjgrr6jmjxjjcb") (features (quote (("large-files") ("ios-sendfile") ("fallback-bufreader") ("fallback-buf") ("default" "fallback-bufreader" "ios-sendfile"))))))

