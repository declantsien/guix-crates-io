(define-module (crates-io sn dc) #:use-module (crates-io))

(define-public crate-sndcld-0.1 (crate (name "sndcld") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1fylkypc8kkj0jfxmgcgknzy54wnmn3gqppywaixzx5hgpphxrz3")))

(define-public crate-sndcld-0.2 (crate (name "sndcld") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1wjn3v87f4kc8hcs8brnadlbw91wahcy8qhfg24rmirrfzxnpdv4")))

(define-public crate-sndcld-0.2 (crate (name "sndcld") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0mknvs41386254n3hh65hab1yr096s4kwybr0a974yxi1xz80d01")))

