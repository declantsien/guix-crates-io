(define-module (crates-io sn or) #:use-module (crates-io))

(define-public crate-snor-0.1 (crate (name "snor") (vers "0.1.0") (hash "0azjnwipczl5hzrznjwb3k6n1kb7610dywshbsvr0n0id9nmms5s")))

(define-public crate-snor-0.1 (crate (name "snor") (vers "0.1.1") (hash "12d58il0m2bj4bz1ch0dn4dm4sv136gafvnqf1jw6jn9vdn80a60")))

(define-public crate-snore-0.1 (crate (name "snore") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0vvhz8q5f9b1n7xkhqxgfl56x2gjmsdr7vxkns4dbb9ld7rilfy5")))

(define-public crate-snore-0.1 (crate (name "snore") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "14wrshyfhy5x2sjkcas04ncwq0pjhnbshmf0xf7v9hsrh87p3rc7")))

(define-public crate-snort-0.1 (crate (name "snort") (vers "0.1.0") (hash "0azy1v3kp1shyjfpbmzmygkklsg511v5i95k7v8a6yybllxkvq0f")))

