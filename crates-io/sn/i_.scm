(define-module (crates-io sn i_) #:use-module (crates-io))

(define-public crate-sni_rs-1 (crate (name "sni_rs") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("io-std" "io-util"))) (default-features #t) (kind 0)))) (hash "16w72wv1w0c6q69zd0rgdh08rp6mqpx07nbcfqbmxd2vf5mks9dj")))

(define-public crate-sni_shuffle-0.1 (crate (name "sni_shuffle") (vers "0.1.0") (hash "134rp1f5al2px4s1kigcdb38vs5w3x0jkbaf4jdnc4iab4c489zz")))

