(define-module (crates-io sn di) #:use-module (crates-io))

(define-public crate-sndio-sys-0.0.1 (crate (name "sndio-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "143gccwfksspfs389ivb40bpnpqqyqwkvw7jm200cnhcwxaww180")))

