(define-module (crates-io sn uf) #:use-module (crates-io))

(define-public crate-snuffles-0.1 (crate (name "snuffles") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 0)))) (hash "0cajw8ivwwjysdx8s7fcf7ykb6hy3l0g8dsp8iqzq61s4r8s6cbs")))

