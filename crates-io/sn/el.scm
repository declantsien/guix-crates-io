(define-module (crates-io sn el) #:use-module (crates-io))

(define-public crate-snells-0.1 (crate (name "snells") (vers "0.1.0") (hash "1j89k7isqxmqdary0nj8mckks6g4ivyyq11rv3z1hhlsf8h9jwhw")))

(define-public crate-snels-0.1 (crate (name "snels") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0hp6gr7fdxm4i2lyg1jcnplmppav1pyrm0wv2chwh40xi1a2cmzs")))

