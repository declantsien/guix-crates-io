(define-module (crates-io sn at) #:use-module (crates-io))

(define-public crate-snatch-0.1 (crate (name "snatch") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)))) (hash "1f888fv1dqz5g1jpyrwnwj0gkp0lyqxpk4cjj9sq41ybjh92k6fk")))

(define-public crate-snatch-0.1 (crate (name "snatch") (vers "0.1.3") (deps (list (crate-dep (name "ansi_term") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0nsqlpcvcbbcmjap0xwv968kzi8c7b7s4lzr5ckicw27ci8l2kl5")))

