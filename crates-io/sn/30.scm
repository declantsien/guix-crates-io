(define-module (crates-io sn #{30}#) #:use-module (crates-io))

(define-public crate-sn30pro-0.1 (crate (name "sn30pro") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("fs" "io-util"))) (default-features #t) (kind 0)))) (hash "0xag3wwdpnalcp66rqm6hyhdylp5a14963vx0nycrsp55b5fqqvp")))

(define-public crate-sn30pro-0.2 (crate (name "sn30pro") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("fs" "io-util"))) (default-features #t) (kind 0)))) (hash "0wl4jban3gycjbn6m51dgg2x9v8l7bvhhv08v1kaac0jjmdidf36")))

