(define-module (crates-io sn es) #:use-module (crates-io))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.0") (deps (list (crate-dep (name "emu") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yik51xl62ba3calir77dvpvvd77l4q5ws78l7f6db4m65pp87lm")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.1") (deps (list (crate-dep (name "emu") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13lmrw12srhbbfhxf93h72cak8b2fvd106imhc46xmggjvvxmfrj")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.2") (deps (list (crate-dep (name "emu") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1c7ypg337fljnm20v08h46l3yfd96xfn54754jlb21ya4kmp6dp5")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.3") (deps (list (crate-dep (name "emu") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00biv1fzxkv9zrxc91xm4kay736g8pcmjwsz6yp5yj9gm1fgxb27")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.4") (deps (list (crate-dep (name "emu") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "07lkjg2ah6h34llgd6xmvygiafz15ybf0mi6ljl475p61mi7i3f0")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.5") (deps (list (crate-dep (name "emu") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hb4j9kdg6a274wf7rq3lcvrwgl2h4hi6vksj79p8qm7nhdmvfm5")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.6") (deps (list (crate-dep (name "emu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b1likckiy6vnd636q5mkpwzgkci3bqmyn3f8cbpywlircxz2nl3")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.7") (deps (list (crate-dep (name "emu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mycma2mkm6vacp4q5wk75li98iyrsfi43lqxq59022kp0id5mig")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.8") (deps (list (crate-dep (name "emu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b4z8ym1571rgh213ghrw8am3vmgivms9qbrd3mcm6vzj90y3i8m")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.9") (deps (list (crate-dep (name "emu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n01lm428fk4i6mrk2321a26gfdfzmf3bbz9ghbap0n1y28q5dn3")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.10") (deps (list (crate-dep (name "emu") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "101aqccq2lvqgi9h36b5hbmhkxri5frr7kjbwxfzwcz6fnx2j8p9")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.11") (deps (list (crate-dep (name "emu") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02zsm7dklldi5zp5x7lcz76amvmbm1m74f5p5nh00ii44nqnvi14")))

(define-public crate-snes-apu-0.1 (crate (name "snes-apu") (vers "0.1.12") (deps (list (crate-dep (name "emu") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "spc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jgn8riryc2dsfx42zwffl1iarm7m876j1cp5b23qqk9sxqw53a8")))

(define-public crate-snes-bitplanes-0.1 (crate (name "snes-bitplanes") (vers "0.1.0") (hash "0zxdl5pwgdp4xzcpyrr04x0rmhcva8l657mz0v4m2zrncv0spvrk")))

(define-public crate-snes-bitplanes-0.2 (crate (name "snes-bitplanes") (vers "0.2.0") (hash "17yvm7gr31gllr24ikrmryzgjg43ylfg65jjgx3l4mh9hspa1y8h")))

(define-public crate-snes-bitplanes-0.2 (crate (name "snes-bitplanes") (vers "0.2.1") (hash "1j3nibcnz6yk3fn359dm06adzkh5hpnbz4kgldlsn9fl13plajga")))

(define-public crate-snes_address-0.1 (crate (name "snes_address") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "1vx8xrnaday6l998c8srv0ijrf167kw6k8gnh9w5q2n0m0s8h6vg")))

(define-public crate-snes_address-0.1 (crate (name "snes_address") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "004nb48r0mcvqfagqyw9p1n2drf7ppk5v07axm9cbf68n8jmvv0m")))

(define-public crate-snes_compress-0.1 (crate (name "snes_compress") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "0ng590lvvvzc9nbrmp187hljljibb502vz1dy01m34f6kv9458mx")))

(define-public crate-snes_compress-0.1 (crate (name "snes_compress") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "1k66zfxnhlziidkjly3q4c96a1zn2fmbjzflhpawpgq2l3w7l4ky")))

(define-public crate-snes_compress-0.1 (crate (name "snes_compress") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "0q7si43kgvjf6hdgnk312ivw4q1d6n9d5s1hggj3qvgcz12a4dji")))

(define-public crate-snes_compress-0.1 (crate (name "snes_compress") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "1jwlahkzn44wh3hxzps653avkcq4vkpd2fzdj5pxmvyirgs991lk")))

(define-public crate-snes_compress-0.1 (crate (name "snes_compress") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "1.*") (default-features #t) (kind 0)))) (hash "1iwdmcmz6615chjx55drlbwlrcsc2086dymriyg3nx4fl2v9zj51")))

