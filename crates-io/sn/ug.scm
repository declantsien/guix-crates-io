(define-module (crates-io sn ug) #:use-module (crates-io))

(define-public crate-snug-0.0.1 (crate (name "snug") (vers "0.0.1") (deps (list (crate-dep (name "sha3") (req "^0.10.1") (kind 0)))) (hash "0jj2p20lw13x4bhl08kd66kah60qsz594sxmlin0a3pk2b8ym84z") (yanked #t)))

(define-public crate-snug-0.0.2 (crate (name "snug") (vers "0.0.2") (deps (list (crate-dep (name "sha3") (req "^0.10.1") (kind 0)))) (hash "00467g40aq9wxdkdk4sl11g8ma7fn08wkih5wlmlp6jg5akk7kbs") (yanked #t)))

(define-public crate-snug-0.0.3 (crate (name "snug") (vers "0.0.3") (deps (list (crate-dep (name "ees") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (kind 0)))) (hash "05ygjp53wr1g09ycm0bkyazcvl584843ckvhl5yl1lp70i060x1x") (yanked #t)))

(define-public crate-snug-0.0.4 (crate (name "snug") (vers "0.0.4") (deps (list (crate-dep (name "ees") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (kind 0)))) (hash "1fs5g6q8kfk54mi77r8x1d85k6khnd6vnnz8bj7w4gy86hjsqwzc") (yanked #t)))

(define-public crate-snug-0.0.5 (crate (name "snug") (vers "0.0.5") (deps (list (crate-dep (name "data-encoding") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "ees") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (kind 0)))) (hash "1zvfm87ipwrb5wn546g78z65g5qjg0lmb4hgzf0b9sxr6z4d60g6") (yanked #t)))

(define-public crate-snug-0.0.6 (crate (name "snug") (vers "0.0.6") (deps (list (crate-dep (name "ees") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (kind 0)))) (hash "14anwvr713n812aygrw66jw4d0bc3wx20am93bzg1ymjqibnysal") (yanked #t)))

(define-public crate-snug-0.0.7 (crate (name "snug") (vers "0.0.7") (deps (list (crate-dep (name "basen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ees") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (kind 0)))) (hash "1mmsnqvyv4nkvr9gip0d59sl207s5ry69q8flr4g0nkgixc8p2j1") (yanked #t)))

