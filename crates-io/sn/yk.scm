(define-module (crates-io sn yk) #:use-module (crates-io))

(define-public crate-snyk-config-0.1 (crate (name "snyk-config") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1ap4bwl680ra4f54iiivgyr3qy4favik5bysi2mjc3k6mqz4ywvi")))

(define-public crate-snyk-config-0.2 (crate (name "snyk-config") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "10h2ms4ibx46nw1six3c5g0vylfz74708kq2z39ich3hjsp501fm")))

