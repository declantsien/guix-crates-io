(define-module (crates-io sn dj) #:use-module (crates-io))

(define-public crate-sndjvu-0.0.0 (crate (name "sndjvu") (vers "0.0.0") (hash "0b4fsi7ijn8fnzf827dbb8i6m5ar9gkwx10rlpm5c5k5z32pvyx4")))

(define-public crate-sndjvu_codec-0.0.0 (crate (name "sndjvu_codec") (vers "0.0.0") (hash "06bg9ji4iajl0asbwc03kpczvvmcmixvgqdalas2hpb8cw2xlbky")))

(define-public crate-sndjvu_format-0.0.0 (crate (name "sndjvu_format") (vers "0.0.0") (hash "1nmfmdfkx84qz6h1pqb6jpsgfnfr3xhvvv8r2zf5gsjz5h8wwyd3") (yanked #t)))

(define-public crate-sndjvu_format-0.1 (crate (name "sndjvu_format") (vers "0.1.0") (hash "1gmqwilz16216n7qw04caj3fmx1ykfkxk3z522lc68nsvgb7fixg") (features (quote (("std") ("backtrace" "std")))) (rust-version "1.65.0")))

(define-public crate-sndjvu_format-0.1 (crate (name "sndjvu_format") (vers "0.1.1") (hash "0mm8369vzgp6mhk7hp9lpz1wvpgi0r979zxzfni0s70dghsp81hh") (features (quote (("std") ("backtrace" "std")))) (rust-version "1.65.0")))

(define-public crate-sndjvu_format-0.1 (crate (name "sndjvu_format") (vers "0.1.2") (hash "13pglxwn3da7618hhgingd5zh8zrigxm4lsz2wxfbzg0fdpiixm2") (features (quote (("std") ("backtrace" "std")))) (rust-version "1.65.0")))

(define-public crate-sndjvu_format-0.2 (crate (name "sndjvu_format") (vers "0.2.0") (hash "06vwb7xdwmhybkhyzyx2mvcjaxr5jd9ngi6hq64pi546n8ybyjca") (features (quote (("std") ("backtrace" "std")))) (rust-version "1.65.0")))

(define-public crate-sndjvu_toolkit-0.0.0 (crate (name "sndjvu_toolkit") (vers "0.0.0") (hash "1syv8pl2fyrgzcjsz5bji8msm66k5lrg1hx5109876x9a2d2vff8")))

