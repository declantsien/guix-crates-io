(define-module (crates-io sn ax) #:use-module (crates-io))

(define-public crate-snax-0.1 (crate (name "snax") (vers "0.1.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-nested") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "snax_impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b87axmlp01wf4kgjkqa7n12mjg9nyyrvgp2f59vsi5mkpf1d920") (yanked #t)))

(define-public crate-snax-0.2 (crate (name "snax") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)))) (hash "1br1wgja7bjqfprqfvbkhicw32a83w7j74mr39q4bbpkpbh2py3y")))

(define-public crate-snax-0.3 (crate (name "snax") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 2)))) (hash "0aihazg5cj7d1maji3bfhm64bglv6b7fxb8nbzalrg2g8zkhszs4")))

(define-public crate-snax_impl-0.1 (crate (name "snax_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "snax_syntax") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rnmh1bq8xpgz76pxbh998vp5x8jz0f5wpbyzgq8w6q6r5v8j7dl") (yanked #t)))

(define-public crate-snax_syntax-0.1 (crate (name "snax_syntax") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)))) (hash "13cg03c3xxm7lamk3d87kvb3c9598grrhmlm4h459mai416h93f8") (yanked #t)))

