(define-module (crates-io sn ec) #:use-module (crates-io))

(define-public crate-snec-0.1 (crate (name "snec") (vers "0.1.0") (deps (list (crate-dep (name "snec_macros") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ncjbji4l81rky43n62mhnkjicijpfm666zwq55cqaxid008l5aq") (features (quote (("std") ("macros" "snec_macros") ("default" "std" "macros"))))))

(define-public crate-snec-1 (crate (name "snec") (vers "1.0.0") (deps (list (crate-dep (name "snec_macros") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1h7lblvf479pn8riz7inn2085cizm5kzwj89b5nkjwy5limv71xs") (features (quote (("std") ("macros" "snec_macros") ("default" "std" "macros"))))))

(define-public crate-snec_macros-0.1 (crate (name "snec_macros") (vers "0.1.0") (deps (list (crate-dep (name "fehler") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b62qn651qxhb700f975sjahpmk3h655x778fafj4ayjvxg8ni4n")))

(define-public crate-snec_macros-1 (crate (name "snec_macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cfpwkh6ddm340am4q01dz2ksn2qsdjfmnywdnpkhag4c51sxmif")))

