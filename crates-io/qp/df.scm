(define-module (crates-io qp df) #:use-module (crates-io))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "00f8rczn772bzjp6v6yzr5nqsky646swk2vyl4p5hajhkvwd2h0n")))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zgaykf8q6825f3aidd9pkg77wwrlk1b2kb9pksfjm5d8n9p1pdp")))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0davbih494abkym280jc2i4n821kh9i7gqvhri91m9bxrca1iyig")))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "01lsa66ffs5w7q3x9a5zqfl44fbgpiyy542qdm4nbc5gma70d65a")))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "119a4h43afhn7mdz4rq4ga2zmxr8jd2hmr0j8rji0ij8j3bz2gql") (yanked #t)))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "08qcdpxm6x09z54q06sp5hfdkagmwkcdjm2583x5samc2x6zspch")))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1bsinhns8183fjwwz4aqz9343sbha3k5s6bjbszl17sihfgdaf7z")))

(define-public crate-qpdf-0.1 (crate (name "qpdf") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0adibgr682pp47jf2nnry3gw4xfbnzsbzpcvpb4kyqdf8j9snvrd")))

(define-public crate-qpdf-0.2 (crate (name "qpdf") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f7w9jk2d4wc3afix9xd3j2myhrhyajrmyhjz59nd2vshk354fys") (features (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

(define-public crate-qpdf-0.2 (crate (name "qpdf") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "091my2iqf4ql38xlv7b98jg8r52mn4539wmm091whin4q1bbvbb8") (features (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

(define-public crate-qpdf-0.3 (crate (name "qpdf") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0s8fyxas04xs5p1c1nd9hv4psnc3y0v4iikidglg40wgc97m5ml0") (features (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

(define-public crate-qpdf-0.3 (crate (name "qpdf") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qpdf-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "042illdb4ljjigq426hfsp16c40cyvfkdigd0p7ii9wfkcwq35aq") (features (quote (("vendored" "qpdf-sys/vendored") ("legacy"))))))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0khdavv3r8s96042b2zj83iims6c98f0q8q4x52i5i5yd2m3bcxp")))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1gh00yw6sxgck1my4acbs5p0hv1k3z3sfd2d7hgrkxnyzim127l9")))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1lzfwxfzgg26cr4jpc84zk5f644xdklsw1dric2msy23wvjxbllr")))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "08rwd4w9p55m7723g3d4flmkyz7wahvcs7pr59nvsj0yb3x30x8k")))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0qsikcs6g5rrmpcwc8zw4y9yl58lxrvl0gfjwwbhc0zgsvapqgm2") (yanked #t)))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1a0zicf8k7l98riprx9i5qw4rbyncnq3nj775q2da863i75jbq6d")))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "110l3w90898kfd6j6ykwsm1njbx9m47yhjkawadryfkzhqxdqms0")))

(define-public crate-qpdf-sys-0.1 (crate (name "qpdf-sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "18yxs9i0z8hs5mjncdxhax2va6c4ic11dyshnl63hl9hz7vw6ix1")))

(define-public crate-qpdf-sys-0.2 (crate (name "qpdf-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1i8wxy0hi3iqq2d0pwrmcwx2cfsx27lq2svf9w1ivsd5ggnz4n57") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-qpdf-sys-0.2 (crate (name "qpdf-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1v1ppcqczp3hwrp8jfwydp0jyhwhw5mffsmhbd9705dndbj0cndn") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-qpdf-sys-0.3 (crate (name "qpdf-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0rkx0362v2qwbxvbcjiv2ys03469fjs9gacxkkf5iswcqcv1gzcp") (v 2) (features2 (quote (("vendored" "dep:cc"))))))

