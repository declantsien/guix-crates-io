(define-module (crates-io qp tr) #:use-module (crates-io))

(define-public crate-qptrie-0.1 (crate (name "qptrie") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug_unreachable") (req "~0") (default-features #t) (kind 0)))) (hash "09xjd07zs82x48yxa9zgc1hqx702c2g1bg3b2lx9j6yrb4dfzldx")))

(define-public crate-qptrie-0.1 (crate (name "qptrie") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug_unreachable") (req "~0") (default-features #t) (kind 0)))) (hash "063gra6m1ji1qihiv6zzh78b64rw4k0mjl86vnflf5v0ns3vqxai")))

(define-public crate-qptrie-0.2 (crate (name "qptrie") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug_unreachable") (req "~0") (default-features #t) (kind 0)))) (hash "1lqyb6n876856msbssigz6662bxjqmzk87967z1nzxsgr10dy004")))

(define-public crate-qptrie-0.2 (crate (name "qptrie") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug_unreachable") (req "~0") (default-features #t) (kind 0)))) (hash "1w48sla3gzslld4b5bwgpknw75r4jdsn8vj7wdbiqhci61yms1vc")))

(define-public crate-qptrie-0.2 (crate (name "qptrie") (vers "0.2.2") (deps (list (crate-dep (name "clippy") (req "~0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug_unreachable") (req "~0") (default-features #t) (kind 0)))) (hash "020f4rnffbsbd6mcml6ps21j5ikxyby2lhh0h7q4n8hyy5k305s1")))

(define-public crate-qptrie-0.2 (crate (name "qptrie") (vers "0.2.3") (deps (list (crate-dep (name "debug_unreachable") (req "~0") (default-features #t) (kind 0)))) (hash "102xpnyaqi0advpdc0ls371jn787nh7fxjn0j4bggbyvd426d99i")))

(define-public crate-qptrie-0.2 (crate (name "qptrie") (vers "0.2.4") (deps (list (crate-dep (name "debug_unreachable") (req "^0") (default-features #t) (kind 0)))) (hash "0yk31bklmn9d07yi5b4qj9i7nl5dm1d430pspx9w7fv2438mwqwa")))

(define-public crate-qptrie-0.2 (crate (name "qptrie") (vers "0.2.5") (deps (list (crate-dep (name "new_debug_unreachable") (req "^1.0") (default-features #t) (kind 0)))) (hash "04ghn8j8pgs7h67hn74vjhzl7rnnb1xawzv78g86hwp7sfk56br9")))

