(define-module (crates-io qp pr) #:use-module (crates-io))

(define-public crate-qpprint-0.1 (crate (name "qpprint") (vers "0.1.1") (deps (list (crate-dep (name "terminal_size") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0af15isd5f3w0d6pjnyfbi9qn37q36zf84dc218v60ql5pkby3gn")))

(define-public crate-qpprint-0.2 (crate (name "qpprint") (vers "0.2.0") (deps (list (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "01rvpp71fxhgaxgxcmgkja0b4ppq7l2c87w0kwzaavb8bdqf24kk")))

