(define-module (crates-io qp id) #:use-module (crates-io))

(define-public crate-qpid_proton-0.0.0 (crate (name "qpid_proton") (vers "0.0.0") (hash "00w4xlwysr5764pvmmyrjsdn95v6aymrbjcmj1z5r3hpp9p2m7dn")))

(define-public crate-qpid_proton-0.0.1 (crate (name "qpid_proton") (vers "0.0.1") (hash "1pdhf6x1v5s8ja59q452dr71rb3jmj2frwsqm6xjgiclxbzrhhpp")))

(define-public crate-qpid_proton-0.0.2 (crate (name "qpid_proton") (vers "0.0.2") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1rjy6dh31ipi1x06n1sryvv52448aszrbyyna1wvn44ldb0bh7ig")))

(define-public crate-qpid_proton-0.0.3 (crate (name "qpid_proton") (vers "0.0.3") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "09sq73zgy2lfwki78mg4cdw3h0pd8g06k3rv0dsljid43l9j2dk8")))

(define-public crate-qpid_proton-0.0.4 (crate (name "qpid_proton") (vers "0.0.4") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1wk9wmi92r5zx2mmn0qp07ws5sfwbm8hgb1rb0im5hgdn5jnqql5")))

(define-public crate-qpid_proton-0.0.5 (crate (name "qpid_proton") (vers "0.0.5") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0m597mmdmscazz5ddak6mc5fmz5ar4qmfbkjdhpfqg6c4pl1y0kj")))

(define-public crate-qpid_proton-0.0.6 (crate (name "qpid_proton") (vers "0.0.6") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1fihfmksjyxkyj7w6c5fb4h32frsxngdhpyx247zp4rcchfbxzvf")))

(define-public crate-qpid_proton-0.0.7 (crate (name "qpid_proton") (vers "0.0.7") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "14fiz4v7n3dvvjnmzfvhwka1sqv297plagmg34rhx83b4r1zqa8b")))

(define-public crate-qpid_proton-0.0.8 (crate (name "qpid_proton") (vers "0.0.8") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0km4caf5xkz6adhqm414m6h2qlcqk7k7v79v3k22g9jqrbq1d4xc")))

(define-public crate-qpid_proton-0.0.9 (crate (name "qpid_proton") (vers "0.0.9") (deps (list (crate-dep (name "cmake") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req ">=0.8.0, <0.9.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "17ar46yajj8irib5ypz724wbj70qzkav171mc0fm0mczf7qqbhm8")))

(define-public crate-qpid_proton-0.0.10 (crate (name "qpid_proton") (vers "0.0.10") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "qpid_proton-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0yba8wlv85b9xxvfy8djikbn99235gzarsxwcfpjgdx8p21p1f1m")))

(define-public crate-qpid_proton-sys-0.0.1 (crate (name "qpid_proton-sys") (vers "0.0.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "10p64qsxggghbfrpihhdlv48aa1l6wzlil53lxbcv9a7rfh0v5jy")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1amn75pzh67a5vz2mm2nvxqqwyamk3j1j52lywdg68yxp3hh6xkh")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0gbn8qkljgjy04ihdv7v9db3ad0ws6ywc2jxaw6qxdm40173lvpz")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0qwzpqf8ckbwsp32cgn5cnsrr9aw29xh29n4if0d355hazx9wmgx")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0n10211lvlisvxpwfv2n6xd35bzkawqmd74b1rihkzkma35sqs0v")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0gcbihk589jj8ap4cmi2zjfkq7z9a489d2ss0b7hq7aw06rd6fd4")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "16lm8n3lhfpi0m9101xflvv7lfv3p5x66j6brj6acbs61ghciy17")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.6") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "00miz5mgzkh7avd9ay4izwljqil5bcy3sy8amzd0a0wg1as6h735")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.7") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1h1g2gw5d03ixs35rd92kvrg5vxzxnkr8jgarmpgyhii7ggrw5ky")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.8") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1128rmnmqm0kvj6i0xicrmpqxz5haysw0m09hkliapcclq7gykkn")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.9") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1qmqyy9z2dhffjprrlwkma9mm39qpp31bl87phdryppiy6p03wq1")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.10") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0f4l796a6ma02y1jxkxyci9nm4ms82b53s4xpjg6z31bgrmykyfv")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.11") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1qmi5kbq1kinr4a5p6jk3pj0qsmcqjskiyg7jn9pr18rs6y412r9")))

(define-public crate-qpid_proton-sys-0.1 (crate (name "qpid_proton-sys") (vers "0.1.12") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0lgyr6gj7c2ybsc64xiv513ca3ss0q4ffpvdn1vgrap7xh4yy7m7")))

(define-public crate-qpidfile-0.9 (crate (name "qpidfile") (vers "0.9.2") (hash "094rn3blah2x79iv0kgjn9l7ciay0gi3dgfrhq1ga633hpmhl1y7")))

