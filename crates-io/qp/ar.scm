(define-module (crates-io qp ar) #:use-module (crates-io))

(define-public crate-qparas-0.1 (crate (name "qparas") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "1kfxr8m6y3vd986b3wd6m9v799fm2dpxiifylla07cnmsn0hvxr0") (rust-version "1.56.0")))

