(define-module (crates-io qp -p) #:use-module (crates-io))

(define-public crate-qp-postgres-0.1 (crate (name "qp-postgres") (vers "0.1.0") (deps (list (crate-dep (name "qp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "08b880hmqv9kvnxn4l46iq6z3jxbs6k6qar0fcjx9ynalga5gj50") (rust-version "1.56")))

(define-public crate-qp-postgres-0.1 (crate (name "qp-postgres") (vers "0.1.1") (deps (list (crate-dep (name "qp") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "10f8c4ndp58fslxl150kfi3l45mfshxs9ndhw8s78xpajbnm8amb") (rust-version "1.56")))

