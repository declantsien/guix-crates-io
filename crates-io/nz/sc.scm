(define-module (crates-io nz sc) #:use-module (crates-io))

(define-public crate-nzsc2p-0.1 (crate (name "nzsc2p") (vers "0.1.0") (deps (list (crate-dep (name "nzsc_core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0l0bvaacljhzzakqqhnj19a9ajd7m1fdiyr0zy8842gdarlr2lsa")))

(define-public crate-nzsc2p-0.2 (crate (name "nzsc2p") (vers "0.2.0") (deps (list (crate-dep (name "nzsc_core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wwdlyc6bjkp0msqywmfmla6hwka4kwnrjq4ld8jbnpn75gz64kk")))

(define-public crate-nzsc2p-0.3 (crate (name "nzsc2p") (vers "0.3.0") (deps (list (crate-dep (name "nzsc_core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "13dihx8n9ipdb9ajfb79haivfym2h3njq2aglzb3ivkr9yzl0zgv")))

(define-public crate-nzsc2p_json_interface-0.1 (crate (name "nzsc2p_json_interface") (vers "0.1.0") (deps (list (crate-dep (name "nzsc2p") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "00d9bqc59vh41klslbs9gr17mj4ra7cscmbmnxya8jlzlna8kw6y")))

(define-public crate-nzsc2p_json_interface-0.2 (crate (name "nzsc2p_json_interface") (vers "0.2.0") (deps (list (crate-dep (name "nzsc2p") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0im3abn8sv17lw3n4w7c8972xw4rjdrzxgdlnpzqimdybi411f37")))

(define-public crate-nzsc2p_json_interface-0.3 (crate (name "nzsc2p_json_interface") (vers "0.3.0") (deps (list (crate-dep (name "nzsc2p") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1acxm0pgh9lk78y4yg1rrzqd51ixcvw9nrdkf4p776vqcl5w5ibx")))

(define-public crate-nzsc_core-0.1 (crate (name "nzsc_core") (vers "0.1.0") (hash "1ksrg3d917plqwgfwkkw637rqz8vpazay8yyhlqg3f4r5pjfflbg")))

(define-public crate-nzsc_core-0.2 (crate (name "nzsc_core") (vers "0.2.0") (hash "1ml2rf44dmbzildw7h1j0swg5if2srq4524mdcp79a2s27rl1zsi")))

(define-public crate-nzsc_single_player-0.1 (crate (name "nzsc_single_player") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0hc4snbn3nch9xa8kli2mbgbll8lypq4hvvv824299jwfm7cqk1p")))

(define-public crate-nzsc_single_player-0.2 (crate (name "nzsc_single_player") (vers "0.2.0") (hash "126gy0wcmw9cjghyr5dk1c56737ws3khd3y2229gng4g45vvpj3j")))

(define-public crate-nzsc_single_player-0.3 (crate (name "nzsc_single_player") (vers "0.3.0") (hash "1d65ijbggfmdck67pd2hbiikkd6jq41s1dg4inmf2afsi0qwqxji")))

(define-public crate-nzsc_single_player-0.3 (crate (name "nzsc_single_player") (vers "0.3.1") (hash "10fm8y8xqpgirafiw95z30gz04icr6vaxacs4w8chahcqhqxw6vj")))

(define-public crate-nzsc_single_player-0.3 (crate (name "nzsc_single_player") (vers "0.3.2") (hash "0qwk7zpig54jm9hqasklkv28ymqjnqaq4wddayv2f5ra7ml5zqns")))

(define-public crate-nzsc_single_player-0.4 (crate (name "nzsc_single_player") (vers "0.4.0") (hash "03qa692zaihcsghmn1s8hz2na76iaywxdjshhyr8kq3ahkc81nzv")))

(define-public crate-nzsc_single_player-0.4 (crate (name "nzsc_single_player") (vers "0.4.1") (hash "1h5rcb28cr2iyv5989hw2bvc9vsm6cbhngyhh61sbkqicxv1hd4g")))

(define-public crate-nzsc_single_player-0.4 (crate (name "nzsc_single_player") (vers "0.4.2") (hash "0s8zy958f3gr033rkpp3nalpvnlxs2pl3d09hwg1898p5fax7hh2")))

(define-public crate-nzsc_single_player-0.4 (crate (name "nzsc_single_player") (vers "0.4.3") (hash "11mndhsnhkzbwddk1lbcbww1wqf68jp1hmxx88nigvzaqkpsck9h")))

(define-public crate-nzsc_single_player-0.4 (crate (name "nzsc_single_player") (vers "0.4.4") (hash "0qf3pqc19g8vqrp1mrb6rchypayzi0kby68cfm5gp8d1pzsk0kqn")))

(define-public crate-nzsc_single_player-0.5 (crate (name "nzsc_single_player") (vers "0.5.0") (hash "0v7242lds9x8s1ypdjxiliy0w0234xsfdm8g2x1wix1ymb8sa4i0")))

(define-public crate-nzsc_single_player-0.5 (crate (name "nzsc_single_player") (vers "0.5.1") (hash "137yqswfddvyg1ymgnj8d3005fr3zpqbqwlzsi64fgq2r5hjqdpa")))

(define-public crate-nzsc_single_player_json_interface-0.1 (crate (name "nzsc_single_player_json_interface") (vers "0.1.0") (deps (list (crate-dep (name "nzsc_single_player") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "04cabjnkb0lil3d4yfhfc12rc8k19bk5hnhw8pcjmn8yfkvw8kza")))

(define-public crate-nzsc_single_player_text_interface-0.1 (crate (name "nzsc_single_player_text_interface") (vers "0.1.0") (deps (list (crate-dep (name "nzsc_single_player") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1brs5ffk2q2s8yfx1wr9k8av13k0n7x7rkkp12p6651qacr6gca8")))

(define-public crate-nzsc_single_player_text_interface-0.2 (crate (name "nzsc_single_player_text_interface") (vers "0.2.0") (deps (list (crate-dep (name "nzsc_single_player") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0glm7r4hj7ahwp6k77bkbxx5haaqb1saj6pl36b48jayj8aw2lrn")))

(define-public crate-nzsc_single_player_text_interface-0.2 (crate (name "nzsc_single_player_text_interface") (vers "0.2.1") (deps (list (crate-dep (name "nzsc_single_player") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1hbc7qs74d66w44yn3pmzsf4mxrsyynl83rzpciwn0v5v1drvpy0")))

(define-public crate-nzsc_single_player_text_interface-0.2 (crate (name "nzsc_single_player_text_interface") (vers "0.2.2") (deps (list (crate-dep (name "nzsc_single_player") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0xgsmiwamg6amryhzkz7h4l9ncvi0asl45mxwwif8zq83fkvn25x")))

(define-public crate-nzscq-0.1 (crate (name "nzscq") (vers "0.1.0") (hash "0yhnaf7jsnp8hid8dvsj9abzbfbw26ydirzaf9lrpr0c51961kwq")))

(define-public crate-nzscq-0.2 (crate (name "nzscq") (vers "0.2.0") (hash "1qmk9jfd70x6mbddhh18hmb74pbpanpchmix9jv51zf8ns7fxzhz")))

(define-public crate-nzscq-0.3 (crate (name "nzscq") (vers "0.3.0") (hash "08cd1qj5jg6bpd5nlb2sd0xy8jk7wl5l1man85yl134swsbdg074")))

(define-public crate-nzscq-0.4 (crate (name "nzscq") (vers "0.4.0") (hash "05sqvi620f0bqcwiasxlkg6lkjn3hi27dc55lh332ch8rnhqmgbn")))

(define-public crate-nzscq-0.5 (crate (name "nzscq") (vers "0.5.0") (hash "0aqs012gyh03492yxr9i32qsg2xrjqdfsb8lm7m7y90jjs2dsz7h")))

(define-public crate-nzscq-0.6 (crate (name "nzscq") (vers "0.6.0") (hash "09q37ip5vjwxxpyxddi8xnpnz4vn2gk7sfh95ap5s9yrw2pi1qq4")))

(define-public crate-nzscq-0.7 (crate (name "nzscq") (vers "0.7.0") (hash "0d3qb9as60wspqcpjqa5a776x22jfvs4bzq0b7xpwzg9v4sqa0dd")))

(define-public crate-nzscq-0.8 (crate (name "nzscq") (vers "0.8.0") (hash "1zbncp925s819kal46zskjq9v9xqwg7245higijsbv8yxixwwllj")))

(define-public crate-nzscq-0.9 (crate (name "nzscq") (vers "0.9.0") (hash "1rwgfi2pm671zqwqypngj2k0a28njhyk9q7yf289q795xx0c1mc1")))

(define-public crate-nzscq-0.10 (crate (name "nzscq") (vers "0.10.0") (hash "0f5kycnk8i1fap8l7msxab88b06ab485mz6d6xz5d73y5jayfsqy")))

