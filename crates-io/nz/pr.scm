(define-module (crates-io nz pr) #:use-module (crates-io))

(define-public crate-nzprimes-0.1 (crate (name "nzprimes") (vers "0.1.0") (deps (list (crate-dep (name "libnzprimes") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1rdbwgb88yqz4gf97aw4wm9gns5bs8wc9h2rnj9bqlyjs4fpn21i")))

