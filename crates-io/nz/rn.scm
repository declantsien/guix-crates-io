(define-module (crates-io nz rn) #:use-module (crates-io))

(define-public crate-nzrng-1 (crate (name "nzrng") (vers "1.0.0") (deps (list (crate-dep (name "libnzrng") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1nln3lihp91i9nzjwzmp86jsqzmk5g86dwm3z7205h4b0frx2dpw")))

