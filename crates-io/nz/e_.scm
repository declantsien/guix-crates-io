(define-module (crates-io nz e_) #:use-module (crates-io))

(define-public crate-nze_game_sdl-0.1 (crate (name "nze_game_sdl") (vers "0.1.0") (deps (list (crate-dep (name "nze_geometry") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nze_tiled") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35") (features (quote ("ttf" "image" "mixer"))) (default-features #t) (kind 0)))) (hash "0mqn89k3a380fm8xzl9n5qk2pkfw01x5wjhplh1z347x6iy7rysx")))

(define-public crate-nze_game_sdl-0.1 (crate (name "nze_game_sdl") (vers "0.1.1") (deps (list (crate-dep (name "nze_geometry") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nze_tiled") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35") (features (quote ("ttf" "image" "mixer"))) (default-features #t) (kind 0)))) (hash "048nz0wcv4iyzsqq8r5whfh9jg0cl9apy534zjzvfsp5n670126r")))

(define-public crate-nze_game_sdl-0.1 (crate (name "nze_game_sdl") (vers "0.1.2") (deps (list (crate-dep (name "nze_geometry") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nze_tiled") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35") (features (quote ("ttf" "image" "mixer"))) (default-features #t) (kind 0)))) (hash "1d3wa7bk5xrpzs9nkq42csjqc4arzrs3s52w8yh4i68kgxfyikpd")))

(define-public crate-nze_geometry-0.1 (crate (name "nze_geometry") (vers "0.1.0") (hash "0qgqnbam9irym4hsr93k0pvfr282dk0ljcazklarjraza2ayzgh5")))

(define-public crate-nze_tiled-0.1 (crate (name "nze_tiled") (vers "0.1.0") (deps (list (crate-dep (name "nze_geometry") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0agr7jprbcydqc9ydwxrs5d0cmrhnacj1qwsf4ghrmmh2fx4d2k7")))

