(define-module (crates-io km pm) #:use-module (crates-io))

(define-public crate-kmpm-0.1 (crate (name "kmpm") (vers "0.1.0") (hash "0jr4mjb6aqsz64q60x6b2sdn7yp5njdws98nm0jkviiaz01ycinf")))

(define-public crate-kmpm-0.1 (crate (name "kmpm") (vers "0.1.1") (hash "1yr2am7785vahnzrgirgipqvw3qghab4x28n2lw9k861847fv74z")))

(define-public crate-kmpm-0.1 (crate (name "kmpm") (vers "0.1.2") (hash "14m6rc23lmcqs00ag4pz86dlx681vvj137n30d737mzskxvar0ig")))

(define-public crate-kmpm-0.2 (crate (name "kmpm") (vers "0.2.0") (hash "1x2w7m7c7jrbp8wlsxyyxxwzkgh16s4vg895rwhmz7f6906przna")))

(define-public crate-kmpm-0.2 (crate (name "kmpm") (vers "0.2.1") (hash "0n8m76qzk9pilk46g9vg663ilzsrnamnjlhxjhajw5i0pcrv6k18")))

(define-public crate-kmpm-0.2 (crate (name "kmpm") (vers "0.2.2") (hash "02phrkbj46ql0s1hl5vnikaj0ibdcij9hy77ab978qgbd1k11vgg")))

