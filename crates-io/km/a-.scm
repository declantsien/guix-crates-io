(define-module (crates-io km a-) #:use-module (crates-io))

(define-public crate-kma-rustlang-vadym-polishchuk-english-parser-0.2 (crate (name "kma-rustlang-vadym-polishchuk-english-parser") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1gb6xg56lglr7blhkq4xiwn1pn9bdmymxcg85bh7bcbg47dhmxpk")))

(define-public crate-kma-rustlang-vadym-polishchuk-parser-0.1 (crate (name "kma-rustlang-vadym-polishchuk-parser") (vers "0.1.0") (hash "0nwp4xi4iia7ix6h2i8vv8lxpr7dm4hdkwn6lfnw52qjsjfz91bg")))

