(define-module (crates-io km lu) #:use-module (crates-io))

(define-public crate-kmlui-0.1 (crate (name "kmlui") (vers "0.1.0") (deps (list (crate-dep (name "egui") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1jzpsffnsw44g6s2in4z54iwa1dp1h1ac9s7v3m1vx62fz6vp8p0")))

