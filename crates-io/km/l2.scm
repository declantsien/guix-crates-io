(define-module (crates-io km l2) #:use-module (crates-io))

(define-public crate-kml2pgsql-0.1 (crate (name "kml2pgsql") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1qdf0dr90mr5in57nw76p0fiqks3fn2awkamnkfl7skba91ychnn")))

(define-public crate-kml2pgsql-0.1 (crate (name "kml2pgsql") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jhfr5h6p0kmr9b5m2dy862jwglmyn88s6k0bkm1x9hy5sxczcgm")))

(define-public crate-kml2pgsql-0.2 (crate (name "kml2pgsql") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "wkb") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1jv0zrssrkz1m8zhq24ra7yb5rxshiajg20kdp5z1ww2s4n6wyyg")))

(define-public crate-kml2pgsql-0.2 (crate (name "kml2pgsql") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "wkb") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "01pck52xp5aazmr0hhz8pfqbjcyvharad3qv8j7bywc9mlb6lxb0")))

