(define-module (crates-io km ai) #:use-module (crates-io))

(define-public crate-kmail-0.1 (crate (name "kmail") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "email_address") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h7b8cq9i7yf9pghw3i2947fsfdc16023lpq6yjsqpbs9xz1nzbb")))

(define-public crate-kmail-0.1 (crate (name "kmail") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "email_address") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1iw0xhck2smw5ajjwc36p5yvgqkr26d3363bi6jcm20k0j0qxlaz")))

(define-public crate-kmail-0.1 (crate (name "kmail") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "email_address") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "047nq5akqz5hwd420cxawmc4n13gs7qd76jqpi75k56ixdnvajzq")))

