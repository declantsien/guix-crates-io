(define-module (crates-io km od) #:use-module (crates-io))

(define-public crate-kmod-0.1 (crate (name "kmod") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0l6vc20zcn4xan9iam77a0865cmzbhan866hdlv1ivsjpr26k1wr")))

(define-public crate-kmod-0.2 (crate (name "kmod") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reduce") (req "^0.1") (default-features #t) (kind 0)))) (hash "0idbshy8g62jgvsj8m9jq5l8g3y875ljj2s2fvxxlrhhlnib8lpx")))

(define-public crate-kmod-0.2 (crate (name "kmod") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reduce") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w98720rv49xr6pw6kzyns2mpqlbsvx3ia4c6jsd5n7r7ag77x0c")))

(define-public crate-kmod-0.3 (crate (name "kmod") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reduce") (req "^0.1") (default-features #t) (kind 0)))) (hash "0iblpvrkipg6ga81hr5l7gz3mga61nw1773sw0rwfy5amdfqgr53")))

(define-public crate-kmod-0.3 (crate (name "kmod") (vers "0.3.1") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reduce") (req "^0.1") (default-features #t) (kind 0)))) (hash "06pdx36ic3vdf5lpmgyac760dfn3gy2fgj719sj5vniqm4kdllcf")))

(define-public crate-kmod-0.3 (crate (name "kmod") (vers "0.3.2") (deps (list (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reduce") (req "^0.1") (default-features #t) (kind 0)))) (hash "10jwcrab0axgk1a5i3jr0aga1s88xjzlym538dr78h184an6w8cv")))

(define-public crate-kmod-0.4 (crate (name "kmod") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0v9sh374w49hpihlkyqmb49jmmdbxs21m3i5dqvksxiadpd180vl")))

(define-public crate-kmod-0.5 (crate (name "kmod") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "kmod-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1h9nlz94cxiw42wchcs9d2palgjyp9nh8z2pgd1pk441b2mc33mh")))

(define-public crate-kmod-sys-0.1 (crate (name "kmod-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.33") (default-features #t) (kind 1)))) (hash "1swn0m4qgq6kbrh34c9iyvz0bqa9f598gsd4y14ap1gsgawqlxpf")))

(define-public crate-kmod-sys-0.1 (crate (name "kmod-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.33") (default-features #t) (kind 1)))) (hash "1cjzj8y1qjqbn76d9vdgsla4x0x6p5v92zvq5x22y52jv8hjl64h")))

(define-public crate-kmod-sys-0.1 (crate (name "kmod-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.33") (default-features #t) (kind 1)))) (hash "0pybm5hgykw286rnay8648y2ajxqc7alnn7vvw8z3kv9dv8dxnzn")))

(define-public crate-kmod-sys-0.2 (crate (name "kmod-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)))) (hash "1hfzpv6qcz1cbzw8bg2658ipw1ahbxsk6mr1j8q55ppm61ssncvz") (links "kmod")))

(define-public crate-kmoddep-0.1 (crate (name "kmoddep") (vers "0.1.0") (hash "1mpfm42x6gbgmbsvhq39wq8yzw60s83jwzqxpff4m8xjg4yryy5w") (yanked #t)))

(define-public crate-kmoddep-0.1 (crate (name "kmoddep") (vers "0.1.1") (hash "1yi90r4mf9mzfvf9za2pxnbr4kmhz7cwyarpgpswn7b3jk7adj0b") (yanked #t)))

(define-public crate-kmoddep-0.1 (crate (name "kmoddep") (vers "0.1.2") (hash "0qdv1jp649dai3k02f6k3jgh6cg0nbw76dm2rrjn9ryq8vzg6zwf")))

(define-public crate-kmoddep-0.1 (crate (name "kmoddep") (vers "0.1.3") (hash "0avcrggw30s77pqnlczfqlmj6mza1i55cri1cqslvks24dffc949")))

(define-public crate-kmodels-0.0.1 (crate (name "kmodels") (vers "0.0.1") (hash "06l5j9576y92zqbgz5j07c29l5fp9kqi9lhml9d1w97c8n2cp15a")))

