(define-module (crates-io km mp) #:use-module (crates-io))

(define-public crate-kmmp-generator-0.1 (crate (name "kmmp-generator") (vers "0.1.0") (deps (list (crate-dep (name "kmmp-structure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "00ks9lg8zr704yy7l1gjlb32qvwr6khd0vq4kqfhs2qzcr7325zm")))

(define-public crate-kmmp-project-manager-0.1 (crate (name "kmmp-project-manager") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05x6hz6vkx05pvax6kh5h11shd117x7p18wwjs5zjjrkc105hi4g") (features (quote (("generate"))))))

(define-public crate-kmmp-project-manager-cli-0.1 (crate (name "kmmp-project-manager-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "kmmp-project-manager") (req "^0.1.0") (features (quote ("generate"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "07z14m1bp4hbzxqdd4wis3lsqs86j9c81cf6xzr0cgk9c1g2jxwk")))

(define-public crate-kmmp-structure-0.1 (crate (name "kmmp-structure") (vers "0.1.0") (hash "1vw5445ylpbn1ghw0w21qag6343n7j8pmqhpfv6728b3cidai5f2") (yanked #t)))

(define-public crate-kmmp-structure-0.1 (crate (name "kmmp-structure") (vers "0.1.1") (hash "0vhhhja7g1k3vylbx8abzk6g3is4p6qjgp6z5qpw466aaghcbllz") (yanked #t)))

(define-public crate-kmmp-structure-0.1 (crate (name "kmmp-structure") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1193wxl4b3i12jf09ancpfv4gi70n1zaj6ckfnkb1kx8ifjhdjl8")))

