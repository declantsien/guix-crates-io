(define-module (crates-io km l_) #:use-module (crates-io))

(define-public crate-kml_to_fgfp-0.1 (crate (name "kml_to_fgfp") (vers "0.1.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1j507f86f53i8w0h0pyxwq0smz7wfrdgg6bgc2qxgyrhc1crirsg")))

(define-public crate-kml_to_fgfp-0.2 (crate (name "kml_to_fgfp") (vers "0.2.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0p2rf15fgz7rzdr0z1558fdmz6dc72gx54bx3gmg0sdpqnbcb840")))

(define-public crate-kml_to_fgfp-0.3 (crate (name "kml_to_fgfp") (vers "0.3.1") (deps (list (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1l47dvpmp0jaly3sg2j3mgd238w35676v3p68pbdbikbvfn8b001")))

