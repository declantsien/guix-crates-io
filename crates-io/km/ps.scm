(define-module (crates-io km ps) #:use-module (crates-io))

(define-public crate-kmpsearch-0.1 (crate (name "kmpsearch") (vers "0.1.0") (hash "0ijlh70h11lrycn6w20w6cz61iqs1zxnr6d4fw0lpx87c5bxz3qc")))

(define-public crate-kmpsearch-1 (crate (name "kmpsearch") (vers "1.0.0") (hash "1m5kq44jhnis1q88bi0i67qxs5dn1289awx84in52pgbx64hq78l")))

