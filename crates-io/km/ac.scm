(define-module (crates-io km ac) #:use-module (crates-io))

(define-public crate-kmac-0.0.0 (crate (name "kmac") (vers "0.0.0") (hash "1k5p5g20xrbyssjsc2vx6lsk8r9r8ihjpbfilfsq42m24y8hx27z")))

(define-public crate-kmacro-0.1 (crate (name "kmacro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.41") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "00w8bmr38qa7fvk9sk5alwwfc9w91dghcvf35ch4i8zpjr9yxbjv")))

(define-public crate-kmacros-1 (crate (name "kmacros") (vers "1.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "199w7cn810fvqg23mkwf2dpfyr8jj1rwjmcnslnpnfck9m0bk4cp") (features (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-3 (crate (name "kmacros") (vers "3.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1rwlyvmz4q455zwq9a194jpwm3xlspffhih69n74y83903a15zqy") (features (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-3 (crate (name "kmacros") (vers "3.0.1") (deps (list (crate-dep (name "kmacros_shim") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1fb5incj2znb2ixp81kg161fbwqlx5zg01ahnb7ys7fk58cbaas6") (features (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-3 (crate (name "kmacros") (vers "3.0.2") (deps (list (crate-dep (name "kmacros_shim") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "10qdwd56062kiimiz8qdzky0wf2sy5a218gzv23aaghb9al4r86a") (features (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-4 (crate (name "kmacros") (vers "4.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1n2gzdpnm50njqsal33m7q5pvmq0k85j8mp9s5xk3rzrj7pnqnb6") (features (quote (("proc" "kproc_macros") ("default" "proc"))))))

(define-public crate-kmacros-5 (crate (name "kmacros") (vers "5.0.0") (deps (list (crate-dep (name "kmacros_shim") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^5.0") (optional #t) (default-features #t) (kind 0)))) (hash "1wlfqkjd2jlz3rs48jv8f4jhm615myzfw2cf8z2n9wv3ild9dgih") (features (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std") ("default" "proc"))))))

(define-public crate-kmacros-5 (crate (name "kmacros") (vers "5.1.0") (deps (list (crate-dep (name "kmacros_shim") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^5.1") (optional #t) (default-features #t) (kind 0)))) (hash "05zxaxrmlwzc1i0apbj7i40552zac2pzd7r1j9392zk5d55v27id") (features (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std") ("default" "proc"))))))

(define-public crate-kmacros-6 (crate (name "kmacros") (vers "6.0.0") (deps (list (crate-dep (name "enum-kinds") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kmacros_shim") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^6.0") (optional #t) (default-features #t) (kind 0)))) (hash "095pg2rd278qmjlj3l63lvzz0az182gxip2wfcmhcpwg52zjfd4x") (features (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std" "enum-kinds/no-stdlib") ("kinds" "enum-kinds") ("default" "proc" "kinds"))))))

(define-public crate-kmacros-6 (crate (name "kmacros") (vers "6.1.0") (deps (list (crate-dep (name "enum-kinds") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kmacros_shim") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "kproc_macros") (req "^6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jwwdzsxajl2y6kn72bmhvsjzhk8r4iai628bb4nqm9hpyznf6y1") (features (quote (("proc" "kproc_macros") ("no_std" "kmacros_shim/no_std" "enum-kinds/no-stdlib") ("kinds" "enum-kinds") ("default" "proc" "kinds"))))))

(define-public crate-kmacros_shim-1 (crate (name "kmacros_shim") (vers "1.0.0") (hash "063bahgvj6qjdw82mkn4fwgp6ms3cvncwmifvpha8r8wcn0p9fn3")))

(define-public crate-kmacros_shim-2 (crate (name "kmacros_shim") (vers "2.0.0") (hash "1lv389zdi7n9i3pw9jq5qnd66m3jkbmw8vbgs2vlg42g1arsp3m0") (yanked #t)))

(define-public crate-kmacros_shim-3 (crate (name "kmacros_shim") (vers "3.0.0") (hash "01r9k7bal2059n3q2ym5v3iwdhkd24a59xmfidfgyshypl682jda")))

(define-public crate-kmacros_shim-3 (crate (name "kmacros_shim") (vers "3.0.1") (hash "1q9kw4qzbs2j6xs3w7q5wyk21czdw2w3nb14j8viymp1dxiqd87c")))

(define-public crate-kmacros_shim-4 (crate (name "kmacros_shim") (vers "4.0.0") (hash "1zy6zmvm24kcgq2rpci9h4q03hplqpybbi8n3ha7m3z2jgs72v7v")))

(define-public crate-kmacros_shim-5 (crate (name "kmacros_shim") (vers "5.0.0") (hash "1z7n2n8bv4szzklj2mn9grp17va5qr697lxry33wwllghsjm8js9") (features (quote (("no_std") ("default"))))))

(define-public crate-kmacros_shim-5 (crate (name "kmacros_shim") (vers "5.1.0") (hash "0zx36clshyczhma6v170pab31f2mfny9x3ilyyx4yl96pnlyn1l9") (features (quote (("no_std") ("default"))))))

(define-public crate-kmacros_shim-6 (crate (name "kmacros_shim") (vers "6.0.0") (hash "1388gijlvipgwjm59dkxz4b3irsiwfl8sigi4g55pklsnql3j3g6") (features (quote (("no_std") ("default"))))))

