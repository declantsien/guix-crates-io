(define-module (crates-io qv ic) #:use-module (crates-io))

(define-public crate-qvic-0.1 (crate (name "qvic") (vers "0.1.0") (hash "007m9lp39mygk1asf1mlxk1nwkgmbsgjwh1dchn55a2ifzkgs4ra")))

(define-public crate-qvic-quinn-0.1 (crate (name "qvic-quinn") (vers "0.1.0") (hash "15nfprkv6y0j3h7101aj8y0bddg88dm3isjqs2w1dpvizi5imm5c")))

(define-public crate-qvic-vtcp-0.1 (crate (name "qvic-vtcp") (vers "0.1.0") (hash "13j5z147znc1rlkpny7a2pckvva0fw1hjx10awx7n9bjg4v2zh0r")))

