(define-module (crates-io sv gm) #:use-module (crates-io))

(define-public crate-svgmacro-0.1 (crate (name "svgmacro") (vers "0.1.0") (hash "08jgnr0nycb279p4sv5nqnay01mprkajbb12kb5a6yqz8ljn1gbn")))

(define-public crate-svgmacro-0.1 (crate (name "svgmacro") (vers "0.1.1") (hash "177j64vy4zjdla5hz03v04rqnij28k8j7vlkws9amn1d2a5w23cq")))

(define-public crate-svgmacro-0.1 (crate (name "svgmacro") (vers "0.1.2") (hash "0y8zs5w8nmj64nrjxn20ykq4c02bj12csa84qqb7a64hrrbsy0pf")))

(define-public crate-svgmacro-0.1 (crate (name "svgmacro") (vers "0.1.3") (hash "06f14lpxnz624vasdwzgknhiq2mdnhm9zv91iwh7m2s3vivzd04d")))

(define-public crate-svgmacro-0.1 (crate (name "svgmacro") (vers "0.1.4") (hash "0ljpamhfjd9bjy55nqmf68jl154ihjnaz2id009jk9hfn97fqbyq")))

(define-public crate-svgmacro-0.1 (crate (name "svgmacro") (vers "0.1.5") (hash "0b5gwvyawkc5v613mqcgif2sksryhh0jg3610f5kld43f3dhmkc3")))

(define-public crate-svgmacro-0.2 (crate (name "svgmacro") (vers "0.2.0") (hash "0l56srbcpbicnp9g1xn4rymyvqzla2mcrwmbkavd55qlq3q4yz3a")))

(define-public crate-svgmacro-0.2 (crate (name "svgmacro") (vers "0.2.1") (hash "0wi7ahcynmxjfn6s343pd86i7fj3cybkknjqgchqkv22mrb3gspz")))

(define-public crate-svgmacro-0.2 (crate (name "svgmacro") (vers "0.2.2") (hash "1y7mhzqb9xwz98spikkjw8w05zavbhb5gp9hp1nd7i7vmjrbrs8x")))

