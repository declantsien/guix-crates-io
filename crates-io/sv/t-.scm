(define-module (crates-io sv t-) #:use-module (crates-io))

(define-public crate-svt-av1-rs-0.1 (crate (name "svt-av1-rs") (vers "0.1.0") (hash "1vh4ixyap0ak98hv10x2d1ch4gcfd65mc7306qiln52654v0z93q")))

(define-public crate-svt-av1-sys-0.1 (crate (name "svt-av1-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "02irkaqhsm3j8zwk8dxpshcayqbvi1cp9v8h00zl1hzcl4pbxlma") (features (quote (("build_sources"))))))

(define-public crate-svt-av1-sys-0.1 (crate (name "svt-av1-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "0lijhaikbl0fksvvs8346jhn2gcd6mnrcw0dd6yrcqn4wx5ss7g7") (features (quote (("build_sources"))))))

