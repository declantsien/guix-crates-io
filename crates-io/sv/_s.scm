(define-module (crates-io sv _s) #:use-module (crates-io))

(define-public crate-sv_str-1 (crate (name "sv_str") (vers "1.0.0") (deps (list (crate-dep (name "lazy-regex") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0i8fp30h04zy001ril15p8py06f1zihcqhfp5n4zkqw6qziqjqks")))

(define-public crate-sv_str-1 (crate (name "sv_str") (vers "1.0.1") (deps (list (crate-dep (name "lazy-regex") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0gqci50qi12nywg6kvyhwlpy8r5mwasjkwjzz9ncrcggqbw3s74s")))

(define-public crate-sv_str-1 (crate (name "sv_str") (vers "1.0.2") (deps (list (crate-dep (name "lazy-regex") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1vw1gashxk9gsqvi96jy634d0y8vgvwviaar7jqyvqw9q150w0rj")))

(define-public crate-sv_str-1 (crate (name "sv_str") (vers "1.0.3") (deps (list (crate-dep (name "lazy-regex") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "05z5q2lvzgw6pchkikzzp7nwyxmg0hna6f56cflaznabrhi118bd")))

(define-public crate-sv_str-1 (crate (name "sv_str") (vers "1.0.4") (deps (list (crate-dep (name "lazy-regex") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0sak39f6b21lril0rfi5pj0vqcixc5zs2ds9r1h71fc5ygl150wi")))

