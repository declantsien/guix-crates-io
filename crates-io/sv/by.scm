(define-module (crates-io sv by) #:use-module (crates-io))

(define-public crate-svbyte-0.1 (crate (name "svbyte") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0z6cigrpfyfsz1k4by66vbsds6m8k8p00vbyw9arihknvdrbqk6p")))

(define-public crate-svbyte-0.1 (crate (name "svbyte") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0a36w8szqd9m67nd9dnb7zdh538r9bzlf1na6x9shr0kxpxciriw")))

