(define-module (crates-io sv ar) #:use-module (crates-io))

(define-public crate-svarog-0.5 (crate (name "svarog") (vers "0.5.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "0pvcqhgfx3w6hrqlnifs10lapnfsvpq6xj0a4wfs0qbpk7g05xak")))

(define-public crate-svarog-0.6 (crate (name "svarog") (vers "0.6.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "08gv4cxzmk40x50741sk0wsvbqn2aiz17vnrcrinphi54i8jjway")))

(define-public crate-svarog-0.6 (crate (name "svarog") (vers "0.6.1") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "1fjr8a1rdykjm1mhqxa4r982ngkwwivlnwx3wzc46rzrjkakqp1a")))

