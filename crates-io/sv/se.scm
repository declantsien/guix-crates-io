(define-module (crates-io sv se) #:use-module (crates-io))

(define-public crate-svsep-1 (crate (name "svsep") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x2vxbp4vmsf2ggpn58lqxl2wagjr588lhhjyx6vfi3mnkdbs9lb")))

