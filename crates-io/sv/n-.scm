(define-module (crates-io sv n-) #:use-module (crates-io))

(define-public crate-svn-cmd-0.1 (crate (name "svn-cmd") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0f2c8aw5bqb017d0ah8469fwf9bx9w4z3jng262gl7lm6rmglpdg")))

