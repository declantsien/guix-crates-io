(define-module (crates-io sv ma) #:use-module (crates-io))

(define-public crate-svmap-0.1 (crate (name "svmap") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "04872d202kvb2icf4h636l818p2lb16l87f4bgwy071isfgap7ik")))

(define-public crate-svmap-0.1 (crate (name "svmap") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1w8x36n8yr8cza23nnndxxshkqkp9hacsfp8wgpbv9smifg7wbwp")))

(define-public crate-svmap-0.2 (crate (name "svmap") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0mkmsvxlf8y50hjw0s607vq8w5rds8myfrbslpi4s23xc1b6psq3")))

(define-public crate-svmap-0.2 (crate (name "svmap") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1kg35hn5dkcpqvrdwx4gs9z8ip7mfwrjj0a0z10k3b1kxzmf19x2")))

(define-public crate-svmap-0.3 (crate (name "svmap") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0yfaw58qqp48rdf3n9zskczf29fbbxmki1qaji0w9zc8318y590i")))

