(define-module (crates-io sv ep) #:use-module (crates-io))

(define-public crate-sveppa-bencode-0.1 (crate (name "sveppa-bencode") (vers "0.1.0") (hash "1lz4nqhafkaizfkxndgb5kllvvqs6z7g0vzifqcq3lvy1jjp983k")))

(define-public crate-sveppa-bencode-0.1 (crate (name "sveppa-bencode") (vers "0.1.1") (hash "04dp6h3xk3nsr6vpfcc52vly78alnfpkahh1vsj23n6s65zk19qm")))

(define-public crate-sveppa-bencode-0.1 (crate (name "sveppa-bencode") (vers "0.1.2") (hash "13j04v18ax06v693wmnimncrw51lnpn36c4ws7930a0kdc8kr4sb")))

(define-public crate-sveppa-bencode-0.1 (crate (name "sveppa-bencode") (vers "0.1.3") (hash "1hwnb84183gilglbwghw3ikkz0v3zb24v3j7611vhllvia9kcirn")))

(define-public crate-sveppa-bencode-0.1 (crate (name "sveppa-bencode") (vers "0.1.4") (hash "14z04kj2k57gmnarz5wfi7dc5zp4b4iqp853w7di50imq1jf5p14")))

(define-public crate-sveppa-bencode-0.1 (crate (name "sveppa-bencode") (vers "0.1.6") (hash "1rl5qa7vs022n6lb7cak02z8vjs1wv7c105cznxlk5569j8w4whm")))

(define-public crate-sveppa-bencode-0.2 (crate (name "sveppa-bencode") (vers "0.2.0") (hash "01nmpq8ifqlyaw63ld2r541m7f4snvz0cchf78cazcv8f4rdbp4c")))

(define-public crate-sveppa-bencode-0.2 (crate (name "sveppa-bencode") (vers "0.2.1") (hash "0xfav6j1rzvmmgwr3gjm8p1wcr0pypjnx04zn0b192a1d4q97g2r")))

(define-public crate-sveppa-bencode-0.2 (crate (name "sveppa-bencode") (vers "0.2.2") (hash "1spbbq9p8jfqksh15jkrd2m9nqhvd15c1km87s72n5yajlavpm3d")))

(define-public crate-sveppa-torrent-0.1 (crate (name "sveppa-torrent") (vers "0.1.0") (deps (list (crate-dep (name "sveppa-bencode") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "07mhjhbfjvlgahhpv7x2c49knk63wpz7npnmhsyxvzlhdcxhnanr")))

