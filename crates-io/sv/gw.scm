(define-module (crates-io sv gw) #:use-module (crates-io))

(define-public crate-svgwriter-0.1 (crate (name "svgwriter") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0998gcp1yi2sqjfs03hgh97rygywdnyncdd0rxbqqn66vfcjsxv6")))

