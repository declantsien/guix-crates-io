(define-module (crates-io sv ga) #:use-module (crates-io))

(define-public crate-svgator-to-solidjs-0.1 (crate (name "svgator-to-solidjs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.23") (default-features #t) (kind 0)))) (hash "1aapcqsgpljl9amfpa0kr95sxva6nn18snvblw2xsmdqq87llflr")))

(define-public crate-svgator-to-solidjs-0.2 (crate (name "svgator-to-solidjs") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.23") (default-features #t) (kind 0)))) (hash "12h9ds0ylv84sm7z9gn1s4r0hcrhwjr1jqdafprl7ivz4rk2hb56")))

(define-public crate-svgator-to-solidjs-0.2 (crate (name "svgator-to-solidjs") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.2.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.23") (default-features #t) (kind 0)))) (hash "0w2bdrykgsc0qk9ac75rr7hjkxwwzrxcc6vri6bdadpbnl118vc7")))

