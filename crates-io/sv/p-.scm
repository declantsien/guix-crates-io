(define-module (crates-io sv p-) #:use-module (crates-io))

(define-public crate-svp-client-0.1 (crate (name "svp-client") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)))) (hash "0m3x2skgbgjgssq6sr9qi7dw3f6s7gbshbpn41hacw0hmm205kih")))

