(define-module (crates-io sv -f) #:use-module (crates-io))

(define-public crate-sv-filelist-parser-0.1 (crate (name "sv-filelist-parser") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1y08aad0c8k4cg5y4svygziq8l39djqnwms96qk0mqvijd49w3rd")))

