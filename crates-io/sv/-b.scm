(define-module (crates-io sv -b) #:use-module (crates-io))

(define-public crate-sv-bindings-0.1 (crate (name "sv-bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "1gxb953z3daclbaysgzh3vp58yw7ldh1273cvl31rn2jfxcrr0j8") (features (quote (("vpi_user") ("vpi_compatibility") ("svdpi") ("sv_vpi_user") ("default" "svdpi" "vpi_user" "vpi_compatibility" "sv_vpi_user"))))))

(define-public crate-sv-bindings-0.1 (crate (name "sv-bindings") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "1sc8j9mjj1nkd01cykwy6x2p9d6w739sb8z568lg26cp2ysnb7fs") (features (quote (("default"))))))

(define-public crate-sv-bindings-0.1 (crate (name "sv-bindings") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "1awmsc6xiqm5w0rfr6d2fj13i52c061hra4v4dix7dxds685x7w5") (features (quote (("default"))))))

