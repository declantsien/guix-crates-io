(define-module (crates-io sv -a) #:use-module (crates-io))

(define-public crate-sv-api-0.0.1 (crate (name "sv-api") (vers "0.0.1") (deps (list (crate-dep (name "sv-bindings") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1h79n1232z3w7l6wy477jkhgmh821yncb2fkicbkcr3nll2d8i8w") (features (quote (("default"))))))

