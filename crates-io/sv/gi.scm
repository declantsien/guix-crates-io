(define-module (crates-io sv gi) #:use-module (crates-io))

(define-public crate-svgize-0.0.1 (crate (name "svgize") (vers "0.0.1") (deps (list (crate-dep (name "quick-xml") (req "^0.31") (kind 0)))) (hash "0jsm4mz3ypdsgmn9q41qj0gxhg8gww55hlvvwjpka1jhjkg0cyi7") (features (quote (("exp") ("default" "attr-core" "attr-styling" "attr-presentation" "attr-event") ("crossorigin") ("attr-styling") ("attr-presentation") ("attr-event") ("attr-core") ("attr-cond_proc")))) (yanked #t)))

(define-public crate-svgize-0.0.2 (crate (name "svgize") (vers "0.0.2") (deps (list (crate-dep (name "quick-xml") (req "^0.31") (kind 0)))) (hash "0pr23hn0i1wp4f8z9k7vzsp1m27y81awy4ix05n9ngh4aiwlgrg5") (features (quote (("exp") ("default" "attr-core" "attr-styling" "attr-presentation" "attr-event") ("crossorigin") ("attr-styling") ("attr-presentation") ("attr-event") ("attr-core") ("attr-cond_proc")))) (yanked #t)))

(define-public crate-svgize-0.0.3 (crate (name "svgize") (vers "0.0.3") (deps (list (crate-dep (name "quick-xml") (req "^0.31") (kind 0)))) (hash "1wx4im7y6z7mi19b3x897fvw8925wzibnxc9jh4dsb2gc63xck39") (features (quote (("exp") ("default" "attr-core" "attr-styling" "attr-presentation" "attr-event") ("crossorigin") ("attr-styling") ("attr-presentation") ("attr-event") ("attr-core") ("attr-cond_proc"))))))

