(define-module (crates-io sv gt) #:use-module (crates-io))

(define-public crate-svgtypes-0.1 (crate (name "svgtypes") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.4") (default-features #t) (kind 0)))) (hash "0scd0ddfi4ksb9r2qhslaxhzzgzqfy382gah95kz5xqr97j0kicp")))

(define-public crate-svgtypes-0.1 (crate (name "svgtypes") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "13qhcfawky3161xnhi8lp49ffcf0n9zl47riidzhhp7262sg4vpw")))

(define-public crate-svgtypes-0.2 (crate (name "svgtypes") (vers "0.2.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.6") (default-features #t) (kind 0)))) (hash "1j87k9vnz65pkqrmy8rskmidjys6s0kf36c37w10gnxflcba2zbd")))

(define-public crate-svgtypes-0.3 (crate (name "svgtypes") (vers "0.3.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 0)))) (hash "0fs5c6v29lxvk409sbhzynnbb7h38b1v07imbr5h4r3jvvk2ag64")))

(define-public crate-svgtypes-0.4 (crate (name "svgtypes") (vers "0.4.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 0)))) (hash "006aqa8z6n396lahbkd5i7jhbfqhiq1sb6qlnpg5kxnxii6dzbnm")))

(define-public crate-svgtypes-0.4 (crate (name "svgtypes") (vers "0.4.1") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 0)))) (hash "0z54y4nxxqwc2z9937ad7l3irl6pm4jkl053dmjavcd8yy9j20cs")))

(define-public crate-svgtypes-0.4 (crate (name "svgtypes") (vers "0.4.2") (deps (list (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)))) (hash "0ay5ch709nbhmgnnb4ji6z42qm951b26iw4pshrbxmn61b40qa06")))

(define-public crate-svgtypes-0.4 (crate (name "svgtypes") (vers "0.4.3") (deps (list (crate-dep (name "float-cmp") (req "^0.4.1") (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (optional #t) (default-features #t) (kind 0)))) (hash "04bhfmjl3a7wq3zivxg415ci5s8yr25pxmrsl3kn3zfc3w3ix938") (features (quote (("element-id" "phf") ("default" "color" "attribute-id" "element-id") ("color" "phf") ("attribute-id" "phf"))))))

(define-public crate-svgtypes-0.4 (crate (name "svgtypes") (vers "0.4.4") (deps (list (crate-dep (name "float-cmp") (req "^0.5") (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (optional #t) (default-features #t) (kind 0)))) (hash "0n12s36r7pk06mss9v3ccj49amjik47sja7jbmcf0nlj50n8hk24") (features (quote (("element-id" "phf") ("default" "color" "attribute-id" "element-id") ("color" "phf") ("attribute-id" "phf"))))))

(define-public crate-svgtypes-0.5 (crate (name "svgtypes") (vers "0.5.0") (deps (list (crate-dep (name "float-cmp") (req "^0.5") (kind 0)) (crate-dep (name "siphasher") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1zv0yb4nfyz78y8k7fmyjqgdh9vf7xc44c9pzry8640szym6ylww")))

(define-public crate-svgtypes-0.6 (crate (name "svgtypes") (vers "0.6.0") (deps (list (crate-dep (name "float-cmp") (req "^0.8") (features (quote ("std"))) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "12zs6rkrazvw2awkh5l1nbmdxj5cpbf6wp3677fajjkxmasd36c1")))

(define-public crate-svgtypes-0.7 (crate (name "svgtypes") (vers "0.7.0") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x47kr9w2wmjbsvawvdn5xyphg3by9hlzisdvxdqb21va11zm7mj")))

(define-public crate-svgtypes-0.8 (crate (name "svgtypes") (vers "0.8.0") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cwlxxkp56nmp2dj7bzn84mkik7215b59d1a53amcz25kaskxfys")))

(define-public crate-svgtypes-0.8 (crate (name "svgtypes") (vers "0.8.1") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hc5ijhyh1cww8vnbr7x4z33gs33g2d1yc11zzcg9ka4n5l2z06c")))

(define-public crate-svgtypes-0.8 (crate (name "svgtypes") (vers "0.8.2") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "0r2mjyrsyrczd05hycw0ww03nqv4hyqsd67qajxpcsmc5f55x5r2")))

(define-public crate-svgtypes-0.9 (crate (name "svgtypes") (vers "0.9.0") (deps (list (crate-dep (name "kurbo") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "0d4pn6c218y1lq4w42by80a3paqvq4mchsjzzv61hnvs830jkvn9")))

(define-public crate-svgtypes-0.10 (crate (name "svgtypes") (vers "0.10.0") (deps (list (crate-dep (name "kurbo") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "0r97kic0l9zdylkz5fwzx6y2yjf5yfs7jwlhkibsc7fgrpnsrzwq")))

(define-public crate-svgtypes-0.11 (crate (name "svgtypes") (vers "0.11.0") (deps (list (crate-dep (name "kurbo") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "19q2xxy8d3sl7406jy8kd4inklp2v62y667sq1l7y9zkww8hcjzd")))

(define-public crate-svgtypes-0.12 (crate (name "svgtypes") (vers "0.12.0") (deps (list (crate-dep (name "kurbo") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "0llyghxgvcg207qc1svr3x193rpd12inj4xjxlk9vxa25pzrj56p")))

(define-public crate-svgtypes-0.13 (crate (name "svgtypes") (vers "0.13.0") (deps (list (crate-dep (name "kurbo") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "0w4xknlff1np8l9if7y8ig6bx44bjr006m5xgj8ih0wnrn4f4i3f")))

(define-public crate-svgtypes-0.14 (crate (name "svgtypes") (vers "0.14.0") (deps (list (crate-dep (name "kurbo") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)))) (hash "0haj29qzd73lkbwavry4ayy9rxwkg8gdmzbw38byh6xm2a7n3msr")))

(define-public crate-svgtypes-0.15 (crate (name "svgtypes") (vers "0.15.0") (deps (list (crate-dep (name "kurbo") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zz8hcf5daflfp6yql5v47x118inxbldijcs2fl0viy9j6lajz6r")))

(define-public crate-svgtypes-0.15 (crate (name "svgtypes") (vers "0.15.1") (deps (list (crate-dep (name "kurbo") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g54d2cc6wlyf0254i98bjffwjhjknk2a13alz4r34xqz56hdqzs")))

