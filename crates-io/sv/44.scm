(define-module (crates-io sv #{44}#) #:use-module (crates-io))

(define-public crate-sv443_jokeapi-0.1 (crate (name "sv443_jokeapi") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c6r41jgblka31c7mklaqb702sjw6k8z6jcwmy4np1i5kfmklqdr")))

(define-public crate-sv443_jokeapi-0.1 (crate (name "sv443_jokeapi") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hdclq8bv1by0fn8j2zjvqx08f651v940378a09dqwpxxpk158xf")))

(define-public crate-sv443_jokeapi-0.2 (crate (name "sv443_jokeapi") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.11") (default-features #t) (kind 0)))) (hash "16kpbr1dzinrgdf0gg5vx7f49jxr2jkv4qfkh22x4q8jx5zlprg9")))

