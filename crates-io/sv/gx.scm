(define-module (crates-io sv gx) #:use-module (crates-io))

(define-public crate-svgx-0.1 (crate (name "svgx") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "10ga04k685j68j7zwbyc6v7l63a2sydx1bikq6jwllyshc0h8qjq")))

