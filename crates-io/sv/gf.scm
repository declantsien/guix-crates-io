(define-module (crates-io sv gf) #:use-module (crates-io))

(define-public crate-svgfilters-0.1 (crate (name "svgfilters") (vers "0.1.0") (deps (list (crate-dep (name "float-cmp") (req "^0.5") (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)))) (hash "115d67yjxhd9zcf9zp1020ccysjyc44r23s7wpfwijsir8a5l0q8")))

(define-public crate-svgfilters-0.2 (crate (name "svgfilters") (vers "0.2.0") (deps (list (crate-dep (name "float-cmp") (req "^0.5") (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)))) (hash "03h94m5vs5p7nij4xvsjdjjcqsic48q30xrvrnvkpsl2gchv65ih")))

(define-public crate-svgfilters-0.3 (crate (name "svgfilters") (vers "0.3.0") (deps (list (crate-dep (name "float-cmp") (req "^0.5") (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)))) (hash "0km39dbwjh3s0k7fkzr3bqkzm9gpbxb4ifmg3p141b3rxqpww3gv")))

(define-public crate-svgfilters-0.4 (crate (name "svgfilters") (vers "0.4.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9") (features (quote ("std"))) (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)))) (hash "1kjbl0khhq548ciw2lnmkk3w2q6ncda6yzgkg7qjvp2zq7mvr6k3")))

