(define-module (crates-io sv m4) #:use-module (crates-io))

(define-public crate-svm40-0.0.1 (crate (name "svm40") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sensirion-i2c") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vg6jq3ywycn15byzp39hs198y6590yqamgg74g0mx8q5y1v6ixh")))

(define-public crate-svm40-0.0.2 (crate (name "svm40") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sensirion-i2c") (req "^0.1") (default-features #t) (kind 0)))) (hash "172kjzkm3f74f8dbmqppi0f07r0agw2klkg61i5rcw4g97nb8c1h")))

(define-public crate-svm40-1 (crate (name "svm40") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sensirion-i2c") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xnxnn8vqc9jvfj7pp66mc4szgkals6hgrpqhb3xi0av97l9x4pp")))

