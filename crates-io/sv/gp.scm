(define-module (crates-io sv gp) #:use-module (crates-io))

(define-public crate-svgparser-0.0.1 (crate (name "svgparser") (vers "0.0.1") (deps (list (crate-dep (name "phf") (req "0.7.*") (default-features #t) (kind 0)))) (hash "159mkhvw17rh5i7dn34i9fhhdg7gblqwzmwglpd5cay4lq198vy1")))

(define-public crate-svgparser-0.0.2 (crate (name "svgparser") (vers "0.0.2") (deps (list (crate-dep (name "phf") (req "0.7.*") (default-features #t) (kind 0)))) (hash "1qa8qrj9wikgl71rh7k57c38ldxclwz52a8hg39p7w9gidfhz500")))

(define-public crate-svgparser-0.0.3 (crate (name "svgparser") (vers "0.0.3") (deps (list (crate-dep (name "phf") (req "0.7.*") (default-features #t) (kind 0)))) (hash "1y49240qz401smvj2h6p8zfxrx31418mjmf5b7fni4v6zxcbzicj")))

(define-public crate-svgparser-0.1 (crate (name "svgparser") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "0.7.*") (default-features #t) (kind 0)))) (hash "0lfpfimgn2hzkjlw1zp2dd7jsjlcmrjrg72frbipw7fj6w440fxj")))

(define-public crate-svgparser-0.2 (crate (name "svgparser") (vers "0.2.0") (deps (list (crate-dep (name "phf") (req "0.7.*") (default-features #t) (kind 0)))) (hash "1nvwqlyvh7rvhnzw85n9qdny1f1mkqbribw84lbzk3cb29x613h0")))

(define-public crate-svgparser-0.2 (crate (name "svgparser") (vers "0.2.1") (deps (list (crate-dep (name "phf") (req "= 0.7.20") (default-features #t) (kind 0)))) (hash "0h4bga8fp5cihkp8gyf4y719n5bsk5x10xrp89mbl0y6z15a40gy")))

(define-public crate-svgparser-0.3 (crate (name "svgparser") (vers "0.3.0") (deps (list (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "1b75bz1wi8dm309sm0gkahdd0lvx9gl0csy23afq4h9drv56i9kb")))

(define-public crate-svgparser-0.3 (crate (name "svgparser") (vers "0.3.1") (deps (list (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "1h0i8kqnvjzjjvq3bbzc06nb8fp27w6a7dbckpbljc3w3cksw642")))

(define-public crate-svgparser-0.4 (crate (name "svgparser") (vers "0.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "0xx3j30irdf4p6i94dr4gm2njknarjcwgj1llf5mfz7s3p7wczqi")))

(define-public crate-svgparser-0.4 (crate (name "svgparser") (vers "0.4.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "0syjy933w57rv6ik1x5xj8dxyazilnj8qn0zp63pi7mgc7azm2xq")))

(define-public crate-svgparser-0.4 (crate (name "svgparser") (vers "0.4.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "0k39zihiv26zbryggvh0vxk8lc6rr5980qa79x83rnkgvpzfcxri")))

(define-public crate-svgparser-0.4 (crate (name "svgparser") (vers "0.4.3") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "1llx80gii5nl92szv823brk2qkj8sfgm3g62rlc1276bqk8sz5li")))

(define-public crate-svgparser-0.5 (crate (name "svgparser") (vers "0.5.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "1mm8jgc3xqkaaqmlbhvdhmn4pkrnhkjj0rk4mm297zfj3arjg760")))

(define-public crate-svgparser-0.6 (crate (name "svgparser") (vers "0.6.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.1") (default-features #t) (kind 0)))) (hash "0c2i4ngryq8mqlfvq8bqm675a2gx00wjj02f2fy8xa71wsmfqhxh")))

(define-public crate-svgparser-0.6 (crate (name "svgparser") (vers "0.6.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.1") (default-features #t) (kind 0)))) (hash "18r8n29qm46sp3g3vak12b96gsnq0xlck8kqcllh3p0p2787d2hq") (yanked #t)))

(define-public crate-svgparser-0.6 (crate (name "svgparser") (vers "0.6.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.1") (default-features #t) (kind 0)))) (hash "05y7a9rf96al0qybdx7zh2jglyjy6jpzn03pihdkbw77283h7090")))

(define-public crate-svgparser-0.6 (crate (name "svgparser") (vers "0.6.3") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.1") (default-features #t) (kind 0)))) (hash "1544l9h0c62maylk0xf3iffqrjl16296d44x8y6lh28h026if1xb")))

(define-public crate-svgparser-0.6 (crate (name "svgparser") (vers "0.6.4") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jx30w022dqva6zia5aq6agmf1jf49dgndp4a33jcmfjyzas9clh")))

(define-public crate-svgparser-0.7 (crate (name "svgparser") (vers "0.7.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ry0wqpahdrxn3aspli9wa21nc3mj15nylxr5c6ifq95gdpgnxqy")))

(define-public crate-svgparser-0.8 (crate (name "svgparser") (vers "0.8.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.3") (default-features #t) (kind 0)))) (hash "0734vfgvjd6ryw82x1dmi4agaixdsc1ykakxgvg31lc9hb7jlj3y")))

(define-public crate-svgparser-0.8 (crate (name "svgparser") (vers "0.8.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wc5xj895in01n9j7yz30kb3gpwv6agk0bz00r71pz02n59g6iic")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.40") (hash "1nzymwlv14iby2m1mph5n0qxfh96g01km2mi6ppxwrsq2a4mn9v1") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.43") (hash "1w1q0igzd6nks50j5mzxlm15hgbz8dmmkcj85gm1qyhh4l2vag2j") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.44") (hash "0mv9yj87zcw3nb2f8wgfkjx8la956gcrfj57g804dzdbsdaxh2zk") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.45") (hash "0ggyjsv56a7dfglsap76bzi04bb38kcs157zlwbvx75xwwnzlc7w") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.46") (hash "0qvf3zrq27rrn99gr753471in4ks4anjnyn6v0bjdhbl7jkz8hah") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.49") (hash "01gr8r6gbzvin3zmwmh395n2d5fv5slsfydv8s435bdq74cy93zl") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.50") (hash "17k9vr03fvp87jzvcqpkh949api00xqpwwn2y4760mfavbnsh620") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.52") (hash "1f4snhgkw960cyhaaicdzqdfbk54py7gd29x5ri2ls5jnqiirria") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.53") (hash "14fn7iywrv314jb7cfv2ik2pnr3yaj8ppizamm55hqc4sc1py4sx") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.54") (hash "1xnwxd83w63jhipwm4z2zl98rck6bi0qdbs664qhb63gp28nkl8v") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.55") (hash "1qym2bgw1waw0g007bnvgxx6h0vb2zv7jx7qx6x14iy906aqxwnp") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.56") (hash "09v67cv0dkzi2krk9n34vz71i310h5b2f5pr6yiqr0wz6gy99cyi") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.57") (hash "0fpqf01r3ai72hnfmr801l94dng7gx1vvlidgjqp43wp68v6fdlp") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.58") (hash "0q31qm8j5sc0gpcw9w5pyh4yxlr902jfy1aifq15v9g65ys5br9k") (rust-version "1.66")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.59") (hash "18zw36iwn1iq8i70dplim3mcmr3g696izll822ahachlmycarbb2") (rust-version "1.74")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.60") (hash "11w2vsd2ki40f97jxdrlsnkmmr3qcvwakrdhf326ax3251048hdy") (rust-version "1.74")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.61") (hash "1rjg6laap2b47a74sqm9fds7khlsq2rhmn7gw4glhh4znbid6lh6") (rust-version "1.74")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.62") (hash "0ss12ww49nq9gv5kk263s7y3b792p8rj0nykp503917v25h985ni") (rust-version "1.74")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.63") (hash "0v9g5xb60xg2rlvypkbak6nv2qfn5v77hdl240a3qlssjxq14y1x") (rust-version "1.74")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.64") (hash "1fpi5dnhvgk2l8n2y435ppkyr9cf4gk6cx3ljb7s359hj5lh1iz4") (rust-version "1.75")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.65") (hash "1m6vhmbxzh39ffxvxgigmqpmml6c8n0jrj7n8fjryw934ssc99wj") (rust-version "1.76")))

(define-public crate-svgplot-2022 (crate (name "svgplot") (vers "2022.0.66") (hash "1ikrf0dp561zycs89p5af8ymrqn6qvi7vx63phpzpbzp1ilb0dg6") (rust-version "1.76")))

