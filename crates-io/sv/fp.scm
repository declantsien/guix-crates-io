(define-module (crates-io sv fp) #:use-module (crates-io))

(define-public crate-svfplayer-0.1 (crate (name "svfplayer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "jtag-taps") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "svf") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qmkh7na4ps9hxiwrm33wjyq920q62mymclz3jxvizc3i89ww1cj")))

