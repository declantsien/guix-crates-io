(define-module (crates-io sv g_) #:use-module (crates-io))

(define-public crate-svg_avatars-0.1 (crate (name "svg_avatars") (vers "0.1.0") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1957wcpwy732mvfzkqsqn9ipr98h0bnwywanmkisjkay5niwwhsj")))

(define-public crate-svg_avatars-0.1 (crate (name "svg_avatars") (vers "0.1.1") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "00ibcfibh8vc1gpg2c9ghcmqxayym00mic5cdfyniafk75l2z922")))

(define-public crate-svg_avatars-0.1 (crate (name "svg_avatars") (vers "0.1.2") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0ws1im117i1irp1k9akv6zl4qmrayrqdw2wy24r3w00sxc1wf398")))

(define-public crate-svg_avatars-0.1 (crate (name "svg_avatars") (vers "0.1.3") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "00sc5ch1wb98vh3wljzirhq44qdvm9w61azc6wr72yqc4xmc41n7")))

(define-public crate-svg_cleaner-0.1 (crate (name "svg_cleaner") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "fern") (req "=0.5.8") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1fy4q3i3sqnj7sisdpsinwklcn00ci62varipckd9k3mpypwn0ys") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

(define-public crate-svg_composer-0.1 (crate (name "svg_composer") (vers "0.1.0") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0skljjr6s141ckwd7499jm7xi4pz5ivbg1353gsdm3h0kv24zh15")))

(define-public crate-svg_definitions-0.1 (crate (name "svg_definitions") (vers "0.1.0") (hash "1dpdcyyqxr9ni0q57hghd4plsm4gqdd1jwb7596bw0y9lihx8gx1")))

(define-public crate-svg_definitions-0.2 (crate (name "svg_definitions") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0k74m6v30na1gplmwx5g5q9ll4nbfg13n22gx7mx2j5r6r0mc1nj")))

(define-public crate-svg_definitions-0.3 (crate (name "svg_definitions") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "0q4qdq8w8qj063a8gg2l2y6qw1m7sh5fgqdaly5l76vk8fxwjm5c") (features (quote (("parsing" "roxmltree"))))))

(define-public crate-svg_definitions-0.3 (crate (name "svg_definitions") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "1hyg3vglvcj1rs5xhcgbc9y8h672qcq7fdq9ip1g2fg2yhjkldrv") (features (quote (("parsing" "roxmltree"))))))

(define-public crate-svg_definitions-0.3 (crate (name "svg_definitions") (vers "0.3.2") (deps (list (crate-dep (name "roxmltree") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "1x9dmnh9y32yzss0zrkvlgw8llvz2w4r0caag52rhl0ijxlc11rb") (features (quote (("parsing" "roxmltree"))))))

(define-public crate-svg_face-0.1 (crate (name "svg_face") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1jjjs4kg207x19yhj9jbrdg4ybzharc3yl366zl5dgmp9sr8madn")))

(define-public crate-svg_face-0.1 (crate (name "svg_face") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0hwylqhvh07dsxkikvrc859bdlc0c4r9n01c8akd02cl60pj44jg")))

(define-public crate-svg_face-0.1 (crate (name "svg_face") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "13rb2ckdlkvy9zhli5953qimr73l884b4pgygx23a4zamiphl994")))

(define-public crate-svg_face-0.1 (crate (name "svg_face") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0br2pv3ca295439iqp46c2m8rq73b8knanx442a3wmry7nrjy6h4")))

(define-public crate-svg_file_parser-0.1 (crate (name "svg_file_parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "0d382qijpcaqia76105qf6gmgxhrad3cq0sk7x8q3l2nq648r0da")))

(define-public crate-svg_fmt-0.1 (crate (name "svg_fmt") (vers "0.1.0") (hash "0b85k4jmy0mqgq04iskajgjmy6cvdid2iw1gbl048qksfc4ljp88")))

(define-public crate-svg_fmt-0.2 (crate (name "svg_fmt") (vers "0.2.0") (hash "1w314j1jngkj4gbj431kyks7810wgr0lywyw6nx6fgbsnff63b0f")))

(define-public crate-svg_fmt-0.2 (crate (name "svg_fmt") (vers "0.2.1") (hash "1z06f8nzjk65l2jzpvfgcb0iqajwm3wrm2prs46g6dypi5ggkr90")))

(define-public crate-svg_fmt-0.3 (crate (name "svg_fmt") (vers "0.3.0") (hash "1nyr91y3n7jpmf3zgrszp9gr575zhj556jki0acbywvx7x7nl3q9")))

(define-public crate-svg_fmt-0.4 (crate (name "svg_fmt") (vers "0.4.0") (hash "0ayksvzxhpa7q4vqayqm6dd3sbypfy8ayw7pg82hxqp1v3zg0rn6")))

(define-public crate-svg_fmt-0.4 (crate (name "svg_fmt") (vers "0.4.1") (hash "1qjhciyls66jw9p4m7zy20ziqp39z9h44l0wzjfjxvhjyhaxzccg")))

(define-public crate-svg_fmt-0.4 (crate (name "svg_fmt") (vers "0.4.2") (hash "16dl8bd7sm2bgvilqp81flk6zbc2fwphm6xqxxvgnpi6lc1aafzq")))

(define-public crate-svg_fmt-0.4 (crate (name "svg_fmt") (vers "0.4.3") (hash "1jpjmac2pvf37k5pn0ngdcvg481yx0k4zx9yarsxcpyg8q7nmq90")))

(define-public crate-svg_metadata-0.1 (crate (name "svg_metadata") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "16lk0236ay3q6lkra43n9jpcz335ngqh1j1a9m3sw8kyl5qricc0")))

(define-public crate-svg_metadata-0.2 (crate (name "svg_metadata") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "08vsyd7xy0ngnf5hbkjk03yjrsax5jynaf9az97wznlmyiw1baq6")))

(define-public crate-svg_metadata-0.2 (crate (name "svg_metadata") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "0k8pz48g9jd5qs8ssi8pp3w4k7v5w1a773ya36i41jsg6yvbkrkk")))

(define-public crate-svg_metadata-0.3 (crate (name "svg_metadata") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "1gh4a918bfxz21z0nvj8xdl4jfgz439j8w8ycmginn6v9k6d8x8j")))

(define-public crate-svg_metadata-0.4 (crate (name "svg_metadata") (vers "0.4.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "03wvsjvbf384aq0w7nm0qivkr2h3l9kfh0zmfc4nrkx4igaqbw5i")))

(define-public crate-svg_metadata-0.4 (crate (name "svg_metadata") (vers "0.4.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0mqwhbd6698dvvv1ggkakw5lcz3m5spxqla9f1az0f93svxv3raz")))

(define-public crate-svg_metadata-0.4 (crate (name "svg_metadata") (vers "0.4.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.13") (default-features #t) (kind 0)))) (hash "1c4fjclk84pz7jmp3yn30sc1ifa41q5i8hma2xm0jqgy5qk3d7yk")))

(define-public crate-svg_metadata-0.4 (crate (name "svg_metadata") (vers "0.4.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1k8cwmki1lyn01q33kyad92ngwz0q2r11d4df48dali5bjfxzkpa")))

(define-public crate-svg_metadata-0.4 (crate (name "svg_metadata") (vers "0.4.4") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 2)))) (hash "002j0na1kfz4pgi43hdcz5baygzk6irnjd5lrmbqqfjldwn3sbx4")))

(define-public crate-svg_metadata-0.5 (crate (name "svg_metadata") (vers "0.5.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13.7") (default-features #t) (kind 2)))) (hash "0d3wz430xgpqhjysgkfmbh5wzb0p5zmvcbs3wljpps93lhznm8xx")))

(define-public crate-svg_metadata-0.5 (crate (name "svg_metadata") (vers "0.5.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0717wb4zkj5qdypg58zs7m1x45i49pdrhzga0k0wvqn2g2y7qk17")))

(define-public crate-svg_minimal-0.1 (crate (name "svg_minimal") (vers "0.1.0") (hash "1ghq8rns9hc2k5acsclan0g0i79qbxminj2kc51xffapaxbw5hp9")))

(define-public crate-svg_minimal-0.1 (crate (name "svg_minimal") (vers "0.1.1") (hash "0klklaan7shzlix98lay1cq73lar03cb07acvb6b0lcqrlgnhs91")))

(define-public crate-svg_panelize-0.1 (crate (name "svg_panelize") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.10") (default-features #t) (kind 0)))) (hash "089y6nl4svsgs7kws4hij6knjhds7k6sxi09fxmqs9pfrk6kv7b9")))

(define-public crate-svg_path_ops-0.2 (crate (name "svg_path_ops") (vers "0.2.0") (deps (list (crate-dep (name "svgtypes") (req "^0.8") (default-features #t) (kind 0)))) (hash "115lxrh1g441q23dl85kk4628spflgjjr548r68r34v192prkkhf")))

(define-public crate-svg_path_ops-0.4 (crate (name "svg_path_ops") (vers "0.4.0") (deps (list (crate-dep (name "svgtypes") (req "^0.8") (default-features #t) (kind 0)))) (hash "1s1vy3fja4h5ssn9w1z1hcv9laz6qgi3b3j1r5ggnvxarbl7cvlw")))

(define-public crate-svg_path_ops-0.5 (crate (name "svg_path_ops") (vers "0.5.0") (deps (list (crate-dep (name "svgtypes") (req "^0.8") (default-features #t) (kind 0)))) (hash "1kg8mjanisb3blfgqxf83yf1w82bldlbrvmc2j9w559ag8jk9yd9")))

(define-public crate-svg_path_simplifier-0.2 (crate (name "svg_path_simplifier") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "kurbo") (req "~0.9") (default-features #t) (kind 0)) (crate-dep (name "usvg") (req "~0.28") (default-features #t) (kind 0)))) (hash "0wkg9javqmbzcbb0q8wghk0msdiz8jbyqyv37fv4zbs9gzah6b75")))

(define-public crate-svg_to_ico-0.1 (crate (name "svg_to_ico") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ico") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nsvg") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0xx6n6q443ksfc6bzffhcqx2dxz0v4qfpz4gz5w4b6850g5mp0fj")))

(define-public crate-svg_to_ico-1 (crate (name "svg_to_ico") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ico") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nsvg") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1nljicmja5f9g4bfz6qxa5f9xnrkyllq4195qhw56ij3vadv3jb5")))

(define-public crate-svg_to_ico-1 (crate (name "svg_to_ico") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "ico") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "tiny-skia") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "usvg") (req "^0.23") (default-features #t) (kind 0)))) (hash "06w28pyj36i9pdrisxwzmvjgy2c9a9gfzdanvddd735v6ls0pf7i")))

(define-public crate-svg_to_lines-0.1 (crate (name "svg_to_lines") (vers "0.1.0") (deps (list (crate-dep (name "ash") (req "^0.29") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "raw-window-handle") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "skia-safe") (req "^0.21") (features (quote ("vulkan"))) (default-features #t) (kind 2)) (crate-dep (name "skulpin") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "svgtypes") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.20.0-alpha4") (default-features #t) (kind 2)))) (hash "029bgl2a4p58vpv1mhbg675jp18n4gqs0nxd05m05hp2v4gmf7sa")))

(define-public crate-svg_to_png-0.1 (crate (name "svg_to_png") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rsvg") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "webp") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1pim0hxkc5zd5lhx14sbsndmf6k2kfmbsla8dimhhp2rbka6na4y")))

