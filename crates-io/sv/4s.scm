(define-module (crates-io sv #{4s}#) #:use-module (crates-io))

(define-public crate-sv4state-0.1 (crate (name "sv4state") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0bdqbsjmhn99dp45f9azanhain2hf0jcfmqq8812f16gz5qzyi7f")))

(define-public crate-sv4state-0.2 (crate (name "sv4state") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1iqqx44js7n3x21vd8xv5c4nn82yajbzdy937xridfr9ka2aw14b")))

