(define-module (crates-io sv is) #:use-module (crates-io))

(define-public crate-svisual-0.2 (crate (name "svisual") (vers "0.2.3") (deps (list (crate-dep (name "generic-array") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "1r254b9a1dq771xgl4bzif8dxsc33jmpv5ng88wjj8imkgq5qn2r")))

(define-public crate-svisual-0.2 (crate (name "svisual") (vers "0.2.4") (deps (list (crate-dep (name "generic-array") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "10gqz1q5a73m7mdirr01nw18p46hgi9p9dz5i54nvr2kxrga450a")))

(define-public crate-svisual-0.3 (crate (name "svisual") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vjvsxvf5p7pgiqbyjj025jc4ax9x04mln7ad38627jppxjpz946")))

(define-public crate-svisual-0.4 (crate (name "svisual") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c38sypnjy2a6nwfravn1a050yph3f2qq6jb5qg8bn95ml2pja40")))

(define-public crate-svisual-0.4 (crate (name "svisual") (vers "0.4.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cxgj0pw4fpg2l0qxbd19zdsd0wqq3j4qyi1hf072b40ra46cyy3")))

