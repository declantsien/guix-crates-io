(define-module (crates-io sv gg) #:use-module (crates-io))

(define-public crate-svggen-0.1 (crate (name "svggen") (vers "0.1.0") (hash "1bqcwk69xcs7isif3d2pixma7ixy6y0i1q6pixynlhh96dw3kg1x")))

(define-public crate-svggen-1 (crate (name "svggen") (vers "1.0.0") (hash "0s4gh2ds2qfqapypryrycphcw94hyh7b3f5h0apdvcakyb322x5k")))

(define-public crate-svggen-2 (crate (name "svggen") (vers "2.0.0") (deps (list (crate-dep (name "rutil") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0i7r4hw1mvv7ndigwhrghlxx5mfbp0g44m8lr9s5i3p1wq3l4q49")))

