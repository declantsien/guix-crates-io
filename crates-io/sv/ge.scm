(define-module (crates-io sv ge) #:use-module (crates-io))

(define-public crate-svgen-0.1 (crate (name "svgen") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)))) (hash "0f6j9hab13svxlacjslgg8a28n6g6i40sjwax8gf22hjl0bsw107")))

(define-public crate-svgen-0.2 (crate (name "svgen") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)))) (hash "0xzcdm9r07nhswlzy7grvcxa60kxgflyvx65a1k7spfm5jzlr5p4")))

