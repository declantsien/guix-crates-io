(define-module (crates-io sv gc) #:use-module (crates-io))

(define-public crate-svgcleaner-0.7 (crate (name "svgcleaner") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^2.14") (kind 0)) (crate-dep (name "svgdom") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hvafrd6iskr1052wgnmyl08sgd53qlp97i2s99ic1z23zavqc73")))

(define-public crate-svgcleaner-0.7 (crate (name "svgcleaner") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^2") (kind 0)) (crate-dep (name "svgdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1jzn43380a3yjvb9nv6znmk29ymn4v63y3vlkzkhb8795w4564b8")))

(define-public crate-svgcleaner-0.8 (crate (name "svgcleaner") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^2") (kind 0)) (crate-dep (name "svgdom") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0nx1skd2bjjfz1bv7asvy08dywj6dwprfd9rnk2w324vml403zq6")))

(define-public crate-svgcleaner-0.8 (crate (name "svgcleaner") (vers "0.8.1") (deps (list (crate-dep (name "clap") (req "= 2.20.0") (optional #t) (kind 0)) (crate-dep (name "svgdom") (req "= 0.3.1") (default-features #t) (kind 0)))) (hash "0iacsldqzy03iqwh2xhd1rpiywbrrrswyf2w5qxjpjhd86pdq60w") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

(define-public crate-svgcleaner-0.9 (crate (name "svgcleaner") (vers "0.9.0") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.5") (default-features #t) (kind 0)))) (hash "03lsp3nykrrv74lwnfmzrxa9rbv4vr8hci5wm1rss0qlwqvgl0qx") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

(define-public crate-svgcleaner-0.9 (crate (name "svgcleaner") (vers "0.9.1") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.6") (default-features #t) (kind 0)))) (hash "0jlzsfjahhpic13bnjwj269ly27qh2wm3vxg8hbsmyagn5zwjy5c") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

(define-public crate-svgcleaner-0.9 (crate (name "svgcleaner") (vers "0.9.2") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "fern") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.9") (default-features #t) (kind 0)))) (hash "1amh16zdr5c5djc6c5h3h0mp64g63pvkw52fm23s60w6snmdv8yv") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

(define-public crate-svgcleaner-0.9 (crate (name "svgcleaner") (vers "0.9.3") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.10") (default-features #t) (kind 0)))) (hash "0d44knphwjsykpmkakcqawkp7z9yhfdhaz0y5frgs62c2rm2n8g8") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

(define-public crate-svgcleaner-0.9 (crate (name "svgcleaner") (vers "0.9.4") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.10") (default-features #t) (kind 0)))) (hash "1fc4m3gn5cr8brxy7j014y9yhv79f8mypvb88jsj7b0n8lfl7gpd") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

(define-public crate-svgcleaner-0.9 (crate (name "svgcleaner") (vers "0.9.5") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11") (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "svgdom") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1xxsr4bwyz9qxmvsizk4hswc8mnwv4y1v0x6x6s0f5444lmsi9jm") (features (quote (("default" "cli-parsing") ("cli-parsing" "clap"))))))

