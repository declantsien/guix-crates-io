(define-module (crates-io jb ra) #:use-module (crates-io))

(define-public crate-jBrain-0.1 (crate (name "jBrain") (vers "0.1.0") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0als2v7whhk8mf754njkyqgvgd9q6girdr21z1kjydki1flf5maa")))

(define-public crate-jBrain-0.1 (crate (name "jBrain") (vers "0.1.1") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "07afjagc65x1dciklkg4bx39dhba3ipqkjxqc6bazdw12i5dzg2s")))

(define-public crate-jBrain-0.1 (crate (name "jBrain") (vers "0.1.2") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "090lv4l52gp1gmilxbiw9hwpnfipn1pa0xzjwfirwbq5yggwdpil")))

