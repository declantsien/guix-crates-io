(define-module (crates-io jb cr) #:use-module (crates-io))

(define-public crate-jbcrs-0.1 (crate (name "jbcrs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "yade") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "11fcivllz0h1vz5bcc93gvg6qg1rsb5k9ms978jgi9lxn77vq9f1")))

(define-public crate-jbcrs-0.1 (crate (name "jbcrs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "yade") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ln7z7dxd646n0r02dfj80gsgcwqznqpmh8wvr1d7glhkwmz7yql")))

(define-public crate-jbcrs-0.1 (crate (name "jbcrs") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "yade") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "08zbmrvkkq3sw9fm0w0wgq7rnf17a5ac6z3dng502az88jgmxy4k")))

(define-public crate-jbcrs-0.1 (crate (name "jbcrs") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "yade") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0srkswk5n3y9lppb95wqc7y18zyzdz6h22s54cnwfwj1bfb045f4")))

(define-public crate-jbcrs-basic-0.1 (crate (name "jbcrs-basic") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "yade") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0zipkag3i32vp9wpmdy9hxwv780w309w5c7mdlxcnmf3ywl5mn6w")))

