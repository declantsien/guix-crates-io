(define-module (crates-io jb ig) #:use-module (crates-io))

(define-public crate-jbig2dec-0.1 (crate (name "jbig2dec") (vers "0.1.0") (deps (list (crate-dep (name "jbig2dec-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13gr0mz9nr01q5dzcmwgqjhnszvvgjj0svz82b9vds5cyw2f1amp")))

(define-public crate-jbig2dec-0.1 (crate (name "jbig2dec") (vers "0.1.1") (deps (list (crate-dep (name "jbig2dec-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pki0d5s0scfa6k4cw8m88y2dz08r47jc4d8jwnl2yhrp35f51lh")))

(define-public crate-jbig2dec-0.1 (crate (name "jbig2dec") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "195fch4cs7939jxypx8asqbnxgxp41368gr3imw9yla5lmvg6h49") (features (quote (("with-image" "image") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.1 (crate (name "jbig2dec") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.22") (features (quote ("png_codec"))) (optional #t) (kind 0)) (crate-dep (name "image") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0snrh1fjpchhn2xngjml5da2v6l570dp6hjagp3k1ljm6y8j752f") (features (quote (("with-image" "image") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.1 (crate (name "jbig2dec") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.22") (features (quote ("png_codec"))) (optional #t) (kind 0)) (crate-dep (name "image") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1db06l7kh75xyp394bnnmsl0mvj1rsmdkwp7nndv48syfv7wc62z") (features (quote (("with-image" "image") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.1 (crate (name "jbig2dec") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (optional #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.15.3") (optional #t) (default-features #t) (kind 0)))) (hash "0z27zb2dvw41picsd7vdf4pg0ykk3zzdv4nsgfkvc1x4am4a2sm3") (features (quote (("with-image" "image" "png") ("default" "with-image"))))))

(define-public crate-jbig2dec-0.2 (crate (name "jbig2dec") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.15.3") (optional #t) (default-features #t) (kind 0)))) (hash "1k4899pby4qg9h87qvk30zn4qxdmq69zrj28a4iswaazy6cj061b") (features (quote (("default" "png"))))))

(define-public crate-jbig2dec-0.2 (crate (name "jbig2dec") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.1") (optional #t) (default-features #t) (kind 0)))) (hash "08y99bwynaxs3g549h8mnanij16fplf741kbn6h1iw3xdj2vjv8d") (features (quote (("default" "png")))) (yanked #t)))

(define-public crate-jbig2dec-0.3 (crate (name "jbig2dec") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.1") (optional #t) (default-features #t) (kind 0)))) (hash "0qc298k69qlwsfa1nha8m8895l8vv44sx5jzl0689ibiazl4fcxw") (features (quote (("default" "png"))))))

(define-public crate-jbig2dec-0.3 (crate (name "jbig2dec") (vers "0.3.1") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 2)) (crate-dep (name "jbig2dec-sys") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.1") (optional #t) (default-features #t) (kind 0)))) (hash "05bb99hp4v654lx7sw8djsknrggvdgrjalcg5ygxhjvjbxv8bifr") (features (quote (("default" "png"))))))

(define-public crate-jbig2dec-sys-0.1 (crate (name "jbig2dec-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "1ab640xdh2w2l982zrzi2yclc6jx88wrp1vp74aqxxn07dsr7y59") (links "jbig2dec")))

(define-public crate-jbig2dec-sys-0.1 (crate (name "jbig2dec-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "00264ip44i221r1ic2qbhmbmb8zjki0lxx4ay2ha8x2pcvypj00m") (links "jbig2dec")))

(define-public crate-jbig2dec-sys-0.1 (crate (name "jbig2dec-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "08ygwqj3m3kzzj6k9qyns6nlnk1xs91fc17a86xiywq8af371m3c") (links "jbig2dec")))

(define-public crate-jbig2dec-sys-0.1 (crate (name "jbig2dec-sys") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "0m0h1m39y3vb21677sqbj3zv228v1gr4dd0kkgs110hjclgffhd4") (links "jbig2dec")))

(define-public crate-jbig2dec-sys-0.18 (crate (name "jbig2dec-sys") (vers "0.18.0") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "1i6gxhf3n44z6354qii6wpvngpm6cbp2wkggpw9jcmjdih8vq4j9") (links "jbig2dec")))

(define-public crate-jbig2dec-sys-0.19 (crate (name "jbig2dec-sys") (vers "0.19.0") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "00lmvnrbwa7h32kwn2cs64pqwxikf4ybl0sihfxl6ziq18wq0jza") (links "jbig2dec")))

(define-public crate-jbigkit-sys-1 (crate (name "jbigkit-sys") (vers "1.0.0+2.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "0im0dxvw0b4csqx6knsqnx0f93aqgbxlr29dnsjg40ndd9jyby2m")))

