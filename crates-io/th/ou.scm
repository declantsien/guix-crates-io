(define-module (crates-io th ou) #:use-module (crates-io))

(define-public crate-thoughts-2 (crate (name "thoughts") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "lazy-db") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^13.0.0") (default-features #t) (kind 0)) (crate-dep (name "stack-db") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "11b1k64lm3pka5f32y93ljq6m7gmkj6xphakn44qbpzpzhqvyhd1")))

(define-public crate-thoughts-1 (crate (name "thoughts") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy-db") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1gdj91g7a2wpwswknll3cd2hyy1zjmmq7x562cvcsw2b7ab5y18f")))

(define-public crate-thound-0.1 (crate (name "thound") (vers "0.1.0") (hash "05yfb60lq8a4dhpppf1s1yj60qbzv7lkx25wbwi1p9x7jlp23zlc")))

(define-public crate-thound-0.1 (crate (name "thound") (vers "0.1.1") (hash "1kl4z335hcnm4ki72i2sn3r42zm7ya9jwrcjs0452bx8b4rs0gig")))

(define-public crate-thousands-0.1 (crate (name "thousands") (vers "0.1.0") (hash "0v9svvspvr3c0v72klamz4gb690mr75kqiqdxf9bnzinskaylbp9")))

(define-public crate-thousands-0.1 (crate (name "thousands") (vers "0.1.1") (hash "1d5h82kbhxlf95y4cfc1yhsgkzcgzcqj5b092hlywg5hmahhgyw0")))

(define-public crate-thousands-0.1 (crate (name "thousands") (vers "0.1.2") (hash "1b4wscjqyx54nx7qrvyjrchx5armw6plcr1dkjczfk5i2i2gdxir")))

(define-public crate-thousands-0.1 (crate (name "thousands") (vers "0.1.3") (hash "05ddinl99y3by1yh4zf05qg1vrgv5dyh0wp97p161h8krmvh95kf")))

(define-public crate-thousands-0.1 (crate (name "thousands") (vers "0.1.4") (hash "0vhms2mcxcgqdab52bm0pr1mmcpffa9q6q3a0q5crcqwq2b2fyak")))

(define-public crate-thousands-0.2 (crate (name "thousands") (vers "0.2.0") (hash "0848gnkn7ah51lrx15z9zmn701ipn6gc4xbk4kfdlfahkypkpxiv")))

