(define-module (crates-io th tt) #:use-module (crates-io))

(define-public crate-thttp-0.1 (crate (name "thttp") (vers "0.1.1") (deps (list (crate-dep (name "actix-files") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "actix-rt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "listenfd") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "12bgaph0ff7mv36s5l1p606h4qz4hiqp5v93d3j0ily35kxwchia")))

