(define-module (crates-io th et) #:use-module (crates-io))

(define-public crate-theta-chart-0.0.1 (crate (name "theta-chart") (vers "0.0.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0xk7mn9cka612x2c6dy8yxvn3889wpkaxagzq736l9696bcfbpp6")))

(define-public crate-theta-chart-0.0.2 (crate (name "theta-chart") (vers "0.0.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0adb2v89mh82i18jf87w7bp8kf9shbp35ha4scnmr3v47imwyv9s")))

(define-public crate-theta-chart-0.0.3 (crate (name "theta-chart") (vers "0.0.3") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0ci5rc84f4rkkj6xmvpqw60121bgl17mmy6v3fbpifm6r4y02msc")))

(define-public crate-theta-chart-0.0.4 (crate (name "theta-chart") (vers "0.0.4") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "06blps3gm6zi9g37wh3i8c7lb1dfas2i766gmmdnrvjnhh0mp53j")))

(define-public crate-theta-chart-0.0.5 (crate (name "theta-chart") (vers "0.0.5") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1lnyzrjhafvyb29gy6iybbaapm0zv2an2anych5h5sbj3yqk999x")))

(define-public crate-theta-chart-0.0.6 (crate (name "theta-chart") (vers "0.0.6") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ddmkz2wknkknjnhr8k5cwf3chcr7ccbmm4hyk5k3hrwsn7cbh9b")))

(define-public crate-theta-chart-0.0.7 (crate (name "theta-chart") (vers "0.0.7") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7") (default-features #t) (kind 0)))) (hash "1asy90gsib2c38bsz7nfj1m862h90zajcz3106rhiwpqn60z9nmb")))

(define-public crate-theta-chart-0.0.8 (crate (name "theta-chart") (vers "0.0.8") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "robust") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ji81lccm81gshv9hx42l5ad040x5phvn62w63a776z3hj3bjg83")))

(define-public crate-thetadb-0.0.1 (crate (name "thetadb") (vers "0.0.1") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l2a05wank8pcbw1h1zppj81dn9swn57cd2bv93s8ixgak21yba6")))

(define-public crate-thetadb-0.0.2 (crate (name "thetadb") (vers "0.0.2") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d7d6py79lfq24b0nk6gc1491rlfzr5zk0npr7di5bx6k5m9aqc3")))

(define-public crate-thetadb-0.0.3 (crate (name "thetadb") (vers "0.0.3") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0agzprnqw69mplpkfb03yx2gq549i9hq3fn9as7dhrvj5m1nz7ck")))

(define-public crate-thetadb-0.0.4 (crate (name "thetadb") (vers "0.0.4") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j9dzp8ak7cqp30ajybn210rvdpb65a6s3n12q839iaqdssjx6d5")))

(define-public crate-thetime-0.1 (crate (name "thetime") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0rl361nmgp31z8avvq2aqqj4rgmjv6br896n6g6j0n1gmr2yfil9")))

(define-public crate-thetime-0.1 (crate (name "thetime") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "1vbzlq8958v9pbm8my1qfx4k0zqc0xw1wz3nicb28bap2rnjxhxn")))

(define-public crate-thetime-0.1 (crate (name "thetime") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0vkxn1azzvb49s1dgj0p456l3ijbgka864mgcg7n550gjncv5khi")))

(define-public crate-thetime-0.2 (crate (name "thetime") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "1v2cz0ibs3wm4g9g44zypmcb1wj7nhj5nbh7bw4bx50rylwjxfi6")))

(define-public crate-thetime-0.2 (crate (name "thetime") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "1g3azhqwzd10hh4k590lliiyb7ink6n837dfwv942c8pazhgbv4f")))

(define-public crate-thetime-0.3 (crate (name "thetime") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "1m7mh167016q6yw683skk828f28s9zk576rnxr9jxdzbp3waz0bl")))

(define-public crate-thetime-0.4 (crate (name "thetime") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "16ifk3d1a4fmkyj8ggif169sfbilr56q5pjf15982kwqnqm3r0ip")))

(define-public crate-thetime-0.4 (crate (name "thetime") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "10zccfpazpy97sdzlz9v1qhxr0qpi1y159g7s30fqwh8mqx0fwjz")))

(define-public crate-thetime-0.4 (crate (name "thetime") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "1g9zc34bwah4km0ry5qxmc7z07m4rrl8dk51n79j66q4zk0k65m9")))

(define-public crate-thetime-0.4 (crate (name "thetime") (vers "0.4.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "045ml771bhn7i3mzn10k6pxl4q67d7wfv04v8ya2dbckibrznwz2")))

(define-public crate-thetime-0.4 (crate (name "thetime") (vers "0.4.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "1l0ki0sij99whbm85bbzprw012i3xn7nc1yjg9qp94xjgx405dxx")))

(define-public crate-thetime-0.4 (crate (name "thetime") (vers "0.4.5") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "0hrg1dlfbyr7g6226h3wn7a0g90iax1cgcxawc14zs74a8j5riwf")))

(define-public crate-thetime-0.5 (crate (name "thetime") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)))) (hash "05g5qqgrybpr5swfrv99sm07k9fk5di3p4i80ra08dixjbkcshm4") (features (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5 (crate (name "thetime") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)))) (hash "0kvqz4f0fd5fczwqbdlp1knqag3skg0r5a2j6xl5qkpdymqb9v7b") (features (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5 (crate (name "thetime") (vers "0.5.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)))) (hash "1w4hh827g8v5lm43kdgwg2hlzlhmrpy7pawll9acsqxr4adpi5n3") (features (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5 (crate (name "thetime") (vers "0.5.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)))) (hash "1klgaia3sj3afi3rssrriwpsnjamds6y2v6v3fhm11pia1dv44g8") (features (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5 (crate (name "thetime") (vers "0.5.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)))) (hash "1xv7ir86fq4had6xhgvif2ax5bmivi98f99d9y2yi9k94l8hk2lw")))

(define-public crate-thetime-0.5 (crate (name "thetime") (vers "0.5.5") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0za78ba13nx2mmmi4y5gdag7yppmkgh76d8zra1hcz46r9bghay3")))

(define-public crate-thetime-0.5 (crate (name "thetime") (vers "0.5.6") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "135kdb879wlvjx2ycxb78iiz5j0aapm7hgp4bd8dhpnm6d58cr18")))

(define-public crate-thetvdb-0.1 (crate (name "thetvdb") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "jsonwebtoken") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mockito") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "pem") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.10") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1spxmmxgzp79gwbxjpxygf1nr9wcss9g9xm65lza52ifwfq97lms")))

(define-public crate-thetvdb-0.1 (crate (name "thetvdb") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "jsonwebtoken") (req "^7.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mockito") (req "^0.23.1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.10.1") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.11") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1wr3hqqpnp1za9sb0ri6vvhar8pzlv68wa69jd2nihz1ycj4bv9p")))

