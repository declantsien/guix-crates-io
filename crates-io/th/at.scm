(define-module (crates-io th at) #:use-module (crates-io))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.0") (hash "1mxfzm1d2385878mmcdrgaxs3pgaixf658k4iz26gzzsh53c9j27")))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z4ih6nlaac4x2iy6z5vdj30jh5z177s9gjwd99wdf7r22jv2kgc")))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.1-1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0grspx6wvv7746hdbsibmpsc2ars38gz3y0msal2cwwm6ih35lji")))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ix1jb8w7qirl4zbdwdnny78c2x02rihsvxm5jn5w8w5x5aagryy")))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.2-1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fww0ajzjzch9rgka2yz535qsm81qdzh41snjqbzrglapsivk51g")))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ypr03vwwhdjqqhd5rl53s961yg0d4z78qa1m1kxd5pbq4s8gqaz")))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.3-1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1766h25hb66marf8sms1z102lr0z7faz7im1ij5aqlbkn4wbcfjf")))

(define-public crate-that-0.1 (crate (name "that") (vers "0.1.3-2") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "188w7f49gr5rnmnfmq2mdwaz4zi4aqjp4wpfl3gspv2907z8ymqm")))

(define-public crate-thatcompiles-0.0.0 (crate (name "thatcompiles") (vers "0.0.0") (hash "0chljh9zwwcdy1jflk6vp4mqx1jyhb33si6fyfaxp0vfkhcbdjld")))

(define-public crate-thatcord-0.0.0 (crate (name "thatcord") (vers "0.0.0") (hash "0zfc2laaz265wsslqmbjj7ffv1p2ca990cp1n61j48y83fzw563x")))

(define-public crate-thaterror-0.0.0 (crate (name "thaterror") (vers "0.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "07l8gqi0q98mc4j4s5whsqlz8ffnkzifbn81r8pfpb9ax8d162x3")))

(define-public crate-thaterror-0.1 (crate (name "thaterror") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "169lgc67v5wvc76z6yfgiyigfibi814ad2i7bib5ksnki2d9sn5p")))

(define-public crate-thaterror-0.2 (crate (name "thaterror") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1zfadzpfwahh6bd9vdxhxnpacv0nh11w3jikp79lf149a2p7p27h")))

(define-public crate-thatkingguy_auth_service-0.1 (crate (name "thatkingguy_auth_service") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1kyjlh9pdsz5sm47xg59w2bs0l1v9vjw878ag228fns0jcbrsc1z")))

