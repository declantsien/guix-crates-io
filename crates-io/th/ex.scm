(define-module (crates-io th ex) #:use-module (crates-io))

(define-public crate-thex-0.1 (crate (name "thex") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12y132dkcbblj227y9hgyprkxdwg0afax4jrhv0nvsz8zdzvv80w") (yanked #t)))

