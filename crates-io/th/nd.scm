(define-module (crates-io th nd) #:use-module (crates-io))

(define-public crate-thndr-0.0.1 (crate (name "thndr") (vers "0.0.1") (hash "1lz22nr9dp4wrdgfym4nh5svhky2lx8cxpi7v64rhgr6al3p0xbs")))

(define-public crate-thndr_app-0.0.1 (crate (name "thndr_app") (vers "0.0.1") (hash "1ykmjc4c5ywr2mw3s1vvfaj0yqdacxncj1gdkx6wvlmlnqg5cxk2")))

(define-public crate-thndr_core-0.0.1 (crate (name "thndr_core") (vers "0.0.1") (hash "1v9pjbkq1vy6qz3gc8n78171yb9hivlmdqhs7cyyf071gw43gcl1")))

(define-public crate-thndr_ecs-0.0.1 (crate (name "thndr_ecs") (vers "0.0.1") (hash "1aldssssjw436ncxqvxzjr7zi69z6hdspfbhnz6mrghmwg72g84a")))

(define-public crate-thndr_event-0.0.1 (crate (name "thndr_event") (vers "0.0.1") (hash "066kg9ylcmnz7dr18xwzwmjf70fbl56xsm8r8hmpk1f4cqmlmf3m")))

(define-public crate-thndr_gpu-0.0.1 (crate (name "thndr_gpu") (vers "0.0.1") (hash "1bv24jpsw2dmgy9ciq3hkakxyd3frj7cr45f898bz16d7w9axpvp")))

(define-public crate-thndr_math-0.0.1 (crate (name "thndr_math") (vers "0.0.1") (hash "0p5y39z8046aams1f2iz5jdq6l8zv8djhazgdiscx9v2dzg695dp")))

(define-public crate-thndr_time-0.0.1 (crate (name "thndr_time") (vers "0.0.1") (hash "13cz0pfxygfy8sw5gn8wxrfz502m9fmfljnsxbg7yrag0flldb4i")))

(define-public crate-thndr_tracing-0.0.1 (crate (name "thndr_tracing") (vers "0.0.1") (hash "1axg9j2qjln8b774gyd6s599nqf42q59x6mg6f96dyqgcazmmvin")))

(define-public crate-thndr_transform-0.0.1 (crate (name "thndr_transform") (vers "0.0.1") (hash "0hsx73fc3wf9ch6p4z8f8kgsxxigpd2wmvafr9w0s5xj5sp5k749")))

(define-public crate-thndr_window-0.0.1 (crate (name "thndr_window") (vers "0.0.1") (hash "0xz7499xnl4i68kwrk6qcmycd6axgy6pmr0211l6mpmhqid5g8b4")))

