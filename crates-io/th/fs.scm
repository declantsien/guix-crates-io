(define-module (crates-io th fs) #:use-module (crates-io))

(define-public crate-thfst-tools-1 (crate (name "thfst-tools") (vers "1.0.0-beta.1") (deps (list (crate-dep (name "box-format") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "divvunspell") (req "^1.0.0-beta.1") (features (quote ("internal_convert" "compression"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1fr1lz7m99z4arnl4hjk0rw280yv6d9a2c22hj2h6gvg6w32ak29")))

