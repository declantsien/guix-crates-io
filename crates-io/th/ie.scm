(define-module (crates-io th ie) #:use-module (crates-io))

(define-public crate-thieves-cant-0.1 (crate (name "thieves-cant") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wrzji9wjgkzbn0x0lha4s7wi6mxsvaaglbr599psm1q99273ddr")))

(define-public crate-thieves-cant-1 (crate (name "thieves-cant") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1vy5cgh9lwbii1k87pmr5xg4gdnh1ha4j8768gv1d21qp1v1acg8")))

(define-public crate-thieves-cant-1 (crate (name "thieves-cant") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "121il84ab7dzra4v1w041d6av8bb3gcxrg0kn1f1sgi337hd7apg")))

