(define-module (crates-io th uj) #:use-module (crates-io))

(define-public crate-thuja-0.1 (crate (name "thuja") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.16.0") (features (quote ("crossterm"))) (kind 0)))) (hash "15rdvcx7ybfxg5bpmjfmgqv16c4r0hkzbkdgli2g5w412arsh2bv")))

(define-public crate-thuja-0.2 (crate (name "thuja") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1zlrxw64p109rhh00kmq09fk0pvbg4iigzixn40rhzr2j4w11ccq")))

(define-public crate-thuja-0.2 (crate (name "thuja") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0lq6yhxq29c0nsrv8izmv7cm2r5qw0zz1gx2c0r0grf7kvcilqg1")))

