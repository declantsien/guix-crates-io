(define-module (crates-io th en) #:use-module (crates-io))

(define-public crate-then-0.0.0 (crate (name "then") (vers "0.0.0") (hash "1s2bn7m4z1ivcsb628bbdlw8d26ripnidrf5krb3k71dh04m42gj")))

(define-public crate-then-0.1 (crate (name "then") (vers "0.1.0") (hash "0a7ngxbr6fgxwfphkrlni5c808yz4igbgdhzfrca265qjva53va8")))

(define-public crate-thenerve-0.1 (crate (name "thenerve") (vers "0.1.0") (hash "00wal47xcqzf75dn0d608ava9xv8yx6qjbm2f5kaspmhcvard54f")))

(define-public crate-thenotion-0.0.1 (crate (name "thenotion") (vers "0.0.1") (hash "199klk6yn5jinsmsyq5d529s7nhz05fsrv49ds9c60ixijxi1ji0")))

