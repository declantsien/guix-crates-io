(define-module (crates-io th ud) #:use-module (crates-io))

(define-public crate-thud-0.1 (crate (name "thud") (vers "0.1.0") (deps (list (crate-dep (name "test-case") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1hcfqwarxrc889w2wj5cplksxs37nxg6k0d7jw5mipnbwv1vdc8r")))

(define-public crate-thud-0.1 (crate (name "thud") (vers "0.1.1") (deps (list (crate-dep (name "test-case") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1g4snmi3sgb5g44xd9xjfz5a23ysj5p78yng8ajjcfffkyirynis")))

(define-public crate-thud-0.1 (crate (name "thud") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0k3z086ll9mn5ybqlbd6fch4p59kdvvhsghy0wgby09kgrx4r0xv") (features (quote (("serialize" "serde"))))))

(define-public crate-thud-0.1 (crate (name "thud") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.68") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1nsx7gr0hcgim2gj9qv0s40jqgmpn20nhf51ykjiybvjljpljw3b") (features (quote (("serialize" "serde") ("ffi" "libc"))))))

(define-public crate-thud-0.1 (crate (name "thud") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1xj8rj9vl02mfkrxw37ijbcb8jdzkp042raa7rm33v6w0rjxxnf2") (features (quote (("serialize" "serde"))))))

(define-public crate-thud-0.1 (crate (name "thud") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0za4d320hpywkkcr5ls1hdk73mna9xhlyr7ai7x5gycykx137n86") (features (quote (("serialize" "serde"))))))

(define-public crate-thud-0.1 (crate (name "thud") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0frpwpy3731jpw0cnih1dkv5dfjv2qvcsrzdvv7cbfdldd98bgii") (features (quote (("serialize" "serde"))))))

