(define-module (crates-io th hp) #:use-module (crates-io))

(define-public crate-thhp-0.1 (crate (name "thhp") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "picohttpparser-sys") (req "^1.0") (kind 2)))) (hash "1y7nk1g1spjdy2f8f8arz0h0h85w3c49cbxcraydvx3nws73ki2v") (features (quote (("std") ("nightly" "picohttpparser-sys/sse4") ("default" "std"))))))

(define-public crate-thhp-0.1 (crate (name "thhp") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "picohttpparser-sys") (req "^1.0") (kind 2)))) (hash "01qvbk3ghcanvk60hhr84jj1gi4qdd3a8kxbzlw4xjjvaksqdrnk") (features (quote (("std") ("nightly" "picohttpparser-sys/sse4") ("default" "std"))))))

(define-public crate-thhp-0.2 (crate (name "thhp") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "picohttpparser-sys") (req "^1.0") (features (quote ("sse4"))) (kind 2)) (crate-dep (name "version_check") (req "^0.1") (default-features #t) (kind 1)))) (hash "0fcx9z1mbjnspmyli8q150kvhn8chcwqmi9zzzzz8j5n79yb29pb") (features (quote (("thhp_simd") ("std") ("default" "std"))))))

