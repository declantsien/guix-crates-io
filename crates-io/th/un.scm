(define-module (crates-io th un) #:use-module (crates-io))

(define-public crate-thund-delta-0.0.1 (crate (name "thund-delta") (vers "0.0.1") (deps (list (crate-dep (name "arrow2") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "arrowalloy") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)))) (hash "0shsybc2gpprjgliny47pnmgprakwr92vr3z763cl4744acsk3qg")))

(define-public crate-thunder-0.1 (crate (name "thunder") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "2.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0dswyx5jdk6xn4swd9zs9falx96dxakmyq1zri2wlwdxq0q6ivv7")))

(define-public crate-thunder-0.1 (crate (name "thunder") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "2.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0pg13yrhk7g0f6fpdyng33yhpbhrdnnsvn8gmyq6ncg551wwfl90")))

(define-public crate-thunder-0.1 (crate (name "thunder") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "2.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1xbal62n27vv4438yrl7c9iyhbgw88p6zr6zxj00pgkxhmfav2z0")))

(define-public crate-thunder-0.2 (crate (name "thunder") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1ysr0b6azbp06pzxlhj7ix8vhwkmjrpsm4xpjl06gk5srb9cvd7z")))

(define-public crate-thunder-0.3 (crate (name "thunder") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0d1rd189c5jcvaaq13p6dc4rsi3ssixr777j0rqy8as8q8ycxw6g")))

(define-public crate-thunder-0.3 (crate (name "thunder") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12") (features (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "18j3pil2sa5798l1cgxggq0r48niladqd2ljjb7bq74nnxqwcyi7")))

(define-public crate-thunderbird-0.1 (crate (name "thunderbird") (vers "0.1.0") (hash "0s9zbp1id665yyppx71glflw5x0gmvw785gjy3lf7srh73dfznix")))

(define-public crate-thunderbird-0.2 (crate (name "thunderbird") (vers "0.2.0") (hash "03gsg4cg4zcbc0aglv67gzyfl0llq0yciikrwn1zwg68p7l0hl0p")))

(define-public crate-thunderbird-0.2 (crate (name "thunderbird") (vers "0.2.1") (hash "0038jvy9mcp42zc2x30s01lm43z92m00fycm8zbzk1n4ndamlhaz")))

(define-public crate-thunderbird-0.2 (crate (name "thunderbird") (vers "0.2.2") (hash "1k93vm8vl1hvngchypp2g6ds4alax64zsl632ijdcy504r1i53s9")))

(define-public crate-thunderbird-0.2 (crate (name "thunderbird") (vers "0.2.3") (hash "1q360ccqli366cjhc0jnczzdvda1i5wdqyxlgfd9vf6jq6nnknym")))

(define-public crate-thunderbird-0.2 (crate (name "thunderbird") (vers "0.2.4") (hash "0gg886g4d88qvfvj71sfh10w7azdckbx888fbi9zlwi2v1x3jml8")))

(define-public crate-thunderbird-macros-0.1 (crate (name "thunderbird-macros") (vers "0.1.0") (hash "0hn1v04xxik81i0s3bdvml1jxcwaq6wfghfk79f2f9l3126gzk7w")))

(define-public crate-thunderbit-0.0.0 (crate (name "thunderbit") (vers "0.0.0") (hash "15hqfyyg3sf1w5vm4qimi9n13wgl0z8krpxywzfbcan9lzrhajn2")))

(define-public crate-thunderbolt-0.1 (crate (name "thunderbolt") (vers "0.1.0") (hash "1ch5g1bs5zn24hxm3qx1qvwaja8j961jyiq1ndlvq5l6l5m1d8pq") (yanked #t)))

(define-public crate-thunderborg_driver-0.1 (crate (name "thunderborg_driver") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "17skf4k35zp9qlw7vk77nkcnmfh534xfk1av7800b0v91zgcfwrb")))

(define-public crate-thunderborg_driver-0.2 (crate (name "thunderborg_driver") (vers "0.2.0") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "08f7ksg3gidhkb419xan73mvs4nci9igmqkxxh89pl15n5rl8kan")))

(define-public crate-thundercat-0.1 (crate (name "thundercat") (vers "0.1.0") (hash "11b7l28z6ilv4mbf4cdyja665dj692g4x5rvp4c9bppjkcx6bsxj")))

(define-public crate-thunderdome-0.1 (crate (name "thunderdome") (vers "0.1.0") (hash "1lgsp76233kff2lxw0w4mgv70zl0jrz8x25z86v03v6m783ws2vf")))

(define-public crate-thunderdome-0.1 (crate (name "thunderdome") (vers "0.1.1") (hash "0px683jsdbgazgkxhyzp07nhhj8pvpy97fz2iw4ydsyhpkp6kbqc")))

(define-public crate-thunderdome-0.2 (crate (name "thunderdome") (vers "0.2.0") (hash "0jj5gw3qrbaqh1f974ap3isk9r1bn53qcqjxjsqcddgfd0w08skq")))

(define-public crate-thunderdome-0.2 (crate (name "thunderdome") (vers "0.2.1") (hash "08vd0pn041xwf15l55ymaqfxh88kwyvm30fhiszbf0cxxwzkw712")))

(define-public crate-thunderdome-0.3 (crate (name "thunderdome") (vers "0.3.0") (hash "1bmcn0fm6pc3zawh74v148baxf85x34g8dkf5z2h3m48srdl2wkm")))

(define-public crate-thunderdome-0.4 (crate (name "thunderdome") (vers "0.4.0") (hash "1wd5pbx4jww66yidkksk0lk5pbvrfyd7bpr1g7cncjwdkn6fb8yf")))

(define-public crate-thunderdome-0.4 (crate (name "thunderdome") (vers "0.4.1") (hash "1q3khv82pp99a3imfjk95rsm7n53rbcl2883l0jcwgn989vr9d47")))

(define-public crate-thunderdome-0.4 (crate (name "thunderdome") (vers "0.4.2") (hash "0hm6pgqh7q0biqhmmpwz6zr1ih41br2i4d1nygkbvl1c2x7n51gn")))

(define-public crate-thunderdome-0.5 (crate (name "thunderdome") (vers "0.5.0") (hash "0sy5jp3wxwgxdpg09m8k8sm32wqnvf84lmr6k1nw98b6m9ws3acn")))

(define-public crate-thunderdome-0.5 (crate (name "thunderdome") (vers "0.5.1") (hash "1x9i9h4jjh12cr0irb2rmfxsfnnv7bbvprp41i9mpdr3gphryswd")))

(define-public crate-thunderdome-0.6 (crate (name "thunderdome") (vers "0.6.0") (hash "08anibamzhsjbb4xzlcir1vrylrsgi8amd4sw1fhyi5v5rda48k6") (features (quote (("std") ("default" "std"))))))

(define-public crate-thunderdome-0.6 (crate (name "thunderdome") (vers "0.6.1") (hash "02wdv7r38zl5sz1nppffak8b3b97pxmi2c9wzvk9mgv06gwp1qcj") (features (quote (("std") ("default" "std"))))))

(define-public crate-thunderstore-api-1 (crate (name "thunderstore-api") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0sr46i8rd7ixa14dn1mnl15zmr4q6j122pl1d6qg7r09nr8n4cqz") (yanked #t) (rust-version "1.63")))

(define-public crate-thunderstore-api-2 (crate (name "thunderstore-api") (vers "2.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0yr90xw4qm2pj17cpb26y70x93q8m7pb4c4fq3il3a4d42rihanz") (yanked #t) (rust-version "1.63")))

(define-public crate-thunderstore-api-0.3 (crate (name "thunderstore-api") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ts-rs") (req "^6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0akbicaa7n43rkmzxpmf6my7hr50ab9amw55arpawi3109dhqqb8") (rust-version "1.63")))

(define-public crate-thunderstore-api-0.4 (crate (name "thunderstore-api") (vers "0.4.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ts-rs") (req "^6.2") (features (quote ("serde-compat" "format" "uuid-impl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0rdg657p95rc2skjwq9zrwjb5d7h85z90iwcs14nnxg6g9gq8riv") (rust-version "1.63")))

(define-public crate-thunderstorm-0.0.0 (crate (name "thunderstorm") (vers "0.0.0") (hash "01qvda7d2ac6zapmdhp5dg5jrngd4fi7dfd0k39f4ps2bg5jx1w8") (rust-version "1.65")))

(define-public crate-thunk-0.1 (crate (name "thunk") (vers "0.1.0") (deps (list (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1xrrnvznn2jwcimynf5vibxk154zkarrxhs0sdrqy4va4q4lalbi")))

(define-public crate-thunk-0.1 (crate (name "thunk") (vers "0.1.1") (deps (list (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ivbh8xqpls5c05cab1z31nabnpngdl40lgj9iqf6v69p3dig9xj")))

(define-public crate-thunk-0.1 (crate (name "thunk") (vers "0.1.2") (deps (list (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ck5877kfx5wgdrlc5fx34lkh88x8abc5i1m57fyw5qnvn4zs9dy")))

(define-public crate-thunk-0.2 (crate (name "thunk") (vers "0.2.0") (deps (list (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1xpxzkgx84dl330fc2jfg8zdsgfbpif6726v6qmg7kdq3anh2qa1")))

(define-public crate-thunk-0.2 (crate (name "thunk") (vers "0.2.1") (deps (list (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "03rbv9qrkyggslx6zjn27y4cmy5102nfbqbrr3ya27i29kpsk48j")))

(define-public crate-thunk-0.3 (crate (name "thunk") (vers "0.3.0") (deps (list (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "100gk1p7mc2i915l0habzlcbl69qd85nn3aw37lbddv6lyybznfq")))

(define-public crate-thunk-rs-0.1 (crate (name "thunk-rs") (vers "0.1.0") (hash "08dxdgzd7j6z0r8yai0vh9zwz6q8f2h72ji97vhshgs3i80c71p7")))

(define-public crate-thunk-rs-0.3 (crate (name "thunk-rs") (vers "0.3.0") (hash "0zhbnr9f7vp45j5abhfqs050n1hflfllg78fchs6d0wpgryn12ir") (features (quote (("windows_xp") ("windows_vista") ("vc_ltl_only") ("subsystem_windows") ("lib") ("default" "windows_xp"))))))

(define-public crate-thunk-rs-0.3 (crate (name "thunk-rs") (vers "0.3.1") (hash "07gpkmcwykf4kgdnyzvxxj5b76s5fb6drl5dd8sifcxdlx6p4xxz") (features (quote (("windows_xp") ("windows_vista") ("vc_ltl_only") ("subsystem_windows") ("lib") ("default" "windows_xp"))))))

(define-public crate-thunk_simple-0.1 (crate (name "thunk_simple") (vers "0.1.0") (hash "1v24xad3xarj18qrd2q9nwnyr9afsp4hkdgxywgmj4c4096y0gj7")))

(define-public crate-thunk_simple-0.1 (crate (name "thunk_simple") (vers "0.1.1") (hash "0slvncswhwfip9d02nn1h6jw8hark7l4n3lic77n5krbkw5rhq26")))

(define-public crate-thunks-0.0.1 (crate (name "thunks") (vers "0.0.1") (hash "04rw0phkm3653bkwbbxkks57ydylpnh0snh4c7yz9l8n0l8pxz2w")))

(define-public crate-thunks-0.0.2 (crate (name "thunks") (vers "0.0.2") (hash "0jza15pw4zfzg322fk9gvjc1b782ixx1imfwyi3f0k3qxkbn7wz3")))

(define-public crate-thunks-0.0.3 (crate (name "thunks") (vers "0.0.3") (hash "1znmbld5zay2ri4iifaxqzlvnp72hnid8xr7427qd0gk9chi4xc3")))

(define-public crate-thunks-0.1 (crate (name "thunks") (vers "0.1.0") (hash "0h5x5qrvmb3h3sxxcymmwkmhvab3qmhwq3s7wzmrp6cz5mfh2drz")))

(define-public crate-thunky-0.8 (crate (name "thunky") (vers "0.8.0") (deps (list (crate-dep (name "tokio") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1mqkpprmfr7j717paikyg098r3lacv9gnlidp2q778wvcsbj7yd1") (yanked #t)))

(define-public crate-thunky-0.9 (crate (name "thunky") (vers "0.9.0") (deps (list (crate-dep (name "tokio") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "13xx5fawywpw1nnw0wm8rx1qb7wyi7ym02lmxxblp6ygwzg9shl4")))

