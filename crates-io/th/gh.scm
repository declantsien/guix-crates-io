(define-module (crates-io th gh) #:use-module (crates-io))

(define-public crate-thghosting-data-centers-0.1 (crate (name "thghosting-data-centers") (vers "0.1.0") (deps (list (crate-dep (name "http-api-client-endpoint") (req "^0.2") (kind 0)) (crate-dep (name "scraper") (req "^0.12") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (kind 0)))) (hash "1kgidkdgh3y878b4hkk48rlxz0ilf233fpxyqfqplmcji9nrnmwl")))

(define-public crate-thghosting-data-centers-0.1 (crate (name "thghosting-data-centers") (vers "0.1.1") (deps (list (crate-dep (name "http-api-client-endpoint") (req "^0.2") (kind 0)) (crate-dep (name "scraper") (req "^0.13") (kind 0)) (crate-dep (name "thiserror") (req "^1") (kind 0)))) (hash "0n5gjchrc4ppak78rmnx96bhmxzg4n8h0893w68xz78cwjmk3apq")))

