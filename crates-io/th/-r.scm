(define-module (crates-io th -r) #:use-module (crates-io))

(define-public crate-th-rust-0.1 (crate (name "th-rust") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0x3ygrvivr81d0b9knivxyn5wgl4867wpdm8f1jwwjnlwz5hrrim")))

