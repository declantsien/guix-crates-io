(define-module (crates-io th om) #:use-module (crates-io))

(define-public crate-thomas-0.2 (crate (name "thomas") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "device_query") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "thomas_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1v5y6835ax3djljga5q651fv36dh1ipb6c3zdvnfzgw15navbhya")))

(define-public crate-thomas-0.2 (crate (name "thomas") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "device_query") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "thomas_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "176cdfd6z8jyg23gsrnsvzlj4szmfvvcfb6qh9kcv3jiia0sspll")))

(define-public crate-thomas-0.2 (crate (name "thomas") (vers "0.2.2") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "device_query") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "thomas_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16l7c0nba6zjsbvykqdjv3imxm3lwnjjzccp7rdcw6ps69jvmygj")))

(define-public crate-thomas-0.2 (crate (name "thomas") (vers "0.2.3") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "device_query") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "thomas_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1dbrmms6r098x28fmri80mv03a49h0r00bb6q5pkqnws0a9bh83k")))

(define-public crate-thomas-0.2 (crate (name "thomas") (vers "0.2.4") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "device_query") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "thomas_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09ir8m5w3s79js8nzalcp0zy0k7gik6njpnvmpm0n1iyxw36w048")))

(define-public crate-thomas_derive-0.2 (crate (name "thomas_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1iq32vyy9v4j07ylfnx7m85pfayf4ppddbv1lkz2150zglzpqq59")))

(define-public crate-thomdabrowski-0.1 (crate (name "thomdabrowski") (vers "0.1.0") (hash "1883k8i55s210hiasjy6vx1xb3s8fp3k06lc8h1n7c9r76j58jak")))

