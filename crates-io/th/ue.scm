(define-module (crates-io th ue) #:use-module (crates-io))

(define-public crate-thue-0.1 (crate (name "thue") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15s0pg0hjzq7r3vd5ly71mpx79wcpm30gh9rz4cccmxj03g8895w")))

(define-public crate-thue-0.1 (crate (name "thue") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rb3n2x3nqxl0zfq9gr3lhlkx3jgri98hyrfjmp56lm8sjpnal1j")))

(define-public crate-thue-rs-0.1 (crate (name "thue-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1hd3j93b8s4qa88c2lzdyh2cf8x2rahpy60aygmlgy64b7117rpv")))

(define-public crate-thue-rs-0.1 (crate (name "thue-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0bghw71l2p97dbslflr1s2ksc551izm65cacracmnq1hnzid9wxf")))

