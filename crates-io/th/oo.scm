(define-module (crates-io th oo) #:use-module (crates-io))

(define-public crate-thoo_readext-0.1 (crate (name "thoo_readext") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1q4qzldnaam8293giw6qlw4njayx1dpqbr9a7rrdf5lx42m9wb6v")))

(define-public crate-thoo_readext-1 (crate (name "thoo_readext") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0xvyzfykd7qgmnmf3l7himcjflmz0bjamz36kbziqhn7md35p27m")))

(define-public crate-thoo_readext-1 (crate (name "thoo_readext") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0cbn9nmz9hb79pdxabgmy0a50sxmj5cnfllx8n4ylia1f6nw57r6")))

