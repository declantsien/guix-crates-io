(define-module (crates-io th ic) #:use-module (crates-io))

(define-public crate-thicc-0.0.0 (crate (name "thicc") (vers "0.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11hgivhp24sx3a2hsj6xpxl3g259h9xjd9l9dr6gxy3853w51nyc") (features (quote (("std") ("default") ("alloc"))))))

