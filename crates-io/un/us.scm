(define-module (crates-io un us) #:use-module (crates-io))

(define-public crate-unused-0.0.0 (crate (name "unused") (vers "0.0.0") (hash "12n42qzbfzjdaqn0d2g97z1vy339fl375hy6y2q07cfqmhkbl852")))

(define-public crate-unused-0.1 (crate (name "unused") (vers "0.1.0") (hash "0rgk12k5klq11v5yz8279h4fn8l21iv2544265zabaipygh8mqyw")))

(define-public crate-unused-pkg-0.1 (crate (name "unused-pkg") (vers "0.1.0") (deps (list (crate-dep (name "cargo_toml") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "crates_io_api") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "package_json_schema") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.202") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1rs0axka8f719s74f9gbcm3cy6473q6kw01qxxxp85zd8iimh08k")))

(define-public crate-unused_variable-1 (crate (name "unused_variable") (vers "1.0.0") (hash "1kfppzhqc1i7x0wcj6z5iw6g5js1vd826rzha5542ifzn4335lfk") (yanked #t)))

