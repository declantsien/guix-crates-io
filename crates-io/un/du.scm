(define-module (crates-io un du) #:use-module (crates-io))

(define-public crate-undulate-0.1 (crate (name "undulate") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.27") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0psp1idgknq0cfdkigix3yr1s3sbza9p1jj701fpa5xkwsdgsjgw")))

(define-public crate-undup-0.0.1 (crate (name "undup") (vers "0.0.1") (hash "1a9q2iv0s8kvw7hfcmlkq0vfpz64x6xl3h72qpnfnvz2z48sq7c2")))

