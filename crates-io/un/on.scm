(define-module (crates-io un on) #:use-module (crates-io))

(define-public crate-unone-0.1 (crate (name "unone") (vers "0.1.0") (hash "1mg56gv8734r06h2z393625ib6agrg47cis670vjaycia7xiqkbx") (yanked #t) (rust-version "1.56")))

(define-public crate-unone-0.0.0 (crate (name "unone") (vers "0.0.0") (hash "1d0jlidrni31w92jc5sip98r2ss0p2xx5s1hn2kmhf3gir5fm73f") (yanked #t) (rust-version "1.56")))

