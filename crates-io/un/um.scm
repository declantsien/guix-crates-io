(define-module (crates-io un um) #:use-module (crates-io))

(define-public crate-unum-0.0.0 (crate (name "unum") (vers "0.0.0") (hash "1w7hygh2jhs1idm87qlbc57pygai2mkqpm1zrb31ldbjrcgkla18")))

(define-public crate-unums-0.0.1 (crate (name "unums") (vers "0.0.1") (hash "0scw4ks599709f22cm3wfa0bz57aivpxh8mp21w9c6irrmkmkfzy")))

