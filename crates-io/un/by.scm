(define-module (crates-io un by) #:use-module (crates-io))

(define-public crate-unbytes-0.1 (crate (name "unbytes") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)))) (hash "10h1y674lvwdhfdi43jr3g9614r8v3g1pwzw2387h7jcpfvzs23r") (features (quote (("std") ("default")))) (yanked #t)))

(define-public crate-unbytes-0.2 (crate (name "unbytes") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)))) (hash "0dw9b6jd74g8nx6ij2xlcd1q4wlwylw3s5w53if0988d1wnng31f") (features (quote (("std") ("default")))) (yanked #t)))

(define-public crate-unbytes-0.2 (crate (name "unbytes") (vers "0.2.1") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)))) (hash "1hsiry68vyvjg3wydsq2qvfky18bwyy9zmzm8zy22ji40fh6ivqy") (features (quote (("std") ("default")))) (yanked #t)))

(define-public crate-unbytes-0.2 (crate (name "unbytes") (vers "0.2.2") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)))) (hash "19q152fjvj3yyr1kn5kxgqwid5accpmlmn7r4f17jj950nlwa3w6") (features (quote (("std") ("default"))))))

(define-public crate-unbytes-0.3 (crate (name "unbytes") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)))) (hash "0qzbr684h6b7lpq1vikkwbvg5f6ri84k8009qakj35k56rbv0sf5") (features (quote (("std") ("default"))))))

(define-public crate-unbytify-0.1 (crate (name "unbytify") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m3i3gzl5qpq6siqm504mmgi5yl0bisgbbcxwf68mf05dzh2hbn3")))

(define-public crate-unbytify-0.1 (crate (name "unbytify") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s2xn9z388wq2zgwdfvjqrhnpi31y0jqq3w9dz4rn5hngn96769q")))

(define-public crate-unbytify-0.2 (crate (name "unbytify") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "0scqzkqb6krrdx1zc1z85kjkfpsfy7nd7hyc6lfja36n9wsk3x31")))

