(define-module (crates-io un zp) #:use-module (crates-io))

(define-public crate-unzpack-0.1 (crate (name "unzpack") (vers "0.1.0") (deps (list (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "09dpn89z4kj82a9yxaq1kklq6y05rqxmmjw800zc52xg4bqzmqaz")))

