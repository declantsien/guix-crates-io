(define-module (crates-io un sy) #:use-module (crates-io))

(define-public crate-unsync-0.1 (crate (name "unsync") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 2)))) (hash "19mx537igldzqk20cdb6xwzc2w8srlsawq0dvh3zawr3a144qq78") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-unsync-0.1 (crate (name "unsync") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 2)))) (hash "0g3f84sbmrzqaxbk9q8pvyvhyk36da6xp2b28vn7x14d21sc4jc4") (features (quote (("std") ("default" "std"))))))

(define-public crate-unsync-0.1 (crate (name "unsync") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("macros" "rt" "sync"))) (default-features #t) (kind 2)))) (hash "0hm2j6gizzadk35n6mcfnc3v1saqlbaplkmis9jbhl8nj6qwb4qi") (features (quote (("std") ("default" "std")))) (rust-version "1.58")))

(define-public crate-unsync_channel-0.1 (crate (name "unsync_channel") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros" "sync"))) (default-features #t) (kind 2)))) (hash "158mhva7rhpf2c2g06jv0l6n9gdrfbz1svfrym1b6mradqqi6w3q")))

(define-public crate-unsynn-0.0.0 (crate (name "unsynn") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (features (quote ("proc-macro" "span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "1rgqgpb1hjail3phya00va65dm3zzpng4c08cxk11z6ipd2c7816") (rust-version "1.59.0")))

(define-public crate-unsynn-0.0.1 (crate (name "unsynn") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.81") (features (quote ("proc-macro" "span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "05pi05k3zzn1xbczpaa7nddrfgvnas0x8ccwiyk3n0h8pzpbkaxn") (rust-version "1.59.0")))

(define-public crate-unsynn-rust-0.0.0 (crate (name "unsynn-rust") (vers "0.0.0") (deps (list (crate-dep (name "unsynn") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "1c69s1z4m49980jba306yq0jkg21i0jzwlc35ys7zjdbkbp2whf8") (rust-version "1.59.0")))

