(define-module (crates-io un la) #:use-module (crates-io))

(define-public crate-unlambda-0.1 (crate (name "unlambda") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "05cizj3jgxc4fc7spx5166lcznxfhdg836ixcxd6a3ydn35kfvhj") (features (quote (("arc"))))))

(define-public crate-unlatex-0.1 (crate (name "unlatex") (vers "0.1.0") (deps (list (crate-dep (name "rquickjs") (req "^0.1") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v1s051ascmv2wkz1jv96cavn2y1blyw1w6akwchnd4cy3m1qgww") (features (quote (("default"))))))

(define-public crate-unlatex-cli-0.1 (crate (name "unlatex-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "unlatex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g0zfc5xkvprky469ganj30kza17kskf02h1k8zja43n2vhl4gl7")))

