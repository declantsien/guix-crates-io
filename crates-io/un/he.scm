(define-module (crates-io un he) #:use-module (crates-io))

(define-public crate-unhex-0.0.0 (crate (name "unhex") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive" "unicode" "string" "env"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1gkbdfsf4a6w5qwbz7340pgv7szgyi2lxrsxc58mn5ha6sbbfai6")))

