(define-module (crates-io un it) #:use-module (crates-io))

(define-public crate-unit-0.1 (crate (name "unit") (vers "0.1.0") (hash "1b1kdb5s29vi3c1agchh9cqc6ci59fhfvk99bgjimx91d4038y3i")))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.11-alpha") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "00ia7w6wvvpr0js085sv6zdyj8rfdzj2dy4qc3pqqhzkh8nyqlh2") (yanked #t)))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.11-alpha.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0nlq65xp06i9m8p9zvrjwj34cx44dxb10rwscvwcsj45889d1rly") (yanked #t)))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.11-alpha.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "04hdpfc2rhy4r0b7q5xp2x1lrvg5c9qyk7rh5f0kwsbjbn6lhx19") (yanked #t)))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.11-alpha.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0ykmp4hvajnyaxjcgx04dsr6q6cgqd6q3vlfm1dfvrnr6ykasjrr") (yanked #t)))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.11-alpha.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "005pkl3wh385bw317y9818dmrwxbi2vddgasfj6a2hr3r4gq08qq")))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.11-alpha.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "13q4isszwnk6msk11ky43hj4py71grajv19wyjb2rhfv19dw4giy")))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.13") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0ha8w7aybylr7a16xzhzr7qwa00hph0nvqav02bgnvjzhgd6c06y")))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.15") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "16wp2i49s0f0m1b8pbvbb358j2z6cn12zrl1r992ijd5m1gjhg01") (yanked #t)))

(define-public crate-unit-conversions-0.1 (crate (name "unit-conversions") (vers "0.1.16") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0pq9vs53bdx6hp2z8h8p1k7pc1w3gygvva9frq5dnp07qgq2sjmg")))

(define-public crate-unit-derive-0.1 (crate (name "unit-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1da9964j19rz7qj8n4ca3c4caby6g562xncxdzxvh6fj7x0j1scb")))

(define-public crate-unit-enum-1 (crate (name "unit-enum") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06wy2rxj3vn1wla8aykhkk3gxm5ipyr8jrlhzwys1dbby4c2swzj")))

(define-public crate-unit-prefix-0.5 (crate (name "unit-prefix") (vers "0.5.0") (hash "1hxxqqp920zlmqdq2dpwq34s3apxkxaw9bwbhym9121pwxasvph4") (features (quote (("std") ("default" "std")))) (rust-version "1.31.0")))

(define-public crate-unit-proc-0.0.1 (crate (name "unit-proc") (vers "0.0.1") (deps (list (crate-dep (name "const-frac") (req "^0.0") (features (quote ("tokenize"))) (default-features #t) (kind 0)) (crate-dep (name "physical-quantity") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "physical-quantity") (req "^0.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (features (quote ("no_std"))) (kind 2)))) (hash "1spr4x3q3wxn2249jx5m094kysq0v6663xyc95smf2qr81bnm7pq")))

(define-public crate-unit-proc-0.0.2 (crate (name "unit-proc") (vers "0.0.2") (deps (list (crate-dep (name "const-frac") (req "^0.0") (features (quote ("tokenize"))) (default-features #t) (kind 0)) (crate-dep (name "physical-quantity") (req "^0.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "physical-quantity") (req "^0.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (features (quote ("no_std"))) (kind 2)))) (hash "032jqqh9lanzv170hy98n2kw4knc9ymqdc0f6bgcjkkcbh4zc1hl")))

(define-public crate-unit-proc-0.0.3 (crate (name "unit-proc") (vers "0.0.3") (deps (list (crate-dep (name "const-frac") (req "^0.0") (features (quote ("tokenize"))) (default-features #t) (kind 0)) (crate-dep (name "physical-quantity") (req "^0.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "physical-quantity") (req "^0.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (features (quote ("no_std"))) (kind 2)))) (hash "161y4d0103pk8vmf3vccs5g0jjpn0l3yyj30wq893wykc5w8mrvr")))

(define-public crate-unit-root-0.1 (crate (name "unit-root") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "01dial22hhddkf77wy01867vk3cz69jsrjavj54slv8g6zdbqqb1")))

(define-public crate-unit-root-0.2 (crate (name "unit-root") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0f8k08j3k8qz236gljyzylka5v766hl065yfy3c4ns3hc770caca") (features (quote (("unstable" "rand" "rand_distr") ("default"))))))

(define-public crate-unit-root-0.3 (crate (name "unit-root") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0xjblvryxx67qyq8zig4razyww18andavcgpn7nrf13qv8qmiw2z") (features (quote (("unstable" "rand" "rand_distr") ("default"))))))

(define-public crate-unit-root-0.4 (crate (name "unit-root") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ibdkjv69izw3aiympvbjnw3ad5mccncs1g7n84qr82rfa796r0b") (features (quote (("unstable" "rand" "rand_distr") ("default"))))))

(define-public crate-unit-root-0.5 (crate (name "unit-root") (vers "0.5.0") (deps (list (crate-dep (name "approx") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1w3yg3ln56mkya8q9b3kn8ia2a5lnl874ki8y6j91k0ml4nri4xj") (features (quote (("unstable" "rand" "rand_distr") ("default"))))))

(define-public crate-unit-root-0.6 (crate (name "unit-root") (vers "0.6.0") (deps (list (crate-dep (name "approx") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1pyc7kr40l01m95k83ghlx0k8qy94vbakdlvhkfbz8yhf7mxvfgw") (features (quote (("unstable" "rand" "rand_distr") ("default"))))))

(define-public crate-unit-rs-0.1 (crate (name "unit-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "16i3hizzwm9bcxifac80mgw8nn2mif1hk16m6ijcq5k572iv6jja")))

(define-public crate-unit-rs-0.1 (crate (name "unit-rs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "15yadkkymic8z54zvblxszm8wwz1jd3m9f9rljdnmq35k685cf06")))

(define-public crate-unit-rs-0.2 (crate (name "unit-rs") (vers "0.2.0-alpha1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "http") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "0f9zz3ghnv343lnliihj8qadsswl9k0qw5a9fbqqqq8nw5dqzvyh") (features (quote (("default")))) (v 2) (features2 (quote (("http" "dep:http"))))))

(define-public crate-unit-rs-0.2 (crate (name "unit-rs") (vers "0.2.0-alpha3") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "http") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "12fqilq42y8jk44m3i8cwp871nadw4m4qf23rqi59f0kgxxgk1ff") (features (quote (("default" "http")))) (v 2) (features2 (quote (("http" "dep:http"))))))

(define-public crate-unit-rs-0.2 (crate (name "unit-rs") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "http") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "0gyhwh8xfx5vism95k8m17bns5y3681m4lsyv83dy1624g70rlkx") (features (quote (("default" "http")))) (v 2) (features2 (quote (("http" "dep:http"))))))

(define-public crate-unit-sphere-0.1 (crate (name "unit-sphere") (vers "0.1.0") (deps (list (crate-dep (name "angle-sc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1jh5zcw7z8340z0x30pl2drzb2hrarxh6gnn2spnr25dwnhqav4l")))

(define-public crate-unit-sphere-0.1 (crate (name "unit-sphere") (vers "0.1.1") (deps (list (crate-dep (name "angle-sc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1anvvkbxbv2i7sf9jmlwlvz29bbjl8igfzknf111952qslshqw5v")))

(define-public crate-unit-sphere-0.1 (crate (name "unit-sphere") (vers "0.1.2") (deps (list (crate-dep (name "angle-sc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1dpmx2f086wm00q21vwq37xqd88b8bsj1f2v0fvl3ayrpz3zw22j")))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.0") (hash "0bjpvdvv2vqcdy3i98d1s4flag8qxgh9hlgls79mvk0fxz74slgn") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.1") (hash "0nv2h63mbaw2my2c4v3dkb7dayid4l9szc26md2dry54f73canl0") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.2") (hash "17j1rk971rpdlb5nzksvk1g4ak1x4gv8jxryahxmz643qa84cgyp") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.3") (hash "0ibrnn1j9r4vs2qcvks7rli4xd1lg8x4zabgw9cdzaaa94l0913j") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.4") (hash "02xrnn0ykcr7w9kkac2vihw05kjxn0rj4mhkaar2afibal2dk5xg") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.5") (hash "0m1qr89i7k4nqv05jqp2g3ww0bnvqzf1xxgs5nbs6qxygmscjr6l") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.6") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qgjzvmv0g5l6l2amva3ag37qc99ighssihlxwnydk53jy4qw414") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.7") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "099r9pbnpqgxgvzlzn1j1azivwynk2vh8jw0xyallqm29k0vgbcz") (yanked #t)))

(define-public crate-unit-testing-0.1 (crate (name "unit-testing") (vers "0.1.8") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rxk0d07q7vzs1qbq1sdb12m8xf1zhxycgm7c5iri7f5bl56k2vm") (yanked #t)))

(define-public crate-unit-testing-0.2 (crate (name "unit-testing") (vers "0.2.1") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "0vxdi93bmp6xm5jahcdxdxwc2y1bywjjii18ynansrs1w8sb215w") (yanked #t)))

(define-public crate-unit-testing-0.3 (crate (name "unit-testing") (vers "0.3.0") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1ll13g51d2ywx89dgz69wpbjhnl779d9r1202d8vlbp8mzmy9lfi") (yanked #t)))

(define-public crate-unit-testing-0.3 (crate (name "unit-testing") (vers "0.3.1") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1xkl3w2kmifja44bv27ckyzvq07qp9421ljbza8m74jny4951m87") (yanked #t)))

(define-public crate-unit-testing-0.3 (crate (name "unit-testing") (vers "0.3.2") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1dh2b2hqv3zv8bd7xj42j3lvmpv8y4mj63d6s5a6r2cwp21h0a2p") (yanked #t)))

(define-public crate-unit-testing-0.3 (crate (name "unit-testing") (vers "0.3.3") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1i7fn1n4hl3spwjc94yi9rzhlxsp3yz5q6av31pajp88n6ww5fbn") (yanked #t)))

(define-public crate-unit-testing-0.3 (crate (name "unit-testing") (vers "0.3.4") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "173qk74ap6x1cdd9f25knfl0h6knjb6j8c9q1ag0q76mrlpccv0i") (yanked #t)))

(define-public crate-unit-testing-0.3 (crate (name "unit-testing") (vers "0.3.5") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1g2pib3k8zsx2y543xrdv35jpi64ahb0lzp4j5lrb2cjndcz76bl") (yanked #t)))

(define-public crate-unit-testing-0.4 (crate (name "unit-testing") (vers "0.4.0") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "17nsair732pz2sfaxv3jzwhya8xnwycjindvcsh90h6nvvnf84yw") (yanked #t)))

(define-public crate-unit-testing-0.4 (crate (name "unit-testing") (vers "0.4.1") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1si1rbkv5j9svf80yk13w068n9hfq1cf881srb4cdpq3626vmq4g") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.0.0") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1ghvdsj67dxlfsklp5yanna19lnvvmmms5pbl8d4wp194a74af2n") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.0.1") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0781czsi5da621mp3dx48xy8dqin41fyb518l3bxpf8j83x51hxz") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.0.2") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1lvfwcrbsd537gjafl1zcj10n3cdrqps45lzvd5cyz815fzli5jc") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.0.3") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "11d6cjbzx8465fr7xzdjgzk30wzrfp7d4x47dy5163lg8risszsy") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.0.4") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0hvv9lplrn9z9pmjfj2rfrri2z02wp4w3mb9cn029bzp9q8ny6n2") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.1.0") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1xaysxnv1k36vn7bw3cz8z9g0kvsy1k0ns1m9lyvda5acf79r7z6") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.1.1") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0n3hyvwg10ir0f2wdqwlivmh8dq37s1544wr7hkwkddwfkrrvlh1") (yanked #t)))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.1.2") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0q6rr82xkxx4szv56z53qql9az9g7jc1aq4z2j9vj4knnwdv45zk")))

(define-public crate-unit-testing-1 (crate (name "unit-testing") (vers "1.2.1") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1f9p61dp8a1j9zgh80vqbgfsvlb9q6kcy0k49jcyvw0zh0fkfwl1")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.0.0") (deps (list (crate-dep (name "ansi_codes") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm_cursor") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "test-env-helpers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1lnyx8b4l7g3b9yn65np70b5pfcbx8c2k92qsrx51994gf40m9f7")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.0.1") (deps (list (crate-dep (name "ansi_codes") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm_cursor") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "test-env-helpers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "17bi2w63wknp8l0xczpmz4jg6simhaz8naxwkyz53253bhcig312")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.0.2") (deps (list (crate-dep (name "ansi_codes") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm_cursor") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "test-env-helpers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1676az2p8p8wl2xc0ixspz099qqcg62lmg877wq2lxy674n6qm6a")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.0.3") (deps (list (crate-dep (name "ansi_codes") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm_cursor") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "test-env-helpers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1lc0y4k8yh4jylprnxigv61ppbcaj0apb225npg57rzzfik6lp3j")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.0.4") (deps (list (crate-dep (name "ansi_codes") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm_cursor") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "test-env-helpers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0lj8jnx35qa7f3rcbdqljhky3kb8c60xi12rcnqrc41nbyl6kzb0")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.0.5") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "test-env-helpers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0qc4h3n71s7wq9hq39gfvk0cyrf90cjp7bn9lq2dkyv08pz17q1j")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.1.0") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0vf741zplhyrg7d88lvlhxz3d7y0k3akcllhnv2kqh566cimrp5a")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.1.1") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1s26j4avmki62jkiy2dmp9sgkg4sbxicyavrgk3qjbaa3wps2qwk")))

(define-public crate-unit-testing-2 (crate (name "unit-testing") (vers "2.1.2") (deps (list (crate-dep (name "colored_truecolor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "is_executable") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "progress_bar") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "10c6gfcf63v51p64jpqvjils63f6qzpnwqz3b0rbliwc1g56n4ns")))

(define-public crate-unit-wasm-0.1 (crate (name "unit-wasm") (vers "0.1.0-beta") (hash "0m6dic29hlm7aspv10ayqln149ljq8dfmm61syhpcr5an7291a19") (links "unit-wasm")))

(define-public crate-unit-wasm-0.1 (crate (name "unit-wasm") (vers "0.1.0") (deps (list (crate-dep (name "unit-wasm-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ps5fw192hjq5rwnssxsm61dn9bv546m5cws1pfjpz21zkmji8bg")))

(define-public crate-unit-wasm-0.1 (crate (name "unit-wasm") (vers "0.1.1") (deps (list (crate-dep (name "unit-wasm-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0dx0z2a6b4lcxxp0nr6czf45vwdkwp0vc73m9adaidwaw088lvdh")))

(define-public crate-unit-wasm-0.1 (crate (name "unit-wasm") (vers "0.1.2") (deps (list (crate-dep (name "unit-wasm-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1vims2hq7v3n5r5a9dpczzdrbxhkwr062ggz9002cl737indc7xb")))

(define-public crate-unit-wasm-0.2 (crate (name "unit-wasm") (vers "0.2.0") (deps (list (crate-dep (name "unit-wasm-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0w9ms96hzgyf9xgjfj4nmff556pn3c8lbfjfb3rrnhkm4g31x6n8")))

(define-public crate-unit-wasm-0.3 (crate (name "unit-wasm") (vers "0.3.0") (deps (list (crate-dep (name "unit-wasm-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0mhr0x1mqhsqs9dpvmiac8x5dam9gyxy4nlipl76g2ggmgfxq938")))

(define-public crate-unit-wasm-sys-0.1 (crate (name "unit-wasm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.43") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1j76zp6z2xsx0bnvw5ja1phy5076vqyv58nm3b6k83a3l86fiyq7") (yanked #t) (links "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1 (crate (name "unit-wasm-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "0hmshh4akjvp5i0v87j26dv864zg71x9bn0p5q60phd5yfshbilf") (links "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1 (crate (name "unit-wasm-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "1ca9ihf9mpqv50yjw5crnzp91ixsna8ify4acaf3m3v8cfdibc86") (links "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1 (crate (name "unit-wasm-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "1chsnvj5mv742rhmwm3gx2q41rllszcdxm3xgfsxxzk1mr7vinyy") (links "unit-wasm")))

(define-public crate-unit-wasm-sys-0.1 (crate (name "unit-wasm-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "0ryhcr5xl6f4257bcbfr8mas7bsxxyy7vl27x7lwvdn4swrhgc6p") (links "unit-wasm")))

(define-public crate-unit-wasm-sys-0.2 (crate (name "unit-wasm-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "17141gf50js0is666lqh6lzh21vxybfa8wqyv00f7y6ln2hn925r") (links "unit-wasm")))

(define-public crate-unit-wasm-sys-0.3 (crate (name "unit-wasm-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "177v8j5xnzksfaw7jf2rml70p8z9l7p9c67kd7nzk7y8wkk5v71s") (links "unit-wasm")))

(define-public crate-unit10-0.1 (crate (name "unit10") (vers "0.1.0") (hash "093v4rqafihgf90060bnf584wydmh25kc7whnsfgyg9p8y4qym6v")))

(define-public crate-unit12-0.1 (crate (name "unit12") (vers "0.1.0") (hash "1h4vc902d37sbbf1s2cpxk31am15ws3ylp50plw4ppsbin31ljpj")))

(define-public crate-unit13-0.1 (crate (name "unit13") (vers "0.1.0") (hash "1456ggas5nm647qbxsrkqllkzxwv4c1fknphrrl4asp6i5an1m4k")))

(define-public crate-unit14-my-first-crate-0.1 (crate (name "unit14-my-first-crate") (vers "0.1.0") (hash "1d7xfk4r94sa1s986pcwl6840vxgjacp9838kgzlawcgvi02yh2m")))

(define-public crate-unit15-0.1 (crate (name "unit15") (vers "0.1.0") (hash "1pmxpa850d3zwfs1ybfvgpn5d17k611d487ns1h793qyr1vqqw48")))

(define-public crate-unit16-0.1 (crate (name "unit16") (vers "0.1.0") (hash "1h04cv1w3alki2fbxn49a7bsqhliwwmw4i7czlqapnmqanx4cdlz")))

(define-public crate-unit17-0.1 (crate (name "unit17") (vers "0.1.0") (hash "1baa2kvd4dhvqlgnjw2j807wgl3x2pkmjhfa6857dixv3qvszac8")))

(define-public crate-unit18-0.1 (crate (name "unit18") (vers "0.1.0") (hash "0by2yhdmqiy0lvf8wicz9hs3dlm1h1dj5ql642fmw13jn4cb0m5f")))

(define-public crate-unit19-0.1 (crate (name "unit19") (vers "0.1.0") (hash "1spsanc4yaqvxwjxfkdzfymsw95w0dahadhsrnwg30k3gjqb3mqb")))

(define-public crate-unit20-0.1 (crate (name "unit20") (vers "0.1.0") (hash "0x3xs844xjk1lkpllr6drcgj6kb27zy1097vyiiz1yncjsgc60gc")))

(define-public crate-unit4-0.1 (crate (name "unit4") (vers "0.1.0") (hash "18hmppzzg1c949z3qkyn2nsss14ah41wfya1lixkfnv4756w8p3d")))

(define-public crate-unit6-0.1 (crate (name "unit6") (vers "0.1.0") (hash "1337r0ib2krpk9dl62p4rf16yhcb7q0adlig9r9d2jfil2w68mjm")))

(define-public crate-unit7-0.1 (crate (name "unit7") (vers "0.1.0") (hash "1rikbrhhbmgc7wc4ahkix320m6sxihpj7msffh3b4sh2dx7pnml8")))

(define-public crate-unit8-0.1 (crate (name "unit8") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "18i2qas3f9i68ma6bh945f8490qkhbpdk3s91qcyq7az7bw84biz")))

(define-public crate-unit9-0.1 (crate (name "unit9") (vers "0.1.0") (hash "0n111qk7w234llkczm42hhc9br61m2qvkhbgbzzrsl75kms2a9kv")))

(define-public crate-unit_conversion-0.2 (crate (name "unit_conversion") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1xhgsb0ga0k7ykz6b3nfd93gx92nnv5m3wcm3phjq6dzk7yq00zr")))

(define-public crate-unit_convert-0.1 (crate (name "unit_convert") (vers "0.1.0") (hash "0qsqmaq0nc35pddxmv18czhrf65zwngbc32wg3f1v9df1zb680cw")))

(define-public crate-unit_converter-0.1 (crate (name "unit_converter") (vers "0.1.0") (deps (list (crate-dep (name "read_input") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0hnsgv9yx5czvbj7vkzxv2g01p7zcffigniq1kpbsh0dlmpqqmb6")))

(define-public crate-unit_converter-0.1 (crate (name "unit_converter") (vers "0.1.1") (deps (list (crate-dep (name "read_input") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "14xdmvdcf92ndsdqma1r7cz06wrn2hyvpbkxqyrrw8ijny93p28x")))

(define-public crate-unit_converter-0.1 (crate (name "unit_converter") (vers "0.1.2") (deps (list (crate-dep (name "read_input") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "006hxyq6f2r9szizmw7j6sgcng6413vrc4n9jjf5pd1b6r5gfy77")))

(define-public crate-unit_converter-0.1 (crate (name "unit_converter") (vers "0.1.3") (deps (list (crate-dep (name "read_input") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0wjhj1cn3x8k9zkkh721syvf8pigss6fmb9ks86mz5p389x5dd7s")))

(define-public crate-unit_converter-0.1 (crate (name "unit_converter") (vers "0.1.4") (deps (list (crate-dep (name "read_input") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "095m5y063asw87bqs71krcjb85va6r4i7w4a0jlz18vcmsc07xf5")))

(define-public crate-unit_converter-0.1 (crate (name "unit_converter") (vers "0.1.5") (deps (list (crate-dep (name "read_input") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "05jpgdvzq7m152qzvmnbj1xa5dv3naq5qmdq98j949vi46gxkwrr")))

(define-public crate-unit_converter-0.1 (crate (name "unit_converter") (vers "0.1.6") (deps (list (crate-dep (name "read_input") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "11mz01lmfpsl64dj0d5qy64035vpy054pfl03dacf3brmk9jkbl6")))

(define-public crate-unit_converter_lib-0.1 (crate (name "unit_converter_lib") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1mzy3vagqqws4rly6b69fc1iarv7rnd58diaxyj5s09j00c05zjx")))

(define-public crate-unit_system-0.1 (crate (name "unit_system") (vers "0.1.0") (deps (list (crate-dep (name "unit_system_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1f9kljzq8y1l6khw1zqjgsdp9ihixrhwsyqvsgy4gnd79w4b0aai")))

(define-public crate-unit_system_derive-0.1 (crate (name "unit_system_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kzzsx4y990p1bbb8y13vxxmyhgisyf8iqmk2xvc2frn8fj21rzf")))

(define-public crate-unit_test_utils-0.1 (crate (name "unit_test_utils") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0j3ssrg1g0ygrdg8lbv2li37yyvwqaznqcj581xdi962zcnxslx0")))

(define-public crate-unit_test_utils-0.1 (crate (name "unit_test_utils") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "13z00rrgjjydfjmrdjvyafakvd9qss139rbkf5j1bmyq7hg7r5zi")))

(define-public crate-unit_test_utils-0.1 (crate (name "unit_test_utils") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09q6qnn20ncn3bnc9wjf03p33aiv6fr665ggrk31cvv63jwvdj2p")))

(define-public crate-unit_test_utils-0.1 (crate (name "unit_test_utils") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "00bcgj061n8bdf23g8mhwd2q265dgsh6z8lpb01sykfhmgr7dvhb")))

(define-public crate-unita-0.1 (crate (name "unita") (vers "0.1.0") (hash "1vqap04ps225rw3z4xrjdg61kcmb6rwywnvx4bjs6wf048w32aax")))

(define-public crate-unitage-0.0.1 (crate (name "unitage") (vers "0.0.1") (deps (list (crate-dep (name "const-frac") (req "^0.0") (features (quote ("std" "ratio"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "physical-quantity") (req "^0.0") (features (quote ("std" "full"))) (default-features #t) (kind 0)) (crate-dep (name "real-proc") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (features (quote ("no_std"))) (kind 0)) (crate-dep (name "unit-proc") (req "^0.0") (default-features #t) (kind 0)))) (hash "0nwgi77ngz4pv0p4hm65b2fjrrk1jnm0npbrlvda6sybsnxdxcy5")))

(define-public crate-unitage-0.0.2 (crate (name "unitage") (vers "0.0.2") (deps (list (crate-dep (name "const-frac") (req "^0.0") (features (quote ("std" "ratio"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "physical-quantity") (req "^0.0") (features (quote ("std" "full"))) (default-features #t) (kind 0)) (crate-dep (name "real-proc") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (features (quote ("no_std"))) (kind 0)) (crate-dep (name "unit-proc") (req "^0.0") (default-features #t) (kind 0)))) (hash "0y0xz4yjr9w5r7zplnxm6yipp16k6zp12rs7fzqa52ccydfvy7v2")))

(define-public crate-unitary-0.0.1 (crate (name "unitary") (vers "0.0.1") (hash "0wk3z03djyv1zqrsdqlfmj86dgp7x12xfqm6x5lnmlayl2g9n9db")))

(define-public crate-unitary-0.0.2 (crate (name "unitary") (vers "0.0.2") (hash "0qky11rq1m8x5fml1ws646sbnarr19c6m91x0x0y66rywlrqn269")))

(define-public crate-unitdc-0.1 (crate (name "unitdc") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0hv30zs91r25klrfs0n13xsf3ggsjwcg8k81w4pvxmlw594kq559")))

(define-public crate-unite-0.1 (crate (name "unite") (vers "0.1.2") (deps (list (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c6ivkhldxn16k2mm3m1lnyg6lrc2s9ald56mrxvs04w196dyfhd")))

(define-public crate-unitedservices_common-1 (crate (name "unitedservices_common") (vers "1.20230703.225444") (deps (list (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.3.1") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "bevy_reflect") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "evalexpr") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "inventory") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "load_image") (req "^3.0.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "once_map") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "ravif") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.29.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.6.3") (features (quote ("runtime-tokio-native-tls" "mysql" "json" "chrono" "decimal" "macros" "offline" "time" "sqlx-macros"))) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0s4qkvyyxpxna7qv5dvdx781pisq8x7ilhfa36jh5si4h3grwg33") (rust-version "1.70")))

(define-public crate-unitig_flipper-0.1 (crate (name "unitig_flipper") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "jseqio") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1awq1px05n8s2q62m2kgglz1967q1b80qgpjmjbrqb3zxrv9lr3g")))

(define-public crate-unitore-0.1 (crate (name "unitore") (vers "0.1.0") (deps (list (crate-dep (name "feed-rs") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "http-body-util") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "humantime-serde") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^1.1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hyper-util") (req "^0.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.5.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "rt-multi-thread" "io-std" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "0y55cy3hbxjkm6vmj7l8gmr6n9dnlvjbqd46k7wfpqfy4mx58baj") (features (quote (("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-unitrust-0.1 (crate (name "unitrust") (vers "0.1.0") (hash "1srd6jkwyqvmjhfabm18n2rywdd8k5ml0d89kjk2prhp96g7rpsw")))

(define-public crate-units-0.0.1 (crate (name "units") (vers "0.0.1") (deps (list (crate-dep (name "tylar") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0k1cbii98l32yn929wv4ba2nb8nyc062vrmh5s9mpjikr2rlpw1r") (features (quote (("unstable"))))))

(define-public crate-units-0.0.2 (crate (name "units") (vers "0.0.2") (deps (list (crate-dep (name "tylar") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0wgmqrrybq4ybksidnxyxb0c5s7dwhysn2wnzcpharjw70jhkrm6") (features (quote (("unstable"))))))

(define-public crate-units-0.1 (crate (name "units") (vers "0.1.0") (deps (list (crate-dep (name "tylar") (req "0.2.*") (default-features #t) (kind 0)))) (hash "18v6j9p63bhddn00lx0jzmwzadzg9bj15m2ah12fwl2sav8wmrms") (features (quote (("unstable"))))))

(define-public crate-units-relation-0.0.0 (crate (name "units-relation") (vers "0.0.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rs-measures") (req "^0.0.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0q24378xb83awcyknsb731kc49gf7yzsxdz12i6d6d1l8lq59xs5")))

(define-public crate-units-relation-0.1 (crate (name "units-relation") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rs-measures") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0l507f7pd01z1kz8p9bz1gvvwxvm4d5awxisgpgfm662qrp3plf6")))

(define-public crate-units-relation-0.3 (crate (name "units-relation") (vers "0.3.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rs-measures") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "17axg52a214mlwld2arswr5pw2j7xn8qs4xh3hy0qm49rxcfiila")))

(define-public crate-units-relation-0.4 (crate (name "units-relation") (vers "0.4.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rs-measures") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1fj12ghbcvf54v1rgjpvmv0055pn3bn2gjgv7ywfvm4pvjwjz84n")))

(define-public crate-unittest-0.1 (crate (name "unittest") (vers "0.1.0") (hash "015gcylpqhjf31zpwdk4qlasb7lbh0cy437rils45cd2pdanj8bi")))

(define-public crate-unitval-0.1 (crate (name "unitval") (vers "0.1.0") (deps (list (crate-dep (name "unitval-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1npnvfg7mcyzw7gbvmjx4s9c20myj455jhlj04gl54q4ii8sx8g3")))

(define-public crate-unitval-0.1 (crate (name "unitval") (vers "0.1.1") (deps (list (crate-dep (name "unitval-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0myj5swvps7gmpphij179pwrrbc6pzrr7w86scxl2j47645pkdx4") (features (quote (("diesel_postgres" "unitval-derive/diesel_postgres") ("default")))) (yanked #t)))

(define-public crate-unitval-0.1 (crate (name "unitval") (vers "0.1.2") (deps (list (crate-dep (name "unitval-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "15g8lfrp9p9hnfvp2ayqq8yidqm8d5kmiprwrld6kbb0c9awwzkx")))

(define-public crate-unitval-0.1 (crate (name "unitval") (vers "0.1.3") (deps (list (crate-dep (name "unitval-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fans6598h947rs2fw42kw837i7yvn0aimp62yn7pj004c80i9l8")))

(define-public crate-unitval-0.2 (crate (name "unitval") (vers "0.2.0") (deps (list (crate-dep (name "unitval-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "15pxxm0xzva7bhqkm47nqnc0d16j02cd9jhhw9p2abgvmrnnxz8n")))

(define-public crate-unitval-derive-0.1 (crate (name "unitval-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0izz9db1wy4gsdpqhfrgbp30x9xvfg6s7yzbg3fc9z79nn41ryv7")))

(define-public crate-unitval-derive-0.1 (crate (name "unitval-derive") (vers "0.1.1") (deps (list (crate-dep (name "diesel") (req "^1.4.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0wpkakj4gd3mmzvz318s0znv8nbjla5vjz8p9wg32ymm9bk1f5hl") (features (quote (("diesel_postgres" "diesel/postgres") ("default")))) (yanked #t)))

(define-public crate-unitval-derive-0.1 (crate (name "unitval-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0d4hlyfmb6vs0bx09qdyw8qvx6anmkm0mk6wzgnvidb0i7y4849z")))

(define-public crate-unitval-derive-0.2 (crate (name "unitval-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0vkyjnb5nyvvfc76zrwf0mvhz8xpy776qfgv2l850b748hzsyd0g")))

(define-public crate-unity-0.1 (crate (name "unity") (vers "0.1.0") (hash "00yidc3xr15cgwwiidxhx5bfyfhhn3jwx5hbis83nj6cira5sk2b")))

(define-public crate-unity-0.2 (crate (name "unity") (vers "0.2.0") (deps (list (crate-dep (name "kondo-lib") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0dwflxgfgwgq52wjs8firryf4dzhd28c71jx791in803jc708ln8")))

(define-public crate-unity-native-plugin-0.1 (crate (name "unity-native-plugin") (vers "0.1.1") (deps (list (crate-dep (name "unity-native-plugin-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0p9zm3fgy3i6z1zl0lbwyx1kanb5k8zy1il9dh0s67ga4icg3r19") (features (quote (("default") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.2 (crate (name "unity-native-plugin") (vers "0.2.0") (deps (list (crate-dep (name "unity-native-plugin-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1wrnzilvsqc0h1h7s22qpwynxf0pmy62pm1vsa34ry6lknkiswwj") (features (quote (("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.3 (crate (name "unity-native-plugin") (vers "0.3.0") (deps (list (crate-dep (name "unity-native-plugin-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xakqd8lkd6k5g25pnzbv59l5kii2glmcvyr2v7n9cldf4wxa3qc") (features (quote (("vulkan") ("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.4 (crate (name "unity-native-plugin") (vers "0.4.0") (deps (list (crate-dep (name "unity-native-plugin-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1nxpajccm2rcqycp359hj8s0wqpa4ddibcpg6nq2k4rqmsbkzw7b") (features (quote (("profiler") ("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.4 (crate (name "unity-native-plugin") (vers "0.4.1") (deps (list (crate-dep (name "unity-native-plugin-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "07gpcsp7wmzxs1qy6y3fxj95nvw6prcsad4s2l8q42sd5rpjl0zf") (features (quote (("profiler") ("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-0.5 (crate (name "unity-native-plugin") (vers "0.5.0") (deps (list (crate-dep (name "unity-native-plugin-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "082njfpsa4w117gqp56dq23b08nhi3r23mxz46nggnp11wcb19nf") (features (quote (("profiler_callbacks" "profiler") ("profiler") ("default") ("d3d12") ("d3d11"))))))

(define-public crate-unity-native-plugin-sys-0.1 (crate (name "unity-native-plugin-sys") (vers "0.1.0") (hash "060f18y132x55dmi3vjk1rvpakb88aks9rv9ydz5n9dhdam64p8y")))

(define-public crate-unity-native-plugin-sys-0.1 (crate (name "unity-native-plugin-sys") (vers "0.1.1") (hash "07dczdrwz4mn3xbghgk7wll8dyzlwlkmw1vvpmp8mz16scqf8rrb")))

(define-public crate-unity-native-plugin-sys-0.3 (crate (name "unity-native-plugin-sys") (vers "0.3.0") (hash "08qs8pnrb5x5idwqwf5g3r1zf5yy6h0vk9h4r9fqh07q5qhvnmyf")))

(define-public crate-unity-native-plugin-sys-0.4 (crate (name "unity-native-plugin-sys") (vers "0.4.0") (hash "198n0dl9rzw8na0ii7sn71d5264xx027r83rns7j6qx5cpjnfk86")))

(define-public crate-unity-native-plugin-sys-0.5 (crate (name "unity-native-plugin-sys") (vers "0.5.0") (hash "1rvpigxcsgffqxkj49dv2gzakdf1292ab0s60wdm33g8l8h9gdh3")))

(define-public crate-unity-native-plugin-vulkan-0.3 (crate (name "unity-native-plugin-vulkan") (vers "0.3.0") (deps (list (crate-dep (name "ash") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1s8zd0b51168w6v0aackr74v8xfm0n9xysjj3rjfw1647k64n1w3")))

(define-public crate-unity-native-plugin-vulkan-0.4 (crate (name "unity-native-plugin-vulkan") (vers "0.4.0") (deps (list (crate-dep (name "ash") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1hj51fysz0c21wr8n10hcigj41hcf0fijbq0x0vagly5a24vck5n")))

(define-public crate-unity-native-plugin-vulkan-0.4 (crate (name "unity-native-plugin-vulkan") (vers "0.4.1") (deps (list (crate-dep (name "ash") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0kkiadzirlr7aw4wn3khnshh4b1ni50s50vrwh9i0l5344x7wdbf")))

(define-public crate-unity-native-plugin-vulkan-0.5 (crate (name "unity-native-plugin-vulkan") (vers "0.5.0") (deps (list (crate-dep (name "ash") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1r0kavjcqaqhbhvcdlzicxkgsghymrcr22m7wkqnr23fyl4s5rl7")))

(define-public crate-unity-native-plugin-vulkan-0.5 (crate (name "unity-native-plugin-vulkan") (vers "0.5.1") (deps (list (crate-dep (name "ash") (req "^0.33.1") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "unity-native-plugin-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1y5nmxx71d950rm4qbax16y66ihfjpdgmvkaxghbqj7pxsa8idp3")))

(define-public crate-unity-tools-0.0.0 (crate (name "unity-tools") (vers "0.0.0") (deps (list (crate-dep (name "gen-iter") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "unity-utils") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0jp6mfgaxi2hbxgi0gd6qk4ff92fpmvbri0vm2bmayjd53jy7a8z") (features (quote (("default"))))))

(define-public crate-unity-unpacker-0.1 (crate (name "unity-unpacker") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0") (default-features #t) (kind 0)))) (hash "1fcgx8s6l8siaj5h2gd5y2zlrsrdx41mfam70ycfk1xryb8y9wl1")))

(define-public crate-unity-unpacker-0.1 (crate (name "unity-unpacker") (vers "0.1.1") (deps (list (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0") (default-features #t) (kind 0)))) (hash "1c8a612yn9hvyzhahzqvz38g24v2ww3kf8nckd8w3r7j5fvadyl7")))

(define-public crate-unity-utage-book-0.1 (crate (name "unity-utage-book") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0xiphw0mhwvqdffd460y97mc0xqk2aww82gngcq975agn8dsiq36") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-unity-utils-0.0.0 (crate (name "unity-utils") (vers "0.0.0") (deps (list (crate-dep (name "fast-walker") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "gen-iter") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1g6qs1l467y4lf6c03z8lmjsqn0n8hfgczfnj7kg2pmpnh6m7zp7") (features (quote (("default"))))))

(define-public crate-unity-yaml-rust-0.1 (crate (name "unity-yaml-rust") (vers "0.1.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0sp3vjllnn63vjyyvvn03kyfxqcp9bav404km9dv9hy65f2azswz")))

(define-public crate-unity-yaml-rust-0.1 (crate (name "unity-yaml-rust") (vers "0.1.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "177mw5maasifk2xapc7yb0brzwz9dyj8jf218x04havbnp4lspyv")))

(define-public crate-unity_native-0.1 (crate (name "unity_native") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "unity_native_proc_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unity_native_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0w5awz17pfq84cp8i2xvxzjfnb812whfh8pwb6c5vc6gv142g83j")))

(define-public crate-unity_native-0.1 (crate (name "unity_native") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "unity_native_proc_macro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unity_native_sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "02ls57wq18hpy97wm8qn7zsjy0zi39j7y9ivgbsxdhh6i4wa0wk1") (features (quote (("profiler") ("default" "log" "profiler")))) (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-unity_native_proc_macro-0.1 (crate (name "unity_native_proc_macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.66") (default-features #t) (kind 0)) (crate-dep (name "unity_native_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1maq40d03j21f7y94s7q58p9nbmhbqpy6fpcy1r9rhwccl5inpd3")))

(define-public crate-unity_native_proc_macro-0.1 (crate (name "unity_native_proc_macro") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.66") (default-features #t) (kind 0)) (crate-dep (name "unity_native_sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cxnh8vckdgahqm73f2dcd0q6fi2vq8dv9z6f3l1wgbqi5kibzf5")))

(define-public crate-unity_native_sys-0.1 (crate (name "unity_native_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "0l96z5g756cgdrifhyambni6jdikjcc168a619ykvjldsa42dzw8")))

(define-public crate-unity_native_sys-0.1 (crate (name "unity_native_sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "0baij5shwsyb95qa215j0038alls5q2whs7j8jp1wbbnwyyl5aiv")))

(define-public crate-unity_rs-0.1 (crate (name "unity_rs") (vers "0.1.0") (deps (list (crate-dep (name "dashmap") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lzma-rs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "04yr9cp3gqvid4rl6w6dinzyyxj2fp2hd9gva7gkczhrplv4kd8c")))

