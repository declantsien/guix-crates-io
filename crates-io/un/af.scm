(define-module (crates-io un af) #:use-module (crates-io))

(define-public crate-unaf-0.0.0 (crate (name "unaf") (vers "0.0.0") (hash "1kjickfqdkbzngy67wpvwbn0i55gcr5ijrmpcx9fcnw1g4pnjz9h")))

(define-public crate-unaf-cli-0.0.0 (crate (name "unaf-cli") (vers "0.0.0") (hash "12krn8zvr57xycq39q6g109l0735nwqp7l4ni0a5z2sj625vpjla")))

(define-public crate-unaf-svr-0.0.0 (crate (name "unaf-svr") (vers "0.0.0") (hash "13vccdmi1fz9c2vqzqscl2rwfwisz5i3wq56s707jk8kgj7p9n3c")))

(define-public crate-unaf-uis-0.0.0 (crate (name "unaf-uis") (vers "0.0.0") (hash "0rfh7vl5ig921himyg1xvx659y8kbj9fh04c83dxj0ybrg4myrhq")))

