(define-module (crates-io un em) #:use-module (crates-io))

(define-public crate-unempty-0.1 (crate (name "unempty") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1l8qn7bpyknrah17dvxsmjqhzms7d56vzllcs6k4kady13i6g5ch") (features (quote (("std") ("default" "std")))) (rust-version "1.56.0")))

