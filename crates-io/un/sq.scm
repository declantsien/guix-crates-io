(define-module (crates-io un sq) #:use-module (crates-io))

(define-public crate-unsquashfs-wrapper-0.1 (crate (name "unsquashfs-wrapper") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "14ydcqn7iz6h1hfgmb3sy72hwav4ypadb01px6icv8n8qms8b9jk")))

(define-public crate-unsquashfs-wrapper-0.1 (crate (name "unsquashfs-wrapper") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "1q7kbfn76k3rbw3rr5pg9cby4jb12hk1kd4nxzkmgskiv55v6724")))

(define-public crate-unsquashfs-wrapper-0.2 (crate (name "unsquashfs-wrapper") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rustix") (req "^0.38") (features (quote ("process"))) (default-features #t) (kind 0)) (crate-dep (name "which") (req "^6.0") (default-features #t) (kind 0)))) (hash "1ffr4nggipmfaz4xlizhr7g9n49n8m1j9rmyxzk7davfag7jpiy0")))

(define-public crate-unsquashfs-wrapper-0.2 (crate (name "unsquashfs-wrapper") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)) (crate-dep (name "rustix") (req "^0.38") (features (quote ("process"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^6.0") (default-features #t) (kind 0)))) (hash "03d0pzrim0jhck0x99zcjmggfych0h6cw98cdpfgbrzhxzzgbhjw")))

