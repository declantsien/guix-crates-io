(define-module (crates-io un am) #:use-module (crates-io))

(define-public crate-uname-0.1 (crate (name "uname") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bb7prg6qligb8xjhxxswajlc3m1f1s1y8v0m57j04rklwv074lw")))

(define-public crate-uname-0.1 (crate (name "uname") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j1xd1rryml4j1hf07kahva9d5ym8m9jz9z20hfdpr1jrbq8jbxp")))

(define-public crate-uname-rs-0.1 (crate (name "uname-rs") (vers "0.1.0") (hash "1j2zyda9w64qzd5v83mn5agfhsjhmbh3x91d2am5iz8m6vx1pn7n")))

(define-public crate-uname-rs-0.1 (crate (name "uname-rs") (vers "0.1.1") (hash "01kr07x0c8r9q7gk6py1bly05hn08i089dkxsmqbkkhqjcjxp634")))

