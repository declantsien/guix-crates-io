(define-module (crates-io un zi) #:use-module (crates-io))

(define-public crate-unzip-0.1 (crate (name "unzip") (vers "0.1.0") (deps (list (crate-dep (name "temporary") (req "^0.6.3") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 0)) (crate-dep (name "transformation-pipeline") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1z9x45y9ab7mf2f9h4zqkfq0jjv5x4wvm8fmxih4nnz9m8mrmrch")))

(define-public crate-unzip-array-of-tuple-0.1 (crate (name "unzip-array-of-tuple") (vers "0.1.0") (hash "186y5addbjx4ls3j4j33jw90fhni35yhipyqawhyygpya00h1mzk")))

(define-public crate-unzip-n-0.1 (crate (name "unzip-n") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1mf28jicpjdavhz02w0v29r5q61zzgiksn8d5iw23jibn7fgn0gw")))

(define-public crate-unzip-n-0.1 (crate (name "unzip-n") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1mgk6gx2v2h1r8h8rshi24lc5s7fasl9pclhyvmlx3bljh24gyjl")))

(define-public crate-unzip-n-0.1 (crate (name "unzip-n") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "173bi6p3vypprk0b07vpj7zcc5n5qimy3460587pyi4n0mdfiry2")))

(define-public crate-unzip-rs-0.1 (crate (name "unzip-rs") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y37375zm5yxc34bkmng9p2dyj8j7a7y6b5y3hixzcr3agrnmywf") (yanked #t)))

(define-public crate-unzip-rs-0.1 (crate (name "unzip-rs") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d0sc9xf0cla0ay53vbwxfzkxdk18ijhjvdgx2z2plzrrhyp1rfc") (yanked #t)))

(define-public crate-unzip-rs-0.1 (crate (name "unzip-rs") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hrinf7vpbdm2l7dv9ns9fg2bv972kp2q88hhrkynlh1qmb1bflf") (yanked #t)))

(define-public crate-unzip-rs-0.1 (crate (name "unzip-rs") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0azj2b5hr35vbhfxix3aa1516dyfsj23jikiwilg0wrjh1q4ariq") (yanked #t)))

(define-public crate-unzip-rs-0.2 (crate (name "unzip-rs") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0c2vh2fs2s0jy8gpih26jx2sasj03s8bkx6gqppgikmy9r0ma122")))

(define-public crate-unzip3-0.1 (crate (name "unzip3") (vers "0.1.0") (hash "1q3cnyixlvf8kj8av2k9y3cnigaxd1kqd9ih2zjlrlk0mf5vnv2g") (yanked #t)))

(define-public crate-unzip3-0.1 (crate (name "unzip3") (vers "0.1.1") (hash "19pscnph9d0g6lx00l8pzgd7jxjr1qvryrk8ppnf9659hk7hv6rj") (yanked #t)))

(define-public crate-unzip3-1 (crate (name "unzip3") (vers "1.0.0") (hash "1mhsjdpdsc24phgmqxh9nbjyrj55jhpynbq3di3h30mhd8qyrh4r")))

