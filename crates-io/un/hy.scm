(define-module (crates-io un hy) #:use-module (crates-io))

(define-public crate-unhygienic-0.1 (crate (name "unhygienic") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "unhygienic-impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xahsqv4c03m7sjx02jlgsgi22ahpy2c2rca483y4dblqd6z791n")))

(define-public crate-unhygienic-0.1 (crate (name "unhygienic") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "unhygienic-impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14ryxxfg8pdlzk1z4vlhanp4anjr3c74andnf73xn12cly5swn1h")))

(define-public crate-unhygienic-impl-0.1 (crate (name "unhygienic-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dagv9axpz6w1iwnipqhw436yyw5ax5i48lr86cfbdyq4kq980x4")))

(define-public crate-unhygienic2-0.1 (crate (name "unhygienic2") (vers "0.1.0") (hash "1m8wlrk3q4k39p7mz3d4vapmsa70799in8dwrjs3vkvn6pkhx8q2")))

