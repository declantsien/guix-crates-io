(define-module (crates-io un ty) #:use-module (crates-io))

(define-public crate-unty-0.0.0 (crate (name "unty") (vers "0.0.0") (hash "1f9ns4lf2lk82l9xqbbqlg1l4r81v38kav3yk4mwa54j4vgq6237") (yanked #t)))

(define-public crate-unty-0.0.1 (crate (name "unty") (vers "0.0.1") (hash "0xqjfkv4fdvdc1k118279crrcwbm7fwkyl9dj0va2rsfkf0lax3j") (yanked #t)))

(define-public crate-unty-0.0.2 (crate (name "unty") (vers "0.0.2") (hash "05ramx2zwhzagw4ydn6dj368wk72z6dbkkk2qvsh13ijlaix5wqs") (yanked #t)))

(define-public crate-unty-0.0.3 (crate (name "unty") (vers "0.0.3") (hash "0af1nqvg57vqlldhbdrqsnrfjg57kkna724m4ly5asbq11187a51") (rust-version "1.56.1")))

(define-public crate-untyped-arena-0.1 (crate (name "untyped-arena") (vers "0.1.0") (hash "13ik6f2213lj13pbgn189m2axll1ya9fv13pfy8w5fiz1hqwqmcy")))

(define-public crate-untyped-arena-0.1 (crate (name "untyped-arena") (vers "0.1.1") (hash "0k5v294av3is10hn01zyxjhpmifmi1xp7brqvmxhyz2801xrhp2m")))

(define-public crate-untyped-bytes-0.1 (crate (name "untyped-bytes") (vers "0.1.0") (hash "0c5n9cnvnlwdxfg4c42xkyrj26wz1rnj2fb5ksxrjrjkab8v2pdl")))

(define-public crate-untyped-bytes-0.1 (crate (name "untyped-bytes") (vers "0.1.1") (hash "0g8fnwnscc0y7f0hbjjpvxvnj1xkhsa9v2i6g7k0ddxhlhby2nkh")))

(define-public crate-untyped-bytes-0.2 (crate (name "untyped-bytes") (vers "0.2.0") (hash "1br2zyy9mkjb6badmawcg147b21r25w1waxl0g7vq6hs5v0a1hs8")))

(define-public crate-untyped_vec-0.1 (crate (name "untyped_vec") (vers "0.1.0") (hash "1mc5gmndf6rgr6m0dd0w7m26gxzfwxy90f5p1cl5fn6kwfbnmhi5")))

(define-public crate-untyped_vec-0.1 (crate (name "untyped_vec") (vers "0.1.1") (hash "0fpyccn068v6cqq537zwd1vn70zmlz7i24ilwwhmb78fc048v9jh")))

