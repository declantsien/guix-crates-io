(define-module (crates-io un ce) #:use-module (crates-io))

(define-public crate-uncertain-0.1 (crate (name "uncertain") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1kr66hnacki7qzgblmql2sfmjj1jd6z2nrhp3mmdfya0zqykb3lx")))

(define-public crate-uncertain-0.2 (crate (name "uncertain") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1m4j4l83n6sngm820dyb5wcmydf8ah22wnyffnsrf36pxp7pyhwj")))

(define-public crate-uncertain-0.2 (crate (name "uncertain") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1qgszvdyaywyinf66hw1kmawggrwh3rm9h1036azhwdq6rhrp0yj")))

(define-public crate-uncertain-0.3 (crate (name "uncertain") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0js3vcjz5hh4nb4p016rvjqvc12f9ibjjj4rvzfric52nkav4n2w")))

(define-public crate-uncertain-0.3 (crate (name "uncertain") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1khr3q6f5ma9hj92g0i9nd5x4ff0d86xhagxi2ywg7i1j86lc40v")))

(define-public crate-uncertainty-0.1 (crate (name "uncertainty") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0fiwnpj1s66fdy0kin3jwhiz5hvyfkv382h92smyz2qgm5f9a4k8")))

(define-public crate-uncertainty-0.1 (crate (name "uncertainty") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kk0d1q14qd81m66k2dml7v652bh0hfag4ybc8lnalig4zxqhnsh")))

(define-public crate-uncertainty-0.2 (crate (name "uncertainty") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0m5d9zc298y3879ck174937syxkd7kd4kc41ipcc4v50v3dq3ga3")))

(define-public crate-uncertainty-0.2 (crate (name "uncertainty") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0gn13xk7zvmwp1klxr7lxy7nfzmggvjsyn00gl77xzbfc1z5yyg5")))

(define-public crate-uncertainty-rs-0.1 (crate (name "uncertainty-rs") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "164a1r9v56j3zaghb8pl58wasdih5fx2ijz66zd6nkyqr9z6zipl") (yanked #t)))

