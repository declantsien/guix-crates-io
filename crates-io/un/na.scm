(define-module (crates-io un na) #:use-module (crates-io))

(define-public crate-unnamed_entity-0.1 (crate (name "unnamed_entity") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "122mj0x3zim83kbhn64df5qlbi2ca84p8jrcg59bqay618grq2vl") (v 2) (features2 (quote (("serde" "dep:serde" "indexmap?/serde" "bitvec?/serde") ("map" "dep:indexmap") ("bitvec" "dep:bitvec"))))))

