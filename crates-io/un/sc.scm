(define-module (crates-io un sc) #:use-module (crates-io))

(define-public crate-unscanny-0.1 (crate (name "unscanny") (vers "0.1.0") (hash "0ivbipc1rnq15fhzgna41p1h01ncq4shycii72f3x5d7czq2mpz9")))

(define-public crate-unscrupulous-0.1 (crate (name "unscrupulous") (vers "0.1.0") (deps (list (crate-dep (name "unscrupulous_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qv2nvyws3dnvml1a6naw27dd6d1irc1iv8xwaz65ms7gx29zqq1") (v 2) (features2 (quote (("derive" "dep:unscrupulous_derive"))))))

(define-public crate-unscrupulous_derive-0.1 (crate (name "unscrupulous_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "04fdhvs8y8riila1k8k40hakpry5q1zb18g0d0vrpjm2i5vf077g")))

