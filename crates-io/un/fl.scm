(define-module (crates-io un fl) #:use-module (crates-io))

(define-public crate-unflappable-0.1 (crate (name "unflappable") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "18l60g8c1x2bzizb8d0dpyy9prfb6czwx1fhkw8v4iibb28zqdkv")))

(define-public crate-unflappable-0.2 (crate (name "unflappable") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "115s445r0xir6ssj6hr9f8l6pfnhai10hj2c844z88n3ykdj8w4x")))

(define-public crate-unflatten-0.1 (crate (name "unflatten") (vers "0.1.0") (hash "1lr9hl59016i08bgx980266nq25mb7r2yrvnmfvnv0cdr48k92yf")))

(define-public crate-unflatten-0.1 (crate (name "unflatten") (vers "0.1.1") (hash "10jcrdhz0rnafgy2wfrjnx0wf148b1f8wc0nihnpk1ixax6glff0")))

(define-public crate-unflatter-0.1 (crate (name "unflatter") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0") (default-features #t) (kind 2)))) (hash "0q68i3nmi6q80wpw7zgin5wbpy555g9g6lyj1lzc13sjmr4xvc8d")))

(define-public crate-unflatter-0.1 (crate (name "unflatter") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0") (default-features #t) (kind 2)))) (hash "0ab3hr24vhxz4jahfi1mzw0ah3jf3m70mxl6finzdncy4v13yaq5")))

(define-public crate-unflow-0.0.1 (crate (name "unflow") (vers "0.0.1") (hash "1x9wvzby2w09bgm5ywvclvlxs6l8glcrpcxrda70lnkg6mkjfkms")))

(define-public crate-unflow-0.1 (crate (name "unflow") (vers "0.1.0") (deps (list (crate-dep (name "antlr-rust") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1n3kbqw3immzkga3avw509l4946f9zyrs4hjhvm0jfghdc8xl9d8")))

(define-public crate-unflow-0.1 (crate (name "unflow") (vers "0.1.1") (deps (list (crate-dep (name "antlr-rust") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lsp-types") (req "^0.81") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "io-std"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.13") (default-features #t) (kind 0)))) (hash "00iwcr5xv6wakimg9v5wfzsspc1rbmh51a3krmp4dlmjxc0i3z2p")))

