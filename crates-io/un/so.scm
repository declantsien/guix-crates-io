(define-module (crates-io un so) #:use-module (crates-io))

(define-public crate-unsolicited-advice-0.1 (crate (name "unsolicited-advice") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "06dz0h1hiksyssdsslqivxxdxacp6y6m96vajrrh6h9r5qxyh4b1")))

(define-public crate-unsound-0.0.0 (crate (name "unsound") (vers "0.0.0-0.0.0-0.0.0") (hash "1z4xl0j6rg8wr5jivichs9xr6l7kj26z4rsnljl3w7da36ysrglk") (yanked #t)))

(define-public crate-unsound-0.0.0 (crate (name "unsound") (vers "0.0.0--") (hash "19307jkax516l6l3n9inq5skdrdfp8ddcjlvnd3qmb7188pph78c") (yanked #t)))

