(define-module (crates-io un ne) #:use-module (crates-io))

(define-public crate-unnest-0.0.6 (crate (name "unnest") (vers "0.0.6") (hash "0pcvmgj8l2ygfimkj0dyh6kgzrg2y6bwwhn3vd8va4j5cvcg1bmc")))

(define-public crate-unnest-0.1 (crate (name "unnest") (vers "0.1.0") (hash "1k5nrqp9da8ch5wz03pnf3lv6jd85x6k41fxbws4amx0xmf98bx3")))

(define-public crate-unnest-0.2 (crate (name "unnest") (vers "0.2.0") (hash "05vjxhajna4x1gl179ciw618bm9qs14vxyn3r2x4sgs1drkhzcvx") (yanked #t)))

(define-public crate-unnest-0.3 (crate (name "unnest") (vers "0.3.0") (hash "0k494g93v7l7sgcg5y2qkdvrriq76ql3fhsyyggmrf1787rdqhvx") (yanked #t)))

(define-public crate-unnest-0.3 (crate (name "unnest") (vers "0.3.1") (hash "1f2wy2jcwqi82byh1l7pqg7jxf8xhvn0f53s96zzwjzyj6bka65r")))

(define-public crate-unnest-ndjson-0.1 (crate (name "unnest-ndjson") (vers "0.1.0") (deps (list (crate-dep (name "iowrap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("preserve_order"))) (default-features #t) (kind 2)))) (hash "1gn6wlz7wyr87indlv3d69n7k7wp4q09jmp7pfrbb08b34835l6d")))

(define-public crate-unnest-ndjson-0.1 (crate (name "unnest-ndjson") (vers "0.1.1") (deps (list (crate-dep (name "iowrap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (features (quote ("preserve_order"))) (default-features #t) (kind 2)))) (hash "15k7az6k7q5jr7qwnsg7x8zv09gz8f37qycxa9n5niqi9238n4g1")))

