(define-module (crates-io un cr) #:use-module (crates-io))

(define-public crate-uncrackable-0.1 (crate (name "uncrackable") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19mg7pibyn0pbxdlyx4ghzxwln893989siyjrs1k3rppk8b0kprv") (yanked #t)))

(define-public crate-uncrx-rs-0.1 (crate (name "uncrx-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "11lx4qbc6l7bwp8z8j7njv15g0ilgpynvnsdf96kxfzz3gayn6m0")))

(define-public crate-uncrx-rs-0.1 (crate (name "uncrx-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0m7m8v8rk2yz86mc1qz04l692s9n5izcx7da1qyzi7mii48h9zkz")))

(define-public crate-uncrx-rs-0.1 (crate (name "uncrx-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "116iac67ziy5j67w4srzflslywmiah53lcks3s4pisfac7r44fmd")))

(define-public crate-uncrx-rs-0.2 (crate (name "uncrx-rs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "087j3x3j3jpbskhvfdj7l2vss552ygh1gxls5676l11xsqb2a6ic")))

(define-public crate-uncrx-rs-0.2 (crate (name "uncrx-rs") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0r0sb71ysc0gpsw1gg95x2z48vfvpcp6l4bdzvm9289jzvbxr3b9")))

