(define-module (crates-io un wr) #:use-module (crates-io))

(define-public crate-unwrap-1 (crate (name "unwrap") (vers "1.0.0") (hash "1c0mma8ih13kp09h8zvniy2cr4l48i4956755zmc22bi75bzc2yp")))

(define-public crate-unwrap-0.2 (crate (name "unwrap") (vers "0.2.0") (hash "1fs30rzgvppfmxqi8i6y3iclf60awd493g7r6n0vv5lf2nin155s")))

(define-public crate-unwrap-1 (crate (name "unwrap") (vers "1.1.0") (hash "0wm4zzm684d5d1lnjvvzzwazlzq7zs7j76x36842zb398bsn34wr")))

(define-public crate-unwrap-1 (crate (name "unwrap") (vers "1.2.0") (hash "15cjb34cj85hgc4hkyjf70snxiqwqgqala9krby8qqsrvjk7apj6")))

(define-public crate-unwrap-1 (crate (name "unwrap") (vers "1.2.1") (hash "03y24m63l85ng23z19bg7vwn6g1h1asg6ldyqwifca23sy6n8cvy")))

(define-public crate-unwrap-enum-0.0.2 (crate (name "unwrap-enum") (vers "0.0.2") (deps (list (crate-dep (name "unwrap-enum-proc-macro") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "00qsdkyp0hkazhyam2x40gx0ihaci4i1dqdw1bjydpyfn9cii4d2")))

(define-public crate-unwrap-enum-proc-macro-0.0.2 (crate (name "unwrap-enum-proc-macro") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quasiquote") (req "=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0nghcxsn533xh28ri84hfd7s6z64v1b679m5fi58jxds8hhxa4rf")))

(define-public crate-unwrap-gui-0.1 (crate (name "unwrap-gui") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 2)) (crate-dep (name "native-dialog") (req "^0.7.0") (default-features #t) (target "cfg(any(target_os = \"windows\", target_os = \"linux\", target_os = \"macos\"))") (kind 0)))) (hash "00mjkni47978548gjhv7lficiaj6qf7y54wjk3p0ngsn8ag7v1fj")))

(define-public crate-unwrap-infallible-0.1 (crate (name "unwrap-infallible") (vers "0.1.0") (hash "1ja59hpjk52ynzm2jbafayk1dnrz74831k3cr18na5x9ylyslx26") (features (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1 (crate (name "unwrap-infallible") (vers "0.1.1") (hash "1ah407i353942p5kzpgfr50a37d5i6a5klmhg93avci2k8mq7lcc") (features (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1 (crate (name "unwrap-infallible") (vers "0.1.2") (hash "0kgcz5l2mjqsp1spq5wbsnyns8yvyf72wrk60irkmsrkrwpk2yjx") (features (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1 (crate (name "unwrap-infallible") (vers "0.1.3") (hash "0mnd8h801190yp31vgm5qfn6f8j3j6094z6vqa2fxsy5qv225hw5") (features (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1 (crate (name "unwrap-infallible") (vers "0.1.4") (hash "1c4qqcah8mdx7hvmjh5j85aci32dsjbsgn0qpmvigjwyb6jh4cvz") (features (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-infallible-0.1 (crate (name "unwrap-infallible") (vers "0.1.5") (hash "1yw1wfv63w5b2r7wpi0b2mrcrqpfxks5gd9r9qn8dhnkg2cw06hm") (features (quote (("unstable" "never_type" "blanket_impl") ("never_type") ("default") ("blanket_impl" "never_type"))))))

(define-public crate-unwrap-log-0.1 (crate (name "unwrap-log") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1nxz5lnw1lcd895g3xn7kcbni5rcqhamfyan5bvp715nhg2ysb7q")))

(define-public crate-unwrap-log-0.1 (crate (name "unwrap-log") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0hi4k4m88gll46a84cmdi5sxf0pflfcdbm1b0ib8ar0vb0m09s9w")))

(define-public crate-unwrap-match-0.1 (crate (name "unwrap-match") (vers "0.1.0") (hash "0f8qra4pch44vlbxcbz2nf55g8zp6hq4vz7p1gvzxsacb1v0ng2r")))

(define-public crate-unwrap-ord-0.1 (crate (name "unwrap-ord") (vers "0.1.0") (hash "0vg9yvgajrwgff0j7cm2glgg24n5qaglpyk0p1cgxs745zvix0xq")))

(define-public crate-unwrap-ord-0.1 (crate (name "unwrap-ord") (vers "0.1.1") (hash "1r53v9qkbml39lg8x55bg34xbc9l5jfmx5sfjysq982jkdfxnisk")))

(define-public crate-unwrap-ord-0.1 (crate (name "unwrap-ord") (vers "0.1.2") (hash "0vv7lq3vi6akxjg2b3pyvi5qiy41kwy5ilhvd02x7li6179vxmai")))

(define-public crate-unwrap-ord-0.1 (crate (name "unwrap-ord") (vers "0.1.3") (hash "07krnmacn2aml3j9xcsk4xh7igq5q8r8h1hrsv42rh7f3vyaflx2")))

(define-public crate-unwrap-sugar-0.1 (crate (name "unwrap-sugar") (vers "0.1.0") (hash "02rpxvqfsnbnwkhp9ica9w35icn44p4bhxg7x1b030ikc7g292pr")))

(define-public crate-unwrap-sugar-0.1 (crate (name "unwrap-sugar") (vers "0.1.1") (hash "0kddrk6ih58hplzz3swzw8r1fl0bjgyx9kwlf028glb3mxlrdhxk")))

(define-public crate-unwrap_all-0.1 (crate (name "unwrap_all") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j1fjgcw3x0vc0mx8sxp2vfglg7s4jm3xhbv7lckf3byixbfcjj0")))

(define-public crate-unwrap_all-0.2 (crate (name "unwrap_all") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "08ldjhjnhmfq47ikcdnlm6bcjk2nvvk046ksaki9wqvcmhh22s5v")))

(define-public crate-unwrap_display-0.0.0 (crate (name "unwrap_display") (vers "0.0.0") (hash "1fhayqlsys1s960mq06iqqbawcvf0c3v9ak3ybc0xdjcf4zy1s12")))

(define-public crate-unwrap_display-0.0.1 (crate (name "unwrap_display") (vers "0.0.1") (hash "1s819fk55vf8giz1vqkigxbqs549kgkfbkbkhxnajskvcrqlc28f")))

(define-public crate-unwrap_goto-0.1 (crate (name "unwrap_goto") (vers "0.1.0") (hash "1qbx3hxfa2jd7d5j67vhcgxxk9sb3pp1m9y57c1p71c0wf8c3pkm")))

(define-public crate-unwrap_goto-0.1 (crate (name "unwrap_goto") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)))) (hash "0g2c2ijfn2x198j0jqb21xbdd6a7jamaivgrk6fzz3hb0bxagcp4")))

(define-public crate-unwrap_goto-0.1 (crate (name "unwrap_goto") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)))) (hash "0v20c963f1fdvxhm6b0rfmh9mlvaywz9fn3nglpyf2ny5d9qpbhn")))

(define-public crate-unwrap_helpers-0.1 (crate (name "unwrap_helpers") (vers "0.1.0") (deps (list (crate-dep (name "loop_unwrap") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d2bdhiy2yrfq92fgyca39522qizw1vzfbfmh865m6fbxpm9d0hc")))

(define-public crate-unwrap_helpers-0.2 (crate (name "unwrap_helpers") (vers "0.2.0") (deps (list (crate-dep (name "loop_unwrap") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y15qc4mm8vkmd077f631v5r30pnkgwzl6k9z66kns0dfdb0lg84")))

(define-public crate-unwrap_helpers-0.2 (crate (name "unwrap_helpers") (vers "0.2.1") (deps (list (crate-dep (name "loop_unwrap") (req "^0.2") (default-features #t) (kind 0)))) (hash "16s4qcbkd87lvpv4276ch2d1wh7vnxk106icj14af448mhqidcyi") (yanked #t)))

(define-public crate-unwrap_helpers-0.2 (crate (name "unwrap_helpers") (vers "0.2.2") (deps (list (crate-dep (name "loop_unwrap") (req "^0.2") (default-features #t) (kind 0)))) (hash "122fmm4lrwc9aw9s2a5acr5s01pyrh7sxkyllgsviw1ayxqmrwgr")))

(define-public crate-unwrap_helpers-0.3 (crate (name "unwrap_helpers") (vers "0.3.0") (deps (list (crate-dep (name "loop_unwrap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unwrap_helpers_proc_macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pkhz0c527msmv35vhgzn3v728fbf5f3cj8km6c3qwkmgjc14rnf")))

(define-public crate-unwrap_helpers_proc_macros-0.1 (crate (name "unwrap_helpers_proc_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06h2b7kfyiyixnwj2m0dmkmg9sbadp6kqqybkwmdjg1wc4iicw7r")))

(define-public crate-unwrap_helpers_proc_macros-0.1 (crate (name "unwrap_helpers_proc_macros") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.81") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "065b06j8vg9rcrx08fvgmhrnp2y4brlfp47am4z0fsxl57v1gggj")))

(define-public crate-unwrap_let-0.1 (crate (name "unwrap_let") (vers "0.1.0") (hash "1jy0p74l3cckqhxzfa0z763n0kgqnfzyd8qrndjg2cznrs17bk2d")))

(define-public crate-unwrap_none-0.1 (crate (name "unwrap_none") (vers "0.1.0") (hash "0pix10dnqak94qjsl1myyr8c09nrm716ldc59rcds1qigs0cfkrw")))

(define-public crate-unwrap_none-0.1 (crate (name "unwrap_none") (vers "0.1.1") (hash "1a13yhlmzrb45dhj6i04v0110qj30v3ziyrj13ivzcilkaibmkah")))

(define-public crate-unwrap_none-0.1 (crate (name "unwrap_none") (vers "0.1.2") (hash "1a9cq0braa4s71qq6vvgv1spk6nw98g9cfisq3n2iizwarchq7a6")))

(define-public crate-unwrap_or-1 (crate (name "unwrap_or") (vers "1.0.0") (hash "0bblwm8i2j1rzc1siglxglvl2ql6nv8bv0vghly2n25gi5hfkgrg")))

(define-public crate-unwrap_or-1 (crate (name "unwrap_or") (vers "1.0.1") (hash "0jjcf9ni36579kqhrgkmk7y4d22gk0png96qjz72i5cp97qf2bwg")))

(define-public crate-unwrap_or_do-0.1 (crate (name "unwrap_or_do") (vers "0.1.0") (hash "1hxsgmnj2gmc1y88dsf66vgvamrf2xhlz68lmq60fdfhgcv4l42r") (features (quote (("std"))))))

(define-public crate-unwrap_or_do-0.1 (crate (name "unwrap_or_do") (vers "0.1.1") (hash "0b2y0x7ybcjhabhv8z8xfjjl9qfcbwf7jaw00gg4ghbb2xzkaq13") (features (quote (("std"))))))

(define-public crate-unwrap_or_do-0.1 (crate (name "unwrap_or_do") (vers "0.1.2") (hash "1ys1xpx55sbaahhga54wpfafc82xl2ki3ncg2xg66s9rdwc15y60") (features (quote (("std"))))))

(define-public crate-unwrap_or_log-0.1 (crate (name "unwrap_or_log") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.3.3") (default-features #t) (kind 2)))) (hash "1v2vp9d63d6vk1j2f1jgjvgkcjvwrqjn56x0rixc66jyhfd5h527")))

(define-public crate-unwrap_or_log-0.2 (crate (name "unwrap_or_log") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.3.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 2)))) (hash "0v518yxcv1ydsq74a1g1qz43zy2932rmpl8wzfn0xjjqfxg547cc")))

(define-public crate-unwrap_or_panic-0.1 (crate (name "unwrap_or_panic") (vers "0.1.0") (hash "16v8g9mqj70gggz4zsba6vb7bnki7sdnnbirrpvl517h5b1794vm") (features (quote (("std") ("default"))))))

(define-public crate-unwrap_or_panic-0.1 (crate (name "unwrap_or_panic") (vers "0.1.1") (hash "0lz9570f66c94fk4gzp8bnvvh94yrcssjaa9j68h1ivb5991zlpk") (features (quote (("default"))))))

(define-public crate-unwrap_or_panic-0.2 (crate (name "unwrap_or_panic") (vers "0.2.0") (hash "1dv29jk31qdwhg4afanxdag1azkwadq70g00wkgk6qf55mdb0cz6") (features (quote (("panic_location") ("default"))))))

(define-public crate-unwrap_or_panic-0.2 (crate (name "unwrap_or_panic") (vers "0.2.1") (hash "13n4s2pzz1vyaa6qlxv0yqdv2m0k2jzwibpz6cjd3rbqjnyxw4z6") (features (quote (("panic_location") ("default" "panic_location"))))))

(define-public crate-unwrap_or_panic-0.2 (crate (name "unwrap_or_panic") (vers "0.2.2") (hash "105a5d7djiw3zigwi6frirqlwkrpxgihhvrmav97hna173lg02gz") (features (quote (("track_caller") ("panic_location") ("default" "panic_location" "track_caller"))))))

(define-public crate-unwrap_or_panic-0.2 (crate (name "unwrap_or_panic") (vers "0.2.3") (hash "0hbjl2pl2cixy17503nw9i5a5nk67z32hf918bgc8cil8xwg6lv4") (features (quote (("track_caller") ("panic_location") ("default" "track_caller"))))))

(define-public crate-unwrap_or_panic-0.3 (crate (name "unwrap_or_panic") (vers "0.3.0") (hash "1l5rxvhwp9bd4g3agri3j8a6hhy2jnbssgzclv6r0lm3fprirfsw") (features (quote (("track_caller") ("panic_location") ("never_inline") ("default" "track_caller"))))))

(define-public crate-unwrap_return-0.1 (crate (name "unwrap_return") (vers "0.1.0") (hash "1ji6ivgvjb7kk22dsmz2wdc200m13crzaz0frz5m8kyqvhc5pcpd")))

(define-public crate-unwrap_return-0.1 (crate (name "unwrap_return") (vers "0.1.1") (hash "1fkanzz8iv1mfdp55nzpsicyi20x6ym7j0vxxhccdf3zv1sc2ll4")))

(define-public crate-unwrap_return-0.1 (crate (name "unwrap_return") (vers "0.1.2") (hash "0jfqkwc1afhpfi2qccbdrjqaf6q9dgzr36dld1aszdfbxhmhwbzc")))

(define-public crate-unwrap_to-0.1 (crate (name "unwrap_to") (vers "0.1.0") (hash "0fdqdxsqfyic16p1zcknj74ak6722k4apy0hz2vc2mypxsr19m6a")))

