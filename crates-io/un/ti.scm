(define-module (crates-io un ti) #:use-module (crates-io))

(define-public crate-untidy-biscuit-0.1 (crate (name "untidy-biscuit") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "xattr") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1w2g6by93y20pvm2p0ml7qif8nn3sphpxsymh1kgq2n247nfmw6l") (features (quote (("xattrs") ("miscellany") ("default" "all") ("datetime") ("all" "datetime" "xattrs" "miscellany"))))))

(define-public crate-untidy-biscuit-0.1 (crate (name "untidy-biscuit") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "xattr") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0brpidfll90rd3ldx6a9f9x4n3lcv99098ps1hb1glypg58ixqxg") (features (quote (("xattrs") ("miscellany") ("id") ("default" "all") ("datetime") ("all" "datetime" "xattrs" "id" "miscellany"))))))

(define-public crate-untidy-biscuit-0.1 (crate (name "untidy-biscuit") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "xattr") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0dyyjslw3lzm429r4ygpd8pfgrzc5kj8q84nxrcqwq05bp32p2mr") (features (quote (("xattrs") ("miscellany") ("id") ("default" "all") ("datetime") ("all" "datetime" "xattrs" "id" "miscellany"))))))

(define-public crate-untidy-biscuit-0.1 (crate (name "untidy-biscuit") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "xattr") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0fz78mv7axjmszp3kgb5xpa9fan6hbkp168164hakl06lqdp9bv6") (features (quote (("xattrs") ("miscellany") ("id") ("default" "all") ("datetime") ("all" "datetime" "xattrs" "id" "miscellany"))))))

(define-public crate-until-0.1 (crate (name "until") (vers "0.1.0") (hash "0124bymkpij3vbd351r1wl5qypkfj84rgskhaj05pp8p3dryqglr")))

(define-public crate-until_regex-0.1 (crate (name "until_regex") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "00nvimkibzlrn7hmdq94q07ms5m5lzcqir8jrsvsxgy8f7an46fq")))

(define-public crate-untildify-0.1 (crate (name "untildify") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1w6pz2vdw914lar5zp3yj7glwxs7crix7f8smq5s53r7q96rg7a4")))

(define-public crate-untildify-0.1 (crate (name "untildify") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1dvblh0s7jmc8yz73xxirn5cr1j9l3wyw1sa0m11j5yg008rwbcr")))

(define-public crate-untis-0.1 (crate (name "untis") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)))) (hash "0r38gc3j16ywjxnkikavnqxl5pg96zwz7rby58v21qp1w9cbi1i1")))

(define-public crate-untis-0.2 (crate (name "untis") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)))) (hash "1i9l0f6rkwhzbyg18g7abdcaaim2nswjcbfpkfiyx2zz0dkhwh3n")))

(define-public crate-untis-0.2 (crate (name "untis") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)))) (hash "08zs45qyh4z0cbpipvf6r3abhbc7anz6bhz0bg19chb4wkchwna0")))

(define-public crate-untitled-0.0.0 (crate (name "untitled") (vers "0.0.0") (hash "0g34697g5sl7fap69cl47anbajkj8d0cg15hnnvxyiqc05nagm26")))

(define-public crate-untitled1-0.1 (crate (name "untitled1") (vers "0.1.0") (hash "0wx8vj2fswf6nqlry48cc6qlxgwih5vww9kkdnlixybzw402jfi7")))

(define-public crate-untitled1-0.1 (crate (name "untitled1") (vers "0.1.1") (hash "1zybg0iy84c7gd46k1ha59km49k8m4j4l5xk326ib32fww79s98k")))

(define-public crate-untitled_lib-0.1 (crate (name "untitled_lib") (vers "0.1.0") (hash "04m9qbxkf1l9366m8jsygg08wj7c4kcxyr1ll85i5jdx19plpwlq") (yanked #t)))

(define-public crate-untitled_os-0.1 (crate (name "untitled_os") (vers "0.1.0") (deps (list (crate-dep (name "uefi") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "uefi-services") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "10gd66x0pnpw4qg8i9yha8rlhkjh1v2adjk9g78nd9lr60rdsr2w")))

