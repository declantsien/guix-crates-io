(define-module (crates-io un we) #:use-module (crates-io))

(define-public crate-unweave-1 (crate (name "unweave") (vers "1.0.0") (deps (list (crate-dep (name "ahash") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "unicode-segmentation") (req "^1") (default-features #t) (kind 0)))) (hash "0bs9nvrcbb07202p1bzfggqz3v0hw402mhd3gw302n7hpsai7i1d")))

