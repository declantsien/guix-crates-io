(define-module (crates-io un sl) #:use-module (crates-io))

(define-public crate-unslip-0.1 (crate (name "unslip") (vers "0.1.0") (deps (list (crate-dep (name "aes-ctr") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1bcbpi4paq96nz1n9975n0x6sbia1fv4iba47dxxbxy1n4gdd05w")))

