(define-module (crates-io un fm) #:use-module (crates-io))

(define-public crate-unfmt-0.1 (crate (name "unfmt") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0agax1nf6bghzgagf2i6hbqj94r6gqqk8h2bkb84brnn8lqk87hy")))

(define-public crate-unfmt-0.1 (crate (name "unfmt") (vers "0.1.1") (deps (list (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unfmt_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16g9315rh11knn7brxmkj982bdi47zvvihpk9bikk6nw4inw1xaa")))

(define-public crate-unfmt-0.1 (crate (name "unfmt") (vers "0.1.2") (deps (list (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unfmt_macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "05h5xwr8a79b8xdfx3ncqnc3jrq1w9dwbp27dx9w68p052m0qb5m")))

(define-public crate-unfmt-0.2 (crate (name "unfmt") (vers "0.2.1") (deps (list (crate-dep (name "bstr") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "unfmt_macros") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mm0prlcpkdnjlsn74q55qdx7dyq3kymg7vdn46hid6jgdg3n0x1")))

(define-public crate-unfmt-0.2 (crate (name "unfmt") (vers "0.2.2") (deps (list (crate-dep (name "bstr") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "unfmt_macros") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "18nifnbbc54b4aa57l3mssh8azanqvs21pym9s9d1jzr0q1d709d")))

(define-public crate-unfmt_macros-0.1 (crate (name "unfmt_macros") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "198wpwrmpw5pp10jbv1qfbi1masqj8zkkjp9ihxm6h1n11z3vfwa")))

(define-public crate-unfmt_macros-0.1 (crate (name "unfmt_macros") (vers "0.1.2") (deps (list (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1bap5wz8hjgdwgk491lwc1sl3mgqcdmdyypprxqc61fyziqav1p2")))

(define-public crate-unfmt_macros-0.1 (crate (name "unfmt_macros") (vers "0.1.3") (deps (list (crate-dep (name "bstr") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1xxvib30s1p0dbwag5y0k7ryir8jn5cp745rlcymdxq47aznv53w")))

(define-public crate-unfmt_macros-0.2 (crate (name "unfmt_macros") (vers "0.2.0") (deps (list (crate-dep (name "bstr") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (default-features #t) (kind 0)))) (hash "08zbqcj1bpn4ww7iiy3zvsx9h7mwyb91h71jmm0lcs4v5ph1yw9c")))

(define-public crate-unfmt_macros-0.2 (crate (name "unfmt_macros") (vers "0.2.1") (deps (list (crate-dep (name "bstr") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (default-features #t) (kind 0)))) (hash "0lvkdvvfaixc9llz8c2i5d6ldk7bc5b5rcbxn2gvzn31imxlzijd")))

(define-public crate-unfmt_macros-0.2 (crate (name "unfmt_macros") (vers "0.2.2") (deps (list (crate-dep (name "bstr") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (default-features #t) (kind 0)))) (hash "17rhh4676h9ri497p45if4r9a9xm5xbiw60k0jbrmfgxv4z1jalj")))

