(define-module (crates-io un hi) #:use-module (crates-io))

(define-public crate-unhinged-0.1 (crate (name "unhinged") (vers "0.1.0") (deps (list (crate-dep (name "ci_info") (req "^0.14.5") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)))) (hash "1snypvvil4r3dyv03szkkh90wys9brh38wsd0mm4zv0k8pvirhvv") (yanked #t)))

(define-public crate-unhinged-0.1 (crate (name "unhinged") (vers "0.1.1") (deps (list (crate-dep (name "ci_info") (req "^0.14.5") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)))) (hash "17i4gxvv22sd0arvarivx5qkdjxn0ybbkawpyh8inhmrrrc05vwi") (yanked #t)))

