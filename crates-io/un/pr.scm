(define-module (crates-io un pr) #:use-module (crates-io))

(define-public crate-unprim-0.1 (crate (name "unprim") (vers "0.1.0") (hash "0m1pks1z4ahhw2ixdsq06bg1x8sk4i2q56hcrzyj4gwvid0z8gcq") (yanked #t)))

(define-public crate-unpro-0.1 (crate (name "unpro") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "0wgc91h2fp5pk5jkvc1c5mcca5lfpah9f6bki3dx3r26v97ys4jm")))

(define-public crate-unpro-0.2 (crate (name "unpro") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "1cynny72qj283f40066gysb4dhwyzcwp2s5qlla3zrg20bbd7k9q")))

(define-public crate-unprolix-0.1 (crate (name "unprolix") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "070a7vccvrf76p3n3nlf5vz8yz4dyib5any6n06zfc58bwdbrjx5")))

(define-public crate-unproxy-0.1 (crate (name "unproxy") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-http-proxy") (req "^1.2.5") (features (quote ("runtime-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.23.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.22.4") (default-features #t) (kind 0)))) (hash "1qk4il35qy87jhkhf1krc8014iamd4csjr2v0mw54vz4i292pd30")))

(define-public crate-unproxy-cli-0.1 (crate (name "unproxy-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.15") (features (quote ("env-filter" "json" "std" "fmt"))) (kind 0)) (crate-dep (name "unproxy") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xj1f10bl3qhblag4f91hhzx5vm6sn4626q288vikfirn14mm7gj")))

