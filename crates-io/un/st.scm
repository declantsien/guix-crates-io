(define-module (crates-io un st) #:use-module (crates-io))

(define-public crate-unstable-0.0.0 (crate (name "unstable") (vers "0.0.0") (hash "1hbdm4pzq102njxvzk8yyiwlvljwr89s2gz27cwb0643nfxkjhf9") (yanked #t)))

(define-public crate-unstd-0.1 (crate (name "unstd") (vers "0.1.0") (hash "1ks7pb46x06qbsb35119h1v9r4y023s6s0j8zvhpsj2zd1yxfmhv")))

(define-public crate-unstorable-0.1 (crate (name "unstorable") (vers "0.1.0") (hash "09y0629xyc7wvjlbnppp0nb617x8hmyvy4safdnbi1v1x2a6f13g")))

(define-public crate-unstringify-0.1 (crate (name "unstringify") (vers "0.1.0") (hash "0dfwzag2lxlkqnwgrl1vkj8j82l23wzqc2zjyr13a3l54r9z97xp")))

(define-public crate-unstringify-0.1 (crate (name "unstringify") (vers "0.1.1") (hash "14mpvs3iy7jky42fj9vpqbxd6w546s104jf6yi9478mz0i5rcz1h")))

(define-public crate-unstringify-0.1 (crate (name "unstringify") (vers "0.1.2") (hash "0z5ddj3cd8zwa00llgw2nqz85fqxblbh1lax7dl1mxh4b8x5jfp7")))

(define-public crate-unstringify-0.1 (crate (name "unstringify") (vers "0.1.3") (hash "0fp5z848p3rflvyplckadwvfd8ib93ghy8skb0spj0mzl9vsayyp")))

(define-public crate-unstringify-0.1 (crate (name "unstringify") (vers "0.1.4-rc1") (hash "0w06wlap4z2igr1ijf00f5m731z6nkm07s9y9x4djj7g1q0spwvb")))

(define-public crate-unstringify-0.1 (crate (name "unstringify") (vers "0.1.4") (hash "0pkbngmchl95m1vairvz6ky9vrw9jsni3f8mi4s2klpa41jdc4ln")))

(define-public crate-unstructured-0.1 (crate (name "unstructured") (vers "0.1.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "13z3sgbakryad8rshb0vavb9z5pwly5a3xc7yykk2saz142axxbc")))

(define-public crate-unstructured-0.1 (crate (name "unstructured") (vers "0.1.1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "1zsxgqfrshfn89allz4aqf37v985i5ki51agfhl5w4vj0lxzcnnq")))

(define-public crate-unstructured-0.2 (crate (name "unstructured") (vers "0.2.0-pre1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "133psh1glhj7g43zlffimy6djranjwxbm9c1fjh8zxr2cdr7xgx4") (features (quote (("selector" "pest" "pest_derive") ("default" "selector"))))))

(define-public crate-unstructured-0.2 (crate (name "unstructured") (vers "0.2.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "0qddf2fq5cvz392kx19qv2czcxl0zxh1vs0ypxyhlj5qhlqx98iy") (features (quote (("selector" "pest" "pest_derive") ("default" "selector"))))))

(define-public crate-unstructured-0.3 (crate (name "unstructured") (vers "0.3.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "04jcbxajh2jkdnq2m2s4hi5mpm33m9bchb5p2hslc9xk71z0p6ma") (features (quote (("selector" "pest" "pest_derive") ("default" "selector"))))))

(define-public crate-unstructured-0.3 (crate (name "unstructured") (vers "0.3.1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "19bqmqbpl27d2wddwk9g1ar2gx77zf5dnpy59xgfg68xyxnkynq5") (features (quote (("selector" "pest" "pest_derive") ("default" "selector"))))))

(define-public crate-unstructured-0.5 (crate (name "unstructured") (vers "0.5.0") (deps (list (crate-dep (name "ordered-float") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "16d9xcnrwrxfkby5kpm7dssii2qmmmkp3yqps2426amhx1nnn6y9") (features (quote (("selector" "pest" "pest_derive") ("default" "selector"))))))

(define-public crate-unstructured-0.5 (crate (name "unstructured") (vers "0.5.1") (deps (list (crate-dep (name "ordered-float") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "1g5p6pgznnbwfqakkz739imgy288lwv7n1q0afxj3kfqp65a5rds") (features (quote (("selector" "pest" "pest_derive") ("default" "selector"))))))

(define-public crate-unstyled-0.1 (crate (name "unstyled") (vers "0.1.0") (deps (list (crate-dep (name "leptos") (req "^0.4") (default-features #t) (kind 2)))) (hash "17q3mjzmlw5j2a3hy3yckazhnid6gli7qaj688bk3g1af1mx4yd0")))

(define-public crate-unstyled-0.1 (crate (name "unstyled") (vers "0.1.1") (deps (list (crate-dep (name "unstyled_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vks2mq0iihz7mrg6l8gyiy7hg3i5n8rh009wdab9kwd8vqy124g")))

(define-public crate-unstyled-0.1 (crate (name "unstyled") (vers "0.1.2") (deps (list (crate-dep (name "unstyled_macro") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1kh4zy8isaadky0v5aja1xizgxvr1p2v3yqh99pzmkaa8hvryv4b")))

(define-public crate-unstyled-0.1 (crate (name "unstyled") (vers "0.1.3") (deps (list (crate-dep (name "unstyled_macro") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0czm1g1qdy28hmh6wdz9d68m72z9harcpbb9x90nks7gsw430f4h") (features (quote (("css-block-lint" "unstyled_macro/css-block-lint"))))))

(define-public crate-unstyled_macro-0.1 (crate (name "unstyled_macro") (vers "0.1.1") (deps (list (crate-dep (name "leptos") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "044j4pdnxbyxm6hryx70kv8kfp7gmqklyr79nl7001cjac0jamwx") (features (quote (("leptos-example" "leptos"))))))

(define-public crate-unstyled_macro-0.1 (crate (name "unstyled_macro") (vers "0.1.2") (deps (list (crate-dep (name "leptos") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "10pfh6kaa6fif6fmrfka513l0pvnl6wkkni394szg0ffrps17krm") (features (quote (("leptos-example" "leptos"))))))

(define-public crate-unstyled_macro-0.1 (crate (name "unstyled_macro") (vers "0.1.3") (deps (list (crate-dep (name "leptos") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "18n1npg5dkcll5ml5g2fjymf1znqb91r7q5a8jp03yc6z2kp3qdb") (features (quote (("leptos-example" "leptos") ("css-block-lint"))))))

