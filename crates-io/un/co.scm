(define-module (crates-io un co) #:use-module (crates-io))

(define-public crate-uncode-0.1 (crate (name "uncode") (vers "0.1.0") (hash "1pkj9xkgw63y0crqjbq3yidlf6gz386kf7xd2pl3d6wwyxfjpc3k")))

(define-public crate-uncomfy-0.0.0 (crate (name "uncomfy") (vers "0.0.0") (hash "11awz32a8qszy41myk4l0xw45vmfxmzxgkdwynxcps6fdkb4s7in")))

(define-public crate-uncompute-0.1 (crate (name "uncompute") (vers "0.1.0") (hash "0kbw3h34p7h326csrgn28zdr1k4jqgnasl1d5zaj9s0vbll6s3dc")))

(define-public crate-uncon-1 (crate (name "uncon") (vers "1.0.0") (hash "1b9vb48p5qn3m0vv4z6mk566a727rvhxdx9q1ivghvyi1dzq5ivi") (features (quote (("std") ("nightly" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-uncon-1 (crate (name "uncon") (vers "1.1.0") (hash "00j21h1wwmcz0raxqkny6c1s31yacl4sf3j79h6sz0mkn8hxi6q7") (features (quote (("std") ("nightly" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-uncon_derive-1 (crate (name "uncon_derive") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "uncon") (req "^1.0.0") (kind 2)))) (hash "16xlh9mj7y8cdkffh3yi7a8hycqzqc4rs9grdrbq8ywnrhlyjm81") (features (quote (("std" "uncon/std") ("default" "std"))))))

(define-public crate-uncon_derive-1 (crate (name "uncon_derive") (vers "1.0.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "uncon") (req "^1.0.0") (kind 2)))) (hash "1b1c6fryf139jdr5wg8s8iz539llnrg3sqg1h2db3hrmajl8bcyr") (features (quote (("std" "uncon/std") ("default" "std"))))))

(define-public crate-uncon_derive-1 (crate (name "uncon_derive") (vers "1.0.2") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "uncon") (req "^1.0.0") (kind 2)))) (hash "199ppfn4vyd7fbj21sk0qkmda02pifpb94babsy50xf7d1xb8cg6") (features (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1 (crate (name "uncon_derive") (vers "1.0.3") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "uncon") (req "^1.0.0") (kind 2)))) (hash "0l6155vvc53s9pvl0bhhi3pbl7zbrndwc3b5vdc952xzylqmwyl1") (features (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1 (crate (name "uncon_derive") (vers "1.0.4") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "uncon") (req "^1.1.0") (kind 2)))) (hash "1wyj2gj7y7vc7g07whrp1nhd12p8f4qwr0v3dqaq60limqjvb0fb") (features (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1 (crate (name "uncon_derive") (vers "1.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "uncon") (req "^1.1.0") (kind 2)))) (hash "0sib9ina87chmg1rpknk687v477mxxl19907c9aqjrv8hdaflq7k") (features (quote (("std") ("default" "std"))))))

(define-public crate-uncon_derive-1 (crate (name "uncon_derive") (vers "1.1.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "uncon") (req "^1.1.0") (kind 2)))) (hash "0973kwsyx103c6hvyykj1bb093gibgqmd7h8km3jm14mkiyznbly") (features (quote (("std") ("default" "std"))))))

(define-public crate-unconst-0.0.0 (crate (name "unconst") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "08x61jrl7w0syqn3mvxlmrl71jhyx2i0mhdqqgn7l7wka9h47s1k") (rust-version "1.62.0")))

(define-public crate-unconst-0.0.1 (crate (name "unconst") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "10d3r7xx0l0rhjrbr8hppnrh4vbna0kgn9cn3xxpq2ihv907qxml") (rust-version "1.62.0")))

(define-public crate-unconst-0.0.2 (crate (name "unconst") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0avwhrh9fi5mqbmlybkhb7l8224whr0xcc8sg23zbj1l42gyls5x") (features (quote (("const")))) (rust-version "1.62.0")))

(define-public crate-unconst-0.7 (crate (name "unconst") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "parsing" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "0h7cl8y1q78kiplnl4f7pbj8bbap16wy7zjlnhah23phvmvznw44") (features (quote (("default" "proc-macro2" "quote" "syn") ("const")))) (rust-version "1.62.0")))

(define-public crate-unconst-0.8 (crate (name "unconst") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "parsing" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ry9a8l9ja0gi84v0b8vwq4q8b726ffs9gvvn5067nn2sdanxm2m") (features (quote (("default" "proc-macro2" "quote" "syn") ("const")))) (rust-version "1.62.0")))

(define-public crate-unconst-0.9 (crate (name "unconst") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "parsing" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "1fg9gvbzbz4hvlimi66daancq4gqgha6mybrr08czbiljw648bgb") (features (quote (("default" "proc-macro2" "quote" "syn") ("const")))) (rust-version "1.62.0")))

(define-public crate-unconst_trait_impl-0.1 (crate (name "unconst_trait_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gqx0fz48d4xk4j3sn033mflivhb9n5132w0cl110dfdnwncwdcr") (rust-version "1.56.1")))

(define-public crate-unconst_trait_impl-0.1 (crate (name "unconst_trait_impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10vznr6vwv7lpd1a5q93n1d7z40raphjm1hxd6x1azmm1j59x0ij") (rust-version "1.56.1")))

(define-public crate-unconst_trait_impl-0.1 (crate (name "unconst_trait_impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0af03ng5w0xj96s0b634kadmjl2709skibr8bdy79s8pr9b4i63i") (rust-version "1.56.1")))

(define-public crate-unconst_trait_impl-0.1 (crate (name "unconst_trait_impl") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1755ks1ygpmm6dzkh7fkbqk9fri50yfpbys2q075jbr001f0mq1p") (rust-version "1.56.1")))

(define-public crate-unconst_trait_impl-0.1 (crate (name "unconst_trait_impl") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dm8kc9ndm9h8d8pw4046mhszw6rz8c6y15mrjm0vhnnsimdxh7m") (rust-version "1.56.1")))

(define-public crate-unconst_trait_impl-0.1 (crate (name "unconst_trait_impl") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0wyy516imirq9aqyy3sw7gkbl2pz022iqnpfzhwfhznpdsx5pkmn") (rust-version "1.56.1")))

(define-public crate-uncover-0.1 (crate (name "uncover") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "1v0dhg95si7bhis346rq9kdrklvmfxkki48b9ymmgzn7g3rav7hi") (yanked #t)))

(define-public crate-uncover-0.1 (crate (name "uncover") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "0rfnl2jfvd1qlmnw2m0ffqvaf3rdmh5h5m7g6rhj2w61mvqw5f4k")))

(define-public crate-uncover-0.1 (crate (name "uncover") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "01lr67xmg31p0xyi5lmfn1523p2z0q0xahzdn8g6ndfg5jqqdsad")))

