(define-module (crates-io un iq) #:use-module (crates-io))

(define-public crate-uniqcache-0.1 (crate (name "uniqcache") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0miavfzqqyigpizqzzf9m8rk7s2r7wxk4fwvm547m12ycgfvv4if")))

(define-public crate-uniqopy-0.2 (crate (name "uniqopy") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1jx3wyb98simqz4lksi33gfqf9krk70zpiraqgaphzqw4g6aydkm")))

(define-public crate-uniqopy-0.3 (crate (name "uniqopy") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1x248m4914ajkjd6ba7j65alrj6mdmr5d1j59vx15k4cppwyxg5y")))

(define-public crate-uniqs-0.1 (crate (name "uniqs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)))) (hash "0ji0shk2acpv6kz5nc4zs822v86x00ra03rqa6mhcwnkly65bw9l")))

(define-public crate-uniqtoo-0.1 (crate (name "uniqtoo") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "07viwf192njk2kz61wbhz96sb6lgpk3h1099c260s90w24qqa1rl")))

(define-public crate-uniqtoo-0.2 (crate (name "uniqtoo") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lyplq61cyd2il65xp1fhi95gnazffhsg3swk4xnqrcnpzcl2q24")))

(define-public crate-uniqtoo-0.3 (crate (name "uniqtoo") (vers "0.3.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0h64l6g7wg5fxqca482ajbn8ybzw3ms0xp508yg2crcv8gq4i4rq")))

(define-public crate-unique-0.1 (crate (name "unique") (vers "0.1.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 2)))) (hash "1kzs5wlb62vzwnj18xh2azmq7r4a1vmd8jjc3pgd3fm37x7w9f3w") (yanked #t)))

(define-public crate-unique-0.2 (crate (name "unique") (vers "0.2.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "0n4r3gyk6z26jhnm1pcncv4p0fxjlqy74gkn9spv7xrmr2ypd0ry") (yanked #t)))

(define-public crate-unique-0.2 (crate (name "unique") (vers "0.2.1") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "1sd869lvw16qn0r1pxg2k75ldjdykp983pm4gacw9nkrdgzflqwp") (yanked #t)))

(define-public crate-unique-0.2 (crate (name "unique") (vers "0.2.2") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1") (default-features #t) (kind 0)))) (hash "1yff1bh01gyd3plqzcqij2c456xnkhnsjmvfbqrslj9xd2mx16jz") (yanked #t)))

(define-public crate-unique-0.3 (crate (name "unique") (vers "0.3.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 2)))) (hash "0a6p693gw6z1cdi6vxbsr6400h2zbqk9h7s1yzfpa9s6ymh5gxd0") (yanked #t)))

(define-public crate-unique-0.4 (crate (name "unique") (vers "0.4.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 2)))) (hash "03pcwxckpqhnzgm6nh36qp50h6469m4s4ibz43n4h2rgzi4s4220") (yanked #t)))

(define-public crate-unique-0.4 (crate (name "unique") (vers "0.4.1") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 2)))) (hash "0i95gi1v0vplq8lfp4zggwmcl5r3n8f8s5zbdm0i9b63ij2drkrm") (yanked #t)))

(define-public crate-unique-0.5 (crate (name "unique") (vers "0.5.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)))) (hash "06m4n18mh8rrs84qic5vb0qz2q3mlilbs02s8vsawimrsvwwakq6") (yanked #t)))

(define-public crate-unique-0.5 (crate (name "unique") (vers "0.5.1") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)))) (hash "11fwrj4my8xily1pvvjwxsgnc9mcjrgcb7pk7x1im8dqk17nc6pl") (yanked #t)))

(define-public crate-unique-0.5 (crate (name "unique") (vers "0.5.2") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)))) (hash "1dpgmzdxxr8fxvxz8045ya2nbwb97bc9742zv1h9rc4pk1lvm7sw") (yanked #t)))

(define-public crate-unique-0.5 (crate (name "unique") (vers "0.5.3") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)))) (hash "1cd4sil23s4w3crgnpi9sr9mzj8gdcdf9rra5rgln1ax1i04c3i7") (yanked #t)))

(define-public crate-unique-0.6 (crate (name "unique") (vers "0.6.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)))) (hash "0jfgbr5fdmz6n0ariihzqksad5wl3ay5r0c4hgf746lw7akgmv9h") (yanked #t)))

(define-public crate-unique-0.7 (crate (name "unique") (vers "0.7.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)))) (hash "17rlk83pg6s66nyrj8v1dcvlb4ikirshs1rzrnf853aa055aczm4") (yanked #t)))

(define-public crate-unique-0.8 (crate (name "unique") (vers "0.8.0") (hash "1zyh3sd6khxcshfdnicm9f2wzhrdll40i6bnrqsgv9mj58lc22k1") (yanked #t)))

(define-public crate-unique-0.9 (crate (name "unique") (vers "0.9.0") (hash "1sdw3inhmbq65rc04j22gv7hw66jc7c4djmzkn1hnhlnx0acffis")))

(define-public crate-unique-0.9 (crate (name "unique") (vers "0.9.1") (hash "15msk90g2spyd0r32sn7y1qzf4gi5zq8a89k9nqzb11q3wp74q6k")))

(define-public crate-unique-count-0.1 (crate (name "unique-count") (vers "0.1.0") (hash "02i42396mq6p1pnal52sagy5ialccyhry8r02n8sq52z7kp0sw7y")))

(define-public crate-unique-id-collection-0.1 (crate (name "unique-id-collection") (vers "0.1.0") (hash "0ffz097wzkn5lp48133pwaiwz8kpxqbvlgagvlfnpq5gwm4ai615")))

(define-public crate-unique-paste-0.1 (crate (name "unique-paste") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "paste-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1lm7l5q881kqd4cabaihirhwqlvgnb2h5whhvpv2y6kwfda7s85p") (rust-version "1.31")))

(define-public crate-unique-rust-0.1 (crate (name "unique-rust") (vers "0.1.0") (hash "13pna57cjdxiyws224gbckzc24hg6q969lfkz6kxm1s0wd4hwri7") (yanked #t)))

(define-public crate-unique-token-0.1 (crate (name "unique-token") (vers "0.1.0") (deps (list (crate-dep (name "triomphe") (req "^0.1.5") (kind 0)))) (hash "1bzwkb4qbi4wni85j0lnkzmqgfmyhaj7fnlw18cmjdxcpl7hhh0k")))

(define-public crate-unique-token-0.1 (crate (name "unique-token") (vers "0.1.1") (deps (list (crate-dep (name "triomphe") (req "^0.1.5") (kind 0)))) (hash "1n2nnihf24ma7rk8llh9sxw3fqc96p381br4ray0875ajwg66cm5")))

(define-public crate-unique-token-0.2 (crate (name "unique-token") (vers "0.2.0") (hash "0rkv989fml506191f9mwvwlxrlpl5f881cjygyq3byj4x4wrxgnk")))

(define-public crate-unique-type-0.1 (crate (name "unique-type") (vers "0.1.0") (hash "03vlih1hks20lvglblzn8h1sc4n3gvlk3zgz4assxff0sl1nh9v5")))

(define-public crate-unique-type-id-0.1 (crate (name "unique-type-id") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0m11d6i80p6awfg4d2amqy1dpk4a8dng37rblwh22192yj53clbj")))

(define-public crate-unique-type-id-0.1 (crate (name "unique-type-id") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0nbx62kq4ffy61s1rrd7l4qp11fa2lwqaj0s766560v8cj0050bi")))

(define-public crate-unique-type-id-0.1 (crate (name "unique-type-id") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1bzwdnqwk8nrgn1vjhq9jlvy61aclsb3glrxml0br8n235s2zz0b")))

(define-public crate-unique-type-id-0.2 (crate (name "unique-type-id") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0x1w5aprq0ymlb07ajz8263xw6xvpskp5yd289g7zfg9yvskihzv")))

(define-public crate-unique-type-id-1 (crate (name "unique-type-id") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1hv5b54pr0bjzry2i5047dczybplw9hp7rfi2bgff21ylp97ycvg")))

(define-public crate-unique-type-id-1 (crate (name "unique-type-id") (vers "1.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1iwmpcvf0d040isjgfcs9h19nwzpigc6d4j0fvijzhz2617y1bnn")))

(define-public crate-unique-type-id-1 (crate (name "unique-type-id") (vers "1.2.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0d1mm4n5dfdscs5i30lrjqafixcgxp4n0pbxdfq3jzp81qva8fp1")))

(define-public crate-unique-type-id-1 (crate (name "unique-type-id") (vers "1.3.0") (deps (list (crate-dep (name "unique-type-id-derive") (req "^1") (default-features #t) (kind 0)))) (hash "0svsaaln74gm45cfsyhnigz43pnr6kzl5xv8sxcfndm166zjyin0")))

(define-public crate-unique-type-id-derive-0.1 (crate (name "unique-type-id-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id") (req "^0.1") (default-features #t) (kind 0)))) (hash "043j3s14dpfbh1ls89zsnpn8qnvs0x92ksk7ffl41fg9jqndfhp8")))

(define-public crate-unique-type-id-derive-0.1 (crate (name "unique-type-id-derive") (vers "0.1.1") (deps (list (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dhzd8rpi55rdr5hwgdbwmc6knfmgcbp8pvvssx0ar915c7wc905")))

(define-public crate-unique-type-id-derive-0.2 (crate (name "unique-type-id-derive") (vers "0.2.0") (deps (list (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hq2vr75w2pv3pbfagk9dbg34vqjkmxad82dl9xvq9vlmc415knl")))

(define-public crate-unique-type-id-derive-1 (crate (name "unique-type-id-derive") (vers "1.0.0") (deps (list (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id") (req "^1") (default-features #t) (kind 0)))) (hash "0dz1363h0klc303yih50na351i334ipn3f5qmxhymfqywvscczsd")))

(define-public crate-unique-type-id-derive-1 (crate (name "unique-type-id-derive") (vers "1.1.0") (deps (list (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id") (req "^1") (default-features #t) (kind 0)))) (hash "0m4zdgkwxxnjqy2p23kj572bcac5svpzr0xfmadx3nsml4nadqh2")))

(define-public crate-unique-type-id-derive-1 (crate (name "unique-type-id-derive") (vers "1.2.0") (deps (list (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id") (req "^1") (default-features #t) (kind 0)))) (hash "0rnbim1c9pl2d29li046cvl9wyz351riis093r2qcakfvhwfbah5")))

(define-public crate-unique-type-id-derive-1 (crate (name "unique-type-id-derive") (vers "1.3.0") (deps (list (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "01p45pp83v00xa8i53m1nq3dsajs23nzpnmpp6rqwiiz00h18zj0")))

(define-public crate-unique_64-1 (crate (name "unique_64") (vers "1.0.0") (hash "0qrwy0gdra0ky6a323iyyq39iyak80dy47wxb7abnndlh9vdwz4f") (yanked #t)))

(define-public crate-unique_64-1 (crate (name "unique_64") (vers "1.0.1") (hash "0mc4m2nbr6yzhwjpkq0gda1jxy99cd37wihrn0cdqhqlfv14zjkc") (yanked #t)))

(define-public crate-unique_64-1 (crate (name "unique_64") (vers "1.0.2") (hash "10x7vwxzbh2qz5m0h5a09azfxc7yaqbr64nn6kz2a7ckc4a56b6x")))

(define-public crate-unique_aggregator-0.1 (crate (name "unique_aggregator") (vers "0.1.0") (hash "0vqv2hyr6frnihrpd83zw3d0z5zx691ad8n8pbw5cqxviqmsacnf")))

(define-public crate-unique_id-0.1 (crate (name "unique_id") (vers "0.1.0") (deps (list (crate-dep (name "atomic_refcell") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "blob-uuid") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1kkrnqnhiyl1rbp7p5q8hrcjl66287z9a45aaqvvzj9rik54wzgz")))

(define-public crate-unique_id-0.1 (crate (name "unique_id") (vers "0.1.1") (deps (list (crate-dep (name "atomic_refcell") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "blob-uuid") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1y62vjiyxd7wkqj22mmmkn3qs1q3pv38ym4d03yq80pdsbwx6kp7")))

(define-public crate-unique_id-0.1 (crate (name "unique_id") (vers "0.1.2") (deps (list (crate-dep (name "atomic_refcell") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blob-uuid") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "13hnnrrhbrwinzsl5kk33ns54039vypfdzkg7fm3m41hvgcpzpv8") (features (quote (("string" "blob-uuid") ("sequence" "atomic_refcell" "lazy_static") ("random" "uuid") ("default" "random" "sequence" "string"))))))

(define-public crate-unique_id-0.1 (crate (name "unique_id") (vers "0.1.3") (deps (list (crate-dep (name "atomic_refcell") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blob-uuid") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wd4p32yzmifkdihwchv4l4bw910rdf4h7pcwa0d9sc2i1afc6in") (features (quote (("string" "blob-uuid") ("sequence" "atomic_refcell" "lazy_static") ("random" "uuid") ("default" "random" "sequence" "string"))))))

(define-public crate-unique_id-0.1 (crate (name "unique_id") (vers "0.1.4") (deps (list (crate-dep (name "atomic_refcell") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blob-uuid") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.0") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ha6j63rxxmwiwq9909dkvrmq6p4zhmif1z0fjnh0jz8kr8wg8mp") (features (quote (("string" "blob-uuid") ("sequence" "atomic_refcell" "lazy_static") ("random" "uuid") ("default" "random" "sequence" "string"))))))

(define-public crate-unique_id-0.1 (crate (name "unique_id") (vers "0.1.5") (deps (list (crate-dep (name "blob-uuid") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (optional #t) (default-features #t) (kind 0)))) (hash "1k34whjazp2vf9fbmcfy3k2iswdczczv1a6lk0vl7v5xvwwmqq5f") (features (quote (("string" "blob-uuid") ("sequence" "lazy_static") ("random" "uuid") ("default" "random" "sequence" "string"))))))

(define-public crate-unique_identifier_msgs-2 (crate (name "unique_identifier_msgs") (vers "2.2.1") (deps (list (crate-dep (name "rosidl_runtime_rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde-big-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "0w0mr6p1p7qnsl8sb185xz47rf7mk46lrfpkhw8inl7wjfw58b9y") (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde"))))))

(define-public crate-unique_port-0.1 (crate (name "unique_port") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "01vv7in1daf4nhn0fr9iycxqv9wdvxs9fcpqq30syxcdca972c83")))

(define-public crate-unique_port-0.1 (crate (name "unique_port") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "0pfz2wdqhrk5g80brk73a0pral2svqzs7xr7x1810myr76niz7fp")))

(define-public crate-unique_port-0.2 (crate (name "unique_port") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "0yawh9kd5x4bg0d3l1zkliqpvgvlipw8f8bzi7iwv6h5l09vzp5d")))

(define-public crate-unique_port-0.2 (crate (name "unique_port") (vers "0.2.1") (deps (list (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "023paz20n6ybsdp6n4j2dr96ff1cvfp1dgqb5qjb9g7s5q9h7yfk")))

(define-public crate-unique_ptr-0.1 (crate (name "unique_ptr") (vers "0.1.0") (hash "11xyqz232m9pn5rmfqlg5fmjy7akh4nk5w9g57581g3hfjy5fdlf") (yanked #t)))

(define-public crate-unique_ptr-0.1 (crate (name "unique_ptr") (vers "0.1.1") (hash "1zvfac31qw3n1b75rwk6mz6krcgnhs3fy9yjl349j1k1sddb7k5d") (yanked #t)))

(define-public crate-unique_ptr-0.1 (crate (name "unique_ptr") (vers "0.1.2") (hash "1lqzpzdmp72k247zz76rqqcvagyyvldsyy2w2qh4wypf1iwb4prf") (yanked #t)))

(define-public crate-unique_ptr-0.1 (crate (name "unique_ptr") (vers "0.1.3") (hash "0wwi97fhdqhk2rwmvxx9jalvy13sa4y033qggzbmpq68svp4dds1")))

(define-public crate-unique_type_id_workspace-1 (crate (name "unique_type_id_workspace") (vers "1.0.0") (deps (list (crate-dep (name "unique-type-id") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id-derive") (req "^1") (default-features #t) (kind 0)))) (hash "0ng7sv2nifb7ffjqnd167isbjrdiqdiin5qn8z1x6457dyqq8njj") (yanked #t)))

(define-public crate-unique_type_id_workspace-1 (crate (name "unique_type_id_workspace") (vers "1.1.0") (deps (list (crate-dep (name "unique-type-id") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unique-type-id-derive") (req "^1") (default-features #t) (kind 0)))) (hash "0manyjlr50qx54m00mw138x0xvss3aghz51778jwha7qn97smz40") (yanked #t)))

(define-public crate-uniqueid-0.1 (crate (name "uniqueid") (vers "0.1.0") (deps (list (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)))) (hash "14wqcnwpw4ibysp5alxnfshrv9rfpqj82fwqvihar8xcbzmxzgbs")))

(define-public crate-uniqueid-0.2 (crate (name "uniqueid") (vers "0.2.0") (deps (list (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.23") (default-features #t) (kind 0)))) (hash "1l00mffbsgc0817s06s3hqaak3mkvrdxkx3k450b0vji27lc0gms")))

(define-public crate-uniqueid-0.2 (crate (name "uniqueid") (vers "0.2.1") (deps (list (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.23") (default-features #t) (kind 0)))) (hash "109ygm7f7zd92hgv0jpb9ps2djv0v0rv26rm34f60clml9rmcf8m")))

(define-public crate-uniqueid-0.2 (crate (name "uniqueid") (vers "0.2.2") (deps (list (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.23") (default-features #t) (kind 0)))) (hash "1148isn2amrr5q1mkml4hvbaq25vk9k9vcgg85lxaan4cy6khwx0")))

(define-public crate-uniqueid-0.2 (crate (name "uniqueid") (vers "0.2.6") (deps (list (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.23") (default-features #t) (kind 0)))) (hash "15h0m1ci5cdm6xi0rmsnn3k2wamlzpz094a1kvd4dn3s5ckmc4ng")))

(define-public crate-uniquewords-rs-0.2 (crate (name "uniquewords-rs") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "18r30rpdjabcldicxn86cb4xqbn2vsnb2h5ig55k8i2sr91va82h")))

(define-public crate-uniquewords-rs-0.2 (crate (name "uniquewords-rs") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "0hpmdzz74ymhczws80p7mjc334zp72pyykfcp9341h6386zglrnc")))

(define-public crate-uniquewords-rs-0.3 (crate (name "uniquewords-rs") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "17r21gz3zgihmgw54lg6vlyhg48ackpgkxm1znq2jbg304chrjiv")))

(define-public crate-uniquewords-rs-0.4 (crate (name "uniquewords-rs") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "06f0rff6mzssyssjaw5sr7fbswh2a81ny35n88lhnb3yrdxdgz33")))

(define-public crate-uniquewords-rs-0.5 (crate (name "uniquewords-rs") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "16w8fvs87cw264llnhi300aq1lp8y7ibqwv3s03b1z6g9anhaigl")))

(define-public crate-uniquewords-rs-0.5 (crate (name "uniquewords-rs") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1zfb720ljsaqfv89iqw7awsj74v0cnp8ywrgxplnki1z8ad6w3ch")))

(define-public crate-uniquewords-rs-0.6 (crate (name "uniquewords-rs") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1vbh3mfdd81h9x64iz6pskyhwj733060bghqjll81xikycpq942q")))

(define-public crate-uniquewords-rs-0.7 (crate (name "uniquewords-rs") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1lhwp254vclnbj06adkkixqb66ph5iwicbgsnlphz0qnmm6dg8cv")))

(define-public crate-uniquewords-rs-0.7 (crate (name "uniquewords-rs") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1pmqina2syffsb0bcydf065r51vbymk1jwpsc2dd7hg89ig26l7b")))

(define-public crate-uniquewords-rs-0.8 (crate (name "uniquewords-rs") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^3.1.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (default-features #t) (kind 0)))) (hash "1k00ch6x1djks78dd8jaa8dr2z2w10cb2cj63mc30aq9fk68mifk")))

(define-public crate-uniquify-0.0.0 (crate (name "uniquify") (vers "0.0.0") (deps (list (crate-dep (name "arcstr") (req "^1") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cflhijrrpdch3s4dw717i785bz19sivgsz818xv77rkr2ni1lll")))

(define-public crate-uniquote-0.1 (crate (name "uniquote") (vers "0.1.0") (hash "10hhd8r1ga9m43kzzryc01nsa8r9gp5pp6kwhbfrqh0yjm9lf3i4") (features (quote (("std") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-0.1 (crate (name "uniquote") (vers "0.1.1") (hash "1p57pgasdavhkpbs5zsbdrxbk56dqf2y84k3ar3amsmy20k2qiy3") (features (quote (("std") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-1 (crate (name "uniquote") (vers "1.0.0") (hash "14skciz60f4iwpmkiipvkavdsryykhskzrvpkr91zyvx9wgmi5cj") (features (quote (("std") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-1 (crate (name "uniquote") (vers "1.1.0") (hash "19hfp49i98v46qqj89dzz6qrlbvgvbgxy7ja0lv5zj01iv0cajcb") (features (quote (("std" "alloc") ("min_const_generics") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-1 (crate (name "uniquote") (vers "1.1.1") (hash "1z9vvvcfdi10yb7mrcjbq2p29l5x377a4v9lhavf2syhygqyym56") (features (quote (("std" "alloc") ("min_const_generics") ("default" "std") ("const_generics") ("alloc"))))))

(define-public crate-uniquote-2 (crate (name "uniquote") (vers "2.0.0") (hash "1capkvz0p90g3nipfq46a3yplj6nkj8rbxdg0rrm40id4jvd6q4b") (features (quote (("std" "alloc") ("min_const_generics") ("default" "std") ("alloc"))))))

(define-public crate-uniquote-3 (crate (name "uniquote") (vers "3.0.0") (hash "0gqwq3kbzdsj5qsc8jfm5v4qwzgnp4rrfvdpm71ch1593h22y664") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-uniquote-3 (crate (name "uniquote") (vers "3.1.0") (hash "1bkl0n41yvs415mqny4b434kr456ysnb3dhic1zrrzppwx95jvxa") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.52.0")))

(define-public crate-uniquote-3 (crate (name "uniquote") (vers "3.1.1") (hash "0yvknyjwpxkksjhj39sv5mljqp584r6a43lbxz01xwpjcwb6x84w") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.52.0")))

(define-public crate-uniquote-3 (crate (name "uniquote") (vers "3.2.0") (hash "0gd21cl7vmwis0snmnnzwi92vlh85f8n5gc95wn4kpbv36ygi418") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.56.0")))

(define-public crate-uniquote-3 (crate (name "uniquote") (vers "3.2.1") (hash "07bd7p6l6knvwj7dzn5vambjss8jqm24l3v52c0af7c0mj5r5ky4") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.56.0")))

(define-public crate-uniquote-3 (crate (name "uniquote") (vers "3.3.0") (hash "037xznqcdvc3riy9498cfrzzdhj2591cz0dpiy0h9wxfnbp01aal") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.64.0")))

(define-public crate-uniquote-4 (crate (name "uniquote") (vers "4.0.0") (hash "0yacyz7c09blbvfkkjlzgyyvif7i6n0y940wpbpw6xi29cqnvmqz") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.64.0")))

