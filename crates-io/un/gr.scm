(define-module (crates-io un gr) #:use-module (crates-io))

(define-public crate-ungrammar-0.1 (crate (name "ungrammar") (vers "0.1.0") (hash "0zdkvbvl793mx3nd3a5qgcj20872mhi300nmjlnwqfmbj542xq8f")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.0.0") (hash "1jlhp0by2dgwgsg33cbmmsii0b1f1carqgv7iqgna99f6nnizf9b")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.1.0") (hash "0fciv1q5r6z44a7qch0fnz55b3xzgyadwf0n4ghcx0mxwqmlwcbi")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.1.1") (hash "1b8a462cdcmcpsaga1ci91n4s1fga12zfp39lkwbrqcfl1c0xqn4")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.1.2") (hash "1cwj0v8ji18dx07nny140z2phm7dz9pcmykqxaqi9rvvqwm19dms")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.1.3") (hash "04ky477kyadl2pfhfvizak6hh1q5xs9wrxqk639mixj5bc33jkfa")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.1.4") (hash "1a8pkv4fjlkr74va2fb372z0v6s7p2nwcvs3w905amdgjfcxh36z")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.2.0") (hash "1l9my5zig0dja8znlg24rvlb8vd3wmk1dv5rpyq2k9hr24gkcwg0")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.2.1") (hash "0cq3mmwz3xvf97n3dlwl3aqz6y2w70srzmr6ls2rw7gdz0xap8fm")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.2.2") (hash "1mv2bqij7csh3wr23w7n6ib2173j7g9s1048wbkpjwv2c2j8ccc7")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.3.0") (hash "16m5qc680gznjs9syycc0yijjah98jr3w13blafslhxcza9yw4bk")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.4.0") (hash "1dsgnhxam31x18llk3w1r5canzadyzdsv99c8xa5glnfydwi75b8")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.5.0") (hash "0mql7ba8xjqw6vnvs8fbzy8cy2k4lip2h2qnaqm8zp1flnnzy6y1")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.6.0") (hash "1vmpa66bgsmq32mjk64h62hpgblnr32vlc66pj7m8z4gjfvc2v7r")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.7.0") (hash "02c52v6c7ajzw8ymlv96mvahhraxr2mj8wkwl5lc7m9qc37zqn3p")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.8.0") (hash "0wpfydm8ccc41vhngvig63vaxj3cdah1jhkwai995y1s821j2fp3")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.9.0") (hash "14qs5cry0fnnp7sqcwa025zddxa96qcpvlsbs2fm6hmrldsshdxi")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.9.1") (hash "05ljmgj1y2gijfyr1n2a1iwbb6v940jpd9b7qix4plxxa4nmgyz1")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.9.2") (hash "12ycnjgf4df3rlprhcnwwz46z53bdvlfhhz8aiixawm884h2x82q")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.9.3") (hash "0yrdniq44d0aii83fdil152fbxgar0sg3bl0v2ls39pkq1r1747m")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.10.0") (hash "0p1kil8f173vl30jpzbs0v2n4adlqvplba7b6lx8v1d8fmlqc96f")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.11.0") (hash "0sja1r428fhqyh1hmpx5525vmiaxa37l4pywl7r4jw1pbmwjkil4")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.12.0") (hash "0hy3ps93iyjhy27sqlmkrgl51q8dlrarzjh8s15x81cxy0drj48p") (yanked #t)))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.12.1") (hash "1sll0gnx4c6l1h1bf5lrsf6f567cssz03d8jn714vlklq8jc42z6") (yanked #t)))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.12.2") (hash "1fkk4dgnp3jl228z6g8ncd0dpjrd2fsa0j9xh3z4ww1hqnkqcrfz")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.13.0") (hash "17wm4b5r3z5hqail6p609nii37y3j8dr4bxg8x0bkhkc2wa06xkn")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.0") (hash "19z3xlgsgk7q6v408h2ax5ljar74p7179gn4lzjc6zn76mrnvvsh")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.1") (hash "14a8ykklzjy0xyd34ycp47pbdzcz02qrm2jp0kjdippmq306ggix")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.2") (hash "0wh9m3jpraxbink2xmcfd70505rgw458r5hwh6y55jp9d8526d2k")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.3") (hash "044r48vgln32k3hhgm2ss5xpmkmf94fa104xmpn9m2wm7w4zbwhf")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.4") (hash "0nw30rwck011if2hvn6gpj1rdyknqcjzx50mid474d0lggakcvhd")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.5") (hash "1x8f11irp522ywqm4n72wcrd75q1g4gmy7dd320ql2s8ya59hpir")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.6") (hash "05d7bvw6g8yhz8y6yc2zf0w1cray9dfdikmip8vgzck3d1kdc77v")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.7") (hash "0gk7b695hwi13jmwi2vcgchm9q7ng9q4zlci978di1xfi90aklaz")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.8") (hash "0a8m8bb9kkn8myy9g76af1gwgilr54bm0df753xwzjj6mn91hg20")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.14.9") (hash "1agx2hpp3sirknnx8j17mzcg222024s6vkx6s5v3s3l8zp15kgk6")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.15.0") (hash "0clpyc7r3iac6sq7k78ijzzf8nh0yq6nirq80mh7s3s505qmc0gd")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.16.0") (hash "03w8hzmdkrc3l1535wpslh9ci16j3ggzqysyrrci8b57pp5p88x6")))

(define-public crate-ungrammar-1 (crate (name "ungrammar") (vers "1.16.1") (hash "13ynrv658ikr4lqi3lk1xbcrr1d1qsjnrb8acwfyrwqbgwsdzrd3")))

(define-public crate-ungrammar2json-1 (crate (name "ungrammar2json") (vers "1.0.0") (deps (list (crate-dep (name "ungrammar") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "write-json") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jagx5vf9zlzhn868vi5qq452nz7v6i6k77fckmi88yk69lr7ngp")))

