(define-module (crates-io un bo) #:use-module (crates-io))

(define-public crate-unborrow-0.1 (crate (name "unborrow") (vers "0.1.0") (hash "0b5g3qgaglg0zppkd6vg522pb106ihgm3yk44vp6p5clll5ydjr6")))

(define-public crate-unborrow-0.2 (crate (name "unborrow") (vers "0.2.0") (hash "115kllz4mx71j97ddzr788bhxa5x1h296ds6d4vk89j13w5c0kzx")))

(define-public crate-unborrow-0.3 (crate (name "unborrow") (vers "0.3.0") (hash "0m9aks8ka9iwr25y1qkbgnly0xhnyp464zmqjwq2p8lkrhdf09bk")))

(define-public crate-unborrow-0.3 (crate (name "unborrow") (vers "0.3.1") (hash "0zynygv2c37zcn98n8xw2y5gjiibinsmmlbhbpi8wkwy0agrabp9")))

(define-public crate-unbothered-gpio-0.1 (crate (name "unbothered-gpio") (vers "0.1.0") (deps (list (crate-dep (name "cancellable-timer") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gpio") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1h640xh21apap5q29gki5i7vg2m1kq1p9zmjnzzcnr209js17jpg")))

(define-public crate-unbound-0.1 (crate (name "unbound") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unbound-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "06c6ka6fbjdgixrz57y6219f1qrsyai8xlnd33qrn4rx5fai4gg3")))

(define-public crate-unbound-0.2 (crate (name "unbound") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unbound-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1g0qh6dgxmh5s183z9zb9jkswqnsr2dkg403s7z5mrjf4ir0ksxv")))

(define-public crate-unbound-0.3 (crate (name "unbound") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unbound-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "038mixa5d16xrrpa3a6ck03bg2rp3q61rrwknfv85ly8rqhwxs8d")))

(define-public crate-unbound-0.4 (crate (name "unbound") (vers "0.4.0") (deps (list (crate-dep (name "gcc") (req "^0.3.41") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 1)) (crate-dep (name "unbound-sys") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jmzlzyihf3vmqcxdma5g11b7pq6fff22px5yqmczg2maa22xajx") (yanked #t)))

(define-public crate-unbound-0.5 (crate (name "unbound") (vers "0.5.0") (deps (list (crate-dep (name "gcc") (req "^0.3.51") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 1)) (crate-dep (name "unbound-sys") (req "^0.5") (default-features #t) (kind 0)))) (hash "0slcp27ikia5vyh44i8z3igi6mj6g1z6d891p4d9jw5k4983d436") (yanked #t)))

(define-public crate-unbound-0.6 (crate (name "unbound") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 1)) (crate-dep (name "unbound-sys") (req "^0.6") (default-features #t) (kind 0)))) (hash "125z3hhikyx4iz0h01dkhh5xy84x8h6h6xgg8f9kcqf32lgb859s") (yanked #t)))

(define-public crate-unbound-sys-0.1 (crate (name "unbound-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.7") (default-features #t) (kind 0)))) (hash "1a08wdg713qdwqvmrndclcjrcib77rz73yzgdqp8fhik58vjnqw2")))

(define-public crate-unbound-sys-0.2 (crate (name "unbound-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.8") (default-features #t) (kind 0)))) (hash "1hwrw5q0fa495lysbxy58aadqv7m0j5hvshbrwy4ccqglp409xpk")))

(define-public crate-unbound-sys-0.3 (crate (name "unbound-sys") (vers "0.3.0") (deps (list (crate-dep (name "libbindgen") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)))) (hash "0pqbg13skpi3f3s8nzxlxkjgv42xjs1qwkd4k7pvnhc1qy1656xq")))

(define-public crate-unbound-sys-0.4 (crate (name "unbound-sys") (vers "0.4.0") (deps (list (crate-dep (name "gcc") (req "^0.3.41") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 1)))) (hash "0w87ymkwprcnzjyvqcgjawzx7aq7n77jvh3kcr5ngddr1wb7gs8x")))

(define-public crate-unbound-sys-0.5 (crate (name "unbound-sys") (vers "0.5.0") (deps (list (crate-dep (name "gcc") (req "^0.3.51") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 1)))) (hash "0ma71bh258dwwqjp66f60iy351vnf5b3crp1jq5zvnb292vxi8xg")))

(define-public crate-unbound-sys-0.6 (crate (name "unbound-sys") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 1)))) (hash "0y3g439x43fbwlnqa0brsvjrkpv22pwyh5bj0rqcrqvfvjxj4qad")))

(define-public crate-unbounded-0.0.1 (crate (name "unbounded") (vers "0.0.1") (hash "071slcrjmi0f6378l5lwqml3dgs4lw91rlg9nklwcydma7j8zn5j") (yanked #t)))

(define-public crate-unbounded-0.0.2 (crate (name "unbounded") (vers "0.0.2") (hash "0j8bhjfzvzzmcl48dnaj4c6cmp0qnayfilh9bfxgzjf9npld189q") (yanked #t)))

(define-public crate-unbounded-0.0.2022 (crate (name "unbounded") (vers "0.0.2022") (hash "10y265s5miq90jlzg0cjfhab525zkmhgar2cn468f513lad8hq7m") (yanked #t)))

(define-public crate-unbounded-0.0.3 (crate (name "unbounded") (vers "0.0.3") (hash "1cn2za8cmrnykfhk8kr7x183j8cl2l6g45i3mndsiqs94cn580rq") (yanked #t)))

(define-public crate-unbounded-0.0.4 (crate (name "unbounded") (vers "0.0.4") (hash "1y3rrg4gz1gfahjm8mmgxhjf02np7n55j4ys23c65hj9bc9cml7b") (yanked #t)))

(define-public crate-unbounded-0.0.5 (crate (name "unbounded") (vers "0.0.5") (hash "1nbx8nk8xiwb22kv7p34bvw1prr6rl0hszhybx3bsnvg2q696syd") (yanked #t)))

(define-public crate-unbounded-0.0.6 (crate (name "unbounded") (vers "0.0.6-dev.0") (hash "1zsif3rgfljaa2ijz67gabr4q7hw58syyb7fj65i39yagj7qb0ij") (yanked #t)))

(define-public crate-unbounded-0.0.6 (crate (name "unbounded") (vers "0.0.6") (hash "1igvxmcwvgh9lxzrfx0psnbsdq1hs201ig70d10i73c0ygdllf2c") (yanked #t)))

(define-public crate-unbounded-0.0.7 (crate (name "unbounded") (vers "0.0.7") (hash "1alr4j1mki3v2b9rma4cxjqrr339q79v18xqkz7xcwsqjcvvk3s6") (yanked #t)))

(define-public crate-unbounded-0.0.8 (crate (name "unbounded") (vers "0.0.8") (deps (list (crate-dep (name "you-can") (req ">=0.0.8-dev") (default-features #t) (kind 0)))) (hash "065d310p0y30nadm2vfwan5sdymw2qv2nfq3vg1ppasx8hi64zr7") (yanked #t)))

(define-public crate-unbounded-0.0.9 (crate (name "unbounded") (vers "0.0.9") (deps (list (crate-dep (name "you-can") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1qlmfykhnra72qsqgq2cfn2wi6fvs965xw3fa1csk92mvr6kllfb") (yanked #t)))

(define-public crate-unbounded-0.0.13 (crate (name "unbounded") (vers "0.0.13") (deps (list (crate-dep (name "you-can") (req "^0.0.13") (default-features #t) (kind 0)))) (hash "1acczl3zyc6pjhnfwf14j2z6wwxa190248nw4c93fbg8675424wz") (yanked #t)))

(define-public crate-unbounded-0.0.0 (crate (name "unbounded") (vers "0.0.0--") (hash "0vfifavymgxn7j2pvw043acannfb6kl36v8hjrza8wj177fsg6p2") (yanked #t)))

(define-public crate-unbounded-0.0.0 (crate (name "unbounded") (vers "0.0.0-removed") (hash "0ha21s9jfj2mbll1rnwida9dpl9rk2l81978hv2sy47fv8ciy6fy") (yanked #t)))

(define-public crate-unbounded-gpsd-0.1 (crate (name "unbounded-gpsd") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ryk61n9k4zcip73ljj58319j27rndpsrsz2l1wblvnpl9226v4a") (yanked #t)))

(define-public crate-unbounded-gpsd-0.1 (crate (name "unbounded-gpsd") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qsahi40wdnlnplwhy3fr68x93gk6ymmasa169c3rh1gm09xn22j")))

(define-public crate-unbounded-gpsd-0.2 (crate (name "unbounded-gpsd") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "052n81hxpklsa2ad3ygclnakiizab6vzmj6skj5hnnw7342p8yd9")))

(define-public crate-unbounded-gpsd-0.3 (crate (name "unbounded-gpsd") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "03jy9sp2y9xm7jxf9wi2x8w5cf6582aaniq0b47iykj8rv1i2a78")))

(define-public crate-unbounded-gpsd-0.4 (crate (name "unbounded-gpsd") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ffhglbj750393vmcfwcbgj833cx652c2m06rz65gm6y3m93w6fa")))

(define-public crate-unbounded-gpsd-0.4 (crate (name "unbounded-gpsd") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02hc9qjyigm25gjwh94fhn20h7dla43g7qw2k1v6bbf2hdczw7sh")))

(define-public crate-unbounded-gpsd-0.4 (crate (name "unbounded-gpsd") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0a6kqhmggdgpg67sf47facf0lrl4k7bbjxjqy7pk12z2p80p7frm")))

(define-public crate-unbounded-gpsd-0.4 (crate (name "unbounded-gpsd") (vers "0.4.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rq6rf86hagi0zh0ys6i8c5c6hy39jhxc08i5cbnk38mz8rrmh19")))

(define-public crate-unbounded-gpsd-0.4 (crate (name "unbounded-gpsd") (vers "0.4.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b3knayrd3ffqiafr07f80mfq8ccavfgwk1w9x1jd3vgzzkkrlr2")))

(define-public crate-unbounded-gpsd-0.5 (crate (name "unbounded-gpsd") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lk0y46hjaf8vm8nvfv8isswri01qcdg982fkkgngky8zyyrvx8a")))

(define-public crate-unbounded-gpsd-0.5 (crate (name "unbounded-gpsd") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09hx7q3hrdb7xsx4hl003xkmskg0cx12isww069b1azr431jnrzz")))

(define-public crate-unbounded-interval-tree-0.1 (crate (name "unbounded-interval-tree") (vers "0.1.0") (hash "16iklrfybn0fw89469pb78p9002kxxnrzz4kbk9mvck8jlw7ajva")))

(define-public crate-unbounded-interval-tree-0.1 (crate (name "unbounded-interval-tree") (vers "0.1.1") (hash "0yl1w47wi49a79z1rh634pmkqcdbn9prqphqknrs6jl662mcg5zh")))

(define-public crate-unbounded-interval-tree-0.2 (crate (name "unbounded-interval-tree") (vers "0.2.1") (hash "1h8r8g48qvwhrfmxm6gayf6dvlfga5l41wbqnkvm495bg9q0ysy7")))

(define-public crate-unbounded-interval-tree-0.2 (crate (name "unbounded-interval-tree") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "00542xfa36kp27vl28xhs4ix54krilgnjkj08ahh3ylamyajpqfs")))

(define-public crate-unbounded-interval-tree-0.2 (crate (name "unbounded-interval-tree") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "16naqq2kdps2ccra3am5d95nmli0ggb6mbal52fvjpvx0x6fagn7")))

(define-public crate-unbounded-interval-tree-1 (crate (name "unbounded-interval-tree") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ci3x41m7bciqkl6fb0dswl1h11408swnsj8nx4l8my75p78qysa")))

(define-public crate-unbounded-interval-tree-1 (crate (name "unbounded-interval-tree") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "140aawasallimndmbvpja46amlva2a4h3v0cc4qmcl0n5p1hfr5d")))

(define-public crate-unbounded-interval-tree-1 (crate (name "unbounded-interval-tree") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0019fz49jia2vb2schcr3mf3dd0wps9dz78k5fjnkhzrz2x8kzad")))

(define-public crate-unbounded-interval-tree-1 (crate (name "unbounded-interval-tree") (vers "1.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0r0jq4csfx5sbavj89g5iacqcw2ajai511smgzyvc5x61dyrdb0g") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.2") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0vrbp3wispbw531ljcsy2cg5db9m856xrydb975dygy3qzcankl4")))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.3") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.2.*") (default-features #t) (kind 0)))) (hash "18na5sivg34zsgz12arx3acwraplksgc2f137zwxa1q9ixx3rv2m")))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.4") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.3.*") (default-features #t) (kind 0)))) (hash "159d03gq887avwkv5ppsdx288xgx9nmlkd9x68bk4q37i0xplvjs")))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.5") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.4.*") (default-features #t) (kind 0)))) (hash "1nzd9b9pbwyz937qcvc9rzgx7nndlc6wz3ll5yzr91dncr3rf38n")))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.6") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.4.*") (default-features #t) (kind 0)))) (hash "01hq3cnk007kmg9kkay07bshr2s4jrgqp2dn370q5bpf2zy5rlsx")))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.7") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.4.*") (default-features #t) (kind 0)))) (hash "0vv41j1zhahxcnih74fz7lbadbpzgwf99iyfbm77x7g3np1axsj2")))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.8") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.4.*") (default-features #t) (kind 0)))) (hash "0fah83yhvc70g33h79wwqdrjrv538hnv1x13m5r0mwxwy6hqfdqy")))

(define-public crate-unbounded-spsc-0.1 (crate (name "unbounded-spsc") (vers "0.1.9") (deps (list (crate-dep (name "bounded-spsc-queue") (req "0.4.*") (default-features #t) (kind 0)))) (hash "1ryv7bl8pqc05slrh17cszzpj9fvdvfa7vmpf9hv60z4h91kxnq0")))

(define-public crate-unbounded-udp-1 (crate (name "unbounded-udp") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.123") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows-sys") (req "^0.35.0") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1lamg0k8k7w49zb81ixmipwnmm5mvf90jcgcnrcfr7ncylzv3jl4")))

(define-public crate-unbounded-udp-1 (crate (name "unbounded-udp") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.123") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows-sys") (req "^0.35.0") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1szfgcj5803lxgcbdgpkfzcy1rhar25yvbhyk9lgyzhx4wfjr0b8")))

(define-public crate-unbox-0.1 (crate (name "unbox") (vers "0.1.0") (deps (list (crate-dep (name "bzip2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "tree_magic") (req "^0.2.1") (features (quote ("staticmime"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0kjpzdgji7xi410ppjhfram2fi1a0blpmva2amncd16ss5066a9g")))

(define-public crate-unbox-0.2 (crate (name "unbox") (vers "0.2.0") (deps (list (crate-dep (name "ar") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bzip2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "cab") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.0.19") (features (quote ("pe32" "pe64" "std"))) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "tree_magic") (req "^0.2.1") (features (quote ("staticmime"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0rfx1p6qj54g6wb82zbkvyxfm5hgbcayj30kljbw9wrrjlbcg4r3")))

(define-public crate-unbox-box-0.1 (crate (name "unbox-box") (vers "0.1.0") (deps (list (crate-dep (name "assert2") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "08x91hrrji3hv4f2k4kpmwmk0sc2vaf0a0i26hsyc510bdq0p5x9")))

