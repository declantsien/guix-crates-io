(define-module (crates-io un or) #:use-module (crates-io))

(define-public crate-unordered-hash-0.1 (crate (name "unordered-hash") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "04yw8wmh8kgjn8agavibh6lw6hcavnnxms5nb8j5zlbv4q0qxf19")))

(define-public crate-unordered-n-tuple-0.0.1 (crate (name "unordered-n-tuple") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "00jwwn77zfszn8kdcbq6arw3zr0jymg155mp0gz3aj0lg5zazkdm") (features (quote (("std") ("default" "std" "serde"))))))

(define-public crate-unordered-pair-0.1 (crate (name "unordered-pair") (vers "0.1.0") (hash "092dqz45cw403c8c1yjh25imb292a4ydgh4lff618aqww4lfigl7")))

(define-public crate-unordered-pair-0.1 (crate (name "unordered-pair") (vers "0.1.1") (hash "1yz8f3rr89cq09rnpzda3m8ahkbgpi4qz8ia7d7vglk0x6zzy03m")))

(define-public crate-unordered-pair-0.2 (crate (name "unordered-pair") (vers "0.2.0") (hash "16359j1a3dbvpy4kbphd07p51l9xn4j76vzjpbgsdf6sk162ia3z")))

(define-public crate-unordered-pair-0.2 (crate (name "unordered-pair") (vers "0.2.1") (hash "1ai8gqyzbqbwhnh98wzb81rhxxy0vc4c2h1lfx7qwsa8vf1fbiwi")))

(define-public crate-unordered-pair-0.2 (crate (name "unordered-pair") (vers "0.2.2") (hash "199slsn4029vw83xi5fs21g35kxbwl2m90mqvdqwcaq4fr8wzzrd")))

(define-public crate-unordered-pair-0.2 (crate (name "unordered-pair") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ww45i8i6yjn0br8jgh5z910mxn9vmp6xs40gpvm5sy3cb94wd4m")))

(define-public crate-unordered-pair-0.2 (crate (name "unordered-pair") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1v5h27bglhb7ln1vqfp0qrm4cavkvwmc5jrfwjs1bpd77q4k8b6q")))

(define-public crate-unorm-0.1 (crate (name "unorm") (vers "0.1.0") (hash "11njr125g26jk2k24p3whv7z92y1kar1n5qfmarnmsczzrs1pxzi")))

