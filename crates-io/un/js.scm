(define-module (crates-io un js) #:use-module (crates-io))

(define-public crate-unjson-0.0.1 (crate (name "unjson") (vers "0.0.1") (deps (list (crate-dep (name "serde_json") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0ymhrhj7h84g2x2130djiska7rl1r5hh57azv1zpp0nv7pzvgq0q")))

(define-public crate-unjson-0.0.3 (crate (name "unjson") (vers "0.0.3") (deps (list (crate-dep (name "serde_json") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "088cypa9xi0ipl78ba16mj8np55qx6l6l3svp3pc3lrsp0apv2n7")))

(define-public crate-unjson-0.0.4 (crate (name "unjson") (vers "0.0.4") (deps (list (crate-dep (name "serde_json") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0d7hwkhlh6z0v9p2d4w3cjhf253ljidp4awrliyi9xzi2z4v2y58")))

(define-public crate-unjson-0.0.5 (crate (name "unjson") (vers "0.0.5") (deps (list (crate-dep (name "serde_json") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1iw7sp2i9y10ajg3zvlw0dawlr3cmvn9j8ibirn53afc8sn2l9fj")))

