(define-module (crates-io un do) #:use-module (crates-io))

(define-public crate-undo-0.1 (crate (name "undo") (vers "0.1.0") (hash "1l6zfrb97jwgl1m812hjx31iki2jz1d3fypxcyp36gwmsdvh5h8v")))

(define-public crate-undo-0.2 (crate (name "undo") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0qpc0b9g7wvg7br0b6jb4lr4b05bg4n9slkmzzwjallk2hg2iafl")))

(define-public crate-undo-0.2 (crate (name "undo") (vers "0.2.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1qjv60v93270z2cp2ckd2mdkx5k7qka5lyg39v9yal3g846sh8vj")))

(define-public crate-undo-0.2 (crate (name "undo") (vers "0.2.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "07kqj7dprvfxdm53h081rqhjrfrg6hpzjj95ccby1in6v93jlhi0")))

(define-public crate-undo-0.2 (crate (name "undo") (vers "0.2.3") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1kp5ivlfsqcjw86flp2rljfw4x726d43ikjx0jcyvv4yj7vshmhf")))

(define-public crate-undo-0.2 (crate (name "undo") (vers "0.2.4") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "02pqi0r2pkx9pjcmiwjlds4mc5rfa1m1ihms0kv61yqv6xd5g501")))

(define-public crate-undo-0.3 (crate (name "undo") (vers "0.3.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0v7h4lj87faa4saw8ylb7jp3fwhnj16h46pvh69rbi4qx7pwsi3r")))

(define-public crate-undo-0.3 (crate (name "undo") (vers "0.3.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0hrg1lrkyzawgywkykrmhgmr1nn9zg3ldc9g4hwry9z89483b6h0")))

(define-public crate-undo-0.4 (crate (name "undo") (vers "0.4.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "17k1w7adqc7kgixn5jmq1vd3xm572y2b0sdhw6zzxq2y60sxikac")))

(define-public crate-undo-0.5 (crate (name "undo") (vers "0.5.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0lgpxakngndp2mvscs2jfsj2nyfdrkgarf2j8hsbbi5rc5rc8yrx")))

(define-public crate-undo-0.5 (crate (name "undo") (vers "0.5.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0h1vlrhyspm9inylx8v2qha771i6ll4naxp7x0fsn3yp2ii837qc")))

(define-public crate-undo-0.5 (crate (name "undo") (vers "0.5.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "099249mashcp8i78fn3zx70yfshxh9nfrvrrdn50100h9idm4ywn") (features (quote (("no_state"))))))

(define-public crate-undo-0.6 (crate (name "undo") (vers "0.6.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1p4q30bp1fapx6cm41bfa578hpyhicr0h04rfd25frrg5qyy468v")))

(define-public crate-undo-0.7 (crate (name "undo") (vers "0.7.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1v4y3h3vlpki1q9b0riwhyg9w0sgrl80n9m5sckrnhy5fkgacd94")))

(define-public crate-undo-0.7 (crate (name "undo") (vers "0.7.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "15ch61d165m6w6d92mnaxplvyib8vwf7rmlpi3lax4n5jzfv3jgy")))

(define-public crate-undo-0.7 (crate (name "undo") (vers "0.7.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0i6zdh7mw4375x2v0llnxymy4sxsb8hl5nrqcylkfd1bsvaj2n7b")))

(define-public crate-undo-0.8 (crate (name "undo") (vers "0.8.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1lfsl1rxcmqw2ff88azmrc1v3hara49h0s80y9znrv5dgvqcgvh7")))

(define-public crate-undo-0.8 (crate (name "undo") (vers "0.8.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "11w1ny8dikynrzbyibx3jx2jnkshyzigfia3xfab7kxss65xyizd")))

(define-public crate-undo-0.9 (crate (name "undo") (vers "0.9.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0qx0ggfswpf7chawp3a9lgr7g8lssv6axkx5yc5hy7hbh7b69zhi")))

(define-public crate-undo-0.10 (crate (name "undo") (vers "0.10.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0dsi9q15jxhhpgna31q7ifx5a6g6pvjnwy5wp6npnmc3zwy1d49v")))

(define-public crate-undo-0.10 (crate (name "undo") (vers "0.10.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0az6jvbnfa4gsjg5824gid16ls9akn6x4bni45vfc723g0abl2vn")))

(define-public crate-undo-0.10 (crate (name "undo") (vers "0.10.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1kcn3jq7k0zrrw68ig06inx2ifxh84qbsjlidw7fmnn4ns5p8j29")))

(define-public crate-undo-0.10 (crate (name "undo") (vers "0.10.3") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1hxrd61qcqbwhyvmdzmwbhxs9rs1gj49h6pddbkrrg0abhla8i84")))

(define-public crate-undo-0.11 (crate (name "undo") (vers "0.11.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "07bmf91ndk0yav9v7334mniybjghvzc7x91w9ri4d5s7cx2ia54h")))

(define-public crate-undo-0.11 (crate (name "undo") (vers "0.11.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0sd2inv4f0fa95xsf88z2fm1ajlnncf7xsj3yi0q2w7yrzb1fbmw")))

(define-public crate-undo-0.12 (crate (name "undo") (vers "0.12.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1szr8p63ssvlv0mwssbzp7ss8lxppcj28m2p6ynnk4gnnd96w5mk")))

(define-public crate-undo-0.12 (crate (name "undo") (vers "0.12.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0gy0xzfz7gnhq6072pvb7j5aqz846zcd1d0zad7gp0ydslmiay9a")))

(define-public crate-undo-0.12 (crate (name "undo") (vers "0.12.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "06cdlvj22x6xr88gy7x9l7klkhr1vl4rs0hj9d1l63ynlnmf58y9")))

(define-public crate-undo-0.13 (crate (name "undo") (vers "0.13.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0pfjy0bl6f5jd7z55913wk9kpgplvigra4p4r9f98wfjn4drnixw")))

(define-public crate-undo-0.14 (crate (name "undo") (vers "0.14.0") (hash "0drxzdvqyf6x5lg6pxzf2bqrclxmg4vaxqhfycmxxpwp9kdx4g0b")))

(define-public crate-undo-0.15 (crate (name "undo") (vers "0.15.0") (hash "0q1g3xiqvlz3jdsl59f2aamnkpjhj3sh69sa1mc5ai9hm0lq8q4s")))

(define-public crate-undo-0.15 (crate (name "undo") (vers "0.15.1") (hash "00nbchqxhh9yafbfc4hy4z9aw3fxaw0bkysal94s045wfvj57xw0")))

(define-public crate-undo-0.15 (crate (name "undo") (vers "0.15.2") (hash "0zfvpgdlqn9yvdwvm1p01akfhyis1anh26pp7nv4gg8dlcr73ign")))

(define-public crate-undo-0.16 (crate (name "undo") (vers "0.16.0") (hash "12xpxvx199nxybswbkxbqf4iyyqgsiqbn78nc1g17p6i13skrxmb")))

(define-public crate-undo-0.17 (crate (name "undo") (vers "0.17.0") (hash "0mn6w8879c6cmffi917a4j9rvs222fdxrfmi5k13w53s4k66jras")))

(define-public crate-undo-0.17 (crate (name "undo") (vers "0.17.1") (hash "0b7x57xwiqa1pi8ff7h1x4xv4dmbaa78ahmnmyc86basadvhhrsn")))

(define-public crate-undo-0.17 (crate (name "undo") (vers "0.17.2") (hash "1fdqx0agdkzwkyq9ij5knwsn4c9n6ixjd4nl7d56s8slwc5v39bq")))

(define-public crate-undo-0.18 (crate (name "undo") (vers "0.18.0") (hash "1gh7fa3i4jwiwb1ymjc68jx2gz6skspj6mw88m8kly3pcv4j1k0j")))

(define-public crate-undo-0.19 (crate (name "undo") (vers "0.19.0") (hash "0diz2qa107905s93k32hkqc0v2k7aa3ib3v4d9isjy7k3zkzxzh0") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.19 (crate (name "undo") (vers "0.19.1") (hash "1lpf0n8pp7kwb2xk49rgd8mwvpkg8qbcffq0q1msvh56ildcyzpq") (features (quote (("display") ("default")))) (yanked #t)))

(define-public crate-undo-0.19 (crate (name "undo") (vers "0.19.2") (hash "0rn4vaqkx7sxqs016bsij08asxrkhdkylk66hqkxafjgy0l1ccxa") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.20 (crate (name "undo") (vers "0.20.0") (hash "1j0vx1lq9m84w9ifk6iq2n5jxbyv7r78ll4n2mpdd0rbkjlj7289") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.21 (crate (name "undo") (vers "0.21.0") (hash "089bwahzzxzkgswrnds6klj6bmb0v21va15gcqnbr38njqf4y3g2") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.22 (crate (name "undo") (vers "0.22.0") (hash "10gwyj92202mvryk20fkxwml6alf482w0vkqmdw1hdhpngwgvvv6") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.23 (crate (name "undo") (vers "0.23.0") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "1pwjkfymnsg6jllim5kkiddbzgslp07d0v1incd00y5rfx3xjnc6") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.24 (crate (name "undo") (vers "0.24.0") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "0m05pmwhyqxic9v8hvc5cxgbraxi7cq91wq4nvir55y2ir0a7vcv") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.24 (crate (name "undo") (vers "0.24.1") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "0hfbd34p76g1n4qr8i1650fp30a0aqsm0xa4plw4ild51sv7g8ss") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.24 (crate (name "undo") (vers "0.24.2") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "0d6xd4kch5x29xsbi1gar39xj9fpqs26cc06dyvdnrqkaw67qlby") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.24 (crate (name "undo") (vers "0.24.3") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "0r9zk8rfn6f32mcfbymns4fhw41pwsa7fwnnx2pbahycwb6dw538") (features (quote (("display") ("default"))))))

(define-public crate-undo-0.25 (crate (name "undo") (vers "0.25.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "189zq4ivks5kapj55lmkp0k3cby0kwjik953sv1mjk4n82k9milf") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.26 (crate (name "undo") (vers "0.26.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "150b280mx6zw6h02v5rxmxs3zqysfnshzvwyhiv3ns00xif87vf1") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.27 (crate (name "undo") (vers "0.27.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "0x9z0i85gnb7dgwk85s90486fvcpqm7lr8laq0xyd57dr0vkai50") (features (quote (("display" "bitflags" "colored") ("default" "chrono"))))))

(define-public crate-undo-0.27 (crate (name "undo") (vers "0.27.1") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "11f50jsj80kdlmwr20b0lpibbyjjgf0vwkk0dwva4y6gl5bigb5c") (features (quote (("display" "bitflags" "colored") ("default" "chrono"))))))

(define-public crate-undo-0.28 (crate (name "undo") (vers "0.28.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "1dnyldcirapqrn7njmvvlc0mz5q8l8q91k94gbvpdg4xss3c312m") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.28 (crate (name "undo") (vers "0.28.1") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "18mq88zfl819wrazj1p7rm8sgika97xdm4nswq0ga2ymvazqchis") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.28 (crate (name "undo") (vers "0.28.2") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "07lq5v3bvlv62912jwxh33djgb1lv3h2gggfgfj2nlyqqkq176jx") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.28 (crate (name "undo") (vers "0.28.3") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0nav6ga0m6a6fnaw8q8ir89q3g6mk0a1j7fjflrgf8717n00h90j") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "029jjg2hgwkl0dsl8qzd2mzzcrycg28kral9qb415nqfx0az0p8z") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.1") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "14g2z8j7vbrwpvdp09h8m996ar1a3nw4cyj7dp6w090cr6gzshic") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.2") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0smhwhhqws8nqzwi24jrbfxs7nggljpcghsfrw3wv7y95br7xgcl") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.3") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "127qyhkp5gz8h7fbjkz59hxvnl8gqpfg8cpvr51kh030vj03n7fc") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.4") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "03ibndrdsqjcrvsb8bskrkk2vw4qf7xxz762zvqsx5qj0v22r00y") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.5") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "185y1klld7cgm6rc8ai2rlpxz8m4ijg0kd9gq48lpwrazjsmb6w4") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.6") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "1r6gf2qaaby2ias2nzfz7v85qdlj0pslgjplmq0a4szahj8vkn56") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.7") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "12a0imsf0ljmfc1zr3nd0gsw7df7p5sfm27mj825b387vcqyz6qk") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.29 (crate (name "undo") (vers "0.29.8") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0fminag35yfm0s2iva111dhhiachak17wxs87s5vwxcc8c4c1h8c") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.30 (crate (name "undo") (vers "0.30.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0sq79xd6acq7wn5v82q0zl00n6spb341pa4fj3fpm28va93yg00r") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.30 (crate (name "undo") (vers "0.30.1") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0b54nnv0ppdm90g4l5gl4yf0k7rqw7x0w2zpg8mhydfjzsa9z2z5") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.31 (crate (name "undo") (vers "0.31.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0bb4sl2jpvlfw5rycm61yp9l6wgwivq9nd7fl10n0mwx6wghrgv3") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.31 (crate (name "undo") (vers "0.31.1") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0vklv279ipg2msfv0gxvjv260pz9ffw26h4vqy177ri55ps5jagi") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.31 (crate (name "undo") (vers "0.31.2") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "1syxhhmgbw3ah8fhdf1m8zhkawvhgg0k71sf2ldskqhn40zzd92a") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.32 (crate (name "undo") (vers "0.32.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0rv8mif8hxqzw6c6szi982ivbj1i6lgbjiqrz6r3j0a2iy931fca") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.33 (crate (name "undo") (vers "0.33.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "1gsq422hhb4hpl3a4m0h5mfr6qmsp9ljnam2byqyzaw8naxjcpc3") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.34 (crate (name "undo") (vers "0.34.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0gby3vcr6kz9m1dgg7p2ybfd91xnmrw6gzp5yj5wg2k9gd041k0g") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.35 (crate (name "undo") (vers "0.35.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "09i7fdgy7gn0ignjp1b1nd7w7f0rlx4926a1kx3mkmn4v2v8hi1p") (features (quote (("display" "bitflags" "colored") ("default"))))))

(define-public crate-undo-0.36 (crate (name "undo") (vers "0.36.0") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0sgan18np23bdk7w3b9iqq46p92gp48m1zdw10jld05202np25k6") (features (quote (("display" "colored") ("default"))))))

(define-public crate-undo-0.37 (crate (name "undo") (vers "0.37.0") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "1qirsbrv08z617adwf6k37p94rqh315jr8yf89zbgm8wqqv0rdwv") (features (quote (("display" "colored") ("default"))))))

(define-public crate-undo-0.37 (crate (name "undo") (vers "0.37.1") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)))) (hash "0mwagm6fql4a1r8zlkbkabvkr3y9lxil73vyr8lkzkvipb7xm0vl") (features (quote (("display" "colored") ("default"))))))

(define-public crate-undo-0.38 (crate (name "undo") (vers "0.38.0") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1cxv8ajklbcwjlqdack13zqlq1rr75qcfhix6344g78i9j8x8hyf") (features (quote (("display" "colored") ("default"))))))

(define-public crate-undo-0.39 (crate (name "undo") (vers "0.39.0") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "09rivrq3zd6vn6jbwnmzyy76x8h0mp1iqvy0cb05v05gffjcjji2") (features (quote (("display" "colored") ("default"))))))

(define-public crate-undo-0.40 (crate (name "undo") (vers "0.40.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1") (default-features #t) (kind 0)))) (hash "0ink031saz45vcwqr9layb7lgb2h9k4zaaa3ywfp7d23rdssy8sg")))

(define-public crate-undo-0.41 (crate (name "undo") (vers "0.41.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1135zg90w0dl5z9xz8gj43in2rzhc33p0wspx5157lpbnpm491gv")))

(define-public crate-undo-0.42 (crate (name "undo") (vers "0.42.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)))) (hash "1h800cfib0ajgg4vbv8cwh75n5a8g8xr8xrwfaqnccpp305d5d6c")))

(define-public crate-undo-0.42 (crate (name "undo") (vers "0.42.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)))) (hash "13xxbfzvk5bfwcswps3rpqaiy57m7k3s6wzbckf0ha7wy445p8k8")))

(define-public crate-undo-0.42 (crate (name "undo") (vers "0.42.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)))) (hash "1bfvd9w9rihibvcaizadzfmwm9b5zbmx3kbbzmgjh999c8qbzqgx")))

(define-public crate-undo-0.42 (crate (name "undo") (vers "0.42.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1fxkxqqg8r4vihikx56lmd336i08dd54srw4da1ahxjbqg310k6h") (features (quote (("default" "alloc") ("alloc" "serde/alloc"))))))

(define-public crate-undo-0.43 (crate (name "undo") (vers "0.43.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "0rdbkpy654yiv3ipn1sjmwg4xfq0vv503giq8xk68l76xscihimh") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.43 (crate (name "undo") (vers "0.43.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1cia5c7sbazsda72y79yck8lra9nghganhzk4pw0csdxlyyxgi29") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.44 (crate (name "undo") (vers "0.44.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1f7x644yzpj0a42lvdp4y0gjcfwsc31cd8qsh7qg47zrq28w69lg") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.45 (crate (name "undo") (vers "0.45.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "0n5k4mfngxnai4c8yyy5dy2d7r4f9nv9ymahhyv1l63lpcrgc3kj") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.45 (crate (name "undo") (vers "0.45.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "17xx91mh6w94863jpmqnaj4g4yjjyqa68kkpbkp7s1clx4cahj9a") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.46 (crate (name "undo") (vers "0.46.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1jflhi24f0xz4zfawdfy92wn7k4jwzdyj158yxxq09cw1wdqmks6") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.46 (crate (name "undo") (vers "0.46.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1y6cza73vysbrpw8f01l2lsk06db7q601rc0pbab9q6p5qm2qmyp") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.46 (crate (name "undo") (vers "0.46.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "0zff92r30zzyppg4mp2gmr8sv1wx0vhnwdchqh3cwny3w94f1z9l") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.46 (crate (name "undo") (vers "0.46.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1x5jy31bgxvlpvn2npzy0apxz6j90lhrnvfnw33i24jcrki8n0z0") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.47 (crate (name "undo") (vers "0.47.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1d3vcgrmza014gjhkb38f1basrhhkc3gap3ay86s9lmdich16n4y") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.47 (crate (name "undo") (vers "0.47.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1qhdz4pchh0l4k5ihfx92316pw0g4lwsz08hn4hqbgprwc8bvb6v") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.47 (crate (name "undo") (vers "0.47.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_crate") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")))) (hash "1hjp9hfdb15cyshjc9pnaqivq5dx8l0qmjyxa22h4ddcydid5ccx") (features (quote (("serde" "serde_crate" "chrono/serde" "arrayvec/serde") ("default" "alloc" "arrayvec") ("alloc" "serde_crate/alloc"))))))

(define-public crate-undo-0.48 (crate (name "undo") (vers "0.48.0") (deps (list (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (features (quote ("formatting"))) (optional #t) (kind 0)))) (hash "1ga2a139lg1sknwzpc543x4l39q2x69d4cy1596yyir15rzi2867") (features (quote (("default" "alloc" "colored")))) (v 2) (features2 (quote (("serde" "dep:serde" "time?/serde") ("alloc" "serde?/alloc"))))))

(define-public crate-undo-0.49 (crate (name "undo") (vers "0.49.0") (deps (list (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0b6jk3pg90w0ym7nxv853w9apyzv90b894bnm0laj0hf5hmbg48j")))

(define-public crate-undo-0.50 (crate (name "undo") (vers "0.50.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0q3f4ql43dx30bppsw15djlvss4h5swialzmqbf8hx70qwnkc868") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "alloc" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-undo-0.51 (crate (name "undo") (vers "0.51.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1dl5nq632d3k52hfq17i5szaxz7zx8dldz1nbh15agg3m2lf12yd") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "alloc" "serde?/std") ("alloc" "serde?/alloc"))))))

(define-public crate-undo_2-0.1 (crate (name "undo_2") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0f6cxgfg2hg0pdaxx82v37xk0nz19cfyj418gr87znfbkaam3d6a") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.64")))

(define-public crate-undo_2-0.1 (crate (name "undo_2") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1ksqj5q7xb2y0pcj9ryh64vr9qjx333q5f7py246sydflmnc7ja6") (features (quote (("default" "serde")))) (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.64")))

(define-public crate-undo_2-0.1 (crate (name "undo_2") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1rv307ajq0rw0h6z28fbhxj6vxyfy3sqsvjhnlgf40adc7jfcbnm") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.64")))

(define-public crate-undo_2-0.2 (crate (name "undo_2") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1hqnakyv9wq5gz7zx2zncb31ldysv09ar5avfd24760d8wpxsmck") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.64")))

