(define-module (crates-io un in) #:use-module (crates-io))

(define-public crate-unin-0.0.0 (crate (name "unin") (vers "0.0.0") (deps (list (crate-dep (name "cfg-if") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "~0.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0nyrrh0ifyvqa0a2l95wa49f0663jsm0fihk885v089sxcrdmh07") (features (quote (("use_serde" "serde") ("std") ("num" "num-traits") ("nightly") ("default" "std"))))))

(define-public crate-unin-0.0.1 (crate (name "unin") (vers "0.0.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "01zqh6n1iv3z7kb3bbvzxbwy6zc9szvvhx42a4rifwzxdn3svq4h") (features (quote (("use_serde" "serde") ("std") ("num" "num-traits") ("nightly") ("default" "std"))))))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.0") (hash "1zhzcxhlwa9b1f4z93rq1jpzasflp2sgz6xng7akik5sw46bw21m")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.1") (hash "06iysmak92clpv59gasncyi1sqlafxqvyzxhc906h0frgprmz4cp")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.2") (hash "191k7rshnlxwhlm18h49v4yrd730pfmxcp5kvfrn727sx9i3yx0h")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.3") (hash "1x21ilf78aqcq9xzb9b7i628wm10rhk0jp0chlv06rkc690l8jw3")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.4") (hash "1mf82l7nk8fjq4c0w3kghbalm1gc5p1l14g0g1r1kvf9j8md7l67")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.5") (hash "14s97blyqgf9hzxk22iazrghj60midajkw2801dfspz3n2iqmwb3")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.6") (hash "0hl9l4w9mhv5qacx7cirm6rarrphw35b5syw2plx13vz884dfhdg")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.7") (hash "1is1gmx1l89z426rn3xsi0mii4vhy2imhqmhx8x2pd8mji6y0kpi")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.8") (hash "1n3kpv78282dprvyiakyzy1qhv1qm06dg945jwvxm03kbnjp4iji")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.9") (hash "0i1sjxlhw7rbvq47xgk0lixgpnswfyks21ks6zgzfw75lccybzjj")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.10") (hash "04pihkk8r4jfcyanjar05plvkjgzkka1lza39ppnradlvri97vjq")))

(define-public crate-unindent-0.1 (crate (name "unindent") (vers "0.1.11") (hash "171may3v15wzc10z64i8sahdz49d031v7424mjsifa205ml6sxp1")))

(define-public crate-unindent-0.2 (crate (name "unindent") (vers "0.2.0") (hash "04wkgf2gbrm5l31hbsvlpz0l6cqy3mv46k5ffcn2q80k4a21lzn0")))

(define-public crate-unindent-0.2 (crate (name "unindent") (vers "0.2.1") (hash "0kw1yivkklw1f5mpcwakxznwzn6br2g3yvbwg7yfvxqzlmg0z8ss")))

(define-public crate-unindent-0.2 (crate (name "unindent") (vers "0.2.2") (hash "15bv56p929ffiwxgkzzc5f5gznp9qlzarjyg3mv6ddnhp4qxk1hg")))

(define-public crate-unindent-0.2 (crate (name "unindent") (vers "0.2.3") (hash "1km2iy6fr6gsh2wvr1mxz86pm4wrlh3fjkinb35qfi3mw5rpvpn7")))

(define-public crate-unindenter-0.1 (crate (name "unindenter") (vers "0.1.0") (hash "00ci95kn8rkn847s660s010cz6lzj91fb17vr1wl8b6b020dsmcd")))

(define-public crate-uninhabited-0.1 (crate (name "uninhabited") (vers "0.1.0") (hash "0r7fjshqx2yc9cslfzxcp9bsv103mzmwcdrvj1ml7dpcajps4s0v") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-uninit-0.0.1 (crate (name "uninit") (vers "0.0.1-alpha") (deps (list (crate-dep (name "require_unsafe_in_body") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "02kczvg0508igmj57k80ak72lxph7h6xfkcmb65rw7115z3r67ap") (features (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (yanked #t)))

(define-public crate-uninit-0.0.1 (crate (name "uninit") (vers "0.0.1-alpha-2") (deps (list (crate-dep (name "require_unsafe_in_body") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "1x8fs029a1rf09yfpcz73ghh2m3blpnlr2hajjrfxxdp1s440myp") (features (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (yanked #t)))

(define-public crate-uninit-0.0.1 (crate (name "uninit") (vers "0.0.1") (deps (list (crate-dep (name "require_unsafe_in_body") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r62z5ivp5vw793mddqb0x9bb7n4771w58villvzf9sqxzzcjl4n") (features (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (yanked #t)))

(define-public crate-uninit-0.1 (crate (name "uninit") (vers "0.1.0") (deps (list (crate-dep (name "require_unsafe_in_body") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04q75v0hnkm0jnqch06z2a52r9pms22z5asmqq9sf56nyjpxcbsf") (features (quote (("polonius-check" "nightly") ("nightly") ("downcast_as_ReadIntoUninit") ("default") ("chain")))) (yanked #t)))

(define-public crate-uninit-0.2 (crate (name "uninit") (vers "0.2.0-alpha") (hash "17lccqk7pk4yhmb2qn3r39av2rnxkxd6pl9bbr0i4i7rnf6xrj89") (features (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2 (crate (name "uninit") (vers "0.2.0-alpha-2") (hash "11p55pbm4m465dbbasv7pvgp5b5kp8yk6i6csy49h6l0lrlvxf16") (features (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2 (crate (name "uninit") (vers "0.2.0") (hash "0dib7ksijmipnlanj2l0c76yisiwfvs64l20hdmsbqx6fvbb5ixk") (features (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2 (crate (name "uninit") (vers "0.2.1") (hash "0fj1ilaqscmcj8bd37dnwp8vgpyc1fxq0g76prkl8xnzw4sf4c4p") (features (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.2 (crate (name "uninit") (vers "0.2.2") (hash "0d4gf92v6f2x3va162ra4iz92jfi3dfdzwghs7zlm1i4zw18lb4q") (features (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain")))) (yanked #t)))

(define-public crate-uninit-0.3 (crate (name "uninit") (vers "0.3.0") (hash "0ij1a6s58hixrc3z2f3ml8splagnaidyblpvwkqhbxf4r9xw085c") (features (quote (("std") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain"))))))

(define-public crate-uninit-0.4 (crate (name "uninit") (vers "0.4.0-rc0") (hash "1zw3lbhzb7slainqw60p5w2arphdkwcr856f6hyipmal17linzcl") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain") ("alloc"))))))

(define-public crate-uninit-0.4 (crate (name "uninit") (vers "0.4.0") (hash "1gydi7biypdvs0hxjhf5kimcxs2ax34scsgyxz48f81hcbs85qvc") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain") ("alloc"))))))

(define-public crate-uninit-0.4 (crate (name "uninit") (vers "0.4.1-dev") (hash "15wzqz3kll3vlcn3pj1bjj182bf85kjfffr8lxiyb6lbj2crvq3j") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("const_generics" "nightly") ("chain") ("alloc"))))))

(define-public crate-uninit-0.5 (crate (name "uninit") (vers "0.5.0-rc1") (hash "00cfp60jks60x2xn8p1rk9jh7p65j5x6l7krwak0zhy2x2in0fsv") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.5 (crate (name "uninit") (vers "0.5.0") (hash "0dh5vmr0rxg7rxbmz643p2a5rxvn5kdpkmmhh3chq7hhs19zmddx") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.5 (crate (name "uninit") (vers "0.5.1-rc1") (deps (list (crate-dep (name "extension-traits") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "01922qc2pd5rl8g2yxdrva67jzmgm961jw8fimh2120m8gvg6w9q") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.5 (crate (name "uninit") (vers "0.5.1") (deps (list (crate-dep (name "extension-traits") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1dlbv44c8cvgy4mjpz9pbxgjzy97d21rbzy72gndi9bcshp0y4ry") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std" "const_generics") ("const_generics") ("chain") ("better-docs" "nightly") ("alloc"))))))

(define-public crate-uninit-0.6 (crate (name "uninit") (vers "0.6.0") (deps (list (crate-dep (name "zerocopy") (req ">=0.1, <0.8") (optional #t) (kind 0)))) (hash "1lxhikbwzvvsh87bcmh3gdps19yz1zjyqix2ns1j4yw5bagx76x6") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("chain") ("better-docs" "nightly") ("alloc")))) (rust-version "1.56")))

(define-public crate-uninit-0.6 (crate (name "uninit") (vers "0.6.1") (deps (list (crate-dep (name "zerocopy") (req ">=0.1, <0.8") (optional #t) (kind 0)))) (hash "1l4z60kx00f7r6dq69p9qfdg9n3g043ckz1dffvbgl2cshljpbz9") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("chain") ("better-docs" "nightly") ("alloc")))) (rust-version "1.56")))

(define-public crate-uninit-0.6 (crate (name "uninit") (vers "0.6.2") (deps (list (crate-dep (name "zerocopy") (req ">=0.1, <0.8") (optional #t) (kind 0)))) (hash "1jb8040sv4f1aidsyjz65w0xh649m072wzw403ql9yfzpsmdm7rm") (features (quote (("std" "alloc") ("specialization" "nightly") ("nightly") ("default" "std") ("chain") ("better-docs" "nightly") ("alloc")))) (rust-version "1.56")))

(define-public crate-uninit-tools-0.0.1 (crate (name "uninit-tools") (vers "0.0.1") (deps (list (crate-dep (name "ioslice_") (req "^0.6.0-alpha.0") (optional #t) (default-features #t) (kind 0) (package "ioslice")))) (hash "15k1z8yckyhkswzrr65h82nri24ajx5isv5kv0bx47gmfh5i6y3b") (features (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.2 (crate (name "uninit-tools") (vers "0.0.2") (deps (list (crate-dep (name "ioslice_") (req "^0.6.0-alpha.0") (optional #t) (default-features #t) (kind 0) (package "ioslice")))) (hash "1324ka39q4cc737s5q99qsxhgj3nmmays88mp9k81wf6cfq2s10s") (features (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.3 (crate (name "uninit-tools") (vers "0.0.3") (deps (list (crate-dep (name "ioslice_") (req "^0.6.0-alpha.0") (optional #t) (default-features #t) (kind 0) (package "ioslice")))) (hash "15k7rvzpw70jp7pba6pj8sl7m0w7p01slb3h9knddya98s7dpd9a") (features (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.4 (crate (name "uninit-tools") (vers "0.0.4") (deps (list (crate-dep (name "ioslice_") (req "^0.6.0-alpha.0") (optional #t) (default-features #t) (kind 0) (package "ioslice")))) (hash "0bxffch1d1dws42x5rvcr4s7y98wssiyxx4fr9y67fgly27hx96m") (features (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.5 (crate (name "uninit-tools") (vers "0.0.5") (deps (list (crate-dep (name "ioslice_") (req "^0.6.0-alpha.0") (optional #t) (default-features #t) (kind 0) (package "ioslice")))) (hash "0vnyw8nvq8pdzg4rgjkqyjfk6fx0q7ggjhi3smqd3q8pssr3c0br") (features (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.6 (crate (name "uninit-tools") (vers "0.0.6") (deps (list (crate-dep (name "ioslice_") (req "^0.6.0-alpha.0") (optional #t) (default-features #t) (kind 0) (package "ioslice")))) (hash "1gipdb7v88bdr46lzlf8pmkklip6w5gv08nc9i4b3i7sqk35fxh6") (features (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninit-tools-0.0.7 (crate (name "uninit-tools") (vers "0.0.7") (deps (list (crate-dep (name "ioslice_") (req "^0.6.0-alpha.2") (optional #t) (default-features #t) (kind 0) (package "ioslice")))) (hash "0f8n79xqz1q495k6fa5nkji7f781l973a5x0b82l0s03437bfn7f") (features (quote (("std" "alloc") ("nightly") ("ioslice-iobox" "ioslice" "ioslice_/alloc") ("ioslice" "ioslice_") ("default" "std") ("alloc"))))))

(define-public crate-uninitialized-0.0.1 (crate (name "uninitialized") (vers "0.0.1") (hash "1w13lh26ww7ib2f4d05lk8pgnma1ccgccsgh0ldxrqqmk0dndz0j") (features (quote (("uninitialized"))))))

(define-public crate-uninitialized-0.0.2 (crate (name "uninitialized") (vers "0.0.2") (hash "10by0nyjl44a4y7y2lgv1h03yycbbwghnvs0932pd0n3252smhbl") (features (quote (("uninitialized"))))))

(define-public crate-uninode-0.1 (crate (name "uninode") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "unicode-normalization") (req "^0.1.17") (optional #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "1gc936wjlrs2kjqx1f8x2lvc2ckgx1swkp5l6lqhr4kh4s7d0233") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "once_cell" "log" "thiserror" "unicode-normalization") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninode-0.1 (crate (name "uninode") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "unicode-normalization") (req "^0.1.17") (optional #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "0wm3p8r1ayjapaiyvb5y57b1jp7nrzly5sqs73y36n4y8968xplf") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "once_cell" "log" "thiserror" "unicode-normalization") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninode-0.1 (crate (name "uninode") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "unicode-normalization") (req "^0.1.17") (optional #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "0d1f1z1kn9pn3lm8ac1z4j4v78c20yrvp0qylxmrmfldhc52pwc7") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "once_cell" "log" "thiserror" "unicode-normalization") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninode-0.2 (crate (name "uninode") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "unicode-normalization") (req "^0.1.17") (optional #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "1bk2h6zgv0gys02319fmgz6pgw42iw0h4b0l0fdc65r4p2gyql3w") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "once_cell" "log" "thiserror" "unicode-normalization") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninode-0.3 (crate (name "uninode") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "unicode-normalization") (req "^0.1.17") (optional #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "15yincghh7v6k530jnmax70dzwn5l55kfna78pwnngsfzkz7ws5j") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "once_cell" "log" "thiserror" "unicode-normalization") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninode-0.4 (crate (name "uninode") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "03b3r38nh9zbrhr6hra1phk7nhpz7pvkz7dnfih88sjf2gynw991") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "thiserror" "anyhow" "once_cell" "async-trait") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninode-0.4 (crate (name "uninode") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "0bbipvhnby7zcj8m02nin33k4nv6mw8z28a84xr0l4yz930d15sx") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "thiserror" "anyhow" "once_cell" "async-trait") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninode-0.4 (crate (name "uninode") (vers "0.4.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (optional #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive" "std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (optional #t) (kind 0)) (crate-dep (name "toml-rs") (req "^0.5.8") (optional #t) (kind 0) (package "toml")) (crate-dep (name "yaml-rust") (req "^0.4.5") (optional #t) (kind 0)))) (hash "1xdkifcms4z58215g6967hq3alpgklgv0l7l0dq9phzkrqa4317i") (features (quote (("yaml" "loaders" "yaml-rust") ("toml" "loaders" "toml-rs") ("serialize" "serde" "serde_bytes") ("loaders" "thiserror" "anyhow" "once_cell" "async-trait") ("full" "serialize" "yaml" "toml") ("default"))))))

(define-public crate-uninstall-0.0.0 (crate (name "uninstall") (vers "0.0.0") (hash "03n21ivbzdpm2m3h04db48d2dg7v6rma4l3lsg4ijimd2na48698")))

(define-public crate-uninterruptible-0.1 (crate (name "uninterruptible") (vers "0.1.0") (deps (list (crate-dep (name "signal-hook") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1gh6c8qkvr990q3jgjblrxdx3bkrlhqwz43xvdpr0qj4q49ynqg5")))

