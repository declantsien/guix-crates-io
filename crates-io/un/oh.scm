(define-module (crates-io un oh) #:use-module (crates-io))

(define-public crate-unohup-0.1 (crate (name "unohup") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1kg73dsksgx8yjxhdizcv1bmbmrmzcjiaf9i3ggviq2258dpdhac")))

