(define-module (crates-io un ta) #:use-module (crates-io))

(define-public crate-untagged-0.0.0 (crate (name "untagged") (vers "0.0.0") (hash "0h4wrrvjmsq5nlgjriispz3bpkh8v2w2djfvzpfhpbnsz1k5p7mm") (yanked #t)))

(define-public crate-untagged-option-0.1 (crate (name "untagged-option") (vers "0.1.0") (hash "12wmlhs7lj5fq7hqg6ff9lkglpyh9w2cja7k4zfvh9a2cwp23sin")))

(define-public crate-untagged-option-0.1 (crate (name "untagged-option") (vers "0.1.1") (hash "1rign7xhg9wgx57jyban2h3948nsfwhff841w5hqg1jyi5h3ymc9")))

(define-public crate-untagged-option-0.2 (crate (name "untagged-option") (vers "0.2.0") (hash "0hkl9jkg7v39k1pkkhk0nav0fn0n2lrw01nbznfny4ys88p9b599")))

(define-public crate-untanglr-0.3 (crate (name "untanglr") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0r45662qi0iy25anzfspw4why458mhrjwsxl2j6im4r3s2mxg1m5") (yanked #t)))

(define-public crate-untanglr-0.4 (crate (name "untanglr") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1hyjmppw5k7k8y63mcpi2p5xsc04m5braqkmnn3g1s82pbw8mf8s") (yanked #t)))

(define-public crate-untanglr-0.4 (crate (name "untanglr") (vers "0.4.1") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1qd2wmcx9gbyg4xmnfa4f26bnjkb26nln8yd9n0hb4rl9d0baa8s") (yanked #t)))

(define-public crate-untanglr-0.5 (crate (name "untanglr") (vers "0.5.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0lbhq2ykrx4pfwih36scpjz972rsk8llhhqqscq0ax9cjxr6vhhf") (yanked #t)))

(define-public crate-untanglr-0.6 (crate (name "untanglr") (vers "0.6.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0mpjh63hivshzy7m9kgf4gfksinmzxmr2222m7nhzmi8bhi31zzi") (yanked #t)))

(define-public crate-untanglr-1 (crate (name "untanglr") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "058nrbabh32bgqyyi8992y8x8crahdw124vqh02lz8fhm2bwvzkx")))

(define-public crate-untanglr-1 (crate (name "untanglr") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0ap4hx2ka2pzy3gfxzwfj7ychqq8953fx2x2k9s6rvplzhz92lfq")))

