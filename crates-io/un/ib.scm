(define-module (crates-io un ib) #:use-module (crates-io))

(define-public crate-unibar-0.1 (crate (name "unibar") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.5") (default-features #t) (kind 0)))) (hash "1qr2wcnmkas581qkg2a9k6v9g0pm23x4hi1sgh2mb7bblwl14k8k") (yanked #t)))

(define-public crate-unibar-0.1 (crate (name "unibar") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.5") (default-features #t) (kind 0)))) (hash "0rvfvmfncbgav5k31z03h1lcm8pbw377fw790p7cjzi93kvyzzq8")))

(define-public crate-unibar-0.1 (crate (name "unibar") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.5") (default-features #t) (kind 0)))) (hash "06vq3gdbbvyqxmai38cc940zgys2n1zwpg1h4iz7z747nic3qwmf")))

(define-public crate-unibar-0.1 (crate (name "unibar") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.5") (default-features #t) (kind 0)))) (hash "0cwywkkk1kava6rv5nq69jiz4mdl1dphvccblqvb7494pfs8n8kc")))

(define-public crate-uniblocks-0.0.0 (crate (name "uniblocks") (vers "0.0.0") (hash "161c7l81ksi65bm81m9ghrv7jmb8f93ncn3i5cczf6zafyjllj2v")))

(define-public crate-unibo-0.1 (crate (name "unibo") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (features (quote ("serde" "unstable-locales" "clock"))) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^6.1.4") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0f9jm9zmjj836hv6d1990lihx39zms4b4k48kmjdzj355av4x9fz")))

(define-public crate-unibox-0.1 (crate (name "unibox") (vers "0.1.0") (hash "1n5f586hbilj559kyx2xcfihx8xrwwkpbw9hw8amnnsv99lc5l0l") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-unibox-0.2 (crate (name "unibox") (vers "0.2.0") (hash "14g0yh7d16zks0xd13yp5xp8r7mvy4zvxhj2k662byfnyqzdpf84") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-unibyte-0.1 (crate (name "unibyte") (vers "0.1.0") (hash "1l64ic0icjk3kgshfkqnfl8hk46mag5sxpvhfvvnh2gpx7syk503")))

