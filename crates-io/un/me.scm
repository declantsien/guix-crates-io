(define-module (crates-io un me) #:use-module (crates-io))

(define-public crate-unmem-0.1 (crate (name "unmem") (vers "0.1.0") (hash "16zc53ava4s113x7amhg39jmcq7s98rkhw4xfl1qvmc86kyk0jpc") (yanked #t)))

(define-public crate-unmem-0.2 (crate (name "unmem") (vers "0.2.0") (hash "1n58fyw3xlp3ls4qickfcg3c0s6kmv3zas890vd0zkz25mk5scfa") (yanked #t)))

(define-public crate-unmem-0.2 (crate (name "unmem") (vers "0.2.1") (hash "1wyfzr7mwkqr5sszcn5xbwzq4nlmljxfqyvj409ascb26ypxvi2k") (yanked #t)))

(define-public crate-unmem-0.2 (crate (name "unmem") (vers "0.2.2") (hash "1a6kyp1yy2hxhvk5ibhwgncacgsmxq55pg4n3sxwmmb86g6kzn4a") (yanked #t)))

(define-public crate-unmem-0.2 (crate (name "unmem") (vers "0.2.3") (hash "1pzk3z3957b00ngk09968mxmh7vk8pwidagpgfxh9mqvpiq1d1m4") (yanked #t)))

(define-public crate-unmem-0.2 (crate (name "unmem") (vers "0.2.4") (hash "036hnz22r549dp7b8jj82yky4hgyz9l3p9dfx4nvhvr4qdnjz1pm") (yanked #t)))

(define-public crate-unmem-0.2 (crate (name "unmem") (vers "0.2.5") (hash "04bv07963gdv24xkb0ki0z1lx6s4qkwdnrl7wbbz1qa77kf8cr4c") (yanked #t)))

(define-public crate-unmem-0.2 (crate (name "unmem") (vers "0.2.6") (hash "0r1n550358vjlv6i4qj8najf6bfyvrjy763zhgv7v36x32av8g2x") (yanked #t)))

(define-public crate-unmemftp-0.1 (crate (name "unmemftp") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.31") (default-features #t) (kind 0)) (crate-dep (name "async_ftp") (req "^4.0.1") (default-features #t) (kind 2)) (crate-dep (name "libunftp") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.21") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05axn7mjfp5k6nqlfdjp9ghrzd3nr49hwmhb5hnwbmg4dnh98jqg")))

