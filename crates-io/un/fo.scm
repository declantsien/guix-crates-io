(define-module (crates-io un fo) #:use-module (crates-io))

(define-public crate-unfold-0.1 (crate (name "unfold") (vers "0.1.0") (hash "0ccpc80mmap4abhril60j89cl9pjv6pnll859l8gs6bwyd57skwp")))

(define-public crate-unfold-0.1 (crate (name "unfold") (vers "0.1.1") (hash "0gk5cgk62fydz5rmi4b9yx272259700y5gnjpf2pf2wgakb3bfry")))

(define-public crate-unfold-0.2 (crate (name "unfold") (vers "0.2.0") (hash "0f65x5jyhlwirbz28vfgs01ldbipy7wz1grw2dlskqlbz7gk9plg")))

(define-public crate-unfolding-0.0.0 (crate (name "unfolding") (vers "0.0.0") (hash "1s9yzpyn4771wa57ilxkdyfyn0qna712ppksafkb3ghhc4mjil4k") (yanked #t)))

(define-public crate-unforgettable-0.1 (crate (name "unforgettable") (vers "0.1.0") (deps (list (crate-dep (name "psm") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1lhjasq47ixxjslwbld3q5d0wfxdbcin2lyzrc0m3bjbpg42q4gi") (yanked #t)))

