(define-module (crates-io un ar) #:use-module (crates-io))

(define-public crate-unar-0.1 (crate (name "unar") (vers "0.1.1") (deps (list (crate-dep (name "ar") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "10bpc9m9xgwfawpj0c8hkhjrpyhcq0398zkxxz87khxq2i20w3yv")))

(define-public crate-unar-0.1 (crate (name "unar") (vers "0.1.2") (deps (list (crate-dep (name "ar") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0hvkss9w50madq4pa8xqpfwzjwxpzf8z4zar5w49z5bsih5izzf4")))

(define-public crate-unarc-rs-0.1 (crate (name "unarc-rs") (vers "0.1.0") (hash "0hqhvxkm8g2s28n2ick1s69f68i9brxgdxbbpfhivp4i12lrx8c7")))

(define-public crate-unarc-rs-0.2 (crate (name "unarc-rs") (vers "0.2.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "delharc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "salzweg") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0rkzafj1gc0048frabm4h2zzwq4xc9bniadmm7x8ap4lg254zrk0")))

(define-public crate-unarc-rs-0.3 (crate (name "unarc-rs") (vers "0.3.0") (deps (list (crate-dep (name "bitstream-io") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "delharc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "salzweg") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1lvi3z9plsvy6g199f67bm84k1xvvkh8cl0y9sz5b334rklcaxiq")))

(define-public crate-unarc-rs-0.4 (crate (name "unarc-rs") (vers "0.4.0") (deps (list (crate-dep (name "bitstream-io") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "delharc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "salzweg") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0jy0qkl2vn8jb71rzk21c5fl3ardkwdhmg6yn99in0qhr2332l2g")))

(define-public crate-unarc-rs-0.4 (crate (name "unarc-rs") (vers "0.4.1") (deps (list (crate-dep (name "bitstream-io") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "delharc") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "salzweg") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0j9ywsg05s9l8z9g7vsinwwk8z6n8d6a138ap223x3wwd0ngksvp")))

(define-public crate-unarchiver-0.0.1 (crate (name "unarchiver") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("std" "derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "io-util" "rt-multi-thread" "macros" "net"))) (default-features #t) (kind 0)))) (hash "12yfjbr3x85ay22nm0dk98mgdaxgr6v544qz7d1mbp05fqbz3s8s")))

(define-public crate-unarchiver-0.0.2 (crate (name "unarchiver") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("std" "derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "io-util" "rt-multi-thread" "macros" "net"))) (default-features #t) (kind 0)))) (hash "15ikcsifs07n3w3r520cd7xw65zc346ik5k6kwi4wlc223pscjzc")))

(define-public crate-unarchiver-0.0.3 (crate (name "unarchiver") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("std" "derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "io-util" "rt-multi-thread" "macros" "net"))) (default-features #t) (kind 0)))) (hash "0w0d3yzgfqa16wws3aid2mjcar7r5fqs92sqzcv72z6d5bgl2iyn")))

(define-public crate-unarj-rs-0.1 (crate (name "unarj-rs") (vers "0.1.0") (deps (list (crate-dep (name "crc32fast") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "delharc") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1h9ln5806sq583mlcd4yr0pkyjc6218pf8zzcagj89d3wpxnwzvq")))

(define-public crate-unarj-rs-0.2 (crate (name "unarj-rs") (vers "0.2.0") (deps (list (crate-dep (name "bitstream-io") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "delharc") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "17jg2xx3vn6cfmw24yv4si268g3m4ky98mjrdv7dwasryv5kzr4w")))

(define-public crate-unarj-rs-0.2 (crate (name "unarj-rs") (vers "0.2.1") (deps (list (crate-dep (name "bitstream-io") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "delharc") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0fb8hjmjqr0cjvg8kaansgw7xdgsc5dkjnzyamahmms5wny6pskh")))

(define-public crate-unarray-0.1 (crate (name "unarray") (vers "0.1.0") (deps (list (crate-dep (name "test-strategy") (req "^0.2") (default-features #t) (kind 2)))) (hash "0kphkczhfj40h1186b14vc67xn775r2j1z1hixq5khh44bbcj9qr")))

(define-public crate-unarray-0.1 (crate (name "unarray") (vers "0.1.1") (deps (list (crate-dep (name "test-strategy") (req "^0.2") (default-features #t) (kind 2)))) (hash "04726lam2vgpbw9a7937r7wprp8jqdvhyiwjp4ylkyvlsjj36hk3")))

(define-public crate-unarray-0.1 (crate (name "unarray") (vers "0.1.2") (deps (list (crate-dep (name "test-strategy") (req "^0.2") (default-features #t) (kind 2)))) (hash "0qr568jrsqpzpv687xl4wfi1wjqnsj3zawarj4qdivwn8n4pii7m")))

(define-public crate-unarray-0.1 (crate (name "unarray") (vers "0.1.3") (deps (list (crate-dep (name "test-strategy") (req "^0.2") (default-features #t) (kind 2)))) (hash "1wnvz3rjgl1fxyifmi3ad81dnvkgyb39rp765h1m6grg05xxlkg8")))

(define-public crate-unarray-0.1 (crate (name "unarray") (vers "0.1.4") (deps (list (crate-dep (name "test-strategy") (req "^0.2") (default-features #t) (kind 2)))) (hash "154smf048k84prsdgh09nkm2n0w0336v84jd4zikyn6v6jrqbspa")))

(define-public crate-unary-0.0.0 (crate (name "unary") (vers "0.0.0") (hash "1hwqjgcziwv8pc7sr5cr5hk4957x2mpnqgj7xx6fr2qhsir46ibl")))

