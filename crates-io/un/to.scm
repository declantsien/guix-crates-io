(define-module (crates-io un to) #:use-module (crates-io))

(define-public crate-untokio-0.1 (crate (name "untokio") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest010") (req "^0.10") (default-features #t) (kind 2) (package "reqwest")) (crate-dep (name "tokio02") (req "^0.2") (features (quote ("rt-threaded"))) (optional #t) (default-features #t) (kind 0) (package "tokio")) (crate-dep (name "tokio03") (req "^0.3") (features (quote ("rt-multi-thread"))) (optional #t) (default-features #t) (kind 0) (package "tokio")) (crate-dep (name "tokio03") (req "^0.3") (features (quote ("rt-multi-thread" "fs"))) (default-features #t) (kind 2) (package "tokio")))) (hash "0l3h1s1w0j37c5s3wxk90j85h5jn4jc8zag48dgkdj4jdynkg6hp") (features (quote (("v03" "tokio03") ("v02" "tokio02") ("default"))))))

(define-public crate-untokio-0.2 (crate (name "untokio") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest010") (req "^0.10") (default-features #t) (kind 2) (package "reqwest")) (crate-dep (name "tokio02") (req "^0.2") (features (quote ("rt-threaded"))) (optional #t) (default-features #t) (kind 0) (package "tokio")) (crate-dep (name "tokio02") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 2) (package "tokio")) (crate-dep (name "tokio03") (req "^0.3") (features (quote ("rt-multi-thread"))) (optional #t) (default-features #t) (kind 0) (package "tokio")) (crate-dep (name "tokio03") (req "^0.3") (features (quote ("macros" "rt-multi-thread" "fs"))) (default-features #t) (kind 2) (package "tokio")))) (hash "14nkgkr1z1sjx8bvcss38cbfw4yfg3zbdk28pk81hiclg14bqnk6") (features (quote (("v03" "tokio03") ("v02" "tokio02") ("default"))))))

(define-public crate-untokio-0.2 (crate (name "untokio") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest010") (req "^0.10") (default-features #t) (kind 2) (package "reqwest")) (crate-dep (name "tokio02") (req "^0.2") (features (quote ("rt-threaded"))) (optional #t) (default-features #t) (kind 0) (package "tokio")) (crate-dep (name "tokio02") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 2) (package "tokio")) (crate-dep (name "tokio03") (req "^0.3") (features (quote ("rt-multi-thread"))) (optional #t) (default-features #t) (kind 0) (package "tokio")) (crate-dep (name "tokio03") (req "^0.3") (features (quote ("macros" "rt-multi-thread" "fs"))) (default-features #t) (kind 2) (package "tokio")) (crate-dep (name "tokio1") (req "^1") (features (quote ("rt-multi-thread"))) (optional #t) (default-features #t) (kind 0) (package "tokio")) (crate-dep (name "tokio1") (req "^1") (features (quote ("macros" "rt-multi-thread" "fs"))) (default-features #t) (kind 2) (package "tokio")))) (hash "0pahql5kq9zglakmfk9n87nrqrvl72da44pmf3mwn6rg8qrqry3b") (features (quote (("v1" "tokio1") ("v03" "tokio03") ("v02" "tokio02") ("default"))))))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.0") (hash "1yl0fnvvl402gvp7z1s52z8skavim1gqb9c70zwvfqnwb0zpssnl")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.1") (hash "0cd116ssv3yyr3287vgfznv56n075dvl1kgq3jx4wnw70xz5gz97")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.2") (hash "1sc0kg3dfngzsw6hs44gashmngig6jzg9kkmd89p6jy4q5x6bw0r")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.3") (hash "0hqlq52q6by48s14fxjivza9kd776a383x0r5kvrxyz35w45c74h")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.4") (hash "0ynym0gnkk5drj388zxlrvbvfgwh7xiz8gpi0khcbs9vqj0gq0bl")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.5") (hash "0k094cfnln48fa12vc0hfs8xfkli8q4cc0f71m4w4vwrs1y2zw81")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.6") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "07mapblxcjywa2f4wcfna3fbjrk1z0jgwv250nk9qcqbs45gqvw5")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.7") (deps (list (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "0nqkp3g4iim9dcmn3gchg1v5vi64fq48x1zj7wz8drmyx18kys0a")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.8") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "0w24i71bqzfvr3lpyrkhl2bxh7pz258mrkgc5fag01080k01rshc")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.9") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "16gq1cxbyzivgw4lf4ixp50mmilkpkp1j3wr8nrrp1vdpdnk9da4")))

(define-public crate-untools-1 (crate (name "untools") (vers "1.0.10") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "0rj9q7q2dfgnzj36v1pm1siri0kvbzjc3dg63pna6fh2m90ig51c")))

