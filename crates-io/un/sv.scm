(define-module (crates-io un sv) #:use-module (crates-io))

(define-public crate-unsvg-1 (crate (name "unsvg") (vers "1.0.0") (deps (list (crate-dep (name "resvg") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "0wpdgpgc4blnqv2lx57gsfdf8wzdh5il65bw0i7l3i4l7b91g65n")))

(define-public crate-unsvg-1 (crate (name "unsvg") (vers "1.1.0") (deps (list (crate-dep (name "resvg") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "026nfwvc2gs9ijgf44rj33q50kn2f1rxhy90jh765xa5xrnmr49z")))

(define-public crate-unsvg-1 (crate (name "unsvg") (vers "1.1.1") (deps (list (crate-dep (name "resvg") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "0ngb3p2bl3n0k32snhrga2d7vswzk1i3ppsfhc1ghwhm1p75iw9w")))

(define-public crate-unsvg-1 (crate (name "unsvg") (vers "1.1.2") (deps (list (crate-dep (name "resvg") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "0id3jchr8mmkqmk75ca0vjksgy57kk6zfrs0l60fs7lnl0x7296v")))

