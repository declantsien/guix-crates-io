(define-module (crates-io un th) #:use-module (crates-io))

(define-public crate-unthbuf-0.1 (crate (name "unthbuf") (vers "0.1.0") (hash "1q35l1is2j8j2ylkh4i8sg3730638v9xjdp9vfxr1fv98b48q4zj")))

(define-public crate-unthbuf-0.2 (crate (name "unthbuf") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "04kndwy82j3g1ard9aclzn0aswjq37w7firwcydzmzm7102wfgp5")))

(define-public crate-unthbuf-1 (crate (name "unthbuf") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "04vmf31v7giwv4vzr6ybzjli1vwsgwgcsrb4hxy0xy415cwgbql2")))

