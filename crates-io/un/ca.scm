(define-module (crates-io un ca) #:use-module (crates-io))

(define-public crate-uncached-0.1 (crate (name "uncached") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "05z5a8d5ajw60a0dpb02m4ixrgcwdsyd1p296rpk72i9mc8r2x9k")))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.0") (deps (list (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "180hpjap63qdf0d8ifhbd3nxjl7bgmk19ipbib8q08abnxf1xinp") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.1") (deps (list (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "0af63gr2v1ldvma1yjk5z9vhyfagmlq5lbq58q03fzv2726lksrp") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.2") (deps (list (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1w44g9z0wqwf39l2h7qlw3jkqsyha7c3hm89cd1gfdzaw4z1vsnq") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.3") (deps (list (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "0yfizabkpbf0hw6s7aw7zivfcv5pq46s9jfk84skgib9g7ysg7rn") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "0azqvhhm5vfcvg8x7050ykriaq95s3hf9li2hj4s1gvhv85crhli") (features (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.5") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "0aq8whnlfns8brlzb7ji0bvfrgip31fav17yky93jvb4km33429h") (features (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.6") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1l3flz044hfdnsddahj08dflqprfydszkm4vkf458l724xryvbjv") (features (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.7") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "08inqy50xnfa2mk6g9zlsi18gnmd1dw9iq4qrynky2zxn011gc09") (features (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.8") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1mxx8dgq5qyx8g41qf6x4wqac0kkhi0j4kkfa5f328gnzk822nxv") (features (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.9") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "0s6ajpdc300jr3fhxgh93lwdfkd88g9amhw6mc179gm4d0qwb6wv") (features (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

(define-public crate-uncased-0.9 (crate (name "uncased") (vers "0.9.10") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "15q6r6g4fszr8c2lzg9z9k9g52h8g29h24awda3d72cyw37qzf71") (features (quote (("with-serde-alloc" "serde" "serde/alloc" "alloc") ("with-serde" "serde") ("default" "alloc") ("alloc"))))))

