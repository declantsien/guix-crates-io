(define-module (crates-io un _a) #:use-module (crates-io))

(define-public crate-un_algebra-0.1 (crate (name "un_algebra") (vers "0.1.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "15008nzg23is15332icjg8na2jfcj2fiaqp2xmrdam7x4r1zadny")))

(define-public crate-un_algebra-0.1 (crate (name "un_algebra") (vers "0.1.3") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "15gx0rd278s0kfg2di158a8l2w8hx9cq20808kdkjij4dhqfwf3z")))

(define-public crate-un_algebra-0.1 (crate (name "un_algebra") (vers "0.1.4") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "07sghb74hsgx8i6bi16nb0fw40gp55h7zzk5bqkkp652f70l2gvc")))

(define-public crate-un_algebra-0.1 (crate (name "un_algebra") (vers "0.1.5") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 2)))) (hash "15alzmrrkckcr41icp78sfd0v3qwrhw5hpbfkfq7g3ijpxrkg46j")))

(define-public crate-un_algebra-0.1 (crate (name "un_algebra") (vers "0.1.6") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "1aq4054zaxijw8mhkyfaf35mrvm3qvg6n3iil5wz09v78kskbq7f")))

(define-public crate-un_algebra-0.1 (crate (name "un_algebra") (vers "0.1.7") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "00wgrjlh3q98lw4zr95s8s62fnh5jcyd134ldkqgix30q8hrnz1k")))

(define-public crate-un_algebra-0.2 (crate (name "un_algebra") (vers "0.2.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "0pw6fci3p4nncq2ld8hd9n8cqbr6j2ylq48n1bnpvdp2zf9yqw5v")))

(define-public crate-un_algebra-0.2 (crate (name "un_algebra") (vers "0.2.1") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "10rgb395g7mmmkaflr371wf6vpjjip3rhlwm6ad5q0hwzpdsx08x")))

(define-public crate-un_algebra-0.2 (crate (name "un_algebra") (vers "0.2.2") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "0k7p56qlpppnnqgcy6b6pvhlgc64l89ldpbch0qw5i1rk7dgcq03")))

(define-public crate-un_algebra-0.3 (crate (name "un_algebra") (vers "0.3.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "03wfwy7g183h6xvc2phbv830kdyvl5alna05h287lni1i1alj7i7")))

(define-public crate-un_algebra-0.4 (crate (name "un_algebra") (vers "0.4.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 0)))) (hash "0dshhvgwkl41hxsn2ni4h2vddyjpbpvz7kyw4kxxydjd3lix1931")))

(define-public crate-un_algebra-0.5 (crate (name "un_algebra") (vers "0.5.0") (deps (list (crate-dep (name "num") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "0.9.*") (default-features #t) (kind 0)) (crate-dep (name "proptest-derive") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1lgvn25nkljyc6md27jxsjbbcvflj3h4afwl6p7vhazx86wkvijc")))

(define-public crate-un_algebra-0.6 (crate (name "un_algebra") (vers "0.6.0") (deps (list (crate-dep (name "num") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "0.9.*") (default-features #t) (kind 0)) (crate-dep (name "proptest-derive") (req "0.1.*") (default-features #t) (kind 0)))) (hash "14bqvkg1hj7xc1ll8h25wcvjli757zx9amw8qqhy730y45dck588")))

(define-public crate-un_algebra-0.7 (crate (name "un_algebra") (vers "0.7.0") (deps (list (crate-dep (name "num") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "0.9.*") (default-features #t) (kind 0)) (crate-dep (name "proptest-derive") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0ccfah6xck9fvq04l8m7l89wi87rq7kl88ay8ivrfifkgfdczlxx")))

(define-public crate-un_algebra-0.8 (crate (name "un_algebra") (vers "0.8.0") (deps (list (crate-dep (name "num") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "0.9.*") (default-features #t) (kind 0)) (crate-dep (name "proptest-derive") (req "0.1.*") (default-features #t) (kind 0)))) (hash "12k771wvb2kp11gyf2m36imhpjypninhwlbx79pxrwp2p3ki2v7b")))

(define-public crate-un_algebra-0.9 (crate (name "un_algebra") (vers "0.9.0") (deps (list (crate-dep (name "num") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "0.9.*") (default-features #t) (kind 0)) (crate-dep (name "proptest-derive") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0n04498wclb339f0czm9rljdnillk5b1gz8kcjnpl0qa90xdcirr")))

