(define-module (crates-io un o-) #:use-module (crates-io))

(define-public crate-uno-cli-0.1 (crate (name "uno-cli") (vers "0.1.0") (deps (list (crate-dep (name "color-print") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rsg4z634ng2x3prvyyz82hawiwxw2i6isjsdaf0ykl1wv3wigw4")))

(define-public crate-uno-cli-0.1 (crate (name "uno-cli") (vers "0.1.1") (deps (list (crate-dep (name "color-print") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wvgqchjnm8mpks63rpdws30vw96b9ayma5m65izc2rr81pmz6jy")))

(define-public crate-uno-cli-0.1 (crate (name "uno-cli") (vers "0.1.2") (deps (list (crate-dep (name "color-print") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1jq0gnlij726qfl1as16w905bhbq1pl9rl82rs0m2vdq0a4v1qn9")))

(define-public crate-uno-cli-0.1 (crate (name "uno-cli") (vers "0.1.4") (deps (list (crate-dep (name "color-print") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11s34aqa2a3hy1c4ywiwnzgs8m6fn3pnym1crlfw59ya1fv9hxrj")))

(define-public crate-uno-cli-0.1 (crate (name "uno-cli") (vers "0.1.5") (deps (list (crate-dep (name "color-print") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1g5bzmrarb3rkqkwvii7lk3yys1a4av5107sgl8fpi0zrmnkpp3i")))

(define-public crate-uno-cli-0.1 (crate (name "uno-cli") (vers "0.1.6") (deps (list (crate-dep (name "color-print") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1awzaf85pqpm3dd1gkfgvflms69xykyf2izv4hxnx8g6f4y8084g")))

(define-public crate-uno-cli-0.1 (crate (name "uno-cli") (vers "0.1.7") (deps (list (crate-dep (name "color-print") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0jp89j67x2pphdc8fs9axa7xa0da8kylpvzxhvpnvx36q19glgfj")))

