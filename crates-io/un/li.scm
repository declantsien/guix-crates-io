(define-module (crates-io un li) #:use-module (crates-io))

(define-public crate-unlicense-0.1 (crate (name "unlicense") (vers "0.1.0") (hash "1c1a20qfkg4r1x4qkl04260377xflkib4l6p7psygv61vfb9bzl1")))

(define-public crate-unlicense-0.2 (crate (name "unlicense") (vers "0.2.0") (hash "1d7mm38n6an3kbr77bx0mfm3ba3pkall3zdfl570cjda5k5kbjrz")))

(define-public crate-unlim-0.1 (crate (name "unlim") (vers "0.1.0") (hash "03lnz7hpf7qcxsxkkfnigm65cd1sizgl583w04kw1vkps7b5h08y") (yanked #t)))

(define-public crate-unlimited-0.0.0 (crate (name "unlimited") (vers "0.0.0") (hash "07pi3jmcmjb9n9cx45jrmypqr0s2lmz7chgy77ma9m6z1495sfv6")))

(define-public crate-unlink-0.0.0 (crate (name "unlink") (vers "0.0.0-alpha0.0") (hash "17y4kq6jli9qmb6a8d2pavkix69myd2cg2qz495q71pd2b8hq550")))

(define-public crate-unlink-0.0.0 (crate (name "unlink") (vers "0.0.0-pre-release1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0492m839hqxfk25vidbk8xvg9cp1fqd91rhzz40b9inln2ji9a39")))

(define-public crate-unlink-0.0.0 (crate (name "unlink") (vers "0.0.0-pre-release2.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0626bgzpmmhkyf4n7nnrpf0a49yhyaf9v80djs4xa5qhr054z29i")))

(define-public crate-unlink-0.0.0 (crate (name "unlink") (vers "0.0.0-pre-release3.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lockfree") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0izx7cq392f5mimvybvwp49b3v8w7fhj1ay5x17j0hj1r35dw1j5")))

(define-public crate-unlink-0.0.0 (crate (name "unlink") (vers "0.0.0-pre-release3.1") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lockfree") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1nqqd3wgsa6gp2l5mwi1s47c92l3vavh5lj6y4imz9qxxp2svigb")))

