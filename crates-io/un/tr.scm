(define-module (crates-io un tr) #:use-module (crates-io))

(define-public crate-untree-0.9 (crate (name "untree") (vers "0.9.5") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)))) (hash "1z22v3sb0idfcqpx0fxvvxk9bfkhsjmzpy35ll728dib0b714y8r")))

(define-public crate-untree-0.9 (crate (name "untree") (vers "0.9.5-better-docs") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)))) (hash "1hgxz8mrg6ivbmjljriwqrd8sl51l277gy6kc6s5c58xf40dna8c")))

(define-public crate-untree-0.9 (crate (name "untree") (vers "0.9.6") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)))) (hash "05akk01dnk5j9kma4gyxpq67kkmviid75v6w8ki02rxs9g87svsi")))

(define-public crate-untree-0.9 (crate (name "untree") (vers "0.9.7") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)))) (hash "0blz3s21xlr3hqhsy8wfj8ximvvlx827vnnb189054pynd82qyd3")))

(define-public crate-untree-0.9 (crate (name "untree") (vers "0.9.8") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)))) (hash "1c1gxid96bl34wjbvgp4lvxmddd843yh7c5kaqrf3n5bby04vv96")))

(define-public crate-untree-0.9 (crate (name "untree") (vers "0.9.9") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "embed-doc-image") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.14") (default-features #t) (kind 0)))) (hash "0h18klil8dv1xfmk8fnwy4k9fj02rkvhkjncicr5r9j839lb0pqr") (features (quote (("default" "build-binary") ("build-binary" "clap"))))))

(define-public crate-untree-0.10 (crate (name "untree") (vers "0.10.0") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "embed-doc-image") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.14") (default-features #t) (kind 0)))) (hash "0mr88iny3qlkgvkr9hl30vw526warw3bcs6w8l1rg77y7ww5p3ay") (features (quote (("default" "build-binary") ("build-binary" "clap"))))))

(define-public crate-untree-0.9 (crate (name "untree") (vers "0.9.10") (deps (list (crate-dep (name "clap") (req "^3.0.13") (features (quote ("derive" "wrap_help"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "embed-doc-image") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16") (features (quote ("terminal_size"))) (default-features #t) (kind 0)))) (hash "1nnzjxgakv87a6n6a21wbcyij681c3rv4xxl0a8dag6lkrxhb1yq") (features (quote (("default" "build-binary") ("build-binary" "clap"))))))

(define-public crate-untrusted-0.1 (crate (name "untrusted") (vers "0.1.0") (hash "0cvn91afbjrnjxw6fdlq0pxpg865n9jnb393l09zsgix1lrannvh") (yanked #t)))

(define-public crate-untrusted-0.2 (crate (name "untrusted") (vers "0.2.0") (hash "1g8a2ssn9ys3lk5xd618zgr55745qqdxh4knjc788mib2y4c3dbx") (yanked #t)))

(define-public crate-untrusted-0.3 (crate (name "untrusted") (vers "0.3.0") (hash "1md10kzcy7kkz8l3wixdsplamwp5b18f5ia2as7b8x24rhkv46iy") (yanked #t)))

(define-public crate-untrusted-0.3 (crate (name "untrusted") (vers "0.3.1") (hash "03accgg26mgv00b3b6491ldih8g284ssry5z3xfrf41swzkc16sx") (yanked #t)))

(define-public crate-untrusted-0.3 (crate (name "untrusted") (vers "0.3.2") (hash "1wwy0bk2r0vyfpnjhj406nh4fxmapkammmfyhgcmylg3291zcg8r") (yanked #t)))

(define-public crate-untrusted-0.5 (crate (name "untrusted") (vers "0.5.0") (hash "0wy632kz6h9sdxd1hlnkiqfkdhv54y12pgfnq3csqspgi4wj8rbb") (yanked #t)))

(define-public crate-untrusted-0.5 (crate (name "untrusted") (vers "0.5.1") (hash "1bm7v5da3i1ihzlnfdpwj2zbpdv81mpmybw74qz86n5ykn0xg4pk") (yanked #t)))

(define-public crate-untrusted-0.6 (crate (name "untrusted") (vers "0.6.0") (hash "0acixwjb54vsvl0whczssfwq7wpbb81j7xdhv5m3yicgiy896cbc") (yanked #t)))

(define-public crate-untrusted-0.6 (crate (name "untrusted") (vers "0.6.1") (hash "0r8k4jklhr0xki9avdbbkz0ssnrc4db9pv1r7hxaa8sxihya9bvh") (yanked #t)))

(define-public crate-untrusted-0.6 (crate (name "untrusted") (vers "0.6.2") (hash "0byf88b7ca1kb5aap8f6npp6xncvg95dnma8ipmnmd4n9r5izkam")))

(define-public crate-untrusted-0.7 (crate (name "untrusted") (vers "0.7.0-alpha1") (hash "0hdadv9wfxq3x4ksmi18m3wfxinhpsal7kn9jvcgapanzb6hl0jd")))

(define-public crate-untrusted-0.7 (crate (name "untrusted") (vers "0.7.0") (hash "1kmfykcwif6ashkwg54gcnhxj03kpba2i9vc7z5rpr0xlgvrwdk0")))

(define-public crate-untrusted-0.7 (crate (name "untrusted") (vers "0.7.1") (hash "0jkbqaj9d3v5a91pp3wp9mffvng1nhycx6sh4qkdd9qyr62ccmm1")))

(define-public crate-untrusted-0.8 (crate (name "untrusted") (vers "0.8.0") (hash "12gf7da7jilyn95ch2njffl7b03pa4b4nswayxxwy87ws58bfa83") (yanked #t)))

(define-public crate-untrusted-0.9 (crate (name "untrusted") (vers "0.9.0") (hash "1ha7ib98vkc538x0z60gfn0fc5whqdd85mb87dvisdcaifi6vjwf")))

(define-public crate-untrusted6-0.1 (crate (name "untrusted6") (vers "0.1.0") (deps (list (crate-dep (name "untrusted") (req "^0.6") (default-features #t) (kind 0)))) (hash "1acsprqdvibrm8jxp7d9nd5xgl1hi6hbjs1nd3k18q7aysrzwkkw")))

(define-public crate-untrustended-0.0.1 (crate (name "untrustended") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0gl03nr8ck3xrp4wm4mxdsp4crm131pi6f0ckq6sfg8wzdhl3p2h")))

(define-public crate-untrustended-0.0.2 (crate (name "untrustended") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0m9nababfc7khfy44xicb56yvgyhyxrzxqr0syj23vzr8qmxx98j")))

(define-public crate-untrustended-0.1 (crate (name "untrustended") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0w5869la3v7c0h3rz1kr4b4jwz7wkn2mgji9lmdx36gnpyczws44") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.2 (crate (name "untrustended") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0z0k6ic7c9qpshmz1g7mzq8i9kiwh5z723kpk9148q68qf5q8h89") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.2 (crate (name "untrustended") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (kind 2)) (crate-dep (name "untrusted") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1kn64z4z0gp3g4hj0xcjq0qpvjm1j9lpmp43fplbd3gjrpwdlh9a") (features (quote (("use_std") ("i128" "byteorder/i128") ("default" "use_std"))))))

(define-public crate-untrustended-0.3 (crate (name "untrustended") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (features (quote ("std"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "untrusted") (req "^0.7") (default-features #t) (kind 0)))) (hash "0znckw61icmx70r6mx2a0jm9k7348psni57dckywm739ba8hsar2") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.4 (crate (name "untrustended") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (features (quote ("std"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (kind 2)) (crate-dep (name "untrusted") (req "^0.9") (default-features #t) (kind 0)))) (hash "0p2pkxr168b882d3i97764jd48bhsa6nznmwgsnmdahzax0j72qz") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untrustended-0.4 (crate (name "untrustended") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1") (features (quote ("std"))) (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (kind 2)) (crate-dep (name "untrusted") (req "^0.9") (default-features #t) (kind 0)))) (hash "05qf9c1p30rf5vj3p6mb24q8xqny4vjbisf3i52ba4ybf57lb87q") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-untry-0.1 (crate (name "untry") (vers "0.1.0") (hash "13s9dbrsj28ycxi9rxv6cjnnix3xw6wfs12hhldbaxg2yn75x7jl") (features (quote (("nightly"))))))

