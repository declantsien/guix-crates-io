(define-module (crates-io un ch) #:use-module (crates-io))

(define-public crate-unchained-0.1 (crate (name "unchained") (vers "0.1.0") (hash "15kmap19i1xa72bhm8wski7c7c4xzy06wwq1f2w91s43avr06fcw")))

(define-public crate-unchained-0.1 (crate (name "unchained") (vers "0.1.1") (hash "1iwhnriknazr9342cxxn7m280lbqddnmw1iinjvsfrxjhdqbsjrh")))

(define-public crate-unchained-0.1 (crate (name "unchained") (vers "0.1.2") (hash "03n7q3ii29fk5dm2756qj3bmpmznxl77nihg9z821rdlajrjd74n")))

(define-public crate-unchained-0.1 (crate (name "unchained") (vers "0.1.3") (hash "102mcwylxz1aaxh3m84g0fnbl87xi8yiy33pkkyq1w4763fl270v")))

(define-public crate-uncheck-ord-0.1 (crate (name "uncheck-ord") (vers "0.1.0") (hash "0vghpgd9nvy9cdd0vp5gz07jws09fh1qs1c5w6h952xqll8wwm6y")))

(define-public crate-unchecked-0.0.0 (crate (name "unchecked") (vers "0.0.0-0.0.0-0.0.0") (hash "1yippy0icym18iiihl6x3ab12qkry7p6vpfc2gi9mbwx0csrza54") (yanked #t)))

(define-public crate-unchecked-0.0.0 (crate (name "unchecked") (vers "0.0.0--") (hash "1c9gj48h2w8m5lkh58b3bdkj09nchdr9n49qa3wg5pv0m05i24lz") (yanked #t)))

(define-public crate-unchecked-index-0.1 (crate (name "unchecked-index") (vers "0.1.0") (hash "1qb61ixxi487i8a0mlrh1x4l7g67fv2qksgwb91dmmyfbp5clz52")))

(define-public crate-unchecked-index-0.1 (crate (name "unchecked-index") (vers "0.1.1") (hash "1x2mjlxkgn53yr9ra680nhgxigfpl0dkscybgw61izjdmbxhfava")))

(define-public crate-unchecked-index-0.2 (crate (name "unchecked-index") (vers "0.2.0") (hash "1rn5ml0lz1p2xdzkzf2lpjif6jg91yjm312m75vrxs7qdfy7gs1p")))

(define-public crate-unchecked-index-0.2 (crate (name "unchecked-index") (vers "0.2.1") (hash "01vp51g3z51ji4xci4gv5y74nkn4k7naml90kc2rsyw1dm9h8b9q")))

(define-public crate-unchecked-index-0.2 (crate (name "unchecked-index") (vers "0.2.2") (hash "0p6qcai1mjayx59cpgk27d0zgw9hz9r1ira5jiqil66f4ba8dfpf")))

(define-public crate-unchecked-proc-macros-0.0.0 (crate (name "unchecked-proc-macros") (vers "0.0.0-0.0.0-0.0.0") (hash "0yj0w3sj16jlqvzbr37xqchrhafdsljxmw6am8x7ms2x051pfdfa") (yanked #t)))

(define-public crate-unchecked-proc-macros-0.0.0 (crate (name "unchecked-proc-macros") (vers "0.0.0--") (hash "008vcd98s4qj6zrk32vvh4xvpfvi28l25binz9mdzjsdy2xbis9w") (yanked #t)))

(define-public crate-unchecked-std-0.1 (crate (name "unchecked-std") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "1dfdz4vfwrib9rks1mjss449qv1kv7mggcn7rpd619z57kdwnazy")))

(define-public crate-unchecked_mutable-0.1 (crate (name "unchecked_mutable") (vers "0.1.0") (hash "18v4wnggcmg889w8s6p0hfxkd2wr3b5hjq7lvlhxrb6qxnxs7ccx")))

(define-public crate-unchecked_unwrap-0.1 (crate (name "unchecked_unwrap") (vers "0.1.0") (hash "16vf7z80z0p2czj5f7nl2xvyk7l1yaxa16yvm382yccb5vm67g1s") (features (quote (("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-0.8 (crate (name "unchecked_unwrap") (vers "0.8.0") (hash "1x805vbv9gifx6m9qn5bs99fpqai3p6rzd4bnzxk5q5jy7gb6zw6") (features (quote (("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-0.9 (crate (name "unchecked_unwrap") (vers "0.9.0") (hash "0nbr9h2h97gs03ryz9grjl76bps4h5m271d8ypdcr496fy8py32w") (features (quote (("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-1 (crate (name "unchecked_unwrap") (vers "1.0.0") (hash "07mym4dnj9zjh0bmzdcz5f120kv408wjya7jii64d8lrkc6l6jh6") (features (quote (("doc_include") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-1 (crate (name "unchecked_unwrap") (vers "1.0.1") (hash "1nswnpff2bcl3rdzsdjxp9712b7y7lv4wg28j2mbz8bv5cxi7ci1") (features (quote (("doc_include") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-2 (crate (name "unchecked_unwrap") (vers "2.0.0") (hash "17z4iz6lc6h9qp14fyfyv1i9kwp6mvagzrl3ds1b12phj2iw1xw8") (features (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-2 (crate (name "unchecked_unwrap") (vers "2.0.1") (hash "003j3i7mwqwm3jxq70w7lhx8fb9ag1scqryq6rrfcc28c9bskzc3") (features (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-2 (crate (name "unchecked_unwrap") (vers "2.0.2") (hash "0kifjkq1qj85faja0rx4n5rdl73nfn6lpax91bg1vdd10f17b8sd") (features (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-3 (crate (name "unchecked_unwrap") (vers "3.0.0") (hash "1n72gf8sz8mzdpabwl2gml3vhz3p9y38fndwyb8w4h700sig691w") (features (quote (("nightly") ("default" "debug_checks") ("debug_checks"))))))

(define-public crate-unchecked_unwrap-4 (crate (name "unchecked_unwrap") (vers "4.0.0") (hash "1220nyxq1458vbdhjjjmvv0bkv0pdkgnljf1aylci3nd49arc99j") (features (quote (("default" "debug_checks") ("debug_checks")))) (rust-version "1.54")))

