(define-module (crates-io un fh) #:use-module (crates-io))

(define-public crate-unfhash-0.1 (crate (name "unfhash") (vers "0.1.0") (deps (list (crate-dep (name "arrow") (req "^5.3") (features (quote ("csv"))) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0l98rqgw5fi1x0ypk2v9vpnc5sj3n77n4pfvmlmvpk4dchl0qp3s")))

