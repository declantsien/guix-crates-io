(define-module (crates-io un al) #:use-module (crates-io))

(define-public crate-unaligned-0.1 (crate (name "unaligned") (vers "0.1.0") (deps (list (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)))) (hash "12mah2q58kvcnk47095qpdypaic2dibglzz2vca3vvsd0nkd1shx") (features (quote (("std"))))))

(define-public crate-unaligned-0.1 (crate (name "unaligned") (vers "0.1.1") (deps (list (crate-dep (name "scopeguard") (req "^1.1") (kind 0)))) (hash "1ji7anh2arjfpl7h7h1iykk8hkiw7jfhq3zx6j5m2v34i1z0r2ml") (features (quote (("std"))))))

