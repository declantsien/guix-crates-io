(define-module (crates-io un wx) #:use-module (crates-io))

(define-public crate-unwx-0.1 (crate (name "unwx") (vers "0.1.0") (deps (list (crate-dep (name "flume") (req "^0.11.0") (kind 0)) (crate-dep (name "xflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "09ji37n1iaq3g5bww3x3n866shyfr33p9y8b5zp8nfxnf3fkj3in")))

