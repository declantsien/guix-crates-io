(define-module (crates-io fu nv) #:use-module (crates-io))

(define-public crate-funver-1 (crate (name "funver") (vers "1.0.0-experimental-glam.8.2") (hash "0id3dqlr48q0ig4yhk5gzdxlsdl9fdfm1044a0s958xc8kfidpvc")))

(define-public crate-funver-1 (crate (name "funver") (vers "1.0.0-experimental-serde.1.0") (hash "1gcfvifnh1qd2y7kgpvrn8jv9p8sajxq8kw23yz5y3n950dzffxc")))

