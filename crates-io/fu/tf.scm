(define-module (crates-io fu tf) #:use-module (crates-io))

(define-public crate-futf-0.1 (crate (name "futf") (vers "0.1.0") (deps (list (crate-dep (name "mac") (req "^0") (default-features #t) (kind 0)))) (hash "10ih28zvmjll4d3mbfdp0bybw6b4wczpl8vvd4h3h90q683rx96b")))

(define-public crate-futf-0.1 (crate (name "futf") (vers "0.1.1") (deps (list (crate-dep (name "debug_unreachable") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "mac") (req "^0.0") (default-features #t) (kind 0)))) (hash "05l0a3q5i9d9as8ds664c1dbk9ii8b7s37ds4xh8ghdyswas1kjl")))

(define-public crate-futf-0.1 (crate (name "futf") (vers "0.1.2") (deps (list (crate-dep (name "debug_unreachable") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mac") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1b98djs2izdchg3p4y9r1fyqxikmh0bxkcv4qm8knmd2h29niag7")))

(define-public crate-futf-0.1 (crate (name "futf") (vers "0.1.3") (deps (list (crate-dep (name "debug_unreachable") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mac") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wwqwcj4pg07hiy4hrl2m5chlq04s133a2w1spf985xswqykzyai")))

(define-public crate-futf-0.1 (crate (name "futf") (vers "0.1.4") (deps (list (crate-dep (name "mac") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "new_debug_unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0fxc18bnabird5jl941nsd6d25vq8cn8barmz4d30dlkzbiir73w")))

(define-public crate-futf-0.1 (crate (name "futf") (vers "0.1.5") (deps (list (crate-dep (name "mac") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "new_debug_unreachable") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0hvqk2r7v4fnc34hvc3vkri89gn52d5m9ihygmwn75l1hhp0whnz")))

