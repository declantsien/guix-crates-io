(define-module (crates-io fu ba) #:use-module (crates-io))

(define-public crate-fubar-cli-0.1 (crate (name "fubar-cli") (vers "0.1.0") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "01zyhmjb9w437lbw95j059jkgszm08zzskiqh42h264bv7qy832b") (yanked #t)))

(define-public crate-fubar-cli-0.1 (crate (name "fubar-cli") (vers "0.1.1") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0dpfw80ry6lh10yr3hpz7hdvkl2kpxvph2a9anrgxbksbn9k9q1m")))

(define-public crate-fubar-cli-0.1 (crate (name "fubar-cli") (vers "0.1.2") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1ymawlpymr3nwfwid66p68cp7rl82dvn3n4ymapcc7xqpf64ydc1")))

(define-public crate-fubar-cli-0.1 (crate (name "fubar-cli") (vers "0.1.3") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0cvvb6hwrc6vinjyxyy9s5lx1ks2cp3n832zk7n49a9980xc2g8k")))

