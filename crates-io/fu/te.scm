(define-module (crates-io fu te) #:use-module (crates-io))

(define-public crate-futex-0.1 (crate (name "futex") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1szxdn1ym9gpcg34dypf53q65i2q1fl52mx3isdl3gvvnxbmg1nj")))

(define-public crate-futex-0.1 (crate (name "futex") (vers "0.1.1") (deps (list (crate-dep (name "integer-atomics") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19f6vrh6ys1nfl66p4jsxm0sz7iba65c76hv5ssn2gmi2p1vqgb1") (features (quote (("nightly" "integer-atomics/nightly"))))))

(define-public crate-futex-0.1 (crate (name "futex") (vers "0.1.2") (deps (list (crate-dep (name "integer-atomics") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zp4295ayy05ka6cxd0k37ryygpsbqy1hvwmsh6jlk8kq5n0j0dr") (features (quote (("nightly" "integer-atomics/nightly"))))))

(define-public crate-futex-0.1 (crate (name "futex") (vers "0.1.3") (deps (list (crate-dep (name "integer-atomics") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lock-wrappers") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1wyfqs6jhkz036gk97zrbzl0jc8q67ikh1nmd98fwbcpm0ahwdsd") (features (quote (("nightly" "integer-atomics/nightly"))))))

(define-public crate-futex-queue-0.1 (crate (name "futex-queue") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "linux-futex") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yygvanxwhsivgsl3kwwv8yh020jmi7v5l845i3aw0fd5kd518yd")))

(define-public crate-futex-queue-0.1 (crate (name "futex-queue") (vers "0.1.1") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "linux-futex") (req "^0.1") (default-features #t) (kind 0)))) (hash "06zcinf4kfa4akf140m453vwwjsf92dy8ry088yvfyijfsib9jfl")))

(define-public crate-futex_channel-0.0.0 (crate (name "futex_channel") (vers "0.0.0") (deps (list (crate-dep (name "linux-futex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 2)))) (hash "112mqxb3fm3ical3339dvj17ixkx5sjspivbk09lksawng91b8z0")))

