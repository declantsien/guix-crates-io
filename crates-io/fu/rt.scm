(define-module (crates-io fu rt) #:use-module (crates-io))

(define-public crate-furtif-0.0.0 (crate (name "furtif") (vers "0.0.0") (hash "0kj7im6mq5y8asicz41wl43ghvk6y7nk13y7l19vcfyk2vfi9da6") (yanked #t)))

(define-public crate-furtif-0.0.1 (crate (name "furtif") (vers "0.0.1") (hash "0kijdbhina2x4nfjxkkawxa8kvvzm0ijs75hj2k9wrydyhcy9z7p") (yanked #t)))

(define-public crate-furtif-0.0.2 (crate (name "furtif") (vers "0.0.2") (hash "1xz5ip4dv46mk5dg6g75gi6rw1jigr06p2lnn6dgvx9mgjz19829")))

(define-public crate-furtif-core-0.0.0 (crate (name "furtif-core") (vers "0.0.0") (hash "0k63siwb26mkz35ch0khwayr2zhwhi8n1lnhj7s3n3vgg850iwcq") (yanked #t)))

(define-public crate-furtif-core-0.1 (crate (name "furtif-core") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hashed-type-def") (req "^0.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7.44") (features (quote ("uuid" "validation"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 0)) (crate-dep (name "silx-types") (req "^0.1.0") (features (quote ("le_silx"))) (default-features #t) (kind 0)))) (hash "19lnsl1s2y1b2cfdmd1kqxdcm03bs066why55jxwq3mkyfmzwl2k") (features (quote (("verbose4" "verbose3") ("verbose3" "verbose2") ("verbose2" "verbose1") ("verbose1") ("default" "verbose4"))))))

(define-public crate-furtif-core-0.1 (crate (name "furtif-core") (vers "0.1.1") (deps (list (crate-dep (name "hashed-type-def") (req "^0.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7.44") (features (quote ("uuid" "validation"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "silx-types") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "17xln0hkzx6khmccpj0cqm7486vqycijwc0hkwnz26rjrxjrqn3i") (features (quote (("verbose4" "verbose3") ("verbose3" "verbose2") ("verbose2" "verbose1") ("verbose1") ("silx" "silx-types" "serde" "rkyv") ("default" "silx"))))))

(define-public crate-furtif-core-0.1 (crate (name "furtif-core") (vers "0.1.2") (deps (list (crate-dep (name "hashed-type-def") (req "^0.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7.44") (features (quote ("uuid" "validation"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "silx-types") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "1knjih9rgj0chvd77rqim57rj5ya7mwlz36b98nn5zm1bnqhkj8m") (features (quote (("verbose4" "verbose3") ("verbose3" "verbose2") ("verbose2" "verbose1") ("verbose1") ("silx" "silx-types" "serde" "rkyv") ("default" "silx"))))))

(define-public crate-furtive-0.1 (crate (name "furtive") (vers "0.1.0") (hash "077mns1h421fin7dzsv65hj9m8c7h6y2g21rfsr90k9a6xl9zfwh")))

(define-public crate-furtive-0.0.1 (crate (name "furtive") (vers "0.0.1") (hash "0rygfmn92d8s6kgmfbvx155pqcsa95p87wa5gry7c69swad4xq0k")))

(define-public crate-furtive-0.1 (crate (name "furtive") (vers "0.1.1") (hash "0ias63crq1zvbb4r0sjnibwiqp5pmbbq6ir5j1hd1xbxazvm99ip")))

(define-public crate-furtive-0.1 (crate (name "furtive") (vers "0.1.2") (hash "0mshhxwkz80a7mzzq7y2c8qlyyp6iz24k3d45m3cp1r2hd9h4ppc")))

