(define-module (crates-io fu ry) #:use-module (crates-io))

(define-public crate-fury-0.0.0 (crate (name "fury") (vers "0.0.0") (hash "1pm92vs4xas3njyg9i3xj2iif1i9cirnm1lrkz8jazq8ailgvdsj")))

(define-public crate-fury-renegade-rgb-0.1 (crate (name "fury-renegade-rgb") (vers "0.1.0") (deps (list (crate-dep (name "bpaf") (req "^0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sn5h639ldnpfdvp4wypvz8gqhx9wn51jw60bj450a54qx3sjy0z")))

