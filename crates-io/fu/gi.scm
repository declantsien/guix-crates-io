(define-module (crates-io fu gi) #:use-module (crates-io))

(define-public crate-fugit-0.1 (crate (name "fugit") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "0jh6794khads1nm4ir0kkwgkvsqrw48qlwi05hzv59lzqpmpm6xi")))

(define-public crate-fugit-0.1 (crate (name "fugit") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "1qap13wncis9g3f3asd9n8p80jrk8m40rnma3zmgghnlsw9ny409")))

(define-public crate-fugit-0.1 (crate (name "fugit") (vers "0.1.2") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "10019z4p0812yb6awjy1nfm4b3k01h77cfcplmnanrgriyzwcsgv")))

(define-public crate-fugit-0.1 (crate (name "fugit") (vers "0.1.3") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "0d3pqmfk07p405rxflrrlhp5r6pk58b5b9a7r29i2axdy9py2f9g")))

(define-public crate-fugit-0.1 (crate (name "fugit") (vers "0.1.4") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "1cxg49may5biqc2mk9a63bnic7gyrkq1360izkz13hxaw5hqpf07")))

(define-public crate-fugit-0.2 (crate (name "fugit") (vers "0.2.0") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "1cdkxaql00xsigk8fkmbb3sxsjgh1s5qsg5lgms5sw8dg5y1b65c") (yanked #t)))

(define-public crate-fugit-0.2 (crate (name "fugit") (vers "0.2.1") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "0syzxsab5lzvrpzf2wyycj38762sn84pm4p7a67v2r4p894dxxgx")))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.0") (deps (list (crate-dep (name "defmt") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)))) (hash "1gcxxdfpa8x6bcq4a1021fn4pwfwkpv14nzz6by74fj5il9hhiaf") (yanked #t)))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.1") (deps (list (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)))) (hash "17qsym9hf5z84kbdj7s4qn55fq048zm6a4hbrqgli5yxmbrfn051") (yanked #t)))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.2") (deps (list (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0d1pwfwm1asbvn03jfk6xgj387a1qsd3k82sxkrsbdlpxav4c8sh")))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.3") (deps (list (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gcd") (req ">=2.1, <3.0") (default-features #t) (kind 0)))) (hash "061wpy9zbgh98w757a12wm6hip5vb4j0g3mic6v0v98iakwqkr12")))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.4") (deps (list (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gcd") (req ">=2.1, <3.0") (default-features #t) (kind 0)))) (hash "1xs7vzgs20lwbfpbg45rmzch59hlalykrh5rl3l3diad8d9wcf67") (yanked #t)))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.5") (deps (list (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gcd") (req ">=2.1, <3.0") (default-features #t) (kind 0)))) (hash "0n6bjwlxhb8zwkyvasyc1yp9ydagsarkc043x7q55jnmhdbmkn7n")))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.6") (deps (list (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gcd") (req ">=2.1, <3.0") (default-features #t) (kind 0)))) (hash "1rc7g8v9a6i8g5457av0jbwyff4r4i9c1dlc0l6p5xnyg6r7pcbs")))

(define-public crate-fugit-0.3 (crate (name "fugit") (vers "0.3.7") (deps (list (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gcd") (req ">=2.1, <3.0") (default-features #t) (kind 0)))) (hash "1rzp49521akq49vs9m8llgmdkk08zb77rry10a7srm9797b6l60p")))

(define-public crate-fugit-timer-0.1 (crate (name "fugit-timer") (vers "0.1.0") (deps (list (crate-dep (name "fugit") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1aayfn9p21bdlf02krmrr4a45vvhc13c542qbns58v95065z08vd") (yanked #t)))

(define-public crate-fugit-timer-0.1 (crate (name "fugit-timer") (vers "0.1.1") (deps (list (crate-dep (name "fugit") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "17z8k32cq8dmv4vddjc4ibcw5cszyy2yd5d0p60p7ac9fdgpnvpp") (yanked #t)))

(define-public crate-fugit-timer-0.1 (crate (name "fugit-timer") (vers "0.1.2") (deps (list (crate-dep (name "fugit") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "08agqskv6i7ywk3f3b2x015ld3zljs9kcx6q4n947rcdvr6wb6h9")))

(define-public crate-fugit-timer-0.1 (crate (name "fugit-timer") (vers "0.1.3") (deps (list (crate-dep (name "fugit") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0jp0vizdr1vwzkbbrcqpsk54cx809bg6xx84jxi9v3rq9ky7nq6r")))

