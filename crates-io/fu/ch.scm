(define-module (crates-io fu ch) #:use-module (crates-io))

(define-public crate-fuchsia-0.1 (crate (name "fuchsia") (vers "0.1.0") (hash "1pwwfngldbf3jm3jyvlzc3l1p7f735bwpg8dxy6d09v35vbc7pj3")))

(define-public crate-fuchsia-app-0.1 (crate (name "fuchsia-app") (vers "0.1.0") (hash "1q6xi66n5llpj70vqwrnvj4c7nli9rr0xr8l5j41kqaa7jziy5zi")))

(define-public crate-fuchsia-core-0.1 (crate (name "fuchsia-core") (vers "0.1.0") (hash "19jc3sblvczjqwhn49ib4ghdxgvrr2wv7h1hfpvfr480jb0vz7mr")))

(define-public crate-fuchsia-cprng-0.1 (crate (name "fuchsia-cprng") (vers "0.1.0") (hash "0cdz637xvmj9zp38c82p83x5jsllm494cw720adyliap8vmzixw1")))

(define-public crate-fuchsia-cprng-0.1 (crate (name "fuchsia-cprng") (vers "0.1.1") (hash "1fnkqrbz7ixxzsb04bsz9p0zzazanma8znfdqjvh39n14vapfvx0")))

(define-public crate-fuchsia-service-0.1 (crate (name "fuchsia-service") (vers "0.1.0") (hash "058i27gv85q6rfmbrvqbm0fpcy0ncm3bqkgzjpcpngn6llh0bxfx")))

(define-public crate-fuchsia-sys-0.1 (crate (name "fuchsia-sys") (vers "0.1.0") (hash "1b17wcc8bdmkmn3c8qyzacsm7sc62lpcg5w1jz1lchs6l5amq0xx")))

(define-public crate-fuchsia-zircon-0.2 (crate (name "fuchsia-zircon") (vers "0.2.0") (deps (list (crate-dep (name "fuchsia-zircon-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1mjmwkjdfcmyv0ghgr1mp00hxp80jjznmq8r389dnrw4ri8qzr0k")))

(define-public crate-fuchsia-zircon-0.2 (crate (name "fuchsia-zircon") (vers "0.2.1") (deps (list (crate-dep (name "fuchsia-zircon-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ngiizz8mc2fj9jycdgcqrhm6hg3mzi9xxc75gjn4cin9qd5ih7n")))

(define-public crate-fuchsia-zircon-0.3 (crate (name "fuchsia-zircon") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fuchsia-zircon-sys") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1bnpfja8b7i17s800kqg1mqvkcw09y24w8jsfxp93pqzs2pnalrv")))

(define-public crate-fuchsia-zircon-0.3 (crate (name "fuchsia-zircon") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fuchsia-zircon-sys") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "06fc7j914ibnmlyb1f6wk8qq428v10f8zgpk4jx57br5qf3h0ldx")))

(define-public crate-fuchsia-zircon-0.3 (crate (name "fuchsia-zircon") (vers "0.3.3") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fuchsia-zircon-sys") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "10jxc5ks1x06gpd0xg51kcjrxr35nj6qhx2zlc5n7bmskv3675rf")))

(define-public crate-fuchsia-zircon-sys-0.2 (crate (name "fuchsia-zircon-sys") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "10jad1bz3xbppgvaw27dkmpnfbnymln9fsrs2b38s15f9ddpkws3")))

(define-public crate-fuchsia-zircon-sys-0.3 (crate (name "fuchsia-zircon-sys") (vers "0.3.1") (hash "1rlsrwb8726hhkn7y1xvzxm3m1adsax6wpv3428pxssz1sdfz786")))

(define-public crate-fuchsia-zircon-sys-0.3 (crate (name "fuchsia-zircon-sys") (vers "0.3.2") (hash "0n8q91a6rlrx0xnypk2w0nkz2ciiaisazrrwnmr6bffn7bqsdcq8")))

(define-public crate-fuchsia-zircon-sys-0.3 (crate (name "fuchsia-zircon-sys") (vers "0.3.3") (hash "19zp2085qsyq2bh1gvcxq1lb8w6v6jj9kbdkhpdjrl95fypakjix")))

(define-public crate-fuchsia-zircon-sys-0.4 (crate (name "fuchsia-zircon-sys") (vers "0.4.0-alpha.1") (deps (list (crate-dep (name "fuchsia-zircon-types") (req "^0.4.0-alpha.1") (default-features #t) (kind 0)))) (hash "1hwpywn7519696xkng1c3gvi0jmnqldzbn1c4r1vww2pazy6xcfm")))

(define-public crate-fuchsia-zircon-types-0.4 (crate (name "fuchsia-zircon-types") (vers "0.4.0-alpha.1") (deps (list (crate-dep (name "zerocopy") (req "^0.8.0-alpha.5") (optional #t) (default-features #t) (kind 0)))) (hash "0735kq1kp2rgz1527n9379l9w4wxd40bhbs98m4gq740cn627lj0")))

(define-public crate-fuchsia_backtrace-0.1 (crate (name "fuchsia_backtrace") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "0f9ciqrygsadyg70zk854248paz46byvn5y8w0h6ghw1sd320jnd")))

