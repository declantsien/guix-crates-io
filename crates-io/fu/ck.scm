(define-module (crates-io fu ck) #:use-module (crates-io))

(define-public crate-fuck-0.1 (crate (name "fuck") (vers "0.1.0") (hash "0929pdw26v53qx02ffmmjdkwv93yk38v6zr53911bh7z1xv3qnka")))

(define-public crate-fuck-0.1 (crate (name "fuck") (vers "0.1.1") (hash "18c7iaihdpwp9fsxr1jr4xprxzj8b2gwkza7q3wm3p8fgf02m8vk")))

(define-public crate-fuck-crate-0.1 (crate (name "fuck-crate") (vers "0.1.0") (hash "1haq4p5sn8vz877bi5kqcb98n3hr3mbibh18d57w4d3d8j16v8xz")))

(define-public crate-fuck-delete-it-0.1 (crate (name "fuck-delete-it") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.54.0") (features (quote ("Win32_Foundation" "Win32_Security" "Win32_Storage_FileSystem" "Wdk_System_SystemInformation" "Wdk_Storage_FileSystem" "Win32_System_IO"))) (default-features #t) (kind 0)))) (hash "025qwc9kbaqvnfryaa2gw52xgr4fkanch2bnmdzsi9g3sv2m11l4")))

(define-public crate-fuck-github-action-0.0.0 (crate (name "fuck-github-action") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "globset") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "json5") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1frq82z2ba452fq6bmzl70xzq7bi1f7dwflkxrhd0r66glk5pszl") (features (quote (("default"))))))

(define-public crate-fuck-github-action-0.1 (crate (name "fuck-github-action") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "json5") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "wax") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1mhr1mzb1pxfmmhg96bsgigdcz4sa1w9y3q3xl5xqq57l1b9y72l") (features (quote (("default"))))))

(define-public crate-fuck-github-action-0.1 (crate (name "fuck-github-action") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "json5") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "wax") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0yck5pg0j0h680jy5647za80j5s6mdg8l07wzfwz3pbqkl6b8zry") (features (quote (("default"))))))

(define-public crate-fucker-0.4 (crate (name "fucker") (vers "0.4.0") (deps (list (crate-dep (name "docopt") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "053339x0whbc7lyb00bv0rrcbbqwm9h3alk20v9i8iadjq436hdb")))

(define-public crate-fucker-0.5 (crate (name "fucker") (vers "0.5.0") (deps (list (crate-dep (name "docopt") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "01g5y40zzh3nxh2kqrxl89bkhkavbv6rqd2mykr355f5dyjh29d0")))

(define-public crate-fucker-0.5 (crate (name "fucker") (vers "0.5.1") (deps (list (crate-dep (name "docopt") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q7ksjp2aas25pa2rh9ibrilhphmwidips2pyhm0qd6v72ny6xib")))

(define-public crate-fucker-0.5 (crate (name "fucker") (vers "0.5.2") (deps (list (crate-dep (name "docopt") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0614ak90xbj1prsb4ljsnj1zahj1w22p90pvh3fgj8wpxyyjh0si")))

(define-public crate-fucker-0.5 (crate (name "fucker") (vers "0.5.3") (deps (list (crate-dep (name "docopt") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "03nmcwrrj3fvn187m0r2dhgcv50cmdc13dqgqc909qybs76gbyb0")))

(define-public crate-fuckit-0.1 (crate (name "fuckit") (vers "0.1.0") (hash "1ckkpf5wg6yb8vlhvdm6qy9a7h9qvrlskh7m0qk4ykwnbp9sxrdw") (yanked #t)))

(define-public crate-fuckit-0.1 (crate (name "fuckit") (vers "0.1.1") (hash "1vml9rym1pz246g341wrj8x3dp2pbgjj19cahl6bnzm97j0p8c61") (yanked #t)))

(define-public crate-fuckit-0.2 (crate (name "fuckit") (vers "0.2.0") (hash "1rqd85nx88azhzipjkk4a7phhw5i9wiw5fjvqy38ixwf0ii34idb") (yanked #t)))

(define-public crate-fuckit-0.2 (crate (name "fuckit") (vers "0.2.1") (hash "1jdw992lnj29ipl94k9s30rln7vm9qvx2rdj5gazmy0bcp5s7fsg") (yanked #t)))

(define-public crate-fuckit-0.2 (crate (name "fuckit") (vers "0.2.2-fuckit") (hash "0zw9smjf6di2i6n59hbaggl2aw9c8yn5dc9idr51bmjkqn09d0gz") (yanked #t)))

(define-public crate-fuckit-0.0.0 (crate (name "fuckit") (vers "0.0.0--") (hash "1gcn14fbvlxs7b2hhcvmiizjyz3bh79md7w9qxh4fpbj4s2195b3") (yanked #t)))

