(define-module (crates-io fu rl) #:use-module (crates-io))

(define-public crate-furl-0.1 (crate (name "furl") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test_bin") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0ry8pixwiwf279y89v0pkhz6hhvc4q5ziirycakh9vb047sc7bhk")))

(define-public crate-furl-0.1 (crate (name "furl") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test_bin") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0xhmak139w3ffr2iz2bwq7jvd2zaa8xz99z3cqi21jcivj1hvipd")))

(define-public crate-furl-0.1 (crate (name "furl") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "test_bin") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0wnqrvqhmfrs8s1ljinrisnfm76xk7jj6czbrnngdgddq85i891m")))

