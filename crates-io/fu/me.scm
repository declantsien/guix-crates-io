(define-module (crates-io fu me) #:use-module (crates-io))

(define-public crate-fume-0.1 (crate (name "fume") (vers "0.1.0") (hash "1paggq5xakphf20dbzlafl7pbvac30vbnm23dv0d9ghwxw2ynrm1")))

(define-public crate-fumen-0.1 (crate (name "fumen") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0b1yx14h5l85zy8l8xxxiqq5mkfqilnx85kya1npn5lwfi40g65f")))

(define-public crate-fumen-0.1 (crate (name "fumen") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0i9yk3zfqqja51mihxm49dgjkszcmhhcz0i7vb82zj2nv2591ryn")))

(define-public crate-fumen-0.1 (crate (name "fumen") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1casqr1488lc14pxni205xd552xc74g76wrgy41zwazr8gkjqbxf")))

