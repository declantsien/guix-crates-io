(define-module (crates-io fu so) #:use-module (crates-io))

(define-public crate-fusor-0.0.1 (crate (name "fusor") (vers "0.0.1") (hash "0h6zgla6kk74walq5im4l1y5dfyc6i7724a8ba08h89c1dbgc5mf")))

(define-public crate-fusor-0.0.2 (crate (name "fusor") (vers "0.0.2") (hash "107ckibvn91yrgb130dpp8glbj473bmlp8ga9qdan81dv5g64h1m")))

