(define-module (crates-io fu nn) #:use-module (crates-io))

(define-public crate-funnel-1 (crate (name "funnel") (vers "1.0.0") (hash "1aw280dvr0bc3b3bp9z1dalz9ivy65wgc1zkklxwsc9ykmavsg18")))

(define-public crate-funnel-1 (crate (name "funnel") (vers "1.0.1") (hash "1c8wqshvwjd6yqfknyff1q693cns18q9xmmchb80bas5g7jk0pfy")))

(define-public crate-funnel-1 (crate (name "funnel") (vers "1.1.0") (hash "0g2icb32hk3i2q6fkgwp1yxqabd3yjbadpnrc6czlc5xj74sp607")))

(define-public crate-funniversaries-0.0.1 (crate (name "funniversaries") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1c0bfw6nw4vvgs5pgk0a4wzjiw3dq0jsmrk1s534d8y2jvzqn6v6")))

(define-public crate-funniversaries-0.0.2 (crate (name "funniversaries") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12xxvmmzypci6qinfiahnqdwqz3inh8p4iac421j1y8z7qc8m4l0") (yanked #t)))

(define-public crate-funniversaries-0.0.3 (crate (name "funniversaries") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02hwaawccxsdzgkmr001l2vwsx7ja4fwqqzwjafmwqvggrz2d7df")))

(define-public crate-funniversaries-0.0.4 (crate (name "funniversaries") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0a6ch69l068lmqfsyw05m8kzlghrdnyg7f8qwm7yfkyx4xb49s79")))

(define-public crate-funniversaries-0.1 (crate (name "funniversaries") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17grwsalcp4jzn602c8pln12247xp0r0qwmbglqyw4xsmzg4dga2") (yanked #t)))

(define-public crate-funniversaries-0.1 (crate (name "funniversaries") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cg5gks6a0adp4rj2q2pm2v27a553vd77nmf505d39l6pj1irmjh")))

(define-public crate-funny-0.0.1 (crate (name "funny") (vers "0.0.1-alpha") (hash "0fksbrp2xdbhjszixb2pm38mri1fy4hcdn7mx39d0ka7wbwf921j")))

(define-public crate-funny_string-0.1 (crate (name "funny_string") (vers "0.1.0") (hash "10j9x3j42m7wx8qilki97xngbki9yjad21xdn0r36q3qvbijam86")))

(define-public crate-funny_string-0.1 (crate (name "funny_string") (vers "0.1.1") (hash "0ihviq081mhzhrqfml0rkzws1synnklyiwdpvfrggnwprdcwk34r")))

(define-public crate-funnybot-0.1 (crate (name "funnybot") (vers "0.1.0") (hash "0sq429zqshnrqrfdq5jk32prjd4z4r68wa9qgwavdz7imxjmqgzh")))

(define-public crate-FunnyString-0.1 (crate (name "FunnyString") (vers "0.1.1") (hash "0jxl4x3sghb8rgpzl2q1k1gx2xcspf2vwpb1l2f4wbzbpqkdr7k4")))

