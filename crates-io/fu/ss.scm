(define-module (crates-io fu ss) #:use-module (crates-io))

(define-public crate-fuss-0.2 (crate (name "fuss") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1sx4akgia94lvsigp3k9a1g5f7a10d1v7nfd8qaaknaqfcczw01z")))

(define-public crate-fuss-0.2 (crate (name "fuss") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mw2lk1k0g9hpxv5767s4ymj5zh286wpn80zi9g9zn7fjb0lkq9v")))

(define-public crate-fuss-0.2 (crate (name "fuss") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pjcnclp5hczmi4gily6vqid0cz839fdn1m1k6c2k0kbqwp1kzl1")))

