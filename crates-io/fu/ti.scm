(define-module (crates-io fu ti) #:use-module (crates-io))

(define-public crate-futil-0.1 (crate (name "futil") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "calyx") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.22.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.129") (default-features #t) (kind 0)) (crate-dep (name "vast") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1jkyavw2b50yg7dq2wjjsb3j51injwn5facghcxny00gpb4iij25") (yanked #t)))

(define-public crate-futil-0.1 (crate (name "futil") (vers "0.1.2") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "calyx") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.22.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.129") (default-features #t) (kind 0)) (crate-dep (name "vast") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "163d7naq3h19jrgkilc1lrzk7f7fcgd75hljy5z9jqmk5ny0q82d") (yanked #t)))

(define-public crate-futilities-0.1 (crate (name "futilities") (vers "0.1.1") (deps (list (crate-dep (name "quick-xml") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "01k9hdnn8w6drjrrgxmazwma7vggsmakwy57fdhnmv8i30jw0v2f")))

(define-public crate-futilities-0.1 (crate (name "futilities") (vers "0.1.2") (deps (list (crate-dep (name "quick-xml") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1fvb7w10zmfvx31f3f4w7dbkmdyypxyb289rxf5yd8brbmn1qyad")))

(define-public crate-futilities-0.1 (crate (name "futilities") (vers "0.1.3") (deps (list (crate-dep (name "quick-xml") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "0whmd5x4n43hlr355q7kcprjgd7lwwck4462km4mznis0792kzg2")))

(define-public crate-futilities-0.1 (crate (name "futilities") (vers "0.1.4") (deps (list (crate-dep (name "quick-xml") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1rj897vz2mh2b0j3q0mjsfqbgkdrw3b4rkvvqlq60drk129gib0l")))

(define-public crate-futilities-0.1 (crate (name "futilities") (vers "0.1.5") (deps (list (crate-dep (name "quick-xml") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1sgbsv31j8i3hagw41139f1pmrra51wf3ql7jv3g0645hdfal7jl")))

(define-public crate-futility-0.1 (crate (name "futility") (vers "0.1.0") (deps (list (crate-dep (name "color-eyre") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mr7mrw8slryiixzs2fji507bd9kwb69rwdl04nxd0wm77bb6rjg")))

(define-public crate-futility-0.1 (crate (name "futility") (vers "0.1.1") (deps (list (crate-dep (name "color-eyre") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "futility-try-catch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w8qjsxqd36q01nqq3n7zkwy5zwdf19wlihcp5nllg2sxf50hqqx")))

(define-public crate-futility-try-catch-0.1 (crate (name "futility-try-catch") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10zd02652r1ds0sl717mx3cpwwx2y3gvd01limab2q3ca6q78kw3")))

(define-public crate-futils-0.0.1 (crate (name "futils") (vers "0.0.1") (hash "004xhnzdd2y88wpvmpp6266i6garnssr0m3pg80q4xnfskmyqhm3")))

