(define-module (crates-io fu nt) #:use-module (crates-io))

(define-public crate-funtime-0.1 (crate (name "funtime") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "16izal3h3i5vda9k33k9zwnl3y32fbqwmra7p0361r0f1y8g2022")))

(define-public crate-funtime-0.2 (crate (name "funtime") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0576w82sp649p30bfb9dmg1gcf2bc2b8l3r4s1x7mfh9gd8jf26c")))

(define-public crate-funtime-0.2 (crate (name "funtime") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0235d9za99lzl5pxfjayf49lskym7crdpr2m9p231c1i78dx0ny5")))

(define-public crate-funtime-0.3 (crate (name "funtime") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1hqyvw4vxhicp6i152n2lbx7j7h383igs137ic0znss1mx4gb8sv")))

(define-public crate-funtime-0.3 (crate (name "funtime") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "19x97gjlqqwddqbaw5mz9qrvz0ir5qcv0jbvqpnqgwaz0vqk94hi")))

(define-public crate-funtools-0.1 (crate (name "funtools") (vers "0.1.0") (hash "01784za94jrbajdbq66q98b7qzy2iplfzzz5jdpcscbx4k6y542a") (yanked #t)))

(define-public crate-funtools-0.1 (crate (name "funtools") (vers "0.1.1") (hash "13bpk7cs4qgh4ws14s62nsilc1ma8r3jc2268z4sb6wca8y08rg0") (yanked #t)))

(define-public crate-funtools-0.2 (crate (name "funtools") (vers "0.2.0") (hash "0lhm9m1ya61bbccglzh6sn7mgkkqa5gjkbywf8cj5a01441hc6b8") (yanked #t)))

(define-public crate-funty-1 (crate (name "funty") (vers "1.0.0") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1g749cr0x6dd8xavqkcrjzzln6wr8p0dg6irlsjlz23vjg6y6bd5") (features (quote (("std") ("default" "std"))))))

(define-public crate-funty-1 (crate (name "funty") (vers "1.0.1") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1j2jgzm1is3xx6vccflp0h4fd76szlyj3qms1zc2y739rq1j39hb") (features (quote (("std") ("default" "std"))))))

(define-public crate-funty-1 (crate (name "funty") (vers "1.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "19wx3p3jmv863y0mjb56sr4qf1kvqhl3fsyslkd92zli0p8lrlzy") (features (quote (("std") ("default" "std"))))))

(define-public crate-funty-1 (crate (name "funty") (vers "1.2.0") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "07lb1f8yih3g694id3n90anlgxf8m6p98bllsnn6dmb5rfwsniqq") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-funty-2 (crate (name "funty") (vers "2.0.0") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "177w048bm0046qlzvp33ag3ghqkqw4ncpzcm5lq36gxf2lla7mg6") (features (quote (("std") ("default" "std"))))))

(define-public crate-funty-3 (crate (name "funty") (vers "3.0.0-rc1") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1y0fn53rjzkshf80sj4qd2jw046qrc590qz3ciq9snwbx7xyjf6r") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-funty-3 (crate (name "funty") (vers "3.0.0-rc2") (deps (list (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "14isdnzb9gch51p2zz0adgfnsbn7555ssw13l2q4wmrs723h6nda") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

