(define-module (crates-io fu mn) #:use-module (crates-io))

(define-public crate-fumnet-0.1 (crate (name "fumnet") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ai7j2wl1yrq06j07kms73gn26dfhjdj8q0ya0ff04jj17igwhlw")))

(define-public crate-fumnet-0.2 (crate (name "fumnet") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)))) (hash "1d4qc1hmz4ff0kyv9y87zc03hji94xqqhl4iycl8l4vh749jswk0")))

(define-public crate-fumnet-0.2 (crate (name "fumnet") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "19dlj8xzb9k0wjdxi1nmb5fp3cx7c2m9gkwsn9njdn0cdfs16h4k")))

(define-public crate-fumnet-0.2 (crate (name "fumnet") (vers "0.2.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0jzrfqylq6hwiijx52lbpsc9d8q9j3nxzbmyvqvwnbk517vx2yry")))

(define-public crate-fumnet-0.2 (crate (name "fumnet") (vers "0.2.3") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "159n8qvj3ggmfjvhrf3n140ndjbvl25la0mg18vqc0m4mc8f356d")))

(define-public crate-fumnet-rs-0.1 (crate (name "fumnet-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0irb130hhvqd8aibl0wk6rc2ib785lq2yhdyvcx2jzccx1hfmnlz")))

