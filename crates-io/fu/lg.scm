(define-module (crates-io fu lg) #:use-module (crates-io))

(define-public crate-fulgor-0.1 (crate (name "fulgor") (vers "0.1.0") (hash "0jjcwpr79wl5nv82ngi8k6fdgchj3ixr4cc1nw6lz9j497bpanga")))

(define-public crate-fulgurobot_db-0.1 (crate (name "fulgurobot_db") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0r4kqf4r4n9qdwr5q314wdp05n9piqff0jcffbdzyihiiw5skpry")))

(define-public crate-fulgurobot_db-0.2 (crate (name "fulgurobot_db") (vers "0.2.0") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0y8g00c7x477njxn1p2qh3k2mh51bcprya2b0a91lg55mvfr5nmb")))

(define-public crate-fulgurobot_db-0.3 (crate (name "fulgurobot_db") (vers "0.3.0") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0pamybvvq6gilb1mxwx5bqslmg7ainbzmgy4lq5wlvwcp4bq8bc4")))

(define-public crate-fulgurobot_db-0.3 (crate (name "fulgurobot_db") (vers "0.3.1") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0yw36s2f645bssjlpz6xwlfjhg5ax6nyjldrz45i5n0p5852r6fr")))

(define-public crate-fulgurobot_db-0.4 (crate (name "fulgurobot_db") (vers "0.4.0") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1iy3lq0r3v2mvv16if1fmfkhc049gfg0xk6laz718h3v8cprp7x9")))

(define-public crate-fulgurobot_db-0.4 (crate (name "fulgurobot_db") (vers "0.4.1") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1k86r1vz8iw8d4y0sig14dff1fpqv0d7df4d0z00xglfav4llrdk")))

(define-public crate-fulgurobot_db-0.4 (crate (name "fulgurobot_db") (vers "0.4.2") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1q2bh34l33a04fxd9gb11mwyqwvyr8k7qawh2ywxh85d6h2i1drv")))

(define-public crate-fulgurobot_db-0.4 (crate (name "fulgurobot_db") (vers "0.4.3") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1vrhhn6pgq50gdkgcwsjp7xa37v7jqnnfp5a287aq7qmsjc2v7fd")))

(define-public crate-fulgurobot_db-0.4 (crate (name "fulgurobot_db") (vers "0.4.4") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1hyhdh1if065nz6pbbq4m8mj8nzff2cic9vnrpwrcbwjj9fpfdi0")))

(define-public crate-fulgurobot_db-0.4 (crate (name "fulgurobot_db") (vers "0.4.5") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (features (quote ("sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "03qwd14dsn3zla0r6di02cii3k1gs6waqbimslm7l1pjf7rjcwg6")))

