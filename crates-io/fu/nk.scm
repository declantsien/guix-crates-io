(define-module (crates-io fu nk) #:use-module (crates-io))

(define-public crate-funk-0.2 (crate (name "funk") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1prq0p836szai7431nb6fnb4b5gbr5cvp7cqb2iqq7y5d7dvgw8m") (features (quote (("schedule") ("default"))))))

(define-public crate-funki_lang-0.1 (crate (name "funki_lang") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "match_cast") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vrw45j9x11sd59wmsrj10h10hvpkzc9jsqakgjy8n0vr9vrx8x5")))

(define-public crate-funki_lang-0.1 (crate (name "funki_lang") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "match_cast") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ivq1161vfriz4szjqs1l0j5jyzgqd81cbn1d5ifkgb396wvpyg7")))

(define-public crate-funki_lang-0.1 (crate (name "funki_lang") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "match_cast") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "07h77nqi0zzlxab0iwrbjr0qx19rlqcgxff88cy9mcd8c051picm")))

(define-public crate-funki_lang-0.1 (crate (name "funki_lang") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0y1h0spwj70g10w0dscsw44g8scrbj1hsrd5fpjh3s42xsr620j8")))

(define-public crate-funki_templates-0.1 (crate (name "funki_templates") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.7") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "match_cast") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0w8cnj2wxzrryj0q8046srj0wcp6sa9kbnidc5775vs6nvvy7mbn")))

(define-public crate-funkjon-1 (crate (name "funkjon") (vers "1.0.0") (hash "1mj6zxcgvpay1xrbg6cs9hjb8zyw6md57xjr6lm9jg123qhahfhg")))

(define-public crate-funkjon-1 (crate (name "funkjon") (vers "1.0.1") (hash "0dn95agkijn4s1yxgcw0zq54m49q9rf5sqffq7bla5xibagfj2d9")))

(define-public crate-funkjon-1 (crate (name "funkjon") (vers "1.0.2") (hash "0gvdpx5ij19l83d5kkmb4ylmlpifig889m9n5pn5l4ldvyv8fsh7")))

(define-public crate-funksteckdose-0.1 (crate (name "funksteckdose") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "wiringpi") (req "^0.2.4") (optional #t) (default-features #t) (kind 0)))) (hash "1x2z864inp20n5fjiwdpknmjyqlz5wz41vk6l50asrlmy0fvwph6") (features (quote (("default"))))))

(define-public crate-funksteckdosen-rest-rs-0.1 (crate (name "funksteckdosen-rest-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "funksteckdose") (req "^0.1") (features (quote ("wiringpi"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4") (default-features #t) (kind 0)))) (hash "0h18s2ssq4sysaikschwykrzr4zjqwdf40xa4h19yqi8nvzwzv0k") (features (quote (("wiringpi" "funksteckdose") ("default" "wiringpi"))))))

(define-public crate-funky-0.0.1 (crate (name "funky") (vers "0.0.1") (hash "1306k7qcx9wvaz3jdrk246qf5ky87wp827zk126cj8pljhda0ijx")))

