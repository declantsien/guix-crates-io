(define-module (crates-io fu tw) #:use-module (crates-io))

(define-public crate-futwaiter-0.1 (crate (name "futwaiter") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "02wlq9i99j4wi3mw0573i8sfhaqah6ag7k2xk4q9a977r9hn3ah5") (features (quote (("global"))))))

