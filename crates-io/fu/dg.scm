(define-module (crates-io fu dg) #:use-module (crates-io))

(define-public crate-fudge-0.0.0 (crate (name "fudge") (vers "0.0.0") (deps (list (crate-dep (name "mysql") (req "^18.2.0") (default-features #t) (kind 0)))) (hash "13282qrh8ihss8as96vz2a3v8kwd999g7lw3lnw0inpx27ifg2g4")))

