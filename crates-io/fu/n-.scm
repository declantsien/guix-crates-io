(define-module (crates-io fu n-) #:use-module (crates-io))

(define-public crate-fun-folder-common-0.1 (crate (name "fun-folder-common") (vers "0.1.0") (hash "1g1arxqy4gcxyzl9m40qzj2di7fsdpkgg5vch81irsb0a4zwpz3d")))

(define-public crate-fun-folder-common-0.1 (crate (name "fun-folder-common") (vers "0.1.1") (hash "0zf0rm88cyvvzjliw97yx5c50i57i9n1xf2xqfn6252s6hj7yh38")))

(define-public crate-fun-folder-common-0.1 (crate (name "fun-folder-common") (vers "0.1.2") (deps (list (crate-dep (name "notify") (req "^4.0.15") (default-features #t) (kind 0)))) (hash "1s0hqpjz0agw0j2x9dm7y0nbv7zlbakpz17nysrlznbap73yklf2")))

(define-public crate-fun-pkg-api-0.1 (crate (name "fun-pkg-api") (vers "0.1.1") (hash "0j3krji34v91327srvcmi4q89na92i057wd9a5vk8ab1l59k7350")))

(define-public crate-fun-pkg-impl-0.2 (crate (name "fun-pkg-impl") (vers "0.2.0") (deps (list (crate-dep (name "fun-pkg-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ys42dkwgyks34zq30kxbw7dkl45lcvxqs3sdf4gkf03z7qpaxnv")))

(define-public crate-fun-pkg-impl-0.3 (crate (name "fun-pkg-impl") (vers "0.3.0") (deps (list (crate-dep (name "fun-pkg-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "08nq83n9dyk7rpbz9ivr3fvr1hd8zynf7znyx4f7jwcpvlva3zjf")))

