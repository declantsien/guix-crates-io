(define-module (crates-io fu nf) #:use-module (crates-io))

(define-public crate-funfsm-0.1 (crate (name "funfsm") (vers "0.1.0") (hash "0ddcwpgb3innrbyh12fndfl6z0db4lq4515qw9k645fj05cv8z8m")))

(define-public crate-funfsm-0.2 (crate (name "funfsm") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "08ciq7g7szvgkzg76xkygxwn12y8jdb4754xnbqpn0frjbz0a5sg")))

(define-public crate-funfsm-0.2 (crate (name "funfsm") (vers "0.2.1") (deps (list (crate-dep (name "assert_matches") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "0l4n0v6gwxhj9shmlf3107l88bgld080p76z873bnl77jkz8m5mq")))

(define-public crate-funfun-0.1 (crate (name "funfun") (vers "0.1.0") (hash "0yn31qylc3qsk7n1waifd2l72fb8q36471v9i0k08fp5h3jksb4i")))

(define-public crate-funfun-0.1 (crate (name "funfun") (vers "0.1.1") (hash "1qsbw87q006vkka2pl8c1czvmfbz2x12qhdd8jhqjzw1sildwmxg")))

(define-public crate-funfun-0.2 (crate (name "funfun") (vers "0.2.1") (hash "0hn4zg6zsmjn417pz9y5i9r6hh68y6y66js9zpid64xyk8v42j5a")))

(define-public crate-funfun-0.2 (crate (name "funfun") (vers "0.2.2") (hash "1390jlm45c8nq61qs8hjsa4wynnwqnj61s3fzz05rgd68x9is8i8")))

(define-public crate-funfun-0.2 (crate (name "funfun") (vers "0.2.3") (hash "1bhwa21q3fbrl9c4hzsd491hdd51xnaqmdiz184qkzjg7zp7vkx7")))

(define-public crate-funfun-0.2 (crate (name "funfun") (vers "0.2.4") (hash "17apapj4wzjkzxjrn2kljf7rfbndhs0jglixlk74lgkf7mlxqcbi")))

