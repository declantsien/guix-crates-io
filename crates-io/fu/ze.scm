(define-module (crates-io fu ze) #:use-module (crates-io))

(define-public crate-fuze-1 (crate (name "fuze") (vers "1.0.0") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "0hvgmvhidcha73f1r5x2gr5vlvpb1ka6ayidhw0j089fshplxhrp")))

(define-public crate-fuze-2 (crate (name "fuze") (vers "2.0.0") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "04whqd6lfkqsl9bvv4yfc5fha6ykbhsj36xqlz627vxl4d3xd04v")))

(define-public crate-fuze-3 (crate (name "fuze") (vers "3.0.0") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.8.2") (features (quote ("sync"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.8.2") (features (quote ("sync" "rt" "time" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1gq3rq9k8g24z227l1rlwb50w4yj8mg1la9xbmny2h3zwk31jxmz") (features (quote (("default" "async-std"))))))

(define-public crate-fuzed-iterator-1 (crate (name "fuzed-iterator") (vers "1.0.0") (hash "0q2pik1i77d0skg2s6ij70l6jzxxz7sd83ay2kqpwl3cpqg125qi")))

(define-public crate-fuzed-iterator-1 (crate (name "fuzed-iterator") (vers "1.0.1") (hash "1dn62ir0406iddff1m0cdqrf40npsvh3a82mwdcr3imxakkf9ja6")))

