(define-module (crates-io fu nl) #:use-module (crates-io))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.0") (hash "0fbmwyrbkgw4q1ms9bmj69j756sy7wgy24rlk30nswc6dfjzrgb6")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.1") (hash "0xm1j1fdfi61ryamkly0wybwv2z8mxjdzncjb8slpwcd0jv3s22g")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.2") (hash "0j4cingi5wcmgy3b9al9rx42fibvzifdc6viwm9m0ipb0ljcg3pk")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.3") (hash "187pr27wiph69v1njww3p56bv8j3ii5n1wkrxay9byb1xns5snkj")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.4") (hash "0l2m69ilgcxiv16i14a4h7dhsb4mq7rgrysp6284b60zlvzp18y0")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.5") (hash "0byf2cifdk7g79pbw2k9y91ri575g4sax1yb85k77xcwpbkvaplq")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.6") (hash "0gybb2a4g8g0xqfahnfh6arr7xz4mfzx80pz558k73bsbyn6zfr4")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.7") (hash "0qkdmj454kkb3angana4d7arz59nz8f2lr1px01vh248i8nj567j")))

(define-public crate-funlib-0.1 (crate (name "funlib") (vers "0.1.8") (deps (list (crate-dep (name "funlib-macros") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0v00s807474xb34w1lz1vmjxxkr3p7vrpiifqhpajz7lgqk43ram")))

(define-public crate-funlib-macros-0.1 (crate (name "funlib-macros") (vers "0.1.8") (hash "1ksaws4vb1j3hl8d0d047vjmg8348r7isxhz21nlhf9b8khcbak7")))

