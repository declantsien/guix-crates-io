(define-module (crates-io fu x_) #:use-module (crates-io))

(define-public crate-fux_kdtree-0.1 (crate (name "fux_kdtree") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "~0.1.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "~0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3.12") (default-features #t) (kind 2)))) (hash "1h7582yrjrq93m3splh8iq5h79qyl8rmjhh2wvbmn1w1z5cj8xjc")))

(define-public crate-fux_kdtree-0.2 (crate (name "fux_kdtree") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "~0.1.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "~0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3.12") (default-features #t) (kind 2)))) (hash "1rvldx2piwib5ah30xkm1jhfhkj4j8mghr03pya77a1ld0jzhsr1")))

