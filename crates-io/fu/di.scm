(define-module (crates-io fu di) #:use-module (crates-io))

(define-public crate-fudi-rs-0.3 (crate (name "fudi-rs") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jxpf2bx5llysq1n7jcb36d7y6l5f0klzxcygs8dsm2ifp5hbrmv")))

(define-public crate-fudi-rs-0.3 (crate (name "fudi-rs") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0xjl270q4cw8jyfqswgzm3kmzaas8avim031x8722nmnwq210b3a")))

