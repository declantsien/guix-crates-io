(define-module (crates-io fu id) #:use-module (crates-io))

(define-public crate-fuid-0.1 (crate (name "fuid") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1a8mxnpyhq317mk6cd6aglsfginm5d91yjccxph4dsnq7n829p6j")))

(define-public crate-fuid-1 (crate (name "fuid") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0a5wh8k8zx7rr49sb2j5srxcakbrzvlzvzbk2dqfp9nic039h9k5")))

(define-public crate-fuid-1 (crate (name "fuid") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1zvj49an15cg4iq3ipfs21k4f8gimqizf9sayk5yhv6jkx1114f1")))

(define-public crate-fuid-1 (crate (name "fuid") (vers "1.2.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1") (features (quote ("v4"))) (kind 0)))) (hash "0g6gpyvxzn9lnr8jg10in79sdrhs6pnc6w49f2xnj3mg8rg5b55b") (features (quote (("std" "serde/std" "uuid/std") ("default" "std"))))))

(define-public crate-fuid-1 (crate (name "fuid") (vers "1.2.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1") (features (quote ("v4"))) (kind 0)))) (hash "1416z1gqwv9ap7qg4mwl61f5qfvb43gaxmx3rnf9lc86qa7vf7kl") (features (quote (("std" "serde/std" "uuid/std") ("default" "std"))))))

