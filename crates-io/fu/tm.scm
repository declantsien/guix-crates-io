(define-module (crates-io fu tm) #:use-module (crates-io))

(define-public crate-futmio-0.0.1 (crate (name "futmio") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6.20") (default-features #t) (kind 0)))) (hash "0xjbsnh6zbs4ibrj9r71jnb7yqpccbf1n8r3jfyz3karc9sf47za")))

