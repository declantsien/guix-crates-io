(define-module (crates-io fu rz) #:use-module (crates-io))

(define-public crate-furze-0.0.1 (crate (name "furze") (vers "0.0.1") (deps (list (crate-dep (name "cbindgen") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w5zfjnqb6554plld48b1xzv5csf3vfrmwgf6zjrmm48jx1ci1ys")))

(define-public crate-furze-0.0.2 (crate (name "furze") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "varintrs") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "10pxiqicybs958c2djhz6zxd10cqy07c8wdbqizxbhcj2kpyqsj8")))

(define-public crate-furze-0.0.3 (crate (name "furze") (vers "0.0.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "varintrs") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0fxm1s9gnx4yib7ndf944an3x4h4rf66pjc3xx648plfhm5vcqcw")))

(define-public crate-furze-0.0.4 (crate (name "furze") (vers "0.0.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "varintrs") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1zi8pkwiwcdjab36s3jsvxb8w3j11n4msbjhjpr8pqyxj8w6fxnj")))

(define-public crate-furze-0.0.5 (crate (name "furze") (vers "0.0.5") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "varintrs") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "02dlsp4x54bg7i8xrvin02n3b0lgkq6a0xh7ay1bbnc53w22vnkf")))

(define-public crate-furze-0.0.6 (crate (name "furze") (vers "0.0.6") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "varintrs") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0dzvra1a7whffm4mbghnbf34wshqal88pk2qkqw93x13vf4b1h5n")))

