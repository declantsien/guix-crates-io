(define-module (crates-io fu rn) #:use-module (crates-io))

(define-public crate-furnace-0.1 (crate (name "furnace") (vers "0.1.0") (hash "0lxihmwg3fmhqfcd8ymsg5gnzp0akixwk5l67nwhlxfb092rfrlq") (yanked #t)))

(define-public crate-furnace-0.2 (crate (name "furnace") (vers "0.2.0") (deps (list (crate-dep (name "arc-swap") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0sgclqkn1ilb3h435mgvxy9v5nlbmkc84rpn7b9xvx6sajyf0010")))

(define-public crate-furnace-0.3 (crate (name "furnace") (vers "0.3.0") (deps (list (crate-dep (name "arc-swap") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1vzldca41k6azihpd7n4vwn9c8xxz7pckyf5hqb8k6343rc2vnya")))

(define-public crate-furnace-iui-0.2 (crate (name "furnace-iui") (vers "0.2.0") (deps (list (crate-dep (name "furnace") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "iui") (req "^0.3") (default-features #t) (kind 0)))) (hash "126jp0lkhxw8fnyyf53gjk6j8x70gi2zhrz38x5dbc75rj685d8c")))

(define-public crate-furnace-iui-0.3 (crate (name "furnace-iui") (vers "0.3.0") (deps (list (crate-dep (name "furnace") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "iui") (req "^0.3") (default-features #t) (kind 0)))) (hash "10lbn2mg0hamn8j5awxq3kmrmyw86g4lxvca5w5ma2864jcy5yw5")))

(define-public crate-furnel-0.1 (crate (name "furnel") (vers "0.1.0") (deps (list (crate-dep (name "brotli") (req "^3.3.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "134mv950v2mxhh7fx5xwdmjfsk918cv8bnlqq6qxdwvy8n9lwq7r") (rust-version "1.56.1")))

