(define-module (crates-io fu z_) #:use-module (crates-io))

(define-public crate-fuz_json_parser-0.1 (crate (name "fuz_json_parser") (vers "0.1.0") (deps (list (crate-dep (name "args") (req "^2.2.0") (default-features #t) (kind 2)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 2)))) (hash "0aby93lvmm58shx8nb2fffivz5nqbk4xdjnjl7s74225prf32di3")))

(define-public crate-fuz_json_parser-0.1 (crate (name "fuz_json_parser") (vers "0.1.1") (deps (list (crate-dep (name "args") (req "^2.2.0") (default-features #t) (kind 2)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 2)))) (hash "0yfbxz65f6dyk2lcz06ly3pk4cvnvr28dx9inrsdyfgzf820hix6")))

(define-public crate-fuz_json_parser-0.1 (crate (name "fuz_json_parser") (vers "0.1.2") (hash "1irmfm1qg3pfcay70zlfnw4qd92a07njs3ldd30l0dvwxkgqqkxs")))

