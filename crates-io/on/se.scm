(define-module (crates-io on se) #:use-module (crates-io))

(define-public crate-onsen-0.1 (crate (name "onsen") (vers "0.1.0") (hash "0inw7adqa4i8rwwzkfsv8ydfx3556j9zdl32lwkisbc108i9966h")))

(define-public crate-onsen-0.2 (crate (name "onsen") (vers "0.2.0") (hash "01xp05a34859vbr4bdiqnxw64pqy1pcxlmnhfprsf8nmk8cyw5qm")))

(define-public crate-onsen-0.3 (crate (name "onsen") (vers "0.3.0") (hash "1d3vzzq3bqa9syhcvh1z9jvrq3y2micd2x3icqa6dq71sgj20lqv")))

(define-public crate-onsen-0.4 (crate (name "onsen") (vers "0.4.0") (hash "0bhqnk4b26yq64qf5wlvsvw3wxwyyp00yy0c5vprq9f6zbi9qgxm")))

(define-public crate-onsen-0.5 (crate (name "onsen") (vers "0.5.0") (hash "0363ciarghfx42dp4vzn1849yg447kf8xgizsk2k99kgf8bawb2l")))

(define-public crate-onsen-0.6 (crate (name "onsen") (vers "0.6.0") (hash "121sri6yk4jl468hvsyzpcfkiwllm4diwk9v6inkbz9x61syqrcf")))

(define-public crate-onsen-0.6 (crate (name "onsen") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1y0s4viz1vq42gzj2pf9qhpgdbrzla6l8dyn71pircfhhvwk45cc")))

(define-public crate-onsen-0.7 (crate (name "onsen") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1imphjwhp9gsbw1kq86q4inak1km91s3xrg1q5jndhlpvg4m9a9w")))

(define-public crate-onsen-0.7 (crate (name "onsen") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0gbpdcyknvwj0qvhxjmnzb4bblcwdraqwvrmxchi260fibim7rvb")))

(define-public crate-onsen-0.8 (crate (name "onsen") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "06lyill60nvdpz2a2rqab1c1v5kv4sqq5z2lm74ndk0plprnclnf")))

(define-public crate-onsen-0.9 (crate (name "onsen") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "15f029z3fbjypbihw26w5lfn4wxid8ygf5mmi6arhlmgsh98wbsy")))

(define-public crate-onsen-0.9 (crate (name "onsen") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "05gkrivnfcbx48xixyrk1m0d6rgah8rvpqc7bngddkkf5p7a3l3v")))

(define-public crate-onsen-0.10 (crate (name "onsen") (vers "0.10.0") (deps (list (crate-dep (name "assoc_static") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "threadcell") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "09rvw8nqqdfkw8vb3gvw718mdwhfvkkfx1ff2scqyn59pl6sjzra") (features (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (v 2) (features2 (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.11 (crate (name "onsen") (vers "0.11.0") (deps (list (crate-dep (name "assoc_static") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "threadcell") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gbwgxk8i556aps9f9pwnab9jbkpp20hjg3khaz81xka9ab99acr") (features (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (v 2) (features2 (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.12 (crate (name "onsen") (vers "0.12.0") (deps (list (crate-dep (name "assoc_static") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "threadcell") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jky0mf2hnaj2mbqga7f9rg520dila35yv9rilx24aif3nx2p7dx") (features (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (v 2) (features2 (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.13 (crate (name "onsen") (vers "0.13.0") (deps (list (crate-dep (name "assoc_static") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "threadcell") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "02sfxh1j807adzi6lf6a90i7xc542vgkrcpp6vg4max34i2ymc47") (features (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (v 2) (features2 (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

(define-public crate-onsen-0.14 (crate (name "onsen") (vers "0.14.0") (deps (list (crate-dep (name "assoc_static") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "threadcell") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)))) (hash "1m5zwkj3rqjb55mh023g9c0wk5wig2l6ifqc1p1pr4pfw1nww0d2") (features (quote (("st_tbox" "tbox" "stpool") ("default" "st_tbox")))) (v 2) (features2 (quote (("tbox" "dep:assoc_static") ("stpool" "dep:threadcell"))))))

