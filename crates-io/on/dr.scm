(define-module (crates-io on dr) #:use-module (crates-io))

(define-public crate-ondrop-0.1 (crate (name "ondrop") (vers "0.1.0") (deps (list (crate-dep (name "dropcheck") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0g3z55p7rjiaqlm1lbjhchpvafm1ha622snw3dz6j0x31rxjmshv")))

