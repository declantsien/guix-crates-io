(define-module (crates-io on _d) #:use-module (crates-io))

(define-public crate-on_demand-0.1 (crate (name "on_demand") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ddgb5gyz753m2qac2l9zw89dl6yhy1qk72gvgk0xc25yakrgspb")))

(define-public crate-on_drop-0.1 (crate (name "on_drop") (vers "0.1.0") (hash "1ncf64qgyvhcab4b6g1s5wdc6947g2sxdkp8lkw71h31nj4dp9sf")))

(define-public crate-on_drop-0.1 (crate (name "on_drop") (vers "0.1.1") (hash "0k00dvwqlis6nvajhsrr4xbhivy1n2gxrjjcg0pmcffyh37dnsqi")))

(define-public crate-on_drop-0.1 (crate (name "on_drop") (vers "0.1.2") (hash "00kpg10wpyh71bmmrxlbwhkjwswfjvn899gxq3pdjqsxxnbl2bcp")))

