(define-module (crates-io on yx) #:use-module (crates-io))

(define-public crate-onyx-0.1 (crate (name "onyx") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "094bfqqgk2qd1amj5sdygq00h5161ckps5zi4x0s1l36fxi02d8a")))

(define-public crate-onyx-0.2 (crate (name "onyx") (vers "0.2.0") (deps (list (crate-dep (name "glium") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ky8ivn4103ysf81hzafw4z0in6q67q1n49znghfaqhvjchgfdap")))

