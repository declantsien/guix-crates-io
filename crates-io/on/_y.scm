(define-module (crates-io on _y) #:use-module (crates-io))

(define-public crate-on_your_marks-0.1 (crate (name "on_your_marks") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1vxalrsfa21jj3cppv728i7a3c12j5111gqi4nqr9sn8ans02n1w")))

(define-public crate-on_your_marks-0.1 (crate (name "on_your_marks") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "18xf0n0m0im77if0masln21lspkcncjq3h9i1fq2y5fp1rz5fv0x")))

