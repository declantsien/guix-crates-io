(define-module (crates-io on -e) #:use-module (crates-io))

(define-public crate-on-email-0.1 (crate (name "on-email") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "imap") (req "2.3.*") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.0.*") (default-features #t) (kind 0)))) (hash "1h8a49cz67zida36idrryfrr4fxg9q20y98nfp6vigzwxgxiq4z8")))

