(define-module (crates-io on ay) #:use-module (crates-io))

(define-public crate-onay_stat-0.1 (crate (name "onay_stat") (vers "0.1.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1mrsfclfdzy32fp4yrf2isa5v4hm0j7rpbh0a1y1912f8cfr6qfv") (yanked #t)))

(define-public crate-onay_stat-0.1 (crate (name "onay_stat") (vers "0.1.1") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0bfkqa7js14n81x0g55kfg229yxkljrxdn6x2fsbsdsh39q1zlh4") (yanked #t)))

(define-public crate-onay_stat-0.1 (crate (name "onay_stat") (vers "0.1.2") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0x4l3pzcav25kyd8ahrn27ycxz151kfn3kyirng3wx2ajvlvf8dn") (yanked #t)))

(define-public crate-onay_stat-0.1 (crate (name "onay_stat") (vers "0.1.3") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "1dsva2npwwx1j4wlm2jfmsgx0kll6srcq2hzhvsxpkzfv2yrdvkr")))

