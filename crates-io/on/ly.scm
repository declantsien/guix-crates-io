(define-module (crates-io on ly) #:use-module (crates-io))

(define-public crate-only-0.1 (crate (name "only") (vers "0.1.0") (hash "0ci7r80yrr6disbsgkvw94bi7k12j0mgf3j5bfl656jdd7ys7126")))

(define-public crate-only-brain-0.1 (crate (name "only-brain") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "07375w5i9dmkqgl7lbhhjqijgxz5b73za4x9vw5hilg7py7fhgns")))

(define-public crate-only-brain-0.1 (crate (name "only-brain") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0zc5n6nfwlyhz03q0iy7w3jpbhshwfzbfhvmcnb13lkqcq7a7p8a")))

(define-public crate-only-brain-0.1 (crate (name "only-brain") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1pjqbpyi02pqpjak27sy6fpmgqnhp1a7w24lgv7vq49ivdsnrafw")))

(define-public crate-only-brain-0.1 (crate (name "only-brain") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "108wnkf1aypm3l8ivnk3d56yzzac2qislnz9xay935fvkbndjjrn")))

(define-public crate-only-brain-0.1 (crate (name "only-brain") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xk13hyfd62l5ijmi7a90jxwmr7qbv1893ilww5k9wmi6mh0vc5f")))

(define-public crate-only_every-0.1 (crate (name "only_every") (vers "0.1.0") (deps (list (crate-dep (name "quanta") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)))) (hash "1jb1bq8s9g2k8xzyamy7xp6g21xamcfv9wqw8xn16a8527873vni")))

(define-public crate-only_scraper-0.1 (crate (name "only_scraper") (vers "0.1.0") (hash "0l61ri4h4h06hq3brsqjgvwdi1za5ly0g273rd1qh5gvzsh4mync")))

(define-public crate-only_scraper-0.1 (crate (name "only_scraper") (vers "0.1.1") (hash "088sv52bi977vb4q9a9a8y9mda9hda747i1sy34pq3lkxyrfhm17")))

(define-public crate-only_scraper-0.1 (crate (name "only_scraper") (vers "0.1.2") (hash "0ndcv1ihf8cy47v00bx5x5zbyr2614p9qi8b64vyh00jv70k1jwx")))

(define-public crate-only_torch-0.0.1 (crate (name "only_torch") (vers "0.0.1") (hash "1g0z621x62dkpkravxjprrbgql9zgp17vl2s2j78fdagd16cxvqs")))

(define-public crate-only_torch-0.0.2 (crate (name "only_torch") (vers "0.0.2") (hash "1is6ys5cifdrrhbq605v3a3h9zad22mn8cfajp4wvzra3ckp9bac")))

(define-public crate-only_torch-0.0.3 (crate (name "only_torch") (vers "0.0.3") (hash "1z24kdhxz74yzhpx5jb95pc1gknbc6pzv591hqzp26n09805bppd")))

(define-public crate-only_torch-0.0.5 (crate (name "only_torch") (vers "0.0.5") (hash "0xm39l8dkj384hwn18xj3h897ismbrx9np5xx5638f4csg0ifgbk")))

(define-public crate-only_torch-0.0.6 (crate (name "only_torch") (vers "0.0.6") (hash "1wvbr53sxckr0488p5pkalmw3drpzbifmfjznc2knpr8did6r22w")))

(define-public crate-only_torch-0.0.7 (crate (name "only_torch") (vers "0.0.7") (hash "0npwfld0gjnnairy1agn20ab9764cidvhhj3bj5ijz9f954l8fk9")))

(define-public crate-only_torch-0.0.8 (crate (name "only_torch") (vers "0.0.8") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1ydchqp8fqx77fz5b07nk151lr1bnr8q6q1cqp6k8fb0ncii37sg")))

(define-public crate-onlyargs-0.1 (crate (name "onlyargs") (vers "0.1.0") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)))) (hash "04kgvf5rxv6gdl1dpxlcxzrajfjnwd0rxp5yn539yf91prfhid4s")))

(define-public crate-onlyargs-0.1 (crate (name "onlyargs") (vers "0.1.1") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)))) (hash "0ybwadcz345509pkch03scvgvim34alzfm1kac9b4xh28qbx156i")))

(define-public crate-onlyargs-0.1 (crate (name "onlyargs") (vers "0.1.2") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)))) (hash "11rmzzpxbvx6r1yp62sl96yfmjm2f420z32p2iwymlb30n0lb9a3")))

(define-public crate-onlyargs-0.1 (crate (name "onlyargs") (vers "0.1.3") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)))) (hash "0cy7ip3pv5nviyln5vkxz2pjfmpsky68126lch1c66gxyr0a7aqk") (rust-version "1.62.0")))

(define-public crate-onlyargs-0.2 (crate (name "onlyargs") (vers "0.2.0") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)))) (hash "1131zsmsr4p8dqfzwl1y02a2a02jngsnl1ad52l9s4f0nmg8mmq0") (rust-version "1.62.0")))

(define-public crate-onlyargs_derive-0.1 (crate (name "onlyargs_derive") (vers "0.1.0") (deps (list (crate-dep (name "myn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "onlyargs") (req "^0.1") (default-features #t) (kind 0)))) (hash "0935vxc72ikq6xfyahpnxjkxi9chd17gq3hq6ihjg37pw0akng85")))

(define-public crate-onlyargs_derive-0.1 (crate (name "onlyargs_derive") (vers "0.1.1") (deps (list (crate-dep (name "myn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "onlyargs") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mzgvjm3146klmdn7aphidbmqxhcafgfs34b2vilb1hzifh5hm1a")))

(define-public crate-onlyargs_derive-0.1 (crate (name "onlyargs_derive") (vers "0.1.2") (deps (list (crate-dep (name "myn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "onlyargs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mk5qmc8v1qh70iv6l7npaycvxm4wp7chh9fadzx2fc8v3amcjj0")))

(define-public crate-onlyargs_derive-0.1 (crate (name "onlyargs_derive") (vers "0.1.3") (deps (list (crate-dep (name "myn") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "onlyargs") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pv4h8pcz9fbccvpy5dj5cf9cmkfj0f60i1hc3v54wajndzxi4w7") (rust-version "1.62.0")))

(define-public crate-onlyargs_derive-0.1 (crate (name "onlyargs_derive") (vers "0.1.4") (deps (list (crate-dep (name "myn") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "onlyargs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "06r866b2pn66x02qnr1478mpvfrd77frgmjk6ybqdb8p697gp32f") (rust-version "1.62.0")))

(define-public crate-onlyargs_derive-0.2 (crate (name "onlyargs_derive") (vers "0.2.0") (deps (list (crate-dep (name "myn") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "onlyargs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1mkwckyj84dnhbphz2bmqqzmdfk58wc08cfcfizdfhxyrv4dx5ly") (rust-version "1.62.0")))

(define-public crate-onlyerror-0.1 (crate (name "onlyerror") (vers "0.1.0") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "myn") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g7qnl9sdn0nfq491xy7fnivpf6h4ps0llzkb6r85vzpr5ij1rix") (features (quote (("std") ("default" "std"))))))

(define-public crate-onlyerror-0.1 (crate (name "onlyerror") (vers "0.1.1") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "myn") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wwj8lpch11cl6q771psxgn7zm87q24zvy756ly2csin17v0kcvw") (features (quote (("std") ("default" "std")))) (rust-version "1.62.0")))

(define-public crate-onlyerror-0.1 (crate (name "onlyerror") (vers "0.1.2") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "myn") (req "^0.1") (default-features #t) (kind 0)))) (hash "1x19nlkkwgvab8zhd39pf693l5dmipr27c49ngrg4mwvz453716j") (features (quote (("std") ("default" "std")))) (rust-version "1.62.0")))

(define-public crate-onlyerror-0.1 (crate (name "onlyerror") (vers "0.1.3") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "myn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1hrg5qrr5rzfx9hpaqr7xf6y0dqpmz1gcc8mk2fggnfg8rgnhrla") (features (quote (("std") ("default" "std")))) (rust-version "1.62.0")))

(define-public crate-onlyerror-0.1 (crate (name "onlyerror") (vers "0.1.4") (deps (list (crate-dep (name "error-iter") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "myn") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1nw70pf1yvgdahfgrvbsgddhrrh6as057227vk77m6yd5kmd89lc") (features (quote (("std") ("default" "std")))) (rust-version "1.62.0")))

(define-public crate-onlytypes-0.1 (crate (name "onlytypes") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jqdnl871r68f9625z4y5jfmj0q1xcc01sf8rqhmh5fgyi2iajgc")))

(define-public crate-onlytypes-0.1 (crate (name "onlytypes") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cir9k2xjh7v3r1b533479phcyvsbz58dap9fp6al19i7kzcx9w5")))

(define-public crate-onlytypes-0.1 (crate (name "onlytypes") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0f0pvp5ji7wwkqpk7s40k5c5bar8ndmrm73yy76ijw5r504i55by")))

(define-public crate-onlytypes-0.1 (crate (name "onlytypes") (vers "0.1.4") (deps (list (crate-dep (name "otcore") (req "^0.1.4") (default-features #t) (kind 0) (package "onlytypes-core")))) (hash "0npfp8gz2a1y14qvbrvdgb36zfsqmfhr9582qhlsln7jidal84qy")))

(define-public crate-onlytypes-core-0.1 (crate (name "onlytypes-core") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y7rc4xvvklxysiiijzsidhi6wq0kach6chac9ys183154nj5783") (features (quote (("debugging"))))))

