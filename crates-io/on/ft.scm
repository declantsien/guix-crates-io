(define-module (crates-io on ft) #:use-module (crates-io))

(define-public crate-onft-0.1 (crate (name "onft") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)))) (hash "199bs9rb1dg0ix6w7z6fa5n3z42czxmxyxgy85i7bdwjcz9jwr8k")))

(define-public crate-onft-0.1 (crate (name "onft") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)))) (hash "0575c51lxxb8lwj6yf6rix6hrwv3na0mpmcg7gagaz0pr38qiqqq")))

