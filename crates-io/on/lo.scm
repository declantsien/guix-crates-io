(define-module (crates-io on lo) #:use-module (crates-io))

(define-public crate-onload-0.1 (crate (name "onload") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "09jhv48nhf4cy94vz0rpw2mfqmrgpdvphcsn66zklbhhi1hij15z") (features (quote (("release") ("default" "release") ("debug"))))))

(define-public crate-onload-0.1 (crate (name "onload") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "0j1sn0ljvl9ziczdz8xd7bxx4x3ijgxk00p2kbsx576jnbfjb4vx") (features (quote (("release") ("default" "release") ("debug"))))))

(define-public crate-onload-0.1 (crate (name "onload") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.60") (optional #t) (default-features #t) (kind 1)))) (hash "12f4vlk6azllmbigy8clmz0sl5rc2z9yyg31b5xj5jqh3dwmic4j") (features (quote (("release") ("default" "release") ("debug" "bindgen"))))))

(define-public crate-onload-0.1 (crate (name "onload") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.60") (optional #t) (default-features #t) (kind 1)))) (hash "1zml9lnfhgy3kpcnn31d5gqc3m4rbr4py4r8h30grp0jlhgi2gfg") (features (quote (("release") ("default" "release") ("debug" "bindgen"))))))

(define-public crate-onload-0.2 (crate (name "onload") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (optional #t) (default-features #t) (kind 1)))) (hash "16cm9hyrhymahshvx2d2yjcwnzmr5wy1c3g3x4szfqpkvd1z5086") (features (quote (("release") ("default" "release") ("debug" "bindgen"))))))

