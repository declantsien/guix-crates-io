(define-module (crates-io on it) #:use-module (crates-io))

(define-public crate-onitama-0.1 (crate (name "onitama") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kia87f7brfn2d7j4vnz40rvm5bsj2qp1ckd7d2dsmv793w7cgwc")))

(define-public crate-onitama-0.1 (crate (name "onitama") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fvvahgz85pv1y0naslccysn907fdq8zrq499a79ljhc14lb593j")))

(define-public crate-onitama-0.1 (crate (name "onitama") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q6896730x6anqrrir84g3p5vq33nj2mvrax2lxrwsxllzabaf85")))

