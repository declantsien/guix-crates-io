(define-module (crates-io on ht) #:use-module (crates-io))

(define-public crate-onhtml-4 (crate (name "onhtml") (vers "4.0.0") (deps (list (crate-dep (name "duplicate") (req "^0") (default-features #t) (kind 0)))) (hash "01k0zg40dybd2aizqy35d5h3c3djw9p0jgwjfajiw80rjnnqvxir") (rust-version "1.56")))

(define-public crate-onhtml-4 (crate (name "onhtml") (vers "4.0.1") (deps (list (crate-dep (name "duplicate") (req "^0") (default-features #t) (kind 0)))) (hash "1ica80h5zb7jnvfw3bbf47cy03rl63v6wcdbwqabx20b6qrichg7") (rust-version "1.56")))

(define-public crate-onhtml-4 (crate (name "onhtml") (vers "4.0.2") (deps (list (crate-dep (name "duplicate") (req "^0") (default-features #t) (kind 0)))) (hash "157ap6vjdsr09lzh1xvjmjl1nry4kbdjj114qk93n5djmzcxak4s") (rust-version "1.56")))

