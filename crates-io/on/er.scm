(define-module (crates-io on er) #:use-module (crates-io))

(define-public crate-oner_induction-0.2 (crate (name "oner_induction") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)))) (hash "13r8p30mg8x9i3csqqliyyqvav0il1yrcr9r7wgfxp3rplcykix9")))

(define-public crate-oner_induction-0.2 (crate (name "oner_induction") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0j273h20bs04gmz2iii0ni1b70ycvrgda6gipwbx1ablv1q14zgf")))

(define-public crate-oner_quantize-0.1 (crate (name "oner_quantize") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "ord_subset") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "030ny0lzk8dchyz8xyvsprcsji3j58565bb36cqlpcdr1mm8dpvq")))

