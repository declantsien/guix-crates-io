(define-module (crates-io on ix) #:use-module (crates-io))

(define-public crate-onix-0.1 (crate (name "onix") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "drm") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27") (features (quote ("ioctl" "mman" "poll" "fs"))) (default-features #t) (kind 0)))) (hash "0qq9fax0mv9l81jlic9w7q6wx0dlh3fd40h212x8bk96x197gvgn")))

