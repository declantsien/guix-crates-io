(define-module (crates-io on ew) #:use-module (crates-io))

(define-public crate-onewire-0.1 (crate (name "onewire") (vers "0.1.0") (hash "1zb435wbffc5mn5lb08g0p2c04wwcnhs3bxy6mb0c1b8b7g2mgg6")))

(define-public crate-onewire-0.1 (crate (name "onewire") (vers "0.1.1") (hash "0ykin8psf8vbhsshc42mm5v6y5blvzjfbmj647j9pb8szpm6p6g2")))

(define-public crate-onewire-0.2 (crate (name "onewire") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0yj9qqkkzl7qinbawf8yhndmh622jdi4f2piiv21rczrjmj463jd")))

(define-public crate-onewire-0.2 (crate (name "onewire") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1l49dkyl2k2byx014b1rdwycmlwakm2xb1yd1li389ffdr6yp6dr")))

(define-public crate-onewire-0.2 (crate (name "onewire") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1s7ln70gccq6c0a7n0rj5kc41l3ln1qschl2pir62jb33fpcn0p7")))

(define-public crate-onewire-0.2 (crate (name "onewire") (vers "0.2.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0lpvy9amlbgw2gnm1n7f2y3kxmq4d1264b7aqvg7i45m8p9z98k8")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0sc49qv815yigqf0vpfajiv0aq76h2f1vybhh6wfr1lgmp82ihqr")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1zxfwglpplyap7dqgbs6rhwg1nq8vf26xkqrk9sfi9bfkfx3n9md")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1ss85dlbjrw3jbdhi1n25wwsf8fj9kiwrv8pdssi5dzjy3pliv16")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0ns0zpcpv86wa9cgdx6187dh1k6r7979z4ga0il74cwqwrv370wx")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.4") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0dgy5dddbm4xajivm7fjpxhwd90glak1v54qw5wmmgqd6k34zm7i")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.5") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0ajq2b9nx018jc4w7lyj5g8p6c5h0ddpzns8l0lrainxzgpiqm0l")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.6") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1c6lf1l7ck555vz1ra8ksycxibac8ca1igr06l5kd3xvyrl5c98k")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.7") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1v62sn3bjy8avzjhzs710sfqna5npdn5yh706pg1ajylwbnnklpi")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.8") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "18p4v8jx5vjb34aysnsfyjrdnp414rr4342pyi51hzkf6qc9cna3")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.9") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0ccqci21hgsi2slw5467sg3fs1i6g44hyl19199nzpii9bp20ysx")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.10") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0klxils1jhm7p9l8hwlkyc07xcdvbyb3yn9nc55rxk85mg4mqsza")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.11") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "03dzw7spd36951z2nwlc0n0va4b3pkqzqb341rr5yppl2j4gz6pm")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.12") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "0pw81ckbj8wdqcp0d9xvnyf8vzjnh0is73dp7kgvy0xriwvqf432")))

(define-public crate-onewire-0.3 (crate (name "onewire") (vers "0.3.13") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "18cy5hq4sgl69l9m7awxgk0g5v87kjhb842mc9ihj30lkvf6ggkk")))

