(define-module (crates-io on el) #:use-module (crates-io))

(define-public crate-oneline-eyre-0.1 (crate (name "oneline-eyre") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)))) (hash "15rh9ih8jpy104zfvyzx2ifiq465fvklisjql66cxh49wshifbw6") (rust-version "1.56")))

(define-public crate-oneline-template-0.1 (crate (name "oneline-template") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unstructured") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ga748sqcsz9d35123n2yyn329rqy1jzxfa821kacg17z7iyqzwl")))

(define-public crate-oneline_str_enum-0.1 (crate (name "oneline_str_enum") (vers "0.1.0") (deps (list (crate-dep (name "simple_string_lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0a9v7wag7bz14jn2v7qi9b1iskyq0yr6ql86ig4ql9jv1ivln3hv")))

(define-public crate-oneline_str_enum-0.1 (crate (name "oneline_str_enum") (vers "0.1.1") (deps (list (crate-dep (name "simple_string_lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d2kqp48arx5q2v93ryp2bv95v52c9rdy78wzxcjixrm7hwf2fzh")))

(define-public crate-oneline_str_enum-0.1 (crate (name "oneline_str_enum") (vers "0.1.2") (deps (list (crate-dep (name "simple_string_lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05czd4dhhcncz1a8vlgbg7qais477wgsc9v51j6zpjgqhac3x117")))

(define-public crate-onelo-0.1 (crate (name "onelo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "=3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.24") (features (quote ("bundled" "functions" "limits" "load_extension"))) (default-features #t) (kind 0)))) (hash "0r8ypkhq4p6f1f80khqbcmxi8q7db5qd6vzpa83ajcm2hlrsli6c")))

