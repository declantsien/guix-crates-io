(define-module (crates-io ox il) #:use-module (crates-io))

(define-public crate-oxilangtag-0.1 (crate (name "oxilangtag") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "03s0vjdzgx6qwa21anmd9badi9arqffs3ycm4lz5k5p750v6ffc9") (yanked #t)))

(define-public crate-oxilangtag-0.1 (crate (name "oxilangtag") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0lfli3bybr2x9xvpr8d9bjvxb5xw4ipni8zac7krlx1bw2f1xfci") (yanked #t)))

(define-public crate-oxilangtag-0.1 (crate (name "oxilangtag") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "05jka0yl3jkdqqadb87cvi1fzl3hwmwgwc6y6mcf4l9wkkplz6y5")))

(define-public crate-oxilangtag-0.1 (crate (name "oxilangtag") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "06qqikyhdrp4i1n3wfr8pc2vz4f4x2j4a4s78fa2fw5rzgsfv4cd") (features (quote (("serialize" "serde") ("default"))))))

(define-public crate-oxilangtag-0.1 (crate (name "oxilangtag") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req ">=0.4, <0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.100") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1x50sm7c7j8qixy19g7sm5r3x1j9h2m0pjl9cmc30l3gjqaz04bl") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std") ("serialize" "dep:serde") ("alloc" "serde?/alloc")))) (rust-version "1.63")))

(define-public crate-oxilangtag-0.1 (crate (name "oxilangtag") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req ">=0.4, <0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.100") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1jwa1z5223hkfldjdwfrxb158w9y918667k9ldzzfsm82xvgiwr3") (features (quote (("serialize" "serde") ("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std") ("alloc" "serde?/alloc")))) (rust-version "1.63")))

(define-public crate-oxilib-0.1 (crate (name "oxilib") (vers "0.1.0") (deps (list (crate-dep (name "directories-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "1d2hix5khf2lmqf02db164y1581y1230l4pglb4p38b62f0h5a8c")))

(define-public crate-oxilib-0.1 (crate (name "oxilib") (vers "0.1.1") (deps (list (crate-dep (name "directories-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "01swkcw3mrykqs3szcas859kdr67sj2q2zpr7i82iz79l7i92miw")))

(define-public crate-oxilib-0.1 (crate (name "oxilib") (vers "0.1.2") (deps (list (crate-dep (name "directories-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "1c176l6n3l4mma6zcdsg1dmiaxyvni7p6kp0gkdbwfsd75ja10b7")))

