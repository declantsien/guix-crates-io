(define-module (crates-io ox al) #:use-module (crates-io))

(define-public crate-oxalis-0.1 (crate (name "oxalis") (vers "0.1.0") (hash "1h80j4kjb44yjgl6wgqlpg60946v2m9hk6mikqrygh4whrg43bvw")))

(define-public crate-oxalis-renderer-0.0.0 (crate (name "oxalis-renderer") (vers "0.0.0") (hash "1yp9lprxw1h6x8117hkyy1zd6scv9ccmp4r6r63qhgicbyz2shzl") (yanked #t)))

