(define-module (crates-io ox sd) #:use-module (crates-io))

(define-public crate-oxsdatatypes-0.1 (crate (name "oxsdatatypes") (vers "0.1.0") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (target "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)))) (hash "184ka5h78q2dll9bam9xj0z1rsvmc1vv76p5zq0wbhqwk40074f0") (rust-version "1.60")))

(define-public crate-oxsdatatypes-0.1 (crate (name "oxsdatatypes") (vers "0.1.1") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (target "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)))) (hash "1n8gns3jxza6p593pxa2wajx99rxpy4qdjazqd6bfvwzrnaxw28c") (rust-version "1.60")))

(define-public crate-oxsdatatypes-0.1 (crate (name "oxsdatatypes") (vers "0.1.2") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (target "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (kind 0)))) (hash "0q6f0i9kgb50pfcy9hanzqr9hhwkyqih06z9skid9rh394kd2b0v") (rust-version "1.60")))

(define-public crate-oxsdatatypes-0.1 (crate (name "oxsdatatypes") (vers "0.1.3") (deps (list (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (target "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (kind 0)))) (hash "1jam5ba308b0pdxq39a8c36c5rsxw6c8kqqjgr16dl5gnmq0gszd") (rust-version "1.60")))

(define-public crate-oxsdatatypes-0.2 (crate (name "oxsdatatypes") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (optional #t) (default-features #t) (target "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (kind 0)))) (hash "00al4gyh8z29rf0l8v7d5y3d79xy55mz0bdvl01v76mka0gkqvdm") (features (quote (("js" "js-sys") ("custom-now")))) (rust-version "1.70")))

(define-public crate-oxsdatatypes-0.2 (crate (name "oxsdatatypes") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (optional #t) (default-features #t) (target "cfg(all(target_family = \"wasm\", target_os = \"unknown\"))") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1f0fa0byygsshwqlwcmfji7578646ajrskc27rhvzx20x9145chm") (features (quote (("js" "js-sys") ("custom-now")))) (rust-version "1.70")))

