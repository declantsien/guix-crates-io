(define-module (crates-io ox ym) #:use-module (crates-io))

(define-public crate-oxymcts-0.1 (crate (name "oxymcts") (vers "0.1.0") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ego-tree") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "noisy_float") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1iswfxi8fbm3liqhmw8n1428dvnqx4988mbk56hxr2cwv6najrmj") (features (quote (("default" "ascii-tree") ("ascii-tree" "ascii_tree"))))))

