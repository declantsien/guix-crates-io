(define-module (crates-io ox _p) #:use-module (crates-io))

(define-public crate-ox_parser-0.2 (crate (name "ox_parser") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "08q4w82lm2njhqzpbz3gis6x668fqim3hmdkgb52ln0s5yfihncm") (yanked #t)))

(define-public crate-ox_parser-0.2 (crate (name "ox_parser") (vers "0.2.5") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "05qi721ki9r2fjhcawjwxw366y6brnfjvh3yrq5nq2573v94h988") (yanked #t)))

(define-public crate-ox_parser-0.2 (crate (name "ox_parser") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0iglz04dxknis41walvykv4qzzip1c2h5vk29900p3hv6r3apkni") (yanked #t)))

(define-public crate-ox_parser-0.2 (crate (name "ox_parser") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0x2n6l3ps60whpj4k2hcz2n3wjfkmmzv447d9bb3s2513g8p8ygm") (yanked #t)))

(define-public crate-ox_parser-0.2 (crate (name "ox_parser") (vers "0.2.6") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0g6c0xc9znp4db9bwmgrsqgsxzwhc1gn2q0y16lchp21da2jj5yy") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1h74h8bhf9jws9zffxini01am2ljabvcnzdr178dvdafwr80797b") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0vilsrznxgr2129slc2zplxbc8l051p891pg1kzmm0i1c2vh4jkq") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.2") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1gd2m8pqa29apvm0rssc136wkwjm8zc2wwkapwmxvr8cs5df6spa") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.3") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1zhl893b428syw6s5wirqxrqv4nf91zn917hndcb03bhcayjf1yw") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.4") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0yq3pkssasnw6rxzpsm13m5y7mimkyyfbrcj1yvgmgj9pndc018l") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.5") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "01z39wggpb4pj2hbfpk9cbcr9y74xsbvgj1dqff9x5wf5h60jb6p") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.6") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0vgka0qx7bh1jp41j4pq2s6d4x44iqh2fancn1m6qbzdlw1ywx13") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.7") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1i3m3pkvkd12sg8qcqg9mhqr7h9rjfawcnh09l39darxbl2hisjl") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.8") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1dxrbqszpi5s0sdqdfjzi0qqqmjmd9gdsxgmfbmjkwhmzwjygi3j") (yanked #t)))

(define-public crate-ox_parser-0.3 (crate (name "ox_parser") (vers "0.3.9") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0szakhh1jnf5d4kc4q8ypq0cc6kr1s2wpgwcziw4x9vm1j8f4kzf") (yanked #t)))

