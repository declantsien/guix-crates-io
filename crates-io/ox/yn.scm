(define-module (crates-io ox yn) #:use-module (crates-io))

(define-public crate-oxynth-0.1 (crate (name "oxynth") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "0.*") (default-features #t) (kind 0)))) (hash "0fjd2mxn5x1q2ci3x6cfdjmrqrkxwq89rdv83v3rb3srd4gvpjl5")))

(define-public crate-oxynth-0.1 (crate (name "oxynth") (vers "0.1.1") (deps (list (crate-dep (name "cpal") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "0.*") (default-features #t) (kind 0)))) (hash "0h5s95x2hjmirjz3ghyzzk50p75hy9xdvwsya03ywk63zrd535cb")))

