(define-module (crates-io ox ca) #:use-module (crates-io))

(define-public crate-oxcable-0.3 (crate (name "oxcable") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "portaudio") (req "*") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "08wk3fyjvyb5h1w2v51nxcsjwls4fy7ykwbq8g3nv6jh6a1pvp7g")))

(define-public crate-oxcable-0.3 (crate (name "oxcable") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "portaudio") (req "*") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0p222l051hpcnriq412fi6ap18baxlnl145yid0nh481lkimk4ba")))

(define-public crate-oxcable-0.3 (crate (name "oxcable") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "portaudio") (req "*") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "07vngmmqkklvmki063lly76wlybqf8mklk27c7dvnrzr4yfi6md9")))

(define-public crate-oxcable-0.4 (crate (name "oxcable") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "portaudio") (req "*") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1y5qz7zm1inzjki5y1hh1qpxz8d4acv2z1z9l9a23ck3rrli2wrd")))

(define-public crate-oxcable-0.4 (crate (name "oxcable") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (kind 0)) (crate-dep (name "portaudio") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1c1jxqrfc0plr8a4z0b2v06i97wvml2y1zp342lwr9gmjghsv4q0")))

(define-public crate-oxcable-0.4 (crate (name "oxcable") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "portaudio") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "0qrjs6kx3fzidpa55y1hqwk8qwbmfmxcfzdjsg460kwlgc1n2ln9")))

(define-public crate-oxcable-0.5 (crate (name "oxcable") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "portaudio") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "0v7lh2k0ankjqlx7iya973ra99awv7m2qkwd7c9gdfid46x3sk90")))

(define-public crate-oxcable-0.5 (crate (name "oxcable") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (features (quote ("complex"))) (kind 0)) (crate-dep (name "portaudio") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "portmidi") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "180p2l51iiipr83ghm0v0201vkkw481b5mwndkxgp26l6awy1vyb")))

(define-public crate-oxcable-basic-devices-0.5 (crate (name "oxcable-basic-devices") (vers "0.5.0") (deps (list (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "oxcable") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0lahxg7cvbbkri5gk0ggj0znvibnfsg6za73w1blwlfvnzplfs7w")))

(define-public crate-oxcable-basic-devices-0.5 (crate (name "oxcable-basic-devices") (vers "0.5.1") (deps (list (crate-dep (name "num") (req "^0.1.27") (kind 0)) (crate-dep (name "oxcable") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0jjwf68v5rk8ghw9c3d5ii6wqlli5475cjimrn2h0lxdqg6i8qhl")))

(define-public crate-oxcart-0.0.0 (crate (name "oxcart") (vers "0.0.0") (hash "19l8x87mn0nccljbqf7pp5gzzi3m1ybwcc54pqj05h1gcm1aypyk")))

