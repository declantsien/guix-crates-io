(define-module (crates-io ox er) #:use-module (crates-io))

(define-public crate-oxerun-0.0.0 (crate (name "oxerun") (vers "0.0.0-pre") (deps (list (crate-dep (name "rlibc") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "xen") (req "^0.0.0-pre") (default-features #t) (kind 0)) (crate-dep (name "xen-sys") (req "^0.0.0-pre") (default-features #t) (kind 0)))) (hash "1jp023wvyfdbrmczg4icidp3srymzmvc3ly3q9al62mzdij3zdn1")))

(define-public crate-oxerun-0.0.0 (crate (name "oxerun") (vers "0.0.0-pre1") (deps (list (crate-dep (name "xen") (req "^0.0.0-pre") (default-features #t) (kind 0)) (crate-dep (name "xen-sys") (req "^0.0.0-pre") (default-features #t) (kind 0)))) (hash "04lv7ax2rk2mw7wh2gqm229yzkr8clciamq0fz9gb13nffyms5px")))

