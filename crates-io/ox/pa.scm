(define-module (crates-io ox pa) #:use-module (crates-io))

(define-public crate-OxParse-0.1 (crate (name "OxParse") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "181szxl6lskzz3y8ngd0xp3rqd1hih7zdpky37lmlmdfpia38hvb") (yanked #t)))

(define-public crate-OxParse-0.1 (crate (name "OxParse") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0wq7gwf0zmzvp2ykj6bcn1g3klba8942z15wphkwljpdz749il6c") (yanked #t)))

(define-public crate-OxParse-0.2 (crate (name "OxParse") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1f43p1kghjrb0icrspzra8y99d714x9wscvh1br5lhkfri8a6m1r") (yanked #t)))

