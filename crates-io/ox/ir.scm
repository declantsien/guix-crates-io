(define-module (crates-io ox ir) #:use-module (crates-io))

(define-public crate-oxiri-0.1 (crate (name "oxiri") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bjkm3gf7lszbz466wplwihq7wwk183m334rhawil0r682q1qqws")))

(define-public crate-oxiri-0.1 (crate (name "oxiri") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wy4dhshkigjdn7lrj3a5qghkcr9l8drjgzrk8da1zw0fjrlzrf8")))

(define-public crate-oxiri-0.2 (crate (name "oxiri") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0lhhrycin6nrha05mk4pclyp10f5zgar3mhmq5vq3vrrbwxw1np5") (yanked #t)))

(define-public crate-oxiri-0.2 (crate (name "oxiri") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0l9z9gz97g1gf042qvlp25n52i2p7iqandk4inmqiqrjacd85a58")))

(define-public crate-oxiri-0.2 (crate (name "oxiri") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0p5xi5ny83gc50a32qpaalf8hmfmz316k61pgdxka48jk345w5xv")))

(define-public crate-oxiri-0.2 (crate (name "oxiri") (vers "0.2.3-alpha.1") (deps (list (crate-dep (name "codspeed-criterion-compat") (req "^2.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.166") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1qjp4lif0lq1k9nhzf4lww3sqa3mz2kizw4k1fb3m97s5k9xl9dj") (rust-version "1.60")))

(define-public crate-oxiri-0.2 (crate (name "oxiri") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4") (default-features #t) (kind 2)) (crate-dep (name "codspeed-criterion-compat") (req "^2.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.166") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1q532b7dgh9b3pvvg9j87a5j0214zikls2srkpfl1sz28vp1fm6h") (rust-version "1.60")))

