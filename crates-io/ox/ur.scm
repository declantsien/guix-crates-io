(define-module (crates-io ox ur) #:use-module (crates-io))

(define-public crate-oxur-component-0.1 (crate (name "oxur-component") (vers "0.1.0") (deps (list (crate-dep (name "petgraph") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0glv6jmzpn0j1qs0n3j55c4rij227pyp460qx76qqkbv9awmbrkg")))

(define-public crate-oxur-component-0.1 (crate (name "oxur-component") (vers "0.1.1") (deps (list (crate-dep (name "petgraph") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "13gl5k75q84j7givd9sp037550ddij4k9xgqx449zn5r12g4gc8z")))

(define-public crate-oxur-component-0.1 (crate (name "oxur-component") (vers "0.1.2") (deps (list (crate-dep (name "petgraph") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0wgjh6pn4jirbpli9kxna9l09kigy9mhr61x1q5q3dyb3bc49yd4")))

