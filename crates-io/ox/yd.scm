(define-module (crates-io ox yd) #:use-module (crates-io))

(define-public crate-oxyd-0.1 (crate (name "oxyd") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "bip0039") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "^0.10.1") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "libsecp256k1") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("getrandom"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "12x99aaidbfayb073668w710rgm55vap8cl20k5d82v5s7ai23r5")))

(define-public crate-oxydeyezer-0.1 (crate (name "oxydeyezer") (vers "0.1.0") (hash "1lppq2fqv13rmpygv1gh5xhw2wd6j58caib01ygg08dx8azcqdv8") (yanked #t)))

(define-public crate-oxydeyezer_lib-0.1 (crate (name "oxydeyezer_lib") (vers "0.1.0") (hash "0fih8ziv9ccl26snyzpjbjqvy0bvr3xs7x72gkxw9psrykdspq4s") (yanked #t)))

(define-public crate-oxydian-0.1 (crate (name "oxydian") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "07ixp8561xhz5k4rv1gfigdambc5npl6xwvn9vjmgnk4fda9lv48")))

(define-public crate-oxydian-0.1 (crate (name "oxydian") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)))) (hash "05qib50mhr0ix1wp0c4jcc9k2rdkwzrapx81b90zfpdzir31ynsf")))

(define-public crate-oxydian-0.1 (crate (name "oxydian") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "183hkkr1fpp3l1l260ahi8p7dddnyrfqwiyqnlc9v4wvclzva1kv")))

(define-public crate-oxydized-money-0.1 (crate (name "oxydized-money") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "iso_currency") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34.3") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.34.2") (default-features #t) (kind 2)))) (hash "13pkfdwam3b318qwj6hn5sb2gd9k3qimhcv3s722y2p4a5vnl0jj")))

(define-public crate-oxydized-money-0.2 (crate (name "oxydized-money") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "iso_currency") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34.3") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.34.2") (default-features #t) (kind 2)))) (hash "10kwg1iif9iwlzfdyzn812v3sxakp5k6ii5m8m3hq7igrjc0ndys")))

(define-public crate-oxydized-money-0.3 (crate (name "oxydized-money") (vers "0.3.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "iso_currency") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34.3") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.34.2") (default-features #t) (kind 2)))) (hash "1rjr1l71m373f0w8dmps6njxkhhjs4f4z7a9gh7lh310vivf4shn")))

(define-public crate-oxydized-money-macros-0.1 (crate (name "oxydized-money-macros") (vers "0.1.0") (deps (list (crate-dep (name "iso_currency") (req "^0.4.4") (features (quote ("iterator"))) (default-features #t) (kind 1)) (crate-dep (name "rust_decimal_macros") (req "^1.34.2") (features (quote ("reexportable"))) (default-features #t) (kind 0)))) (hash "1fcfgxiywa364dh1ypq31lh0bj5jslkn0a4i2qnywj7wjzs324lc")))

(define-public crate-oxydized-money-macros-0.2 (crate (name "oxydized-money-macros") (vers "0.2.0") (deps (list (crate-dep (name "iso_currency") (req "^0.4.4") (features (quote ("iterator"))) (default-features #t) (kind 1)) (crate-dep (name "rust_decimal_macros") (req "^1.34.2") (features (quote ("reexportable"))) (default-features #t) (kind 0)))) (hash "04y57iy57k58kzw4zsy3n0j8qlf6bp7gpinmf62p431ylsb4axwa")))

(define-public crate-oxydyze-0.1 (crate (name "oxydyze") (vers "0.1.1") (deps (list (crate-dep (name "sdl2") (req "^0.33") (features (quote ("unsafe_textures" "ttf"))) (default-features #t) (kind 0)))) (hash "0372vaghq1sk3spqhb2p54kr926yyqqkvh3cyw3gd2iqakwqimvc") (yanked #t)))

