(define-module (crates-io ox zy) #:use-module (crates-io))

(define-public crate-oxzy-0.1 (crate (name "oxzy") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "extism") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "02an2zgdxkf3hbgkwhkbj46xj7havf9q4q9zqycgv793jwqi9426")))

(define-public crate-oxzy-0.3 (crate (name "oxzy") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "extism") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "oxzy-lib") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "06pdh742fbgl1y73lsyhrg2mdax14yha0f23r8p5s01an6zc2sgb")))

(define-public crate-oxzy-lib-0.1 (crate (name "oxzy-lib") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "152djgaag3iwjwmypvjdf43v00rc21z26p9j4hh0fr1l9a394czz")))

(define-public crate-oxzy-lib-0.2 (crate (name "oxzy-lib") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ki0nk812qdy1ranynr1g8b85cpwn6dh2vbjxbrhcr5dx76h0mr5")))

(define-public crate-oxzy-lib-1 (crate (name "oxzy-lib") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "01cpgwh2nxa4b214v9m1mps5d7ja3s5mqy29npgaiklrdyacrlrw")))

(define-public crate-oxzy-lib-1 (crate (name "oxzy-lib") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13ilwav8kjvngb51lyyycpdgvjw4zi60n8a3kr92g7gqny2jrcs9")))

(define-public crate-oxzy-lib-2 (crate (name "oxzy-lib") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04vag661zc49a0afw9cn8xvhy50i5y848rassjzydwd1pvcfkcpr")))

(define-public crate-oxzy-lib-3 (crate (name "oxzy-lib") (vers "3.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.103, <1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dwxjy95cv8cpk3p7rlsa8vwnswn2kvrgp91fpsr23dlkibqxn07")))

(define-public crate-oxzy-lib-3 (crate (name "oxzy-lib") (vers "3.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.103, <1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17x11mzv6nfg583rx6ghy9ndgpgy976h62ssf0x247zybzk8qkrd")))

