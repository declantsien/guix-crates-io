(define-module (crates-io ox fo) #:use-module (crates-io))

(define-public crate-oxford_join-0.1 (crate (name "oxford_join") (vers "0.1.0") (deps (list (crate-dep (name "brunch") (req "0.1.*") (default-features #t) (kind 2)))) (hash "0hifb651vljvkp7y5bhqrj2i3fcqsw81ng1z952ihxmvmma88s5y")))

(define-public crate-oxford_join-0.1 (crate (name "oxford_join") (vers "0.1.1") (deps (list (crate-dep (name "brunch") (req "0.1.*") (default-features #t) (kind 2)))) (hash "1y7ab6mj53s9qv7sfbv9akxim736clvzd8g3w8zwb97rkcsax26l")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.0") (deps (list (crate-dep (name "brunch") (req "0.2.*") (default-features #t) (kind 2)))) (hash "0dnamrm2yv8c71kvmbcp87n0bb80c2qza41l908cnhij59ir4dxb") (rust-version "1.56")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.1") (deps (list (crate-dep (name "brunch") (req "0.2.*") (default-features #t) (kind 2)))) (hash "14bq9y0h6i2njvzbvdgancdjab2x3far7i29g085s1qb4qrz9zcg") (rust-version "1.56")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.2") (deps (list (crate-dep (name "brunch") (req "0.2.*") (default-features #t) (kind 2)))) (hash "07rzzgbjiv99xk1hd4qm8wkxxgh8x824r2s0yx8xi98s7m6dcsad") (rust-version "1.56")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.3") (deps (list (crate-dep (name "brunch") (req "0.2.*") (default-features #t) (kind 2)))) (hash "0lr94c5djnb09rl8lnwlj2cir5cv7pl5fr8jkrkyhj8ccxp8wfiv") (rust-version "1.56")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.4") (deps (list (crate-dep (name "brunch") (req "0.2.*") (default-features #t) (kind 2)))) (hash "1l5bsaww1174fg16hi55yarmpg2g186ynp0h7xnsshhsldkj3m43") (rust-version "1.62")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.5") (deps (list (crate-dep (name "brunch") (req "0.3.*") (default-features #t) (kind 2)))) (hash "1dxvhn6mdddzjyr0dbpqdl1dyskg5hdjqchpbky12axsd9bima2q") (rust-version "1.62")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.6") (deps (list (crate-dep (name "brunch") (req "0.3.*") (default-features #t) (kind 2)))) (hash "07q37fcvq46xdbqk7g1qnach41m8r16xybhh8a482l1mhcfxi1w1") (rust-version "1.62")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.7") (deps (list (crate-dep (name "brunch") (req "0.4.*") (default-features #t) (kind 2)))) (hash "1p4xkzz1pj7w4d16q6izvg9lyp8jsgkpjhcwgsnbyk94rdi4zmcn") (rust-version "1.62")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.8") (deps (list (crate-dep (name "brunch") (req "0.5.*") (default-features #t) (kind 2)))) (hash "1dgwyr3c56lvkx5451wgc3ixpg9chjwqnk0sk84v12ifl29k0p3l") (rust-version "1.62")))

(define-public crate-oxford_join-0.2 (crate (name "oxford_join") (vers "0.2.9") (deps (list (crate-dep (name "brunch") (req "0.5.*") (default-features #t) (kind 2)))) (hash "1c3kfz5hn4nn33swpsdll3piszgsn0mfkyry99d7spd3iymmlxcn") (rust-version "1.62")))

