(define-module (crates-io ox yt) #:use-module (crates-io))

(define-public crate-oxytail-base-0.1 (crate (name "oxytail-base") (vers "0.1.0") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0mdgkvy8mbsvcjn5yw0v6wm2x4mh0paj58g7fivgigprm911bpy2") (rust-version "1.75")))

(define-public crate-oxytail-base-0.1 (crate (name "oxytail-base") (vers "0.1.1") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10q0hxm2v2asr15hi044gq26z28lk7zhf50d4fjfikb8k9l0w9v0") (rust-version "1.75")))

(define-public crate-oxytail-base-0.1 (crate (name "oxytail-base") (vers "0.1.2") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13da0c1a3v3s7jp8jnj9hfz3js9syr15wnd85whqg29fqcwsw6dr") (rust-version "1.75")))

(define-public crate-oxytail-theme-dark-0.1 (crate (name "oxytail-theme-dark") (vers "0.1.0") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oxytail-base") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "oxytail-theme-defaults") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17avpy3b7apa5fh8kg5mv554qzclnrhl4dhmwgad4hfirxjfn8qa") (rust-version "1.75")))

(define-public crate-oxytail-theme-dark-0.1 (crate (name "oxytail-theme-dark") (vers "0.1.1") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oxytail-base") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oxytail-theme-defaults") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "099q550h0w52d1iaa6f5yppsidl4rn9xa6dx6dgf7r4kl98l3bw3") (rust-version "1.75")))

(define-public crate-oxytail-theme-dark-0.1 (crate (name "oxytail-theme-dark") (vers "0.1.2") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oxytail-base") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "oxytail-theme-defaults") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "168wnf02pd08791sarrmmmmqmyyy5y6ls93k6a4z18g23zpb3c76") (rust-version "1.75")))

(define-public crate-oxytail-theme-defaults-0.1 (crate (name "oxytail-theme-defaults") (vers "0.1.0") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oxytail-base") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1b83zkdlik9gsx1wbakd1kinlj5gvbm61shmsl14hf5f5lj7ri9q") (rust-version "1.75")))

(define-public crate-oxytail-theme-defaults-0.1 (crate (name "oxytail-theme-defaults") (vers "0.1.1") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oxytail-base") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0r7jspghvqf1br9xr1pwrfs1mw1jicpv866rvhwyvpl67rzbh8q5") (rust-version "1.75")))

(define-public crate-oxytail-theme-defaults-0.1 (crate (name "oxytail-theme-defaults") (vers "0.1.2") (deps (list (crate-dep (name "floem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oxytail-base") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1m1kcpl30dd74a6kn056wmzc3r6y3yfv7q9vzcddrr8qig5aq66h") (rust-version "1.75")))

