(define-module (crates-io ox en) #:use-module (crates-io))

(define-public crate-oxen-0.0.1 (crate (name "oxen") (vers "0.0.1") (deps (list (crate-dep (name "glium") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "10532vkl0rilsxd9g0ls31hz9gg6jzmxx1fx3qpsspvhjk4ly98b")))

(define-public crate-oxen-0.0.2 (crate (name "oxen") (vers "0.0.2") (deps (list (crate-dep (name "glium") (req "*") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0rm9r7nr46xxpnwbpc45rj7y8pjq4hbm2z2aj2ml5m2qfi2pmpx5")))

