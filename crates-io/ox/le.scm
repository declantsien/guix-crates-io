(define-module (crates-io ox le) #:use-module (crates-io))

(define-public crate-oxlex-0.1 (crate (name "oxlex") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "oxlex-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "18wkhzzznsw255pmrh13xn8yck4mdlp0xg4xki9j53bmcdmjh8zn") (features (quote (("derive" "oxlex-derive") ("default" "derive"))))))

(define-public crate-oxlex-derive-0.1 (crate (name "oxlex-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (default-features #t) (kind 0)))) (hash "02l5q4ihx1dy5ay98wxhfmp623hai1f4nf7ah92zx19z54pp3zva") (features (quote (("default" "syn/full"))))))

