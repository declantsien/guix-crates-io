(define-module (crates-io ox y-) #:use-module (crates-io))

(define-public crate-oxy-linux-pwm-0.1 (crate (name "oxy-linux-pwm") (vers "0.1.0") (hash "03wksmifg76dfjvrkzlckrcp7ianjcga8w7jf6gaxkiwr3x53h3g")))

(define-public crate-oxy-linux-pwm-0.1 (crate (name "oxy-linux-pwm") (vers "0.1.1") (hash "0x30fjc1jvys8p4hw5666amn9dmppvwzmg6h9lq82a019sfb8ks6")))

