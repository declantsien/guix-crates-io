(define-module (crates-io ox t-) #:use-module (crates-io))

(define-public crate-oxt-confidence-0.1 (crate (name "oxt-confidence") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nc6vs1fr8h30cq4bk1d04ycy5hm6z0bxacr6s4k04gldv6shs8w") (yanked #t)))

(define-public crate-oxt-confidence-0.2 (crate (name "oxt-confidence") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0absjalhwfhpbm5wbbjidfi3p3pid9hz60df6xjja5jzf36339pc") (yanked #t)))

(define-public crate-oxt-confidence-0.2 (crate (name "oxt-confidence") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1g2pjrq09hxnxj61xzxjgmbzzzjhjv9w8v9jq9m2cwc7j2ikhmc7")))

(define-public crate-oxt-confidence-0.2 (crate (name "oxt-confidence") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.199") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ndd0cqh70ji8y0wivghcia1bqwywv59463j4pbkfzldnk6x2rqv")))

(define-public crate-oxt-confidence-0.2 (crate (name "oxt-confidence") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.199") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1iww9wpr5vpsd90rjq7bafgy1pf1d0lvs0iiizwgzz56hzczf6f3")))

(define-public crate-oxt-confidence-0.2 (crate (name "oxt-confidence") (vers "0.2.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.199") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1g937094981zsg81mlyb00xl1g8n3xz3ip49frmr70yjyrysrvln")))

(define-public crate-oxt-confidence-0.2 (crate (name "oxt-confidence") (vers "0.2.5") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.199") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0km56l5r6jk9lzmj13kcjlvqhqcr8halcwgs2ws1xv5zjfs380zi")))

(define-public crate-oxt-confidence-cli-0.1 (crate (name "oxt-confidence-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "oxt-confidence") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "13fp7am3ds7ij3ywimp1pdnvsngl14dwn74imv2aqy9015lwss5z")))

