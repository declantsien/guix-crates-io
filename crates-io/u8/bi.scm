(define-module (crates-io u8 bi) #:use-module (crates-io))

(define-public crate-u8bits-0.1 (crate (name "u8bits") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "surjective-enum") (req "^0.1") (default-features #t) (kind 2)))) (hash "0nrairdkyii8pb0sdfaq3qcmzdq1d5p43lhz7cwsb8n8y2p7q3yq")))

