(define-module (crates-io pl t-) #:use-module (crates-io))

(define-public crate-plt-cairo-0.1 (crate (name "plt-cairo") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cairo-rs") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "draw") (req "^0.4.0") (default-features #t) (kind 0) (package "plt-draw")) (crate-dep (name "png") (req "^0.17") (optional #t) (default-features #t) (kind 0)))) (hash "0r04hk699k88max3hm8sxh68dqp9mf167mpqmiz109xa392158ja") (features (quote (("svg" "cairo-rs/svg") ("default" "png" "svg")))) (v 2) (features2 (quote (("png" "dep:png" "cairo-rs/png"))))))

(define-public crate-plt-draw-0.1 (crate (name "plt-draw") (vers "0.1.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.15") (features (quote ("png" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 0)))) (hash "0g3l17lz5y6mwvrq6hl7kr6bk09majiq2j6m8yq3galvy89brxjd")))

(define-public crate-plt-draw-0.2 (crate (name "plt-draw") (vers "0.2.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.15") (features (quote ("png" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 0)))) (hash "00s9bhl19l3qf9cvrw2xg9334gdn16lkc4clw03jglcy59srn4x9")))

(define-public crate-plt-draw-0.3 (crate (name "plt-draw") (vers "0.3.0") (deps (list (crate-dep (name "cairo-rs") (req "^0.15") (features (quote ("png" "svg"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 0)))) (hash "1avxk3vkz9snkv8c9pr3la4pvw6nfq72dqh10xvbzjimbpy0hcid")))

(define-public crate-plt-draw-0.4 (crate (name "plt-draw") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s3by6sx9v0q9jqnqlds9748xqx7ky0sf1ffmr59yv0y325bpqvd")))

(define-public crate-plt-rs-0.1 (crate (name "plt-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)) (crate-dep (name "proc-maps") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0p8c64b4152r8pja0nd1g6fs7qm872k2h65dcid31wrch9d73b1j")))

(define-public crate-plt-rs-0.2 (crate (name "plt-rs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "1x0ibbvjr2xcs1vrxsqb8xc2jrym8amslax6lnkqiyc0kb0wavzg")))

