(define-module (crates-io pl ma) #:use-module (crates-io))

(define-public crate-plmap-0.0.1 (crate (name "plmap") (vers "0.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req ">0.3") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g5jf0kf96bqmajrgkia898yc040gs01dw0sz8gdvgvgb1n405x1")))

(define-public crate-plmap-0.2 (crate (name "plmap") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req ">0.3") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x9pi107rkparkhw5xmkjmydc8cv6q1nymb4bxb1cg3yvpnmgcd3")))

(define-public crate-plmap-0.2 (crate (name "plmap") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req ">0.3") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r15gj64hgb6lkz1isvd8279m3mh89ikzf9x16jq0fyd6b1w75gz")))

(define-public crate-plmap-0.2 (crate (name "plmap") (vers "0.2.2") (deps (list (crate-dep (name "crossbeam-channel") (req ">0.3") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "02mz1xx13swjhlg34nr5w8wxscln36s6ni876vjxwbp1735fqaa0")))

(define-public crate-plmap-0.2 (crate (name "plmap") (vers "0.2.3") (deps (list (crate-dep (name "crossbeam-channel") (req ">0.3") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "0753gdx97gppndykwy4rjzca76c685lkpgyidhvadd8c05him4xr")))

(define-public crate-plmap-0.3 (crate (name "plmap") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req ">0.3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req ">0.3") (default-features #t) (kind 0)))) (hash "0k0yx20hjaxqhygv1d69jvi062svbgmmyl2d2afxmsa3f4bshndh")))

(define-public crate-plmatrix-0.1 (crate (name "plmatrix") (vers "0.1.0") (hash "19l4zfdflakg70ga3zb483996ns73a8kqyhh6kq6msys0alf5whk") (yanked #t)))

