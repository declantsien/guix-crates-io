(define-module (crates-io pl ic) #:use-module (crates-io))

(define-public crate-plic-0.0.0 (crate (name "plic") (vers "0.0.0") (hash "0f93ksd0ca233i8sv4hzmwndwv708sakzsbbwnw7933wn98lfwlq") (features (quote (("primitive-id") ("default")))) (yanked #t)))

(define-public crate-plic-0.0.1 (crate (name "plic") (vers "0.0.1") (hash "1mx7qmln2pbavmsxsp4pnbr3b20i113x2d2pagcpxlfvhgwgrn0x") (features (quote (("primitive-id") ("default")))) (yanked #t)))

(define-public crate-plic-0.0.2 (crate (name "plic") (vers "0.0.2") (hash "0ylvgj36wiw9z37ynjyf4azagpcpd9yxzdv1l480wyyn66zhdmia") (features (quote (("primitive-id") ("default"))))))

