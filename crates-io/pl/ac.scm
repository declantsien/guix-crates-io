(define-module (crates-io pl ac) #:use-module (crates-io))

(define-public crate-place-0.0.0 (crate (name "place") (vers "0.0.0") (hash "08smrcs1na4nb1x2ivmd8nxcny9wrrc26q6m0fpffcmzah41m93f")))

(define-public crate-place-0.1 (crate (name "place") (vers "0.1.0") (hash "16wnqla46mamya09gsvgdrf16c6cjraxx5sakk2a09dcdcnrksds")))

(define-public crate-place_capitals-0.1 (crate (name "place_capitals") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "0qw6y0wkvzgjqdm06xrqsb05kfc7qf9s4gr4f7mhryf4aczhsg0v")))

(define-public crate-place_capitals-0.1 (crate (name "place_capitals") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "1phirgvk3sipc6c72piqww2rd1rh496r1kda6pbh6jglbhm808hn")))

(define-public crate-place_macro-0.1 (crate (name "place_macro") (vers "0.1.0") (deps (list (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "13scls1m4wxd4gcp837f110i7cc74s4iwhg54llfxia4vpv282q7")))

(define-public crate-place_macro-0.1 (crate (name "place_macro") (vers "0.1.1") (deps (list (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "17m72im4hwkzxk34cv3z93m44vf54v6rxhm0gqp52xfmn3521cvv") (yanked #t)))

(define-public crate-place_macro-0.1 (crate (name "place_macro") (vers "0.1.2") (deps (list (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1576bb7va2mi8nxbjgs9fw59rgw9bj57h78hppv4rf22gxc5zxaw")))

(define-public crate-place_macro-0.1 (crate (name "place_macro") (vers "0.1.3") (deps (list (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "059h9x1ziann60v52l0hqf9iq93liiwnhz5x7fdrfkwbhwbfsz8c")))

(define-public crate-place_macro-0.2 (crate (name "place_macro") (vers "0.2.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0z6z7kgayvjqvshpckv8pzs941czmzwppc0y8yg9q74g5xmng5h0")))

(define-public crate-place_macro-1 (crate (name "place_macro") (vers "1.0.0") (deps (list (crate-dep (name "place_macro_proc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ahp3y37q820x5bb81hfkmsvaw7lyakbyzfk2y9y7dgb5rh11nfy")))

(define-public crate-place_macro_core-1 (crate (name "place_macro_core") (vers "1.0.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "litrs") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.71") (default-features #t) (kind 0)))) (hash "17cfj5w6lf1s8j4mb6crqr7ip5ik049a7szvdxs7w89812rax90m")))

(define-public crate-place_macro_proc-1 (crate (name "place_macro_proc") (vers "1.0.0") (deps (list (crate-dep (name "place_macro_core") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.71") (default-features #t) (kind 0)))) (hash "1asmhywvra6m42ml4pjzhh55ljyyya70phnk34g6m3sc0iihv3r7")))

(define-public crate-placeholder-0.1 (crate (name "placeholder") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0zxm5vha1qji00kf006f3jl51fmf59v5dcx47mib1hj0d2cjx8ms")))

(define-public crate-placeholder-0.1 (crate (name "placeholder") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0hi492kir9q14kp6502lhk48414f5v8z049bpzgclw7f0j228al7")))

(define-public crate-placeholder-1 (crate (name "placeholder") (vers "1.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "05kccryqhxjyrqqi18gkvar4nn33kmchwa4mij4mhy0ms5s8cj4a")))

(define-public crate-placeholder-1 (crate (name "placeholder") (vers "1.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "087b5nmdcxf82vrsyrvfapz1a0k0qq4v9fi5yyrj1v6icvm93dpi")))

(define-public crate-placeholder-brchd-0.0.0 (crate (name "placeholder-brchd") (vers "0.0.0") (hash "1fy6827ypi99qg4db8wwadd5njklr500sd180vq65xpkdklgn9hs") (yanked #t)))

(define-public crate-placeholder_closure-0.1 (crate (name "placeholder_closure") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0x74ys481va4pn7n1d7w51mh2x35niypifwmk8iy27blj1b159k3")))

(define-public crate-placement-new-0.1 (crate (name "placement-new") (vers "0.1.0") (deps (list (crate-dep (name "placement-new-derive") (req "=0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "13n6idq3v1bbxxi86x1prg5g9wd8999xj9kxjh7ihmkbb68g5z1v") (features (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (yanked #t)))

(define-public crate-placement-new-0.2 (crate (name "placement-new") (vers "0.2.0") (deps (list (crate-dep (name "placement-new-derive") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0q0s1yn6w2k4bhfj0iqzqp5jxg4i2v26j0006g3bwid6hffy2yzs") (features (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (yanked #t)))

(define-public crate-placement-new-0.2 (crate (name "placement-new") (vers "0.2.1") (deps (list (crate-dep (name "placement-new-derive") (req "=0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "19vg753cqv2kzw4x3k3i9lq12395r162rzpvr5gs0x7pqhwp0gzk") (features (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (yanked #t)))

(define-public crate-placement-new-0.3 (crate (name "placement-new") (vers "0.3.0") (deps (list (crate-dep (name "placement-new-derive") (req "=0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1r9pqimzy0476bs9xjaa8b6nvam13vn3336c9kn6884s3l85bdg1") (features (quote (("derive" "placement-new-derive") ("default" "alloc" "derive") ("alloc")))) (yanked #t)))

(define-public crate-placement-new-derive-0.1 (crate (name "placement-new-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p35lsr2j0lgvldrvww65ijp1mq1zq0rhvkwa2kgsymdibcnqyd5") (yanked #t)))

(define-public crate-placement-new-derive-0.2 (crate (name "placement-new-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15b3nkhs8chsw15kbh62a3kkr3d6dnbb4xmnzyfdb0sxxddbnfk4") (yanked #t)))

(define-public crate-placement-new-derive-0.2 (crate (name "placement-new-derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1drinxksw1xj7y70q8g1c3cbjm4hfd2mj7s6vbrmmgn14wfzqrjg") (yanked #t)))

(define-public crate-placement-new-derive-0.3 (crate (name "placement-new-derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kvy77yl4cm390caz6gw5xvnmhbkqr6zl55gx8ld7i34mn947ifn") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.0") (hash "0psr109hn9llhrq1bc15jmc6z7ggnnwb7hj1jhv72bkkwwgg79zs") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.1") (hash "03iinfdb8cb71scj54dj8ms55913kf69hxiqvirq08khcmffmqmw") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.2") (hash "19vks1abmp3swjnplz3b62qjq39n9q824pn9h2gwrkvyl1xhp4vk") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.3") (hash "1bclwnc6x7zamk9ykj3w16kglxic1iyywvnzpv3shfb2hyx8lpa6") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.4") (hash "05s43w32plchvdvhhql0yp4agxlw4zzgljqmpvfkvafcm43gzkn5") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.5") (hash "03s41d2ha5klw9lp5d631aklnzgk9zj2byqqg687ssj19934ganx") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.6") (hash "11rnwrwicakvlhpjs2yqd0igilhjsg8nyms87p7682yx8145nrq3") (yanked #t)))

(define-public crate-placements-tree-0.1 (crate (name "placements-tree") (vers "0.1.7") (hash "0bdifkfgmpma89y20z59zglfj9li4dr94j0g1h99dmanpk31znxa")))

(define-public crate-placer-0.1 (crate (name "placer") (vers "0.1.0") (hash "0w8b02m51rnvpchrn47prijsazz1hdl94ys6s56sjcww0wq356ay")))

(define-public crate-places-0.0.0 (crate (name "places") (vers "0.0.0") (hash "09xinq5qzd7rz0507xyj5f6fs9lapg07qg5pxrlxrn4yvj8dznpq")))

(define-public crate-places_119-0.0.1 (crate (name "places_119") (vers "0.0.1") (hash "08x2m1gp2j67swkpkrhjn5f9ks8937wf1d67hmab213d4qdc447b") (yanked #t)))

(define-public crate-places_119-0.0.2 (crate (name "places_119") (vers "0.0.2") (hash "1im00742iy6rqmvhwmba9ipzydmq44sli7x7aay6kpqwnm6fsbwz") (yanked #t)))

