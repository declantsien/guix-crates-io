(define-module (crates-io pl or) #:use-module (crates-io))

(define-public crate-plort-0.1 (crate (name "plort") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "062bpxm3k351827w3675y5a3mqr6j4v59za5417p1sfcd56jf368")))

