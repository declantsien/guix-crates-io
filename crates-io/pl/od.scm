(define-module (crates-io pl od) #:use-module (crates-io))

(define-public crate-plod-0.2 (crate (name "plod") (vers "0.2.1") (deps (list (crate-dep (name "plod_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kcb4yirqwx9fsbmf9vihd9wnvglcnaag4fb1bzfzp0caak7ww1k")))

(define-public crate-plod-0.2 (crate (name "plod") (vers "0.2.2") (deps (list (crate-dep (name "plod_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xqfzdk5j0y4dakj3f2drg6cvysrx38hrdjw7lb5pdgqazc3v2n3")))

(define-public crate-plod-0.3 (crate (name "plod") (vers "0.3.0") (deps (list (crate-dep (name "plod_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jpxxcd9pnq29r7yp6afpp7bp3l2vdqn30d3c658pmskixc4lrr6")))

(define-public crate-plod-0.4 (crate (name "plod") (vers "0.4.0") (deps (list (crate-dep (name "plod_derive") (req "^0.4") (default-features #t) (kind 0)))) (hash "1f3224saxcrks66x3838ayxlwcpmiba8afhx9xqrp0g7r8sapz9a")))

(define-public crate-plod_derive-0.2 (crate (name "plod_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13flj9a7zawdrmx4diipysnhipkdx5d4s8izhdmh97kday0426vp")))

(define-public crate-plod_derive-0.2 (crate (name "plod_derive") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mwdpyk0frvbcngi1y8gchsjjibwiqv9vhn1ksm2d7v2zwf3j4r7")))

(define-public crate-plod_derive-0.3 (crate (name "plod_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sskchf050z94k5p3wri6pjxl45idlz4hj9hzd0wx11njsy3yjk9")))

(define-public crate-plod_derive-0.4 (crate (name "plod_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hfsrwh2wlwrrh16n3r1gcwnhgna983sm2ynv7myk6hiypmq7bg4")))

