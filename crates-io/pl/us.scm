(define-module (crates-io pl us) #:use-module (crates-io))

(define-public crate-plus-0.0.0 (crate (name "plus") (vers "0.0.0") (hash "1djgr7q829yayh5zmayrvm3saajxvyicvmg62fiq6kilrijk9vfd")))

(define-public crate-plus_minus-0.1 (crate (name "plus_minus") (vers "0.1.0") (hash "0ncz2v2d6dgvphsz5agzs1wj9jvxhb5iy93503n8w10xv34dmlv8")))

(define-public crate-plus_minus-0.1 (crate (name "plus_minus") (vers "0.1.1") (hash "060f9zbgn3cnfzbw5rjn71022c36m1jl55mxjm04yp79zivmiw3p")))

(define-public crate-plusapi-0.1 (crate (name "plusapi") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kh6alzbl6769213vbp77q592fmk2lcq15fi9cxcv6mhz7ph5rf3") (features (quote (("blocking" "reqwest/blocking"))))))

(define-public crate-pluscodes-0.1 (crate (name "pluscodes") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "099dpakb9w6aqfh9d30g8yfpp8mpxi5kwf2pgbc02mnf1c40slw1")))

(define-public crate-pluscodes-0.2 (crate (name "pluscodes") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "15hy2hmc9fv1a8gd9768n0k3zjk637hixq2vp6m1pziiw9yw8hn1")))

(define-public crate-pluscodes-0.3 (crate (name "pluscodes") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1f49q0wncngc6r5xiqw98pzgni18ccvzflrn3h5rkd3qhayphdsc")))

(define-public crate-pluscodes-0.4 (crate (name "pluscodes") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1k4dbb5x4i2w9mzd8z39vf2ln25v6z1npxidsb3c5z94k53mmvbh")))

(define-public crate-pluscodes-0.5 (crate (name "pluscodes") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1yif11xk9qpp243gi2r6jmry99p6c1ssgjvif1526kv00a72ilz5")))

(define-public crate-pluser-3 (crate (name "pluser") (vers "3.0.0") (deps (list (crate-dep (name "accumulate") (req "^3.0.0") (features (quote ("ink-as-dependency"))) (kind 0)) (crate-dep (name "ink_env") (req "^3.0.0") (kind 0)) (crate-dep (name "ink_lang") (req "^3.0.0") (kind 0)) (crate-dep (name "ink_metadata") (req "^3.0.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "ink_primitives") (req "^3.0.0") (kind 0)) (crate-dep (name "ink_storage") (req "^3.0.0") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0dcxgs0hr6abgzmq3kbsv4fn2m2lb9i441vdgq0v9prwj3bcglqb") (features (quote (("std" "ink_primitives/std" "ink_metadata/std" "ink_env/std" "ink_storage/std" "ink_lang/std" "scale/std" "scale-info/std" "accumulate/std") ("ink-as-dependency") ("default" "std"))))))

(define-public crate-plushy-0.0.0 (crate (name "plushy") (vers "0.0.0") (hash "0dd7352fxwiiib8w4l0z8myik2gsbg1ql1immv708qwkrl7ma6p4")))

(define-public crate-plushy-0.1 (crate (name "plushy") (vers "0.1.0") (deps (list (crate-dep (name "thunderdome") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0vdnmzr0vp4dkacq67vgksan04q5rcj9dys416s209qpi7w16rbk")))

(define-public crate-plushy-0.1 (crate (name "plushy") (vers "0.1.1") (deps (list (crate-dep (name "thunderdome") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0jyai2x1z382b4ir4msclrd3axg1f45dsv0w5qhk7xjxsqx20lpx")))

(define-public crate-plushy-0.1 (crate (name "plushy") (vers "0.1.2") (deps (list (crate-dep (name "thunderdome") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0af4gd81ymw54lninr2v87x38ccfjw4aigds4hmy0bp76cyrcr24")))

(define-public crate-plushy-0.1 (crate (name "plushy") (vers "0.1.3") (deps (list (crate-dep (name "thunderdome") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1sm86ipqz8ka50c33m8if20vhlzj2k033rldlf1dhblibynwrfpp")))

(define-public crate-pluster-0.0.0 (crate (name "pluster") (vers "0.0.0") (hash "16n9p8mdy2arqig1g4m7rjxw0bljzkz21yjj6w1pigf2mx94c29d")))

