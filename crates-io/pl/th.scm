(define-module (crates-io pl th) #:use-module (crates-io))

(define-public crate-plthook-0.1 (crate (name "plthook") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1h8ny2gx46rw4nwprrja3rjf3x52r8m8c4gn3ki96jqr0492c2b1")))

(define-public crate-plthook-0.2 (crate (name "plthook") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "072irvq160mjjjbpfzc5gihq6zbp0r5giijp417al2h1ahg62l0b")))

(define-public crate-plthook-0.2 (crate (name "plthook") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "15jg9m5g1apcwza06kxn04qrwwsvhs33f0jcgbpdjvsmn8bmcf9j")))

(define-public crate-plthook-0.2 (crate (name "plthook") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "10rvsrb28zx2cr82028541y36sfpib70l9vxyzgcyrwhgwdjcg2n")))

(define-public crate-plthook-rs-0.1 (crate (name "plthook-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1gv3q7j4w0g63bl1cvwib4zdici20zmpvwgx496k1zmlmb29lfyd") (yanked #t)))

