(define-module (crates-io pl cn) #:use-module (crates-io))

(define-public crate-plcnext-0.1 (crate (name "plcnext") (vers "0.1.0") (hash "0c3h5p2cmr1sm51z4119c9xwda5bk12163fc1scbkzi2l216bhzj")))

(define-public crate-plcnext-0.2 (crate (name "plcnext") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "plcnext-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "06n2gkcskyfk3kj3xamlbiv30d3gcmc8y0xybxv4zxv8m6rhpgm2")))

(define-public crate-plcnext-axioline-0.1 (crate (name "plcnext-axioline") (vers "0.1.0") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "115d1cs55kisk3mkabfry40ayh6hif2b4x40rmxg7q40shinvm9m")))

(define-public crate-plcnext-commons-0.1 (crate (name "plcnext-commons") (vers "0.1.0") (deps (list (crate-dep (name "cpp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.4") (default-features #t) (kind 1)))) (hash "1mflnrzf6khag9x63yazgf50x3pp4dgky1amkfwgvp45wqkvc50m")))

(define-public crate-plcnext-device-0.1 (crate (name "plcnext-device") (vers "0.1.0") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)))) (hash "0clj3rxzm1v77ad38hkfcm81mzw6w1zb69sy55nngnbp08ywdr8x")))

(define-public crate-plcnext-sys-0.1 (crate (name "plcnext-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)))) (hash "1zzh9wgin3716kncr1irvv2r04wv6ns7wrzkn4yxb9n7r5mrjas4")))

(define-public crate-plcnext-sys-0.2 (crate (name "plcnext-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)))) (hash "1bn6xjvfhrcmdcpf38ghwlx14ypdskw45szp0jdv6kvh5zvk38hp")))

