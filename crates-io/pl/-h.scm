(define-module (crates-io pl -h) #:use-module (crates-io))

(define-public crate-pl-hlist-1 (crate (name "pl-hlist") (vers "1.0.0") (deps (list (crate-dep (name "pl-hlist-derive") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1walb6s3jgdwywkjgbjjidfb4dlz9qwp85np34n3mhgv2bgqq5x9")))

(define-public crate-pl-hlist-1 (crate (name "pl-hlist") (vers "1.0.1") (deps (list (crate-dep (name "pl-hlist-derive") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1127l32c25kkvvbqm9bbs1mcl8ghi91fj0s1hl5gnpy88kshfwq7")))

(define-public crate-pl-hlist-derive-1 (crate (name "pl-hlist-derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0a8pvp0vlskvxf2pa3gmcg4mpfnwvddrnkfd003njlhs9r2fr5hj")))

