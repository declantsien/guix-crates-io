(define-module (crates-io pl ru) #:use-module (crates-io))

(define-public crate-plru-0.1 (crate (name "plru") (vers "0.1.0") (hash "04q0mmzb083rxs15kqyk592bklwqbvza01qfhqzwfiz7s7b5gplf") (features (quote (("no_std"))))))

(define-public crate-plru-0.1 (crate (name "plru") (vers "0.1.1") (hash "0yz90dpz0rpfq91bqzraw73hx8iazff0an1mwi546nq6k4j8yz5g") (features (quote (("no_std"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.0.0") (deps (list (crate-dep (name "pgrx") (req "=0.7.4") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "0qah6rrn0jxl004m1iiccxl9xbp4ylzw11ac9blhnz9y28pq5f2g") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.1.0") (deps (list (crate-dep (name "pgrx") (req "=0.8.1") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "17h7dpfc3cvl4wsk0p2jshflrb7wqs3vcxh992fqndsdqspz8c0i") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.1.1") (deps (list (crate-dep (name "pgrx") (req "=0.8.2") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "1s5gvb26icm0ccrgdz5qrky0k1w476glv2yi95ifgrs1913654ij") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.1.2") (deps (list (crate-dep (name "pgrx") (req "=0.8.3") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "038ksph9l9zsnpqkjkaamdfrp4nbvh4n4rg3bmaw7lklqs5ayxac") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.1.3") (deps (list (crate-dep (name "pgrx") (req "=0.8.4") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "0pj38rvy5nd670l3f8wgkw8iygs3brh5159fhmvsk5g385ylyd8i") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.0") (deps (list (crate-dep (name "pgrx") (req "=0.9.5") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "19c0xwhpbkm74wbcacf6br2jx7504xk14bc3k73h543flv0lkciv") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.1") (deps (list (crate-dep (name "pgrx") (req "=0.9.6") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "1jpf6vrvn18dj9pp3pm62f6pd04fssnx8w0i8qi59z5hwlraddyw") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.2") (deps (list (crate-dep (name "pgrx") (req "=0.9.7") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "0di3ls1196iidm0mqh3b2l1p7cq3wfzn90cfy0l3bhd4izihlngv") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.3") (deps (list (crate-dep (name "pgrx") (req "=0.9.7") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "03yb99p71nzih295y7mwwhzpjg6pjmsqgnbbpw4gm0xkkzd4nih7") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.4-beta.pg16b2") (deps (list (crate-dep (name "pgrx") (req "^0.10.0-beta.0") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "0fmklfi6c6qhgs5g5l7782yzk7dkm3h4axc1n5ynxa950vxr68id") (features (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.4-beta.pg16b3") (deps (list (crate-dep (name "pgrx") (req "^0.10.0-beta.3") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "093xxv58znjjnmig3vxq49h5nlary9fp7jpcqif51afz36f0b5wg") (features (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.4") (deps (list (crate-dep (name "pgrx") (req "=0.10.0") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "04rwgk88wqwhcskj50l4pn0snjr0h3xzpbjfm5r053d1688h50vx") (features (quote (("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13")))) (yanked #t)))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.5") (deps (list (crate-dep (name "pgrx") (req "=0.10.0") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "1bph07jclxipqywr146kx0hmbvp68cnlwnj250gz7mvq33kqhgfd") (features (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.6") (deps (list (crate-dep (name "pgrx") (req "=0.10.1") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "1zmr65gsvrsld9zqci7i0fhi7lnhy8m4gw99l375g0g82qdwzk15") (features (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.7") (deps (list (crate-dep (name "pgrx") (req "=0.11.0") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "0zizp87lhdfw85d2xvsfxj0gq3jixck9shs7njq28x3267biczgc") (features (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgrx-1 (crate (name "plrust-trusted-pgrx") (vers "1.2.8") (deps (list (crate-dep (name "pgrx") (req "=0.11.0") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "0llf7ygqvk7fwksrq5xs4vgb3m441zjzn46yixbgmzm26iran3hc") (features (quote (("pg16" "pgrx/pg16") ("pg15" "pgrx/pg15") ("pg14" "pgrx/pg14") ("pg13" "pgrx/pg13"))))))

(define-public crate-plrust-trusted-pgx-0.0.0 (crate (name "plrust-trusted-pgx") (vers "0.0.0-do-not-use") (deps (list (crate-dep (name "pgx") (req "=0.7.2") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "1cvx6bbikhnq066wc83m2pwkygx2ld568gazq65f44snjk01xxax") (features (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

(define-public crate-plrust-trusted-pgx-1 (crate (name "plrust-trusted-pgx") (vers "1.0.0-rc.0") (deps (list (crate-dep (name "pgx") (req "=0.7.2") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "02myqc78bihbr25mkx6pmmg3gwb9fy8dir624nr24p9lh7bmbh77") (features (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

(define-public crate-plrust-trusted-pgx-1 (crate (name "plrust-trusted-pgx") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "pgx") (req "=0.7.4") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "0gbzdvam9dm520zrm7rgvb2jdnppp85zgqng5693sbfc6zyaj0ia") (features (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

(define-public crate-plrust-trusted-pgx-1 (crate (name "plrust-trusted-pgx") (vers "1.0.0") (deps (list (crate-dep (name "pgx") (req "=0.7.4") (features (quote ("no-schema-generation"))) (kind 0)))) (hash "1j9sfnvdacz02254lzl7cnvybcc3dnbg5ahf9p9js6jddg8hcsg1") (features (quote (("pg15" "pgx/pg15") ("pg14" "pgx/pg14") ("pg13" "pgx/pg13"))))))

