(define-module (crates-io pl oy) #:use-module (crates-io))

(define-public crate-ploy-0.1 (crate (name "ploy") (vers "0.1.0") (hash "0fjnbc6kkpbflfwfmqrlqjakav6dv887m8cziivbsyii0qk51pn8") (yanked #t)))

(define-public crate-ploy-0.0.1 (crate (name "ploy") (vers "0.0.1") (hash "0fhscp2ks40q604kw34953118vnhql7fcbqczqp5jic2lzjxva2p")))

(define-public crate-ploys-0.0.0 (crate (name "ploys") (vers "0.0.0") (hash "1hj12vs7lwiyximm7bi8wksrncjrp704srwdh4zp0h28p78hxsmh")))

