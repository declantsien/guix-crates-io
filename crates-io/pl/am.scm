(define-module (crates-io pl am) #:use-module (crates-io))

(define-public crate-plamo-0.1 (crate (name "plamo") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.43.1") (default-features #t) (kind 1)))) (hash "18vqqxm0a42aqrjh3k0hm6mmvb3q8vgdibrgwaqx6f6gnq5lgm5c")))

(define-public crate-plamo-0.2 (crate (name "plamo") (vers "0.2.0") (hash "08248k4xi4phgvmg5r5gaq2r8acn1rna1jlpbf4q7zmm0chal8xj")))

(define-public crate-plamo-0.3 (crate (name "plamo") (vers "0.3.0") (hash "0nqc3jjbp5pnhmpm4pz2nngxy5d18yjl2y9bjrs1cgv786ysxdxa")))

