(define-module (crates-io pl yp) #:use-module (crates-io))

(define-public crate-plyparse-0.1 (crate (name "plyparse") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ply-rs") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1djg57r0a99bvaaz077jlg2wmqp4bvaiz0yygpzav9454dw2vswz")))

(define-public crate-plyparse-0.1 (crate (name "plyparse") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ply-rs") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0rh4f8r48rcdd2p1hhmxqgc30r3z09dz3innxgfsilrvap92g9ap")))

(define-public crate-plyparse-0.1 (crate (name "plyparse") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ply-rs") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "06mxv3an3h1k694wm4mxfw5lvqw2vzd0799frm6rlxqmqcnwifdn")))

