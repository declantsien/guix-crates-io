(define-module (crates-io pl pr) #:use-module (crates-io))

(define-public crate-plprql-0.1 (crate (name "plprql") (vers "0.1.0") (deps (list (crate-dep (name "pgrx") (req "=0.11.3") (default-features #t) (kind 0)) (crate-dep (name "pgrx-tests") (req "=0.11.3") (default-features #t) (kind 2)) (crate-dep (name "prqlc") (req "^0.11.3") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1r59a1gsyf8hnnb6awwnws40mywz6qcldc5iqdbndbg75gb2b2rc") (features (quote (("pg_test") ("pg16" "pgrx/pg16" "pgrx-tests/pg16") ("pg15" "pgrx/pg15" "pgrx-tests/pg15") ("pg14" "pgrx/pg14" "pgrx-tests/pg14") ("pg13" "pgrx/pg13" "pgrx-tests/pg13") ("pg12" "pgrx/pg12" "pgrx-tests/pg12") ("default" "pg16"))))))

