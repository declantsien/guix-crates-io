(define-module (crates-io pl ur) #:use-module (crates-io))

(define-public crate-pluralistic-rs-0.1 (crate (name "pluralistic-rs") (vers "0.1.0") (deps (list (crate-dep (name "arr_macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0z5jaq1xrfk46dml5m1mlsrxsnxyxh5xzchn9raz397yf9fvjy6d")))

(define-public crate-pluralistic-rs-0.2 (crate (name "pluralistic-rs") (vers "0.2.0") (deps (list (crate-dep (name "arr_macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1fh1aq79ghrfw0z0wbmf363npvizfj5dzm8gimjvx5pwrb3kv2h6")))

(define-public crate-pluralize-0.1 (crate (name "pluralize") (vers "0.1.0") (hash "0g2hqkllkfyvbpjk1ijd9npcil6zgf5pqm2n3jnbp7b8mvd7qp92") (features (quote (("std") ("default" "std"))))))

(define-public crate-pluralize-0.2 (crate (name "pluralize") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1xl7xjb4dxrcvyqqfb367ap49h2nfz7xpl3lz3hraxgm7jd222cn") (features (quote (("Remover") ("Options")))) (yanked #t)))

(define-public crate-pluralize-0.2 (crate (name "pluralize") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0qx2ds82bpav0kqz2fpwh9yg9z7fbqjq0fgsar1pn8xy3i4vxk05") (features (quote (("Remover") ("Options"))))))

(define-public crate-pluralize-rs-0.1 (crate (name "pluralize-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "voca_rs") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "1vskdkp68kix0s0z6r77s97j5b2l6lg108apmb9hhdzc4shh8g80")))

(define-public crate-pluralize_derive-0.1 (crate (name "pluralize_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.16") (default-features #t) (kind 0)))) (hash "0axwc1gi6i0zdp2989985x73ds4jnijwnz5dvzlxf9wwk8rnpi5r") (yanked #t)))

(define-public crate-pluralize_derive-0.1 (crate (name "pluralize_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.16") (default-features #t) (kind 0)))) (hash "1ccjyrpmmgsq89awsicja8sskgmz02gkkir684cq8m2xmahkb7gi")))

(define-public crate-pluralizer-0.1 (crate (name "pluralizer") (vers "0.1.0") (hash "19j0hlllfk4lk1xdqrh2p07iisqvah05fqbpvqn551lj841986v0")))

(define-public crate-pluralizer-0.2 (crate (name "pluralizer") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1ya1f95ghlrblc4h0w5hxzdnypsr673j5qv8b116cyx33ww9j7l4")))

(define-public crate-pluralizer-0.3 (crate (name "pluralizer") (vers "0.3.0") (deps (list (crate-dep (name "ctor") (req "^0.1.21") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0fs9ms1dz29lkrknaakdklfl9s4mf5s74gnpk1q1vjq2vr7i40v9")))

(define-public crate-pluralizer-0.3 (crate (name "pluralizer") (vers "0.3.1") (deps (list (crate-dep (name "ctor") (req "^0.1.21") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0bhv0dmh7fyv4p4bq58v173hs14jy010ijfdkh3cx6x9n5p6lmpp")))

(define-public crate-pluralizer-0.3 (crate (name "pluralizer") (vers "0.3.2") (deps (list (crate-dep (name "ctor") (req "^0.1.21") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1qcpblf93r1sl7rx2zh0fpbkv4rraq86dzzb9w5mx9zdxj9ggfa8")))

(define-public crate-pluralizer-0.4 (crate (name "pluralizer") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "136kfwk9w8l9fr1nzlb9sm3j39j1y15rv9kfhihqnyxnjip63r1m")))

(define-public crate-pluralizer-cli-0.1 (crate (name "pluralizer-cli") (vers "0.1.0") (deps (list (crate-dep (name "pluralizer") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0489znsgzb7hqilpdi5nsggpmj3sglszibns9wglzw9nw7lgp6kd")))

(define-public crate-pluralizer-cli-0.1 (crate (name "pluralizer-cli") (vers "0.1.1") (deps (list (crate-dep (name "pluralizer") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0rkvhf12zy6pwh80xasj0mchvpglp0chimwgg2kqyraxbmv5aipq")))

(define-public crate-pluralizer-cli-0.1 (crate (name "pluralizer-cli") (vers "0.1.2") (deps (list (crate-dep (name "pluralizer") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0xjf4s519pxxhsqvny4ahk4c2lprs7fz8gvrx6fwnh7ksd7g0mbv")))

(define-public crate-plurals-0.1 (crate (name "plurals") (vers "0.1.0") (hash "1iyrm6lhsmdwhxd1mzx1w14l017yamnn734ph44r6pbazsbaxybi")))

(define-public crate-plurals-0.2 (crate (name "plurals") (vers "0.2.0") (hash "0hvn4jvgddzd843rw78kzyk3qn22845q48k84p06y2ld7kacxnir")))

(define-public crate-plurals-0.3 (crate (name "plurals") (vers "0.3.0") (hash "0lnqkb2dxrdfnahiipa23lkrwx97nz1mbkqad1vj6zbqa3nsgdc4")))

(define-public crate-plurals-0.4 (crate (name "plurals") (vers "0.4.0") (hash "14nlk91j1ylwashicppb1qi8z7bm5kjx5g4aijg68hm4ajrsb9p8")))

(define-public crate-plurid_delog-0.0.0 (crate (name "plurid_delog") (vers "0.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qn27bkjv60qmx4lzgbr6d72whipls6h3yrllw7sn14h33xncxhn")))

(define-public crate-plurr-0.2 (crate (name "plurr") (vers "0.2.1") (hash "00fg678zlp0xs1v3fkq9niqqckq2v5r5a2y6cpk7mzqv6n0idsvl")))

