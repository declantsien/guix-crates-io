(define-module (crates-io pl ai) #:use-module (crates-io))

(define-public crate-plaid-0.1 (crate (name "plaid") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yup-hyper-mock") (req "^1.3") (default-features #t) (kind 2)))) (hash "1awj2m0mwfxb0v630g1vxv1qj3clfil23nb53liq6nvwgvg6y5bc")))

(define-public crate-plaid-0.2 (crate (name "plaid") (vers "0.2.0") (deps (list (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yup-hyper-mock") (req "^2.0") (default-features #t) (kind 2)))) (hash "0q50kvn199cc29mzvspyzlhi8a43kn3694smw49fqszbrxfndgh8")))

(define-public crate-plaid-0.3 (crate (name "plaid") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.3.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 0)))) (hash "1ygqid76c12yqgfy6ldjd8vrnpawccpcygzgqcwwr839yzpvyvrk") (yanked #t)))

(define-public crate-plaid-1 (crate (name "plaid") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.3.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 0)))) (hash "14mwpbcnc1a9f4pzrckc0n5lvyj4s85hz49zsvggdjx16l4k4827")))

(define-public crate-plaid-1 (crate (name "plaid") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.3.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 0)))) (hash "0zbgmhinnn72f09xi2n46s3ns2xfv00p4wrg7zzmafwval9pxd1x")))

(define-public crate-plaid-1 (crate (name "plaid") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.3.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 0)))) (hash "1b75vivqdiawjb1fh9hs5q71cb3366yika4g0cbwp895chcp41b8")))

(define-public crate-plaid-2 (crate (name "plaid") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0y0nphj9dnm7p7r250rb41xnm2jww52fw8p5yxd2kl0dvhj0if6k")))

(define-public crate-plaid-2 (crate (name "plaid") (vers "2.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0lxaz40bwk84xn73cd698i6wl0czm13qa5jkahv9nm4vxk3p3z5a")))

(define-public crate-plaid-3 (crate (name "plaid") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "09gxlmmmnxpnp0jaljvspbbif0g1nw4q2bysxpra28bykiyr4fh3")))

(define-public crate-plaid-3 (crate (name "plaid") (vers "3.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1vv50ri118vzni66iyz3axkjcwskyzhddj0jz93dx9xca28g0zzg")))

(define-public crate-plaid-3 (crate (name "plaid") (vers "3.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1rhc3vzh8zsjzgcckx44zk1y05nd923giv86w2jkbcv2qic5g0m9")))

(define-public crate-plaid-4 (crate (name "plaid") (vers "4.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0lz1akvcfmin265xij3wrm7zn18qrrnnwaanrcs9x0glk26a0m6p")))

(define-public crate-plaid-5 (crate (name "plaid") (vers "5.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1mphc85sqhrxs387gr4k5makcdznbvmqvc1f1my21kdy8xvdk0h1")))

(define-public crate-plaid-6 (crate (name "plaid") (vers "6.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.15.0") (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0nnd12f86gbqi5rpm1gw6k78g4cds5yv1z3l76hfw37pf0j1n0xw")))

(define-public crate-plaid-7 (crate (name "plaid") (vers "7.0.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.33") (features (quote ("serde-with-str"))) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "04hd8p72400yymb2yyysii4bklycjmwba9jkdzb0x3q4xw4hagzr")))

(define-public crate-plaid-7 (crate (name "plaid") (vers "7.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.33") (features (quote ("serde-with-str"))) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0cxmdy8slzm1ani09kpb7g0mvq8qk0lvyw6wlvrifzji11r51756")))

(define-public crate-plaid-8 (crate (name "plaid") (vers "8.0.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.21.3") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.33") (features (quote ("serde-with-str"))) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "12dgz1vxia33497qp8897bs56igfapl4n1z8380ww0rgdrac5sak")))

(define-public crate-plaid-link-hotfix-1 (crate (name "plaid-link-hotfix") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.15.0") (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "014am27j9q0yj2rx4wm06xaspk65fbl8yjr9wypbvagfjdj1rh2b") (yanked #t)))

(define-public crate-plaid-openapi-0.1 (crate (name "plaid-openapi") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nfz91y33wbpgmbia3x12jcxpsfz2nvmh0skw9m7d82dhz0z6skj")))

(define-public crate-plaid-openapi-0.1 (crate (name "plaid-openapi") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "openapi-client-generator") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1xr74ghb044bbw13xfhmkib28kych2d0n4g7yginb1n2gvd9ggf4")))

(define-public crate-plaid-openapi-0.1 (crate (name "plaid-openapi") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "openapi-client-generator") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0p7bz9vighrirx03lylpxh3wxd1dcl7hy1v6q60ngsvv7ml95ww6")))

(define-public crate-plaid-openapi-0.1 (crate (name "plaid-openapi") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "openapi-client-generator") (req "=0.1.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1a41hr897401iz3c9adr92kkh0119g60wxh6a3ydv972ssn5an56")))

(define-public crate-plaid-openapi-2 (crate (name "plaid-openapi") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0llggx1kas5109wvm5lvd5pgjqzf38pz0vr4mibhv0zrjfsjfc5b")))

(define-public crate-plaid-openapi-3 (crate (name "plaid-openapi") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1rgrh6fp494ndipk5nns2sfkfjgj71mm8fjv8dsmh8mn3a8d4a5q")))

(define-public crate-plain-0.0.1 (crate (name "plain") (vers "0.0.1") (hash "05r30m5fq9634c4y52j3y6by861f2ag40a2qi0x592n4zjg475kd")))

(define-public crate-plain-0.0.2 (crate (name "plain") (vers "0.0.2") (hash "1bkg9ijslpasy9dqhd43yf94dsqbnkckpmwfl2hxdjwhd5830n2r")))

(define-public crate-plain-0.1 (crate (name "plain") (vers "0.1.0") (hash "1l9j84ax4mbfrc5dfdb84yhg83cn7ichbld6ppzn0709xkgqc776")))

(define-public crate-plain-0.2 (crate (name "plain") (vers "0.2.0") (hash "01m3a63ailmcm0z9jxm0rx0j0gccr2gs0l9c27j81l51lrl94v8i") (yanked #t)))

(define-public crate-plain-0.2 (crate (name "plain") (vers "0.2.1") (hash "08iix96dsybqd8g3622pr0762s82p640y0nf0dskbvh4awyl4mfs") (yanked #t)))

(define-public crate-plain-0.2 (crate (name "plain") (vers "0.2.2") (hash "08ssdqsx665qc789df923sz7fnxqhq401i167nv6y0dbgrxf1sha") (yanked #t)))

(define-public crate-plain-0.2 (crate (name "plain") (vers "0.2.3") (hash "19n1xbxb4wa7w891268bzf6cbwq4qvdb86bik1z129qb0xnnnndl")))

(define-public crate-plain-binary-stream-0.1 (crate (name "plain-binary-stream") (vers "0.1.0") (hash "0kf85lr7zr6yabk1gyxfxh99mnxc6rma1h0r3bfsqjadf0acj6p9")))

(define-public crate-plain-map-0.1 (crate (name "plain-map") (vers "0.1.0") (hash "02l3b4s054yjhsz0f5wgpkn2js6ginbyqh0k9jl96nwdzxy7mxkh")))

(define-public crate-plain-map-1 (crate (name "plain-map") (vers "1.0.0") (deps (list (crate-dep (name "smallvec") (req "^0.6.10") (default-features #t) (kind 0)))) (hash "0i4cmcralif8jzw9wwwz9izz306v1bv6w7mffhf6ax7z13b18arl")))

(define-public crate-plain-map-2 (crate (name "plain-map") (vers "2.0.0") (deps (list (crate-dep (name "smallvec-stableunion") (req "^0.6.10") (default-features #t) (kind 0)))) (hash "1pa0wdjcgvvbkm90c2wla4zdq7m9ki03hwas9n7kwpifja7bkjdq")))

(define-public crate-plain_enum-0.1 (crate (name "plain_enum") (vers "0.1.0") (hash "1fcky00h9hj67qcmfj2warqcv3glqk4giqy6jgikvm0zxg89g60j")))

(define-public crate-plain_enum-0.1 (crate (name "plain_enum") (vers "0.1.1") (hash "1l5fqjpj6k6yx2pqd147l2mbw3n4as862j8bwc447blc3by5gvic")))

(define-public crate-plain_enum-0.1 (crate (name "plain_enum") (vers "0.1.2") (hash "0dyy5l10m93y0ykqnqj46m6kb0mbwx9lc13l4ys4ja0j0jiyypzl")))

(define-public crate-plain_enum-0.1 (crate (name "plain_enum") (vers "0.1.3") (hash "1l1dl8xvdmqi9kx2kmmn0qpybwh5dgfww88qvv2nd6mf3r9a0yqn")))

(define-public crate-plain_enum-0.1 (crate (name "plain_enum") (vers "0.1.4") (hash "0k5gsnfysw3lzgvz797jkbsip3jnkc65d95x64dgz8f8vfckbnfp")))

(define-public crate-plain_enum-0.2 (crate (name "plain_enum") (vers "0.2.0") (hash "0s4rm2cbapnnnr45fwv7wa36f46j70fw1f1kd1hsjbr2llijxicm")))

(define-public crate-plain_enum-0.3 (crate (name "plain_enum") (vers "0.3.0") (hash "0vgfgjpxawz1rn3gvfbcp1bz3dlgbns6zk9qq9gyfmj7r1d8wgnp")))

(define-public crate-plain_enum-0.3 (crate (name "plain_enum") (vers "0.3.1") (hash "004204gap9ndyfyirkzvlqp386xmi3l8h4ny1dciknxg527ykash")))

(define-public crate-plain_enum-0.4 (crate (name "plain_enum") (vers "0.4.0") (hash "02lz355cs1blpqfkrf92fzvzyi9zjhb3r2dzwr07jzdq3yw7bz09")))

(define-public crate-plain_enum-0.5 (crate (name "plain_enum") (vers "0.5.0") (hash "0hjbjvi6diy60n2yij16bxpbk0cdpbdzih6wqgv0s93rfirsihcn")))

(define-public crate-plain_enum-0.6 (crate (name "plain_enum") (vers "0.6.0") (hash "1n01w081r4s0airfgbcbzh7k2a08b7mx1fcqm4gashl5nf8mridd")))

(define-public crate-plain_enum-0.6 (crate (name "plain_enum") (vers "0.6.1") (hash "1y3mrbmdn4w42y9n112zsn7x6j24jv8s7rzbj2g17n67r27vs9zi")))

(define-public crate-plain_enum-0.6 (crate (name "plain_enum") (vers "0.6.2") (hash "06agnhzrczf4izlw396g4h76lm56rvxsn4hj2jjqw0v39zkri7kw")))

(define-public crate-plain_enum-0.6 (crate (name "plain_enum") (vers "0.6.3") (hash "0mish4wrwpq0mr94bd7j8mv48ir9z3f84h55z44fb4g8nz0ydfm5")))

(define-public crate-plain_enum-0.7 (crate (name "plain_enum") (vers "0.7.0") (hash "1rqz2fb1y5dlg57s0nfvdlq77qsa2i29ax40s0441h70rrv24whm")))

(define-public crate-plain_enum-0.7 (crate (name "plain_enum") (vers "0.7.1") (hash "0y5s29mcjnzy1v2cic4wv0d26b89fqlmdsns6i5bvb7agwr7h5f6")))

(define-public crate-plain_enum-0.8 (crate (name "plain_enum") (vers "0.8.0") (hash "11n2gwk78vxq75wxsq41lr30k4r6nr46ix7ykdxgl8hxvasfiwiq")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.0") (hash "1qjf2b09j1xy71cdfv3nhsimqhgx9pfllfrr91c6a7fdf68ag8ia")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.1") (hash "07f3gdlh99k5mrn6fcyzji9cdcmrdb14rllk137rxvjv9x3wi48v")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.2") (hash "0hdd76kss03j4fckrfgx32lla6j0j9xlskrf45fpb3jrwnck9syz")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.3") (hash "0i7wvwp379fc7r5iy6n28wvdhvkfcfi2zc3zfp21pv6m3vwzk5xw")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.4") (hash "0i5n53145hbvv10zv4w4fd5639jz25jx5mijfniagbrqsq3ykdy8")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.5") (hash "14mm0avhjw7w5ggmwn2dhqcyg1akpqzcmif8fc67px04s5lz9bx5")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.6") (hash "1vvcrxpqb7bh0dpagn7hj1vphxq5i69il6538cfrpxx45agzql81")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.7") (hash "1iw6ylw5br2jqy006rx5jgplhnmiscdbm545z8v4lfv4vd3d55vk")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.8") (hash "1jw8n2ngw9w5i96y1h8ncg12x7lijdrb9hvpkk75z3cfnkd7ps8s")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.9") (hash "16z3k17k41dy5drakfhfzmwgl005gqq88nkn1gfhmwsgzgfnb85b")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.10") (hash "1cf9n8d053sh2c64imgfsshxb5cy6h8jrdqw06p6jnk4fzkjrddm")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.11") (hash "13dmanm2wgg0ca58xz9k30fg98p2d5pw7ia3fm074mazm4zw138d")))

(define-public crate-plain_enum-0.9 (crate (name "plain_enum") (vers "0.9.12") (hash "1sy3pl9a5fc1dl74vpv8pjb0p5ys2ck74yzfpg09ynz2a87k6qgi")))

(define-public crate-plain_enum-0.10 (crate (name "plain_enum") (vers "0.10.0") (hash "13p1jgd54nbavkmr7djkj2wfrj4vfz0qip991pk25qxpm6zri656")))

(define-public crate-plain_enum-0.11 (crate (name "plain_enum") (vers "0.11.0") (hash "0p0gsi8jivm5l73gzv6m5d42v54fr0wrv8iqaqanxwx9r5djck06")))

(define-public crate-plain_enum-0.12 (crate (name "plain_enum") (vers "0.12.0") (hash "1va1cvf2r7lnbnv4lwfcyqsp8hn9dlnmsy59nkfy9rn345mxwc7j")))

(define-public crate-plain_hasher-0.1 (crate (name "plain_hasher") (vers "0.1.0") (deps (list (crate-dep (name "crunchy") (req "^0.1") (default-features #t) (kind 0)))) (hash "15frg7m34nb5pm520znvixi3cmny8infrl075ha13xcj763q1bl3")))

(define-public crate-plain_hasher-0.2 (crate (name "plain_hasher") (vers "0.2.0") (deps (list (crate-dep (name "crunchy") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "03x98j8b3hkyb9g7hqv21qck8z5ybn4d4zcvvc5ayjnkn6367ylm") (features (quote (("std") ("default" "std"))))))

(define-public crate-plain_hasher-0.2 (crate (name "plain_hasher") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "crunchy") (req "^0.2.2") (kind 0)))) (hash "0aha0xljkjcsmvbmp36ckqd2gddyw6ial2hvlam56shs0vsj874g") (features (quote (("std" "crunchy/std") ("default" "std"))))))

(define-public crate-plain_hasher-0.2 (crate (name "plain_hasher") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "crunchy") (req "^0.2.2") (kind 0)))) (hash "1k5lwdi76gz4c47z0bbkxgr5i9y89cclq3yp8cn7rs6x3d4yc68y") (features (quote (("std" "crunchy/std") ("default" "std"))))))

(define-public crate-plain_path-0.1 (crate (name "plain_path") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "0ds53pq089c5kxici5rcbhrg1xmq3drsj4pb0772wmp0m05986wz")))

(define-public crate-plainjson-0.1 (crate (name "plainjson") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "unicode_reader") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0fkyslnpf2k9qdaw382x74q9cscn7hmwlf1n0grdhfhnhyzdv6h4")))

(define-public crate-plainjson-0.1 (crate (name "plainjson") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "unicode_reader") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "085pam98wcxbss24wbr198acvpwf14gif19l3bfq678hgf7vgd80")))

(define-public crate-plainjson-0.1 (crate (name "plainjson") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "unicode_reader") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1vnvykhk9v5a2w14l0mpm8bxqkn95vmp6g26dh5i0cwbap720wiq")))

(define-public crate-plaintalk-0.0.1 (crate (name "plaintalk") (vers "0.0.1") (hash "1vxw4m41qd154v98mffnlk5swcx0xz3k4lzks5rngzk6hmldd76n")))

(define-public crate-plaintalk-0.0.2 (crate (name "plaintalk") (vers "0.0.2") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "0hh5rrmmgzfj5x4688ycjahjvrrngpf9zcjp51rsvahv5y133f9h")))

(define-public crate-plaintalk-0.0.3 (crate (name "plaintalk") (vers "0.0.3") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "1vj6fbqq6h2jaab82s99q6znxbrlza020cd1cw98afa5cqb3zd9f")))

(define-public crate-plaintalk-0.0.4 (crate (name "plaintalk") (vers "0.0.4") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "0ar53aggq688cf9x1wzrdpwmnbsmzm77jdrzin94j5mizgpqlrzi")))

(define-public crate-plaintalk-0.0.5 (crate (name "plaintalk") (vers "0.0.5") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "1mfpqh8q33fwf7y7vdg8qng9ypkg2mssvc4wk7c6gagg699xq8k0")))

(define-public crate-plaintalk-0.0.6 (crate (name "plaintalk") (vers "0.0.6") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "1khfckryi10iw9vsj0zxm0bjr4xby6mz2hicjhc5k3sfhar2drps")))

(define-public crate-plaintalk-0.0.7 (crate (name "plaintalk") (vers "0.0.7") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "0j98435ksl2w8d1amhyqwk7bhl1ih36zxhyyhmivak4pkd7k6vb5")))

(define-public crate-plaintalk-0.0.8 (crate (name "plaintalk") (vers "0.0.8") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "1assbyh13l1cz00a7gg1jchdjdcaww4bjxqhwjiw1bwl07avlp1j")))

(define-public crate-plaintalk-0.0.9 (crate (name "plaintalk") (vers "0.0.9") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "08cfjc86wl3ixspfgmx5fxnmrqc1v5ivmnqr9sp2anxrdw3v33yq")))

(define-public crate-plaintalk-0.0.10 (crate (name "plaintalk") (vers "0.0.10") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "0776n7sbhmqcjjhqil7ccz4hvsl6hi23iahc3vxlc97y1c97s1g5")))

(define-public crate-plaintalk-0.0.11 (crate (name "plaintalk") (vers "0.0.11") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "1jrha0fhny3rg06dc5jn1iaz66s4h81ky76liamgxchzmkyfs9k4")))

(define-public crate-plaintalk-0.0.12 (crate (name "plaintalk") (vers "0.0.12") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "00xzh1kk7dvn5kjhgfc86dlij863xlmj7w724vfj8m902g10n73m")))

(define-public crate-plaintalk-0.0.13 (crate (name "plaintalk") (vers "0.0.13") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "18s84jxrf659g36wcn1wwa0wikqwibvr1qbq8y5wrngsrczirwhk")))

(define-public crate-plaintalk-0.0.13 (crate (name "plaintalk") (vers "0.0.13-1") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "1g702s2f8i4wngmflrjr90fs83k23j0lpyv8i97ynkl7fhpfd1ij")))

(define-public crate-plaintalk-0.0.14 (crate (name "plaintalk") (vers "0.0.14") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "1jqxpibszfja9nvgzzyh5w04m9psfg3iral62j669sjv3jbxs2v0")))

(define-public crate-plaintalk-0.0.15 (crate (name "plaintalk") (vers "0.0.15") (deps (list (crate-dep (name "num") (req "~0.1") (default-features #t) (kind 0)))) (hash "0ga6mdbk5scaq1m8gb3k9x1pbi5l4n453dsw2r542ghn37ynrwr2")))

(define-public crate-plaintext-0.0.0 (crate (name "plaintext") (vers "0.0.0") (hash "11cs8f2ryvs78rkf6hbp4i9nk0jla7znn0cwpzbr8sk1h6wz1nxy") (yanked #t)))

(define-public crate-plaintext-0.27 (crate (name "plaintext") (vers "0.27.1") (deps (list (crate-dep (name "asynchronous-codec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "tetsy-libp2p-core") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "unsigned-varint") (req "^0.7") (features (quote ("asynchronous_codec"))) (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0js8zm90xr4xvr06pvq2z325raicb5fgp56lfza7dh934hbdksgq")))

(define-public crate-plaintext-0.27 (crate (name "plaintext") (vers "0.27.0") (deps (list (crate-dep (name "asynchronous-codec") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "tet-libp2p-core") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "unsigned-varint") (req "^0.6") (features (quote ("asynchronous_codec"))) (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1j144b7yznd4priq9yxiqyfbmh0ryjicw4cc941iy27gbsi0m23h")))

