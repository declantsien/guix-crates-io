(define-module (crates-io pl wo) #:use-module (crates-io))

(define-public crate-plwordnet-0.0.3 (crate (name "plwordnet") (vers "0.0.3") (deps (list (crate-dep (name "bstringify") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.28.2") (default-features #t) (kind 0)))) (hash "07wbw6fiv77zpzp3h2gldidx6dm43qmpiqbjn4cscfci6yn0bvl3") (yanked #t)))

(define-public crate-plwordnet-0.0.4 (crate (name "plwordnet") (vers "0.0.4") (deps (list (crate-dep (name "bstringify") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.28.2") (default-features #t) (kind 0)))) (hash "1l1g6vb6p514s02qjcfxbwsphyfv60ii8bb2k9k23yf6ari0sg2m")))

