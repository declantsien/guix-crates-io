(define-module (crates-io pl oc) #:use-module (crates-io))

(define-public crate-ploc-bvh-0.0.1 (crate (name "ploc-bvh") (vers "0.0.1") (deps (list (crate-dep (name "bevy_math") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("cargo_bench_support"))) (kind 2)) (crate-dep (name "fastrand") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "radsort") (req "^0.1") (default-features #t) (kind 0)))) (hash "1q81sh021aa11wv3qb06g3xkpmnngl9f8jk91c72kz8lmc7db7jr")))

(define-public crate-ploc-bvh-0.1 (crate (name "ploc-bvh") (vers "0.1.0") (deps (list (crate-dep (name "bevy_math") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("cargo_bench_support"))) (kind 2)) (crate-dep (name "fastrand") (req "^1.9") (default-features #t) (kind 2)) (crate-dep (name "radsort") (req "^0.1") (default-features #t) (kind 0)))) (hash "16kjiw6kjq076gsihhrzil3xd8n1lyn7zab26ws9layrd1v0vrww")))

