(define-module (crates-io pl ck) #:use-module (crates-io))

(define-public crate-plckit-0.1 (crate (name "plckit") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "03xb20k21i0yh6wn939c1ar4xqpvsp9x8gyi7gnappi78bk7gram")))

(define-public crate-plckit-0.1 (crate (name "plckit") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0wmrgqfx66yw6p20zbxi0pyd3yw4qz664lxvmdni7yswml1xwsai")))

(define-public crate-plckit-0.1 (crate (name "plckit") (vers "0.1.2") (deps (list (crate-dep (name "bma-ts") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "012xv91hj7xfkaki8asr3ybx1d3kvx3gbb0dsfx3mav27p7ldjbv")))

