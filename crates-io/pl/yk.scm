(define-module (crates-io pl yk) #:use-module (crates-io))

(define-public crate-plykit-0.1 (crate (name "plykit") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "04ma4b2s3m7zky61yyknwiqqh3agwbkf4c8khd33lik35kbb7gsf")))

(define-public crate-plykit-0.1 (crate (name "plykit") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1p1alr30q2z48yblp43v6gvsc6rdpvh5ysyy181j7kn5ycd50497")))

(define-public crate-plykit-0.1 (crate (name "plykit") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1c1x5fcy4cj8926ah6iw8sarqpxqk5aa23lzmd460h1l83fvj34k")))

(define-public crate-plykit-0.1 (crate (name "plykit") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0w6xkz2bmrqjm4qy22gql5i1978nsabggia47kb921xyhp8b7n5g")))

(define-public crate-plykit-0.1 (crate (name "plykit") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1db7apyd8sq1j98dnhslgdwxshyl4jclz1lzxxzhzm9qfd3cgsa2")))

(define-public crate-plykit-0.1 (crate (name "plykit") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "09dvll2d0a62wvng9c164kqg8l039xkdycywxrplccmypmcjpfl1")))

(define-public crate-plykit-0.1 (crate (name "plykit") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0h67a0kk04dfhfknd2gdhagkxq6fxbls0ih1730nn2gqkacwzxwm")))

