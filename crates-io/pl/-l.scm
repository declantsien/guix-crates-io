(define-module (crates-io pl -l) #:use-module (crates-io))

(define-public crate-pl-lens-1 (crate (name "pl-lens") (vers "1.0.0") (deps (list (crate-dep (name "pl-lens-derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "pl-lens-macros") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0g9ysb0pvqx8kmjwdd9vc0xij92bvr168bh88v8vlmy57fgfj3da")))

(define-public crate-pl-lens-1 (crate (name "pl-lens") (vers "1.0.1") (deps (list (crate-dep (name "pl-lens-derive") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "pl-lens-macros") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1iwx763bwx2ax1xr76g85xdapq3i1d7znydjwahjwrc08rhbpdhp")))

(define-public crate-pl-lens-derive-1 (crate (name "pl-lens-derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bmxymsn0wjzffjbjk9zf0yfwcjc8h5n1bb89lkw4kd3hb5lfzyr")))

(define-public crate-pl-lens-derive-1 (crate (name "pl-lens-derive") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ycg3fjxdlabglfmdqi251jf3nyma4s6qaqs6dcc0gkizvqidlvi")))

(define-public crate-pl-lens-macros-1 (crate (name "pl-lens-macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "006lgaw49d6nsacn8dkq71iaznpcfyy7j541gs21jskwdl152600")))

(define-public crate-pl-lens-macros-1 (crate (name "pl-lens-macros") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "170fzxiv4f4mq0yqp6yby3d9ccc37qi9ckyrhpkfv304fdfvkm3q")))

