(define-module (crates-io pl io) #:use-module (crates-io))

(define-public crate-pliocomp-0.1 (crate (name "pliocomp") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0x7swnnz4zm2mx6w5h0iaca13823k8dy6rvyzrnh0rnazs19azbs")))

(define-public crate-pliocomp-0.1 (crate (name "pliocomp") (vers "0.1.1") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qdizmzf5yq4b4skj0f0hsww5dsbma5mhswmz5l0ay6qcagw6ycj")))

