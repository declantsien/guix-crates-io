(define-module (crates-io pl of) #:use-module (crates-io))

(define-public crate-plof-0.1 (crate (name "plof") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "http-body-util") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sigb8nqhvbghc2jlhldxafbga4xfddxkgcf5imks7jldr0f51c3")))

