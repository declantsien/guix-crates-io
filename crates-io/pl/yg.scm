(define-module (crates-io pl yg) #:use-module (crates-io))

(define-public crate-plyg-0.1 (crate (name "plyg") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1vjmw1ghm84ys5k34igsxvxkddhn1cl7yq0vwnzzyq04p0dbry7z")))

(define-public crate-plyg-0.1 (crate (name "plyg") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0nw5dhj0l655zjdy3snkmd9gbaqzhj5414kkf4468f3q43rfr3wl")))

