(define-module (crates-io pl op) #:use-module (crates-io))

(define-public crate-plop-0.0.1 (crate (name "plop") (vers "0.0.1") (deps (list (crate-dep (name "aci_png") (req "^0.8.0-pre0") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.13") (default-features #t) (kind 0)))) (hash "1978r565famxsa32vyfni4afb3385ci9x2cm2hc0k47dzwqqc169")))

(define-public crate-plop-cli-0.1 (crate (name "plop-cli") (vers "0.1.0") (deps (list (crate-dep (name "aws-config") (req "^0.46.0") (default-features #t) (kind 0)) (crate-dep (name "aws-sdk-s3") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wma3dfl2mwk1mmgi4kyqn8chafh3cqfaqskw9lzlis5gzm40lpi")))

(define-public crate-plop565613631-1000 (crate (name "plop565613631") (vers "1000.1.0") (hash "1aa9d20d71rq4xw5035v89nw0wcykr7f33qbr4mcwvz9qh6mii4l")))

(define-public crate-plop565613631-10 (crate (name "plop565613631") (vers "10.1.0") (hash "1yj538dm4d8f6h13y382dpja540scj2nxnh4pmq0h18vdw8a38hm")))

