(define-module (crates-io pl rs) #:use-module (crates-io))

(define-public crate-plrs-0.1 (crate (name "plrs") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.13.2") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "0fzcv642z12iv3dpqvf6z4a7z9z4kq7v24kx2a8sv1nyxxzf7rpg")))

