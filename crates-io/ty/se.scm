(define-module (crates-io ty se) #:use-module (crates-io))

(define-public crate-tyser-0.1 (crate (name "tyser") (vers "0.1.0") (deps (list (crate-dep (name "md-tangle-engine") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "org-tangle-engine") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s19xcfx64inlvhv3bi7mf0gmc2cyh4266dvbbvyw06jia3dr5ij") (yanked #t)))

