(define-module (crates-io ty en) #:use-module (crates-io))

(define-public crate-tyenum-0.1 (crate (name "tyenum") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.30") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00mzgbd7av09lw5ahbgakiclh4jjrxg7xm71jh1v30jkqdv1q1v8")))

(define-public crate-tyenum-0.2 (crate (name "tyenum") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00w16n1rwsgp6kaa6bxi862ls1cv5dqwws7g4glgy70w7d9c9ix2")))

(define-public crate-tyenum-0.2 (crate (name "tyenum") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "09gxf4v9a1dwzc9qcz284m1csc247d7v47f4sis7m9scy2qkmm9a")))

(define-public crate-tyenum-0.5 (crate (name "tyenum") (vers "0.5.0") (deps (list (crate-dep (name "tyenum_attribute") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "12ids4b0qz02sqgfdw6ilrkcmwkf42np5ws7qgy63vlgvnq61b8j")))

(define-public crate-tyenum_attribute-0.5 (crate (name "tyenum_attribute") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.31") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "127cv3i7fzaz3wgqlqc9a8nlqr52fqba7x22kdr2i09p3vr85vz7")))

