(define-module (crates-io ty pp) #:use-module (crates-io))

(define-public crate-typprice-rs-0.1 (crate (name "typprice-rs") (vers "0.1.0") (deps (list (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "1y172kk2nh0zg3k0rrbybnl58mllq8csf38lgni96zdax1i9f4hd")))

