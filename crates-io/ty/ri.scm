(define-module (crates-io ty ri) #:use-module (crates-io))

(define-public crate-tyria-0.0.1 (crate (name "tyria") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kakiw6ancsxlrlzgalrcl5pl91pzymk4dxqs16nizv3lfff8mx4")))

