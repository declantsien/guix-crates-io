(define-module (crates-io ty gh) #:use-module (crates-io))

(define-public crate-tyght-map-0.1 (crate (name "tyght-map") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0rvpll3iv29ypg7hry9va9706grw8akg9biz0bjv8j1xrib2dfh5")))

(define-public crate-tyght-map-0.1 (crate (name "tyght-map") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1fmn6x5igz4cz33y5la34gwndc3kcmp7chwh7k4pg9fixyh7faxa")))

(define-public crate-tyght-map-0.1 (crate (name "tyght-map") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0dhc8g50bnr5i4wvm8gfpbg1nvwnh7djm63a5fi3j3ndyfvmsrgv")))

(define-public crate-tyght-map-0.1 (crate (name "tyght-map") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1w6jwibp3ppwnwbgv4k29nzcmsbjwd8gmsjz94mx23pfmzlx9ifd")))

(define-public crate-tyght-map-0.1 (crate (name "tyght-map") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1vw8pnp14r68cbhrqp4zj34ibdl6wm6mmf8np9z6vmq389prv84h") (features (quote (("size-32" "size-16") ("size-16") ("defaults" "size-32"))))))

(define-public crate-tyght-map-0.1 (crate (name "tyght-map") (vers "0.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "11kjkm9mjq8al9wkpng4nf04zpam43bkzzdwg1dr9bsl3cb9bgay") (features (quote (("size-32" "size-16") ("size-16") ("default" "size-32"))))))

(define-public crate-tyght-map-0.1 (crate (name "tyght-map") (vers "0.1.1") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0dcm9q0qihqqbk3gf5wl7wf6d4n04lq4s5mkyw9qb8hks4sdx6is") (features (quote (("size-32" "size-16") ("size-16") ("default" "size-32"))))))

(define-public crate-tyght-map-0.2 (crate (name "tyght-map") (vers "0.2.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0866x98gg90f70a9q1ki0jdsla64y3ajkafh23f62faqm3mdk27c")))

