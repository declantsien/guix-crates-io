(define-module (crates-io ty oz) #:use-module (crates-io))

(define-public crate-tyozo-0.1 (crate (name "tyozo") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1lmxy2wi70vzxi21ib829rkrlg6xq9fxla7fyfwfd6lacfgbn3yl")))

