(define-module (crates-io ty py) #:use-module (crates-io))

(define-public crate-typycal-0.1 (crate (name "typycal") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1cy7myrmrx77iigd7v9h6vnqbvarh79lhzzjpdpvi1pczlsjd9w7")))

(define-public crate-typycal-0.2 (crate (name "typycal") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1sgbgm00g704j1l0p2dfa6j38djbslk4a09ihfwsmqcdl7zy6lg2")))

