(define-module (crates-io ty gr) #:use-module (crates-io))

(define-public crate-tygress-0.1 (crate (name "tygress") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "nix") (req "^0.23") (optional #t) (default-features #t) (kind 0)))) (hash "19w4gy5d6vb1mwpzm1nm071bb8kfnidh2cyc180z8d4wxk4i7vic") (features (quote (("std") ("overwrite" "bindgen") ("netdev" "std" "nix" "bindgen") ("inline_never") ("inline_always") ("default")))) (yanked #t)))

(define-public crate-tygress-0.1 (crate (name "tygress") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "nix") (req "^0.23") (optional #t) (default-features #t) (kind 0)))) (hash "18dz3d3skkhcf4w76iwjradxc6invpxhpy3czndyfyvaq9a2lkkm") (features (quote (("std") ("overwrite" "bindgen") ("netdev" "std" "nix" "bindgen") ("inline_never") ("inline_always") ("default")))) (yanked #t)))

