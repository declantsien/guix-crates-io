(define-module (crates-io ty fe) #:use-module (crates-io))

(define-public crate-tyfers-0.1 (crate (name "tyfers") (vers "0.1.0") (hash "1vb1qrgblkll9880da6iwqmx0wly2y9gigw1dk31syhf0zpqy2iy")))

(define-public crate-tyfers-0.2 (crate (name "tyfers") (vers "0.2.0") (hash "0gq8bhxl94fjhvf6qqa9m0r47nwiqrhvh690xv58yxakc7rfplv7")))

