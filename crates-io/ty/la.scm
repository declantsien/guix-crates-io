(define-module (crates-io ty la) #:use-module (crates-io))

(define-public crate-tylar-0.1 (crate (name "tylar") (vers "0.1.0") (hash "0bppxq631hi63gf8nfar5wdb9a6f7imd071rmnwrd2cxcalr13sk")))

(define-public crate-tylar-0.1 (crate (name "tylar") (vers "0.1.1") (hash "1w1b0jcdn0vkz2wa6wkjk81larly3xxk7i1mn8g7cmmn7r12ggh4") (features (quote (("nightly"))))))

(define-public crate-tylar-0.1 (crate (name "tylar") (vers "0.1.2") (hash "0q0i624hpxpjvwa84llanm9nkbxjk1yv10q0c13k44w9z1gq7g1a") (features (quote (("nightly"))))))

(define-public crate-tylar-0.2 (crate (name "tylar") (vers "0.2.0") (hash "13n1s8pydqzik7nrhxvr6npj9lmlmwa4lzplbbmz8a4awyvridrs") (features (quote (("nightly"))))))

(define-public crate-tylar-0.2 (crate (name "tylar") (vers "0.2.1") (hash "08jnf3g2dgdswr7awdv5sp1iq0nck5z918k4mf60q9hzs2kr5c87") (features (quote (("nightly"))))))

(define-public crate-tylar-0.2 (crate (name "tylar") (vers "0.2.2") (hash "172cybq1vabwln1q18n72ch1d45jlimhmg9wmnhwdhx6diw6cmqh") (features (quote (("nightly"))))))

