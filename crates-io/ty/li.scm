(define-module (crates-io ty li) #:use-module (crates-io))

(define-public crate-tylift-0.1 (crate (name "tylift") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (default-features #t) (kind 0)))) (hash "0iqvnrsk6irqn50rvma2gr4ppz2yws3vi3x8kavfz6447vah5bd5")))

(define-public crate-tylift-0.2 (crate (name "tylift") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (default-features #t) (kind 0)))) (hash "1gcan1y2phx8m7krc7x9aglgc2wskgkylynjb124gwlcb73xyg4i")))

(define-public crate-tylift-0.3 (crate (name "tylift") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fclm71gf8m3nysmvpsp7mf4776ljcaa2hnvmk8w2as9g83n3z2l") (features (quote (("span_errors"))))))

(define-public crate-tylift-0.3 (crate (name "tylift") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i0pz2g4595xzl1ai4m7gqd5bxzbp8mrla56wiwfcz6j706llvg3") (features (quote (("span_errors"))))))

(define-public crate-tylift-0.3 (crate (name "tylift") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h2zcyck49pp1mkrzwd30vnrjmf294l6v2in02imgdgfs779qh1i") (features (quote (("span_errors"))))))

(define-public crate-tylift-0.3 (crate (name "tylift") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.35") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0b5sqwvl9r0r6ickljnf619gxcsx13bnmdds6fgv571v48d4ankp") (features (quote (("span_errors"))))))

(define-public crate-tylift-0.3 (crate (name "tylift") (vers "0.3.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.35") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1akcckx7h89zp3h6iqny9gfn5jgqkb7q12yvfspabdmxisidn7pv") (features (quote (("span_errors"))))))

(define-public crate-tylift-0.3 (crate (name "tylift") (vers "0.3.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.63") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1697qsrglfayznchvi17sjg27wqq4yp4yb3bxg8qjm8kbl0pk0lv") (features (quote (("span_errors"))))))

(define-public crate-tylisp-0.1 (crate (name "tylisp") (vers "0.1.0") (deps (list (crate-dep (name "frunk") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "typenum-uuid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n93w0kyvg0wgyna1w80lw0d1chqmn5nxqrlmms8gcb42kpy8k7x") (features (quote (("const"))))))

