(define-module (crates-io ty me) #:use-module (crates-io))

(define-public crate-tyme4rs-1 (crate (name "tyme4rs") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1vf0vhkby9hgdri2n3iaqnhi0h2m53mx7clil6yn2lq5bvpgbkcr")))

(define-public crate-tyme4rs-1 (crate (name "tyme4rs") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "0s2g7haplw6afwk69ai299r64sbpzxlw86gplb5gy8n58n3in5wv")))

(define-public crate-tyme4rs-1 (crate (name "tyme4rs") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1x71l1z7vma5fpxldxhnkzjbsrbv8lsqwxccvia6lh1fsla9z633")))

