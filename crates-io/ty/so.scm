(define-module (crates-io ty so) #:use-module (crates-io))

(define-public crate-tyson-0.1 (crate (name "tyson") (vers "0.1.0") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "13hinskan4bx71ajp991nima4kyd3mgy8c0l1kjw9vn2rcq8wqn6")))

