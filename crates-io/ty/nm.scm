(define-module (crates-io ty nm) #:use-module (crates-io))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "0cv0qhx8r0vclg6v4iarbzs6ywbnb8hsrlfpwrzviym7m0zknjgb")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "0vm04jzx4p46lgkcvvy9w8di0v098y792davz5lcrmkff6vxdafh")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "0v2m3hi09va5mk2gqnnxjjnpczxv5k0j9dsc4c08mbdjxxq6hrjj")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "0a6yg0z3pm7slyilj8xzrqkdirgrb5cjxv2qhyiax85i6pzxi1sy")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^5.1.1") (features (quote ("std"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "0knk747j6hl2fw2i008z135sxjki315waqik1alv8q9rjs0vfzrn")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.5") (deps (list (crate-dep (name "nom") (req "^5.1.1") (features (quote ("std"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "02pawkxxf0ibh3nyrl62d4ll2s7gvvrxx4dhn7jh9b855bdv2c8m")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.6") (deps (list (crate-dep (name "nom") (req "^5.1.1") (features (quote ("std"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "0gb2a06qbcls9j1ynymafnbj5as0rps83a8vckvx3hy95nm2rpx4")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.7") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1cpk4q81rspn31mgr041rn4v9qvhk87xsaxrr2ah8k393vgfqnfd")))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.8") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0i9n1012dcr5hpn4gh1026bkvcr2fqcgac4f0dykq7cnhpy3y6qb") (features (quote (("default"))))))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.9") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0s90k7fwwlkwlr8pxai3jwvwi7ws86185rnxsjalcap4d4g4826c") (features (quote (("default"))))))

(define-public crate-tynm-0.1 (crate (name "tynm") (vers "0.1.10") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.31") (default-features #t) (kind 2)))) (hash "1vc12w4b1imk2gbbiy9cpk785v6g14s8ayiyzq9qwiyid5gd0c5x") (features (quote (("info") ("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

