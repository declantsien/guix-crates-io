(define-module (crates-io ty -l) #:use-module (crates-io))

(define-public crate-ty-lib-0.1 (crate (name "ty-lib") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fi7a4qfnp3bz29r6ib3f2id7qkbszix0qqcv729slg5jjcw695q")))

(define-public crate-ty-lib-0.2 (crate (name "ty-lib") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "validator") (req "^0.12") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ny0c7p5mdhrabzv3k3gizy4jfn0dirg926ywsfmpm87x517nwl5")))

