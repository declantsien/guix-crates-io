(define-module (crates-io ty co) #:use-module (crates-io))

(define-public crate-tyco-0.0.9 (crate (name "tyco") (vers "0.0.9") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tower") (req "^0.4") (default-features #t) (kind 2)))) (hash "011vm0kmdglipkvs4pyx4zgffxdaliygf70d8wbz4iga69c3fzyw")))

(define-public crate-tycoon-0.1 (crate (name "tycoon") (vers "0.1.0") (hash "0an7nfirkdvf6magycv20hm4xw76lh8209nzar7zwvpg9kj3xd34")))

