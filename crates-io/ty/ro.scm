(define-module (crates-io ty ro) #:use-module (crates-io))

(define-public crate-tyrosine-0.0.1 (crate (name "tyrosine") (vers "0.0.1") (deps (list (crate-dep (name "tyrosine-common") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1lwp05m3901cyd4v26hv632gp8wx7szcpmdsyz7xr807zin3y99i")))

(define-public crate-tyrosine-common-0.0.1 (crate (name "tyrosine-common") (vers "0.0.1") (deps (list (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sorted-vec") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0jzwc1yr73kbisxx8n4lasfa1f4h8ndldiw4ajhvf7ivb69y5vff")))

