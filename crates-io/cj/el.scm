(define-module (crates-io cj el) #:use-module (crates-io))

(define-public crate-cjel-be-0.0.0 (crate (name "cjel-be") (vers "0.0.0") (hash "1q5k01m5lbxp4hvy6gq79l0xrmh5y670x6ygxm1dirq311qz08lw") (yanked #t)))

(define-public crate-cjel-ir-0.0.0 (crate (name "cjel-ir") (vers "0.0.0") (hash "16hw2isspvh8gw1wrcivdcx54238d5q0m220r3aacrqab3zvjgcc") (yanked #t)))

(define-public crate-cjel-rt-0.0.0 (crate (name "cjel-rt") (vers "0.0.0") (hash "0qi1j0n93iyd8452wzhispgws22sn6n7mxckqx93n2f2z24agzka") (yanked #t)))

