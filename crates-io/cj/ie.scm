(define-module (crates-io cj ie) #:use-module (crates-io))

(define-public crate-cjieba-sys-0.1 (crate (name "cjieba-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1jdhmgpsfmpxg37jck6y7x9dbvjky9vy27l5s4vy88xssgwnzk6p")))

(define-public crate-cjieba-sys-0.1 (crate (name "cjieba-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "084zi2qa6l04ivjdlph31sd74n040lwmzmgyg266cc2sxxqvwz9s")))

