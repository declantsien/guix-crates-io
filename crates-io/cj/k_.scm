(define-module (crates-io cj k_) #:use-module (crates-io))

(define-public crate-cjk_entity_extractor-0.0.1 (crate (name "cjk_entity_extractor") (vers "0.0.1") (hash "1y8cfzx849gpq5x7fms2p357cz5hyljvdn8jxmysir11yr8pnp3n") (yanked #t)))

(define-public crate-cjk_entity_extractor-0.0.2 (crate (name "cjk_entity_extractor") (vers "0.0.2") (deps (list (crate-dep (name "cjk") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vc9caqgk4zbzdlvr0ahjcms0m6na214dz7ymd0szg9zsr2a7bpj") (yanked #t)))

