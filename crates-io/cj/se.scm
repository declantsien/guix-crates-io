(define-module (crates-io cj se) #:use-module (crates-io))

(define-public crate-cjseq-0.1 (crate (name "cjseq") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "4.4.*") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04rdg2pai9vmwkxnzmm2yjcc6kl3yirmd7r3hcsyxfr8f3vx4994")))

(define-public crate-cjseq-0.2 (crate (name "cjseq") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "4.4.*") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "122bh0nnsywg8lx910i9r7z8bjd0b3wcm88x6rm42rl5w1mzh53d")))

(define-public crate-cjseq-0.3 (crate (name "cjseq") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bb91a88ymslq9w4cdff41n0z5nwarv8pkrh243n1y4kr7gm97hw")))

