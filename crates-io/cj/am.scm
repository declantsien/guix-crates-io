(define-module (crates-io cj am) #:use-module (crates-io))

(define-public crate-cjam-0.1 (crate (name "cjam") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17nwmn1lfjcw6vl618cj35pcsw89klkkpswllf3kjflkn3nwzkrk")))

(define-public crate-cjam-0.1 (crate (name "cjam") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0zwxv4bbnl6x6jcv9xvib76bpnnpka6ss3dp285j3zwahhbd8s0y")))

(define-public crate-cjam-0.1 (crate (name "cjam") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1c9wm0a9hx91cwa0vr4l78bpylxdssii4x8cnxxhx1d9icgmmbn3")))

(define-public crate-cjam-0.1 (crate (name "cjam") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "05m20c7qslgpd1dvhq373kj2h641zm41g1fh0c497pk778lqw556")))

