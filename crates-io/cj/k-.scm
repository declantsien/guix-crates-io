(define-module (crates-io cj k-) #:use-module (crates-io))

(define-public crate-cjk-align-0.1 (crate (name "cjk-align") (vers "0.1.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "056ig26ani91ysvinb0jxrk8aqlfp1qzwmv4954d3y4i1rnkpr2i")))

