(define-module (crates-io cj dn) #:use-module (crates-io))

(define-public crate-cjdns-0.1 (crate (name "cjdns") (vers "0.1.0") (deps (list (crate-dep (name "bencode") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fj67zjcj64lp7adqdf7w6xi4xi2frmvqagldsy3l9zi8d15d18w")))

(define-public crate-cjdns-0.2 (crate (name "cjdns") (vers "0.2.0") (deps (list (crate-dep (name "bencode") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wd4ji6ikrbj1hhsf7mvp0j9m5jn95qmjzamyysrpvjnsp9nydz2")))

