(define-module (crates-io a0 kz) #:use-module (crates-io))

(define-public crate-a0kzg-0.1 (crate (name "a0kzg") (vers "0.1.0") (deps (list (crate-dep (name "bls12_381") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0zy25c6f7c41iapxpd05hmf356hcgr15fxxr62n914lvh7dwwkw8") (yanked #t)))

(define-public crate-a0kzg-0.1 (crate (name "a0kzg") (vers "0.1.1") (deps (list (crate-dep (name "bls12_381") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1j71q198jjvwmcrx77rb57v32cgrg7bax5i8c6r7pc725qaq8nhq") (yanked #t)))

