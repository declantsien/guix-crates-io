(define-module (crates-io px p-) #:use-module (crates-io))

(define-public crate-pxp-parser-0.0.0 (crate (name "pxp-parser") (vers "0.0.0-b4") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.149") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "0wi29zzlzgzz02ylya9qcjnm8p530nimrz0nk8n7hqgigii8f1z8")))

