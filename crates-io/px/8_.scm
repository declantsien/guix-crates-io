(define-module (crates-io px #{8_}#) #:use-module (crates-io))

(define-public crate-px8_plugin_lua-0.0.11 (crate (name "px8_plugin_lua") (vers "0.0.11") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "1vwbv3l7syqiihmm2maymzbg1cr33c9wmizhg28a34vz6nfvf459")))

