(define-module (crates-io px #{2r}#) #:use-module (crates-io))

(define-public crate-px2rem-0.1 (crate (name "px2rem") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0j17vi4xd6mshfs1yb7fx3fi3j92yjgdb5hmgnh2a9rd9db733mh")))

