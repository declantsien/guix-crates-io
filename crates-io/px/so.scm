(define-module (crates-io px so) #:use-module (crates-io))

(define-public crate-pxsort-0.1 (crate (name "pxsort") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1p53nip3ad23i50v4jxvvhf5lxg7cxjbyggsajgbfms7h1h5lldq")))

(define-public crate-pxsort-0.2 (crate (name "pxsort") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0g4jdp26rvfzz316lbg1df94l1rr1r7rxnl5wynlwpg71ybsadpn")))

(define-public crate-pxsort-0.2 (crate (name "pxsort") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "17m9xmzr3c5n48ic4kn83i02h011wx57gdbg6f1bw71y2la5vgfv")))

(define-public crate-pxsort-0.3 (crate (name "pxsort") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "01kycyc6d3glh5njwjkzjj8vva99vzyz4jmk1fhzadz4ylk6xcg5")))

(define-public crate-pxsort-0.3 (crate (name "pxsort") (vers "0.3.1") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0di98ivfk72cplwalxpal5d37jpppw0vvzwzb1cwq7cx85s4q8r1")))

(define-public crate-pxsort-0.3 (crate (name "pxsort") (vers "0.3.2") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "05kxhvvi31paf53qy6x25ad7kay90674jxyf3gigc5sllkajnajg")))

(define-public crate-pxsort-0.3 (crate (name "pxsort") (vers "0.3.3") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1q8ddhpwqac23gwnl72s3z87hqimzbg2znk5kq7yxnlha6f545p0")))

(define-public crate-pxsort-0.4 (crate (name "pxsort") (vers "0.4.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0s6xy6ch07nzgrhs9zkp00hcqp7bfcsmpgz9i9qllaljf1vcri6z")))

(define-public crate-pxsort-0.4 (crate (name "pxsort") (vers "0.4.1") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1akhxmcvr2avgd1zzg6fyin4995c9l8f6gzxm361si2rgw2nx6fq")))

(define-public crate-pxsort-0.4 (crate (name "pxsort") (vers "0.4.2") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0xliaiws1nf62w5jylka7hpahwcsnpk9r3p3i7hqhmkvnslbkmly")))

(define-public crate-pxsort-0.5 (crate (name "pxsort") (vers "0.5.0") (deps (list (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "118n5z2b5gp9369zk530fd3469ngk94acnw7gwaqwmwl2s6cvndk")))

