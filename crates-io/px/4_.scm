(define-module (crates-io px #{4_}#) #:use-module (crates-io))

(define-public crate-px4_macros-0.1 (crate (name "px4_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zyimcfyqb2196xckqw7dnkyw75s1jjs763vjsdal4cicqha4cph")))

(define-public crate-px4_macros-0.2 (crate (name "px4_macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0khx0q50xidd3n726imimsxp9xq8mi8s7sbiaim6p1y5505kj007")))

(define-public crate-px4_macros-0.2 (crate (name "px4_macros") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02yv5snijndcs1fdm1rz4w9x42jggw82fmnj2blnk0iybr2byz70")))

