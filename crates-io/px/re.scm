(define-module (crates-io px re) #:use-module (crates-io))

(define-public crate-pxrem-0.1 (crate (name "pxrem") (vers "0.1.0") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "12i8dbq6i07fwmibs1glvshz1k4pl1ad112885a2ibxm5v4rnmcl")))

(define-public crate-pxrem-0.2 (crate (name "pxrem") (vers "0.2.0") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0wny65l0bjvwzab7rkrhmiqnd4bxvd1721ysxq6l53rr9dagkdgm")))

(define-public crate-pxrem-0.2 (crate (name "pxrem") (vers "0.2.1") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "02pizhyal3si1wg0czw99snpmxb30yg45yv302bpm4spdwpkdrby")))

(define-public crate-pxrem-0.2 (crate (name "pxrem") (vers "0.2.2") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0fy5cvmhnqy1d7xsbwlsy23rk8yhcnn100c9z14cg44q2269z02b")))

(define-public crate-pxrem-0.3 (crate (name "pxrem") (vers "0.3.2") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "18a4yz9flr135i1pn11b9zkwdd9pyp8ifb5himhjqz3w0bvpg2mh")))

(define-public crate-pxrem-0.3 (crate (name "pxrem") (vers "0.3.3") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0x98az03l0yifnrmszjnzm64h4cjphdgjz5wc7218pmm7g3lxa15")))

