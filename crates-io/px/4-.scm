(define-module (crates-io px #{4-}#) #:use-module (crates-io))

(define-public crate-px4-ecl-0.1 (crate (name "px4-ecl") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "px4-ecl-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0iwcjjirad1b27md7fa6nnfhf1qz3kyzwmsyrai9l1h8pbxbgn1k")))

(define-public crate-px4-ecl-sys-0.1 (crate (name "px4-ecl-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "cpp-vwrap-gen") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "1fzck84xcb85wm1y7p6qh6i3w3l3l2vkqf2gp41w091ypwp78n9r") (links "px4-ecl")))

(define-public crate-px4-ulog-0.1 (crate (name "px4-ulog") (vers "0.1.0") (hash "0820g166wzmp0racqr0mi08zcssxg3mrc6fa92halg5q1i4vmf92")))

(define-public crate-px4-ulog-0.1 (crate (name "px4-ulog") (vers "0.1.1") (hash "00wkddm0f1sly7h71l4534yh25v1c7bfs0dzp39va2dxld0g5mjg")))

