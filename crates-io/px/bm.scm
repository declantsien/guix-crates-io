(define-module (crates-io px bm) #:use-module (crates-io))

(define-public crate-pxbm-0.1 (crate (name "pxbm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "parse-display") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0573lvjyr3xy8pg1hws4vjc109mhgmiw98g9vzdmd8wpjznq19a0")))

