(define-module (crates-io x2 #{64}#) #:use-module (crates-io))

(define-public crate-x264-0.1 (crate (name "x264") (vers "0.1.0") (deps (list (crate-dep (name "x264-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cmys8agmpmxaldgqy9sjrp6x9dq7m6yj8dmda8gnar0icvwdjyk")))

(define-public crate-x264-0.1 (crate (name "x264") (vers "0.1.1") (deps (list (crate-dep (name "x264-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zfpds7dxsp81h6mpc6nip3lm0vb7kv3xlhpff6zrr9a9ikpcs0i")))

(define-public crate-x264-0.2 (crate (name "x264") (vers "0.2.0") (deps (list (crate-dep (name "x264-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vz68z0l2fm925wcsdasf037vnvgahnghi9dbj7z4h0glnmnbclp")))

(define-public crate-x264-0.2 (crate (name "x264") (vers "0.2.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "x264-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zvm551p9vbx1r3xy5myy0hbascy7lwly463h8zmwwydmd61jcps")))

(define-public crate-x264-0.2 (crate (name "x264") (vers "0.2.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "x264-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pk9pli3h7hj2c1bggbxd0idnzvzi5kq6ih3s7fncc83hb74d8v2")))

(define-public crate-x264-0.3 (crate (name "x264") (vers "0.3.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "x264-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1q5rxks6vfrizkj729agsv45m03rxj4lg8k4yvcpsm2q9csrlrc9")))

(define-public crate-x264-0.4 (crate (name "x264") (vers "0.4.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "x264-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0br149fxs2j31shczpjk58x2r0h3vzw560659x5j6yfsk5cmvbm8")))

(define-public crate-x264-0.5 (crate (name "x264") (vers "0.5.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "x264-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "14pc1pnhcrhy17qwzgwrda5ffzz1j6r3zdq2bz8cxgnfb39gz4xw")))

(define-public crate-x264-dev-0.1 (crate (name "x264-dev") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0.12") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.26") (default-features #t) (kind 1)))) (hash "1ds041kliyi6xm1r9ka4a9h1g99h8m7b7di1akw8niwkw328dyak")))

(define-public crate-x264-dev-0.1 (crate (name "x264-dev") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0.12") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.26") (default-features #t) (kind 1)))) (hash "08am104hlpdf76s1qww23mfns9azm5qdkc33npr7r1avda6mv0n4")))

(define-public crate-x264-dev-0.2 (crate (name "x264-dev") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0.12") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.11.1") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4.26") (default-features #t) (kind 1)))) (hash "0jqjrgavhilwzcv18vh2g98hpyx7lb8yb495ypc64k6cs8bx32g8") (links "x264")))

(define-public crate-x264-framing-0.1 (crate (name "x264-framing") (vers "0.1.0") (deps (list (crate-dep (name "framing") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "x264-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "06zlzfjcvq15f2153rdajjxx13riyj3wxynabcrgf2nwijzcbk0n")))

(define-public crate-x264-next-0.4 (crate (name "x264-next") (vers "0.4.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "x264-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bcgh9bx295aib13y6yjj3fyc97x97gwqsl7c4znkyhhjsa7xfil")))

(define-public crate-x264-next-0.4 (crate (name "x264-next") (vers "0.4.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "x264-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ak0329b079aw1kzpzy3hsf2arnj9gm2q5gxwzahibir65j9gpwx")))

(define-public crate-x264-sys-0.1 (crate (name "x264-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.23.1") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1") (default-features #t) (kind 1)))) (hash "1fnr0r6hy0yy7gd82zzn09vgk9x1cn9skch0d4cqpim5rf9iz40m")))

(define-public crate-x264-sys-0.2 (crate (name "x264-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1") (default-features #t) (kind 1)))) (hash "1v3kndi94sw9c6zjfwrfzfww4nm4zrlfnc13lcb3468y232zkn2y")))

(define-public crate-x264-sys-0.2 (crate (name "x264-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.1.0") (default-features #t) (kind 1)))) (hash "0pj8qqyp59cb85imsdyh934csd5zxp2s1j15j7p2rwqx9pnzaxqj")))

