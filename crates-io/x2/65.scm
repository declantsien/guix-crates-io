(define-module (crates-io x2 #{65}#) #:use-module (crates-io))

(define-public crate-x265-0.1 (crate (name "x265") (vers "0.1.0") (deps (list (crate-dep (name "x265-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g4px4bbjn6vf9m062xadrpnky3w5swch61clhxndmmwpw86r7kh") (yanked #t)))

(define-public crate-x265-0.1 (crate (name "x265") (vers "0.1.1") (deps (list (crate-dep (name "x265-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0whgyr62ji2ihrfxbhw6sjki8cm7hlwjyn9wg5b4gjyanrl59abp") (yanked #t)))

(define-public crate-x265-sys-0.1 (crate (name "x265-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.0") (default-features #t) (kind 1)))) (hash "178kvbcx06hzi4xxhf55cacr0q4kwc1sj6lr4bqvfsmzqy2b69m3")))

