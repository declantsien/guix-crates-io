(define-module (crates-io uk ey) #:use-module (crates-io))

(define-public crate-ukeygen-0.1 (crate (name "ukeygen") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "17r5hw8673d97p11vnjkdknfv3mqc3hj3n4j7y3h25v6k9bfc56p")))

(define-public crate-ukeygen-0.1 (crate (name "ukeygen") (vers "0.1.2") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0x2j8av5fy7xk4d5zcw4v2hxrhmxlpsyr5ymbry4xbg8ln42wa9x")))

