(define-module (crates-io uk al) #:use-module (crates-io))

(define-public crate-ukalman-0.1 (crate (name "ukalman") (vers "0.1.0") (deps (list (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "19l04nhgn0ks1ap680k9mfaj0bzkcwspzsn97vq8a43i9d4yd9s6")))

