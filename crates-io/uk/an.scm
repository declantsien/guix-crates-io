(define-module (crates-io uk an) #:use-module (crates-io))

(define-public crate-ukanren-0.0.1 (crate (name "ukanren") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rpds") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1zz0m9r79vciyjawakshdgfww0c0m9gyfnps9bfj5hj5klb3bxvc")))

(define-public crate-ukanren-0.0.2 (crate (name "ukanren") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rpds") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "take_mut") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1mlb46x73qdmc87a4jy3lgif7w43hwfc4sl3dykcymv4m6j0vgl3")))

(define-public crate-ukanren-0.0.3 (crate (name "ukanren") (vers "0.0.3") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rpds") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "take_mut") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1v0yl5q095fs2vaylbpgwsmpb9ks5ii5nnskh44mgdb2vi7y3wdx")))

(define-public crate-ukanren-0.0.4 (crate (name "ukanren") (vers "0.0.4") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rpds") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "take_mut") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1ffz5h1qva6hfd3x62ndh8afprnzcim6x031llzn57fwfrapjlps")))

(define-public crate-ukanren-0.0.5 (crate (name "ukanren") (vers "0.0.5") (deps (list (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "rpds") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "take_mut") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1yqdnbs0fxyvs527q7mqym8a1g5c3wjn3nqjbq6xv3jf4i41wlxd")))

