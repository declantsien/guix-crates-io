(define-module (crates-io uk ra) #:use-module (crates-io))

(define-public crate-ukraine-1 (crate (name "ukraine") (vers "1.0.0") (hash "0ng9h7kb87rfgfvsv8z2s6alm1r1309bpvcn6dzbx0h1qq7z6l67")))

(define-public crate-ukraine-1 (crate (name "ukraine") (vers "1.0.1") (hash "1xwy05pw281przmq3kydc74j0njsfzn5jhrwkcl4i2yibmy7dmiz")))

(define-public crate-ukraine-1 (crate (name "ukraine") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1wvk9rl0bxl46w2kd30b98618n3f3ci978xfg9j2bgppjyg6q4mi") (features (quote (("lozynsky") ("default" "lozynsky"))))))

