(define-module (crates-io y8 #{6-}#) #:use-module (crates-io))

(define-public crate-y86-assembler-0.1 (crate (name "y86-assembler") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hxgqizyfgavavni2raysjq8d3r2k85mz6az7ivn3i2fkn6k8c16")))

(define-public crate-y86-assembler-0.2 (crate (name "y86-assembler") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j15yr4x0870f1knn7gv3azckdgy4izkxjhb7cvapi5n6xlc1dxh")))

(define-public crate-y86-assembler-0.3 (crate (name "y86-assembler") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "191fsp8yf0n6anz5fhm0z31zmni96d1cmhbymm30wa40b2z598w0")))

(define-public crate-y86-lib-0.1 (crate (name "y86-lib") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b4dy1gp7vmd2za9wpfnfg838n6mlzpgm5mi5wvsjxcvjrlw71mw")))

