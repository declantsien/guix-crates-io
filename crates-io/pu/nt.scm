(define-module (crates-io pu nt) #:use-module (crates-io))

(define-public crate-punt-0.1 (crate (name "punt") (vers "0.1.0") (deps (list (crate-dep (name "crc-any") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "05aqv58c2hrd8y4svddc0gdfj96bn0ksqrrn6wjkz22d3qskq5pb")))

(define-public crate-punt-0.2 (crate (name "punt") (vers "0.2.0") (deps (list (crate-dep (name "crc-any") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "16d7qrmbsc3jrhwnmj50kc5wfw3q7j9j6y24xkkk498cwsjzyr9v")))

(define-public crate-punt-0.2 (crate (name "punt") (vers "0.2.1") (deps (list (crate-dep (name "crc-any") (req "^2.4.3") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "18fcg2azca5yrdr2bd6gr6ag1fagrsmbdq9vdcrlymi3as8lpchv")))

(define-public crate-punt-0.3 (crate (name "punt") (vers "0.3.0") (deps (list (crate-dep (name "crc-any") (req "^2.4.3") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "19fzx745gzgxd7n1h0cnwwrp6wakvv1g5dqpy3s6dd9vjg04djhh")))

