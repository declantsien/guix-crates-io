(define-module (crates-io pu b_) #:use-module (crates-io))

(define-public crate-pub_this-0.1 (crate (name "pub_this") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1zns7argy1bf6hpss2z3k7iil92cbc3nyyww964g9z6w7jsyxp16")))

