(define-module (crates-io pu cc) #:use-module (crates-io))

(define-public crate-puccinier-0.1 (crate (name "puccinier") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "1xj087ykr74z1jhw3rjx12zn102vgcjpcklzzr37qakh9rgwp1im")))

(define-public crate-puccinier-1 (crate (name "puccinier") (vers "1.0.2") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (features (quote ("std"))) (kind 0)) (crate-dep (name "xflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zy95klia5f5y4smhak16crm6v3axzzibzdny4xmdqzsm8vwk52w")))

(define-public crate-puccinier-1 (crate (name "puccinier") (vers "1.0.3") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (features (quote ("std"))) (kind 0)) (crate-dep (name "xflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "0b226h71c0ms8rq50rmnzs06bp8gmpcx7vs0qg720jza0aadwqv7")))

(define-public crate-puccinier-1 (crate (name "puccinier") (vers "1.0.4") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (features (quote ("std"))) (kind 0)) (crate-dep (name "xflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "16phacl6p106rwnbbdhh7j0mx26my08fx39hyal08zb75xc21npv")))

(define-public crate-puccinier-1 (crate (name "puccinier") (vers "1.0.5+deprecated") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (features (quote ("std"))) (kind 0)) (crate-dep (name "xflags") (req "^0.3") (default-features #t) (kind 0)))) (hash "05jr8a6wvcq0vd6hmfdwamqrvcrz04y3z02vs11ds3wx2jvi231w")))

