(define-module (crates-io pu rg) #:use-module (crates-io))

(define-public crate-purgatory-0.1 (crate (name "purgatory") (vers "0.1.0") (hash "167q34k4sf62wbnyldzqlrqrv4yx1hv8q1zhw16g6dgsf9818qkx")))

(define-public crate-purge-0.0.0 (crate (name "purge") (vers "0.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "env" "unicode" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gsjdrxyjrxqw6za8qagifnsfhpg8lwnq3zm9ssk5aqi5xz5cpdf")))

(define-public crate-purgo-0.1 (crate (name "purgo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colorized") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "size") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "00h82lp29rwqr12rw7vg504qvi9gfaalch5rk718wkn18njkzgqp")))

