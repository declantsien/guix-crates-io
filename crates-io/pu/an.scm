(define-module (crates-io pu an) #:use-module (crates-io))

(define-public crate-puan-pv-0.1 (crate (name "puan-pv") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "05ica96hl88v7227xgz34j23f2x8w78xy2yjpkyzbi5qwnjmw6kf")))

(define-public crate-puan-pv-0.1 (crate (name "puan-pv") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "0n3v96cvvjqfm5c02ngdk34fll5vi3v18s6ngjpv83hzxzkk12jn")))

(define-public crate-puan-pv-0.1 (crate (name "puan-pv") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "1f78kgailng2cn8099hsrlsgvicjqa69xm89qsknk0a22prsjxl3")))

(define-public crate-puan-pv-0.1 (crate (name "puan-pv") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "02vag7ywgdq79v0pl8grccpz99y3bbc8bwqn0zzyqj72sr4djzb0")))

(define-public crate-puan-pv-0.1 (crate (name "puan-pv") (vers "0.1.4") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1n7rw8ml5lyxkchw72cgl7lc042krz5y2jgl804avlgianydkrpj")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "025c4wg7lbmr3dv8ynjgaq5ip7yq16wj5x5rg58nci8fgfqf4jcg") (yanked #t)))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "09dkq89k7r3gj1035l997f2lnidxz19lg019hvz8ffgark8myg9z")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "0gwnwfss596hxrpj00amifqc2vxzq5z5q9py7m7h437nwj2xv4zv")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "10x9fwkbgygygapdpyf70ck5lf4wr3bc8z5fhv2sfdfwci3nahfw")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.4") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "1qamacz9mvcm0x9qh5xnphych4ialm1sq71f841mis6k55plj2ii")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.5") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "077qy2mwsx8z1mqn0jas8xca3yfm9drhb9flfy0y6a7b8nqgdgbz")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.6") (deps (list (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "0j5548kqdqfpfpwh2r861gvwhk0yc66k5fx8hfyh8xzjxnkbjyia")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "1cyppx6sqnj8kb782k0sdjyhjkzhbvp4dhhd5zr0akl92ncdvljn")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "07znpcl2n2cnpyfd9yv3rnfam2nsksb4cq7fr96pq0p7gj5bl3z9")))

(define-public crate-puan-rust-0.1 (crate (name "puan-rust") (vers "0.1.9") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.4") (default-features #t) (kind 0)))) (hash "05zj80v1q0g20dzjmfrmfl7lzqdsl0pyb8ph885kzpzqrib4vqxv")))

