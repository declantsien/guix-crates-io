(define-module (crates-io pu ck) #:use-module (crates-io))

(define-public crate-puck-0.1 (crate (name "puck") (vers "0.1.0") (hash "1sprh7vwp463aa64x9wqad78k485diah07x7vh6bsvgzwfcgz5yd")))

(define-public crate-puck_codegen-0.1 (crate (name "puck_codegen") (vers "0.1.0") (hash "08hqzh5ra0ykd0pw1kzr012nbj6r03df1rvrzn944ns1pfm7mwvw")))

