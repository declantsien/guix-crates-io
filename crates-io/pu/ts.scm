(define-module (crates-io pu ts) #:use-module (crates-io))

(define-public crate-puts-0.1 (crate (name "puts") (vers "0.1.0") (hash "0jswizca92armbmdm3hnnq6j8b7siqjs790lfhdj64f8ynq5rxmg")))

(define-public crate-puts-0.1 (crate (name "puts") (vers "0.1.1") (hash "145a84wi0znygxjm6qppn3k7hbnc0h47pqdcm5rn87k6sjfqlzfw")))

(define-public crate-puts-0.1 (crate (name "puts") (vers "0.1.2") (hash "1wh27wxmq4z408xwbdskx9vhzpqvpzagrlf5s3shbjw6qbxa2zc8")))

