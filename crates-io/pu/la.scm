(define-module (crates-io pu la) #:use-module (crates-io))

(define-public crate-pulau-rs-0.1 (crate (name "pulau-rs") (vers "0.1.0") (hash "1y5j5mxg53fvbddnr6lab7ic4f6k97rn2xqb3vk3xy2rvjh406k8")))

(define-public crate-pulau-rs-0.2 (crate (name "pulau-rs") (vers "0.2.0") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 2)))) (hash "0rzd3ybiv422d81py8xa53x7xbbxnl5q86kz3w0fayjicmlazw2f") (features (quote (("traits") ("default" "traits"))))))

