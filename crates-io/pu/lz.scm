(define-module (crates-io pu lz) #:use-module (crates-io))

(define-public crate-pulz-0.0.1 (crate (name "pulz") (vers "0.0.1") (deps (list (crate-dep (name "pulz-ecs") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "pulz-graph") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "pulz-render") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0xfyaq77cjj4rncih7m70m0pnk9c7jnaxprrhzxfaqid47adbavx")))

(define-public crate-pulz-arena-0.0.1 (crate (name "pulz-arena") (vers "0.0.1") (hash "04ifabqwkji1hq3w5510f2ykblnha675zg8xfpdh5mv6jmywh62y")))

(define-public crate-pulz-arena-0.1 (crate (name "pulz-arena") (vers "0.1.0") (hash "1j7l8xvxl3lw2w8hsx6prp52b0r6hlpxay0npl0sfab18j6xsh88")))

(define-public crate-pulz-arena-0.1 (crate (name "pulz-arena") (vers "0.1.1") (hash "18izhk1syv2vhj9wis4h48vc7x2lij8vmhv0ji4bxbcfv9zdpwwf")))

(define-public crate-pulz-arena-0.2 (crate (name "pulz-arena") (vers "0.2.0") (hash "0vknwpsi5248idi61n0bz41dg5dz5qbcw1gr2zkqg1ag5av1h3p5")))

(define-public crate-pulz-arena-0.3 (crate (name "pulz-arena") (vers "0.3.0") (hash "1bi9wjyp89ckff1mjwic36z8rfwmbs7r3wh0khix2m58jk9jwhv0")))

(define-public crate-pulz-arena-0.4 (crate (name "pulz-arena") (vers "0.4.0") (hash "0ibfkxvkbhmkxlll2kp0ispcy8038a8rvr755dgz1bx04aicmrk7")))

(define-public crate-pulz-assets-0.0.1 (crate (name "pulz-assets") (vers "0.0.1-alpha") (hash "1i6cpg88898dmh4x9g418hmdp7kpxkmyhd2hwv4m5ffrw0l3rjlp")))

(define-public crate-pulz-bitset-0.1 (crate (name "pulz-bitset") (vers "0.1.0-alpha") (hash "1kz77r0l12xw5dp5zb5bnfskj494yf0x3rwm581z55ir541vblxj")))

(define-public crate-pulz-ecs-0.0.1 (crate (name "pulz-ecs") (vers "0.0.1") (deps (list (crate-dep (name "pulz-arena") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "002adxfbvgq6xfy0drck1ysv9c4kv7n4qf5cpkw347896fdcxyjk")))

(define-public crate-pulz-ecs-0.1 (crate (name "pulz-ecs") (vers "0.1.0-alpha") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pulz-bitset") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "pulz-ecs-macros") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "pulz-schedule") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sqww6hz6f78vk3z28ysps82f5x8l8rxy695144zipcg9ng0l654")))

(define-public crate-pulz-ecs-macros-0.1 (crate (name "pulz-ecs-macros") (vers "0.1.0-alpha") (deps (list (crate-dep (name "darling") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-crate") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19rmlzr2bs75mm7lbfcz0q2lhpzp0an8hagx6dsvcrmbm6wbki04")))

(define-public crate-pulz-executor-0.1 (crate (name "pulz-executor") (vers "0.1.0-alpha") (deps (list (crate-dep (name "async-std") (req "^1.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10") (features (quote ("attributes"))) (default-features #t) (target "cfg(not(target_os = \"unknown\"))") (kind 2)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("rt"))) (optional #t) (kind 0)))) (hash "0wbgd7pkj5g1nqryf59i3lgp0cf26xi1ibbw79q6bish7nxhjfhz")))

(define-public crate-pulz-gltf-0.0.1 (crate (name "pulz-gltf") (vers "0.0.1-alpha") (hash "1cvmnb4hwgqjfpwxd61qwkc8g5blkv5nprs9gsz1gwvsc366jla0")))

(define-public crate-pulz-graph-0.0.1 (crate (name "pulz-graph") (vers "0.0.1") (hash "0yffx94y98dggh3ilxnq51vc51xn3w5wcnp9qc5d1yffm7lv4vpz")))

(define-public crate-pulz-input-0.0.1 (crate (name "pulz-input") (vers "0.0.1-alpha") (hash "1zmnl0n17ah26jx5ijrv9y33hcln4q4a59pg3vvydxf9x1xfix8c")))

(define-public crate-pulz-render-0.0.1 (crate (name "pulz-render") (vers "0.0.1") (deps (list (crate-dep (name "pulz-ecs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1sia6nhmprs7kwrf3r5vwal4mk506c7pghqv8apy424mzv7qbsr4")))

(define-public crate-pulz-render-wgpu-0.0.1 (crate (name "pulz-render-wgpu") (vers "0.0.1-alpha") (deps (list (crate-dep (name "pulz-render") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "04paf5pb5fab9gf2cx33z7xidkh473wv5pxidphxcp7ly54w3pfd")))

(define-public crate-pulz-schedule-0.1 (crate (name "pulz-schedule") (vers "0.1.0-alpha") (deps (list (crate-dep (name "async-std") (req "^1.10") (features (quote ("attributes"))) (default-features #t) (target "cfg(not(target_os = \"unknown\"))") (kind 2)) (crate-dep (name "atomic_refcell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "downcast-rs") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pulz-executor") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "pulz-executor") (req "^0.1.0-alpha") (features (quote ("async-std"))) (default-features #t) (target "cfg(not(target_os = \"unknown\"))") (kind 2)) (crate-dep (name "tinybox") (req "^0.2") (default-features #t) (kind 0)))) (hash "00z9szzy5h3a30v483wycmf2yaaqvb5xyknb157lzbqviwnc7qic")))

(define-public crate-pulz-transform-0.0.1 (crate (name "pulz-transform") (vers "0.0.1-alpha") (hash "1pc1ccz9wgz8906pilpdsrhic7j2r9p8pmv3gqcc6g1jlwa4r0y3")))

(define-public crate-pulz-window-0.0.1 (crate (name "pulz-window") (vers "0.0.1-alpha") (hash "0lvwa8bh78v7j9jr4rdn46ld99axrrl6dg4c7gjfx4nd0nifdzrx")))

(define-public crate-pulz-window-winit-0.0.1 (crate (name "pulz-window-winit") (vers "0.0.1-alpha") (deps (list (crate-dep (name "pulz-window") (req "^0.0.1-alpha") (default-features #t) (kind 0)))) (hash "1qc5b2ywwn20fhdvrrcs7mlzrlvrgl3sni5585782y5ga6bhcv1j")))

