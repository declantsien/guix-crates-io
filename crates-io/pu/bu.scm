(define-module (crates-io pu bu) #:use-module (crates-io))

(define-public crate-pubUse-0.1 (crate (name "pubUse") (vers "0.1.0") (hash "0fv0zd6143drnd7aiwvnanp646jkzl2w1czvd2q7x3b648h9z7g4") (yanked #t)))

(define-public crate-pubuse_test-0.1 (crate (name "pubuse_test") (vers "0.1.0") (hash "0i20n0mavpflnbrsjg7bign7mvyz4l1ngaldfamcw7r332dwg6q9")))

