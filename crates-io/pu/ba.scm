(define-module (crates-io pu ba) #:use-module (crates-io))

(define-public crate-puball-0.1 (crate (name "puball") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ixkrsp92ayr3chh94di5v3l64a8ch6l32i1shk79bgs3zwp3d2m")))

(define-public crate-puball-0.1 (crate (name "puball") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "03my8zybfafzqvy8iy3q7nfg7b4h20z80pgq59n26f09k22z2xrd")))

