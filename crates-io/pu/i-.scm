(define-module (crates-io pu i-) #:use-module (crates-io))

(define-public crate-pui-arena-0.5 (crate (name "pui-arena") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pui-core") (req "^0.5.1") (optional #t) (kind 0)) (crate-dep (name "pui-vec") (req "^0.5") (default-features #t) (kind 0)))) (hash "0d0z163b2p1qn8j02749kv9z64dgl48avszl2ww7fa8h9a3hdxyx") (features (quote (("slotmap") ("slab") ("scoped" "pui") ("pui" "pui-vec/pui" "pui-core"))))))

(define-public crate-pui-arena-0.5 (crate (name "pui-arena") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pui-core") (req "^0.5.2") (optional #t) (kind 0)) (crate-dep (name "pui-vec") (req "^0.5.1") (kind 0)))) (hash "1czzzlccn12dk4vh1ywsjlqcvcscdzw9kalk3fdqznj93cvrb1zn") (features (quote (("slotmap") ("slab") ("scoped" "pui") ("pui" "pui-vec/pui" "pui-core") ("default" "pui"))))))

(define-public crate-pui-cell-0.5 (crate (name "pui-cell") (vers "0.5.0") (deps (list (crate-dep (name "pui-core") (req "^0.5.1") (kind 0)) (crate-dep (name "typsy") (req "^0.1") (kind 0)))) (hash "08gssxsln64vx0jbr8xgfy90gz7vkqrk92d3wb5myzapm9p4ix5n")))

(define-public crate-pui-cell-0.5 (crate (name "pui-cell") (vers "0.5.1") (deps (list (crate-dep (name "pui-core") (req "^0.5.2") (kind 0)) (crate-dep (name "typsy") (req "^0.1") (kind 0)))) (hash "0qhc0sp7w0wzjylyi7kxfla1ji8ssrf8jphfzxj711riiz9zlph2")))

(define-public crate-pui-core-0.5 (crate (name "pui-core") (vers "0.5.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "radium") (req "^0.6") (default-features #t) (kind 0)))) (hash "1zcvhswyls9bq4y0w07vi2f0s1218fqvpywcb5b02nr33f196nvg") (features (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-pui-core-0.5 (crate (name "pui-core") (vers "0.5.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "radium") (req "^0.6") (default-features #t) (kind 0)))) (hash "0c5nxq0q34z3j62p12l5m1f4qh6sklp5d0w6yj40q91v62cpm8ax") (features (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-pui-core-0.5 (crate (name "pui-core") (vers "0.5.2") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "radium") (req "^0.6") (default-features #t) (kind 0)))) (hash "1f6hxv8qmcp3gngfcfsqzsqh3923ddrhn3bk7f8q55gk7lvf5wa6") (features (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-pui-vec-0.5 (crate (name "pui-vec") (vers "0.5.0") (deps (list (crate-dep (name "pui-core") (req "^0.5.1") (optional #t) (kind 0)))) (hash "0wfh9qvaik33c1dk8k34i5ckz6rszbj80brr92al6fqlb6ncimaz") (features (quote (("pui" "pui-core") ("default" "pui"))))))

(define-public crate-pui-vec-0.5 (crate (name "pui-vec") (vers "0.5.1") (deps (list (crate-dep (name "pui-core") (req "^0.5.2") (optional #t) (kind 0)))) (hash "1fkaankf8xx2zwvywghfhf0yky6579lrmfppcb4lxrfbfa0yim2y") (features (quote (("pui" "pui-core") ("default" "pui"))))))

