(define-module (crates-io pu ny) #:use-module (crates-io))

(define-public crate-puny2d-0.0.1 (crate (name "puny2d") (vers "0.0.1") (deps (list (crate-dep (name "font-loader") (req "~0.11.0") (default-features #t) (kind 0)) (crate-dep (name "glyph_brush_layout") (req "~0.2.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "~0.23.13") (default-features #t) (kind 2)) (crate-dep (name "lru") (req "~0.6.4") (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "~0.5.0") (default-features #t) (kind 2)) (crate-dep (name "ocl") (req "~0.19.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "~1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "~0.9.2") (default-features #t) (kind 0)))) (hash "0l40rfw73k1d21qxd17l5y7jzwibl5yjhjw3azw58894pwfvk96n") (features (quote (("gpgpu" "ocl"))))))

(define-public crate-puny2d-0.0.2 (crate (name "puny2d") (vers "0.0.2") (deps (list (crate-dep (name "font-loader") (req "~0.11.0") (default-features #t) (kind 0)) (crate-dep (name "glyph_brush_layout") (req "~0.2.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "~0.23.13") (default-features #t) (kind 2)) (crate-dep (name "lru") (req "~0.6.4") (default-features #t) (kind 0)) (crate-dep (name "microbench") (req "~0.5.0") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "~1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "~0.9.2") (default-features #t) (kind 0)))) (hash "09vmq4g46dx8axm0pj5i2k39zpgpmx34vsm5f94azh7n6r4x01w3")))

(define-public crate-punycode-0.1 (crate (name "punycode") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mjndy4ffb45wl6rghym2kx6gjlnys9mdsvw1k9sl7l617zcwas2") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.2 (crate (name "punycode") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ya78whqfq2896wj68kpnhia43xm5asmh8h1jrw0xrahqzmnpzmw") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.3 (crate (name "punycode") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0q9p6n1k1q2hlhr48kr3wv8vm7y4azp1lpfjp51hf42lfxw9i2y8") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.4 (crate (name "punycode") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1lwa292nhg4p9zv4k1kmwj5zyxnkl5l1blmjhc4d793hr8n13pbd") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-punycode-0.4 (crate (name "punycode") (vers "0.4.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zm5722qaz1zhxb5nxnisj009crrknjs9xv4vdp9z0yn42rxrqg9") (features (quote (("dev" "clippy") ("default"))))))

