(define-module (crates-io pu bk) #:use-module (crates-io))

(define-public crate-pubkey-0.1 (crate (name "pubkey") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.9") (default-features #t) (kind 0)))) (hash "1c2akj4jaf9rx1c99f9q863swah319by19hmf6ihyx652r9byzzn") (yanked #t)))

(define-public crate-pubkey-0.1 (crate (name "pubkey") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "^1.7") (default-features #t) (kind 0)))) (hash "015kj8pnzrnind6p5x454w6a6h9js3js97ywlhxmfnw89lfcz1mm") (yanked #t)))

