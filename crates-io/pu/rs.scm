(define-module (crates-io pu rs) #:use-module (crates-io))

(define-public crate-purs-0.1 (crate (name "purs") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.26.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tico") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "try_opt") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0czwkxlyqn0cdl7l8jl65g1rccdzdb6a9jmwj6hz22gvywp14zp0")))

(define-public crate-purse-0.1 (crate (name "purse") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "dashmap") (req "^5.5.3") (optional #t) (default-features #t) (kind 0)))) (hash "0jaxckdqdprs31yk1yiihfqsignvzx2gfpxlrc60n64hy13r8ack") (features (quote (("atomic" "dashmap")))) (rust-version "1.71.0")))

(define-public crate-purse-0.1 (crate (name "purse") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "dashmap") (req "^5.5.3") (optional #t) (default-features #t) (kind 0)))) (hash "1139cax4mrm1dn7aa611zdjkjp5zl0jxc9kav83s3wyx4jyxfyd7") (features (quote (("atomic" "dashmap")))) (rust-version "1.71.0")))

(define-public crate-purse-0.1 (crate (name "purse") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mkh7kfn9dy092sikmsbgdmf4nwh1vf3fi3qib55r69cq2yqagpp") (rust-version "1.71.0")))

(define-public crate-purson-0.0.0 (crate (name "purson") (vers "0.0.0") (hash "1zn7mpf1gw0x6js9zbhraw848yz2vp67wsn9jcyay5v1zmkwl9ad")))

