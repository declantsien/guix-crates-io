(define-module (crates-io pu g_) #:use-module (crates-io))

(define-public crate-pug_tmp_workaround-0.1 (crate (name "pug_tmp_workaround") (vers "0.1.9") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1a038b04z6a2jival3hxvgf9l7dzh7m4226l8mbqqhdvxfpc20q9")))

(define-public crate-pug_tmp_workaround-0.0.1 (crate (name "pug_tmp_workaround") (vers "0.0.1") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "11jyv6m6sj5rsmw5rglhhvbhjxhdq7b821pxb3m0sv53mm33fl9x")))

(define-public crate-pug_tmp_workaround-0.0.2 (crate (name "pug_tmp_workaround") (vers "0.0.2") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1phsypghahnn6544c1w8krlhy4n37xf5yk92gfy67cqmvg7s3cgy")))

(define-public crate-pug_tmp_workaround-0.0.3 (crate (name "pug_tmp_workaround") (vers "0.0.3") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1jzvzgqy8bs7zbpzj72m493hzadv79m73xcb70ygmcb4q1i0pmzy")))

