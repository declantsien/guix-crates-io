(define-module (crates-io pu id) #:use-module (crates-io))

(define-public crate-puid-0.1 (crate (name "puid") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "1swl7v7qha1ivphmx449xf3rmqw1bnrq9qj2jsrzqw0fkc0pgmf2")))

(define-public crate-puid-0.2 (crate (name "puid") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "1rwvnnqxwkfvfryankijmbs5wy1wfrkdxxgbii4yd7077mid61cf")))

