(define-module (crates-io pu ri) #:use-module (crates-io))

(define-public crate-purilo-0.1 (crate (name "purilo") (vers "0.1.0") (hash "0mkwwcqq1jsb0nla3zff4cw8gp5jfg5d8ikg4g94g1aa0zqy2q47")))

(define-public crate-purity-0.1 (crate (name "purity") (vers "0.1.0") (hash "095y5rm3w52np6gmvz846iv7m251qfgx5c55sywdlndy21g56pv4")))

