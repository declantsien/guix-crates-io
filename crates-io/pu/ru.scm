(define-module (crates-io pu ru) #:use-module (crates-io))

(define-public crate-puru-0.1 (crate (name "puru") (vers "0.1.0") (hash "07yfqzibn33x3wzk48c80f0f9zwpdzl2zn1a162pn6psx2gxa8g0")))

(define-public crate-Puru_Test_FILE-0.1 (crate (name "Puru_Test_FILE") (vers "0.1.0") (hash "1f42dadp7igyw6ys44c84vxbnmkh3xgyrvv4ix3sxkviqf5prv45")))

(define-public crate-purua-0.0.1 (crate (name "purua") (vers "0.0.1") (deps (list (crate-dep (name "combine") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "combine-language") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "15gn26vzkxnw1rys4xfkwa5rx6vs7r3nm26awqf0w905s5hkc18h")))

(define-public crate-purua-0.0.2 (crate (name "purua") (vers "0.0.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0pjms030n7ig2b9jjq12s9wvkc2j6xi66m0mrmwhkjsvy1vicgbw")))

(define-public crate-puruda-0.1 (crate (name "puruda") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "peroxide") (req "^0.21") (features (quote ("dataframe"))) (default-features #t) (kind 2)) (crate-dep (name "puruda-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0n1i4qnpqi97sdnbgqsj97vj846cwmdi8bhvg4gp6z635i0fd37g") (features (quote (("default" "csv"))))))

(define-public crate-puruda-macro-0.1 (crate (name "puruda-macro") (vers "0.1.0") (hash "1dn9fxpdaqd38az3n5pnh1hapr624w2ah30z49mcddf7f4nrbd9l")))

(define-public crate-puruda-macro-0.1 (crate (name "puruda-macro") (vers "0.1.1") (hash "1mcdr1lr84mi0l020qwmhswkwnc80kl5v2fkpmdwzn8q1nz5lw7b")))

(define-public crate-puruspe-0.1 (crate (name "puruspe") (vers "0.1.0") (hash "1hn15lzfkjrb5b7mp2qdb7xww73rwhjp260z9px18aiqy08b21kv")))

(define-public crate-puruspe-0.1 (crate (name "puruspe") (vers "0.1.1") (hash "1qb56s1lr9b01wlbam2cf18fy9sy1sf8ayr6v78yf886xnpj3jy2")))

(define-public crate-puruspe-0.1 (crate (name "puruspe") (vers "0.1.3") (hash "1rfg0xq52g5ccv0a37zpw8rdyrxff15hh37n6cni6427irj7418i")))

(define-public crate-puruspe-0.1 (crate (name "puruspe") (vers "0.1.4") (hash "1hx03s2yhsxqpzsl2vn60qb2l2mdbwxhmnj620fzxfd4hp3mmrln")))

(define-public crate-puruspe-0.1 (crate (name "puruspe") (vers "0.1.5") (hash "0xdkbg07h5jgbjvskb6m7p5nv3s69df5iwnmsq4x48sh7251aziv")))

(define-public crate-puruspe-0.2 (crate (name "puruspe") (vers "0.2.0") (hash "1f43khpag8xsdf65j6y1lwz6h8akkwwr139v6za6zfmjkzhnaxzy")))

(define-public crate-puruspe-0.2 (crate (name "puruspe") (vers "0.2.1") (hash "1p8qpj7mkwxwnshsnf72i70fqdzlx8wm7d7snd5hjaryfk1cghyb")))

(define-public crate-puruspe-0.2 (crate (name "puruspe") (vers "0.2.2") (hash "01xrc6fjxpj2gva2pq4w2lr5imx1w4lay91w3cdjx6yfvgnygda9")))

(define-public crate-puruspe-0.2 (crate (name "puruspe") (vers "0.2.3") (hash "12kw4h111i20576h6vf7krqjs8fw3w5mb7avzh3izcklmskphrrc")))

(define-public crate-puruspe-0.2 (crate (name "puruspe") (vers "0.2.4") (deps (list (crate-dep (name "peroxide") (req "^0.35") (features (quote ("plot"))) (default-features #t) (kind 2)))) (hash "0gzx9lilwxmk2m8fblnnjsinzh3vryfh8pmsbylyl9gn2pbyx886")))

