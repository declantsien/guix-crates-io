(define-module (crates-io pu rr) #:use-module (crates-io))

(define-public crate-purr-0.1 (crate (name "purr") (vers "0.1.0") (hash "0z8285ld8dzpdmfyp970s0546i7zzqrjpk2jdqgxjmwrmq85pydb")))

(define-public crate-purr-0.1 (crate (name "purr") (vers "0.1.1") (hash "0si0lzv5pwxpymgxmylsgmgc6b2g4q83753d1v0izj84wk12wh9f")))

(define-public crate-purr-0.2 (crate (name "purr") (vers "0.2.0") (hash "1x4xjgrh8c549d633mxb3dmb3n0zi44s7ag7z79ggaghr0lmfpv1")))

(define-public crate-purr-0.3 (crate (name "purr") (vers "0.3.0") (hash "1732g5x7vwx3h6l1fdp9xwm4lg74bl27k7wn1z1y0k4phxrk4v39")))

(define-public crate-purr-0.4 (crate (name "purr") (vers "0.4.0") (hash "01pxbkrzli9fcw36zycpinq9ib6hxk7idk2ic7sjzbnnj3605c9p")))

(define-public crate-purr-0.5 (crate (name "purr") (vers "0.5.0") (hash "06nahi7jaj1s2gjarl73diffpajqigdc6svv5vx8c0d7772x3p85")))

(define-public crate-purr-0.6 (crate (name "purr") (vers "0.6.0") (hash "0f8z3hhb15c9z66g5cfxd77pbsfsnwx06xpb9aadfysrc93as0m3")))

(define-public crate-purr-0.6 (crate (name "purr") (vers "0.6.1") (hash "19bh27z6nwcwnxdc5iscjmqyik7sqcprrfw3mjja0gnf58p55yi8")))

(define-public crate-purr-0.6 (crate (name "purr") (vers "0.6.2") (hash "0ii883fvc8xxl9qmzl2cm1xf9hrzzdqcpbqgcnsc4237c5aal2ia")))

(define-public crate-purr-0.6 (crate (name "purr") (vers "0.6.3") (hash "0lg44nzjaklpvvdqayfrcnlhbsgmp11cw9xg0h1f1nv5q91y6nsz")))

(define-public crate-purr-0.7 (crate (name "purr") (vers "0.7.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1ka4n29kk697p21snc3cc6blqywhd4gslsla9m3xymrqc8krjfdz")))

(define-public crate-purr-0.8 (crate (name "purr") (vers "0.8.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "1m89akx3kqcsa89y6364j1yiggwv72v2cp1jng8k9ggbgipqhznl")))

(define-public crate-purr-0.9 (crate (name "purr") (vers "0.9.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)))) (hash "12hh4264rfic7mm9zlsaqvglj79y6xjx1g0mly8m3js1cn7wyppn")))

(define-public crate-purrchance-0.1 (crate (name "purrchance") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "05xp3zwxq1ljpacn986k0xx6fv88g7pc6cdbizcwmgn8yvcx8w2q")))

(define-public crate-purrchance-0.2 (crate (name "purrchance") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "089kgclcivd9x9m3ymj7vl9f36wkjispwb26ml3k8kwgyl2qzs69")))

(define-public crate-purrchance-0.3 (crate (name "purrchance") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0q8f1629ycq7k46bxhv412l7mfi8l8xz5vpsx7ggk14gv1lvbxc0")))

(define-public crate-purrchance-0.4 (crate (name "purrchance") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "06z37mx4sddjhjqhff97figajzi4v37vbdl4ddc77h71rxiajp46")))

(define-public crate-purrchance-0.4 (crate (name "purrchance") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "12w8936jgdc82kcmr3l5yxb1fg7zlxmg61pinr3ja9s6w5g4wc8v")))

(define-public crate-purrmitive-0.1 (crate (name "purrmitive") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.8") (default-features #t) (kind 0)) (crate-dep (name "nsvg") (req "^0.5.1") (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "0l67jwhqqq2sd8ix48xl599kwl5ndi5caplpgq8n3zdlxrz77aw1") (features (quote (("cli" "clap"))))))

(define-public crate-purrmitive-0.1 (crate (name "purrmitive") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nsvg") (req "^0.5.1") (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dssxp094j526j6zi8ncmrh19gnvg9n7w6xs63anpwgllfal97j1") (features (quote (("cli" "clap"))))))

(define-public crate-purrmitive-0.1 (crate (name "purrmitive") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nsvg") (req "^0.5.1") (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "046lnmqbca662jzlqxzbbcmr23qjn7148khakf64yx7mkrnwy18n") (features (quote (("cli" "clap"))))))

(define-public crate-purrmitive-0.1 (crate (name "purrmitive") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dyn-fmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nsvg") (req "^0.5.1") (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mqi1cy1a2vn4f97zzgbvj61842knk0hkmy1qk1yl7r7rmdgmqsv") (features (quote (("cli" "clap"))))))

(define-public crate-purrmitive-0.2 (crate (name "purrmitive") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dyn-fmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nsvg") (req "^0.5.1") (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.0") (default-features #t) (kind 0)))) (hash "12cc0apzim5iy4nh24zqm7dahg7p4cnfyd69vhyhf4mw86dwpc41") (features (quote (("cli" "clap"))))))

