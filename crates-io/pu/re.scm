(define-module (crates-io pu re) #:use-module (crates-io))

(define-public crate-pure-0.0.0 (crate (name "pure") (vers "0.0.0") (hash "0m2kadzswmbmhkxihknspvqxd0kair9847syh9s9cb027039c0bi") (yanked #t)))

(define-public crate-pure-hfsm-0.1 (crate (name "pure-hfsm") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.7") (features (quote ("union" "const_generics"))) (default-features #t) (kind 0)))) (hash "13f8701rbhy0yiicvc5wykgq0yybmpmhcfjwdz5mh63k0i86w11m")))

(define-public crate-pure-rust-coloring-game-0.1 (crate (name "pure-rust-coloring-game") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1w5b2xizcig6q61y9gjikh85wrdhgwyaqsi4dfpzr05v8583q1w7") (yanked #t)))

(define-public crate-pure-rust-locales-0.1 (crate (name "pure-rust-locales") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "08j79jkwlqmfz9brgmicpx7zwyj2hk1fwsk279p62wn3fx6zzmxc")))

(define-public crate-pure-rust-locales-0.2 (crate (name "pure-rust-locales") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "10qa6786142007csrlnzxf1jrw0qbhws4i1mmi5nz6r716gx6hg5")))

(define-public crate-pure-rust-locales-0.2 (crate (name "pure-rust-locales") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "0lhylri9gw5zjma5ycwa8sb9li5j45nc43a5k182pl42xcc9f96c")))

(define-public crate-pure-rust-locales-0.2 (crate (name "pure-rust-locales") (vers "0.2.2") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "0krkbxgghn3s1m82vb2yvjgp0171wpgpj0bgvkwdgfnv9vbq6gj4")))

(define-public crate-pure-rust-locales-0.3 (crate (name "pure-rust-locales") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "1qimzrh8a6cni5vdi9cdrr6r7rgs3ibwycyzw74habfk4kcgc5pf")))

(define-public crate-pure-rust-locales-0.4 (crate (name "pure-rust-locales") (vers "0.4.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "0lwzmzm6ry197llb0zfrnmiqnd70ixjxw6zl4mis2ahhy9q3s6wi")))

(define-public crate-pure-rust-locales-0.5 (crate (name "pure-rust-locales") (vers "0.5.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "08q6w1y5af8kp52w3rik5d6b5j66222xgbhmzb5f589wwf79vhgs")))

(define-public crate-pure-rust-locales-0.5 (crate (name "pure-rust-locales") (vers "0.5.1") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "02rb2dff0w31z0b356mxzbr3lmwx2nwk28pjhwqsyq40szqrkwby")))

(define-public crate-pure-rust-locales-0.5 (crate (name "pure-rust-locales") (vers "0.5.2") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "0vhyqi44xc3za938h3mazvarjqardjqmxkzimin999j7kr7x6msd")))

(define-public crate-pure-rust-locales-0.5 (crate (name "pure-rust-locales") (vers "0.5.3") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 1)))) (hash "0ryjj0gs4hfadqx9vl4sgi32zyb2dlvwpxca1m1kmrw9hk1g7gv5")))

(define-public crate-pure-rust-locales-0.5 (crate (name "pure-rust-locales") (vers "0.5.4") (hash "17g0gjy6nxz71yn0j1l2yyvzjng7fxnqs3070kkx8w3dkz8gcwjg")))

(define-public crate-pure-rust-locales-0.5 (crate (name "pure-rust-locales") (vers "0.5.5") (hash "0gmfmckk0lcvma3hm3rnysjc89w5z77x8bgf5vrm8yr3gd91xf1y")))

(define-public crate-pure-rust-locales-0.5 (crate (name "pure-rust-locales") (vers "0.5.6") (hash "1n1jqy8g7ph9lvzncc8vy5jaqq2dljryp1agcnp5pwwi9zy4jp5l")))

(define-public crate-pure-rust-locales-0.6 (crate (name "pure-rust-locales") (vers "0.6.0") (hash "0ssj0ysdc8pj61wwc4kcyc9294w5j6ydbplfsw8h1fwf09ynv1ab")))

(define-public crate-pure-rust-locales-0.7 (crate (name "pure-rust-locales") (vers "0.7.0") (hash "0cl46srhxzj0jlvfp73l8l9qw54qwa04zywaxdf73hidwqlsh0pd")))

(define-public crate-pure-rust-locales-0.8 (crate (name "pure-rust-locales") (vers "0.8.0") (hash "1plq4hl7291dvg99iklw7bi5phpbyykp91hzfb50iy5v9snav21m")))

(define-public crate-pure-rust-locales-0.8 (crate (name "pure-rust-locales") (vers "0.8.1") (hash "0fkkwggiq2053rmiah2h06dz6w3yhy9pa82g30vy3sbcmqcgv40i") (rust-version "1.56.0")))

(define-public crate-pure_cell-0.1 (crate (name "pure_cell") (vers "0.1.0") (hash "1j1201ia54fivvgcw3k5fh98pkc86w26l76p018f81j2c5cxbkd9")))

(define-public crate-pure_cell-0.2 (crate (name "pure_cell") (vers "0.2.0") (hash "0kckwd357kfw1zn5qg4zv1f9pym5vfxmjdd8484ypf7z5hx28kmz")))

(define-public crate-pure_decimal-0.0.1 (crate (name "pure_decimal") (vers "0.0.1") (deps (list (crate-dep (name "decimal") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "18arg00i0cfd4qymhi02y2b9m8zbp6y9q02b7d4s7hz0n0hpszqk") (features (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.2 (crate (name "pure_decimal") (vers "0.0.2") (deps (list (crate-dep (name "decimal") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "19fkg5237z8s311paqv9sn83jzx2cwl24dnm9mlvjh52w64pqmw6") (features (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.3 (crate (name "pure_decimal") (vers "0.0.3") (deps (list (crate-dep (name "decimal") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1pl1bwir2rv3hd4q035sngd88hbcf1kngg0cd0zz0xnnbljs3jf0") (features (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.4 (crate (name "pure_decimal") (vers "0.0.4") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "decimal") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "179dyj185v2sfi96igbrkm8w8750nrjmp3wn716c50mkm0sw3nc2") (features (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.5 (crate (name "pure_decimal") (vers "0.0.5") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "decimal") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1gpyw1y68pjk99q52ahsq984vwmli7dk8nzkp1j63fm0fkj8b0mf") (features (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.6 (crate (name "pure_decimal") (vers "0.0.6") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "decimal") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1ccaz4z41knd1j1nli6nw2yy0npsq3dxpkk4cjgasr02zzlcnrp1") (features (quote (("default" "serde"))))))

(define-public crate-pure_decimal-0.0.7 (crate (name "pure_decimal") (vers "0.0.7") (deps (list (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "decimal") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "01vpqvlcpsmaxsmaxr2msk5hykbrpx0ckr1zxycnh50fd39fin92") (features (quote (("default" "serde"))))))

(define-public crate-pure_lines-0.1 (crate (name "pure_lines") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "09850d6mr794vl9aldf6knf0ry2ngs3zyhcdr1vws53lhrsrq3cy")))

(define-public crate-pure_lines-0.2 (crate (name "pure_lines") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1zfhady2ibssk1mvgwx113p0ip1r2y06sgx6pnsqrc4nb1f3znan") (features (quote (("quick"))))))

(define-public crate-pure_pursuit-0.1 (crate (name "pure_pursuit") (vers "0.1.0") (deps (list (crate-dep (name "array-concat") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "libc-print") (req "^0.1.17") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1wmjviw6jyaghp80ms70cqgg7n1hbd9v2bsckpgqjgi0pkdfjw1a")))

(define-public crate-pure_pursuit-0.1 (crate (name "pure_pursuit") (vers "0.1.1") (deps (list (crate-dep (name "libc-print") (req "^0.1.17") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1xg0479jnjlq2hddvhlvnv3cvw5nnfjmxwx0jlfhhnfz5gkv9dnv")))

(define-public crate-pure_pursuit-0.1 (crate (name "pure_pursuit") (vers "0.1.2") (deps (list (crate-dep (name "libc-print") (req "^0.1.17") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (features (quote ("libm"))) (kind 0)))) (hash "00sn48v4bhk4s30dqwa2v8mmg3yi89d137z0bgjf7xrqkra0grkj")))

(define-public crate-pure_pursuit-0.1 (crate (name "pure_pursuit") (vers "0.1.3") (deps (list (crate-dep (name "libc-print") (req "^0.1.17") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (features (quote ("libm"))) (kind 0)))) (hash "1hg1saqj0lgga1262yxbrsdl6fp9fjgq7xv3cd162pbkdfs0nin9")))

(define-public crate-pure_pursuit-0.1 (crate (name "pure_pursuit") (vers "0.1.4") (deps (list (crate-dep (name "libc-print") (req "^0.1.17") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (features (quote ("libm"))) (kind 0)))) (hash "1y4js442acgy3c2zvdnxl6idjf26dayczbn2ydywbj0kaxi8548d")))

(define-public crate-pure_ref-0.1 (crate (name "pure_ref") (vers "0.1.0") (hash "1ipg2rhvnxr097z2inhrc74laa8v22qbgckw4cxmnmkyhf7y6hxc")))

(define-public crate-pure_ref-0.1 (crate (name "pure_ref") (vers "0.1.1") (hash "1ryzw6gad35indld7294wpl61hy7p4rgkdwfbch81n55yl350mr9")))

(define-public crate-pure_ref-0.1 (crate (name "pure_ref") (vers "0.1.2") (hash "0q7flrmzs4l6i055ckasj51gjakagaaic97i6ni3waibg6fhb08r")))

(define-public crate-pure_ref-0.1 (crate (name "pure_ref") (vers "0.1.3") (hash "1ls9ncddqbjaqxcqp72pkb0451r083djj9544621wrv5qzkkphnz")))

(define-public crate-pure_stack_machine-0.1 (crate (name "pure_stack_machine") (vers "0.1.0") (hash "0j4z7mkq6sa6p2pqgj3ayjjj8j0fz85g76l8zxincbv5i4w8q759") (yanked #t)))

(define-public crate-pure_vorbis-0.0.1 (crate (name "pure_vorbis") (vers "0.0.1") (deps (list (crate-dep (name "ao") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.11.0") (default-features #t) (kind 2)) (crate-dep (name "enum_primitive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "ogg_vorbis_ref") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "scoped-pool") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1lpvdycqvgq1jvy57k7xi1ffv2p9x2ajd402y40g1n67wq2bmviy")))

(define-public crate-puremp3-0.1 (crate (name "puremp3") (vers "0.1.0") (deps (list (crate-dep (name "bitstream-io") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "sample") (req "^0.10") (default-features #t) (kind 2)))) (hash "10yq20kk0m8bc6554gdr3xsssv7sfl9n3q1rq5qaywz376xyzdzj")))

(define-public crate-purescript-corefn-0.1 (crate (name "purescript-corefn") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1sphkh6wsx5nzblyami6cbbckv06zdzk6rx6i42swr2g4fxcrmx9")))

(define-public crate-purescript-corefn-0.1 (crate (name "purescript-corefn") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0ss28snpk1na3mncag0h2qwa3a72li335rhyc7124cr85p3p1aag")))

(define-public crate-purescript-corefn-0.1 (crate (name "purescript-corefn") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0fv1xgimsdch3m3c2pxzz1xmb3lgjkn6qh0ybjb7n1l4qxrffb9w")))

(define-public crate-purescript-corefn-0.1 (crate (name "purescript-corefn") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1id0jd9a835ikl0mxvkssbnfq5ir3y6rzv427ysd9w8nqlw2qsh6")))

(define-public crate-purescript-corefn-0.1 (crate (name "purescript-corefn") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1q02g6wlgg5l4yxg6nhxlaz67mkksn7qd3iv8v42ipd1zgxm4d0z")))

(define-public crate-purescript-corefn-0.2 (crate (name "purescript-corefn") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.36") (default-features #t) (kind 0)))) (hash "0lrbk1z9mqnjpnhpb06nkqv7ap0azrk7lczi9s933l1ly7pa7wss")))

(define-public crate-purescript-corefn-0.2 (crate (name "purescript-corefn") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.36") (default-features #t) (kind 0)))) (hash "0byrs3sgxqlmdv7y7rzwp8sb1b3693gb4br00bgryvl7rqmi9v18")))

(define-public crate-purescript-corefn-0.2 (crate (name "purescript-corefn") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.36") (default-features #t) (kind 0)))) (hash "05x4hdjs9vsamnl54mw0fcibpniypcr0b1b0zd2kvjzyp7a7vc85")))

(define-public crate-purescript-corefn-0.3 (crate (name "purescript-corefn") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0n76r7qx4gxmk9d05wkp0fn7946c9sq3kc2dx47ggb0xwi2xc1g8")))

(define-public crate-purescript_waterslide-0.1 (crate (name "purescript_waterslide") (vers "0.1.0") (hash "003mbxmpd8414gjaanybpbicwsv9qlaj30yl45nnsppr5n35bqid")))

(define-public crate-purescript_waterslide-0.2 (crate (name "purescript_waterslide") (vers "0.2.0") (hash "1pvyazs23wvxm9i74pckzq53x19gqiysfrcrll3yzp1yaamsm0jf")))

(define-public crate-purescript_waterslide-0.2 (crate (name "purescript_waterslide") (vers "0.2.1") (hash "11yvifz9fchbdqs61gmqglx462r6ksc8gg1nan2kk3i05c1gk7x1")))

(define-public crate-purescript_waterslide-0.3 (crate (name "purescript_waterslide") (vers "0.3.0") (hash "0k2pmrk5lar5c33byz91xsvnx1l81lq6lz9j53r1zivjvg4s6jbl")))

(define-public crate-purescript_waterslide-0.3 (crate (name "purescript_waterslide") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "0j3chgjlz4qrq7d8w1z7d9y35fddavcj1kbxwm8mx4l3rzmdacwr") (features (quote (("uuid_support" "uuid") ("chrono_support" "chrono"))))))

(define-public crate-purescript_waterslide_derive-0.1 (crate (name "purescript_waterslide_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "purescript_waterslide") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0q3fr758pq2kvmjglwkgg5gip1sgsbgmpm0xlj9qb7i1djd72l9d")))

(define-public crate-purescript_waterslide_derive-0.2 (crate (name "purescript_waterslide_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "purescript_waterslide") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0fyjv8fp25a6piszgj9z8ny7y5kvqmxg0lgprxwkw5nzxm61mm1f")))

(define-public crate-purescript_waterslide_derive-0.2 (crate (name "purescript_waterslide_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "purescript_waterslide") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "1j823ihf38qj20qakyqh6s5qzaznlajip43l7133smqqmv89s7w4")))

(define-public crate-purescript_waterslide_derive-0.3 (crate (name "purescript_waterslide_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "purescript_waterslide") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0m3z4xm44qd9vpqcb5d36jpjy4wrx0f5i8zy940mmbgxmj9hk793")))

(define-public crate-purescript_waterslide_derive-0.3 (crate (name "purescript_waterslide_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "purescript_waterslide") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.11") (default-features #t) (kind 0)))) (hash "0n1ji8b30kcx8lpr2vaxxz1fqfdmdhrgi3vq5rxg25idcqkm2n6b")))

(define-public crate-purewasm-0.1 (crate (name "purewasm") (vers "0.1.0") (deps (list (crate-dep (name "purewasm-bindgen") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "purewasm-cbor") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "purewasm-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "purewasm-json") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "purewasm-wasmtime") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0j7hp2ib3i7alr09fhp78f58jz3dd3mcik4ahpcrdm23f1wr4d6p") (features (quote (("wasmtime" "purewasm-wasmtime")))) (v 2) (features2 (quote (("bindgen-json" "purewasm-bindgen" "dep:purewasm-json") ("bindgen" "purewasm-bindgen" "dep:purewasm-cbor"))))))

(define-public crate-purewasm-bindgen-0.1 (crate (name "purewasm-bindgen") (vers "0.1.0") (deps (list (crate-dep (name "lol_alloc") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "purewasm-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "purewasm-proc-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "alloc" "derive" "alloc"))) (kind 0)))) (hash "1m9snz5virxaxhcqf98hmkahzlsl7rcwp207pw5nxlbs3knmdz4m")))

(define-public crate-purewasm-cbor-0.1 (crate (name "purewasm-cbor") (vers "0.1.0") (deps (list (crate-dep (name "ciborium") (req "^0.2.1") (kind 0)) (crate-dep (name "purewasm-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "alloc"))) (kind 0)))) (hash "12fmg6v3k79qa4p1ymg28b7gdm3n089lwpmjsm96q8gjm6bgpxrc")))

(define-public crate-purewasm-core-0.1 (crate (name "purewasm-core") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "alloc"))) (kind 0)))) (hash "0vflhd2xqr1dzd81yh5qc2w1w83mx47g90wkyzryn3bmjl3zw3pw")))

(define-public crate-purewasm-json-0.1 (crate (name "purewasm-json") (vers "0.1.0") (deps (list (crate-dep (name "purewasm-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive" "alloc"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (features (quote ("alloc"))) (kind 0)))) (hash "14lwlgjhbsv1m0rjls9azv0c8bzkwgzncrl3xgpqp82qyfwmwbk7")))

(define-public crate-purewasm-proc-macro-0.1 (crate (name "purewasm-proc-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r6l9ywnvxzhn4gchcl1q25cf7xfq3q14rzfa8s5j82jrcxjj9jw")))

(define-public crate-purewasm-wasmtime-0.1 (crate (name "purewasm-wasmtime") (vers "0.1.0") (deps (list (crate-dep (name "wasmtime") (req "^12.0.1") (default-features #t) (kind 0)))) (hash "00z8xalr822psxpnbkicakxil1g4mcqqmzhcip6my3r6yb4rmd6q")))

(define-public crate-purezen-0.0.0 (crate (name "purezen") (vers "0.0.0") (hash "0lfhmn42ikkr42n3cs7z3gcmslrw2qjgvfv6cy4n7hf4zzd138wr")))

(define-public crate-purezen-0.0.1 (crate (name "purezen") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1") (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (kind 0)))) (hash "01cgk5b8hk3ga0y3fch2k8vad3wnfy3pvrg275wbbvc7gi446v61") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-purezen-0.0.2 (crate (name "purezen") (vers "0.0.2") (deps (list (crate-dep (name "failure") (req "^0.1") (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (kind 0)))) (hash "14ljk9h4hjwdla1z4dyv0ns1nzh4mnpi5yb1cc6vcqa3nl667ygz") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

