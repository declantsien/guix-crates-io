(define-module (crates-io pu pi) #:use-module (crates-io))

(define-public crate-pupil-0.1 (crate (name "pupil") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sydr1g7c1jswpz1s6w80as58aifk92wqih3iqpdr4qz054nw7qw")))

(define-public crate-pupil-0.1 (crate (name "pupil") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zcrhr8g4171d0ynl7whs6qlas1m3gkmkpz2n9k1wg5yrdimgili")))

(define-public crate-pupil-0.1 (crate (name "pupil") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bz0w5cfihhylw8qyfd8cr05vpsnfcqj588ab7kn7hc7k0ql5k3n")))

(define-public crate-pupil-0.1 (crate (name "pupil") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d0ljkl6p5izm0nrrkkqwdnmnvxndyzvpvfyc098driv5hn2irm2")))

