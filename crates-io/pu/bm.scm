(define-module (crates-io pu bm) #:use-module (crates-io))

(define-public crate-pubmed-0.1 (crate (name "pubmed") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1a3k0a5in5ivzj2q8f4377rsd4g6f615862w9hjanbirjs50v90m")))

(define-public crate-pubmed-0.1 (crate (name "pubmed") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "01gqp0rh88yjr85q5ra4f7kwrigyjfjjf1fwnfsm3508vmvdas1a")))

(define-public crate-pubmed-0.1 (crate (name "pubmed") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.11") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0y2injpaf947s4is2c9qxpfip0kqlskfn9p2xaki7q6jrrdfmwfy")))

