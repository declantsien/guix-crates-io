(define-module (crates-io pu dd) #:use-module (crates-io))

(define-public crate-pudding-kernel-0.1 (crate (name "pudding-kernel") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "pudding-pac") (req "^0.1.0") (kind 0)))) (hash "01lvqjlrcbn7yl35cwrdh2d64rljkad93iiyjlbax4ysaibx8pbp") (features (quote (("std") ("pl390") ("default" "std"))))))

(define-public crate-pudding-kernel-0.1 (crate (name "pudding-kernel") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "pudding-pac") (req "^0.1.2") (kind 0)))) (hash "1aavcg53qyw7h03mzxdy0jfbp1qrx16wrn7ifafd6fzl9035sl6d") (features (quote (("std") ("pl390") ("default" "std"))))))

(define-public crate-pudding-kernel-0.1 (crate (name "pudding-kernel") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "pudding-pac") (req "^0.1.3") (kind 0)))) (hash "1n60gin0amknpj7cj2mwdx9nmha8wq2asm2asd2vy92mj5ymd93m") (features (quote (("std") ("pl390") ("default" "std"))))))

(define-public crate-pudding-pac-0.1 (crate (name "pudding-pac") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1qr2823z2mgwv6n5vs6d6z8as8r99bmvpnn8jwgjgrx9b3wmcj7z")))

(define-public crate-pudding-pac-0.1 (crate (name "pudding-pac") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "08az9d65qy533qqlxkzvlby49z13pmqm5p8f2sjpfnglphpcqwwh")))

(define-public crate-pudding-pac-0.1 (crate (name "pudding-pac") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0snxp6jm9m2aiv5pqh74mzrjh0jbnj2f2kkrlibfv20k4iih01j0")))

(define-public crate-pudding-pac-0.1 (crate (name "pudding-pac") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1808dx7qj5khgq47zdql82pm3a3bj04xaiqrnwhw6kzjf2f59b8c")))

(define-public crate-puddle-0.1 (crate (name "puddle") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6.2") (features (quote ("unstable"))) (default-features #t) (kind 0)))) (hash "1mr4k83n26lywh5r5d0rrrw7k9a4b9plv5jkbsqf4a0bx1949y0c")))

