(define-module (crates-io pu bs) #:use-module (crates-io))

(define-public crate-pubsub-0.2 (crate (name "pubsub") (vers "0.2.0") (deps (list (crate-dep (name "threadpool") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0pfqcydjsl403hj0z81f2qj010nb0bv4n1bn27mkc8g32hpkm32b")))

(define-public crate-pubsub-0.2 (crate (name "pubsub") (vers "0.2.1") (deps (list (crate-dep (name "threadpool") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "01sk54xzw8mj2n1b918dl65v5ckc69kayvxzkxnnnzz2lynkxalk")))

(define-public crate-pubsub-0.2 (crate (name "pubsub") (vers "0.2.2") (deps (list (crate-dep (name "threadpool") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1kbv1p0491bisl6k4khkwmazbiw989dxqq9crni4xcv6l1xqyih9")))

(define-public crate-pubsub-0.2 (crate (name "pubsub") (vers "0.2.3") (deps (list (crate-dep (name "threadpool") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "132y4x8gr3dfn2lmca09drp56mgyi6h632fjl2s6kbnmx8ksnhvr")))

(define-public crate-pubsub_grpc-0.1 (crate (name "pubsub_grpc") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.1") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "1j2g3zv0qxyzw2py49xkgsjlnni2wmqq6hrp1ip3w7cgcqcbw3ni")))

(define-public crate-pubsub_grpc-0.4 (crate (name "pubsub_grpc") (vers "0.4.0") (deps (list (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.4.0") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "1lhnl6bnva3wm3rkzgrn6dqxkrpyg97jwwp25nn10nkys4ccjplp")))

(define-public crate-pubsub_grpc-0.5 (crate (name "pubsub_grpc") (vers "0.5.2") (deps (list (crate-dep (name "prost") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.5.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.5.2") (default-features #t) (kind 1)))) (hash "066jhmfqmf5n05cxnpdigycimajlrq8whban56r065h07nlpp78v")))

