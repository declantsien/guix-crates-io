(define-module (crates-io ul an) #:use-module (crates-io))

(define-public crate-ulang-0.1 (crate (name "ulang") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "1zghbriayf2n8spfw7g35b8xsgxwxpnjz9n0l8fs9irvx7g6ynck")))

