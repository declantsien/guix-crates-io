(define-module (crates-io ul n2) #:use-module (crates-io))

(define-public crate-uln2003-0.1 (crate (name "uln2003") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0479fnd8c597gd1dxb2jpjvva9nfmsz28v12s56b72ci390pydai")))

(define-public crate-uln2003-0.1 (crate (name "uln2003") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "13pr08slmprrnpflmi3xd2md37cpcniag26djlz88qfvqfih2bwn")))

(define-public crate-uln2003-0.2 (crate (name "uln2003") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1qpqg4lxfwfgfd5kqkaqhskw5fpfxsg60j8psa42l3c9xvv7nxlw")))

(define-public crate-uln2003-0.2 (crate (name "uln2003") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0fhqffm89hrfsm14fp2kn0skhjj1fh6wq71pdj8f3yc7sj569jww")))

