(define-module (crates-io ul og) #:use-module (crates-io))

(define-public crate-ulog-0.1 (crate (name "ulog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^0.3") (default-features #t) (kind 0)))) (hash "19zq5gvgyr6w3ds4257fckg7749bk84qhraf7dvj8700j72vh9za")))

(define-public crate-ulog-0.1 (crate (name "ulog") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^0.3") (default-features #t) (kind 0)))) (hash "09pyfis3ik3j45xb3rykr8li73mfzrkqhns9lwd7qmvd2xv2yp9p")))

(define-public crate-ulog-0.1 (crate (name "ulog") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^0.3") (default-features #t) (kind 0)))) (hash "07sf2pmvkb7m8snx04b6kyppbvjf18g63cqvhf718hiycb92k69i")))

(define-public crate-ulog-0.2 (crate (name "ulog") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^0.3") (default-features #t) (kind 0)))) (hash "1prq8ly6a73aggm3w2z070mkglwpsgx4nimqjbhx20kbgdf0h4dq")))

(define-public crate-ulog-0.3 (crate (name "ulog") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thread_local") (req "^0.3") (default-features #t) (kind 0)))) (hash "07w61w9w6z7r0wl0m2a4vqgnq8sx9wwhbnkbdssqh0s55ylr2pdi")))

