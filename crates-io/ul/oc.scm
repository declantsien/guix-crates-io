(define-module (crates-io ul oc) #:use-module (crates-io))

(define-public crate-uloc-0.0.0 (crate (name "uloc") (vers "0.0.0") (hash "1z6wka0v2m68sxmh8vjzpm105fsy7idi98b10vifdqs5n9m77k7b")))

(define-public crate-ulock-sys-0.1 (crate (name "ulock-sys") (vers "0.1.0") (deps (list (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yncf6cv3pdj36j2nvvif311zvxgyvina06g0ygdvhrhlbhndb9j") (features (quote (("weak-aarch64-macos") ("experimental-weak"))))))

