(define-module (crates-io ul ur) #:use-module (crates-io))

(define-public crate-uluru-0.1 (crate (name "uluru") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "163fmmlwz0zwjkx2hlmhs8gv2im6yvcd7c0aq7ig8j7x8pjgqa6d")))

(define-public crate-uluru-0.2 (crate (name "uluru") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.6") (kind 0)))) (hash "1fcx8yszwafvc5za1vb5skqjc8lcfdri7byqm50aajwnxbq314ai")))

(define-public crate-uluru-0.3 (crate (name "uluru") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.6") (kind 0)))) (hash "17vdfjab08vdykcyjmprzxq8mqh06jzx3idky32ds27kja8nwq6j")))

(define-public crate-uluru-0.4 (crate (name "uluru") (vers "0.4.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5") (kind 0)))) (hash "1jp16mgi5kg7w8damvsf4raygxhd2pflnkik5pamgfifqg83jyvd")))

(define-public crate-uluru-1 (crate (name "uluru") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5") (kind 0)))) (hash "0nkz17zpcf7dmb7nb8mi9n05782b8cqwp9d95pa9pj2sh29mx0p8")))

(define-public crate-uluru-1 (crate (name "uluru") (vers "1.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5") (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "17w8pwphqmsrjmwlsck49ba6jvwnqp6a1cg2cln7dc4mb6308mj9")))

(define-public crate-uluru-1 (crate (name "uluru") (vers "1.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5") (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "1nzmkq149yqn5pmxj6pk6ga6k2qanpfszf3n1qi6xjsf552nzvmn")))

(define-public crate-uluru-2 (crate (name "uluru") (vers "2.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)))) (hash "15zqgxkhqfych2n41rwn91bm487y851y7yrfb05cfi58441gq7p8")))

(define-public crate-uluru-2 (crate (name "uluru") (vers "2.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)))) (hash "08g371x51i3mzvycjdc87szby6mv4halijy6cgs847xkzipj5rak")))

(define-public crate-uluru-2 (crate (name "uluru") (vers "2.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)))) (hash "1ism59zf6srw3z3qasgadl732sldd2pn3kh4ll7m04w0agd6fyff")))

(define-public crate-uluru-2 (crate (name "uluru") (vers "2.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)))) (hash "0widdhxllh1836f2q04wm20hc1pv6scvkbaiz2b7f8kvjjfwr39h")))

(define-public crate-uluru-3 (crate (name "uluru") (vers "3.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)))) (hash "1nr6s90iqy7wr0ddv9zfbmyw5dc7xjfbb09c8sjbcphz38k34jkr")))

(define-public crate-uluru-3 (crate (name "uluru") (vers "3.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)))) (hash "1njp6vvy1mm8idnsp6ljyxx5znfsk3xkmk9cr2am0vkfwmlj92kw")))

