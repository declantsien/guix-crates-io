(define-module (crates-io ul tn) #:use-module (crates-io))

(define-public crate-ultnote-0.0.1 (crate (name "ultnote") (vers "0.0.1") (hash "0i4w3s1ax7m6d815nf2qp73d2vsanapik2yfwl91dfx7bims94r7")))

(define-public crate-ultnote-0.0.20 (crate (name "ultnote") (vers "0.0.20") (hash "1myppz7fa6k50pzndvlgldfhrnm40ssshrzv2b1b9kz14j3iscif")))

