(define-module (crates-io ul -s) #:use-module (crates-io))

(define-public crate-ul-sys-0.1 (crate (name "ul-sys") (vers "0.1.0-alpha") (hash "1rw5w0lzlhdis0vqccqk05qj21sfh2m9p548nw6a05p382kbd2wq")))

(define-public crate-ul-sys-0.1 (crate (name "ul-sys") (vers "0.1.0-rc1") (hash "1insisgpafa3qjzd002cd5yg840ypb4zvjz9s3qwjdwb9x3j763m")))

(define-public crate-ul-sys-0.1 (crate (name "ul-sys") (vers "0.1.0-rc1-2") (hash "1bffrk616nwvfp3x42fcry2lq1x2pxfjkhchcfsvm4jg2p36vph2")))

(define-public crate-ul-sys-0.1 (crate (name "ul-sys") (vers "0.1.0-rc2") (hash "0srzy5bh6189zhcipz6mjjcfjkgh8dap43ybgdyq11ffx797g8dq")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.0.0") (hash "0rqiha9g37h56bas92q2fvcizplvjvc0f04m4i9gb7aivnm9xg1i")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.0.1") (hash "0j87nm69hb1qzb639rx0hz529jdjn3zmh01sh6dqjdsvfqdksvsk")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "11wpbrqp2dm27zil580hc7169vi2vvba0mpjm6m131r5ldrjic2k")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "1b8mxmm4k05cddk8qh6ivmbh1na7gjs9dmvnnw512i4fkryb33x8")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "18ax0vl6plk45xwd1nwhzkb3c248z7ymgp6k90xrr0xx3l1mcbf9")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "0lh7kdblvzwlrklp4l2sx9afjr3d4l01fzwikrnn3m5jlmx5qv0g")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "02lmqxnbnqfxafcchy7i3w5nhw7vvl7f4nf6zlam8p28wnabh09r")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1avk2zqpacx2fs7vbgb5v00r76j6gkx6c9qsksklzfq0m98mcnrb")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.0") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.59.0") (default-features #t) (kind 1)))) (hash "1ka3p4m18h4cifxpas65qmp05x53s4pc1vsnbwzjwmcaf15z2pi0")))

(define-public crate-ul-sys-1 (crate (name "ul-sys") (vers "1.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.0") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.59.0") (default-features #t) (kind 1)))) (hash "0yn9bjmsndbi695m42r52m18908nkcf735j9s9x11639qkndmj03")))

