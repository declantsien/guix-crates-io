(define-module (crates-io ul la) #:use-module (crates-io))

(define-public crate-ullage-0.1 (crate (name "ullage") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "^80") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 0)))) (hash "1p57h99grmwjm4qw4g1k2ljynpd637ljzcqqzsahc1gydw7k13ar")))

(define-public crate-ullar-0.1 (crate (name "ullar") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "minimap2") (req "^0.1.17") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "segul") (req "^0.21.1") (default-features #t) (kind 0)))) (hash "146wv75ma6i1kkipqiba9yxx0fp8ivvwaxmqdr0nc56iddmfffl4")))

