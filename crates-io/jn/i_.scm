(define-module (crates-io jn i_) #:use-module (crates-io))

(define-public crate-jni_fn-0.1 (crate (name "jni_fn") (vers "0.1.0") (deps (list (crate-dep (name "jni") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xbvbxgxvsh3092y7wbhmfpwy03igij085bds7biajjjc8chw0sf")))

(define-public crate-jni_fn-0.1 (crate (name "jni_fn") (vers "0.1.1") (deps (list (crate-dep (name "jni") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c167f7gp6p6zkdsvp4vgffp5vzin152ddbhbp6nq4q21b061rgy")))

(define-public crate-jni_fn-0.1 (crate (name "jni_fn") (vers "0.1.2") (deps (list (crate-dep (name "jni") (req "^0.21") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ya2i4z7j1z5gsrqmi8ls0qxbgavxnlwksiwj8xksks2kbhzf9wh")))

