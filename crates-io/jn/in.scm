(define-module (crates-io jn in) #:use-module (crates-io))

(define-public crate-jnino-0.1 (crate (name "jnino") (vers "0.1.0") (deps (list (crate-dep (name "derive_jface") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jni") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1lzb1xsgl8f13s3r23fhh5g0f4gwi6c18nk3q0rbqfwn0nqw920x")))

(define-public crate-jnino-0.1 (crate (name "jnino") (vers "0.1.1") (deps (list (crate-dep (name "derive_jface") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "jni") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d8038323qa9y2z4rvdk006xmwpd8pbvxf3b1j1ppyb297182iv5")))

