(define-module (crates-io mr sc) #:use-module (crates-io))

(define-public crate-mrsc-0.1 (crate (name "mrsc") (vers "0.1.0") (hash "1f8h5kjdq4h5nzgk8g0z71wn5k30gfl3gp0impvla9qv2p1pv9xk")))

(define-public crate-mrsc-0.2 (crate (name "mrsc") (vers "0.2.0") (hash "002kd48rjdla82ycfma7f5wam5bjcb7vwniy0jv8inq8w257h720")))

(define-public crate-mrsc-0.3 (crate (name "mrsc") (vers "0.3.0") (hash "12y4pqbqlqfgyrvzy4r7iaw5gihhfgmi6rhm87wi75s03dw7nfs1")))

(define-public crate-mrsc-0.3 (crate (name "mrsc") (vers "0.3.1") (hash "19apgb22zyvnla4bgj1zbmh2gc56iy5w610dcm5nzw5iry40v9jg")))

