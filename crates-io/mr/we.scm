(define-module (crates-io mr we) #:use-module (crates-io))

(define-public crate-mrwei-0.1 (crate (name "mrwei") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "1ix1bbzyqah0xz96mpf5259lwa3mdzdi3dh812yfiy5d5xj3cyk5")))

