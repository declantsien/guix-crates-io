(define-module (crates-io mr -r) #:use-module (crates-io))

(define-public crate-mr-regex-0.1 (crate (name "mr-regex") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "id-arena") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0lmcv8042hnjaazg651m512xdqv0bkxw0r482avvfrxwf6y9hpra")))

