(define-module (crates-io mr sl) #:use-module (crates-io))

(define-public crate-mrslac-0.1 (crate (name "mrslac") (vers "0.1.0") (hash "194xzrm1s21220khc75p2gxg56hgc1g7qgxgn1b4i0fplxzas3l1")))

(define-public crate-mrslac-0.2 (crate (name "mrslac") (vers "0.2.0") (hash "0678riw9k92b6g16infrykz4c154fggi2sdj71ca5kkdd9wh71hn")))

