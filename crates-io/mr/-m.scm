(define-module (crates-io mr -m) #:use-module (crates-io))

(define-public crate-mr-mime-0.1 (crate (name "mr-mime") (vers "0.1.0") (deps (list (crate-dep (name "autocfg") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "intern-str") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jj22yvqq9c1bs7dx40a36fb0m0c1l6dnn55sigz8sixbgk93i86") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mr-mime-0.1 (crate (name "mr-mime") (vers "0.1.1") (deps (list (crate-dep (name "intern-str") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (kind 0)))) (hash "17ay0hdrdkhmamfzl0jfn0sjw68kdhppn230qfl542pw96b59566") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

