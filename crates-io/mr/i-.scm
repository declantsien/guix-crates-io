(define-module (crates-io mr i-) #:use-module (crates-io))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)))) (hash "0nhckj838zmyy0647kd2qi4pmanl6xjc8x1mk0s5rghbfg2z3cna")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)))) (hash "023zc76rn3f9k52vkrc9aq3djz8wg2qb0qig7sc11hnl73541jd2")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "16zjzidcywxck12d566f0spf5vwkqkpr8rf24q6bns58282vhgj5")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "084g7zi2xrvjsrm69vy2pca3xpyfn1vkqk3l5wlkh2w9kml4f3cb")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "1xh6ysq08dpcgwf64176b2vyldc761dd8p98lns1pn8fhwb8cngi")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "1k6fnlx90yhclpvvh4dhcqyygj6gayhvdjp1hpm623mk4h0qnvmr")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "0nyzi5lzq487cabfm6sim50mkq99n2gnwwf2fyf6mrhhsjcammq8")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "0w75kjm4h074qhfsa13d5gmlwzqy4nsqy3j0bi6d6f5w1b5grcqa")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "0r7qxa3jvnjzdxik3v87yvahacqc6gidrkrn1krcxsvaz9ysc0db")))

(define-public crate-mri-sys-0.1 (crate (name "mri-sys") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "0aq7xn01i7l1v7pl1n9pnli69chjm5d889jfb3vl0p2ngg937wli")))

(define-public crate-mri-sys-0.2 (crate (name "mri-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "166xxi9f0qxpj156mfqrsd0dh50xmda6s2vhb5ajj92ybw21xn8w")))

(define-public crate-mri-sys-0.2 (crate (name "mri-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "10naphija1gddhb714gvk71pybnbl25kilhv666hwqznbzk15k5l")))

(define-public crate-mri-sys-0.2 (crate (name "mri-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "0mnqm3fgw90gp7aicnzvhpa9mgjk0bp643zd97bqx4vhz05g245z")))

(define-public crate-mri-sys-0.2 (crate (name "mri-sys") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "02939l2qjw183m6k232iawhags6b19qr60kfgympvwcx4dwmb0xd")))

(define-public crate-mri-sys-0.2 (crate (name "mri-sys") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">= 0.2.11") (default-features #t) (kind 1)))) (hash "0fcgdb3rb6skvy82alhyppyg9pr069z4yll1szdan6wzdl4mxh6m")))

(define-public crate-mri-sys-0.2 (crate (name "mri-sys") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req ">=0.2.86") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">=0.2.86") (default-features #t) (kind 1)))) (hash "12ajhgl744igrsxdngjpr0cr2sjjlaif9pcsyhf2fb38jw1hp1p7")))

(define-public crate-mri-sys-0.2 (crate (name "mri-sys") (vers "0.2.6") (deps (list (crate-dep (name "libc") (req ">=0.2.86") (default-features #t) (kind 0)) (crate-dep (name "libc") (req ">=0.2.86") (default-features #t) (kind 1)))) (hash "0n2gnlljkqqvzlvnlxf0w0mhla9w15m7cbbsqw8cyiarghcgs5ww") (features (quote (("helpers") ("default" "helpers"))))))

