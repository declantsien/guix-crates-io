(define-module (crates-io mr do) #:use-module (crates-io))

(define-public crate-mrdo-0.1 (crate (name "mrdo") (vers "0.1.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "02agf9jgpg7nfppshic60bzhnffpkf56zdinlhajrnl09zf9cg26")))

