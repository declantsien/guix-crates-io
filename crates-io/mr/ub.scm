(define-module (crates-io mr ub) #:use-module (crates-io))

(define-public crate-mruby-eval-0.1 (crate (name "mruby-eval") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "1vikagmkz7bh4pm1zd7qia7dpx97p29yp8l7qqifra3i8bkj3x29")))

(define-public crate-mruby-sys-0.1 (crate (name "mruby-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (default-features #t) (kind 1)))) (hash "02z6nv9971af8p9h3yn1i3xvxz330gs16xzq07rdld5vscfa0qry") (links "mruby")))

(define-public crate-mruby-sys-0.1 (crate (name "mruby-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)))) (hash "1whp3lbm86mrflzxjfnrjc1sbsqdqg5rl7w6rjcnlwm0c6d6ia53") (links "mruby")))

(define-public crate-mruby-sys-0.2 (crate (name "mruby-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)))) (hash "0avgq35yb0w7c8996hp6h14a0bi0w38plhd0nnh00xwwr6g1d01l") (links "mruby")))

(define-public crate-mruby3-sys-3 (crate (name "mruby3-sys") (vers "3.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "1xncbd678iz6701ii73m0ygmm711gr79nmn8v1l3pdlpcpcdls23")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.0") (deps (list (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "04r5wr4fkaqxbllq39wmx59swr9m7g61kg3l8gfxp1ggpnr0ydl0")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.1") (deps (list (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1sbjmzq507kr9h8gy2j6n73ccwl6m5zl9vxrdpfzpvxkzz5g9i74")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.2") (deps (list (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1z8ybqs9ynql50pb19b2j952f3z4p6h4vjff065kb9drfvxx2613")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.3") (deps (list (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "11g61mvw8caprlzv99v5wkpj7jh0id1057k4c199lwwpjg8p86l8")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.4") (deps (list (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "15c9si8ygv888hswnsnmhwbbgy0zpb2v2k81j57jq4kjfr4n9wbz")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.5") (deps (list (crate-dep (name "getrandom") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0qb6sd906g7srfd7avr4pcp3rxmg7fw2296x1yyz1x6y5n3aqgsg")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.6") (deps (list (crate-dep (name "getrandom") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1igyv624dm7x7d9x0zkwn099iymvsd425020jyzirva1a2c8cdyh")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.7") (deps (list (crate-dep (name "getrandom") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0g88y6v9nq4nv37m4i50m2sn01a9343fsawffyg1npaxmrszc2xv")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1ffy0mj6plvk5yncq7cx4646zafmfh2kfz09s3s2qzc98h739mih")))

(define-public crate-mrubyedge-0.1 (crate (name "mrubyedge") (vers "0.1.9") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0bwqdi55c8r5j7cvaqc6l0ki190pc8dnldnha6zp9ijha2vf43ix") (features (quote (("wasi" "getrandom") ("no-wasi") ("default" "wasi"))))))

(define-public crate-mrubyedge-0.2 (crate (name "mrubyedge") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0nmiyx3fz6cay761cm815fqbvib2q28g5avpp23b7b8y9n75icfx") (features (quote (("wasi" "getrandom") ("no-wasi") ("default" "wasi"))))))

(define-public crate-mrubyedge-0.2 (crate (name "mrubyedge") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "191r87mvlfcjnw60v0gmmv6lrcjgcw96s0damidmyhdqp2x2lcaq") (features (quote (("wasi" "getrandom") ("no-wasi") ("default" "wasi"))))))

(define-public crate-mrubyedge-0.3 (crate (name "mrubyedge") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plain") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "simple_endian") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0ny4swyyibzyp4qldx4177vvd8ylkpgrc0d9y15slw49snbzffjj") (features (quote (("no-wasi") ("default" "wasi")))) (v 2) (features2 (quote (("wasi" "dep:getrandom"))))))

