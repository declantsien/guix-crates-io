(define-module (crates-io mr aa) #:use-module (crates-io))

(define-public crate-mraa-0.0.1 (crate (name "mraa") (vers "0.0.1") (hash "0d70kjmrwssj4gnpqy88vw16ah21lsv176fcynxpfjyd9rc3npjy")))

(define-public crate-mraa-sys-0.1 (crate (name "mraa-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52") (features (quote ("logging" "runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "070gh92ivqa19mxhagjna2jfxmqk9rw57igp0nx4vr5z3b7ccpp2") (links "mraa")))

