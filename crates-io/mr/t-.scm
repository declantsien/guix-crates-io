(define-module (crates-io mr t-) #:use-module (crates-io))

(define-public crate-mrt-nom-0.1 (crate (name "mrt-nom") (vers "0.1.0") (hash "1psbrybgssj8g70jdmz8p65lc3ywlfy5as55knwk23w75hhpmz0l")))

(define-public crate-mrt-rs-0.1 (crate (name "mrt-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0r6nk124r3rxil1vyiml5ni70hfxln0wwp8x3spsvyab9s47mcjy")))

(define-public crate-mrt-rs-0.2 (crate (name "mrt-rs") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "1qr6b64xj0qw2g6bbxglk96cxmc6chjrg6id000s23gmhglxjkas")))

(define-public crate-mrt-rs-0.2 (crate (name "mrt-rs") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0sna2wykaymadyicyidaykbq1chafgj1w58hr3wg1x05h5m6dr64")))

(define-public crate-mrt-rs-0.3 (crate (name "mrt-rs") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "1ir2yjgpahf8hs5zx9n9dc96aidxrwjwz6783dh6yydz7iq116i8")))

(define-public crate-mrt-rs-1 (crate (name "mrt-rs") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "1j2zrsrrdpxqjb8jckfsbzgy78qxsx6acw0yxsnqyqlvcm39634n")))

(define-public crate-mrt-rs-1 (crate (name "mrt-rs") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0yz12ni123aghdwcncz5hi8lis7cbkl161ycvwi4haa6xzvl7yy4")))

(define-public crate-mrt-rs-1 (crate (name "mrt-rs") (vers "1.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0v4nvbvhmifa4z785nwhzf9x9lbxcdi91b8kx3p74zigh0ga4zh9")))

(define-public crate-mrt-rs-1 (crate (name "mrt-rs") (vers "1.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "15p53amspg3lgzf8dx9m9m52y0cjx4vb1cq6bn246hffi6fqi8yi")))

(define-public crate-mrt-rs-1 (crate (name "mrt-rs") (vers "1.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0fp8njb4j44jn2jlripklpvmkrrh6asad5i3dbm8nxkiadrw7xmk")))

(define-public crate-mrt-rs-1 (crate (name "mrt-rs") (vers "1.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 2)))) (hash "05rlfr7nig6qzj0ms27cyxip67vzj08li66qjxw3a9wpybgk0h1y")))

(define-public crate-mrt-rs-2 (crate (name "mrt-rs") (vers "2.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "0gfxcbv47nj9p1ha8sbm7s99dnm9li9r4vwblcikma7y0jm14dfk")))

(define-public crate-mrt-rs-2 (crate (name "mrt-rs") (vers "2.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "1pik04wdr8z5yzxr7snzm9icwmbfz2s8jbf4lxnid2hfm7llr3lq")))

