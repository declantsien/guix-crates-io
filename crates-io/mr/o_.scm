(define-module (crates-io mr o_) #:use-module (crates-io))

(define-public crate-mro_mini_grep-0.1 (crate (name "mro_mini_grep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l246bfkxj5wzssppdcmhc1an7xim9g0r149s9p8yydbv22c51qh") (yanked #t)))

