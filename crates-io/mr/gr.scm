(define-module (crates-io mr gr) #:use-module (crates-io))

(define-public crate-mrgraph-0.0.1 (crate (name "mrgraph") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "meritrank2") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1zxmxklp0ygzw9hdkizbhi5d3k8w9vsfp9sa684bl9z9b5wh73x6")))

(define-public crate-mrgraph-0.0.2 (crate (name "mrgraph") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "meritrank2") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0sdiybgkj8fvady4w8wy8yh42ma9vrzsqck30w584s3m9ch945fg")))

(define-public crate-mrgraph-0.0.3 (crate (name "mrgraph") (vers "0.0.3") (deps (list (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "meritrank2") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0isdlkd7ysr96gmyd2dasj64kar72h5pccqzyw2dbsnv14dhxfka")))

(define-public crate-mrgraph-0.0.4 (crate (name "mrgraph") (vers "0.0.4") (deps (list (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "meritrank2") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0m4nckq74ryv7x13hfyx2wk039h0zf1qay48hdmpi5dg9pci1dsg")))

