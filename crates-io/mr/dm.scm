(define-module (crates-io mr dm) #:use-module (crates-io))

(define-public crate-mrdm-0.1 (crate (name "mrdm") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "07dhvca5qwkb6fds6xa14w4jxz3gwaiy5nyfg3ijr6bj5fh5gpr2")))

