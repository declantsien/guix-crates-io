(define-module (crates-io mr us) #:use-module (crates-io))

(define-public crate-mrust-0.0.1 (crate (name "mrust") (vers "0.0.1") (hash "0lzvwwrphi229f0i5sxvy44m67rr6vdafhbaqk7jf7n3x57mmpwl")))

(define-public crate-mrusta-0.0.1 (crate (name "mrusta") (vers "0.0.1") (hash "16dpn08kpy8csjsh25pfcx6gc6asragmj0w8brsjy0ww0xiik879")))

(define-public crate-mrusty-0.1 (crate (name "mrusty") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "04nlsbzjvrsf401pwpjn7wbykzvk01ssgp509m78z8srd3j77dcn") (features (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.1 (crate (name "mrusty") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1dcy7ri8jhj8nv7yl1fcasxi6nrxnx5ysaavwv6dijjslyvijbpj") (features (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.0") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "10451kk7596xklyw5694c4whyzjrzizgag6jl9kbgw60z6z815vy") (features (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.1") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1qg32jkz0y0aph4p9vj4wcvwm298rzhaljfzpxqgg0467m3ivh6j") (features (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.2") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1nxnhzwgh6636a3wx4rg17vwbvm5q064x11qzi53i86lhv0wl8m7") (features (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.3") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0f0qw8676chn45gmgwbwl90fpgppl8dixxdqf9wkx689bfnm6m8j") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.4") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "16m4q999jxa0cq78xg6csmxcj7jjz7av8l2a02pz20zcwimxgr2h") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.5") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0bgmjs0mb9vf3ahzwcyh1x6k8jrrkb2qyay9n48i8zirpkripzx3") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.6") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0b8wvxasja96qcjv3rkx7vz9rligw06cgnv24mxj9ivldssinfpv") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.7") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ckq9hrxlrkdf89pbm6698b4dck3hsgwaf409i8yyxaa6xm47a5m") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2 (crate (name "mrusty") (vers "0.2.8") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0i16122pq0cqqx6ckq8jmlqmm33kg24wby0anv05aq5419x6qmfd") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.3 (crate (name "mrusty") (vers "0.3.0") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0px3571pp00kwz417cjwx6hzv3r5bhzcbfqkpsd0p5c4j0w949nl") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.3 (crate (name "mrusty") (vers "0.3.1") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "0n7q4kbl35lyr80z845crkrql5070pj17mh0qvr3ri87vn7640j6") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.3 (crate (name "mrusty") (vers "0.3.2") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "1rdcvsx0fqjm3mrsqmyc6qcdykng8lw1njl95bp6xqp9p1f2jxs0") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4 (crate (name "mrusty") (vers "0.4.0") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "0ma188ql9vl5jw14zdp9ffhrpw1sxn3vy7l6zk95r8k7yxs5nra4") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4 (crate (name "mrusty") (vers "0.4.1") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "1vpr4dylzq1rxa3pqbgxrpbf2wlx00g91jir9s5n1bhalw6zr68c") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4 (crate (name "mrusty") (vers "0.4.2") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "10amfpsa1494pvhpq0xrhx4v3psdir21bj383ilkpm4ph6hjw6a5") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4 (crate (name "mrusty") (vers "0.4.3") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "1kfhg16g38238knq8zx4ix89fi0ym1pz0s9wvmaygr18ycjqx115") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.5 (crate (name "mrusty") (vers "0.5.0") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "0kxjg8kzggn15kp3zndbllyh5df9v62c3c51qpmwx2xkyndqiq67") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.5 (crate (name "mrusty") (vers "0.5.1") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "18xdm63z4s8isi5fbvzl3afd2g9hfni81zg58m4pffmwyinsh10b") (features (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-1 (crate (name "mrusty") (vers "1.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3.22") (default-features #t) (kind 1)) (crate-dep (name "rl-sys") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.4") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "120rqfw4canh7sddxqs5w81gdsd910y4gh5z5mmkf88c66yp6kgh") (features (quote (("gnu-readline" "rl-sys"))))))

