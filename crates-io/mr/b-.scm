(define-module (crates-io mr b-) #:use-module (crates-io))

(define-public crate-mrb-sys-0.1 (crate (name "mrb-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req ">=0.55.0, <0.56.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 1)))) (hash "0fwq4038v89ma7sxj9vwn7mxyss357lfisqsp962yj5h9fj6a0n6") (links "mruby")))

(define-public crate-mrb-sys-0.1 (crate (name "mrb-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0z992r6bkc0lm5sm9by8y8xckaag1ycrm8wvd4c2im9p4ldwap1j") (links "mruby")))

(define-public crate-mrb-sys-0.1 (crate (name "mrb-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "149x7cwlg6rs48k1vkagmnjdyj5xvvvsmw65hy4yca8kcpc15vpa") (links "mruby")))

