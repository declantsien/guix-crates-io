(define-module (crates-io mr t_) #:use-module (crates-io))

(define-public crate-mrt_parser-0.1 (crate (name "mrt_parser") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ip_network") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "06iz6m5xbmvas56lb3ga7aw8c07cxgrk2cl8lg0lygw5r1wxzh82")))

(define-public crate-mrt_parser-0.2 (crate (name "mrt_parser") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ip_network") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ckxjv6qznq13rxbn92dj8j8zyh6is8m8p37id64k4n76jwp7wy6")))

(define-public crate-mrt_parser-0.3 (crate (name "mrt_parser") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ip_network") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0irb6fkpi7crhr11xz3r7k55f7wfqqsgk5a189lv2ajm53icpq89")))

(define-public crate-mrt_parser-0.4 (crate (name "mrt_parser") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ip_network") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "080i86y9lr62q9z7jl31n1shj3pknd679f5lali72dc5p6ccdavs")))

(define-public crate-mrt_parser-0.5 (crate (name "mrt_parser") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ip_network") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0yqfw2chj9mm4kmdl2kvxn2ydpgxkmvv381qvhj22q9z9zldyqg5")))

