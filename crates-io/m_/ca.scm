(define-module (crates-io m_ ca) #:use-module (crates-io))

(define-public crate-m_calc-0.1 (crate (name "m_calc") (vers "0.1.0") (hash "0i6n0sz4l8cq59i0l4qad0ysf5z06bg2vcsgshw6449fd3wm3dk5")))

(define-public crate-m_calc-0.1 (crate (name "m_calc") (vers "0.1.1") (hash "01kqfn9wpi6yjv12flwil5l16vn2pr74lvbbm5z6dxzv8ijidyyy")))

(define-public crate-m_calc-0.1 (crate (name "m_calc") (vers "0.1.2") (hash "03gikasm09638xam955vb7l6kgrplsa8rsh8gpn8zcr5dmn4c92f")))

(define-public crate-m_calc-0.1 (crate (name "m_calc") (vers "0.1.3") (hash "0cbrzl6hcy33046hfx21p1y2yr9sm4jwmnrz768gvjca0hlz2fj9")))

(define-public crate-m_calc-0.1 (crate (name "m_calc") (vers "0.1.4") (deps (list (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0iijcjkfxq93kamm9fbaj05i2a6m5mpxyqn588q0hjsssj10l5q8")))

