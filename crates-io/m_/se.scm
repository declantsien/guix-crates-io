(define-module (crates-io m_ se) #:use-module (crates-io))

(define-public crate-m_server-0.1 (crate (name "m_server") (vers "0.1.0-alpha") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1bcbc3mjfj5mw6kvwgcs39zf42avyw5kfnjy2czkiyf52jnxg6cf")))

(define-public crate-m_server-0.1 (crate (name "m_server") (vers "0.1.1-alpha") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0jmxp3gxj7g6xh7j1j0qa1h9x0qlmmb7rmmn5a9yv5vmh61kqhbd")))

(define-public crate-m_server-0.1 (crate (name "m_server") (vers "0.1.2-alpha") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1m2cfqgh02bm3n05z59jvhplfr498x799syv48p7v8b4w4csrdrs")))

(define-public crate-m_server-0.1 (crate (name "m_server") (vers "0.1.3-alpha") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0nynwklim164m52ykrnrrs7ap9pdgxq8fpqn9xkw4nd8rjdiwka3")))

(define-public crate-m_server-0.2 (crate (name "m_server") (vers "0.2.0-alpha") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1hnzp7zhizwf4a6if99jap4cbvxwqs992j5z8yqf7g5awqbqfyqi")))

(define-public crate-m_server-0.3 (crate (name "m_server") (vers "0.3.0-alpha") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "09j6m4gpgxzh4nymmkafjxcwd12h15dvc331i4j8mj9kxa705caz")))

(define-public crate-m_server-1 (crate (name "m_server") (vers "1.0.0-alpha") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gy3kg6qj3l6bfam4ifvknb05aviq0l1vfw4nwzrfjrmwbs833ki")))

(define-public crate-m_server-1 (crate (name "m_server") (vers "1.0.1-alpha") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1530b6m46gxhf48jx20v1xr4vh5a76xmwavcbd9dgyr7gji3qa7v")))

