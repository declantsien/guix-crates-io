(define-module (crates-io m_ le) #:use-module (crates-io))

(define-public crate-m_lexer-0.0.1 (crate (name "m_lexer") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "19fd9mc4qa81prh6g7mpas8n6bq94k8mpm4ysnqgjhj01dvrhfnb")))

(define-public crate-m_lexer-0.0.2 (crate (name "m_lexer") (vers "0.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1j579izdmyv4yxhyvh6w8pzfjsmg79bq88d2dyj5zm97yk05k4cd")))

(define-public crate-m_lexer-0.0.3 (crate (name "m_lexer") (vers "0.0.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1wz9saa4wdv4q2vq23g2ab2jbs48a5wjhzb23w6qczjpqnyrvys9")))

(define-public crate-m_lexer-0.0.4 (crate (name "m_lexer") (vers "0.0.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "19v7hk4i3avgvmhhv26bf5hjfjpwkrvy81dfbdd5hb8nj6zixrd7")))

