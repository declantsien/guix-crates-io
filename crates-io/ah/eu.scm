(define-module (crates-io ah eu) #:use-module (crates-io))

(define-public crate-aheui-0.0.1 (crate (name "aheui") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rpaheui") (req "^0.0.1") (features (quote ("clap"))) (default-features #t) (kind 0)))) (hash "1zqdy721ka9vvngavyznki2g4wr9k4jgdql8fynhk0sgb81cjz9g") (features (quote (("default" "bin") ("bin" "clap" "num-traits")))) (rust-version "1.67.1")))

