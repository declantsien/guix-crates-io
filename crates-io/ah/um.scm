(define-module (crates-io ah um) #:use-module (crates-io))

(define-public crate-ahum_dialoguer-0.4 (crate (name "ahum_dialoguer") (vers "0.4.0") (deps (list (crate-dep (name "console") (req ">= 0.3.0, < 1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2") (default-features #t) (kind 0)))) (hash "05fs2r9ygrlgzzyd2461hsdbrmpi16qhnyswsbkkx380nyl8wpnh")))

(define-public crate-ahum_dialoguer-0.5 (crate (name "ahum_dialoguer") (vers "0.5.0") (deps (list (crate-dep (name "console") (req ">= 0.3.0, < 1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2") (default-features #t) (kind 0)))) (hash "1bvm3p62yvcqx0ikqdif3brg7k0ga83y1c1h89vhff7w9dvk1fbn")))

