(define-module (crates-io ah re) #:use-module (crates-io))

(define-public crate-ahref-0.1 (crate (name "ahref") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1bj1s57rky4mlkrg62gvlf42asd7nxx1cfrislccik2lla4qlcq3") (rust-version "1.72.0")))

(define-public crate-ahref-0.1 (crate (name "ahref") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1mnd4i77wg2bkpf3lhbz16fc304f3llp9cgqyvyq745g8rprxibz") (rust-version "1.72.0")))

(define-public crate-ahref-0.1 (crate (name "ahref") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "0kb3y640x02sign0ckmk4h2zv11g6swfk9dyz0rhav6a9mq8y0za") (rust-version "1.72.0")))

(define-public crate-ahref-0.1 (crate (name "ahref") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1sxq7y9l43spqd97hyk3kn6674nyrclkzqa2v0d27w3k7ms160hz") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1w61ld6mg84c38hxyk5jych9lsm8jkx2cd735qf30chhv3y7kllv") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "0yra8b8vm28441v4awk3d16k8pm3hhq0bcykbsf40xvlm1qg9x2d") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "0gx59hx8h6d17p1sp1c0qhyhg9810wkjgmwqy70h6v0s64h3aq7f") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "05afshs626x7mrnhkkk9k71rql60gv18sr9ry82a3swxz9b3gf75") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "19wb4lfwl9ww3mscwf33zlisijd8z3z4308lqgx1hlkzh0za2y51") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "198z8hyvw4sklzazv02kbx9x9375ayzrvd5izqz7w9zk0xwvzk7m") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1h4dhx83rw4vm357d3vnp0jds5i10qwvbh2pwp1xs187zcd836sv") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1fsfj3vsrf44jy29w2v16djqs4i86ilpfqxvq1w9iz3z570zcayd") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.8") (deps (list (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "tagparser") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1k0vn56f06nbc48zf2k6vzbbmimj33f9gh4czpj66k501wwb9xaz") (rust-version "1.72.0")))

(define-public crate-ahref-0.2 (crate (name "ahref") (vers "0.2.9") (deps (list (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "tagparser") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "07pz5zz3qz93wg2cw4xzrigsrdb2992ffnlvdmmcvazcm3hl4dcn") (rust-version "1.72.0")))

(define-public crate-ahref-0.3 (crate (name "ahref") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "tagparser") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0kjd8jpp1qc15j7msmqvr50pqhr7inha7w7gwzcgwkddckyfy9rr") (rust-version "1.72.0")))

