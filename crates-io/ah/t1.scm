(define-module (crates-io ah t1) #:use-module (crates-io))

(define-public crate-aht10-0.0.1 (crate (name "aht10") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (kind 2)))) (hash "0d7g5nl31w652b6zvaxhds4nm41nkflhk444dbxrf0f2j54m09wl")))

(define-public crate-aht10-async-0.0.1 (crate (name "aht10-async") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "=1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "0qgqsmyic729hjadr1nkfkf1y0m2mb3ijndzcm5kak19s4abmzbx")))

(define-public crate-aht10-async-0.1 (crate (name "aht10-async") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "03v9zsf56vkw8rh8ih6ggdglhd03pgqbnrl6w1b7r423ci823ywf")))

(define-public crate-aht10-embedded-0.0.1 (crate (name "aht10-embedded") (vers "0.0.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7.6") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "embedded-alloc") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "rp-pico") (req "^0.8.0") (features (quote ("rp2040-e5"))) (default-features #t) (kind 2)) (crate-dep (name "usb-device") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "usbd-serial") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0vxs1czls1iqnfskkf38d5xq9fgqs39k3kp1fknzfny7ray0awfw")))

