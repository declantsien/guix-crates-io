(define-module (crates-io ah uf) #:use-module (crates-io))

(define-public crate-ahuff-0.1 (crate (name "ahuff") (vers "0.1.0") (deps (list (crate-dep (name "endio_bit") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9") (features (quote ("const_generics"))) (default-features #t) (kind 0)))) (hash "14244raw7zzr3c8w4m4gb4ygik19fqa4w3qdhdvdd26flxr6zb31")))

(define-public crate-ahuff-0.1 (crate (name "ahuff") (vers "0.1.1") (deps (list (crate-dep (name "endio_bit") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9") (features (quote ("const_generics"))) (default-features #t) (kind 0)))) (hash "13qp3hcnpi8if20nb55cnq5c28r62cm6daahz0h7lpmxa90l79hd")))

