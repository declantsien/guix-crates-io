(define-module (crates-io ah sa) #:use-module (crates-io))

(define-public crate-ahsah-0.1 (crate (name "ahsah") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0v22rvb8j38mbj8i390hqsn69kgy2pad0rycq7pb378pjp2hdiah")))

(define-public crate-ahsah-0.1 (crate (name "ahsah") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1dnljg2c5874vsyppf8yyxf89809cf043ljxvgkj3402p9xfmv1h") (v 2) (features2 (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1 (crate (name "ahsah") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "023gifvk0mf17pyyjzqkfj43kps122c6rf1j50adhpnpnwivqxz1") (v 2) (features2 (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1 (crate (name "ahsah") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0634vm98s4jvia3nzw73l705nrydjnmsshwazzfd8s8hwxyhd5z7") (v 2) (features2 (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1 (crate (name "ahsah") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0kl7hccnwgi2d8f2rnpk370wjk8m7c5dsa0j9df8scmhrglx4ckd") (v 2) (features2 (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-0.1 (crate (name "ahsah") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "10s0ynk1204805j1q1yd5xp4m2yvywxbgabgzvxhj0c3m9w6xivj") (v 2) (features2 (quote (("args" "dep:clap"))))))

(define-public crate-ahsah-1 (crate (name "ahsah") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "02y742gi8q27m88dwi2k3vr09n1rgnyqxs518wbj9nk483i3k5bd") (v 2) (features2 (quote (("args" "dep:clap"))))))

