(define-module (crates-io ah ri) #:use-module (crates-io))

(define-public crate-ahri-1 (crate (name "ahri") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1msq9nqm8a6c72wn86s76ala38a82d89njfj94d4amkqfhya9dz7")))

(define-public crate-ahri-1 (crate (name "ahri") (vers "1.0.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "mysql") (req "^24.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0a9wnr1qaxb1q816z27idbhflbz54flgh3q9kpl72b6id68i8dbs")))

