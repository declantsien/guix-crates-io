(define-module (crates-io ah ma) #:use-module (crates-io))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "18amp8ss9jfawv2kxdh8n2spnc7px8967fakg33ncab1h35kwj2c") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x5zh78xdpvmiwggxx5phj9n1l385mprnvh63hs2psg086hks3az") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "157yyvaablgx51glxzk0fa4jljgjbgvly4h0xrzj6pgr77na8jaa") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "122ixp56b3gvph5zn0iqghqqhdvlrddi6cz92qxq78g6hf4pcgjw") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11a6qgdfd9x3sp5rif6ff9jwapraznmihdzc15yynn9lwny37ldw") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "03m3cq066sy54zp22qd5cdy9j5km77mxp1f99n91q9mvqcaf6xdf") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1683yc7llwm7mlbn73703hb1zmpj21z36avb601g7icf5mkv85ip") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z0khn25mv3kk4anghbdfjc5my172w25rhw29hblfrnqglpi58cj") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1392w1j2r7dy4rdhgd7h2ghqjpmbk74fi4ssyp0nbz8sqrxqy9ry") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02dnrqsqs5cs2wcvcv881xzq94b9f582y0dg1n8hs8yyv7gx1356") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.10") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j57cbaim786c6gawl17z21yjnaykv6r5azw8lmyn43jhydrjslj") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mvy38a5y9f1nc2cb4lwjhjj6ad07kxwgcdbzxk6m9cd8hb06m2f") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mlfbj7lqsgvlbmr1cn9yb8bxymif7p824chqnlky87s9nnrhvcn") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v5lj91p5c940fqzx7xvl5apkja4s4r779ykr814rm6aw07zk33w") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kl6s2qwnpr4ig9anqqg2p3wdvh23ga25havqv9gb6zznfwy4ayz") (yanked #t)))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.15") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1plj3j3l5m16ry8cccykryp0nzw769p1lg3dxs7rb3q7bi1pjxnf") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.16") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ph54731qc9qinxxljz04r76n0i1fz037fb9dc42scsksibf0sbf") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.17") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k5wc0klkhw4zfldzwv28ahazdpqz6qyhp73vf5czhl53h25nvki") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.18") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "183xv0yq3wmd74ys5vnr960lklaypsmw9z0dv6lik27qc7w13s99") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.19") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wa4da2vsymacz88d228lhg5v4w8s1a06sq0d9s3c27nh3y0xw1h") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.20") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f71dsg5d7ykcj98dbkdaq1avf0aa9m0zv2c139hgh2h4z51zy0x") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.21") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zlla49nkf4547f497c95mcg99ca4n8sxjw25wj4pi6n7vw51q8m") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.22") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bf54g35r1mm5bbf6ak4lflibzh1bidhhcq52w1acv3q7dg27k7y") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.24") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gdprfkdpiygsk4x8j8iljmfqxbq9rr4m6mylrcl8679g8csx4l6") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.25") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wqq42lq2m1km3m7cyandxw3p94d599bzkbdbcknns00ncc93lif") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.26") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i0chd0l9p60x740c1nd0cc7rw8aqdzlx8z72sh05wxnd6shfixg") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.27") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g1nx21k27azfwr4snfq8hfw7dcnq7v4c6x6hm7igndlhndqkbri") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-0.1 (crate (name "ahmadtest") (vers "0.1.28") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bxb7qy6sg2spp9by0hi9fmqd4n6q4k3yhwvs84frlz942vrwy6r") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "14hqw8kf496mimvh4nmp974ki5icqm8x04mgmz0ij3szjb4d06bk") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02w3csk3ak81gllpn08x64m9c0j3bik00v27ipayj40ys6r8sc6x") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.8") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "08y22fvrz9j63gdyfq1wd5dnl84008rm7i4l4aqyng2gmrf1yw5l") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.9") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jn9m3s150a97k7dfp3kgrgsj2y9wil4b2yy8lmriy9lq1d1kkd9") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.10") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mndrp8i2s3lya6kjdp48f0qb36q9g8h6ndgnkbyzmm15k52jxdn") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.11") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c3hy4piyilahf5hq2jpp5hh04lz4p6gnc844vl7j7vmqg062brm") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.12") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q9d0l9xcp6w4xwlrih99pkvcxr3fr3c060wnvs70wwr5wnv7knz") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.13") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z2xxda1ygb9m50dla40w8im0jc2jvcx5v2s5vay06gzw774dl7q") (yanked #t) (links "LexActivator")))

(define-public crate-ahmadtest-1 (crate (name "ahmadtest") (vers "1.2.14") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04zalrfhy1khcr2dwhcgmxri7z6c5d581a4189a7ml8r5ff8mkqq") (links "JustCheck")))

