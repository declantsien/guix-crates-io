(define-module (crates-io t4 ru) #:use-module (crates-io))

(define-public crate-t4rust-derive-0.1 (crate (name "t4rust-derive") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0csd6sf35s2vwq13dqdx2390csaa52x2286692m46826x6lqm2aq")))

(define-public crate-t4rust-derive-0.1 (crate (name "t4rust-derive") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bwhrw0jk7gb7kgna63cndqs5k7cbpgpvnmz9gh2m13d6zviclm7")))

(define-public crate-t4rust-derive-0.2 (crate (name "t4rust-derive") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13k01yjm8wlrh7v9k0j3piq3ps0vgra6qh0szq4b5cs7nbplznh5")))

(define-public crate-t4rust-derive-0.3 (crate (name "t4rust-derive") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cd4r11yn1jpaxh0ih6rxisnqxkljrk8rr4j50ayjihkjdr56yxr")))

