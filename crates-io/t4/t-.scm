(define-module (crates-io t4 t-) #:use-module (crates-io))

(define-public crate-t4t-games-0.0.1 (crate (name "t4t-games") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "t4t") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "0as2k6zgwc0v1a7q8nljcf65ybaccy1yik8z1kd5g017wx2598b3")))

(define-public crate-t4t-games-0.0.2 (crate (name "t4t-games") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "t4t") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "18rzsd8glygcixb31s671njg403b4f9l52kdgzqvh32xwzi9sn4b")))

(define-public crate-t4t-games-0.0.3 (crate (name "t4t-games") (vers "0.0.3") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "t4t") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)))) (hash "1q667k4vpa1fql91vfy2k80cxipk15s0fg18ajqis7nsr7ifbq27")))

(define-public crate-t4t-games-0.0.4 (crate (name "t4t-games") (vers "0.0.4") (deps (list (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "t4t") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "test-log") (req "^0.2.16") (default-features #t) (kind 2)))) (hash "0k0l2i9plasi9i570lfaarpk06qg8md0w8c2y52g96r9sq2g6g3h")))

