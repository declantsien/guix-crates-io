(define-module (crates-io t4 _i) #:use-module (crates-io))

(define-public crate-t4_idl_parser-0.1 (crate (name "t4_idl_parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "nom-greedyerror") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dyn58dwcq3a7ahnqc89729v44nm5n2bjnq0423lsyf6g3j9wp36")))

(define-public crate-t4_idl_parser-0.1 (crate (name "t4_idl_parser") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "nom-greedyerror") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jbx2fya7zfc6djm8y02b7y5rivmgcxaa730hkc40i44v4cr2wff")))

