(define-module (crates-io ci p_) #:use-module (crates-io))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.0") (hash "1c6282l2cc06y8kf9iizs2dghwb00mjy3sbanq56agpv2y6hl3i0")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.1") (hash "152hpyv8dvs591gf8ahl9ld69hrv1bnigqmlf80yqrz7j5fp8qas")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.2") (hash "1nph4x0554iaw49y56j4j5vl9x6p9jf2hrsdg8i0xjih9p8gs0n1")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.3") (hash "0iikly33as7y3mvp1vfh82vmxaxri246yvzk3pzf7rvwhhpqy1bm")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.4") (hash "0hjg60bi6dvhy60vdr7yvb8yg5s7hizwgg5p34fb79bsqcf70gi9")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.5") (hash "0jscsq9iyj0smfn6bz2pa2rhg7n0fy37a8qaa498vpk7ammbbalm")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.6") (hash "1cd1asjcnzs3f7h1i6swjpxr24ck89pcdy8969kj9yqwz0vbyap4")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.7") (hash "0bxxglflr9qdzyylpbmb6xfjsa4qmwwrf546grwcqwn74dzflxx3")))

(define-public crate-cip_rust-0.9 (crate (name "cip_rust") (vers "0.9.8") (hash "062ky8fx4qbpffz16p630njmj107l6nhh9kdy0sai8y7asfn5kb3")))

