(define-module (crates-io ci tp) #:use-module (crates-io))

(define-public crate-citp-0.1 (crate (name "citp") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "1hxkvzraamhcqqa7qcpmxry1zyvxyqsjgwvn10d7mp0f9659g9i5")))

(define-public crate-citp-0.2 (crate (name "citp") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "socket2") (req "^0.5.3") (default-features #t) (kind 2)) (crate-dep (name "ucs2") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0s0l1sncbd55sj2p7hfyf9sixicfnbwmw7yf826pcv1jnawdk7b1")))

