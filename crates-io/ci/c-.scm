(define-module (crates-io ci c-) #:use-module (crates-io))

(define-public crate-CIC-bitboard-0.1 (crate (name "CIC-bitboard") (vers "0.1.0") (deps (list (crate-dep (name "tracing-core") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "169z3ia5df4mrchqpkrpfgffy467ps8ghbwwjjlqd762nwbsmfhs") (yanked #t)))

(define-public crate-cic-fixed-0.4 (crate (name "cic-fixed") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1d2yl4vs6lh3mr0hgdq8f3ymaqgc57my9gc1zi50dy62029268sd") (rust-version "1.68.2")))

(define-public crate-cic-fixed-0.4 (crate (name "cic-fixed") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1dls29wlv002g3cx93as2lds508qnw52n3scm677ikj05vg1acgd") (rust-version "1.68.2")))

(define-public crate-cic-fixed-0.5 (crate (name "cic-fixed") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "08q7i9h9y2smndpg61p637bvw8q9cmclnak8ahv2nwn2kbs30jvs") (rust-version "1.68.2")))

