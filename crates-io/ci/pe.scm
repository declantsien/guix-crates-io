(define-module (crates-io ci pe) #:use-module (crates-io))

(define-public crate-cipepser-bicycle-book-wordcount-0.1 (crate (name "cipepser-bicycle-book-wordcount") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "00f7glr4n3547k74w3mhak5jkawk58240aq7i7y679br5bj7z70g")))

