(define-module (crates-io ci _i) #:use-module (crates-io))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.0") (hash "0w3wk4qxpy66caw0sn0ll1qjvf01yq62f1jyvsaw226735lp60v2") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.1") (hash "1p43s398gblcjk0yzq4fs9n7ppavdlkpcyzs5vb422lyh0n7ygf2") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.2") (hash "1f10f4wbyd5p7d7s9y73h5fgr4zbim8n81dgi4h639143akcgyhp") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.3") (hash "000j9k58v7cqfzswc8v3vkysz928hhqsg5azw31g6bagmc886aq7") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.4") (hash "1znsa5vlbf35g5cfigxp84mn5zp96wzwixkqx449hzd3qdcipjfp") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.5") (hash "0fqd4069mllvxkfmbhwfghgxxxrb82bryddkxrsfm2vhw3rpsm4r") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.6") (hash "10mckkpy93pm1lr45was70i36sndyi4f7542bfkphnjdpzmwma6j") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.7") (hash "0b3i4jl6h3lis17cwmsxf4j8i233bykxhi0mrhlnc7f47lq7xai2") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.8") (hash "0s7b38ns2aj3krsq8cc87jnf28j7lxgwrn01a617gjpiywsnrxpw") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.9") (hash "0gybgj88c5mbq293bxc4kcwxyildzwasmbkisjxh4v5mx3xvl8f4") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.10") (hash "1xz4m95i24xi3vgyza596aaqjih7c01irqrb04f7il52p3ji29q8") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.11") (hash "03s8lxbvy96hrb2biz0mm99gb9fj4311qjm5rh7rrj3sjhh8a42i") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.12") (hash "0zqrry901bc0hmv91n5dcn0w5fd5lgk63bpz9amwdgxq6x5ac888") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.13") (hash "0m1x17s0mv28bjsmzs357gxj1nbg35s8125m7pz383dgv60j7ri9") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.14") (hash "0fivjxzy8kfnhv1v7p7cx6dmc29wgxn372gynwvzgynmpqh7s4sr") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.15") (hash "1nrp87maabg1bhgc4a3xg22zak43hyavf2l0p1m8pjfh3vvs1c82") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.16") (hash "0jwfgd9x77bw8frvlgbsdg3iwjwfvvl68nqk5dr44wydb4nhq9np") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.17") (hash "0s2f9bdky25wj65ndywil8dgs8vq0c121f7v8praidfx0yvx9w3h") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.18") (hash "0v2k750m38sn8rk9vdi260jb61nmcbx2g7m0600n17n97xgvwxsg") (features (quote (("default"))))))

(define-public crate-ci_info-0.1 (crate (name "ci_info") (vers "0.1.19") (hash "0rwghmkpjkzl0s2d2la3r30f3q2hgwjnn83c8n02s45bqfbrg1j1") (features (quote (("default"))))))

(define-public crate-ci_info-0.2 (crate (name "ci_info") (vers "0.2.0") (hash "02i8fwx70xy8wypyzih85r80f657l90zindy1ciss1hfcifmynb8") (features (quote (("default"))))))

(define-public crate-ci_info-0.2 (crate (name "ci_info") (vers "0.2.1") (hash "12arfh5zbn33yc9j3v2h2s9p66sjcagmq4mpb2l75cl4xb1f653s")))

(define-public crate-ci_info-0.3 (crate (name "ci_info") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "14ba28p0fhq40xlhqlaysaxa22f8inpysbcbm3rjcs7sibbpygj6") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.3 (crate (name "ci_info") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "00pr17g6q6i752acrkycg0hkq3lm0p634anm41g3m6lqg8q83s75") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.4 (crate (name "ci_info") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0cmhcsxvgq7nh07s2wycz6xp02zrky6fv1cwfxrn1960gqs6ikyz") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.5 (crate (name "ci_info") (vers "0.5.0") (deps (list (crate-dep (name "envmnt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0n19q7ka1g2jc2lk5yy0ff7ssb4wmqkqdk6jqx2pys3klwbqacmy") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.5 (crate (name "ci_info") (vers "0.5.1") (deps (list (crate-dep (name "envmnt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0l4s8557yprbkjick9c2lavq72yjlpilrw9g6x9a9m1jpgs3lxic") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.6 (crate (name "ci_info") (vers "0.6.0") (deps (list (crate-dep (name "envmnt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ggwmb8jhh83c070aycw3q9m9qi8zymj62wwqifp632ichm37xml") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.7 (crate (name "ci_info") (vers "0.7.0") (deps (list (crate-dep (name "envmnt") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1gpa9b5zwbgqhbw6vi24yybf2ri549g1acxaxdwx47w7727pa8rb") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.7 (crate (name "ci_info") (vers "0.7.1") (deps (list (crate-dep (name "envmnt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "01n3gxmwp765m6xg1fl8v1y12wsvbqvlcai27kdr5d2skrijyfb7") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.8 (crate (name "ci_info") (vers "0.8.0") (deps (list (crate-dep (name "envmnt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1rcv5apb0m9hm40061dm16kis64gymdg8z6bskhvz41hizcs9q9d") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.8 (crate (name "ci_info") (vers "0.8.1") (deps (list (crate-dep (name "envmnt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0g86x0qbcj17whmlgql28dbgbzxj6awbd6qqjayycya55cs8bc4k") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.9 (crate (name "ci_info") (vers "0.9.0") (deps (list (crate-dep (name "envmnt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0apdcqggfs7q8ca1apkhi016gb6i2dsgqw6vmx370pi87lf0ksd4") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.9 (crate (name "ci_info") (vers "0.9.1") (deps (list (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ndzf92pgsz35jcki27r9pw5h6m9h8cqjz0w9nzd9bqaxd9nhfp5") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.9 (crate (name "ci_info") (vers "0.9.2") (deps (list (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1fg3pr8vn6yzjklwn8gsh15dwhlzafnsqqkadzhcwv7ik51svi59") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.10 (crate (name "ci_info") (vers "0.10.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0s2y0k245ii38s62nlddgzrlil9qvvjbdmzwpnfi1bji9ad001xl") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.10 (crate (name "ci_info") (vers "0.10.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0ash7xpr47nk3lysjr836r6xibdbq468mibzr9v132mcmsdmz751") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.10 (crate (name "ci_info") (vers "0.10.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0pn4lfq630bf6aki3kz41555b4fs8iyc1a69biwm6mwc1v3kixi4") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.11 (crate (name "ci_info") (vers "0.11.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1df30hr61f896fpb8xylb0m2b2w263ndi5fmm1zmgjx7m2ss6h6j") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.13 (crate (name "ci_info") (vers "0.13.0") (deps (list (crate-dep (name "doc-comment") (req ">=0.3.0, <0.4.0") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req ">=0.8.0, <0.9.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req ">=0.11.0, <0.12.0") (default-features #t) (kind 2)))) (hash "1ywksbzfsyq4nq2bk8247yxf7m3v6qxlk3cg1nc3vs7x0aabqr7c")))

(define-public crate-ci_info-0.13 (crate (name "ci_info") (vers "0.13.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)))) (hash "1ydh9b2qlrjcbk10zcqh22f3abhc2ni92gnnwki5g3pyc1lrjaj3")))

(define-public crate-ci_info-0.13 (crate (name "ci_info") (vers "0.13.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)))) (hash "1064ss1mlmrg1c1br1z8khmdp6sqasfrm1483a20rlkxjya9542d")))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0yqq0jvb6g5d0hapfp1p5141v2kjhlbs1mcpnn5k8ji21bw75p0i") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0jacp57cgk64848fib44mhc4wgsfia33ilk7vs05f0l1i4hx9vk1") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "07wv0k263qpda7hfbkm7vv0h7cisg3xz258qq115vprr97pckwcq") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "08vkxaqnhqsvld4f30bfy5bqpyzn0jlq5f61j8dz0hi26xj2h3hz") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.4") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1w6ig56kr8nmhshr9wiwybwbzsjy5j8ag6597bsfmg0jzm31jqvb") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.5") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0x0jxyxc4fpzryr38ihf0dqm9lr2wn2lar1xcphzpbkv8zmxsr5p") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.6") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1pifp5cqfr809a6m1aarlq0pxckkqdfx3194ijilqx0y184f75pv") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.7") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "120nhmimkmanpgy5jbnr5prqx32csr6384ki1zz3y4g2wzgkhm34") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.8") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "10xvcnizxp6giwnybcca2m5n31b8l91qxg579dpla6bw74hj3m2q") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.9") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1y6vmmwqw8yqdblyrbwdrz0hbb2gdkaapxnzk7l8730c10wjz9k2") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.10") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0bbj54vnhv8shn6r1y1jrsb9las0bpjchbvik0g9j3vyv8yylw18") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.11") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1nc0dkfqir9z5349mp4pqcj5x0lqsdm2zx8530xngy5b778wif3f") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.12") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "02ib59qx7s21373r7v7h56k73lcga9q7k8n3b1ni915c6bbgv4f4") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.13") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "14jsjpmwsvdlx0f5v7a3fbz0jgm0wd4bdywvmhc80m6lq6j52873") (features (quote (("serde-1" "serde" "serde_derive"))))))

(define-public crate-ci_info-0.14 (crate (name "ci_info") (vers "0.14.14") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "envmnt") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "12z3qr6d4j0j1al0f6mk1anvzq04y874423b9m1lsb0zvmxvn3c4") (features (quote (("serde-1" "serde" "serde_derive"))))))

