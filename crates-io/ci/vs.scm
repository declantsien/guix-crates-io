(define-module (crates-io ci vs) #:use-module (crates-io))

(define-public crate-civs-0.1 (crate (name "civs") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bng6s8w5rk7f60y2pwy8cyq2x5amgmr71vv9vdj8nplra4np4ps") (features (quote (("debug"))))))

(define-public crate-civs-0.1 (crate (name "civs") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1k4gfx0biaa2alafmzh7w5man7sahr6w09kgng65813f58mwmhlg") (features (quote (("debug"))))))

