(define-module (crates-io ci rt) #:use-module (crates-io))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.3") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "10q04xdnhqkhn2ldp9aamb71m0ww4369s7vxnkn472rf2flvpp8p")))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1957mqr4xp064jwf3wn8410j7c1dsgz4zvf0c40zb8nqjzppcn8q")))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0n2pa5nj8gda2sjlnds6iily2caxmad7qk0f27kmpcfrgq8r0nyi")))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1x97j2pzkdf2d4dwmylrhv02h7k753mfnllsj05v3ygfccxv7660")))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0glvbmy6ajg0js5554yn71w4hjgsafsr10qxbay8ibb9b063n8w2")))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "04x5bzzhyrhgb9rfwapn4kcni2cbvy87chpdmvcdicadbnqyfw7p")))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1dpfzasahvc651qjl3nfzvy80hxrj92dyb30v7j8sm716jcvjllw")))

(define-public crate-cirtrace-0.1 (crate (name "cirtrace") (vers "0.1.10") (deps (list (crate-dep (name "clap") (req "^2.33.0") (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "123bsnyw31660yp3rxdn29wyisayd3wpygdr4kc08r93zwx91pfx")))

