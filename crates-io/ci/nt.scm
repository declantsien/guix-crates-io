(define-module (crates-io ci nt) #:use-module (crates-io))

(define-public crate-cint-0.1 (crate (name "cint") (vers "0.1.0-alpha0") (deps (list (crate-dep (name "bytemuck") (req "^1.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "003djgfmq07znf2p0hlhn6nkqwdcqwwivnhrmjk9v5dccn7kgypn")))

(define-public crate-cint-0.1 (crate (name "cint") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "bytemuck") (req "^1.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ds44icapgpjd6d99bqiwrggnbix4yd24109ck0dx646i02wvkd0")))

(define-public crate-cint-0.1 (crate (name "cint") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "15qp7rjw34ac5dn8hin6pq07dhy7hkgsnblk017d7x8kv7q5hh6f")))

(define-public crate-cint-0.2 (crate (name "cint") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "0165mdi320jrd8lg1vj8ar47nj4j7y72bisxma55gmky7gv8c2fi")))

(define-public crate-cint-0.2 (crate (name "cint") (vers "0.2.1") (deps (list (crate-dep (name "bytemuck") (req "^1.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "0iywx93gawf39qb09w00algyk68dkq1v2hzhsyl8qib2pdcrjw00")))

(define-public crate-cint-0.2 (crate (name "cint") (vers "0.2.2") (deps (list (crate-dep (name "bytemuck") (req "^1.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "0yyp8y2nsxhs2z768hmqypfj4x0m8s2h3yar6vrp6kc552pgx0sx")))

(define-public crate-cint-0.3 (crate (name "cint") (vers "0.3.0") (deps (list (crate-dep (name "bytemuck") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "00sm6r7ckafhx0y99s9sjjsigakqknnrqdsfpf08m1s6vpbp7cw7")))

(define-public crate-cint-0.3 (crate (name "cint") (vers "0.3.1") (deps (list (crate-dep (name "bytemuck") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "16l9glvaxshbp3awcga3s8cdfv00gb1n2s7ixzxxjwc5yz6qf3ks")))

