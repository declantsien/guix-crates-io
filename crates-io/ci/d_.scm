(define-module (crates-io ci d_) #:use-module (crates-io))

(define-public crate-cid_fork_rlay-0.3 (crate (name "cid_fork_rlay") (vers "0.3.1") (deps (list (crate-dep (name "integer-encoding") (req "~1.0.3") (default-features #t) (kind 0)) (crate-dep (name "multibase") (req "~0.6.0") (default-features #t) (kind 0)) (crate-dep (name "multihash") (req "~0.8.0") (default-features #t) (kind 0)))) (hash "154lwcg3ri4jh1l3mlpyzc2wj2hy25ljqm7anh7ibxys19agixzj")))

