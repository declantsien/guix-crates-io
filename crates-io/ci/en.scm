(define-module (crates-io ci en) #:use-module (crates-io))

(define-public crate-cienli-0.1 (crate (name "cienli") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1w20n0v2an3bdxl48737j7dgc6p05538ahsz32v4i3pjzm2ysqwr") (yanked #t)))

(define-public crate-cienli-0.1 (crate (name "cienli") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1nslbimrrk8mq4fnwlcb44n90fxl0x54qxvhzw0hxblfp1d6s8q0")))

(define-public crate-cienli-0.2 (crate (name "cienli") (vers "0.2.0") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "17j6waayl0m3v9kc7vcq2rv29lz0h64w187vqlx1fg64j476ybip")))

(define-public crate-cienli-0.3 (crate (name "cienli") (vers "0.3.0") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0wr8cq1ah55s3lpqai43ccz6zr1ak7jx625zpyv2f1h922p5xqbm")))

(define-public crate-cienli-0.3 (crate (name "cienli") (vers "0.3.1") (deps (list (crate-dep (name "num-integer") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (optional #t) (default-features #t) (kind 0)))) (hash "1brzwzpybhhjnp211xa777bc387m9gx5dcxnrp6wzi0kbvjdzfi7") (features (quote (("xor") ("vigenere") ("scytale") ("rot") ("polybius_square") ("default" "affine" "atbash" "bacon" "caesar" "polybius_square" "rot" "scytale" "vigenere" "xor") ("caesar") ("atbash")))) (v 2) (features2 (quote (("bacon" "dep:regex") ("affine" "dep:num-integer"))))))

