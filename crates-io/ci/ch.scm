(define-module (crates-io ci ch) #:use-module (crates-io))

(define-public crate-cichlid-0.1 (crate (name "cichlid") (vers "0.1.0") (hash "1x7wqmjngd6rl56jmmwgs5f6fal4ikf00k0wl7dyd2drrprwsb6l") (features (quote (("no-std") ("default"))))))

(define-public crate-cichlid-0.1 (crate (name "cichlid") (vers "0.1.1") (hash "0jiqav7h3zkdb6bidyhah7xs993szzviq91s23vgjccxc166fm4f") (features (quote (("no-std") ("default"))))))

(define-public crate-cichlid-0.1 (crate (name "cichlid") (vers "0.1.2") (hash "1ilkvzznpmhb35smy235nzdz41w7zllvfcrd62g5x4yxqx4xpm9r") (features (quote (("no-std") ("low-mem") ("default"))))))

(define-public crate-cichlid-0.1 (crate (name "cichlid") (vers "0.1.3") (hash "0xxazfr2axv56w9q4232bnaxifpmm7zsh01bklgrqmkmbxigd5wm") (features (quote (("no-std") ("low-mem") ("default"))))))

(define-public crate-cichlid-0.2 (crate (name "cichlid") (vers "0.2.0") (hash "09c9yy12bg19izdvrzhzwzbg9ksjxsj10dcfv78v2r0369karkmx") (features (quote (("no-std") ("nightly") ("low-mem") ("default"))))))

(define-public crate-cichlid-0.2 (crate (name "cichlid") (vers "0.2.1") (hash "0d6s3c79ryx0jhs5288h9bp1ws1i19pcrrvvbb8x36v0x6rhm3p0") (features (quote (("no-std") ("nightly") ("low-mem") ("default"))))))

