(define-module (crates-io ci st) #:use-module (crates-io))

(define-public crate-cistring-0.1 (crate (name "cistring") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0haq07203s6ahkyxmay5dz7xg8cg5fnp8sb1cwwmixmlnklf15s7") (features (quote (("default"))))))

(define-public crate-cistring-0.1 (crate (name "cistring") (vers "0.1.1") (deps (list (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wn4wk4x2a6aml97a7sndgwmp1g9018cd0643wfmh8rijjbirs7i") (features (quote (("default"))))))

