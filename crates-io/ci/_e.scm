(define-module (crates-io ci _e) #:use-module (crates-io))

(define-public crate-ci_env-0.1 (crate (name "ci_env") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l7d13ifyfpqlgshmcfa9qfkj2bvnhinkhk6hi31d0xzlkmpn5bi")))

(define-public crate-ci_env-0.1 (crate (name "ci_env") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hdgff5z9rb8sijkji4s9v82kzkj9ilmy8lqdm4cqa85qgf2zv2d")))

(define-public crate-ci_env-0.2 (crate (name "ci_env") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02xmybqblg1lzg9x13nvqblx7m0x0pm6yxs52cv7q3a7kv2lbv5q")))

(define-public crate-ci_env-0.2 (crate (name "ci_env") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wgaag1wlyvq9fnx0c56602dq9vfzq3vp51yh56ghhpd8d5rv9iq")))

(define-public crate-ci_env-0.2 (crate (name "ci_env") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10ad2cj3b0h6qsz9m19s0yyp0bb8x5klpq80vc768hzaxwhmqfi7")))

(define-public crate-ci_env-0.3 (crate (name "ci_env") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1g8whc94avyihfxdlga307i5ghnyaccisxdhc9ld9csd69iafriy")))

