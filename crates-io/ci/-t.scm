(define-module (crates-io ci -t) #:use-module (crates-io))

(define-public crate-ci-testing-kbartush-0.1 (crate (name "ci-testing-kbartush") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "xdr-rs-serialize") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "xdr-rs-serialize-derive") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "06yr2k1ahkqrzbvrg2a1xs35cisvf0yn7dg8c369lzzwbal1fzi3")))

(define-public crate-ci-testing-kbartush-1 (crate (name "ci-testing-kbartush") (vers "1.5.5") (deps (list (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "xdr-rs-serialize") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "xdr-rs-serialize-derive") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "1bbmdll68xr09w3msmp23fp29pfrh1gyzdg952z2dvsrkdj5lm41")))

(define-public crate-ci-tron-0.0.0 (crate (name "ci-tron") (vers "0.0.0") (hash "1qbhg2mccsl2si75izjjdh8haw35hdiywqgpqyr6169yqx81j4z7")))

