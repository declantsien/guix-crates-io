(define-module (crates-io ci sc) #:use-module (crates-io))

(define-public crate-cisco-logger-0.1 (crate (name "cisco-logger") (vers "0.1.0") (deps (list (crate-dep (name "paris") (req "^1.5.8") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "syslog_loose") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0fp8xki4864fqhxb90nwby41z8jsfqrnmf2xn12452li61968yzz")))

(define-public crate-cisco-logger-0.1 (crate (name "cisco-logger") (vers "0.1.1") (deps (list (crate-dep (name "paris") (req "^1.5.8") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "syslog_loose") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0knngwacfppzakh1h6cjrp51166p557sbpy7ychi6q5cqndc68r6")))

