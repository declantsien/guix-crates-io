(define-module (crates-io ci sm) #:use-module (crates-io))

(define-public crate-cismute-0.1 (crate (name "cismute") (vers "0.1.0") (hash "0p1pzh1072knwagm57dxvfjdv31vkvsgcyiczrimnvf8drqv92s4") (features (quote (("switch"))))))

(define-public crate-cismute-0.1 (crate (name "cismute") (vers "0.1.1") (hash "1mz0kkxxji86fdbbzpmcfj61czh6fn4arb7rnd260vj9f5lmswzh") (features (quote (("switch"))))))

(define-public crate-cismute-0.1 (crate (name "cismute") (vers "0.1.2") (hash "1jpa11b41a5qr7zsw2x21y2xwk4p7vg2ymq3bifs4ydiyxbwq411") (features (quote (("switch"))))))

