(define-module (crates-io ci fu) #:use-module (crates-io))

(define-public crate-cifuzz-0.1 (crate (name "cifuzz") (vers "0.1.0") (hash "191mx3cjzhw6g4vmx2975n6s3w4dafn3zwas5d1jr07a6hx8z3pj")))

(define-public crate-cifuzz-rs-0.1 (crate (name "cifuzz-rs") (vers "0.1.0") (hash "1ycv6b6dcgf143zqqfrxxrha4a0wq4gy42nyacwqm00xnvdsdy7b")))

