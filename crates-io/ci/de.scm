(define-module (crates-io ci de) #:use-module (crates-io))

(define-public crate-cider-0.1 (crate (name "cider") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "072gi5i7pmvxgnk0z8g8qicpzfsi3rivbak9d8synbhq09xzcm55") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-ciderver-0.1 (crate (name "ciderver") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "06k7a6q3ra20wbvj7ggnz63mjja49jydcn8a44snl314apsgw69r")))

