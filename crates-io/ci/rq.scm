(define-module (crates-io ci rq) #:use-module (crates-io))

(define-public crate-cirque-0.0.1 (crate (name "cirque") (vers "0.0.1") (deps (list (crate-dep (name "llq") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1m2prx370ysdhh6372s9m82lzhy403262mw5qk7pxfm8224n2wg9")))

