(define-module (crates-io ci me) #:use-module (crates-io))

(define-public crate-cimetrics-rs-0.1 (crate (name "cimetrics-rs") (vers "0.1.0") (hash "174fhbgmmbh7y0j4j7h8vsmpgqmwfmcspv2w6rd5rr8mblkjjlmi")))

(define-public crate-cimetrics-rs-0.2 (crate (name "cimetrics-rs") (vers "0.2.0") (hash "19znwzsibihllvs4008n60ilr6b63pjiswr31chlj0mx89zhi5vn")))

