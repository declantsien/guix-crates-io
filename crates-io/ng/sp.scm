(define-module (crates-io ng sp) #:use-module (crates-io))

(define-public crate-ngspice-0.0.1 (crate (name "ngspice") (vers "0.0.1") (hash "061fya0qpkfxnz4paw8vz5rva8k74qkd0m676pcmrcyxbjshv0wx")))

(define-public crate-ngspice-parser-0.1 (crate (name "ngspice-parser") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1c5xl2chzh04gm8wfzyywkbii2zmac10a33av9wmkphpdk544bsg")))

(define-public crate-ngspice-parser-0.1 (crate (name "ngspice-parser") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.132") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "16mlqmrhazvw9dbjc24rcvj3793nnssjd1al9nwslrzs6jwb1jp7")))

(define-public crate-ngspice-sys-0.1 (crate (name "ngspice-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "16mspmksss9c5f77xynnr60jghx6q8wbyrypzviplspys2cmjf8g")))

(define-public crate-ngspice-sys-0.2 (crate (name "ngspice-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "06wjxcp143343dh9rcnvh7wzirb2fq1wl78mknh2klbv2i62bqp0")))

(define-public crate-ngspice-sys-0.2 (crate (name "ngspice-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)) (crate-dep (name "libloading") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "1i5jnqkmnnkmnd0hqani78fjq14sjc13s7g5fk2yji075ycmwddq")))

