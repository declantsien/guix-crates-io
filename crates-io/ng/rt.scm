(define-module (crates-io ng rt) #:use-module (crates-io))

(define-public crate-ngrtc-0.0.0 (crate (name "ngrtc") (vers "0.0.0") (hash "0kakg0qpxw9wxpxvg9fw9qzhrc7qvhx5jz3z4b8ri9k2rwxcb96y")))

(define-public crate-ngrtc-0.0.1 (crate (name "ngrtc") (vers "0.0.1") (hash "0xsxwlyklhjq93qz6vlg5wvl2px46glr8ndxbg10hxmy1dvh7cia")))

