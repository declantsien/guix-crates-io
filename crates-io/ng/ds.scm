(define-module (crates-io ng ds) #:use-module (crates-io))

(define-public crate-ngds-0.1 (crate (name "ngds") (vers "0.1.0") (hash "0hk3bvspig8wjg4989xrkyyxiqw6a6vn6sj4ix7cya7vimxwj1r8")))

(define-public crate-ngds-0.1 (crate (name "ngds") (vers "0.1.1") (hash "0mybglry8ajx25sq40v1b8h6ahdyf9vh587rjmmgfyl2mfs6nhp1")))

(define-public crate-ngds-0.1 (crate (name "ngds") (vers "0.1.2") (hash "08mxvyh21j08x37bk7pd1cyficrhd1p4vl3q0wzd7dhzvln3d55f")))

