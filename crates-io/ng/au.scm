(define-module (crates-io ng au) #:use-module (crates-io))

(define-public crate-ngau-0.1 (crate (name "ngau") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.115") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xwnisd28d8pyvxh0l4wv5k0s1x6z232p5wj5xpj49cx12bq7qrp")))

