(define-module (crates-io ng ra) #:use-module (crates-io))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.0") (hash "1w0n8xph6rd9abl6qby18j573xhlg0fnbyk796p10659h2irbw40")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.1") (hash "1wirj2ssckxib228zv9m50vi726w63zs7r331z031ksfqrwkzzz3")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.2") (hash "0mkph2bfp736g4r6jfmibq29xyiagrxqqvkqwb5k7xz2gng4j2fa")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.3") (hash "19cc77jgh05gbcc1waq7nggl868fszsq9ag4kgzf86r4bw82ya34")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.4") (hash "0lky7hj59sdwcy7aaljw4hngahcnvrq4zznslwavhhbrvywjddrq")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.5") (hash "0140c9h6dzzsslyz9maz964pv5bcjl4bfqh8l31iwwgyadwqrpi1")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.6") (hash "1w9fw8i60f0h29lvb6f0azvnfypcxf9lhccsccqy2yyzg593761c")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.7") (hash "1f5ns37l869a7pcmxkz59vvismnw5mz1cp7yykybw4hraxr44z7w")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.8") (hash "0plj85hi1p9rgjrabk3x1w0yf58k5r8vanq8cjn5rpscnwf8ysxq")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.9") (hash "0vjjv4li7gm1hv7cdss4zs66sdg6gm1v1nlw0j8zyql3ss2600kz")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.10") (hash "1gad8h48z8ky2cf2xvirgb1rri2waixdaih7s7jimhnqrarwshvs")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.11") (hash "1rdvwy1pmvw5rfyj66m9b99hl3v9k0llx7klv5z5rvj8ckmb2slg")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.12") (hash "17n64qsv4agaavwcs579kvw21rwig68zay0lgkxsh6bard0fxfax")))

(define-public crate-ngram-0.1 (crate (name "ngram") (vers "0.1.13") (hash "0x9761ivkli2xjirfij7wchj4csm9dq0xj5ia80fk1sh90snpkmw")))

(define-public crate-ngram-search-0.1 (crate (name "ngram-search") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qccbjbygvxcnikc6q00md70y691s0drpx8afd32r7n5vdgqvcm8") (features (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-ngram-search-0.1 (crate (name "ngram-search") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jnfhifqgxqzxmpbsg5412pajz62412cb2wkzyx4irjz8pcdbnkk") (features (quote (("mmap" "memmap") ("default" "mmap"))))))

(define-public crate-ngram_iter-0.1 (crate (name "ngram_iter") (vers "0.1.0") (hash "14qhg8ldlyll8qmkjwyb6wx6iic5xax8x0vrcr2ss85iiixlnvnx")))

(define-public crate-ngrammatic-0.2 (crate (name "ngrammatic") (vers "0.2.0") (hash "0a70d6g8cwz5knk5lcc9nx2r90r9hijz6kkkydirk53y95jnwnca")))

(define-public crate-ngrammatic-0.2 (crate (name "ngrammatic") (vers "0.2.1") (hash "0glxfl3jaiadslq3scsj422absvgmdyjqq08x15lhhb15xvi6z72")))

(define-public crate-ngrammatic-0.3 (crate (name "ngrammatic") (vers "0.3.0") (hash "0bnc2zk1iqvlv0khk05x2a5dbb09ljkl7iphms20mv6ah8s29jpc")))

(define-public crate-ngrammatic-0.3 (crate (name "ngrammatic") (vers "0.3.1") (hash "0s5jhrrz2zhz63vawi8l6nyivyaiw2wsf5ng61fykcpiva709lgn")))

(define-public crate-ngrammatic-0.3 (crate (name "ngrammatic") (vers "0.3.2") (hash "1241l3d9ymgx1g4cj3gnlndm2x9nxq4znn22dvr2xnjbq3hnp7mp")))

(define-public crate-ngrammatic-0.3 (crate (name "ngrammatic") (vers "0.3.3") (hash "0ysn0y867q8dsg94zrk7njj6a4a5ym0x5y2747w9ljvz6hrvwfmz")))

(define-public crate-ngrammatic-0.3 (crate (name "ngrammatic") (vers "0.3.4") (hash "00dav66wi21s500d64q2vmzzbnxirbss99cwvjrzga5ib2qmmhgh")))

(define-public crate-ngrammatic-0.3 (crate (name "ngrammatic") (vers "0.3.5") (hash "14b9wwcfgmih9d2dkf76kq5ficlfjzbs16bp1db5m1pmngs3wvf0")))

(define-public crate-ngrammatic-0.4 (crate (name "ngrammatic") (vers "0.4.0") (hash "1zpnnpn4bs4ifmj3i7k2wb08nqr8lnxmkschlaw7znl2gsc2yvrw")))

(define-public crate-ngrams-0.1 (crate (name "ngrams") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.23") (optional #t) (default-features #t) (kind 0)))) (hash "0ssj0j6nfqigyi9jl2y7cngd0d87psm4aplna0f4pi299jzi4g0l") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ngrams-0.1 (crate (name "ngrams") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.23") (optional #t) (default-features #t) (kind 0)))) (hash "1hvj0xj5gxnhfgfj98yd5j0mqsp7y7fn0gbzdbn2vbk1x1jxxxcj") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ngrams-1 (crate (name "ngrams") (vers "1.0.0") (deps (list (crate-dep (name "clippy") (req "^0.0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 2)))) (hash "0wnqh6b12dpxk37bzaidizgd028abahcylyfl2w4z1lkf8khpkmd") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ngrams-1 (crate (name "ngrams") (vers "1.0.1") (deps (list (crate-dep (name "clippy") (req "^0.0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 2)))) (hash "1wivp31qmfqy08jkhknwkvlg2znsq28hmhpa3b8v653rgpi774md") (features (quote (("dev" "clippy") ("default"))))))

