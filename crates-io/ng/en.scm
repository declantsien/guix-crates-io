(define-module (crates-io ng en) #:use-module (crates-io))

(define-public crate-ngen-0.1 (crate (name "ngen") (vers "0.1.0") (hash "1bw4gpcd64iccpc5j1ablm2gs4amim32nn6947izm0wr8m5jrr67")))

(define-public crate-ngen-0.1 (crate (name "ngen") (vers "0.1.1") (deps (list (crate-dep (name "cgmath") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34") (features (quote ("image"))) (default-features #t) (kind 0)))) (hash "1mlj9ljsnry3pwgidr0mq2kkp6dm1b0sxn1ajycjsk17zn3vqmax")))

(define-public crate-ngen-0.1 (crate (name "ngen") (vers "0.1.2") (deps (list (crate-dep (name "cgmath") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34") (features (quote ("image"))) (default-features #t) (kind 0)))) (hash "0gn79az16marjcdgbn4lv8znxivbnak65y5ksrzri5nzasbfdv8i")))

(define-public crate-ngen-0.1 (crate (name "ngen") (vers "0.1.3") (deps (list (crate-dep (name "cgmath") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34") (features (quote ("image"))) (default-features #t) (kind 0)))) (hash "1hhcz4v3nk9601x4s5x9rvbyan0swgl66z22g3pkjs7f404lfkj8")))

(define-public crate-ngen-0.1 (crate (name "ngen") (vers "0.1.4") (deps (list (crate-dep (name "cgmath") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34") (features (quote ("image"))) (default-features #t) (kind 0)))) (hash "184ds6dh5mr4ccwlxcaz54d13hqlxgkf34r9mv938w0djh8i1m15")))

(define-public crate-ngen_nsl-0.1 (crate (name "ngen_nsl") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x0a6v12zb20cc3m934lpjpfbl9r0m0bbggnanf7q3yhl5kn9wvr")))

(define-public crate-ngenate-audio-0.0.0 (crate (name "ngenate-audio") (vers "0.0.0") (hash "1lnqasmla1s3kf7kdh134nj2ipn218qsd4n0s5jckysqhlqf53wa")))

(define-public crate-ngenate-compute-0.0.0 (crate (name "ngenate-compute") (vers "0.0.0") (hash "0xix7vvz46jffd923d48rmhjb6phzr1phd3piwxc764s0fpqma8z")))

(define-public crate-ngenate-data-0.0.0 (crate (name "ngenate-data") (vers "0.0.0") (hash "1z6bm7gc2ni4vblfyc6qmsbz7hcnllig62fgi8d00b0rixbrlqbs")))

(define-public crate-ngenate-ecs-0.0.0 (crate (name "ngenate-ecs") (vers "0.0.0") (hash "19x7wbbh620691aws109s420pn7bx9fgsyzhxlsvc9gm982w3b7v")))

(define-public crate-ngenate-editor-0.0.0 (crate (name "ngenate-editor") (vers "0.0.0") (hash "1gzzhag83kk61ic4y9a1m419843mvnmjxdpq01gg0yqnwghwf0g6")))

(define-public crate-ngenate-flex-storage-0.0.0 (crate (name "ngenate-flex-storage") (vers "0.0.0") (hash "0qrpxf5fm2hy5syhq6v2nsz0sj59dszdd7ll6y3w411g19zjmchp")))

(define-public crate-ngenate-graph-0.0.0 (crate (name "ngenate-graph") (vers "0.0.0") (hash "0b0mb3ckh9vp3px594g1blzdwgr4dr09n8wnwljllqaxwcq8snpx")))

(define-public crate-ngenate-math-0.0.0 (crate (name "ngenate-math") (vers "0.0.0") (hash "189fhcn1pna7bgb0d37dklqd9mj8g341cslcrsnk4snij5vpnmby")))

(define-public crate-ngenate-query-0.0.0 (crate (name "ngenate-query") (vers "0.0.0") (hash "0qng13miwjq23pz1wz9288n1xin869ia2lg2v8502dmx1paj3yfd")))

(define-public crate-ngenate-spatial-engine-0.0.0 (crate (name "ngenate-spatial-engine") (vers "0.0.0") (hash "0gxqwpc7j5f3rjyiiciij7g5zbqqqd6nyhmaafpll3zq70lgrfjm")))

(define-public crate-ngenate-storage-0.0.0 (crate (name "ngenate-storage") (vers "0.0.0") (hash "1x4sbcl6canyl2dgwxpl2kzklknxcqzs3kzyrs6smkn4v6vl6km2")))

(define-public crate-ngenate-ui-0.0.0 (crate (name "ngenate-ui") (vers "0.0.0") (hash "0mbfjkqc70byixlxxiy7yjsfa7bvcs3kmzn8cjpmnhxjxjx5aqk7")))

(define-public crate-ngenate-visual-0.0.0 (crate (name "ngenate-visual") (vers "0.0.0") (hash "0p64hv9a48x4cjk5r995g658j689i48wgkd32r06b6znmr3iwhfx")))

