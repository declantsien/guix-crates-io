(define-module (crates-io ng ht) #:use-module (crates-io))

(define-public crate-nghttp2-0.0.0 (crate (name "nghttp2") (vers "0.0.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1g46a5w3az536hx63icf43blnqzpxns5l4g2x62qq1bj6fnng8yf")))

(define-public crate-nghttp2-sys-0.1 (crate (name "nghttp2-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "028j4vc9dykd5886ci1sm2ygbdf4nrx5lhk8aksmpf94s5vqhyk4") (links "nghttp2")))

(define-public crate-nghttp2-sys-0.1 (crate (name "nghttp2-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.40.0") (default-features #t) (kind 1)))) (hash "1fjcvbqbbl1bs73z5wjmhbqxj6zxj5rvnsjllbznicdba0prnhri") (features (quote (("rustup" "rustfmt") ("rustfmt") ("default")))) (links "nghttp2")))

