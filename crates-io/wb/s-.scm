(define-module (crates-io wb s-) #:use-module (crates-io))

(define-public crate-wbs-backup-0.1 (crate (name "wbs-backup") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "01zi1rq2d8jgdih9pwxvdv1pq7vg3mxny5xlf8a57kzg7lvcv2hx")))

(define-public crate-wbs-backup-0.1 (crate (name "wbs-backup") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1srf1lblxa4kpyaahbn9ga76ghryjlxqiir0xc9lfs75z7zlnbni")))

(define-public crate-wbs-backup-0.1 (crate (name "wbs-backup") (vers "0.1.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "11zndwxkvnh5ripvbqjk9z9j57xsfxsxa6dy2s6hxzmba6mk2ibc")))

(define-public crate-wbs-backup-0.1 (crate (name "wbs-backup") (vers "0.1.4") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vr6708gbh2yhmfs790iins65rcdllzsvh2c5m9yhd3671v40y3k")))

(define-public crate-wbs-backup-0.1 (crate (name "wbs-backup") (vers "0.1.6") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hw9p2k2idp4czghgxwd672x9axiy3nbv5lrcyp7kz380rfgr9iy")))

(define-public crate-wbs-backup-0.1 (crate (name "wbs-backup") (vers "0.1.8") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "121qn8fg2pd331kdvdhrvivyvnq1cdc8pdllpacng4q86nbgb0cv")))

(define-public crate-wbs-backup-daemon-0.1 (crate (name "wbs-backup-daemon") (vers "0.1.10") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "17kxy8a91wgmscd4ikmr8cl8h4lay5156x65gjmww9mqq2gdrlm7")))

(define-public crate-wbs-backup-daemon-0.1 (crate (name "wbs-backup-daemon") (vers "0.1.11") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0b10i069qhb2zqz51qvhiqqyyxl3wih6mndlbb9q84n1ifbqgv4g")))

(define-public crate-wbs-backup-daemon-0.1 (crate (name "wbs-backup-daemon") (vers "0.1.12") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gnxb31xfazmwaqpg4sj5sxmjf564pksg5iqhdgmmzsygl7qlr4k")))

(define-public crate-wbs-backup-daemon-0.1 (crate (name "wbs-backup-daemon") (vers "0.1.13") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0daa0agif7nmlcddhhv0rp86aqsjhb457gbkczsj7dr5f0mxlbpm")))

(define-public crate-wbs-backup-daemon-0.1 (crate (name "wbs-backup-daemon") (vers "0.1.14") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "05f9ay3s3f7xgrv08qdx0szjdy59a5s6rjpw1hcs505prjdim8kc")))

(define-public crate-wbs-backup-daemon-1 (crate (name "wbs-backup-daemon") (vers "1.1.15") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ijjm6qynxxxyy4zbb1fbd3zbj9hvk34rrj04d72jxyngsqc5y40")))

