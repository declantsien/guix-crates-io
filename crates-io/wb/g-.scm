(define-module (crates-io wb g-) #:use-module (crates-io))

(define-public crate-wbg-rand-0.4 (crate (name "wbg-rand") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k1blnqrhc8pn6pbqgx191hlddcpmksbldawhgnnzxcin46f21am")))

(define-public crate-wbg-rand-0.4 (crate (name "wbg-rand") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yikmkkshr0dg94hvi5g62w2wq4xwbak93l9px818p1di7f2d7ii")))

