(define-module (crates-io pk ce) #:use-module (crates-io))

(define-public crate-pkce-0.1 (crate (name "pkce") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0d2zb65809mjbz8hsqb7cky6ryag5ysifn5lz4pvaqkjx2wg7laf")))

(define-public crate-pkce-0.1 (crate (name "pkce") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0wpp66kplxc5lca9fsxgiwn7am9ksrlqqwk2wagwlik8j63nczpz")))

(define-public crate-pkce-0.2 (crate (name "pkce") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "1ly2352pkq0f0yf2pmg8j98ysw1idp9jizhlr44xx0mxmvidna72") (v 2) (features2 (quote (("js" "dep:getrandom"))))))

