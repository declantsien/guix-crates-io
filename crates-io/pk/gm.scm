(define-module (crates-io pk gm) #:use-module (crates-io))

(define-public crate-pkgman-0.2 (crate (name "pkgman") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "07zpisdl9s1ykyrngvwqs9yhgcwysn816sr4vzwi912i34fpn4h4") (yanked #t)))

(define-public crate-pkgman-0.2 (crate (name "pkgman") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "04mv7g6m97g5w6nzjd7ppsaizc549ayp95nihhgj1r07fmyc1f20") (yanked #t)))

(define-public crate-pkgman-0.2 (crate (name "pkgman") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0kx23widhsycwp6w6xycb5vnj6ifamj7y7mfxmbdxw55y6rs5xfy") (yanked #t)))

(define-public crate-pkgmatcher-0.0.0 (crate (name "pkgmatcher") (vers "0.0.0") (hash "0hwpzdsjxqp3bag1dcks03kn3nglz2s8qri0ma1dphqk2ccq8pvp")))

