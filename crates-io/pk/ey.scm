(define-module (crates-io pk ey) #:use-module (crates-io))

(define-public crate-pkey-sys-0.1 (crate (name "pkey-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vmpb2v3vf0kada72z84jp9qd4pvnk7wgcz5m2yvrh9rl6f9qhvg")))

(define-public crate-pkey_mprotect-0.1 (crate (name "pkey_mprotect") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nmqmhjm9wmx12n0733lc97kg646ia153xlrk2ymy09zwxc12khw") (rust-version "1.57")))

(define-public crate-pkey_mprotect-0.1 (crate (name "pkey_mprotect") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pkey-sys") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bz5wnczwjrh07ykw6z7vwi0g1j7d6bfkyqpjhcl9v76zfzl7jz0") (rust-version "1.57")))

(define-public crate-pkey_mprotect-0.1 (crate (name "pkey_mprotect") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0639x46j2dikyb1qyndsnhxqc59zx37vxp8d16n63wz3nwcp1sbn") (links "pkey-sys") (rust-version "1.57")))

(define-public crate-pkey_mprotect-0.1 (crate (name "pkey_mprotect") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cmigndpkrg6w5naqbw6a25a6gl0ys0r0m97r9j6i48kppmlr1n6") (links "pkey-sys") (rust-version "1.57")))

(define-public crate-pkey_mprotect-0.2 (crate (name "pkey_mprotect") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "00w5p5dw2xaacaqvlay6fms8azd19d02b5xnfkf3hdcvjd9py2m8") (rust-version "1.59")))

(define-public crate-pkey_mprotect-0.2 (crate (name "pkey_mprotect") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lyb0ab5pgkmajzzbknhwfg9npn8ixas1hvdwnxj6kzazbnfvwjj") (rust-version "1.59")))

(define-public crate-pkey_mprotect-0.2 (crate (name "pkey_mprotect") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xbsqs168jkry7rxfmq874rd8lxqxvw16swrd0q61iizq6qxagm4") (rust-version "1.59")))

(define-public crate-pkey_mprotect-0.2 (crate (name "pkey_mprotect") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "160k33x8qqxr8i0zgpzs61jf1h2b33mawdn3dfy365123bi03xqh") (rust-version "1.59")))

