(define-module (crates-io pk _l) #:use-module (crates-io))

(define-public crate-pk_lexer-0.0.1 (crate (name "pk_lexer") (vers "0.0.1-dev.0") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1jbp5nczgrpqh8lc327i15b108ihhph81zg0hggqhj0x5bcz4i9y")))

(define-public crate-pk_lexer-0.0.3 (crate (name "pk_lexer") (vers "0.0.3-dev.0") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0b1ja9chsdbi8pviv8bp6s8hvm74dn7qvbhgwrwrdllv2r00zx8h")))

(define-public crate-pk_lexer-0.0.4 (crate (name "pk_lexer") (vers "0.0.4-dev") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "01svq6bssvrd14i4sm552gcib7dm652d9idzzfzfyxx3mlisknkw")))

(define-public crate-pk_lexer-0.0.5 (crate (name "pk_lexer") (vers "0.0.5-dev") (deps (list (crate-dep (name "logos") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1009q365my9asvh6s32r8mzdib69ndk364qcgrfzcayvljx4d66h")))

