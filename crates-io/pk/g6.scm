(define-module (crates-io pk g6) #:use-module (crates-io))

(define-public crate-pkg6depotd-0.0.1 (crate (name "pkg6depotd") (vers "0.0.1-placeholder") (hash "1s1pc8lyazvzvzj1vs3kljxd4g85za4qz69bpfjb3a71lywx52g1")))

(define-public crate-pkg6dev-0.0.1 (crate (name "pkg6dev") (vers "0.0.1-placeholder") (hash "0rxn45wnbj11383jn4hdhdbz88dva4hdm352hdwnwc9z25didcqb")))

(define-public crate-pkg6dev-0.1 (crate (name "pkg6dev") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "libips") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gdfa8lcsa1qn11p8fvj5i316zy3ir95bnp9wg1kw8768fiaymi7")))

(define-public crate-pkg6dev-0.1 (crate (name "pkg6dev") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "libips") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "userland") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1anmxdcqwg4g96nxn5miz2j6fk5vrcslj83hninhrdq7wi27hb4w")))

(define-public crate-pkg6repo-0.0.1 (crate (name "pkg6repo") (vers "0.0.1-placeholder") (hash "0jrwwh9k6kn1sh57ikh2llcx6hsvsbkiaqgg5pc6ylk20bvpgmvy")))

