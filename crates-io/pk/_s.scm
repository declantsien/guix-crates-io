(define-module (crates-io pk _s) #:use-module (crates-io))

(define-public crate-pk_stl-0.1 (crate (name "pk_stl") (vers "0.1.0") (hash "15mhfqlfkrfg2b0sv25fm523xalzaq400gbdip6bf6czcgdzbzj7")))

(define-public crate-pk_stl-0.2 (crate (name "pk_stl") (vers "0.2.0") (hash "0a2324qdqw963j6blkiygqj71063ijqj8vbkhzzd1rg5fifxn8ij")))

(define-public crate-pk_stl-0.3 (crate (name "pk_stl") (vers "0.3.0") (hash "09pwkh0xg1ys1c1rps2yaazczclfw1is8pdaz0wlayznz3k5bnkd")))

(define-public crate-pk_stl-0.3 (crate (name "pk_stl") (vers "0.3.1") (hash "02i5jzkw2mp6wc39d35fgv52azhilrnv0xln51w1m9b9r815gkz5")))

