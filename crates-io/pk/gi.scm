(define-module (crates-io pk gi) #:use-module (crates-io))

(define-public crate-pkginfo-0.1 (crate (name "pkginfo") (vers "0.1.0") (deps (list (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xz") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1vjdq9m3kkc3q6zl3iyiip9v7zwdfzac2zx2701vj8r7g359nmw0")))

(define-public crate-pkginfo-0.1 (crate (name "pkginfo") (vers "0.1.1") (deps (list (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xz") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0pzx71n30wak59hfhmychl3ss9b1vdb227a1f97wpgm9xbgghnva")))

(define-public crate-pkginfo-0.1 (crate (name "pkginfo") (vers "0.1.2") (deps (list (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xz") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0ql6bhng1lz0pdgl0lgnziv77sd9abdc9ay3xyqz3sc5rqa386ms")))

(define-public crate-pkginfo-0.1 (crate (name "pkginfo") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "xz") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0nmcnzfi3wryqr055i8n44vn8d3lpa6v1anbn5wsyzwrhkv00mw5")))

