(define-module (crates-io pk l_) #:use-module (crates-io))

(define-public crate-pkl_fast-0.1 (crate (name "pkl_fast") (vers "0.1.0") (hash "099wqgf15g16jllwqwsdv9a1za45bw2fanx85kd0jpw7pl22r5dp")))

(define-public crate-pkl_fast-0.1 (crate (name "pkl_fast") (vers "0.1.1") (deps (list (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "09fs75niz7pvdz30s6nx3l9axidc57zdibx3if47lb7byddc105x")))

