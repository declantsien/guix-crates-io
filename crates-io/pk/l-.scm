(define-module (crates-io pk l-) #:use-module (crates-io))

(define-public crate-pkl-bind-0.0.1 (crate (name "pkl-bind") (vers "0.0.1") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rmp") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1a2lwr2wb0b4ldwdgkajgd94ry2bjx8iksl5rlmmjc4p2hc45xif")))

(define-public crate-pkl-rs-0.1 (crate (name "pkl-rs") (vers "0.1.0") (hash "1n17vl7iah3sbbmymvzn13syfqymgp09ksawgihsfrxdngb66rxs") (yanked #t)))

(define-public crate-pkl-rs-0.0.0 (crate (name "pkl-rs") (vers "0.0.0") (hash "19fk9lssgbqb1ysnjw50zxiarfavc69mdqxlhjd989g5ijj6g80s") (yanked #t)))

(define-public crate-pkl-rust-0.0.1 (crate (name "pkl-rust") (vers "0.0.1") (hash "1ks589lr74xk5hi5k493sxz8sq92kmmqd6mz2g9677bn1zrf7hcm")))

