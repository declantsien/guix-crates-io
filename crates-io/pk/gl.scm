(define-module (crates-io pk gl) #:use-module (crates-io))

(define-public crate-pkglock-0.1 (crate (name "pkglock") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0qx0729jwlwv2xk73h1sd5i6daf75cs86yz59sjh7ry851wb1b3x")))

