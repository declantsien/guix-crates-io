(define-module (crates-io pk gp) #:use-module (crates-io))

(define-public crate-pkgparse-0.1 (crate (name "pkgparse") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1m8sr8lrk2qn90ll47xnnqwm8vyyjri4wws9g5c0a319w21jq3vi") (yanked #t)))

(define-public crate-pkgparse_rs-0.1 (crate (name "pkgparse_rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0qszrsq09flcy8x3gqkb5aihvd1yy042ckw02kmlq37wrwmpfgla")))

(define-public crate-pkgparse_rs-0.2 (crate (name "pkgparse_rs") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1lbnyhx2n39hgiijwl4viyz8ljgv74n4xjmrazlxgwggzf712r6k")))

(define-public crate-pkgparse_rs-0.2 (crate (name "pkgparse_rs") (vers "0.2.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lkk1idczjfmfg20a1rg5pmp9z90p88slvs9sjlx8wnrfxgg1643") (yanked #t)))

(define-public crate-pkgparse_rs-0.2 (crate (name "pkgparse_rs") (vers "0.2.11") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "147i4lgjrhs2aj75z9qwspsnvmp3lilabpfs24sjb73zznzmzd48") (yanked #t)))

(define-public crate-pkgparse_rs-0.2 (crate (name "pkgparse_rs") (vers "0.2.111") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jnyplh9zfk2hv3dg255sfhhgxjff75f0wsw68hgydjajkq8d9zz")))

