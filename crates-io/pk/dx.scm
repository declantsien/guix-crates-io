(define-module (crates-io pk dx) #:use-module (crates-io))

(define-public crate-pkdx-0.1 (crate (name "pkdx") (vers "0.1.0") (hash "0cgi15rk635dzraww8nmbr0qpckc4s7lvdi86qh1cb186lp2hvxp")))

(define-public crate-pkdx-0.1 (crate (name "pkdx") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "tui-logger") (req "^0.8") (default-features #t) (kind 0)))) (hash "09irma79cz9936hghfi9cdfishmlfbnhhm55xsjrk6rnhimbsqnx")))

