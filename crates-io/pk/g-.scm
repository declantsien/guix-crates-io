(define-module (crates-io pk g-) #:use-module (crates-io))

(define-public crate-pkg-cli-0.0.0 (crate (name "pkg-cli") (vers "0.0.0") (hash "0yww73a26mxdjw4lk8s4jlypj8wbifzvdqh7vzkz570wn7p4ncdw")))

(define-public crate-pkg-config-0.0.1 (crate (name "pkg-config") (vers "0.0.1") (hash "0bfhjb74cbcjfpmczqslybdswzz39mr71p6ynzkaag659q2994xj")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.0") (hash "0b4k8dlwhn1rrf08fh4fq12hf7f4w2ns0n7pz4mbs6p0w4dpzj97")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.1") (hash "13za5amy913dqxbzb9zmlg591xwcifxpw4vssn7gxcx20q46isnq")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.2") (hash "1y17hbpq6vdfj70jjzhykqiff54znn5qbrb8rrsw39c97kf9anf2")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.3") (hash "11lvddxmhppww1dbyjvbscm77nd3v153p8qj0x1nlk9ljkc3yd89")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.4") (hash "0kb529ad14nvl0n2ddliwqwp4fr1h778z3fz3r9qqyb55yj7jqr2")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.5") (hash "084g2kdnvnbb871qrgxabqh5fnh0pm06iygw9iciy08jq2v3x4r5")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.6") (hash "1dy3l4asjilppbfigykdnyh4dfwm026y2qh5cg5kp0c3bs9s4yyj")))

(define-public crate-pkg-config-0.1 (crate (name "pkg-config") (vers "0.1.7") (hash "07q644m1r5wxm83flpg9y8lwczzk4sn61v4mhrxd386n6pvw177b")))

(define-public crate-pkg-config-0.2 (crate (name "pkg-config") (vers "0.2.0") (hash "0mhz9w9lm6m7qpmnmyiqlamzc15k6gm1i42b5gx7dqggb0gfw479")))

(define-public crate-pkg-config-0.2 (crate (name "pkg-config") (vers "0.2.1") (hash "0y2l9agsv34jgq4xgy16jpw6dvj9mhnx7ds2h8606wzsn66di3p3")))

(define-public crate-pkg-config-0.2 (crate (name "pkg-config") (vers "0.2.2") (hash "0i5vdfkv0yxymj54b2f4arx2hvigrkyhv60sm3rdnjdryy1sjmqd")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.0") (hash "0fqivyv0hkbw6snbs1avs3gk93jj7v8i901kyv6havsjq22msl0r")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.1") (hash "095504zj76dxcddlv8xfbg6x68x1f4ynay2laqk6w1vvp6ba1rca")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.2") (hash "1mlz6qb3j875siya4as2waharlmz3p265f2d69j3nzvsfqxmdx3d")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.3") (hash "0qc3yj7n3yawrjfink9fzxvpb6rsyb8csirxhnaa5ljqiknchynr")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.4") (hash "11dlhx4xbzisrahpiivp93asg2mxsrq7djdqm5akjg88v22md7qb")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.5") (hash "15f45i57nsn1kbsvzyxmsag0arksgb3zxawjniizqckixh38413i")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.6") (hash "0byv7s8al3hwyrawj5vj02ps1w5cv7m3c47wlyiig92cwayq09p2")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.7") (hash "1spb4a9l23csnzgkza2nsih5i3xkrcnxg6ammr2v8ag0ixdmi8an")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.8") (hash "1ypj4nj2z9z27qg06v3g40jyhw685i3l2wi098d21bvyri781vlc")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.9") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)))) (hash "00x9vc7667m4r8sn8idgpmj9yf1ih6bj1cdrshf1mkb5h5mlr2rs")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.10") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0f85girr3pqa7abkidwwfym0r6v997jgzwv3n6ahknva8sbyj9q5")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.11") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0bs5a8zysyhynmvbqz09jxw9kk4bcvjpycllcbsp7frxb7imw38i")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.12") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "05mdsmd4wa9bd98hlzzslrydh1ky283sp1740zp0ai9mr3dy8lka")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.13") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0id7xbza5a799jz66l2qqgd2216b1xih61yvfsy3q8c33jm30ihh")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.14") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "135ia995lqzr0gxpk85h0bjxf82kj6hbxdx924sh9jdln6r8wvk7")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.15") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1byjfivxlpbh549scss9kp893pzwfig93w14bwxxn557lp7x5hd7")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.16") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1slikl3p0qbxy37crxynz7zznaf5gzl7ag9w0fyp17zlj06kgmbj")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.17") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0xynnaxdv0gzadlw4h79j855k0q7rj4zb9xb1vk00nc6ss559nh5")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.18") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0cxc4yd9qb40944a2svgci41bws68f1hqvyljhrldwbadda94r6k")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.19") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0k4860955riciibxr8bhnklp79jydp4xfylwdn5v9kj96hxlac9q")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.20") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1jz6qlr0k0nz5bxijdba6bznll98zkfnqx763swr6y1qni0i16vw")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.21") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0vg6ysfj5bxaf3c5dfaqmy1laf2d231qd8s0w3469gpccjxzrqhh")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.22") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "07vy6mn0q6k2adrs7min3rpy999q7kprph0vb1414iwlybs5sa8j")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.23") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0ginjbmkks5avg4cmv566rzm9ahr108bz5yg2bjz7wfp1m7ym8yi")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.24") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1ghcyjp5537r7qigmgl3dj62j01arlpddaq93a3i414v3iskz2aq")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.25") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1bh3vij79cshj884py4can1f8rvk52niaii1vwxya9q69gnc9y0x")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.26") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0q2i61dhqvawc51zfzl3jich57w0cjgfa894hn6ings7ffgsbjba")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.27") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "0r39ryh1magcq4cz5g9x88jllsnxnhcqr753islvyk4jp9h2h1r6") (rust-version "1.30")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.28") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "16kgffwncx5hsppsdf54z6jnjkhwywqy601cxk3rqncyi9zmilv9") (rust-version "1.30")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.29") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1jy6158v1316khkpmq2sjj1vgbnbnw51wffx7p0k0l9h9vlys019") (rust-version "1.30")))

(define-public crate-pkg-config-0.3 (crate (name "pkg-config") (vers "0.3.30") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1v07557dj1sa0aly9c90wsygc0i8xv5vnmyv0g94lpkvj8qb4cfj") (rust-version "1.30")))

(define-public crate-pkg-gentoo-0.0.1 (crate (name "pkg-gentoo") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "03kw9gzh0dafikb1bnir6mij54nrjaz6c7cw4ah5dx3sy75v8rs1")))

(define-public crate-pkg-gentoo-0.0.2 (crate (name "pkg-gentoo") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n14sxkcvn5sccnzfr9x4lj4ckzz3g27xw4jalrmb8z78r34a7yf")))

(define-public crate-pkg-gentoo-0.0.3 (crate (name "pkg-gentoo") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sedregex") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1m54z3avbh8sxi4a789cyxglfws3x585m7y6xj88638ac12n1fzk")))

(define-public crate-pkg-gentoo-0.0.4 (crate (name "pkg-gentoo") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sedregex") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0lgi5hnql3x0pldxchdi8qj1rbk5x88qs1mfs5kj2xq8ry4r6c80")))

(define-public crate-pkg-gentoo-0.0.5 (crate (name "pkg-gentoo") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sedregex") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0651v9sqkrc9h04vkcl24ir23mplc6b1lbal9mbq2qf8mf21d5r4")))

(define-public crate-pkg-gentoo-0.0.7 (crate (name "pkg-gentoo") (vers "0.0.7") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sedregex") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "05lfzr52d77530a8acm15dh31k7kwafgqwy2xz7xidalds8ixgyv") (features (quote (("gentoolkit"))))))

(define-public crate-pkg-gentoo-0.0.8 (crate (name "pkg-gentoo") (vers "0.0.8") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sedregex") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1ggxbyvhgvyc2jzfisn83d93ajg6zjdazfmk18rqvjk0w3hnpgbl") (features (quote (("gentoolkit"))))))

(define-public crate-pkg-gentoo-0.0.9 (crate (name "pkg-gentoo") (vers "0.0.9") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sedregex") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0i92fcys1im47djs6s14wn4s7zs0pdld3n9b8macjw2jyg6ydj6f") (features (quote (("gentoolkit"))))))

(define-public crate-pkg-gentoo-0.1 (crate (name "pkg-gentoo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sedregex") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0yyzv8dzdwpixm3rz2nh9yyb7sxw5ph6vhk46226whp1mcqnzgqp") (features (quote (("gentoolkit"))))))

(define-public crate-pkg-johndoe-0.1 (crate (name "pkg-johndoe") (vers "0.1.0") (hash "0axpyg879x3xqiyssyi54d7x028syidma5y6v9c74b7469nclhj3") (yanked #t)))

(define-public crate-pkg-macros-3 (crate (name "pkg-macros") (vers "3.0.0") (deps (list (crate-dep (name "git2") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)))) (hash "1xmspi4jvlawn41773mvzw9xbkp5dz8hnch3q1vc98r3294jdv6h") (features (quote (("git" "build" "git2") ("default") ("build"))))))

(define-public crate-pkg-types-0.1 (crate (name "pkg-types") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 0)))) (hash "1h7xjzrf34fbk3bznr9qlqfryawslbymg6s1wyb8zcpmq44n6yla")))

(define-public crate-pkg-utils-0.1 (crate (name "pkg-utils") (vers "0.1.0") (deps (list (crate-dep (name "display_derive") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wfh83qswsliy7wqg67ds8gx335qfj1mj22wzhk1mjc4swwj2x6j")))

(define-public crate-pkg-version-0.1 (crate (name "pkg-version") (vers "0.1.0") (deps (list (crate-dep (name "pkg-version-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "04cargk0rf6716a261vsiwcdx56mdjf32pwj0wcqwiyihiyh6svi")))

(define-public crate-pkg-version-0.1 (crate (name "pkg-version") (vers "0.1.1") (deps (list (crate-dep (name "pkg-version-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0pbhh4lclhlf2l8jnm3yqr6da4kc72ara3nx5966kgrgx9k4xpls")))

(define-public crate-pkg-version-0.1 (crate (name "pkg-version") (vers "0.1.2") (deps (list (crate-dep (name "pkg-version-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "1c96cl8l968sd7w6im5js8dksqawcq1ps4icy1z5l9dkzvcpx1qc") (yanked #t)))

(define-public crate-pkg-version-1 (crate (name "pkg-version") (vers "1.0.0") (deps (list (crate-dep (name "pkg-version-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0xx3pkd538g409m3wpgf3jpi6wh7g917wxb5bqs1082bxrhqz10f")))

(define-public crate-pkg-version-impl-0.1 (crate (name "pkg-version-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1mvffs00ssi3ssznwpmpjxjh4n0jaggm1slmhygnxi67kf4cj6gk")))

(define-public crate-pkg-version-impl-0.1 (crate (name "pkg-version-impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0fqs6qsgx8848cp0mvwwlzf01h2l0i88rf10qkmabx3b8xfvyr0m")))

