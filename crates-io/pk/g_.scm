(define-module (crates-io pk g_) #:use-module (crates-io))

(define-public crate-pkg_compile_time-0.1 (crate (name "pkg_compile_time") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "15lj46k64va32y39j57dfmmp5kcmgrihgjnqlvc70ap6rvb984h8")))

(define-public crate-pkg_compile_time-0.1 (crate (name "pkg_compile_time") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1i12ws6baycsfv5xaf20skw3jqavc3psgzcxap4kszkpmqqgs3nd")))

(define-public crate-pkg_compile_time-0.1 (crate (name "pkg_compile_time") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.22") (default-features #t) (kind 0)))) (hash "14zrrb3j2bl8sh6i933yml0hw3qc3v1arcm3pwh46dqkzryhx5z8")))

(define-public crate-pkg_impl-2 (crate (name "pkg_impl") (vers "2.0.0") (hash "18md743ykvfpbqqbsck441j9bdzk13dzv831r4ggwry24fj6y7i7")))

(define-public crate-pkg_manager-0.1 (crate (name "pkg_manager") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)))) (hash "0lxc33h30kfjv1lqr0gm3npr9vv91k6zrgj5f6ak3405rax9nlv3")))

(define-public crate-pkg_manager-0.1 (crate (name "pkg_manager") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)))) (hash "1vilcaca6mfj1mszzdd2p6w1a5nnvn7jir98x7spx2ccy3hlfjwx")))

