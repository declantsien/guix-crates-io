(define-module (crates-io pk bu) #:use-module (crates-io))

(define-public crate-pkbuffer-0.1 (crate (name "pkbuffer") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "147jnbh0chns40d4j8389jk49jr9c5z0lc9vcx2vj5y2q8llqx6n")))

(define-public crate-pkbuffer-0.2 (crate (name "pkbuffer") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1hph7qzl8r83flknpch69z75a5x3pz6h0z3gvfrkwwm7c8fgnr35")))

(define-public crate-pkbuffer-0.3 (crate (name "pkbuffer") (vers "0.3.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1iyvpbbdar2fm6d20aqpghm1bfw8i9pdjfpxj3vyfy5d28nwgqhq")))

(define-public crate-pkbuffer-0.4 (crate (name "pkbuffer") (vers "0.4.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "pkbuffer_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rmfxjr2xrr30n4rmhlmpdgdaw6j5ph8xyh0inni2244i7y746cb")))

(define-public crate-pkbuffer-0.4 (crate (name "pkbuffer") (vers "0.4.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "pkbuffer_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vzdaz6q73ph26vyll9wxc9b9mnbd3swbil2syqpjrqrvn67aqqw")))

(define-public crate-pkbuffer-0.4 (crate (name "pkbuffer") (vers "0.4.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "pkbuffer_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "151dsvv4q7hg0ycsy4lvajh98wb4lc79d5pnln74kc5swrqx3hmz")))

(define-public crate-pkbuffer_derive-0.1 (crate (name "pkbuffer_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "0mfif5xzzflhxkbvscsigaw8zwxs8if6x7xpchb9qp3ip4kav1kw")))

