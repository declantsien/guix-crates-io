(define-module (crates-io pk gb) #:use-module (crates-io))

(define-public crate-pkgbuild-0.1 (crate (name "pkgbuild") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pkgspec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.3") (default-features #t) (kind 2)))) (hash "0kmms8klw2ihw0lz7c3mscf85pdg9lk8idc7ki46cb8lfn2jsl2d") (yanked #t)))

(define-public crate-pkgbuild-0.1 (crate (name "pkgbuild") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pkgspec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.3") (default-features #t) (kind 2)))) (hash "04h5ik9slhsd71m45jabq1zmbwzydlma7g7zablcw1jshqp2rq53") (yanked #t)))

