(define-module (crates-io pk _c) #:use-module (crates-io))

(define-public crate-pk_compiler-0.0.1 (crate (name "pk_compiler") (vers "0.0.1-dev.0") (deps (list (crate-dep (name "pk_parser") (req "^0.0.1-dev.0") (default-features #t) (kind 0)))) (hash "1md8jw2pzy9f5acn1yz5ywz3mcwzwancclv0ma5k0ch7m3wj35nc")))

(define-public crate-pk_compiler-0.0.2 (crate (name "pk_compiler") (vers "0.0.2-dev.0") (deps (list (crate-dep (name "pk_parser") (req "^0.0.1-dev.0") (default-features #t) (kind 0)))) (hash "0zi36z67pf05kj98z9628ifslbw4wa80gslfnlx426j5z6ffgs6w")))

(define-public crate-pk_compiler-0.0.3 (crate (name "pk_compiler") (vers "0.0.3-dev.0") (deps (list (crate-dep (name "pk_parser") (req "^0.0.3-dev.0") (default-features #t) (kind 0)))) (hash "0zv3hfxhag51i3mmsdmnny99ygbqns77599fqd3j10xd56vfm9lp")))

(define-public crate-pk_compiler-0.0.4 (crate (name "pk_compiler") (vers "0.0.4-dev") (deps (list (crate-dep (name "bytecoding") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pk_parser") (req "^0.0.4-dev") (default-features #t) (kind 0)))) (hash "0h4gvfgqy66pzgbnl4bwjilmflky9l4k3fryj15glm9cq64qn6gh")))

(define-public crate-pk_compiler-0.0.5 (crate (name "pk_compiler") (vers "0.0.5-dev") (deps (list (crate-dep (name "bytecoding") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pk_parser") (req "^0.0.5-dev") (default-features #t) (kind 0)))) (hash "1520qnvgimd80paxxm5sdnqs1r50a14j5qavcsypmqmyib76xxl6")))

