(define-module (crates-io pk _v) #:use-module (crates-io))

(define-public crate-pk_vm-0.0.1 (crate (name "pk_vm") (vers "0.0.1-dev.0") (deps (list (crate-dep (name "pk_compiler") (req "^0.0.1-dev.0") (default-features #t) (kind 0)))) (hash "01pmz7pp79zci0va4rx29jbphxgdwkd8hqw2mhsnlg2f3jvf5kz8")))

(define-public crate-pk_vm-0.0.2 (crate (name "pk_vm") (vers "0.0.2-dev.0") (deps (list (crate-dep (name "pk_compiler") (req "^0.0.2-dev.0") (default-features #t) (kind 0)))) (hash "0dz070lnw04vmr1rbr8gszk52hha4892p2g3skiwa8a7f7j723gw")))

(define-public crate-pk_vm-0.0.3 (crate (name "pk_vm") (vers "0.0.3-dev.0") (deps (list (crate-dep (name "pk_compiler") (req "^0.0.3-dev.0") (default-features #t) (kind 0)))) (hash "07pwdrccs8d4x58l6cq76vq1s2drrqlqz9wa0ld9zn20ys9m5mxr")))

(define-public crate-pk_vm-0.0.4 (crate (name "pk_vm") (vers "0.0.4-dev") (deps (list (crate-dep (name "pk_compiler") (req "^0.0.4-dev") (default-features #t) (kind 0)))) (hash "0rm8f484czscak7yndvh3z78ji1vfmn9bpmlcgv13w7hbdrzw8pw")))

(define-public crate-pk_vm-0.0.5 (crate (name "pk_vm") (vers "0.0.5-dev") (deps (list (crate-dep (name "pk_compiler") (req "^0.0.5-dev") (default-features #t) (kind 0)))) (hash "0zqjxjd9nm9pj9r95nhqqcyr9282vmhnqyq0v0vh0cr8gyb1j0mj")))

