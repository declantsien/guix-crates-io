(define-module (crates-io pk he) #:use-module (crates-io))

(define-public crate-pkhex-core-0.1 (crate (name "pkhex-core") (vers "0.1.0") (hash "09qn33kggmp1aw63rral259l5y2d673krr2137lywi29a1l2n840")))

(define-public crate-pkhex-rs-0.0.0 (crate (name "pkhex-rs") (vers "0.0.0") (hash "13h1awnnm3kv3mr870vb12s6d7vrmcgjc72kiq10yvhxasqjwa4i")))

(define-public crate-pkhexcore-0.1 (crate (name "pkhexcore") (vers "0.1.0") (hash "1waxh8f138ms7ax452l90qwdijmb3vp18q93k73cspnywh62gqws")))

