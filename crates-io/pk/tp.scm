(define-module (crates-io pk tp) #:use-module (crates-io))

(define-public crate-pktparse-0.1 (crate (name "pktparse") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "1szfazaxn7pkr2yy82dhcn4cl8sl1zd41ddwrkibrs3rj4b6zdc7")))

(define-public crate-pktparse-0.2 (crate (name "pktparse") (vers "0.2.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.2") (default-features #t) (kind 0)))) (hash "0x7d8bzzyf86angvxww6xvlrzr49iqwrb85nab43sz4p7bvmf7lx")))

(define-public crate-pktparse-0.2 (crate (name "pktparse") (vers "0.2.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0biba0kdjnhvc3d3khfg0kn4fjc42lgzfczkladw65d92m7vcjv2")))

(define-public crate-pktparse-0.2 (crate (name "pktparse") (vers "0.2.2") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0bq35b0vd3kq9b573rfr5ggxzfz32cz420vhy443bs9c603igdhi") (features (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.2 (crate (name "pktparse") (vers "0.2.3") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "142b2fml8sdk23n4kr0kjcqnagzl6ycb7vdbsla57v3mp94b8mjp") (features (quote (("derive" "serde" "serde_derive")))) (yanked #t)))

(define-public crate-pktparse-0.3 (crate (name "pktparse") (vers "0.3.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "096y97j32ylhcc099iz1kay0p16cdl3l01mwsr371l6k4q22qw3h") (features (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.4 (crate (name "pktparse") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fb04kscjccd0zvqrigfizig7vpyg7n0hlrdq8k3amyfml6x4ghz") (features (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.4 (crate (name "pktparse") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0qif4556q7zx17cs5smh3zr7h9pjlq8l4qsz8yhxyri370yirj0r") (features (quote (("derive" "serde" "serde_derive"))))))

(define-public crate-pktparse-0.5 (crate (name "pktparse") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "17a78c29l070mlwjdr3bdph785c9v5rps0if3v0s21w4nwaybhck")))

(define-public crate-pktparse-0.6 (crate (name "pktparse") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1barfz96pp7xh65d6vvhq2rd9vjcqdqkhzx9afs2gdylij923sxa")))

(define-public crate-pktparse-0.6 (crate (name "pktparse") (vers "0.6.1") (deps (list (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1iqaap6ql02q103papms150sn8kilbanh12qvp8rg24935w04sl1")))

(define-public crate-pktparse-0.7 (crate (name "pktparse") (vers "0.7.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ri1glnajfw0a7n6wdcabcj6l93rk58f9bbsnz7xgx7n1h3x6vgv")))

(define-public crate-pktparse-0.7 (crate (name "pktparse") (vers "0.7.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "06sy7lwnhwmkyqfdbi4cs11z55rihipxafbdspnd5zg76pnbgbm8")))

