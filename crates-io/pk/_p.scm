(define-module (crates-io pk _p) #:use-module (crates-io))

(define-public crate-pk_parser-0.0.1 (crate (name "pk_parser") (vers "0.0.1-dev.0") (deps (list (crate-dep (name "pk_lexer") (req "^0.0.1-dev.0") (default-features #t) (kind 0)))) (hash "1qnh27vmgfzzvnynn3gd473l4sxnyymmmdp1msjzc4fbk96cvljh")))

(define-public crate-pk_parser-0.0.3 (crate (name "pk_parser") (vers "0.0.3-dev.0") (deps (list (crate-dep (name "pk_lexer") (req "^0.0.3-dev.0") (default-features #t) (kind 0)))) (hash "1661mr07c63bc30flfw60m0wc44i0k9wjww0l7v1iv6c5d2qy3vq")))

(define-public crate-pk_parser-0.0.4 (crate (name "pk_parser") (vers "0.0.4-dev") (deps (list (crate-dep (name "pk_lexer") (req "^0.0.4-dev") (default-features #t) (kind 0)))) (hash "19g3d31n4449090zayhpgpldzcwxz76898vzxqmvzx2w91krxdxk")))

(define-public crate-pk_parser-0.0.5 (crate (name "pk_parser") (vers "0.0.5-dev") (deps (list (crate-dep (name "pk_lexer") (req "^0.0.5-dev") (default-features #t) (kind 0)))) (hash "0bv27zz0kaip09agx8v0kpqkpmhxhdmrwqdpq2r83zm87s7rlr39")))

