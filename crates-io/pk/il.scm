(define-module (crates-io pk il) #:use-module (crates-io))

(define-public crate-pkill-1 (crate (name "pkill") (vers "1.0.0") (deps (list (crate-dep (name "heim") (req "^0.1.0-alpha.1") (features (quote ("process" "runtime-tokio"))) (kind 0)) (crate-dep (name "skim") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.13") (features (quote ("full" "macros"))) (default-features #t) (kind 0)))) (hash "1p7102jsd78waxmcp6m5v85dd3lnarkwkb6mdhzqd1vmvrfvsdha") (yanked #t)))

(define-public crate-pkill-lib-0.1 (crate (name "pkill-lib") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.26") (default-features #t) (kind 0)))) (hash "1jslxx7iqqwn6qw9zjv8z5va6bwhx9jd1gn1fnmdh7sj91rdgrg1") (yanked #t)))

