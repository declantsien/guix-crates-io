(define-module (crates-io xl ib) #:use-module (crates-io))

(define-public crate-xlib-0.0.1 (crate (name "xlib") (vers "0.0.1") (hash "0ibfryfvc7k8ny37pmfvrsck9vzpndhrz3f7gp7wkma690wbd1j2") (yanked #t)))

(define-public crate-xlib-0.0.2 (crate (name "xlib") (vers "0.0.2") (hash "1k0vj2inkcrhqrq7pkz1pha9x7vnmzri53xxxvqbdf8zdj1h1np7") (yanked #t)))

(define-public crate-xlib-0.0.3 (crate (name "xlib") (vers "0.0.3") (hash "076gcffnihbbawi3h6dk3q9h963l8wfqdfkwd73fxaqpxpm2iyzy") (yanked #t)))

(define-public crate-xlib-0.0.4 (crate (name "xlib") (vers "0.0.4") (hash "0i3i8fywgg689422rphwk3v995s2knr4ybxm19d061xk7gcv4bwr") (yanked #t)))

(define-public crate-xlib-0.0.5 (crate (name "xlib") (vers "0.0.5") (hash "0qsrgdr4sxikzhjcpzbc87c2zs19vz5rbzqp4khdrayx5bvfzmzk") (yanked #t)))

(define-public crate-xlib-0.1 (crate (name "xlib") (vers "0.1.0") (hash "16b1fcks9zd1405ikwhvjjbivabx1kwcnyd0pqxx5iqd0w7kn59k") (yanked #t)))

(define-public crate-xlib-display-server-0.1 (crate (name "xlib-display-server") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "leftwm-core") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.0") (features (quote ("os-ext"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.4") (default-features #t) (kind 0)))) (hash "0ikfb58rffa7hgsmcj1krkph91kpilsc7k9ad9c0r4gykrdg4ssa")))

(define-public crate-xlib-display-server-0.1 (crate (name "xlib-display-server") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "leftwm-core") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.0") (features (quote ("os-ext"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.4") (default-features #t) (kind 0)))) (hash "01a5r3difqzhqvp0c96hj5qbn4lmkc14bgk40bz3qcdzxr832cwk")))

(define-public crate-xlib-display-server-0.1 (crate (name "xlib-display-server") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "leftwm-core") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.0") (features (quote ("os-ext"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "x11-dl") (req "^2.18.4") (default-features #t) (kind 0)))) (hash "1ikwgzmxcmz5q85d3pqly1n1k4zwlp8qr7if8sy6ni8hs0v8wc91")))

