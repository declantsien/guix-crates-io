(define-module (crates-io xl if) #:use-module (crates-io))

(define-public crate-xliff-0.1 (crate (name "xliff") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1sag7gdrjhb334lacbirvpbgqyycndamp3k8pv4gw25dbwm1924f")))

(define-public crate-xliff-0.1 (crate (name "xliff") (vers "0.1.1") (deps (list (crate-dep (name "quick-xml") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0xrf4h6g20mk4z5acfslprh7wi5sh70qbiim8ndyrc1r37vpwlha")))

(define-public crate-xliff-0.2 (crate (name "xliff") (vers "0.2.0") (deps (list (crate-dep (name "quick-xml") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1rsxsqf7nfpngrvxxkf95yy2a3c8szbgqyxxxjpbdar7pjxzf036")))

(define-public crate-xliff-0.3 (crate (name "xliff") (vers "0.3.0-alpha.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)))) (hash "009pwjgnfp3ndsi9xih4ky5i8sxq74iz50ki1in4qgyh85vhxn85")))

(define-public crate-xliff-0.3 (crate (name "xliff") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)))) (hash "078s1vs8d7xz336p4xr1bml0r6n1x2sfa3glpvbcn7k49ss89xlh")))

(define-public crate-xliff-0.3 (crate (name "xliff") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.63") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1zs0lg0lcydx93paic8g42zs8mzxpkd9i4fkyhk31c504wxik348")))

(define-public crate-xliff-0.3 (crate (name "xliff") (vers "0.3.0-alpha.3") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.63") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1md7i24ff096l0kbky512fixpx3smsbkj30w4dy5bz35pyc0zw7q")))

(define-public crate-xliff-0.3 (crate (name "xliff") (vers "0.3.0-alpha.4") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.63") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "014babsg4lgip97niwrbr04q7b5szx0wvnh2k45yldrdcfx5kqc3")))

