(define-module (crates-io xl oc) #:use-module (crates-io))

(define-public crate-xloc-0.1 (crate (name "xloc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1mjvd0sihzcvmhsp70cdcl9gzfh1qrdkawdrm83hm6hc5xiczi6y")))

(define-public crate-xloc-0.1 (crate (name "xloc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0c83b4xisw047v81m6ikdpzlc6ybg0qz86rz7zxxjac0xkjca0xs")))

(define-public crate-xloc-0.1 (crate (name "xloc") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0wax0dmkndj7g1dbnf0xbkfss0ms18iy95pqkfidx23y84l7pgcp")))

(define-public crate-xloc-0.2 (crate (name "xloc") (vers "0.2.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0nhjhnnf84ixasd47csypdbhyip1mlrzl12hz3gs5agp1rjalsba")))

