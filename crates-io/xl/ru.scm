(define-module (crates-io xl ru) #:use-module (crates-io))

(define-public crate-xlru-cache-0.1 (crate (name "xlru-cache") (vers "0.1.2") (deps (list (crate-dep (name "heapsize") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)))) (hash "1zdzwpq0m4irz17n403mzm9mskwr03k5rgryr1wqpz75h62zsjm4") (features (quote (("heapsize_impl" "heapsize" "linked-hash-map/heapsize_impl"))))))

