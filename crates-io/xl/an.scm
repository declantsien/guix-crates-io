(define-module (crates-io xl an) #:use-module (crates-io))

(define-public crate-xlang-0.0.0 (crate (name "xlang") (vers "0.0.0-alpha") (hash "0npqjajhj2pflqrwz9pqr20skhacrrh85bfvivifnk961f06jlsz")))

(define-public crate-xlang-0.0.1 (crate (name "xlang") (vers "0.0.1-alpha") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "xlang-syntax") (req "^0.0.1-alpha") (default-features #t) (kind 0)))) (hash "1870331a8nj7hjq7qnwybrl87khd620v96c38v03frly243l3cy1")))

(define-public crate-xlang-0.0.2 (crate (name "xlang") (vers "0.0.2-alpha") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "xlang-syntax") (req "^0.0.2-alpha") (default-features #t) (kind 0)))) (hash "080c7wm10avdwyf7p9cn5v32110627plnqr59wbk55l9d454rh9q")))

(define-public crate-xlang-0.0.3 (crate (name "xlang") (vers "0.0.3-alpha") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "xlang-syntax") (req "^0.0.3-alpha") (default-features #t) (kind 0)))) (hash "14ibdlp7fdfi00g7vhriz85sid2vaznf7f9kx8j3hg226p9yy0ir")))

(define-public crate-xlang-0.0.4 (crate (name "xlang") (vers "0.0.4-alpha") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "xlang-syntax") (req "^0.0.4-alpha") (default-features #t) (kind 0)))) (hash "17cjig31pibrv3j96jhzwccvcqxzc71mxb93s6wxkcwr0yzcah5i")))

(define-public crate-xlang-0.0.5 (crate (name "xlang") (vers "0.0.5-alpha") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "xlang-syntax") (req "^0.0.5-alpha") (default-features #t) (kind 0)) (crate-dep (name "xlang-vm") (req "^0.0.5-alpha") (default-features #t) (kind 0)))) (hash "0i7lysppv82cwcls1j3m0mffw6h3f0vih8nv1dhjdgsjjyd84i5l")))

(define-public crate-xlang-syntax-0.0.1 (crate (name "xlang-syntax") (vers "0.0.1-alpha") (deps (list (crate-dep (name "lalrpop") (req "^0.19.8") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.8") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "1r2wz1pz7x515fjs5i2pvzc8piagsplha99gz3ng8sgznilhr6bj")))

(define-public crate-xlang-syntax-0.0.2 (crate (name "xlang-syntax") (vers "0.0.2-alpha") (deps (list (crate-dep (name "lalrpop") (req "^0.19.8") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.8") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "12980w91m3x5k2gj12lgb6dsnx6nd7fw7l45vzxa50sxds29sjc7")))

(define-public crate-xlang-syntax-0.0.3 (crate (name "xlang-syntax") (vers "0.0.3-alpha") (deps (list (crate-dep (name "lalrpop") (req "^0.19.8") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.8") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "1p70xkyqhpr16hsvyil8zrig9gjq1zxabc22c93ydhzg1qy08ja7")))

(define-public crate-xlang-syntax-0.0.4 (crate (name "xlang-syntax") (vers "0.0.4-alpha") (deps (list (crate-dep (name "lalrpop") (req "^0.19.8") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.8") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "19w7qb7yvrjl19z561qi430yxwcry4ya97hjhiabi6saqp0b0s4d")))

(define-public crate-xlang-syntax-0.0.5 (crate (name "xlang-syntax") (vers "0.0.5-alpha") (deps (list (crate-dep (name "lalrpop") (req "^0.19.8") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.8") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "09xjw1j1d635rc0f7fm6s68l9x9ir8cswi1zfb57by1am2d50zds")))

(define-public crate-xlang-vm-0.0.5 (crate (name "xlang-vm") (vers "0.0.5-alpha") (deps (list (crate-dep (name "gc") (req "^0.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "085qlbmykqjkvnbwmab834shbr2h697scwy8rjr4gn287rkgi03p")))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.0") (deps (list (crate-dep (name "xlang_host") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qqz4zrhi7rhn0ikcn8r66id8q6k61w771l3whq0yc8l4ddnxcpk") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.1") (deps (list (crate-dep (name "xlang_host") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jvc4xvcfibi4q4ga2fipnqib5iqv3wlba155g1l5sxkin5jvxph") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.2") (deps (list (crate-dep (name "xlang_host") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1awnyw56fgq035m1i1d58llyyd9r1n90q4684s2580z0scj18dn7") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.3") (deps (list (crate-dep (name "xlang_host") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0isrwm0lvrmsfjj4lhjnlpagddvr6g8c5h3f8q6n7g7zcfq25hyz") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.4") (deps (list (crate-dep (name "xlang_host") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1pxr913mx80j0xrvvd9an14yi07pbggirwfqdvff72zrm9nvi0x5") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.5") (deps (list (crate-dep (name "xlang_host") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1yij060r4kp09gcwd3qfd3icw2zlb9dlqmpdgyjhdl4c43a8hg3d") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.6") (deps (list (crate-dep (name "xlang_host") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "12gv31m5vj7pbqppgl4qvd74i6danmrbmcpyyakca2pvpyz1p7n6") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.7") (deps (list (crate-dep (name "xlang_host") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0van3wnqnh0mid3s9wnyy050sj161h61i4j220b1a3f0j5df7yvf") (yanked #t)))

(define-public crate-xlang_abi-0.1 (crate (name "xlang_abi") (vers "0.1.8") (deps (list (crate-dep (name "xlang_host") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1f9rnsj05255jhsky5wlfvsv8fkjlfkl97y3074c0mafiwfvvcg1")))

(define-public crate-xlang_abi-0.2 (crate (name "xlang_abi") (vers "0.2.0") (deps (list (crate-dep (name "xlang_host") (req "^0.2") (default-features #t) (kind 0)))) (hash "18xwhbqg6y04x0fq8fihh30sgidwhqr9qg4fp9vs3mrfvdy40hrr")))

(define-public crate-xlang_backend-0.0.0 (crate (name "xlang_backend") (vers "0.0.0") (hash "1za3lcd3gnqyjgag42jjc8crchclri9j09jymkaz9vp892yv24wz")))

(define-public crate-xlang_host-0.1 (crate (name "xlang_host") (vers "0.1.0") (hash "0z749qcdipjz5ddhmjs8jf8kiw3sfc4brqn7gdg6ailnzlyn4w2p")))

(define-public crate-xlang_host-0.1 (crate (name "xlang_host") (vers "0.1.1") (hash "1j95aw76dz55zrglbri0qs64qlsfwbk3x5rk9dil725sqs3mmf6s")))

(define-public crate-xlang_host-0.2 (crate (name "xlang_host") (vers "0.2.0") (deps (list (crate-dep (name "cfg-match") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "07zp86vgm80jz6ck6qa00bhmfc8xjhv1al49mjar3mblj9h7k72l")))

(define-public crate-xlang_host-0.2 (crate (name "xlang_host") (vers "0.2.1") (deps (list (crate-dep (name "cfg-match") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "01qj9f3yvfknr3pn48g49dw6kl0m5phgp0kx45nkpssdvck1zk38")))

(define-public crate-xlang_interface-0.1 (crate (name "xlang_interface") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "xlang_abi") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "xlang_host") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "xlang_targets") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hlgak073swmffhwcsj4c23irlm38c0xm7k6aikz76wfc47ddd7r")))

(define-public crate-xlang_opt-0.0.0 (crate (name "xlang_opt") (vers "0.0.0") (hash "0swnn8231dbgj4yrw14z4f5l9hr2n9lcrfh0k9yd0h64bn0ba0rh")))

(define-public crate-xlang_struct-0.1 (crate (name "xlang_struct") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "fake-enum") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "xlang_abi") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "xlang_targets") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1w2xns12kfs8za4fdiksgxqwnpfzn87madmrarbd02b51apifjz7")))

(define-public crate-xlang_targets-0.1 (crate (name "xlang_targets") (vers "0.1.0") (deps (list (crate-dep (name "target-tuples") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "xlang_abi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dmfz7czm5lnq2vs1hmvlr0ifmnhpnzvi7wy4vrbffkr07km7dx1")))

