(define-module (crates-io xl in) #:use-module (crates-io))

(define-public crate-xlint-0.2 (crate (name "xlint") (vers "0.2.1") (deps (list (crate-dep (name "camino") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "guppy") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "hakari") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1xa0pxs4vh17qzvh1l0qp0h7jbcgsraxw29vxagl4n8cywdzjy1l") (yanked #t)))

(define-public crate-xlint-0.2 (crate (name "xlint") (vers "0.2.2") (deps (list (crate-dep (name "camino") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "guppy") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "hakari") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "02s56l9fylaklxn3rrizv9w4dvxs1md0zbv4nbd78r2dq8lljvvm") (yanked #t)))

