(define-module (crates-io xl is) #:use-module (crates-io))

(define-public crate-xlist-0.0.0 (crate (name "xlist") (vers "0.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (features (quote ("derive" "env" "unicode" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1nbrwnkgbxfkq4bhr018s94rw7za8q8cy1iazxzs86bs43vng0h8")))

