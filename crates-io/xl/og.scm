(define-module (crates-io xl og) #:use-module (crates-io))

(define-public crate-xlog-0.1 (crate (name "xlog") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.0") (default-features #t) (kind 2)))) (hash "0w4w15a7acv95lnq31qyp5whmli3bnssxv6kyqncrqswfdzzad9j")))

(define-public crate-xlog-0.1 (crate (name "xlog") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.0") (default-features #t) (kind 2)))) (hash "0wj5prspi8rrj9fvx5q40ynf9s6qbcg1dxnz7wlj82y3bza7n54y")))

(define-public crate-xlog-0.1 (crate (name "xlog") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.0") (default-features #t) (kind 2)))) (hash "0fh37731wvrqlwsksrqq774gsngx0qwmgxb4k1b8i40jx3s91lpx")))

(define-public crate-xlog-0.1 (crate (name "xlog") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vq5w5a9fbmcpczbab2g39iz5va33n2i92i0gn7nznyfan02bgg2")))

(define-public crate-xlog-0.1 (crate (name "xlog") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "femme") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "0lk247i3vikgb7apl74wa83lf5msd6988xam8s0f8dqjqmrk483b")))

(define-public crate-xlog-0.1 (crate (name "xlog") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "femme") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "17fpjxqja7r1nw0n6rd37fnpbblhgji8drmgdgy09d31jmz18gx0")))

(define-public crate-xlog-0.1 (crate (name "xlog") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "femme") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "14k7rwn6vlmg0wpizpp9jil3d59lmx0rx49ajbmpghks5g8j4wqs")))

(define-public crate-xlog-0.2 (crate (name "xlog") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "femme") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "06ad7rsrba9wyf0j7kpscmfyd6ikhm51n8byn6rybw98ragd2mrr")))

(define-public crate-xlog-0.2 (crate (name "xlog") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "femme") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "0x4apk1qnrl6amk5ixawcv604my872dsgdkiyw95ilipjbiwjb4g")))

(define-public crate-xlog-0.2 (crate (name "xlog") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "femme") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.11") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "1y7ql7hhmvya0gb2vfisgim4giflll48wnr2nynp0i4b292fb4xn")))

(define-public crate-xlog-rs-0.1 (crate (name "xlog-rs") (vers "0.1.0") (hash "0ll3z8j2a791ivxgb9wmzjvkj6spmic966ijd26g3k9rm9wnlm2r")))

(define-public crate-xlog-rs-0.2 (crate (name "xlog-rs") (vers "0.2.0") (hash "10yjj5xd74y0ri53a14xnpmvj3xb09nawgbjniy5d63yywx6gn00")))

(define-public crate-xlog-rs-0.3 (crate (name "xlog-rs") (vers "0.3.0") (hash "1cdmd2dgab1rkhnw81df7a46qglanz32r3rg23lbn0vml9q76sf6")))

(define-public crate-xlog-rs-0.3 (crate (name "xlog-rs") (vers "0.3.1") (hash "16iss7czbghbvmxdzxl64jadm2anpm2iyn8nwjjqygxalprkrdq2")))

(define-public crate-xlogic-0.0.0 (crate (name "xlogic") (vers "0.0.0") (hash "01qxdyc76ryh7w23ayy0gg58gkg0ba9v6hh3rm9bsl7fp4pqz0kr")))

(define-public crate-xlogic-0.1 (crate (name "xlogic") (vers "0.1.0") (hash "0l7dspzpk84k77pycrbnp36n29qbkcd9wyzrjz4q32cxykvh7q6k")))

(define-public crate-xlogic-compiler-0.1 (crate (name "xlogic-compiler") (vers "0.1.0") (hash "1hqkx7h89cc184wkbk7myq9da2as84l5xrmhhhi90q7qcrharmjd")))

(define-public crate-xlogic-core-0.1 (crate (name "xlogic-core") (vers "0.1.0") (hash "17xapmq0877rhifmlx0fy4nzaigzhci30sjrl9kqmgix5ybczkk9")))

(define-public crate-xlogic-derive-0.1 (crate (name "xlogic-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "155irdg4ly4fh9fkb1ll92dn512bbjw920gxniksg61ssaqxn0f4")))

(define-public crate-xlogic-tests-0.1 (crate (name "xlogic-tests") (vers "0.1.0") (deps (list (crate-dep (name "xlogic-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ycll5dl66j73d0ssab5vngvxy311b4ciiknrvc629yx3yns38ms")))

