(define-module (crates-io xl _t) #:use-module (crates-io))

(define-public crate-xl_to_csv-0.1 (crate (name "xl_to_csv") (vers "0.1.0") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0vzlnd5mm9cvi15pi338gsr1b8gxrmvnghlyn36grhh6j85b32ln")))

(define-public crate-xl_to_csv-0.1 (crate (name "xl_to_csv") (vers "0.1.1") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0r79lwmbym2zvh0id2br7x5cgrc8igm4dmikimndq26h9wvj5f80")))

(define-public crate-xl_to_csv-0.1 (crate (name "xl_to_csv") (vers "0.1.2") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "100rf1gb8rbj2bwdabb26cp88sj9dks6iqzp9qhbi3j818j8wsdc")))

