(define-module (crates-io xl #{2t}#) #:use-module (crates-io))

(define-public crate-xl2txt-0.1 (crate (name "xl2txt") (vers "0.1.0") (deps (list (crate-dep (name "calamine") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0ga54dfi1zsvwvj5nqka7k5dxx40p203sc74j7vl12nmf6gxpm3b")))

(define-public crate-xl2txt-0.1 (crate (name "xl2txt") (vers "0.1.1") (deps (list (crate-dep (name "calamine") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "18zcy3kv80ijwb6nrqnq80rk85hhwc216i29dl1hq6isy9iifcz3")))

