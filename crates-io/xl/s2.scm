(define-module (crates-io xl s2) #:use-module (crates-io))

(define-public crate-xls2txt-1 (crate (name "xls2txt") (vers "1.0.0") (deps (list (crate-dep (name "calamine") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "0ln21j5vbcbjx1kl4c2lcn94bw9wm6bwzk201ir3fai9zhxkxh8n")))

(define-public crate-xls2txt-1 (crate (name "xls2txt") (vers "1.0.1") (deps (list (crate-dep (name "calamine") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "1b3ak0sk3llssvpi23dxk635sx898apd7175k0q7l96p4m1x150c")))

(define-public crate-xls2txt-1 (crate (name "xls2txt") (vers "1.0.2") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "06k6v1n7qvn8s8d0gx87ai7nahvj8aijnaly3dwcl8xg82rkdr4k")))

(define-public crate-xls2txt-1 (crate (name "xls2txt") (vers "1.1.0") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "guard") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "06z9498piqmrbs18ng68bcqn4i95npisvywfxh9savm5397xznk5")))

(define-public crate-xls2txt-1 (crate (name "xls2txt") (vers "1.2.0") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "guard") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1g44cda7aifffa0zmjbhfz3y19xxfk0gmfqyzgn7ac2hwhjbcln3")))

(define-public crate-xls2txt-1 (crate (name "xls2txt") (vers "1.2.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "calamine") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "guard") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)))) (hash "05b6q9v963sapldn497z2jw58cmcqqyr80yhsik83kp3yzs1r0wi")))

