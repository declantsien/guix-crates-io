(define-module (crates-io yy id) #:use-module (crates-io))

(define-public crate-yyid-0.1 (crate (name "yyid") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "10hdvndf9x9y400n1sy674zi8zg83gshw0rv9zypi5yd43w28xqh")))

(define-public crate-yyid-0.1 (crate (name "yyid") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "18skasbl84iibvr890sr43c904jr560arxzgjrp6h1mvhap41cb9")))

(define-public crate-yyid-0.1 (crate (name "yyid") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "03fbxskc78kps5w4vc7jvxbpjsym106r8qmf3qvp7g2g8426lqal")))

(define-public crate-yyid-0.1 (crate (name "yyid") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ms1snw282g84pq0k8hbq8sy03jhdgdrlb0v7z6ms1ls9jidcl8n")))

(define-public crate-yyid-0.2 (crate (name "yyid") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kw4bs0g3sb73p4wm1gb09q5pn7y0dsgbci202y0vx0qr0406vf8")))

(define-public crate-yyid-0.2 (crate (name "yyid") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ypw3fn5vrpwv08pv77778i3fc4s2607m0s558w6nj7n3r54n4jz")))

(define-public crate-yyid-0.2 (crate (name "yyid") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "035f1498wa99awyxciyhj7vyxx4xdaa5xali4hpnasrn8acwjxcm")))

(define-public crate-yyid-0.2 (crate (name "yyid") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "10012jkjrgvap9ax0a6yrlqjimkdb73i7w0v0w9bhczjbzmzp2rb")))

(define-public crate-yyid-0.2 (crate (name "yyid") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "15ihjcl4yxmfx781jirdjzwhs6v2ry55g9fndrfyix1hpbhxmwr7")))

(define-public crate-yyid-0.3 (crate (name "yyid") (vers "0.3.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19ck7v439x70fvmq5211v88s4ddcsggaa8269ql35a5mgmf55wby")))

(define-public crate-yyid-0.4 (crate (name "yyid") (vers "0.4.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dc6f3g43f38438bcgrj7pk0g0q60zz8l6flv9pch3hpchvpykil")))

(define-public crate-yyid-0.5 (crate (name "yyid") (vers "0.5.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n3y6kvcq7kxgrnqwbgw6sgqxzqj8a6ap6sigk4gdjyk6xr7fa2n")))

(define-public crate-yyid-0.6 (crate (name "yyid") (vers "0.6.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1808ywys6mr6nljpm8v9333lg4q7j36s99r2yk9x2wvb7kr8z767") (features (quote (("std"))))))

(define-public crate-yyid-0.7 (crate (name "yyid") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "08d6v1pafwa5dwb2hidj8p7adihaicpn36ixrwx0jbj6n0yyjsd0") (features (quote (("std") ("fast-rng" "rand") ("default")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

