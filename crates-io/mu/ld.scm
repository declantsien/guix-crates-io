(define-module (crates-io mu ld) #:use-module (crates-io))

(define-public crate-muldiv-0.1 (crate (name "muldiv") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "0.3.*") (default-features #t) (kind 2)))) (hash "1ndylirsmq6bv2jxp8ds4gb6rij1msihxgfrl75z4vdkpvg2cz0v") (features (quote (("x86-64-assembly") ("default"))))))

(define-public crate-muldiv-0.1 (crate (name "muldiv") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "0.3.*") (default-features #t) (kind 2)))) (hash "0gjvdhij5c6xbfzglnz58vpy2hwqrhs69qi0rhc2mn4c5smgbghw") (features (quote (("x86-64-assembly") ("default"))))))

(define-public crate-muldiv-0.2 (crate (name "muldiv") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "03wqr4v1lwm18m094mz9vwpysyfgjpmf0dbqi5n5cb53s82rl6j5")))

(define-public crate-muldiv-0.2 (crate (name "muldiv") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "014jlry2l2ph56mp8knw65637hh49q7fmrraim2bx9vz0a638684")))

(define-public crate-muldiv-1 (crate (name "muldiv") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1wq74n9sh4mgc31hrs56bzrq29w8v9g9znyyrhl2f60ll7fnw4xm")))

(define-public crate-muldiv-1 (crate (name "muldiv") (vers "1.0.1") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1c6ljsp41n8ijsx7zicwfm135drgyhcms12668ivvsbm1r98frwm")))

