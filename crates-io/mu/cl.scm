(define-module (crates-io mu cl) #:use-module (crates-io))

(define-public crate-mucli-0.1 (crate (name "mucli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.15") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "simplecrypt") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "18kb5d3n1pdlrvz2xqr2zn9wry7k2binhinrhbhypvd6cigahs5f")))

