(define-module (crates-io mu ne) #:use-module (crates-io))

(define-public crate-mune-0.0.0 (crate (name "mune") (vers "0.0.0") (hash "15l2h20hmk1sf19c5lcfpkymxvkxj6j4pv3hlk70qqnsfd2i29b2")))

(define-public crate-muneeb_function-0.1 (crate (name "muneeb_function") (vers "0.1.1") (hash "0dn8jx8nr5s9719s7hvblim3068z5zyrldr220afvjv1jh1z2jf4")))

(define-public crate-munemo-rs-0.1 (crate (name "munemo-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0fmvn6hx2nllw6xz7kj8w9488jsmqd7k43ybnd68pi3micixzmvq")))

(define-public crate-munemo-rs-0.1 (crate (name "munemo-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "12bj2aca3d6n395fic1lzpjsb0lwvcf4c563pilc4dkky8xmz2rg")))

