(define-module (crates-io mu g-) #:use-module (crates-io))

(define-public crate-mug-bancha-0.0.0 (crate (name "mug-bancha") (vers "0.0.0") (hash "0pv31cpaybcsg0vdmax91i5ij0dqaid59jjsqxbz7idl6zmhhrnh") (yanked #t)))

(define-public crate-mug-bancha-0.0.1 (crate (name "mug-bancha") (vers "0.0.1-discontinued") (hash "04j5q87kwfpbc3sblc5a6pjlj319n9yz8a9akfcs3ibcg79vxw54")))

