(define-module (crates-io mu bo) #:use-module (crates-io))

(define-public crate-mubo-0.1 (crate (name "mubo") (vers "0.1.0") (hash "1j8f8dkm2r2gkwsb0260795v1yhxqsq2pjbpbjpq9pj1g0rwmmps")))

(define-public crate-mubo-0.1 (crate (name "mubo") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0xhfi1lcdzwy6883li33rafgxv13kkpfb4vgfn9xk0c98km07wig")))

(define-public crate-mubo-0.1 (crate (name "mubo") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0885vpxzj8svc7d30bjn2ky9plyd117w4vdsm1bww7yclrfqgqnp")))

(define-public crate-mubo-0.1 (crate (name "mubo") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1xlz0f0sahkafxxnnb13kbllkps5k5p15jzf2ppk73rd6329n5is")))

(define-public crate-mubo-0.1 (crate (name "mubo") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "13p770nl0x64jlhh3mqnji0wvk6bg2227lddgmf35mpy6c7lg27m")))

(define-public crate-mubo-0.1 (crate (name "mubo") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0fqd7iw8pgxhajpfws3j6vb1a4jsyxrgc8iajzw6imw36ph9l8kh")))

(define-public crate-mubo-0.1 (crate (name "mubo") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1z4knczz1rnivyw7abyfapbpxgpjx4p122js1d69rr838xi3a565")))

