(define-module (crates-io mu sc) #:use-module (crates-io))

(define-public crate-muscab1-pac-0.0.1 (crate (name "muscab1-pac") (vers "0.0.1") (deps (list (crate-dep (name "bare-metal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req ">= 0.5.8, < 0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ha2hmwxa2a5kjn71ri10cwi3laz0rw401wh3s10bmxrs97v5gpm")))

(define-public crate-muscat-0.1 (crate (name "muscat") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "npyz") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fmrg40w9xms6sa5qv3s7jp1vn0z9g544imdn7xvw9mdlgkw7vd8") (yanked #t)))

(define-public crate-muscat-0.1 (crate (name "muscat") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "npyz") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1219rw7w1afpa1bhrqznkfc2320gm49wgmva624dk21gzrv5x911")))

(define-public crate-muscat-0.2 (crate (name "muscat") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "npyz") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "01j6j3h9krhb8727nzz4bj639mbsw2mg58ggpjvwfynch3n9bgnp")))

(define-public crate-musclecalc-0.0.0 (crate (name "musclecalc") (vers "0.0.0-alpha") (hash "1ajfgq11iis7jzn0xabgzfwknf07k1sfgs5wrw3wbnybz1him6yq") (yanked #t)))

(define-public crate-musclecalc-0.1 (crate (name "musclecalc") (vers "0.1.0-alpha") (hash "070mgwl6nwpf59mw0fj8gmrzg9d2jg8jsrr877rblfl4fsjg32cn") (yanked #t)))

(define-public crate-musclecalc-0.1 (crate (name "musclecalc") (vers "0.1.0") (hash "0amrhk26qnwda1kp2ngydz95b2lkfrpydwnndnjssg4267lplzyb")))

(define-public crate-musclecalc-0.2 (crate (name "musclecalc") (vers "0.2.0") (hash "1849qng8s7flzsmlmmv46zm4w6h4502wfwd04s8nhdyra832asd3")))

(define-public crate-musclecalc-0.2 (crate (name "musclecalc") (vers "0.2.1") (hash "0kmqkn2xhslyihw5bz73yxs8zfwlvhpm67xf5liidn0izplnqrhd")))

(define-public crate-musclecalc-0.2 (crate (name "musclecalc") (vers "0.2.2") (hash "057hlaybcrrlnkm405fii6410d3drsqry8sw58qz0dg7caiqgrxk")))

(define-public crate-musclecalc-0.2 (crate (name "musclecalc") (vers "0.2.3") (hash "1hh3xw203rasiz9xz6bwrxz3i9ng6rh95i3zks1h04m7vg0zqy2f")))

(define-public crate-musclecalc-0.2 (crate (name "musclecalc") (vers "0.2.4") (hash "0khzjh82frrd6l8llfa9rpcy3k35bzhismgrha28m6npqp3fpwh3")))

(define-public crate-muscleman-0.1 (crate (name "muscleman") (vers "0.1.0") (hash "0kkz73p3r11y63sdsqxk2lk1aagds86sghs799ihh8ag90pn84qv")))

(define-public crate-muscleman-0.2 (crate (name "muscleman") (vers "0.2.0") (hash "1f4z5y8cqnj6niqavdg77g2iv13f1nr7cqlhd5n0s2j59s1p2kw4") (yanked #t)))

(define-public crate-muscleman-0.2 (crate (name "muscleman") (vers "0.2.1") (hash "1k2nbxkjxw1kp1inmw5gn56yizlcb6dr875n1mvlsd7ak4b1g058")))

(define-public crate-muscleman-0.3 (crate (name "muscleman") (vers "0.3.0") (hash "00g08fyhb24vkpg6g96yj8zag9mnhxpsvg2n6xnxg9wy2xd46dy2")))

(define-public crate-muscleman-0.3 (crate (name "muscleman") (vers "0.3.1") (hash "1wa7nlch8hcl37a4wh3m1h3p5bgb5fvbiwq9fzk0mmvgxfvn619x")))

