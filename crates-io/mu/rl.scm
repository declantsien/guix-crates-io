(define-module (crates-io mu rl) #:use-module (crates-io))

(define-public crate-murloc-0.1 (crate (name "murloc") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bzz4h8ykg2h04mdx3inzp3apzcgn7h7kac9nwgl0msrwiv6bi1l")))

(define-public crate-murloc-0.1 (crate (name "murloc") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "12b1mqzqzfpjz458cq9i9dibjs1729fw565xn0294yq0m5hzybm1")))

(define-public crate-murloc-0.1 (crate (name "murloc") (vers "0.1.2") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n0y3lggwlvkwa042c09ldrkkiqiv7r0hdmxpkn4ga8a11pjpnyg")))

