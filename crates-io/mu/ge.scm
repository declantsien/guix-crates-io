(define-module (crates-io mu ge) #:use-module (crates-io))

(define-public crate-mugen-0.0.1 (crate (name "mugen") (vers "0.0.1") (hash "11ry89mjlah553hx05yfv8n8cxv0aaj5z8qj4fch6l0jqdrh1ki8")))

(define-public crate-mugen-air-0.0.1 (crate (name "mugen-air") (vers "0.0.1") (deps (list (crate-dep (name "indoc") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "15pn7f963m4k5a8a27z3dvmp2cw518ll77xmh62kvs0q46yf1fv6")))

(define-public crate-mugen-sff-0.0.1 (crate (name "mugen-sff") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0qkij269k71v8ncjd650bgxmlk914sxnw9c1b6k67np7qi4ylhcd")))

(define-public crate-mugen-snd-0.0.1 (crate (name "mugen-snd") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "05vmsk1qclzr6h3n5byhfr38fbx13vy6hw7dvwi3i28g7ky4fisz")))

