(define-module (crates-io mu r3) #:use-module (crates-io))

(define-public crate-mur3-0.1 (crate (name "mur3") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "06xvdwybg2f0cb5knwz92smc32ri0dkwmv4hqgj8vdi13sg4ibwp")))

