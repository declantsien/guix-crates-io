(define-module (crates-io mu eu) #:use-module (crates-io))

(define-public crate-mueue-0.1 (crate (name "mueue") (vers "0.1.0") (hash "0nhv9igw45c2f09cyaapnsvq88003dppaxy7mhpsrqmn080x3964")))

(define-public crate-mueue-0.1 (crate (name "mueue") (vers "0.1.1") (hash "1sfmamqf1z1hmhbs6nwxymqn0izdch9w60q3g33np9z8sid0s3pw")))

(define-public crate-mueue-0.1 (crate (name "mueue") (vers "0.1.2") (hash "0y39pcpnmzgc6xw9bmislgkjkxk3v46j00wlvpgbjg84kj5l22r7")))

(define-public crate-mueue-0.2 (crate (name "mueue") (vers "0.2.0") (hash "1890ini8j5czd44qmrj5i2d6ljrrd145ydv7kbrrhw93bd1x93dr")))

(define-public crate-mueue-0.2 (crate (name "mueue") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "04ccx84r1m0wzjbp2w96ljvlc3hhiv472dqn1gyidzs2qn5ksf6c")))

(define-public crate-mueue-0.2 (crate (name "mueue") (vers "0.2.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1sdyaqqbh122j7njs87vs2b8ziybkyyz3i2dhgzzhbqf6krx37nq")))

(define-public crate-mueue-0.2 (crate (name "mueue") (vers "0.2.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1sa65wp8srv3fhgxm2fd0a9vgmbapa74vyhvbrsk8qxd46c6jj1m")))

(define-public crate-mueue-0.2 (crate (name "mueue") (vers "0.2.4") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "08l1kinf0hik7byp1n4932w9b1pwzsxya8nw4hxi864q6mad5hs5")))

(define-public crate-mueue-0.2 (crate (name "mueue") (vers "0.2.5") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.16") (default-features #t) (kind 0)))) (hash "05hdrvw1m70a8kksinvi7fmcwbqbxrv5yqgsvp850ri3c7m9inls")))

(define-public crate-mueue-0.2 (crate (name "mueue") (vers "0.2.6") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.16") (default-features #t) (kind 0)))) (hash "09s2pm4m53ng8khplrrizfr3yk1rfs88znsr2riazz81jx8x48j2")))

(define-public crate-mueue-0.3 (crate (name "mueue") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1dlbfd0ply48q0vfnd75lg8849m2gh1gr2fv0yf2zi7fxlhxfffl")))

(define-public crate-mueue-0.3 (crate (name "mueue") (vers "0.3.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "03y3kafczggx4vkhg0hjjp3plbbc7m41ffxw7c9cjjr5nr68qilp")))

(define-public crate-mueue-0.4 (crate (name "mueue") (vers "0.4.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0dg228k103d0vipjq1zka74m7flivrg90hgvvbflqwzx7d5p9gzi")))

(define-public crate-mueue-0.5 (crate (name "mueue") (vers "0.5.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "14qdi48n8hhszxgsfqdpm8fdd1m6163bww2v207gp4r652mna00y")))

(define-public crate-mueue-0.5 (crate (name "mueue") (vers "0.5.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1qrg39qd345wg5gj3p3b09fqr7iwmmvbq4c14x0w4251y7gv74v1")))

