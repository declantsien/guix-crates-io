(define-module (crates-io mu nd) #:use-module (crates-io))

(define-public crate-mundane-0.1 (crate (name "mundane") (vers "0.1.0") (hash "1dfa3101vsmi9si48cs1bl2afpx8r8vhxffs10lgjrllqp01c178")))

(define-public crate-mundane-0.2 (crate (name "mundane") (vers "0.2.0") (hash "1qbjgl6dg5vpdsy9rhyngvhps79qlys6qjimy5hzvmna06dwyaav") (features (quote (("rand-bytes") ("kdf") ("insecure"))))))

(define-public crate-mundane-0.2 (crate (name "mundane") (vers "0.2.1") (hash "0dbnjfi8wq1ccss69q7dy2sdb1dnnfjbldpjw6fdsqragmwhmdka") (features (quote (("rand-bytes") ("kdf") ("insecure")))) (yanked #t)))

(define-public crate-mundane-0.2 (crate (name "mundane") (vers "0.2.2") (hash "0immrhzd1gk6ax3z7ir1ma8jig4qhqvp2jqmf0wdnvmy68x4kqcs") (features (quote (("rand-bytes") ("kdf") ("insecure"))))))

(define-public crate-mundane-0.3 (crate (name "mundane") (vers "0.3.0") (hash "04q8373hn7cm8fvifwj4njwvas3vnz3h69450087v3dxzicjwn0j") (features (quote (("rand-bytes") ("kdf") ("insecure") ("experimental-sha512-ec"))))))

(define-public crate-mundane-0.4 (crate (name "mundane") (vers "0.4.0") (deps (list (crate-dep (name "goblin") (req "^0.0.24") (default-features #t) (kind 1)))) (hash "0pvrgfj9bbfqpcicn7dz3w3l07i6fyd12zalk5naah5w2ad1a2yv") (features (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.4 (crate (name "mundane") (vers "0.4.1") (deps (list (crate-dep (name "goblin") (req "^0.0.24") (default-features #t) (kind 1)))) (hash "0labydgjhazh8qgsimwx6zd622ibhzj35qjgbcgz199b26hp7psq") (features (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes")))) (yanked #t)))

(define-public crate-mundane-0.4 (crate (name "mundane") (vers "0.4.2") (deps (list (crate-dep (name "goblin") (req "^0.0.24") (default-features #t) (kind 1)))) (hash "0b2k541a002880mms7i6mw22ywsbpfmd3g0j9al570ky1pmq4mkk") (features (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.4 (crate (name "mundane") (vers "0.4.3") (deps (list (crate-dep (name "goblin") (req "^0.0.24") (default-features #t) (kind 1)))) (hash "00470k07yfpxdaf2jgqfrz8mb1w4clx6kv23d29a4crzvkypbri5") (features (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.4 (crate (name "mundane") (vers "0.4.4") (deps (list (crate-dep (name "goblin") (req "^0.0.24") (default-features #t) (kind 1)))) (hash "0z09kp3lzspr5lcrip7knaayp7hsj7mg0jxiljx9gdkapdaah3wy") (features (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundane-0.5 (crate (name "mundane") (vers "0.5.0") (deps (list (crate-dep (name "goblin") (req "^0.0.24") (default-features #t) (kind 1)))) (hash "018kl1jzszd7ydlh23lnkfsnvza3fvlg41mwqdr3l4sbgh358y14") (features (quote (("run-symbol-conflict-test") ("rsa-test-generate-large-keys") ("rsa-pkcs1v15") ("kdf") ("insecure") ("experimental-sha512-ec") ("default" "rsa-test-generate-large-keys") ("bytes"))))))

(define-public crate-mundis-rayon-threadlimit-1 (crate (name "mundis-rayon-threadlimit") (vers "1.9.13") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "1253sjrgw4rqzc3a942nabrqx3fh7jdpma16q9d6p9r8xpvmw7bj")))

