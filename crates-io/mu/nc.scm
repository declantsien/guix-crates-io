(define-module (crates-io mu nc) #:use-module (crates-io))

(define-public crate-munch-0.1 (crate (name "munch") (vers "0.1.0") (hash "19jyq55c9z7x45523w3rs18pkq9sg76m31fb1gz028lsg8s67xc8")))

(define-public crate-munch-0.2 (crate (name "munch") (vers "0.2.0") (hash "1q2w2a44c03387m0zj8hrcxkdkl9lnmkgis7648dj82qnbjp7vw9")))

(define-public crate-munch-0.3 (crate (name "munch") (vers "0.3.0") (hash "14hirhhlka8qqagwk7axmn4sg00hjhxb9zrpzryjrgks59h6105x")))

(define-public crate-munch-0.3 (crate (name "munch") (vers "0.3.1") (hash "123k0qliadfzzmiyxxq0xd82c21id09v67bm7j004kxv73bxajfs")))

(define-public crate-munch-0.4 (crate (name "munch") (vers "0.4.0") (hash "1rmd05cq0k64b26g5315j4ka8lr551sswbkmg44l74f9i9pg944b")))

(define-public crate-munch-0.5 (crate (name "munch") (vers "0.5.0") (hash "09gy20ff2mhpadzc6ifg4zn5bn7i96bcnh77wh3n8lqkc8i1pvdx")))

(define-public crate-munch-0.5 (crate (name "munch") (vers "0.5.1") (hash "0amwi1jg9fymsxc16xbldy6wvqrli4w1wz755pvs1lq4rqs4nil5")))

(define-public crate-munch-0.6 (crate (name "munch") (vers "0.6.0") (hash "0zg9jh3dr10kvzw2lrli2hgw57sgifzkh436yrglqjf1igqhqws0")))

(define-public crate-munch-0.7 (crate (name "munch") (vers "0.7.0") (hash "1pl8rcfipsb5f90gyj3g2xjdadz7qmq0vk0641f0p1fz93sl46hv")))

(define-public crate-munch-0.8 (crate (name "munch") (vers "0.8.0") (hash "16k6h7djal9k739c9dfv1js8ragvmk8w6dr20jpkwgvy88vyrwl2")))

(define-public crate-muncher-0.5 (crate (name "muncher") (vers "0.5.0") (hash "1l3k8gz9vzfmr2f4ljrvln2hnfdzxvigcdpya8ni6b8pkndhswx7")))

(define-public crate-muncher-0.6 (crate (name "muncher") (vers "0.6.0") (hash "1bifaj56ji8cn7qw2phfmq6a1lvkpmydi205c17jfnyha3199zfw")))

(define-public crate-muncher-0.6 (crate (name "muncher") (vers "0.6.1") (hash "0wc6jxfz9bkjhb2g6cwycnrvhvz2sscxlhxq046r8ilxwciyxbph")))

(define-public crate-muncher-0.6 (crate (name "muncher") (vers "0.6.2") (hash "19r88fjd8rd2i8n9z79law59ydacl2g6nv0760sn2x0ygq38md7r")))

(define-public crate-muncher-0.7 (crate (name "muncher") (vers "0.7.0") (hash "1srr15ps4dhb0g33cshgsdbr3qckyiwgb9ki26rp5nv7mnj7fn86")))

