(define-module (crates-io mu dd) #:use-module (crates-io))

(define-public crate-mudder-0.1 (crate (name "mudder") (vers "0.1.0") (hash "1gdkh291sn643bsgcgcy40x6nl95l9ddwkkhcn4chw236csa4a7p") (yanked #t)))

(define-public crate-mudder-0.1 (crate (name "mudder") (vers "0.1.3") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1vvcknx81q02x7a7x2hs39fjq6cj967kqdhsajvjcr1cl4206jbf")))

(define-public crate-mudder-0.1 (crate (name "mudder") (vers "0.1.4") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0fplyqgwzid62ri0d3s26qvg9vfl7dgl6mq316p58mrg6nli0yir")))

(define-public crate-mudder-0.1 (crate (name "mudder") (vers "0.1.5") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0cyg43ps9jaszzwnab79wy2c7lrm0scq8jhymrqmigng0pqdmj1c")))

(define-public crate-mudders-0.0.0 (crate (name "mudders") (vers "0.0.0") (hash "1l9js7f6cjwx2kx2xhmxbh5vqhx2bvihmmfq08s4ls20lp1nzzjr")))

(define-public crate-mudders-0.0.1 (crate (name "mudders") (vers "0.0.1") (hash "15pvzjfpz3p8pi759l4ifx5z6p3ar34di32khh9my25kxml3bbrj")))

(define-public crate-mudders-0.0.2 (crate (name "mudders") (vers "0.0.2") (hash "1jbpkc8ckyiw0q9a8smqfknhc1zcw8yakcm9mif128hqqy87hqwx")))

(define-public crate-mudders-0.0.3 (crate (name "mudders") (vers "0.0.3") (hash "1qk4wcjxrc7gsn3ik797gkhpmw4ckganqyb820r6xg524zwz4lgs")))

(define-public crate-mudders-0.0.4 (crate (name "mudders") (vers "0.0.4") (hash "0mxx0x0972rd8xadjxh91q6fmqk9wv3ign2a2j1bcddw8iijcni9")))

(define-public crate-muddy-0.2 (crate (name "muddy") (vers "0.2.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "const-hex") (req "^1.11") (default-features #t) (kind 0)) (crate-dep (name "muddy_macros") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)))) (hash "1ff3w2ikwvg0zmm1nqaik17qxdfi7my8cayprzg7k1v3hz86i2r2")))

(define-public crate-muddy-0.2 (crate (name "muddy") (vers "0.2.1") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "const-hex") (req "^1.11") (default-features #t) (kind 0)) (crate-dep (name "muddy_macros") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)))) (hash "1ab28fy0wwhzzhls92nvg4azqcf3p0ga8003kagpa75rbhc28dj1")))

(define-public crate-muddy_macros-0.2 (crate (name "muddy_macros") (vers "0.2.0") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0cwkhl9fvrqndj4zijghylkk9hwawpb61nicg0cycyxsv7fsp3kq")))

(define-public crate-muddy_macros-0.2 (crate (name "muddy_macros") (vers "0.2.1") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "08hdbrl3jkzar5y4nc203kqjh6ingkz7ji4x18jhlhy30d53r03j")))

