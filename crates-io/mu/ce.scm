(define-module (crates-io mu ce) #:use-module (crates-io))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.0") (hash "17gd5rkfj9pnl28l237bbgwyzhfr471r1qb1abdd7cx4wdh0lihv")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.1") (hash "1b3daaasf35p5318babx57vx35k6j0lh6pp1hfb15hjk40yq1hsb")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.2") (hash "0qhxv4cv2aqic61prxdhq8vw53kw5a7nx06j9mxrzhfh7fg3fkps")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.3") (hash "0fr3cw85a4slgx4mjzs9q0ry0m9wkzlhv58v4cmqnwbpr0d853n8")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.4") (hash "169zl0nhyxfms0wkx98fg7kpq6d8j6yvl9mgpv9njf5yaff1vc11")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.5") (hash "0w008970mp3qpsnz95if6ihli5rfhdg3kzynm2h55r0scaaj0ysw")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.6") (hash "0m9jfz9xa7wwfyak62ch7svim7v0g4mffai78jgxb9xja990g22r")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.7") (hash "0vdx4nbw97786j97jv0m44bpzx740g9r65nw5hsfl938lz0ankbg")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.8") (hash "0gcql2jbig50yp2ylh8waq121kiylz3vybn0wvvjsl3x52hcimqz")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.9") (hash "0zklv44d2ac7w46f4qb86f3q2av6w8ny5vl80akd4b4c72mq50zn")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.10") (hash "0ylb5hs9lwkw6v9l045qb6d9sdmi310rvkgg9z6398fim3z11p6y")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.11") (hash "03ix3yv7y98clvxqfqfgbgd2dqk2v3j73a7sz6ir9f6hxy90spyy")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.12") (hash "02janm8jqnrxjh67hx23m2jmk6nj5kww6aj25317xrd57zgvgzjd")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.13") (hash "0sz0dqm912h9aspr64g55n3iwp6cxwc1bh0q127qiswbnmlk8h5r")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.14") (hash "0kmrfb1p2qwa16l3bj3swk77d28993fkzm0gp0ajmglvd2r8v1yg")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.15") (hash "15hcdl1gfsshqn6pw7bqk8apbdq53njnadk0dx4n21x67snn0qci")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.16") (hash "1b4p157v8v415yq6z1nc6d8s97j762bwiji853ikwiv528palphm")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.17") (hash "0fand07g5kpm88062ng6x6wz4dzhmw7p8xa8fnhzy9lbv01cslh4")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.18") (hash "1ym9s64b9ky4k2rq030s0jp4lcia8pfaf1bflwj7sqx21pafrjpk")))

(define-public crate-mucell-0.1 (crate (name "mucell") (vers "0.1.19") (hash "0wjrs0c7ksdvkdhq24zaihcd2j376xkbn7n9kr5wdh24w0ybbzmq")))

(define-public crate-mucell-0.2 (crate (name "mucell") (vers "0.2.0") (hash "1r4r2dma67qm1cjbkc00ngjs5hq1rhdp0s36dgs0i9cmszrx7k8q") (features (quote (("no_std"))))))

(define-public crate-mucell-0.3 (crate (name "mucell") (vers "0.3.0") (hash "15had13r2q9jwciljdv8x78i73l7b0dmycadzrrbkjz728d0b3jv") (features (quote (("no_std") ("const_fn")))) (yanked #t)))

(define-public crate-mucell-0.3 (crate (name "mucell") (vers "0.3.1") (hash "00b68z6s8k2623fb0bwm2z11jph89cn4iw10ykj6rrf3sgc702nh") (features (quote (("no_std") ("const_fn"))))))

(define-public crate-mucell-0.3 (crate (name "mucell") (vers "0.3.2") (hash "10xd94fsysj0qgky8c9b5ndk785hfvf3ib4zrhh118h2r29bkdb3") (features (quote (("no_std") ("const_fn"))))))

(define-public crate-mucell-0.3 (crate (name "mucell") (vers "0.3.3") (hash "0n50nizcgrf8n0xlypy14m7m1wlv0fnmsl5qkizp269rzkn2smlx") (features (quote (("no_std") ("const_fn"))))))

(define-public crate-mucell-0.3 (crate (name "mucell") (vers "0.3.4") (hash "15r8522jd4s50w2zbcfpzvdgiz4mc73hhv4d5fgmr68nakgfkc4f") (features (quote (("no_std") ("const_fn"))))))

(define-public crate-mucell-0.3 (crate (name "mucell") (vers "0.3.5") (hash "12z20722hv8wbx6bfishjkbbmmahdlaj8yrx4y6cziwd9ar8b4vc") (features (quote (("no_std") ("const_fn"))))))

