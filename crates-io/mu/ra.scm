(define-module (crates-io mu ra) #:use-module (crates-io))

(define-public crate-mural-0.0.0 (crate (name "mural") (vers "0.0.0") (deps (list (crate-dep (name "wgpu-types") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "1bjd89mrm0hmzs9w3fi1nxnhf81zfhj9ns36yilq3xkcd8spzyhd") (features (quote (("default"))))))

