(define-module (crates-io mu dr) #:use-module (crates-io))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.0") (hash "1pxb26glgd7b51h25aakyr5r4qyjdjjxy66xim86d2d7kaqz93fh")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.1") (hash "0pprisl401ix7zp1869wxal229w7hf25wndyq3dinab7mc4yqfax")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.2") (hash "09lri2naag4c62hal263x3iid46wn77azwslrhql2y9rq1nzgjfq")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.3") (hash "0lvs14xr8wg5w0p34d6dr189444iwb0k7d4xns9cfjshk8f7a75s")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.4") (hash "1jz2kff6i7f9c3llh104z99nd6plsgb9xsbj0w0mpn57crc0bpsd")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.5") (hash "1gy8312gaiibzc9q3mbg7n38a12dbdkm1j8argy2fr59qvg9p04p")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.6") (hash "1wpmgim94psrcwxya1aclwfs5nmhm2ba4nnnz0axfmvvyqwrrp1s")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.7") (hash "1f0fb96lgjqzjf0wzwc77flj0fnkvihqynkcaqwa35z410hs4bd1")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.8") (hash "0qjjwi23ijp8gzpfysvian63nxwcd7nhbccj7yqsm2dv1a73yh3f")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.9") (hash "0bjdws8ly6imrwk6f1194msbsj53dlhj9f0i245px7pdnwm5syir")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.10") (hash "06jm6a73029cm05a958gwymf4g0ixmn0ch72dx9rsnlkbh2lx15w")))

(define-public crate-mudra-0.1 (crate (name "mudra") (vers "0.1.11") (hash "0g5vq9vhzxlw8ajjassdfbp3y19b7vvj9zyld04awpf6v770rqwh")))

(define-public crate-mudrs-milk-0.0.0 (crate (name "mudrs-milk") (vers "0.0.0") (hash "01d0vq4kddkyx46g0rbf7w47nx06cal7albdnrhkp55whh4nbxb2")))

(define-public crate-mudrs-milk-0.0.1 (crate (name "mudrs-milk") (vers "0.0.1") (deps (list (crate-dep (name "alacritty_terminal") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)) (crate-dep (name "backtrace") (req "^0.3.63") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "generational-arena") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "newtype") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.9.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "ringbuffer") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "tellem") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.29") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "tracing-appender") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.4") (features (quote ("env-filter"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.16.0") (features (quote ("crossterm"))) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "vte") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "15w9srln241l0w08jx7ixkzkpzwfpsf8nsbwhs3k6w9h4c3f6n94")))

