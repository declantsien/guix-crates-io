(define-module (crates-io mu db) #:use-module (crates-io))

(define-public crate-mudbase-0.1 (crate (name "mudbase") (vers "0.1.0") (hash "0rwhlf3ifdsgla6x98b4ibfi4hhag7v2vwy48dxbbax1vh7givdy")))

(define-public crate-mudbase-0.1 (crate (name "mudbase") (vers "0.1.1") (hash "0vlvbhgbqav4m17cdy7pfc65lw2n35wx63sw3hifmpwg7dkhv0wm")))

(define-public crate-mudbase-0.1 (crate (name "mudbase") (vers "0.1.2") (hash "0szrp8ilkxw83y1wi2iqp06k2p6a7drpxk8rwlz768mdp4bbmsj7")))

(define-public crate-mudbase-0.1 (crate (name "mudbase") (vers "0.1.3") (deps (list (crate-dep (name "mudbase_server") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n0sdk61c939yfdxf4q3g6h771iny8xjacwpjvxci85yzxki4vrv")))

(define-public crate-mudbase-0.1 (crate (name "mudbase") (vers "0.1.4") (deps (list (crate-dep (name "mudbase_server") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g28mkwiaznfcdrmdw938cfqa15zq7zngzcr9nxrcgissmgzwwrl")))

(define-public crate-mudbase_server-0.1 (crate (name "mudbase_server") (vers "0.1.0") (hash "032xzbrsfydg54imzlnljqj7y0jjrpwhfya9aqzjjb8rlsrq1zhw")))

