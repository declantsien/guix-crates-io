(define-module (crates-io mu nk) #:use-module (crates-io))

(define-public crate-munkres-0.0.1 (crate (name "munkres") (vers "0.0.1") (deps (list (crate-dep (name "bit-vec") (req "*") (default-features #t) (kind 0)))) (hash "0fcadcys2p6lkrj8l5qjcji1rwx0g47sgw55mcq4gcbf5qn5vm7s")))

(define-public crate-munkres-0.0.2 (crate (name "munkres") (vers "0.0.2") (deps (list (crate-dep (name "bit-vec") (req "*") (default-features #t) (kind 0)))) (hash "0i3jz695rivm8i044g838jjghz50c0znfxahkwf0mh9qb2syx4rs")))

(define-public crate-munkres-0.1 (crate (name "munkres") (vers "0.1.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)))) (hash "09dh4n99pc7l0b738a5n7gzg7z88dm9g93hpvlmbfc6vy9k0fhvf")))

(define-public crate-munkres-0.2 (crate (name "munkres") (vers "0.2.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ixg4caj5kgnw6a7v3g3z4v072x8knfc9nfni81m8lw81dmxlrm0")))

(define-public crate-munkres-0.3 (crate (name "munkres") (vers "0.3.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ry38qdqpxj1lq47vs3zaa0zb3wjz5qz8l3jagi31y5d2cw05jl4")))

(define-public crate-munkres-0.4 (crate (name "munkres") (vers "0.4.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ivhlihbq7zm0q3v6gmgbgnvbghqa0vilw65aa3wrlxhgccichb7")))

(define-public crate-munkres-0.5 (crate (name "munkres") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "17xkvigdqcs04kgpbddx046i0cdqhyi0v1xfrvzxx1a5yspz4a58")))

(define-public crate-munkres-0.5 (crate (name "munkres") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)))) (hash "00sz5r0qll72sdkwicra2qikj58i2nr6hr3jhgmhw241z7silmbx")))

(define-public crate-munkres-0.5 (crate (name "munkres") (vers "0.5.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14") (default-features #t) (kind 0)))) (hash "1kir2cwsp9s1qm8wrp8h6a67raf40zl88j1nahymkhab94ajbkkl")))

