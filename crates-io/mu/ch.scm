(define-module (crates-io mu ch) #:use-module (crates-io))

(define-public crate-muchutils-0.1 (crate (name "muchutils") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "iowrap") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "tempfile-fast") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v335d3ycby63fsy7khzdlxi4p6ivjgmv5v3n3y096l3l14fznkz")))

