(define-module (crates-io mu jo) #:use-module (crates-io))

(define-public crate-mujoco-0.0.0 (crate (name "mujoco") (vers "0.0.0") (deps (list (crate-dep (name "mujoco-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ggv412k08fc8vq1s4p5hrk1s069rkrs6jl092cbi286nygnbmw0")))

(define-public crate-mujoco-rs-0.1 (crate (name "mujoco-rs") (vers "0.1.0") (hash "0zwgix5d5yw2gvb4r31mqwn07ywwfd78l7drs68444rm6f786b38")))

(define-public crate-mujoco-rs-sys-0.0.2 (crate (name "mujoco-rs-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1hffcfig7c0zya52m8nhffjjvm8yxh9mfal2np7ibmsa7zsyxg45") (features (quote (("mj-render") ("default" "mj-render")))) (links "mujoco")))

(define-public crate-mujoco-rs-sys-0.0.3 (crate (name "mujoco-rs-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1yssm0n49bqfsis0i81faln5nw8kdiqbxwpqvrl4mw0p01b6955j") (features (quote (("mj-render") ("default" "mj-render")))) (links "mujoco")))

(define-public crate-mujoco-rs-sys-0.0.4 (crate (name "mujoco-rs-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0dbgkjcm340yb1i26x63p4dfj81sjli63cm98y3j4qang0ihl3g2") (features (quote (("mj-render") ("default" "mj-render")))) (links "mujoco")))

(define-public crate-mujoco-rust-0.0.2 (crate (name "mujoco-rust") (vers "0.0.2") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mujoco-rs-sys") (req "^0.0.2") (kind 0)))) (hash "1sh60wni80z3dgj4199c41v08vjb407c6ip3g3rxa35ak73p0qvn") (features (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

(define-public crate-mujoco-rust-0.0.3 (crate (name "mujoco-rust") (vers "0.0.3") (deps (list (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mujoco-rs-sys") (req "^0.0.2") (kind 0)))) (hash "138snpcyw89c4xlk86wc5h76r4pjklf6x1qw48fwmfkrs5zgdmis") (features (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

(define-public crate-mujoco-rust-0.0.4 (crate (name "mujoco-rust") (vers "0.0.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mujoco-rs-sys") (req "^0.0.4") (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (default-features #t) (kind 0)))) (hash "0x4yd4h4dlhycv8f3bvg5z2wp8qc3g063imarkq6rz7z12ps939r") (features (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

(define-public crate-mujoco-rust-0.0.6 (crate (name "mujoco-rust") (vers "0.0.6") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "~5.0.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mujoco-rs-sys") (req "^0.0.4") (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.0") (default-features #t) (kind 0)))) (hash "0scqgsb08gdgqr7q77x27c3an8bl8qhv4v3xxc8v28103nyzxqvj") (features (quote (("mj-render" "mujoco-rs-sys/mj-render") ("default" "mj-render"))))))

(define-public crate-mujoco-sys-0.1 (crate (name "mujoco-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)))) (hash "0g8qc48nbma4lcjiyvky7ann0biq5kdh9wr7hbgdyhkgqj334dq3") (yanked #t) (links "mujoco200")))

(define-public crate-mujoco-sys-0.1 (crate (name "mujoco-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)))) (hash "193kzzd2ln75wb8ry4xn089bx7d08558938d0ir31vvxp4zkz0bc") (yanked #t) (links "mujoco200")))

(define-public crate-mujoco-sys-0.1 (crate (name "mujoco-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0zaa39h7z1magiqx9f0815q61vf3zl79gdy8z4hmmdx1f0h0mrxc") (features (quote (("mj-render") ("default" "mj-render")))) (yanked #t) (links "mujoco200")))

(define-public crate-mujoco-sys-0.1 (crate (name "mujoco-sys") (vers "0.1.3") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0l7r556131nplq2amm81h1xls657wa20jzv3y5gqqdyn7hin3937") (features (quote (("mj-render") ("default" "mj-render")))) (yanked #t) (links "mujoco200")))

(define-public crate-mujoco-sys-0.1 (crate (name "mujoco-sys") (vers "0.1.4") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "088kiy8xkakfmvix6vl8c07qj3a9xq6sr557dc715g1p4qr4qpxs") (features (quote (("mj-render") ("default" "mj-render")))) (yanked #t) (links "mujoco200")))

(define-public crate-mujoco-sys-0.0.0 (crate (name "mujoco-sys") (vers "0.0.0") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "15cjbrdr8g6yks9ib26y9dbcq1vxa7xyk59xy22lmbdl0n42kv5g") (features (quote (("mj-render") ("default" "mj-render")))) (links "mujoco200")))

(define-public crate-mujoco-sys-0.0.1 (crate (name "mujoco-sys") (vers "0.0.1") (deps (list (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1lgmr86l1vr6vzysr1mv3lvvqq1rmkrj70qwazpqs0cb3yhjp2bg") (features (quote (("mj-render") ("default" "mj-render")))) (links "mujoco200")))

