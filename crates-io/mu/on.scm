(define-module (crates-io mu on) #:use-module (crates-io))

(define-public crate-muon-0.1 (crate (name "muon") (vers "0.1.0") (hash "1kswxh07hd954qjf41a582xh9zrhydb17l3j0qqgp5qrwa6b96gz")))

(define-public crate-muon-rs-0.1 (crate (name "muon-rs") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "0s672ysvkyvj8xw9gyxqm3w8q93as9qvi4f96gpv4siab8r610wf")))

(define-public crate-muon-rs-0.1 (crate (name "muon-rs") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1j9486bbdqfaxbxkb5v1bvp0krk0gxhvygp7yd0slwaazrbjv7ji")))

(define-public crate-muon-rs-0.2 (crate (name "muon-rs") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "173974sbvj162cvlaf5zqj5p7h4337cms839z37jk62aa5vw5x10")))

(define-public crate-muon-rs-0.2 (crate (name "muon-rs") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "0kpgiaaz4ibah12ahgd7mh986jpy74r7nlvlqq5q7wx3y8jag419")))

(define-public crate-muon-rs-0.2 (crate (name "muon-rs") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "15a05wmc15m0jwy8qvwwp52pcb7yk9w7qk87gsjmlsa779i16ahp")))

(define-public crate-muon-rs-0.2 (crate (name "muon-rs") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0yxy17df87np8waghb9wr8rdgg5ky9sj7bn9giqvi3qzrpvy00lz")))

(define-public crate-muonts-0.1 (crate (name "muonts") (vers "0.1.0") (deps (list (crate-dep (name "burn") (req "^0.11.1") (features (quote ("ndarray" "train" "autodiff"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "036xq0vpbj3r200h7f95pd868j4bklrjzl6b07sj44847r0x6qw7")))

