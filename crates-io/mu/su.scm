(define-module (crates-io mu su) #:use-module (crates-io))

(define-public crate-musubi-0.1 (crate (name "musubi") (vers "0.1.0") (hash "0lc6zwp6fkz9v1l910p2x651mh98gz9qsmjhd1cy8rrwg5ymrihq")))

(define-public crate-musubi-0.1 (crate (name "musubi") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1x72j6n70zkhb0yj77in7i3nk85k1hc29rx7lmq1d0sikpy4pfal")))

