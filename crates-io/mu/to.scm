(define-module (crates-io mu to) #:use-module (crates-io))

(define-public crate-muto-0.1 (crate (name "muto") (vers "0.1.0") (hash "1x0jqbymfvlzxfnidsfwdf6zyraq8klq8r3bjg0yr7xdlm0gk1bm")))

(define-public crate-mutos-0.1 (crate (name "mutos") (vers "0.1.0") (hash "1b97ingb57s070043ki9jq9f8zhs5jw9nz8wkxc86ynr4p8fn2hp")))

(define-public crate-mutos-mush-0.0.0 (crate (name "mutos-mush") (vers "0.0.0") (hash "17nfhym7xyz2aj2a43zrgbwmg48yxvmiwbmfiy4k4knjxy1z5lbi")))

(define-public crate-mutos-utils-0.0.0 (crate (name "mutos-utils") (vers "0.0.0") (hash "0q34rlk5zjx9cx7s17bk423jlv9931xk0gvg1rgr6l6fhv1hq2sc")))

