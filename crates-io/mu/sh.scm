(define-module (crates-io mu sh) #:use-module (crates-io))

(define-public crate-mush-0.1 (crate (name "mush") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libssh2-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 2)))) (hash "0lnni9ajnvimfq6smmz1ji4xnfrd9ymsbl4vhz5gxbq1hh4q660j") (yanked #t)))

(define-public crate-mush-0.1 (crate (name "mush") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libssh2-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 2)))) (hash "0jy79b6gih38d0ji5ffi4f5bqmdccnjdar3qp3dlwi8kys2y57g1") (yanked #t)))

(define-public crate-mush-0.1 (crate (name "mush") (vers "0.1.2") (hash "0q5d92xlqx6fxygsvzq7bbiw4f8yxbiygh7q982yqa0bzq9kh1zp")))

(define-public crate-mushid-0.0.1 (crate (name "mushid") (vers "0.0.1") (deps (list (crate-dep (name "crc32c") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "mac_address") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "02bhzgwv9zjwxwsk1s0gc8a5xmfxym9jj9ijihk80p875wx8n6ys")))

(define-public crate-mushid-0.0.2 (crate (name "mushid") (vers "0.0.2") (deps (list (crate-dep (name "crc32c") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "mac_address") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "1lc449x6fgj0w9wpar0wyrfipw4l74akq0wbbwhldfzn1mh0n2w9")))

(define-public crate-mushin-0.1 (crate (name "mushin") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ar0j2jdmhnk1z48xx5h6rzxn1c9gh3sgvyggkaykq8apgnql7nj")))

(define-public crate-mushin-0.1 (crate (name "mushin") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)))) (hash "12y0asw6bkqm56kp38f03mzras583215sj7j4l3r838ig52bjyid")))

(define-public crate-mushin-0.2 (crate (name "mushin") (vers "0.2.0") (deps (list (crate-dep (name "arrayfire") (req "^3.8") (default-features #t) (kind 0)))) (hash "0pd9gfw8n37szlzdbal3r85fvzcpgwqghzxxwp8fn45vg6cxxjkc")))

(define-public crate-mushin-0.3 (crate (name "mushin") (vers "0.3.0") (deps (list (crate-dep (name "arrayfire") (req "^3.8") (default-features #t) (kind 0)))) (hash "1aqrdrmr29yi5xrdvxixailnkdhfijl0ddj78xd95labl4k9l8ck")))

(define-public crate-mushin-0.4 (crate (name "mushin") (vers "0.4.0") (deps (list (crate-dep (name "arrayfire") (req "^3.8") (default-features #t) (kind 0)))) (hash "04gac1ycg9i57zjshpvcqcndanzlvkry3x208jsc8rwxcvc2z9f5")))

(define-public crate-mushin-0.5 (crate (name "mushin") (vers "0.5.0") (deps (list (crate-dep (name "arrayfire") (req "^3.8") (default-features #t) (kind 0)))) (hash "0jsrn6cc4yy6mch55d1x5nnsahqdhcly0xl2i2a6rfxd5h5cxai0") (features (quote (("nn") ("default" "nn"))))))

(define-public crate-mushin_derive-0.1 (crate (name "mushin_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19qzkbnkl00zjbssb4kjjwaxp038cjx0f4p59qsmnxz2c5w9cb30")))

(define-public crate-mushin_derive-0.1 (crate (name "mushin_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1c3bnyrann9xm4fjzn7rpkgys3dvw3yrlplh9q380wsb81qqzxpj")))

(define-public crate-mushroom-0.0.0 (crate (name "mushroom") (vers "0.0.0") (hash "18cqrfhh53gidq27g5q4rknbqw0n9ix41cb5hgkxb85x32s2xrnl")))

