(define-module (crates-io mu rm) #:use-module (crates-io))

(define-public crate-murmel-0.1 (crate (name "murmel") (vers "0.1.0") (hash "1vili9rrpfbbj24f04vj2p14ksq9yfxg6ds1x3x3ax8b74wkzpmn")))

(define-public crate-murmel-0.2 (crate (name "murmel") (vers "0.2.0") (deps (list (crate-dep (name "bitcoin") (req "^0.21") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "bitcoin_hashes") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "= 0.3.0-alpha.18") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hammersbald") (req "^2.4") (features (quote ("bitcoin_support"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lightning") (req "^0.0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lru-cache") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.2") (default-features #t) (kind 2)))) (hash "1gw101p93bpkrarv49kq7x5j1d21dsisp40691s656qr9sq5v1s4")))

(define-public crate-murmel-0.2 (crate (name "murmel") (vers "0.2.1") (deps (list (crate-dep (name "bitcoin") (req "^0.21") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "bitcoin_hashes") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "= 0.3.0-alpha.18") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hammersbald") (req "^2.4") (features (quote ("bitcoin_support"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lightning") (req "^0.0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lru-cache") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.2") (default-features #t) (kind 2)))) (hash "1h2iydfwr9pqm2mgvd3l8j6rny42q7sz3ni8lyjcxsx7b5ks4asm")))

(define-public crate-murmur-0.1 (crate (name "murmur") (vers "0.1.0") (hash "18p0yk5v72x0cz1bc8a421lwznnx2js4qafqsfs1p0hxx4bf9kg8") (yanked #t)))

(define-public crate-murmur-0.3 (crate (name "murmur") (vers "0.3.0") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "1c40zq6ir91wmi9q8h3bhihwwx3wshr5b8il5mzp67kmg6lw50dy") (features (quote (("default")))) (yanked #t)))

(define-public crate-murmur-0.3 (crate (name "murmur") (vers "0.3.1") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "168qfxf4g3lcgx9sk22ry0hy6kj12lagwn2cddbyizsq9445slzy") (features (quote (("default")))) (yanked #t)))

(define-public crate-murmur-0.3 (crate (name "murmur") (vers "0.3.2") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0ipc1g1nkns7rm4bn95g6igdv4fhgm9ngg933pyms174iphi3vxp") (features (quote (("default")))) (yanked #t)))

(define-public crate-murmur-0.3 (crate (name "murmur") (vers "0.3.3") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0cd8ycq9pkvbw3aj9v6kxylxlad23iy4vxbww10hdsp8fr0x2sl7") (features (quote (("default")))) (yanked #t)))

(define-public crate-murmur-0.3 (crate (name "murmur") (vers "0.3.4") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "1in1d9k203i0np6015fil5wkznrv4k5jlyjrip5yvia25vq0x65x") (features (quote (("default")))) (yanked #t)))

(define-public crate-murmur-0.3 (crate (name "murmur") (vers "0.3.6") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "12f29k38d38bhxly7v3wfvqd43h1nsj0v8nsi5ihfhq7s5k8gswr") (features (quote (("default")))) (yanked #t)))

(define-public crate-murmur-1 (crate (name "murmur") (vers "1.0.0") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^4.0") (default-features #t) (kind 0)))) (hash "1q6xh32l3wb12bgca6x4ihy7k2izmslc5kynkagr7bbgx5sghvlg") (features (quote (("default"))))))

(define-public crate-murmur-1 (crate (name "murmur") (vers "1.0.1") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^4.0") (default-features #t) (kind 0)))) (hash "11qy1dzyngs9ap8l9z1ghqq7ylhp8jivpf7z3khnqy8ffw2d3vc3") (features (quote (("default"))))))

(define-public crate-murmur-1 (crate (name "murmur") (vers "1.1.0") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^4.0") (default-features #t) (kind 0)))) (hash "1c9qacmkjk9v7avzkp6ywm162gl52bzijx2ka0b2ndhds5wfvshj") (features (quote (("default"))))))

(define-public crate-murmur-1 (crate (name "murmur") (vers "1.2.0") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^4.0") (default-features #t) (kind 0)))) (hash "1aacizb9z8k3468q88z7ncbidg7nx8cq0fh0p8xqs6wvq55hixyz") (features (quote (("default"))))))

(define-public crate-murmur-1 (crate (name "murmur") (vers "1.2.1") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^4.0") (default-features #t) (kind 0)))) (hash "0c583fkbvh0fjg7grq0h4mp97ivvid1p0pd46qx7isy1mrahw654") (features (quote (("default"))))))

(define-public crate-murmur-2 (crate (name "murmur") (vers "2.0.0") (deps (list (crate-dep (name "color-backtrace") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "enum-iterator") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^4.0") (default-features #t) (kind 0)))) (hash "1qmmcwi8r3zykdz2yzavxgc0y3lfr3mx0xy90cw2fv5s00kxb0zv") (features (quote (("experimental") ("default"))))))

(define-public crate-murmur2-0.1 (crate (name "murmur2") (vers "0.1.0") (hash "1a1wixpdsm3rw90yivbwnfj4y88rqdvqp5rm5vdig8294pg5ln7v")))

(define-public crate-murmur3-0.0.2 (crate (name "murmur3") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0x4yppch73r9zdnznfanabmdim15gsbzc1y59hpngbwcqd43ypli")))

(define-public crate-murmur3-0.1 (crate (name "murmur3") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1caaq6pa6786fmk5b7y65g7s3mna8ld2m54bvdpqqaz0gcr46xd4")))

(define-public crate-murmur3-0.2 (crate (name "murmur3") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1b4jy5ln0l9cxj9fk3f2588if5k2n9gc4cvp55x1nqd3b8xpw0vv")))

(define-public crate-murmur3-0.3 (crate (name "murmur3") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "08iycs8gzvym8ffjk7s5sdnzdhm36nqbd0f5j6cfdz2g2s2zxrzc")))

(define-public crate-murmur3-0.4 (crate (name "murmur3") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r29i708216zjrcy4bgjsbmikwpii0bxc32y7hqrqp9skdsgryx9")))

(define-public crate-murmur3-0.4 (crate (name "murmur3") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)))) (hash "073p2hncdnngin5mjbxnc8v35x4azqclmz4d712ga0zwkrcgk651")))

(define-public crate-murmur3-0.5 (crate (name "murmur3") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (features (quote ("i128"))) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "00y0aqka56hwaa4i2cmgs8kmywkkswvpkr3wqvm223qbgdzxrhbg")))

(define-public crate-murmur3-0.5 (crate (name "murmur3") (vers "0.5.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1zrcnvf32nzag6r5ap7i3xqacq9pzmdb1iih4ri8xlw5wj457b9y")))

(define-public crate-murmur3-0.5 (crate (name "murmur3") (vers "0.5.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0jvi9hsppwln53xvcvad79zm0jx2qb531q7qnqlhkfijy4f12llj")))

(define-public crate-murmurhash-0.1 (crate (name "murmurhash") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1pav57p730i2nspg26c6l6j5njqlg76fnswyy8r1b68rcrmi58bi") (yanked #t)))

(define-public crate-murmurhash-0.1 (crate (name "murmurhash") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "07mhy94dwfm53ycwl3xfnkagdgph5pwm75x9gwz4w7jbxxzzsj0m") (yanked #t)))

(define-public crate-murmurhash-0.1 (crate (name "murmurhash") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0g59k4s59s1r72v72sabxd9xvqz8yvym56dbswsgl0sq1sfjh8cj") (yanked #t)))

(define-public crate-murmurhash-0.1 (crate (name "murmurhash") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0z9an11f7y1fiji1y93dzzyh9alkj2a2w1py3kxh2l37lyw28c6h") (yanked #t)))

(define-public crate-murmurhash-0.1 (crate (name "murmurhash") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "04achm0wl1idmmlglmy4nbi72d32fmg0s2vc9al4ckywn287k58f") (yanked #t)))

(define-public crate-murmurhash-0.1 (crate (name "murmurhash") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1lbplvsb9d6zi112nfaw090h12k6xabz716nm96jlg96x9z3frn5") (yanked #t)))

(define-public crate-murmurhash-0.1 (crate (name "murmurhash") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1b0mmjcvd6j6sa2abylv1rc1m0ahjzy7lb173pw27ri8nk8drwgz")))

(define-public crate-murmurhash3-0.0.1 (crate (name "murmurhash3") (vers "0.0.1") (hash "196c5nww4h7szscya2skpy9qahzp0lmda6ma7bqihpak3p08xnyj")))

(define-public crate-murmurhash3-0.0.2 (crate (name "murmurhash3") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "06a6bgyqs2q3z82garqk41rlwbc8448nwb4crzr5bg661s52f18m")))

(define-public crate-murmurhash3-0.0.3 (crate (name "murmurhash3") (vers "0.0.3") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1a4mlr6k1p3fhqm3f352jfafa1zp47wvbgqxk8c5blzv3ywgcmf9")))

(define-public crate-murmurhash3-0.0.4 (crate (name "murmurhash3") (vers "0.0.4") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "02vy9363knyzpff1z530mkkkl1wsaxqmsk28hlyj4iiamww8l8rh")))

(define-public crate-murmurhash3-0.0.5 (crate (name "murmurhash3") (vers "0.0.5") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0r1n79wr1z5pl3rlvas9mwrgxyny4wnvyrrp1120aj7lr9r37652") (features (quote (("nightly"))))))

(define-public crate-murmurhash32-0.1 (crate (name "murmurhash32") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1nx30qc99jag05334bssnzck8jfhyizn5pfdjhzl9zqw7sfxpzhv")))

(define-public crate-murmurhash32-0.2 (crate (name "murmurhash32") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0jjxn87g5n36ys1c4fibxq5c1i0njqidn8zvi6bgx18f5y4gydnp")))

(define-public crate-murmurhash32-0.3 (crate (name "murmurhash32") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1ps4ry1nrsvihqzvawcp48arlbicpxmrj50xqp2rl8adq2s0sf6r")))

(define-public crate-murmurhash32-0.3 (crate (name "murmurhash32") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "16yqpn81xx4g7wr0wihcc0wzxlzfcdv2mmi97d48394nm5mbz591")))

(define-public crate-murmurhash64-0.1 (crate (name "murmurhash64") (vers "0.1.0") (hash "0fisms0slin2il67510dwlya9x8yr287f3jjv35r4imllj42iqzh")))

(define-public crate-murmurhash64-0.1 (crate (name "murmurhash64") (vers "0.1.1") (hash "04p2f62zy6n759yssm16vpjb7hfqhmhwyvfl5vfy2k3ji43zn3sp")))

(define-public crate-murmurhash64-0.2 (crate (name "murmurhash64") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "189hlcxb9pmgcpknh7vla3vhp9ddn942s77h4i087xckj8sar23l")))

(define-public crate-murmurhash64-0.3 (crate (name "murmurhash64") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lc7n8612m89caf8xh2dxsgaxbi74xjf0kpf63d0gjfyx7vdh5cx") (features (quote (("hasher"))))))

(define-public crate-murmurhash64-0.3 (crate (name "murmurhash64") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0174pwq37czfk296rc7ibvrxgdpn0wh3nxfpada651c9x94gwags") (features (quote (("hasher"))))))

