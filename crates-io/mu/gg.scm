(define-module (crates-io mu gg) #:use-module (crates-io))

(define-public crate-muggle-0.1 (crate (name "muggle") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.34") (features (quote ("std"))) (optional #t) (kind 0)))) (hash "0n7fpabl4sgln45837402mmnhywysp6dylbh11z1vaygsl3f5287") (features (quote (("trace-errors" "tracing") ("trace-calls" "tracing"))))))

(define-public crate-muggle-0.1 (crate (name "muggle") (vers "0.1.1") (deps (list (crate-dep (name "angel") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "muggle_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "01cynjbvgiilxn17k89z43djg7zv7si3sxwaana0rcbbxawqdb3w") (features (quote (("trace-errors" "angel/trace-errors") ("trace-calls" "angel/trace-calls"))))))

(define-public crate-muggle_macros-0.1 (crate (name "muggle_macros") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "080l9kn65a9xf3a6m7zw30za6svpipacz2l18zkjdfmnxk5bpra4")))

