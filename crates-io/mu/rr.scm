(define-module (crates-io mu rr) #:use-module (crates-io))

(define-public crate-murray-0.1 (crate (name "murray") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jz66vcfyf47rj8sd9gkr8pgfi4k6fx3xad7mjln8x5v9hsgdcms")))

(define-public crate-murray-0.1 (crate (name "murray") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0arldvzcbiljs5c8qddiynnnbvq6lyimkj9178bq2q3zb8ksy20j")))

(define-public crate-murray-0.2 (crate (name "murray") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dkampxdi6gbkl30l7gys0dajvygnpyhbin913b3x5kiki7bxl5f")))

(define-public crate-murray-0.3 (crate (name "murray") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0asykli6fj0jv1hsh30qg759jjknv5bngaxc8a83m6bg0h0m06d6")))

(define-public crate-murray-0.4 (crate (name "murray") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "03xxya7zhiar5s87p35mshwvzwqbryc4k33xfafvdswchs3kgyjf")))

(define-public crate-murray-0.4 (crate (name "murray") (vers "0.4.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wx7hv5nqili1n8i6784pqy56481ij4cydrflw5vcydf50mh5pim")))

(define-public crate-murray-0.4 (crate (name "murray") (vers "0.4.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jbbsnnnam7j1i7xjdd27w25d6avrs9rnbp6595fd7cmd0kdqjsb")))

(define-public crate-murray-rs-0.1 (crate (name "murray-rs") (vers "0.1.0") (deps (list (crate-dep (name "httpmock") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.26.2") (features (quote ("derive" "strum_macros"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gr86mqac717knn0rxvivgiap20mvl5b6vc2zhvh1gwxs6g8xr26")))

