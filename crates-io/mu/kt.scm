(define-module (crates-io mu kt) #:use-module (crates-io))

(define-public crate-mukti-metadata-0.1 (crate (name "mukti-metadata") (vers "0.1.0") (deps (list (crate-dep (name "semver") (req "^1.0.10") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0i3nd1jq351ijr1fw3vaa4ccznxc95902jx137gq6fc4nv6kizqf") (rust-version "1.59")))

(define-public crate-mukti-metadata-0.2 (crate (name "mukti-metadata") (vers "0.2.1") (deps (list (crate-dep (name "semver") (req "^1.0.20") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0vdw8h24x1b47a1mvc5hlv59l6xy2hclfsa5wg7dnqnkl0x1sr1p") (rust-version "1.59")))

