(define-module (crates-io mu lb) #:use-module (crates-io))

(define-public crate-mulberry-0.0.0 (crate (name "mulberry") (vers "0.0.0") (deps (list (crate-dep (name "nalgebra") (req "^0.21.1") (optional #t) (default-features #t) (kind 0)))) (hash "1cr3n8bln9hmqvvd9fld304vmvkh8i5gcqbvpgsqz6dn4gl3pl23")))

(define-public crate-mulberry-0.0.1 (crate (name "mulberry") (vers "0.0.1") (deps (list (crate-dep (name "nalgebra") (req "^0.21.1") (optional #t) (default-features #t) (kind 0)))) (hash "0phgz4a5lm72yncg4ljv1cgd8swkdwf1wf94w30d08brf90hwp47")))

(define-public crate-mulberry-0.1 (crate (name "mulberry") (vers "0.1.0") (deps (list (crate-dep (name "alga") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21.1") (features (quote ("alga"))) (optional #t) (default-features #t) (kind 0)))) (hash "123fqc2nx6mm76jxizc0khqrsn1nbc5wnxmkachj9rxbszmx9l25")))

