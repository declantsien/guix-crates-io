(define-module (crates-io mu tu) #:use-module (crates-io))

(define-public crate-mutual-message-exchange-0.0.1 (crate (name "mutual-message-exchange") (vers "0.0.1") (hash "13kmpbjc74b9azrrxvhn4ijsbdc9scblp6c3q2g9piyqw2siaf3z")))

(define-public crate-mutual_strip-0.1 (crate (name "mutual_strip") (vers "0.1.0") (hash "0d7i31vjc51qkh1l3788yg7hgc45l7nvzrj965gq2ni0d5f36nas")))

(define-public crate-mutual_strip-0.1 (crate (name "mutual_strip") (vers "0.1.1") (hash "11cpnj5n24prrn5fzp78kif12v4rnrxk8dbwwbckp06yxhmjisna")))

(define-public crate-mutually_exclusive_features-0.0.1 (crate (name "mutually_exclusive_features") (vers "0.0.1") (hash "0ba25qskhidj9iypcndvdyci1jg6qivz2aj7yxr67p43d5yp46pc") (yanked #t) (rust-version "1.56.0")))

(define-public crate-mutually_exclusive_features-0.0.2 (crate (name "mutually_exclusive_features") (vers "0.0.2") (hash "1aszrpmcqf7znfgmgy2lkvc0j6nh79zmxvwzxviyxfripwm1q3cl") (yanked #t) (rust-version "1.56.0")))

(define-public crate-mutually_exclusive_features-0.0.3 (crate (name "mutually_exclusive_features") (vers "0.0.3") (hash "03mavdaw06y4pwha3q80vz5mhy3cv09mxn31zjvknxqh0sqc00kd") (rust-version "1.56.0")))

(define-public crate-mutually_exclusive_features-0.1 (crate (name "mutually_exclusive_features") (vers "0.1.0") (hash "0xw55kqw8y8y1q5lys9132vm3zlmw8nxz5bkzxrgj56k8mj1wkp9") (rust-version "1.56.0")))

(define-public crate-mutuple-1 (crate (name "mutuple") (vers "1.0.0") (deps (list (crate-dep (name "pyo3") (req "^0.21.1") (features (quote ("extension-module" "gil-refs"))) (default-features #t) (kind 0)))) (hash "1wl4qw2dcflyj0mwnk1xvnabrcxyi5zqmrhqkbzhlnbg31xm9qiq")))

