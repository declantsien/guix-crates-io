(define-module (crates-io mu t_) #:use-module (crates-io))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.0") (hash "1l9l18crjsly6qw392l6vdj0pjy87x833flckir7gk96sjgv9q0r") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.1") (hash "0k3kd3dg0f4xp04zqqvyspwas13ikya2403c7kyahjqhr8dl96i5") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.2") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "19zlsn5j952505hdwflwmk7idap3psr83h31775g8vxrsqrkbqx3") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.3") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "1hpq1mjh91hz04vcgg41f6jwaa8plr7d64hlzqzkdllj5c5l0xgz") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.4") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wd6187myssvzi5hd3hxrxcmqwac4aa0g7lpi012902xxb4yqapd") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.5") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bbv5j1s6p0svjk82wil1x1w1940y1gi9j95555v8yzy790j9w5h") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.6") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bywn02hixlz44yq6x31mqppq8z8vqvjqwlnjq1vvsz9pxs97dnh") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.7") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "1711v69m5lgms123v9hsyp98mzmjkh78c37sb6izd44yl9fvnr0x") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.8") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "092z3acr982j3zszfdjc5mp72cgsamhhj7d7ic68fh6gqqk07jq8") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.9") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "19jmrgqqxwrjjhcmzz83pxk8pd3mxlkbxxbjdbp3fqx5z2n1yhxn") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.10") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "15wr2qcxs92xgpkqr11d63lnahzbizl455crfnzh6ydwpzllx78q") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.11") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "1843nfwpcfqjbg7689b6c418apfn9lrwd0vxldrws6n34awc95s2") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.12") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dp3qfcp285ypvblbpgizjw0i50nwfbhrpv6qhr9ii5mmbfjlarv") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.13") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nv03l0a358i6af4swr4qd0n4gxh6ip926p5wdlb59kb328pzajb") (rust-version "1.65.0")))

(define-public crate-mut_family-0.1 (crate (name "mut_family") (vers "0.1.14") (deps (list (crate-dep (name "sealed") (req "^0.4") (default-features #t) (kind 0)))) (hash "01ajkfzf6gzpjckggmwb91ljha37j74srpf17hp4a2lvig49idp2") (rust-version "1.65.0")))

(define-public crate-mut_guard-0.1 (crate (name "mut_guard") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0yr89mz4l2m24v27bdijjnynaxfycb5yh2bdabg398hw7mr56q26")))

(define-public crate-mut_immut-0.1 (crate (name "mut_immut") (vers "0.1.0") (hash "1rcy92i2xb4y5nlqdpv1ppf62h76mbgb14d69nvv6g2f9w3cfh4n") (yanked #t)))

(define-public crate-mut_immut-0.2 (crate (name "mut_immut") (vers "0.2.0") (hash "1nsymmpnp9wylp9nv5znypnw69k3ypl2vlfb77rz4678lr4p62j1") (yanked #t)))

(define-public crate-mut_immut-0.2 (crate (name "mut_immut") (vers "0.2.1") (hash "1cii3mclwjllwkyjh4nhcp5dydbkprsha9rnfdwprzl5x5pprlv0") (yanked #t)))

(define-public crate-mut_set-0.1 (crate (name "mut_set") (vers "0.1.0") (hash "0cfn350006k2nki8scaq0b2vrcg1fd6w2wdm446k93j9r17khfyb")))

(define-public crate-mut_set-0.1 (crate (name "mut_set") (vers "0.1.1") (hash "1dgclzm2mhgvv1vpxr3rb8ag734vn4ha00p47nl3yarg3as15l59")))

(define-public crate-mut_set-0.1 (crate (name "mut_set") (vers "0.1.2") (hash "0w4l859gacznghkdjyks2r9yvypdjf8fzfhv8siw8bv1nnar4zzb")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.0") (hash "1f3isdv6zjxw7afgja193948a64r0xwwd0v2qr7jj0j8xqkdyq7f")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.1") (hash "1kfgzyq3q1v65sqlkn2iaznik6dv6il9jcrb6a7xsfal6a7jf20d")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.2") (hash "0ygf3zrb15fi8rrv2b1y4n0m5vq3yiaalg0w4l663miy8xdz9y4b")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.3") (hash "0vm99hxz5zalywjkcvp53jbci1brcq0w3skkgswxc5372pxlprz2")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.4") (hash "1cm7xf8f8s3jcscdbbz5yv6hqr3b92dfbwlk19q2k1gk6mxsfkn8")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.5") (hash "1brvz08aj50qans7nlcp240kvdm1angy6qzr75dbl50aaw25p6f6")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.6") (hash "0izvk7gi8f91mgqbwgwb94101nalk8rm6k1wxj7sbb7gnnis8f78")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.7") (hash "08xd3mdknivdrcvlpmpi7qxjzyj5d6vvjz7k0y4cfbxcfmfd348v")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.8") (hash "0wabcdj9i418l2bx2ya3s07l4dxkiqv6c49adgirzqb3qa09cfqz")))

(define-public crate-mut_set-0.2 (crate (name "mut_set") (vers "0.2.9") (hash "1bkpb91pb8c79axkn7fnqgl4mngc2x2cp2swn1ghhxp0rs258b5d")))

(define-public crate-mut_set-0.3 (crate (name "mut_set") (vers "0.3.0") (hash "0paq32i6i1m21kwx36ljwvgd041j99in9gpzfd2j4wfvpdfj4cki")))

(define-public crate-mut_set-0.3 (crate (name "mut_set") (vers "0.3.1") (hash "1g69r2f65gn7y2q0mipx8kinf5k0ragcy5h8aqc5r4mi386lcan8")))

(define-public crate-mut_set-0.3 (crate (name "mut_set") (vers "0.3.2") (hash "0kml91b7j8q59hhcw4k5qcnvlb6kw3ydas1kyilkfcgld524sghg")))

(define-public crate-mut_set-0.3 (crate (name "mut_set") (vers "0.3.3") (hash "0jj9al6gc4yx1r5pzs1mgkanjmbh6id89yk8aylpsqin5jfwn2l9")))

(define-public crate-mut_set-0.3 (crate (name "mut_set") (vers "0.3.4") (hash "1kipmx2m8bx0pmx1mh3zmvclqv7ky11ysd57iggamki1qfllwqlv")))

(define-public crate-mut_set-0.3 (crate (name "mut_set") (vers "0.3.5") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gjivcs3ywiwrxls5wq5bps7297nvlggib0p6zmlh23z4lfhv4bc")))

(define-public crate-mut_set_derive-0.1 (crate (name "mut_set_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "046kdjyyw5bkkg84ka4amq5h6c1bzfhjinrv6ppscmi476b9znwb")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "0cigsbj2dm2k3f2vnfx3psgqr1bpy6b2jmll655zx50g9jaa83jl")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "1w51vhsfvx3dwd7ski5c5nmpjhxzjvrjxgi6qik812h6iwp8mzhn")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "1jfw5z6yav26hyii4lzms62wcdy67k3552cmb7p90fdw35n4bwzz")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "0d9dr7bkwb9157fdaznb8w0mra63hilhas6zkxs2gczhnfq9p8zz")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "0hzakz054iq85xfi42f8sc3w24xl9nh89l0w5swkb0ci47yaf0av")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "1pch2zvxxnzp3rl51cznfy8lv1b7jlm31iidb5rksaskhg7y8r30")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "1yr7bqsg58v6vkz906plh54z72245x18l0ackb8krfdms5ba9f4r")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "13il3idh73nlc0n3y9w6dvr12cdb3j50gknsaqav96w5z70p2ayz")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "06p554lws026bgvjzdhb5i4vwjxhjs3273vmxdiyvkqivn6980y0")))

(define-public crate-mut_set_derive-0.2 (crate (name "mut_set_derive") (vers "0.2.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "0a65vg1996z3qj48cbx0z3pq9vzcam84pd38icvfbjrngdjpqads")))

(define-public crate-mut_set_derive-0.3 (crate (name "mut_set_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "0pwwj8q5k20n5jdqxzpiy9kjiis2d5y766p7d6sbkkpnx32b49f4")))

(define-public crate-mut_set_derive-0.3 (crate (name "mut_set_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "0gz1m7s8bc9bxkbqxgi8xq8sfjy4fppqfa8r8jc3612gcassqqhx")))

(define-public crate-mut_set_derive-0.3 (crate (name "mut_set_derive") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "00d4qsp0q3r6902dsq10cdp5sbwchb85ihnq3wi9g85gvn8q4vsd")))

(define-public crate-mut_set_derive-0.3 (crate (name "mut_set_derive") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "0vf6xnbys15zikq914x66w36amcfn64gsfmdr7pgik1x0pz03185")))

(define-public crate-mut_set_derive-0.3 (crate (name "mut_set_derive") (vers "0.3.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "17c1c782z9yl1aqcf1rqfkb2nwwm4mnsd4c584crfyqpc0ic5zwm")))

(define-public crate-mut_set_derive-0.3 (crate (name "mut_set_derive") (vers "0.3.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "1ysv0kpi244hhz7xgfgdlzby42qqvqch5k2rdhy16x3rm0rq023y")))

(define-public crate-mut_static-0.1 (crate (name "mut_static") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.7") (default-features #t) (kind 0)))) (hash "076xyvjbfjwrlq98qr93jsgri320yrjvx6fqaa6hcxyd3ism3mnx")))

(define-public crate-mut_static-1 (crate (name "mut_static") (vers "1.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.7") (default-features #t) (kind 0)))) (hash "16nq25b3jmbc45ikf82nqrb5qm7206gb2ixji8w3833c8x8wyncl")))

(define-public crate-mut_static-1 (crate (name "mut_static") (vers "1.0.1") (deps (list (crate-dep (name "error-chain") (req "^0.7") (default-features #t) (kind 0)))) (hash "0rr3lhkkdzwjcplm95y85mw2373ni8vv7fjvpzgf2cifrg1sdiim")))

(define-public crate-mut_static-2 (crate (name "mut_static") (vers "2.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.7") (default-features #t) (kind 0)))) (hash "1s9mfilxiszrs7a7c67imcw9a34pvqzv8f4sgy5f6jl44xjic9ra")))

(define-public crate-mut_static-3 (crate (name "mut_static") (vers "3.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.7") (default-features #t) (kind 0)))) (hash "0nm6yx6zzwwh1avkck86yjdsmcfy5l001m8k89440070y42s3il5")))

(define-public crate-mut_static-4 (crate (name "mut_static") (vers "4.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.9") (kind 0)))) (hash "0rznxzmqhn5bgxhqsjgcx37rdlchrj5p02l037a8q0p86xh2kwcw")))

(define-public crate-mut_static-5 (crate (name "mut_static") (vers "5.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.10") (kind 0)))) (hash "0rkg6d34kxsgzz206zn054f2gfsb38brlz54pcc3ckls73mxi314")))

