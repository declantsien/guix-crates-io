(define-module (crates-io mu sk) #:use-module (crates-io))

(define-public crate-musk-6 (crate (name "musk") (vers "6.6.6") (hash "06bnpj20530qm1jxdazkb49zvj2316qchfyi6clba0rnjk1zmai5") (yanked #t)))

(define-public crate-musk-0.0.0 (crate (name "musk") (vers "0.0.0") (hash "0a98721mr1fqiqbr3hnlikgpg7dd73bgc26h7qm3bb0mvly0ar4b")))

