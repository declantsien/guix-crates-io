(define-module (crates-io mu tv) #:use-module (crates-io))

(define-public crate-mutview-0.1 (crate (name "mutview") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0l68ln2f13bx0a814cqxxx4jsjgrqnk08xfgn30w69ybbm7spg4k")))

(define-public crate-mutview-0.1 (crate (name "mutview") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1j5zfc42c0fyiwymx6byc1c3swjfzh4awqmdl8ar5g8rlgcwyph6")))

(define-public crate-mutview-0.1 (crate (name "mutview") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0xwb0w54yhplrssf5wprsfvy1872ai5n00285dbrz3dq9dn0bv7y")))

