(define-module (crates-io mu se) #:use-module (crates-io))

(define-public crate-muse-0.0.1 (crate (name "muse") (vers "0.0.1-dev") (hash "0f9cykm9xpzcqlbkvid1f030v00zdcd4cmkr9bz986xjj2290rsm") (yanked #t)))

(define-public crate-musescore-rip-0.1 (crate (name "musescore-rip") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "headless_chrome") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.9") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "svg2pdf") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vl14vmyh83h90vhzz60s2ica709ykvzpy48lf3wjhw0abck946v")))

