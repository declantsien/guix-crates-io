(define-module (crates-io mu t-) #:use-module (crates-io))

(define-public crate-mut-binary-heap-0.1 (crate (name "mut-binary-heap") (vers "0.1.0") (deps (list (crate-dep (name "compare") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 2)))) (hash "080mq66729mkmsdzmk2yc7kwzq8858w87cnhxm39vq6cav9h3fxa") (rust-version "1.56.0")))

(define-public crate-mut-cell-0.0.0 (crate (name "mut-cell") (vers "0.0.0") (hash "1gr8wvfdgv3zsqfqy2vxjc448qr9pcs0x2gn80150jq9rf33j9s8")))

(define-public crate-mut-rc-0.1 (crate (name "mut-rc") (vers "0.1.0") (hash "0gd9yk6jd8k3jb35iksfxgjh5vy44lxm5rna9xn0bkqnymfg25fq")))

(define-public crate-mut-rc-0.1 (crate (name "mut-rc") (vers "0.1.1") (hash "090i80af5yprms5r9apbx530an0886118d3hi3vdbafvvmydwawg")))

(define-public crate-mut-rc-0.1 (crate (name "mut-rc") (vers "0.1.2") (hash "0gsnpr8kl4dga1sk5yci97dlwh9whmrpfvzh0451fxqs45dh61x1")))

(define-public crate-mut-str-1 (crate (name "mut-str") (vers "1.0.0-alpha.1") (hash "00yn120dpjzll59ia501rj0hgc2zrq4g74if1c2h2a965k2gsghx") (features (quote (("std") ("default" "std"))))))

(define-public crate-mut-str-1 (crate (name "mut-str") (vers "1.0.0-alpha.2") (hash "0kfkgzhy6s1gbry1r0dh53pmr49hk4h5niba17vwmqf4qx8gym0h") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1 (crate (name "mut-str") (vers "1.0.0-alpha.3") (hash "0ybfngm1allsh18naqi2l861ffyj6b5dfcv9v3fb8yfaxcd83255") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1 (crate (name "mut-str") (vers "1.0.0") (hash "0jxarw6yab400k74y01xmf210l004q631s3wbiy6fmp19ikda4b7") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1 (crate (name "mut-str") (vers "1.0.1") (hash "15z3gxxpw3vcplbyvhyv66iz2iirpw7dv0nsfzdvcsnlii0bsxr2") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1 (crate (name "mut-str") (vers "1.0.2") (hash "1fniy3x85b1d320b2npc9py50bam63pr3gkfccwh8f2l596zdgkf") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mut-str-1 (crate (name "mut-str") (vers "1.1.0-alpha.1") (hash "0kldkl28xy23l1aygawl1yv24wg7jkzdbz37zc4p564yhvng0x3q") (features (quote (("std" "alloc") ("future") ("default" "std") ("alloc"))))))

