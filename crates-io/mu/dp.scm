(define-module (crates-io mu dp) #:use-module (crates-io))

(define-public crate-mudpie-0.0.1 (crate (name "mudpie") (vers "0.0.1") (hash "0hzmfv9viasxfs6g3fww7pcj7id85vkjyv1n366wssyv14dniavh")))

(define-public crate-mudpie-0.0.2 (crate (name "mudpie") (vers "0.0.2") (hash "1n1p4mcawdvclazqi7hy240139w7g79i49rk91nwa2dqp8gy4g0m")))

(define-public crate-mudpie-0.0.3 (crate (name "mudpie") (vers "0.0.3") (hash "1m267bj2a0ildqb1yb7ydwdgxfgv22imlffy1ky328mh8gsj11cx")))

(define-public crate-mudpie-0.0.4 (crate (name "mudpie") (vers "0.0.4") (hash "165j8c17kpdhm36n3vpmv1b2mdysqvyy5r3a9m96iqg6raiqr9r6")))

(define-public crate-mudpie-0.0.5 (crate (name "mudpie") (vers "0.0.5") (hash "18mw768pfwz6cpwsw0g7mkc7p53wahac84m155v5ap0ag1wvhf9z")))

(define-public crate-mudpie-0.0.6 (crate (name "mudpie") (vers "0.0.6") (hash "1k256a2idyv9f7wjasgfhn8wqmafwirax86yh55851s32kh3qbkb")))

(define-public crate-mudpie-0.0.7 (crate (name "mudpie") (vers "0.0.7") (hash "1by0fk6x01r6kjc592y91bzq5yf2a4qxzp6q565swp3m184b7jrv")))

(define-public crate-mudpie-0.0.8 (crate (name "mudpie") (vers "0.0.8") (hash "0fwn7pfd5x750k0zvyzbnfl03ws0652bsdzxmjqbm60mi2b04rjz")))

(define-public crate-mudpie-0.0.9 (crate (name "mudpie") (vers "0.0.9") (hash "18k6imdfzb6nmz4bqkxd3l2p9brwri86nsg5yxwvga0ahbvn9s1s")))

(define-public crate-mudpie-0.1 (crate (name "mudpie") (vers "0.1.0") (hash "1l7b78h8knafxgrq4s6fh41nb24qra725kpcddinqm34pk34yp1w")))

(define-public crate-mudpie-0.1 (crate (name "mudpie") (vers "0.1.1") (hash "06igp7hb2qfxh8ynwzspzpcsajgcmyzd0wgx7cwsmfahdrc0p7fx")))

(define-public crate-mudpie-0.1 (crate (name "mudpie") (vers "0.1.2") (hash "0x5isxc11fd87xn7ssgq3rpdj701f7w5sf7nhgrxq7p28zawnib4")))

(define-public crate-mudpie-0.1 (crate (name "mudpie") (vers "0.1.3") (hash "1z7g4zvdhgmk875w8a0zhqhz87sqd4hcahcbhym29mjz26xnj48l")))

(define-public crate-mudpie-0.1 (crate (name "mudpie") (vers "0.1.4") (hash "19xil60kcqwvqrgaahc27vm5jmhabfgylci49j31i6rni07f55fc")))

(define-public crate-mudpie-0.2 (crate (name "mudpie") (vers "0.2.0") (hash "1qjy73y39iix8vbnwx2q085bw1ax1l3dqvcazffhy0hf05bv8cck")))

(define-public crate-mudpie-0.2 (crate (name "mudpie") (vers "0.2.1") (hash "1ahsx1kcmqfz25k7cy5f9wp6zqivjsh012f5mp0b77hy2dzrj5fv")))

(define-public crate-mudpie-0.2 (crate (name "mudpie") (vers "0.2.2") (hash "1pdpssh1zckr9zw28chqrmx8bnbl1f0ppzra13q087w3bzkx4dvq")))

(define-public crate-mudpie-0.2 (crate (name "mudpie") (vers "0.2.3") (hash "1f7l1z9sq3ck1z28azbz8w9icpigiw8y7718hn3m36l5pi4inqk8")))

(define-public crate-mudpie-0.2 (crate (name "mudpie") (vers "0.2.4") (hash "1bp0kqd1fqfb9vww500w20lwikc1br58gdjlrmad3624vhp9nydm")))

(define-public crate-mudpie-0.2 (crate (name "mudpie") (vers "0.2.5") (hash "0c9xqjc10815flm97hqajvgpxq7h4ghszpp2fn142m4lvb16qknj")))

(define-public crate-mudpie-0.2 (crate (name "mudpie") (vers "0.2.6") (hash "0dqcz2gcyvqw48xcw5qq43rnpvyndb1wf6dzlbx6z6ridnil9j7g")))

