(define-module (crates-io mu tf) #:use-module (crates-io))

(define-public crate-mutf8-0.1 (crate (name "mutf8") (vers "0.1.0") (hash "1yvgpfqbwmwj8pb7ybsbydf2f0wfckxmm4p34r2vvmjmlmd8sgnc")))

(define-public crate-mutf8-0.2 (crate (name "mutf8") (vers "0.2.0") (hash "0ifg2byhfygkwslkdx8i52vrlc2jplz8aq1msfd8w62cd9149d4p")))

(define-public crate-mutf8-0.3 (crate (name "mutf8") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 2)))) (hash "1ad81s5hiw4f3kvla9dwpwqa6bzvr29ir909x1nn549crhv8g7jp") (features (quote (("use-serde" "serde") ("default" "serde"))))))

(define-public crate-mutf8-0.4 (crate (name "mutf8") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 2)))) (hash "16h0bmy9qwgd3qiz2w1qshxxda4cy6id878pwhi4l0s3n10q6rd6") (features (quote (("use-structs") ("use-serde" "serde") ("default" "use-structs"))))))

(define-public crate-mutf8-0.4 (crate (name "mutf8") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 2)))) (hash "1kq8xcxy0nyj19r2xn6v28bbgv1ybs8ywiq76p37c92h69zvsghq") (features (quote (("use-structs") ("use-serde" "serde") ("default" "use-structs"))))))

(define-public crate-mutf8-0.5 (crate (name "mutf8") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 2)))) (hash "1wzmanw8xla2rkgnjy8ychazn6gxs3wqrrz9j509ga3r5yziqi25") (features (quote (("use-structs") ("default" "use-structs"))))))

