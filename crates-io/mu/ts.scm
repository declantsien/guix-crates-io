(define-module (crates-io mu ts) #:use-module (crates-io))

(define-public crate-mutself-0.1 (crate (name "mutself") (vers "0.1.0") (deps (list (crate-dep (name "object") (req "^0.30") (features (quote ("read_core" "write_std" "elf"))) (target "cfg(unix)") (kind 0)) (crate-dep (name "object") (req "^0.30") (features (quote ("read_core" "write_std" "pe"))) (target "cfg(windows)") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing" "printing" "clone-impls" "proc-macro" "full" "extra-traits"))) (kind 0)))) (hash "0lrzazxn6ry50mg3acv4ra60bma8a72ybw9ifwfpbac544hlp84g") (rust-version "1.61")))

(define-public crate-mutself-0.2 (crate (name "mutself") (vers "0.2.0") (deps (list (crate-dep (name "object") (req "^0.31") (features (quote ("read_core" "write_std" "elf"))) (target "cfg(unix)") (kind 0)) (crate-dep (name "object") (req "^0.31") (features (quote ("read_core" "write_std" "pe"))) (target "cfg(windows)") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing" "printing" "clone-impls" "proc-macro" "full" "extra-traits"))) (kind 0)))) (hash "1yjk6128fjfhj0r0jl82x2dhcm2jpmb5arlffpxyajdpg0cgfllv") (rust-version "1.61")))

(define-public crate-mutslices-0.1 (crate (name "mutslices") (vers "0.1.0") (deps (list (crate-dep (name "trybuild") (req "^1.0.83") (default-features #t) (kind 2)))) (hash "18ffskp2dffx1p1qdxp77nhbwp2prdxhgmbl70h336161zxkvg5m")))

(define-public crate-mutstr-0.1 (crate (name "mutstr") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1syrqajm19z106yg6igjqx7y3lw7w02ck3kcqrwp4m1la2bqk1s7") (features (quote (("drop") ("default" "drop"))))))

(define-public crate-mutstr-0.1 (crate (name "mutstr") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1c2jfkp17sb485pjzpr1mgs6svc13pba6r329ffg08l2qn7vi77q") (features (quote (("drop") ("default" "drop"))))))

(define-public crate-mutstr-0.2 (crate (name "mutstr") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qxppcs4bh63maf1hxkpkimk90ln21k832sa1qnrf9zgi07vdiks") (features (quote (("drop") ("default" "drop"))))))

