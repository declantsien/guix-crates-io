(define-module (crates-io mu js) #:use-module (crates-io))

(define-public crate-mujs-0.0.1 (crate (name "mujs") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qda16qb7g20acc7ixwq42rlalymc7vbvdiapcxwapq1sdzrai6z") (yanked #t)))

(define-public crate-mujs-0.0.2 (crate (name "mujs") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "062a1lqkm08mjcacjb0h81yli955a5niv08dviy0sfj6xgsgx932")))

(define-public crate-mujs-0.0.3 (crate (name "mujs") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0hl1paxqxl83g183s19r765ca8m2grar02x1ba6fj1jdb36nhgil")))

