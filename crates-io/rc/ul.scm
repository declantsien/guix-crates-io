(define-module (crates-io rc ul) #:use-module (crates-io))

(define-public crate-rculock-0.1 (crate (name "rculock") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.3") (default-features #t) (kind 0)))) (hash "0q0w7lvsnpwpncikikslylf58gmmmfrwv23xl988ki7551jm2adh") (yanked #t)))

(define-public crate-rculock-0.1 (crate (name "rculock") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.3") (default-features #t) (kind 0)))) (hash "15z3skqvj30zbhzmkkk8wildqf232l7vx9hsvjnnx4jp9q6bgqg2") (yanked #t)))

(define-public crate-rculock-0.1 (crate (name "rculock") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wghwyr0jca3dcf438mrnk83j2j36jpraplav0jxbm2qlblqmgy7")))

