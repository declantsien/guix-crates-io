(define-module (crates-io rc #{4o}#) #:use-module (crates-io))

(define-public crate-rc4ok-0.1 (crate (name "rc4ok") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "=0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "=0.4.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "=0.8.5") (default-features #t) (kind 2)))) (hash "07670vnk1h1yw24sklg1ai9gpigznslipwh0a8m07vbx6nfvwmzf")))

