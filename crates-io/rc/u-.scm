(define-module (crates-io rc u-) #:use-module (crates-io))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1zz8qy89ga88x28zg53qna7y9h1b7c34wz90xybxbppnvbwcm20y")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "141v63878w24h5rsyi685p0dzqsnnm1gng41g2zqjhiiclygqrh2")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ww441in6c0ffx0blqgm7rx6idm7ydca12s9z82ynb622rc8g6h7")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1mvl66pgc7jlzvcvkzzgxcs39cvizxjv3g0ijfc6jr54yrk74wvj")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "045vd3d3f07f8pajhpb1qc34rzxzbm6bs0qilmdmihp9n2vw1b5b")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0vnb6axdk5xz5ccdn8bljnyv39ghdzsvldzxb5cspmskv6w0zz8g")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0p7gcdsc4g8b7wwym9g046wj647khcanhhh7pm76xl7iybd2j1vv")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.7") (deps (list (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "09j6a8skimb3qy85nkgi10m37wfs4fxjj3yhmhisbcx9k041qy14")))

(define-public crate-rcu-clean-0.1 (crate (name "rcu-clean") (vers "0.1.8") (deps (list (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "162mi74mkxxijkfvy9wdd7bm7s8ian1cv7fgyvyd8sp3lzzl27f3")))

