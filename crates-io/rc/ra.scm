(define-module (crates-io rc ra) #:use-module (crates-io))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "0g0073kfj38zf1p6dr6p1cqv06vlpzm5nm6f5hqpyk8imcdyr68d")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "1ngyyx06ybwwf18wpl1mn1vj2jdfpap7jnrsa0a5j7zzagxjnfxr")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "1ijqcad9zg5863hgb8p6xg4b362y463icfck10v0xwnlvkr2pb0w")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "13spzbvp7ff66pl8ynmkc3zigfl905v9b4jgifnmjpr9b1m7xdr6")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "1m25wkwgi47avyzbfg9c8gin5s4k86va9jpnhpi1w89x495xnc8s")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "19dk847hva7yy39kgsiqzzzmip43gdp7wfaq36wbm8jv3gps9flv")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.3") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "1w6gm8q48ggscq62n08ifbcf18bsxxsgdsc0hz8vniklivc1dkk9")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.4") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "0mnqwr6378fa0ls995wc76p1vwfi6l8cjyids922ikccafxbn2bd")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.5") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "1wwgjhiiq7b9zzhksd0vc6xrq2573dki73dy0pnd35bq98r0sasv")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.6") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "0nljd7c4fi7syjh4kw46r1sbdff7ynh670bh0wa60bbxwwqb0mah")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.1.7") (deps (list (crate-dep (name "clap") (req ">=2.33.0, <2.34.0") (default-features #t) (kind 0)))) (hash "1q23ngl95g7f39rkb9h11fz5n6bm769pshk9bl9lf4wpiycm9zf4")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hw0z1cq45fm7pr1mgvf0xg1ipyxks25sjgf58f8ixdwh2v2y96i")))

(define-public crate-rcrawl-1 (crate (name "rcrawl") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "150rzbb1dz940gawbd413p8m88wmbyfqykr35apb07lirgqafmhy")))

