(define-module (crates-io rc oc) #:use-module (crates-io))

(define-public crate-rcocos2d-sys-0.1 (crate (name "rcocos2d-sys") (vers "0.1.0") (hash "0p8mmglslngqfmaha7n4bpc0lqwarzx42slpvcczznz2s77yadr8")))

(define-public crate-rcocos2d-sys-0.2 (crate (name "rcocos2d-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "1i0cwq7qkyyjjs87dx2jqg61xkw5p3v4ddl5bhyfr3s93mmgnd4f") (yanked #t)))

(define-public crate-rcocos2d-sys-0.2 (crate (name "rcocos2d-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0i44j30lnsry386ylb4y02h4d93kvspjlv0di7l6kjwcypq1bgrv")))

(define-public crate-rcocos2d-sys-0.2 (crate (name "rcocos2d-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "1gg23wsir3pshshcmg043d0xy1ggsgrk6lsny92hzbqcif86aqpw")))

(define-public crate-rcocos2d-sys-0.2 (crate (name "rcocos2d-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "19pmrl7cv3342j9x1g0mial808nvbjnipc6wrgk3h2hwssfkgxjn")))

(define-public crate-rcocos2d-sys-0.2 (crate (name "rcocos2d-sys") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "1j29rr902961dypwhyx3gdvdv9g893ha1nadwqz3163xqf3ysrbd")))

