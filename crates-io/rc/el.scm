(define-module (crates-io rc el) #:use-module (crates-io))

(define-public crate-rcelebrone_functions-0.1 (crate (name "rcelebrone_functions") (vers "0.1.0") (hash "0h4xifh0azychlnc6dbsiahbgbb821d3zlmrhq6z6ssgxvp85v7j")))

(define-public crate-rcelebrone_functions-0.1 (crate (name "rcelebrone_functions") (vers "0.1.1") (hash "1hwbwwlwix46395hlrqdhv6hmrxfg101znzwylb7bmnbxka81jzp")))

(define-public crate-rcelebrone_functions-0.1 (crate (name "rcelebrone_functions") (vers "0.1.2") (hash "0010mkpk7sw5bd7qd5z7948fxhgsqn4lc527ywhsw5g5vl1wh6sp")))

(define-public crate-rcelebrone_functions-0.1 (crate (name "rcelebrone_functions") (vers "0.1.3") (hash "189r8ncmkf4wm836q02hhk4zf9k92ny9nd2c86ml0ywr6lchrphw")))

(define-public crate-rcelebrone_functions-0.2 (crate (name "rcelebrone_functions") (vers "0.2.0") (hash "1h6k48ksrlzpabkh5w4hlmqbz0ddj7bgwjjlbb86bddfrsbm6mfv")))

(define-public crate-rcelebrone_functions-0.2 (crate (name "rcelebrone_functions") (vers "0.2.1") (hash "1qsdgs2p444fcp4njpxy1sd0zp4nnbbr1xm1ddcg0h03x4gys3ad")))

(define-public crate-rcell-0.0.0 (crate (name "rcell") (vers "0.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0n47db3r58aad4qap4b1bmhaax69mszyfsqja2rprqk06ja1vy5a")))

(define-public crate-rcell-0.1 (crate (name "rcell") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0rnqfhvfvrakplprqpfvcmy5shpr7j749c4v3cxiqc5cgqjxb3gl")))

(define-public crate-rcell-0.2 (crate (name "rcell") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0bd9p6y53l9m2rwx6ws3mzibgppyf4bbj2419w7k0j675jzvh024")))

(define-public crate-rcell-0.3 (crate (name "rcell") (vers "0.3.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "16hvajhvjpd31xalypais3xd9glykrwaahwa5wg4crad19pinnvh")))

(define-public crate-rcell-0.4 (crate (name "rcell") (vers "0.4.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "1z8aqhdixq4ad9p97lsbsx3jabqj348lx87yrm4zqv19j1m60vf1")))

(define-public crate-rcell-0.5 (crate (name "rcell") (vers "0.5.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0963gg6y26sji6chhwpbi0j0445cvn4q6xcld783nmisvdqh8ngp")))

(define-public crate-rcell-1 (crate (name "rcell") (vers "1.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "1af4pglf26x5pgrhal5cw4nn2zbpcwii2blpbj8rhhxh6gpvhq9x")))

(define-public crate-rcell-1 (crate (name "rcell") (vers "1.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "05sa46j894kdk7ghy25cfq7b6a3gk9h3rmqzvnhw5g5l35lyxsww")))

(define-public crate-rcell-1 (crate (name "rcell") (vers "1.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "10mkr7560r1z16rjkjm4yd2bvln8bg54mws63wy3ijy15ijl6yvf")))

(define-public crate-rcell-1 (crate (name "rcell") (vers "1.1.2") (deps (list (crate-dep (name "parking_lot") (req ">=0.11, <=0.12") (default-features #t) (kind 0)))) (hash "13gbvql3zc7k27r5dkiyhqggz8307pfxw9j50n74nn9v6jkq2nnz")))

(define-public crate-rcell-1 (crate (name "rcell") (vers "1.1.3") (deps (list (crate-dep (name "sharded_mutex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f7iscpsl0cnr6zsp6z397b7kl4m2hd4vsafhf0whjarj3llmh62")))

(define-public crate-rcell-2 (crate (name "rcell") (vers "2.0.0-pre0") (deps (list (crate-dep (name "sharded_mutex") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qviggzwp1dpq98m2hkg8ihqc1rkf9lh0v1dbvwjm7mjcb2zlsag")))

(define-public crate-rcell-2 (crate (name "rcell") (vers "2.0.0") (hash "08ycymcz5za1wh3xpxc59b7gw3pa3v288md36ryra9vcx4cgg0ny") (features (quote (("sync") ("default" "sync"))))))

