(define-module (crates-io rc nb) #:use-module (crates-io))

(define-public crate-rcnb-rs-0.1 (crate (name "rcnb-rs") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "byte-unit") (req "^4.0.9") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "^0.8.0") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1d08zy1nnfq2wmsjlxf0i8xwssj5qvhsw4crzwqchn29aacyi7b4")))

