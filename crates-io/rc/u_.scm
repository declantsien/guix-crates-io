(define-module (crates-io rc u_) #:use-module (crates-io))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.0") (hash "1arqwlsv5rmwmxzyyc3qcpyjfg54qmhvz1lnzhlq5qvlk5mmvm8p")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.1") (hash "1pxmpynasgg2lxmgkck2xs1pkj2dp97bijcgk71h9cia9wybad8m")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.2") (hash "19j3iv0m1l8hk8mm7jy5qx7lfk5jmq8g0laz7nvcppn96ai8v353")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.3") (hash "1fwzdxjdgap5wf6j68dmzwxkxks7lsdzjyf65zh91lhklp0z5932")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.4") (hash "084q2cfhs6fmcpkn8yz7nwnm0yrp2yg820rpk7y6fvw3k6nq1n73")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.5") (hash "1g9mw4qnyjxk2dh263qr8mvlqpl0xvikpnlsh6xlv14584gmap17")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.6") (hash "0d39gx7i0xfd70lfc5xma2nxqmyyzkxmb37dx1fx40xyhdf0qimg")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.7") (hash "1hk2yg0bz91g4qqb42x0fhhz9kn486jrarhpjc0gfrzk0dibk9h9")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.8") (hash "0m3b2vvv0lr4psi7f08kncq0sccwz080sgmbxsgzghqfzc6rq4z1")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.9") (hash "0fvihy5axb4br8cf7pw725sq5r3s5m2q20mj6mb9cdd2cq61gqn8")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.10") (hash "0g7ixcgki5n5hmfv5nlqcs1kp3yhb7ls9fppmjp3lvfv88id97dr")))

(define-public crate-rcu_cell-0.1 (crate (name "rcu_cell") (vers "0.1.11") (hash "1p87vyxzkllpznfjq0js12xcwyv0ky27vdbbg808v4jgln6xsvl9")))

