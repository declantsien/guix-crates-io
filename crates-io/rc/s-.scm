(define-module (crates-io rc s-) #:use-module (crates-io))

(define-public crate-RCS-Module-0.1 (crate (name "RCS-Module") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "122h9g8nxjai4mxzss812rs44438sayk8bza7vr8zy6l31sy41nk") (rust-version "1.56")))

