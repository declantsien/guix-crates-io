(define-module (crates-io rc ap) #:use-module (crates-io))

(define-public crate-rcap-0.1 (crate (name "rcap") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1vz30yqdg1v9ybc68gpp4gmlrq8jq9dv2isyqz0j229sk9laz2cc")))

(define-public crate-rcap-0.1 (crate (name "rcap") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1qidlb8f3m85s8fhfvxjvadx4g84rxxf23nh5n5snif2g4n6pp6g")))

(define-public crate-rcap-0.1 (crate (name "rcap") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "102rwbvx8l8433cv9kav756rvir7skzjaa6xqrhlkzg17iw6n3iq")))

(define-public crate-rcap-0.1 (crate (name "rcap") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1mymrblmjq8qgzrg7fbgb5dmppc30pa4j4ipdzzm7sny5hk7h30x")))

(define-public crate-rcap-0.1 (crate (name "rcap") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1ajq04w9i5y9c9ga63g6yp6s29gxncwz776zhim8jrvc9aaj1rl4")))

