(define-module (crates-io rc ms) #:use-module (crates-io))

(define-public crate-rcms-0.1 (crate (name "rcms") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)))) (hash "17313wpi6ngdkv7cs9a7nm3gjmnlxp4ys5spdy9ij1afalkyws2h")))

