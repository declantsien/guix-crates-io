(define-module (crates-io rc by) #:use-module (crates-io))

(define-public crate-rcbytes-1 (crate (name "rcbytes") (vers "1.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1k143s282vp449dw1zi9hp79d855xxhybjkyb965gnshzf04rr04") (features (quote (("std") ("default" "std"))))))

(define-public crate-rcbytes-1 (crate (name "rcbytes") (vers "1.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0lwckxlml08vidd79q7f96dhh77q7hxl52a7dcfjn4k1ncbm1zkd") (features (quote (("std") ("default" "std"))))))

