(define-module (crates-io rc rt) #:use-module (crates-io))

(define-public crate-rcrt1-1 (crate (name "rcrt1") (vers "1.0.0") (deps (list (crate-dep (name "goblin") (req "^0.4") (features (quote ("elf64"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ascxkrx7ljyxl60pn8pib4ckkaqnjjxfw3iwghq5mc0bwjh01v0")))

(define-public crate-rcrt1-2 (crate (name "rcrt1") (vers "2.0.0") (deps (list (crate-dep (name "goblin") (req "^0.5") (features (quote ("elf64"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p6nwk76xmjq6mbwijaf7sximacv8c2xmqdc651i6w7im5l1mf10") (rust-version "1.56")))

(define-public crate-rcrt1-2 (crate (name "rcrt1") (vers "2.1.0") (deps (list (crate-dep (name "goblin") (req "^0.5") (features (quote ("elf64"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cd56mphqm5pcjmz704v0ghjfk29p237vzzxmvxmdgywz04b9wbw") (rust-version "1.56")))

(define-public crate-rcrt1-2 (crate (name "rcrt1") (vers "2.2.0") (deps (list (crate-dep (name "goblin") (req "^0.5") (features (quote ("elf64"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bii9qkkvmc0q29blhi7v1rsdrgzasqarkfm4lk7py38dcbglfq6") (rust-version "1.56")))

(define-public crate-rcrt1-2 (crate (name "rcrt1") (vers "2.3.0") (deps (list (crate-dep (name "goblin") (req "^0.5") (features (quote ("elf64"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ic342ahm3dcgi77m858al4cq7njc578vg2j1ah4qj01g5bqz0fv") (rust-version "1.56")))

(define-public crate-rcrt1-2 (crate (name "rcrt1") (vers "2.4.0") (deps (list (crate-dep (name "goblin") (req "^0.5") (features (quote ("elf64"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qw0v75h42rp8icdrv7skr8lsak0wqc89b5yl65jir535jkrk2jg") (rust-version "1.56")))

(define-public crate-rcrt1-2 (crate (name "rcrt1") (vers "2.5.0") (deps (list (crate-dep (name "goblin") (req "^0.6") (features (quote ("elf64"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08cl7sc5gzjlwxi33ig4r2y5wfi4dsfhhcyaigqchw3rpnig8ilf") (rust-version "1.56")))

