(define-module (crates-io rc -b) #:use-module (crates-io))

(define-public crate-rc-borrow-1 (crate (name "rc-borrow") (vers "1.0.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "19107jwh878awifb58nwcknkjcy2yxs76r0r0vx2q1702945dbh3") (features (quote (("std") ("default" "erasable" "std")))) (yanked #t)))

(define-public crate-rc-borrow-1 (crate (name "rc-borrow") (vers "1.1.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1k63f6jbrqsjlx0dn551hw1fayvhpj8mii3ffwzcscqi4sszzjlg") (features (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1 (crate (name "rc-borrow") (vers "1.2.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1g6ccqspqszb90fk4vvxs75pszzaj7y5w350vn816ldw2b1digdp") (features (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1 (crate (name "rc-borrow") (vers "1.2.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0l7xl1qid17anbn36xy06ppcy40pdxfkdd24z6j7x450rspz0m0f") (features (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1 (crate (name "rc-borrow") (vers "1.3.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1l5ha7h5m8jk9y3p4sydj7s0003m64b1y6injycqvmr6ff8kqvgp") (features (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-borrow-1 (crate (name "rc-borrow") (vers "1.4.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mszljysiqjrrinms54mbb9klb5s83plixw6khr1x9nphv2vf3dg") (features (quote (("std") ("default" "erasable" "std"))))))

(define-public crate-rc-box-1 (crate (name "rc-box") (vers "1.0.0") (deps (list (crate-dep (name "erasable") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1r9s5whj70ffilj4gx6yf17mg0k38ld3f9vsk5676v7kdlfpaf3n") (features (quote (("default" "erasable"))))))

(define-public crate-rc-box-1 (crate (name "rc-box") (vers "1.1.0") (deps (list (crate-dep (name "erasable") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slice-dst") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1yj1b8vnnf31ilbxy2f9977lgcmahmckprhdyp1cl6569q60wdci") (features (quote (("default" "erasable"))))))

(define-public crate-rc-box-1 (crate (name "rc-box") (vers "1.1.1") (deps (list (crate-dep (name "erasable") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slice-dst") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "115qyny9sn3ljg676ddrnqlpq94y6hf8zcilmlxv5nrmmxkfc721") (features (quote (("default" "erasable"))))))

(define-public crate-rc-box-1 (crate (name "rc-box") (vers "1.2.0") (deps (list (crate-dep (name "erasable") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slice-dst") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unsize") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1n1zb14isg05apb21n8h4b0bm58kvrdc5aydq8q402dzx9chfsg0") (features (quote (("default" "erasable"))))))

