(define-module (crates-io rc gc) #:use-module (crates-io))

(define-public crate-rcgc-0.1 (crate (name "rcgc") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "0scnkpyqil5vjc6hisdfminl7igvw5njd0nqii8k7k6cvrvf6j73")))

