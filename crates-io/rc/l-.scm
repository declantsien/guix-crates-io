(define-module (crates-io rc l-) #:use-module (crates-io))

(define-public crate-rcl-sys-0.0.0 (crate (name "rcl-sys") (vers "0.0.0") (hash "1g9zgcql606a270x9g8lsjc7zj1nhnjxwj0pgi2zf6qrjlmxj5sm")))

(define-public crate-rcl-sys-0.0.2 (crate (name "rcl-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "15kkgdr2fihafwf4h80gf0wjhnb0p0ch3y35vx73zg5lnpj476dh")))

