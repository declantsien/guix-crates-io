(define-module (crates-io rc at) #:use-module (crates-io))

(define-public crate-rcat-2 (crate (name "rcat") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)))) (hash "0ikmiqfm2z4lf4wf7ipy7x5qaslq7ahlflzwcdmvbh0vq7vp5pwa")))

(define-public crate-rcat-2 (crate (name "rcat") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)))) (hash "16yc4dn3r13wa5b087qcgy01v3v9k6dk2s09jr7mz1ljgnn56994")))

(define-public crate-rcat-2 (crate (name "rcat") (vers "2.0.2") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)))) (hash "0vca1b1n9x0hqp958vb8k23v7pmsy4cclkxnr11g9vc9f2i3xw4q")))

(define-public crate-rcat-2 (crate (name "rcat") (vers "2.0.3") (deps (list (crate-dep (name "clap") (req "^2.26") (default-features #t) (kind 0)))) (hash "0gf84l97z87n8r443klgqw7qsrywpbfgks48ikz4m9x07wv5vs8b")))

(define-public crate-rcat-2 (crate (name "rcat") (vers "2.0.4") (deps (list (crate-dep (name "clap") (req "^2.30") (default-features #t) (kind 0)))) (hash "183sdvanafz6mda9yar0yvdhl2sdfw25f6gl1819gq02zxghx5gc")))

(define-public crate-rcatt-0.1 (crate (name "rcatt") (vers "0.1.0") (hash "0gw1sxlsndppx0zh0r4simzkyi5znszhdyxqf4mzc7r6m8ijpa5k")))

(define-public crate-rcatt-0.2 (crate (name "rcatt") (vers "0.2.0") (hash "0lr7pd9vkxnir431rf51hjbi3rcfc9zz25z9ghjgr5ibpb4a1kz7")))

