(define-module (crates-io rc lu) #:use-module (crates-io))

(define-public crate-rclua-0.0.1 (crate (name "rclua") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.97") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)))) (hash "0czgxm46h26nnpf07gcwq694vqgpsxgj49c4ym1rmgwf8zawiqqd")))

(define-public crate-rclua-0.0.2 (crate (name "rclua") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.97") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)))) (hash "0iany2pxmw0b872yn26nap8iw7yqpgsqfk82an021lsriq6ilckw")))

(define-public crate-rclua-1 (crate (name "rclua") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.97") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)))) (hash "1cnxrs9jglmy2vqh2r815wicjcj20gp6zkbj7222nlnambgp06q0")))

(define-public crate-rclua-1 (crate (name "rclua") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.97") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)))) (hash "0prlb6bnj4h6291i48z1hl9fwg7izp8i664jcnr84mmi6yapv3qr")))

