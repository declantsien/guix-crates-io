(define-module (crates-io rc ue) #:use-module (crates-io))

(define-public crate-rcue-0.1 (crate (name "rcue") (vers "0.1.0") (hash "18rbp0rgrkdcvyxrika0l0wpyn7656qsw1xs74678bbvq4zj2lvj")))

(define-public crate-rcue-0.1 (crate (name "rcue") (vers "0.1.1") (hash "1fy4q5506c444bm31shgw1zlr7368biiyxd34mdnv4dhnbqnjmhm")))

(define-public crate-rcue-0.1 (crate (name "rcue") (vers "0.1.2") (hash "1xbs1qwszgz5r33clrsjnvpyf7vlkiy7000bsq0nxr22i96i4r7y")))

(define-public crate-rcue-0.1 (crate (name "rcue") (vers "0.1.3") (hash "0zlrvwwwk5dsjaqqk0s4wrdxr2rzsqnmbv72dmj5i0gic8fli8gw")))

