(define-module (crates-io rc me) #:use-module (crates-io))

(define-public crate-rcmerkle-0.1 (crate (name "rcmerkle") (vers "0.1.0") (deps (list (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "0zifgf2k0hpy4i814dhl1ymywy9bw58g36r8fp9sr0wp6450w1x2")))

(define-public crate-rcmerkle-0.1 (crate (name "rcmerkle") (vers "0.1.1") (deps (list (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qbkz5p69kskbkrdfy4bc2rn2g00glq1hkis2m0dwar2dzap4ddi")))

