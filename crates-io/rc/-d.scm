(define-module (crates-io rc -d) #:use-module (crates-io))

(define-public crate-rc-dlist-deque-0.1 (crate (name "rc-dlist-deque") (vers "0.1.1") (hash "0i9wj9ga0f677qn2x6j3cj3idm6fadr2l7xl8wpyvmsq58jjz6ia")))

(define-public crate-rc-dlist-deque-1 (crate (name "rc-dlist-deque") (vers "1.0.0") (hash "1790j4l0v3ydwx05rc7fad49q5ilws9bypin8446n1190rj34n72")))

(define-public crate-rc-dlist-deque-1 (crate (name "rc-dlist-deque") (vers "1.1.0") (hash "05zgv7xw2bx50jwyvrd96n9xchbax603ycphiipirvf83qjp7yid") (rust-version "1.31")))

(define-public crate-rc-dlist-deque-1 (crate (name "rc-dlist-deque") (vers "1.1.1") (hash "0bajxqgzb9rnkxaml35ni166z4cvskyqjn8n0gfmnz6ypm84g4rq") (rust-version "1.31")))

(define-public crate-rc-dlist-deque-1 (crate (name "rc-dlist-deque") (vers "1.1.2") (hash "00x0j4385ibh5b8m73zbfx4y0mdbrakj11dhrnxnh1aqv10bmh7b") (rust-version "1.31")))

