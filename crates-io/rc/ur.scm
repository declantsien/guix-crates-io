(define-module (crates-io rc ur) #:use-module (crates-io))

(define-public crate-rcurl-0.1 (crate (name "rcurl") (vers "0.1.0") (hash "1k6xnf4z8bf90hysapkvr7sagk74r4y4v6wivzl5p8n73yccir77")))

(define-public crate-rcurs-0.1 (crate (name "rcurs") (vers "0.1.0") (hash "02397g2wm3wnv9ni99ngdg22z8fvi77y2kpln85gg0ybjymab8qc") (features (quote (("std") ("default" "std"))))))

(define-public crate-rcurses-0.1 (crate (name "rcurses") (vers "0.1.0") (deps (list (crate-dep (name "ncurses") (req "^5.91.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0l1y3ah97w7p95g6jyj9g4hah4f68zm2f72wdflcvxkiydrngxrf") (yanked #t)))

