(define-module (crates-io rc -u) #:use-module (crates-io))

(define-public crate-rc-u8-reader-1 (crate (name "rc-u8-reader") (vers "1.0.0") (hash "1amcj58pmaqfz3316j9lg6lqw5j98sx1xipizhi134bcg59lf1pa") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-1 (crate (name "rc-u8-reader") (vers "1.0.1") (hash "1mzd2fwgwnds8kvzb53gxqa9akmwhjjic4m0iq2lnflgxgx2h9zk") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.0") (hash "0qfzyrjqsq6h8g1g1slxsfgf25p75wzsx45dxpxm35vilnafdazq") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.1") (hash "0by6csyg8ns7iv356xrbl9bjgh1yvhjvjsk68xx6xv94dg4cyjc4") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.2") (hash "0ydzvnf73q56q16vp6fqn41lwg376xl6qk7aa6aj7n3fcw3s45gz") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.3") (hash "15yahippxhwiqlc774ms8xi8s365dkl38m3k4yb5y12k020kch0b") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.4") (deps (list (crate-dep (name "debug-helper") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g8csxrp6px4am64ijz5ibqfijf50dj948wbaji5wvx69xp7wj17") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.5") (deps (list (crate-dep (name "debug-helper") (req "^0.1") (default-features #t) (kind 0)))) (hash "05q63llpa2s0g3z4lbkj486xjccpn79czsa479ccz38xlhsxxjj1") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.6") (deps (list (crate-dep (name "debug-helper") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mxp5r6if04ibadh4pxsn22hxp9n7zflq43yv8z280wzi71mshx0") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.7") (deps (list (crate-dep (name "debug-helper") (req "^0.2") (default-features #t) (kind 0)))) (hash "10w9mfi75dcg832hdrvym7wrc32a2688p4d42a18n015j0wdhah2") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.8") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)))) (hash "1sd87qw3s0a83qmdcibgzq95pchql9p08wwy0slcpgs53s5kdkqw") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.9") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)))) (hash "0c5k5vhkwwyw57y0x1jykz0h4lqmgkz47hagqhv6xwlzacdswqlp") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.10") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)))) (hash "1h7sxhq50jhkj7x6r0ly3369l7w6cvndl7sagggaznbaxpnbkypa") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.11") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)))) (hash "0k311kaygdqlzy3k71043r9wxymi4rk05j7r5gxvjscmlzisiiyq") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.12") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bw4ji3nwk8nrm3nvbq8jw5rg44j1plpdklsgra1zm3bk62qbl5a") (features (quote (("nightly"))))))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.13") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)))) (hash "15sddzia7wlp5dhczc5bjdr94pfrdp8yaxsypdxi4r9y2zfbpxrz")))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.14") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0s8zvcl655ism4j8xhx7qa2vw3i2a1c3ywmcgl5jc1vggrl42aw1")))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.15") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (optional #t) (kind 0)))) (hash "0imhivh4p0ib9vlk5d4bd01aqv2ssv47y7zbpl1j39qjfsaslq3j")))

(define-public crate-rc-u8-reader-2 (crate (name "rc-u8-reader") (vers "2.0.16") (deps (list (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (optional #t) (kind 0)))) (hash "0havzvikj3gx6kdghnz6c1cdfkri3g4zsawfwmkns7ycxmhg73qs")))

