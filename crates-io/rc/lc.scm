(define-module (crates-io rc lc) #:use-module (crates-io))

(define-public crate-rclc-0.8 (crate (name "rclc") (vers "0.8.3") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rcalc_lib") (req "^0.9") (default-features #t) (kind 0)))) (hash "04xdygzbq5kg1963pc30srvbri4i7482nka3zj4iw2bhn2hlbl9n")))

(define-public crate-rclc-0.8 (crate (name "rclc") (vers "0.8.4") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rcalc_lib") (req "^0.9") (default-features #t) (kind 0)))) (hash "0g5hfrfqmrgzfnr0v93nz1a2d2gbwv66kh8vgx4bqz38c908pcqk")))

(define-public crate-rclc-0.8 (crate (name "rclc") (vers "0.8.5") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rcalc_lib") (req "^0.9") (default-features #t) (kind 0)))) (hash "13vb958ry963f3sq44wr8x907mfi9fsh4lc7zz2b0ddxcfas9qz4")))

(define-public crate-rclc-0.8 (crate (name "rclc") (vers "0.8.6") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rcalc_lib") (req "^0.9") (default-features #t) (kind 0)))) (hash "170xzkbxpq06r0rc1kiw9wa181s8jjkk88m5h1dnygcj97ycs434")))

(define-public crate-rclc-0.8 (crate (name "rclc") (vers "0.8.7") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rcalc_lib") (req "^0.9") (default-features #t) (kind 0)))) (hash "03218r722lrkzrsmxb913pvh6p20wc934znnr6p4h2jpmvj61a91")))

(define-public crate-rclc-1 (crate (name "rclc") (vers "1.0.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rcalc_lib") (req "^1") (default-features #t) (kind 0)))) (hash "07ydcifn09vrrjvzi7z2y1cjj2w93p3rv151xygk8m7w14qkqkrk")))

