(define-module (crates-io rc ha) #:use-module (crates-io))

(define-public crate-rchan-0.2 (crate (name "rchan") (vers "0.2.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0dql9mivdzfw73p1g4ibl4id6rll3viijs20ny1y9610ijgrxsg8")))

(define-public crate-rchan-0.2 (crate (name "rchan") (vers "0.2.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0hyl2y4mh8yqk064zh7rmzpg102hzhrqdglc1vyirb6laxyrxxsf")))

(define-public crate-rchan-0.3 (crate (name "rchan") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0p3zyx71zkcpf49cqc3c0jnpfqxymqf8bk7ax5gsgnk909yfkarp")))

(define-public crate-rchat-0.0.1 (crate (name "rchat") (vers "0.0.1") (hash "1l192dp2rgky49s8mmygzmz07z0xb9ac82yj99p4nc47r3qavzlh")))

