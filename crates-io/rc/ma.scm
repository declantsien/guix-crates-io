(define-module (crates-io rc ma) #:use-module (crates-io))

(define-public crate-rcman-rs-0.1 (crate (name "rcman-rs") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1ch4a9zfydnar1gj5mr4bj97hp8l5j630hrsg81yr6pyv9g1j22x") (yanked #t)))

(define-public crate-rcman-rs-0.1 (crate (name "rcman-rs") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0q3nkhbwp0ckxg543lkzdvzai6y7grm9amdjpjykljbbavc8qqs2") (yanked #t)))

(define-public crate-rcman-rs-0.1 (crate (name "rcman-rs") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "04ynvxvc3aw2fm5xy7wgwklsf3x4k23f3b5ag0hya6n84b7nyf8j") (yanked #t)))

(define-public crate-rcman-rs-0.1 (crate (name "rcman-rs") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0q4bmi5bx1xaf83hpnpf0vmkjlw5yvk2a0j6ibixhjw33p89yw8r") (yanked #t)))

(define-public crate-rcmark-0.1 (crate (name "rcmark") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libcmark-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kvgyfbr2arrwlrkfw41ys997gxr8yryr8x8p6q3ncdvm83vs0b8")))

(define-public crate-rcmath-0.0.1 (crate (name "rcmath") (vers "0.0.1") (deps (list (crate-dep (name "num-bigint") (req "^0.2") (default-features #t) (kind 0)))) (hash "0289vy4l52lig4pbz9qfk1dlvx0lcnzin7r8a59nnj9i1vzjf9l4")))

