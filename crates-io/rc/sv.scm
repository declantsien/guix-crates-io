(define-module (crates-io rc sv) #:use-module (crates-io))

(define-public crate-rcsv-0.1 (crate (name "rcsv") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winnt" "fileapi" "handleapi" "memoryapi" "std"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0629x55bnv8hb1l0g5gq7pv3bjjsfxs6wg4msdf4b56wmngx0yqz")))

