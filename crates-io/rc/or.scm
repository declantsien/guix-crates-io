(define-module (crates-io rc or) #:use-module (crates-io))

(define-public crate-rcore-0.1 (crate (name "rcore") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rair-env") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rair-io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rtrees") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5") (default-features #t) (kind 0)))) (hash "0p6mq9479cgh6k8lhqfnxrdj5pvypkihy6nyywblgj0v5zim05d5")))

(define-public crate-rcore-console-0.0.0 (crate (name "rcore-console") (vers "0.0.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1mixlx8llp7s8xihdjdbqdyw9r3ib2drfyjh46cdcvrfdndf9ak3")))

(define-public crate-rcore-task-manage-0.0.0 (crate (name "rcore-task-manage") (vers "0.0.0") (hash "0va43k7mjbhp371vlhllv5lm4dknvjgxsvs6l2f6w5yv0nr3b2qk") (features (quote (("thread") ("proc"))))))

