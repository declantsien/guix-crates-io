(define-module (crates-io rc ir) #:use-module (crates-io))

(define-public crate-rcir-0.2 (crate (name "rcir") (vers "0.2.0") (hash "0z2bqpqy10myz42kfjlnk3x56p53k8697vlc9i3fjmw1yalj3051")))

(define-public crate-rcir-0.3 (crate (name "rcir") (vers "0.3.0") (hash "1f5w5h6m21lqaz9lvlsa4vxjc2575pr0nvfbqqlsid8515d6qzca")))

