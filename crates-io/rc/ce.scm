(define-module (crates-io rc ce) #:use-module (crates-io))

(define-public crate-rccell-0.1 (crate (name "rccell") (vers "0.1.0") (hash "0qwza5ig4sf3q34cvcrzlphr13jqim5dbrn33bi83zfrmlp480sb")))

(define-public crate-rccell-0.1 (crate (name "rccell") (vers "0.1.1") (hash "0cj6sbjbvzqbdr6g1agp8pgcmkhhy2izk6m7ra9fgc54w6plnhj6")))

(define-public crate-rccell-0.1 (crate (name "rccell") (vers "0.1.2") (hash "188gir7nk2fm4nbsif9d3afa855rx3gihj2bilfjsns5byi1drb2")))

(define-public crate-rccell-0.1 (crate (name "rccell") (vers "0.1.3") (hash "1xmxnhcc6yqw3wnrc4yjlaiisbdk1vwx0xxfgh43y3jhz1jaj722")))

