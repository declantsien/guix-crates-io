(define-module (crates-io rc od) #:use-module (crates-io))

(define-public crate-rcodec-1 (crate (name "rcodec") (vers "1.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pl-hlist") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "06mi0fs2wza0p939y0j4aj0fnrc8hp20zw07c3499486226h03bf")))

(define-public crate-rcodec-1 (crate (name "rcodec") (vers "1.0.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pl-hlist") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vykrs9jgbaxmbzr17b2q2rzz5ifnb4dn19zb7q65gqqgkfb1xmr")))

