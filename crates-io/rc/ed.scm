(define-module (crates-io rc ed) #:use-module (crates-io))

(define-public crate-rced-0.0.0 (crate (name "rced") (vers "0.0.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1k475b1rhimvifg5wi18zppcakv1g36zmkh05sxpgp5fc16vzanf") (yanked #t)))

(define-public crate-rced-0.0.1 (crate (name "rced") (vers "0.0.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0mad5gapk5jfcjn9cpz1qzf44qgfdcj8h63ggrhz8nqciaypjd13") (yanked #t)))

(define-public crate-rced-0.0.2 (crate (name "rced") (vers "0.0.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "023i8vkfhwcrrsl3saaf8961aa9l84zj6pf7829z1pp1jzmx95p2") (yanked #t)))

(define-public crate-rced-0.0.3 (crate (name "rced") (vers "0.0.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "15vfy5pbd34qjah5v6k0sgsnr71pv8v8cz3pp0pq8mapi10k30ic") (yanked #t)))

(define-public crate-rced-0.0.4 (crate (name "rced") (vers "0.0.4") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0vvqn75lqprxynvcaf8c945s097c426zayyfgwqirypb1f5010yg") (yanked #t)))

(define-public crate-rced-0.0.5 (crate (name "rced") (vers "0.0.5") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ddfc4vp3gfrlk4xw7z9qp0xs4ibcpxfl72islbbg0iav3l70lq5") (yanked #t)))

(define-public crate-rced-0.0.6 (crate (name "rced") (vers "0.0.6") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "10ir7qkpn56v3d0x8yx1y62m9mfjg6jq7zyczhl1j6vyrmw08v3h") (yanked #t)))

(define-public crate-rced-0.0.7 (crate (name "rced") (vers "0.0.7") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0f1a1g6q0c2dd764a85i0akd0fzb6kvz5bsg8lwfh4aw3kmg1pc4") (yanked #t)))

(define-public crate-rced-0.0.8 (crate (name "rced") (vers "0.0.8") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1lqqk8nw7ibzr87zw30irq7xhkn8jyl370idmjn9cvy1rvbc1g01")))

