(define-module (crates-io rc ka) #:use-module (crates-io))

(define-public crate-rckad-0.0.1 (crate (name "rckad") (vers "0.0.1") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "042n6kb48hkzrasvw23sip288fm253d8pgbk24yras17i4b70z0s")))

(define-public crate-rckad-0.0.2 (crate (name "rckad") (vers "0.0.2") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "0d1vnkyz1mxa3xfardznm4kz26qk5cwv1kycqk87ddjr78v5ms42")))

(define-public crate-rckad-0.0.3 (crate (name "rckad") (vers "0.0.3") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "0vf48zq4gs0kqvsrakbi3x6mnl72njhllx1kv6n6s6ch2n3jc7hn")))

(define-public crate-rckad-0.0.4 (crate (name "rckad") (vers "0.0.4") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "0bkrqh685103ml35w0lm121n2l32h3cvddw4rgbh8kbqs9rbkb8j")))

(define-public crate-rckad-0.0.5 (crate (name "rckad") (vers "0.0.5") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "107pss06mdhzk6hngpja9a3r6armkcfja3171rvp692mhczrknkx")))

(define-public crate-rckad-0.0.6 (crate (name "rckad") (vers "0.0.6") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8") (default-features #t) (kind 0)))) (hash "1925cfn4lm1i039s9hr8i5yay1f703frgwdypvwsp12zzwj43y2j")))

(define-public crate-rckad-0.1 (crate (name "rckad") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "blake3") (req "^0.3") (kind 0)) (crate-dep (name "postcard") (req "^0.5") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (kind 0)))) (hash "1bp4p339bl6anh0awf7y197m0s162lh3ja26pxzxa5zhz5bzk5n9")))

