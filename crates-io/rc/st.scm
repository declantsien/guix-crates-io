(define-module (crates-io rc st) #:use-module (crates-io))

(define-public crate-rcstr-0.1 (crate (name "rcstr") (vers "0.1.0") (hash "1q34hlxgpngn62jgqq8s9ycr3jb5igx8w8abpqbfffcqzzgs6ann")))

(define-public crate-rcstring-0.1 (crate (name "rcstring") (vers "0.1.0") (deps (list (crate-dep (name "rlibc") (req "*") (default-features #t) (kind 0)))) (hash "1gnfgjdkkjkvjfp48rswbv8si50xghkk74jzafjz3b5f45w8jx4w") (yanked #t)))

(define-public crate-rcstring-0.1 (crate (name "rcstring") (vers "0.1.1") (hash "1nijrvgp4jarshb3dc4qjpgw9hx4xf703h5vrlgcxh2q6qvdx8ww") (yanked #t)))

(define-public crate-rcstring-0.1 (crate (name "rcstring") (vers "0.1.2") (hash "1ji38sh4vhlb8f7vsvv14r3mnv2x3a08ir5fv2p842rgark9glmn") (yanked #t)))

(define-public crate-rcstring-0.2 (crate (name "rcstring") (vers "0.2.0") (hash "0w3mg0wz0hmkd05dl8rj5411w4jyi0q3nr4jsr17rmfifwids472")))

(define-public crate-rcstring-0.2 (crate (name "rcstring") (vers "0.2.1") (hash "18q161xfcrdc2j3mn2cszjxn8s7ad2bf2b1gwhx0nz8axml1x1rv")))

(define-public crate-rcstruct-0.1 (crate (name "rcstruct") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hld8xhjdxg0i5jbp6b07pdzcz772rgdp3q9igr7r4fdaz1vpg0a")))

(define-public crate-rcstruct-0.1 (crate (name "rcstruct") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qgn5ympcja9q0kzq57pl3r98yq6sckjn0l84f46msy4p7jpmbxr")))

(define-public crate-rcstruct-0.1 (crate (name "rcstruct") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00apy6mxyhywjffpr80ky4s9pg2cjsjfyd8c55j45p5k3in9sxs3")))

(define-public crate-rcstruct-0.1 (crate (name "rcstruct") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07f374a6iw162qw682iy3rpfxa6zyhzpbjdalyc0m3bjd7rswq81")))

(define-public crate-rcstruct-0.1 (crate (name "rcstruct") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z2pl029h0n8yilnqhny387vy3a05rvsqh101j52kczqc998zjl7")))

(define-public crate-rcstruct-0.2 (crate (name "rcstruct") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1myqsqznp42n7scp8pzsfs3gqqncsm3fjxgsfv0ixzn5dj5ywqf4")))

