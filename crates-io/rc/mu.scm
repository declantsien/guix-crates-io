(define-module (crates-io rc mu) #:use-module (crates-io))

(define-public crate-rcmut-0.0.1 (crate (name "rcmut") (vers "0.0.1") (hash "0vdml8bxsi5ydkrp8y1ph6jfiwagdv87k1jqg147277bxc49acki")))

(define-public crate-rcmut-0.0.2 (crate (name "rcmut") (vers "0.0.2") (hash "1l85wjpg1b1c36cjj584ngnmjk12x01khs0r8i7sbhylfis02lgq")))

