(define-module (crates-io rc ro) #:use-module (crates-io))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0wvm8sz8n96n816zand99abdxak9xpvi6zhwilrfhg57bg2lzcnw")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1ma6hcb35il2zdvyzsi2hs3r9aa8b2qfkjpqxfx3vpvnaxp2qibm")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.0.3") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0g4q8bjlhwrya458myzlykqxdbwy358r57l69dddf0jzsj21a90d")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0yh8j4g6482gxsq69pfd9wvxkrqrjh17225asmd433nmvl1ggiss")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.1.1") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "00d82r3bpnylvar8n30vghhyv5srcqdh17w5bsa5cynszw1cr33k")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.1.2") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "02hl8h9bwzh4bkawfj97szvgk3crbms6idiixi5aghvrrq1v12mw")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.1.3") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "13iw9v22k6y6vwibinkldkgy2frfl1a2iq0bvjqd4jjc6fkzyy9i")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.2.0") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1zy3pzlxyffgi6p07dxl7cb516qmg0jnphwyvcvxzv7s68hc4xww")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.2.1") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0a6ahy2sfc8fhb5qmzpygr2l8r2cw5a25di4z1r85230x5swdk9h")))

(define-public crate-rcron-1 (crate (name "rcron") (vers "1.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "cron") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0cwp7jyg51n6rh4qv55rhknzhgsxspv7i8z0xl44c4xhdfr4wx79")))

