(define-module (crates-io rc ob) #:use-module (crates-io))

(define-public crate-rcobs-0.1 (crate (name "rcobs") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1wzx11ykl8wrjb7914p3w93dbfq8zrvr0gmmpg81wjd70fi1ik27") (features (quote (("std") ("default" "std"))))))

(define-public crate-rcobs-0.1 (crate (name "rcobs") (vers "0.1.1") (deps (list (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0lm206p86ihr2fkxxd2bxjr13k8fsindfhx01blmb0bg6z7c9k86") (features (quote (("std") ("default" "std"))))))

