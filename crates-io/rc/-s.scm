(define-module (crates-io rc -s) #:use-module (crates-io))

(define-public crate-rc-slice2-0.3 (crate (name "rc-slice2") (vers "0.3.1") (hash "0yb37w1knwwslkapn4slpiqva9ks94q3fcbybgmw068q259a0jx5")))

(define-public crate-rc-slice2-0.4 (crate (name "rc-slice2") (vers "0.4.0") (deps (list (crate-dep (name "smallvec") (req "^1.11.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jkizq3987k2lj8q7hqv9xszykmij65x3azy5ldix7lfx3c9rqq0") (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-rc-slice2-0.4 (crate (name "rc-slice2") (vers "0.4.1") (deps (list (crate-dep (name "smallvec") (req "^1.11.0") (optional #t) (default-features #t) (kind 0)))) (hash "0s39hrs2rlaqsr4rrkk5avabw5jb848w9ihhdfkggp2y6c9axm4c") (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-rc-storage-0.1 (crate (name "rc-storage") (vers "0.1.0") (deps (list (crate-dep (name "actix-files") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "actix-form-data") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "storage-list") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sys-mount") (req "^1.5.1") (kind 0)) (crate-dep (name "temp-file") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1gc9bsi6p01inp7sdpam9vgwdj64jnkf486i7y5y6ifgndx3p312")))

(define-public crate-rc-storage-0.1 (crate (name "rc-storage") (vers "0.1.1") (deps (list (crate-dep (name "actix-files") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "actix-form-data") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "storage-list") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sys-mount") (req "^1.5.1") (kind 0)) (crate-dep (name "temp-file") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0xscifka103v29sn55w42wsjd1kkbld1fds9x9fh84v1bh8l36ph")))

