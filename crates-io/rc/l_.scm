(define-module (crates-io rc l_) #:use-module (crates-io))

(define-public crate-rcl_interfaces-1 (crate (name "rcl_interfaces") (vers "1.2.1") (deps (list (crate-dep (name "builtin_interfaces") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "rosidl_runtime_rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde-big-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "02abpdvhdhcx0xkp3kbidv0h42a09qyc6h268hxk2jk9fh9grfa2") (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde" "builtin_interfaces/serde"))))))

