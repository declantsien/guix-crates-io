(define-module (crates-io rc _s) #:use-module (crates-io))

(define-public crate-rc_slice-0.1 (crate (name "rc_slice") (vers "0.1.0") (hash "0ckm5sh2gl9l0c22nskkia42br26wir6r4158vhflr6ag1rjwdyn") (yanked #t)))

(define-public crate-rc_slice-0.2 (crate (name "rc_slice") (vers "0.2.0") (hash "0js6jrx66y5iv6znzs9qhv7jh3r0j33xr1gc0h9bf8zs8915wail") (yanked #t)))

(define-public crate-rc_slice-0.3 (crate (name "rc_slice") (vers "0.3.0") (hash "10n33wb45vaqx3kyjlszc4y8x2ivplc6v1nwzfx4xhsgra27w7xz") (yanked #t)))

(define-public crate-rc_slice-0.4 (crate (name "rc_slice") (vers "0.4.0") (hash "1cflgdqr9zf9iph89yb1q18617l09jwavzmi2f8mxbspfa15a7ds") (yanked #t)))

