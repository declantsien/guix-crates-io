(define-module (crates-io rc ry) #:use-module (crates-io))

(define-public crate-rcrypt-0.1 (crate (name "rcrypt") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "bcrypt") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0763c1vhrd73vm30kmw92s7h8d9b2lxhi8wdvmvm4vlzzwsjfmrz")))

(define-public crate-rcrypt-0.1 (crate (name "rcrypt") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "bcrypt") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "042fhiqd70dn7f4sn9iacjiknvrhza9n4scg1pw5yfmmyz4p0b58")))

(define-public crate-rcrypt-0.2 (crate (name "rcrypt") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "bcrypt") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0zpy75zp0h4wckf5f2npj3xdj2ay458qdyfil0kfind3wfjics2d")))

(define-public crate-rcrypt-0.3 (crate (name "rcrypt") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "bcrypt") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "12li1swdd3s9i75xdr5rx8nhrr3jqww36vg560qhka8ylrwa4gxk")))

(define-public crate-rcrypt-0.4 (crate (name "rcrypt") (vers "0.4.0-alpha.1") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "blowfish") (req "^0.9") (features (quote ("bcrypt"))) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zlkp1gymkij4l8znxfmffnjvwjzdalgazkd71sq4gycbrz221jn")))

(define-public crate-rcrypt-0.4 (crate (name "rcrypt") (vers "0.4.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "blowfish") (req "^0.9") (features (quote ("bcrypt"))) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "07ifdm6kl16zj5wr1v6bi2w4vqd7zkk4c122yb33qlhb5yzw6w4w")))

(define-public crate-rcrypto-0.1 (crate (name "rcrypto") (vers "0.1.0") (hash "1iqwgdnm1vvcyky2ml0yza0v1457i7ws34v0z4v49bnwnc554gp6")))

(define-public crate-rcrypto-0.1 (crate (name "rcrypto") (vers "0.1.1") (deps (list (crate-dep (name "rmath") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1jbi77kaxxgxb4yhlg3wlj6p2v1iz0nmpgxil4yfyhyqzfrinfmp")))

(define-public crate-rcrypto-0.1 (crate (name "rcrypto") (vers "0.1.2") (deps (list (crate-dep (name "rmath") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "039afnjhg0rlhk3hilwipcjfw6g56mr22dh24kagpigs56pvhxxk")))

(define-public crate-rcrypto-0.2 (crate (name "rcrypto") (vers "0.2.0") (deps (list (crate-dep (name "rmath") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1mjkf59gbv04397rhywbn4jn4n0m3dj1dyyr9v8wayvj2cj7z6w6")))

