(define-module (crates-io rc #{5-}#) #:use-module (crates-io))

(define-public crate-rc5-cipher-0.1 (crate (name "rc5-cipher") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1na77is4i95ciiiysswmjgvqll8raix2xffjrpfzsy4mpkh9bm3d")))

(define-public crate-rc5-cipher-0.1 (crate (name "rc5-cipher") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1gvwnlp8acgl5mm087xk6mlz9b0ydxsxmh012rs7whxn2fh13ryc")))

(define-public crate-rc5-cipher-0.1 (crate (name "rc5-cipher") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "19zqipmzbx6p9iqq2wsdmn01cm5q7zp7ba1356rkgyb1giamx6w7")))

(define-public crate-rc5-rs-0.1 (crate (name "rc5-rs") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "secrecy") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "01ds4kkyzpbqz80jaiadsk2jwji95cksqlbadm6w2cx698cvma7x")))

(define-public crate-rc5-rs-0.1 (crate (name "rc5-rs") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "secrecy") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qwvaiyq5va7cj5i67f26bn0wqd7ynp3szgsnfxvkrv4v1c7al92")))

(define-public crate-rc5-rs-0.1 (crate (name "rc5-rs") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "secrecy") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zvynfdjhaqivjmwncv68iv3rhvh7nsiyyihbmcga71dykfbn4y0")))

