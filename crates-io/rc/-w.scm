(define-module (crates-io rc -w) #:use-module (crates-io))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.0.0") (hash "06hif5jyrasvzbg192c0521c4266j9jfhr3f48w4qicfp9zd8rw0")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.0") (hash "1akbi27vknab7l639zzsinng7j57jzr7ry019vbk6rqxip3q7p1q")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.1") (hash "0ygag6xb5awysyqf7hxsvqfixlk8k215r35xckn9s2y4cn8p0wdl")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.2") (hash "0b96crshidcq69vwzflza8wq7cin4az6y8icbd8iq3c6fb7zn04n")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.3") (hash "0ndswry8w7s62h9vl3wxyyshvlqpr23zjkv16p31qvyv1zkqp9f8")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.4") (hash "0mqdcmz1n5y2ikh656jcy0wnard5w9gs17459yah6ig9skh8ljyx")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.5") (hash "0h85a5ws5dh116pcf2nrsm3axvp74l0zxsj4bhfpn0v2bgdlkbvh")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.6") (hash "019yd9xa93lvmjjkm25hxyxg5b76avsl0qcwiaw6fllbdbp36x6r")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.7") (hash "1mlm1kapph2ckb6niwcspnrpggv52h8fcnjcwgxvin310wrqy62h")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.8") (hash "18c781kv6k3439s7yk3jsr5s1pyc0r2bm4fw21dxnpx9aalk8zzs")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.9") (hash "1faxxrfk0p2vnvzdb68050r40d8lbg88z9i01rs2aasdrszjpcd2")))

(define-public crate-rc-writer-1 (crate (name "rc-writer") (vers "1.1.10") (hash "0j711c6ja8mr3c0y53wdcv4sjpdh87ay37l8pa62vwi98b3lk06a")))

