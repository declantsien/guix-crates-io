(define-module (crates-io rc t_) #:use-module (crates-io))

(define-public crate-rct_derive-0.1 (crate (name "rct_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.5") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0zx5854mpzzhhbcpxnb4xgwws8xgp2yi1kpcsnynqi7xnxmhs9b0") (features (quote (("default"))))))

