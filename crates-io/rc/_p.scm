(define-module (crates-io rc _p) #:use-module (crates-io))

(define-public crate-rc_pdf-0.1 (crate (name "rc_pdf") (vers "0.1.0") (hash "0phfqwym6f5bh66i5wwjifd17dl3l49wmaflzbdgya6y6vh3jg73") (yanked #t)))

(define-public crate-rc_protocol-1 (crate (name "rc_protocol") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "0xr7sr0kaw43p03nxw2xjkxlvxfnfs3a2sd0bkqy38xphrafym2p")))

