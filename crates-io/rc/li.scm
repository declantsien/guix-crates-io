(define-module (crates-io rc li) #:use-module (crates-io))

(define-public crate-rcli-0.0.0 (crate (name "rcli") (vers "0.0.0") (hash "00ia57cmyx4pwdj9jqlfq7z0wfrz8h6c1jdnrzqj42gazy3lhv7v")))

(define-public crate-rclio-0.0.1 (crate (name "rclio") (vers "0.0.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)) (crate-dep (name "rprompt") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rtoolbox") (req "^0.0") (default-features #t) (kind 0)))) (hash "09k745h4mn7dkswdjfy0c8hkrfdzg1271rqqxx5qidrnrbb6jfjl")))

(define-public crate-rclio-0.0.2 (crate (name "rclio") (vers "0.0.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)) (crate-dep (name "rprompt") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rtoolbox") (req "^0.0") (default-features #t) (kind 0)))) (hash "02arf5v3qllyfkl3ayhnyx6fw61xf2zjbnij66vmx5mbn88gyyja")))

(define-public crate-rclio-0.0.3 (crate (name "rclio") (vers "0.0.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)) (crate-dep (name "rprompt") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rtoolbox") (req "^0.0") (default-features #t) (kind 0)))) (hash "1rfqb47n14j8pxgggqlpkz7853jcsrr7zqpch6mgn1riipzyz09p")))

(define-public crate-rclip-0.1 (crate (name "rclip") (vers "0.1.0") (hash "0jmqrm0rvqzyqz7vfwbmn1f24wb6nxq6k7b8yw5m5gd7z0df089q")))

(define-public crate-rclip-0.2 (crate (name "rclip") (vers "0.2.0") (hash "16rzzpdl87a8hfjcfifaahc0gkpa1a0781q73l9m2i2bcmdwzs78")))

(define-public crate-rclip-cmd-0.0.1 (crate (name "rclip-cmd") (vers "0.0.1") (hash "0mz09y8d2gpfhd15ivbp3vjklbj47gkpzjm3zlgdmvbg0ha5r5w0") (yanked #t)))

(define-public crate-rclip-cmd-0.0.2 (crate (name "rclip-cmd") (vers "0.0.2") (hash "13q5fzxrivv6mwf1wxdirxlzmqm642saxhijwvp2p10d4w1ck5my") (yanked #t)))

(define-public crate-rclip-cmd-0.0.3 (crate (name "rclip-cmd") (vers "0.0.3") (hash "07rh8lflyhm19i41495l90zma7yahj4af77hskscbpjxm5lp36x7") (yanked #t)))

(define-public crate-rclip-cmd-0.0.4 (crate (name "rclip-cmd") (vers "0.0.4") (hash "1cahfrvny6i3s4855z4f1nlgfmp5fip4vygwss70krna2pnsb2a1")))

(define-public crate-rclipd-0.1 (crate (name "rclipd") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^0.9.0") (features (quote ("xfixes"))) (default-features #t) (kind 0)))) (hash "0425jjb339g4vxh36n7a3ga9i3kvflgi937grs9jmc65ndbz9qk0")))

(define-public crate-rclipd-0.1 (crate (name "rclipd") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^0.9.0") (features (quote ("xfixes"))) (default-features #t) (kind 0)))) (hash "1b16v14vv5hpj1vmbdy0j905rk91qvh1z3mn0b088iqkf6a69xc5")))

(define-public crate-rclipd-0.2 (crate (name "rclipd") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^0.10.1") (features (quote ("xfixes"))) (default-features #t) (kind 0)))) (hash "1as9fz8nj5kkid5mxkyq502zkghmk9lg9n3xkz4z4ck5363bllzz")))

(define-public crate-rclist-0.0.1 (crate (name "rclist") (vers "0.0.1") (hash "1l0fik737nqwqhad1rajj9svpqg6wixnn0ahql1xavk08vdp01xx")))

(define-public crate-rclite-0.1 (crate (name "rclite") (vers "0.1.0") (hash "1xfdx5ingw5ssj7d20pa1ws97832cab6mvcmflhdn7isn0h6lcjc") (yanked #t)))

(define-public crate-rclite-0.1 (crate (name "rclite") (vers "0.1.1") (hash "137q3bd49q95f5p2rvmwg6sk6f2sivibgs5lcphplq3d9ka3ll62") (yanked #t)))

(define-public crate-rclite-0.1 (crate (name "rclite") (vers "0.1.2") (hash "06b1h89psbq5y4lkb051alm5j8ah8fj1rchv9ygbq2jld78blghh") (yanked #t)))

(define-public crate-rclite-0.1 (crate (name "rclite") (vers "0.1.3") (hash "1vycl7bfcc6zl4vhfca0lx4xwms8z8kgr4h3w1hwqabd25g4f9v3") (rust-version "1.47")))

(define-public crate-rclite-0.1 (crate (name "rclite") (vers "0.1.4") (hash "0kfksvwamb5sb3jm8m1zyi6p94hc0g5d1fs7bhyrmjh0i4y6bwm7") (rust-version "1.47")))

(define-public crate-rclite-0.1 (crate (name "rclite") (vers "0.1.5") (hash "13jpjzlz167qxfxm1b67rd936cmypsr2d5awg73qf5mw2h79rrdl") (features (quote (("small")))) (rust-version "1.47")))

(define-public crate-rclite-0.2 (crate (name "rclite") (vers "0.2.0") (hash "10sqfrp0lsmjq82xq29gnxvk025mjhykyn4h1ff62qayfy25nhcc") (features (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (rust-version "1.47")))

(define-public crate-rclite-0.2 (crate (name "rclite") (vers "0.2.1") (hash "0my24yzwnnzh4xk694zyg9fw9fywbnzm87hsr9f9srp9llprxngl") (features (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (rust-version "1.47")))

(define-public crate-rclite-0.2 (crate (name "rclite") (vers "0.2.2") (deps (list (crate-dep (name "branches") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fjphcjn4ijmimqif4ha3y140d77k06294yn9iqw4ly3siid8cg4") (features (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (rust-version "1.47")))

(define-public crate-rclite-0.2 (crate (name "rclite") (vers "0.2.3") (deps (list (crate-dep (name "branches") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "1j1vhnd5wnacb3w86hnrsaijgxi8fld3cxwkrfa03d2pgn8a4s4j") (features (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (rust-version "1.47")))

(define-public crate-rclite-0.2 (crate (name "rclite") (vers "0.2.4") (deps (list (crate-dep (name "branches") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 2)))) (hash "1fa379adiwincgywigi341zd969d3459ljq71n5ymwwficp0r7zf") (features (quote (("usize-for-small-platforms") ("default" "usize-for-small-platforms")))) (rust-version "1.47")))

