(define-module (crates-io x5 #{01}#) #:use-module (crates-io))

(define-public crate-x501-0.0.0 (crate (name "x501") (vers "0.0.0") (hash "1n2bv83k10vy6r3g55zira10q42qcc51k8jdpnnma3sacz2zpcn4") (yanked #t)))

(define-public crate-x501-0.1 (crate (name "x501") (vers "0.1.0-pre.0") (deps (list (crate-dep (name "der") (req "=0.6.0-pre.1") (features (quote ("derive" "alloc" "oid"))) (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1cxf8wf16hrg3awzb6vrrm2v7ymdd4vpxds5p92jmizjgrc30gja") (rust-version "1.56")))

