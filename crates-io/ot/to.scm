(define-module (crates-io ot to) #:use-module (crates-io))

(define-public crate-otto-0.1 (crate (name "otto") (vers "0.1.0") (hash "1h6k3k0zv7jgrpac0y36c7098f1rlly9lyx6239ghm7idv54m35l")))

(define-public crate-otto-test-0.1 (crate (name "otto-test") (vers "0.1.0") (hash "0qr5s6prng5vxzgwk66h6fv4s7wv61k2milld9kyvawi8vgcfqka")))

(define-public crate-otto_vec-0.1 (crate (name "otto_vec") (vers "0.1.0") (deps (list (crate-dep (name "otto_vec_derive") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0kvg844jazxg33qknmbbsmi9qircp03h0q1vxji0bgyl961aiqfd")))

(define-public crate-otto_vec_derive-0.0.1 (crate (name "otto_vec_derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.42") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0c6vv3iqvfihj1qx4daxilbflklwkkawjbmbnbakry4z5p199h3r")))

