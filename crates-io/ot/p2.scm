(define-module (crates-io ot p2) #:use-module (crates-io))

(define-public crate-otp2-0.0.1 (crate (name "otp2") (vers "0.0.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "unix-time") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "03va6k2gamxfj3nrrf68llbqlxypaijyj53xsrdz1f2pr9jp6qic")))

