(define-module (crates-io ot -r) #:use-module (crates-io))

(define-public crate-ot-rs-0.1 (crate (name "ot-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "185f5r46djrhnicldnm2jdfxdwr7xhbbaaw52i12f9sbhckvgp4z")))

