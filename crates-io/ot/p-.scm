(define-module (crates-io ot p-) #:use-module (crates-io))

(define-public crate-otp-rs-0.1 (crate (name "otp-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0r63vwrh7ri3bwm9h4jyzlp96bklnsnfmzzadz96cs48d57mljn5") (yanked #t)))

(define-public crate-otp-rs-0.1 (crate (name "otp-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "15ha43fqsb1v8px2snhqd2k871n9fgfhxqm5xbcma81g5my2dysm")))

(define-public crate-otp-simple-0.1 (crate (name "otp-simple") (vers "0.1.0") (deps (list (crate-dep (name "hmac") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "1bqp3bbj8jnvwj8bpdkbk5gy29h8kgl9vhc49248gw91cv2cynpk")))

