(define-module (crates-io ot he) #:use-module (crates-io))

(define-public crate-othello-0.1 (crate (name "othello") (vers "0.1.0") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "build_const") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "013fqs36ys4v2msd132syazz90b5xcwh5jrnvxzsrag3wr931z7q")))

(define-public crate-othello-0.1 (crate (name "othello") (vers "0.1.1") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "build_const") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "1mfa8m3h4yg8yib7cwj4jjpqxjfpq9akp7bhva51a5pk11brgbva")))

(define-public crate-othello-0.1 (crate (name "othello") (vers "0.1.2") (deps (list (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bitintr") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "build_const") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "13qj5gxj1gjgw5lsi8v3d5wln5xxiszb4ypq6jgwnb3l6d43mykm")))

(define-public crate-othello-agent-0.1 (crate (name "othello-agent") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rq5ahi6g4gykky8srshclbi0hycdjm8wbj25hr0zc3pybahjax9")))

(define-public crate-othello-agent-0.1 (crate (name "othello-agent") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15i88p2y26figb0rfw90h23p12hc0v6q8qn5ylbmyfhrs3x5f5hc")))

(define-public crate-othello-agent-0.1 (crate (name "othello-agent") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0s6s9bvmcpwhscngzlzndyw7da7vz8b13wy52qn4fpw4scgn43ix")))

(define-public crate-othello-cli-1 (crate (name "othello-cli") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wn6w6yqizbl56ql2f6cfap9ihjnp3cv9b97cvqgd42bzlxncmfq")))

(define-public crate-othello-cli-1 (crate (name "othello-cli") (vers "1.0.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rbmwvzcjsiglywzxm6qc4r1f6a3pf0r2xsrssyxx7rxim9nn3h9")))

(define-public crate-othello-cli-1 (crate (name "othello-cli") (vers "1.0.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0glwlvw0ifi2kgw2r4ry4i7j6mipm7amgz3vmfzi8xii00gzamka")))

(define-public crate-othello-cli-1 (crate (name "othello-cli") (vers "1.0.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "06hf809n81kp2cj5hy810ifiw1r9fxv11jlkipgw69nrmmarc9ik")))

(define-public crate-othello-cli-1 (crate (name "othello-cli") (vers "1.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1mhsl4p37zk97dxy71z4z67zqw5175r8ndxcx1lkxc3b61swy1bq")))

(define-public crate-othello-cli-1 (crate (name "othello-cli") (vers "1.3.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0yv83ciddppyvv2yy1w0976whxy0ghx7xpsbcihf1shcwqwbfly3")))

(define-public crate-othello-cli-1 (crate (name "othello-cli") (vers "1.4.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1lmm999k9y3p32aiqhgml346zm2c5sb7jnkvbbhxv6xwqfy6qs6r")))

(define-public crate-other-lib-name-0.1 (crate (name "other-lib-name") (vers "0.1.0") (hash "0201il9wvr1j8mczyjkxyc0mi1jjvnr3cqq719v2l4fva4xdhkp8")))

(define-public crate-other-pocket-0.1 (crate (name "other-pocket") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.13.7") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "mime") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0j3q7qc4ajpxqrfpffrghf1bpk95bw54wgazz59g9q39j0gm01c0")))

(define-public crate-otherwise-3 (crate (name "otherwise") (vers "3.1.1") (hash "1xpnminwkk9gpfzsmwdaqjrsgwfp0vrpmlqj52zahxkik6p31cax")))

