(define-module (crates-io ot ps) #:use-module (crates-io))

(define-public crate-otps-0.1 (crate (name "otps") (vers "0.1.0") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1ng5a7pdwdsq38i6pygh0p2d9v72vw5jmjbq4gm709hdvlbkii0s")))

(define-public crate-otps-0.1 (crate (name "otps") (vers "0.1.1") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1gqyk2f7xs9xsmxjvpcl6a2sn41ky0avxqjq1n3171mx7klkxcv9")))

(define-public crate-otpshka-1 (crate (name "otpshka") (vers "1.0.0") (deps (list (crate-dep (name "ring") (req "^0.17.0-alpha.7") (kind 0)))) (hash "0kc7g4pw23r5crfa19da7wzd6afc17gw5xzsnf12awbn9qiifw76") (features (quote (("std"))))))

(define-public crate-otpshka-1 (crate (name "otpshka") (vers "1.0.1") (deps (list (crate-dep (name "lhash") (req "^0.3") (features (quote ("sha1" "sha256" "sha512"))) (default-features #t) (kind 0)))) (hash "09q93z5ajj9fhhki5qyspsr30if486mnvzpy5i35x4zkw16p29q9") (features (quote (("std")))) (yanked #t)))

(define-public crate-otpshka-1 (crate (name "otpshka") (vers "1.0.2") (deps (list (crate-dep (name "lhash") (req "^1.0.1") (features (quote ("sha1" "sha256" "sha512"))) (default-features #t) (kind 0)))) (hash "19s9v2rw44dln0sj04v7car52ciysnpqhbdn74hrgip6k6msq442") (features (quote (("std"))))))

