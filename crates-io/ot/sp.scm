(define-module (crates-io ot sp) #:use-module (crates-io))

(define-public crate-otspec-0.1 (crate (name "otspec") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "otspec_macros") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yfpinni4a1zj4fn1czsyf6pkkki1kn03i0pinzc4nfhxsrk9b2r")))

(define-public crate-otspec_macros-0.1 (crate (name "otspec_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.60") (default-features #t) (kind 0)))) (hash "13ral68v137xl027afklxr4i1finwmnx61a4hn9l55csnxfas1q2")))

