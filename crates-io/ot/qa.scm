(define-module (crates-io ot qa) #:use-module (crates-io))

(define-public crate-otqa-1 (crate (name "otqa") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1k8i5szw27f8766jxa17wfr9k8dgdijhzq6v1w6ylraj2x7mknw6")))

(define-public crate-otqa-1 (crate (name "otqa") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "115ih641mmy25s49yvfh81azwhsmglhhhbwjfi165wazhy6vqg8p")))

(define-public crate-otqa-1 (crate (name "otqa") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "09079b8by3f56y3qgbin2jqijk36ixr91i25aaqr70av92qdcvl4")))

