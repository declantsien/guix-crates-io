(define-module (crates-io ot or) #:use-module (crates-io))

(define-public crate-otoroshi_rust_types-0.0.1 (crate (name "otoroshi_rust_types") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "extism-pdk") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "02h2kb3vs2qhfci3mpjwn7rhp2b9bdb0q6k0h3byqzcxwljf59mw")))

