(define-module (crates-io ot _u) #:use-module (crates-io))

(define-public crate-ot_utils-0.1 (crate (name "ot_utils") (vers "0.1.0") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1ga2bj47l8i4akw1j0dq4ikxmmplm8nky6fhbqyvskl4871g23my")))

(define-public crate-ot_utils-0.1 (crate (name "ot_utils") (vers "0.1.1") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "08valyn6h8nv5a332b4is7p00bia0dsgsb1da75yvhg4wijc9yhw")))

(define-public crate-ot_utils-0.1 (crate (name "ot_utils") (vers "0.1.2") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "141xxnnavnhivx3s1f6xasd6nxmwkhlaaihc0v03qslx37bkasxl")))

(define-public crate-ot_utils-0.1 (crate (name "ot_utils") (vers "0.1.3") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1r5whndm9qs1md159xpgjnnpqkm6siq52bwdr8lj7i1fv162xah7")))

(define-public crate-ot_utils-0.1 (crate (name "ot_utils") (vers "0.1.4") (deps (list (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "000x22r5xxvjakcxgvjb4vlb8s39ngqlrclpmi7w16m9fy2hdq7s")))

