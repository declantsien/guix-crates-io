(define-module (crates-io ot m8) #:use-module (crates-io))

(define-public crate-otm8009a-0.1 (crate (name "otm8009a") (vers "0.1.0") (deps (list (crate-dep (name "embedded-display-controller") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vfwwr6gxgx8fv7lriw6mx4zkc4ix0x4v1d0byg41r1xx2i5jq75")))

