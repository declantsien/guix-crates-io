(define-module (crates-io mh -z) #:use-module (crates-io))

(define-public crate-mh-z19-0.1 (crate (name "mh-z19") (vers "0.1.0") (hash "1njrlh24y9ql7isih7f05gy3k3k32xjbdypi25cs0c88cv3r3by9")))

(define-public crate-mh-z19-0.2 (crate (name "mh-z19") (vers "0.2.0") (hash "1lra0bn7s9z038hxpa4104h5ydmwsa5lg05c18mnzpbnc60miyq3")))

(define-public crate-mh-z19-0.2 (crate (name "mh-z19") (vers "0.2.1") (hash "04inwgk7npkra0lj5lsj8vdwsmrpjikqgf4lf62lnrffya8f4fvj")))

(define-public crate-mh-z19-0.3 (crate (name "mh-z19") (vers "0.3.0") (hash "1324dh9vmmd6rjgrlf1fbh97hlq0ckw7m9mr0smbcikljshxgm6z") (features (quote (("std") ("default"))))))

(define-public crate-mh-z19-0.3 (crate (name "mh-z19") (vers "0.3.1") (hash "067jpm2xhjb279l42yz4qf67kp9a7kafp4qrakfqyv6drq9nh2sd") (features (quote (("std") ("default"))))))

(define-public crate-mh-z19-0.3 (crate (name "mh-z19") (vers "0.3.2") (hash "0pargl3n5s12rxd2h7kfmav00xhgbpqcr3r038kjbqyqllr0vn7m") (features (quote (("std") ("default"))))))

(define-public crate-mh-z19-0.3 (crate (name "mh-z19") (vers "0.3.3") (hash "0a7a5s7aj78wcjyrz6g4f20xij1fpa06fcg747aip9nxlhl05qh8") (features (quote (("std") ("default"))))))

(define-public crate-mh-z19c-0.1 (crate (name "mh-z19c") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1zy641ia6dmzb19phlhdw0mjlj0qy7phaapw3v8cwnmgq6jqiygb")))

(define-public crate-mh-z19c-0.1 (crate (name "mh-z19c") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0f7js7n4vgfvgfhgwlsqp6dmyg40flqqpxzjh046848rhha1q7s6")))

(define-public crate-mh-z19c-0.2 (crate (name "mh-z19c") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "17i5ax8bfl6ncp1szj3lmabdvccsjaqrxr0x0ghb50izpl68dvnc") (features (quote (("std"))))))

(define-public crate-mh-z19c-0.3 (crate (name "mh-z19c") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "11061lrqf2vw4mc4p9s0328bf466rkm3mhl273fkzl4cjxdlgs91") (features (quote (("std"))))))

(define-public crate-mh-zx-driver-0.1 (crate (name "mh-zx-driver") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "prometheus") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "prometheus_exporter") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1hys611kq7x5bly8mfppng245bvhqisjikjkyh93chhxpyqmgp9y") (features (quote (("examples" "linux-embedded-hal" "prometheus" "prometheus_exporter" "env_logger" "log"))))))

(define-public crate-mh-zx-driver-0.2 (crate (name "mh-zx-driver") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "prometheus") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "prometheus_exporter") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0na80vykmflask6g0dhyzv3nglpgrshdn76izg4mdn3r6j35zb20") (features (quote (("examples" "linux-embedded-hal" "prometheus" "prometheus_exporter" "env_logger" "log") ("default") ("async" "futures"))))))

