(define-module (crates-io mh gi) #:use-module (crates-io))

(define-public crate-mhgit-0.1 (crate (name "mhgit") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0f2q1b81sipx9l781qmjmbv9jd08dcspfcgm6wjfpm1zsqqiv7dk")))

