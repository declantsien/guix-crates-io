(define-module (crates-io mh te) #:use-module (crates-io))

(define-public crate-mhteams-0.1 (crate (name "mhteams") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("blocking" "json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pbgq8l0x6vpk3s0hzsqnh29a8z1gdyhxcd11nmh0adwlbkbbi74")))

(define-public crate-mhtemplate-1 (crate (name "mhtemplate") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02js8cik8r9jy0ayxpiddwkiil68sqhzbzb2nm43q6092d02vjmz")))

