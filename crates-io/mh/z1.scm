(define-module (crates-io mh z1) #:use-module (crates-io))

(define-public crate-mhz19-0.1 (crate (name "mhz19") (vers "0.1.0") (deps (list (crate-dep (name "err-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)))) (hash "0cn00zsnx1jvd78m7lb5czrqn05ichfss5rhv2n3w5xxffmabrrs")))

