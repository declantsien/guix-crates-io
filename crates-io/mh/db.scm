(define-module (crates-io mh db) #:use-module (crates-io))

(define-public crate-mhdb-1 (crate (name "mhdb") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.111") (optional #t) (default-features #t) (kind 0)))) (hash "1xc18ga8pzx4039v36kbz99fbk0sk4421pp1grr11c986gp7phgz") (features (quote (("std" "serde" "bincode") ("default" "std"))))))

(define-public crate-mhdb-1 (crate (name "mhdb") (vers "1.0.1") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.111") (optional #t) (default-features #t) (kind 0)))) (hash "1ricx58msj88f0n115i69rxq50cfx3n3rri0adlai0bn4jbx0h0c") (features (quote (("std" "serde" "bincode") ("default" "std"))))))

(define-public crate-mhdb-1 (crate (name "mhdb") (vers "1.0.2") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.111") (optional #t) (default-features #t) (kind 0)))) (hash "1glgl2njr520gmdh9fzmgi2lcjilvwncjpncxpf6irjdarid3m2v") (features (quote (("std" "serde" "bincode") ("default" "std"))))))

(define-public crate-mhdb-1 (crate (name "mhdb") (vers "1.0.3") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.111") (optional #t) (default-features #t) (kind 0)))) (hash "1ad6maxmbvpk2x7ijk8a027yw4flfghzvrhqh4nnayl3gr721ph4") (features (quote (("std" "serde" "bincode") ("default" "std"))))))

