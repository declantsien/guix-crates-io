(define-module (crates-io mh _v) #:use-module (crates-io))

(define-public crate-mh_very_own_guessing_game-0.1 (crate (name "mh_very_own_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1bgg6kyaphzcx3afqg3ldj2sc17qgwhpk3r8jz8cqhknisvx625p") (yanked #t)))

(define-public crate-mh_very_own_guessing_game-0.1 (crate (name "mh_very_own_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0h0rcqyrls84spdnm2z3l37gd7kxjdyhpvlc954qmkk7hcj7c35d")))

