(define-module (crates-io li sb) #:use-module (crates-io))

(define-public crate-lisbeth-error-0.1 (crate (name "lisbeth-error") (vers "0.1.0") (hash "0fb85a7q8984s7z5hpniihz23hn9zcwiyn0fjw7kk63hvljqxlhw")))

(define-public crate-lisbeth-tuple-tools-0.1 (crate (name "lisbeth-tuple-tools") (vers "0.1.0") (hash "1g5szwd820kygwa4lpsj7yjy1xdsi7a14akmwpp4d4ibcpmlg68s")))

