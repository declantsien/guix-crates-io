(define-module (crates-io li nm) #:use-module (crates-io))

(define-public crate-linmath-0.0.1 (crate (name "linmath") (vers "0.0.1") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lr33fqfvjarypvjzrrfsfzwknws5qr8l8rfljwly6blwlhdspq2")))

(define-public crate-linmath-0.0.2 (crate (name "linmath") (vers "0.0.2") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "003nirbzaiyjq8syqmyic78min0snb3vyv4dsriksqvmk5sw9cpq")))

