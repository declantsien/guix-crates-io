(define-module (crates-io li uh) #:use-module (crates-io))

(define-public crate-liuhatry_test_crate-0.1 (crate (name "liuhatry_test_crate") (vers "0.1.0") (hash "18dk4hzv6rpgzvmydp2p2mnywx57fcadz5h7rbzzf8k01nvfnvi9")))

(define-public crate-liuhatry_threadpool_example-0.1 (crate (name "liuhatry_threadpool_example") (vers "0.1.0") (hash "1wclpw19sy0qjf0cmigw7cznczfvb3y39sl6cn2d245li8vy4fmq")))

