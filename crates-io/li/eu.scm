(define-module (crates-io li eu) #:use-module (crates-io))

(define-public crate-lieu-0.1 (crate (name "lieu") (vers "0.1.0") (hash "0jkvjq3jgyqbly7ngprjr7d1ipc3fsa99c7935yqxb2d456lal5i")))

(define-public crate-lieutenant-0.0.0 (crate (name "lieutenant") (vers "0.0.0") (hash "03n65h7ha26z6rq7m64lgdqlgvviyxr7nm574wsr08125znkwlgz")))

