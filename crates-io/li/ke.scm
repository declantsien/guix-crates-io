(define-module (crates-io li ke) #:use-module (crates-io))

(define-public crate-like-0.1 (crate (name "like") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1iq9zfjdqknlszsryqmxcgbgnbjb78znqi9znwvrna97dwxbpbay")))

(define-public crate-like-0.1 (crate (name "like") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "0hzaag8v7sz1blv792bwa9k9wga6vnkzg2s0cjnb60k07dkcirbb")))

(define-public crate-like-0.1 (crate (name "like") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1yxg4km2fwq84znxwdwkcahsnszgg53s2rxnndr0g487jcvf0y4d")))

(define-public crate-like-0.2 (crate (name "like") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "05dfi91cn8a52jhqhnl7jd0dl7db8fpdgcmy79402p1vyblihg9f")))

(define-public crate-like-0.2 (crate (name "like") (vers "0.2.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1shng1dkj93k26x73pccig44mvlcpk1vhcdfqfvd6wqi9ik05cdn")))

(define-public crate-like-0.3 (crate (name "like") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0a3yi1kysc8aa2cg3m1ynhs2g9rq4jyvi34bijs0isn8ip7q5v2b")))

(define-public crate-like-0.3 (crate (name "like") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0w0snb3l0p3im5nhyi8s4m8dwr0lsafw99v36khgm8dinbj82wpw")))

(define-public crate-like-shell-0.1 (crate (name "like-shell") (vers "0.1.0") (deps (list (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1964j3bridks5s82rzcalp6kfbc7dakj0ia05i2qsmysbkdzffm0")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.0") (deps (list (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1mrqnxkm73s5vnqsbydisyqdf5d5xa0qv9x65sgbwa8vxhrhf2ac")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.1") (deps (list (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0zvqj2nmb8m3fkfp4hj6x12jgfyqm8y1vxjmm6wd9gd7daw3j6wx")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0xvmkad3fg47djm10jwh5vp5vl95zgd695fpp3v2pqq437fh1746")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0072y2qlp2m8w211lfiigfh7lyck4vwlvb517xnpiss2cbih351z")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0c3gcx11fik5bghqm64vm5hf67xwds5iksxqplgpxx5468qsby5a")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1clvhqqg7v6ihhgg6w5qpfzn7fmbvqxckcrah73wj8v33nf0hqr0")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1nz7aa68ghbvs4lm6nwc3dkrjsa138zr7mbp9jjzcr3xw8mfwjlh")))

(define-public crate-like-shell-0.2 (crate (name "like-shell") (vers "0.2.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0x0h5hj78p9abjng2fg8mv0vjzdw6v4k157vmiciprqyj8wap9sa")))

(define-public crate-likejs-0.1 (crate (name "likejs") (vers "0.1.0") (hash "1sjb30pknacw7z111wl1yzh592v8zq62bxy37c4j0f3n48ic2q0i") (yanked #t)))

(define-public crate-likejs-0.1 (crate (name "likejs") (vers "0.1.1") (hash "1kw2cy413gv82fl9ca7nm5liq4msbavvp87yqx5ikzzky3knzs1n") (yanked #t)))

(define-public crate-likejs-0.1 (crate (name "likejs") (vers "0.1.2") (hash "1qrv6yw8z583bpifn07qs52vwmskdyn3xrjbx6y6nk3mw0j7fp9s")))

(define-public crate-likelove-closures-0.1 (crate (name "likelove-closures") (vers "0.1.0") (hash "1j37b0791nwr9wjw25pzhyazar13i5cp9nfwp3rrlh5hj94kdn13")))

(define-public crate-likely-0.1 (crate (name "likely") (vers "0.1.0") (hash "1xdr1gm1aywy9bv19rjpdbidxwcmrslmisgjcyxndh18fm674xm3")))

(define-public crate-likely-0.2 (crate (name "likely") (vers "0.2.0") (hash "190cds33apb6w2dgzglcphzh4qz87l23rzm2vbg6608ndwb24dwx")))

(define-public crate-likely_stable-0.1 (crate (name "likely_stable") (vers "0.1.0") (deps (list (crate-dep (name "const_fn") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)))) (hash "00ags6bsxr4w7jl04lrx6iycwcyk4k6raz2dc1mpl89bxm6h6s25") (features (quote (("default") ("check_assembly")))) (yanked #t)))

(define-public crate-likely_stable-0.1 (crate (name "likely_stable") (vers "0.1.1") (deps (list (crate-dep (name "const_fn") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)))) (hash "108w5nqr3s30pp59mva95kqbh07xrlbcjp6ad2w7yiynfvpdj0yp") (features (quote (("default") ("check_assembly")))) (yanked #t)))

(define-public crate-likely_stable-0.1 (crate (name "likely_stable") (vers "0.1.2") (deps (list (crate-dep (name "const_fn") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)))) (hash "0sny4jgvix0hyl38d3f210dxppc0gj9w3xniak2851jn3dg21av9") (features (quote (("default") ("check_assembly"))))))

(define-public crate-likemod-0.1 (crate (name "likemod") (vers "0.1.0") (deps (list (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zdn8pv175d0jnsn1q0y3p1fb2zbd7p7p1w46pjann48i5fzipfr")))

(define-public crate-likemod-0.2 (crate (name "likemod") (vers "0.2.0") (deps (list (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0n2117bjkgjj6fxlvz6q6y6wx6dsc4d2srll9y2fp6hzl71v8qka") (features (quote (("async" "futures" "tokio"))))))

(define-public crate-likeness-1 (crate (name "likeness") (vers "1.0.0") (hash "091dk18s2yrdfpbkikzn1rhpqlp38iwfg6r2vq4dr2ysvb97m5h3")))

(define-public crate-likeness-1 (crate (name "likeness") (vers "1.1.0") (hash "16xq33624075a8d072cx8rnzz7vvpjaqkxijv7yda4p7904mv85n")))

