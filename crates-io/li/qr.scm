(define-module (crates-io li qr) #:use-module (crates-io))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1yxiqg87zsbxp2g10zfcyxznc8cm28xmglln6sqlsqmgv7d47lda") (yanked #t)))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "02cb8k9wcrnaf84nfbk5rxc5dpykikr38gyrwxg7caa6d9wscsah") (yanked #t)))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "03hjyjbp1rww9lgnjwgac43zmq8v28c5103habh51zqmkxv6z75l") (yanked #t)))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0czggqw2wgj3x00rhmizjz8lzw0q0qnchipwfvrd1qc1if3xqx54")))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1k21ar7201ql1k58k49qpna6h73kzdr9k5lpgzyivy915mj8d7mn")))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1j436w9c87ymfpw6ibxd1asfg78h8qki7xcw7hg1k8k01r4kjwfi") (yanked #t)))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1kh7gpl2v9d7lyvg1x1r9dn33zfcg2nlvpl7bxkaxyzh3k04bqbl")))

(define-public crate-liqr-0.1 (crate (name "liqr") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "09k1dpjc35xvqb1v6b92zg8sggs5610qc3hb5dv983rp7ml000sx")))

(define-public crate-liqr-1 (crate (name "liqr") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0j0ljc953hcw8cggnfj6n18a5yximhskz78dr74ff4vb2n9i4wsh")))

(define-public crate-liqr-1 (crate (name "liqr") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0s50p2yg590r983rjy84zp5lbhlnysll06zxfqrazlb9lnybjg6s")))

