(define-module (crates-io li pm) #:use-module (crates-io))

(define-public crate-lipmaa-link-0.1 (crate (name "lipmaa-link") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1vl1yg61946g8spa1wd1s0zzb439y30srnfy6mnkfl3yi6pfr0vc")))

(define-public crate-lipmaa-link-0.1 (crate (name "lipmaa-link") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1ybfkhwa2dwk1x6dhc8gxkmxvffhv0108mfr0ms4wjh6v6fyxyyq")))

(define-public crate-lipmaa-link-0.2 (crate (name "lipmaa-link") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "00xdqw6fqzjh4n56w5mld0c46mfnga1anh9sr4d1vklj8ps29wzl")))

(define-public crate-lipmaa-link-0.2 (crate (name "lipmaa-link") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0lqql1814vn52zpnn7xs27s5660apjjrxagbaq15rgghlfzqkmyz") (features (quote (("std") ("default" "std"))))))

(define-public crate-lipmaa-link-0.2 (crate (name "lipmaa-link") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1jzv2rnb6my5s9gq9q91788lvk46xsfkrvwn5f6gc6jczggg1x9n") (features (quote (("std") ("default" "std"))))))

