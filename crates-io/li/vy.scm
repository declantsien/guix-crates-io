(define-module (crates-io li vy) #:use-module (crates-io))

(define-public crate-livy-0.0.1 (crate (name "livy") (vers "0.0.1") (hash "04vb07z4q0h17y8w5xqlzjcrnabd2vdmpz8r8f3s1afjicqisbrn")))

(define-public crate-livy-0.0.2 (crate (name "livy") (vers "0.0.2") (hash "0yafwxvs33rl7l0g0zyj2nr7zf4m6zlmp6vg2lyd3rqnqvp38kss")))

(define-public crate-livy-0.0.3 (crate (name "livy") (vers "0.0.3") (hash "04gr6jx57i7gwghrw8j313vramdm2srhfxbn8lvdjs2i2556ja8d")))

(define-public crate-livy-0.0.4 (crate (name "livy") (vers "0.0.4") (hash "0rfimvxr32vmpxhcx1sy16vxmrixmd8xf3b6gz30bqzq2zin5snw")))

(define-public crate-livy-0.0.5 (crate (name "livy") (vers "0.0.5") (hash "0x88lif60wqm9m89bjnjwm14frq2fzf8kldnk0am1as8nlkl4xia")))

(define-public crate-livy-0.0.6 (crate (name "livy") (vers "0.0.6") (hash "1lvc5pxjcpq6mbrxhczisg28z4p7a4ldbd9ibr2184qvi4nqamyi")))

(define-public crate-livy-0.0.7 (crate (name "livy") (vers "0.0.7") (hash "0kywa79hsrkhdrzkxxlk9jy3v2kmvp0qgilcwhkqrhi0mxkfiphp")))

(define-public crate-livy-0.0.8 (crate (name "livy") (vers "0.0.8") (deps (list (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "14wmr2wmkya4wmrzqwv7v3a5p92lgi2kg122v62xpk5flx66017m")))

(define-public crate-livy-0.0.9 (crate (name "livy") (vers "0.0.9") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1593nf9n1jw9xr2g5kk59azybrfc0rsz895bliafyyz7hkr4vxm2")))

(define-public crate-livy-0.0.10 (crate (name "livy") (vers "0.0.10") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mnv114pbgj5akzfgz2mw049njsf8vqy7x2s1nlxkyr5qhdnsi0h")))

(define-public crate-livy-0.0.11 (crate (name "livy") (vers "0.0.11") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0naicyhg4qfc88gs38y9azp53ilky0h2yh7ssyrrzkix5nzfa01r")))

(define-public crate-livy-0.0.12 (crate (name "livy") (vers "0.0.12") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j744k2gmcisf769zsq3sdvh14fci39szbrgrb799yqlnxaafgrn")))

(define-public crate-livy-0.1 (crate (name "livy") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0npfcnacww318j042057qpwssgrrgz7kcqklw41k4wn2fvqf9m5x")))

(define-public crate-livy-0.2 (crate (name "livy") (vers "0.2.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1djajkf4g9pcspcyvvndzxai36q364v4cj591rw08apn2j6mqxz9")))

(define-public crate-livy-0.3 (crate (name "livy") (vers "0.3.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pmdbnaiic37pc5zqgwhsary3flfxbhw6wlwg4bp7yx568avar6c")))

(define-public crate-livy-0.4 (crate (name "livy") (vers "0.4.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xiin89aqlmjfq875ns5v1hmfgvcr1y82vcqb4msa5h7ssgyy0pd")))

(define-public crate-livy-0.5 (crate (name "livy") (vers "0.5.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zvagz18l4bdhrbv734fk6djh6173djpj8i8j89ad7bhz3hj4gsp")))

(define-public crate-livy_manager-0.0.0 (crate (name "livy_manager") (vers "0.0.0") (hash "19vd1gyzy5jqn4zd4klk402pw9vxyg1acwwllr62hlb37g1dapwy")))

(define-public crate-livy_manager-0.1 (crate (name "livy_manager") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "ldap3") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "livy") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "18qjrfh69n0zs14swnhcqkrvvnsscn0xf4p7fbs0ci67m038iq1n")))

(define-public crate-livy_manager-0.2 (crate (name "livy_manager") (vers "0.2.0") (deps (list (crate-dep (name "argparse") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ldap3") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "livy") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "params") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "persistent") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "router") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "02ckhbpw1bdvl38pgm16875hd7ga9wim6spc3qa7jwpi6sakm3cj")))

