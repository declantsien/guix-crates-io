(define-module (crates-io li ut) #:use-module (crates-io))

(define-public crate-liutongshuo_decoding_macros-0.1 (crate (name "liutongshuo_decoding_macros") (vers "0.1.0") (deps (list (crate-dep (name "liutongshuo_decoding_macros_impl") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0dl0ygk9f0zm11qdswcnns03627ww57n96wb0v4nqg7ykp55mb69")))

(define-public crate-liutongshuo_decoding_macros-0.0.1 (crate (name "liutongshuo_decoding_macros") (vers "0.0.1") (deps (list (crate-dep (name "liutongshuo_decoding_macros_impl") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "05a4wxgjh4d3yk2ak9iaw8q86kk2x73c894806dlakgh85xz3w3p")))

(define-public crate-liutongshuo_decoding_macros_impl-0.0.1 (crate (name "liutongshuo_decoding_macros_impl") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "02f6c10vmavvaqb2z5q8ycpl4zqwpv0raag3a3ciyg91w6j34y0f")))

(define-public crate-liutongshuo_decoding_macros_impl-0.0.2 (crate (name "liutongshuo_decoding_macros_impl") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dniniwjp6318b3zdplr85ail4r98yh62yk92bpm2mi8wss0va3k")))

(define-public crate-liutongshuo_decoding_macros_impl-0.0.3 (crate (name "liutongshuo_decoding_macros_impl") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "0zi7288nq3a586b3vfp7gqp7c62d51bb5fbzk5bbr5zkx280alqd")))

