(define-module (crates-io li ao) #:use-module (crates-io))

(define-public crate-liao-generator-0.1 (crate (name "liao-generator") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1bwsvvlc15n0709pf46y9ma9740v8ffflz0y6327ljgc91310psh")))

(define-public crate-liao-generator-0.1 (crate (name "liao-generator") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1pisyn3v10dbalmvyq6z9dndw3c4ffyy1vgvdv5ig34c0mfmvhqy")))

(define-public crate-liao-generator-0.1 (crate (name "liao-generator") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16imddj6jkk7adw4wkgvipbig0ynj9g7n9zysfqjnldmw0ifgfwx")))

