(define-module (crates-io li sp) #:use-module (crates-io))

(define-public crate-lisp-0.0.1 (crate (name "lisp") (vers "0.0.1") (hash "174rc41g8av0089rlphh6s5r36dyj7acb6vxcal0pkra4zif8h4f")))

(define-public crate-lisp-by-rust-0.1 (crate (name "lisp-by-rust") (vers "0.1.0") (deps (list (crate-dep (name "codespan") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "00kwywp9rk01hsxz2c7yzrzz8kj9zkxp49lwgsd3qx1ibxzsvvr6")))

(define-public crate-lisp-by-rust-0.0.9 (crate (name "lisp-by-rust") (vers "0.0.9") (deps (list (crate-dep (name "codespan") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1pr0xh2hkgidi6ndqsv58qyccmn09lnpana6p2zfbsxpxzml9a0q")))

(define-public crate-lisp-rs-0.0.1 (crate (name "lisp-rs") (vers "0.0.1") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0516hcdi3mqhyppwkawxc495nrxmk177x1150nmlz5i90wp243xl")))

(define-public crate-lisp-rs-0.3 (crate (name "lisp-rs") (vers "0.3.0") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0g1rcm4l2z5qzqxp2winp09zlha7f1w38v4d8b13yccg482kikal")))

(define-public crate-lisp-rs-0.3 (crate (name "lisp-rs") (vers "0.3.1") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0dqcgk1h09n8igha7n3s80m0mldqrlvg05q30zhya6473292wfl5") (features (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3 (crate (name "lisp-rs") (vers "0.3.2") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0isl75q1l86840kvd5b2hcds1fj9nf5blp96gmajna9ysc0gzp8m") (features (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3 (crate (name "lisp-rs") (vers "0.3.3") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "153bngy8sz66ri3038v911xv0psgknaj6qyvhdps128xf98j19d4") (features (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3 (crate (name "lisp-rs") (vers "0.3.4") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zgd38p9w4hn68sng7hdiqqqyg844i2vcjqc043h8844592pfxdf") (features (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3 (crate (name "lisp-rs") (vers "0.3.5") (deps (list (crate-dep (name "linefeed") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1xwl028kp4a9qdkswvwgsmycwb2lk4z00dc3x4xqg5b4bcspz2l0") (features (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp_iter-0.1 (crate (name "lisp_iter") (vers "0.1.0") (hash "1x6cqi27ppd4acs8cp02gqnlciyl25kb1hm39p5icnczviyxmli6")))

(define-public crate-lisp_parser-0.1 (crate (name "lisp_parser") (vers "0.1.0") (hash "0hl0lak9krjkm1qk8gfqv3dgxb28a187m3qn1j5xlbn65q8hv39j")))

(define-public crate-lisp_parser-0.1 (crate (name "lisp_parser") (vers "0.1.1") (hash "1mjyw921bswj8ivi5d0ikrd55g1mfl47d4fddp5y6v0qfrcb6g7h")))

(define-public crate-lisp_parser-0.1 (crate (name "lisp_parser") (vers "0.1.2") (hash "1m4vdnpg9mbqidrqkv5w8nzfkx0amf9vw1fhrxad8mw1gg9m8yxf")))

(define-public crate-lisp_parser-0.1 (crate (name "lisp_parser") (vers "0.1.3") (hash "15s6zn79acmyjm3m0sr5gpqsbw6cra6j1clj8i7kkv53yi3gkymx")))

(define-public crate-lisper-0.1 (crate (name "lisper") (vers "0.1.0") (hash "01dc598jgig0k65y0iks0ak3wbvfnfpyl2zyblif53iby8zlc38w")))

(define-public crate-lispi-0.1 (crate (name "lispi") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "1gqjp4dxa8c6prq38jmsw4523h1swdwyq12m3lqbcypqij8i5wlp")))

(define-public crate-lispi-0.2 (crate (name "lispi") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "0c0rwfwh16llsga02ywf1kg4dlnb9qhcgnmw5j3ryfzwsj8f2r79")))

(define-public crate-lispi-0.2 (crate (name "lispi") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "1i91afga9ymdi4ax1hv50393razzjy450rpiia0zqzgc5r6g1lkv")))

(define-public crate-lispi-0.3 (crate (name "lispi") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "1sx62wkx5a3jb43whcccaqk76mnb66mlad115iiwh85pskm4mpls")))

(define-public crate-lispi-0.3 (crate (name "lispi") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "0hpvkxqg2a846rgnv4zazx449wm9q21wf8r663pqi1ns65xlrwwa")))

(define-public crate-lispi-0.3 (crate (name "lispi") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "17l4cjbnnkmkaic01whd8nmymcl01j9d8mxbj6mf4w1mwzc1yxbb")))

(define-public crate-lispi-0.3 (crate (name "lispi") (vers "0.3.2-1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "192fgc32202k3kcg2p6sj01g0v65l0rn9vikvsf5ji31dd3w8pqw") (yanked #t)))

(define-public crate-lispi-0.3 (crate (name "lispi") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (features (quote ("derive" "custom-bindings"))) (default-features #t) (kind 0)))) (hash "0v91960s3911i9wrd76z6h0i98q8la1x5r54r00jp4vzd8w7xgfk")))

(define-public crate-lispify-0.0.0 (crate (name "lispify") (vers "0.0.0") (deps (list (crate-dep (name "pretty") (req "^0.12.1") (features (quote ("termcolor"))) (default-features #t) (kind 0)))) (hash "0mwyzvyvfg27nnyz8zg3hd96jwn4drwzc6c1l6hifwsf41k410lq") (features (quote (("default"))))))

(define-public crate-lispify-0.0.1 (crate (name "lispify") (vers "0.0.1") (deps (list (crate-dep (name "pretty") (req "^0.12.1") (features (quote ("termcolor"))) (default-features #t) (kind 0)))) (hash "0k2jcy92vbrv1i482f00qyb48m0958akw2p0462l6krigyb3n1zx") (features (quote (("default"))))))

(define-public crate-lispify-0.0.2 (crate (name "lispify") (vers "0.0.2") (deps (list (crate-dep (name "pretty-print") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0qi3l8wmynkld9ksxggz3k3l6z1dvxg7w2p25z924snxbqgq96nb") (features (quote (("default"))))))

(define-public crate-lispify-0.0.3 (crate (name "lispify") (vers "0.0.3") (deps (list (crate-dep (name "pretty-print") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1fj3vpc233c7ln8haxyb1c0na34nac3qips7zy34vlqva12cxggr") (features (quote (("default"))))))

(define-public crate-lispify-0.1 (crate (name "lispify") (vers "0.1.0") (deps (list (crate-dep (name "pretty-print") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0r88g26g0x4v2bka618698z15v7v3yky2c6hcix9jqr1a7a0kppg") (features (quote (("default"))))))

