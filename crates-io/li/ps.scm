(define-module (crates-io li ps) #:use-module (crates-io))

(define-public crate-lips-0.0.0 (crate (name "lips") (vers "0.0.0") (hash "0hiq02k34q7d10nqylfz7ba43r6d3cbsb8f58k2spxbici7jwv4c")))

(define-public crate-lipsi-0.1 (crate (name "lipsi") (vers "0.1.0") (hash "0yxqylnwx5qzxw7g8gsgkixb9z5b9p27b9qd2dv2hjpmssfhr0d9")))

(define-public crate-lipsi-1 (crate (name "lipsi") (vers "1.0.0") (hash "10g5ncmdyzf841wznlkymg8bcd2n971g77f0m66vxawd9d8738nl")))

(define-public crate-lipsi-1 (crate (name "lipsi") (vers "1.0.1") (hash "1vq369fl5knd5lcacrjndl5y839q0v7y7y8rcfh566kiiy1yk216")))

(define-public crate-lipsi-2 (crate (name "lipsi") (vers "2.0.0") (hash "0z5xrvgzz4lahby8v4aicxwlbf4s6kc6y1dzxvmwz1swj9pi1brk")))

(define-public crate-lipsi-2 (crate (name "lipsi") (vers "2.0.1") (hash "1fkijm100nhph1yb8bkin6502ljdvknvz2xqi9ag5cdr4754qicw")))

(define-public crate-lipstick-0.1 (crate (name "lipstick") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19.5") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0r7c5javl6a79aycf1hbwrqw1yjfd2j1l088jxv1r9vsz63xpsjy")))

(define-public crate-lipstick-0.2 (crate (name "lipstick") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (features (quote ("full" "parsing" "printing"))) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0fcp2kk6yfb6dgw4pj408nhb6iv5n8hijzmml19pkjfajmswfx5d")))

(define-public crate-lipstick-0.4 (crate (name "lipstick") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "compiler") (req "^0.4.0") (default-features #t) (kind 0) (package "lipstick_compiler")))) (hash "0jvb9shkmd39xl5yma360ygqzz5rg7fp6vplwl297f11p7vhhjam") (features (quote (("dev" "compiler/dev"))))))

(define-public crate-lipstick_compiler-0.4 (crate (name "lipstick_compiler") (vers "0.4.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.32") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "string-interner") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.82") (features (quote ("full" "parsing" "printing" "visit"))) (kind 0)))) (hash "1wvpa5r4amw6app9j7hbasa9plb6kkmsf7aic5a16c0kan4y5lhj") (features (quote (("dev" "syn/extra-traits"))))))

(define-public crate-lipsum-0.1 (crate (name "lipsum") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "012wy3fv9xjl0xlmz98rnx9laps3zvq4kaf8ialgak5m5gpa4lwn")))

(define-public crate-lipsum-0.2 (crate (name "lipsum") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "19zcdgmvjmmi9vb27f9p5zhlmyq9svaws6z82xmv6zsq5c72cilg")))

(define-public crate-lipsum-0.3 (crate (name "lipsum") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hal592x6g55bavrf4h723xcfp35yn4n1imbhjv00cgs7vrxyww1")))

(define-public crate-lipsum-0.4 (crate (name "lipsum") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.3") (default-features #t) (kind 2)))) (hash "17vc49g8c4g17smyvwjp15jqf1d7wpjyikz7kgjbjw28r0vk5jxw")))

(define-public crate-lipsum-0.5 (crate (name "lipsum") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1asxgwqxkw9y9fz4ckyr51417z4vcfi64qp26kyqs5sf6sw38vw2")))

(define-public crate-lipsum-0.6 (crate (name "lipsum") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.6") (default-features #t) (kind 2)))) (hash "0nlxkz8zjxqmbrxqasr36a5fqn2n33cxy11w0x0a0b6mcx04dr2q")))

(define-public crate-lipsum-0.7 (crate (name "lipsum") (vers "0.7.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "174b9z087h36782nsndspg7bq8y9j2wjadqz2js1mdwq16wgrg97")))

(define-public crate-lipsum-0.8 (crate (name "lipsum") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0sn5k0hgx099x2qdx0xlx8a5b74sfc55qnbyrhnh72baqxqp5vj2")))

(define-public crate-lipsum-0.8 (crate (name "lipsum") (vers "0.8.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "1iq4n3bdzayv7dcq9shmim2xkr4n9awypvqigz2f3b7qlr0kzj49")))

(define-public crate-lipsum-0.8 (crate (name "lipsum") (vers "0.8.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "1x39vkjyr23bgm3s9m9rl56q1gj0rvxqjsb6hr2f8dzky531hid8")))

(define-public crate-lipsum-0.9 (crate (name "lipsum") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0hd5kjd5y3dy7cpfhbjkjp8p2fld7hh7rqmcb5xfcvxdsbr9wplw")))

(define-public crate-lipsum-0.9 (crate (name "lipsum") (vers "0.9.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0r40mf2cwh4fp9pdfcc1n8hjxw05w7galjvb1z23r5pq38jn0s33") (rust-version "1.56")))

(define-public crate-lipsum-cli-0.1 (crate (name "lipsum-cli") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lipsum") (req "^0.8") (default-features #t) (kind 0)))) (hash "1r8k2ygv3m20rl1pjrkhcip2l3s37c6w70sg1w9ky4y3i4m1vlqw") (yanked #t)))

(define-public crate-lipsum-cli-0.2 (crate (name "lipsum-cli") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lipsum") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0z2f9gb70iy2k9cm9gp5ncvshjqspgnrrj1f8y5g1z8vm61x8bsv")))

(define-public crate-lipsum-cli-0.2 (crate (name "lipsum-cli") (vers "0.2.1") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lipsum") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0rgp4sp7phpp2qwvhab67pnad6lfpx5fzzp8rrsz9yzw3qxvv4fk")))

(define-public crate-lipsum-cli-0.2 (crate (name "lipsum-cli") (vers "0.2.2") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lipsum") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0wlr35gb41dkz9l3pm56h975gcaiv65azi0dlnlfi2x92wqi2lzl")))

(define-public crate-lipsum-cli-0.3 (crate (name "lipsum-cli") (vers "0.3.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lipsum") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0xd3sfwmr8pxjvz10m0gjmc9xc52q5irbjh2ca81v10yvh75y43n")))

(define-public crate-lipsum-cn-1 (crate (name "lipsum-cn") (vers "1.0.0") (deps (list (crate-dep (name "dynfmt") (req "^0.1.5") (features (quote ("curly"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "18xfrngrsjrmn2f3xcxcfw5946jh4sp37zw3gklwngicwk0hgg7p")))

(define-public crate-lipsum-cn-1 (crate (name "lipsum-cn") (vers "1.0.1") (deps (list (crate-dep (name "dynfmt") (req "^0.1.5") (features (quote ("curly"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "01sbsh0q0bv3k8prv4ms3lkg7ibl0a6jc0ifnl08pylv70kdh14g")))

