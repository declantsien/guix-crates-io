(define-module (crates-io li ns) #:use-module (crates-io))

(define-public crate-linspace-0.1 (crate (name "linspace") (vers "0.1.0") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "numscale") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fvjnyknrkk66gkmwqgnfnqlbqihxs9c5y5p961nrri7d0n06cnv")))

(define-public crate-linspace-0.1 (crate (name "linspace") (vers "0.1.1") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "numscale") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z31xxkh7ypbra6q275lcrzqc9b8z08zzg6gc3vm1jmiqb5ql37w")))

(define-public crate-linspacers-0.1 (crate (name "linspacers") (vers "0.1.0") (hash "1dmqg0dhdm2g4p7d263lq7k9bvwblmz8xpz5y5vv0dzvrf232lva")))

(define-public crate-linspacers-0.1 (crate (name "linspacers") (vers "0.1.1") (hash "1da95sw1i51ak91vw2x4zwghxm3x99a1vy7yrmlv0798hxn7jz8l")))

