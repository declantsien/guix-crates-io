(define-module (crates-io li fi) #:use-module (crates-io))

(define-public crate-lifi_client-0.0.1 (crate (name "lifi_client") (vers "0.0.1") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1kwpnddngsh9zaxfbfr0a3m5qzl79frgk9vd2kdrnbbcbddfr1c7")))

(define-public crate-lifi_client-0.0.2 (crate (name "lifi_client") (vers "0.0.2") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "ra_common") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1qgza17i8j0yj02bidfiq064l8gq06bkfpfpkli54bnmp93vgv0f")))

