(define-module (crates-io li sh) #:use-module (crates-io))

(define-public crate-lish-0.1 (crate (name "lish") (vers "0.1.0") (deps (list (crate-dep (name "liso") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "shellish_parse") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (features (quote ("signal"))) (target "cfg(unix)") (kind 0)))) (hash "0gnws7r6li3ax8inz81fxyzpy177b5x473xwgbzaqjv3kzngcjfq") (features (quote (("no-unix") ("default"))))))

(define-public crate-lisher_test-0.1 (crate (name "lisher_test") (vers "0.1.0") (hash "07m3dcsq9pbn54qnpifvnwz2hdxgwahrw5rf4y0lpqgjkm3z9ls6")))

(define-public crate-lisho-0.1 (crate (name "lisho") (vers "0.1.0") (hash "13zrqsswm0xgzb76ryx11nl48yk2vpy6l4hfh78g0bqb6jvdz49i")))

(define-public crate-lisho-0.1 (crate (name "lisho") (vers "0.1.1") (hash "10rk48rjayh4jh4sh3y5ad7jm8p7zwzn5y918rnv1wbbwxl7yna3")))

(define-public crate-lisho-0.1 (crate (name "lisho") (vers "0.1.4") (hash "1ahj1n1dqdw89lw5y6dhjx749xbxbyrddq1rfpjzfr22n1624wrq")))

(define-public crate-lisho-0.1 (crate (name "lisho") (vers "0.1.5") (hash "1p443fvh8pv1arsx6pcl1aqkv8j3ybmjl8h8dha1dhwadc24a4ib")))

