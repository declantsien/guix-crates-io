(define-module (crates-io li cc) #:use-module (crates-io))

(define-public crate-licc-0.1 (crate (name "licc") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "1z4jd3qrwqwl751qb297aqvjwl1mmpdrkih21pcpfqklgrxfszi2") (features (quote (("readonly") ("default"))))))

(define-public crate-licc-0.2 (crate (name "licc") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.23") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.112") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "1p1ig0ngqwlapm32mnfsfnql94318g161p064ddkbvdjjfqr8am9") (features (quote (("write") ("default"))))))

