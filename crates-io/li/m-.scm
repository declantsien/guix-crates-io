(define-module (crates-io li m-) #:use-module (crates-io))

(define-public crate-lim-bit-vec-0.1 (crate (name "lim-bit-vec") (vers "0.1.0") (deps (list (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0wv6im6p0b3hi9hs8nj5cwxk001djq536mn4011krksm5w5kcmxa")))

(define-public crate-lim-bit-vec-0.1 (crate (name "lim-bit-vec") (vers "0.1.1") (deps (list (crate-dep (name "list-fn") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0m2g6w3avjkdkpyhk7q63c63g2gbndx1pw4shgngfwl6c5frl47a")))

(define-public crate-lim-bit-vec-0.1 (crate (name "lim-bit-vec") (vers "0.1.2") (deps (list (crate-dep (name "list-fn") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "08m7ncxf4w9y0wb5y04jzx1fp5v14bkxii3jyfi18qcwv20yvi4p")))

(define-public crate-lim-bit-vec-0.2 (crate (name "lim-bit-vec") (vers "0.2.0") (deps (list (crate-dep (name "list-fn") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1vhiyw2v0rw1g6dza0a37h0g113d89vd39zmwxlr438v092n32rx")))

(define-public crate-lim-bit-vec-0.3 (crate (name "lim-bit-vec") (vers "0.3.0") (deps (list (crate-dep (name "list-fn") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1hvzbbdmh5hw4gb4i7d031r51q193nbnb1pqq0mgv07g0igixmnq")))

(define-public crate-lim-bit-vec-0.3 (crate (name "lim-bit-vec") (vers "0.3.1") (deps (list (crate-dep (name "list-fn") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1p6w9mcnp8aknzg3x1p6yxjprnk0mwsb9sh9iy43vfkay7n0w95w")))

(define-public crate-lim-bit-vec-0.4 (crate (name "lim-bit-vec") (vers "0.4.0") (deps (list (crate-dep (name "list-fn") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0plpwb4c4sdfw99vm29pmzkwy4k9hqar59i46wy0v33c57fg2mnz")))

(define-public crate-lim-bit-vec-0.4 (crate (name "lim-bit-vec") (vers "0.4.1") (deps (list (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0yxycq2p9spd083cb818q0jpnxk114xq0jzdjrr8znascjvm18il")))

(define-public crate-lim-bit-vec-0.4 (crate (name "lim-bit-vec") (vers "0.4.2") (deps (list (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "14w6szw8dkfs7b180pp4qsci0j3kcv6z13flfajsmfx6z6a43pvi")))

(define-public crate-lim-bit-vec-0.5 (crate (name "lim-bit-vec") (vers "0.5.0") (deps (list (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "110nglc33vqggqs1np0d23gsz6hb04vp571h7qkpgm6qvdd66zic")))

(define-public crate-lim-bit-vec-0.5 (crate (name "lim-bit-vec") (vers "0.5.1") (deps (list (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "162vszpcjqmqx8gx77l9z42ciwxdnxj280naa19q1af7ghmsndnx")))

(define-public crate-lim-bit-vec-0.6 (crate (name "lim-bit-vec") (vers "0.6.0") (deps (list (crate-dep (name "list-fn") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1nfwk47sdn3szjw654fwzxzfrm90q0m2819lp2jxma772ral6fzc")))

(define-public crate-lim-bit-vec-0.7 (crate (name "lim-bit-vec") (vers "0.7.0") (deps (list (crate-dep (name "list-fn") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1h51gxdck2kg2b0nk3bxvszh9q1c56537s468nzv3190nkncr344")))

(define-public crate-lim-bit-vec-0.8 (crate (name "lim-bit-vec") (vers "0.8.0") (deps (list (crate-dep (name "list-fn") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1v7grrjfx6d1kjaflxf6k2vcyyjbh46hikm6rhkpp3yjcgf7q5x4")))

(define-public crate-lim-bit-vec-0.8 (crate (name "lim-bit-vec") (vers "0.8.1") (deps (list (crate-dep (name "list-fn") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0ljfvqmgfi43s8x64h01r91c1454p86nppn24q1k5yzf2vq54l2y")))

(define-public crate-lim-bit-vec-0.8 (crate (name "lim-bit-vec") (vers "0.8.2") (deps (list (crate-dep (name "list-fn") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "uints") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1lgvkzdmndpw97alklawq7hw2f3v2bspqqjvk5jcydrdy3zgz3rq")))

