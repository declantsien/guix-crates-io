(define-module (crates-io li tu) #:use-module (crates-io))

(define-public crate-litua-1 (crate (name "litua") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.7") (features (quote ("lua54" "vendored"))) (default-features #t) (kind 0)))) (hash "00f4k0r5mh9lkahxj19fimkwlndqgs5akzh2m6pd72ki47mcil3v")))

(define-public crate-litua-1 (crate (name "litua") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.7") (features (quote ("lua54" "vendored"))) (default-features #t) (kind 0)))) (hash "1mv00x8i55aldwkzb679455234p2k6mi5ny68z4bpijqybda6k80")))

(define-public crate-litua-1 (crate (name "litua") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.7") (features (quote ("lua54" "vendored"))) (default-features #t) (kind 0)))) (hash "165wks37yr3gxvxcz8m7f6bvyyrmi296nv0804awf3kcm51randh")))

(define-public crate-litua-2 (crate (name "litua") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.8.8") (features (quote ("lua54" "vendored"))) (default-features #t) (kind 0)))) (hash "0vzklycas43b81zm4vpc84i234hyiw1i528bwa05mdiqfl0x8n6d")))

(define-public crate-liturgical-0.1 (crate (name "liturgical") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0lkqxm8k9lk0rdi00kyn7bzxfcc0m69bs8xjrx554kxflkb9vhi6")))

(define-public crate-liturgical-0.2 (crate (name "liturgical") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "1ncgw24f3dpd99y4ddg4yrizrg1imw4k3qvrq5lyawvi4lk9r6qa")))

