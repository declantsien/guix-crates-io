(define-module (crates-io li nq) #:use-module (crates-io))

(define-public crate-linq-0.0.1 (crate (name "linq") (vers "0.0.1-init") (hash "07gjzhgxai05wjaabb0jdzhwf2pm2g1d7zsvf60icgg37cwk4nms")))

(define-public crate-linq-0.0.1 (crate (name "linq") (vers "0.0.1-preview") (hash "1pg5s47sl0cj2v8xkim647my84c3sqcw6jwdwk7shxjv89f47383")))

(define-public crate-linq-0.0.1 (crate (name "linq") (vers "0.0.1-preview2") (hash "1xhhql972hnbnf9xpp49jc5d1aww0h84w6rsrkd8n50gxzca5ih2")))

(define-public crate-linq-0.0.1 (crate (name "linq") (vers "0.0.1-release") (hash "11xk8d0gpw3dqq83pd9m99fna2whsyjwbkdj7qnjplfxd64mgpjd")))

(define-public crate-linq-rust-0.0.1 (crate (name "linq-rust") (vers "0.0.1") (hash "01psk99m4bapcrlzpp7n19dd2yjcjl2jv5d1hi2f37pxypxlfr7n")))

