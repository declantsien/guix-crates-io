(define-module (crates-io li zw) #:use-module (crates-io))

(define-public crate-lizwpc-0.1 (crate (name "lizwpc") (vers "0.1.0") (hash "1k4176qfkp31nx93nqyb98bhfi5673zk6gf8nl6a40hkzgvr32cm")))

(define-public crate-lizwpc-0.1 (crate (name "lizwpc") (vers "0.1.1") (hash "1gah0w8pbbqbjkrimndm820xhk47wk9jhjdwjc9655g0mr6r8fcb")))

(define-public crate-lizwpc-0.2 (crate (name "lizwpc") (vers "0.2.0") (hash "0sm954dcl0basyzfr617gh8a8k8287rpcx424x09pgyxczd7wb1a")))

(define-public crate-lizwpc-0.2 (crate (name "lizwpc") (vers "0.2.1") (hash "1v4jwl6bgpwiz7msmxzlxcvx2cygl4kdzsw9wh2dl4lbswmqqr8b")))

