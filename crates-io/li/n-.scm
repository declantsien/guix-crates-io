(define-module (crates-io li n-) #:use-module (crates-io))

(define-public crate-lin-alg2-0.1 (crate (name "lin-alg2") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (optional #t) (kind 0)))) (hash "06qcb5fz61harbsv6g6aq3sc1zh0vfyhjjp4z4zv9yyr6yrv5phi") (features (quote (("no_std" "num-traits/libm") ("computer_graphics"))))))

(define-public crate-lin-bus-0.1 (crate (name "lin-bus") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "15l12848k0nspwp69gp59441s032q7yq3blv0xrzja1azwvqcjd8")))

(define-public crate-lin-bus-0.1 (crate (name "lin-bus") (vers "0.1.1") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "140mrqm8aacxngcxsj0mskmhwa8cns5i6gwdxs0dnm6qs7r7iw8n")))

(define-public crate-lin-bus-0.2 (crate (name "lin-bus") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0af3xn4c62aqhzsxilqbd773rq0p8v8g2bg80cpmyph3ck8mwgmd")))

(define-public crate-lin-bus-0.2 (crate (name "lin-bus") (vers "0.2.1") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0g9w8wvyi49kj6pyrv1jb2ij90kpi55sdfq2fkvbdjy622hbwj4d")))

(define-public crate-lin-bus-0.3 (crate (name "lin-bus") (vers "0.3.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1hc8m74rjpsm2g8wy6wk2gxh7mx1mzl4r6nsc5s5y466pgnhxr1d")))

(define-public crate-lin-bus-0.3 (crate (name "lin-bus") (vers "0.3.1") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1qmqla8i83rfs15vj0sg9lvg51zf3s5h9hn7daa4ygxqdam95w6k")))

(define-public crate-lin-bus-0.3 (crate (name "lin-bus") (vers "0.3.2") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1byjy0k2cqygh2azxwc1sr30hsik35zamkm7ha8wn913lbxm2n69")))

(define-public crate-lin-bus-0.4 (crate (name "lin-bus") (vers "0.4.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1f4splrfwdkfnksk6p59y2y3l1iacgx9rw7nl8l7hn32q3rd7b9j")))

(define-public crate-lin-bus-driver-serial-0.1 (crate (name "lin-bus-driver-serial") (vers "0.1.0") (deps (list (crate-dep (name "lin-bus") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)))) (hash "1n7xca3w9bdvszrmmn2p666qc7g8pyjql4garphwfyrmjb225c2g")))

(define-public crate-lin-bus-driver-serial-0.1 (crate (name "lin-bus-driver-serial") (vers "0.1.1") (deps (list (crate-dep (name "lin-bus") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mmpnb904463ig1ifbw7ci7crb3fsaj3va5b59wqsbwv2g2i4n0n")))

(define-public crate-lin-bus-driver-serial-0.2 (crate (name "lin-bus-driver-serial") (vers "0.2.0") (deps (list (crate-dep (name "lin-bus") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)))) (hash "18lyrdngynaidqp3fwx1l4lh5wvh05yfic8y38cw7pms0xw097ah")))

(define-public crate-lin-bus-driver-serial-0.3 (crate (name "lin-bus-driver-serial") (vers "0.3.0") (deps (list (crate-dep (name "lin-bus") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)))) (hash "1818r0gkf3qz9jfvy32a1w1arwz4is8algqfar9cyh0h2kd323cj")))

(define-public crate-lin-state-0.0.1 (crate (name "lin-state") (vers "0.0.1") (deps (list (crate-dep (name "lin-state-macros") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29") (features (quote ("sync" "rt" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "055dyv24rbrxckfwhin1ll0mslh7m66bbn1whjpvg7m3vmnzz2kz") (features (quote (("default" "macros")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("macros" "dep:lin-state-macros"))))))

(define-public crate-lin-state-macros-0.0.1 (crate (name "lin-state-macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0482vly9nyjhpfp6x0mcj4gb0ccvvv8xikj9pc9r7vrv9cdbig1y")))

