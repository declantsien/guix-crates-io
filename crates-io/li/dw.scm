(define-module (crates-io li dw) #:use-module (crates-io))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.0") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1gl4pkyqpxwjf4wyw8imnd8lacwjf03r6i2fpw39zxncmbfmd4ck") (yanked #t)))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.1") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1qjcjs0bf4vk095ifj7kqsbpnzrd1ls44y7vjwwlysg686aaw4gj")))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.2") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0kncbh3phq4vf5lspcrl1549sqrffg824b4rb3kj55r7wyixc8wq")))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.3") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1xgi0gkjnk9311gabrkdhl8ic3n86igsilm97zjbx567xv24db7g")))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.4") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0ldg9qnangmy3598cxx4v2gch8v8x9mjllr9067b8isc7hdmszki")))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.5") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1ighjzlsl4fnzinxk7mnbbl3svgl6fv9i15mapazi7swrapfwn6z")))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "evdev") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ky95nm2v1hc1z8yrdjwbmlisrvcylyd8c1ah22ywa9ph8r5h5h5")))

(define-public crate-lidwatch-0.1 (crate (name "lidwatch") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "evdev") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m40b9rs2q8p53djrhyxmx830dvl4wr95ia3f9k9zyri1asd4fqi")))

(define-public crate-lidwatch-1 (crate (name "lidwatch") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "evdev") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "032ka2gllvf2smdays6rx4zwr18djgcsiigzfjqzag73v4xrv37a")))

(define-public crate-lidwatch-1 (crate (name "lidwatch") (vers "1.0.1") (deps (list (crate-dep (name "evdev") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.11") (default-features #t) (kind 0)))) (hash "0dwz9cbsx2bjhfwbb61p35b0ywywqq0zxk5dd9g4cm2calqigpg0")))

