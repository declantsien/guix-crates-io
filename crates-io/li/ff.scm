(define-module (crates-io li ff) #:use-module (crates-io))

(define-public crate-liff-0.1 (crate (name "liff") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "levenshtein-diff") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid-readable-rs") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1wy9rmjgi86cjgw18rhp43a2p7liky3vscb6akda1n181zsnvb1l")))

(define-public crate-liff-0.1 (crate (name "liff") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "levenshtein-diff") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "snap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid-readable-rs") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0vxlxaqmixpq150k1kigkld0x9yrp9z6ygbsxh5ww9xmgn47bh92")))

