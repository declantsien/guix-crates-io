(define-module (crates-io li dr) #:use-module (crates-io))

(define-public crate-lidrs-0.1 (crate (name "lidrs") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "0.5.*") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "property") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.5.*") (default-features #t) (kind 0)))) (hash "1kyq7nlw9lc8rfb76m70gpdjkafahxparaqs0rs4554h6yiz0bsm")))

(define-public crate-lidrs-0.2 (crate (name "lidrs") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "0.5.*") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "property") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.5.*") (default-features #t) (kind 0)))) (hash "166lbajnga6bl136qwhjr2b78kk424xia87bscxknya0qsc7b9x9")))

(define-public crate-lidrs-0.2 (crate (name "lidrs") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "0.5.*") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "property") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.6.*") (default-features #t) (kind 0)))) (hash "10k1x3gimvv5j8fqdhn4d1dk0hcgcp8n9i35x82q5janpcv3m8c0")))

