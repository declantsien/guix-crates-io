(define-module (crates-io li ge) #:use-module (crates-io))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.0") (hash "0v4bnibla4czy6c2x94zzwwhk35mziqwg3w7z2vciyyjxjq0mk5r")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.4") (deps (list (crate-dep (name "ligen-core") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1srywg4zpkwg92phh4qrgra3fkr4yds3h66xx7smbyrs6d7nmb7l")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.6") (deps (list (crate-dep (name "ligen-core") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "07vdhjlj7hhm7m892yfq8i1lajq62w3wqwg780bcc74iv6ml0aza")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.7") (deps (list (crate-dep (name "ligen-core") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0fismsbgi4plzqins5b4krld26k86fb99xv3ggkrx3hsc5bkrizj")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.8") (deps (list (crate-dep (name "ligen-core") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0fm4js3x525f6xilsj0j76bqgwicm56sgsyc5imzz6j4hw02fxs2")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.9") (deps (list (crate-dep (name "ligen-core") (req "^0.1.9-alpha.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1qhkv8nawwjc4081am1xhrc0jal09mxyghydhxsp88vfpvjqvk60")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.10") (deps (list (crate-dep (name "ligen-core") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "05qhbsbf10ijk9ziq82vim0gq3x1iqhd45dw2l6b4qll0ii1wd1p")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.11") (deps (list (crate-dep (name "ligen-core") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0inai0775ryykk0h4vip7b4pvidyd3w17a2lzhvnmgbxy0zmdkrk")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.12") (deps (list (crate-dep (name "ligen-core") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1prb1x2wh1fs7clk0ykb6jhnk691prykhv6q9jxg19iha3975jic")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.13") (deps (list (crate-dep (name "ligen-core") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "ligen-macro") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "19f6lb3nya7canpaz8ykfpw6p9i7cc7i8jyyp4y3s0wi96vwynci")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.14") (deps (list (crate-dep (name "ligen-core") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "ligen-macro") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1rjd4wla69sa2c0ygbdl6vgxpkqrba55chlm9qzhdf4ikry9vmab")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.15") (deps (list (crate-dep (name "ligen-core") (req "=0.1.15") (default-features #t) (kind 0)) (crate-dep (name "ligen-macro") (req "=0.1.15") (default-features #t) (kind 0)))) (hash "1hmyg0s6m1p49h7xrqnqvjq2qnldiq6b5sjhpn35visrb08ccdqd")))

(define-public crate-ligen-0.1 (crate (name "ligen") (vers "0.1.16") (deps (list (crate-dep (name "ligen-core") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "ligen-macro") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "08jam43r7ak1gmxgzw6wjya8r3v90zwjaagyxh6iqw0ka5qc56mp")))

(define-public crate-ligen-c-0.1 (crate (name "ligen-c") (vers "0.1.0") (deps (list (crate-dep (name "ligen-c-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ligen-c-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16z3amgl3rdh8qmvjwrgybpxzf0xgnkzi73zp9p4sjb65m0wjxyv")))

(define-public crate-ligen-c-0.1 (crate (name "ligen-c") (vers "0.1.2") (deps (list (crate-dep (name "ligen-c-core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ligen-c-macro") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0zjrpspklv2ccybwnldx3xbv6ll95fq1zrpd8hb7m3ckivsc5xw7")))

(define-public crate-ligen-c-core-0.1 (crate (name "ligen-c-core") (vers "0.1.0") (deps (list (crate-dep (name "cargo_toml") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "ligen") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0jnfpszbjiy94aak0w9dssaxk6hl8mj0szlxai62prxgibrl99bx")))

(define-public crate-ligen-c-core-0.1 (crate (name "ligen-c-core") (vers "0.1.1") (deps (list (crate-dep (name "cargo_toml") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1jwqjz4vyar6iz4j8vj6lr1cy4f2mc77jksrlq57nqq0lzy84r7x")))

(define-public crate-ligen-c-core-0.1 (crate (name "ligen-c-core") (vers "0.1.2") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "11vz9qyj0jhf7ccxgiwmkanywr8d576xk56sx430zd7wmf8knz9w")))

(define-public crate-ligen-c-macro-0.1 (crate (name "ligen-c-macro") (vers "0.1.0") (deps (list (crate-dep (name "ligen") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "ligen-c-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d1srxllawdc6aa3kjm7bhn8yrj5816ayb4sn1imspp82qmpkhcj")))

(define-public crate-ligen-c-macro-0.1 (crate (name "ligen-c-macro") (vers "0.1.2") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "ligen-c-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0648z8gsnf23agqrz1jcvrkrpy8d32zfz9jmafbsvr8lg5ih7kl6")))

(define-public crate-ligen-cmake-0.1 (crate (name "ligen-cmake") (vers "0.1.0") (deps (list (crate-dep (name "ligen-cmake-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08kpfx7sy4yd2n6sqsn606w3nr03kfsjy9dmkdln5fprkslmv96s")))

(define-public crate-ligen-cmake-0.1 (crate (name "ligen-cmake") (vers "0.1.1") (deps (list (crate-dep (name "ligen-cmake-core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jszv35i0k6hc580aqma3h6f6cqzj6zkwfcyy66i4bgdp02w8bc5")))

(define-public crate-ligen-cmake-0.1 (crate (name "ligen-cmake") (vers "0.1.2") (deps (list (crate-dep (name "ligen-cmake-core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-macro") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1laxbgykyfnawzh5h6b209ahvz71n9mh9kn2g3xf2607wqnxnxr6")))

(define-public crate-ligen-cmake-0.1 (crate (name "ligen-cmake") (vers "0.1.3") (deps (list (crate-dep (name "ligen-cmake-core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-macro") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1cgvwhqswxnzzcsncfbmna6ni7926saaiqb2rb86p7qv5w93jbgb")))

(define-public crate-ligen-cmake-0.1 (crate (name "ligen-cmake") (vers "0.1.4") (deps (list (crate-dep (name "ligen-cmake-core") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-macro") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1f29b345px58lyf0l5lx22d7mcg9cjifjihrphp00fl6m04lrbyp")))

(define-public crate-ligen-cmake-core-0.1 (crate (name "ligen-cmake-core") (vers "0.1.0") (deps (list (crate-dep (name "ligen") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "1rdxs4ymxz1bnqrrm81wny92kgk2wv7w2bryzk978pjfvb9gvh6c")))

(define-public crate-ligen-cmake-core-0.1 (crate (name "ligen-cmake-core") (vers "0.1.1") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1whgna6590y4nqmvnv8xk8il433cyfg8kp9i8wl1vmn3bviyrzsm")))

(define-public crate-ligen-cmake-core-0.1 (crate (name "ligen-cmake-core") (vers "0.1.2") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1j1al3x5bic23h91d3r37s8f1fn7n0850paggij22irfz3zgm9lr")))

(define-public crate-ligen-cmake-core-0.1 (crate (name "ligen-cmake-core") (vers "0.1.3") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1rfgkn2zk0mmmhjhk04yg8vl13hmab33xqcwllqmwz12l82cc43z")))

(define-public crate-ligen-cmake-core-0.1 (crate (name "ligen-cmake-core") (vers "0.1.4") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1cqqjvffpr3habqdfx915sgfbpfk7g0d87rx5dafvqv43kray51b")))

(define-public crate-ligen-cmake-macro-0.1 (crate (name "ligen-cmake-macro") (vers "0.1.0") (deps (list (crate-dep (name "ligen") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d69j740x7227ak6lngcmacbvsh3v3j2m3rjwwpxkm9d4g52dn1d")))

(define-public crate-ligen-cmake-macro-0.1 (crate (name "ligen-cmake-macro") (vers "0.1.1") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jjlbhwsd80s72sz3q8jkn66rm3wr2rqg1cd875j0yyfp3r5znpi")))

(define-public crate-ligen-cmake-macro-0.1 (crate (name "ligen-cmake-macro") (vers "0.1.2") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1wqsyh1p8b4pjznpzaws2qd8g4qiq3rk4zs52zw7xfq5gvmp0ii2")))

(define-public crate-ligen-cmake-macro-0.1 (crate (name "ligen-cmake-macro") (vers "0.1.3") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1wp28jv394idzcf31xnfp41a4vbi8nvicrggzpm7jz9dn4f8hjsl")))

(define-public crate-ligen-cmake-macro-0.1 (crate (name "ligen-cmake-macro") (vers "0.1.4") (deps (list (crate-dep (name "ligen") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "ligen-cmake-core") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "091bhh215r8jh922k3pw2hjknqbwfnh61cz3ar0m2bgd5ja3b969")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1va9x24hcdm455x1p7j9g89jc39v79iq81v83s7y5nxra3fjsd41")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zdbg1n10p01f244jnpgiksmcd4lsi13r4fyyjdw2z9rkdg0247z")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.3") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "079xjdwwz946nz9197r3xw3pj272gc7ri9scm356adwi3764bgh6")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.4") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0d86jz8hl660aw23zzcaxnwir0h7cbfpihjbblc8hb8r5kh709cs")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.5") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h0m4q21y038v2lhr1sr4vdwihw6h6n2h3jclgi0dnq0h1xc8npz")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.6") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lga10d7k8zgg166c0wnsgff89080xxsimv0adnjzhwf2hpws64j")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.7") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02a60fzldklhkblxmsdh4p865q39lb8fmria9zpgxrrc12qnla8b")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.8") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18s17slag929fm6g086m8bpmwx92vx1icfhqwdbgmwpl5ksnjpf8")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.9") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b933lls4jp1qdfr1ihajcvqgrzm5kvlxk12s8m4zksxa6jhir14")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.10") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19pm5l58akfkib6dq9nvw3pnb35zd7dymcnypzsvc16fq8px2glf")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.11") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qam366qm7j3ya0nck0g4x13k62066711m40ql8pq13bfisihbrb")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.12") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "154md3axzz3iprhsh273dj4nij6y2dbi2h3ifq9dyzs4xg3w7rfd")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.13") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0f0nb63pg5p39q9r08f5m3q6iiqf3gngsz6w3wmyr6dfkcpyplfq")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.14") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02i6vwfxfszww0yq2l5yxgikxjy5n9nw44nr1ghq5hcyiydp8yzq")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.15") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h8d2zy7yfp3cmxvah9ciscgrxz5sgy10mfd55ld46cq1afl2p6y")))

(define-public crate-ligen-core-0.1 (crate (name "ligen-core") (vers "0.1.16") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "shrinkwraprs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b2k4wk1c8kd8805jd4x0l87lblf0xba77zjyjyd2bb6jbi0w9rz")))

(define-public crate-ligen-macro-0.1 (crate (name "ligen-macro") (vers "0.1.13") (deps (list (crate-dep (name "ligen-core") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "1navyn5yc2kz9ljggvm7p2wvjzicha09gbqqgcg03jnsgifc79ss")))

(define-public crate-ligen-macro-0.1 (crate (name "ligen-macro") (vers "0.1.14") (deps (list (crate-dep (name "ligen-core") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0nslg985aqxz8vyjq52qz5vzbzsjkhq5fsa9y6ss4ap2wyg3zyz2")))

(define-public crate-ligen-macro-0.1 (crate (name "ligen-macro") (vers "0.1.15") (deps (list (crate-dep (name "ligen-core") (req "=0.1.15") (default-features #t) (kind 0)))) (hash "0sd4qps3wmwypjywjwfq8kqwbzs5c3ghisnj1p0r1vwn2yz56vad")))

(define-public crate-ligen-macro-0.1 (crate (name "ligen-macro") (vers "0.1.16") (deps (list (crate-dep (name "ligen-core") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "185w0kvcvfn7sqa8nfnlpw3ws6nbckp7j77hm54vldpjawka0xk1")))

