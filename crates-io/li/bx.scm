(define-module (crates-io li bx) #:use-module (crates-io))

(define-public crate-libx11-0.1 (crate (name "libx11") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0hpsy5qd1c3i299s20svpvxa7iw9gmjyxm23mrry3p6gp9pgkwjg") (yanked #t)))

(define-public crate-libx11-0.2 (crate (name "libx11") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1zpzf5xgr9k7mvk07jy685gq2iscvzi73mqjlvhp725xyg86nlfl") (features (quote (("xinerama") ("keysym_extra")))) (yanked #t)))

(define-public crate-libxas-0.1 (crate (name "libxas") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0v9syskn8v4n9yb7ddf7g0jh4nvi4j4jvlf3hqffcdwn6jsgxciq") (features (quote (("rawbin") ("default" "rawbin" "chip8" "chip8-raw") ("chip8-raw") ("chip8" "chip8-raw"))))))

(define-public crate-libxbku-common-0.1 (crate (name "libxbku-common") (vers "0.1.0") (hash "14ws09dqpl1dk8qm2y576iazw8yj0vblybl7ki8kd19zbhvmnnbr")))

(define-public crate-libxbku-common-0.2 (crate (name "libxbku-common") (vers "0.2.0") (hash "0kci0x87jmm3vp66vpigs9qrmx2lmlsmkqnaxcwmzs7bsycwgkqy")))

(define-public crate-libxbku-common-0.3 (crate (name "libxbku-common") (vers "0.3.0") (hash "04ya003rm5w6281hi7yi6lx2svnbckgv2w0rkdls3gnzbywz6i1w")))

(define-public crate-libxc-sys-0.1 (crate (name "libxc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "09492v52d6sjcab7y7b8rjf3x3n21gyb6kb9c565ppdj3vwpibq5") (yanked #t)))

(define-public crate-libxc-sys-0.1 (crate (name "libxc-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "1gaf1n1m6rmcrfqrmfw7b1ylg1z2zch390w4ns3q8cz2zg6d61r9")))

(define-public crate-libxcp-0.1 (crate (name "libxcp") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "blocking-threadpool") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "libfs") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "15z4w9wgb9n9327cqm4jfd4yljs37p2pdkn01an5x18d5bvkdzp8") (features (quote (("use_linux" "libfs/use_linux") ("parblock") ("default" "parblock" "use_linux")))) (rust-version "1.70.0")))

(define-public crate-libxcp-0.18 (crate (name "libxcp") (vers "0.18.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "blocking-threadpool") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "libfs") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0ia3nlgshka78imab4flbc9kjybd2phh3k5jgf89jlgvl5yfgx76") (features (quote (("use_linux" "libfs/use_linux") ("parblock") ("default" "parblock" "use_linux")))) (rust-version "1.70.0")))

(define-public crate-libxcp-0.19 (crate (name "libxcp") (vers "0.19.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "blocking-threadpool") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "libfs") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "13ap1i1229rxwl0nwjq209xaryby2dcvmf7bh871lb8xb28474a3") (features (quote (("use_linux" "libfs/use_linux") ("parblock") ("default" "parblock" "use_linux")))) (rust-version "1.70.0")))

(define-public crate-libxcp-0.20 (crate (name "libxcp") (vers "0.20.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "blocking-threadpool") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "libfs") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "189i60lk7218vwvj111br12xi25vfirxgsvxi4ap8cx5jqmy9aj2") (features (quote (("use_linux" "libfs/use_linux") ("parblock") ("default" "parblock" "use_linux")))) (rust-version "1.75.0")))

(define-public crate-libxcp-0.21 (crate (name "libxcp") (vers "0.21.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "blocking-threadpool") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "libfs") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1m1mf9306q7n5ba6dn0irwx4x7nzi485g1m1z09n0pcmpikklsds") (features (quote (("use_linux" "libfs/use_linux") ("parblock") ("default" "parblock" "use_linux")))) (rust-version "1.75.0")))

(define-public crate-libxcp-0.21 (crate (name "libxcp") (vers "0.21.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "blocking-threadpool") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "libfs") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1f679fs1x1qggpsm2srd8gz5jbxnlyi3jcvi0v8w70w09qp6vgv5") (features (quote (("use_linux" "libfs/use_linux") ("parblock") ("default" "parblock" "use_linux")))) (rust-version "1.75.0")))

(define-public crate-libxdiff-0.1 (crate (name "libxdiff") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "libxdiff-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13nf89k79i4csj7dkyghpn72jyvsvqj6wqwsn74kiyylknv0rs2s")))

(define-public crate-libxdiff-0.2 (crate (name "libxdiff") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "libxdiff-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11lxjabjsi6ksgj4i1pp8cvk3r6r348lfdrhnc2s4irr7qjb7llq") (features (quote (("std") ("default" "std"))))))

(define-public crate-libxdiff-sys-0.1 (crate (name "libxdiff-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)))) (hash "1im615y1bmwf7n1w3nf7xkr351ig8aqqwyi8iv46drfbsp1b5wk6")))

(define-public crate-libxdo-0.0.1 (crate (name "libxdo") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "*") (default-features #t) (kind 0)))) (hash "1kx2vgfq53wvmvgss5r958dgnxv18hfdm0m0s0klam6jgiphyxq4")))

(define-public crate-libxdo-0.0.2 (crate (name "libxdo") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "*") (default-features #t) (kind 0)))) (hash "0gvxnzc207s89wr2jz92gmz9anfqivbqzihki2z7fmc2y10v3z8b")))

(define-public crate-libxdo-0.0.3 (crate (name "libxdo") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "*") (default-features #t) (kind 0)))) (hash "1dc98j13y4fpr2rnjsdf5s2xairvpk08hkkpiidjy36isf937wvr")))

(define-public crate-libxdo-0.1 (crate (name "libxdo") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "*") (default-features #t) (kind 0)))) (hash "1cx66q72hi6kfvl95mqwqvzxy2d54rsnm9pnzi0wkpi8yck3nfnf")))

(define-public crate-libxdo-0.2 (crate (name "libxdo") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13ifb972nyfvgwz58zawvi615f1c2f272dr2lhbbcrikq9mbi6rb")))

(define-public crate-libxdo-0.3 (crate (name "libxdo") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1ck7wfaw5ah1671492ya2rk1vb4swp00fzdw6rzy23k689ranih0")))

(define-public crate-libxdo-0.4 (crate (name "libxdo") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1vh2x5gs32chvw9zzspcczc8vn1mnl0akq2cl42aj1afv9m6f9lk")))

(define-public crate-libxdo-0.4 (crate (name "libxdo") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1252vxr0rg8lpfhj15m3s55rivbinvrcpxg24pklvb2z9qkany67")))

(define-public crate-libxdo-0.5 (crate (name "libxdo") (vers "0.5.0") (deps (list (crate-dep (name "libxdo-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0q6hvjdjvdcwhaqqs13z2w985zxvmsi7w33m877rv9kc183nagy5")))

(define-public crate-libxdo-0.5 (crate (name "libxdo") (vers "0.5.1") (deps (list (crate-dep (name "libxdo-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0m54zc0krz739clmjmjzgxxbsvalf23njpi7qjccns2sdwnwgyha")))

(define-public crate-libxdo-0.5 (crate (name "libxdo") (vers "0.5.2") (deps (list (crate-dep (name "libxdo-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1qbsgsqnqszcnayq3wkvmn8yk7awma80dls06cvvhmvmy5nfh9ic")))

(define-public crate-libxdo-0.6 (crate (name "libxdo") (vers "0.6.0") (deps (list (crate-dep (name "libxdo-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1nqlina6li1bmap0144h4hdsczyyfyinf87qvrw8xlm3as3kncq0")))

(define-public crate-libxdo-sys-0.0.1 (crate (name "libxdo-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req ">= 0.1.3") (default-features #t) (kind 0)) (crate-dep (name "x11") (req ">= 0.0.14") (default-features #t) (kind 0)))) (hash "03dln9w0d5sd806qi9hd44w8mw5hm1i7ckfzwdmmxadqvjkpbnn4")))

(define-public crate-libxdo-sys-0.1 (crate (name "libxdo-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req ">= 0.1.3") (default-features #t) (kind 0)) (crate-dep (name "x11") (req ">= 0.0.14") (default-features #t) (kind 0)))) (hash "0jg6yjbpcr2ympfa4nr9qydyxvgxgqn3m6rhmi6rr211zx317lyw")))

(define-public crate-libxdo-sys-0.9 (crate (name "libxdo-sys") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "*") (default-features #t) (kind 0)))) (hash "0zkiav21sr0bwf9wav9pmxy4n0bwnc4bjn6xbik3d84xmvbg00gg")))

(define-public crate-libxdo-sys-0.10 (crate (name "libxdo-sys") (vers "0.10.0") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1vx1fr68qvl4zp6mv7zal48248pzf3wjskrrl7qvwgdm03yly28y")))

(define-public crate-libxdo-sys-0.11 (crate (name "libxdo-sys") (vers "0.11.0") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.12.1") (default-features #t) (kind 0)))) (hash "04ljl0lmirg8a9q7w8ib2sybx35nnzpbw2xciayip0xpwbkvj8yv")))

(define-public crate-libxdp-0.0.1 (crate (name "libxdp") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1422niiwpa9q7zk4rpf91dvcsw2j27nqs6sg2sz61fhqjbmhqivj")))

(define-public crate-libxdp-sys-0.0.1 (crate (name "libxdp-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1iapr5mc7wbrzmpwksw869p9zgg1i0prkfsbh4ca8ikbyg4h324d") (yanked #t)))

(define-public crate-libxdp-sys-0.0.2 (crate (name "libxdp-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "09d3m9qa15v5zds8fk365znk4c4sf8yqg86kann27q0a74b31ich") (yanked #t)))

(define-public crate-libxdp-sys-0.1 (crate (name "libxdp-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)))) (hash "1m8lhryj5dhjwm09w69m0z0yfa9fsffsrjzl9ikvh23468sraqcv") (links "libxdp")))

(define-public crate-libxdp-sys-0.2 (crate (name "libxdp-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1pcfsh6118h2gzi6f1554pg7jz67qz615hwnjnwvcpb4nkn7jlcp") (links "libxdp")))

(define-public crate-libxenon-0.1 (crate (name "libxenon") (vers "0.1.0") (hash "15vq7g2l5ilq5i1vn19bfqi5x4fi6grc7rw49lw457lr3anb01c5")))

(define-public crate-libxget-0.0.0 (crate (name "libxget") (vers "0.0.0") (hash "04az3v1v6r2vdvv0k05mbj0fn1qqkci34n43g0hq5qgwka3qrc84")))

(define-public crate-libxid-0.1 (crate (name "libxid") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0i3q48g0g9hh0jdqmdji4ialgv96i3ylgnqlal6n9q6vanzd2jcy")))

(define-public crate-libxid-0.1 (crate (name "libxid") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "197m6zx7m7p10czihx9nrvw71rx41bgjrm1qyyg78ryrysaqz24x")))

(define-public crate-libxid-0.1 (crate (name "libxid") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0czdrx6wd0p56iaqys9svqn7b111ynvacg0n1dxgh78rljqhbwa9")))

(define-public crate-libxid-0.1 (crate (name "libxid") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "14ilyl6l8a27ij2isbpy01fiqiwkphdbb7hs0v0gs029zpyvqa2x")))

(define-public crate-libxid-0.1 (crate (name "libxid") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v9nklx642b7jazd84k4wq9cx614vfcsr5ylq1w8vcwq0zqq5x37")))

(define-public crate-libxid-0.1 (crate (name "libxid") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z3wscasq497ifdlh34gg3qmjk1ggjlfkhxc20s9b7cgqjsa40nl")))

(define-public crate-libxinux-0.1 (crate (name "libxinux") (vers "0.1.0") (hash "0iwvsnshm27jmqjw1vg5rsh3fzvy9spzdijidrgr8gvf4dqg4ri3") (features (quote (("xeorarch") ("xeonix"))))))

(define-public crate-libxinux-0.2 (crate (name "libxinux") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy-matcher") (req "^0.3.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("json" "blocking"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0dfyhxhp7vkxfck0s07pcxv0cnkr034sgg1lxim9mkw2lh52drc1") (features (quote (("pkgs-async" "tokio" "reqwest" "fuzzy-matcher") ("pkgs" "reqwest" "reqwest/blocking" "fuzzy-matcher") ("blocking" "pkgs") ("async" "pkgs-async"))))))

(define-public crate-libxinux-0.2 (crate (name "libxinux") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy-matcher") (req "^0.3.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.2") (features (quote ("json" "blocking"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0mvmxjf35gv6pyaw08crkal046gxaj1dnd7dwxr3yfhm7gn4kawy") (features (quote (("pkgs-async" "tokio" "reqwest" "fuzzy-matcher") ("pkgs" "reqwest" "reqwest/blocking" "fuzzy-matcher") ("default" "pkgs") ("blocking" "pkgs") ("async" "pkgs-async"))))))

(define-public crate-libxivdat-0.1 (crate (name "libxivdat") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "1xminfan07n3p273lks7ldkrpc99sfqh2vv7a487qw941p3dwb84")))

(define-public crate-libxivdat-0.2 (crate (name "libxivdat") (vers "0.2.0") (deps (list (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "0hsk3kn3gzj2sy0xcj4rh4hp1n1iyiir4rs2wifiif50q6941sil") (features (quote (("macro" "high-level") ("high-level"))))))

(define-public crate-libxlsxwriter-sys-0.8 (crate (name "libxlsxwriter-sys") (vers "0.8.7") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "02hrakxssk4y072vg5msmzb35zdqga3s9vga1imb02fhvw177jfz")))

(define-public crate-libxlsxwriter-sys-0.9 (crate (name "libxlsxwriter-sys") (vers "0.9.4") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "04rywjs4l5cc5rjy8ryb2a7rh9zgb739aj7zfc43bysa9j4wk4gj")))

(define-public crate-libxlsxwriter-sys-1 (crate (name "libxlsxwriter-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0pph7phzvd2yd3djpskjxihjxcx305cm6yshllk4x1ij764f9l73")))

(define-public crate-libxlsxwriter-sys-1 (crate (name "libxlsxwriter-sys") (vers "1.0.2") (deps (list (crate-dep (name "bindgen") (req ">=0.54, <0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "045rd3cmgmpd9rz9q8r5ci1qavmpijhb2qgljqr6ycqzyswdvd7i")))

(define-public crate-libxlsxwriter-sys-1 (crate (name "libxlsxwriter-sys") (vers "1.0.6") (deps (list (crate-dep (name "bindgen") (req ">=0.54, <0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0g84zvln8cbqia46j938ydcykx97xxfpchl614lni1jnmdmsfcbn")))

(define-public crate-libxlsxwriter-sys-1 (crate (name "libxlsxwriter-sys") (vers "1.1.1") (deps (list (crate-dep (name "bindgen") (req ">=0.58, <0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0z73a47mcy6a68bn1xp9lhs9xlgxamvnzhr7zyvcvhk0xvlrzy29") (features (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

(define-public crate-libxlsxwriter-sys-1 (crate (name "libxlsxwriter-sys") (vers "1.1.4") (deps (list (crate-dep (name "bindgen") (req ">=0.58, <0.60") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1fpghabxgi9drv0dgnmd6422c2lbsmbhvzpfsb3s3f4rfqbgbalf") (features (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

(define-public crate-libxlsxwriter-sys-1 (crate (name "libxlsxwriter-sys") (vers "1.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "196qcj3dh74q0zfi59ii41mvx9d35by76h5y7a3zhl520dx4i6jc") (features (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

(define-public crate-libxlsxwriter-sys-cs-1 (crate (name "libxlsxwriter-sys-cs") (vers "1.1.4-2") (deps (list (crate-dep (name "bindgen") (req ">=0.58, <0.60") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0kpskgf7kabc7ylzd2jaa825q75qgb2v1zjpfz92rcxy6si3hjqb") (features (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

(define-public crate-libxm-0.0.1 (crate (name "libxm") (vers "0.0.1") (deps (list (crate-dep (name "libxm-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0668rn9qjj4jm9kd9y3xcaqc101p3pd8sa9i94rsz63ia5a31vsc")))

(define-public crate-libxm-0.0.2 (crate (name "libxm") (vers "0.0.2") (deps (list (crate-dep (name "libxm-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0z09dwqkmllypwwv0b82ldm3g9nyn3pc3rrpzmmhg1lyfmak3fbf")))

(define-public crate-libxm-0.0.3 (crate (name "libxm") (vers "0.0.3") (deps (list (crate-dep (name "gcc") (req "^0.2") (default-features #t) (kind 1)))) (hash "09wlr16x9bz3130mvfyz5lfkvwyvvyjgjfyskpnacfqaxa7vpbpx")))

(define-public crate-libxm-0.0.4 (crate (name "libxm") (vers "0.0.4") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0nd16np0w5jjw5b1855fyafy1i03zzf6gk75a2jxhf67g3vb7rzb")))

(define-public crate-libxm-0.0.5 (crate (name "libxm") (vers "0.0.5") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "1yc0i7zcdvk8pnqi269p3nryz7bjq3s32jbs2hriicavvmj8vdzg")))

(define-public crate-libxm-0.0.6 (crate (name "libxm") (vers "0.0.6") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0ba54ql458pajflwrhkxad1dkrghfaqfcbccabjap1pmywbak4jc")))

(define-public crate-libxm-1 (crate (name "libxm") (vers "1.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "00b41g2v2ahlyz5w6x903p1qjmxgbq4vcfm5yzw4gqhhnq9f6i0a")))

(define-public crate-libxm-soundboard-0.0.1 (crate (name "libxm-soundboard") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03bakxr1lpfl5ppasb2r84v3vxrav33xairf5l3baghbhpvarbv1")))

(define-public crate-libxm-soundboard-0.0.3 (crate (name "libxm-soundboard") (vers "0.0.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0044x7fz9gxiavghcx2lil97lwgkwyib274cnnsqlvbpd48g9a68")))

(define-public crate-libxm-sys-0.0.1 (crate (name "libxm-sys") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "0.*") (default-features #t) (kind 1)))) (hash "0vnabyqfv88d8gjr3xarhl8l963gdcwlkzmw9zvrq2ca7mw4r7lh") (yanked #t)))

(define-public crate-libxm-sys-0.0.2 (crate (name "libxm-sys") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "^0.2") (default-features #t) (kind 1)))) (hash "0g0gzf0m1n60vg81nhs38h8a672jvzwi689j090hkqprk2yngmlp") (yanked #t)))

(define-public crate-libxml-0.0.2 (crate (name "libxml") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "07s71lmj3mab8zp89ljnrp42sw8y5ciprgrmyf5faxzddf29f13c")))

(define-public crate-libxml-0.0.3 (crate (name "libxml") (vers "0.0.3") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0h6apwk7jl6rnpfl9ivsdzhw3355wxfh06yx255w5w0wh2968jap")))

(define-public crate-libxml-0.0.4 (crate (name "libxml") (vers "0.0.4") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0jhx2zqlailr7zz04r4pwm4v82b6zmy166457rx6rzz6n6sa4pmq")))

(define-public crate-libxml-0.0.5 (crate (name "libxml") (vers "0.0.5") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "198k91b4h7f5wg5fdkf1l2jr3bb55xkwr59p79chl6hm89ksb9xb")))

(define-public crate-libxml-0.0.6 (crate (name "libxml") (vers "0.0.6") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1mh3zjgn9jjgzkykwz4y1xwmzkc69b3vmbihc9sm92yy08d8pi6d")))

(define-public crate-libxml-0.0.61 (crate (name "libxml") (vers "0.0.61") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "15i7r4fp4jkmqsdq34nlkff40nmyrj4mvqp4ah9lhqb7nbqj5lnj")))

(define-public crate-libxml-0.0.62 (crate (name "libxml") (vers "0.0.62") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1g7h1x1478pj1yxlrvq665vavph49wx7z40lxd2zcgirqffs21h8")))

(define-public crate-libxml-0.0.63 (crate (name "libxml") (vers "0.0.63") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0mdffz4wv6kbrcq0hsda8c8v5ack1i44gmr5r2yf5lgz1j1xjmr8")))

(define-public crate-libxml-0.0.7 (crate (name "libxml") (vers "0.0.7") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1ygi2radg034mxgbynigifq631pr6prjiwlxww7fdfp1gdgbm14g")))

(define-public crate-libxml-0.0.71 (crate (name "libxml") (vers "0.0.71") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1byp9ribdjga2cn9f1kgsj5yq2nal2azrhnafx061xahgr8y6djx")))

(define-public crate-libxml-0.0.72 (crate (name "libxml") (vers "0.0.72") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "06psyd62aig50940817b4wl7vhqlh5z3zlvq845r0hnphazc1c2c")))

(define-public crate-libxml-0.0.73 (crate (name "libxml") (vers "0.0.73") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "04a8vq51fvm9ic8ymjmdvrbzni20q9x203x7wcdxfx4agi3n97qv")))

(define-public crate-libxml-0.0.74 (crate (name "libxml") (vers "0.0.74") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "18bdk23n6rjkxkabr623qg3dff904czby4cwyqqr810al2ma1rm6")))

(define-public crate-libxml-0.0.741 (crate (name "libxml") (vers "0.0.741") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1q3h0kp8jb19hfrgvl0ad051ia409lbf6zfrz8a1pd5xh4sza0hn")))

(define-public crate-libxml-0.0.742 (crate (name "libxml") (vers "0.0.742") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0lwnjjcqywnc96pj137iq56gj676j75lazgcalgr2ilcywvkzsxv")))

(define-public crate-libxml-0.0.743 (crate (name "libxml") (vers "0.0.743") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "04xfd41nad08mc0vrsbbj4h9dmrc7py1lqig83gnhqi8vg4gkaa4")))

(define-public crate-libxml-0.0.744 (crate (name "libxml") (vers "0.0.744") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0sa5zsnpmcnajr1w54axgslhnphkid6f3kic2fnzdirfslycs0qf")))

(define-public crate-libxml-0.0.745 (crate (name "libxml") (vers "0.0.745") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1n86yyrcpxnhx0dhz336s9g7svsbnkw40c6zcz2qpagqc2b46c20")))

(define-public crate-libxml-0.0.746 (crate (name "libxml") (vers "0.0.746") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0v1j5vklx01irn3wka9vma0cll58nw7wfcx0dpz6zv3dpn5vq8kf")))

(define-public crate-libxml-0.0.747 (crate (name "libxml") (vers "0.0.747") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "07afwkchl0af9cfwx7r1lvnmjaqs8n6i7x63srsl725vzrllhb5n")))

(define-public crate-libxml-0.0.748 (crate (name "libxml") (vers "0.0.748") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "08qqp6xbdzjyyzycvwyfhpd239jfrvqrp5i4p85ay1wm2bzrsbv4")))

(define-public crate-libxml-0.0.7481 (crate (name "libxml") (vers "0.0.7481") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "03zrlq8w9iz0bba1bn8vf759zm1lkvb6vqxvsb8s196b3sk7pd4x") (yanked #t)))

(define-public crate-libxml-0.0.749 (crate (name "libxml") (vers "0.0.749") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0a5raq077zcdv5z1m9w8h0pa7hgbvxa90rlka7yh78a9iiapia5m")))

(define-public crate-libxml-0.0.75 (crate (name "libxml") (vers "0.0.75") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0mwb0ngxxh6l4vl0f6m9g40sfwfxp0qfdf62vp9kcrzlsxbf7lcx")))

(define-public crate-libxml-0.0.751 (crate (name "libxml") (vers "0.0.751") (deps (list (crate-dep (name "gcc") (req "^0.3.21") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "13cq9vlyjn263q9lqgj0ci27g3qgwk2ima6av2fb4z4rkgk0rs86")))

(define-public crate-libxml-0.1 (crate (name "libxml") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "0fvxlclx6k0zbc826m5l2h3gqmd4biqcwjdwllfl4hagj30n8rfg")))

(define-public crate-libxml-0.1 (crate (name "libxml") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "~0.1") (default-features #t) (kind 0)))) (hash "1b5f51x75wxbxz7qsixvi3nc9kgfvxgmv859wx0pnfv217kisq1q")))

(define-public crate-libxml-0.1 (crate (name "libxml") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19aq2vwi5baqiggsz5idpz3wgqxkv4by8rkvi25ms4ysn7xlyy8c")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.0") (deps (list (crate-dep (name "gcc") (req "^0.3.54") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0wa5dihwxyjafpnwm32m6fdvzv16wj7xvhw5273pwxp4g0mkk8wl")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1504smqq7b0s5lamh9bzv5ksfnnmkpv4cgwlxxahwqvd3plhrhir")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0b0wjvz9qkbi0sig5l7q4b3v1vg68vv4gq0n4qmzy39yhil2svwq")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0prhq74nlahbyqpkvhyngix57qdip3d2n8k96ds4nrg9hqsm60b0")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "16d0hf93p33qxr6kcqpg1slc5zfp6wybyhx52n8wvs30xsikwp58")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1pk0rq79lmsmhbz6is6x8xdbp32km5c5ffhzzi4i5zvgfb2xa5ia")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "02f6wxjy2j76jy9l4b27rgmvbjhn0xqdl10vl32clx8fwd89644k")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1x4v2gz0c13sldcvcq96zkp5ddxcwc1w0bl66bn7p1jxxpqkpswi")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "02bd9fz009v81xahl91ny5in2n90iv1kw8k411n5caczwl00bh8b")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1g3xna751pzn86l3bm3bx1g4b17rk4cksssw4q343pcyhf2vvxzi")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.10") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "05rvxjcb45ka8r4dv6marrnkvvin4g8xvbgkv3zndirbm3n6l0dd")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.11") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1i1jp3x7splvs358nkisb5p67bfwgzx1jr4iibvddyw1a8crmhkl")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.12") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1q7zr3a08bxmq8zamp1aq6v2p4s88jycyc23a7plgd3mcjnsa9gd")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.13") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1d7ipn6g0xb8012ik9pcc4nswjnfyp9pmq419l6pivga4syn3gpv")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.14") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0rlm5jxk5psixva60z3w9iakwlmia3aasiq6pfgnis4q3aiprajn")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.15") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(macos)") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "06s65w08ycwlrwcbrvy01kkfc9vc12izil33ygb4n5q50hl2km8k")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.16") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(macos)") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1xx1bsay6c0jy9vi7qcvliidya33lvbkxwpdfz9qmzl23f11xxxh")))

(define-public crate-libxml-0.2 (crate (name "libxml") (vers "0.2.17") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(macos)") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1z567rqb55hxn0g7hpvcsh3nfvz9mdzlwk4mk179z9bkf7v10r29") (yanked #t)))

(define-public crate-libxml-0.3 (crate (name "libxml") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(macos)") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1f42qrc1mim85ivh2d4bn2wbqrn7dfg1i3l1q32yajcr835pxh02")))

(define-public crate-libxml-0.3 (crate (name "libxml") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(macos)") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1b4pvmbg5bpphbdq2zszhz5csanfl8zgxh35538callhjdw5lzv8")))

(define-public crate-libxml-0.3 (crate (name "libxml") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(macos)") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "03pfl4nq54764psrh2n5ddpvc4n9mdgrfd07mn3776syn16hwcfa")))

(define-public crate-libxml-0.3 (crate (name "libxml") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(macos)") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "rayon") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1v3v3qnk9anik100q450gj8bhjylzizn0gzym4jnvcxwqbg3rrsz")))

(define-public crate-libxmlb-0.3 (crate (name "libxmlb") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ffi") (req "^0.3.0") (default-features #t) (kind 0) (package "libxmlb-sys")) (crate-dep (name "gio") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "11sxid23p1b8zg61aa713jgcpx3pckj8zip7694ryn0l6sans2y8") (features (quote (("dox" "ffi/dox"))))))

(define-public crate-libxmlb-sys-0.3 (crate (name "libxmlb-sys") (vers "0.3.0") (deps (list (crate-dep (name "gio") (req "^0.17") (default-features #t) (kind 0) (package "gio-sys")) (crate-dep (name "glib") (req "^0.17") (default-features #t) (kind 0) (package "glib-sys")) (crate-dep (name "gobject") (req "^0.17") (default-features #t) (kind 0) (package "gobject-sys")) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "shell-words") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "system-deps") (req "^6") (default-features #t) (kind 1)))) (hash "1i1h7cj3k3i28bijglp4q28h9dbacfkpi8c8s34v8bfsg76ps5ln") (features (quote (("dox" "glib/dox" "gobject/dox")))) (links "libxmlb-1")))

(define-public crate-libXNVCtrl-sys-0.1 (crate (name "libXNVCtrl-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "0ammlyi5nwwzb9z3mj2zf6vq0x4nws9ds39rcsxci7lnq7pj46z0") (yanked #t)))

(define-public crate-libXNVCtrl-sys-1 (crate (name "libXNVCtrl-sys") (vers "1.29.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.18.2") (default-features #t) (kind 0)))) (hash "1dr4h56vy2wqaacvl0c92i331319zibb1nh2ysh7j38wr36ww896")))

(define-public crate-libXNVCtrl-sys-1 (crate (name "libXNVCtrl-sys") (vers "1.29.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.18.2") (default-features #t) (kind 0)))) (hash "0qy8w3hxx6sc5r4ysjimrnsdj8dzhhflx1zg7ih13qvdsc4mxfwf")))

(define-public crate-libXNVCtrl-sys-1 (crate (name "libXNVCtrl-sys") (vers "1.29.2") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)) (crate-dep (name "x11") (req "^2.18.2") (default-features #t) (kind 0)))) (hash "0qyv97drg4n4al95k4wxn0av014zmfmvb0vw7y6vx0kg3lhnvg6w")))

(define-public crate-libxslt-0.1 (crate (name "libxslt") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libxml") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0vg5l31g5gnrc3xfixc12s4azmx9pybrx5cp9m2l9zk52377mg6i")))

(define-public crate-libxslt-0.1 (crate (name "libxslt") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libxml") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0zxvrix5dwlzi3l3lnx15x4yphfgzklql1i0xx9sqzvpwjjr71ds")))

(define-public crate-libxslt-0.1 (crate (name "libxslt") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libxml") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0898x8894lww4zjmvfys0k3vvhjaci54d28fp5f1jswxv1h2y6f7")))

