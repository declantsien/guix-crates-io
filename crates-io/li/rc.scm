(define-module (crates-io li rc) #:use-module (crates-io))

(define-public crate-lirc-sys-0.0.0 (crate (name "lirc-sys") (vers "0.0.0") (deps (list (crate-dep (name "nix") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "08fa3kyq2d1lyvxmm49j9vgjpl361q615pyf17i9hc6sck6zkajd")))

(define-public crate-lirc-sys-0.0.1 (crate (name "lirc-sys") (vers "0.0.1") (deps (list (crate-dep (name "nix") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0qxbsfwg7njpr9xnsl6ds6f8w4gjxf7fn1xl36qm5yr82fyi37bi")))

