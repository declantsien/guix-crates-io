(define-module (crates-io li ga) #:use-module (crates-io))

(define-public crate-ligab-0.1 (crate (name "ligab") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "http_req_wasi") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0a065f1l49kyggjv9z5389fh48y8wzj39wr78llb8viiqp39h4il")))

(define-public crate-ligab-0.1 (crate (name "ligab") (vers "0.1.1") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "http_req_wasi") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0rzgc1a4sbnn1212h5fjm301ry5yr63ihs0jfffxcllms1z930pl")))

(define-public crate-ligab-0.1 (crate (name "ligab") (vers "0.1.2") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "http_req_wasi") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1psranj51wijvcbv6bd4z7s024r8g0pfv4fy5jwk0hj6p5zqgccx")))

(define-public crate-ligature-0.1 (crate (name "ligature") (vers "0.1.0") (hash "1jn02a1ryd3s7wj1ql8iw23ra7xxjbfjp00miqf1w2zrblaj14km")))

(define-public crate-ligature-0.2 (crate (name "ligature") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qb61vk52cjkgv857w0nqs6dl7iac7ps9z81f5i3hx18j82dzyaz")))

(define-public crate-ligature-0.3 (crate (name "ligature") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gfwlprmg4kz72rrvq7ayk6wvljx9nfvymf1ws6m54rirq7m1fh0")))

(define-public crate-ligature-0.4 (crate (name "ligature") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1vbkc0zrasy54g1h1pjrlgds50yb9ifgqm7n9n29pzpi1n6gzbp7")))

(define-public crate-ligature-0.5 (crate (name "ligature") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0qhmwrn6n5s07mdjvwbi9s9bgdgn0vaj9qjsx1rv9wshw6py9izv")))

(define-public crate-ligature-0.6 (crate (name "ligature") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1kx3n7312wwdzqhb2834in4hvfqnwq9fy622h6cfw4zfrx6i6438")))

(define-public crate-ligature-0.7 (crate (name "ligature") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "03b8hcz3bx6hlcx9bb7rknkbpw41rm4fm89cs5j1s44khinj9756")))

(define-public crate-ligature-benchmark-0.1 (crate (name "ligature-benchmark") (vers "0.1.0") (deps (list (crate-dep (name "ligature") (req "^0.5") (default-features #t) (kind 0)))) (hash "03syvxryjyfhjqarasjxspvbg4i0x6kxk0l63vvki1b935pfnpm6")))

(define-public crate-ligature-graph-0.1 (crate (name "ligature-graph") (vers "0.1.0") (hash "18zr0hvpwvk6hk63mvc62wcw0wq3hqcwblbqpjkmcpcpgxr6kkgz")))

(define-public crate-ligature-in-memory-0.1 (crate (name "ligature-in-memory") (vers "0.1.0-SNAPSHOT") (deps (list (crate-dep (name "ligature") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rpds") (req "^0.9") (default-features #t) (kind 0)))) (hash "0n101v7qz8g6s6acgkck8pxvhsjl8rv5agqym4n4zmn29mmrijcp")))

(define-public crate-ligature-in-memory-0.1 (crate (name "ligature-in-memory") (vers "0.1.0") (deps (list (crate-dep (name "ligature") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rpds") (req "^0.9") (default-features #t) (kind 0)))) (hash "1gda6qrrjbb009ha8dcp2c0vhsylzcsz98r62yhwgxi4n8mnlj7a")))

(define-public crate-ligature-repl-0.1 (crate (name "ligature-repl") (vers "0.1.0-SNAPSHOT") (deps (list (crate-dep (name "lig") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ligature") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ligature-in-memory") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wander") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qk755z3v9jxmb10kycm7blypbc9z08hp26nwnj64kpk9hdq0c9r")))

(define-public crate-ligature-repl-0.1 (crate (name "ligature-repl") (vers "0.1.0") (deps (list (crate-dep (name "lig") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ligature") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ligature-in-memory") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wander") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nlzh2wvww9kkdqqvgmcrsak81ll0wd65jlls3z9vmby4gdwsxd2")))

(define-public crate-ligature-sqlite-0.1 (crate (name "ligature-sqlite") (vers "0.1.0") (deps (list (crate-dep (name "ligature") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25.3") (default-features #t) (kind 0)))) (hash "1id0laz9mcriry5a9idxr72j92rd0lpsy2lgfcp01bv7lvl9p7r4")))

(define-public crate-ligature-test-suite-0.1 (crate (name "ligature-test-suite") (vers "0.1.0") (deps (list (crate-dep (name "ligature") (req "^0.5") (default-features #t) (kind 0)))) (hash "1a08lk71qm6yrgbch2glbp4y5llzfjndvhd7qrw993h9gfg6w8rs")))

