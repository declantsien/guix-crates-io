(define-module (crates-io li _s) #:use-module (crates-io))

(define-public crate-li_stephens_py_hkhan-0.1 (crate (name "li_stephens_py_hkhan") (vers "0.1.0") (deps (list (crate-dep (name "li-stephens") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "0qf9nvb6y1vx2kbnp6s310pakrqml9nxpsxm0fij5pmc3yaasmz7")))

(define-public crate-li_stephens_py_hkhan-0.1 (crate (name "li_stephens_py_hkhan") (vers "0.1.1") (deps (list (crate-dep (name "li-stephens") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "0wmijhklbrh4qy1935c2jds4lx3n6c6i28zx56khpxm1md42876d")))

