(define-module (crates-io li ft) #:use-module (crates-io))

(define-public crate-lift-0.1 (crate (name "lift") (vers "0.1.0") (hash "1pxw961ngf9kj4s33li653xrlszx02g54y582s7ssbj8gfiijw47")))

(define-public crate-lift-fail-0.1 (crate (name "lift-fail") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ssf7x32ys9pb51x2rn045a7hxzh7l3qqi176c5cdc5xhr4n2314") (yanked #t)))

(define-public crate-lift_result-1 (crate (name "lift_result") (vers "1.0.0") (hash "17schvvhx394zjwigmhl343i2fi854ihgvx9l0zpbndpfqz7gl8v") (yanked #t)))

(define-public crate-lift_result-1 (crate (name "lift_result") (vers "1.0.1") (hash "1a8cjnljpl7w1n8mkwh457l8sjac9pzl7b5ky02hbv5gsqjmclmd")))

(define-public crate-liftbridge-0.1 (crate (name "liftbridge") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "121q0jnfqxlahawsr1s00bdsijvn0w4x3acqn5l154bk1s1yx85f")))

(define-public crate-liftbridge-0.1 (crate (name "liftbridge") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "17q1jkmm0gp22qrfadq4hd7lxbgav71pz62c1rnp2ssy2ja2scsc")))

(define-public crate-liftbridge-0.1 (crate (name "liftbridge") (vers "0.1.2") (deps (list (crate-dep (name "actix-rt") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "12axcjw24gi4yyyywkxjrw64qmp9755lb9r3hrvgch09bzsg6s7r")))

(define-public crate-lifted-0.1 (crate (name "lifted") (vers "0.1.0") (hash "0np8cjay4lkh0bjglb0qlk181zhxdmliq3yrfcp2mvpacna6sc0s")))

(define-public crate-lifted_elgamal-0.1 (crate (name "lifted_elgamal") (vers "0.1.0") (hash "0qm4zzq3yvf5ay55vis1iibdwjq988p1yam2gj68isfpwg2awjh8")))

(define-public crate-lifterr-0.1 (crate (name "lifterr") (vers "0.1.0") (hash "0df3ww3gqmshpjsj4d8zsdqr7sv16mf9sghk1q0x87g8xcnfvw6d")))

(define-public crate-lifterr-0.2 (crate (name "lifterr") (vers "0.2.0") (hash "12rxdaadc2b75dflsvwb3pkpgqb7bl4hgm83yi72y6ajxbqw9crm")))

(define-public crate-lifterr-0.3 (crate (name "lifterr") (vers "0.3.0") (hash "006p6rf4c28x3rvba3ng1liqr150pwdb779jx0fm8z06rxhl4gmv")))

(define-public crate-lifterr-0.3 (crate (name "lifterr") (vers "0.3.1") (hash "04fx7nfhb4j95jvm543fswvcpzcd24k0fcj4bqlz4iv1wqazjm56")))

(define-public crate-lifterr-0.4 (crate (name "lifterr") (vers "0.4.0") (hash "1amkmpc6d1abs4wr885bn60v9fwy7zv0qzx1p6qm5kfhbig7yxjg")))

(define-public crate-lifterr-0.4 (crate (name "lifterr") (vers "0.4.1") (hash "0lviw3bmhy5pap0xp3pc92dg5pnddncajdk458rx3rh1xh8rylwx")))

(define-public crate-liftoff-0.1 (crate (name "liftoff") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "leg") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)) (crate-dep (name "sane") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g51069850id1l03gjh1asv8kx07gfjpirqixv4dparqh9mg36fy")))

(define-public crate-liftoff-0.1 (crate (name "liftoff") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "leg") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)) (crate-dep (name "sane") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v307z833rc4p6qgfzr6z8y9pvcw1r4v4kwm9qb5hjnvdrm43mim")))

(define-public crate-liftor-0.1 (crate (name "liftor") (vers "0.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1j7yyvd3d1j7v0xjx06zg4aqdamx1a21qp7y7g87lzd3fw607zk9")))

