(define-module (crates-io li ar) #:use-module (crates-io))

(define-public crate-liar-0.1 (crate (name "liar") (vers "0.1.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "12lwpvi1mljdgckikfvfxrcdw60qczshn89g9bsc0lx2yymlxlyw")))

(define-public crate-liar-0.2 (crate (name "liar") (vers "0.2.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "1n251vawvq9f2a0yv8pxkcvq5i2q23cjavxj2jk99kdxrgxrcm40") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-liar-0.3 (crate (name "liar") (vers "0.3.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "0ichsinicns5z3jck5zkdacbr7wxzy520bvsr9l3fsjibd6r43p4") (features (quote (("std") ("default" "std"))))))

(define-public crate-liar-0.4 (crate (name "liar") (vers "0.4.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "1s0w574c4rssxwy4lxlk9n8fxxhzyhacbbnmfnb6112z0vcic6qh") (features (quote (("std") ("default" "std"))))))

(define-public crate-liar-0.5 (crate (name "liar") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rustc_version") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "1766wmkk0c2mnspv1xs7h101xaziixylpxvjqnwchacwc7618432") (features (quote (("std") ("default" "std" "libc"))))))

