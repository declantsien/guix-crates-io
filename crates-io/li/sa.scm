(define-module (crates-io li sa) #:use-module (crates-io))

(define-public crate-lisa-0.1 (crate (name "lisa") (vers "0.1.0") (hash "1jnydwx0507hy96nkczhpla9qywxq7ybmlza4wif1phx177g3xzg")))

(define-public crate-lisa-0.2 (crate (name "lisa") (vers "0.2.0") (hash "1h2xp9hb8p2lj0n3sxpllrvdkq1b34n044wx9bnz11hcws02r4d2")))

(define-public crate-lisa_test-0.1 (crate (name "lisa_test") (vers "0.1.0") (hash "06vis1cbq8pb6a1sfy6n5nxvrf4pjxif25b0i0knxlgvs867bsk0")))

