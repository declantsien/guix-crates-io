(define-module (crates-io li b2) #:use-module (crates-io))

(define-public crate-lib2-0.1 (crate (name "lib2") (vers "0.1.0") (hash "0xq1rgnb1386qxjzfxgaisrk0868rrpnk7hd4zh14jprk9nrmr9v")))

(define-public crate-lib2048-0.0.1 (crate (name "lib2048") (vers "0.0.1") (deps (list (crate-dep (name "console") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "193aj1kyayc5kwrksiwy0h3dqqajpm9m5fv77s6c6gcwvx88mwfz")))

(define-public crate-lib2048-0.0.2 (crate (name "lib2048") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "04ljkqx81dyv6w4049hssjxx5yjq1f1w1bbyfvf922craag30gvf")))

(define-public crate-lib2d-0.1 (crate (name "lib2d") (vers "0.1.0") (hash "08p4y1fr7fz3wd9ra90y4gpfcvnqxpf2yb663zy7sln72iq5d2ni")))

(define-public crate-lib2MyTest-0.1 (crate (name "lib2MyTest") (vers "0.1.0") (hash "06bawsblagr0xgm94814rp69rdmh62m63z6p9w1zkpfdcv59bj0q")))

