(define-module (crates-io li ea) #:use-module (crates-io))

(define-public crate-liealg-0.1 (crate (name "liealg") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)))) (hash "1mvl0j391vc0ln1g13i38vk4zg5j3qmghl4ag7l84qgb0yh2wrq6") (yanked #t)))

(define-public crate-liealg-0.1 (crate (name "liealg") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)))) (hash "1d1b8vam6yfzss4pjy956z0aw9fn7v57vrhyxg1ci47bniihaqgy") (yanked #t)))

(define-public crate-liealg-0.2 (crate (name "liealg") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1s0x74lmiqwgg71lcaa460bq7811ydypbb379l1hg6kc1qpzmhln")))

(define-public crate-liealg-0.3 (crate (name "liealg") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "092c962b1yhhv37xfdgmapayzj85b8aahdpn0gdxi7sbb1jrv376")))

