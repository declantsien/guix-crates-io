(define-module (crates-io li mn) #:use-module (crates-io))

(define-public crate-limn-0.0.0 (crate (name "limn") (vers "0.0.0") (hash "156wihw4yxsbi9v1xmdafcjx9afq8ymwh5m1zjrs4shnl5mgaq5i")))

(define-public crate-limn-layout-0.0.1 (crate (name "limn-layout") (vers "0.0.1") (deps (list (crate-dep (name "cassowary") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^0.1.4") (default-features #t) (kind 2)))) (hash "11fqcaxjvn6gx7b39b4pmyx9xawq4yiqlagzqfs046m4mw8l60f2")))

(define-public crate-limn-text_layout-0.0.1 (crate (name "limn-text_layout") (vers "0.0.1") (deps (list (crate-dep (name "euclid") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1dxqvhga8vpbdyzz2vh6y5wwalh5vqy5hpfspqgcn90xcra4p1j4")))

