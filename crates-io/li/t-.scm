(define-module (crates-io li t-) #:use-module (crates-io))

(define-public crate-lit-html-0.0.0 (crate (name "lit-html") (vers "0.0.0") (deps (list (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "138rvwqzr5c08h6190s4b13wcbky81yn7506db1xphqrgqj5yh7c")))

(define-public crate-lit-html-0.0.1 (crate (name "lit-html") (vers "0.0.1") (deps (list (crate-dep (name "globals") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "lit-html-macro") (req "^0") (default-features #t) (kind 0)))) (hash "1gxhj6581llg5f3kjb43dz38lh04n5asl73q2dips9mv8s0n5gma")))

(define-public crate-lit-html-0.0.2 (crate (name "lit-html") (vers "0.0.2") (deps (list (crate-dep (name "globals") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "lit-html-macro") (req "^0") (default-features #t) (kind 0)))) (hash "0bb93nxq992nfcck2z740nma7kyn83ccz2sn2wvmm5vddyr459xl")))

(define-public crate-lit-html-0.1 (crate (name "lit-html") (vers "0.1.0") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "0ycqnmvnv8lmc3var23spdw0ybva383cxjkykiilb807gpbcn5jw")))

(define-public crate-lit-html-0.1 (crate (name "lit-html") (vers "0.1.1") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "1zcqzlrmv3ywaczpydkxl6w63nhad59bscbqy1nfvskmppf4hlvr")))

(define-public crate-lit-html-0.1 (crate (name "lit-html") (vers "0.1.2") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "1a3f08scxdkylp2nmm392ajiljgqzxmsjbaw8306cbjw2cs748gx")))

(define-public crate-lit-html-macro-0.0.0 (crate (name "lit-html-macro") (vers "0.0.0") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qw6c76x45v040ykmcwa5nxqdfj6bhy8czbp70js61pvx28gg9ra")))

(define-public crate-lit-html-macro-0.0.1 (crate (name "lit-html-macro") (vers "0.0.1") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0schdqlghnk1ggdpmw1s3ilpbvpj25xsdm9vm1fy1a65mrf8a3cc")))

(define-public crate-lit-mod-0.1 (crate (name "lit-mod") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1q0cgh76wwsizbjmf83fbv9lin8m2v40m074k4s4q0mz9dr7w781") (rust-version "1.64")))

(define-public crate-lit-mod-0.2 (crate (name "lit-mod") (vers "0.2.0") (deps (list (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0adwlsvdqzl1iyqy4kl9xis0ghpycy3mzi9s2g4my54na0xjpbr2") (rust-version "1.64")))

(define-public crate-lit-sev-snp-utils-1 (crate (name "lit-sev-snp-utils") (vers "1.4.4") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.57") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.20.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.12.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cached") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.42") (default-features #t) (kind 0)) (crate-dep (name "pem") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "0il1a3b6ssp9zh7nj7cbq3bnhhgvxdks9wfnz9ji6s9s6rm5amsb") (features (quote (("trust-dns" "reqwest/trust-dns") ("default"))))))

(define-public crate-lit-vek-0.1 (crate (name "lit-vek") (vers "0.1.0") (hash "0sz7k9cpx8mjbqsyl95na1lap4f4jdjpi8d1qg7qkn2088w2ac5z")))

