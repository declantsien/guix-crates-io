(define-module (crates-io li on) #:use-module (crates-io))

(define-public crate-lion-0.0.0 (crate (name "lion") (vers "0.0.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1i8kjbc94rspm3b0jyvq2xcclgdhqjwm1zzqyggyvlg5yb79bddl") (yanked #t)))

(define-public crate-lionel-says-hello-0.1 (crate (name "lionel-says-hello") (vers "0.1.0") (hash "0sgi5m47qr4hshzcdpa2ffnvkjygi339iwl9fx43sdcn2l64ah15")))

(define-public crate-lionel-says-hello-0.1 (crate (name "lionel-says-hello") (vers "0.1.1") (hash "0r4xjg891jz53ix90gkil9v334b4syyp99b734xlf15grwfk66i6")))

(define-public crate-lioness-0.1 (crate (name "lioness") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1czzv58yh0xngvybbcgkcql51chwzsh08mmsndk4d59f415pafn4") (yanked #t)))

(define-public crate-lioness-0.1 (crate (name "lioness") (vers "0.1.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "05h9fq9k7dg2pwp8g0ydqg46wk53smhcw2j60xzpxa2w24dmlfdz")))

(define-public crate-lioness-0.1 (crate (name "lioness") (vers "0.1.2") (deps (list (crate-dep (name "arrayref") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "chacha") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "keystream") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1fdcm7ikvbwgfn5y50ifvxry4x8d6dw124jpjif44b54ddq2dsaa")))

(define-public crate-lioness-rs-0.1 (crate (name "lioness-rs") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "^1.0.0") (features (quote ("traits-preview"))) (default-features #t) (kind 2)) (crate-dep (name "chacha20") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "cipher") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.0-pre.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14") (features (quote ("more_lengths"))) (default-features #t) (kind 2)))) (hash "0l4hlw6zzxifkv8lparpvyxdi8fidb58h4j6cvxvm89qfklis7fl") (features (quote (("block-cipher"))))))

(define-public crate-lioness-vnpmid-0.1 (crate (name "lioness-vnpmid") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "chacha20") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "09dihamhjl704cgiqsawlgmylwblm3fmab2jxqk7finxbn0mbb6q") (yanked #t)))

(define-public crate-lionfish-0.1 (crate (name "lionfish") (vers "0.1.0") (hash "0hxvfk5qpz7h97dk0mffzdnbvfvrvms5jdi1h43rqkd78chw72vx")))

(define-public crate-lionfish-0.1 (crate (name "lionfish") (vers "0.1.1") (hash "0v3s7mfysb8lzwkpava18961gi2q6v0m88skkncxjyjksgl6jhyr")))

