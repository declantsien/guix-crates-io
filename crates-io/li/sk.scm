(define-module (crates-io li sk) #:use-module (crates-io))

(define-public crate-lisk-api-rust-client-0.1 (crate (name "lisk-api-rust-client") (vers "0.1.0") (deps (list (crate-dep (name "mockito") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.9.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.90") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.90") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1nnzigwmpcipwjw4agz66k808zd9gfamblgmkzl1ig57qyax5dzz")))

