(define-module (crates-io li b6) #:use-module (crates-io))

(define-public crate-lib6502-0.1 (crate (name "lib6502") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1hp5zlx7nlayl772kgyinrskyi35ni4lldxqzbs88zvnvxjpl1g0")))

(define-public crate-lib6502-0.1 (crate (name "lib6502") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0cn6q1p2m1sc928csgkpj78yvz13c4zbbhry3ah6qrvn1j4vz6s9")))

(define-public crate-lib6502-0.2 (crate (name "lib6502") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "020spi4fvkjry7gm7bdy6nzgh8fyqclxpiasabmziaj731s4wnzx") (yanked #t)))

(define-public crate-lib6502-0.2 (crate (name "lib6502") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1a91nzwxig3bd021m9gkbwm7j5f1viwy6mgnx0xxywdwsj971jm3")))

