(define-module (crates-io li ef) #:use-module (crates-io))

(define-public crate-lief-0.0.1 (crate (name "lief") (vers "0.0.1") (hash "1i2imwd254664zpgg0ik5l94frnccrysni70b82l1cwhidnpgvzd")))

(define-public crate-lief-0.0.2 (crate (name "lief") (vers "0.0.2") (hash "1a81590ys32yr7l0daxxkw3pms04db2ayc3r1clfw3zr60wa371p")))

(define-public crate-lief-cwal-0.1 (crate (name "lief-cwal") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.3") (features (quote ("ico"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "lief-cwal-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "picky") (req "^7.0.0-rc.3") (features (quote ("wincert" "x509" "pkcs7"))) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (kind 2)) (crate-dep (name "widestring") (req "^1.0.2") (features (quote ("alloc"))) (kind 0)))) (hash "1ab286vkl34hql3qa1v8lw3vhsd7cdpyps1pwazjbk40r2n2hj6p")))

(define-public crate-lief-cwal-sys-0.1 (crate (name "lief-cwal-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1cdlsi6qach3hivcw2qzgsm6agl8bywicc844xyfjcka6yps6sji")))

(define-public crate-lief-sys-0.11 (crate (name "lief-sys") (vers "0.11.5") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0kr74ly05rfjwyp1qnq7rr4zwy2s8ph7kmajpkbncrfmiwm61a1l") (yanked #t)))

(define-public crate-lief-sys-0.0.1 (crate (name "lief-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0bqqimf54mfkxwa6w4f6n9lgn4xfkrymlfw4dhlhmk0qbirsp4h9")))

