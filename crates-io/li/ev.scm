(define-module (crates-io li ev) #:use-module (crates-io))

(define-public crate-lieval-0.1 (crate (name "lieval") (vers "0.1.0") (hash "06cgaac8kn1y5ry9h2z8dqhr5mgizkjzjidc1rq430h1sy8r39jg")))

(define-public crate-lieval-0.1 (crate (name "lieval") (vers "0.1.1") (hash "1ag7di6dfk458frzbg6y60hpbww4m0y4bc7825r0mikfkm0xq3gf")))

(define-public crate-lieval-0.1 (crate (name "lieval") (vers "0.1.2") (hash "1pal7hsfiw5a4174563zqlwi2dhin9d4ym7mh0c67gnh9l7q9icv")))

(define-public crate-lieval-0.2 (crate (name "lieval") (vers "0.2.0") (hash "1rhm21x7lgizk4py3dc81282i0lcqppjlx81d1zavbmzvgwysjgm")))

(define-public crate-lieval-0.2 (crate (name "lieval") (vers "0.2.1") (hash "0i6cfdhm1ircwa83937dxbnjf4qjp8vvk6bc3f2vbimydq1y3l59")))

(define-public crate-lievre-0.0.1 (crate (name "lievre") (vers "0.0.1") (hash "0kipj27gwhs5k06p153xfaxvy27nwbssnc219nmjsz9anf9mxk6v")))

(define-public crate-lievre-0.0.2 (crate (name "lievre") (vers "0.0.2") (hash "0smyywrc4l9abxb06g9kmv5w4sywjn2nzkf8w6y0z2dhlzqi0r66")))

(define-public crate-lievre-0.0.3 (crate (name "lievre") (vers "0.0.3") (hash "1dny582avj9xha9klgvav0ixiwvyvbwa1sy9cs7ipir43mllw5mx")))

(define-public crate-lievre-live-0.0.3 (crate (name "lievre-live") (vers "0.0.3") (hash "0bm1j8gailpj7zjw15g4h87azz6hkrgllymx3qhqkx7xvd2l5nn9")))

(define-public crate-lievre-standalone-0.0.3 (crate (name "lievre-standalone") (vers "0.0.3") (hash "1mg0dk63a0gvvl3k6l8fc06fmpkn0q931k4xvvh4mqwf6wfr3pls")))

