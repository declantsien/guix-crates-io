(define-module (crates-io li bj) #:use-module (crates-io))

(define-public crate-libjade-0.0.1 (crate (name "libjade") (vers "0.0.1-dev.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "fs_extra") (req "^1.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 1)) (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (target "cfg(not(windows))") (kind 1)))) (hash "0hajcfl8rkixa4a3h2aqhdhqkdw198znli1x6n8k0ab6rnmi9jhj")))

(define-public crate-libjade-0.0.0 (crate (name "libjade") (vers "0.0.0") (hash "1yf1777j7x9pjv1b1fx2c21kgm7ihc6a8y9dbvymg5ap5afxwhpn")))

(define-public crate-libjade-sys-0.0.1 (crate (name "libjade-sys") (vers "0.0.1-dev.1") (deps (list (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (target "cfg(not(windows))") (kind 1)) (crate-dep (name "fs_extra") (req "^1.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "1if8xrhn1psbyhhyr72k1i6v0r8m5f3w8qri1p5wy87mf5ycam4h")))

(define-public crate-libjade-sys-0.0.1 (crate (name "libjade-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (target "cfg(not(windows))") (kind 1)) (crate-dep (name "fs_extra") (req "^1.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 1)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 2)))) (hash "00x1f05jy7qmcvxgwsz2v57q6zcszc2lv4sld87b00vnmsm5hysl")))

(define-public crate-libjade-sys-0.0.2 (crate (name "libjade-sys") (vers "0.0.2-pre.1") (deps (list (crate-dep (name "bindgen") (req "^0.68") (optional #t) (default-features #t) (target "cfg(not(windows))") (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libcrux-platform") (req "=0.0.2-pre.1") (default-features #t) (kind 1)) (crate-dep (name "pretty_env_logger") (req "^0.5") (default-features #t) (kind 2)))) (hash "18sp430a371ihpfzwx6sflcbwcism46cfgvkpvbx0xafb5qdrk4c") (features (quote (("bindings" "bindgen"))))))

(define-public crate-libjade-sys-0.0.2 (crate (name "libjade-sys") (vers "0.0.2-pre.2") (deps (list (crate-dep (name "bindgen") (req "^0.69") (optional #t) (default-features #t) (target "cfg(not(windows))") (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libcrux-platform") (req "=0.0.2-pre.2") (default-features #t) (kind 1)) (crate-dep (name "pretty_env_logger") (req "^0.5") (default-features #t) (kind 2)))) (hash "13pvizs2hq2pjrlwgdf3w03qgfkdwp8cyv73xdd8zgvnljxj4kgc") (features (quote (("bindings" "bindgen"))))))

(define-public crate-libjayjeer-0.1 (crate (name "libjayjeer") (vers "0.1.0") (deps (list (crate-dep (name "cfg_eval") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "^0.12.3") (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("json" "blocking"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^3.4.0") (features (quote ("base64"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.0") (features (quote ("static_secrets" "zeroize" "precomputed-tables"))) (kind 0)))) (hash "0lpw0k0rhwhlq4l8nzq28na0arpx3hnnw34fwp7g6wkp1lh5accj") (features (quote (("std" "rand/std" "x25519-dalek/alloc" "hkdf/std" "sha2/std") ("default" "client" "std")))) (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_with") ("message" "std" "dep:libaes" "dep:hmac" "rand/getrandom") ("client" "std" "message" "serde" "dep:url" "dep:reqwest" "dep:serde_json"))))))

(define-public crate-libjayjeer-0.3 (crate (name "libjayjeer") (vers "0.3.0") (deps (list (crate-dep (name "cfg_eval") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "^0.12.3") (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("json" "blocking"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^3.4.0") (features (quote ("base64"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.0") (features (quote ("static_secrets" "zeroize" "precomputed-tables"))) (kind 0)))) (hash "11fjiznxzgp2cr8bcxnkh8md91gbpsdbwdhsgb0bb0dhy9gn9455") (features (quote (("std" "rand/std" "x25519-dalek/alloc" "hkdf/std" "sha2/std") ("hazmat") ("default" "client" "std")))) (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_with") ("message" "std" "dep:libaes" "dep:hmac" "rand/getrandom") ("client" "std" "message" "serde" "dep:url" "dep:reqwest" "dep:serde_json"))))))

(define-public crate-libjess-0.1 (crate (name "libjess") (vers "0.1.0") (hash "00kj0114v0wdzwwhxc2ypx42vky53zjpbv2xm0vs4y4a7p7cbq2y") (features (quote (("fs") ("default" "fs"))))))

(define-public crate-libjess-0.1 (crate (name "libjess") (vers "0.1.1") (hash "1k54ilzw2ayhdv50zj9vi6xwd085pibqdqzna02ddqlvl49p9rh6") (features (quote (("fs") ("default" "fs"))))))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1jcgrl0rgz7f9d30hcxrsc9wkcnfwmrr9wrx8rsip6mqgk60sfjb")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1shvn1zjq4ppqqnl1ymh8wjyi5zi06yp5gqr54zn9y6h2slilplv")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "0p2cpicda86z9wysrzl8hj1syj2054v0808hymrcr7k1pzi69xry")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.3") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1c5iqjlfy4rx25shf5y78gr925m4vkv38w056m3d2rz98219lhr2")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.4") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "11fnqqahm8xxddzaisp33d7vdlaaqvs6ql51a5qm5b7xqwvvkgwb")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.5") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "0v0vz5q743fs4g4wfzki399cn9j8s0px62bgy73v8glby395xdas")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.6") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "10l96zwixf1q2ami2mp8bmlprkzscr9ar79hy40227ak0kf2fc41")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.8") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "00kq241ja5cmj56yhrdj2ry6ipsspg4zv3qg5gd740wnvxbkm610")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.9") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1s9ihz0i0igy3dy02rfygf7rxv715z36gjxwiayjlw9rk5y2h77a")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.10") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1l6b54ig2nlqmb355afigdn0px9z1gzinzvxp8shgqg7bbphgcjp")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.11") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "0jsxx8z26lkpgylf575lw5dq84w4zmi3yz0r3n8l129py9vh40np")))

(define-public crate-libjit-sys-0.1 (crate (name "libjit-sys") (vers "0.1.12") (deps (list (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1d8gswqcp052i1d9xb7xfkqirqdl1h685mah4c3p168s0sqxm7vh")))

(define-public crate-libjit-sys-0.2 (crate (name "libjit-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0mmzhgg4sk0hgmbp9b9x6m9zr21xicjc7r1df3lm35vv2syxp72f")))

(define-public crate-libjokes-0.1 (crate (name "libjokes") (vers "0.1.1") (deps (list (crate-dep (name "json") (req ">=0.12.4, <0.13.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req ">=0.10.0, <0.11.0") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0rdk1njm90p95znsx4nv6cp1s72pz2z0jns1qfsyknlx9vkl4iic")))

(define-public crate-libjokes-0.1 (crate (name "libjokes") (vers "0.1.2") (deps (list (crate-dep (name "json") (req ">=0.12.4, <0.13.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req ">=0.10.0, <0.11.0") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0zinhqw0f0y5axp1xf1l52z016ng7d38cpcdr7smwcn4vrf8wiw9")))

(define-public crate-libjp-0.1 (crate (name "libjp") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "diff") (req "^0.1.0") (default-features #t) (kind 0) (package "jp_diff")) (crate-dep (name "graph") (req "^0.1.0") (default-features #t) (kind 0) (package "jp_graph")) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "multimap") (req "^0.1.0") (default-features #t) (kind 0) (package "jp_multimap")) (crate-dep (name "partition") (req "^0.1.0") (default-features #t) (kind 0) (package "jp_partition")) (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.7") (default-features #t) (kind 0)))) (hash "17c74w5szf9ha0lmm0jrah8q4y03wzcb0p071sfvsj99rjafrx52") (yanked #t)))

(define-public crate-libjpegturbo-sys-0.0.1 (crate (name "libjpegturbo-sys") (vers "0.0.1") (deps (list (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "18py5ygqydax2kcyh0kv94hq7si3rykv59v0npgkpa6c9g7l0adz")))

(define-public crate-libjpegturbo-sys-0.0.2 (crate (name "libjpegturbo-sys") (vers "0.0.2") (deps (list (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0b53y0si4q23wwql117sfim3r9by3p3c9gpz0r3n425xphyzaxvk")))

(define-public crate-libjuice-rs-0.1 (crate (name "libjuice-rs") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libjuice-sys") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ws0lylzdsf5dnczjfqnw0faq74f49bz7rj8y8bld0jkam75slrv")))

(define-public crate-libjuice-sys-0.9 (crate (name "libjuice-sys") (vers "0.9.7") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0cy7v4bndi19rbw85zjz9b86c7m2kijx2pnhxyqhd7c4rl4w4krv")))

(define-public crate-libjwt-0.1 (crate (name "libjwt") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0gn5a7dhjnjq22waf0wgyin6qffwxnbnwvsdcgigyka59wbq3wng") (features (quote (("default"))))))

(define-public crate-libjxl-src-0.2 (crate (name "libjxl-src") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0zgqfyx690zc60vkplz8hp4ivr8ihlg5cwbj5lcr87d2z8b94c7c")))

(define-public crate-libjxl-src-0.2 (crate (name "libjxl-src") (vers "0.2.1") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0cq17abdgf7ysi4bkrqjzcq9hvqsknl3c2j71fmmgmr851isfahg")))

(define-public crate-libjxl-src-0.2 (crate (name "libjxl-src") (vers "0.2.2") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0hmhb245h7gy5jldhklp0g4d7i0bxilsmx34rw8h49ksaiq8658g")))

(define-public crate-libjxl-src-0.2 (crate (name "libjxl-src") (vers "0.2.3") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0xckvkrwnqpl2j3n1q5w71y6bd8iahsjj0jy6ak7yacjgs7ikkq8")))

(define-public crate-libjxl-src-0.2 (crate (name "libjxl-src") (vers "0.2.4") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "10j5jlgmqihkp0cqiwz3sm3vy4xmk8lqb2vx7r0z2wnrj5kgivdb")))

(define-public crate-libjxl-src-0.2 (crate (name "libjxl-src") (vers "0.2.5") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1d92yig1wwsic2wis7wx3lrwbiz4f19k33il0d7cr6y3flf59ywx")))

(define-public crate-libjxl-src-0.2 (crate (name "libjxl-src") (vers "0.2.6") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "18x33c1f9lgrafwlwqffh8y5j7zfrmbsz42xz21mz68hnh94s1ck")))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.0") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1zh0jg9kw6f2v7j9r1xrr8dh9pfy7zlprs8rizn2891cn9a2r2m5")))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.1") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "100czjlm341grxcpshh0w7rm5468hr42rj9w91v66z3qs40fssrh") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.2") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1nd9rsdzicbdngqis818437hb4qk578vl7lhrp99qar22fp9xf6k") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.3") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0gd399ykqv8y2n1hb1f168q4kyzq04xpflwv57bqckhscb4iybyl") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.4") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0iwjbwqa58p9k9nmbckjl4g6rp0qi6lfmi93flf4xy2cfw2vbqmb") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.5") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1k5cb8bj292b9z0m6lgdxljz7fds8v9mi5k307pl0ak3d954zklw") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.6") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "028vlwa7xpy6pm9qx7bx0vhnhaal8glynakzwrja3qyxpgswdvb2") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.7") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "18dy74ly5j5pdyszqyphnrhrajcrfyrs0v3b67c3wkz5nzc53zzk") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3 (crate (name "libjxl-src") (vers "0.3.8") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "121vffh32krwjq3iwy1fxb8j9p2mdr0rviw12mjrp0r6b2rhwry1") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7 (crate (name "libjxl-src") (vers "0.7.0") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0ayy8fg76hv7gf8dgb5wb8891pc5kwzcpbab0pg1z4abhl0mq9gs") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7 (crate (name "libjxl-src") (vers "0.7.1") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0bv1lnzrny1xwx8r647gakcwsxknjmc4wficsggcmx3w9pdw81r2") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7 (crate (name "libjxl-src") (vers "0.7.2") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1fmw9s91am173znxx2v559fm9g9360fsa6h7wlcjrvpr7bvg561m") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7 (crate (name "libjxl-src") (vers "0.7.3") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "19z9cf0w6p0rwvqcazvpf4h0fg6z9q4wmh8j868fzp81j7yg91fl") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7 (crate (name "libjxl-src") (vers "0.7.4") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0wqg3ra5ma787x72zp53yarivfcxpqqiz64ny0nyba1cpyaybnxz") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7 (crate (name "libjxl-src") (vers "0.7.5") (deps (list (crate-dep (name "cmake") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.48") (default-features #t) (kind 1)))) (hash "0fj00dvp1br03hvnv4fmc5r60pm8igxyr01mkq9kdv0vhlwm0kww") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7 (crate (name "libjxl-src") (vers "0.7.6") (deps (list (crate-dep (name "cmake") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.48") (default-features #t) (kind 1)))) (hash "0x8z666q6x20ag3ismp45l8is3cdnlnsiycifzllcnrv0r096vms") (features (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-sys-0.2 (crate (name "libjxl-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.2.5") (default-features #t) (kind 1)))) (hash "0pwflj9fj60cr413kswv0lgfq3har6brmsq9s8fx7ykhmcd6p7z1") (links "jxl")))

(define-public crate-libjxl-sys-0.2 (crate (name "libjxl-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.2.5") (default-features #t) (kind 1)))) (hash "10c0bh0cc3gir709hhg00yb4bp0wp0czf468dwnrq7z7sswsyl92") (links "jxl")))

(define-public crate-libjxl-sys-0.2 (crate (name "libjxl-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.2.6") (default-features #t) (kind 1)))) (hash "0bx8h70fmzvk6n8s02v467zk8vvyczwx9dvx5f6g3dvns0l1qch8") (links "jxl")))

(define-public crate-libjxl-sys-0.3 (crate (name "libjxl-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0ljn9gbxb36xnwfwww3fd1ccq634j3i4ddq54b2md9nbq18n3b85") (links "jxl")))

(define-public crate-libjxl-sys-0.3 (crate (name "libjxl-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "05hjhp4i64klwgl8zdjljsfvzrlgnldl7vpsylgd9am7x03mjb9a") (links "jxl")))

(define-public crate-libjxl-sys-0.3 (crate (name "libjxl-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.3.1") (kind 1)))) (hash "0lwhl6hq6z23w6mi4v106dqifg7ks2hgfgm9kidr9r3h0hz306fx") (links "jxl")))

(define-public crate-libjxl-sys-0.3 (crate (name "libjxl-sys") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.3.2") (kind 1)))) (hash "1a7xyr4bl3hfqp473h890zy0ppqhkx1x8b7v22bza3lak945086z") (links "jxl")))

(define-public crate-libjxl-sys-0.3 (crate (name "libjxl-sys") (vers "0.3.4") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.3.2") (kind 1)))) (hash "0l1ckrb3mpl2h8fx04id5zmfmhzwzmfxjl89h3cv25k2063y1y5i") (links "jxl")))

(define-public crate-libjxl-sys-0.3 (crate (name "libjxl-sys") (vers "0.3.5") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.3.4") (kind 1)))) (hash "1flbd10kpgq2v1bx3klkjpp6rpi9316cpx2nihhn877ihl85zh7z") (links "jxl")))

(define-public crate-libjxl-sys-0.3 (crate (name "libjxl-sys") (vers "0.3.6") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.3.5") (kind 1)))) (hash "0knd74dlmmgfqgv82hfvs7p0hirgfww2lvv3d7r9r2jfxbwl8q11") (links "jxl")))

(define-public crate-libjxl-sys-0.7 (crate (name "libjxl-sys") (vers "0.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.7.0") (kind 1)))) (hash "17529bhcqamv4iz1rzk611valbcfjg2lhbmsh7y42w5lnkl20l3s") (links "jxl")))

(define-public crate-libjxl-sys-0.7 (crate (name "libjxl-sys") (vers "0.7.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.7.3") (kind 1)))) (hash "1hbyg1rnaiqhfjjjj0dxm0xph3m38b7h7px7cpwbppljds3hvf84") (links "jxl")))

(define-public crate-libjxl-sys-0.7 (crate (name "libjxl-sys") (vers "0.7.2") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "libjxl-src") (req "^0.7.6") (kind 1)))) (hash "1258vld9qksh7rcix9k393b1pbkqdz0bgh7xw8mg7szar12lfv9m") (links "jxl")))

