(define-module (crates-io li np) #:use-module (crates-io))

(define-public crate-linprog-0.3 (crate (name "linprog") (vers "0.3.0") (deps (list (crate-dep (name "uuid") (req "^0.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1630c3c2cjpiysy052m9z4bq50xjp99b8g3x1r8w4mhfdd2ppglq") (yanked #t)))

(define-public crate-linprog-0.3 (crate (name "linprog") (vers "0.3.1") (deps (list (crate-dep (name "uuid") (req "^0.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0i9pcjcjkbp5rmfjsa2ysklyr29l8v834lv4rrka29xj875bmhrf")))

