(define-module (crates-io li l_) #:use-module (crates-io))

(define-public crate-lil_http-0.1 (crate (name "lil_http") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "070333zqmdv9akkykgv3xyjk6wr8ya9fg4sap7v8200wii5sg07g")))

(define-public crate-lil_http-0.1 (crate (name "lil_http") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "06a1limlwppkb24h36lndp717rz38868jndv6dahdgqcpik0z1i2")))

(define-public crate-lil_http-0.1 (crate (name "lil_http") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.13") (features (quote ("json"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1mzgp6v3fr0pyviglx58ql5c125k8vad01xwvwpawj4ka8y61lvw")))

(define-public crate-lil_iti-0.1 (crate (name "lil_iti") (vers "0.1.0") (deps (list (crate-dep (name "powershell_script") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0dc2ca1bfg023q88pgdnx3m72m69q84p9g0kwgf3gf7ikihb7w63")))

(define-public crate-lil_iti-0.1 (crate (name "lil_iti") (vers "0.1.1") (deps (list (crate-dep (name "powershell_script") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1h62f2rfyh4bbvc2bn7c25c4m72yn54lnfa8hdmmkyvy772drs98")))

(define-public crate-lil_iti-0.1 (crate (name "lil_iti") (vers "0.1.2") (deps (list (crate-dep (name "powershell_script") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1pl9rkc3wrq8i1xy0r3slxzxka34vvxdkar2wihvrzdpq8x266id")))

(define-public crate-lil_iti-0.1 (crate (name "lil_iti") (vers "0.1.3") (deps (list (crate-dep (name "powershell_script") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1dgdaksg4fzjay13w94ma2384ld8zfgj65cm1amk4c596kv15qb3")))

