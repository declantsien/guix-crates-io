(define-module (crates-io li a-) #:use-module (crates-io))

(define-public crate-lia-plugin-0.1 (crate (name "lia-plugin") (vers "0.1.0") (deps (list (crate-dep (name "lia") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11k6dvbz7ppnxh790mjzcnjpbnra2brzix9qma2gvv59z6ja8mqp")))

