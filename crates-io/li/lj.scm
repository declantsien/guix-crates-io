(define-module (crates-io li lj) #:use-module (crates-io))

(define-public crate-liljag_gps_crate-0.1 (crate (name "liljag_gps_crate") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vwcqi2zx8k11pwrihzvnxhkavz6xmwd4yxkmzvqjywkc0aa3fd0")))

(define-public crate-liljag_gps_crate-0.1 (crate (name "liljag_gps_crate") (vers "0.1.1") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x7nj3szjjm6i8gnj853iin6fap4i5diz8paqmb1ya6qsawnidmk")))

(define-public crate-liljag_gps_crate-0.1 (crate (name "liljag_gps_crate") (vers "0.1.2") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hc1dlpyb5a5fhxwavb3z7g6c6msrykkg5m8mgd9pamf6372fvk6")))

(define-public crate-liljag_gps_crate-0.1 (crate (name "liljag_gps_crate") (vers "0.1.3") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n63zqmw0iga9swzi00q8hgha8msvr69xxa5l7zi40bd13rvwq8f")))

(define-public crate-liljag_gps_crate-0.1 (crate (name "liljag_gps_crate") (vers "0.1.4") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yr70kihrmcjw98mmy3fqj18h23q3zhinwc50nprzh8r86h2n9jv")))

