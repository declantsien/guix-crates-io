(define-module (crates-io li es) #:use-module (crates-io))

(define-public crate-lies-0.0.0 (crate (name "lies") (vers "0.0.0-rc1") (deps (list (crate-dep (name "lies-impl") (req "^0.0.0-rc1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0g93hrgzxnnl2m4md615k6bhdsanb1lp3lb91x25q35wwd56qrhw")))

(define-public crate-lies-0.0.1 (crate (name "lies") (vers "0.0.1") (deps (list (crate-dep (name "lies-impl") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "10bbyzvb0bd6vz1xd3rh9863zvm1hipq925mj8dbvjnyf5vwh28j")))

(define-public crate-lies-0.0.2 (crate (name "lies") (vers "0.0.2") (deps (list (crate-dep (name "lies-impl") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1334nyynxbnfgd600jxpr3qi9p3fw5b2krlfkwvqnxf268cpzk14")))

(define-public crate-lies-0.0.3 (crate (name "lies") (vers "0.0.3") (deps (list (crate-dep (name "lies-impl") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ha5763lm8p21s9c9a8ndnhvrq7jfbr32vgw5f2f1sdrls1cxjkd") (features (quote (("about-per-workspace" "lies-impl/about-per-workspace") ("about-per-crate" "lies-impl/about-per-crate"))))))

(define-public crate-lies-impl-0.0.0 (crate (name "lies-impl") (vers "0.0.0-rc1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ca2vijwakb7psksmfrpizj97s05kn8xp8s3nwvq8lad5wwrhfvx")))

(define-public crate-lies-impl-0.0.1 (crate (name "lies-impl") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1rqmprvlsczjb8n1fqndnpwf6qil3f0gbp8yrrvir7dgi9xz4s9q")))

(define-public crate-lies-impl-0.0.2 (crate (name "lies-impl") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1pwy9vbhm2f1sf0r7mpldvnq6m1k96nzs9mbzivkalq10d6qdbrb")))

(define-public crate-lies-impl-0.0.3 (crate (name "lies-impl") (vers "0.0.3") (deps (list (crate-dep (name "cargo_metadata") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s18qvyajidrw0hhfkcwnyvk2kv10gkckvg8xjwkrh90pj5gmjvs") (features (quote (("about-per-workspace") ("about-per-crate"))))))

