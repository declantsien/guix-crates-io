(define-module (crates-io li lv) #:use-module (crates-io))

(define-public crate-lilv-0.0.1 (crate (name "lilv") (vers "0.0.1") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2-urid") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "04fkrwv8wdqp3bkqz5naic84klqd9bn8ykk8dfn35i52ylm21bzq")))

(define-public crate-lilv-0.0.2 (crate (name "lilv") (vers "0.0.2") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2-urid") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0f86fpri787l56bcispfjlvc7fawk8dzdg7xlh3i9f0mm18aqq68")))

(define-public crate-lilv-0.0.3 (crate (name "lilv") (vers "0.0.3") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2-urid") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0hpi5r825b6780njdxmzf9228cp1vg3p4cb143jfdrb1h8l53fpq")))

(define-public crate-lilv-0.0.4 (crate (name "lilv") (vers "0.0.4") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1rzrpkraf94f3icpcy3zx9wnrlddgrnv11mj82q1rbca6ng7530s")))

(define-public crate-lilv-0.0.5 (crate (name "lilv") (vers "0.0.5") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "00bsmcrmhigxa8drkwjjjry607iarjv7db483mi5l64pdz4dx0bf")))

(define-public crate-lilv-0.0.6 (crate (name "lilv") (vers "0.0.6") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1bfbaxd7r0a0x9vi10xhizmj9b74wdfxxs3jyq1ip7c4i6561l4c")))

(define-public crate-lilv-0.1 (crate (name "lilv") (vers "0.1.0") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0b5f9g9x2dk90nkplcr3lrm9ndydhl2p5bsrlrhh2k3y3d1vq454")))

(define-public crate-lilv-0.2 (crate (name "lilv") (vers "0.2.0") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0bw69m9slyb5ymski655xrffs73b94whg55jh10hhx2kakg44zi4")))

(define-public crate-lilv-0.2 (crate (name "lilv") (vers "0.2.1") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "08qmgg0c68sr85zsx4xwjv06qbvi00ydf9cpwqi38pl0q8h8ljb7")))

(define-public crate-lilv-0.2 (crate (name "lilv") (vers "0.2.2") (deps (list (crate-dep (name "lilv-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0yij4pj82lbk4zbk3kj5630yx5y9723qcy3m6y4gb8y79jg5p2xs")))

(define-public crate-lilv-sys-0.1 (crate (name "lilv-sys") (vers "0.1.0") (hash "0yy9wh9n6pcnkpmp9bi48v9dyq352g8sy14xg2ca0vdm6ym10v5g")))

(define-public crate-lilv-sys-0.1 (crate (name "lilv-sys") (vers "0.1.1") (hash "0h4yqnkv9jp1dvch7203nmvj8cyhz1zpnx7nxplnk6iss1k6yn2p")))

(define-public crate-lilv-sys-0.1 (crate (name "lilv-sys") (vers "0.1.2") (hash "0dylr8vigp7rifr63846bdmcimkdpdk344kdfrsg8ipxm2rjnivd")))

(define-public crate-lilv-sys-0.2 (crate (name "lilv-sys") (vers "0.2.0") (deps (list (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r36gvf3dpx96c45j2dd5mllanbjpjnf9ahdxknnz2ypyn2kry3p")))

(define-public crate-lilv-sys-0.2 (crate (name "lilv-sys") (vers "0.2.1") (deps (list (crate-dep (name "lv2_raw") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g7mb1nnpq098pmf2xyhph1hxyv5kibis5wzh40jvdldvdpnmrdf")))

