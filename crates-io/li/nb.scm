(define-module (crates-io li nb) #:use-module (crates-io))

(define-public crate-linbra-0.0.1 (crate (name "linbra") (vers "0.0.1") (hash "1ck7c3k4p77kc1rj338h0yl9n4g3wsjhas5872szxvzhhx7mgp11")))

(define-public crate-linbra-0.0.2 (crate (name "linbra") (vers "0.0.2") (hash "10l0w5gmy8d8ripcy57scayj4y6gd9bbh4ar7h21nyxqvp36yc8l")))

(define-public crate-linbra-0.0.3 (crate (name "linbra") (vers "0.0.3") (hash "0768s0lvnbb5kxx5q8i3qgdh5p3s8ycxipzmwsnxpxbmpn75bx7x")))

