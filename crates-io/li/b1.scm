(define-module (crates-io li b1) #:use-module (crates-io))

(define-public crate-lib1232132-1 (crate (name "lib1232132") (vers "1.0.0") (hash "0s9a9n8lddx2r0h09c2nnbp117388r6f13xpdi2ccvkanq3xkj29")))

(define-public crate-lib1232132-1 (crate (name "lib1232132") (vers "1.0.2") (hash "0ic7r2qrrzsim1jwadh2zrx6qlpy43lgqwyqmzzz7ww4v7anwjgb")))

(define-public crate-lib1232132-1 (crate (name "lib1232132") (vers "1.0.1") (hash "07d71wq8gnxqjbns4wk71xdbh6qskmy3vk7l1wagdal0pmavhfra")))

(define-public crate-lib1337patch-0.1 (crate (name "lib1337patch") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.7.1") (default-features #t) (kind 2)))) (hash "1q351bia45wjq24rlya0pz3sclysd2171fyb2y6cc3pjmn8nv1zi") (yanked #t)))

(define-public crate-lib1337patch-0.1 (crate (name "lib1337patch") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3.7.1") (default-features #t) (kind 2)))) (hash "1xp6f7dmfqw5j7fi3jdd788fzfnwydkcfwpwvbz9ggil3w3iiglp") (yanked #t)))

(define-public crate-lib1337patch-0.1 (crate (name "lib1337patch") (vers "0.1.2") (deps (list (crate-dep (name "tempfile") (req "^3.7.1") (default-features #t) (kind 2)))) (hash "06jxhp5snk7ql62ka7mlx6npxzx4vw97vpl7ryhq7m6pgmvpp9n9") (yanked #t)))

(define-public crate-lib1337patch-0.2 (crate (name "lib1337patch") (vers "0.2.0") (deps (list (crate-dep (name "tempfile") (req "^3.7.1") (default-features #t) (kind 2)))) (hash "0lb99pk8ahvmz6xc72a6mqw8fj2r96dq008yiwz17l250b92np2q") (yanked #t)))

(define-public crate-lib1337patch-0.2 (crate (name "lib1337patch") (vers "0.2.1") (deps (list (crate-dep (name "tempfile") (req "^3.7.1") (default-features #t) (kind 2)))) (hash "1jvqvksc4lzci6acbpcfni3km8hl4x9apfqk7mh80m35z61h1m9m") (yanked #t)))

(define-public crate-lib1337patch-0.2 (crate (name "lib1337patch") (vers "0.2.2") (deps (list (crate-dep (name "tempfile") (req "^3.8") (default-features #t) (kind 2)))) (hash "0lysl7dn01gz32z5rr60imzwiwggk0h1q0li7klqq5sayvqcas8a")))

(define-public crate-lib1MyTest-0.1 (crate (name "lib1MyTest") (vers "0.1.0") (deps (list (crate-dep (name "lib2MyTest") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1crb597wakygwvrbya9g95p2hg34w5j0xk9ip3qllyh5dx7gl1pl")))

(define-public crate-lib1testingDep-0.1 (crate (name "lib1testingDep") (vers "0.1.0") (deps (list (crate-dep (name "lib2") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yivzpil22q49hpckd13z33vmx490xkf8fd3aq5ffjr19k3d77xs")))

(define-public crate-lib1testingDep-0.2 (crate (name "lib1testingDep") (vers "0.2.0") (hash "1ycv3bqmx2lr2w2pxwf43c2w9m7vky0xlbz1bl0nhhvicn61hc4d") (yanked #t)))

(define-public crate-lib1WithModTest-0.1 (crate (name "lib1WithModTest") (vers "0.1.0") (hash "10wdjxpj3b7zqmmyin7h1lan42whbma338hk960zvvj4iqvc7fvk")))

