(define-module (crates-io m6 #{80}#) #:use-module (crates-io))

(define-public crate-m68000-0.1 (crate (name "m68000") (vers "0.1.0") (hash "1892qq7585ic6l6r73jc2v7a6lybj79c9gbi4vzp1avf1jx55gdq") (features (quote (("cpu-scc68070") ("cpu-mc68000"))))))

(define-public crate-m68000-0.1 (crate (name "m68000") (vers "0.1.1") (hash "1wl09kw4chif3qxrqrl6cb24ab6jw8iwj1ir2akz8v159cw7qlcm") (features (quote (("cpu-scc68070") ("cpu-mc68000"))))))

(define-public crate-m68000-0.2 (crate (name "m68000") (vers "0.2.0") (hash "1zpryzckymplkxrwlyg43np1abmln30h15c13ck77qx9x1cq58md") (features (quote (("ffi") ("default"))))))

(define-public crate-m68000-0.2 (crate (name "m68000") (vers "0.2.1") (hash "1v8yqz1aflzcgb3vsy0pphc278vn37rr140i4l52via4j1s0fy7s") (features (quote (("ffi") ("default"))))))

