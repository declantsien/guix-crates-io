(define-module (crates-io m6 co) #:use-module (crates-io))

(define-public crate-m6coll-0.1 (crate (name "m6coll") (vers "0.1.0") (deps (list (crate-dep (name "m6arr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "m6bitmap") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "m6entry") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sk7z6v6mb9iakm1iblgm7d0nh0py0wkim9k6mcfvy5mnyv8yk5g")))

(define-public crate-m6coll-0.1 (crate (name "m6coll") (vers "0.1.1") (deps (list (crate-dep (name "m6arr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "m6bitmap") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "m6entry") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16x1wim7kwhisfsjqg1cwvmln7vky3dv403nk996yq88y00y9wi3")))

(define-public crate-m6coll-0.2 (crate (name "m6coll") (vers "0.2.0") (deps (list (crate-dep (name "m6arr") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "m6bitmap") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "m6entry") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xi61bj6g7wdj7kqpifqzmzh3a99dglghjcvxffi4zrar4pszpb0")))

