(define-module (crates-io m6 st) #:use-module (crates-io))

(define-public crate-m6stack-0.1 (crate (name "m6stack") (vers "0.1.0") (hash "0inkq7nqcdy3020s3y801ci8lmx38k6mggbv3mj8b19mzykwzr5x")))

(define-public crate-m6stack-0.2 (crate (name "m6stack") (vers "0.2.1") (hash "00j7zwjq7qww1r04c1vpg4i9z48ak4fbvwyfzi2g3kqvagmvsnrv")))

