(define-module (crates-io m6 #{1-}#) #:use-module (crates-io))

(define-public crate-m61-modulus-0.1 (crate (name "m61-modulus") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "17c6l07661paz6dl0xhcbiz90746jcjdgrkzf9vva34ynkc1bhqj") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-m61-modulus-0.1 (crate (name "m61-modulus") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "0k54wv9v9a7j3xy66gscmfz4xqq4q1agv0sbffcfclg8sqd6pqvz") (features (quote (("std") ("nightly") ("default" "std"))))))

