(define-module (crates-io m6 ar) #:use-module (crates-io))

(define-public crate-m6arr-0.1 (crate (name "m6arr") (vers "0.1.0") (hash "134diw4lq4mv7q1cvh5v97vb833sgrs510rkjghl4wjvnpjv401d")))

(define-public crate-m6arr-0.1 (crate (name "m6arr") (vers "0.1.1") (hash "0crsgcglzxgp63j424w3wzpw7rlkdzknaaaybvzyl01x65z1ll0d")))

(define-public crate-m6arr-0.2 (crate (name "m6arr") (vers "0.2.0") (hash "0xd4xvh59zf4d25k4hpn9yh9932dg31r8hdiafp5i9yq1m23gvyf")))

