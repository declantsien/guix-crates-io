(define-module (crates-io m6 -k) #:use-module (crates-io))

(define-public crate-m6-key-set-0.2 (crate (name "m6-key-set") (vers "0.2.1") (deps (list (crate-dep (name "indexmap") (req "^1.6.2") (default-features #t) (kind 0)))) (hash "1g2f62254g8bwyprqggz5qsqqv0s1ma1y3s401j2ig74kq24jn3p")))

