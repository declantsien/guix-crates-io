(define-module (crates-io v_ hi) #:use-module (crates-io))

(define-public crate-v_hist-0.1 (crate (name "v_hist") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "174wrii50clah713jvps2qxw72n8d607cbxfih8vphqvr2yq1vvg")))

(define-public crate-v_hist-0.1 (crate (name "v_hist") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1240nhmh63c36w1m7y5jfrszxap59ca8nn8qlzs4ip2j1bph9brg")))

(define-public crate-v_hist-0.1 (crate (name "v_hist") (vers "0.1.2") (deps (list (crate-dep (name "console") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1c8wdl4zvq83jikiky45r2phc2qw9kd96sbigfixsb07gqna9r8i")))

(define-public crate-v_hist-0.1 (crate (name "v_hist") (vers "0.1.3") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1vvkgk39v6bpz9miyswkc8q9s04pvmga8730ph54ga9hqffhb0jc")))

