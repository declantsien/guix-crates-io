(define-module (crates-io v_ au) #:use-module (crates-io))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.0") (hash "08cf8qd6x647pv7mrcrdbpr2b4l144c4071pwjym61dg6lgym4jr")))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.1") (hash "18yan45lwzp3j7j3gwzh1i8vk5jkhy4igdssg0llcvl4f22w115h")))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.2") (hash "1wasxmi4b43mb13hhj674vmx0nx9bjy1nc4j9c28gvcsg3pvz9p5")))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.3") (hash "09s12mqfp24gq7pw8g0cfawp1zqclng7vr79xfxhxmb3i4c1l92y")))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.4") (hash "0jjn73y0yhk2aj8kfwa8jhwrfwwg3l0yj219ib6qxxl2nmxyhpbq")))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.5") (hash "0k7qv55li6n8f4mrl5yjhddybaxzs4sqd2i6vxdqx6ajqi6zlhms")))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.6") (hash "1dhbg09q3ln2mi6flzz2pyk97x430inpxm5gg2r8x79lb6sbcg53")))

(define-public crate-v_authorization-0.1 (crate (name "v_authorization") (vers "0.1.7") (hash "0hxacxzdny7cpfxp10svkqfahhhv8jf2vjlrgz3mggdf4d0m1y03")))

(define-public crate-v_authorization-0.2 (crate (name "v_authorization") (vers "0.2.0") (hash "0iayp0c74nr182r24hhcypd5zhxw29kif1vbw01vqz4ab8l6gqx9")))

(define-public crate-v_authorization-0.3 (crate (name "v_authorization") (vers "0.3.0") (hash "15wsn81gvjrb4fxpma6a8z96vypw0xlvkklrg25rz52203zhvkj7")))

(define-public crate-v_authorization-0.4 (crate (name "v_authorization") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "039hsz2nxaqr3zsv1d3n3f1iis1jq0jab9a9p3kp3v6agzq9mlgf")))

