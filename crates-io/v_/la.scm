(define-module (crates-io v_ la) #:use-module (crates-io))

(define-public crate-v_latexescape-0.3 (crate (name "v_latexescape") (vers "0.3.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.1") (default-features #t) (kind 1)))) (hash "01iqh02rslbcvnqdkdcvqxfanbhia4k9ksccf2asfr9wl687v8w9")))

(define-public crate-v_latexescape-0.4 (crate (name "v_latexescape") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.1") (default-features #t) (kind 1)))) (hash "0sd4pgqsin5anc67ml9i0i2wkmw8dbb8sf3d83f184n3jhlwx24d")))

(define-public crate-v_latexescape-0.4 (crate (name "v_latexescape") (vers "0.4.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.1") (default-features #t) (kind 1)))) (hash "090w8gwryr4np5hlinwsjvp32039wihayzv8lf14d9cbq8kna93h")))

(define-public crate-v_latexescape-0.4 (crate (name "v_latexescape") (vers "0.4.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 1)))) (hash "15y71a10lyarwjyx3dbn04sabryd7j50rf6qsaib4i2x32vm96k2")))

(define-public crate-v_latexescape-0.4 (crate (name "v_latexescape") (vers "0.4.3") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 1)))) (hash "0zw59q7a68j7wfah7bvk74b419b00a6vl044m9p1yfb0qx8ynfrr")))

(define-public crate-v_latexescape-0.4 (crate (name "v_latexescape") (vers "0.4.4") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 0)))) (hash "0b1lpcw4dbh31lcb45jimn11rf7566px4k80kayvw98rs5dzpg9m")))

(define-public crate-v_latexescape-0.4 (crate (name "v_latexescape") (vers "0.4.5") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 0)))) (hash "0ghqmnyham8rccvf8rz6y1qijvs2llysb7slbidvycrc0agsyvk6")))

(define-public crate-v_latexescape-0.5 (crate (name "v_latexescape") (vers "0.5.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qc8bihi580bvcngayr1jmjvlvywkisn5i9106klsvalw7dgjplr")))

(define-public crate-v_latexescape-0.6 (crate (name "v_latexescape") (vers "0.6.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.9") (default-features #t) (kind 0)))) (hash "0iyrfz4dfr5c339q5z2grnbbilh9kam2vhzwks6rfk8488gpk7ql")))

(define-public crate-v_latexescape-0.9 (crate (name "v_latexescape") (vers "0.9.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.12") (kind 0)))) (hash "07bnrq7dq9ngqwbkw70azl4agm3b4yhniv658vjm0kyczgy88mwy") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_latexescape-0.9 (crate (name "v_latexescape") (vers "0.9.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.13") (kind 0)))) (hash "1h10x0n402gbkf2h2p0fnmhlrlmvwdvvxrcy309739jamxvad6z6") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_latexescape-0.9 (crate (name "v_latexescape") (vers "0.9.3") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.13") (kind 0)))) (hash "15pxp1zgsmdlvqd06chy709spyic9dnn193fmnphrzpvk3jhlnlp") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_latexescape-0.9 (crate (name "v_latexescape") (vers "0.9.4") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.13") (kind 0)))) (hash "018kfn9c2nkaw1xrrknpm12qzzwxa81p3vllxjjn7v7n8r50qkrc") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_latexescape-0.10 (crate (name "v_latexescape") (vers "0.10.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.14") (kind 0)))) (hash "18v34r4wr862zcly7c11795cizvpd8qryp7dckrmj9qs42qp8ncw") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_latexescape-0.11 (crate (name "v_latexescape") (vers "0.11.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.15") (kind 0)))) (hash "09jqik9vhzwk890w0sazgzm7fxa756irznz5w59d9129gp0n92nc") (features (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_latexescape-0.12 (crate (name "v_latexescape") (vers "0.12.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.16.0") (kind 0)))) (hash "02b02pigkwfwwj4vrikjisgw6fw77dsn1frpzchawda9y09l29bj") (features (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_latexescape-0.12 (crate (name "v_latexescape") (vers "0.12.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.16.1") (kind 0)))) (hash "14ham2gcsy8c68i5i504zgwdnli5d6ihr26d5wsqa1ijsqzrg9la") (features (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_latexescape-0.13 (crate (name "v_latexescape") (vers "0.13.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.17") (default-features #t) (kind 0)))) (hash "0drjxjrhj42vhj78vyhmzj9w7s1194q5cksk6bmvccv2vrw9frg7")))

(define-public crate-v_latexescape-0.13 (crate (name "v_latexescape") (vers "0.13.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.18") (default-features #t) (kind 0)))) (hash "0pcbya5sp15x5vlrm7r8f899gdzpmzbmmhkm870prfrkrsxybj2y")))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.0") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "0prhb3lhpzihbmycfk9pnxssa5gaii651fdm49kyzl70bnc6kiz3") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.1") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "0h73dzflwr53h2v9359jbw7ls6yp9zc5pw3z9p23knrmalnhjv83") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.2") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "0yd2fm8bwix87xh4pnhzl2ckw3bxwydq9gvx4jk0z1i3hz82dv4j") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.3") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "1cjb44z1cdk30753y5d9cs15jqyflj0h61ysw8szy0sazx5icvaq") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.5") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "02ws66yhggnpyn5b06yq5qvpbdwhlr5zlp7q36y4iwr1z95zni8f") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.6") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "15gb2qxvl9yjbc9h5zpshxa86w9am4ng3rdzpyh8chdkasb1jfrl") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.7") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "1vdzq2bvvzrgnj3fy8nn19ck9jjxw7haimh64clbi33icx94klc3") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_latexescape-0.14 (crate (name "v_latexescape") (vers "0.14.8") (deps (list (crate-dep (name "buf-min") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1wbgv7k5gm2dvnsvji6rh9n7nj9bydka4lxdcysfzmfpj43kx6mf") (features (quote (("bytes-buf" "buf-min"))))))

