(define-module (crates-io v_ js) #:use-module (crates-io))

(define-public crate-v_jsonescape-0.1 (crate (name "v_jsonescape") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.11") (default-features #t) (kind 0)))) (hash "0xn26gf8j0g95rlir0yhgwrj6mpqmspl6vdc4q96274p6ldbzg16")))

(define-public crate-v_jsonescape-0.2 (crate (name "v_jsonescape") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.12") (kind 0)))) (hash "19ni5frwyzf04j5ggwmlhx0pvv4n11lh92wpxxwv12hlhgmghchn") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2 (crate (name "v_jsonescape") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.12") (kind 0)))) (hash "0a0idwg1ppd7xwn73ay4q9f08h5bkkdrrmxnsjf62c1arb5537kw") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2 (crate (name "v_jsonescape") (vers "0.2.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.13") (kind 0)))) (hash "0z51kg32ky8l2zcspcikg3jg4h93lir9212wjzq6awij60zszf91") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2 (crate (name "v_jsonescape") (vers "0.2.3") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.13") (kind 0)))) (hash "020zs2jcbzfwcry6z3xd5xl9163y913bphkiym1gs2978pq2sym6") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.2 (crate (name "v_jsonescape") (vers "0.2.4") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.13") (kind 0)))) (hash "121yzyx4yg6vnz0rf5g4y2lgqmcaybxgx7a0g690rrm7f5c88h6c") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.3 (crate (name "v_jsonescape") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.14") (kind 0)))) (hash "0ljcnra58yk2c0yrwk1wjq6ckx48rhm90da32v0qig1slaabl48l") (features (quote (("default" "bytes-buf") ("bytes-buf" "v_escape/bytes-buf"))))))

(define-public crate-v_jsonescape-0.4 (crate (name "v_jsonescape") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.15") (kind 0)))) (hash "1s94y37viysb3sd9nzmf8yanhgc5sqqrs46phr24x6fpysv0nfks") (features (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_jsonescape-0.5 (crate (name "v_jsonescape") (vers "0.5.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.16.0") (kind 0)))) (hash "1d8asbsaqxwarjzjgwwqwdhjkkzdqrqb3p7j3hd36im3wgb53spg") (features (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_jsonescape-0.5 (crate (name "v_jsonescape") (vers "0.5.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.16.1") (kind 0)))) (hash "04x17s15swi6vligp8qh532a5abcw0y6822ly3825fpsvph4b743") (features (quote (("default" "bytes-buf-tokio2") ("bytes-buf-tokio3" "v_escape/bytes-buf-tokio3") ("bytes-buf-tokio2" "v_escape/bytes-buf-tokio2"))))))

(define-public crate-v_jsonescape-0.6 (crate (name "v_jsonescape") (vers "0.6.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.17") (default-features #t) (kind 0)))) (hash "1dpaqmq99vanz0lcc0c2n00h5q4w0zmr63wcyj6a248gns3zrdcp")))

(define-public crate-v_jsonescape-0.6 (crate (name "v_jsonescape") (vers "0.6.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.18") (default-features #t) (kind 0)))) (hash "1z92gyd35aj9rgcc266b2mjcs7nlq0a436zsfkgxz58v6nqz3h39")))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.0") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "0bzhaqzifscj80d9c7xs4708pa77czdcfkmsrm6cn4ymbkvaa427") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.1") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ayhsx05388n93hiz7zgx4l2j8kx7ld293w3ybnhm9s2mgc6qfjc") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.2") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "14vj1wvmkw82xjwhlbazkk8fl6j5q3wdhry80m31hqyjqhc4l8sm") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.3") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "11ywb100cs68cxzd4xn2yp4s4q8hj24wj4wr5h4p18m5lxy98zia") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.5") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "0avr8mjzymdfa23p9ixs0xaw8rrvssp7ynapv2l4d3nnz4ydvl7z") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.6") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "0zqq753vy547igpxc1d0c8dkz0yvjp26430v0n9mjvpg6z104c4z") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.7") (deps (list (crate-dep (name "buf-min") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)))) (hash "1awwqi8p7czhsx21ydzij3a6y3idw4zccyzsd9hjxcsp76glmfsn") (features (quote (("bytes-buf" "buf-min"))))))

(define-public crate-v_jsonescape-0.7 (crate (name "v_jsonescape") (vers "0.7.8") (deps (list (crate-dep (name "buf-min") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1s0hpfkpwf7agr34fnn4qlqxh9hxy5qnh6i3qd40r8ab8v61k0my") (features (quote (("bytes-buf" "buf-min"))))))

