(define-module (crates-io v_ az) #:use-module (crates-io))

(define-public crate-v_az_lmdb-0.1 (crate (name "v_az_lmdb") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "lmdb-rs-m") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "v_authorization") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lpn99a2sphhyddh82z1qcd5lqqvzz4krq0nl3gdr18wvj5x92nh")))

