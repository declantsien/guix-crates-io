(define-module (crates-io v_ sh) #:use-module (crates-io))

(define-public crate-v_shellescape-0.3 (crate (name "v_shellescape") (vers "0.3.2") (deps (list (crate-dep (name "v_escape") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.1") (default-features #t) (kind 1)))) (hash "0472a6rmg8lpafsdlsz8841k28k1qr3diwj6i1aha3ab3gza3wlb")))

(define-public crate-v_shellescape-0.3 (crate (name "v_shellescape") (vers "0.3.3") (deps (list (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 1)))) (hash "1ak8x43dv7rq3wvj6rjji0bfy4w57id2sjssl7rsv8x87c5b3ds1")))

(define-public crate-v_shellescape-0.3 (crate (name "v_shellescape") (vers "0.3.4") (deps (list (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 0)))) (hash "0bpzn0bdgi8gf66fm6h1favr2zhbxq6a71y068kff2awpj3vb8yx")))

(define-public crate-v_shellescape-0.3 (crate (name "v_shellescape") (vers "0.3.5") (deps (list (crate-dep (name "v_escape") (req "^0.7") (default-features #t) (kind 0)))) (hash "1sxr6waz0bgmsnv44drxw29q4r1wlyx5kdlm0npvn4ynrqr8cz25")))

