(define-module (crates-io wz -p) #:use-module (crates-io))

(define-public crate-wz-publish-test-0.1 (crate (name "wz-publish-test") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03zrbm74nayf7yayl57ilzrvmdypmg7az0l3rrrm97byk2bacxkr")))

(define-public crate-wz-publish-test-0.1 (crate (name "wz-publish-test") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "01ccpydy1cqckpdxzbqdqkv5pxbyyiclvxdc57yv6n7vf11ixvv2")))

(define-public crate-wz-publish-test-0.1 (crate (name "wz-publish-test") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0340ijn6bcr5balqyc8am6pixr85n266a2km2y6bd7mi6lfn4q3g")))

(define-public crate-wz-publish-test-0.1 (crate (name "wz-publish-test") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0p8yham2ml52z2kijv3q5g37dyypgklrxhs10lqima00kfclgjpz")))

(define-public crate-wz-publish-test-0.1 (crate (name "wz-publish-test") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rf5gqm3i884pddpv9bcryq3dbfhni3li8jgr0pikmfgsqdklybw")))

