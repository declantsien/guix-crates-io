(define-module (crates-io wz -c) #:use-module (crates-io))

(define-public crate-wz-conf-0.1 (crate (name "wz-conf") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (kind 0)))) (hash "0xhs5605dfm0q4f1mvylk90nria2g69rifzvqswmv9ghmqays4hw")))

(define-public crate-wz-conf-0.1 (crate (name "wz-conf") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^3.2.4") (default-features #t) (kind 1)) (crate-dep (name "clap_mangen") (req "^0.1") (default-features #t) (kind 1)))) (hash "1d4dgcc4mps0jncndfc8xbnh56iwzqjnp3ga9vn3gqxlfspspwm0")))

(define-public crate-wz-conf-0.1 (crate (name "wz-conf") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^3.2.4") (default-features #t) (kind 1)) (crate-dep (name "clap_mangen") (req "^0.1") (default-features #t) (kind 1)))) (hash "00b8b2y6vg3mm35d2iplffj04whlihxzjkkl50zb4kw1hw4mxb0w")))

(define-public crate-wz-conf-1 (crate (name "wz-conf") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (kind 0)))) (hash "1y4nwhp8lvn85nwgqrj1yg30c3jrf3dqh2mqlzkiinmh6000s7hr")))

(define-public crate-wz-conf-1 (crate (name "wz-conf") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (kind 0)))) (hash "16jccrh9yzm547w5fxm6ncppwfginpgsnryr2y1y4lb2k7j4aaa7")))

(define-public crate-wz-core-0.1 (crate (name "wz-core") (vers "0.1.0") (hash "0dy1pgk5l3amdgkfbrcny0rfajrzi7wd4inyxz4pm8xr3rmqczxw")))

(define-public crate-wz-core-0.1 (crate (name "wz-core") (vers "0.1.1") (hash "0jgmd6xyz791j775zbnrfrs4pfqwrxnxjhlngg5h8gzxdhldq5rs")))

(define-public crate-wz-core-1 (crate (name "wz-core") (vers "1.0.0") (hash "1hxc4b1s9kgvymlhlww41zm8ffhj6sgsy535ly20q40vxfn7iqgb")))

