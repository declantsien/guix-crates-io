(define-module (crates-io wz iu) #:use-module (crates-io))

(define-public crate-wzium-library-0.0.2 (crate (name "wzium-library") (vers "0.0.2") (hash "0fxkcc82d6h732s2n6grls20zm2n0s17ln1pp1ihwvfihh4ingbg")))

(define-public crate-wzium-library-0.0.3 (crate (name "wzium-library") (vers "0.0.3") (hash "1sdib361zrb89cw9ij7xpm7hq6i3rp6q9341ydrhg0lb3lqhq0vr")))

(define-public crate-wziumLibrary-0.0.1 (crate (name "wziumLibrary") (vers "0.0.1") (hash "1ifqgymyilx74mdxg84cmy3azd5xj0byhxkqpj4chhvbbvmdb8zj")))

(define-public crate-wziumLibrary-0.0.2 (crate (name "wziumLibrary") (vers "0.0.2") (hash "1rhphmcwc5g4jcnc0arc144dy9r280wy777scph6ic5v3hy7a6w9")))

