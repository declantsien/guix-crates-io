(define-module (crates-io ug uu) #:use-module (crates-io))

(define-public crate-uguu-1 (crate (name "uguu") (vers "1.0.0") (deps (list (crate-dep (name "rodio") (req "^0.8.1") (features (quote ("mp3"))) (kind 0)))) (hash "17bjdm1k9cq0wakihl1id06cyr6n615mcbb6axbb8vjgmn2z28vf")))

