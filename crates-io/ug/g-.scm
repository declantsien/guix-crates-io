(define-module (crates-io ug g-) #:use-module (crates-io))

(define-public crate-ugg-types-0.1 (crate (name "ugg-types") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1w9qqgwn46c3n9ayk3icqxy10bi8i01q16n32pd4ai1lrq9lzcdn") (features (quote (("client"))))))

(define-public crate-ugg-types-0.1 (crate (name "ugg-types") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0m11jvp8yc77l1fy43j0h3sdlqvfl2s29k3svvb7iy72hy7p5b8z") (features (quote (("client"))))))

(define-public crate-ugg-types-0.1 (crate (name "ugg-types") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0z7nzfqwzyl7pbyd5r1pb59b2wjx4r92im7z3r7886hn8gmwigy6") (features (quote (("client"))))))

(define-public crate-ugg-types-0.2 (crate (name "ugg-types") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1wq7jsixjbwx7141plqx9y31kz2laibgmkqvd7dfsjd0jhm1sh7x") (features (quote (("client"))))))

(define-public crate-ugg-types-0.2 (crate (name "ugg-types") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1npyjk9ghdbjv5kkq6bdjhhmvzicv22madgswphdk0pm1f8pgjxb") (features (quote (("client"))))))

(define-public crate-ugg-types-0.2 (crate (name "ugg-types") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "19fdmqpgyzdbqz12c3xi623s4v8a4dj7nk572g7fa4kziw7sfynb") (features (quote (("client"))))))

(define-public crate-ugg-types-0.2 (crate (name "ugg-types") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1ny4a193k017ixqg7sxdfs3lddvfrszjdpgj4cr20jz769dky5b7") (features (quote (("client"))))))

(define-public crate-ugg-types-0.2 (crate (name "ugg-types") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1ba3j0kmc7jg4brmxvxzlrsbf64cz4jma53js28azrjqyhrx3j35") (features (quote (("client"))))))

(define-public crate-ugg-types-0.3 (crate (name "ugg-types") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1q8v5w2z0r6v9d9i3np07pzg7d24xhggb6zpwpqq59p8x78yjyqm") (features (quote (("client")))) (rust-version "1.68.2")))

(define-public crate-ugg-types-0.4 (crate (name "ugg-types") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cb3dhyb6zad66mibpsdk343yyr7r50f6ifkdvhn2h4niyv4i6s8") (rust-version "1.74")))

(define-public crate-ugg-types-0.4 (crate (name "ugg-types") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1p2szm3xk8bziz5ia1yr64mjn92322l19xfd9v7ag3l5m2lxgvd7") (rust-version "1.74")))

(define-public crate-ugg-types-0.5 (crate (name "ugg-types") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0k5qnvlr2l8bylq4gy03wja9fflnlngc652rhsz1dsfw8zwkf97m") (rust-version "1.74")))

(define-public crate-ugg-types-0.5 (crate (name "ugg-types") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bjyjrh2g969q747wn7jia7b3978r10894qrfcg9cdbvqvz48h55") (rust-version "1.74")))

(define-public crate-ugg-types-0.5 (crate (name "ugg-types") (vers "0.5.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "01j953hkwlmqx7basp31cq2r1zpz50kgh4hrkz7wdjci3jlzvq8k") (rust-version "1.74")))

