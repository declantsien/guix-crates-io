(define-module (crates-io er re) #:use-module (crates-io))

(define-public crate-erreport-0.1 (crate (name "erreport") (vers "0.1.0") (hash "1ymsmg37ffizb87li9hii86kdp3srdr3v1yz6rg8sa74zv7ip1mn")))

(define-public crate-erreport-0.1 (crate (name "erreport") (vers "0.1.1") (hash "056bil62zwdlwd7rizmy1db27dq2g8ijdlxl4kmfqr89hlh68nx3")))

(define-public crate-erreport-0.2 (crate (name "erreport") (vers "0.2.1") (hash "1gb1028jfn453z7vny94z5z17j3ypi79djmy2lc5hlpbqihm0krs")))

(define-public crate-erreport-0.3 (crate (name "erreport") (vers "0.3.0") (hash "1xlxbqyqgbds77cf4ccxdfpxw48imzxw9706mcmkcvssa4p62b4i")))

(define-public crate-errer-0.1 (crate (name "errer") (vers "0.1.0") (deps (list (crate-dep (name "termcolor") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1f50dd482n9c9q4rnv71q2swh23dn7a1q4q7d5azcm3p521vyn8h")))

(define-public crate-errer-0.1 (crate (name "errer") (vers "0.1.1") (deps (list (crate-dep (name "termcolor") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0z8w2dcyl8x01nj0sx3ly1nbl82py12swdx1yy1m2p9iya4h10sv")))

(define-public crate-errer-0.12 (crate (name "errer") (vers "0.12.1") (deps (list (crate-dep (name "termcolor") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0h8lqi6gbh4rdjhpb9cbl26745d60vr3bg8h3ihw9l0z0zpli445")))

(define-public crate-errer-0.13 (crate (name "errer") (vers "0.13.0") (deps (list (crate-dep (name "termcolor") (req "^1.0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1haizd2kp5abqdfimxf9flc88qaf8mvx7ghldb25xvhwdllg2c4v") (features (quote (("print-error" "termcolor"))))))

(define-public crate-errer_derive-0.1 (crate (name "errer_derive") (vers "0.1.0") (deps (list (crate-dep (name "errer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1rsa4l3n5hh27f5jz435f65k7si0d3zcymgqdl8v940pykfz2n6m")))

(define-public crate-errer_derive-0.1 (crate (name "errer_derive") (vers "0.1.1") (deps (list (crate-dep (name "errer") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1sqpafhqr4dz6gagvl00adm5a4nh2v0v7j9xbwpg7zx1mbvdlpz8")))

(define-public crate-errer_derive-0.12 (crate (name "errer_derive") (vers "0.12.1") (deps (list (crate-dep (name "errer") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1i52snsy00fa3rzh2smcsv7jb287li7jysh8iwgx0qzcvv90nadr")))

(define-public crate-errer_derive-0.12 (crate (name "errer_derive") (vers "0.12.2") (deps (list (crate-dep (name "errer") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0izcn5mvzliwm2nxmy9bf2is1fdxw36qiw3iy585bp10sdg4kfn8")))

(define-public crate-errer_derive-0.13 (crate (name "errer_derive") (vers "0.13.0") (deps (list (crate-dep (name "errer") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "04hy0rrgl9pxpcyzr26syhrhflp6xw97ccv3bwgdp0kli3j4jc2h")))

(define-public crate-errer_derive-0.13 (crate (name "errer_derive") (vers "0.13.1") (deps (list (crate-dep (name "errer") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1kw0glg9qrn95lhxzhg7b92749f6mx98qxn5sfb3kfnjc0d7dvsv")))

(define-public crate-erreur-0.1 (crate (name "erreur") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1agmyf2dxgqnj77biypbfvjyq8629vz7jrwssfs4jlpvlw8nzyrd")))

(define-public crate-erreur-0.1 (crate (name "erreur") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0iw93ccndig6f3aj6b157afsls8b28k9zpp20iwx72cs8mbzfngv")))

(define-public crate-erreur-0.1 (crate (name "erreur") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1j1abzvlca13rphyxz4mqlrw8w0zilg5k75k7k3g65b68wvc6a5p")))

