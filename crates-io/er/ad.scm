(define-module (crates-io er ad) #:use-module (crates-io))

(define-public crate-erad-0.1 (crate (name "erad") (vers "0.1.0") (deps (list (crate-dep (name "comat") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "10r7a025v4rmn611innlk6vvjfkkivw39adayhljrmcbppmzaaf5")))

(define-public crate-eradicate-0.1 (crate (name "eradicate") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0k5p25lp07yh50v4bbxyvyk2c3k4sl44fa0ha1335ra8blsyhs5x")))

(define-public crate-eradicate-0.1 (crate (name "eradicate") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1p6v1kbb2wn0hfi68g247q0fqw4ywhd332jj6w13g7b8b14h7i7x")))

(define-public crate-eradicate-0.1 (crate (name "eradicate") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1c40ysds3hbnqpq11nkb34wiwy57nj366wgvpcpagks1n0vppbhj")))

(define-public crate-eradicate-0.1 (crate (name "eradicate") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1zxh5cbl9sbl3gpyyg7hv7j6yjlc4z2xn9az5zdkmlc1m1lhfb8c")))

(define-public crate-eradicate-0.1 (crate (name "eradicate") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1cp1nlkn9dx1lsmz7wf6dk53ahkbvnif11g42w8s76a0d2ypkgyh")))

