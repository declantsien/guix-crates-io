(define-module (crates-io er oc) #:use-module (crates-io))

(define-public crate-eroc-0.0.0 (crate (name "eroc") (vers "0.0.0") (hash "0xiz89d10dmgk4p09bgspalk91kr7drk9l8w9xc95axpfamas7l4")))

(define-public crate-eroc-nostd-0.0.0 (crate (name "eroc-nostd") (vers "0.0.0") (hash "1qwxjshg2nls7xdsf46pzf2ls7xdawbmg88lb6x4qxxpav4apms7")))

(define-public crate-eroc_crypto-0.0.0 (crate (name "eroc_crypto") (vers "0.0.0") (hash "040586ljph2jwhj6f7ll0dm0ck88ppqhnqq2q6wx7hs38anzfip4")))

(define-public crate-eroc_microstd-0.0.0 (crate (name "eroc_microstd") (vers "0.0.0") (hash "14jm6b85jynibni2sd10yhav1i6kzh055w8my8krpl07bxz1r11w")))

(define-public crate-eroc_test-0.0.0 (crate (name "eroc_test") (vers "0.0.0") (hash "1r914wp6znmwwcjzl6ml3xb4qliccdn9scyrdhykrx25yi8zv1ss")))

(define-public crate-eroc_test_macro-0.0.0 (crate (name "eroc_test_macro") (vers "0.0.0") (hash "12678lrcyqfskvlqb1bqs64nf6wd99afnr3hq6idmbvs79lv3hf7")))

