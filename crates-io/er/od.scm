(define-module (crates-io er od) #:use-module (crates-io))

(define-public crate-erode-0.1 (crate (name "erode") (vers "0.1.0") (deps (list (crate-dep (name "deno_ast") (req "^0.22.0") (features (quote ("transpiling"))) (default-features #t) (kind 0)) (crate-dep (name "deno_core") (req "^0.187.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ca8q70gbgpdk7afjf2lx4kbgpcwgm8rlxbmms7bngj3b1iibwkp")))

