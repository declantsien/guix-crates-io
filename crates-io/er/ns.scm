(define-module (crates-io er ns) #:use-module (crates-io))

(define-public crate-ernst-0.1 (crate (name "ernst") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8.11") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "ftree") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2.5") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03gg90jms7g1ga4pq1i0czckz2k7pbrz8yvy9lmyz6sva1gxcz0c")))

