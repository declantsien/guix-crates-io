(define-module (crates-io er rc) #:use-module (crates-io))

(define-public crate-errctx-1 (crate (name "errctx") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "05kmv7imq0pg3w05v8pxrvbqba796bdil6frnwsnx50zifh2bgqv")))

