(define-module (crates-io er fs) #:use-module (crates-io))

(define-public crate-erfs-gen-0.1 (crate (name "erfs-gen") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.50") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "deflate") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1xy7dkjab6v2zlqgl4714r83xkg29kkw6gzl3z86kqwk8qj9zayh") (yanked #t)))

(define-public crate-erfs-gen-0.1 (crate (name "erfs-gen") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.50") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "deflate") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1waps62gmd5l09g05qwhi8dv712w0jgpmlkdfwzbfikygnj76mcg")))

(define-public crate-erfs-gen-0.1 (crate (name "erfs-gen") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.49.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.50") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "deflate") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0jgrljya60p8f45f1im1vc20v37yhps1p33y1bdczjg8vflpgj1s")))

(define-public crate-erfs-rt-0.1 (crate (name "erfs-rt") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.50") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0zn7nls0g3c3z5ljw2i10ivnm2qx2wq1n3xnqcrp1w6krqbsbymw") (yanked #t)))

(define-public crate-erfs-rt-0.1 (crate (name "erfs-rt") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.50") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "08zl9kvjapa00wmjjlc7248ls4qd32bxbf63rz17586560g06sw3")))

(define-public crate-erfs-rt-0.1 (crate (name "erfs-rt") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.49.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.50") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "09b9c5pxbbppjqgm1bjbfwgpaarz8gb4m2isfvfsglz0gb2ccw99")))

