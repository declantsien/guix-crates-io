(define-module (crates-io er tr) #:use-module (crates-io))

(define-public crate-ertrace-0.1 (crate (name "ertrace") (vers "0.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "034za1kgvlra753jvac0khf38p66i6hhx58rslhizfg8hs5ra2zm") (features (quote (("std") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-ertrace-0.2 (crate (name "ertrace") (vers "0.2.0") (hash "0dddbziar6s57kyykq35w4vkbi69mf8bmpr3a7g3s1377jl14aik") (features (quote (("std") ("default" "std"))))))

(define-public crate-ertrace-0.2 (crate (name "ertrace") (vers "0.2.1") (hash "0nrz41jd9fsp41bl5fkhami3yds2rl3glxc06hsv0q73mjil4c4v") (features (quote (("std") ("default" "std"))))))

