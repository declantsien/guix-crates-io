(define-module (crates-io er dp) #:use-module (crates-io))

(define-public crate-erdp-0.1 (crate (name "erdp") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 2)))) (hash "0fr0wvkcv7l872xlgiba4dc6kj4ciyx8mc915y8fccmxwrvadsm5")))

(define-public crate-erdp-0.1 (crate (name "erdp") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 2)))) (hash "073c1zqiv15l2hklwbph4pmcq7bk87bfjwxpypp0q36z0ks5l8ra")))

