(define-module (crates-io er r_) #:use-module (crates-io))

(define-public crate-err_or-0.1 (crate (name "err_or") (vers "0.1.0") (hash "1ssrg46wkwck7i0m406rzs6mxzf8wss6gr3aw6y985siqkmlnrxy") (features (quote (("std"))))))

(define-public crate-err_prop-0.0.1 (crate (name "err_prop") (vers "0.0.1") (deps (list (crate-dep (name "cgmath") (req "^0.12") (default-features #t) (kind 0)))) (hash "15zr4n1phh82r3rjl9xvjmha6zl1hr6mjv6a7r8w3vqmmqcrwmng")))

(define-public crate-err_prop-0.0.2 (crate (name "err_prop") (vers "0.0.2") (deps (list (crate-dep (name "cgmath") (req "^0.12") (default-features #t) (kind 0)))) (hash "186lc7h95db9nhxqkhrff1zjfyb2mlr6lxqjajclbdgvfx5x1ksx")))

(define-public crate-err_tools-0.1 (crate (name "err_tools") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0s5lsschf0dk4vxxl3l33axzalwfwqgxg74dalylbh15nyvyp2l8")))

(define-public crate-err_tools-0.1 (crate (name "err_tools") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "08rmajx5b7z4j12m6j2r4szv1qw9j4nqjdkqmz5vqfl2yrgxs944")))

