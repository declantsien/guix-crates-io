(define-module (crates-io er rl) #:use-module (crates-io))

(define-public crate-errling-0.1 (crate (name "errling") (vers "0.1.0") (hash "193yq17yjsq5cpqj19h4yfih84z89y6kd065a02zn57xa95kr71q") (features (quote (("stable" "default") ("experimental" "stable") ("default"))))))

(define-public crate-errln-0.1 (crate (name "errln") (vers "0.1.0") (hash "0ipr9bcmzyw3qrsmxsl5i143l8wgry6njysjzmajsxkm68nq5fka")))

(define-public crate-errloc_macros-0.1 (crate (name "errloc_macros") (vers "0.1.0") (hash "1z7czdnfl5wqb2qfm8j9ilffb442fpf89jl1czh9px8n1waj2bhb")))

(define-public crate-errlog-0.0.0 (crate (name "errlog") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "1p33lpnhfm377qmvqj67c6468jfp4b59za004r2pikyq44jjlv27") (yanked #t)))

(define-public crate-errlog-0.0.1 (crate (name "errlog") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "1ii5628mxqs7wvbv8zc6wz6s4g758s1kiqchv8i4a7pr8vlgzmmz") (yanked #t)))

(define-public crate-errlog-0.0.2 (crate (name "errlog") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "16y5mbhpg4kw7wqd862gq26rpjpvbrnra1w71l4wvq7ckd5pnwys")))

