(define-module (crates-io er os) #:use-module (crates-io))

(define-public crate-erosion-0.1 (crate (name "erosion") (vers "0.1.0") (deps (list (crate-dep (name "plotters") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0m4x7wznzzgi4prbsrwq22kixsrv23q121izwpkxzgbsxa6n43pw")))

(define-public crate-erosion-0.1 (crate (name "erosion") (vers "0.1.1") (deps (list (crate-dep (name "plotters") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1klib3qfczkcrd14gw5azib0m4vysk2y8k5v142hxjxbr00q41bl")))

(define-public crate-erosion-0.1 (crate (name "erosion") (vers "0.1.2") (deps (list (crate-dep (name "plotters") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0f1rp1bincq2gf5pmy9ssvh9z9xm042khjxz7q4n752jb65d0a5f")))

(define-public crate-erosion-0.1 (crate (name "erosion") (vers "0.1.3") (deps (list (crate-dep (name "plotters") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1bnx3bl29bixv7l11jw0sgjllafh08d6pi5sn6g2kwgiqcf2acji")))

(define-public crate-erosion-0.1 (crate (name "erosion") (vers "0.1.4") (deps (list (crate-dep (name "plotters") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0hjpqs35cpzvzz6dya73rcyhs71rdqn0dj744q3v10h5hil2kmn0")))

