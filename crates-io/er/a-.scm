(define-module (crates-io er a-) #:use-module (crates-io))

(define-public crate-era-jp-0.1 (crate (name "era-jp") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "~0.4.0") (default-features #t) (kind 0)))) (hash "1qdsybjmpcsqlg34a606gplsqb044k1wah3g263pw4220nmkiryq")))

(define-public crate-era-jp-0.1 (crate (name "era-jp") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "~0.4.0") (default-features #t) (kind 0)))) (hash "0b8sj47gvvh94q9irl5h00qky0523m8afsfvbirgxn7yx284j014")))

(define-public crate-era-jp-0.1 (crate (name "era-jp") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "~0.4.0") (default-features #t) (kind 0)))) (hash "13liz9zidk0r7rfs81fhpqzxnjvgggy9f1lqlr1dcg2cg0rwyicz")))

(define-public crate-era-jp-0.1 (crate (name "era-jp") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "~0.4.0") (default-features #t) (kind 0)))) (hash "11779gn3b4bsy3qp7wzp76dcabm2dcv793avak60l46bj8nzbxpf")))

