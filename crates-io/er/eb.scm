(define-module (crates-io er eb) #:use-module (crates-io))

(define-public crate-erebus-sdk-rust-0.1 (crate (name "erebus-sdk-rust") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.3") (features (quote ("json" "rustls-tls" "gzip" "brotli" "rustls-tls-native-roots"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("rt-multi-thread" "macros" "time" "net"))) (default-features #t) (kind 0)))) (hash "1qil2psa9nach304q911vgmq8vv2rmk1wwi0c1g4hg2651l5fmmf")))

