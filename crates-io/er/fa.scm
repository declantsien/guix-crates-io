(define-module (crates-io er fa) #:use-module (crates-io))

(define-public crate-erfa-0.2 (crate (name "erfa") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1r8mb5aswvjr4hfq73gq2ya98q89vgsld44zp09ddmvvz3g80f7n")))

(define-public crate-erfa-sys-0.1 (crate (name "erfa-sys") (vers "0.1.0") (deps (list (crate-dep (name "autotools") (req "^0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "0.3.*") (default-features #t) (kind 2)) (crate-dep (name "pkg-config") (req "0.3.*") (default-features #t) (kind 1)))) (hash "06kyknqs3qlzzn803kk6x5r5gyq7a74lc7g240336s0ndqh1c0lz") (features (quote (("static" "autotools")))) (yanked #t) (links "erfa")))

(define-public crate-erfa-sys-0.1 (crate (name "erfa-sys") (vers "0.1.1") (deps (list (crate-dep (name "autotools") (req "^0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "0.3.*") (default-features #t) (kind 2)) (crate-dep (name "pkg-config") (req "0.3.*") (default-features #t) (kind 1)))) (hash "1gf67zjs9fqg2qd0r45qzvqwl19ndlnc6jcz1zdx6y9v83gdwpxk") (features (quote (("static" "autotools")))) (links "erfa")))

(define-public crate-erfa-sys-0.1 (crate (name "erfa-sys") (vers "0.1.2") (deps (list (crate-dep (name "autotools") (req "^0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "0.3.*") (default-features #t) (kind 2)) (crate-dep (name "pkg-config") (req "0.3.*") (default-features #t) (kind 1)))) (hash "01mnyds9cmdc3xm2mny353sx043acsr66c3jbk1k58j15xaw8cqj") (features (quote (("static" "autotools")))) (links "erfa")))

(define-public crate-erfa-sys-0.2 (crate (name "erfa-sys") (vers "0.2.0") (deps (list (crate-dep (name "autotools") (req "^0.2.5") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)))) (hash "0yki0vyclz7yypqcrnh0mkb9yi66r0w8rfszwhffvzlp87wq5r78") (features (quote (("static" "autotools")))) (links "erfa")))

(define-public crate-erfa-sys-0.2 (crate (name "erfa-sys") (vers "0.2.1") (deps (list (crate-dep (name "autotools") (req "^0.2.5") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)))) (hash "12akx25l3i7s13098xbhigp6z1y62m1rkdg58jwdj3jjh2pd6a1k") (features (quote (("static" "autotools")))) (links "erfa")))

