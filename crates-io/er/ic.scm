(define-module (crates-io er ic) #:use-module (crates-io))

(define-public crate-eric-1 (crate (name "eric") (vers "1.0.0") (hash "0jf6bykhs5dxf4r35zdbqp8wzp32g6vw0kij7lcdiyf3x343wzxx")))

(define-public crate-eric-1 (crate (name "eric") (vers "1.1.0") (hash "122lmwbmxd6y047rqysjks0i1zyz1a727yj85fh007620kk1j4vn") (yanked #t)))

(define-public crate-eric-1 (crate (name "eric") (vers "1.2.0") (hash "1nrzr9n9nys383r82d3wnbsrvcr6dcvppskb344arayd4bz5wjb2")))

