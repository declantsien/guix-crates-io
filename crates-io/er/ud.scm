(define-module (crates-io er ud) #:use-module (crates-io))

(define-public crate-erudite-0.1 (crate (name "erudite") (vers "0.1.0") (hash "0wkdcgc0dkcdysfkfcxzsm9rjz1sqkdz2fdz34ki24ssbf4jyya0") (yanked #t)))

(define-public crate-erudite-0.1 (crate (name "erudite") (vers "0.1.1") (hash "0xcfxk4m5lk3anmsya1dvzxbchj9i3kvdf5v7vwvjiqwakbida32") (yanked #t)))

(define-public crate-erudite-0.0.0 (crate (name "erudite") (vers "0.0.0") (hash "1f03hfz3cgzcg19qw08nil8bwipds3nrqkjq6hax03axb8cnfbwn")))

