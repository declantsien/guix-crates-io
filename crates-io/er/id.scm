(define-module (crates-io er id) #:use-module (crates-io))

(define-public crate-eridani-0.0.0 (crate (name "eridani") (vers "0.0.0") (hash "1ldqb9ccrfsyysssi4hcrdhvvbhh9wv3f12vp0bjy8857zi4sw3z") (yanked #t)))

(define-public crate-eridani-0.1 (crate (name "eridani") (vers "0.1.0") (deps (list (crate-dep (name "bimap") (req "^0.6") (kind 0)) (crate-dep (name "generic-array") (req "^0.14.7") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.26.1") (features (quote ("derive"))) (kind 0)))) (hash "14xrvnqpqdswd673n0n3bdq9a6wc4s2vqz26qqihq5ny3lcr461x") (features (quote (("web" "runtime") ("target_web" "compiler") ("target_std" "compiler") ("std") ("runtime") ("ffi") ("error_trait") ("default" "compiler" "runtime" "std" "target_std") ("compiler")))) (v 2) (features2 (quote (("serialise" "dep:serde"))))))

