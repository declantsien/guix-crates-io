(define-module (crates-io er ra) #:use-module (crates-io))

(define-public crate-erra-0.1 (crate (name "erra") (vers "0.1.0") (hash "1z2pd25zlimsrg7hfgxyb07yygla4vs00flx9ywga2khr142a2fg")))

(define-public crate-errable-0.1 (crate (name "errable") (vers "0.1.0") (hash "15q6nnyddcbp279l66w1b021cpj300rdy3ziv7hp96rjapjgk985") (yanked #t)))

(define-public crate-errant-0.0.0 (crate (name "errant") (vers "0.0.0") (hash "0i03dyc6a3xn5gf8ia5w1pllsrjyqrkfpzlfy47mp2q31h92z00k")))

(define-public crate-errata-1 (crate (name "errata") (vers "1.1.0") (deps (list (crate-dep (name "errata-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0skg0c49an6civxjdcj431nwl70qf41vyydqp1h1l1dvchw266p6")))

(define-public crate-errata-1 (crate (name "errata") (vers "1.1.1") (deps (list (crate-dep (name "errata-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0azsz5cgxd331gi13i6vc7byiizih12q3w3wzs96xhki2chxhpbl")))

(define-public crate-errata-2 (crate (name "errata") (vers "2.0.0") (deps (list (crate-dep (name "errata-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gzrks4czjvca5aqd4cxfzjdkpfcchqyjchpqggx9y5wl8blm91v") (features (quote (("default") ("color"))))))

(define-public crate-errata-2 (crate (name "errata") (vers "2.1.0") (deps (list (crate-dep (name "errata-macros") (req "^0") (default-features #t) (kind 0)))) (hash "07qz93mzac5w3gpqvbx6hgcc2hri23rkhfmvfn8lmsnm3knlil7c") (features (quote (("default") ("color"))))))

(define-public crate-errata-2 (crate (name "errata") (vers "2.1.1") (deps (list (crate-dep (name "errata-macros") (req "^0") (default-features #t) (kind 0)))) (hash "07d89xc0c1c5xacds223v6nmhfwsm5y5iw0fld4izqasiykibgfh") (features (quote (("default") ("color"))))))

(define-public crate-errata-macros-0.1 (crate (name "errata-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n2vyg2111sagccxa6q0rb35ck07q20na82bz7p9ppy28hnygf4d")))

(define-public crate-errata-macros-0.2 (crate (name "errata-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mrksz5blm7zj1s31lrm73qg74crr0jxvxlv8cyk0jfw38r1wsw4")))

(define-public crate-erratum-0.1 (crate (name "erratum") (vers "0.1.0") (hash "19rj6i1qgbaw7kdh7kmhjcl479c22d6q0w5p49jq7pkc0swpqfcd") (yanked #t)))

(define-public crate-erratum-0.0.0 (crate (name "erratum") (vers "0.0.0") (hash "1p2km5xgwm7dnjnpwwxi1b4xigp5c0vp3zdwkv56qmgckz8g8ng3")))

