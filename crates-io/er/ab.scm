(define-module (crates-io er ab) #:use-module (crates-io))

(define-public crate-erabu-0.1 (crate (name "erabu") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0ik20bq2j4vcd7sb6bs7c9nj0wp9l42hlfpzyiihs58cavgnvxfl")))

(define-public crate-erabu-0.1 (crate (name "erabu") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1gwwfr21nalzcfphgy5a8rpvhbyqkbqpckfzqwfpb2n0ydiwpwgx")))

