(define-module (crates-io er at) #:use-module (crates-io))

(define-public crate-eratosthenes-0.1 (crate (name "eratosthenes") (vers "0.1.1") (hash "1zj1v059ml99nard0ick9ajl13k6bx8rcp378d5769a3r4viiib9") (rust-version "1.70")))

(define-public crate-eratosthenes-0.2 (crate (name "eratosthenes") (vers "0.2.0") (hash "0fdcmgig8qcrj4sxm7n42mpglkxyif9rppj6cbf5s3fqd8i85b58") (rust-version "1.70")))

