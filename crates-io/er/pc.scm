(define-module (crates-io er pc) #:use-module (crates-io))

(define-public crate-erpc-rs-0.1 (crate (name "erpc-rs") (vers "0.1.1") (deps (list (crate-dep (name "erpc-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gfqapagf953licrbr4s0mwkp78p0q0a530mkz3sb22ghbmrvh7j")))

(define-public crate-erpc-rs-0.1 (crate (name "erpc-rs") (vers "0.1.2") (deps (list (crate-dep (name "erpc-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0in7xramlzjs1747lwixg3iy5abjg0n5ayj9iqkcq5p79fi6ky92")))

(define-public crate-erpc-rs-0.2 (crate (name "erpc-rs") (vers "0.2.0") (deps (list (crate-dep (name "erpc-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1yf1im3qf45yl2jihi04caxa4cjfi36pzb5skmzbz8110zbi80l8")))

(define-public crate-erpc-sys-0.1 (crate (name "erpc-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q81x9zzgnxqnr5ah14g6q00iycxmyp6cnvmkqcrdi4f45irs97h")))

(define-public crate-erpc-sys-0.2 (crate (name "erpc-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0990ppf82jb044zzl0zbiznr7vvpydzdaq2ydwfz62avqg80ihac")))

