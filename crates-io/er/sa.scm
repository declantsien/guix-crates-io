(define-module (crates-io er sa) #:use-module (crates-io))

(define-public crate-ersatz-0.0.1 (crate (name "ersatz") (vers "0.0.1") (deps (list (crate-dep (name "bit-set") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0qg8y0bbbm1m4kam10vl562fb88w4jycz5bds1mhxd1fprax815x")))

