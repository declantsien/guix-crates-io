(define-module (crates-io dx pr) #:use-module (crates-io))

(define-public crate-dxpr-0.1 (crate (name "dxpr") (vers "0.1.0") (hash "0sy626gkl4q533dhlpajpjv7r3640c4dlsjpn012izylvz1m99ly") (features (quote (("std") ("default" "std"))))))

(define-public crate-dxpr-0.2 (crate (name "dxpr") (vers "0.2.0") (hash "10dv6dzrq15wsnzhibzy102vjrcckjjha5jyfpgnc95lzn8yvwqb") (features (quote (("std") ("default" "std"))))))

(define-public crate-dxpr-0.2 (crate (name "dxpr") (vers "0.2.1") (hash "0shlsszzfngs5rcn2a8b7ll4jwdya22s7mqsrzvmrxvzm49s86jh") (features (quote (("std") ("default" "std"))))))

(define-public crate-dxpr-0.2 (crate (name "dxpr") (vers "0.2.2") (hash "1qcj88y36h52d9pw71j5phwsfirp9yb6haz20vsbmvwpw6sfrpld") (features (quote (("std") ("default" "std"))))))

(define-public crate-dxpr-0.2 (crate (name "dxpr") (vers "0.2.3") (hash "0zsy8dcdij0y9h3p7rc1czhjxgcwshcjjclsz6qaaipl1pyqc5mq") (features (quote (("std") ("default" "std"))))))

