(define-module (crates-io dx rs) #:use-module (crates-io))

(define-public crate-dxrs-0.0.0 (crate (name "dxrs") (vers "0.0.0") (hash "16n46ypri677pwiziznc0j0ykl3x24mhynlw629d928gir1wy6vz") (features (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

(define-public crate-dxrs-0.0.1 (crate (name "dxrs") (vers "0.0.1") (hash "0rn2x9828vcc3dlzv60yqmx6d5nvibrwag97hvs7g0ksy3sn2ih9") (features (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

