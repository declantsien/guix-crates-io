(define-module (crates-io dx -r) #:use-module (crates-io))

(define-public crate-dx-rs-0.1 (crate (name "dx-rs") (vers "0.1.0") (deps (list (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tr-rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0nyfcq0lzx37gq0xq467wznnahyr3yp37pxi1p6psi910kj2psmb")))

