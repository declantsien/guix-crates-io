(define-module (crates-io dx li) #:use-module (crates-io))

(define-public crate-dxlib-0.0.1 (crate (name "dxlib") (vers "0.0.1") (hash "1jbm30gdkj28rbyrgfdhvywidfvr5lxyg00zjmyrvpy1wf1i9kbw") (yanked #t)))

(define-public crate-dxlib-0.0.2 (crate (name "dxlib") (vers "0.0.2") (hash "1z4pzmp7affcwwxh79lwb6rg5g6kcgbxcl3qhbijwk6nqv0a0yd9") (yanked #t)))

(define-public crate-dxlib-0.0.3 (crate (name "dxlib") (vers "0.0.3") (hash "03g4kygq55c6pdsvap6j3yqybs5hbm3s0n5dr7h12j27yhvdgnfg") (yanked #t)))

(define-public crate-dxlib-0.0.4 (crate (name "dxlib") (vers "0.0.4") (hash "028mrdiqya00jyxil97yb2896l5xqd3kcmppyxqwcj5xjwv58cic") (yanked #t)))

(define-public crate-dxlib-0.0.5 (crate (name "dxlib") (vers "0.0.5") (hash "1crfpg0k8kkvkycs1nvhj2ndif8s2var9c1139kqhnjwdny0mjig") (yanked #t)))

(define-public crate-dxlib-0.0.6 (crate (name "dxlib") (vers "0.0.6") (hash "14hylkjzk0xpwzshq4df61x0bchm8w47kya7dwq12j33lbvnznhg") (yanked #t)))

(define-public crate-dxlib-0.0.7 (crate (name "dxlib") (vers "0.0.7") (hash "1v7sqikh142cywl1s8if2zszdiy7z8ss8k9xq52ya3qzwg966518") (yanked #t)))

(define-public crate-dxlib-0.0.8 (crate (name "dxlib") (vers "0.0.8") (hash "0dl2vs2f2frjp8j0iaaf8l5j11shn62ghz8zpl5xaky1pwn2zf6r") (yanked #t)))

(define-public crate-dxlib-0.0.9 (crate (name "dxlib") (vers "0.0.9") (hash "1c3886lr1cmv992yr7wi7j0pw3hy116pqzibfdp2sy41nxjwpk99") (yanked #t)))

(define-public crate-dxlib-0.0.10 (crate (name "dxlib") (vers "0.0.10") (hash "04fhnr1wplw9jz906v64xgz5m36cbzaqbmqk2bh4w4qpx0kzjd40") (yanked #t)))

(define-public crate-dxlib-0.1 (crate (name "dxlib") (vers "0.1.1") (hash "1dl0mmna2010qdfypa4lh8adlanabwxs8nv5fiaww690iizmpyp3") (yanked #t)))

(define-public crate-dxlib-0.1 (crate (name "dxlib") (vers "0.1.2") (hash "1cc683f7s0maiiki73k9n6kay8wh1g9bmz1i49kjcpm30vjc0rvb") (yanked #t)))

(define-public crate-dxlib-0.1 (crate (name "dxlib") (vers "0.1.3") (hash "1082zbrj1gw4cmdgnnil8cg7rkc96gc97jyjhkfyi8684d26zxr3") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.0") (hash "04p9bmr5i4yk9j0jc0xcrcl1f6i4s6m0xfv8y7h9nfdr23lfp2i3") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.1") (hash "1xz276g504i3554pk99aqw7av3089cc8ph8bn87gxjc35bhjnpjr") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.2") (hash "0qg0xl4x48xw9i133z6i93psnb5js31mn3n532f72fldryiv9zz0") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.4") (hash "1wpk08gw7di0y7lf9rcaal75xrr18nyv5kbkj6xjvvlpjphqh0gl") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.3") (hash "19qcxi1a3nl5pv5rjdxhhkqldx3jyzc3s1qa86x1ng0zm4wzdrdv") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.5") (hash "19afgxx6hi6ca7d9a5cm6g6vsl1is9yin1sw3sjfs9kiz09991yb") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.6") (hash "12zwkag52hb24s8njdsrzx8ky69lpgrx2q3m840izhy1mazsrkjk") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.7") (hash "1l3kj4scd2p76bkqqbbrcxk8qcgqwdpdmknhniavz6f8albr89jz") (yanked #t)))

(define-public crate-dxlib-0.2 (crate (name "dxlib") (vers "0.2.8") (hash "0rx6qsdsmsw8f5azp8jydin8ag0b9914grq8cy8b3689xx177xvl") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.0") (hash "1mag2mw2j09jwi6cizfansg6zli88jva104g9bzx7d03548038q9") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.1") (hash "1sriqkhvzrnhcl3i57q2r9nbb2369x3bccpmglysvdgdkvg1xjy4") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.2") (hash "14bm5xll58vqgdgr98w3zs4cwd4jidklbhzax5pkkwyk9cpf17z1") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.3") (hash "179vk2sya0z4hg4ry7spzcsvda7rv9knjf73y7j4v17zc6nx8dwp") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.4") (deps (list (crate-dep (name "Fullerene") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0csm7xchgn4awx1rjbchi536x69h0rjwdwl1dy6smafvsvkhw7wx") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.5") (deps (list (crate-dep (name "Fullerene") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1z95m2zm0cbhhj5f557srly0dnihfk1cqwi574zwrk17wg3l8cdq") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.6") (deps (list (crate-dep (name "Fullerene") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "17b6r1ryl5lhrwrgh53z3l0c1aaknb7pqf1x9flp4j99zlmsl6pq") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.7") (deps (list (crate-dep (name "Fullerene") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xij75v6h44bjd1k6hpssh34phv3hrxplljfxs01b43fxar8lw9b") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.8") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1b656i6n0v1mn35gxdyfaar2z5g23fyvpa0amp2an7s5sfs1zawg") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.9") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "16mz9imxyar4rm89p0xij8g78k4xk9rhr5awi3pzd4s335i1ibpi") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.10") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1zlh0ml7x4nqql02m1n7a84jw5snvqyw8h7ld7mls8kywmysigcs") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.11") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "02qzbs404xdf0659bm8qqlqyi25hy1b06g8sld34z8g1hrbfy1id") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.12") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dk3cx01kq2pxvh93nbnxvvwjngs6j7r7i2xln4im1ia1di7fm6m") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.13") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pbmkki8p3wwwnjbfdydc8sjs7fi6yj49v6ivnkvlgb68r2q0i81") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.14") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "13m5m1qhj0sl37pqw2jqdi1qcg9npzzvzyfimrshx9s9nb2rs2zv") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.15") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0j2h284bpx42bza210w2hhj40bv3imywnz01rrqcxzmjrnpc1xvs") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.16") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "06grx7c09n60p240qjsnbha3lj8lncw1bx3x6yg9c8z2bfgmbpp7") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.17") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xm6majvlbam8p61j6m0568xaldgl184w7jmvvsn7dxp7a05gbrp") (yanked #t)))

(define-public crate-dxlib-0.3 (crate (name "dxlib") (vers "0.3.18") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gvjdqfb65rjr15gvybqd5csxpr3hgcgh72fcp7fvb3j2z0pvba1") (yanked #t)))

(define-public crate-dxlib-0.4 (crate (name "dxlib") (vers "0.4.0") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "04jq8rgrjms5dkc7v4n7km6iac4219bzkbi4qm7dlg0xgnxdips8") (yanked #t)))

(define-public crate-dxlib-0.4 (crate (name "dxlib") (vers "0.4.1") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1g6wx5zj9sx5q3ldd742ax3v00ajsgadq748yv2xmlfpxj83dm3i") (yanked #t)))

(define-public crate-dxlib-0.4 (crate (name "dxlib") (vers "0.4.2") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "16g9yzv8c4xlwh47ashrj0ah7hzm2mls04d2ixfb54rdx1w1d2n4") (yanked #t)))

(define-public crate-dxlib-0.4 (crate (name "dxlib") (vers "0.4.3") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "04g516ypysqmmf821jk8c6c6kh9waj0771zp5h335x80kplv262f") (yanked #t)))

(define-public crate-dxlib-0.4 (crate (name "dxlib") (vers "0.4.4") (deps (list (crate-dep (name "Fullerene") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0k74v8blp4f365735hlglry6bvfly3g3jyg408pkf5wijl4ml8jn") (yanked #t)))

(define-public crate-dxlib-0.4 (crate (name "dxlib") (vers "0.4.5") (deps (list (crate-dep (name "Fullerene") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1364c4nd66h60rdj674wmdi99wxkrqpnn00w0b1v9b3rk15w7ax7")))

(define-public crate-dxlib-0.5 (crate (name "dxlib") (vers "0.5.1") (deps (list (crate-dep (name "Fullerene") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "polyhedron-faces") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dpr2af6yfzkk5x951a01crpi2rclyarhwdzvb113k7570qmf757")))

(define-public crate-dxlib_sample_0001-0.0.1 (crate (name "dxlib_sample_0001") (vers "0.0.1") (deps (list (crate-dep (name "dxlib") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1pcwd0d0fy64ibcjrrgxkz3magbgbx9x1lskk3ji37pqyg2vs8p9") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.0.2 (crate (name "dxlib_sample_0001") (vers "0.0.2") (deps (list (crate-dep (name "dxlib") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1ki7vpqk1vp4w5dxd4hsmbjl7fwidr3qil91jyq4pm6fmnk4giza") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.0.3 (crate (name "dxlib_sample_0001") (vers "0.0.3") (deps (list (crate-dep (name "dxlib") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0w5fhh78xz5knhl2aa772qmg5bf7v0yy3fapj7643xb1587sw2a3") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.0.4 (crate (name "dxlib_sample_0001") (vers "0.0.4") (deps (list (crate-dep (name "dxlib") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0rb1nn7n38r6avcc080ax82sj2i9v1wm8fczgc395jsdbp2plw14") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.0.7 (crate (name "dxlib_sample_0001") (vers "0.0.7") (deps (list (crate-dep (name "dxlib") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1bjlpbih77bv0f9gv50z75mlmml0wf1ziglij4j8g77qiybjpadw") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.0.8 (crate (name "dxlib_sample_0001") (vers "0.0.8") (deps (list (crate-dep (name "dxlib") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0wmdcpdrsmwdg3r4p5k9x12j7a8hjc978fgdj6bx0hksgrdimgyk") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.0.9 (crate (name "dxlib_sample_0001") (vers "0.0.9") (deps (list (crate-dep (name "dxlib") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "0ii4mnyivw418sxwqlw0sqwlh665cnvysv7b0xv670jx1vws7cqg") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.1 (crate (name "dxlib_sample_0001") (vers "0.1.1") (deps (list (crate-dep (name "dxlib") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0mn0llpma9qgs51r3yyay8c3k497fv9wgapjiw3r7k6201h0cz6p") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.1 (crate (name "dxlib_sample_0001") (vers "0.1.2") (deps (list (crate-dep (name "dxlib") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1gq1qskyv6vjiksbknp0qsqc9ifhxc9jmabpjb7v72vy79bqqa19") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.2 (crate (name "dxlib_sample_0001") (vers "0.2.0") (deps (list (crate-dep (name "dxlib") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05nc20g6vw6r3pwvwk7f55yacsxr0v51qrxp3ss1gwwgk3czw9mh") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.2 (crate (name "dxlib_sample_0001") (vers "0.2.2") (deps (list (crate-dep (name "dxlib") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0sd4b62zkl1px6n0pczyrnad9k10r16jrv2bwql4i9wc0jwj1r4k") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.2 (crate (name "dxlib_sample_0001") (vers "0.2.8") (deps (list (crate-dep (name "dxlib") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "16mmd1sk8gpvx36mvwc21llha5wh2b72w9c9vrp4895y038bim4i") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.0") (deps (list (crate-dep (name "dxlib") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1rkmafg8abgr8gvdbhhw0d7pb773hn85irglinibhmwpfj7ishf8") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.1") (deps (list (crate-dep (name "dxlib") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1l36fnpyf8ns0r8rr24k9c6zjji2g5nnikvbwsvsr9g3bwg5rnah") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.3") (deps (list (crate-dep (name "dxlib") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1rc9h3niafsxfkyrfv4n48swna0kqxcc0yh11wi5v0jlj4fpqjcl") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.4") (deps (list (crate-dep (name "Fullerene") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0c7jfyv26bkfbx8p362blzjjd6yggm54bc49nlsmvv63qcg6alxi") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.5") (deps (list (crate-dep (name "Fullerene") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0sb65p8lwyvn3fd00xlz96ph93galca0yx77lfzz4nqjrsvfn3fn") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.8") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "10d50m0b1fx7qjyy6lhfbdsci30x7fbkadn5i25fk23r6awfi2wr") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.17") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mg5g5lzqr0sgbqvv1wi1zmns4wf5mgpsza6w80ya27wfa5ps1ms") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.3 (crate (name "dxlib_sample_0001") (vers "0.3.18") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.3.18") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k3iydsaq8vp9fk5mlfp3sqrmm6ig63yi2m15vsppam85j9j75ml") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.4 (crate (name "dxlib_sample_0001") (vers "0.4.0") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qqqkrw1jzngs70k713bhk1j7wl1yyyaxwsz7lgncyja0nbgqvlh") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.4 (crate (name "dxlib_sample_0001") (vers "0.4.1") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "05p22zxs2ixfxdif62fhrlkzzy443sh1s956nnmmf7xgzl8spnmd") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.4 (crate (name "dxlib_sample_0001") (vers "0.4.2") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1l390risc4z6zwz64cfg77jg6a7gfidnx3j7vkb8cy2rqlq8shcg") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.4 (crate (name "dxlib_sample_0001") (vers "0.4.3") (deps (list (crate-dep (name "Fullerene") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gh7k6zxij33l3v230m4yn5iz0wd5qrkq5ny7sk2cc27rgnfvqzs") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.4 (crate (name "dxlib_sample_0001") (vers "0.4.4") (deps (list (crate-dep (name "Fullerene") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fvspndfbwym78sb9pzs2prgy9ncqpnm7li6sih47kxywphdrk0h") (yanked #t)))

(define-public crate-dxlib_sample_0001-0.4 (crate (name "dxlib_sample_0001") (vers "0.4.5") (deps (list (crate-dep (name "Fullerene") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mjb6ra72lkv4n1j6si3yqj7r6s1n4gyml8n9jrssnlfravisiqb")))

(define-public crate-dxlib_sample_0001-0.5 (crate (name "dxlib_sample_0001") (vers "0.5.1") (deps (list (crate-dep (name "Fullerene") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dxlib") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "polyhedron-faces") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vj7nbg4v8kb71f6pa2p146qw0b37pmfz09sr9qqsr9hz68c1nky")))

