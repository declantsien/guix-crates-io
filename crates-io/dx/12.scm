(define-module (crates-io dx #{12}#) #:use-module (crates-io))

(define-public crate-dx12bindings-0.1 (crate (name "dx12bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "0k4bpp77699pz8akrabdskw7h3s71ahbjspi7jdrs8arwv8x6lfs") (yanked #t)))

(define-public crate-dx12bindings-0.0.1 (crate (name "dx12bindings") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "08zw71n93k46bjpv5vs2dh3vqvwmmm7d8d3ckd7k1mfcp1pgyv22") (yanked #t)))

(define-public crate-dx12bindings-0.1 (crate (name "dx12bindings") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "1b2zrfknhd8saxv83fmqxl1dqdvwgqml0c06azy30wl9xigchvma") (yanked #t)))

(define-public crate-dx12bindings-0.1 (crate (name "dx12bindings") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "0z63g0k9fyysb81a8clyqaznspcljzlcs3949vg4a5pbb6xddrj7") (yanked #t)))

(define-public crate-dx12bindings-0.1 (crate (name "dx12bindings") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "1b1qvd3yz72p0ajg2axk0qkyh68a0dlyym8ddcy55i2cmqp0b4cc") (yanked #t)))

(define-public crate-dx12bindings-0.1 (crate (name "dx12bindings") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "find-winsdk") (req "^0.2.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "pixwrapper") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "08r6ar67plk7s0mv3g79kamg77r3kbfk3nwqdjhchn7120sxpbrd") (yanked #t)))

