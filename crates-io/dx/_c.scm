(define-module (crates-io dx _c) #:use-module (crates-io))

(define-public crate-dx_core-0.1 (crate (name "dx_core") (vers "0.1.0") (hash "1afirwjn3fv0if5zxcvr50hic376zcs4s1ga50xgw9b5yc41k03w")))

(define-public crate-dx_core-0.1 (crate (name "dx_core") (vers "0.1.1") (hash "14m28g2vg20ldhav9qnm84pvximksac792vcm6c3n6rcdb2215g4")))

(define-public crate-dx_core-0.1 (crate (name "dx_core") (vers "0.1.2") (hash "05p57ypy4jiddyk5jk4xz6ccnam7syhbwg8my1535jmp89kigbpj")))

(define-public crate-dx_core-0.1 (crate (name "dx_core") (vers "0.1.3") (hash "0df3wi9xvavi0hihmgywpgbh04742z3h8gz7pacampmak3dn89jj")))

(define-public crate-dx_core-0.1 (crate (name "dx_core") (vers "0.1.4") (hash "029b6kp5xd4c70l411d98bk9dl1yszj76llvsy5hka42kba4hni5")))

(define-public crate-dx_core-0.1 (crate (name "dx_core") (vers "0.1.5") (hash "07gahw2n1pqxkjmy5zg7wj8bscphzq0p2vw4md6d9k8z5b8rsgik")))

(define-public crate-dx_core-0.1 (crate (name "dx_core") (vers "0.1.6") (hash "097qmrfr7ws9c61p45xq43pkgqgbwhiybadyvjjqhs4jdfdxgb77")))

(define-public crate-dx_core-0.2 (crate (name "dx_core") (vers "0.2.0") (hash "1sbqcs1dp223pwzmhwjclbyxrpfhsh74108xbyrracgqqf0pzdq9")))

(define-public crate-dx_core-0.2 (crate (name "dx_core") (vers "0.2.1") (hash "0kr1h41yasb2b0cs35r048acdwcs4sbywaskg9iy9siwfdyjk9y1")))

(define-public crate-dx_core-0.2 (crate (name "dx_core") (vers "0.2.2") (hash "0jsxwv1frq8wr12xpqdaagx86rphlq1svzspdbsiibyan1gki6ha")))

(define-public crate-dx_core-0.3 (crate (name "dx_core") (vers "0.3.0") (hash "0280pap4x3k85mh1wjprdmax445wvih8888dgdy9d7a4w7diwpyh")))

