(define-module (crates-io dx f-) #:use-module (crates-io))

(define-public crate-dxf-model-0.1 (crate (name "dxf-model") (vers "0.1.0") (deps (list (crate-dep (name "dxf") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0zk3203knd6v5gryac824w668j330mbi8zfww4c73lrvrk1pdlpc")))

