(define-module (crates-io dx cw) #:use-module (crates-io))

(define-public crate-dxcwr-0.1 (crate (name "dxcwr") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("implement" "Win32_Foundation" "Win32_Security" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D_Dxc" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (default-features #t) (kind 0)))) (hash "19bw79j7miz4b22sfp10mgp18y63v6ga3a55ls8zxcps9p535iy1")))

(define-public crate-dxcwr-0.1 (crate (name "dxcwr") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51.1") (features (quote ("implement" "Win32_Foundation" "Win32_Security" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D_Dxc" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (default-features #t) (kind 0)))) (hash "1cmmfy27jwykq0dqidfq230z8pzqp7qid160iyhz2rz5w6zdkgc4")))

