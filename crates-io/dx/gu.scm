(define-module (crates-io dx gu) #:use-module (crates-io))

(define-public crate-dxguid-sys-0.0.1 (crate (name "dxguid-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0bd097mmi35g98f7kd6cd91dhxnjc6q7vasy0rna8wrc0kh1n7rs")))

(define-public crate-dxguid-sys-0.2 (crate (name "dxguid-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "07xa6h6bcsm3bc33b4nlr00is8ankxp6n6dv81bl4yxc80y2dzb7")))

