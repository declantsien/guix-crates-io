(define-module (crates-io js -o) #:use-module (crates-io))

(define-public crate-js-object-0.1 (crate (name "js-object") (vers "0.1.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "05bzljd2l4kmpczddbddy82hmvaqcv19mws34xq091crqnz78wvc")))

(define-public crate-js-object-0.2 (crate (name "js-object") (vers "0.2.0") (deps (list (crate-dep (name "js-intern") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "0s813isfiz8zy9cqixmbg4mq5v7by4pcyr1fdds7ns3k4xxkx8z1")))

