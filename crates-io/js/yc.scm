(define-module (crates-io js yc) #:use-module (crates-io))

(define-public crate-jsyc-compiler-0.1 (crate (name "jsyc-compiler") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "~0.10") (default-features #t) (kind 0)) (crate-dep (name "resast") (req "~0.2.1") (default-features #t) (kind 0)) (crate-dep (name "ressa") (req "~0.5.2") (default-features #t) (kind 0)))) (hash "1vhi8avqf5ja3r6ny9hjzzc6n7irs374nl17559c6kw8cql3jcrp")))

