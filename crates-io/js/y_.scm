(define-module (crates-io js y_) #:use-module (crates-io))

(define-public crate-jsy_mk_194-1 (crate (name "jsy_mk_194") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0n3h1ym71q84jkihmvqqirfyzcf4qw4k5hi5pxj9dmn00krrm5kw") (rust-version "1.66")))

(define-public crate-jsy_mk_194-1 (crate (name "jsy_mk_194") (vers "1.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1wk0x9r11610815rmsykg9ygrjhlq5qd8f2v2a9nb4236rwr20ax") (rust-version "1.66")))

(define-public crate-jsy_mk_194-1 (crate (name "jsy_mk_194") (vers "1.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0f4w84h7xb3gg41vzsc5qa2pm384wf1f225i3apdmzsc6ibkrbd5") (rust-version "1.66")))

