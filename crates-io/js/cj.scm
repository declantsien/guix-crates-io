(define-module (crates-io js cj) #:use-module (crates-io))

(define-public crate-jscjs-0.0.1 (crate (name "jscjs") (vers "0.0.1") (deps (list (crate-dep (name "jscjs_sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "jscjs_sys") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "10h1m61gq5zh9niih30y4qxxjq8qkk00r3990v2ivfkxd2sgv8l9")))

(define-public crate-jscjs-0.0.2 (crate (name "jscjs") (vers "0.0.2") (deps (list (crate-dep (name "jscjs_sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "jscjs_sys") (req "^0.0.1") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "03b8bs8bzmnzlf4nid2xgkhxz77gn8wba668padk0gk0j10qg5jd")))

(define-public crate-jscjs_sys-0.0.1 (crate (name "jscjs_sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "clang") (req "^1.0.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)) (crate-dep (name "libz-sys") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 1)))) (hash "03w4wmgc5v5gig4936i8h9rygva14w576qzd0gmjd7wx0kr7mp3z") (yanked #t)))

(define-public crate-jscjs_sys-0.0.2 (crate (name "jscjs_sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)))) (hash "0s83s15r6xc92p14ipf9d1v2smgyryaib1pcmmza7p0j9222nqkd") (links "jscjs")))

(define-public crate-jscjs_sys-0.0.3 (crate (name "jscjs_sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1jw133gbx25bl9zksq5m8y1y45pfzf6by1rn0g9ss010vmsbmpf0") (links "glib-2.0")))

