(define-module (crates-io js hi) #:use-module (crates-io))

(define-public crate-jshiohaha-newworld-test-0.1 (crate (name "jshiohaha-newworld-test") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "0n66qxq76yjdxp9mjr5ghrgyd57qp4ir7mw4ri4mcl2zxg7m7la9") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.1 (crate (name "jshiohaha-newworld-test") (vers "0.1.1") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "0a0ayhv0ka50cbnb7bv5aiy59nvhk3jnc2mys6gdqhkj1238ba9c") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.1 (crate (name "jshiohaha-newworld-test") (vers "0.1.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "0h8zqh9hrf5bm881zi7mv7fmyg6sz451440paymkx26f6p57556w") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.2 (crate (name "jshiohaha-newworld-test") (vers "0.2.2") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "1b9i9m6k6v59j93f414n7fcl650k2h3r401xw5j61h0wlldgpkan") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.2 (crate (name "jshiohaha-newworld-test") (vers "0.2.3") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "18d5wx9yi4kajs5fwznlfr9p705g65msd6frmmqyiknhz8bzix2r") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.2 (crate (name "jshiohaha-newworld-test") (vers "0.2.4") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "0kj57xmzqfq09nnjwxm5zdkg0ccikga8nkikzxj1xim8yv1wqk8r") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.3 (crate (name "jshiohaha-newworld-test") (vers "0.3.4") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "0r878hvw9gi8baaldhcvx0z4nsv8mr8jbjrck573g0cg6drily2a") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.4 (crate (name "jshiohaha-newworld-test") (vers "0.4.4") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "1dgsf9valqlm4qfn7wihpdxj37va2racx3c53rpj2z12w9p00r96") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.4 (crate (name "jshiohaha-newworld-test") (vers "0.4.5") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "1mawzwripg15m3gwak7njmhc4g2dhjiw551wnfmvz2c46sgqzfki") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.4 (crate (name "jshiohaha-newworld-test") (vers "0.4.6") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "00qnwrq1pj08f98kq6bf0n5v5xq5k646ya6r5s99f6rgqq6pp2pk") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.4 (crate (name "jshiohaha-newworld-test") (vers "0.4.7") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "1lad71ah70bkzb76diyz86jp948hxnivw1k5di78vbjlpc4f7zdf") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.4 (crate (name "jshiohaha-newworld-test") (vers "0.4.9") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "0hj60cnivccvx1b65iwng81l7l5ng105mk0c6gsvsar4s3w1ks7a") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.4 (crate (name "jshiohaha-newworld-test") (vers "0.4.10") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "0g79wh7g5q3vvny4r0vyz07safcl72g6q2s8i77san91i1yffkyk") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-jshiohaha-newworld-test-0.4 (crate (name "jshiohaha-newworld-test") (vers "0.4.11") (deps (list (crate-dep (name "anchor-lang") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "1gx9sa6hhycdi0d23yykm5zbkkday6331n26d6sxxp604cf68bpm") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

