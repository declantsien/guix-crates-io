(define-module (crates-io js _o) #:use-module (crates-io))

(define-public crate-js_option-0.1 (crate (name "js_option") (vers "0.1.0") (deps (list (crate-dep (name "serde_crate") (req "^1.0.125") (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_crate") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 2) (package "serde")) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)))) (hash "19l5lr7ccbbwb0qfryndfdanl7ja2hnjs9idj8cvg0zi6kf3djjp") (features (quote (("serde" "serde_crate") ("default" "serde"))))))

(define-public crate-js_option-0.1 (crate (name "js_option") (vers "0.1.1") (deps (list (crate-dep (name "serde_crate") (req "^1.0.125") (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_crate") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 2) (package "serde")) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)))) (hash "0g2273zp51nik16s95zyxqhv5qh6ybdrhdh1cykr65bsjmri6hk8") (features (quote (("serde" "serde_crate") ("default" "serde"))))))

