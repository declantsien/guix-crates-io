(define-module (crates-io js #{8c}#) #:use-module (crates-io))

(define-public crate-js8call_lib-0.0.1 (crate (name "js8call_lib") (vers "0.0.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)))) (hash "1kskz3ziryk1p8x60nw2shmxkyz1180957xhm9lar5qajixhy7cm")))

(define-public crate-js8call_lib-0.0.2 (crate (name "js8call_lib") (vers "0.0.2") (deps (list (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)))) (hash "14gfcwx8yjn1m3lixrs5qgm1adm91fn9fj1w0qgg30dz7yk3b1am")))

