(define-module (crates-io js mx) #:use-module (crates-io))

(define-public crate-jsmx-0.0.1 (crate (name "jsmx") (vers "0.0.1") (hash "11f3ds7nn8cjx846d1hnwl2r5lmqr8x3nrxfr0csmklxxif3c7is") (yanked #t)))

(define-public crate-jsmx-0.1 (crate (name "jsmx") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h9swdzj13dyr7q9lcvqjm39f4ksfdg7ikamzj0lb4x9lhhfwsxj") (yanked #t)))

(define-public crate-jsmx-0.1 (crate (name "jsmx") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m29d7jdd9xspdgx9vnqjxbr5gm64cm9c8ixflcm31kjp35x6qmm") (yanked #t)))

(define-public crate-jsmx-0.2 (crate (name "jsmx") (vers "0.2.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c5qg29xladcvp6zsm1jb8qkdy84vvw130nzhr1x3xvvn4nv8sk0") (yanked #t)))

(define-public crate-jsmx-0.2 (crate (name "jsmx") (vers "0.2.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hgk15vykmly4v63xyprq1pwfn2am8lg1886iv8a3iib872f7a7r") (yanked #t)))

(define-public crate-jsmx-0.2 (crate (name "jsmx") (vers "0.2.2") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hvmmz3ik147mmy9w03vxmgfx1bhsg14s1v2zx7gd74ghan4y7gh") (yanked #t)))

(define-public crate-jsmx-0.2 (crate (name "jsmx") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m7fij81n59gkng9b89xb932r9yph8xdn2pgwi27xdlrxnkzs6yb") (yanked #t)))

(define-public crate-jsmx-0.2 (crate (name "jsmx") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1377byaqk2k0w158yg9zfqzskwbb81f9shdz34apliqf9l5w9aga") (yanked #t)))

(define-public crate-jsmx-0.2 (crate (name "jsmx") (vers "0.2.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rz6hvgyfwdwk2migs7nlpqbspznsa6y50fhkndjdc6rwwgrbq9q") (yanked #t)))

(define-public crate-jsmx-0.3 (crate (name "jsmx") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1px19i3j36my7360z1iz8z2zfxarlggngdyq7138v6vclj7dnypw") (yanked #t)))

(define-public crate-jsmx-0.3 (crate (name "jsmx") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09xw7jp3n1il8q5njmydb5wx6i20z7yyp1xrm11ddnc155i4cy90") (yanked #t)))

(define-public crate-jsmx-0.3 (crate (name "jsmx") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04h0wxp4qmznycygalxn01h67g6znbk7sf7fxirwcqi94v94lc0a") (yanked #t)))

(define-public crate-jsmx-0.3 (crate (name "jsmx") (vers "0.3.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11dqid26bi7qankw1xsdab3ixwfmgi2mj3hbv11q0ab5408va5sm") (yanked #t)))

(define-public crate-jsmx-0.3 (crate (name "jsmx") (vers "0.3.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10nn5k1ylvpzn61njnlaj1ccj1xzi87ck4j2x4g602d225kgv52f") (yanked #t)))

