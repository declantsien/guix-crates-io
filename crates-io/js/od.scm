(define-module (crates-io js od) #:use-module (crates-io))

(define-public crate-jsode-0.1 (crate (name "jsode") (vers "0.1.0") (deps (list (crate-dep (name "jsode_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1czp48yrdwsqj1svhs7imkddrn8qjj6wicjvsj7xjnf6sbc2aysd") (features (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.1 (crate (name "jsode") (vers "0.1.1") (deps (list (crate-dep (name "jsode_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0akripk28w9praza6zr7akvgx2j66m6q5ly35gyfdnan6pqb08wb") (features (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.1 (crate (name "jsode") (vers "0.1.2") (deps (list (crate-dep (name "jsode_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "19y9ixjsm4qxhs9d2flasz1l9pan4s5ya2qz18drwr88k9vxk228") (features (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.1 (crate (name "jsode") (vers "0.1.3") (deps (list (crate-dep (name "jsode_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yv1rkpk1h17vqp7qb5s75fba75gd966cdxms4jaypqpnlf7f9bb") (features (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode-0.2 (crate (name "jsode") (vers "0.2.0") (deps (list (crate-dep (name "jsode_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h5v8wrap16fjiasr82r4q7lsw694c4nk2kvk4wch8y0hgymk0fw") (features (quote (("unstable") ("macro") ("default" "macro"))))))

(define-public crate-jsode_macro-0.1 (crate (name "jsode_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.57") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k64s1ka0an1kd4bkh66gqcdb7crzbii7rcsrkp7jkxsiq1vb2m8")))

