(define-module (crates-io js -b) #:use-module (crates-io))

(define-public crate-js-bindgen-0.0.0 (crate (name "js-bindgen") (vers "0.0.0") (hash "1ni9wkiqnk73gphqqxzpldrbkakpm5w11c6yy1xqa2rrmls05j03")))

(define-public crate-js-bindgen-0.0.1 (crate (name "js-bindgen") (vers "0.0.1") (hash "1w9fjcqilzh5q0q50achz3i4jd338a8nffpn5w19bkbhdlf08b6q")))

(define-public crate-js-bindgen-0.0.2 (crate (name "js-bindgen") (vers "0.0.2") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)))) (hash "015rkljhzxlgpy4953wcf4vw98jrv0hpm51929f3xz2w2s0xk0b1")))

(define-public crate-js-bindgen-0.0.3 (crate (name "js-bindgen") (vers "0.0.3") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)))) (hash "0jv9fn3nf42gfhq4h3gfas45cax81l9sg5c9mxpzjndaszkznjx9")))

(define-public crate-js-bindgen-0.0.4 (crate (name "js-bindgen") (vers "0.0.4") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)))) (hash "13f534bav2wy8rhilz18s9j27ylnh4clclkzp92p3vwkzn8l4fwq")))

(define-public crate-js-bindgen-0.0.5 (crate (name "js-bindgen") (vers "0.0.5") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)))) (hash "1jqljma3nxl45ppnhl7804k88pmnklaqfdblqgl2z80qpxp70c12")))

(define-public crate-js-bindgen-0.0.6 (crate (name "js-bindgen") (vers "0.0.6") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)))) (hash "0bdlr29m34whw817gvabhsmmlkarb8zq6fiyqprv62v114l9xi81")))

