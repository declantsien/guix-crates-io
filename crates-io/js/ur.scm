(define-module (crates-io js ur) #:use-module (crates-io))

(define-public crate-jsurl-0.1 (crate (name "jsurl") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (features (quote ("std" "preserve_order"))) (default-features #t) (kind 0)))) (hash "19gfm7anqc3imaf4flwqqk02r45hc9ifbsc9h2syix5qpgb3phph")))

