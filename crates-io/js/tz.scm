(define-module (crates-io js tz) #:use-module (crates-io))

(define-public crate-jstz-0.1 (crate (name "jstz") (vers "0.1.0") (hash "0wmyxi7wnfqwvjd712wxj39403c4d9zl4hrbj9cwpvq63vrxiskk")))

(define-public crate-jstz_api-0.1 (crate (name "jstz_api") (vers "0.1.0") (hash "1fcfs1qf3jim8wf4ajapv802czn27qjf46hksqhdn3d1fdl8bsja")))

(define-public crate-jstz_bridge-0.1 (crate (name "jstz_bridge") (vers "0.1.0") (hash "090wlnxv72bzw1j00b3hdgzv9g6287alzra6vzb8mx1g189kkf0j")))

(define-public crate-jstz_cli-0.1 (crate (name "jstz_cli") (vers "0.1.0") (hash "1401li0750z20rkl9xw3p4nzqj2wn6m53cp5ld8di61x5k4wj5qv")))

(define-public crate-jstz_core-0.1 (crate (name "jstz_core") (vers "0.1.0") (hash "0flrs3y05f44hz20m0qwv12cf8ml65gr1n2hz1mi93wqsbcsszs3")))

(define-public crate-jstz_crypto-0.1 (crate (name "jstz_crypto") (vers "0.1.0") (hash "0bk3zckdh58fxj9f4s3ipkp4aq1sb5bkdf00c3sgkpnr1194q9m0")))

(define-public crate-jstz_crypto-0.1 (crate (name "jstz_crypto") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "boa_gc") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tezos_crypto_rs") (req "^0.5.2") (kind 0)))) (hash "04yyxk22adknr6c8xf50liw8w94yr11bldwz9x16m1ba9s2aqsfh") (yanked #t)))

(define-public crate-jstz_crypto-0.1 (crate (name "jstz_crypto") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "boa_gc") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tezos_crypto_rs") (req "^0.5.2") (kind 0)))) (hash "136xcfy40fmn4m3bbd02bsq6fj5lfgz5r7ygvqpdsc8w5jk3bmw7")))

(define-public crate-jstz_kernel-0.1 (crate (name "jstz_kernel") (vers "0.1.0") (hash "1wcxf3b6jgdn8ahz1y04z7w73109jzxcmqn05b27bqaj8l7prfyj")))

(define-public crate-jstz_node-0.1 (crate (name "jstz_node") (vers "0.1.0") (hash "1qzjrhkdb253bjrpjscdlcj2n5hp2yby5r76x7nnn7n7ak38z4lw")))

(define-public crate-jstz_proto-0.1 (crate (name "jstz_proto") (vers "0.1.0") (hash "021szjcmhv1m2kwmhs5inxw92l3xmw949zbfyg5srmmzddwpg03v")))

