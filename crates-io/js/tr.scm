(define-module (crates-io js tr) #:use-module (crates-io))

(define-public crate-jstr-0.1 (crate (name "jstr") (vers "0.1.0") (hash "07rz9qmqp4kqqshd41chqf8bvcf6h5m9d6ycz5chyha14ryy9pxr")))

(define-public crate-jstr-0.1 (crate (name "jstr") (vers "0.1.1") (hash "0hkq11mx18f9cjqfx7068n7009wra6vfiw3blhvaa1jjq03ba8hl")))

(define-public crate-jstream-ext-0.1 (crate (name "jstream-ext") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)))) (hash "1863qdgmhmwricm43mdid9kd4plf0jislkk96m59w14w1wg6lqb2") (features (quote (("sink") ("default" "sink"))))))

(define-public crate-jstring-0.1 (crate (name "jstring") (vers "0.1.0") (hash "1w75lbg13kfzkxg0nj28m891814pzf3nb7caqz7mavc8ig8djfnv")))

