(define-module (crates-io js -r) #:use-module (crates-io))

(define-public crate-js-regex-0.1 (crate (name "js-regex") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1h5di1kw2n8qz7aa56i5ilvwfwchca4d196rwdf3wr8npfpgymj7")))

(define-public crate-js-regexp-0.1 (crate (name "js-regexp") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "176myvvks4xc4czna6rdan1gyp9l33qjyy9gnvmmxhf8pw7yzj1h")))

(define-public crate-js-regexp-0.1 (crate (name "js-regexp") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "0qpvbr5lgcd8fahnwbmi1ycwvjhp49aj3w9x99v58bdqg0cr7mz8")))

(define-public crate-js-regexp-0.2 (crate (name "js-regexp") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "js-regexp-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "186j264a3w9hdwj9pqhpj0jmsd5nm9xkjbg1sh5cgdn0rf416ii9")))

(define-public crate-js-regexp-0.2 (crate (name "js-regexp") (vers "0.2.1") (deps (list (crate-dep (name "js-regexp-macros") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "0zq0sfy4d4inisz9rx56y0pvwnmh94xk6jdd2966rkrawpasgrq7")))

(define-public crate-js-regexp-macros-0.2 (crate (name "js-regexp-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.26") (default-features #t) (kind 0)))) (hash "1lj2g2jnkmg04qs59kkbdd04a3rsq09hprn5snyii898qdiczws3")))

(define-public crate-js-regexp-macros-0.2 (crate (name "js-regexp-macros") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.27") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lkadbyn0k93rx4jkdc8fyil3h8bjdnj92lm0a916r9l1wmrzx28")))

(define-public crate-js-resolve-0.1 (crate (name "js-resolve") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0yb0c0rygkk7rpizapbr0cmcis9yil48l4rb3qyi36gs8sfz0lxk")))

(define-public crate-js-resolve-0.1 (crate (name "js-resolve") (vers "0.1.1") (deps (list (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "02fninaf61afbrj13wmd3id79vipy2qdpi0bwdbkknvwygzh665y")))

(define-public crate-js-resolve-0.1 (crate (name "js-resolve") (vers "0.1.2") (deps (list (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1i5xzv53ikyrcdr5q71q9ylzwl0m289sv18m4xa40dmfl03w9l70")))

(define-public crate-js-resolve-0.1 (crate (name "js-resolve") (vers "0.1.3") (deps (list (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "07x6gis9bx5swllkcj2v66gwrj533bpi687y4038n7l0iih2jv91")))

