(define-module (crates-io js et) #:use-module (crates-io))

(define-public crate-jset-0.1 (crate (name "jset") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)))) (hash "1vzxvn6krmzigmykmx8fjrx7phk79vyjzs955p99wwrmp4kf87px")))

