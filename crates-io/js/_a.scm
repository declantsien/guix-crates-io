(define-module (crates-io js _a) #:use-module (crates-io))

(define-public crate-js_arc-0.1 (crate (name "js_arc") (vers "0.1.0") (deps (list (crate-dep (name "async-channel") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (kind 0)))) (hash "0xx5ia0k2kfkssa21a4rr0kgvbx3jccvlr5wbb7mfq9sngc8z95c")))

