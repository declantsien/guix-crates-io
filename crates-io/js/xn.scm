(define-module (crates-io js xn) #:use-module (crates-io))

(define-public crate-jsxn-0.1 (crate (name "jsxn") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 2)))) (hash "1kzy4s9rrjzq99z3x65r239aq3irsg9xi0gvj9k4zl8434ssjavj")))

(define-public crate-jsxn-0.1 (crate (name "jsxn") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 2)))) (hash "1cr80ywykiyqf54clzd8rnlg4gglp89izig6k7ii62bf58c0lwzh")))

