(define-module (crates-io js -f) #:use-module (crates-io))

(define-public crate-js-function-promisify-0.2 (crate (name "js-function-promisify") (vers "0.2.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.73") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.23") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.0") (features (quote ("Window" "IdbFactory" "IdbRequest" "IdbOpenDbRequest"))) (default-features #t) (kind 2)))) (hash "06khqk6hqfwnvchlhg58v4bdqp2df1i5rkscrl48zry2y2xcmc34")))

(define-public crate-js-function-promisify-0.2 (crate (name "js-function-promisify") (vers "0.2.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.73") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.23") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.0") (features (quote ("Window" "IdbFactory" "IdbRequest" "IdbOpenDbRequest"))) (default-features #t) (kind 2)))) (hash "1v0by7qzi97dzl8vzsm4na39qrnk6wnasifahnw0i68y6713ilrj")))

