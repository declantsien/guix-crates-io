(define-module (crates-io js _c) #:use-module (crates-io))

(define-public crate-js_call-0.0.1 (crate (name "js_call") (vers "0.0.1") (deps (list (crate-dep (name "cstring") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "woke") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1cdc9xpnpmrgr9hc118xxl1ac0vldjwi1qajszfkn87dwdazdnyy")))

