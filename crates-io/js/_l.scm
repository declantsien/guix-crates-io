(define-module (crates-io js _l) #:use-module (crates-io))

(define-public crate-js_lib-0.0.1 (crate (name "js_lib") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0r8faa4qqqygkjm2x23j2g1fih18y5ggmi439v4pzkml0kbxqff8")))

