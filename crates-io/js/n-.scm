(define-module (crates-io js n-) #:use-module (crates-io))

(define-public crate-jsn-base-0.1 (crate (name "jsn-base") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0p7y2bqk40ah7qi9s0jqg59s4h0fcfb5zs21wv4vwcjj3k5gvnp7")))

