(define-module (crates-io js to) #:use-module (crates-io))

(define-public crate-jstorchtools-0.1 (crate (name "jstorchtools") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.21.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "1272rfy7kgqcjvh7spqw6l27wdgdh9h3cgppwpq557bada93aqxf")))

(define-public crate-jstorchtools-0.2 (crate (name "jstorchtools") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.21.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "09g83dw8932q6bf2yf3kpvfq984n814gb6mizgip58c45x387gm5")))

(define-public crate-jstorchtools-0.2 (crate (name "jstorchtools") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.21.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "19qgh1lrhc25mjbnciryidpcmkp62qa7wmdb7ccwngl9s775wwlp")))

