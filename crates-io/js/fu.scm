(define-module (crates-io js fu) #:use-module (crates-io))

(define-public crate-jsfuck-0.1 (crate (name "jsfuck") (vers "0.1.0") (hash "13sngxrq76dibr879k59fcc039zv2rqn8lx0qy3xw7xhk76pba9c") (yanked #t)))

(define-public crate-jsfuck-1 (crate (name "jsfuck") (vers "1.0.0") (hash "0wvx9vpbyxk5xh2134jxgh4y1s1vrg34r2y3sc0mk98vw6xmh7jh") (yanked #t)))

(define-public crate-jsfuck-1 (crate (name "jsfuck") (vers "1.0.1") (hash "1na152knsxa4xqanzlyjsxz93h0bkj9qvzsxdqx7fmwrfg5irjr7") (yanked #t)))

(define-public crate-jsfuck-1 (crate (name "jsfuck") (vers "1.0.2") (hash "070c6c9i6w8ch2jv9bvnckfnsib8nw733v6ag80r0bng3dpgjipx") (yanked #t)))

(define-public crate-jsfuck-1 (crate (name "jsfuck") (vers "1.0.3") (hash "114ik1hh9a16q2vn1vxk90b19xlagi6jx9ikfcbcfax8j224yvzc") (yanked #t)))

(define-public crate-jsfuck-1 (crate (name "jsfuck") (vers "1.0.4") (hash "06gx6j17ibjpdayfpkfarhrhk6w0y5yw1dad9vghkz9dnfhdlvmb") (yanked #t)))

(define-public crate-jsfuck-1 (crate (name "jsfuck") (vers "1.0.5") (hash "0ff21pj3jszy7fs5yb39ka6sb3flvhfyzibasqrgfk4hnpa0fzjz") (yanked #t)))

(define-public crate-jsfuck-1 (crate (name "jsfuck") (vers "1.0.6") (hash "1ib5pg93fzw2ibfd96i99swy63097nb1j4ijrii45lkdmk6mlhh3")))

