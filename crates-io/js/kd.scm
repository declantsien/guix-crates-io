(define-module (crates-io js kd) #:use-module (crates-io))

(define-public crate-JskDict-0.1 (crate (name "JskDict") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zsic25m4wjz5kjlwxn2y9yqz1vf68m2vcln89jidq00si8gpd1v")))

