(define-module (crates-io js no) #:use-module (crates-io))

(define-public crate-jsnom-1 (crate (name "jsnom") (vers "1.0.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "14nz16q1mf57rri0arwqb7j84m7vcr7cgv4qjgvclnh41vhg95w5")))

(define-public crate-jsnom-1 (crate (name "jsnom") (vers "1.0.1") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "1p78li19sb9zqn4p8k7jbi704wg0h8ygak3g5gc08lbais0w15q6")))

