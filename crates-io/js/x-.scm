(define-module (crates-io js x-) #:use-module (crates-io))

(define-public crate-jsx-one-0.0.1 (crate (name "jsx-one") (vers "0.0.1") (deps (list (crate-dep (name "swc_common") (req "^0.11.0") (features (quote ("tty-emitter"))) (default-features #t) (kind 0)) (crate-dep (name "swc_ecmascript") (req "^0.46.0") (features (quote ("parser"))) (default-features #t) (kind 0)))) (hash "1xz2hyllr72vdrj3dpsm1x8hgi92wq652gl1fpagm89r8fsrs9al")))

