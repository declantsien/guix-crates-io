(define-module (crates-io js _p) #:use-module (crates-io))

(define-public crate-js_parser-0.1 (crate (name "js_parser") (vers "0.1.0") (deps (list (crate-dep (name "aleph-syntax-tree") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rslint_parser") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0c36w4mcqsghmjvzy5knch6ixsrmdp4n1f5ac61db8sz9p14jdjd")))

