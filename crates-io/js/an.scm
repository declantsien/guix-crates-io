(define-module (crates-io js an) #:use-module (crates-io))

(define-public crate-jsandbox-0.1 (crate (name "jsandbox") (vers "0.1.0") (deps (list (crate-dep (name "deno_core") (req "^0.165.0") (default-features #t) (kind 0)) (crate-dep (name "deno_runtime") (req "^0.91.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.1") (default-features #t) (kind 0)))) (hash "11kj0086nqak1znsp7a0z25ldlwd4lwajnv9rxajiz3j4f4vsp9c")))

(define-public crate-jsandbox-0.2 (crate (name "jsandbox") (vers "0.2.0") (deps (list (crate-dep (name "deno_core") (req "^0.204.0") (default-features #t) (kind 0)) (crate-dep (name "deno_runtime") (req "^0.126.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.1") (default-features #t) (kind 0)))) (hash "0p9jgqrqhjjv2c28kx0cw7f8gb7i8r7x75n08sy5cb6zlx8w2abf")))

(define-public crate-jsandbox-0.3 (crate (name "jsandbox") (vers "0.3.0") (deps (list (crate-dep (name "deno_core") (req "^0.204.0") (default-features #t) (kind 0)) (crate-dep (name "deno_runtime") (req "^0.126.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.1") (default-features #t) (kind 0)))) (hash "1ddcnjrk6sl0n9dddg1ibw9q3c4462dzd4im8swq7rzjljxxq2b4")))

