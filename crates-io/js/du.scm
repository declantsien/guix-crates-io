(define-module (crates-io js du) #:use-module (crates-io))

(define-public crate-jsdu-0.1 (crate (name "jsdu") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "wild") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0fdh38327lqsivmhw7l1a2bmncaa2dvs84k075p2idid95xd5mf6")))

