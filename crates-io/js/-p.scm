(define-module (crates-io js -p) #:use-module (crates-io))

(define-public crate-js-promises-0.1 (crate (name "js-promises") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 2)) (crate-dep (name "stdweb") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "06wnqxhwg0yw5azf92hbd3hrq8zhsamzvw1a7gxs551kw9rcickw")))

