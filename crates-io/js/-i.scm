(define-module (crates-io js #{-i}#) #:use-module (crates-io))

(define-public crate-js-intern-0.1 (crate (name "js-intern") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "0rg9a9qvnfxbhpx9gmlh3gnf9wdy717cnp7gy8zqwr8di8mvi7vb")))

(define-public crate-js-intern-0.2 (crate (name "js-intern") (vers "0.2.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2.40") (default-features #t) (kind 2)))) (hash "1f4pwjyxipqhlymrn8zpsi7i2yv9yx6hahm858d9qyimd3rpqlkc")))

(define-public crate-js-intern-0.2 (crate (name "js-intern") (vers "0.2.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2.40") (default-features #t) (kind 2)))) (hash "1ybfy5q8w9nd5j7w24nii3zim01gz29nysqpid198gj5fq8iw0q3")))

(define-public crate-js-intern-0.3 (crate (name "js-intern") (vers "0.3.1") (deps (list (crate-dep (name "js-intern-core") (req "= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "js-intern-proc-macro") (req "= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2.40") (default-features #t) (kind 2)))) (hash "117xs2wf3fhsv2xkm8mf7xjaa5nl6l88j1z5axc2847c6sq0x6aw")))

(define-public crate-js-intern-core-0.3 (crate (name "js-intern-core") (vers "0.3.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2.40") (default-features #t) (kind 2)))) (hash "15lpdg9m86fbbgalzj1hpcbwy0iwp7i1kpl9f5xmm8yp9dsm5qbs")))

(define-public crate-js-intern-core-0.3 (crate (name "js-intern-core") (vers "0.3.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2.40") (default-features #t) (kind 2)))) (hash "1vrmng7zxy0a4az37b4n79f6s73098cdhzz5096ywimcpwfnq9gg")))

(define-public crate-js-intern-proc-macro-0.3 (crate (name "js-intern-proc-macro") (vers "0.3.0") (deps (list (crate-dep (name "js-intern-core") (req "= 0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "052pk1zlf3x6jhfbgpr1ngrn55dwwwniab7k77icf4n13rqac7jb")))

(define-public crate-js-intern-proc-macro-0.3 (crate (name "js-intern-proc-macro") (vers "0.3.1") (deps (list (crate-dep (name "js-intern-core") (req "= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18qkxmghqb8h5ichxqgrcwv0y0ihvl7w2320wj58548whifyig4f")))

