(define-module (crates-io js -m) #:use-module (crates-io))

(define-public crate-js-macros-0.1 (crate (name "js-macros") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "123xm0w0gc1pp32h4kb1hkygldgqb1kshw23wm7xdk41xbr2c6yw")))

(define-public crate-js-macros-0.1 (crate (name "js-macros") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "18ss0gwr7xr3r2caghgs2as18j8vld02rlifv8slz6f25hl01nm6")))

(define-public crate-js-macros-0.1 (crate (name "js-macros") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1dql6ww5n1wspn0rx5pqbjbd00bmyz0zw3fb8zcibbf2nvjq7nnc")))

(define-public crate-js-macros-0.1 (crate (name "js-macros") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "052gxcwhdhjz0hal5b5fdk30vwm7l584vkl3ggc7f986yki94bma")))

(define-public crate-js-macros-0.1 (crate (name "js-macros") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "12hjv7ykkxxzag4rpbg35i7j8gamkaayxhbb39knsw379vagj6v4")))

(define-public crate-js-macros-0.1 (crate (name "js-macros") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1aqadxkm7r3y0hzmiwnwi2a07336fp05kzfd3lb0kc3a554yknzg")))

