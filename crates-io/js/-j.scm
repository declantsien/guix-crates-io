(define-module (crates-io js -j) #:use-module (crates-io))

(define-public crate-js-json-query-0.3 (crate (name "js-json-query") (vers "0.3.0") (deps (list (crate-dep (name "argh") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "v8") (req "^0.39.0") (default-features #t) (kind 0)))) (hash "0sg7a5k35x17ljym05fp300h3kmpw4hvgk9yddvxqa987w8l6sk2")))

(define-public crate-js-json-query-0.3 (crate (name "js-json-query") (vers "0.3.1") (deps (list (crate-dep (name "argh") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "v8") (req "^0.39.0") (default-features #t) (kind 0)))) (hash "1g8ngsl9svqwaj3bqali8ss96rhj7kpvjzgpwfxpvy8479dy8wh8")))

(define-public crate-js-json-query-0.4 (crate (name "js-json-query") (vers "0.4.0") (deps (list (crate-dep (name "argh") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "v8") (req "^0.39.0") (default-features #t) (kind 0)))) (hash "1jn4804ami5va96jy1kahd1nwfrdhnn3yglqf1rnw57hfhxcd2vy")))

