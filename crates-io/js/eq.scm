(define-module (crates-io js eq) #:use-module (crates-io))

(define-public crate-jseqio-0.1 (crate (name "jseqio") (vers "0.1.0") (deps (list (crate-dep (name "ex") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "12pjycwhdgm6njjihpla4psb6kcyn0xncnm4g0wxpv0xn9plsm1a")))

(define-public crate-jseqio-0.1 (crate (name "jseqio") (vers "0.1.1") (deps (list (crate-dep (name "ex") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1f43n96lnvnamxgh6xknj0wlk5g1rd56lp6rg4k2qnd0knlxgx49")))

(define-public crate-jseqio-0.1 (crate (name "jseqio") (vers "0.1.2") (deps (list (crate-dep (name "ex") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "107447pz6ph9356p5lr5g6bf3210wwhrzcd0r4pmsidag1p7g1gb")))

