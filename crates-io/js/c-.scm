(define-module (crates-io js c-) #:use-module (crates-io))

(define-public crate-jsc-sys-0.1 (crate (name "jsc-sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "07v4pkgm6n0yc2ndcyig91l30481k53aysyip1viszzikjv01z3s")))

(define-public crate-jsc-sys-0.1 (crate (name "jsc-sys") (vers "0.1.2+r201969") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1p6adcrfrm4xlw8xn0nijqbrhsi7s4hix3vj0rr615picschjwg3")))

