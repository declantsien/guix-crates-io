(define-module (crates-io js mn) #:use-module (crates-io))

(define-public crate-jsmn-0.1 (crate (name "jsmn") (vers "0.1.0") (hash "0cq7h2j5wnkj190a6gwwqjm1lgiyibpl176j8585ivr0d1knchwa")))

(define-public crate-jsmn-0.1 (crate (name "jsmn") (vers "0.1.1") (hash "1vngw5yln39h7npl3z63l7jgkchbpd59zq733jr3a3i07is8c152")))

(define-public crate-jsmn-0.1 (crate (name "jsmn") (vers "0.1.2") (hash "1akwmfsmzspyd56bph45w80anmlhp7pvdxnnkijzly35mvkql573")))

(define-public crate-jsmn-0.1 (crate (name "jsmn") (vers "0.1.3") (hash "1szngbxmfpa0k20h904vv5ai7wjjl7gjfxi813sy82fy3gp6ggaa")))

(define-public crate-jsmn-0.1 (crate (name "jsmn") (vers "0.1.4") (hash "09s2iyawry14rwrmdrv1qnvnzcpx06zkfhinfs2v6m4vf0da579m")))

(define-public crate-jsmn-0.1 (crate (name "jsmn") (vers "0.1.5") (hash "0rpyj2hcy72y5b7ypwz5j350bqfgf7ihijzwhsfmld91dm75yd03")))

(define-public crate-jsmn-rs-0.1 (crate (name "jsmn-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)))) (hash "11cprbicfxkbzd0rhhrv6fzzs77xxdnyjgf8xl4w5rygwayjklmp") (features (quote (("strict") ("parent-links")))) (links "jsmn")))

(define-public crate-jsmn-rs-0.2 (crate (name "jsmn-rs") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.25") (default-features #t) (kind 1)))) (hash "1y11kr0gik38z1kbp51799w0fpvbj014xdxr2w5d6iywf07rqm7q") (features (quote (("strict") ("parent-links")))) (links "jsmn")))

