(define-module (crates-io js _i) #:use-module (crates-io))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0zrxy7sf6m6gb4jr9m0bgdx6zjl0ggcf6wal7q2r87c52mggczy2") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.1") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1l1jjjzhyf5z34rkial3jhif33d9lffj64llfvf3yjr7s35c3p8l") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.2") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1z7w1sz467yycj24ccaxzb2c89y2rn1ig5iwid6560wlyrf68a3s") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.3") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "09dcb6yv3j1cw8vv9g3cld7p9jgjzbip3ml9jvfw0y1dcyvdhds9") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.4") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0brsf20c70a0mmp4lllkdxfh944l943dhjjh02gkdw1v7z8w0zgr") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.5") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0cfzrcc6psqap25ah5ha0dmacmpskgykbq33b1ksv257f2rppavp") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.6") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "09f3833nha0p0fcy3p7qg8pfzrj13i2kmkc1xqkqk8v9nbqhh40d") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.7") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0g2s2p0a9d819yxmxn1j04lz9c9f51zgscz85c767lawi90iq021") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.8") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0rjgg2nxv8ahb5qj8kbb4rx0xin1svqw54k5glm2y4k40pb66aqv") (features (quote (("std") ("default" "std"))))))

(define-public crate-js_int-0.1 (crate (name "js_int") (vers "0.1.9") (deps (list (crate-dep (name "rocket_04") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "rocket")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1wljgn25m945bjimgqz0nx2lrc2lvrlll92zk3fdd89m6bsrfrxr") (features (quote (("std") ("lax_deserialize" "serde") ("default" "std"))))))

(define-public crate-js_int-0.2 (crate (name "js_int") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1k1ar0wkb88nvm8cbzjxj1p8yjqp7axjaqpk70dphsx9g3h8kbpw") (features (quote (("std") ("lax_deserialize" "serde") ("default" "std"))))))

(define-public crate-js_int-0.2 (crate (name "js_int") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1zbly9dfmlja7v5x8vqjxbipanwf6qzb4dip9d5qlmhrrndvmyny") (features (quote (("std") ("lax_deserialize" "serde") ("default" "std"))))))

(define-public crate-js_int-0.2 (crate (name "js_int") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "02hn954yv5wksn26ck6lq19y3a0sswapf82hi65www5jf1agjdyr") (features (quote (("std") ("lax_deserialize" "float_deserialize") ("float_deserialize" "serde") ("default" "std"))))))

