(define-module (crates-io js _t) #:use-module (crates-io))

(define-public crate-js_typify_gostruct-0.1 (crate (name "js_typify_gostruct") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "06qx4kmalc42pip3h60m03n7ca3bp1jl5rb20wfs1aq9yg99gg5i") (yanked #t)))

(define-public crate-js_typify_gostruct-0.1 (crate (name "js_typify_gostruct") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "12k6j1j8qd5qn1rspsiw2mj3p73wh744pcd10mpqv88ip282idys") (yanked #t)))

(define-public crate-js_typify_gostruct-0.1 (crate (name "js_typify_gostruct") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0jp614irg5h14ah52k01zhdkfijdmqqwwnjq6zgql442mihc3wb0") (yanked #t)))

(define-public crate-js_typify_gostruct-0.2 (crate (name "js_typify_gostruct") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "15ayq9n72ciyrm9py9y91d32hf9l83g3pxf7n0p0jmppqk22wg7s") (yanked #t)))

(define-public crate-js_typify_gostruct-0.3 (crate (name "js_typify_gostruct") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1qy7nnyszaprrks8zdk3qw73cd1ggji95zm9nhr18ksn82yj9a64") (yanked #t)))

(define-public crate-js_typify_gostruct-0.3 (crate (name "js_typify_gostruct") (vers "0.3.1") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0lfnpnbj626ld4gvy68aqpic6sjzddjawmm3fd1ibdrzvzr6yvk7") (yanked #t)))

(define-public crate-js_typify_gostruct-0.3 (crate (name "js_typify_gostruct") (vers "0.3.3") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0vz9lrbznjjj4hzf360xbxw67p67z37v33k18bfxfd3pkvjlx3i2") (yanked #t)))

(define-public crate-js_typify_gostruct-0.3 (crate (name "js_typify_gostruct") (vers "0.3.4") (hash "04pcgmhpd68kh27334gbj4ivj9rdaw5fhiz6jdszvssmqlkc4rhc") (yanked #t)))

(define-public crate-js_typify_gostruct-1 (crate (name "js_typify_gostruct") (vers "1.0.0") (hash "0snnwijrk6ybdr3y81nf0ly6rc43r8x3p8bw8rp565kv892vhqb7") (yanked #t)))

(define-public crate-js_typify_gostruct-2 (crate (name "js_typify_gostruct") (vers "2.0.0") (hash "07qjdbdbwac721h0sm9v83k2q358bvc23l8qch7qa35gwg1ichpj") (yanked #t)))

(define-public crate-js_typify_gostruct-2 (crate (name "js_typify_gostruct") (vers "2.0.1") (hash "161yzc3z6p1n3jpfx3688zy645nwjkd4p94x04wvn9b79rwxdzqx") (yanked #t)))

(define-public crate-js_typify_gostruct-2 (crate (name "js_typify_gostruct") (vers "2.0.2") (hash "1c3rgpy9cm50wpj68cigrcgrp4902iijbgwmzs238w9x3hnrrmsn") (yanked #t)))

(define-public crate-js_typify_gostruct-2 (crate (name "js_typify_gostruct") (vers "2.1.0") (hash "1gzh5s58lr510vdf924zq3q91fg1h5cv0khx45b9b3f5x4953zk3") (yanked #t)))

(define-public crate-js_typify_gostruct-2 (crate (name "js_typify_gostruct") (vers "2.1.1") (hash "05k3zlwcwjfqa2pnz21vnbm5qlq3jppc18lnqky0a8iaqxpwgghw") (yanked #t)))

