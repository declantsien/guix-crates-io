(define-module (crates-io yj um) #:use-module (crates-io))

(define-public crate-yjump-0.1 (crate (name "yjump") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "1y7z4qnj87v7jrpg94dhdj0djqs7l78jf2irsfm4hy754zdcw4zn")))

(define-public crate-yjump-0.1 (crate (name "yjump") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "1r1ri383jll1z1z2ia9zpsfdfz3vr5b41pw949izvsh2dvfvn901")))

(define-public crate-yjump-0.1 (crate (name "yjump") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "11vrx1ifylw2lyxn0hicdwkbcr2fpxzqgnipqc0mmxjcs2dd5ayh")))

(define-public crate-yjump-0.1 (crate (name "yjump") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "0g76slg35igg80hp912sm1g17s04nlvjms4xgsyb2n7b2b9bvbp9")))

(define-public crate-yjump-0.1 (crate (name "yjump") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "16rk9fybbp1w13lmswi2jw87vpybhr71d0f7nw0k42yjalaw5g1p")))

(define-public crate-yjump-0.1 (crate (name "yjump") (vers "0.1.5") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "0nrnbxja8z6vxkxfnydygpjqfbl1v8gw47vw8fk66dpzy2qilyg3")))

(define-public crate-yjump-0.1 (crate (name "yjump") (vers "0.1.6") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)))) (hash "0fqsdmchz7qb80v3nwvwgychpk0g9dbfi3n5fkwpv7axnrmp393w")))

