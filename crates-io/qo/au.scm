(define-module (crates-io qo au) #:use-module (crates-io))

(define-public crate-qoaudio-0.1 (crate (name "qoaudio") (vers "0.1.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)))) (hash "1d1pr62hhvv4h93dnvgqbv1k9snvcpqm1ili2mj1l15b3z5igwn4") (features (quote (("default"))))))

(define-public crate-qoaudio-0.2 (crate (name "qoaudio") (vers "0.2.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)))) (hash "1ggkpjkya5fbk9nybrsncz8brfw79g5zpnp4qw3m8rrqgv0ab1y4") (features (quote (("default"))))))

(define-public crate-qoaudio-0.3 (crate (name "qoaudio") (vers "0.3.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)))) (hash "038il8y4g3mka397bb0lkwwjiwab84a4l9g5ink23vxl8vb5whfd") (features (quote (("default"))))))

(define-public crate-qoaudio-0.4 (crate (name "qoaudio") (vers "0.4.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)))) (hash "0dzhgiljp482n835v9kzdrw4f46jvv5dn5r9incwv959lj8a1b9x") (features (quote (("default"))))))

(define-public crate-qoaudio-0.5 (crate (name "qoaudio") (vers "0.5.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)))) (hash "0caijgy2ni93v30f9vy81jbbp8dc8bf2cz1a2flyrhhrsx74gks0") (features (quote (("default"))))))

(define-public crate-qoaudio-0.6 (crate (name "qoaudio") (vers "0.6.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)))) (hash "1mrn790vgk1dvpwagyc3h2mf7s0k9cf1plbpgqa3wxnm28s2pzn9") (features (quote (("default"))))))

(define-public crate-qoaudio-0.7 (crate (name "qoaudio") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rodio") (req "^0.17.1") (optional #t) (kind 0)))) (hash "0h94zawnl8xsalrr2mncs3incgj8vnc47818r4mjqswhqdyv8imz") (features (quote (("default"))))))

