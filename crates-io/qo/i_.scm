(define-module (crates-io qo i_) #:use-module (crates-io))

(define-public crate-qoi_rs-0.1 (crate (name "qoi_rs") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 2)))) (hash "062s01516f8545af5s4sa3argqapw1fs8fnxrv42dn6p88xc0ri3")))

(define-public crate-qoi_rs-0.1 (crate (name "qoi_rs") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 2)))) (hash "0sannrmwcdgz21bq64rv9kxrji25x1bj56v9ba337v4c5bql9b7p")))

