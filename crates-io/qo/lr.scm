(define-module (crates-io qo lr) #:use-module (crates-io))

(define-public crate-qolrus-0.1 (crate (name "qolrus") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "00a6dln8jxhrjvmcq74a1cmqjhy1x8m64bsfswp799xgmf68fdc9") (rust-version "1.72")))

