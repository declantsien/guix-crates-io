(define-module (crates-io qo l-) #:use-module (crates-io))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.0") (hash "1ajgr7f1mybpbphdlyw9mscn7qqspagqc7ysa83yrhb3ip16gm48") (features (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (yanked #t)))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.1") (hash "1mxnfh22aadjmvch7rj74xkljxazszdkc1v18klcm257yjm500z3") (features (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (yanked #t)))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.2") (hash "04w9cfyyfcqb2xvync20wm2vizzgzhcykia7qgx76rvj0689ahav") (features (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (yanked #t)))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.21") (hash "140rm8qsis0akgw8shw619qaxw475f2jrd9cmw2z9ap7b263pzvv") (features (quote (("type_aliases") ("sae") ("macros") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases") ("default" "full")))) (yanked #t)))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.3") (hash "1icjk9iaylwv4svrvim9f1qz5vvj4abl3zyirwdsc89wp1j773cd") (features (quote (("type_aliases") ("sae") ("macros") ("generics") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases" "generics") ("default" "full")))) (yanked #t)))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.4") (hash "0zlrj5p1q749wflfvl67265628v36dy99snrlsdfwfn0qs9ay2nk") (features (quote (("type_aliases") ("sae") ("macros") ("generics") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases" "generics") ("default" "full")))) (yanked #t)))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.5") (hash "1zi8q87c1qm956c05rgcklllf94zli7xlkacp8mg09vzg81qv3wk") (yanked #t)))

(define-public crate-qol-rs-0.1 (crate (name "qol-rs") (vers "0.1.41") (hash "0pswq7v8asa79jja3srgwc0yqa2xxzvdfvhmlf9hxvxk0f6c69s7") (features (quote (("type_aliases") ("sae") ("macros") ("generics") ("funcs") ("full" "macros" "sae" "funcs" "type_aliases" "generics") ("default" "full")))) (yanked #t)))

