(define-module (crates-io qo pt) #:use-module (crates-io))

(define-public crate-qopt-0.1 (crate (name "qopt") (vers "0.1.0") (hash "0w12jc709zz33ja3pyad34zhmzy7d34c3d5mz1js8banzpkkhr6l")))

(define-public crate-qopt-0.2 (crate (name "qopt") (vers "0.2.0") (hash "0zz3jbwbpw58ri19qx1ydnqdkg6zj7vmxbpf8kqkfmwckj67an3w")))

(define-public crate-qopt-0.2 (crate (name "qopt") (vers "0.2.1") (hash "0kqrq511a6pbngazippc710kna5zz5qyz6pchai719ry4mgi114l")))

(define-public crate-qopt-0.3 (crate (name "qopt") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1dvzix7qja6hr50spn2b7339i605y8j8qid4lqvgfryky2xk05gd")))

(define-public crate-qopt-0.4 (crate (name "qopt") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0xld2kwgr6qghhkn31qp6n2s4yrg472ns4avyvzrxgyvfkck5q4l")))

(define-public crate-qopt-0.4 (crate (name "qopt") (vers "0.4.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1sdy0ik8892si1srpkgyisvnmqgz2grn3mpwv7far4m7w3ysbmvb")))

(define-public crate-qopt-0.5 (crate (name "qopt") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0wmqgb9fwvxn1xhzlzcwa0grsl2h9cqk113ddc5py628a8idy8gi")))

(define-public crate-qopt-0.5 (crate (name "qopt") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "067hlv2pdq8y5ri6g3aah02mar2aikqfm3kcp6kkkf991zipf717")))

(define-public crate-qopt-0.6 (crate (name "qopt") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "09rkk1kmr30yjqw15b062cw9cjcq3s1rhd8iz5d4xcsld9fmndnc")))

(define-public crate-qopt-0.7 (crate (name "qopt") (vers "0.7.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "10ja16q5hirr5hzm9g3bckldxnnv7m2zkgy62xiy6k3dd9cd37q0")))

(define-public crate-qopt-0.7 (crate (name "qopt") (vers "0.7.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1xxrjblgykd30hcjsh4j8177clxmrgyigdygqkpzasfz7v3j3lrj")))

(define-public crate-qopt-0.7 (crate (name "qopt") (vers "0.7.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "05dbaaav5nnhgds10rvi3mq23y5i6kxga1kw9ipv9jfkl4p54h1z")))

(define-public crate-qopt-0.7 (crate (name "qopt") (vers "0.7.3") (deps (list (crate-dep (name "maria-linalg") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "16sm9ah1h3l90njrvaj7vy2c9mnbi7m5al2gjbnv4141iixflhig")))

(define-public crate-qopt-0.8 (crate (name "qopt") (vers "0.8.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0q5m9jk3jgg70l1wc6rcifb4ni50d6z13z1h6hrpfh6pfbnrxi9x")))

(define-public crate-qopt-0.9 (crate (name "qopt") (vers "0.9.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0zmri4j1bdpq4k3j8i40pdyvnsq8xk3znmra5h05i0cawlv7cils")))

(define-public crate-qopt-0.9 (crate (name "qopt") (vers "0.9.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1fx839rwkbaql8np4zdsdkakgq726dsf9dxmff13rzs9mkx27xg8")))

(define-public crate-qopt-0.9 (crate (name "qopt") (vers "0.9.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1mddl3lycmn6yplzldmwmw2kxpglpajnr90kv19f4az6ar7v5s2l")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "06hh0ykf0nga80qpnc3391xyigv9gprfs6nv7wwbyd16szppmx2v")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1jndwfhykv8al2xmd90y6sxbnla8njcgzdhxawvw6vs38kjn8fcv")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0p8qxc0wrijz3cb7ksyp34bz3a6azay7fafgp4662fv86avagdgs")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.3") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "16gcc492bfgvnaj6vjg0c9x06qc7904fvnp5a8c4ia8f1gc78s53")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.4") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1n5q2fc8ldrjg2cj6xz54mmgsx8121wh1rpx6radss63bsyxpmbd")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.5") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1yfw6xcchasfgi42h3pnh40bd8yc2vcnc0r4j6p8xyk4wa6q0xl3")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.6") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "141zaxm2g6f1y5j3bzn4axi8cnkl0sq506crr8ra2zz43gsrafxw")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.7") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0x9hdgsh2qx9zcv105l9b2sdwgybx4a6fhqaq5z4z22kyhs6fhzb")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.8") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1gkvcksm453x9gphgz6fwwbrjkv9r1p5bwrk3kja51n64yf9w2ys")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.9") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "016qp0h9xnymn4f06d9wl8h3084dcfz9rhfw5xs7pnsbdcl9k634")))

(define-public crate-qopt-0.10 (crate (name "qopt") (vers "0.10.10") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0f0li1h01acwb0i6q6wpb3l0kmpnnx6vm5a419c4zvb4k3j4n45g")))

(define-public crate-qopt-0.11 (crate (name "qopt") (vers "0.11.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "019c2ddhp02x2gkn5fr04rpk4simvf6svrjngfpdqgj5n0n1rm1b")))

(define-public crate-qopt-0.11 (crate (name "qopt") (vers "0.11.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0r0r8gxs0kx658x9ql9p228qjj4w34bvfin16xxc3qj8llaz1nf9")))

(define-public crate-qopt-0.11 (crate (name "qopt") (vers "0.11.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0jja6drvxcgwjqz3khamildifvhf6rbhqdv9m72r29rl8i4rpl71")))

(define-public crate-qopt-0.12 (crate (name "qopt") (vers "0.12.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1m95qmyfsyn63v0cscrh17g7ixcjlls8si6bg25hm71vw1qkh7zm")))

(define-public crate-qopt-0.13 (crate (name "qopt") (vers "0.13.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1w0plkgw7i8gpnr569q9l4x8q2j3vbjk3waapx7xgim1bjypx3f1")))

(define-public crate-qopt-0.14 (crate (name "qopt") (vers "0.14.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0i2cyvnjhjpwxa7q1q5l4namm61999yd09cmy23h9y0k42pr7k2q")))

(define-public crate-qopt-0.14 (crate (name "qopt") (vers "0.14.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1p89kfqkvd1gmn0fapg3c0zwc56xlbnzyd1idry6jj7nqa4xx6b3")))

(define-public crate-qopt-0.14 (crate (name "qopt") (vers "0.14.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0jv7shaj58ivfpqn60sh8xm40hrqsmrhmyn9vd3r3wffzbw7iiwp")))

(define-public crate-qopt-0.14 (crate (name "qopt") (vers "0.14.3") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "050lqy7ynasbm38c8arpin6c78vpykxbzkdqwp56nshvw70fxd26")))

(define-public crate-qopt-0.15 (crate (name "qopt") (vers "0.15.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1rrxpndb3ikpmd0fxkslwx02kyyrb3c3mww3m8b7k0gx9yirz508")))

(define-public crate-qopt-0.15 (crate (name "qopt") (vers "0.15.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0ns55xxx1akhfb19lv0v7yyln562h59yr5m1lvz0m7l0b5r27p72")))

(define-public crate-qopt-0.15 (crate (name "qopt") (vers "0.15.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0z0c4yqlfcs1px97jxkb5ajrg2d7s5qcmkkchh36yn6zqn4hrfx3")))

(define-public crate-qopt-0.16 (crate (name "qopt") (vers "0.16.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "15f8p0fvn4g3w54b7srj17pwbkz36c74q7m4p3pysxsldxdyrx4s")))

(define-public crate-qopt-0.17 (crate (name "qopt") (vers "0.17.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1yml7kphf75ixy5j2vanb8cawlji599shjzlrmpbpc4xfd3m2dmb")))

(define-public crate-qopt-0.18 (crate (name "qopt") (vers "0.18.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1sxbmq3xnhdqhbgkqhk6g7qv7agfayk0xyydxw332k5rg1q84px6")))

(define-public crate-qopt-0.18 (crate (name "qopt") (vers "0.18.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1jjlsfnb020lcmvcq3576a51wzbd1vx9vycwnzr6vzi7lwmwim0a")))

(define-public crate-qopt-0.18 (crate (name "qopt") (vers "0.18.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1656296an38l3647lk05ac05b0s22810z3bfmfhxqihjrgz8gp81")))

(define-public crate-qopt-0.19 (crate (name "qopt") (vers "0.19.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1cn599q02vrxnvbgss273wkkc5jfh45ls3v06l2pbvg7disivjzh")))

(define-public crate-qopt-0.19 (crate (name "qopt") (vers "0.19.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "130inm7222vpgl3fnhql6snn6472l87gmlkd72g1din5a0hx0i30")))

(define-public crate-qopt-0.19 (crate (name "qopt") (vers "0.19.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0ia9b22pb3mjmzmc4sg0xxi750g65yg55nvy1kfdjbkhxmfvjdsl")))

(define-public crate-qopt-0.19 (crate (name "qopt") (vers "0.19.3") (deps (list (crate-dep (name "maria-linalg") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1vpz73g5vyg1kj76x3la48f97pg1r70znmxk20v31nkragavla5n")))

(define-public crate-qopt-0.19 (crate (name "qopt") (vers "0.19.4") (deps (list (crate-dep (name "maria-linalg") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0l9ilhw6fl8167mdjwyx38q74wdpa5c7a0bylfhcb6nhy4ndd7d5")))

