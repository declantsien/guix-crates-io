(define-module (crates-io qo ic) #:use-module (crates-io))

(define-public crate-qoiconv-0.1 (crate (name "qoiconv") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "libqoi") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00g53hh62zz28sjkjj2j655smn6ir0q6pfs6pkkk2s59gcnimcbm")))

(define-public crate-qoiconv-rs-0.5 (crate (name "qoiconv-rs") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0k5mng6vdfk62b05a8gwivygy98x73bjl4hgzbdqf80miczljckf")))

