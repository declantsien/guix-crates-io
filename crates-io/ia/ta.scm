(define-module (crates-io ia ta) #:use-module (crates-io))

(define-public crate-iata-0.0.1 (crate (name "iata") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1k6j8dm8nnxagizhpxr96z3553gab6yhfgslzcjidl1sg2hbzm6h")))

(define-public crate-iata-0.0.2 (crate (name "iata") (vers "0.0.2") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "065lqqaazvxbjsjrzjvxaa1w1c9cm3zy7vmrw5q4i6chfyymkiz4")))

(define-public crate-iata-0.0.3 (crate (name "iata") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "10zwid4c9wd928p7iz3dhfx402bll0hkpr17h6ily9m5piyin4rr")))

(define-public crate-iata-0.0.4 (crate (name "iata") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1sa5628cbb6cdh5rqfdbpiyn8xcwlw47x3wyw5adkgvmzr1yqy82")))

(define-public crate-iata-0.0.5 (crate (name "iata") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "15aywqs4vlf18r9gvwg6mfyg58k1h1fkd398wggdl5n83rqsz3ap")))

(define-public crate-iata-0.0.6 (crate (name "iata") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0r7kivilrvrcqgiqs3xw6933zd7vfq8w4zc4a3xfvsdjqgb55y3h")))

(define-public crate-iata-0.1 (crate (name "iata") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0mm1rbvgd8hjgwj1qpsmn3js1nrfbqdvsrj43rw55fm46jg0glb1")))

(define-public crate-iata-0.1 (crate (name "iata") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0i4fxjbbpgpqfy7y0288jyiicyigxpjwf9bk98ajc5lfjxv9ycyb")))

(define-public crate-iata-0.2 (crate (name "iata") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1wmalxxfdhzcq4xvb3dzc8hqhkmcmf81kdphy73dqd8qgdp2k7mr")))

(define-public crate-iata-0.3 (crate (name "iata") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)))) (hash "09s80df8mnmsrrcfk6ms27j45mzs1ciy0r9p0bk1aml00pyj1giv")))

(define-public crate-iata-0.4 (crate (name "iata") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2") (default-features #t) (kind 0)))) (hash "1iffmhkkcli2iv86my6dyjrp1y5j9h0wy2g6zl0df9lr3xp01b7w")))

(define-public crate-iata-0.5 (crate (name "iata") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1f3rb644cgmdrrxv18m48mwp2psi9y6xr2fcyp4zfcq254swnf68")))

(define-public crate-iata-0.6 (crate (name "iata") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "196gc0g21g8y52j7pyabfa0a9ly0i51q8ybrqzlw9gwngdqv5jy4")))

(define-public crate-iata-0.6 (crate (name "iata") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d9q3hhqzyj6bgypmh3sanf5r84amgabaamd064s8indf230898h")))

(define-public crate-iata-0.6 (crate (name "iata") (vers "0.6.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "028ww4c4s3cfv6qmwxcs5ynw47fhxmhvld7nngv4i1nf271rqqb9") (yanked #t)))

(define-public crate-iata-0.6 (crate (name "iata") (vers "0.6.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ncnl3wc75liiyfqqs2ldfzajjjj9asha0f6if6x9pqjxfpyizmk")))

(define-public crate-iata-0.7 (crate (name "iata") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1vw6h99c0w62pi2mxiphf5rddi9shqa7y0fip7syfrr50nbgbafv") (yanked #t)))

(define-public crate-iata-0.7 (crate (name "iata") (vers "0.7.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0afly973m5vbcfics267ykv3vqn2wdim7rbsrrxrawczmjcf8x6i") (yanked #t)))

(define-public crate-iata-0.8 (crate (name "iata") (vers "0.8.0-alpha.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bzcmbshaij73ll064zjcgib2w9djin96ca4b2ivgzrvr0849vzw")))

(define-public crate-iata-0.8 (crate (name "iata") (vers "0.8.0-alpha.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i76wpv2cwbaypvwz06iwamy6s86p2i38hq0x3pnyphgqvg3ayg9")))

(define-public crate-iata-types-0.1 (crate (name "iata-types") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "04rllqgblmsap3vjm0pjjwhgz3r7icnr23mhj3n94bprbf6hiclh")))

(define-public crate-iata-types-0.1 (crate (name "iata-types") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "0hl5zw4w5sd0njw6rm73qbwbyj8zsh9zv96z4r46iic74z19xrcg")))

(define-public crate-iata-types-0.1 (crate (name "iata-types") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "0bpjx09cnv5b7d5k64clkcdq335af8qk4bglgp9qhdxqqg4a1pl1")))

(define-public crate-iata-types-0.1 (crate (name "iata-types") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "0mvqs8q0a0nyfsr2yizaiil034q5akiqkv3y5jz792xbnlcxyjxj")))

(define-public crate-iata-types-0.1 (crate (name "iata-types") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)))) (hash "168rbz6bx6zrwxml96r422d625xyp343myrbxna2fnmk955i9ajd")))

(define-public crate-iata_bcbp-0.1 (crate (name "iata_bcbp") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0b72qq0x3gc1sni8pdkzinfx2kk2jcghqpd2v8v9xrn6wjnvqm24") (yanked #t)))

(define-public crate-iata_bcbp-0.1 (crate (name "iata_bcbp") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qh4zqlx100475lr2zhydbpsa2gi7z2by6zswgb96i9vb9y3zvlg")))

(define-public crate-iata_bcbp-1 (crate (name "iata_bcbp") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "1i18687k63x2v9f15y33kdr9351zd9c98z2ba9f8ssrqgfdvcqdk")))

