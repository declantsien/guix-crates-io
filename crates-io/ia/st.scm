(define-module (crates-io ia st) #:use-module (crates-io))

(define-public crate-iastconvert-0.1 (crate (name "iastconvert") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "iasthk") (req ">=1.0.0") (default-features #t) (kind 0)))) (hash "0pasm8l2nmygy50zzgkmskhbdlqzz2lign887q84j53pnlc9j5ry")))

(define-public crate-iastconvert-0.1 (crate (name "iastconvert") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "iasthk") (req ">=1.0.0") (default-features #t) (kind 0)))) (hash "04fdh56cfq9iw2z2wmzf11rrgwh9z5lfkx8nmcg72q1nnh4pqr67")))

(define-public crate-iastconvert-0.1 (crate (name "iastconvert") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "iasthk") (req ">=1.0.0") (default-features #t) (kind 0)))) (hash "1hk8hwicf18jbp2z3jkkcr4lxpmi98h5ysrigqg0915nxiszy3yd")))

(define-public crate-iasthk-1 (crate (name "iasthk") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0x7s69qy4slpbypnwjn5xi5g3m6kbc3hkfi2i9vscgv4f833n9cy")))

(define-public crate-iasthk-1 (crate (name "iasthk") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0r51l11bksp20r92r1njbazc0364vyw6xa97b1iqwbw86sfzsz13")))

