(define-module (crates-io ia ca) #:use-module (crates-io))

(define-public crate-iaca-marker-macros-0.1 (crate (name "iaca-marker-macros") (vers "0.1.0") (hash "069pwmymdk5sidb690z0vv1lrx5x0j0z0ibxx8bp44gnf85994rl")))

(define-public crate-iaca-markers-0.0.1 (crate (name "iaca-markers") (vers "0.0.1") (hash "0nmcvd16vnsbsmr9sar5f7anm6v8pgs77h7qzvphsaldmcmac7rc")))

