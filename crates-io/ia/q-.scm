(define-module (crates-io ia q-) #:use-module (crates-io))

(define-public crate-iaq-core-0.1 (crate (name "iaq-core") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ihx8n1i6afagyjb7jc66wapnq1rk0ca3gk4yw80jm3q23syv9hh")))

