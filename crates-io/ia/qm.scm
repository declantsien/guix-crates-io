(define-module (crates-io ia qm) #:use-module (crates-io))

(define-public crate-iaqms-inmem-settings-0.1 (crate (name "iaqms-inmem-settings") (vers "0.1.0-dev.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1qkij6dblw1wga2jkmhw9zwmbxv892b7idklk23z7xcgrizfabyc") (yanked #t)))

(define-public crate-iaqms-inmem-settings-0.2 (crate (name "iaqms-inmem-settings") (vers "0.2.0-dev.0") (deps (list (crate-dep (name "redis") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1z3784hdri5q3lvlgxc5phnj7vnl5i5cz1485iyapm454wx62nwf")))

