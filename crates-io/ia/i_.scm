(define-module (crates-io ia i_) #:use-module (crates-io))

(define-public crate-iai_macro-0.1 (crate (name "iai_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "12fyx0y1by9vrh3vpkzwarpwgvhskc3bmcrjib3hp5jjh66167y9")))

(define-public crate-iai_macro-0.1 (crate (name "iai_macro") (vers "0.1.1") (deps (list (crate-dep (name "iai") (req "^0.1.1") (features (quote ("macro"))) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "12xx8qn2740dhxldivc2zhhvqmfb488ry1dr2qyxw1n4ps2pyli5")))

