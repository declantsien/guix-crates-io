(define-module (crates-io ek ey) #:use-module (crates-io))

(define-public crate-ekey-0.1 (crate (name "ekey") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11pyffg4qxpydxrm8gk9z8akfapkq5kg095rh4s9ppaxgc4bhyli")))

(define-public crate-ekey-0.2 (crate (name "ekey") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dv042n4j0bd7zlnpbwgya76y2gvz2mn2b4cssx6zj9qirkh7dl4") (features (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.2 (crate (name "ekey") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qh6mppp2ij1r4gjj5w66s3ba33nzcyq06c0s49bk9vnd5cq8d0b") (features (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.3 (crate (name "ekey") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16h2zvp60wkdscmk0x8ngxszj5dk661vccmy04abkhvgm9fzj7d0") (features (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.4 (crate (name "ekey") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vdzcbz0ic5mjzvb58is8n09zcz8bgvknjh8g2r7nshndbmmsv6s") (features (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.5 (crate (name "ekey") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1df2ml49qi4yr3zawcdm6jiyy6mldy1041p7g6g3wxrprcwp2x13") (features (quote (("std") ("default" "std"))))))

(define-public crate-ekey-0.6 (crate (name "ekey") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0j98rl982gsas90ly1rhnhmj0672wi8f3bdvz8rcc4ldij8vw0ib") (features (quote (("std") ("default" "std"))))))

