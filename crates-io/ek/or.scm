(define-module (crates-io ek or) #:use-module (crates-io))

(define-public crate-ekore-0.1 (crate (name "ekore") (vers "0.1.0") (hash "0xwvy590nw362h4rf9zj2kyys2kjn1vc9mhgx57nzcgsnxv3ypm3")))

(define-public crate-ekore-0.0.1 (crate (name "ekore") (vers "0.0.1") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1s8lx40874l4qla19wqmf1qgh8qqjzbjnxa51qzgvm81xh7cfla8") (rust-version "1.60.0")))

(define-public crate-ekore-0.0.2 (crate (name "ekore") (vers "0.0.2") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1h7aklikp94v50n6d2y5lrm5q1c0kkm0i03gls5fs053pcawrk56") (rust-version "1.60.0")))

(define-public crate-ekore-0.0.5 (crate (name "ekore") (vers "0.0.5") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1w75d2k40x91gf770gnn3xvsbzgcikl9q01kygb0a9ny2ljazp2m") (rust-version "1.60.0")))

(define-public crate-ekore-0.1 (crate (name "ekore") (vers "0.1.1-alpha.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1yml825pzf4c694y7cp59zq0cfmsrqvfhmg6ql22l780c58p7k77") (rust-version "1.60.0")))

(define-public crate-ekore-0.1 (crate (name "ekore") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "18h9vl5hpjcp4aq5cda1aplqlxz27ahyhkzi5kw3fkjn5hxykvq5") (rust-version "1.60.0")))

(define-public crate-ekore-0.1 (crate (name "ekore") (vers "0.1.1-alpha.2") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1s8mcanaxcnvhr38a04vkpl6qy9zdyv625ivvbz9vf9mas10kgcl") (rust-version "1.60.0")))

(define-public crate-ekore-0.1 (crate (name "ekore") (vers "0.1.1-alpha.3") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0ig5kxy7gd1sl58sbza14lqwwkr919dwmzniimchz4yixnc2d304") (rust-version "1.60.0")))

(define-public crate-ekore-0.1 (crate (name "ekore") (vers "0.1.1-alpha.4") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "19h4lk0z7jkqc8vlbvw5j1afnrdq5fnv4k73d0qdk633pycszqnz") (rust-version "1.60.0")))

(define-public crate-ekore-0.1 (crate (name "ekore") (vers "0.1.1-alpha.5") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "17hl3ihhn8w6nmhcpyinxxvdnni9h3rixfkcsypbqzk9pzdvikcw") (rust-version "1.60.0")))

(define-public crate-ekore-0.14 (crate (name "ekore") (vers "0.14.4-alpha.1") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1kva5hj8cz1q0q69xk9h80wpnby9hdxw9ma0gxi730vz4jcc1437") (rust-version "1.60.0")))

