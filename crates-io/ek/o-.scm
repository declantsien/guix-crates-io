(define-module (crates-io ek o-) #:use-module (crates-io))

(define-public crate-eko-gc-0.0.1 (crate (name "eko-gc") (vers "0.0.1") (hash "00cmh86i48bsxnmfz4rlgl5cqi2v50qp771zshnv2fx5ihsqz6bz")))

(define-public crate-eko-gc-0.0.2 (crate (name "eko-gc") (vers "0.0.2") (hash "1qi6bzkz0k72fkf4yzs1qkb77g8kqynynfj3f06hv7ych30rlqvd")))

(define-public crate-eko-gc-derive-0.0.1 (crate (name "eko-gc-derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "140m9s5khcb9svlzlh2av6dw29139fga1sp0gq3vd42v32ls8d0x")))

(define-public crate-eko-gc-derive-0.0.2 (crate (name "eko-gc-derive") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "08vhfj8n4gjli57pp36x56cy39hqkmmca5b2vkddq250j08v3wh3")))

