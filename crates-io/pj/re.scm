(define-module (crates-io pj re) #:use-module (crates-io))

(define-public crate-pjregtest-0.1 (crate (name "pjregtest") (vers "0.1.0") (deps (list (crate-dep (name "bitcoincore-rpc") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0kn92h5gpskr0nx71scs1pqi65jwl680b765pgrinqpb93x8vy4h")))

(define-public crate-pjregtest-0.1 (crate (name "pjregtest") (vers "0.1.1") (deps (list (crate-dep (name "bitcoincore-rpc") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "194sdhxwb0l93d1cc75rvcak01p9xfg74pi3f8w030as3a0bamk0")))

