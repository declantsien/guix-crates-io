(define-module (crates-io pj li) #:use-module (crates-io))

(define-public crate-pjlink-0.1 (crate (name "pjlink") (vers "0.1.0") (deps (list (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1q6schldc7i6k3i2w4r2if569pqsag1a934kmrqw3syk8svb551c")))

(define-public crate-pjlink-0.1 (crate (name "pjlink") (vers "0.1.1") (deps (list (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0s51cni4n729bzlp5x2l2izkxll75cnnp96ssyfb9x650mn09ifs")))

(define-public crate-pjlink-0.2 (crate (name "pjlink") (vers "0.2.0") (deps (list (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1vrclfxz2ng1nr30zqq37w9vjcx0prcjpws4xvwmin63jhlxa2ml")))

