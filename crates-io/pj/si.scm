(define-module (crates-io pj si) #:use-module (crates-io))

(define-public crate-pjsip-sys-0.0.1 (crate (name "pjsip-sys") (vers "0.0.1-alpha.0") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "1pfh24c7h5jwhnvl00s2zxysffvirnd7b9sf7w349bvsjzkpp8gh") (yanked #t)))

