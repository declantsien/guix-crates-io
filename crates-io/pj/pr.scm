(define-module (crates-io pj pr) #:use-module (crates-io))

(define-public crate-pjproject-sys-1 (crate (name "pjproject-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "169y96jhpvz2cjrmwc6d3vya65dnpzfha8609nz6ghpay5rbwli7")))

