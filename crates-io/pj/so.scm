(define-module (crates-io pj so) #:use-module (crates-io))

(define-public crate-pjson-0.1 (crate (name "pjson") (vers "0.1.0") (hash "0a88r7sjhhrpz2jqmdkvn8mjh20zafbh56976kbrhgmarmi2yrbz")))

(define-public crate-pjson-0.1 (crate (name "pjson") (vers "0.1.1") (hash "0z1nhc4z99hxmiyzdh1rmlk8c5w11r7677y9nwrn3r26ig88cdjb")))

(define-public crate-pjson-0.1 (crate (name "pjson") (vers "0.1.2") (hash "11i9jgcr5i71gc3fh2l0f31mzmgbr4i1jr383nii2yapq6j50ydp")))

(define-public crate-pjson-0.2 (crate (name "pjson") (vers "0.2.1") (hash "14jn1lgqdkahzh00mca2aqvlzkvn2xiddavjcp5697by2k10f1fk")))

(define-public crate-pjson-0.2 (crate (name "pjson") (vers "0.2.2") (hash "0sm3f30z6fqpa1ldj5zj5xaanna22babx35y4l589zndzb1cqah5")))

(define-public crate-pjson_parser-0.1 (crate (name "pjson_parser") (vers "0.1.0") (hash "0ikfm1knw1yr9y431nn0mqsnkrb63g8ipp46r2sjdqxgff63p4c4")))

