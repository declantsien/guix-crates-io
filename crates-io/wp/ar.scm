(define-module (crates-io wp ar) #:use-module (crates-io))

(define-public crate-wparse-0.2 (crate (name "wparse") (vers "0.2.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ycdgdqc8bsq8j530fssbkzhi5444mbsip3hf9a56b5jid8rc393")))

(define-public crate-wparse-0.2 (crate (name "wparse") (vers "0.2.1") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zlwb62i9587pglq7r70m5ad5sb2rdc87rri5ai5jjl574lx1hg0")))

