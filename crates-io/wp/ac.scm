(define-module (crates-io wp ac) #:use-module (crates-io))

(define-public crate-wpactrl-0.1 (crate (name "wpactrl") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1c3xzdnwp741l24zsww4fa317g0mzny1dv8gpx4ppz5361h11imj")))

(define-public crate-wpactrl-0.1 (crate (name "wpactrl") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cfj5nsadqylkh5kni90ij1yxhnha2wnmldrcx5hj4yisbn2ss7c")))

(define-public crate-wpactrl-0.2 (crate (name "wpactrl") (vers "0.2.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m8hh3fzi7bdal71f2xh14jwwmxkx5l5jx141ybai2kwswf9ihsd")))

(define-public crate-wpactrl-0.2 (crate (name "wpactrl") (vers "0.2.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f3rj2c66ql24zqix5rs1pg4sm8ml1dc2anr6vj7mnc3palaz66p")))

(define-public crate-wpactrl-0.3 (crate (name "wpactrl") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0msvfx3m5vrgx2xp5hkcvi1cydng5nb3cpjm9z3n696fn8x9xm8q")))

(define-public crate-wpactrl-0.3 (crate (name "wpactrl") (vers "0.3.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "04swcf3afgrhbj7pcxkkcyfqc8a8rf2a59x3kjm78yfnhh5ar1zl")))

(define-public crate-wpactrl-0.4 (crate (name "wpactrl") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.101") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (kind 0)))) (hash "00q36izr8qhvbbcdrbvpdgl664jnkvvpi11r591r2w2vl0vhsz4m")))

(define-public crate-wpactrl-0.5 (crate (name "wpactrl") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.122") (kind 0)) (crate-dep (name "log") (req "^0.4.16") (kind 0)) (crate-dep (name "serial_test") (req "^0.6.0") (kind 2)))) (hash "0777zch7blq0sy2xs2k3bcf3sr0j37wma402l2dajw5wzqk74x8b")))

(define-public crate-wpactrl-0.5 (crate (name "wpactrl") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2.122") (kind 0)) (crate-dep (name "log") (req "^0.4.16") (kind 0)) (crate-dep (name "serial_test") (req "^0.6.0") (kind 2)))) (hash "0a70myr3r2yvyjvv62gvpgqn631yfjpw34v2drss2x2kjplkzvcg")))

