(define-module (crates-io wp f-) #:use-module (crates-io))

(define-public crate-wpf-gpu-raster-0.1 (crate (name "wpf-gpu-raster") (vers "0.1.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.17.2") (default-features #t) (kind 2)) (crate-dep (name "typed-arena-nomut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "usvg") (req "^0.4") (default-features #t) (kind 2)))) (hash "0003my9f52hif1d2kfgr5896plqd8h390vadjb5vqiqny4jaxb8n") (features (quote (("default" "c_bindings") ("c_bindings"))))))

