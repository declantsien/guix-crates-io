(define-module (crates-io wp a-) #:use-module (crates-io))

(define-public crate-wpa-ctrl-0.1 (crate (name "wpa-ctrl") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "0ipq4rwk8jd8ix29dq52amfp0x41cymnric7jk7g1i09nscfazh9")))

(define-public crate-wpa-ctrl-0.1 (crate (name "wpa-ctrl") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "1kjf96dln1cranakj6w4zwsmf8qndi0z6b3rr665y39avjc8n35k") (yanked #t)))

(define-public crate-wpa-ctrl-0.1 (crate (name "wpa-ctrl") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "0rszwhz17fgxx6glghcjbfp7g26bjv2s77v9z4d10n8f6qv9qqzw")))

(define-public crate-wpa-ctrl-0.1 (crate (name "wpa-ctrl") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "1r6flp8mdgj1sqxg944vl88zbn3yvdjxmq6zaw1yf1zq3gr29hcr")))

(define-public crate-wpa-ctrl-0.2 (crate (name "wpa-ctrl") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "06wjyb9vpyzcb9icqgzrr7mzay2w1fi6i71qlx24l4yqdsns8b0n")))

(define-public crate-wpa-ctrl-0.2 (crate (name "wpa-ctrl") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "str-buf") (req "^3") (default-features #t) (kind 0)))) (hash "1czjchr8fkdk86w7v0h9baf9vyzn4iyndq57l6g4ljz24y2agg1q")))

(define-public crate-wpa-psk-0.1 (crate (name "wpa-psk") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-rc.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.0") (kind 0)) (crate-dep (name "pbkdf2") (req "^0.10.0") (kind 0)) (crate-dep (name "sha-1") (req "^0.10.0") (kind 0)))) (hash "0d6661f39igk852b0w4kldiqn3r8m1pnx00c62676vmkyzqxq423")))

(define-public crate-wpa-psk-0.1 (crate (name "wpa-psk") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12") (kind 0)) (crate-dep (name "pbkdf2") (req "^0.10") (kind 0)) (crate-dep (name "sha-1") (req "^0.10") (kind 0)))) (hash "0v062wbxs6v5hf5pfn5ic2ha1dcv5y87lghn60ylgbscmcx7v7d0")))

(define-public crate-wpa-psk-0.1 (crate (name "wpa-psk") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12") (kind 0)) (crate-dep (name "pbkdf2") (req ">=0.10, <0.12") (kind 0)) (crate-dep (name "sha-1") (req "^0.10") (kind 0)))) (hash "117lygaksmggp3bcd3kdhpdiy49pc15dgfkdmnmx9fjmn8a97bl9")))

(define-public crate-wpa-psk-0.1 (crate (name "wpa-psk") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12") (kind 0)) (crate-dep (name "pbkdf2") (req ">=0.10, <0.12") (kind 0)) (crate-dep (name "sha-1") (req "^0.10") (kind 0)))) (hash "0b17k9b6gra5ycg0wxhkk1z6amap2v3a6pwwh34gzmv23asp631b")))

(define-public crate-wpa-psk-0.1 (crate (name "wpa-psk") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11.0") (kind 0)) (crate-dep (name "sha-1") (req "^0.10.0") (kind 0)))) (hash "1xyn22qyxl1lrx1jnjan5ppqxmjfzvvwrrbvhzh5pf3dr7q45l8s")))

(define-public crate-wpa-psk-0.1 (crate (name "wpa-psk") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11.0") (kind 0)) (crate-dep (name "sha-1") (req "^0.10.0") (kind 0)))) (hash "053cad6qdjxzb6s1aw928jwki9xlp651a5qragvsfxxzm42j523d")))

(define-public crate-wpa-psk-0.2 (crate (name "wpa-psk") (vers "0.2.0") (deps (list (crate-dep (name "hmac") (req "^0.12.1") (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11.0") (kind 0)) (crate-dep (name "sha-1") (req "^0.10.0") (kind 0)))) (hash "1i0hpvrfrbrbn9gdgrkifpp29b1yl6nppvk55w3bqjkkdh9ijs2y")))

(define-public crate-wpa-psk-0.2 (crate (name "wpa-psk") (vers "0.2.1") (deps (list (crate-dep (name "hmac") (req "^0.12.1") (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11.0") (kind 0)) (crate-dep (name "sha1") (req "^0.10.5") (kind 0)))) (hash "1hrpk8265di9x3rrs0r4nsfl1y6lmcgc5vd3yz48ha0fp384793l")))

(define-public crate-wpa-psk-0.2 (crate (name "wpa-psk") (vers "0.2.2") (deps (list (crate-dep (name "pbkdf2") (req "^0.12.1") (features (quote ("hmac"))) (kind 0)) (crate-dep (name "sha1") (req "^0.10.5") (kind 0)))) (hash "1597r3hifkm23jk3xi5kqjzmr8wmk22lq7fhql6mksc850k4xg4b")))

(define-public crate-wpa-psk-0.2 (crate (name "wpa-psk") (vers "0.2.3") (deps (list (crate-dep (name "pbkdf2") (req "^0.12.2") (features (quote ("hmac"))) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (kind 0)))) (hash "0vb02wxml1v47kpbnzi8npli39ld5dni57nbxdqbqjavnxcf74y9")))

(define-public crate-wpa-psk-cli-0.1 (crate (name "wpa-psk-cli") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wpa-psk") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1si0zng9n57dv5c48w3y8hhjn0x70dvsmjmr8xd1cbjfz72hqs0g")))

(define-public crate-wpa-psk-cli-0.1 (crate (name "wpa-psk-cli") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0.8") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "wpa-psk") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09y4g2ras93py5igbiyg0jill6njh1yz7xkk869fkh6b3b66gk2h")))

(define-public crate-wpa-psk-cli-0.1 (crate (name "wpa-psk-cli") (vers "0.1.2") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.8") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "wpa-psk") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1bpqppvlagm03f7x6rzdgi8fjsrag15bx2mxg989fv0qgwsyz5l3")))

(define-public crate-wpa-psk-cli-0.1 (crate (name "wpa-psk-cli") (vers "0.1.3") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "pledge") (req "^0.4.2") (default-features #t) (target "cfg(target_os = \"openbsd\")") (kind 0)) (crate-dep (name "wpa-psk") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1ikjykybx3swri9yy5nv58vrhnl6054zf4wghpcrnkrz2cssm7jg")))

(define-public crate-wpa-psk-cli-0.1 (crate (name "wpa-psk-cli") (vers "0.1.4") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pledge") (req "^0.4.2") (default-features #t) (target "cfg(target_os = \"openbsd\")") (kind 0)) (crate-dep (name "wpa-psk") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1y0ch0b2z1dh6r2an1bgjpg47876grakkb0f7143q81d3zrygrds")))

(define-public crate-wpa-psk-cli-0.1 (crate (name "wpa-psk-cli") (vers "0.1.5") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pledge") (req "^0.4.2") (default-features #t) (target "cfg(target_os = \"openbsd\")") (kind 0)) (crate-dep (name "wpa-psk") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0imk785jak2zsfv8q85gcg0zyll63g789adm5z65dgh9wv1nbz8r")))

