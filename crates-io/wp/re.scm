(define-module (crates-io wp re) #:use-module (crates-io))

(define-public crate-wpress-oxide-1 (crate (name "wpress-oxide") (vers "1.1.0") (deps (list (crate-dep (name "clean-path") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0dpri5i9gb9i8v2wlczmpbjimjagqi40qg0ywi8l63bp2sl5irnk")))

(define-public crate-wpress-oxide-1 (crate (name "wpress-oxide") (vers "1.1.1") (deps (list (crate-dep (name "clean-path") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "09im14kg6gchpac8w5qri3m8gbh94xjly8q8hlq772jzavqiw36b")))

(define-public crate-wpress-oxide-2 (crate (name "wpress-oxide") (vers "2.0.0") (deps (list (crate-dep (name "clean-path") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0g4i9x9gifwh0kpahqmkh5w51ccm95m3r4ph3r3hdzvm4g2r15wg")))

(define-public crate-wpress-oxide-2 (crate (name "wpress-oxide") (vers "2.1.0") (deps (list (crate-dep (name "clean-path") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0ll4ri1ncvcndb59gfbd1j8z3gv70l7krg5j7dppynaxxlcnrhf5")))

(define-public crate-wpress-oxide-3 (crate (name "wpress-oxide") (vers "3.2.0") (deps (list (crate-dep (name "clean-path") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "1dsd22q8sxd3n7hh0qhj2p1a2jq8f129lzcp364mvi2mr0d0nc85")))

