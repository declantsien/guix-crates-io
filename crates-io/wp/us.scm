(define-module (crates-io wp us) #:use-module (crates-io))

(define-public crate-wpush-0.1 (crate (name "wpush") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.1") (default-features #t) (kind 0)))) (hash "0gj3dqwpmp1n2201chwxrwqgzzip4prikk706kq1v1qswmvzfj59")))

(define-public crate-wpush-0.1 (crate (name "wpush") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "tinytemplate") (req "^1.1") (default-features #t) (kind 0)))) (hash "1c421x25bgcakl90b1rs8hv1m2kr3b6fbhb6hny4lrb0xbcihprj")))

