(define-module (crates-io wp t-) #:use-module (crates-io))

(define-public crate-wpt-interop-0.1 (crate (name "wpt-interop") (vers "0.1.0") (hash "0glsgx87nq4q9giy4ykn9jlhwrigw3ck8wilp66061ll8dq03yh2")))

(define-public crate-wpt-interop-0.2 (crate (name "wpt-interop") (vers "0.2.0") (deps (list (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2") (default-features #t) (kind 0)))) (hash "1lfb9r5imnh7qsrmb3i21w5dr81i9c61f0jy6f1k0qaspp3fh64b")))

