(define-module (crates-io wp sl) #:use-module (crates-io))

(define-public crate-wpslugify-0.1 (crate (name "wpslugify") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "1d1dzrnaxm7s6vh5dzbx19r25n1xdvwhwk2s8ic9n8l97vym7v7x")))

