(define-module (crates-io wp il) #:use-module (crates-io))

(define-public crate-wpilib-0.1 (crate (name "wpilib") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.36.0") (default-features #t) (kind 1)))) (hash "0xyvss1na1lz2qaczk1kl79p1lbxbjd35jd4zcbfa3azgvbp7q74")))

(define-public crate-wpilib-0.0.0 (crate (name "wpilib") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.37.4") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1na8imzb6v7zljwzkvfhjwbfnm5sa9m2qv9w6x1k3visnrz70s79") (features (quote (("dev")))) (yanked #t)))

(define-public crate-wpilib-0.2 (crate (name "wpilib") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.37.4") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1xgm1y1lq8vpd17bdrbsv1czgcc615zw7sqw31jjlpxkkhnawvzv") (features (quote (("dev"))))))

(define-public crate-wpilib-0.2 (crate (name "wpilib") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.37.4") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0b4gyg0r3srg4nr8bx6f22s13b2f6zhllazafr5xkz0vps1ihzs9") (features (quote (("dev"))))))

(define-public crate-wpilib-0.2 (crate (name "wpilib") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.37.4") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "01wl7zgklhgxvj9y30shclvqkz8b3d5cc3dy9j91wc6bz9racf3d") (features (quote (("dev"))))))

(define-public crate-wpilib-0.2 (crate (name "wpilib") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.37.4") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wpilib-sys") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1hy7dlpi57rw2l481rs0s278wsc6a6fwkbs93ym39j84phg3yyh7") (features (quote (("dev"))))))

(define-public crate-wpilib-0.3 (crate (name "wpilib") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wpilib-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "030rmzf33f5y2m40y5k1jy5kfwbgvfcc53svm0mdrwjwjlmxxaqf") (features (quote (("dev"))))))

(define-public crate-wpilib-0.3 (crate (name "wpilib") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wpilib-sys") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0hi9qwhmhiv6rlybkpfs3mndhzxbd3n7sm2wcjxk27wrsrk4gi75") (features (quote (("dev"))))))

(define-public crate-wpilib-0.4 (crate (name "wpilib") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "wpilib-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "02q7d2iy53b11cm63wiy5xhc4qlkzxa4ranfqf64sqnjhym8mp8j") (features (quote (("dev"))))))

(define-public crate-wpilib-hal-0.1 (crate (name "wpilib-hal") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)) (crate-dep (name "clippy") (req "^0.0.114") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "085nm5wgc5w8slc18y7ff2nf2xvry6nnlhkvmpawhbpdsvyfi6xh")))

(define-public crate-wpilib-sys-0.1 (crate (name "wpilib-sys") (vers "0.1.0") (hash "178ysj21zpfqhsq9k2h54q99c49ymbrjlx6jxy5zw5b66g9wfcbp")))

(define-public crate-wpilib-sys-0.2 (crate (name "wpilib-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.37.4") (default-features #t) (kind 1)))) (hash "1lih5ag8fv57ykdi6aird12nkjyg7gbacaddplal10yhlfwxpl7d") (features (quote (("dev"))))))

(define-public crate-wpilib-sys-0.3 (crate (name "wpilib-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.43.1") (default-features #t) (kind 1)))) (hash "0c66cbi2k5mnqzwnap9hl6yyknf4crv5zy22yd58k7x1ncp5sbgb") (features (quote (("dev"))))))

(define-public crate-wpilib-sys-0.3 (crate (name "wpilib-sys") (vers "0.3.1") (hash "11gx0iw1j3p1c0p87v30n56b2zywlx9ncvlxrrakxwjkafp8ksaw") (features (quote (("dev"))))))

(define-public crate-wpilib-sys-0.4 (crate (name "wpilib-sys") (vers "0.4.0") (hash "1w7xb465wa3kwq08g0qcgs1l3bg52asryfhqbp8nh6drqma83hma") (features (quote (("dev"))))))

(define-public crate-wpilog-0.1 (crate (name "wpilog") (vers "0.1.2") (deps (list (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "frc-value") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "single_value_channel") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 2)))) (hash "0za9kjn4ydxcr0zhzanzlpy7svbs8z6fz4zkqvzcpvrxsbfd0r08") (features (quote (("default" "tracing")))) (v 2) (features2 (quote (("tracing" "dep:tracing"))))))

(define-public crate-wpilog-rs-0.1 (crate (name "wpilog-rs") (vers "0.1.0") (deps (list (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0309f7a9sfqkamzskkzlnr59n1l6hncamm2s8vnn3i2r1c6inm2h") (features (quote (("tracing"))))))

(define-public crate-wpilog-rs-0.1 (crate (name "wpilog-rs") (vers "0.1.1") (deps (list (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "single_value_channel") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1dhvrqphmvk7va2vqqxg0i8pyx3pfqdgjc6sdnab4n8k0vcp4s00") (features (quote (("tracing"))))))

(define-public crate-wpilog-rs-0.1 (crate (name "wpilog-rs") (vers "0.1.2") (deps (list (crate-dep (name "bimap") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "single_value_channel") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 2)))) (hash "18kgzk53wvjy93kf920kfxdg0bmc2zpzwdfp0yj1w0fwzqv55s3q") (features (quote (("default" "tracing")))) (v 2) (features2 (quote (("tracing" "dep:tracing"))))))

