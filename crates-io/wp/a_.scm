(define-module (crates-io wp a_) #:use-module (crates-io))

(define-public crate-wpa_passphrase-0.1 (crate (name "wpa_passphrase") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11.0") (kind 0)) (crate-dep (name "sha1") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1b16ilb7cw27szr58zx5p308j4cz9b9yl57bp7snlkb3ry6z9csx")))

(define-public crate-wpa_passphrase-0.2 (crate (name "wpa_passphrase") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.11.0") (kind 0)) (crate-dep (name "sha1") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "079b1xv69c391vv90wy9azpi0cdc05gw44fr5wnlanxg30wq367g")))

(define-public crate-wpa_passphrase-0.3 (crate (name "wpa_passphrase") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.12.2") (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1rsn5n8n956rf4qsrvpigva4hpbnqcniqim0716yhb4r0mfhx85g")))

(define-public crate-wpa_supplicant-sys-0.1 (crate (name "wpa_supplicant-sys") (vers "0.1.0") (hash "1flsqjbbkxdhyjjlqd5zam98424w1a0yn8m1brara505r382yakz") (yanked #t)))

(define-public crate-wpa_supplicant-sys-0.1 (crate (name "wpa_supplicant-sys") (vers "0.1.1") (hash "0bvnj0crslr0rfl2x1x3gayq8rs6y22gdh94hy2navxw654hjj1i") (yanked #t)))

