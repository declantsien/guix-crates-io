(define-module (crates-io gf xm) #:use-module (crates-io))

(define-public crate-gfxmath-0.0.0 (crate (name "gfxmath") (vers "0.0.0") (hash "1gmxxw13nan37gwdnqzbjqi1nwrpl2dbn053gnhf98p03l632hlc")))

(define-public crate-gfxmath-quat-0.0.0 (crate (name "gfxmath-quat") (vers "0.0.0") (hash "0pb1grvnk3z70ws3wmiad3bnmm7qmmm95fzcnk8yzrz51zi0ph8g")))

(define-public crate-gfxmath-vec2-0.0.0 (crate (name "gfxmath-vec2") (vers "0.0.0") (hash "12q0bbdlhrs5zpbw9h3h1n3rjphv3w50pvaj009d0dia3p54r1fi") (yanked #t)))

(define-public crate-gfxmath-vec2-0.1 (crate (name "gfxmath-vec2") (vers "0.1.0") (deps (list (crate-dep (name "opimps") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1zbqf2q5jhkw1i65n6a4ifmz3yqxblj1d547hv68h88wkb3d7xxd")))

(define-public crate-gfxmath-vec2-0.1 (crate (name "gfxmath-vec2") (vers "0.1.1") (deps (list (crate-dep (name "opimps") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0b2b0j8n33w2n99lmly0xkjwriy8yl07yp514lg0qbrv67vjdxmb")))

(define-public crate-gfxmath-vec2-0.1 (crate (name "gfxmath-vec2") (vers "0.1.2") (deps (list (crate-dep (name "opimps") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1kq083wzg3miy638430rj7hnfdh5aigmw05vbvzzdmvr9ljqqdpy")))

(define-public crate-gfxmath-vec2-0.1 (crate (name "gfxmath-vec2") (vers "0.1.3") (deps (list (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "15lvqlmb0pdykflv1ba0w6s460ky9h41q0629g2m0jh5gkwfl5nr")))

(define-public crate-gfxmath-vec2-0.1 (crate (name "gfxmath-vec2") (vers "0.1.4") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "khronos_api") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (default-features #t) (kind 2)))) (hash "1wx62p1xzssx670qrvw667l1rqkyxyga32xvgamg3kfcz25l92i4")))

(define-public crate-gfxmath-vec2-0.1 (crate (name "gfxmath-vec2") (vers "0.1.5") (deps (list (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "04729ypf086az7hql0hyg8iidrfqiixb0xf87adkfrqzc9i7cvzj")))

(define-public crate-gfxmath-vec2-0.1 (crate (name "gfxmath-vec2") (vers "0.1.6") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "khronos_api") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "opimps") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 2)))) (hash "0sia79nsjyqj0l0in01ak1816nj54a4yg8b9ylaz6xq5gvjk6hdx")))

(define-public crate-gfxmath-vec3-0.0.0 (crate (name "gfxmath-vec3") (vers "0.0.0") (hash "0kb7x6rzji0a1rhl79x96pw4fb2f26jvg199f2ld8j70dymmd8d7") (yanked #t)))

(define-public crate-gfxmath-vec3-0.1 (crate (name "gfxmath-vec3") (vers "0.1.0") (deps (list (crate-dep (name "opimps") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "13fjhkj3mipm54f15sh583gwhhdlizv48syzmi951hb2h8qwk1x4")))

(define-public crate-gfxmath-vec3-0.1 (crate (name "gfxmath-vec3") (vers "0.1.1") (deps (list (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1vim4pbnl638ha9jgc3v68kza1za698l1v6d6sck2264m891v8hm")))

(define-public crate-gfxmath-vec3-0.1 (crate (name "gfxmath-vec3") (vers "0.1.2") (deps (list (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1prqr549cm37igyd4967mbwg0195ndpkkl4dz3yhx3q7frb31v70")))

(define-public crate-gfxmath-vec3-0.1 (crate (name "gfxmath-vec3") (vers "0.1.3") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "khronos_api") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (default-features #t) (kind 2)))) (hash "0kfrzb5hjs0psd9ci19g7m8alsdj96js90f3q7i56jz6icpn4myh")))

(define-public crate-gfxmath-vec3-0.1 (crate (name "gfxmath-vec3") (vers "0.1.4") (deps (list (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "14gybr711i1m1ys78y858qsk3lr9n6cx5qj4vr7b2pb10xiibs9p")))

(define-public crate-gfxmath-vec4-0.0.0 (crate (name "gfxmath-vec4") (vers "0.0.0") (hash "15133bgna9sykmxi5n4sw620zh89c417fdh6b86s1a5h93221cjb") (yanked #t)))

(define-public crate-gfxmath-vec4-0.1 (crate (name "gfxmath-vec4") (vers "0.1.0") (deps (list (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0r0pqqzcbcr6xhr5f6p7arhsf57fnisvdl6ig7wycpmqlly021kx")))

(define-public crate-gfxmath-vec4-0.1 (crate (name "gfxmath-vec4") (vers "0.1.1") (deps (list (crate-dep (name "opimps") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0r3vr4ccc0hzngqq0d1isd1bnf1ain22vby607mx1jc4my1w83wp")))

