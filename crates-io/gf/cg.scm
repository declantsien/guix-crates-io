(define-module (crates-io gf cg) #:use-module (crates-io))

(define-public crate-gfcgi-0.3 (crate (name "gfcgi") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1nygh8mjrxck8m923a93k56cj3izkydpcdb9jz93sgf0ka16madg")))

(define-public crate-gfcgi-0.3 (crate (name "gfcgi") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1gi7d4w2j9qka1yx5lws7j90gf907gx9y0bkcpah83an6m78wb32")))

(define-public crate-gfcgi-0.4 (crate (name "gfcgi") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0djjqrg7a5jkn08lighiji86j46aj8vkw8jsnkjgp75gmrrxcn9g")))

(define-public crate-gfcgi-0.4 (crate (name "gfcgi") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1g0rinxvxgs26j608qp9d4a4sx2qnmgb6l2dgvi0kna9rpkbkzbh") (features (quote (("spawn"))))))

(define-public crate-gfcgi-0.4 (crate (name "gfcgi") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "11alp9wjj8fzrl194kpkr7svqv0a0ksjnvq2z4gmxhwk4wh2154m") (features (quote (("spawn"))))))

(define-public crate-gfcgi-0.4 (crate (name "gfcgi") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1g6dxpyzbpsdr5jc2akg77f1gpqjclwf8l8r68c7q7kc3j0sa4ld") (features (quote (("spawn"))))))

