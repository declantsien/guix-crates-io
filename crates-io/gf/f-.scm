(define-module (crates-io gf f-) #:use-module (crates-io))

(define-public crate-gff-derive-0.1 (crate (name "gff-derive") (vers "0.1.0") (deps (list (crate-dep (name "gff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (default-features #t) (kind 0)))) (hash "1y62kk3vra9vhs5wdh2vfhppilfmifb04091ar6nhpxnxych3k19")))

