(define-module (crates-io gf as) #:use-module (crates-io))

(define-public crate-gfastats-0.1 (crate (name "gfastats") (vers "0.1.0") (deps (list (crate-dep (name "gfaR") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "01mnkgj9xmrv431wdi78ky12xsrazwx29nnrkrdfm8n1g27z0fkb")))

(define-public crate-gfastats-0.1 (crate (name "gfastats") (vers "0.1.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "gfaR") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0g5jhip28fyfvsj56jpkg23679jihj8vyb3a54ysw7p1ghm7808n")))

(define-public crate-gfastats-0.1 (crate (name "gfastats") (vers "0.1.2") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "gfaR") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fn09170q3n404kakx5lhncy050fwd8x2jxh2hpafbykd5fcknp1")))

(define-public crate-gfastats-0.1 (crate (name "gfastats") (vers "0.1.3") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "gfaR") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "14zc9ndybwdv58pnlw1iwy9jvsr28ysrm97w3i4psxz5p6k4k505")))

