(define-module (crates-io gf ld) #:use-module (crates-io))

(define-public crate-gfld-1 (crate (name "gfld") (vers "1.0.0") (deps (list (crate-dep (name "git2") (req "^0.13") (kind 0)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "walkdir") (req "^2") (kind 0)))) (hash "1970dvdn6pmblfgvpfnmsd3s9rnbb0k3sj7l35p4blj5zp8655hj") (yanked #t)))

(define-public crate-gfld-1 (crate (name "gfld") (vers "1.0.1") (deps (list (crate-dep (name "git2") (req "^0.13") (kind 0)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "walkdir") (req "^2") (kind 0)))) (hash "00lcv5zgrxs6pj1hck3rcq8b221y1gpqbc51bpd4cy6hps5abhbm") (yanked #t)))

(define-public crate-gfld-1 (crate (name "gfld") (vers "1.0.2") (deps (list (crate-dep (name "git2") (req "^0.13") (kind 0)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "walkdir") (req "^2") (kind 0)))) (hash "0dif6v475gk4wisxci7cjvdhs4i9jvhxqmwrbl86h8jlv5l8khhq") (yanked #t)))

(define-public crate-gfld-1 (crate (name "gfld") (vers "1.0.3") (deps (list (crate-dep (name "git2") (req "^0") (kind 0)) (crate-dep (name "log") (req "^0") (kind 0)) (crate-dep (name "walkdir") (req "^2") (kind 0)))) (hash "1hgvc00654hx5p4k75gkh7km5cs60vvwarxgiw4f2kdwg8sbk4d4") (yanked #t)))

(define-public crate-gfld-1 (crate (name "gfld") (vers "1.0.4") (deps (list (crate-dep (name "git2") (req "^0") (kind 0)) (crate-dep (name "log") (req "^0") (kind 0)) (crate-dep (name "walkdir") (req "^2") (kind 0)))) (hash "12mndwvr9cyhw4bxa03n613ffky95z462pn7afhzhpmg03k0f8dq") (yanked #t)))

