(define-module (crates-io gf a-) #:use-module (crates-io))

(define-public crate-gfa-reader-0.1 (crate (name "gfa-reader") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0mri9bbv7hv84w6fn3h5xn7b2vm2rfbwwqxvrvinj5y73cmbqy6s")))

