(define-module (crates-io gf lu) #:use-module (crates-io))

(define-public crate-gflux-0.1 (crate (name "gflux") (vers "0.1.0") (deps (list (crate-dep (name "glib") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "gtk") (req "^0.6") (default-features #t) (kind 2) (package "gtk4")))) (hash "1m7b0z3g7s7z45zjfkdkwjnkqi60m22jz8pm3g8gsmgcdhnh1f2g") (yanked #t)))

(define-public crate-gflux-0.1 (crate (name "gflux") (vers "0.1.1") (deps (list (crate-dep (name "glib") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "gtk") (req "^0.6") (default-features #t) (kind 2) (package "gtk4")))) (hash "1ddxi11xrmxkbwskrmpdal6d9qh8ppvbq51adxldx7m9vfqkfdzx")))

(define-public crate-gflux-0.1 (crate (name "gflux") (vers "0.1.2") (deps (list (crate-dep (name "glib") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "gtk") (req "^0.6") (default-features #t) (kind 2) (package "gtk4")))) (hash "15hzh6d7hd6wjsw66lsfq0xblc8vmf4idigj4z1ghr8gd1ym7xcx")))

(define-public crate-gflux-0.1 (crate (name "gflux") (vers "0.1.3") (deps (list (crate-dep (name "glib") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "gtk") (req "^0.6") (default-features #t) (kind 2) (package "gtk4")))) (hash "10zxya83dia98l0r7j9xkqifhfj94km44bhg5rxy064c0sv6ajxr")))

