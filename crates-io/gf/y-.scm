(define-module (crates-io gf y-) #:use-module (crates-io))

(define-public crate-gfy-hash-0.1 (crate (name "gfy-hash") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^1.8") (default-features #t) (kind 0)))) (hash "0rssh2kjn6342dc0ffmgzxyhnvf22vn47kwv1k6881basf3rmkkr")))

(define-public crate-gfy-hash-0.3 (crate (name "gfy-hash") (vers "0.3.0") (deps (list (crate-dep (name "wyhash") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ppxagvxjfwn4067jqqv3j28h7vvv1xdb6jsp7d202my7bvqrknc") (features (quote (("list_builtin") ("default" "list_builtin"))))))

