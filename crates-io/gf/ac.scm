(define-module (crates-io gf ac) #:use-module (crates-io))

(define-public crate-gfacut-0.1 (crate (name "gfacut") (vers "0.1.0") (deps (list (crate-dep (name "gfaR") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0rrmfiivkgw498s9hyz3b0dwwahjgqkihjw8nb9y1k3qnzn845c8")))

(define-public crate-gfacut-0.1 (crate (name "gfacut") (vers "0.1.1") (deps (list (crate-dep (name "gfaR") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0x3i56d7bydj4lcsd370793xswg0lhqd2g3hmzqjx6kqg8nqdxgp")))

(define-public crate-gfacut-0.1 (crate (name "gfacut") (vers "0.1.3") (deps (list (crate-dep (name "gfaR") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0kqp9jwaxhipvdh2y3ds1qyih006skx7vbhcz6nsrkyrrdayb804")))

(define-public crate-gfacut-0.1 (crate (name "gfacut") (vers "0.1.4") (deps (list (crate-dep (name "gfaR") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "07jih540s38m2gajnsw4qbr20rww3vhzf2yid8x6fpr81dhyxq54")))

