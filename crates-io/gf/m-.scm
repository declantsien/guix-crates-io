(define-module (crates-io gf m-) #:use-module (crates-io))

(define-public crate-gfm-autolinks-0.1 (crate (name "gfm-autolinks") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req ">=1.0.1, <2") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "unicode_categories") (req "^0.1") (default-features #t) (kind 0)))) (hash "00v0nlils304a2n1yawg3kmc2x76gs1p38css06m6g7jdcdpzgh6")))

(define-public crate-gfm-autolinks-0.2 (crate (name "gfm-autolinks") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req ">=1.0.1, <2") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "unicode_categories") (req "^0.1") (default-features #t) (kind 0)))) (hash "0maxadskmzc75m4gbhl5i4r0nwx516ry3sac98rg4a6yxmy40ng7")))

