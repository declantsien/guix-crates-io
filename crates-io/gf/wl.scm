(define-module (crates-io gf wl) #:use-module (crates-io))

(define-public crate-gfwlist-domains-0.1 (crate (name "gfwlist-domains") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "publicsuffix") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)))) (hash "02ml707pn0cwv4az0sb76rslgi666yr23wwaz1fn9rvgw5y0v2lh")))

(define-public crate-gfwlist-domains-0.1 (crate (name "gfwlist-domains") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "publicsuffix") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)))) (hash "0xr8bzmm386bhyzbnm1mh8irra7izxyxajrjc2ym6lphvdji3q70")))

(define-public crate-gfwlist-domains-0.1 (crate (name "gfwlist-domains") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "publicsuffix") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)))) (hash "0bjdl6x6wb449rwhfgsc0wpwmikyi8harqlbmwwqcxmbs7nrkg1y")))

