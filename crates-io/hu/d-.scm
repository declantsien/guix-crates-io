(define-module (crates-io hu d-) #:use-module (crates-io))

(define-public crate-hud-slice-by-8-1 (crate (name "hud-slice-by-8") (vers "1.0.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1gi99qc6h71kb8mm0qdybgnp1k905wqjvvizswfy125g0fbm7y48") (yanked #t)))

(define-public crate-hud-slice-by-8-1 (crate (name "hud-slice-by-8") (vers "1.0.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0bkyx85mkmyc62w9avz68h7w6rsz3941yp2b2rdvy0svk53lgs8g")))

(define-public crate-hud-slice-by-8-1 (crate (name "hud-slice-by-8") (vers "1.0.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0m5f8apbjgxlrrjjgd60hm50k1qpg6xm9d52p8p216zz3bwy4zc1")))

(define-public crate-hud-slice-by-8-1 (crate (name "hud-slice-by-8") (vers "1.0.8") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0w1d2vcv6zbjh0h2z2vyvcrq1asshsqwl4xidjm39f0p1ia4938m")))

