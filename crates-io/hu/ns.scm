(define-module (crates-io hu ns) #:use-module (crates-io))

(define-public crate-hunspell-0.1 (crate (name "hunspell") (vers "0.1.0") (hash "1pj68n5wgyzd2j93g05hjfqkq4mpb9r6kc8fcpab0vd6669hm84z")))

(define-public crate-hunspell-0.1 (crate (name "hunspell") (vers "0.1.1") (hash "0h92svciilalj4lssn8dkm0njlym6f8nh8r76zxnjdvh7n4lwd9a")))

(define-public crate-hunspell-0.1 (crate (name "hunspell") (vers "0.1.2") (hash "1cg06k6ccdlk0vp270ir7slwcrj5npgv89v04mf1mm5d34cli8cj")))

(define-public crate-hunspell-rs-0.1 (crate (name "hunspell-rs") (vers "0.1.3") (deps (list (crate-dep (name "hunspell-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1kz222kslibk0r1nlxq2hspb960dg5zwnm3hbi0p67gc3hg9k829")))

(define-public crate-hunspell-rs-0.1 (crate (name "hunspell-rs") (vers "0.1.4") (deps (list (crate-dep (name "hunspell-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "16kiack7alfvhgl88rc539dj8pnfvpdd6rvxghmac46nzfbzy3yx")))

(define-public crate-hunspell-rs-0.1 (crate (name "hunspell-rs") (vers "0.1.5") (deps (list (crate-dep (name "hunspell-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1nbh2s8lk12c7jjmylibj14f4vbiavbrnswkmzdj7lylam056lw6")))

(define-public crate-hunspell-rs-0.2 (crate (name "hunspell-rs") (vers "0.2.0") (deps (list (crate-dep (name "hunspell-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ixzjimrc315z48i9zpfvzmlbikf75xg7vg953flsgsswyy3y26y")))

(define-public crate-hunspell-rs-0.3 (crate (name "hunspell-rs") (vers "0.3.0") (deps (list (crate-dep (name "hunspell-sys") (req "^0.2.1") (kind 0)))) (hash "0f08j4zipz2g98p4yzf9w26xfan1msdl2ilq8q9fv3rhqqpx293c") (features (quote (("default" "bundled") ("bundled" "hunspell-sys/bundled"))))))

(define-public crate-hunspell-rs-0.3 (crate (name "hunspell-rs") (vers "0.3.1") (deps (list (crate-dep (name "hunspell-sys") (req "^0.2.1") (kind 0)))) (hash "0i3dc9m52plc6q6csmjsrkq9r3k9vnbdm631kdaq00si1a2vlh8a") (features (quote (("default" "bundled") ("bundled" "hunspell-sys/bundled"))))))

(define-public crate-hunspell-rs-0.3 (crate (name "hunspell-rs") (vers "0.3.2") (deps (list (crate-dep (name "hunspell-sys") (req "^0.3.0") (kind 0)))) (hash "0h6p6h3macmiyngcclvw1cgrsfz8222ja4036ap8ldq3v2gb4mh4") (features (quote (("default" "bundled") ("bundled" "hunspell-sys/bundled"))))))

(define-public crate-hunspell-rs-0.4 (crate (name "hunspell-rs") (vers "0.4.0") (deps (list (crate-dep (name "hunspell-sys") (req "^0.3.0") (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "09zbazhsqrri12bg9idggz7bhvwbs0fnrrcs0cszmfb0jygy4b66") (features (quote (("default" "bundled") ("bundled" "hunspell-sys/bundled"))))))

(define-public crate-hunspell-sys-0.1 (crate (name "hunspell-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "1w9rsys2jj863vkn8jlh27lk506vrlzagmk5jsnii53p5mnzhxgw")))

(define-public crate-hunspell-sys-0.1 (crate (name "hunspell-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "0mw9lpnsn8019n7gv49wx2qabiymvvix6yz2m31av525nm5qgfdh")))

(define-public crate-hunspell-sys-0.1 (crate (name "hunspell-sys") (vers "0.1.2") (deps (list (crate-dep (name "autotools") (req "^0.2.1") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "1wzl46yb00lhszrl7fyg15ks5xj84f9d2s4ahzvpvb4kvvdb9n0r")))

(define-public crate-hunspell-sys-0.1 (crate (name "hunspell-sys") (vers "0.1.3") (deps (list (crate-dep (name "autotools") (req "^0.2.1") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1784869wkkgmk2lx1bf8wynbsyn7g90bdwx84bwfkvydj8byijvh")))

(define-public crate-hunspell-sys-0.2 (crate (name "hunspell-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1fl6f5is5c4df9b9m53qahkjncjkyigjkkbqmg39j2xzkify69p6") (features (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.2 (crate (name "hunspell-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1knx937ic4wnlfcrx9dn0763705z8ydnmxs11jpyc7r1x5wdhhxq") (features (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.2 (crate (name "hunspell-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0kmlknlz3irlfyrdzv850z2jlh18spj39gk3lyaasqqvsbildlhm") (features (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.3 (crate (name "hunspell-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "17igwhlhk07fgz21dbnj78ing9gs92bj4f7hrsk7xhym478161br") (features (quote (("default") ("bundled" "cc"))))))

(define-public crate-hunspell-sys-0.3 (crate (name "hunspell-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (features (quote ("logging" "which-rustfmt"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1v3x1gicafv2jlq1vr9hf9vgsxaiinflzni0bizcnyp4ji9n0a4j") (features (quote (("static_libclang" "bindgen/static") ("static") ("default" "bindgen/runtime") ("bundled" "cc"))))))

