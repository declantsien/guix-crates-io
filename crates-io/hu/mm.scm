(define-module (crates-io hu mm) #:use-module (crates-io))

(define-public crate-hummer-0.1 (crate (name "hummer") (vers "0.1.0") (deps (list (crate-dep (name "hex-simd") (req "^0.5") (default-features #t) (kind 0)))) (hash "0yjj0yskl7838n77pf6w9jpb6l8l232qyzhm8dw2nlhpqshfbif4")))

(define-public crate-hummer-0.2 (crate (name "hummer") (vers "0.2.0") (deps (list (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 0)))) (hash "1v3n4yxlhr8izvnxmcq06pzgpw5p9zqy5s0c17i41hxf0dds8g1p")))

(define-public crate-hummingbird-0.1 (crate (name "hummingbird") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "13fbbh4wk025hy3m27izr5jx2ph8i8cf8rn3akdcnvz6h69frxab")))

(define-public crate-hummingbird-0.1 (crate (name "hummingbird") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "079ybgvcg685ml3fj67cwb5r18nfncllbhn8zsx8l57wkwaa6ljf")))

(define-public crate-hummingbird-0.1 (crate (name "hummingbird") (vers "0.1.2") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ckah7asg67wannskpqlca7n27fywhmx5wqvfn2fn5bbcpl43nnl")))

(define-public crate-hummingbird-0.1 (crate (name "hummingbird") (vers "0.1.3") (deps (list (crate-dep (name "hyper") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "0i3qcps6pbvhlj9f5zv3jbbcix8fzwrxmxkizaky5xl22v00qgym")))

