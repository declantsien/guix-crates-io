(define-module (crates-io hu i-) #:use-module (crates-io))

(define-public crate-hui-derive-0.1 (crate (name "hui-derive") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1if14y025av5j7inlr5pckjk0jf78j11x0l7wxxnhy3m9aaid1s4") (rust-version "1.75")))

(define-public crate-hui-glium-0.0.1 (crate (name "hui-glium") (vers "0.0.1") (deps (list (crate-dep (name "glam") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "hui") (req "=0.0.1") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "14yp567d0805nngmlmsmwwlqabch8klxa8jhqqx8fll8qhydnwy2") (yanked #t)))

(define-public crate-hui-glium-0.0.2 (crate (name "hui-glium") (vers "0.0.2") (deps (list (crate-dep (name "glam") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "hui") (req "^0.0") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1bld2ga3hasqciqz2ir0ay1z4b4d857dsa7mzx93kkx5nn36vmn9") (yanked #t)))

(define-public crate-hui-glium-0.1 (crate (name "hui-glium") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "glam") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "hui") (req "^0.1.0-alpha") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0n5c6fz7h8v4k7hbnfbyfk5nd91ilq6c015zyamai1crrhayhbjf")))

(define-public crate-hui-glium-0.1 (crate (name "hui-glium") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "glam") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.34") (kind 0)) (crate-dep (name "hui") (req "=0.1.0-alpha.3") (features (quote ("builtin_font"))) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0lv4nb2gskm8b71fbxssv1gp4zgfm9d4r15x6i90v1sxvvh28aif")))

(define-public crate-hui-glium-0.1 (crate (name "hui-glium") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "glam") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.34") (kind 0)) (crate-dep (name "hui") (req "=0.1.0-alpha.4") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0j9sbrqpwgy0lk5vsdlhi6fibzq64favvspmvgq6afrc78g325sw") (rust-version "1.75")))

(define-public crate-hui-wgpu-0.0.0 (crate (name "hui-wgpu") (vers "0.0.0") (hash "1ggdraa9lrwv1z4nrf4xr8l4fgi4n06yhppibhc9k2paa8fw8071") (yanked #t)))

(define-public crate-hui-winit-0.1 (crate (name "hui-winit") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "glam") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "hui") (req "=0.1.0-alpha.4") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.29") (kind 0)))) (hash "1gv348hq1m1pfld790a5zbhnds17amsdph36a1yapam741ynnhb8")))

