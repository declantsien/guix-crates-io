(define-module (crates-io hu nd) #:use-module (crates-io))

(define-public crate-hundred-doors-0.1 (crate (name "hundred-doors") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.101.0") (default-features #t) (kind 0)))) (hash "0maj9y0wffrb49wrgy003y6c99lxsjidffp5pvlhq4vsln2j6r69")))

(define-public crate-hundred-doors-0.1 (crate (name "hundred-doors") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.101.0") (default-features #t) (kind 0)))) (hash "1yqkadg6qd3h70ssfv8zi1q2vzarjgkl80f4pxjikclapf55brz2")))

(define-public crate-hundred-doors-0.1 (crate (name "hundred-doors") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.101.0") (default-features #t) (kind 0)))) (hash "0w2mjfzf8ydg9drnzmipbb26ixywfwz60yiipk647crlny7rmzb1")))

(define-public crate-hundred-doors-0.1 (crate (name "hundred-doors") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.101.0") (default-features #t) (kind 0)))) (hash "0p8fmlizi021dl0khh3k8i71q17iam23j1m7n2p0q42n4hsk5wvn")))

