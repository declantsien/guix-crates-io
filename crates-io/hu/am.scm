(define-module (crates-io hu am) #:use-module (crates-io))

(define-public crate-huam-rust-utils-0.1 (crate (name "huam-rust-utils") (vers "0.1.0") (hash "1fz00piwxg25bjc24p7lwy1lcmv6zig9mi0rngj9nb0cmwp6zd4y") (yanked #t)))

(define-public crate-huam-rust-utils-0.1 (crate (name "huam-rust-utils") (vers "0.1.1") (hash "0piv46qrhq2nzhavcjm39vn4gbvmcc4xjc4svgdnfy09d38qfvi4")))

