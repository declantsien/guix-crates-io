(define-module (crates-io hu n-) #:use-module (crates-io))

(define-public crate-hun-error-0.1 (crate (name "hun-error") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0x077qwzzyk1xp9qfyl3kmbn5q2a61qzhd1lbsbypisqqwnfpx1w")))

(define-public crate-hun-error-0.1 (crate (name "hun-error") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0lqkgs4w0356ibd4wi1ds4ciirn72636ikrc1l6qijzrfmaa6yq9")))

(define-public crate-hun-offsetof-0.1 (crate (name "hun-offsetof") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "034yncpn4b04ff48arq4k77wmf0fxs228vpralryli3480ymlrhw")))

(define-public crate-hun-offsetof-0.1 (crate (name "hun-offsetof") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pyp34dsd12ndjngjfmzhi0scy2jwwi0c68yasqpjslcz9x8ac5a")))

