(define-module (crates-io hu bp) #:use-module (crates-io))

(define-public crate-hubpack-0.1 (crate (name "hubpack") (vers "0.1.0") (deps (list (crate-dep (name "hubpack_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (kind 2)))) (hash "135q4bx6dzyjmjj5qmzfhq7qic3sjc8i7shml97v75sdm7ifbq3h")))

(define-public crate-hubpack-0.1 (crate (name "hubpack") (vers "0.1.1") (deps (list (crate-dep (name "hubpack_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (kind 2)))) (hash "1lqmny19c98v3227hj73jwsipsa8f9zib3bz4wzdg7dla473r3xz")))

(define-public crate-hubpack-0.1 (crate (name "hubpack") (vers "0.1.2") (deps (list (crate-dep (name "hubpack_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (kind 2)))) (hash "00s3abngbfldqdfbvv94c11962884y1rkam31dggc6g5x95bi831")))

(define-public crate-hubpack_derive-0.1 (crate (name "hubpack_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1342pxnvdv4p91i04k67pdz1jknz9jjc43m4h84zajrkkf0p9g9m")))

(define-public crate-hubpack_derive-0.1 (crate (name "hubpack_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0nhjrinvml9yvswdrnbrzmbp12dmz2092c7pis0yhvpimwh874hg")))

