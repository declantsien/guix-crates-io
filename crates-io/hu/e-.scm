(define-module (crates-io hu e-) #:use-module (crates-io))

(define-public crate-hue-bridge-0.1 (crate (name "hue-bridge") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "15x01y0bn3x85b6yw7jwbxb4m91p3ign5zmfppqhjl7xqspngzk8")))

(define-public crate-hue-bridge-0.1 (crate (name "hue-bridge") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("native-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("fs"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0f28jzjnrl2r4mjabbrzii091pm57lcy1xsisasm00fxi33bf3ry")))

