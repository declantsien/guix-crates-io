(define-module (crates-io hu ff) #:use-module (crates-io))

(define-public crate-huff-1 (crate (name "huff") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "huff_coding") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1k1a6705j5and3ksdf8q4w63zpjx3pkac3vckq498k8h4lyh7y9w") (yanked #t)))

(define-public crate-huff-1 (crate (name "huff") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "huff_coding") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0ywsxldp46bjbrq82cnld2ak3qlzc9rvi1lfan6qgr1algvarm1r") (yanked #t)))

(define-public crate-huff-1 (crate (name "huff") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "huff_coding") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1ridnfd5z3qhby98aqjryyqcchn3582l0a7p068s28bx61b9x35m") (yanked #t)))

(define-public crate-huff-1 (crate (name "huff") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "huff_coding") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1hjfmv14849diwyncvmhbdxnlb4gd6snl4zc01g75alc0q46afvc")))

(define-public crate-huff-1 (crate (name "huff") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "huff_coding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10l0x0qwxhcmxv333brqs8d924wihb57af0j80b2bc9lly27qvz8") (yanked #t)))

(define-public crate-huff-1 (crate (name "huff") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "huff_coding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16ahx2wi49n9jnd4rgkdlmvbm4xv8k47kwxqvsc0vqrk99kf008p")))

(define-public crate-huff-1 (crate (name "huff") (vers "1.0.6") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "huff_coding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "066nc9y8idqrspwyihxfd9g59391mr1279b3nwk2ra2va5g0vz95")))

(define-public crate-huff-tree-tap-0.0.1 (crate (name "huff-tree-tap") (vers "0.0.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0wimg811hkv1v2n8k81pin8r4zc3jz3rpwb2kkwh7qdrdz3saiiv")))

(define-public crate-huff-tree-tap-0.0.2 (crate (name "huff-tree-tap") (vers "0.0.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "05mk2kjnd2f502a61284b0iskcxalsvhmjbak7icmv3mnsgpkifr")))

(define-public crate-huff-tree-tap-0.0.3 (crate (name "huff-tree-tap") (vers "0.0.3") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11hzsy2189bis0wsyh2072ajbwa5kk8zcc6i3wxgywjzkpv5zkj5")))

(define-public crate-huff-tree-tap-0.0.4 (crate (name "huff-tree-tap") (vers "0.0.4") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "1iffx2awdffr5mkqadlfixrsb1266jkl3zs686kr3glxcvl7qbxa")))

(define-public crate-huff0-0.1 (crate (name "huff0") (vers "0.1.0") (hash "1dvfsvs3igp2kfdnw2fdai1cnpri3z4bc7bfr2r45g1x7m0ndxhr")))

(define-public crate-huff_coding-0.0.0 (crate (name "huff_coding") (vers "0.0.0") (deps (list (crate-dep (name "bitvec") (req "^0.20.1") (default-features #t) (kind 0)))) (hash "1msrprz2w09vigkigsjdrv1vgy8467fncsjm471w8cp9119n79fs")))

(define-public crate-huff_coding-0.0.1 (crate (name "huff_coding") (vers "0.0.1") (deps (list (crate-dep (name "bitvec") (req "^0.20.1") (default-features #t) (kind 0)))) (hash "1ambn07g6jqab5a241l2psiq6waijg3f9ahgi4mmv2z3ijz4dkzy")))

(define-public crate-huff_coding-1 (crate (name "huff_coding") (vers "1.0.0") (deps (list (crate-dep (name "bitvec") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "funty") (req "=1.1.0") (default-features #t) (kind 0)))) (hash "1vc6xkcbr867x760rg8k0zd25ynvgwb8s74r8s1dd1xp30r8wdkg")))

(define-public crate-huff_lexer-0.3 (crate (name "huff_lexer") (vers "0.3.0") (deps (list (crate-dep (name "huff_utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "1mdhlwyi1zl92sczmszzbmanvpr5d1889qnmhcjhlgaxhgmpvg98")))

(define-public crate-huff_rs-0.1 (crate (name "huff_rs") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "readonly") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "throbber") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ghhmic9ph7h521pf7i9iz5xqcj07v0ijkpw0jaj4azqqqy78xfs")))

(define-public crate-huff_rs-0.1 (crate (name "huff_rs") (vers "0.1.1") (deps (list (crate-dep (name "ahash") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "readonly") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "throbber") (req "^0.1") (default-features #t) (kind 0)))) (hash "13qm0wqpry4vasykd266vx1rm4yp3dv9gr6xfkfz3fyqg8kl8gx6")))

(define-public crate-huff_rs-0.1 (crate (name "huff_rs") (vers "0.1.2") (deps (list (crate-dep (name "ahash") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "readonly") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "throbber") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kxvdf3ir7laz506bac1m6r8cb3q59n8d62sgxamk6n8khz4r4cz") (yanked #t)))

(define-public crate-huff_rs-0.1 (crate (name "huff_rs") (vers "0.1.3") (deps (list (crate-dep (name "ahash") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "readonly") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "throbber") (req "^0.1") (default-features #t) (kind 0)))) (hash "0plc4x1g81mvaciry9fqkh2lry920jad4xnqqn9qrr1d6ab89fdz")))

(define-public crate-huff_utils-0.1 (crate (name "huff_utils") (vers "0.1.0") (deps (list (crate-dep (name "codemap-diagnostic") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pathdiff") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.34") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0gg6wn0840d5vf7sach8pz6drm2gh15qy120ycwm61sajsrb604x")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.16") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "1ammv4z5lbsrib5pw1w005zw9c1y4ajdh4sfgbfywn8jak8xfdc6")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.18") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "1mljrmj91r328yb4r94rcyg81n3f3jyjkig90w9b4w202rsci77g")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.19") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "1gxch2s617k13b5fqrrizm9yxf7srsw81iwl7bwfm9kfwa79lqbp")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.20") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "0m0c93v21xnc8pismyr0gfivn0ql7faljchrk3apabzbi40f72pq")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.21") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "1l0bkqkp084897pvb5wni6qp1d8h12qrcga4n02f00888rnp24ry")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.22") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "1wbjd0an831cwvg3aq8y26fz9lrf84g66skicwx071vh9r6d1rfp")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.23") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "02ypk9rn0a3hq54ymjwifwi6c3i327d5l7d3qw89y8a34i8c6bnn")))

(define-public crate-huffcomp-0.1 (crate (name "huffcomp") (vers "0.1.24") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)))) (hash "0si7bmssa5shdx6mmi4ibfnq983ccrgdmcdqg2d29cnhapylczci")))

(define-public crate-huffman-0.0.1 (crate (name "huffman") (vers "0.0.1") (deps (list (crate-dep (name "bitstream") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zq9fm893ml8fzljzjin2hzr01d1wxyvcmp7sys47x4kh0h67qlh")))

(define-public crate-huffman-0.0.2 (crate (name "huffman") (vers "0.0.2") (deps (list (crate-dep (name "bitreader") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gg6n5dra4jqbvzc2m9ibs2nkx5jacmpwzq7vz9pgc2vsli8mc8k") (features (quote (("nightly"))))))

(define-public crate-huffman-0.0.3 (crate (name "huffman") (vers "0.0.3") (deps (list (crate-dep (name "bitreader") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mmhyizph979swi1p0fdlk3s0hxi5jxgmzn28v018ss6a1jlmqph") (features (quote (("nightly"))))))

(define-public crate-huffman-codec-0.1 (crate (name "huffman-codec") (vers "0.1.0") (hash "18g8grd1j4n7r20h5aqikm02dm4wqxwbill6al1sqa9s8hr8w6mv")))

(define-public crate-huffman-codec-0.1 (crate (name "huffman-codec") (vers "0.1.1") (hash "0x3hg8j425w2kvrj095xlp8xnmj4m30v5lqcncrgqsg1jib3xipl")))

(define-public crate-huffman-codec-0.1 (crate (name "huffman-codec") (vers "0.1.2") (hash "0lz3nax03zf4ix3z5qpydcmffkqgqym2hxnd4s83dq5cc1vrra6m")))

(define-public crate-huffman-codec-0.1 (crate (name "huffman-codec") (vers "0.1.3") (hash "0rg7yj0k81fv3s88mkwd6m7gs23v5nivvzz6jqq27aqbfly8l70m")))

(define-public crate-huffman-codec-0.1 (crate (name "huffman-codec") (vers "0.1.4") (hash "0b374rvhhgwl704kylvwd7hdiqh3njcrkdrf9dl4zcdsqkhp922i") (features (quote (("nightly-features") ("default"))))))

(define-public crate-huffman-codec-0.1 (crate (name "huffman-codec") (vers "0.1.5") (hash "1z46iwfrlsxxacq8dhva6iplyj2vwmcvz8jpd5lzfsv9ch9awzxr") (features (quote (("nightly-features") ("default"))))))

(define-public crate-huffman-codec-0.1 (crate (name "huffman-codec") (vers "0.1.6") (hash "14xlf90hd1drjw7yqjwhckrck6h71f58ms6rrwmws6ggd278i05n") (features (quote (("nightly-features") ("default"))))))

(define-public crate-huffman-coding-0.1 (crate (name "huffman-coding") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "bitstream-rs") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.0") (optional #t) (default-features #t) (kind 0)))) (hash "1kz1nijcc1n6yvy5khbk81060pjrjgc1ydqzlvxn7fzjgyski1rw") (features (quote (("default-features") ("bin" "clap"))))))

(define-public crate-huffman-coding-0.1 (crate (name "huffman-coding") (vers "0.1.1") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "bitstream-rs") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.0") (optional #t) (default-features #t) (kind 0)))) (hash "01yf50a4g8z8m350gdzaq2mqy5wsbv7n1zgdxclxq5bp01v61jxa") (features (quote (("default-features") ("bin" "clap"))))))

(define-public crate-huffman-coding-0.1 (crate (name "huffman-coding") (vers "0.1.2") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "bitstream-rs") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.0") (optional #t) (default-features #t) (kind 0)))) (hash "1kf4sdyaz0jmd40d5rv22b2xxspjqr244zdqrxxc29apv559fhxx") (features (quote (("default-features") ("bin" "clap"))))))

(define-public crate-huffman-compress-0.1 (crate (name "huffman-compress") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vn4ydn86n2scsg7g0qh44kpikwc95xyhh7m0imsa9g6fqrcmqkk")))

(define-public crate-huffman-compress-0.1 (crate (name "huffman-compress") (vers "0.1.1") (deps (list (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "15418sni551qb9174hdbgl38n4bxc678shn0vxjyvjggg5qw3km2")))

(define-public crate-huffman-compress-0.2 (crate (name "huffman-compress") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v486dvjjc2ga2wryxias1id49r5vn0prslm3w7wbw8jy34zi7nz")))

(define-public crate-huffman-compress-0.3 (crate (name "huffman-compress") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "01ckxcpp5m2kwp2mr8nxvx85h8qisyhnckj0cs14ykzyksm37d44")))

(define-public crate-huffman-compress-0.3 (crate (name "huffman-compress") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1ix4ih06h65c3k4bvnn6dvb8m2zq577z2y0c22ibywf12jcqrxkm")))

(define-public crate-huffman-compress-0.3 (crate (name "huffman-compress") (vers "0.3.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1wr5fzgk2062in3yd3ac5m9yvc2dcqnw3amx3y89dbgym0l94js1")))

(define-public crate-huffman-compress-0.4 (crate (name "huffman-compress") (vers "0.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1c30wg1xipv24l5m9xl8yn3n0m4krj1a71yn7vfmvd6ssn1jc39p")))

(define-public crate-huffman-compress-0.5 (crate (name "huffman-compress") (vers "0.5.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1h0y27ml61v5xpg7gmxam8z6d3s7dsasabpg5n0s9y3ckvf3pxs7")))

(define-public crate-huffman-compress-0.6 (crate (name "huffman-compress") (vers "0.6.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)))) (hash "18yl72psgsdz798i11r6hypjhl5vhgjsj1gc2na3m6vsqlgbvld3")))

(define-public crate-huffman-compress-0.6 (crate (name "huffman-compress") (vers "0.6.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0xvys77r8231xnv6hm4qkw65k4d1rrrdj1gdy2wr2q9mjbwhylnx")))

(define-public crate-huffman-compression-0.1 (crate (name "huffman-compression") (vers "0.1.0") (hash "0bbpq0l58iq8w6hl0jachxwl5dlp8lncph45jyfkla592xczy0w3")))

(define-public crate-huffman-compression-0.1 (crate (name "huffman-compression") (vers "0.1.1") (hash "1v320g4ak0zshmblgxb5ndvk14axai141yb0gxzppxcb9cnc2jrf")))

(define-public crate-huffman-compression-0.1 (crate (name "huffman-compression") (vers "0.1.2") (hash "1fpsrvslhijs7zx11bm3x1pr8i7ykrhyj9ll723rad04fgrzcdzv")))

(define-public crate-huffman-encoding-0.1 (crate (name "huffman-encoding") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rkvsw1nd554ymfqr4sljv7ii7lhs2w72b559aqjns4paaywffhg")))

(define-public crate-huffman-encoding-0.1 (crate (name "huffman-encoding") (vers "0.1.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xmjs6s68pnirkcxwx5gy10h5y73v288nym78lxx19k32mjmyfg0")))

(define-public crate-huffman-encoding-0.1 (crate (name "huffman-encoding") (vers "0.1.2") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "09m9ckpq9jidijdb0h3k5pwcp05vhqvqzyqdzkqhxj3y2n9xlkb8")))

(define-public crate-huffman-rust-0.1 (crate (name "huffman-rust") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "15d1iqncicmn2hiyhsq4l44kfa8366ym0hhm3h8dm89zjmwkic1a")))

(define-public crate-huffman-rust-0.1 (crate (name "huffman-rust") (vers "0.1.1-alpha") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "1afp534a2rrvy1idszx6g04ddbii5bzfy20hkgnmsc9cxxya6j7d")))

(define-public crate-huffman-rust-0.1 (crate (name "huffman-rust") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)))) (hash "1qsz3w1hivh1a1l8c31jnfs63bk2hf08dgh00agircx7a1kscs3c")))

(define-public crate-huffman_rs-0.1 (crate (name "huffman_rs") (vers "0.1.0") (hash "1g5610ad64m78ijl6v7s228v9sq5fx1idrk25apw7s24ycnw0fn2")))

(define-public crate-huffman_tree-0.1 (crate (name "huffman_tree") (vers "0.1.0") (hash "1p5ll78s315dn7l7h3p39djfjz97vb02gfb4xsd77pp7ckjl90xz")))

(define-public crate-huffmanrs-0.1 (crate (name "huffmanrs") (vers "0.1.0") (hash "0zknbkf49sp73wdgmbq48pdsi9vkzrvlmdf10dfymzhcqzbz2ryv")))

