(define-module (crates-io hu ba) #:use-module (crates-io))

(define-public crate-hubakc-0.1 (crate (name "hubakc") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "minreq") (req "^2.6.0") (features (quote ("https-native" "proxy"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1k9ajkdybymdvy9fvxcghr4b3017bixadqnyrknv7f3hw4xlik5f")))

