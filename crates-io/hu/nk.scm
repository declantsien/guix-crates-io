(define-module (crates-io hu nk) #:use-module (crates-io))

(define-public crate-hunk-0.1 (crate (name "hunk") (vers "0.1.0") (hash "0rcnc7rdfyq5vvdc09ajj8by6w49y3mbpyrbdk9jzg5rnjsjv62z")))

(define-public crate-hunktool-0.2 (crate (name "hunktool") (vers "0.2.1") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0brwsbwfxa9ph8xcxiz28sq3m3sqpx9kfvzn55lnlq3h8hvg7l08")))

(define-public crate-hunktool-0.3 (crate (name "hunktool") (vers "0.3.0") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1alrywc4gici4ys44gpd2flcm71w5kghczw74yns1p1vdhmrr9g9")))

(define-public crate-hunktool-0.4 (crate (name "hunktool") (vers "0.4.0") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0g0l5pcprn58smsawwwfpfywiaczwgc8aw5inldrq483pjv4qxqm")))

(define-public crate-hunktool-0.4 (crate (name "hunktool") (vers "0.4.1") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06c7js8czfmbrq58p2bbdk7pprjddh03zrs6lmi88k5shawiwk92")))

