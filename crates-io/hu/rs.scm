(define-module (crates-io hu rs) #:use-module (crates-io))

(define-public crate-hurst-0.1 (crate (name "hurst") (vers "0.1.0") (deps (list (crate-dep (name "linreg") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0bb4jv0b5a95ddfdq6zf5qpfkpib3m2c7sir131minsdsifj8kff")))

