(define-module (crates-io hu b7) #:use-module (crates-io))

(define-public crate-hub75-0.1 (crate (name "hub75") (vers "0.1.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1r88jhf1j0k9sza7j64lxhx59ddyqn8kyp05pdxp7mdbgp3fi2xn")))

(define-public crate-hub75-0.1 (crate (name "hub75") (vers "0.1.1") (deps (list (crate-dep (name "embedded-graphics") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0g470wxkq8rzvchsp98qjaqhar2cdbjyqpzkw2101lwrxc50ccqq")))

(define-public crate-hub75-pio-0.1 (crate (name "hub75-pio") (vers "0.1.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "pio") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pio-proc") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rp2040-hal") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0kcdg5ax2wy8dxh26j1l4h455av6mh0ayx5s8p8pbsvxq870a02c")))

(define-public crate-hub75-remastered-0.1 (crate (name "hub75-remastered") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-02") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "embedded-hal-1") (req "^1.0") (default-features #t) (kind 0) (package "embedded-hal")))) (hash "14ym0km000cqri0xh1jfpn38r5zaqc4lirsmwbazxb4zjqaphic9") (features (quote (("hal-1") ("hal-02")))) (v 2) (features2 (quote (("defmt" "dep:defmt" "embedded-graphics/defmt" "embedded-hal-1/defmt-03"))))))

