(define-module (crates-io hu rr) #:use-module (crates-io))

(define-public crate-hurrahdb-0.1 (crate (name "hurrahdb") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xnv021mx53ljm398a0wyil0m6cbnynqd9ip152g01wqpkssqax9")))

(define-public crate-hurricane-0.1 (crate (name "hurricane") (vers "0.1.0") (hash "1bcb66wlg8nds5207s202qjx24rcaqciv0119b7l728z7crqly8x")))

