(define-module (crates-io hu lk) #:use-module (crates-io))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.0") (hash "12mrklxc20y3xwgx9q53cl2prcsqxssbxaqid5h864yczc22bg14") (yanked #t)))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.1") (deps (list (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1drwiip73fsfn35gwvd8753hzll4y6q4vcpphb2afjn5ncip1h8p") (yanked #t)))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.2") (deps (list (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "10icq47mrz3z7hsc2cx7izsghdzq0anzpcc9glwcspzm899dnk21") (yanked #t)))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.3") (deps (list (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1awjm3r0a6aqmppx5p92jp3b84xbzhyw9mnjqm5mjqgj0mqcg9rj") (yanked #t)))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.4") (deps (list (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1vdgygw9nnqqc86nn97wsbyadm8dsrl031jj271smn8jm5w0mx37") (yanked #t)))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.5") (deps (list (crate-dep (name "brown") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1v6ybdfzwhid3ixp70q90nhg7miy6nhs061zggxafnlyzdm9jbw7") (yanked #t)))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.6") (deps (list (crate-dep (name "brown") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "187f3bhkmb4jyhf17hbngspz547cm967xmivin87r6ksfahfnzw3") (yanked #t)))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.7") (deps (list (crate-dep (name "brown") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1h8nvrjxrsn5mbgjawr1w9kg4nr60wjvr90lhvpnqgibblnpqxpa")))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.8") (deps (list (crate-dep (name "brown") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "fltk") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "fltk-flow") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "qndr") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0r0x1wlmj0kk5gb4lw4zkp4cd9qdwmdvn0m76xqp1bf5pa8qin1v")))

(define-public crate-hulk-0.1 (crate (name "hulk") (vers "0.1.9") (deps (list (crate-dep (name "brown") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "fltk") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "fltk-flow") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "qndr") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "08fcy5bffpsh09dxx5qv26hx1igbgfsq5gpn1aa0rqqws4aahvw1")))

(define-public crate-hulk-rs-1 (crate (name "hulk-rs") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n579krrblcfd9j814ivypvg9ybgk4hqzfa0cq9m2n487lpy4y44")))

(define-public crate-hulk-rs-1 (crate (name "hulk-rs") (vers "1.2.2") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z8pv834hfhir7bhwkhpwhdpbgk4xbhrdipx13bhxxxiv0g50cx0")))

(define-public crate-hulk-rs-1 (crate (name "hulk-rs") (vers "1.2.3") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0w3gm2fkzpx29zwhg4k0224rk1yihjaankilbafhqcmqisqlsvlx")))

