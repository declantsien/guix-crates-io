(define-module (crates-io hu nc) #:use-module (crates-io))

(define-public crate-huncomma-0.1 (crate (name "huncomma") (vers "0.1.0") (deps (list (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "021nswflvkanm4yc1aq12lgzkjd3czhzaj7ciza0pi9izvs9gi9y")))

(define-public crate-huncomma-0.2 (crate (name "huncomma") (vers "0.2.0") (deps (list (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "0ng8sbkif51lx85njahkd62hw8ib0iqv4ywhnss3zhlcaxl3rrib")))

(define-public crate-huncomma-0.2 (crate (name "huncomma") (vers "0.2.1") (deps (list (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "1nrmi0w0pwy1csqh5n3qwypw4diwnfy9dmzj4sqkfs4gw1zz3xkb")))

