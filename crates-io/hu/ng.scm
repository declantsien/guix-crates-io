(define-module (crates-io hu ng) #:use-module (crates-io))

(define-public crate-hungarian-0.1 (crate (name "hungarian") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "04pkwmyjb1kdwmba2nwwdc21fqxqymiicibdk4m7r87x12laa6df")))

(define-public crate-hungarian-1 (crate (name "hungarian") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0h4fzlnc4c8la45mhsw0cbms6534w84mglbl2psmyxrr3aw7j88j")))

(define-public crate-hungarian-1 (crate (name "hungarian") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "19vvcsis5ygyadxr2nh55nvvkfb16apb6pp4gy5b754v5akcnw44")))

(define-public crate-hungarian-1 (crate (name "hungarian") (vers "1.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^2.0") (default-features #t) (kind 2)))) (hash "0va294d5l1964rqlkzw2ljwpiv6ilj37g1qx33cj39jffbmwa9zv")))

