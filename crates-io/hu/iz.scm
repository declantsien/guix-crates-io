(define-module (crates-io hu iz) #:use-module (crates-io))

(define-public crate-huiz-0.1 (crate (name "huiz") (vers "0.1.0") (deps (list (crate-dep (name "idna") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1mgcpnq0lwnr8gw98qaxnrpp3dr8l8msrzhix50c7qxr2h0xw8gj")))

