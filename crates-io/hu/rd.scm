(define-module (crates-io hu rd) #:use-module (crates-io))

(define-public crate-hurdles-0.1 (crate (name "hurdles") (vers "0.1.0") (hash "0j4a38z5nkpys9a07wfbkg6v8g1rs75r5hgllklgicifaqp0srkl") (features (quote (("nightly"))))))

(define-public crate-hurdles-0.1 (crate (name "hurdles") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot_core") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1bvby8bfqzfwy3922f3g8pg9q8s5fdj9cr36kqhhwgviib2gswyx") (features (quote (("nightly"))))))

(define-public crate-hurdles-0.1 (crate (name "hurdles") (vers "0.1.2") (deps (list (crate-dep (name "parking_lot_core") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0kb1cdajb8wpd1wyqqkvl9ipbch895vx3bzliy4ji7kcr2j3pm63") (features (quote (("nightly"))))))

(define-public crate-hurdles-1 (crate (name "hurdles") (vers "1.0.0") (deps (list (crate-dep (name "parking_lot_core") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "12cir0rbd5dj6r3vrzlbfp3jlnkn4cna9675r9li8qbvrz9ksizc") (features (quote (("nightly"))))))

(define-public crate-hurdles-1 (crate (name "hurdles") (vers "1.0.1") (deps (list (crate-dep (name "parking_lot_core") (req "^0.4") (default-features #t) (kind 0)))) (hash "1i7rd38hpmq1majraf0zi60k5v6hxdq9qxf5ijik411104zyg9bv") (features (quote (("nightly"))))))

