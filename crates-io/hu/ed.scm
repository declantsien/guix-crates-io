(define-module (crates-io hu ed) #:use-module (crates-io))

(define-public crate-huedot-api-0.0.1 (crate (name "huedot-api") (vers "0.0.1") (hash "0dl01g29xrdk4jhyflxbd9fx4cmhbpiwkavgniyl22jjvrk17lcb")))

(define-public crate-huedump-0.1 (crate (name "huedump") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "18dica00bdfgkilw5afsr8vnw0s1xqlmavaqi0vnr2vh19zcvldw")))

(define-public crate-huedump-0.1 (crate (name "huedump") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "12f129a7p09j4jgrr2hvm8ccr59qy870wg2hwk3xs05xc25wmf1h")))

(define-public crate-huedump-0.1 (crate (name "huedump") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gsyhmlcm4z4jz3vv1hp3rdplrnj1jrjwi5xnrqra8n0i4ami6hg")))

