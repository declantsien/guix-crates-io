(define-module (crates-io hu to) #:use-module (crates-io))

(define-public crate-hutool-0.0.1 (crate (name "hutool") (vers "0.0.1") (hash "00ilf9dsrbk5pax3fmiq1kaymx68s47928aikj9qcrf926m0wx0i") (yanked #t)))

(define-public crate-hutools-0.0.1 (crate (name "hutools") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chronoutil") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "17arq92ajsmhlaxfpn0d7fvbw0yqjjiynm9xk65xncg6xcn2g85n")))

(define-public crate-hutools-0.0.2 (crate (name "hutools") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chronoutil") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "ulid") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "1dli988d5ifdwqnq19kszrcza2svwabf4vds1hzcfk09hrgy75zb")))

