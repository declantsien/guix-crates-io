(define-module (crates-io hu la) #:use-module (crates-io))

(define-public crate-hulahoop-0.1 (crate (name "hulahoop") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (default-features #t) (kind 2)))) (hash "0g8knkzzqhbifsba748ns1s1rjl0dpx68iz4px3yq83bmc8jpjy1") (v 2) (features2 (quote (("fxhash" "dep:rustc-hash"))))))

(define-public crate-hulahoop-0.2 (crate (name "hulahoop") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (default-features #t) (kind 2)))) (hash "14micny2rv420ra6hgqsryib6827m36rixc8acch9lqmnmb6a6fv") (v 2) (features2 (quote (("fxhash" "dep:rustc-hash"))))))

