(define-module (crates-io qy za) #:use-module (crates-io))

(define-public crate-qyzanaq-0.1 (crate (name "qyzanaq") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "1m7c4rn6sgw2kjrki71qjfnfw7y25myi0xnz85svl5mx7knvgz04")))

