(define-module (crates-io kg -l) #:use-module (crates-io))

(define-public crate-kg-lang-0.1 (crate (name "kg-lang") (vers "0.1.1") (deps (list (crate-dep (name "kg-diag") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "kg-js") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kg-tree") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "kg-utils") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1mg0w5d5rsq61dg0axh8jflfh9z5pc5yxdwsj3jrwf1066398m91")))

