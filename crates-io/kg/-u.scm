(define-module (crates-io kg -u) #:use-module (crates-io))

(define-public crate-kg-utils-0.2 (crate (name "kg-utils") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0apywdh4hs6hzhhwga7wh50qd37grnpda2q3b1zwdnm7cf84rks7")))

(define-public crate-kg-utils-0.2 (crate (name "kg-utils") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yibkyk1zbfwqghkdm0ngsa9cnngywybsrhf1zr47jwhadcv8yk0")))

(define-public crate-kg-utils-0.2 (crate (name "kg-utils") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b05d8lg36d5qvhndn020mrn2m5ah86r6d0wywrz3rvq751b2cp9")))

(define-public crate-kg-utils-0.2 (crate (name "kg-utils") (vers "0.2.3") (deps (list (crate-dep (name "heapsize") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ya36lx5qshs7c5irjpbp23jrk2yzqmhk0a24icvn2xf6ngisd7r") (features (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.2 (crate (name "kg-utils") (vers "0.2.4") (deps (list (crate-dep (name "heapsize") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (optional #t) (default-features #t) (kind 0)))) (hash "08p5cbb4c5b00adk7hl83g8iar736l3rkf489pghc7gax9jjn8ya") (features (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.3 (crate (name "kg-utils") (vers "0.3.0") (deps (list (crate-dep (name "heapsize") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (optional #t) (default-features #t) (kind 0)))) (hash "1rzimdalw7dy969cmq9vdvglcdwc2c4w2019h8jm43q1dmjiq837") (features (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.3 (crate (name "kg-utils") (vers "0.3.1") (deps (list (crate-dep (name "heapsize") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (optional #t) (default-features #t) (kind 0)))) (hash "1426y45v42k0c7x5z168qwg4bian5v309b6irpyxpfqhgdivix7g") (features (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

(define-public crate-kg-utils-0.4 (crate (name "kg-utils") (vers "0.4.0") (deps (list (crate-dep (name "heapsize") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.9.0") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (optional #t) (default-features #t) (kind 0)))) (hash "1rwlnsyqjn1xamvf47vb15zbcad7w76wyqwg7a52l6mk6in0ajfc") (features (quote (("serde_impl" "serde") ("heapsize_impl" "heapsize") ("default" "serde_impl" "heapsize_impl"))))))

