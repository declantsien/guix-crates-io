(define-module (crates-io kg lt) #:use-module (crates-io))

(define-public crate-kgltf-0.1 (crate (name "kgltf") (vers "0.1.0") (deps (list (crate-dep (name "kserde") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kg4fg4li61g2wixp3mb3ny0jpyh5cbwi12pxj16jlpyvz9cank4")))

