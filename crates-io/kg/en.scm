(define-module (crates-io kg en) #:use-module (crates-io))

(define-public crate-kgen-0.1 (crate (name "kgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0j3nisv7gs0yyy20byc0rxbnab0129cq0adsjyxl3f5fhz2aj9cm") (yanked #t)))

(define-public crate-kgen-0.1 (crate (name "kgen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13ac1qhh5mhbnw9mwmdf4rgxby273zgqzlp8rh3z2sk76qv7lk4x") (yanked #t)))

(define-public crate-kgen-0.3 (crate (name "kgen") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "12ck6vj5ss7pakkq33vj6qxp3c7wicfwnir49n9cjgb025g352h9") (yanked #t)))

(define-public crate-kgen-0.4 (crate (name "kgen") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0hrhgf9n6jxhc5m39r9f5f1688ayr68yyp2qwr84m089bqaarmiq") (yanked #t)))

(define-public crate-kgen-0.4 (crate (name "kgen") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0r2vlf6wfcw2dp87qd8g3y5sq4hwpc9qx3nfiwygz8fkcjg17ygd") (yanked #t)))

(define-public crate-kgen-0.4 (crate (name "kgen") (vers "0.4.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1pwi73g36w4la3ggqg6asmajwv2b591haa9y31dybbd1ykigdf06")))

