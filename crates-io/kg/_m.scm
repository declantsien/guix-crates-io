(define-module (crates-io kg _m) #:use-module (crates-io))

(define-public crate-kg_moment-0.0.1 (crate (name "kg_moment") (vers "0.0.1") (deps (list (crate-dep (name "time") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "0jq5vxbxzkwj6qxhvj4y3zc02yd5s4rl1vwmncf0q192bpiliasq")))

