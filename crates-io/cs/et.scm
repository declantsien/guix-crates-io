(define-module (crates-io cs et) #:use-module (crates-io))

(define-public crate-cset-0.1 (crate (name "cset") (vers "0.1.0") (deps (list (crate-dep (name "cset-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hfy31jwsgs8v4375y399fxrrvphb3000ajgbqz0a160dcy7mhnj")))

(define-public crate-cset-0.1 (crate (name "cset") (vers "0.1.1") (deps (list (crate-dep (name "cset-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z3wvxhhvilr5j7v2g4lbrrdzkj4a44a6qcz4bylsznlyvr0hdcg")))

(define-public crate-cset-derive-0.1 (crate (name "cset-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1jfh7sxfzn5qmjkswljgyhf0jqrbasws12378m9yawg69d5al5z9")))

(define-public crate-cset-derive-0.1 (crate (name "cset-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0kj576ad7najsa9xqaq7hhdl8m8qpwiykcdqx1ki8x19xzi99dh1")))

