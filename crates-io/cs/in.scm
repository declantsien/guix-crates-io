(define-module (crates-io cs in) #:use-module (crates-io))

(define-public crate-csinsertion_sort-0.1 (crate (name "csinsertion_sort") (vers "0.1.0") (hash "1b9qzqbp01pwf73g8cj3yqv86220j60r38yk3n6872m3469vc8s0") (yanked #t)))

(define-public crate-csinsertion_sort-0.1 (crate (name "csinsertion_sort") (vers "0.1.1") (hash "0mf9sshi9dxmjzidfwpp51dfkrrhi6dirycb528h834pxjlm1yfm") (yanked #t)))

(define-public crate-csinsertion_sort-0.1 (crate (name "csinsertion_sort") (vers "0.1.3") (hash "1kh4449dlx4gdjasi13vs6vwf4xg4mglw0m93vzfwwhd23jy35xx")))

