(define-module (crates-io cs c4) #:use-module (crates-io))

(define-public crate-csc411_arith-0.1 (crate (name "csc411_arith") (vers "0.1.0") (hash "12iahm9ncwliyvdpnsp0nvw9h12apav6ah36k7gqmig0j9p4fmay")))

(define-public crate-csc411_image-0.1 (crate (name "csc411_image") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "08i22w847n4hx32my4hba0sg99rahzxh3qdpmanriw5bab1kl0i8")))

(define-public crate-csc411_image-0.2 (crate (name "csc411_image") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "1n1g7p4mcjz0fd3mvdg6azg1kzirq2zxy5g78c5ssq3rgn1vd6ic")))

(define-public crate-csc411_image-0.2 (crate (name "csc411_image") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "17dlam0xq9a20xd2061hf7xsrjl9ih5wfpfgwxdxfcczd00dr5mk")))

(define-public crate-csc411_image-0.2 (crate (name "csc411_image") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "0wnswc2qij3vhdl45ds724dylxx8wala0mrm7ijbgppyjx1pmfgj")))

(define-public crate-csc411_image-0.2 (crate (name "csc411_image") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "1m35cxza553mfs42gnykid8csyn5zs5w57c8xx47bfxr40r4wcvk")))

(define-public crate-csc411_image-0.3 (crate (name "csc411_image") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "0bnn24s3jq5176h3x4jmr6p6rx8g1289m7v5l1h2hf9jly5ry218")))

(define-public crate-csc411_image-0.3 (crate (name "csc411_image") (vers "0.3.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "17c93sk1kzhfqlz8apx31y9ywja5pdvfjb30f2i7g74xaifb6spp")))

(define-public crate-csc411_image-0.3 (crate (name "csc411_image") (vers "0.3.2") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "194sq1rdfcp211czf4067sli2z2dg9317l2p176lqqgib9x6yfbm")))

(define-public crate-csc411_image-0.3 (crate (name "csc411_image") (vers "0.3.3") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "17wsmzbvnzzqp4hfi1c3cl9cplw97rxpgb16n1qwmd7hhqzd9rfa")))

(define-public crate-csc411_image-0.3 (crate (name "csc411_image") (vers "0.3.4") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "0ng7z4hx6k8vc9v71b9gwn30yhma4phkqf5xr3x1ncwi06qih8vv")))

(define-public crate-csc411_image-0.4 (crate (name "csc411_image") (vers "0.4.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)))) (hash "1j5j38dlg4rc21pdnw7mygs05zdyqmn41nmhkn7jx32jfx0yn61c")))

(define-public crate-csc411_image-0.5 (crate (name "csc411_image") (vers "0.5.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "0z8i2xxp5sz28ajn1pz1bjvgd8wy3asfwxkw5vzh3563p3xsqnsi")))

(define-public crate-csc411_image-0.5 (crate (name "csc411_image") (vers "0.5.1") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "184j09wi1wkzzsxqwgsv0m4ah1lmz70xg6gv9xdz92g887h6wzwr")))

(define-public crate-csc411_image-0.5 (crate (name "csc411_image") (vers "0.5.2") (deps (list (crate-dep (name "image") (req "=0.24.7") (default-features #t) (kind 0)))) (hash "1iqi2wr7i0gbr5gnxsb0s0i7pp2zgz8slrgxk7cdivm8s1agak6v")))

(define-public crate-csc411_rpegio-0.1 (crate (name "csc411_rpegio") (vers "0.1.0") (hash "1inas32l1ji0kg28p2n1i7gl4lv0299qfapp5l5szxgbqf21fmzh")))

(define-public crate-csc411_rpegio-0.2 (crate (name "csc411_rpegio") (vers "0.2.0") (hash "13kkb4r2ddaf8z09b21w53aiymdn8iahllfm2020vs57lj34ilnh")))

(define-public crate-csc411_rpegio-0.3 (crate (name "csc411_rpegio") (vers "0.3.0") (hash "1fj5x5zndg10y8rffmrv0d4ffy932razlvf1y81mbg3l1jjsyv8m")))

(define-public crate-csc411_rpegio-0.3 (crate (name "csc411_rpegio") (vers "0.3.1") (hash "1pq1h347ps588vbhcp9viv69bkpds08nc639siijkvwvkc72sx6g")))

(define-public crate-csc411_rpegio-0.4 (crate (name "csc411_rpegio") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0ssg0iq9k7vpwsiq7cs0r5acvkapdddwgwnkp0p1gzfkq698ixn3")))

(define-public crate-csc417-0.1 (crate (name "csc417") (vers "0.1.0") (hash "1l05czn5m67mibvbm3mk1fscb6ic9fzy6fl31rdhda7rk7dcydxc")))

