(define-module (crates-io cs #{50}#) #:use-module (crates-io))

(define-public crate-cs50-1 (crate (name "cs50") (vers "1.0.0") (hash "04hg19k2h7cn427wlz5q50hkl5ch586j3z8bl36vvxqd50cxz8mm")))

(define-public crate-cs50-1 (crate (name "cs50") (vers "1.0.1") (hash "1l5281qxqvanbkyjpra9901zvk0wgys2lj7aliayc8hhajrmzqrm")))

