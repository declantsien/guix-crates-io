(define-module (crates-io cs cl) #:use-module (crates-io))

(define-public crate-csclib-1 (crate (name "csclib") (vers "1.0.0") (hash "0cqg1dg01ph1530xqsv6280di8yx9lrxjmjinpcjls4b36p61k03") (yanked #t)))

(define-public crate-csclib-1 (crate (name "csclib") (vers "1.0.1") (hash "0l3i4rqanbjwvs3id15h7lnxqy0xjn91qmjr16ffzlwr09mr235v")))

