(define-module (crates-io cs vk) #:use-module (crates-io))

(define-public crate-csvksjtex-0.1 (crate (name "csvksjtex") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "1sj1lqlnw3ml5cqxvnsr6qy7cbnjflj07cxvsl647srf1k0dlab5")))

