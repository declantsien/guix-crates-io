(define-module (crates-io cs te) #:use-module (crates-io))

(define-public crate-cstea-1 (crate (name "cstea") (vers "1.0.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rettle") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08a0mk3wyr5g0scrmxadh1fcdflysk5hx5wqy3h2yczdvdyq2jmk")))

(define-public crate-cstea-1 (crate (name "cstea") (vers "1.0.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rettle") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xsx9pzzsl4y346wsb9wi1crb0nd40sls06aqsdnq532bqbykrn8")))

(define-public crate-cstea-2 (crate (name "cstea") (vers "2.0.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rettle") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yvkmcbd78fd4n8xlwgpl6kjl6aw8vqfbm1pf0mmny72qx5p3cfb")))

(define-public crate-cstea-2 (crate (name "cstea") (vers "2.0.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rettle") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0972jdcgd6pgh5n845jf3f80pnl51drnlh823ldwxpkyc9s12x8z")))

