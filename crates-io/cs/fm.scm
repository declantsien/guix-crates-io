(define-module (crates-io cs fm) #:use-module (crates-io))

(define-public crate-csfml-audio-sys-0.1 (crate (name "csfml-audio-sys") (vers "0.1.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "sfml-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1l3bm9bxqfbqmlsm7a3y33j6b5az64gnc5vwllby7jvh4hxi8gph")))

(define-public crate-csfml-audio-sys-0.2 (crate (name "csfml-audio-sys") (vers "0.2.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1i943399jcn24brhk5xh7lsm5inxx7f399qyb2ik5pwyf7qvw70j")))

(define-public crate-csfml-audio-sys-0.4 (crate (name "csfml-audio-sys") (vers "0.4.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "190s0qidhndc5mj781vv99c97932gvzaqqmz9f5ysv8fs3ympxf3")))

(define-public crate-csfml-audio-sys-0.5 (crate (name "csfml-audio-sys") (vers "0.5.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0hl2iabrpys2apd7n82i72x8q8is5r1ln98a6dfd9m66cfvd6a5r") (links "csfml-audio")))

(define-public crate-csfml-audio-sys-0.6 (crate (name "csfml-audio-sys") (vers "0.6.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "15nsg4kz0r3a4b59cw5hivkklskb5gfl095qy3gq5igjnqjhvqpl") (links "csfml-audio")))

(define-public crate-csfml-graphics-sys-0.1 (crate (name "csfml-graphics-sys") (vers "0.1.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "csfml-window-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "sfml-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mcww7f0618zd5fmicbyzl1rmw1dzi1107978zmp8ik6rjl8is3j")))

(define-public crate-csfml-graphics-sys-0.2 (crate (name "csfml-graphics-sys") (vers "0.2.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "csfml-window-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0pcsw6sg94kxza0a0rimy06fkiabm5hawhqqapg678ib5vm56idl")))

(define-public crate-csfml-graphics-sys-0.3 (crate (name "csfml-graphics-sys") (vers "0.3.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "csfml-window-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1351d43b6x4lfbb1db7jvwn6pg5zagbnm6nrq02qgf9yh3xl8ymn")))

(define-public crate-csfml-graphics-sys-0.4 (crate (name "csfml-graphics-sys") (vers "0.4.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "csfml-window-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "11w4dl9grs9nnhmq4g37b7g3p95bnm11dq9f89809jknfd5w626m")))

(define-public crate-csfml-graphics-sys-0.5 (crate (name "csfml-graphics-sys") (vers "0.5.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "csfml-window-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0gpdba1xkh2fyn32d5j4viq7wy27y2ngi3cny5388na5h1jhqksf") (links "csfml-graphics")))

(define-public crate-csfml-graphics-sys-0.6 (crate (name "csfml-graphics-sys") (vers "0.6.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "csfml-window-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "15n9lqkprs2958q2s9q8w1lnz0l0dsja4qppadmx9rkdybjv94mm") (links "csfml-graphics")))

(define-public crate-csfml-network-sys-0.1 (crate (name "csfml-network-sys") (vers "0.1.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "sfml-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03ll0fzpxm9cr9qpg6bpyalp84jrhjnq9qwf8yv4ajvk4hll64dx")))

(define-public crate-csfml-network-sys-0.2 (crate (name "csfml-network-sys") (vers "0.2.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1vxgcnac0w2ybxy1p9vgchz6lgl1ndj7xgni470yg29bzgbp6x83")))

(define-public crate-csfml-system-sys-0.1 (crate (name "csfml-system-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0v90fyf40hvr1kyc6xa6paphk65n186mi1k20sg598mwjagk04zn")))

(define-public crate-csfml-system-sys-0.2 (crate (name "csfml-system-sys") (vers "0.2.0") (deps (list (crate-dep (name "sfml-build") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0g3gzpgz7irlk9adjb9pfmr2yalk15n3yc317q4s20yac864d9bn")))

(define-public crate-csfml-system-sys-0.2 (crate (name "csfml-system-sys") (vers "0.2.1") (deps (list (crate-dep (name "sfml-build") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1ifiawxp8cs96akx6x00ikqr6kgs25rsf6jm9fz2bf33nhppwf4f")))

(define-public crate-csfml-system-sys-0.4 (crate (name "csfml-system-sys") (vers "0.4.0") (deps (list (crate-dep (name "sfml-build") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "1zd1xxa4xrz0y3v6r3al49xvs7qh8ig5yyf05wnmkmsi0sn6js12")))

(define-public crate-csfml-system-sys-0.5 (crate (name "csfml-system-sys") (vers "0.5.0") (deps (list (crate-dep (name "sfml-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0pg3mirhg1lb330q8ynskicgv9hspdmp165p9vanh1bzvb1fzghf") (links "csfml-system")))

(define-public crate-csfml-system-sys-0.6 (crate (name "csfml-system-sys") (vers "0.6.0") (deps (list (crate-dep (name "sfml-build") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "1byg6z04f2ss9k9847d7s5an96jc16hbkbda0cb4hw6fkv5vf6a0") (links "csfml-system")))

(define-public crate-csfml-window-sys-0.1 (crate (name "csfml-window-sys") (vers "0.1.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "sfml-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rm4q8lfq9c0y81y15z6d0wxilf17yj59vc99wlj7xrjw7c2s16a")))

(define-public crate-csfml-window-sys-0.1 (crate (name "csfml-window-sys") (vers "0.1.1") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "sfml-types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kzvqg8wn3r5d2hb733dh93kixk2mdcpjsyn5bkag73ld4drckzp")))

(define-public crate-csfml-window-sys-0.2 (crate (name "csfml-window-sys") (vers "0.2.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0ja5sf3jqx56zadn9gsxpb03xfnzaialibksbd1dpgmjxmzjd3nj")))

(define-public crate-csfml-window-sys-0.4 (crate (name "csfml-window-sys") (vers "0.4.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "065n3ifbn8b0xslrkc6v8xcb30ap3d667vqcjvh2myd982c0vzc6")))

(define-public crate-csfml-window-sys-0.5 (crate (name "csfml-window-sys") (vers "0.5.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1lqxqhbzmvgyiyycjf5pbbic6ic1f5ariknfgizicik6186h138x") (links "csfml-window")))

(define-public crate-csfml-window-sys-0.6 (crate (name "csfml-window-sys") (vers "0.6.0") (deps (list (crate-dep (name "csfml-system-sys") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "sfml-build") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "05qs6pmggglrlgbaw80xg2awc82hpxdz403775ykgzdanv17cw72") (links "csfml-window")))

