(define-module (crates-io cs f_) #:use-module (crates-io))

(define-public crate-csf_benchmark-0.1 (crate (name "csf_benchmark") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csf") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fsum") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ph") (req "^0.7") (features (quote ("wyhash"))) (default-features #t) (kind 0)))) (hash "14k15d0h3r7dj11kczgvayp624g5plbx3wv9m44681912pafl1k3")))

(define-public crate-csf_benchmark-0.1 (crate (name "csf_benchmark") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csf") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ph") (req "^0.7") (features (quote ("wyhash"))) (default-features #t) (kind 0)))) (hash "1fqlbrwll5mi4zmf601qlg94lsy8sf40gjfxg2khfx3f7k2l8g10")))

(define-public crate-csf_benchmark-0.1 (crate (name "csf_benchmark") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csf") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ph") (req "^0.7") (features (quote ("wyhash"))) (default-features #t) (kind 0)))) (hash "0k2lmaj7l120rcyhknbmilf7lxd0q053kll93fdijfzccvbn1ydg")))

(define-public crate-csf_benchmark-0.1 (crate (name "csf_benchmark") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csf") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ph") (req "^0.8") (features (quote ("wyhash"))) (default-features #t) (kind 0)))) (hash "06k1pdkfz7j1dqdh1rw3wx4g19j9hw8j0snabl1zp9dj434x4337")))

(define-public crate-csf_benchmark-0.1 (crate (name "csf_benchmark") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csf") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ph") (req "^0.8") (features (quote ("wyhash"))) (default-features #t) (kind 0)))) (hash "1jslw71whkhh6vrk2pxrvx5pzpkwgqdf0n3v4nvlldqlqy5lmmll")))

(define-public crate-csf_benchmark-0.1 (crate (name "csf_benchmark") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csf") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ph") (req "^0.8") (features (quote ("wyhash"))) (default-features #t) (kind 0)))) (hash "0k6vjgq9ivr6l5z8r07nqiphkgj2j84f0wwx1qhkq4iid5nf7xy2")))

