(define-module (crates-io cs si) #:use-module (crates-io))

(define-public crate-cssifier-0.1 (crate (name "cssifier") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "1adwxkdhrs0pr2918j72m9vmpsvs939j6zm9bgpnvc1by9wjiqm5")))

(define-public crate-cssifier-0.1 (crate (name "cssifier") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "0l3spd9wcwzyy59gbzsc577nfa9p19irilm80vrwcapajzl09ikw")))

(define-public crate-cssifier-0.1 (crate (name "cssifier") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d1ik872897fvnkyc8sjw0yhrbnjwfx16bsc19k6b8yjm3vrwdv5")))

(define-public crate-cssifier-0.1 (crate (name "cssifier") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "0srmjkjs25q3jw62lvf7fc2j5gdk1z7ggk2ncbnrh0y4h2kb96fa")))

