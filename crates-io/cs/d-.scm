(define-module (crates-io cs d-) #:use-module (crates-io))

(define-public crate-csd-rs-0.1 (crate (name "csd-rs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0w6abwwkzlsgxsxn9h9bfdixsj4sc7awbwsr2wcw2daj1awkkm1k")))

