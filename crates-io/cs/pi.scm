(define-module (crates-io cs pi) #:use-module (crates-io))

(define-public crate-cspice-0.0.1 (crate (name "cspice") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cspice-sys") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_plain") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1ziyfcdwa4f9sas79v57g1pf15iwx11hs6ymc8kdhx4x8hyrxycl")))

(define-public crate-cspice-0.1 (crate (name "cspice") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cspice-sys") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_plain") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0q8akc8a277xcdmnwz9n7cpn5h7iv0shc4z27991z398msjbxx85")))

(define-public crate-cspice-sys-0.0.1 (crate (name "cspice-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (optional #t) (default-features #t) (kind 1)))) (hash "0mjisl42y0r3w6xirngph0k51xs35cgjx2kizk3adrz8k9za5cbd") (features (quote (("generate" "bindgen")))) (links "cspice")))

(define-public crate-cspice-sys-1 (crate (name "cspice-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1lcdzfil75wg6kxpm8vaw22p4gapn9zqld6mbp6pa368yh9iwhxz")))

(define-public crate-cspice-sys-1 (crate (name "cspice-sys") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "0hd9zp1mqa6xwsdrmjmvdf8hwdg7shx6wiismqsv33fclbvv37hf") (yanked #t)))

(define-public crate-cspice-sys-1 (crate (name "cspice-sys") (vers "1.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "06fz98vk50mqd5c19p71jsfm2mfk0nd14m9n7rzh30i8kz30k0iq") (yanked #t)))

(define-public crate-cspice-sys-1 (crate (name "cspice-sys") (vers "1.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1181fq2zi7d964ks2cigh4hsigy9ap6s8hfza9cgzd6q7xrc70wp")))

(define-public crate-cspice-sys-1 (crate (name "cspice-sys") (vers "1.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("blocking"))) (optional #t) (default-features #t) (kind 1)))) (hash "01127lljyj935knxkd7p726cl58kfkq6ki9ic3xmwnp5wgqqlqgq") (v 2) (features2 (quote (("downloadcspice" "dep:reqwest"))))))

