(define-module (crates-io cs vt) #:use-module (crates-io))

(define-public crate-csvtool-0.0.1 (crate (name "csvtool") (vers "0.0.1") (hash "1pvcvl73gch0sax1d4q7mkh4ipmwhhb4wi7k2ckv4ydxrw7z3vl2")))

(define-public crate-csvtoron-0.2 (crate (name "csvtoron") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jwpywgzschq5l19zpsxz5zfbf5mab93lcwapcam8jk2fh0pzzvd")))

(define-public crate-csvtosql-core-0.1 (crate (name "csvtosql-core") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "0dd2mrnv7gv0djxlp5jr2j3f1ih6rvqh51dpmfsd43xfgzr44sxj")))

(define-public crate-csvtosql-core-0.2 (crate (name "csvtosql-core") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "09fla8bjcn6ngj0g50nsml31fr7g6bhynrzkcj2235mlhnh207ki")))

(define-public crate-csvtosql-core-0.2 (crate (name "csvtosql-core") (vers "0.2.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)))) (hash "0g4826vil01kqqi6k2654h3dg1w4cd2srw49gawanb56fhjwkli7")))

