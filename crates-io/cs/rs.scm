(define-module (crates-io cs rs) #:use-module (crates-io))

(define-public crate-csrs-0.1 (crate (name "csrs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "00mypd7j3zvad4azmk9481v4wn20n1jkniplm374jpx824fswjds")))

(define-public crate-csrs-0.1 (crate (name "csrs") (vers "0.1.1-dev") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1sb11qiklprwbwd3f8fikxxcy2k6amk9aaa3nmsgxr366lqr91f3")))

(define-public crate-csrs-0.1 (crate (name "csrs") (vers "0.1.2-dev") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0lc02asfzw8zx9ryi3q09phps4kx0k2bxgv6dz5xvnlr0ivqds21")))

