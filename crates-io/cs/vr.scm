(define-module (crates-io cs vr) #:use-module (crates-io))

(define-public crate-csvr-0.1 (crate (name "csvr") (vers "0.1.0") (hash "0v60kjdfp62c0yavasd7cv13bia30sgy4g341ryh3m9d0k6z35ac") (yanked #t)))

(define-public crate-csvr-0.1 (crate (name "csvr") (vers "0.1.1") (hash "04viax469qwnzkqbiqy8kky6mcpx5rlkb7v3ykw43l0cdgg5rf2r")))

(define-public crate-csvre-0.1 (crate (name "csvre") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "01cixr2qd6vmk1lix1bjfk96kh96zgzgp6mzidx31csh0fsqa4s4")))

(define-public crate-csvroll-0.1 (crate (name "csvroll") (vers "0.1.0") (deps (list (crate-dep (name "rollbuf") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1lphmvswxgmkf2bv07fazw14xy3c95qp00zp6a6745y48v4mv2zv")))

(define-public crate-csvrow-0.1 (crate (name "csvrow") (vers "0.1.0") (hash "1ww6bwb4ij65hahdwd1xv4m0a90l3m3dbjxik2fcixrgw7c6kwha")))

(define-public crate-csvrow-0.1 (crate (name "csvrow") (vers "0.1.1") (hash "0ks47mk81xrxigshbfavrb6h7n95khnn9gq0qsgsn1g3andw5a3i")))

(define-public crate-csvrow-0.2 (crate (name "csvrow") (vers "0.2.1") (hash "01fbfh0ywimvlwdsljmrf1bbka76h819sd56fnyygvjj3k0gpp5l")))

