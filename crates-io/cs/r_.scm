(define-module (crates-io cs r_) #:use-module (crates-io))

(define-public crate-csr_matrix-0.1 (crate (name "csr_matrix") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)))) (hash "1mdf3jbfm7131742fmf91mcl5ys9s7f8vykzqfmfbdqmv7jc6fn6")))

