(define-module (crates-io cs s3) #:use-module (crates-io))

(define-public crate-css3-0.1 (crate (name "css3") (vers "0.1.0") (hash "1xxndrkcj5vzmryw2amkf287kl1nlxh51ddxf1d95gdwr5iy4dy8")))

(define-public crate-css3-0.1 (crate (name "css3") (vers "0.1.1") (hash "0pnk2szvazr4h102kr1f8dz1ljaq0i2c1lndxf8a4cgsbga53f7s")))

(define-public crate-css3-selector-0.1 (crate (name "css3-selector") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0xccwj23f4w5w1nbp4bkc2d6yyq3s43yrb2mpbqn1ax4mqq7hb0n")))

(define-public crate-css3-selector-0.1 (crate (name "css3-selector") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x1s4y8bgapj8pchg8plczrc12mn1y0iwzxgjbds577cbnlnpyqb")))

(define-public crate-css3-selector-0.1 (crate (name "css3-selector") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "063nj3mm28dfdry6bqmaw756wd9d7dmmyh05h9jh86s364haq6rv")))

(define-public crate-css3-selector-0.1 (crate (name "css3-selector") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gy8s88bzkvkk7hi4xirvf4k4r41gi171v9gixd3s4qdl6jhz9xn")))

