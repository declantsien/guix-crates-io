(define-module (crates-io cs #{45}#) #:use-module (crates-io))

(define-public crate-cs453getters-0.1 (crate (name "cs453getters") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.41") (default-features #t) (kind 0)))) (hash "0gs5q378p4i5kr4qz5494k70i3b949a20dascn0hvpwa8mqggr1l")))

