(define-module (crates-io cs um) #:use-module (crates-io))

(define-public crate-csum-0.1 (crate (name "csum") (vers "0.1.0") (deps (list (crate-dep (name "checksums") (req "=0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "=3.2.22") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "08l2j4gq6aadpdmpvjgvk3zxbv1pgh4mnc2v5rs0jsnfm81zmbfc") (rust-version "1.63")))

(define-public crate-csum-0.1 (crate (name "csum") (vers "0.1.1") (deps (list (crate-dep (name "checksums") (req "=0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "=3.2.22") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "11fh2vzjx2lwmkixf8yvc5hmnnh4pv5shfg6n8w5m974shmndmlk") (rust-version "1.63.0")))

(define-public crate-csum-0.2 (crate (name "csum") (vers "0.2.0") (deps (list (crate-dep (name "checksums") (req "=0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "=3.2.22") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "00xw534r5kjspkbjgpig5yxx0551q6jgkn73bb4kj0cmj8whx8fy") (rust-version "1.63.0")))

(define-public crate-csum-0.2 (crate (name "csum") (vers "0.2.1") (deps (list (crate-dep (name "checksums") (req "=0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "=3.2.22") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1496pvnh80ma65msllmfkxxx1wiacnb9ga9dw6gim9rrkb1fgibw") (rust-version "1.63.0")))

