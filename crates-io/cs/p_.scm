(define-module (crates-io cs p_) #:use-module (crates-io))

(define-public crate-csp_generator-0.1 (crate (name "csp_generator") (vers "0.1.0-alpha") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fapvh2s209v4jz4ii00kf9n5m1frz40sw0zrgibmxd079xailhl")))

(define-public crate-csp_generator-0.1 (crate (name "csp_generator") (vers "0.1.0-beta") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ljx11fm6571cshkkdvfijhmk4p7zbniy36q1dfgvjbrljnh5smk")))

(define-public crate-csp_generator-0.1 (crate (name "csp_generator") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lm89zjsl6sk53g5fc7i7vbydc7gsv9949cibkk9avpb6yj8mh1y")))

(define-public crate-csp_generator-0.1 (crate (name "csp_generator") (vers "0.1.0-rc") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rj0h63swgdw5zrf4x0yza0rkvjifm1v5cim73dxq730dvwiak8p")))

(define-public crate-csp_generator-0.1 (crate (name "csp_generator") (vers "0.1.0-rc.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rf06rzd0asw9xln49l0y3vvp0ljn03dj6ncykl6n4r0hlh7knr4")))

(define-public crate-csp_generator-0.2 (crate (name "csp_generator") (vers "0.2.0-beta") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1880mya6s97zzjvnpp8ngza30z1h32apd2kdp1lb0g7kkn458dd9")))

(define-public crate-csp_generator-0.2 (crate (name "csp_generator") (vers "0.2.0-beta.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n8s18027isvivd249cpkgz4n9x32w6pdbq1mkxn2ckqiv0ixzs7")))

(define-public crate-csp_generator-0.2 (crate (name "csp_generator") (vers "0.2.0-beta.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0a0n4h0am56rxiwyyxvzy9lrayv4v0czy26sdyizswd8gyc0sx8p")))

(define-public crate-csp_generator-0.2 (crate (name "csp_generator") (vers "0.2.0-beta.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hqxj762giw1vrl3j7nrlbb73xxk96cjm8s2xj2vy8gjvv0jx02c")))

(define-public crate-csp_generator-0.2 (crate (name "csp_generator") (vers "0.2.0-rc") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h2vmls5wp83mc2q1yz47ij6sp83mq418i6xhilcdqix2v3cjgji")))

