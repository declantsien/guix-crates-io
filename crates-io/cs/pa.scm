(define-module (crates-io cs pa) #:use-module (crates-io))

(define-public crate-csparse21-0.2 (crate (name "csparse21") (vers "0.2.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "04j2mi1vb23vhxq8wm75vna40s2kawmmw21550w795x987z5wxwc")))

(define-public crate-csparse21-0.2 (crate (name "csparse21") (vers "0.2.3") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "09nfvrfjsaxmdv55a7y36abkpc6cjvxmclrjnkhamzq7xfqlcc98")))

