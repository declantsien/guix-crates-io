(define-module (crates-io cs ta) #:use-module (crates-io))

(define-public crate-csta-0.1 (crate (name "csta") (vers "0.1.0") (deps (list (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0db4k7yj7l40lnvrrixxvbv5074f2pgz2k98vfc6fzkfngjxw1hq")))

(define-public crate-csta-0.1 (crate (name "csta") (vers "0.1.1") (deps (list (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "098yjranpgg1f8hcl0m6qd72n4crsl0f7ab5bdl7ds7xp7rnlka2")))

(define-public crate-csta-0.2 (crate (name "csta") (vers "0.2.0") (deps (list (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zj183qs9z6zb6cj0a71rga0yndb2gn7l33gwhbj8dh3z19dp4cm")))

(define-public crate-csta-0.3 (crate (name "csta") (vers "0.3.0") (deps (list (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ga5xfs3vqfs7i4vb5mqa86i5v269y53hi3qqzwxb5kl8lgqkzli")))

(define-public crate-csta-0.4 (crate (name "csta") (vers "0.4.0") (deps (list (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1flqmqrws51hqly3j63nkwkj5wchcbc569zkh7w7ghnwwg8aln1z")))

(define-public crate-csta-0.5 (crate (name "csta") (vers "0.5.0") (deps (list (crate-dep (name "piston_window") (req "^0.112") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "plotters-piston") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1dip136f7vgdbaiqckggsd3jp3rj03qpizblmrvvwbdmd76imcrd")))

(define-public crate-csta-0.6 (crate (name "csta") (vers "0.6.0") (deps (list (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xsdhs9q8nxpfwl4m0mwg56jdqb9mgb08pgy1jmgcy5sakg0v3bm")))

(define-public crate-csta-0.7 (crate (name "csta") (vers "0.7.0") (deps (list (crate-dep (name "plotters") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "18i2zqj2bgrimb2p64c6b67qli8azz2l1n2qvkbcbjrh53w3ll1l")))

(define-public crate-csta-1 (crate (name "csta") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0qx10wxgc941sdghcm560vr8xpad8kgs5wr5vr7mfjqq3f434ww2") (features (quote (("default"))))))

(define-public crate-csta-1 (crate (name "csta") (vers "1.0.1") (deps (list (crate-dep (name "csta_derive") (req "=1.0.0") (default-features #t) (target "cfg(any())") (kind 0)) (crate-dep (name "csta_derive") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dcvy4as3k94niq9lgpvfi938i0hmps6ifsy7fmb1j88p4q3m10r") (features (quote (("derive" "csta_derive") ("default"))))))

(define-public crate-csta_derive-1 (crate (name "csta_derive") (vers "1.0.0") (deps (list (crate-dep (name "csta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0syflpcpmla62i1zs6rk14fc9kjwvcl24a0ix0f6qpnk7kcmgrz6") (features (quote (("default"))))))

(define-public crate-cstars-0.1 (crate (name "cstars") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "lib_cstars") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "197nshf1yh35fvlxksj0fvnagcmi5jnwrz4fp3ypj9afdsslzf08")))

