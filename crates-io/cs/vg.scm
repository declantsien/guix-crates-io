(define-module (crates-io cs vg) #:use-module (crates-io))

(define-public crate-csvgen-0.1 (crate (name "csvgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0-pre.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "079jaxii7rm4vzb1c0bh4lnp53q0s6j5xyjrbp4c4gy1pdjk06cm")))

(define-public crate-csvgen-0.1 (crate (name "csvgen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.0-pre.1") (default-features #t) (kind 0)))) (hash "0r4ir036hhvklgjiqrdfbxqx3x2x3y5dbay0f6zsf4h9ziacsr7y")))

