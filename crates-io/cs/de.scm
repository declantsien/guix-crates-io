(define-module (crates-io cs de) #:use-module (crates-io))

(define-public crate-csdeps-0.1 (crate (name "csdeps") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nza6bcimpjz53iz94qd2gjz4nv8i5h208k275x7md5k7llgg6m9")))

(define-public crate-csdeps-0.1 (crate (name "csdeps") (vers "0.1.2") (deps (list (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wbh1p53fifbnxrk5gqjygbjp086c3yrq2s448mlcn2ibpp0732x")))

