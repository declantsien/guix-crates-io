(define-module (crates-io cs vx) #:use-module (crates-io))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ijlzklpbq5byvnskyk6jbm546prxvz971yv0in61jc6mqn8a3db")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pj960mx9svmx6mkwsp94c6nq550lw35j1cjpm94ymg8qrbin8wp")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qamszbzknxibnyss3ng1irk8p88rhl7kwx8w1mp7cjf72cgp7gp")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1411jsbj650yp4xyq9gkfinjxgwdvdi2s1f7dmyrfy2hgvfya5bi")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.4") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0yw7k3b4f2kxlxlk9l30izsq66rlilwllzpbnliq6v8m5qancv3h")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.5") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0rxxbdsccw5jrlmwvkxyjhdjjyw364nnjs1zzbqnjgkgr35261il")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.6") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "18vxm1zam5yzjq0xdrc4x11vs585jr8lvr60l065nnbzkx08fws5")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.7") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0a5gz3hazkyg5z6x5ay6i96ifxd0001n85sb6aj3abrh2lscr2lc")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.8") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1v21vpvijhysadnx2v79xl90fppyf4z0ksaa6aj5v7pqdlwlsbf8")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.9") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0jhcm6szm420cmqvxq5rz9g0vbgg2cqsg6hl4ri4qmlvbhyzfmmj")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.10") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0q954gcy5acqva4fvvqkpgalyycd7473gpgki09ip44zqjl9kdxq")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.11") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "068j7sfkprwjf730sxcvl2yg86m349p56l8d5n2jc0f1i7c24c3y")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.12") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0j1whfgg4szpcc9djmsiw4mywmrh7kryp03nc40v8bfln514yp07")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.13") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0zjhr4r5g7cbvmxdib24kyv05xmdmlpd4rx050ilg3mi9278n3rv")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.14") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1573m6sj7havk9d2lm6lxlxs72zrdkk5p83lsw1gikx08mkkq1x5")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.15") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "01naccjgsbldngl9a5xhd9s1kb1f87m2bvf59jdv1rwxs97a2qv7")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.16") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "13fbhpz89cir7k67qbc6qa1qql86353f3h3782jj88kkxikb10cz")))

(define-public crate-csvx-0.1 (crate (name "csvx") (vers "0.1.17") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1drcvnidyiw73z9v5p91wngvz8lgvp7pca14pyhza0qxigyiw24j")))

