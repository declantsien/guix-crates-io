(define-module (crates-io cs pr) #:use-module (crates-io))

(define-public crate-cspretty-0.1 (crate (name "cspretty") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "182slzlcd925489zwabzj8x1fzxbpf5pp6vc6b32s6h9zg7bha3q")))

(define-public crate-cspretty-0.1 (crate (name "cspretty") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0in2svhwkvn2lxyzznsbwy865d3mh7jk2np62ldzaidmyyyqv926")))

(define-public crate-cspretty-0.1 (crate (name "cspretty") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0jprnlw1aw6kgzz1qr26nhzmvrkrvrl7widwl9bjalm32b7xrgwd")))

(define-public crate-cspretty-0.1 (crate (name "cspretty") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1dygrc5j4r7m87h1l80cl45qvx681f36ijawrqndzx049jz017y8")))

(define-public crate-csprng-0.1 (crate (name "csprng") (vers "0.1.0") (hash "06l2n3wpzlfsb25jqrid8vmis9s55zdip3596g0hf55rl9jdnvah") (yanked #t)))

