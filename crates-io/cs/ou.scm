(define-module (crates-io cs ou) #:use-module (crates-io))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0myww3z0bbd6nq9fnzl069sczlxmzmq53vlx9169c5fxj11zmsl6")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "09dk432d54z28w2mbgvzrchl9sl8l81d2yfaz27d0f6j5cgacvzl")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cj1fsc0d3scj7jmrw67gi2nzslsaw1dynlq23sqz1964pdnrkrc")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r9n4q7a1aj87rp2lnb2qk5ajw2i4pnmp01232dpc95m7c478c7i")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pf2ic4rkfgw1r11ncgv6kzmnr0svm4hv9bq3dc7z2ankrnzc59y")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ml88zm106j0hj1np85d3zck7dwgdb913v163x5ycv77zbxl266c")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.6") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "1i19g4hhp3xm9ckvavf64flbdjznc282ny20wf5n3xk3p2fj29p6")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.7") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "csound-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "0ii3zppp4md7b4qbb82llxbbxnfl8zn296szf95drk1qkbrqyjhr")))

(define-public crate-csound-0.1 (crate (name "csound") (vers "0.1.8") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0) (package "bitflags")) (crate-dep (name "csound_sys") (req "^0.1.2") (default-features #t) (kind 0) (package "csound-sys")) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0) (package "libc")) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2) (package "rand")) (crate-dep (name "va_list") (req "^0.1.3") (default-features #t) (kind 0) (package "va_list")))) (hash "0zfm90ilp0bm364vx3bcr0d11nm0i4hr680zvg5c5xb5fdfh8jcd")))

(define-public crate-csound-sys-0.1 (crate (name "csound-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "1g9sq3zypxwjmz140xpjgp4rz0n8mhg6plivm128963kqmzg5s68") (features (quote (("dynamic" "pkg-config") ("default" "dynamic")))) (links "csound64")))

(define-public crate-csound-sys-0.1 (crate (name "csound-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0albx8z0md3sy9ngjd1rjyll0msfkl4k9fx4ik8qdp9bgbd2xx91") (links "csound64")))

(define-public crate-csound-sys-0.1 (crate (name "csound-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0) (package "libc")) (crate-dep (name "va_list") (req "^0.1.3") (default-features #t) (kind 0) (package "va_list")))) (hash "0fgyphfq9pf5fkmiddr3nvap5wg9gmfcqrlys9j1n4gdx9liicc6") (links "csound64")))

