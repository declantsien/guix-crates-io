(define-module (crates-io cs pl) #:use-module (crates-io))

(define-public crate-csplib-0.1 (crate (name "csplib") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1272jxv9m3cvbspf0hzkd1vwji1g5fwc11jb30fwdawnz01g2yn1")))

(define-public crate-csplib-0.1 (crate (name "csplib") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1zhg7q0wpwvwpwvzf1jkks2lw1bdxs1ipa99r2blssrilnan8b7n")))

(define-public crate-csplib-0.1 (crate (name "csplib") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "csplib-macros") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0mj5m5n2i3rbqjaa3pc4d42c5rsjsnjas2cf0cp8k92r95afzxcm")))

(define-public crate-csplib-macros-0.1 (crate (name "csplib-macros") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0q5xxv4x52xnb3y4097gzfj958zv8qn8qlnqiddpmcg6d80wivcq")))

(define-public crate-csplit-0.1 (crate (name "csplit") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xzlp2yy8dmhmc2pb858ij56xfva0638wx6n7bjv8h4dj7j0220z")))

