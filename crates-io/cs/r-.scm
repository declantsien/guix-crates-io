(define-module (crates-io cs r-) #:use-module (crates-io))

(define-public crate-csr-gen-0.2 (crate (name "csr-gen") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.21.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0n72h064iylisaz4qw8ri60dg8ljdal9z0r5bqqph6apwspqmhj0")))

