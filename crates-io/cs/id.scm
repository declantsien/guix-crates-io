(define-module (crates-io cs id) #:use-module (crates-io))

(define-public crate-csidh-0.1 (crate (name "csidh") (vers "0.1.0") (deps (list (crate-dep (name "const-primes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crypto-bigint") (req "^0.6.0-pre.12") (kind 0)) (crate-dep (name "oorandom") (req "^11") (default-features #t) (kind 0)))) (hash "1238mr3qazwf0rj4xi2057gy1281ri94n60p9rby4p6pq0q5b9dz") (features (quote (("no_cm_velu") ("no_cm_p_plus_1_over_4") ("no_cm_order") ("no_cm" "no_cm_velu" "no_cm_p_plus_1_over_4" "no_cm_order")))) (rust-version "1.76")))

(define-public crate-csidh-0.2 (crate (name "csidh") (vers "0.2.0") (deps (list (crate-dep (name "crypto-bigint") (req "^0.6.0-pre.12") (kind 0)) (crate-dep (name "oorandom") (req "^11") (default-features #t) (kind 0)))) (hash "1yspyxy1l7wkllfsf57ysjy07hc3z9g15l21ykccr00a2453iybb") (rust-version "1.76")))

