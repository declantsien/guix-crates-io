(define-module (crates-io cs i_) #:use-module (crates-io))

(define-public crate-csi_parser-0.1 (crate (name "csi_parser") (vers "0.1.0") (hash "11j4dv9whg5clsivvf0bx0bi0mkg5cp5m7y4n8b7x58l7w56xsld") (yanked #t)))

(define-public crate-csi_parser-0.1 (crate (name "csi_parser") (vers "0.1.1") (hash "0qpssnchiib8mq1w7w0vqvmn5hfsf76qyl1cr74hdhf3jpm784n6") (features (quote (("std") ("no_std") ("default" "std")))) (yanked #t)))

(define-public crate-csi_parser-0.1 (crate (name "csi_parser") (vers "0.1.2") (hash "07i876arfirb8dcmz4rlmp8xj572i17zd952hlslc1l2qa5lqmal") (features (quote (("std") ("no_std") ("default" "std")))) (yanked #t)))

(define-public crate-csi_parser-0.1 (crate (name "csi_parser") (vers "0.1.3") (hash "067ahm8c1mr2ji24kv29fxnhmxzpand31j11z79bkq555mfm7z2c") (features (quote (("std") ("no_std") ("default" "std"))))))

