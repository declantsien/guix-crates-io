(define-module (crates-io cs on) #:use-module (crates-io))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.0") (hash "0ajsy8yvilv3lqr7dx0zkclg7c7ca8h0d22920asn013l3dwh6zj")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.1") (hash "014hki7xmwjwgd0s9mlcr4kgplj87xlkc2a6d7wyjpvv03r5gja6")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.2") (hash "15pvgggb5id3qj7sf871dblh3zh7i8d7fg54bigqmh3kkbfwnhns")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.3") (hash "01lf756zkgrjb8y9c27lx64iqwh329w8mymp4scyldhhgabcbyl4")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.4") (hash "1gdphm41zq3lb1fjplgry72n3xp9d806ixq3brckp88hp0hr2wj7")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.5") (hash "1hwml7ahk6k1zg8rrsi8plyz2m3dbb40713a87fyjh3ad9nqi3nj")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.6") (hash "0wl59arkklhbfnwm7xqgxyr6bb53kxg5qpxjfc792hrfd93d2qq3")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.7") (hash "153fhz8phrvmvwdns3vl95wcbl1mgp8ndllj9f0cm974y7493q45")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.8") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0s3vczgg5ry2x7f632bkcpsr3mxy3nrnk2hgb61z6fzxxrp4bc7r")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.9") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "09mfljbj1rmxqf8pppcqi4fyd5k9s64hkhiw4xzb95nc7rqci4mm")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.10") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "024zl478ibv4lhaf5qrsi803iaiqbqgg0bzb13qgr5x8j6pigbbr")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.11") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "07d3jlghpir8gfian933qlh06pz378vdx9daglbj97kvxb0k7y8v")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.12") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0d1gp8hxjwwq6kqhmci6gakk371axrcw5s0x5kbkb24wjd76011i")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.13") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0dbmfp4x56h7vfgf8aq7yixsl31hn6ip5jqrvg0p7c96dmzynhm6")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.14") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "08nmzcml8pag0mpb1xpk65fgbx9vil7hm96353j0rb9wq2vf092z")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.15") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0sk0bifxd95905kqk1wk2inpm9s5rn507l10rsq6kxjgibyrpa04")))

(define-public crate-cson-0.1 (crate (name "cson") (vers "0.1.16") (deps (list (crate-dep (name "rustc-serialize") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "084dx19l8in1p6fkxi6c7zy34m01pcvbf2gpqyfz0sdi5h9k8fva")))

