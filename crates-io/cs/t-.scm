(define-module (crates-io cs t-) #:use-module (crates-io))

(define-public crate-cst-locks-0.1 (crate (name "cst-locks") (vers "0.1.0") (deps (list (crate-dep (name "asc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)))) (hash "13jncy06as9gv6vn9azdqnrpl6z20i8413w4l79v9fxl24j6wr6s") (features (quote (("default" "asc" "parking_lot")))) (yanked #t)))

