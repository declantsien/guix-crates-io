(define-module (crates-io cs -s) #:use-module (crates-io))

(define-public crate-cs-string-rw-0.1 (crate (name "cs-string-rw") (vers "0.1.0") (hash "0iddrpw4x84bmqy6acvs807vlgqpfvfshx7j9rv6vdhhav395qqj")))

(define-public crate-cs-string-rw-0.2 (crate (name "cs-string-rw") (vers "0.2.0") (hash "0i5cbb1qgyjikqs189c594bf74vgrq8zrc50lvf0zf2xmnz7l3wa")))

(define-public crate-cs-string-rw-0.2 (crate (name "cs-string-rw") (vers "0.2.1") (hash "1hggw5jsgsp4qj2r1yavs3rwnjhviwwm4zr6nhi2vl6rkykdp3ir")))

(define-public crate-cs-string-rw-1 (crate (name "cs-string-rw") (vers "1.0.0") (hash "0xb2hibz0762a1dq3w2p8axfl4h726l0c0ym19sr2i2h1s1ihm8p")))

