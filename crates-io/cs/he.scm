(define-module (crates-io cs he) #:use-module (crates-io))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.0") (hash "1wzgf1a10bv89f3m3ggvvd805xzqw5f394lpqh5qf0bnm726rzx8") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.1") (hash "0a5m8hhs7rw3h67fx31qbv4bzwgx569a6wal7kz9xas4xfbz5wza") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.3") (hash "061awmyil83403ysz1y9jy3nrjp6h4ch69wj0819dzpyc0gw0kk5") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.4") (hash "09m8hm9hpy74nklma4df9rgcdj9azvq86x41f0ganczs8c5hzbp2") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.5") (hash "1im2kkxc7bsrbxkl7r7qfbwp5saynjh0qkrn6kr2sivxxshc12m9") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.6") (hash "01nhbmk8dsv90sb1p2pdvi85nqvj341dxn7j6hi7w3vwz379g33p") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.7") (hash "0q7lbg71z76cfknj7kq7193138nn3pk01vhfj03qc4ppk1rnk2sk") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0rq391a80bfzramlr1i67qgi5kk4h23yl9nhyaycqf0gss3wg1az") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.9") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1i21kdvyhxyfcs53qirvl307b5c6xh7hx75k0amrvj2w68n17ydv") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.10") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "16jbvbf4rf900593ln7k2846qlhbvhkirxrv2agjl5jl9093bngx") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.11") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1wllj696gn3am868v7kjabp98l4lxklz41mwpri3v9x7951am7cs") (yanked #t)))

(define-public crate-csheap-0.1 (crate (name "csheap") (vers "0.1.12") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1mccjq8q2w0x6mwdsdl74aa0gp0ci826ns79k3qc1jimw0ils09a")))

