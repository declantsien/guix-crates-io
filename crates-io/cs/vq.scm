(define-module (crates-io cs vq) #:use-module (crates-io))

(define-public crate-csvql-0.1 (crate (name "csvql") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "00inrp5a6bxxyyfhmb083yw958miy5r9vjkjzgq8f38f1yrvqq0l")))

(define-public crate-csvql-0.1 (crate (name "csvql") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "16d7c210plx4p83ca013qjnyx73wpc07wva9y11kbmfrn200lchg")))

(define-public crate-csvquery-0.1 (crate (name "csvquery") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "19lxw952ma1c9vfqvh5xya2wg2vq857fx7vhzx6k67anl9dyf041")))

(define-public crate-csvquery-0.2 (crate (name "csvquery") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0q11gm6jhxswpr1g025706n1v7s8sgla1lf640hkqqg7z542145z")))

(define-public crate-csvquery-0.3 (crate (name "csvquery") (vers "0.3.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0fdn873zjlg2kvrqvmm4vd691d8rjj83m47jbvn0rar766baxsji")))

(define-public crate-csvquery-0.3 (crate (name "csvquery") (vers "0.3.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "047g87md2l0g1qspgssgqgq96gy0ldshr416cwrzqihwsmckg2gf")))

