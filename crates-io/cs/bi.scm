(define-module (crates-io cs bi) #:use-module (crates-io))

(define-public crate-csbindgen-0.1 (crate (name "csbindgen") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "092kzbw6pr5b9ybj275p2pjjmb8v09vi9xfmyiiy5a7z0xwfyvc4")))

(define-public crate-csbindgen-0.1 (crate (name "csbindgen") (vers "0.1.1") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qbv71dcf2mmi1hw3772sxcs02phqldbyiapdfxdlv4a5ypwwfv3")))

(define-public crate-csbindgen-0.1 (crate (name "csbindgen") (vers "0.1.2") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12w85zwicjfdmif83d34dr5xdkdybpg2cdgdbkiq7d0645dsd7yw")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.0.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lbksrk6br85jxs4g9xp76h08yzdffixglsfwiam2wisxp4j9gyx")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.1.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09dih52wpbrbizhvnnw6bf5zipjfg1j5gf09m561wzk1mlhqji4g")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.2.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fmrhcixbvaqs0rrzbbbvw3g8adjqzyyxiadx6r63v5jpqlg9n3x")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.3.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qd0pnghpim1vss0nkw0gpks63d98crnqnfhas33b1j9ryxqi5wc")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.4.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bml5c03gpnjvnkc5h6p2nd7crrnq54fyv2sabmgkyzax5lg75s6")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.5.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "080hpis1j6c7198jgr1mlx5j7gzw0xy59drwvczlylm0g6sllffm")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.6.0") (deps (list (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xrcz83srp6vqc35zsi6k6ikndscb63ha4rds2aylcb590c3siiq")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.7.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ikzzhwwv8y9hd4nix73yi2rjf81dlni2d55nky688q868dd7zpj")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.7.1") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1l4z1afdi5hfv6kaav1cqk81n3zjc50rqp3khb9s1jfvsm6ig42h")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.7.2") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n2v45rrh9n1g52kci4zwaagplvckp771yd3fdcrm2fkl679zjhn")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.7.3") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15l10l2w85fv5qbvqanf82i3d0241wc7j00iqs9x10lk2fksdpy0")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.7.4") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v71vqd9abjmsvarhlisg8g9030558jnjxdgzl7dq56dpgfdghqc")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.7.5") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "097y8waq725hbiad28i3bsl1g4g4aqrv8439dqc15vb66nx3jlib")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.8.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i7mmnavhccjmlzwv6mq636ys332npxx7r36xygpfi6jila8jhzc")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.8.1") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l53h8abhsmwfw4k7k3a3j357kb0y1vx34fiz7b3i22mbnni2y2a")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.8.2") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qa9j5jrfqkfc0ma1gghlsqd991rifwq580i50nfj9f7iyqv2iw7")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.8.3") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09wvh3zlqinvrdi8qyimvyqpx6s8faqf44kml59lgpkaw992fbyz")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.9.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11dkszhpmi0r01i5cz4x77601p4m02an1bha1z91kq9mmrnp4j68")))

(define-public crate-csbindgen-1 (crate (name "csbindgen") (vers "1.9.1") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "173sfqjibiwrj1qqjd428sjxhlqw8dk1rqxxdjaydq1mdxjynw6g")))

(define-public crate-csbinding_generator-0.5 (crate (name "csbinding_generator") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csharp_binder") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1djbl3gdba22gb88af9831y6nqxaapdj8m5gy4jx1h4dmlz369l6")))

