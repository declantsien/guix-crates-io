(define-module (crates-io cs -c) #:use-module (crates-io))

(define-public crate-cs-cli-3 (crate (name "cs-cli") (vers "3.0.1") (deps (list (crate-dep (name "chainseeker") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.6") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)))) (hash "1c2xdfpc8sdbyjv564g6zlwpnbs7z7ybwnvq25c9iyfsll4q6h14")))

