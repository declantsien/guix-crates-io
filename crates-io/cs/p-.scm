(define-module (crates-io cs p-) #:use-module (crates-io))

(define-public crate-csp-hashes-0.0.1 (crate (name "csp-hashes") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "01lrgnkwm2zn86iad0jyfq3ai6l5jd5i01ag5frljrnqbd4x87rb")))

(define-public crate-csp-hashes-0.0.2 (crate (name "csp-hashes") (vers "0.0.2") (deps (list (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1yszzskhc7mlv1aj2lgss41s551254d43bx6cwmqf17i7482bjva")))

(define-public crate-csp-hashes-0.0.3 (crate (name "csp-hashes") (vers "0.0.3") (deps (list (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1cds1h4i7bfqg69zb4ihn2q8j2r85plfp5wnz06djxzww6igy4np")))

(define-public crate-csp-hashes-0.0.4 (crate (name "csp-hashes") (vers "0.0.4") (deps (list (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0wmks52ach6j35cfiqhynypmhmnli3hwnsg4fdjdky2i63kzg9gq")))

