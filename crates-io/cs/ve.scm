(define-module (crates-io cs ve) #:use-module (crates-io))

(define-public crate-csvenum-0.1 (crate (name "csvenum") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1k2ynlgqvaafspal6s0bs32jdklhqw0s1xa7gx51k5lw7khbzbyp")))

(define-public crate-csvenum-0.1 (crate (name "csvenum") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1lg83zjifi2355kmichcnwrzalrqd34rd3nraizawd04d1yakp2s")))

(define-public crate-csvenum-0.1 (crate (name "csvenum") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "07j6046rm9yphmymjs2j4cm9w8k580lm0nfw8frjnh8zqr8rv42q")))

(define-public crate-csvenum-0.1 (crate (name "csvenum") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1f9fi081ir8jd94rxyfmvbfg8jxnx9jg41bf0jb6bmabvg80ns4j")))

(define-public crate-csvenum-0.1 (crate (name "csvenum") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1h52jliahfkfp4racvirqapkslmgzfhz4w62pswjcsckh06k2364")))

