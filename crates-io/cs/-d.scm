(define-module (crates-io cs -d) #:use-module (crates-io))

(define-public crate-cs-datetime-parse-1 (crate (name "cs-datetime-parse") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.34") (features (quote ("macros" "local-offset"))) (default-features #t) (kind 0)))) (hash "0vwc1wi29z3qjh76llc9vdaqd0g0a1801zhs8sj0vs2wmk9hrv8b") (v 2) (features2 (quote (("serde" "dep:serde" "time/serde"))))))

(define-public crate-cs-datetime-parse-1 (crate (name "cs-datetime-parse") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.34") (features (quote ("macros" "local-offset"))) (default-features #t) (kind 0)))) (hash "1042gmsl2d3wr0xbj75iq6hjin73df2whg6sd5369p8bh0rz5xmb") (v 2) (features2 (quote (("serde" "dep:serde" "time/serde"))))))

