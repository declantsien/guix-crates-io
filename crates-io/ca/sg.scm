(define-module (crates-io ca sg) #:use-module (crates-io))

(define-public crate-casgen-0.1 (crate (name "casgen") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16991944rkf3vqay380xbryiwagv2jxs25bsdb1p73zfpbyqwfmj")))

(define-public crate-casgen-0.1 (crate (name "casgen") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wf92869ribb88g1w8wrmbgcp35sbhvm2ni190yvsfdkgnjys80q")))

(define-public crate-casgen-0.1 (crate (name "casgen") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1cw8y1h5082gx43cx99bjls46sr1jm3vfvx8gij1pd8sl9kgprxg")))

(define-public crate-casgen-0.1 (crate (name "casgen") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1cdfnda17j8kawghvdkx17my5pg9k9r3n7bhz4ly7n5xalv85a4r")))

