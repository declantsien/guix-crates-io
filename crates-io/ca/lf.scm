(define-module (crates-io ca lf) #:use-module (crates-io))

(define-public crate-calf-vec-0.1 (crate (name "calf-vec") (vers "0.1.0-alpha") (hash "0q13crsklpg0vpz8f7dpg00na4gvqgf072dsnyd9xi58ivqx0wah")))

(define-public crate-calf-vec-0.2 (crate (name "calf-vec") (vers "0.2.0-alpha") (hash "0rbiwaqgznjdf3pjlbmcsnak9byyf314nj5q70ccallq4nlnyr2d")))

(define-public crate-calf-vec-0.3 (crate (name "calf-vec") (vers "0.3.0-beta") (hash "10nwh3bqzvpcm7b5b3ly57byizw7phywl1y3p6j2q5lyba20gixa")))

(define-public crate-calf-vec-0.3 (crate (name "calf-vec") (vers "0.3.1-beta") (hash "1bp924977j3k8852w4an6p6vgmd6xzcjic9pamdqcjm3z4b67r31")))

(define-public crate-calfsay-0.1 (crate (name "calfsay") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1559appg10dsb633iwlczpr1wiwzilcq1b30ax7b3ifh79dvlf05")))

(define-public crate-calfsay-0.1 (crate (name "calfsay") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0lsq8w7hxapah3ckh8faf7y576b069bsxnglx2gjpxalzaqfdlag")))

(define-public crate-calfsay-0.1 (crate (name "calfsay") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0bfsxxkkk2ngzh6frkdpf7aqlzjadq4qnmpbxys98q1ldap9fwkp")))

(define-public crate-calfsay-0.1 (crate (name "calfsay") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0zhafqsbh1s7cyv7naq019ff19i0rs36vn4wmfawvh39qqjpbpaz")))

