(define-module (crates-io ca p1) #:use-module (crates-io))

(define-public crate-cap1xxx-0.1 (crate (name "cap1xxx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "02gpkx1j1yg9r98fmc9dslpj1b1m0bmrcca9wrx37lfxvd9mjj1a")))

