(define-module (crates-io ca r_) #:use-module (crates-io))

(define-public crate-car_registration-0.1 (crate (name "car_registration") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "~0.6.0") (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m5awbmvzydwc3m1lh2lhhmgmgks5vw6j6q1y5n9mw5n9mlgiii1")))

