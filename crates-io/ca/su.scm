(define-module (crates-io ca su) #:use-module (crates-io))

(define-public crate-casual-0.1 (crate (name "casual") (vers "0.1.0") (hash "1g09vih417krbn8vqz4b7abx813i4fvj83p62xz0s3gnn8wj0w7b")))

(define-public crate-casual-0.1 (crate (name "casual") (vers "0.1.1") (hash "14gvbw1nbxxjjp9js27kliib4857dvg2mf19b055nwjqi760wawv")))

(define-public crate-casual-0.1 (crate (name "casual") (vers "0.1.2") (hash "047i78v3whw2fmqdvpn5lprwjkk2ck307sbv3y5zmdpnczq75061")))

(define-public crate-casual-0.2 (crate (name "casual") (vers "0.2.0") (hash "081l7kxfxpdss7ln0x3c5b71hzv4h5l6c8z33zbvny0y2w6w1fh2")))

(define-public crate-casual_logger-0.1 (crate (name "casual_logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0xv4d0fzhkzz0qdry0lppm0nzwlfpbqlgg9p7w20mhhmvlfx1f0j")))

(define-public crate-casual_logger-0.1 (crate (name "casual_logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1w76wl6i0z047yq062lnj4vzb8x31bkrmdrwsqsf7l4wv2b2wihn")))

(define-public crate-casual_logger-0.1 (crate (name "casual_logger") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "09zddxlr7i3rrhyws95jn8hilg0xc0ryc9d6danb5aj417i97jhr")))

(define-public crate-casual_logger-0.1 (crate (name "casual_logger") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0755whncs9xjm50qr3nlb5zslhgc8q46zly31x50prqd9bg3aapn")))

(define-public crate-casual_logger-0.1 (crate (name "casual_logger") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1fby65npy2z094cm86bwrc3wzc84psk4dr016w4qzvgjw0zc0bar")))

(define-public crate-casual_logger-0.1 (crate (name "casual_logger") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17x0lrj6axfqbbsb53y9n4vn5fi0lgbd4zkiaqx10flsqq9nps6s")))

(define-public crate-casual_logger-0.1 (crate (name "casual_logger") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0w8c1wmxa3fw7ws9sycjhv9g0sh0v41iqb2ckjk0shg7bdg9xnyy")))

(define-public crate-casual_logger-0.2 (crate (name "casual_logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0bms6phcimwhng3myhp5gazlns80hfjkhbzhywpqf1kr300d92aq")))

(define-public crate-casual_logger-0.2 (crate (name "casual_logger") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1aa136mhz8vayl7lnzldfi8ms0rc1zvkbb0wm6873blsmv3jccal")))

(define-public crate-casual_logger-0.2 (crate (name "casual_logger") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "13jmgv9lz11jnraijk1ccdzs12nlh5mncm9a0s5c15c5vybph4qd")))

(define-public crate-casual_logger-0.2 (crate (name "casual_logger") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "11wkl5hih44vm5hy7yl77x41a9hgph8aw1vriyc71mbp2icxhppq")))

(define-public crate-casual_logger-0.2 (crate (name "casual_logger") (vers "0.2.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ffh30s5npzn0a7g35yi1x1arnzb8886272fslwz3vl4a59zmmdf")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0r9k7kjdni9kv72lgiaval6hnn3jqp1fql648ad8ndihnr26sn5x")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "18ynrzimmqkh4hj1mnnqzgi23iqph08m1g7gsq6xl9h71b6w3x0w")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "116krl6jn0d782am1yrdm0vrnrkp6wx7c5758z1isyca2l3l07ii")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1dzmbyllnzval2wm8k5a2bx2c93i6gmp3zzp5fs2flmnszcdnzqg")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0yk00zfsy1wzx000yk89kh06fadj1n0m7p0ig5jizj9ynyk4hmyx")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "03076w7376fig0wikkscqyrh5lqbyp15dxznw9k5p3niqp24lzai")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0gvg2a4bp9m8b0lps81b4dzw1cyh4dlmnmw794jxfa3n3r1hvs62")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1k0n3m0ld8a1xbs2yf1j91ci6kwfnivshr6qi0cwp3f174ccbl4l")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.8") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0ylhd7rf8p7z6ppviqimhv53d7mbmsfv5vcwncm16g2hhy2ir15h")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.9") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0m698y3pb0sgwvd9mnnpjncqiphlc2r39igqjycmm6zrrlw812yz")))

(define-public crate-casual_logger-0.3 (crate (name "casual_logger") (vers "0.3.10") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1x8xw3pwa99g4d2z46556rqs58bzazr2dp1jd25r33vab3r080f3")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "153rq4alhbvvx6hc3pqfnl12a9icx0i0r74rrqx1d38aq77b13iv")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0npx0kp5ak8gk6y7s44x5z18n3vp3jvd4m2mlcbpivdcwzjbxcjk")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1g20d1vd00c84jbglmhm36alfr0mgbk5babsgmdmgpbd77b9vall")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12g1knjz50274kw7bmjlfb017ps4xz6csffcgzkmsjk7yv4wvv1f")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0lcx6053x9wzc7gp1f5ib8vw2harjsf2ly3xixm8z383w1hpjajm")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "19j0zabd52fgvifhy6rvq005a58h4171fni7k2wgd3ji1cj4sfqf")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17shh0057pa9w8fs1cp3pgqnkdfnc2r3d9ckn9xrvvibf1qprv19")))

(define-public crate-casual_logger-0.4 (crate (name "casual_logger") (vers "0.4.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0m6x1r59qlsa90i1q5bikqr43i1q5lw4iags8k5sn52w88grkay2")))

(define-public crate-casual_logger-0.5 (crate (name "casual_logger") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0kd0506q09439jv067gf828ni4l66kqrmd3hsrgikzn1slbqdf0z")))

(define-public crate-casual_logger-0.5 (crate (name "casual_logger") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "092bi71f5wrli7kdj3nq70b2bcy9cfz9wj5j6npqx5qwq4yns98m")))

(define-public crate-casual_logger-0.5 (crate (name "casual_logger") (vers "0.5.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1v54psy7xxw9kibhlmsplqzb2i6gm1x1ra0xh43yids8a6c1mk9p")))

(define-public crate-casual_logger-0.5 (crate (name "casual_logger") (vers "0.5.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0maxa6449hlp4f7cnkxf5gpvyp932pybc58y2bvjy8nvd9xc7vbj")))

(define-public crate-casual_logger-0.5 (crate (name "casual_logger") (vers "0.5.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "09gmhaxa1kiadpmwbsi4nxf68zla9b8dy551lh52d8061vs2d36f")))

(define-public crate-casual_logger-0.5 (crate (name "casual_logger") (vers "0.5.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xv6d77zwpbfshfh2b2szn2y6xzkzziml9rxs0vcm8w5zbx88ryb")))

(define-public crate-casual_logger-0.5 (crate (name "casual_logger") (vers "0.5.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "04dxcb6whaf17pv6l8zq3a2y9p2fwwsjizkc2b4j1v2713xbs1g3")))

(define-public crate-casual_logger-0.6 (crate (name "casual_logger") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1qgzn5di2a8ay5gnsxg0f9f41nvkvb02gphsaf9j50xjkcvbkq95")))

(define-public crate-casual_logger-0.6 (crate (name "casual_logger") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1l014z6723pn3kmv9qk4rmlzn92yadhqk4yp416fdhb4hzwmilsj")))

(define-public crate-casual_logger-0.6 (crate (name "casual_logger") (vers "0.6.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02dvwaffgxgg0y3bw6cqx3qh9p9bsddjs1fwwpam37s04s3m2y3b")))

(define-public crate-casual_logger-0.6 (crate (name "casual_logger") (vers "0.6.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1z6zacyff0g5xxciylkbh042j34zfwmvgfjza3gq2cjgiasdkkla")))

(define-public crate-casual_logger-0.6 (crate (name "casual_logger") (vers "0.6.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17scn8kf2dlkvgjgjln556g32la7czkq2i09rfsmn422l1i0xmyc")))

(define-public crate-casual_logger-0.6 (crate (name "casual_logger") (vers "0.6.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "177mcki9aw5b6vxyij7p1hshdqj0r0s5d09jlbqbfa2k08pjpl3p")))

(define-public crate-casuarius-0.1 (crate (name "casuarius") (vers "0.1.0") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "0liqffz0fpimwbh40k5ihv1wgblc5ksfy0x39hgqf8p3mi64j0qr")))

(define-public crate-casuarius-0.1 (crate (name "casuarius") (vers "0.1.1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (default-features #t) (kind 0)))) (hash "1nfh53l0dgnsn5q0y8wi5ni192fcpilil29k44mjxkzcdbz3dxqd")))

(define-public crate-casus-0.1 (crate (name "casus") (vers "0.1.0") (hash "06x0fj8xl7xfrfxijib3i6zbwj6bmwcs5jbhn4xmbgm8fpglzmpr")))

