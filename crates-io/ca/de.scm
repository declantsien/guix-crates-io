(define-module (crates-io ca de) #:use-module (crates-io))

(define-public crate-cadelo-0.0.1 (crate (name "cadelo") (vers "0.0.1") (deps (list (crate-dep (name "ascii") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "kerbeiros") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.7") (default-features #t) (kind 0)))) (hash "1zja2hffsgkkr1x3cqzplpx1lgfzpciixb7538njfnrcg1cpdsfc")))

(define-public crate-cadence-0.1 (crate (name "cadence") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "08kzm823gc06wnrfd0h46sgzp4xdcf6z9fnnrj2xqr68rrdr5i0w")))

(define-public crate-cadence-0.2 (crate (name "cadence") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0x5a3p0jc45020by7xn1znznmgl3r556xw5zxqai6l4grdqsb0ls")))

(define-public crate-cadence-0.2 (crate (name "cadence") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1aaggp7bvipvg0h45wlxd4a6w0al3s97lydkryqhmm5463rws2n5")))

(define-public crate-cadence-0.2 (crate (name "cadence") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1sy9bhb3hycsdd2h5205sbh2gqqwiawx55x3g36si4170b2s0zy7") (yanked #t)))

(define-public crate-cadence-0.3 (crate (name "cadence") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "10c33vr78ghhpfcbr4xbgifyv190zidfr6xdwshy42kc7ij5lm4h")))

(define-public crate-cadence-0.4 (crate (name "cadence") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qhn7c3zms60jzfly19kn6rapj8pl5v2dfg558p5v4n1d1k02gir")))

(define-public crate-cadence-0.5 (crate (name "cadence") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1n365bhlg11r7hyf0mzp0m3llm2f4mwfl1y87dwpmiir1ik64nxz")))

(define-public crate-cadence-0.5 (crate (name "cadence") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dph8llsamwz4fqfqfpjb1nymx7865r099gv97apl39rmwfjz58k")))

(define-public crate-cadence-0.5 (crate (name "cadence") (vers "0.5.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kjd5zhpdq4s7pr4fyxg6anp8xc5cmq9n9fgd50gs611l1sqlmdw")))

(define-public crate-cadence-0.6 (crate (name "cadence") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0f73s8xlv0pflw19ywq7mggi2y0an747zz4cnnpaz8rccdf4kgbn")))

(define-public crate-cadence-0.7 (crate (name "cadence") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0g3ijdh8y74mq8f4bfxq309jwr52d92vgig557csih9dgn2jgz06")))

(define-public crate-cadence-0.8 (crate (name "cadence") (vers "0.8.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1qvsaz20ch15s2pxlaa7m96ngdagjil2yfv34w90z4792vn21ccp")))

(define-public crate-cadence-0.8 (crate (name "cadence") (vers "0.8.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "08qnmvzg98hl67xsjlnkbw8xvfpjkrbgcpada523sf2h7yzxbqg4")))

(define-public crate-cadence-0.8 (crate (name "cadence") (vers "0.8.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0sci8qr1rg6rhp84jvb9awsjg2803d758nfpb4yy4l4iyp9l1yxl")))

(define-public crate-cadence-0.9 (crate (name "cadence") (vers "0.9.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0lm0njs10rff2z539410v1fwng56z0qi9d67gqrcdzv3kr7702vn")))

(define-public crate-cadence-0.9 (crate (name "cadence") (vers "0.9.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "034xs089p52ifl2ka424k7xqcgpyvxkm1lvzxsj16yaskzscbbia")))

(define-public crate-cadence-0.10 (crate (name "cadence") (vers "0.10.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "01jrsznvv7hy33wgzhcygg8h3nrgwaz13xfrlp0yhl0xyid6svbw")))

(define-public crate-cadence-0.11 (crate (name "cadence") (vers "0.11.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "19cmhzy62lfkfh0y2kpg0xnd0ngw8zv83r2s4195wrwr07qz63ys")))

(define-public crate-cadence-0.12 (crate (name "cadence") (vers "0.12.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0n27qq5yi85rl32d7jnd7k32hn9myi59f8fk4ryhmcfvgq84lrm2")))

(define-public crate-cadence-0.12 (crate (name "cadence") (vers "0.12.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "13n80aa938glz3xarcm3a6kclasdy3fs4bqlbhkwj6rjnarkv3dl")))

(define-public crate-cadence-0.12 (crate (name "cadence") (vers "0.12.2") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1prs1p5rcx64gmm4vc5l5qwa85lbjfz24c9qd8bbv5j6id4870bv")))

(define-public crate-cadence-0.13 (crate (name "cadence") (vers "0.13.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0gcmk6q9lanrw0xid6rnj3yry8h9mb2m1q3b3bbwspg6y5sx8zm2")))

(define-public crate-cadence-0.13 (crate (name "cadence") (vers "0.13.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0cwa92jg01vzvjyh7jhg05429s9mwr4ps86py4k1f9f8spcmlwpi")))

(define-public crate-cadence-0.13 (crate (name "cadence") (vers "0.13.2") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "137q7ac6a0vshhy1hcr0hq7a2mgd1rzy3s55h4yszz8fl3h2qqcr")))

(define-public crate-cadence-0.14 (crate (name "cadence") (vers "0.14.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1850likmckn98w918s8aj6rb22w0pa41fbcqp5khqaigps5rhi7q")))

(define-public crate-cadence-0.15 (crate (name "cadence") (vers "0.15.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "115b60gfdfyd2ish9ffgi4xxwcp2nc58lbb82gqr8pvxw9zdagbb")))

(define-public crate-cadence-0.15 (crate (name "cadence") (vers "0.15.1") (deps (list (crate-dep (name "crossbeam") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1hl77cajlhpcjx83w13svkg2srq9s0k5s5xq70w7w6hg6a56hajk")))

(define-public crate-cadence-0.16 (crate (name "cadence") (vers "0.16.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0h1ai68v5jynaax6d1fk1sm296cjdyh1qvaihv09l15lnlzv8w1r")))

(define-public crate-cadence-0.17 (crate (name "cadence") (vers "0.17.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "17nrikymgcmsbqd2znlmpqx8m8wyf85sfagwhkw0s1qg6bfaqybr") (yanked #t)))

(define-public crate-cadence-0.17 (crate (name "cadence") (vers "0.17.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1w3xkijpcssyl33m21w920qvnm1avlp363bamk6qyb99ps768g3p")))

(define-public crate-cadence-0.18 (crate (name "cadence") (vers "0.18.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0hyyi848vnj396a1fc3fc2qj4qg79492y2xj2gyh4cd44sijm21c")))

(define-public crate-cadence-0.19 (crate (name "cadence") (vers "0.19.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0rq92g1slij3daqxbcg869vpmq0fl151rmyn39z4mvmqxhzdvkqg")))

(define-public crate-cadence-0.19 (crate (name "cadence") (vers "0.19.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1ny0mk6bzzigbhmq7c6c2ahmfc2vidbv16mbjq46ba5k20hq1j2c")))

(define-public crate-cadence-0.20 (crate (name "cadence") (vers "0.20.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0jp9rm08a9x30pc1fai415hdizc7yfd3fpvzy2bgq8kzz0zm4mqw")))

(define-public crate-cadence-0.21 (crate (name "cadence") (vers "0.21.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1qrwk3hyrs1yydfrd5nvr7wjgyfkbvzbh1bm6slww3w6ks6zhmg2")))

(define-public crate-cadence-0.21 (crate (name "cadence") (vers "0.21.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "104ivx5pwkfrcwj5s4f54jlwic06bls4rxwagy3v1pfdfpgrqnz4")))

(define-public crate-cadence-0.22 (crate (name "cadence") (vers "0.22.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0pp3anjqdj7g3yzgq892cyhcv0vvq6hck6g8ig83yaf318hd30b2")))

(define-public crate-cadence-0.23 (crate (name "cadence") (vers "0.23.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "00wd31dnjvw4qg8l8wsvp603y1aqwmvgy26b164m02vnhpsq3q55")))

(define-public crate-cadence-0.24 (crate (name "cadence") (vers "0.24.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1h10mydd4q8fr0cb7xb3ivxpa6jrzxxkh5qqp68xkgwq0l5w6yp4")))

(define-public crate-cadence-0.25 (crate (name "cadence") (vers "0.25.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1lq6xr7q9ap7545sh51jbxf79iy0nmvdx9731yv5j59cc593jzw4")))

(define-public crate-cadence-0.26 (crate (name "cadence") (vers "0.26.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0cp0gp2bmsxg1w5jsvxkakhnh2lqik8s1qqw6l3k8xpzgxrmns57")))

(define-public crate-cadence-0.27 (crate (name "cadence") (vers "0.27.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1d9jqzas11dcsxcngh84g7idf2zkqxmpzf81kxzdyjhh0vbkinis")))

(define-public crate-cadence-0.28 (crate (name "cadence") (vers "0.28.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "04ypl4d32774lhqcykjp1lmwy7hvv5yh4islsjvpb25gd5n7n4yd")))

(define-public crate-cadence-0.29 (crate (name "cadence") (vers "0.29.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "06as10gk9lhhcnbva63mb36618iyj50f6f36rffrs77j11s1ls7b")))

(define-public crate-cadence-0.29 (crate (name "cadence") (vers "0.29.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0ba8m4lyvxxgqbhfvymqd33j2hikl5b99dydvh0k20jv0yy8d4pk")))

(define-public crate-cadence-1 (crate (name "cadence") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "189fvpk0rnaiyw4k4b1anq2a3lr1nj0hn42mx2mrf82hkxsimdga")))

(define-public crate-cadence-1 (crate (name "cadence") (vers "1.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0qc7zvc14nawy47gmgr28whhlf76xhhz9p85dr2s64b92bi3m4ji")))

(define-public crate-cadence-1 (crate (name "cadence") (vers "1.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "026vzwyr4w5m910aw3wnda9azmnn1d5m8pfdiqbi0m3ypmannas7")))

(define-public crate-cadence-1 (crate (name "cadence") (vers "1.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0ycxfxl90mv6kw0zwyc7ap54sfkk05mgn1z0cfg6anglsqksc4bn")))

(define-public crate-cadence-1 (crate (name "cadence") (vers "1.4.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0lrprfdcyavqwkr67a8wgxgn3hqdmzryh6l0p55zzgwyknbqncrg")))

(define-public crate-cadence-macb-0.1 (crate (name "cadence-macb") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "19xg513p0vwlyr3r6v6wihld65chwzc1ylqcn7ff7araq9clai2l")))

(define-public crate-cadence-macros-0.24 (crate (name "cadence-macros") (vers "0.24.0") (deps (list (crate-dep (name "cadence") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0kfp55ml49yyv3ssz6gjf916q597xdw3m01cdxxxvlr0n7pmzxgx") (yanked #t)))

(define-public crate-cadence-macros-0.25 (crate (name "cadence-macros") (vers "0.25.0") (deps (list (crate-dep (name "cadence") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "064snyarln5g7hc8gywg4mj3skg2p1vz9gj93m59z0bhxqrmvh49")))

(define-public crate-cadence-macros-0.26 (crate (name "cadence-macros") (vers "0.26.0") (deps (list (crate-dep (name "cadence") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "0rzfjs5r72zjlmbzff91rnjnkvlb1znngws50sfvn3lx1p9nj89h")))

(define-public crate-cadence-macros-0.27 (crate (name "cadence-macros") (vers "0.27.0") (deps (list (crate-dep (name "cadence") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "10fx0ir50nz7lbzyknwagl1a5mmyx365km5ilxg1fzzbfwl32ixm")))

(define-public crate-cadence-macros-0.28 (crate (name "cadence-macros") (vers "0.28.0") (deps (list (crate-dep (name "cadence") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0jxxcg5q38jaars9w3ckblylvy23q6k8ckh41xb0h2s3lw214jz9")))

(define-public crate-cadence-macros-0.29 (crate (name "cadence-macros") (vers "0.29.0") (deps (list (crate-dep (name "cadence") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1n6ha475r78g00hbqgxy85bq4wq871nzglykpbpjxlg7133v8way")))

(define-public crate-cadence-macros-0.29 (crate (name "cadence-macros") (vers "0.29.1") (deps (list (crate-dep (name "cadence") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1087qx7msmcjdqymn7fhg8vzcp13d5wnzd0029hxyad3r2ihg7qs")))

(define-public crate-cadence-macros-1 (crate (name "cadence-macros") (vers "1.0.0") (deps (list (crate-dep (name "cadence") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "190y704xfc64lv5xc9sy0mz5ds0vybr2hva5dplz7ci27czb8lln")))

(define-public crate-cadence-macros-1 (crate (name "cadence-macros") (vers "1.1.0") (deps (list (crate-dep (name "cadence") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "096jmj8k79w4x138vsyc1zkghhaalhiifz9kkrkdr16wr34cr2ji")))

(define-public crate-cadence-macros-1 (crate (name "cadence-macros") (vers "1.2.0") (deps (list (crate-dep (name "cadence") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0arbqypb8dmr75zdjvb96gl0vrwr5ywxiq687g1i94dpyswsdjik")))

(define-public crate-cadence-macros-1 (crate (name "cadence-macros") (vers "1.3.0") (deps (list (crate-dep (name "cadence") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1i16s2163ggsgv43bq94ywhh84ldlhy4vrkfxgk31rbw67vxchni")))

(define-public crate-cadence-macros-1 (crate (name "cadence-macros") (vers "1.4.0") (deps (list (crate-dep (name "cadence") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0kzc3jaxknx3nvq0chg6qzpwn5y5a1z6pxjk9fqxpggvsw7kc0k7")))

(define-public crate-cadence-with-flush-0.29 (crate (name "cadence-with-flush") (vers "0.29.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0lszgxl5y0fm7m0d6mkfw3caz26nssz88cqklxkjmmdmkj4mg7rs")))

(define-public crate-cadence-with-flush-0.29 (crate (name "cadence-with-flush") (vers "0.29.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "01m9svigpajxh4lvnnz8gvs4wxsc5g409633r2hidvdf5cscfz1b")))

(define-public crate-cadence_json-0.1 (crate (name "cadence_json") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "01yam1ky11kq104dr288qr0fhszmra40ki830da3zqp7pzd29qpa")))

(define-public crate-cadence_json-0.1 (crate (name "cadence_json") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_with") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "1vxssanfpzvg8xh6ha3wj4a1yvhwgi1972cnbqd2gfgvv3dc81kq")))

