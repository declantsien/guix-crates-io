(define-module (crates-io ca lp) #:use-module (crates-io))

(define-public crate-calpro-0.1 (crate (name "calpro") (vers "0.1.0") (hash "03pci4jkkl08kis8njia53kzlc4hc027qsnihjcagzdwdr6izh0z")))

(define-public crate-calpro-0.1 (crate (name "calpro") (vers "0.1.2") (hash "1m02k8d8k7s674iw7ddq7cns07nv98x0xz2rrbgmmbhaymv1m9xl")))

(define-public crate-calpro-0.1 (crate (name "calpro") (vers "0.1.3") (hash "1k3ivdjmzhqma5g4igvgcaij1in5nn96bvx8df5dmw2gbbglvx3s")))

(define-public crate-calpro-0.1 (crate (name "calpro") (vers "0.1.4") (hash "0iq2k60pkzqp53bgcnyqvgixqic371l4y00y6af1hpm3l548hi2d")))

(define-public crate-calpro-0.1 (crate (name "calpro") (vers "0.1.5") (hash "0mqczhh2zljj0bkf164xx528mv6v5741sg0x1jpwc9q76mr73wpf")))

(define-public crate-calpro-0.1 (crate (name "calpro") (vers "0.1.6") (hash "06p64syi200ygr9cffpysbclbrfp23lfkjzngal8i7xrpm1wxgjj")))

(define-public crate-calpro-0.1 (crate (name "calpro") (vers "0.1.7") (hash "1lf90jj46mfqsrsylpvfxz0c36kfir5g9jsryj2f80c1vyj4kq9q")))

(define-public crate-calpro-core-0.1 (crate (name "calpro-core") (vers "0.1.0") (hash "0jxy14bgdmmr5jkrc9yiwa61p0s75vr8297lxdwh4yf523mwqx01")))

(define-public crate-calpro-core-0.1 (crate (name "calpro-core") (vers "0.1.1") (hash "172d1djch0mdv2hfn9lf98q6rfxajnplfj13jray6whidvr4frrx")))

