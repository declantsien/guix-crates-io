(define-module (crates-io ca in) #:use-module (crates-io))

(define-public crate-cain-0.0.1 (crate (name "cain") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.84") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0yyy9aqs23rra9ndlmfvnrhdgvwxlgz62hn1pmbjx8wv2kcfg2g3")))

(define-public crate-cain-0.1 (crate (name "cain") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.84") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "14nxv30jm97lv5ga9fy13xa4dpbfxw06gwjm4ypzlf6j6knaiy7a")))

(define-public crate-cain-0.1 (crate (name "cain") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.84") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1ca5973ik5vpdgyavlbk6dfhxkxy0ab6shbv53irqcbsg6hf6s1a")))

(define-public crate-cain-0.1 (crate (name "cain") (vers "0.1.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.84") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0n499jbiwly2m04q3jj6526z3il87xnddcqnwcjvv9ppal19j7k6")))

(define-public crate-cain_ls-0.1 (crate (name "cain_ls") (vers "0.1.0") (hash "1h42h2dcm187bnipq2326g0m96q7alxn92gy4rs0grp1k44vayg6")))

(define-public crate-cain_ls-0.1 (crate (name "cain_ls") (vers "0.1.1") (hash "0jp0jsax54paifsjhjxbd03m7i6pccbh0mk5864grrmys4qq8va7")))

(define-public crate-cain_ls-0.1 (crate (name "cain_ls") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0cqswfcbjcxp7iprj4jp0mlflb6abxflrxj2dzcy8rsq67pdnalj")))

(define-public crate-cain_ls-0.1 (crate (name "cain_ls") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1lfsr82lr1fkwsb9qv3zac4y1wznd01p5b9ad7yylzzq7r8pairj")))

