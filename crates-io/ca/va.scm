(define-module (crates-io ca va) #:use-module (crates-io))

(define-public crate-cavalier_contours-0.1 (crate (name "cavalier_contours") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "static_aabb2d_index") (req "^0.4") (default-features #t) (kind 0)))) (hash "02v59aibahq3x7yi5pfvjwrp6faz9iq81h52byd143v48xismhv9") (features (quote (("allow_unsafe"))))))

(define-public crate-cavalier_contours-0.2 (crate (name "cavalier_contours") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_aabb2d_index") (req "^0.6") (default-features #t) (kind 0)))) (hash "07y804fbk8p74kmq0f81jlmcjp40gwpzqqwpyhzya18988mcca46") (features (quote (("allow_unsafe"))))))

(define-public crate-cavalier_contours-0.3 (crate (name "cavalier_contours") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_aabb2d_index") (req "^0.7") (default-features #t) (kind 0)))) (hash "1n65ppkd3ghifs52ybkqvbs22jh82cyyhfnvz0ldicixxki4hlip") (features (quote (("allow_unsafe"))))))

(define-public crate-cavalier_contours-0.4 (crate (name "cavalier_contours") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_aabb2d_index") (req "^2.0") (default-features #t) (kind 0)))) (hash "15wajjf9ss08h7cnvbiykamq0lylpk7syaldbsgca8m57nvi8vfr") (features (quote (("unsafe_optimizations" "static_aabb2d_index/unsafe_optimizations") ("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-cavalier_contours_ffi-0.1 (crate (name "cavalier_contours_ffi") (vers "0.1.0") (deps (list (crate-dep (name "cavalier_contours") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w28pbihbbi9kiwnhrmzsiw7rndacp6fsrdkpvh6av1xwln84zxz")))

(define-public crate-cavalier_contours_ffi-0.2 (crate (name "cavalier_contours_ffi") (vers "0.2.0") (deps (list (crate-dep (name "cavalier_contours") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gpl9zywr9fm64a6bwp0bvbdzphy57lrpd0c5wj7dr20djgyhgd1")))

(define-public crate-cavalier_contours_ffi-0.4 (crate (name "cavalier_contours_ffi") (vers "0.4.0") (deps (list (crate-dep (name "cavalier_contours") (req "^0.4") (default-features #t) (kind 0)))) (hash "1wm1f16sq6xwzal66n3a8pfg686kziniv8kpbbm4sycj6liiqs8f")))

