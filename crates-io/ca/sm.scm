(define-module (crates-io ca sm) #:use-module (crates-io))

(define-public crate-casm-0.0.0 (crate (name "casm") (vers "0.0.0") (hash "173dvx3iicadgm2fncsxcn29w1hssw5bicy7c5pjccrmcdn5h0xy") (yanked #t)))

(define-public crate-casm-be-0.0.0 (crate (name "casm-be") (vers "0.0.0") (hash "11yvg71zj0r8jg6r3zhvmcc9kv5daw7y6g0039sw989vgz1dxzhh") (yanked #t)))

(define-public crate-casm-fe-0.0.0 (crate (name "casm-fe") (vers "0.0.0") (hash "12kwpglxfi0rq5jc3cqa2ahys88w6licbns1cijxqcmrqfc8d3bc") (yanked #t)))

(define-public crate-casm-ir-0.0.0 (crate (name "casm-ir") (vers "0.0.0") (hash "0qy98van5h0m1v12m2pgwfmxy6ifl5ffiyn4sfx0gwkw0523pig7") (yanked #t)))

(define-public crate-casm-rt-0.0.0 (crate (name "casm-rt") (vers "0.0.0") (hash "0sarsdixmh4qzz638qlxd2qykskqljkf1d4a1blimp8b5c5ddkfp") (yanked #t)))

(define-public crate-casm-tc-0.0.0 (crate (name "casm-tc") (vers "0.0.0") (hash "1a6z2hk457l4wpmfckgd6zvchqzwsg0389fidx84rin90vy887hf") (yanked #t)))

(define-public crate-casmap-0.1 (crate (name "casmap") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "fxread") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1a6y60nxr00x1j7wzn4s5lcwdgi48sqs6lxqv57iqcmai5m6ir9v")))

(define-public crate-casmap-0.1 (crate (name "casmap") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "fxread") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0cbpgkav6yi5sjyj804is8n0hbvd2x7r8ly3ij0hxfrkk951czf6")))

(define-public crate-casmap-0.1 (crate (name "casmap") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "disambiseq") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "fxread") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0fxnnjw6b26641hik88hjr9d0hhn44yyl6arzdkys83snrgdgggv")))

(define-public crate-casmap-0.1 (crate (name "casmap") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "disambiseq") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "fxread") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0dyna0ap3v4ppvvv8qp1n57bl0i9bi2nkcw9rdjbrsizd19y26hl")))

(define-public crate-casmc-0.0.0 (crate (name "casmc") (vers "0.0.0") (hash "0b2hr4mg43glc9wj7kzi42chx9686b75jbd6zfvvymvi7b1x1pd5") (yanked #t)))

(define-public crate-casmd-0.0.0 (crate (name "casmd") (vers "0.0.0") (hash "0pl4g5az04l1q5scdb9p5lvq7v062gk04af4g14kyrqawqx4a61f") (yanked #t)))

(define-public crate-casmf-0.0.0 (crate (name "casmf") (vers "0.0.0") (hash "0zifqscsmijkq8sanxnhb534zl5xp0p6yshjl0zj4dz576pbbwhf") (yanked #t)))

(define-public crate-casmi-0.0.0 (crate (name "casmi") (vers "0.0.0") (hash "13sas7chkp14aqn0hblh4n05jvj4f3gfshd2cvlgafnpk4yj168m") (yanked #t)))

