(define-module (crates-io ca so) #:use-module (crates-io))

(define-public crate-caso-0.1 (crate (name "caso") (vers "0.1.0") (deps (list (crate-dep (name "avalog") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "piston_meta") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0gvxyb66q3dlp3fgv2ikaz72z1vgqsysxyb7m71lk8aihk081sv1")))

(define-public crate-caso-0.2 (crate (name "caso") (vers "0.2.0") (deps (list (crate-dep (name "avalog") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "piston_meta") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0n8nv13l0bmyz3jiqwdy3140nz1nywsd8z8hm29hxsh8y7dvb4wi")))

(define-public crate-caso-0.2 (crate (name "caso") (vers "0.2.1") (deps (list (crate-dep (name "avalog") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "piston_meta") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1vwm1x8wp3qgxkv7cwv6rd316534c2zh5wvv6dzrv38yard3wvsq")))

(define-public crate-caso-0.2 (crate (name "caso") (vers "0.2.2") (deps (list (crate-dep (name "avalog") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "piston_meta") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "072frags4xbhqywcgncggvjgq13kljj99acdcq8m5f5wqyzksnf6")))

(define-public crate-casol-0.1 (crate (name "casol") (vers "0.1.0") (hash "19l9k75makxzglg3y5fvmds5hpyr67c9l4fd7fmhkzfcgzzyd1xd")))

