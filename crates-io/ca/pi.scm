(define-module (crates-io ca pi) #:use-module (crates-io))

(define-public crate-capi-0.0.1 (crate (name "capi") (vers "0.0.1") (hash "1h4j446v2r34zrz6zji1dr6088sk2qi18bfp5spfxlvmwasd2zhz") (yanked #t)))

(define-public crate-capi-0.0.2 (crate (name "capi") (vers "0.0.2") (hash "0jqqrc0v5mb8myxf5dib8bi1l1zwc653i7q812acxcp9rhn2mqwm") (yanked #t)))

(define-public crate-capi-0.0.3 (crate (name "capi") (vers "0.0.3") (hash "06y5hnvxrm16whrd2y7hwr5184abvv6zdygk1nwfq8k5q11a4d9b") (yanked #t)))

(define-public crate-capillary-0.1 (crate (name "capillary") (vers "0.1.0") (hash "0r2fkx04fynyzs4hixp2xh5lbgkbqbzifay52x15ag2pqgp6jhh3")))

(define-public crate-capillary-0.2 (crate (name "capillary") (vers "0.2.0") (hash "0g031dn7vxzi75rg5hk8ncnr21g4i3k7vv8ms3grrmzhfqrfkif3")))

(define-public crate-capillary-0.3 (crate (name "capillary") (vers "0.3.0") (hash "0k9nlqjz8x20qjl40v9b4z4pd82i59s6c1w9v6l6d6lffv7qbrl6")))

(define-public crate-capillary-0.4 (crate (name "capillary") (vers "0.4.0") (deps (list (crate-dep (name "petgraph") (req "^0.6.2") (features (quote ("stable_graph"))) (kind 0)))) (hash "1f8lqv79v4754qp268vb8f0ffwj38jmh2xvkk38fpmh91c34vcd8")))

(define-public crate-capisco-0.0.0 (crate (name "capisco") (vers "0.0.0") (hash "1l4rrgfkymbrcgzxhzi3jx3hpyvw59q0kza0givpnn2jaj74r705")))

(define-public crate-capitalize-0.1 (crate (name "capitalize") (vers "0.1.0") (hash "1j0i9vd4f3zv4jl1bwy4ihb5qcfxcgpyy94rbbal4r9c0lpzwb3h")))

(define-public crate-capitalize-0.2 (crate (name "capitalize") (vers "0.2.0") (hash "1d404fbmdhrbx870xnivq0zfz93m6bx1z01d30f169rgw3ni92md")))

(define-public crate-capitalize-0.3 (crate (name "capitalize") (vers "0.3.0") (deps (list (crate-dep (name "data-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "0a758pvh9swx550fvighrl8h03y78ac4vxywwcv3547mm88hci0h") (features (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3 (crate (name "capitalize") (vers "0.3.1") (deps (list (crate-dep (name "data-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "0y4rhfwgvkscgvh7xczvr74xvmg1nrfa5k6bry4l13brgmkgdhmi") (features (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3 (crate (name "capitalize") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "data-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "0bgqfbagw14jakh9cvn5d8czxgpqsjpa68z8fas48ypbvzqnn589") (features (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3 (crate (name "capitalize") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "data-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "19xiap5yrg93pkwj9ifgjyyb5cylnqc8ia0xr4v7paalkm512m6d") (features (quote (("nightly") ("default"))))))

(define-public crate-capitalize-0.3 (crate (name "capitalize") (vers "0.3.4") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "data-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "1gxmp8s740i1g6pqfmq9cbcv6v5x0dsfcbsqqzl5x0r2201p2lkb") (features (quote (("nightly") ("default"))))))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (default-features #t) (kind 0)))) (hash "12j80hrmvmp2gr8ax1kgsw6is2k8fyspxgglx3lp4znq3c01v46y")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (default-features #t) (kind 0)))) (hash "1sf1irfrb18h85b6mdfanrl4ibs3yyasfdp1p1yb2mznknsxj7a2")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (default-features #t) (kind 0)))) (hash "1j71i0i5r1s57nh8wxis4jfs4ka1ja2i0s9rdjcw350f5vd4az0b")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (default-features #t) (kind 0)))) (hash "0m0qc0aqbvxdzcvmrwlxcppd6vq8zrfwwm7ns7vwymq7anmig7vx")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (default-features #t) (kind 0)))) (hash "04gp4j4kap705dhzdij1z36z88m022q55bmnxbylvqj9pd6j5s2y")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util" "sync"))) (default-features #t) (kind 0)))) (hash "0xzj1jmgc7r47wa0kskk1fdnrgwws81diwidw4608cvns08nbm34")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util" "sync"))) (default-features #t) (kind 0)))) (hash "16xsrpvpvnm6ghcqrcgf6dmg82wnn17azkckzry572n7apxy6ahv")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util" "sync"))) (default-features #t) (kind 0)))) (hash "0gcz133gy21p676cvjc4aadnc2zabvirlccian94p3yjfvssi847")))

(define-public crate-capitan-lib-0.1 (crate (name "capitan-lib") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util" "sync"))) (default-features #t) (kind 0)))) (hash "14dzqi28y605r1k70yc0q3ip0yxblx27ym99m8cppfwwhqq0crhn")))

(define-public crate-capitan-lib-0.2 (crate (name "capitan-lib") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util" "sync"))) (default-features #t) (kind 0)))) (hash "0ysjwnikygd0qa2my2s1xxqdsk1d5gp3h008qi7rq5a5lanqri54")))

(define-public crate-capitan-lib-0.2 (crate (name "capitan-lib") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util" "sync"))) (default-features #t) (kind 0)))) (hash "12yafrrvafiyjbfprhhwxxbksdcg235rxvnzvq3srx6zn7y1lwdb")))

(define-public crate-capitan-lib-0.3 (crate (name "capitan-lib") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util" "sync"))) (default-features #t) (kind 0)))) (hash "0xlm0zy2lf330i1qs75r77qwlhxyk2fylcw7h1df5ard7rfn4s90")))

