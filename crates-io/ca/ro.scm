(define-module (crates-io ca ro) #:use-module (crates-io))

(define-public crate-caro-0.7 (crate (name "caro") (vers "0.7.0") (deps (list (crate-dep (name "async_executors") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "async_executors") (req "^0.6") (features (quote ("tokio_ct"))) (default-features #t) (kind 2)) (crate-dep (name "audi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "litl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "mofo") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "ridl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1mkdcfsslsk5gg2mvrzbakl3a4b6vxwzs4sxl79i2qvbn287qy1q")))

(define-public crate-caro-0.7 (crate (name "caro") (vers "0.7.1") (deps (list (crate-dep (name "async_executors") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "audi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "litl") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "mofo") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "ridl") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "tracing-futures") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "async_executors") (req "^0.6") (features (quote ("tokio_ct"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.87") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.21.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)))) (hash "1n7wkhjlc38mfv1bw0pg6q0rmz0y31cxb8fwgi64wabajacqr7dx")))

(define-public crate-carol-test-0.1 (crate (name "carol-test") (vers "0.1.0") (hash "11zl1lc5l64n8rd35si71iv8dcljncri8qqmcry43zdx2k7lvjci")))

(define-public crate-carol-test-0.1 (crate (name "carol-test") (vers "0.1.2") (hash "0zhcyyx8syqamh4y0pi2mc40f8wd7rpdi40zi7kxj1fb01xjdqmg")))

(define-public crate-carol-test-0.1 (crate (name "carol-test") (vers "0.1.3") (hash "1s17jjks6kixq6d5673ycxwyx649a0rd0sgizxgvn1xlc6m4mgf1")))

(define-public crate-carol-test-0.6 (crate (name "carol-test") (vers "0.6.8") (hash "1m5mq863qgy00wb0wswh6mayi35k58dfsn49bha0ck4357qm2c4a") (features (quote (("32-column-tables"))))))

(define-public crate-carol-test-0.8 (crate (name "carol-test") (vers "0.8.0") (hash "18cv8dz7622hg4xpyk4211dqm5wk6mbnm9wir6mdz7fp7m29zmsz") (features (quote (("32-column-tables")))) (links "a")))

(define-public crate-carol-test-0.1 (crate (name "carol-test") (vers "0.1.4") (hash "0f5w4sxgwnimp5gn7axmb970i13gdx0fm5isbiq4i2g15c7dn09g")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.1") (hash "0acs7zc6jy2q325cjh9vfz2bs66zjs7qnxz805aw6yyr3jdlxgw8")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.2") (hash "087kdxamvvvqnkvdb3rhxh5mmvklv3vr0raf4fmbym48zdrd023g")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.3") (hash "1i95l8kq1ihwqzcgygh36lm0s5wi9468q1zk5mp11ia6842cya79")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.4") (hash "006jbgiv97kml02iyjpbd4aqx00ln7vkf9qjkwc2s4m0m07d0k5l")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.5") (hash "166q8ffb28vi62yi6ckk6i7zv49pkv23fbxjgjhyan00wzix0w94")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.6") (hash "0k25hay5fpdnyj0dpxkh20f2kqrdrfssbjyjsbrf6ayzc3a15pa7")))

(define-public crate-carol-test-0.0.1 (crate (name "carol-test") (vers "0.0.1") (hash "1vhwv9rzfcnm5v2m1zgw74jhb9yqji00yrzwll6f8wmcspx7yihr")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.7") (hash "1i7xap4kww5z0iq95f3zcxg87bnm2v38k3cgbkzz3i0khkzahnhp")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.0.35") (hash "0yc6wdbvxk68diw37f6bkxn4vavybyhy191vnv6dqki71sq2q452")))

(define-public crate-carol-test-0.0.0 (crate (name "carol-test") (vers "0.0.0") (hash "0mm8jhfm85qy2hg4v0f3aa511kbcah65plkfln895zhlnlan6if8")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.1.0") (hash "16zkchkq5aap9rz8sm52lv2633v5f0fab2g2g2azlzd3dbf8lqc6")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.1.1") (hash "0faw6v4mjihnwwc571hg8iqqa8cwj6vd3i7whqz35cgx0nwb31ms")))

(define-public crate-carol-test-2 (crate (name "carol-test") (vers "2.0.0") (hash "1ks525ij94fgqh0av4ghlryzy82d75pyzpg58n02kqh67mhm9dnh")))

(define-public crate-carol-test-1 (crate (name "carol-test") (vers "1.1.2") (deps (list (crate-dep (name "carol-test") (req "^2") (default-features #t) (kind 0)))) (hash "11nk24hrkappfhb49r729xifi5bml05mbm4lw6szs9204xa8gkm3")))

(define-public crate-carol-test-2 (crate (name "carol-test") (vers "2.1.0") (hash "15454rp6n9yn13n36gr7pv38z881vka47jxnb095lhb97bhffirj")))

(define-public crate-carol-test-2 (crate (name "carol-test") (vers "2.2.0") (hash "1frf56klmvjypn31fzk2xr5299mwj3m2256lkqmsckk9i3qj1nvy")))

(define-public crate-carol-test-2 (crate (name "carol-test") (vers "2.2.1") (hash "03sjq2ir6c4k2bwv2m53zajj5g250wyqx4rcm52hdqdw79dn9n21")))

(define-public crate-carol-test-2 (crate (name "carol-test") (vers "2.2.2") (hash "1rb0k39n984amvc7rr8f6lx1yrdjfqv6vw1il33ciba7ls5znx8l")))

(define-public crate-carol-test-2 (crate (name "carol-test") (vers "2.3.0") (hash "1i05bbihlsdx58afghi91q77glbjvwlbdkcxcmpn33mq59xl778b")))

(define-public crate-caroltestdisregard-0.2 (crate (name "caroltestdisregard") (vers "0.2.0") (hash "0rbxardzarnv91vbn9ng371aw62i7jz4syl03xw30i3r1r3360ap") (yanked #t)))

(define-public crate-caroltestdisregard2-0.2 (crate (name "caroltestdisregard2") (vers "0.2.0") (hash "0wc437dhnhc4md9sqvhpm8bsa8g0y54zwh44vyd0rsklws3xwvbl") (yanked #t)))

(define-public crate-caroltestdisregard2-0.3 (crate (name "caroltestdisregard2") (vers "0.3.0") (hash "1m8y2k9b099pf2pmh61p3amsac6y9dapbnqk750bi4f8n2l99m9b") (yanked #t)))

(define-public crate-carousel-0.0.1 (crate (name "carousel") (vers "0.0.1") (hash "0pr46gfgm9lbxwmm1a921b8ak8qgl1xvbiias0ja7an3plwl48k8")))

