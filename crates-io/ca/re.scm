(define-module (crates-io ca re) #:use-module (crates-io))

(define-public crate-carebridge-provider-0.1 (crate (name "carebridge-provider") (vers "0.1.0") (deps (list (crate-dep (name "iced") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1izk12vpr687k3hbyzbpf6a48ahg62lphq0zgkhm31yrkvx5g4g3")))

(define-public crate-caret-0.0.0 (crate (name "caret") (vers "0.0.0") (hash "16qa2xbm1hgyzg67p6yazzlsykmxa58x62vpz9fzad7k31qb71mp")))

(define-public crate-caret-0.0.1 (crate (name "caret") (vers "0.0.1") (hash "07jkrwjlx3vb4i501grmf97xbp563cawr0i2rxxvwp1a8imivfia")))

(define-public crate-caret-0.0.2 (crate (name "caret") (vers "0.0.2") (hash "1qkh2sihbr323zl8k3w3nmlf4lldyp0zw73bzi2wf588j39yrklm")))

(define-public crate-caret-0.0.3 (crate (name "caret") (vers "0.0.3") (hash "1sg4y841hrwswwksirrdzwcsavl32cbipwx2xpymxrlhpj8gnmrl")))

(define-public crate-caret-0.1 (crate (name "caret") (vers "0.1.0") (hash "068mv8ln6ykb8a4mc862bnd4zc3mx96iq2r7pn8mr9hjkz02q489")))

(define-public crate-caret-0.1 (crate (name "caret") (vers "0.1.1") (hash "0vifh2r0svmpckpp750wsav71zkix7x2rmbl2bx74dd6i3v4n0bc")))

(define-public crate-caret-0.2 (crate (name "caret") (vers "0.2.0") (hash "0laxl3pcnxqwb79ca5mg6zx4f4b6nzpsk7dnr0hdk5xya90yakki") (rust-version "1.56")))

(define-public crate-caret-0.2 (crate (name "caret") (vers "0.2.1") (hash "18pi7y59aqil7qi5h7bml6iw1c41b00rf0n7yyi5j67y0dk2qi49") (rust-version "1.56")))

(define-public crate-caret-0.2 (crate (name "caret") (vers "0.2.2") (hash "1wmkk5b9wb8g74xxgl08hkw2fvgwxpcnzkjsz4nqs4ghzm7nksnf") (rust-version "1.56")))

(define-public crate-caret-0.3 (crate (name "caret") (vers "0.3.0") (hash "1m9xd82ywvfzzfv9s4nsq0a0412ig2cn3x9wik3dd15dg54076cd") (rust-version "1.60")))

(define-public crate-caret-0.3 (crate (name "caret") (vers "0.3.1") (hash "0h26c2904bka9ad55l0ag6fq50civfgynrnwblgcjfch35ddg0sp") (rust-version "1.60")))

(define-public crate-caret-0.3 (crate (name "caret") (vers "0.3.2") (hash "0gcy9wpj0j7jvx6kvgjvwf6i6i5kqnnvm37fq51392n0vs5r20b9") (rust-version "1.60")))

(define-public crate-caret-0.4 (crate (name "caret") (vers "0.4.0") (hash "0br038cmg8f6wrh88wc2zx5qkqj6k8a2n7r3h4dar0s5vqr7nhcj") (rust-version "1.65")))

(define-public crate-caret-0.4 (crate (name "caret") (vers "0.4.1") (hash "1m6fp9cqw28qf625v90gkvd15bsfj7141r23yblnzfwn1492kyb9") (features (quote (("full")))) (rust-version "1.65")))

(define-public crate-caret-0.4 (crate (name "caret") (vers "0.4.2") (hash "13782q7b9f0g9wbzp9i217w6b77kkifh7xahpmfqj8s6wjf43vx9") (features (quote (("full")))) (rust-version "1.65")))

(define-public crate-caret-0.4 (crate (name "caret") (vers "0.4.3") (hash "0s77pp1lrzh8lyi728gdh78ncb2dp4q4vzg1akvhc7p91zypfdma") (features (quote (("full")))) (rust-version "1.65")))

(define-public crate-caret-0.4 (crate (name "caret") (vers "0.4.4") (hash "0jlwcmmw955b7y7r5l6z414sqcmqsb1pc118zg73xdx8nwc2sivs") (features (quote (("full")))) (rust-version "1.65")))

(define-public crate-caret-0.4 (crate (name "caret") (vers "0.4.5") (hash "1nqq0a83pqfjn0b10p0hh5kr7b5cxjjcnr36srzq3vxq0jcvnm9d") (features (quote (("full")))) (rust-version "1.70")))

(define-public crate-caretaker-0.1 (crate (name "caretaker") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0sz1xrq98nnzz9i9zisnj9msr9h5hay8vqysy50vxxnbzdprjqkh")))

(define-public crate-caretaker-0.1 (crate (name "caretaker") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0xfhk6xj950lrs2a48mkaxfqn1r8ldvlf9xd014asjdgjfb2zvr8")))

(define-public crate-caretaker-0.1 (crate (name "caretaker") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1ib1w8w8spac08q08b7fjwf96a7q50fj8z57699y9820r8h509dj")))

(define-public crate-caretaker-0.1 (crate (name "caretaker") (vers "0.1.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0rxlvfn0f8r9vrdwj1i47cmkpw4nq28gwx66d5b0vp58znwmndav")))

(define-public crate-caretaker-0.1 (crate (name "caretaker") (vers "0.1.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "07fvx4f8fdqg5q4vb5msz230r5mv5mqwnm1ng4mxlnx0avrg2bp4")))

(define-public crate-caretaker-0.2 (crate (name "caretaker") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "1lnsbc9yv2fiv7sb59f6hnd2cfnwwsm1q00nm78g9sza0r5fz7cy")))

(define-public crate-caretaker-0.2 (crate (name "caretaker") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.6") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0frck2n0insa4fb77c7zq4l1mbkksdr3965lp0qk3gvl257j5v3i")))

(define-public crate-caretaker-0.2 (crate (name "caretaker") (vers "0.2.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.12") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0m99j2gq4008ssnw4m1l645sg66sr08x3lhsjwn1hvfwq0ydj3a0")))

(define-public crate-caretaker-0.2 (crate (name "caretaker") (vers "0.2.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("std"))) (kind 0)) (crate-dep (name "clap_derive") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.15") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "05mlzk38n33kyaqllw3c0cx4bhxpwnfbcszc5d4wqvk6vmbdsc9x")))

(define-public crate-caretaker-0.2 (crate (name "caretaker") (vers "0.2.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("std"))) (kind 0)) (crate-dep (name "clap_derive") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "122621h8ck9pcsxn3s5fsx5kp9xlr64rr3civ3rkh4kch1nmq4r6")))

(define-public crate-carey_rpn_calc_test_ver-0.1 (crate (name "carey_rpn_calc_test_ver") (vers "0.1.0") (hash "13f3cn5dpilvp2k2mj8p3aqmhpjmjll9m1srq0058h1balic1v25")))

