(define-module (crates-io ca br) #:use-module (crates-io))

(define-public crate-cabrillo-0.1 (crate (name "cabrillo") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (features (quote ("regexp_macros"))) (default-features #t) (kind 0)))) (hash "0r2pixsw2anhv3phz9ajgm2smbf6nl2p46idbzb7mig94pc5zlmq")))

(define-public crate-cabrillo-0.2 (crate (name "cabrillo") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (features (quote ("regexp_macros"))) (default-features #t) (kind 0)))) (hash "0y057pq69a3nwwz6gsx0d73yh0ywvzfr2z4l5agbdi8z4n6kfyhg")))

(define-public crate-cabrillo-0.3 (crate (name "cabrillo") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "0i8160s8k5xr34mivx2dgsplfpvs062xc6pwfv6xb2zvqajs1c20")))

