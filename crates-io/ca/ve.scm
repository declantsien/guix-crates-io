(define-module (crates-io ca ve) #:use-module (crates-io))

(define-public crate-cave-fmt-0.1 (crate (name "cave-fmt") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "0xap36y32kxmbw299wlp122gn24hipjxcx5rhwa8xpqnynlvjwhp")))

(define-public crate-cave-fmt-0.1 (crate (name "cave-fmt") (vers "0.1.2") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "19ssixgnl12s05qmlbx5jfyxahvsd91adm49kl1n7pf6zlzl0wia")))

(define-public crate-cave-fmt-0.1 (crate (name "cave-fmt") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "1gjnazck3c7676w2y26r07wc1gb6c27nmwh5w3z5xzbkq7lc2jl0")))

(define-public crate-cave-fmt-0.1 (crate (name "cave-fmt") (vers "0.1.3") (deps (list (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "vek") (req "^0.15.8") (default-features #t) (kind 0)))) (hash "1619gmwk6l6gx1wdc6hm9kc4vsiy0h853wv8m38wmrsllx5h8dbl")))

(define-public crate-caved-0.0.0 (crate (name "caved") (vers "0.0.0") (hash "1l6cgxyq5znf6lazdc9z98ww6x8zh95z7bicrz08k8l04yyicafi")))

(define-public crate-cavegen-0.1 (crate (name "cavegen") (vers "0.1.0") (hash "1d7pj4w1mi8jrbppp13a4ib06x8v0kw7r2xpkgklij64ldvla7sq")))

(define-public crate-caveman-0.1 (crate (name "caveman") (vers "0.1.0") (deps (list (crate-dep (name "protobuf") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen") (req "^3.2.0") (default-features #t) (kind 1)) (crate-dep (name "rustc_version_runtime") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "11vbh5cxxy4fjjqk4482z7f3434i9hc6my1xw68wscnifc1bz913")))

(define-public crate-caves-0.1 (crate (name "caves") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_fs") (req "0.11.*") (default-features #t) (kind 2)) (crate-dep (name "atomicwrites") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "1.0.*") (default-features #t) (kind 2)) (crate-dep (name "rocksdb") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0iz8xpb3f4z725m55f3c9bfgcp7l7q27xsm6lb7lgcn5sm4zh0qd")))

(define-public crate-caves-0.2 (crate (name "caves") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "atomicwrites") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rocksdb") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07ls7bd6m5qs3ayb15ywcmfwnw27rigr7cy3pk3m5z3rkjf9nqrc") (features (quote (("with-rocksdb" "rocksdb"))))))

(define-public crate-caves-0.2 (crate (name "caves") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "assert_fs") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "atomicwrites") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rocksdb") (req "^0.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "060wigf7p6561llizp9aw4ahajyzy7vs18y0yfqfkzwywiikszy6") (features (quote (("with-rocksdb" "rocksdb"))))))

(define-public crate-cavestory-save-lib-1 (crate (name "cavestory-save-lib") (vers "1.1.1") (hash "07cm07v3mnr7dd42n5cxi6xlcyzlvgkqjypjqnbnl2rc3k0mi52y")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.0.0") (hash "0mr9ijhb53lnl3pp70x84m0s3rafihb8jw989izdb79c6rcy8q24")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.0.1") (hash "0dm0k7ydbpp49yz1q7r23i6svqxsnafilkvr56ynzpw3y6wg6pkz")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.1.0") (hash "1xarxj5i27pmwrd4gjgcl8iz7vwd4cwrgx6lczfr1kdyrsnl6d63")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.2.0") (hash "16ys521c914g3gjfh3mvj1agf3y6b8y9ljkw3p7bi482wixz862k")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.3.0") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zwg2qshkgmg47in8k9l52v6r7krrs89nqpshhzjvkxss8irsiip")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.3.2") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1kwdnwz3524x8bv37svpjg54ljvnrkb62rcz81amdf9wbzf8h6cl")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.4.0") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1knxsjapnkqxicjy4k5qkj0348cmfna2ap5gwd8iab7ylsk6idji")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.4.1") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18ijh2a1ml52n7d3qdmrf6f2kmwpyky9kw63ppm9nzc6n6ar7f9m")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.4.2") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10ksam44srrvvf164c618n8n2aabmga488djc2hydnjivcgid1pg")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.4.3") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "074h9j7xm4wnpcdwhydkw4qsgj4iih9n3anl013cnzahlzarr3rj")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.4.4") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1kcvjxq1rc6sagbikkxmz3qlmpx4sqzwcal2qg5wfabq9pmqjf5i")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.5.0") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gi11bwaxji7a0zbzmm53sa0q40nna2hgp7nijxaqnrcz2kg5pmy")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.6.0") (deps (list (crate-dep (name "bit-struct") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0aicls3glc1vbggibnk83h5jlifjwsyfq86i6vgrl2wb7l760c0m")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.6.1") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "121m63nhqilxzbyy1i4g55hyfrc0q28qsn0b1wv7fhwqqim29q1p")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.6.2") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rk41067vh98xk80a816m6wzgzfw4r0ixcicdw739snvh2kqm0ln")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.7.0") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wqzxghb35218n1lr859cgds76y6izj61zkcwbfvf8ccbmzq5k7p")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.8.0") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1xwd3y9rk2qqjmmd0a3p2hpvrnik419agv23xi7wika4a288q9y7")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.8.1") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "13sfrwq2vb6bvia4k8wlrm4khn5grbkb9h0rpagv4c4zxv2vwyw8")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.8.2") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0mwghpwx5byix9lnid3yik0ynvl049qw8vsp3p0ij0bq12ljkgxl")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.8.3") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "01qsrrsqn48121zmdq79dmqqvv3f7kklq9766s1hfb9p81wk61qp")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.8.4") (deps (list (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1ndhd297fwdd4kh75ckjs32679piy60hdws8dni2idi6f57hiy84")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.8.5") (deps (list (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive" "std"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "10qpvmfnfb2hsc28lx4695k8mkbqrlsb5d6wr4asn4zcbx5pwn73")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.9.0") (deps (list (crate-dep (name "strum") (req "^0.26.0") (features (quote ("derive" "std"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1ydzqyzpmiydk7bisydmi7nzsibcswk83h9hr8amadkwp392dngv") (features (quote (("strum") ("default" "strum")))) (rust-version "1.75")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.9.1") (deps (list (crate-dep (name "strum") (req "^0.26.0") (features (quote ("derive" "std"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1h26ycnkvbcs6x6wxmkaz1kybkd7a51dqq41ji926dq73049bpr7") (features (quote (("strum") ("default" "strum")))) (rust-version "1.75")))

(define-public crate-cavestory-save-lib-2 (crate (name "cavestory-save-lib") (vers "2.9.2") (deps (list (crate-dep (name "strum") (req "^0.26.0") (features (quote ("derive" "std"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "01lm5w03kvib3jkk92hb2n8vmihsgl8mh2v5ghqjz631clncv0iz") (features (quote (("strum") ("default" "strum")))) (rust-version "1.75")))

(define-public crate-cavestory_save_editor-0.1 (crate (name "cavestory_save_editor") (vers "0.1.0") (deps (list (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1zrscap7cmf59lm411cjggrxhxc3wr64yb3k4hknjfzcnyshz2d8")))

(define-public crate-cavestory_save_editor-1 (crate (name "cavestory_save_editor") (vers "1.0.0") (deps (list (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0csvrp2klqshfn966psv56vk0857030gdvdjqwnk2rf26wzazl8l")))

(define-public crate-cavestory_save_editor-1 (crate (name "cavestory_save_editor") (vers "1.0.1") (deps (list (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "16xyixdi37wfzhyjzqcxxfrzl9mlr4k0v7lxwh0mnhpmiyraj6dz")))

(define-public crate-cavestory_save_editor-1 (crate (name "cavestory_save_editor") (vers "1.0.2") (deps (list (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00m0cn77kcfnhsqk5x7691svarkxd21gfx019g6i9db6m9bxdab5")))

(define-public crate-cavestory_save_editor-1 (crate (name "cavestory_save_editor") (vers "1.0.3") (deps (list (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0fz28893ikw3d0jjlspl6g9isyisdmhfsjwcf3w5p3zpk65gwcwi")))

(define-public crate-cavestory_save_editor-1 (crate (name "cavestory_save_editor") (vers "1.1.0") (deps (list (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1ldm0yzqaf7b11k2b884sd81qwgkp06kbpyxn4w1a6m6jrs2n04b")))

(define-public crate-cavestory_save_editor-1 (crate (name "cavestory_save_editor") (vers "1.1.1") (deps (list (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "18nxr0kz1s3gx8fmp2ndkvmswr683i963m5igdq2rf5dm96kwiqp")))

