(define-module (crates-io ca td) #:use-module (crates-io))

(define-public crate-catdog-0.1 (crate (name "catdog") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs_io") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "07fff4mxi9lr24wfa694sbl7fk2w31j8jsgwbkmdrzddcp9nydx0")))

(define-public crate-catdream-0.1 (crate (name "catdream") (vers "0.1.0") (deps (list (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "string-builder") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0k46i0iz778m1zrdhg9ha6zm6f6x3lqifh23fb3wzzmbz8v28rrg")))

