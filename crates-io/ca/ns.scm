(define-module (crates-io ca ns) #:use-module (crates-io))

(define-public crate-cansi-1 (crate (name "cansi") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "parse-ansi") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1ahidq9z0367kq6gpgw5ykxdj5xhwbzmbfv32pwl7r723yzv8xnm")))

(define-public crate-cansi-1 (crate (name "cansi") (vers "1.1.0") (deps (list (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "parse-ansi") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "06r3jx3zh54n38ijx03dazxdrvc7sxjwrv9z8a2bwv6f23s407y1")))

(define-public crate-cansi-1 (crate (name "cansi") (vers "1.1.1") (deps (list (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "parse-ansi") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0m7nn2mac5fpnvaw4rv0nxsi8a1b8nnc43kx61l6llh82kck6gj5")))

(define-public crate-cansi-2 (crate (name "cansi") (vers "2.0.0") (deps (list (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "159avyiivyrk85nif2pwydx259zvacqyx2kw10ryky6l972xds2m")))

(define-public crate-cansi-2 (crate (name "cansi") (vers "2.1.0") (deps (list (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1w0q959phwy7qgpfwq34i3391byc1fccrc2nzxrmd5shjdyi2pjz") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-cansi-2 (crate (name "cansi") (vers "2.1.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0kqbp1wk3nmbxmaamrd6j5srnkmcwnczjpn95mk39hjgv20r4nqq") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-cansi-2 (crate (name "cansi") (vers "2.2.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0av9z6xvsw0qvbqnk3lkjcq6pym1biqxwnhg810y3bnyyczvm44n") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-cansi-2 (crate (name "cansi") (vers "2.2.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1y3yjr2fkla0cssj23lg0l58m0g6af6f8xyf2ms031in2n3sxp2b") (features (quote (("std") ("default" "std") ("alloc"))))))

