(define-module (crates-io ca ja) #:use-module (crates-io))

(define-public crate-caja-0.1 (crate (name "caja") (vers "0.1.0") (hash "00a51288z220yrjzl822gi0q2a11bld9yhb5xgqfcssl1qrhqyak")))

(define-public crate-caja-0.1 (crate (name "caja") (vers "0.1.1") (hash "03cnhlhzdl0b12vlf277spkxmryi3aqd2wi981fc0h43k78hxmiq")))

(define-public crate-caja-0.2 (crate (name "caja") (vers "0.2.0") (hash "1vkd7vv43hx94sz7lbam8fv254ajlb09g2qsk6adg3wj5l4qy9b9")))

(define-public crate-caja-0.2 (crate (name "caja") (vers "0.2.1") (hash "0jrbhv31a7bgxbg4grfwajh8b09bnh1l0yq3af0mh1x227dcyflf")))

(define-public crate-cajal-0.0.0 (crate (name "cajal") (vers "0.0.0") (hash "19nm4pqa84jb3w7fd342c3ypacmv3mww75625vv97zggvbwx84bp")))

