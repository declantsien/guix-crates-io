(define-module (crates-io ca mt) #:use-module (crates-io))

(define-public crate-camtrap_dp-0.1 (crate (name "camtrap_dp") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0cx5d6cpd5qnp16rj3d7fh3s57hxbdjdh0wkclanp07iv96q96d4")))

