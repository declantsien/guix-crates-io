(define-module (crates-io ca nr) #:use-module (crates-io))

(define-public crate-canrun-0.1 (crate (name "canrun") (vers "0.1.0") (deps (list (crate-dep (name "canrun_codegen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1ishw9ck91zsd4v9klc2h3aai0bpxyrw3rrnhvq16glzsmb5w86n")))

(define-public crate-canrun-0.2 (crate (name "canrun") (vers "0.2.0") (deps (list (crate-dep (name "canrun_codegen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1aijlda5waza6j24cli35l6w15jww5pmdsb1ba3n4yslrnwbkg4i")))

(define-public crate-canrun-0.3 (crate (name "canrun") (vers "0.3.0") (deps (list (crate-dep (name "canrun_codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1lvqi057z8lcp4jiza07zc9adwv5m8b1vmjzbhc6rbwm65jma6k6")))

(define-public crate-canrun-0.4 (crate (name "canrun") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)))) (hash "06vralmgxl5pf76i9f469ggr6fg8qz44sl8hg4jp2yqbvcncd3g6")))

(define-public crate-canrun-0.5 (crate (name "canrun") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)))) (hash "0947qa52vsjx3hs321631pav9l9sriqk3vmmvvkgbh8kk4sgc677") (rust-version "1.64.0")))

(define-public crate-canrun_basic-0.1 (crate (name "canrun_basic") (vers "0.1.0") (deps (list (crate-dep (name "canrun") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vp7ld80iym12fz4djya1p6aqxz1wdj6y5cgdn8x5948pmhahhgn")))

(define-public crate-canrun_codegen-0.1 (crate (name "canrun_codegen") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "02n9aafm4hghg7r51iqy6w3cv9qlmfnn3fyjnf5h0kp44vhsyjlv")))

(define-public crate-canrun_codegen-0.2 (crate (name "canrun_codegen") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "1nx1vmw31xldvf2j8lrj4bvz4x8c9jqzzrp9b61xd3h4308xvj25")))

(define-public crate-canrun_collections-0.1 (crate (name "canrun_collections") (vers "0.1.0") (deps (list (crate-dep (name "canrun") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1d978y0y7zr859j0lgxww4vs5zkk7xgl31qs27yzffa6lihb1xyh")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.0.0") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "0.15.*") (default-features #t) (kind 0)))) (hash "0mzd45a51nifak9lqxd1q1pvbf40gbsshm2zc5hi04ckfzxxfdga")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.0.1") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "0.15.*") (default-features #t) (kind 0)))) (hash "0wnbr1rv6fxdnqm5gv3wd0ssgnkw0nppr3wy1qlynf9w32dd5kj6")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.0.2") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "0.15.*") (default-features #t) (kind 0)))) (hash "19fxbz34z88vhv61sj462k8d8lvxw32awfy185faz3k0wq9z0k8y")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.1.0") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "0.15.*") (default-features #t) (kind 0)))) (hash "13y7nzv5r6n1g7hicypa4sn9ssrlzy4vwi361mhdy64q6fpr6gb1")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.2.1") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "^0.15") (default-features #t) (kind 0)))) (hash "0bnwmvgp33wh06qjwgzbrmw9ijh4vl3v1ia9z9x41x2klh2x847j")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.3.1") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "^0.15") (default-features #t) (kind 0)))) (hash "1yq5ckjxhvbc9qxbb0421lglmjhq8n9azghg3f1lk550dbznljgs")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.3.2") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "^0.15") (default-features #t) (kind 0)))) (hash "0bc4n8djayvw31606zjfdgymw7b53c8654b0d1v0jrwyxg0d3a4n")))

(define-public crate-canrust-1 (crate (name "canrust") (vers "1.3.3") (deps (list (crate-dep (name "fontconfig") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sfml") (req "^0.15") (default-features #t) (kind 0)))) (hash "0aalsrrdjz6dfgxqizx1p1w88m8hnfq93drp8lisg67scrqsm0m6")))

