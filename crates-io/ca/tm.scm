(define-module (crates-io ca tm) #:use-module (crates-io))

(define-public crate-catmark-0.0.0 (crate (name "catmark") (vers "0.0.0") (hash "176d83crq12ia340xxbppv14w6p1fs05knr379b766r7z4xykm3j")))

(define-public crate-catmark-0.1 (crate (name "catmark") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sxc98zgbsgg19xvghxss7ysj69xl8xarna4s2xb5ar57k26aaci")))

(define-public crate-catmark-0.2 (crate (name "catmark") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "04yw4k5kh7sf4q3rhcp9q8svy4a3n724vplxhgfqgimmjywipllx")))

(define-public crate-catmark-0.2 (crate (name "catmark") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ns8i4fiags1krhcirs7xiy0av3aipr50ss630l4lgak8dkw92pg")))

(define-public crate-catmark-0.2 (crate (name "catmark") (vers "0.2.2") (deps (list (crate-dep (name "ansi_term") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ws90dqqzlvhqi8z4w58s2w7jh8kclmff8b00hx3r0vbnm87gwxk")))

(define-public crate-catmouse-0.1 (crate (name "catmouse") (vers "0.1.0") (hash "1pbawlk7ynfsj6fzmdhkzcgwlm9bhcl7rdn6fbd2ld6s6vfhsm48")))

