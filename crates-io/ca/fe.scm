(define-module (crates-io ca fe) #:use-module (crates-io))

(define-public crate-cafe-0.1 (crate (name "cafe") (vers "0.1.0") (hash "1y07s92f8zwa10fsl8s8wi2xh5xkhxbwv1ran02bdq7hcasqgda8")))

(define-public crate-cafe_core-0.1 (crate (name "cafe_core") (vers "0.1.0") (deps (list (crate-dep (name "coffee_lang") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "latte_fs") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "mocha_audio") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (kind 0)) (crate-dep (name "stb_image") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "tea_render") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0d23kgd2m25y7xsazz085mibqnb9cnqpx2s5x06lmjfy23mj1l4v")))

(define-public crate-cafe_core-0.1 (crate (name "cafe_core") (vers "0.1.1") (deps (list (crate-dep (name "coffee_lang") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "latte_fs") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "mocha_audio") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (kind 0)) (crate-dep (name "stb_image") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "tea_render") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0cy92jfw74kpmgpagfw3ya1fnics1am8m4sla47agymv4n04x121")))

(define-public crate-cafe_core-0.1 (crate (name "cafe_core") (vers "0.1.2") (deps (list (crate-dep (name "coffee_lang") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "latte_fs") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "mocha_audio") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (kind 0)) (crate-dep (name "stb_image") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "tea_render") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pick2i96szvx15js9yz3xnq52mhg44cv9lzjj349p3h0hm1jic9")))

(define-public crate-cafebabe-0.1 (crate (name "cafebabe") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0b4axrw9xp50q45ik3lsm218a3dfwhsf5c9sb3apdv2z862f3irb")))

(define-public crate-cafebabe-0.2 (crate (name "cafebabe") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "18l2ybzk7h5v6byh82wgfka467fpikwb2k9zw2llg89a2pd0ysw4")))

(define-public crate-cafebabe-0.3 (crate (name "cafebabe") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "118qvm7agxjz1as2nsb3yfnqsm357x1vdib8dbs9a7pyd7ka0bb5")))

(define-public crate-cafebabe-0.3 (crate (name "cafebabe") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ah6lqh0fbpjyyl5i0hdhw731lwm6bpl5bn89xgvddvqbkph47nk")))

(define-public crate-cafebabe-0.4 (crate (name "cafebabe") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0v4zspf6r7xz48ysa2bsksji669mi7jnw93wmk071csc57c8hvbx")))

(define-public crate-cafebabe-0.4 (crate (name "cafebabe") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0q97h1w2p3cwsin8pcgipymwgklqfm7ysrwlql4j4wgzndly9p6f")))

(define-public crate-cafebabe-0.4 (crate (name "cafebabe") (vers "0.4.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "133r88dwxpjzaf6kmfclkq3mbgnkhf4v0kbjmg0xg00i6vif8d27")))

(define-public crate-cafebabe-0.5 (crate (name "cafebabe") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0aiw4fqia5vz9c4b76n01xmhixs5wq9ypshir3srz0zx5qn8mbip")))

(define-public crate-cafebabe-0.6 (crate (name "cafebabe") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "12c526mm43v42f4icabjpd78kks0h359mxp2gxy7qsbjnzm7wjqk")))

(define-public crate-cafebabe-0.6 (crate (name "cafebabe") (vers "0.6.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0860qympjgkyv6jwz7xwlicfrc4j4f56943s4nw8w4nf3njfxd9w")))

