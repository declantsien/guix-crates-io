(define-module (crates-io ca bb) #:use-module (crates-io))

(define-public crate-cabbage-0.1 (crate (name "cabbage") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("rustls-tls" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.30") (default-features #t) (kind 0)))) (hash "1qn7cd0gc3b61vdn5bjr4gcxpqdxcvxq9ypwsr7h8hgpw3kbv036")))

(define-public crate-cabbage-0.1 (crate (name "cabbage") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("rustls-tls" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.30") (default-features #t) (kind 0)))) (hash "0ivj5gcfgsl9yy3jb081f3z52qsp4nil59k1hc433l7wnypxjijx")))

(define-public crate-cabbage-0.1 (crate (name "cabbage") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("rustls-tls" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.30") (default-features #t) (kind 0)))) (hash "0lx02yd47xfs7lp2p3hx8hm8p96g5w2brqapcfcin5abi29f85gc")))

(define-public crate-cabbage-0.1 (crate (name "cabbage") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("rustls-tls" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.30") (default-features #t) (kind 0)))) (hash "1g6slwpvcnnpv3kkk157kxyzsqnq4z7g7l4rbspvph5hd1s66ldv")))

