(define-module (crates-io ca ye) #:use-module (crates-io))

(define-public crate-cayenne-0.1 (crate (name "cayenne") (vers "0.1.0") (hash "0i4j61amrh2q28fiyxs2xsi9dh37k0csdiys64yfkxq3r4y7vfch")))

(define-public crate-cayenne_lpp-0.1 (crate (name "cayenne_lpp") (vers "0.1.0") (hash "1928h1mjcwwshahdkm2sfvxwd93ibf9as3sipr0gcwnns8p0w176")))

