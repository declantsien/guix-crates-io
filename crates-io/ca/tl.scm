(define-module (crates-io ca tl) #:use-module (crates-io))

(define-public crate-catlines-1 (crate (name "catlines") (vers "1.0.3") (deps (list (crate-dep (name "docopt") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "087k5bf9c8nhp455nk99z18ijaxhivwnl0jczr48lhgk4sj3hwa0")))

(define-public crate-catlines-1 (crate (name "catlines") (vers "1.0.4") (deps (list (crate-dep (name "docopt") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "18vin9nc1iqn9i3dj6lkp9k68pna7qnxni5hqckrm5y1q34yp0q1")))

(define-public crate-catlog-0.0.0 (crate (name "catlog") (vers "0.0.0") (hash "13y1rwa6qs9acwvx267162ra3h27wvn5599imlamgbgbb0mjwyac") (yanked #t)))

