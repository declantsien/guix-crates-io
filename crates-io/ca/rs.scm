(define-module (crates-io ca rs) #:use-module (crates-io))

(define-public crate-cars-0.1 (crate (name "cars") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "04hcqdkx5hlyq09056klyvzcrlrcqn15br6bl74bzwvhnwrbg758")))

(define-public crate-cars-0.1 (crate (name "cars") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0i49f5dbabbhgjixk8h1ska282inr5pfbz8y5msgfn161s0zi38n")))

