(define-module (crates-io ca se) #:use-module (crates-io))

(define-public crate-case-0.0.1 (crate (name "case") (vers "0.0.1") (hash "1qqlmzwwmdlg8x6gb6f0f2xbs3qajs4pcz0ck9r4kw9hqay9i7cr")))

(define-public crate-case-0.1 (crate (name "case") (vers "0.1.0") (hash "1hgc6fdg01qfh0qx5c50n717vh0xqvrlvxix8ksng5p291mid2z8")))

(define-public crate-case-1 (crate (name "case") (vers "1.0.0") (hash "074wrwl3fijyyi368a08xllgxyhb1i45hgzk88gjjq3xh1xhwv7x")))

(define-public crate-case-conv-0.1 (crate (name "case-conv") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0zrj0zx5mnq415x16n5zdw1x7cpi2qy3jp2qjh9sfbkgffsxvjhk")))

(define-public crate-case-conv-0.1 (crate (name "case-conv") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0l0ii1lyakzgx43fj66y3zrvass9q695885qbany9cwhjrbv2blm")))

(define-public crate-case-conv-0.1 (crate (name "case-conv") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1n66n4qfa6mfl5jkhjl17r10rfkx3zry5gckszyyfh5r0hlrbqww")))

(define-public crate-case-conv-0.1 (crate (name "case-conv") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0sn40cirybqf9iw93x0a8p4jbvx302b1mljlsr8vpnz7frcfcbfd")))

(define-public crate-case-conv-0.1 (crate (name "case-conv") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1wf6l20l0075rb76dij610q0204sjl97xaxasiw98fa8wskpndn1")))

(define-public crate-case-conv-0.1 (crate (name "case-conv") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "19z7zfx4f4bayanwszzjp1yc2ldv8rdibzb5f4dhpmqkikwzglm3")))

(define-public crate-case-conv-0.1 (crate (name "case-conv") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "03sx0z9sxhlb37mdjw13j2bmlxny5yyrasql63zls747iwjpg780")))

(define-public crate-case-macro-0.1 (crate (name "case-macro") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (default-features #t) (kind 0)))) (hash "11ya8avlnsf4m9kh2840xywzxq5zxxsxzpp0bi2f2dirlahw5yyd")))

(define-public crate-case-switcher-1 (crate (name "case-switcher") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0dljp53mv28g38mn11wih4x7vpngl681wp6q0x2d0na4q5dwbbdr")))

(define-public crate-case-switcher-1 (crate (name "case-switcher") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "16nk37zfr9dz4kbdcjqmcf4z4ld3n54ss1nsa7j1d9xp81w89fp3")))

(define-public crate-case-switcher-1 (crate (name "case-switcher") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0wzy1p4vkj5q77xv4g68sp5yfnwri79j19s04fja4vs065llqp9i")))

(define-public crate-case01-rs-0.1 (crate (name "case01-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1n8qpk2w8dhc7qysg4d96crddrc082kgy1bbfk5p2axdz5kzslqx")))

(define-public crate-case01-rs-0.2 (crate (name "case01-rs") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1gp2lb50n9jj4x65m9zcpkp62fqywlqmp8d3xy1vxjhgwwf6jcim")))

(define-public crate-case_convert-0.1 (crate (name "case_convert") (vers "0.1.0") (hash "1sk8wwymajp79v9ysf4d6cfpqfr9k7in4nxps2c11q886bx88m9z")))

(define-public crate-case_converter-0.1 (crate (name "case_converter") (vers "0.1.0") (hash "14j6g5ag48qglbnnzsc11cmgk77fkx8ri9dlrg2bwl7prkw35070")))

(define-public crate-case_converter-0.1 (crate (name "case_converter") (vers "0.1.1") (hash "1j9gi1x6c6v8rdk967f3mpzfai737f3zqykfagb4i6s7ws6p3vlk")))

(define-public crate-case_insensitive_hashmap-0.9 (crate (name "case_insensitive_hashmap") (vers "0.9.0") (deps (list (crate-dep (name "unicase") (req "^2.6.0") (default-features #t) (kind 0)))) (hash "0c9f4v5nbxrcbi6a3rbxwibg7f0slrccvq25fkn86xmzxmqqf2bg")))

(define-public crate-case_insensitive_hashmap-0.9 (crate (name "case_insensitive_hashmap") (vers "0.9.1") (deps (list (crate-dep (name "unicase") (req "^2.6.0") (default-features #t) (kind 0)))) (hash "0zflqrsf30c8r46399d7h7kd33irn2i2p4pm8ppn5223kbb3ggd7")))

(define-public crate-case_insensitive_hashmap-1 (crate (name "case_insensitive_hashmap") (vers "1.0.0") (deps (list (crate-dep (name "unicase") (req "^2.6.0") (default-features #t) (kind 0)))) (hash "14jms4i46qj10cpxjwxi5wscbnjjb93b1gz7vvg7hbzh7g2kp2nc")))

(define-public crate-case_insensitive_string-0.1 (crate (name "case_insensitive_string") (vers "0.1.0") (hash "0gsa5jk59aykdsj40xiic6yalsgikba96s7csndsk4nizh5q19mc")))

(define-public crate-case_insensitive_string-0.1 (crate (name "case_insensitive_string") (vers "0.1.1") (deps (list (crate-dep (name "compact_str") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1fmhfyl684bnnzdkpmgxnamzg5f8vs421nbkiw1l6b7ayxgxgvz3") (features (quote (("compact" "compact_str"))))))

(define-public crate-case_insensitive_string-0.1 (crate (name "case_insensitive_string") (vers "0.1.2") (deps (list (crate-dep (name "compact_str") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0gfmqph255ngpi9vh9350142dfa9zm652qvc37hmhs0wsrj1wa0h") (features (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1 (crate (name "case_insensitive_string") (vers "0.1.4") (deps (list (crate-dep (name "compact_str") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vlx2gs9lq8pqczmy1ps4im1nb4l7nnwa6in8p8fpcdwdwm94qar") (features (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1 (crate (name "case_insensitive_string") (vers "0.1.5") (deps (list (crate-dep (name "compact_str") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "11xb6ng3cn5mfvwd37ilxl58m6mq07fw9dra6z348448ga34x5gc") (features (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1 (crate (name "case_insensitive_string") (vers "0.1.6") (deps (list (crate-dep (name "compact_str") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cd025l2ldqh8qr23ghdr1r7yw6nfw7br3s0hndz4l7c05pxjljj") (features (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.1 (crate (name "case_insensitive_string") (vers "0.1.7") (deps (list (crate-dep (name "compact_str") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.155") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ib3xg3kghysd0p22mj8a5filkz1zlc4f6919rl3d2xs3ry9fz40") (features (quote (("compact" "compact_str" "serde"))))))

(define-public crate-case_insensitive_string-0.2 (crate (name "case_insensitive_string") (vers "0.2.0") (deps (list (crate-dep (name "compact_str") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "13ppdxlj5z6lp9j4di18982fqwr3ickdkdiz2qpqjsx3rqlpj63i") (v 2) (features2 (quote (("serde" "dep:serde") ("compact" "dep:compact_str"))))))

(define-public crate-case_insensitive_string-0.2 (crate (name "case_insensitive_string") (vers "0.2.2") (deps (list (crate-dep (name "compact_str") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bk9292vq7525jhswfipdppv3lyzj0998k1mmgdp7jfwb94msfr9") (v 2) (features2 (quote (("serde" "dep:serde") ("compact" "dep:compact_str"))))))

(define-public crate-case_insensitive_string-0.2 (crate (name "case_insensitive_string") (vers "0.2.3") (deps (list (crate-dep (name "compact_str") (req "^0.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1k8vh8vg3rga6fl478919mrhf103sl09bq49vjmiah9rggi9n8pw") (v 2) (features2 (quote (("serde" "dep:serde") ("compact" "dep:compact_str"))))))

(define-public crate-case_style-0.1 (crate (name "case_style") (vers "0.1.0") (hash "1b4s92sv5ldk2c4bib98cam3d71b7s72s1cxx2x5gc0k2mb7ba7s")))

(define-public crate-case_style-0.2 (crate (name "case_style") (vers "0.2.0") (hash "04b9s4spf2h41djrzarxc9b2gfz5i2vy2xg982kmw0zqjlf6vqkv")))

(define-public crate-case_style-0.2 (crate (name "case_style") (vers "0.2.1") (hash "15lqyl018n81skx6ra6wiv8czsz9z4y0b5nb12pkd4libbq03rhb")))

(define-public crate-casec-0.1 (crate (name "casec") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1wbi8kvyml4vvb8c471k4w204ar0fyv7k29wj3bqs894b069yl8p")))

(define-public crate-casec-0.1 (crate (name "casec") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "129ycrkcgqrrl28p0man4hfnqwvkf3q2avy00lc5cnqsgwkqahzi")))

(define-public crate-casec-0.1 (crate (name "casec") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1pmsc82c8iiggxz67i4xl7c1ldwqp9skzxsqwn2bj5w9870jsgsi")))

(define-public crate-caseconv-0.1 (crate (name "caseconv") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1di0cpi92lj6s7s8w0bx4pgf276pjsm3gmwj0lnglswrfasm0pi5")))

(define-public crate-caseless-0.0.1 (crate (name "caseless") (vers "0.0.1") (hash "154snivwqmiza7fbqcxj19v73hi2lqgmcjnhkklbkhh51r30s1an")))

(define-public crate-caseless-0.1 (crate (name "caseless") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "17a1lnjd7002v90w3aafll0amgbq51qadpvqh7r5slr3lm3kb8jd")))

(define-public crate-caseless-0.1 (crate (name "caseless") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yvdvmzw86qfsyxfxqn3si3v880hsz6154msrfspb4hcmj33z2dn")))

(define-public crate-caseless-0.1 (crate (name "caseless") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "12jg65q349h4cbaj3jbq72770fzh59mfcmdpihl2qx523fiyjhvr")))

(define-public crate-caseless-0.1 (crate (name "caseless") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rrgnijdamajbc0acj2r5vv7qv41nnl4hwg9vsxdlp7prxsv0l49")))

(define-public crate-caseless-0.2 (crate (name "caseless") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "16pk1jc6cvb1q42nqid8wld38ij894bbnvk69bwv97ad0cw1c9p3")))

(define-public crate-caseless-0.2 (crate (name "caseless") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zrx8rrks6mq08m37y3ng8kq272diphnvlrircif2yvl30rsp3c0")))

(define-public crate-caser-1 (crate (name "caser") (vers "1.0.0") (hash "1bi0b8rxqlizkr1bwmhgnkjj3l7k5a6ap8myfxx0sf9h3wi7pqv9")))

(define-public crate-caser-1 (crate (name "caser") (vers "1.1.0") (hash "022d9jdhq5i3v8b58y1g09li9vpi2packz46dj9b0hbd10g0a52v") (yanked #t)))

(define-public crate-caser-1 (crate (name "caser") (vers "1.1.1") (hash "05bcd7blb77xly672n85z0rqnv807dg2gzli4fjh6f4il8hsyc8g")))

(define-public crate-cases-0.0.1 (crate (name "cases") (vers "0.0.1") (hash "065bi39h4xy4d4h64wxgha289nlb4wglyhkg2pvzbsga26rj961a")))

(define-public crate-cases-0.1 (crate (name "cases") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "1crl74szg9p56mvhnx4fq4ih7wkwzrvq76w5wn1j9pf35w2m1ac5")))

(define-public crate-cases-0.2 (crate (name "cases") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "095391lyj5nyvnh6hvri26criv5zhdq0vxz9nh8rz64vsywp5sl4")))

(define-public crate-cases-0.2 (crate (name "cases") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1093l8b3xwwm4b4q3wvaqwgn4p4l2hjpvpkrdky01sr8cc5a9f83")))

(define-public crate-cases-0.2 (crate (name "cases") (vers "0.2.2") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "0jfjc2pl5p2nxravymq5hggrgwkdpq91zn6wcrnf82bki6byvz1h")))

(define-public crate-cases-0.2 (crate (name "cases") (vers "0.2.3") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1ycr6zhxd2blqpdhfxg3lc7bsvx8rwgkvlja9xq0j5hipbsiac6z")))

(define-public crate-cases-0.2 (crate (name "cases") (vers "0.2.4") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "006z0r8s6lz4lp3ryz7vfc1k1yiw2pq43j8g2mmwjzdc261flcng")))

(define-public crate-cases2-0.1 (crate (name "cases2") (vers "0.1.0") (hash "0yj11h8yyz82h3j0zwa7bjywn3rrr6cqpjq9jw0d90lgfn8yc5cp")))

(define-public crate-casey-0.1 (crate (name "casey") (vers "0.1.0") (hash "0cmm401p2x9issa4jx1z3lb4wj8pgr3kfv941a40pkdivsjdm9wj")))

(define-public crate-casey-0.1 (crate (name "casey") (vers "0.1.1") (deps (list (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0z697gq0als7sq21pd2qj88yk3k365nnkqxgmnqlxmzm9krawzls")))

(define-public crate-casey-0.2 (crate (name "casey") (vers "0.2.0") (hash "1psvwqb2k2cq2j8nbkcsxcr3ccldf03vx93s4l80p367d87s8cfj")))

(define-public crate-casey-0.3 (crate (name "casey") (vers "0.3.0") (hash "12yygps5b4i547q6mppi97kx5sd9n6pvmcmrr40pgrakmwcfxn90")))

(define-public crate-casey-0.3 (crate (name "casey") (vers "0.3.1") (hash "02rlb20kc1qihayk8jdrjy273f5zfh1kn1dy4c1dsjgkqwpgq5ss")))

(define-public crate-casey-0.3 (crate (name "casey") (vers "0.3.2") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "04xrlh00gl1r886nlr2mnh00pl9sv9xpinfhv47w8c9r6kcrxm1c")))

(define-public crate-casey-0.3 (crate (name "casey") (vers "0.3.3") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "057a7x7g3q7ycmq3zcrwzs6iqn5by5ncx0jmf5kz576s1l9qbgps")))

(define-public crate-casey-0.4 (crate (name "casey") (vers "0.4.0") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sv5ll6aavwcigmr53b22dg16adlz4pa2pb73367sna974k8cib1")))

