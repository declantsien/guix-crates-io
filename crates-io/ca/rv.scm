(define-module (crates-io ca rv) #:use-module (crates-io))

(define-public crate-carve-0.0.0 (crate (name "carve") (vers "0.0.0") (hash "0mn6ibc23l474k9wkyg5rvl0j8478jxwl57klj4cm5r8b2q55lqv") (yanked #t)))

(define-public crate-carver-0.1 (crate (name "carver") (vers "0.1.0") (hash "01c81caiwn0nyyaz2zzz7m122cw7d9yp8z4852c8qbf46vr18g8b")))

