(define-module (crates-io ca tr) #:use-module (crates-io))

(define-public crate-catr-0.1 (crate (name "catr") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "06xg4l182q8fbx7h3hzprnqm7sn91znan5s4bkd2jvjh6ndmnzx4") (yanked #t)))

(define-public crate-catr-0.1 (crate (name "catr") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0nkbsbf3lmpfzc0qpq799kvpa0ihzbf5divpbicc56vfdm3mbza9") (yanked #t)))

(define-public crate-catr-0.1 (crate (name "catr") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1qywqqc99hl5dn0gs6n3sbnd6a1qxnzpksndr1rj8qy2ygsaf4wy")))

(define-public crate-catread-0.1 (crate (name "catread") (vers "0.1.0") (hash "04nkvn69xvmy2d8nb86y899501ph1mpz7rzq23ai2myvf31nnn0y")))

(define-public crate-catread-0.2 (crate (name "catread") (vers "0.2.0") (hash "1ygrgdjv3j6sv87by79ajd8405jcmzblman1w5pg82619jnzcvq5")))

