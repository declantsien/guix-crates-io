(define-module (crates-io ca po) #:use-module (crates-io))

(define-public crate-capo-0.0.0 (crate (name "capo") (vers "0.0.0") (hash "0ym75xnph95kf3741cbgfxadp10y5skc9psqpcwya2ap951j3ks3")))

(define-public crate-capo-0.0.1 (crate (name "capo") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "errno") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0h8i6gcf8a30s9ywz74rbqmx73j5f30fsx9s19y0n1iq6dn4i941")))

