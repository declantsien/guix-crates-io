(define-module (crates-io ca rc) #:use-module (crates-io))

(define-public crate-carcinator-0.0.0 (crate (name "carcinator") (vers "0.0.0") (hash "19plclyhflwgmxghwi99j6wxvcllczr9jmi1mrdbm3cp3z2rfiyv")))

(define-public crate-carcinisation-0.1 (crate (name "carcinisation") (vers "0.1.0") (hash "1ps7q9v9kh14m4gfp9i9mhks8fnvcviz2j7kjv73qh4d42lsmr5k")))

(define-public crate-carcinize-0.1 (crate (name "carcinize") (vers "0.1.0") (hash "0dks65blpn53rnwsg42rhixd7774nk1yh038hvg60sy6q8fqqpzi")))

