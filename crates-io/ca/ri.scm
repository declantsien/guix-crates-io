(define-module (crates-io ca ri) #:use-module (crates-io))

(define-public crate-caribon-0.2 (crate (name "caribon") (vers "0.2.1") (deps (list (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "1yrgawnsrzmv9l31gz9wpngjmg4sk2g4abzz11giqxcslyyl2p5h")))

(define-public crate-caribon-0.2 (crate (name "caribon") (vers "0.2.2") (deps (list (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "0h5i36n99cdmwwv7vp0mxp6g81r0nlx0ad118l3z8rqh5fiiag4a")))

(define-public crate-caribon-0.3 (crate (name "caribon") (vers "0.3.0") (deps (list (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "0s4ra3cmr8p6i6564v4r1fzb807wazrvx7z0aygwkgc4x3d1q5q3")))

(define-public crate-caribon-0.4 (crate (name "caribon") (vers "0.4.0") (deps (list (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "1sy2fskci1slc9zm20l4q4bla54f968a3h1s8k7gl5yhwhi8pw2x")))

(define-public crate-caribon-0.5 (crate (name "caribon") (vers "0.5.0") (deps (list (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "0yb3ad5cvy83gf4fx1mjvkz7mci9bipysdv4wwgg5fki7dg4zl2z")))

(define-public crate-caribon-0.5 (crate (name "caribon") (vers "0.5.1") (deps (list (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "1rbzx4izqpwkmchf7k5amjaw36ly4hhh2brp24wv60cjcywafyij")))

(define-public crate-caribon-0.5 (crate (name "caribon") (vers "0.5.2") (deps (list (crate-dep (name "edit-distance") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "07pggrlw6bfjz513ix7apxps3lr6mm525vgnz8xvw2cxzavfasi4")))

(define-public crate-caribon-0.6 (crate (name "caribon") (vers "0.6.0") (deps (list (crate-dep (name "edit-distance") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "1iag7bzmsy334pj3glxxjh16p3rnp7d61cwzf235zkrhbjz3pkkq")))

(define-public crate-caribon-0.6 (crate (name "caribon") (vers "0.6.1") (deps (list (crate-dep (name "edit-distance") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "09qp8aa34a8mlvv7874kikz2w7vnykr1wagjylfx2k44hyclni5q")))

(define-public crate-caribon-0.6 (crate (name "caribon") (vers "0.6.2") (deps (list (crate-dep (name "edit-distance") (req "*") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "*") (default-features #t) (kind 0)))) (hash "13lcj15wgvx4p7c105rl3bs2jjkzgjj3hcqfgv699a4805yc4k31")))

(define-public crate-caribon-0.7 (crate (name "caribon") (vers "0.7.0") (deps (list (crate-dep (name "edit-distance") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nzjpkm2h9qzflcw9ldjkhkdwa7d74qcfxjighss4q66basn4ryi")))

(define-public crate-caribon-0.7 (crate (name "caribon") (vers "0.7.1") (deps (list (crate-dep (name "edit-distance") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mgl6pq99k5h3c9fcjzb8fycgciv3f6yknxaj4admyysynga8wdb")))

(define-public crate-caribon-0.7 (crate (name "caribon") (vers "0.7.2") (deps (list (crate-dep (name "edit-distance") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gwz51mi77b52hgj37cbvvcq5kysd732niq4pw50i5f9mhcimrnc")))

(define-public crate-caribon-0.7 (crate (name "caribon") (vers "0.7.3") (deps (list (crate-dep (name "edit-distance") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "stemmer") (req "^0.3") (default-features #t) (kind 0)))) (hash "19irnvi00j3i1w9k6nnbrbg74slq3ndbrlq42jbfk3w8dmhnjya9")))

(define-public crate-caribon-0.7 (crate (name "caribon") (vers "0.7.4") (deps (list (crate-dep (name "stemmer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.5") (default-features #t) (kind 0)))) (hash "10fwfmi34dfab4gmkkg1i5qfvyg1v1j6zr4xq2xc4lmz38id8qml")))

(define-public crate-caribon-0.8 (crate (name "caribon") (vers "0.8.0") (deps (list (crate-dep (name "stemmer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.5") (default-features #t) (kind 0)))) (hash "1jw8mrlyd5i3dq4sgs5889f5zj3wmc541afl424xdlm0kdrszv15")))

(define-public crate-caribon-0.8 (crate (name "caribon") (vers "0.8.1") (deps (list (crate-dep (name "stemmer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.6") (default-features #t) (kind 0)))) (hash "1bj78bs11r8lsxy20qbhymkkfarixqynqm6rdhx4qizs37qmlifr")))

(define-public crate-carina-0.0.0 (crate (name "carina") (vers "0.0.0") (hash "0kzqap6bhr9qhyraij7gbv7j69qdln5r5bn9vyr4xwfqbb67mdcp")))

(define-public crate-caring-0.1 (crate (name "caring") (vers "0.1.0") (deps (list (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "interprocess-traits") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "memfd") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sendfd") (req "^0.2") (default-features #t) (kind 2)))) (hash "0b5zl3az2ixwh6fhzbgrq1nxcn8w55gxpa8fl2a1lldkayxfwwc3")))

(define-public crate-caring-0.1 (crate (name "caring") (vers "0.1.1") (deps (list (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "interprocess-traits") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "memfd") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sendfd") (req "^0.2") (default-features #t) (kind 2)))) (hash "0kv96302skgxvzsy665izsxrfzww5m8a5cslava4rpsakh26s88n")))

(define-public crate-caring-0.2 (crate (name "caring") (vers "0.2.0") (deps (list (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "interprocess-traits") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "memfd") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sendfd") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1jlys14mx27215mbblqq6jvz9452vgylkhgvq79wz8vfkrqn15p8")))

(define-public crate-caring-0.3 (crate (name "caring") (vers "0.3.0") (deps (list (crate-dep (name "interprocess-traits") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "memfd") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sendfd") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0kx7r9ggw8q4svp3ws03np9jv53k5l50a8dd1b0b33j6z6s7klcl")))

