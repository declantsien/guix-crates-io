(define-module (crates-io ca ul) #:use-module (crates-io))

(define-public crate-cauldron-0.0.1 (crate (name "cauldron") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "02s6pw5hlby1pygxws1arjmivfnnkym6p65zl0f9xf6c0kaq1qcy") (yanked #t)))

(define-public crate-cauldron-0.0.2 (crate (name "cauldron") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0yvv6d3xzwqdw3j73arscz2br3w1h3dggr88xqhvya9avr11qlzj") (yanked #t)))

(define-public crate-cauldron-0.0.3 (crate (name "cauldron") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "10brr2i17xkarf5pziflv9bcvfsgligw590zk1ccmdqzbbf9ad5i") (yanked #t)))

(define-public crate-cauly-rust-leetcode-utils-0.1 (crate (name "cauly-rust-leetcode-utils") (vers "0.1.0") (hash "1i0hnarpdvqjjyis2r2hnfn9kypvhbwfazgsrvalj82xcw1slm0d")))

(define-public crate-cauly-rust-leetcode-utils-0.1 (crate (name "cauly-rust-leetcode-utils") (vers "0.1.1") (hash "04yzrbmhlyy7im351ysas01ag2ah0hy7z0zzbq12xwyq1zpy9zxl")))

