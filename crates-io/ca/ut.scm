(define-module (crates-io ca ut) #:use-module (crates-io))

(define-public crate-cautious-octo-funicular-0.1 (crate (name "cautious-octo-funicular") (vers "0.1.0") (deps (list (crate-dep (name "mdbook") (req "^0.2.3") (default-features #t) (kind 1)))) (hash "1gv1x3nii7q5vqq5ip134xagpzd9gwarsqkmfvhzr28p1rz1mv2l")))

(define-public crate-cautious-octo-funicular-0.1 (crate (name "cautious-octo-funicular") (vers "0.1.1") (deps (list (crate-dep (name "mdbook") (req "^0.2.3") (default-features #t) (kind 1)))) (hash "077l8nbqbvvrrgz0xd4gbv4dk352i9wqfmiygil7wmx1ciirwgr3")))

(define-public crate-cautious-octo-funicular-0.1 (crate (name "cautious-octo-funicular") (vers "0.1.2") (deps (list (crate-dep (name "mdbook") (req "^0.2.3") (default-features #t) (kind 1)))) (hash "1wgcl0fs8xsfskc4xv7w80n1jpj4d0vrsjdsj8ni321w7nrfzni7")))

(define-public crate-cautious-octo-funicular-0.1 (crate (name "cautious-octo-funicular") (vers "0.1.3") (deps (list (crate-dep (name "mdbook") (req "^0.2.3") (default-features #t) (kind 1)))) (hash "09kswc8yp666s962vafvb4ymnakhc8qv5ghwl5hmzpil5l6y1b5h")))

(define-public crate-cautious-octo-funicular-0.1 (crate (name "cautious-octo-funicular") (vers "0.1.4") (deps (list (crate-dep (name "mdbook") (req "^0.4.7") (default-features #t) (kind 1)))) (hash "0fx35dy1xj0cjwawrzgx3lzf2kw858371ym6gpahy2riwjmffgxs")))

(define-public crate-cautious-octo-funicular-0.1 (crate (name "cautious-octo-funicular") (vers "0.1.5") (deps (list (crate-dep (name "mdbook") (req "^0.4.7") (default-features #t) (kind 1)))) (hash "1nmwkn4ajqk5lwhjrb4fhx9dig1y6n3g521qg2gvq9fac6c2hrg8")))

