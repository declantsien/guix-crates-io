(define-module (crates-io ca rm) #:use-module (crates-io))

(define-public crate-carmen-0.1 (crate (name "carmen") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "11nj4ii0y0axp0dav0kdxrmji9ykplvwdgb7icnasm4dk9qm3x3c")))

