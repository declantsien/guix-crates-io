(define-module (crates-io ca pn) #:use-module (crates-io))

(define-public crate-capng-0.1 (crate (name "capng") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "0jfh1lxkbvanvjbcdc4mpkpf1v23m3qhfi5lqcplq0py9sa4xrn1")))

(define-public crate-capng-0.2 (crate (name "capng") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "132ac9daps6f8sjby2rp7ijadhzvyk73aki41n8imwgli00z1h8n")))

(define-public crate-capng-0.2 (crate (name "capng") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "1f00ragbgdn43dgy2kfi3hdv702s1yh8bqs8x8sgkyprj2piriiy")))

(define-public crate-capng-0.2 (crate (name "capng") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "0x95j3fn8f2ra6d63rxk2phd9y7mjlfi41i6wi1kcq1kh92fky7n")))

(define-public crate-capng-0.2 (crate (name "capng") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.69") (default-features #t) (kind 0)))) (hash "11cyx3xyk226w9ayi7915p8zas1az8j2iv89hf5pwzzhjdppc9ks")))

(define-public crate-capnp-0.0.1 (crate (name "capnp") (vers "0.0.1") (hash "1gsvw0s5hmjs223ya4r8qgl5yjsgjai7kxh8gwd3312i7znlgcwc")))

(define-public crate-capnp-0.0.2 (crate (name "capnp") (vers "0.0.2") (hash "1nw8kkf7dnf96nh301kwgni30l21g1qb69w4jcd9vgrcg81i0c4h")))

(define-public crate-capnp-0.0.3 (crate (name "capnp") (vers "0.0.3") (hash "1dvhjznhkbj0z6alxmv38imwj2s2m6fn9y62qn1kv8r52z2gz3qn")))

(define-public crate-capnp-0.0.4 (crate (name "capnp") (vers "0.0.4") (hash "1pm03rpxk4dakm1iilsxsqy0x0d01knarm1zcayvmpb3rc8ilvnv")))

(define-public crate-capnp-0.0.5 (crate (name "capnp") (vers "0.0.5") (hash "07a7nz8zrls41vnny9nyd2ggj83xh7ncnhbfzc0mpadz0l0nnpwj")))

(define-public crate-capnp-0.0.6 (crate (name "capnp") (vers "0.0.6") (hash "08j479r7sp1n2ghdcmfxbjrvdqnqkll3brg56l30vp5shs2z8lnm")))

(define-public crate-capnp-0.0.7 (crate (name "capnp") (vers "0.0.7") (hash "1jm6k5qhr28c1bi3brbb9hq48h8kcikqz080vcsb1ga00pydp6nv")))

(define-public crate-capnp-0.0.8 (crate (name "capnp") (vers "0.0.8") (hash "157xx696k8i5psjfbmswn8fvr0vn3465cf38ld9ppy30k5576gh6")))

(define-public crate-capnp-0.0.9 (crate (name "capnp") (vers "0.0.9") (hash "07gxggc6n7an9vsdy0i1yvyfz1mpihxag442yw1mriani22jvaw9")))

(define-public crate-capnp-0.0.10 (crate (name "capnp") (vers "0.0.10") (hash "13s56axb4vz7xq2697as3q9mlyigb7msjsqdlxmwcgqrk6ackbpp")))

(define-public crate-capnp-0.0.11 (crate (name "capnp") (vers "0.0.11") (hash "1vb8kakqmb71q4jlbca86dadhg1zawkadlaf72mlsjb6nxb1h6j2")))

(define-public crate-capnp-0.0.12 (crate (name "capnp") (vers "0.0.12") (hash "0486gbs45n961f7ig9zzkdg5d8ld5bywz8mg0n2n3ylx8r1q1sq7")))

(define-public crate-capnp-0.0.13 (crate (name "capnp") (vers "0.0.13") (hash "1flcs3zvifayxkh8j6yvxdvliixrkr6acl44vlcm6sgnr1ap8qhk")))

(define-public crate-capnp-0.0.14 (crate (name "capnp") (vers "0.0.14") (hash "1mnn27m7h7i0xwsq6kdhhp79dvl52c6zs6r9ik46ja8as0wmcicn")))

(define-public crate-capnp-0.0.15 (crate (name "capnp") (vers "0.0.15") (hash "0gjc4f6sjjhjgxnmas4fxvmgpgsqshbdakqb4pa1mxy3s035vgdr")))

(define-public crate-capnp-0.0.16 (crate (name "capnp") (vers "0.0.16") (hash "0v6j6pfdgslrdi9s1079pnk7vb298x8hbkpzwhibq7ssp0l9b2xa")))

(define-public crate-capnp-0.0.17 (crate (name "capnp") (vers "0.0.17") (hash "08jj00aq9crsz5f85xc7hbp3hmrlywzl20bqpxz1j58rlhn09v23")))

(define-public crate-capnp-0.0.18 (crate (name "capnp") (vers "0.0.18") (hash "00hkvc4nff7swvkxyl06i9h9w18daswyram627g9gzspgwmk65s4")))

(define-public crate-capnp-0.0.19 (crate (name "capnp") (vers "0.0.19") (hash "0scwrw9d6bqyszwh2pg7cl7bhr450myyfqxnx72nkw5c9g9rlwxp")))

(define-public crate-capnp-0.0.20 (crate (name "capnp") (vers "0.0.20") (hash "13k7bhf8rwswd1g4bljxhcnw4y8awih0mnhz835p10gc2sdawyvk")))

(define-public crate-capnp-0.0.21 (crate (name "capnp") (vers "0.0.21") (hash "1dc3alw8lj4f6ngfrc3m0mxsi01hj9qsa9dlkq3kmja5dwisfqh5")))

(define-public crate-capnp-0.0.22 (crate (name "capnp") (vers "0.0.22") (hash "1yy9nsk1x9ba0p533h2pyclfjkmxbsb9dwmpv2qazp9bllpahnp9")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.0") (hash "0afa1crlzy3lmnkd02zvs4wnlrmxkw76qz48rjzirzvvchnhfpj3")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.1") (hash "1pknxs6xb1fba0gbn2br5m35072m0nwf4b46jbnkwyaabf5vf0dq")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.2") (hash "1mc0kpv5vc9s0r59sxnar9fibbsasn6d3p282b9rk87w7iignvxd")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.3") (hash "1gsy16v9qvcfsgziak5yb1rr69yasmrfhzj0vfylifdqg5fn3zyh")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.4") (hash "0wiw0v5lh7hyf9c66pgv9fhqdgbh81dl1ikxf0flzj2yix8rla55")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "19p4wxiwd1r0a3vzm8kc8hndhl1x8bj86mqy185vawbpqkl8m0g9")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1mbzvnca1yqj1v4j7zsg0gls7p6j9qh3jfac2p0cgrjidhhzas5i")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11vdzp80xadr1mvg371ww7qmfx33dp75sjjhvm1ds9fcj6r2cl06")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.8") (hash "0c556q6kcg39dhkjazsg7s7qjmh2f8hp1la88hgyrpnyzs47mb7x")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.9") (hash "0kvz5ja29w7zrc4xl3glwk4vla1fg0h3pbzfcvkjfi3c9a5k06mv")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.10") (hash "1kprdnbi5rbr59dga0ax6d22zaaa0vjids4abcm4avby2jj27nnp")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.11") (hash "0z6byvvwfis5whqyf608h2yyyzl6nwwl33sydln6zfya3j3bz8wk")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.12") (hash "1aw4vc05wxg013akabmxyb674mgi8w7apkmqm0xp72g9g9b8ripg")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.13") (hash "0976w2w4kj2nsqi5x9g98swhrikjkcc6d257jkajfvy9r5f5k04x")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.14") (hash "1n9qfd2sw49aav1bx3jm49wk3rxp5pmaxqyp4jqclrxv890bfavr")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.15") (hash "1ax4l6440wdc0lykq4k8nlnkh03q14ynmzqvlw4pqfspnnqm4s6f")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.16") (hash "011amqp2ab3l6m38pkq16x24yw4mg4ijh2sszp6fdqicxa2bvbzd")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.17") (hash "0y73h39rna8frzbhcms07i6qkriabshd4wsn6pn9ivjyv53fmziy")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.18") (hash "0n6jfja1drgfpi0ikknxnfb44b5schb4m5xgf2xhah4j2w4vsra7")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.19") (hash "0if15ngx4qd07wvvrcjf28q377p008yxkzlhydsr7kh1nig1jag4")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.20") (hash "1gr8gg5p0f8md04kz91wwzq1iyiaby56psjjd8gyxy0w4kr3x7as")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.21") (hash "1rcd57snfp00iycxm97l8xk2p5ip2v2xzcjb5s3dinpdhscc6b7d")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.22") (hash "08b905am9pylczcapwsai3vx6mw6jbal65whm42533rh49905pcg")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.23") (hash "10ad63slrs1iqcbh4fqyq4bi1k8pgh2bhnvbxj3c1jj4c4jwinqw")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.24") (hash "01qibk9qaf49r5nl6xq9mlq3jvyb7pvd5djc4sjc4ai7xhn6n6mj")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.25") (hash "0ssyfv85w2h8vya8ckgnpfy7zdsqcg9s7fh6ka7a8gv9s4qwj209")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.26") (hash "0anczb59h1zbyzpm1253pc4bzw9b7qpb2g63kbnl2c0gf6brz094")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.27") (hash "07byb0a6i3zygp51pbv2yiwpsdx56rwkzwrfi4ijbniyd79yn7kg")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.28") (hash "16pk8cyjg749ji8clf6v2nzbj17nicq2g49gxhjf8r4f77sg75qq")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.29") (hash "0xxyzwakn381hls1pf1dxvrgild0s2v7kkrp5izag3yhpjn61hhh")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.30") (hash "11l6xaix3y8r76wn0v0khxm5933wfp8g2vzh3l2bfjsspdfyirk8")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.31") (hash "0x7zjc6z2ni3gvzsm5pdl039cg890bd13l2ssvw6900za0gm16ra")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.32") (hash "0453imx2pq8wrjc145wvgc604hh6yhribxm6fjia5n2lhrz48ash")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.33") (hash "1czicxqym2yc09zxf0b8bnfx9min8d3qpn8wdpx21d5mn71xdmvh")))

(define-public crate-capnp-0.1 (crate (name "capnp") (vers "0.1.34") (hash "0rfrqzr52x8a13535kbmrvf5am47p8zhm1m9xzb5l7y4w853mky2")))

(define-public crate-capnp-0.2 (crate (name "capnp") (vers "0.2.0") (hash "13bv3nvflz55cxzmvangdrcpskl91kjr8s6x2zmqzmz8dns5470j")))

(define-public crate-capnp-0.2 (crate (name "capnp") (vers "0.2.1") (hash "06jabpvhnlad21zhv4w77gkq2fj9b9x6c0db9jhhvfc5lmi4hw28")))

(define-public crate-capnp-0.2 (crate (name "capnp") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "18q2hzvmzsmibqrmv117lpzh592lm9bqapv2ssvmkbdvq0fr07yk")))

(define-public crate-capnp-0.2 (crate (name "capnp") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "04pscpq12f6bjdj65f16rqcwkpgcn0n6557hhn15dhgz6lnp5yxc")))

(define-public crate-capnp-0.2 (crate (name "capnp") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1nb62ff494xa1dnydkm6c91jmp8l0qxa8wqc610h55l31aqnxhch")))

(define-public crate-capnp-0.2 (crate (name "capnp") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1wky6j9dbf86mg3gfvrcfwi209c9f1s1inisf5n2lsm01xbbp0la")))

(define-public crate-capnp-0.3 (crate (name "capnp") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0v3xcrvbhw800k8vps8jpw4ng8z95vg93ahi7ag7djnf2vfnc0vw")))

(define-public crate-capnp-0.3 (crate (name "capnp") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "04cc1pxk7k7z9s856ck8hpjmi4yb24d1hlll0zn59cgy38scv7j5")))

(define-public crate-capnp-0.3 (crate (name "capnp") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1mfdrnw7r8v1nh93mgp83n6qf3y31r4sgm2glaxz2kn8in69q933")))

(define-public crate-capnp-0.4 (crate (name "capnp") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "00wccm51f8gf671r1cvlkp0h958qjcpzpyzqz48fw0p8qr7hwy6s")))

(define-public crate-capnp-0.4 (crate (name "capnp") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1j0yimpfxha4629xzc9c5xsncakg9j9a3awcxwvdhjwxrg73rfw7")))

(define-public crate-capnp-0.4 (crate (name "capnp") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0wwd9x3hd8wszcbzj3k3wb68bwww7vfv5v88in8a9vpi1wnl7zcd")))

(define-public crate-capnp-0.4 (crate (name "capnp") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0xdp3winq2kwxy5sdz7y1z14ppxijd5n2xv50xgxxyx1dcnx7y2n")))

(define-public crate-capnp-0.4 (crate (name "capnp") (vers "0.4.4") (deps (list (crate-dep (name "byteorder") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "08zq9mnz2sb6mw1ix71k0dy8snf53d4bqqmqab8z54sjxvybdcmb")))

(define-public crate-capnp-0.5 (crate (name "capnp") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1vpy1qkglxwl7i09svh9973szbm5mz4qqp81c3jfcpzzppz2fvgc")))

(define-public crate-capnp-0.5 (crate (name "capnp") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0123frfbx5dxcl226chy2kbidadlpq44sqz4wd93r4wv7wxjcrs9")))

(define-public crate-capnp-0.5 (crate (name "capnp") (vers "0.5.2") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1pw6yvr1gdi39xz2ybzwvkhbk7gzca7dfvisqg57dsvgqb3iblzd")))

(define-public crate-capnp-0.5 (crate (name "capnp") (vers "0.5.3") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1qmxbxickbp08x7794p8pn2p8jpyy1ykh1fkrkbabni492z6wrmp")))

(define-public crate-capnp-0.6 (crate (name "capnp") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "054frx8h8vb7l1kpwizaj13awcav9h9ybwjg5j3mnkwgrk42sv7i") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.5 (crate (name "capnp") (vers "0.5.4") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1dx2d5fal2zx1mmws6kr9v64sfg3qms9ri204amh0v7984kp0fsc")))

(define-public crate-capnp-0.6 (crate (name "capnp") (vers "0.6.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "141ak24wdp5rjl6snswva3fqqrdpidrpniym8y3gwd7fipcrpmjc") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.6 (crate (name "capnp") (vers "0.6.2") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "09d64dkl8dyy6blckj1b1dm4ycpxis6h8nrwznsn6n6izrdz04r4") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7 (crate (name "capnp") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "13qyhb8gxkf9mp37x5i6sf22a5xxjclpzwkvyxkia0k3v4rh485l") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7 (crate (name "capnp") (vers "0.7.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1r10cxpwrwpd1j4wmyy3ma72l6hzkkjbb7mc56fsbahg0xw3xzn2") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7 (crate (name "capnp") (vers "0.7.2") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1mx7aklzy1j3hwamsy4z5jq1a7zcci61z24s3gdb25z3nij1c3in") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7 (crate (name "capnp") (vers "0.7.3") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1mh7x8pwh2hq2y9x1diq19jx5b4qccl6c2n2ipwr3zv9h91i8wax") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7 (crate (name "capnp") (vers "0.7.4") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1gf1p9qaxjzymj2i60xyim3lhg0ia3n4vrcjh0cklxirp8dnlv27") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.7 (crate (name "capnp") (vers "0.7.5") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0zjb0hx7fp06hpkbig5wdwcj0yjvd3kp609bpn46h0hi974n8lmm") (features (quote (("rpc" "gj"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "074bnhwzwq4qgl0i8jjxkc8p5n03w848kkwzsiz6yhxw12p6w7x2") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "14x7cvgljhgl0milcx9r41wlsgjlkvgymv0w0cq3scf81l23x4lx") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.2") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1cy0ada1p9m40pw5fmlffvrh9k9xia85iwxlp809kp9v92digixl") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.3") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1w8db8p524022ammvdzb7xpi5xcq6ygagddycyk5wjj7cnkiv0dw") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.4") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1zbimyc73k1lv4jj4l6wi9hp8px4nb1nqfj1mid793qjnw5i14nl") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.5") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0b2v5c985v8vgmzm63ah3iy28g019aksgpl8kzdh20mbkvvh4w9v") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.6") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1b7zcxfjv8qrhz64c75mp7hhj8l5p8d5nz54dndl2gk73y5221b0") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.7") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1gly9yyb8drarnw2340bc86vjab8s6y9biqlbw4x16gidl9649sx") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.8") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "180zlnj84anmp556ygsxk2jk5zdsww3s9gfq431bh9vcn3f4rd3w") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.9") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1xakjbx99jkjb9n9f4sy9aq55vy582snc85v007ym9ly14khp85c") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.10") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ibsaallfnzywk9364bn3bnw0ikbbcj53fih1ycqlgd9c7a2c9f4") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.11") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1qbhsjskqgz7vygwx9qiwx5b8lzvbx4g0f47kdzkk7r52jyyyjbc") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.12") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "01bzk6810016i55y1rschn1y9h8w1j7pvqpy1yrf2k25r5k35bvs") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.13") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "08shfan4kicvjmdqg1j98qgd17q0g9p9lxj57f4rhb1ki8ic94hl") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.14") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "00jx57qvgm4x3v48cxvl1gmq86lhd73mi715qakh4nrig59kbwz5") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.15") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0vqgj8hrh7pj1mq3hx420xh75a5xgd8icx60v5g78in8cax16jbs") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.16") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "06cs53sabmfm9n4ac3kinbymxmggcnyks05lq8s378wxhlnp7wa5") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.8 (crate (name "capnp") (vers "0.8.17") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0qhimabww2pqcp4g89m4j34mbzg1kmvav67d6v6mxlan2vbw6rb5") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9 (crate (name "capnp") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "106d3rlg0rkrrnaiqr3wj6zs17icr0ks8sjq2dvldaxfjmvsbkxk") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9 (crate (name "capnp") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "09921jvh8snfjhii0721zag1nrqdrk3vb96gz96a5b53xb8lia6q") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9 (crate (name "capnp") (vers "0.9.2") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1fdwaknpsgb3hkqah5n5gby19x6nsn5w34fpzxm4ld34mbvbkbrh") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9 (crate (name "capnp") (vers "0.9.3") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1m6wmyh0awqrdkjjr1zzal4n1256r2qhcbbgpq8d3zsgqz788rfy") (features (quote (("rpc" "futures"))))))

(define-public crate-capnp-0.9 (crate (name "capnp") (vers "0.9.4") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1j7ab3zxfxj4lv11n673l1gljyl8v6qrs1rgld9n5sfsm7r0jf2z") (features (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.9 (crate (name "capnp") (vers "0.9.5") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1iqnhzsxj954zx4wkim2h4amri6j28fbcwrns6gnfkvnibhl1g2h") (features (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10 (crate (name "capnp") (vers "0.10.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0kvgvp1j38vskg04binjf7g4r9sz40wisa99sqr241rl24fyxpzs") (features (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10 (crate (name "capnp") (vers "0.10.1") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0fhj9vspmw1dljfvx45gbc4h8scjbfdwdqdp3i9y7g4182cdqc9s") (features (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10 (crate (name "capnp") (vers "0.10.2") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0530cbhfs9sv8pap78ihc11gvmcwq963hsskqq0v7d9r52g68wrh") (features (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.10 (crate (name "capnp") (vers "0.10.3") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "17hsnmlcrzksjjpwpz51y8g36xzq8042i2cwns0lsg7rixfw2rxq") (features (quote (("rpc_try") ("rpc" "futures"))))))

(define-public crate-capnp-0.11 (crate (name "capnp") (vers "0.11.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1hjrfg2lnmk6ysqssj5nqmgrycpw2awhbswslb8wbrrdwg39xx2r") (features (quote (("rpc_try"))))))

(define-public crate-capnp-0.11 (crate (name "capnp") (vers "0.11.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0wk1446g5yv4ik9ykcs2c34p6zls9b6l9xp2daf5dvcf5c9s6iwd") (features (quote (("rpc_try"))))))

(define-public crate-capnp-0.11 (crate (name "capnp") (vers "0.11.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0aj7zvdsrdpk8bwmlj09a8ns40y36112jk7grg0in80afybglnik") (features (quote (("rpc_try"))))))

(define-public crate-capnp-0.12 (crate (name "capnp") (vers "0.12.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0xgdy0jp50aiinylal321x8ayv30wcdm5r9v9b753q8i4qj0shj0") (features (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.12 (crate (name "capnp") (vers "0.12.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0s53f90sbnnr59h56n864vxkn2kllqpr2r9kdhi4pnhbhpq8hb71") (features (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.12 (crate (name "capnp") (vers "0.12.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1jdyr73qpb2jpdh412kj13bra5qm636qj3j36g3yhx8gk9xz48gm") (features (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.12 (crate (name "capnp") (vers "0.12.3") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0vnly50b8l80ywvlxqjh0aqblxs123prk1k2cbf5r4327n065z5r") (features (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1m6h79jhqmqgzsxi37231wysdra72zlnv1vnlyj4803rgri9ipvn") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0ahif19nf6ja8jbchrwykxhmzwglp212vqcdyif5zw3ijy1dqz95") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "139slq5zqwma5ics7d44rpg8v4x977cc37vabmxgq7cdkrqbvn9i") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.3") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "18d668hysl099mkmfhvirrha7b8iywdxlg3a25jdyjcf8l3ad7vk") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.4") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0rygm9zl6fv101girxx7g2l1wk82i8wlc4avybzsam5jg9594w0r") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.5") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0p8hja2xh0czdjlavwqia12jzvrxs07jgsnkr10kvm34fzc9lkrr") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.6") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1bx2pj0ysmyd1ryd5s43m3c7n5k067x1aciqiiyg4kjswlcs6xjf") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.12 (crate (name "capnp") (vers "0.12.4") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1gx64cd2zwr46mbzgcvp8d35bby7r1w6r6p012b1izv9bzhwfvbh") (features (quote (("unaligned") ("rpc_try"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1jmcx7gmgbyxi1ma3zd21m6rl1dxyrg2xvp0wsd5zlqrjhxz2czf") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0kyiyxg202ya98yw3rg74biw5x7cxfbf2kncg80p0rmqxvmcr77d") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1mldpiv3mqqznwvrh0mprwidg6xwvacy5vvxhg60k46ljscwd82p") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.3") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0z79pr9y3zq3dyi3aqfz0m3cynz17rayxd62vmwm42s235qqm6xf") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.4") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1lfy5ymj6barnw6p69zsdrhdcbamfrg3b244p27xwz6qs8mbb52h") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.5") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0rd0y5mi00mmaw4kyg6bwp7ivxds8raplgws4s9864b8dxr65hhn") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.6") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0s6y6j4bririm6hhq69n6s98zk132m7s9ykj0rrg2iiijzddgm91") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.7") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1x7240vww8c8zl2b1w7b5mg966fl83i21kmchllsfwndzij16kp9") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.8") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0shsygnbjl8n93bpaschbw9r807cc0jhajrkhsvk4zjymfqa7vw2") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.9") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "05b0pbk3zblcflv4dr3j01g4vkraaxmvhvz8x6whwdxfzfxxpddi") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.10") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1wxajl7xsdhwxpwcb509ak8q47cgingwqknk6hn2ib36vhxx7wnx") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.15 (crate (name "capnp") (vers "0.15.0") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0j60l41rjkk7wbi8nal70nxn43rbcfly13msw82nhc23qlshg18f") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.15 (crate (name "capnp") (vers "0.15.1") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "13dcjgsz0wcq7r78h8lx84sb0szsrlz8agbrb4ks8nn5xxqrv4pl") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.15 (crate (name "capnp") (vers "0.15.2") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1kn6nv6yssyxysgvjqh48xrzck0zrbihw74cc00ffls5rzfi9amg") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.14 (crate (name "capnp") (vers "0.14.11") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1wwp619sib07raykk93n6j7amvsinlchnicxfjnnb7bx5if0ijid") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.13 (crate (name "capnp") (vers "0.13.7") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1xkfv6mm38y4d6ykxmvs1pdfywnn3n5bklsqcsz9xh72i9ffd3s5") (features (quote (("unaligned") ("std") ("rpc_try") ("default" "std"))))))

(define-public crate-capnp-0.15 (crate (name "capnp") (vers "0.15.3") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0m0vld69w45f2dgcmvx2d99z2bz9g2yv5l0zsxifsa6jg2mzfikq") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.16 (crate (name "capnp") (vers "0.16.0") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "08m87lf5mzl809y5mbhzq79wqsxsk8ccmgj3d0gakwamrdm0qh1m") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.16 (crate (name "capnp") (vers "0.16.1") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1zxvccbqmllydfnhxgzvjd15q7lwnyqi70hmv8yn5qx7mjws3v0q") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.17 (crate (name "capnp") (vers "0.17.0") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "17f237m6xn634dji18bg67vmr6r3km7d0h0iw78n27b0s4rd9qhk") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.17 (crate (name "capnp") (vers "0.17.1") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0pdzzcvn9j626bmj3036pgzp5pd8f7z6l58i78dcg3jiyvpklm6f") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.17 (crate (name "capnp") (vers "0.17.2") (deps (list (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0y6nbxd19miz9z35kfz8n0hxqjrcxnf7i497gkzbnl4jv0hm1rlm") (features (quote (("unaligned") ("sync_reader") ("std") ("rpc_try") ("default" "std")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.0") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0kjbqklcqh5kp97y29r9gxdvbhj1ppz413dlvmg5mj1gg4jbjjx0") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.1") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0xkz5vpm10d8kvqdmgyxlhbg59zy5yj73s195ni2lx6rkdrbvpcy") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.2") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1hw6c9wmqi9n80fa3nax2c8ggc02rkj9gx531h20fxj93i2aj4n1") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.3") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1r2arc5z1382fxa2rpn7ihw7n1q00wrpcj7s4flbf69cn8fym729") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.4") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1q2ag519i130lrmih49b6icwmvcdfaqns1bnjvxl0vsg8ac8avlx") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.5") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1xibw9vnjm7silis9666c3axd4c35xnqm8995mhvk4c84crd1x8b") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.6") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "16fnicp6yjvmb20l2lqyib8bfmi61ndzniyxn33541x4d03bpf6g") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.7") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "14a0w63gr4bq8fyn0wyhl4xv0ca3ab0rnnll1vm8vlva3cxklfnf") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.8") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0qkp40kxcfbg1ci3whil79qb81rg3da6gzkp1f9qqn3pnma85bjw") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.9") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0vqx6nq6p32a9shjj5min80s1ask5w4dvbpwiic0r7ipr8ifid6z") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.10") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1w73akdaqxlsb1i3xvdvklp0ajfqwr513dd3jpicim6ncx4l4y0f") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.11") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1g359r16q3wd8r9n3afhc1jpmvx1ywz3sbxjz26bhv0vnfrrm5gj") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.12") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1hbdd5v29m1xri32m4ax959y6dq7wa1gd480damvfqlxvpwcjid5") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.18 (crate (name "capnp") (vers "0.18.13") (deps (list (crate-dep (name "embedded-io") (req "^0.5.0") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "19246srp8rpbx55yclg64rjl83a05b982aqckf8dpsirbcd6fjrq") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.19 (crate (name "capnp") (vers "0.19.0") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1csaxfqd1s8kc4zysrbf6jk94537v8f7bn9ijlrki7nijb1ssmam") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.19 (crate (name "capnp") (vers "0.19.1") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0whwpigh2my89jsam841q2jc3zn5p6skqkcn6pvx4cxfjqgxsmsz") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.19 (crate (name "capnp") (vers "0.19.2") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "004fx630950msmn3q9adz290gajhwzd4bx1mm2lplna0hl4bggxl") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.19 (crate (name "capnp") (vers "0.19.3") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1v90fxhhwgcszxday345rmjp3nicpnbvshnccd55qsbszgk3465i") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.19 (crate (name "capnp") (vers "0.19.4") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "05wpj2mlxnmvapsp9zc0m37gh491v46m9zvq67xg4mrq0q5h0z2y") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.19 (crate (name "capnp") (vers "0.19.5") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "05ydz1vi0a8jllwzcsv9i6yczb8kbwwl0g47vazw1csl44kqbv9s") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-0.19 (crate (name "capnp") (vers "0.19.6") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0spngfp2cg25f66abfv1h2hj05j152bf06f2nwydviya29wkhwfy") (features (quote (("unaligned") ("sync_reader") ("rpc_try") ("default" "std" "alloc")))) (v 2) (features2 (quote (("std" "embedded-io?/std") ("alloc" "embedded-io?/alloc")))) (rust-version "1.65.0")))

(define-public crate-capnp-futures-0.0.1 (crate (name "capnp-futures") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.7") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1gilrnvfpymzd8azsazilm0cyibr2ch42k4mp0zy3czvjaav5gic")))

(define-public crate-capnp-futures-0.0.2 (crate (name "capnp-futures") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.7") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "16xlnvijbncfx0y47snrv4h0vcjik0sqiqami621nr10mk0dnh70")))

(define-public crate-capnp-futures-0.1 (crate (name "capnp-futures") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.8") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0zan0sfpg7c1ar7sms6phvb3p5x8cvrrb7bw5kbb417n01dai4pc")))

(define-public crate-capnp-futures-0.1 (crate (name "capnp-futures") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.8") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ypd3fhz5hpr162q2xakwp2s8imfs5wgfsqpb6pjj91j7g9gj4hv")))

(define-public crate-capnp-futures-0.9 (crate (name "capnp-futures") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.9") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1406i6x4x45yxkp9a12b8628xm49dgrc33gwwfnwwyr4j2g7czr5")))

(define-public crate-capnp-futures-0.9 (crate (name "capnp-futures") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.9") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0mcjzi0zk6bqlhlb8m51f2h2xzpgc095sw54dwfkskhqqismq640")))

(define-public crate-capnp-futures-0.10 (crate (name "capnp-futures") (vers "0.10.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.10.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0m6fhhkv6bqr40fn5hnnh1k0rx7dxv424ihc1y6zfkd11lrb5df6")))

(define-public crate-capnp-futures-0.10 (crate (name "capnp-futures") (vers "0.10.1") (deps (list (crate-dep (name "capnp") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.10.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0qdiqkp9mh4acpa0dqhpzv2gwf949rj3m85mgwl1rih6gvgbh1zs")))

(define-public crate-capnp-futures-0.11 (crate (name "capnp-futures") (vers "0.11.0") (deps (list (crate-dep (name "capnp") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.11.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1zfrzjc908xl697j32dglyp6wyp36hwmla421n82jb57jzjjbpsz")))

(define-public crate-capnp-futures-0.12 (crate (name "capnp-futures") (vers "0.12.0") (deps (list (crate-dep (name "capnp") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.12.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0dgd709ck6hh9gf88izibcdi039qqygzq6c092y6r23pd0gpknz8")))

(define-public crate-capnp-futures-0.13 (crate (name "capnp-futures") (vers "0.13.0") (deps (list (crate-dep (name "capnp") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.13.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std" "executor"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1hl0x954imbqiv1ka7bj2syayfqmkfsnnqvypijdhvkna0ax5sqj")))

(define-public crate-capnp-futures-0.13 (crate (name "capnp-futures") (vers "0.13.1") (deps (list (crate-dep (name "capnp") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.13.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0nc4ikxhdnlq3jchz3s6cjnv7n5f47p7y55wxkby0v88mqfzzyg9")))

(define-public crate-capnp-futures-0.13 (crate (name "capnp-futures") (vers "0.13.2") (deps (list (crate-dep (name "capnp") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.13.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "18q93ncbfcnjq7zhvy9idnifypmi2qcp775q7454y3r4lvvdcyyw")))

(define-public crate-capnp-futures-0.14 (crate (name "capnp-futures") (vers "0.14.0") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.14.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1y7f8sf638q8w04806nwvhhrb91s5hzk0ax8r4nrgx7hj94h2j2k")))

(define-public crate-capnp-futures-0.14 (crate (name "capnp-futures") (vers "0.14.1") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.14.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "10zrpagd3iw97dkdl9qx4qsc3bgk3yzlzrzyqniqkcdciy6285aa")))

(define-public crate-capnp-futures-0.14 (crate (name "capnp-futures") (vers "0.14.2") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.14.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0fp6lr04w50mzfpxvvrbdm9pny8ch17514y7qgcsk6giqqf808cq")))

(define-public crate-capnp-futures-0.15 (crate (name "capnp-futures") (vers "0.15.0") (deps (list (crate-dep (name "capnp") (req "^0.15.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.15.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1l6c6x307ldb7aygpqamvp2ndd05jvnmyvsvv8j2f8phd4acjq26")))

(define-public crate-capnp-futures-0.15 (crate (name "capnp-futures") (vers "0.15.1") (deps (list (crate-dep (name "capnp") (req "^0.15.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.15.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0dwiryfrv274wcw4q9bznvl0vpmm74dcqq5pc10chlfscigmvpdd")))

(define-public crate-capnp-futures-0.15 (crate (name "capnp-futures") (vers "0.15.2") (deps (list (crate-dep (name "capnp") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.15.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "022zzda8394sasdlpnljzlsa2dgcl9w7grirqnk9ryrdmvyc8p37")))

(define-public crate-capnp-futures-0.16 (crate (name "capnp-futures") (vers "0.16.0") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.16.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "05vsm5yvx5a6k4cv6hvkx8hl77ipf6lkszd0351d9ik7srwj9gkc")))

(define-public crate-capnp-futures-0.17 (crate (name "capnp-futures") (vers "0.17.0") (deps (list (crate-dep (name "capnp") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.17.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "163iq48mngz5rywj9amqya39fkzfhx4hy8vz6pg952r2mzh21mbi")))

(define-public crate-capnp-futures-0.18 (crate (name "capnp-futures") (vers "0.18.0") (deps (list (crate-dep (name "capnp") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.18.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1bx827s5na5yqpkl78y31wspvyn70m44a6yqrly1qka5x071jcfi")))

(define-public crate-capnp-futures-0.18 (crate (name "capnp-futures") (vers "0.18.1") (deps (list (crate-dep (name "capnp") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.18.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0qm7w8i594lk5gkz6wqg6542mjkqw5dcqv3kn3r6p883vh2sbry6")))

(define-public crate-capnp-futures-0.14 (crate (name "capnp-futures") (vers "0.14.3") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.14.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "16q04fxbikxgla9nd8v2fzl6x7cz0lbr9shssb38bk1ikif13mq6")))

(define-public crate-capnp-futures-0.15 (crate (name "capnp-futures") (vers "0.15.3") (deps (list (crate-dep (name "capnp") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.15.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1gvfb0nimyqav12z7d6llrri40b9b005rrcslvbby4abcrpgbnzy")))

(define-public crate-capnp-futures-0.16 (crate (name "capnp-futures") (vers "0.16.1") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.16.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "004rik3cinxf03h7whab07n4hdssad9xgvm4riv4ps26ghd0wn8l")))

(define-public crate-capnp-futures-0.17 (crate (name "capnp-futures") (vers "0.17.1") (deps (list (crate-dep (name "capnp") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.17.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0l67dch5lzgwmbkckvgkslv2qahaijw263frki7d7dcqn5xnc056")))

(define-public crate-capnp-futures-0.18 (crate (name "capnp-futures") (vers "0.18.2") (deps (list (crate-dep (name "capnp") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.18.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "1wgq1khlzwhhw07k6bpkd45bjvrvdm11fa7hg3rly0avgy2pnsdq")))

(define-public crate-capnp-futures-0.19 (crate (name "capnp-futures") (vers "0.19.0") (deps (list (crate-dep (name "capnp") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.19.0") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("executor"))) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "06zvxipmy0xdsxxqr54gs4yi8rgl33iprfhx4nzc0fsfncy4ib2z")))

(define-public crate-capnp-gj-0.1 (crate (name "capnp-gj") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.1") (default-features #t) (kind 0)))) (hash "1srhnfbxmz9x9v7n3qxha3ay19ri8fglc3xavrihm903crr6ljsq")))

(define-public crate-capnp-gj-0.2 (crate (name "capnp-gj") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gjio") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zpvgxl7bhakbg94cy6jdyj2ylmm0hmqvmrdb6i367zd51qqzs6a")))

(define-public crate-capnp-gj-0.2 (crate (name "capnp-gj") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gjio") (req "^0.1") (default-features #t) (kind 0)))) (hash "18kg5l1qna7yf13gqgfabyxkpv97dgk19dp8f4g1x7qv87lqbh51")))

(define-public crate-capnp-nonblock-0.1 (crate (name "capnp-nonblock") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.5") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1j7m7b11k5w6a5c10hg5w4qpmn69d2h99p9fxr6bnrjpkafc72z5")))

(define-public crate-capnp-nonblock-0.2 (crate (name "capnp-nonblock") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.5") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1i8klh5hm6rns79pbyang7699n4f0dadvzzyvzw8rvll0kcyf03y")))

(define-public crate-capnp-nonblock-0.2 (crate (name "capnp-nonblock") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.5") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0jf9yyfq6k2vqwayi80jxb4r2qpq0r1mllz37bqcw99fq55zhyvi")))

(define-public crate-capnp-nonblock-0.3 (crate (name "capnp-nonblock") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.6") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1gimf0sxhhvzrnpack3ac2l9plbl5qw6fgz5azj10zr1ib5sp38x")))

(define-public crate-capnp-nonblock-0.4 (crate (name "capnp-nonblock") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.6") (features (quote ("quickcheck"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "13cn1bggsrcfmk9vc4xkp2v6c89n5fpr70c0v4a63nw621jld0fi")))

(define-public crate-capnp-rpc-0.0.1 (crate (name "capnp-rpc") (vers "0.0.1") (deps (list (crate-dep (name "capnp") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0cdsym2gzmzcmxsjyndj79bwjan67cg81cc5b5nqavvgqlgczqak")))

(define-public crate-capnp-rpc-0.0.2 (crate (name "capnp-rpc") (vers "0.0.2") (deps (list (crate-dep (name "capnp") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0pnlr62ap5pqpba7abmv342kih176ajl684kl4xs17p40hpfgkgl")))

(define-public crate-capnp-rpc-0.0.3 (crate (name "capnp-rpc") (vers "0.0.3") (deps (list (crate-dep (name "capnp") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0262fyakszn5lqiwsjqzykn614x7ww1jcll2km9kc8m5mn10qnvi")))

(define-public crate-capnp-rpc-0.0.4 (crate (name "capnp-rpc") (vers "0.0.4") (deps (list (crate-dep (name "capnp") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.10") (default-features #t) (kind 1)))) (hash "1i2p9siy4lr8fgyyrbad7jip2pj0s7y33zsnbix7wk21kp7dw4y8")))

(define-public crate-capnp-rpc-0.0.5 (crate (name "capnp-rpc") (vers "0.0.5") (deps (list (crate-dep (name "capnp") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.11") (default-features #t) (kind 1)))) (hash "1q5rz85dijmk1m6094zwfwvcmirzg99syycpj14pkzzkip9827rk")))

(define-public crate-capnp-rpc-0.0.6 (crate (name "capnp-rpc") (vers "0.0.6") (deps (list (crate-dep (name "capnp") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.12") (default-features #t) (kind 1)))) (hash "07xmh7j5syd896nn905g0ly02hpzljcjywgv84i1aaj0kb1qc8ka")))

(define-public crate-capnp-rpc-0.0.7 (crate (name "capnp-rpc") (vers "0.0.7") (deps (list (crate-dep (name "capnp") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.14") (default-features #t) (kind 1)))) (hash "0lxlmn19m6n49kk4ns4hpklsgzxzmv604k7h2fr8laa301rh1x5x")))

(define-public crate-capnp-rpc-0.0.8 (crate (name "capnp-rpc") (vers "0.0.8") (deps (list (crate-dep (name "capnp") (req "^0.0.15") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.16") (default-features #t) (kind 1)))) (hash "1v7v6x1v2mxxy98ar49x76j7bkhlsmsd09694d7qxm4kfmh3qf95")))

(define-public crate-capnp-rpc-0.0.9 (crate (name "capnp-rpc") (vers "0.0.9") (deps (list (crate-dep (name "capnp") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.17") (default-features #t) (kind 1)))) (hash "03z5vbhs14ax6qh36ws9r0bjawpmsmsgcq4jc75m7wl6a8g7i7nb")))

(define-public crate-capnp-rpc-0.0.10 (crate (name "capnp-rpc") (vers "0.0.10") (deps (list (crate-dep (name "capnp") (req "^0.0.17") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.18") (default-features #t) (kind 1)))) (hash "05ji7jmdyp53ykv59q8kccm4j2xziv0qa198gywryhlk9v588hj3")))

(define-public crate-capnp-rpc-0.0.11 (crate (name "capnp-rpc") (vers "0.0.11") (deps (list (crate-dep (name "capnp") (req "^0.0.18") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.19") (default-features #t) (kind 1)))) (hash "1awbhmcjv5phclbmbzzafz9myqcafw9jwqw82dxjyxjlki1n3p8x")))

(define-public crate-capnp-rpc-0.0.12 (crate (name "capnp-rpc") (vers "0.0.12") (deps (list (crate-dep (name "capnp") (req "^0.0.18") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.19") (default-features #t) (kind 1)))) (hash "0r4yv73l54bykr7wxhz4pl4vshcy22l4phsmznqmfhdv4nalyc71")))

(define-public crate-capnp-rpc-0.0.13 (crate (name "capnp-rpc") (vers "0.0.13") (deps (list (crate-dep (name "capnp") (req "^0.0.19") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.20") (default-features #t) (kind 1)))) (hash "1177wp5hbgjkx23ckpw7mp0g28c1z1rkpksfl3q8p34kh10f84l0")))

(define-public crate-capnp-rpc-0.0.14 (crate (name "capnp-rpc") (vers "0.0.14") (deps (list (crate-dep (name "capnp") (req "^0.0.20") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.21") (default-features #t) (kind 1)))) (hash "0kpfymsfqznj0hqaprrqvlnf6xld0cmwqgp7gjg54cdjbinmwpym")))

(define-public crate-capnp-rpc-0.0.15 (crate (name "capnp-rpc") (vers "0.0.15") (deps (list (crate-dep (name "capnp") (req "^0.0.21") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.0.22") (default-features #t) (kind 1)))) (hash "0gcg2xy8i1yj8g7f0y05wvxxaaxbx6s1z5br216nnnra5bldfvhb")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.0") (deps (list (crate-dep (name "capnp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0sym5l49br50rpffywy3w8p5g8byfr6402220vih8h9i88gbfvdf")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.1") (deps (list (crate-dep (name "capnp") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1zjpi3qk0jw9ghyfq8qzfwncqnxchjfmvy82nnflm8zj0wja5xw6")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.2") (deps (list (crate-dep (name "capnp") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "0115rsga2ssw133068jjf6s3nhzn06fr56fk1p2hg74yvygbmvvh")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.3") (deps (list (crate-dep (name "capnp") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.3") (default-features #t) (kind 1)))) (hash "1y2ypqpgzf37djm1a09bdn3fn6wdjwmrmk6r34c16spkhi7vcz7b")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.4") (deps (list (crate-dep (name "capnp") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.4") (default-features #t) (kind 1)))) (hash "0wxqllxwlncnbkkj223i88hfrfvzl535y93lq47n2iq8r81nhxgm")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.5") (deps (list (crate-dep (name "capnp") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "0wyyr90n5qyjnpwr8lah60rj82r645ndpcdfl5if6ykk4ql47r0s")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.6") (deps (list (crate-dep (name "capnp") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "1130gz0xikcblfhfrd3y435i0kgsjwrwzr8p6paa0526rq55yvfp")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.7") (deps (list (crate-dep (name "capnp") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.9") (default-features #t) (kind 1)))) (hash "0yr4ssdcdz8cwxrhlli6x7qjkzk98i6vk54ngkrl8zkykqy3i45k")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.8") (deps (list (crate-dep (name "capnp") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.10") (default-features #t) (kind 1)))) (hash "1x96mzrpkikjhjs7l6q6i2b924m33scpm1shvmwh1vzxgl4fmrkb")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.9") (deps (list (crate-dep (name "capnp") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.11") (default-features #t) (kind 1)))) (hash "10w7ds05b01clscvb7mgkys0m24hf0h8cgcmcg495f0l8f1qw32m")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.10") (deps (list (crate-dep (name "capnp") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.12") (default-features #t) (kind 1)))) (hash "0b9nikqsmq10brbxjqli8yin9szps4j39455x0h1hcwhrd78bz4a")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.11") (deps (list (crate-dep (name "capnp") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.13") (default-features #t) (kind 1)))) (hash "0aycyyyxcndpwlkzqms7c3bs2kmd3lcnsabqg28wg7i3ai408jlk")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.12") (deps (list (crate-dep (name "capnp") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.15") (default-features #t) (kind 1)))) (hash "0vrxc5yr9hi1wy1k0nv9r1xby09r2qr7jsh40vsjg8d7qcj7qwiv")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.13") (deps (list (crate-dep (name "capnp") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.18") (default-features #t) (kind 1)))) (hash "0dgg47h9yw5c7902qdj0qkslrl0aj8xk82slb8l62382srvmw5dd")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.14") (deps (list (crate-dep (name "capnp") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.19") (default-features #t) (kind 1)))) (hash "1rdn2yvrar6v22brxrm2sndv809i3y0f4z50fh7a1csdbz68czkq")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.15") (deps (list (crate-dep (name "capnp") (req "^0.1.20") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.20") (default-features #t) (kind 1)))) (hash "1ncfs9v8p8c4sblyqa49asqqnwvkh6yikg7812rxc0xfy5fd2kg3")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.16") (deps (list (crate-dep (name "capnp") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.22") (default-features #t) (kind 1)))) (hash "1m6qgiari8qq66ij9r9b2szq40bbj3bnpvckjhy4yxh5yiv6ri1z")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.17") (deps (list (crate-dep (name "capnp") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.24") (default-features #t) (kind 1)))) (hash "0is3gz2i5l0kih5n31q0zg9k71194v1z7dm38dxs3fhbmyipz7cw")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.18") (deps (list (crate-dep (name "capnp") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.26") (default-features #t) (kind 1)))) (hash "0zd3gxj3abcipwfm6c4p13bw6n1fz23wjq57p887jcn932x05i7i")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.19") (deps (list (crate-dep (name "capnp") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.26") (default-features #t) (kind 1)))) (hash "0n0csdff3l17kq3dqlk1bdxadzi24i0w6klv6g0df6s8as526w0m")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.20") (deps (list (crate-dep (name "capnp") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.27") (default-features #t) (kind 1)))) (hash "0aqry0zxcn3cavzk2ljdi9jg0y19sh0v20byn875nzk1n1z1qgvs")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.21") (deps (list (crate-dep (name "capnp") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "1h2z3qnwl9fpj8ihd87nwjv5vfk8kd7g5xg4836zbkb1bj2x0n2s")))

(define-public crate-capnp-rpc-0.1 (crate (name "capnp-rpc") (vers "0.1.22") (deps (list (crate-dep (name "capnp") (req "^0.1.33") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.1.29") (default-features #t) (kind 1)))) (hash "02kwp39xj089lvqzjqmr14b01ka1dx2nmmanq5awg2jas8jgbmrz")))

(define-public crate-capnp-rpc-0.2 (crate (name "capnp-rpc") (vers "0.2.0") (deps (list (crate-dep (name "capnp") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "0by6bhj0ciy4s9p59h0sgjkhi4sbq0y30g9k9pqq7ahhcbfayg2x")))

(define-public crate-capnp-rpc-0.2 (crate (name "capnp-rpc") (vers "0.2.1") (deps (list (crate-dep (name "capnp") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "14xn6aaryz26pa6aigdzy18dv1v0cl51mhdqb3ql3m5fqdpb85md")))

(define-public crate-capnp-rpc-0.2 (crate (name "capnp-rpc") (vers "0.2.2") (deps (list (crate-dep (name "capnp") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.2.2") (default-features #t) (kind 1)))) (hash "0snx9d7cy5dqa2k3g8bhblmjl57fjmdv5fwljylzg5qbg34jmc3w")))

(define-public crate-capnp-rpc-0.2 (crate (name "capnp-rpc") (vers "0.2.3") (deps (list (crate-dep (name "capnp") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.2.3") (default-features #t) (kind 1)))) (hash "17j9ji6z2l26lzypc7vlm3sbd5avi8fixnh3564y6n2ymlgvx3w6")))

(define-public crate-capnp-rpc-0.3 (crate (name "capnp-rpc") (vers "0.3.0") (deps (list (crate-dep (name "capnp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1xv9w60mkh36q5yqfb1ic94r9x820giyagss80j05b6qisk2wdqy")))

(define-public crate-capnp-rpc-0.3 (crate (name "capnp-rpc") (vers "0.3.1") (deps (list (crate-dep (name "capnp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1xjsx50lwsbf4f40smcz6m5l2jsw5wrqinsbbysnfnh950m1jycd")))

(define-public crate-capnp-rpc-0.3 (crate (name "capnp-rpc") (vers "0.3.2") (deps (list (crate-dep (name "capnp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.3.1") (default-features #t) (kind 1)))) (hash "1prvq3q9jp6bcr1pndizr9ww70gd1v2ww1m8nf6dcw1qq2w0pdd9")))

(define-public crate-capnp-rpc-0.4 (crate (name "capnp-rpc") (vers "0.4.0") (deps (list (crate-dep (name "capnp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "1r6d24ib5mwbh0gpx1rjp65w4zya074m29aha72m4lk5g83sk0a6")))

(define-public crate-capnp-rpc-0.5 (crate (name "capnp-rpc") (vers "0.5.0") (deps (list (crate-dep (name "capnp") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.5.0") (default-features #t) (kind 1)))) (hash "1lkildlj9cv97cz3g56c9lb1j1cc09nwxa5q4l9c5mb034na272p")))

(define-public crate-capnp-rpc-0.6 (crate (name "capnp-rpc") (vers "0.6.0") (deps (list (crate-dep (name "capnp") (req "^0.6") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.1") (default-features #t) (kind 0)))) (hash "0r81qcs28mkkkqx5d46f1qrg5nakc0l4iw1zwv8gbnvg2nk1a529")))

(define-public crate-capnp-rpc-0.6 (crate (name "capnp-rpc") (vers "0.6.1") (deps (list (crate-dep (name "capnp") (req "^0.6.1") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w0zkv12w53aifv4gnbs9hs2kvlm7rxc9mjgyysbhwdq50b7nn3d")))

(define-public crate-capnp-rpc-0.6 (crate (name "capnp-rpc") (vers "0.6.2") (deps (list (crate-dep (name "capnp") (req "^0.6.1") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j8amwka627vjj06rjg7hanjm859gnc66a50s5gvwk57blvjr6k3")))

(define-public crate-capnp-rpc-0.6 (crate (name "capnp-rpc") (vers "0.6.3") (deps (list (crate-dep (name "capnp") (req "^0.6.2") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kaa8k3aacfza6ypls50d3f88y9mnwrjyg5g0db8gghaqakarfqg")))

(define-public crate-capnp-rpc-0.7 (crate (name "capnp-rpc") (vers "0.7.0") (deps (list (crate-dep (name "capnp") (req "^0.7") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gjio") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w3ykr950nkwv61ndj7fx1qwrzvfzacd0188icckn8fbvfba8fqk")))

(define-public crate-capnp-rpc-0.7 (crate (name "capnp-rpc") (vers "0.7.1") (deps (list (crate-dep (name "capnp") (req "^0.7") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gjio") (req "^0.1") (default-features #t) (kind 0)))) (hash "099zw7ccxyaqik232q0qynz3aii200jhmcpgxjv3fhf8afq0gic3")))

(define-public crate-capnp-rpc-0.7 (crate (name "capnp-rpc") (vers "0.7.2") (deps (list (crate-dep (name "capnp") (req "^0.7") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gjio") (req "^0.1") (default-features #t) (kind 0)))) (hash "01hyk35g7b667n0i7ibc9y4ylpd2592w23sgahwhffa5k800lih8")))

(define-public crate-capnp-rpc-0.7 (crate (name "capnp-rpc") (vers "0.7.3") (deps (list (crate-dep (name "capnp") (req "^0.7") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gjio") (req "^0.1") (default-features #t) (kind 0)))) (hash "1721rjprb9lqc5v3pxls06mdzgsz8vbxiyjnm7024s3sd91pa81m")))

(define-public crate-capnp-rpc-0.7 (crate (name "capnp-rpc") (vers "0.7.4") (deps (list (crate-dep (name "capnp") (req "^0.7") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "gj") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gjio") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mf9mgc5dfb252mz018ri7b69jnx63y674imw7mapi8hav2c0aib")))

(define-public crate-capnp-rpc-0.8 (crate (name "capnp-rpc") (vers "0.8.0") (deps (list (crate-dep (name "capnp") (req "^0.8") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "00x9c3gjziv9h5wd0fg0xmz4s9h081pd18794yd93yvi0ynbp43r")))

(define-public crate-capnp-rpc-0.8 (crate (name "capnp-rpc") (vers "0.8.1") (deps (list (crate-dep (name "capnp") (req "^0.8") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0265ar9m89zsmcrn30gp5c8s4y30n397jm44c39vvvs69wiisrnk")))

(define-public crate-capnp-rpc-0.8 (crate (name "capnp-rpc") (vers "0.8.2") (deps (list (crate-dep (name "capnp") (req "^0.8") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1bg77ilj329gzbpq00n4lhfbx3p6wqcazjrc4w01ah40r8dq4661")))

(define-public crate-capnp-rpc-0.8 (crate (name "capnp-rpc") (vers "0.8.3") (deps (list (crate-dep (name "capnp") (req "^0.8.13") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.8.8") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "03d05dc9i9y3z5cvyp03dpfy8b40q2fqpv5cs6cp0b826irlmwbh")))

(define-public crate-capnp-rpc-0.9 (crate (name "capnp-rpc") (vers "0.9.0") (deps (list (crate-dep (name "capnp") (req "^0.9.0") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.9.0") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0ih0awlnz2n395zadv1gc4spzcdzii90jx6w7vmy0fmvj8dmydyf")))

(define-public crate-capnp-rpc-0.10 (crate (name "capnp-rpc") (vers "0.10.0") (deps (list (crate-dep (name "capnp") (req "^0.10") (features (quote ("rpc"))) (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.10") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1j6xg7yays1hlm1045wviyn1642yvvi2p4kba26yk07a0kafr3jn")))

(define-public crate-capnp-rpc-0.11 (crate (name "capnp-rpc") (vers "0.11.0") (deps (list (crate-dep (name "capnp") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.11.0") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1lsvz2h366vkgkyfhs5k6kwc7zv3sb1fxq8hcym2iyk5rax9hx29")))

(define-public crate-capnp-rpc-0.12 (crate (name "capnp-rpc") (vers "0.12.0") (deps (list (crate-dep (name "capnp") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "capnpc") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0bblh6nq75pkppd4ljk8f6dcblxqq48n96x7ac9in4p41jdy2rh9")))

(define-public crate-capnp-rpc-0.12 (crate (name "capnp-rpc") (vers "0.12.1") (deps (list (crate-dep (name "capnp") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1cav4hyggvaa6shl74hlckmc0zc5yd9bvi0kiw9inp5676cpnyw9")))

(define-public crate-capnp-rpc-0.12 (crate (name "capnp-rpc") (vers "0.12.2") (deps (list (crate-dep (name "capnp") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "16d446hhivsnl5v56qvzdfpp5vdg7p5wynh9jfwaianiyxrk0v98")))

(define-public crate-capnp-rpc-0.12 (crate (name "capnp-rpc") (vers "0.12.3") (deps (list (crate-dep (name "capnp") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06sfy78n6c15xppsn00xlgda56rlw4bx2mskbsjvrizvizlbpgdi")))

(define-public crate-capnp-rpc-0.13 (crate (name "capnp-rpc") (vers "0.13.0") (deps (list (crate-dep (name "capnp") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "0kgyl4pllfwm1ss1va12x2skr0kq7sx267gws1znrmzxpbw17m79")))

(define-public crate-capnp-rpc-0.13 (crate (name "capnp-rpc") (vers "0.13.1") (deps (list (crate-dep (name "capnp") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "17p0y0yk68pzsnpmaklhiqrrlrrv0ld8nhbg4qflmgibshi8b69p")))

(define-public crate-capnp-rpc-0.14 (crate (name "capnp-rpc") (vers "0.14.0") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "147sbdydl79f4igk4acygahcr8my9lxjhnl10i4h1d1cfvp97yki")))

(define-public crate-capnp-rpc-0.14 (crate (name "capnp-rpc") (vers "0.14.1") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "0pm9bjw481lw1zp8lmzkpsjrb85lbjg5s46piqbc3wk8dzwifksc")))

(define-public crate-capnp-rpc-0.15 (crate (name "capnp-rpc") (vers "0.15.0") (deps (list (crate-dep (name "capnp") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "0cdwsylxibwn20gibmfrr52aab4jdzdwspd4rjhzphs83wqv1fcn")))

(define-public crate-capnp-rpc-0.15 (crate (name "capnp-rpc") (vers "0.15.1") (deps (list (crate-dep (name "capnp") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "1ixx493z2p6gi32hyfv0zw0f3lgrxrnhx4cvfrrrr12gy4jv2x02")))

(define-public crate-capnp-rpc-0.16 (crate (name "capnp-rpc") (vers "0.16.0") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "04dsyxjnwkn590d289z2hzk2sdbkmc78pyp436x36rdy2wmhkwl6")))

(define-public crate-capnp-rpc-0.16 (crate (name "capnp-rpc") (vers "0.16.1") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "1k8azw6xcim3xv25dklr0mwjrlhgvbbxv4bysxkxa7fsn407f2b2")))

(define-public crate-capnp-rpc-0.16 (crate (name "capnp-rpc") (vers "0.16.2") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "1rilhmk4rm03wqi0h8b43pjfi0dgva0cqj23pfww07fx7d1c16k1")))

(define-public crate-capnp-rpc-0.17 (crate (name "capnp-rpc") (vers "0.17.0") (deps (list (crate-dep (name "capnp") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "0y878gx7r59j8gbrhldavshxi92sfxg7lhjhqfy1qj9yg1lyif4s")))

(define-public crate-capnp-rpc-0.18 (crate (name "capnp-rpc") (vers "0.18.0") (deps (list (crate-dep (name "capnp") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "0400mpdijax2dg29m688liccall6xxwrplr7w8l0cq3l1yg7mf4l")))

(define-public crate-capnp-rpc-0.19 (crate (name "capnp-rpc") (vers "0.19.0") (deps (list (crate-dep (name "capnp") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "05hahpmfjnn49mzv4b2zp4y6si9g6yvjy6kgm2ia9apndjcbl78p")))

(define-public crate-capnp-rpc-0.19 (crate (name "capnp-rpc") (vers "0.19.1") (deps (list (crate-dep (name "capnp") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "capnp-futures") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.0") (features (quote ("std"))) (kind 0)))) (hash "1qpnh33441vhpp625ih43dvv7bpqm7ifigw65qw6pp7vg2vglz18")))

(define-public crate-capnp_conv-0.1 (crate (name "capnp_conv") (vers "0.1.0") (deps (list (crate-dep (name "capnp") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "capnp_conv_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jyddnjfyahbvh5ccykcsjml632r95vmcxhxwjsn0d2qkrlcbay4")))

(define-public crate-capnp_conv-0.2 (crate (name "capnp_conv") (vers "0.2.0") (deps (list (crate-dep (name "capnp") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "capnp_conv_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ryby1c33w68k1kj92dawbslyrxxv5hy58qcmv4d45pxydh3ki5l")))

(define-public crate-capnp_conv-0.3 (crate (name "capnp_conv") (vers "0.3.0") (deps (list (crate-dep (name "capnp") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "capnp_conv_macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qpypn2jw20p1vmz293hjhzg0ikl2lhfvlbrx99ljy4aqriycma8")))

(define-public crate-capnp_conv_macros-0.1 (crate (name "capnp_conv_macros") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1n0h0bk0hxz3bn7rfrpp2wa7b6nsqaakmj33fpcy2mps9d2z3d8w")))

(define-public crate-capnp_conv_macros-0.2 (crate (name "capnp_conv_macros") (vers "0.2.0") (deps (list (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0cj60d98z90w8n1rq30bp95zckjwb4ccvw1dxzncs4csv3v6d2gq")))

(define-public crate-capnp_conv_macros-0.3 (crate (name "capnp_conv_macros") (vers "0.3.0") (deps (list (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0ki50ykvnsp7wxby397y7r1ih9nva29xqkl0za9dxc7csn11a32m")))

(define-public crate-capnpc-0.0.1 (crate (name "capnpc") (vers "0.0.1") (deps (list (crate-dep (name "capnp") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1b0krkmb8j6gw8vapmnlrzkdb0jy8l84mwxjcsq7c8d64fpylh9w")))

(define-public crate-capnpc-0.0.2 (crate (name "capnpc") (vers "0.0.2") (deps (list (crate-dep (name "capnp") (req "~0.0.2") (default-features #t) (kind 0)))) (hash "1pr1rq7gs8ahs343vgchfqgnzj4s7q03qb1hw4pj0w54sh127nxh")))

(define-public crate-capnpc-0.0.3 (crate (name "capnpc") (vers "0.0.3") (deps (list (crate-dep (name "capnp") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1f5ag8vh5bv3fwjbkpz2czh45jc13i19dgc31wzw7lvn1w1dqdps")))

(define-public crate-capnpc-0.0.4 (crate (name "capnpc") (vers "0.0.4") (deps (list (crate-dep (name "capnp") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1nb563nc8k976lqwyq74757h3i0jb9h12bjcwkp7g173argwb1iy")))

(define-public crate-capnpc-0.0.5 (crate (name "capnpc") (vers "0.0.5") (deps (list (crate-dep (name "capnp") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1c3xfdqily0042lc0ay5di04pfk7ljba4cgvni110a3r5s7z2n11")))

(define-public crate-capnpc-0.0.6 (crate (name "capnpc") (vers "0.0.6") (deps (list (crate-dep (name "capnp") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1s7vzk9kfj0z4jl7v2fxbmrqa95r4q9kmjh2fy7igr16g2c02hlr")))

(define-public crate-capnpc-0.0.7 (crate (name "capnpc") (vers "0.0.7") (deps (list (crate-dep (name "capnp") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1i08cy78yabnpvnxrymg3sl5bxdhbx2jdg3s6y2b3v86q9ix79fd")))

(define-public crate-capnpc-0.0.8 (crate (name "capnpc") (vers "0.0.8") (deps (list (crate-dep (name "capnp") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "19hslflzlzx4qr13af7ikz0fa92w4vz8bw60q1hiadp0f43jvkmg")))

(define-public crate-capnpc-0.0.9 (crate (name "capnpc") (vers "0.0.9") (deps (list (crate-dep (name "capnp") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1lgyklhc80p9dwlf00cmhfgfq2w7cw2hz9jgf87nplj6gwvwl0pg")))

(define-public crate-capnpc-0.0.10 (crate (name "capnpc") (vers "0.0.10") (deps (list (crate-dep (name "capnp") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0r3834yn8kvl1w5wfv3c1d5v6w97mr090nkj8hvylhyfx2b360pj")))

(define-public crate-capnpc-0.0.11 (crate (name "capnpc") (vers "0.0.11") (deps (list (crate-dep (name "capnp") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "1374dyjw7h22mxvz198rnkxv4agv6q8sa2qyam1j4f8q0n0d1rmf")))

(define-public crate-capnpc-0.0.12 (crate (name "capnpc") (vers "0.0.12") (deps (list (crate-dep (name "capnp") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "06wxchy2ila5fnp8zdxa1dkiafm46kffz4j2rwg4mn65zmv6d5i4")))

(define-public crate-capnpc-0.0.14 (crate (name "capnpc") (vers "0.0.14") (deps (list (crate-dep (name "capnp") (req "^0.0.13") (default-features #t) (kind 0)))) (hash "083vs4kma3jm6ydbn8w9lhn6r71f6dbzgjz8d9sp8sbbvffqlidk")))

(define-public crate-capnpc-0.0.15 (crate (name "capnpc") (vers "0.0.15") (deps (list (crate-dep (name "capnp") (req "^0.0.14") (default-features #t) (kind 0)))) (hash "09279rra5sa309xpxihmyr9srf3k6g1pbh370d2asdsxyc45nnm1")))

(define-public crate-capnpc-0.0.16 (crate (name "capnpc") (vers "0.0.16") (deps (list (crate-dep (name "capnp") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "1kcjc86j8i7rrdsh9n5nij7c5y9jw1b99zbxi42k2vhggk0hd3zz")))

(define-public crate-capnpc-0.0.17 (crate (name "capnpc") (vers "0.0.17") (deps (list (crate-dep (name "capnp") (req "^0.0.16") (default-features #t) (kind 0)))) (hash "121bilarpghwk8wf9c2628y969hnjpdjvl8r8080z8kmrfpq3gkp")))

(define-public crate-capnpc-0.0.18 (crate (name "capnpc") (vers "0.0.18") (deps (list (crate-dep (name "capnp") (req "^0.0.17") (default-features #t) (kind 0)))) (hash "1ipzq09qhiv4qfi5xsblvh0wd1y2360rcf1x4vn95fcqyf94kjci")))

(define-public crate-capnpc-0.0.19 (crate (name "capnpc") (vers "0.0.19") (deps (list (crate-dep (name "capnp") (req "^0.0.18") (default-features #t) (kind 0)))) (hash "1lphb3j8fvs4qsys8yr6ifzyp98vgxcc811f51swdkvqpa4apzyy")))

(define-public crate-capnpc-0.0.20 (crate (name "capnpc") (vers "0.0.20") (deps (list (crate-dep (name "capnp") (req "^0.0.19") (default-features #t) (kind 0)))) (hash "152vasxvlqk662xa6674jwwsmrv9p60yg996y27q9pc0b1zgxy0g")))

(define-public crate-capnpc-0.0.21 (crate (name "capnpc") (vers "0.0.21") (deps (list (crate-dep (name "capnp") (req "^0.0.20") (default-features #t) (kind 0)))) (hash "1x1l3xldy34kazj06mamb0lg619i6cpn20p2vzw0ng38b0kyzfq4")))

(define-public crate-capnpc-0.0.22 (crate (name "capnpc") (vers "0.0.22") (deps (list (crate-dep (name "capnp") (req "^0.0.21") (default-features #t) (kind 0)))) (hash "0xb33wwvsb4v484xwqrkw5pbbv0101izhgbnchnhd0h24y11q00p")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.0") (deps (list (crate-dep (name "capnp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17nza6z939an1v2r0qj2fmifzh0pfxxnf48br10rr07i5zk2ph4n")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.1") (deps (list (crate-dep (name "capnp") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0mz516i2w6j50dckz601c64cc1dl166cy4m4chsak3f4k0fqqnh0")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.2") (deps (list (crate-dep (name "capnp") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0xsdcbyjk9yc113fpfr2rn163xv9p1bw0chv7d1bf3dv2nghxsd1")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.3") (deps (list (crate-dep (name "capnp") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "15821yvv6jhayfrs5yjpdpr8zagndl057kwrj8p0ffrcfqh70aj8")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.4") (deps (list (crate-dep (name "capnp") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0ahypywflsfm0x8ql4909b27x1kkj8cv254ijk09ksi3lavc86kj")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.5") (deps (list (crate-dep (name "capnp") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0r5w1bp94lf27mhwnwcrxrv9159sn36gm3j5vlpakhs0yq8xyk4p")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.6") (deps (list (crate-dep (name "capnp") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1wj2ss0amzbjqjhjmb42nyy9assfbdf0d4kvsg572jbwyk451k63")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.7") (deps (list (crate-dep (name "capnp") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "148gpsab5garsjlbj4366awnz9j1l0jq95gmdz9bi12lyc7jjqfv")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.8") (deps (list (crate-dep (name "capnp") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "17da2hmdqz121j1nfa58v6xw54sjfmwbf422c0p6zz29hi5489bg")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.9") (deps (list (crate-dep (name "capnp") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1ybdxiwdmc6wxkwpcwfs2dkg11vl6inx01xfhw18i5ss3641ylq9")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.10") (deps (list (crate-dep (name "capnp") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1356g84wfzn9m1kcrc0y2mlad5d4ig3vqxqp60nwpk4f4d02n5rw")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.11") (deps (list (crate-dep (name "capnp") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "0dy4zfglf30h2afc5s9b74ddn798vadmlvv8wjwvs6c421pvsg10")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.12") (deps (list (crate-dep (name "capnp") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "15pbsif6gadw3wi1qal6jqdxngyshf0n899i2vb0pd1wp8f3asnv")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.13") (deps (list (crate-dep (name "capnp") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0r4ix03wr4zhw382wjg28408ps7z86x1jxkp391b9wrv0jy68shw")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.14") (deps (list (crate-dep (name "capnp") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1vxfgfbffvd5qfz6sla9rg7gqzh2h7l41449ps8h89m1nr95282z")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.15") (deps (list (crate-dep (name "capnp") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0gdh0zg99q93k7x8adwzglisvpaq5v4raf7561p4xf70qdbnhp7c")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.16") (deps (list (crate-dep (name "capnp") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1i7jkrv05y4m1y89wi9q4lsar8yiq82jizv9jshpvvl9dmlcvkn7")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.17") (deps (list (crate-dep (name "capnp") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1zzf73w467gwn5fzzkp7azrg77rajpx3ixyrlvj4p82kw0l863as")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.18") (deps (list (crate-dep (name "capnp") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0w7c1xlyvdb6k29rb12vpzwd3w0mqjgskl3j4x5nh70w6pw7pfic")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.19") (deps (list (crate-dep (name "capnp") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "12mndvpcaqj63i4592xhbf3pxpb3sjjmxri287r8ibq84mp8rbzs")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.20") (deps (list (crate-dep (name "capnp") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "1csnkbp3dgx747273xxqas53i2k83ngfzgsxvfvagsqi0vjlbnjc")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.21") (deps (list (crate-dep (name "capnp") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0pjn669nsv0wljmbjff998xrw0s0agq1wg0pdlalh3g7xpj1nls0")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.22") (deps (list (crate-dep (name "capnp") (req "^0.1.23") (default-features #t) (kind 0)))) (hash "04ydx9kwqqdksyyf9sj8hhhvfw4xlaz2w2fpxb1k9nkr0qxbiirz")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.23") (deps (list (crate-dep (name "capnp") (req "^0.1.23") (default-features #t) (kind 0)))) (hash "11211v9m106lgfridp1178f4x6kg4qrs8hkhn0n0l3qjrck1dijl")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.24") (deps (list (crate-dep (name "capnp") (req "^0.1.24") (default-features #t) (kind 0)))) (hash "1z5dsf2mgaq9x1cf0p5h2l6rgijl155sz6a2ssp2g9754fp08ann")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.25") (deps (list (crate-dep (name "capnp") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "0xcpx9xz6lx8ryzhz8dsbgnd5gyj24np7qmmd71i100bc9h5w3gg")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.26") (deps (list (crate-dep (name "capnp") (req "^0.1.26") (default-features #t) (kind 0)))) (hash "1ddyng5ah2508m4qcgdlraps9i1s81yqcdyb05l1y0bxhmry9ff1")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.27") (deps (list (crate-dep (name "capnp") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "0xjk4cgybzsjmvlfhhhc167mal8wgcrgm01qxyjmyqxmiajr90iz")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.28") (deps (list (crate-dep (name "capnp") (req "^0.1.28") (default-features #t) (kind 0)))) (hash "0lx66lyda6w2adl77rpn524lkvn3l073zdzh2154fxp26abs59yl")))

(define-public crate-capnpc-0.1 (crate (name "capnpc") (vers "0.1.29") (deps (list (crate-dep (name "capnp") (req "^0.1.33") (default-features #t) (kind 0)))) (hash "1iiina2wk5il99lbwa8srl58z28qp4kmw3zn7h47cjx113vl7757")))

(define-public crate-capnpc-0.2 (crate (name "capnpc") (vers "0.2.0") (deps (list (crate-dep (name "capnp") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ijdjrn3in96nrsfzjiwi3ly59avy66s3ra5pslsxhkisqn11hzv")))

(define-public crate-capnpc-0.2 (crate (name "capnpc") (vers "0.2.1") (deps (list (crate-dep (name "capnp") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0wd4gxjggy9imp6i7gnjj6jd01sdk4c0yjw5314zdv8gsx880r34")))

(define-public crate-capnpc-0.2 (crate (name "capnpc") (vers "0.2.2") (deps (list (crate-dep (name "capnp") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0nbs2yhpv72dxr483ykbl282fiyx4pp2193nidlywh6r22phdd4k")))

(define-public crate-capnpc-0.2 (crate (name "capnpc") (vers "0.2.3") (deps (list (crate-dep (name "capnp") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0yya026qz5v0gf9kzmwlfyijlcflvffy09ymjk88jfj8klc4bb0m")))

(define-public crate-capnpc-0.2 (crate (name "capnpc") (vers "0.2.4") (deps (list (crate-dep (name "capnp") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0ybjb17q1p27w3wl3f7vflivxqgawx33a13c3rrla8lv75vc83hp")))

(define-public crate-capnpc-0.2 (crate (name "capnpc") (vers "0.2.5") (deps (list (crate-dep (name "capnp") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0z2i6v79ibl1wzr51xc0j1ixwpah6j35ga5pmvl9h5llqczs8g8j")))

(define-public crate-capnpc-0.3 (crate (name "capnpc") (vers "0.3.0") (deps (list (crate-dep (name "capnp") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0fha7a297nf7hycmpwryj7fsmjv905lsk57rrgkiz39kp0hxzp3x")))

(define-public crate-capnpc-0.3 (crate (name "capnpc") (vers "0.3.1") (deps (list (crate-dep (name "capnp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0yppiwk8h05ngbfhhs8c1vby1zpmgiqrfzranfhmgw0qb9zj6hm7")))

(define-public crate-capnpc-0.4 (crate (name "capnpc") (vers "0.4.0") (deps (list (crate-dep (name "capnp") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "00yki7sqnask9z28rzrqhp8qfqkzg7zqyab2vpcrg3imfhvb64ba")))

(define-public crate-capnpc-0.4 (crate (name "capnpc") (vers "0.4.1") (deps (list (crate-dep (name "capnp") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1zrmdk5qzsfbqk9n9lm1nv1qdyxjmh888v9ni42ymhzak25nrcf8")))

(define-public crate-capnpc-0.4 (crate (name "capnpc") (vers "0.4.2") (deps (list (crate-dep (name "capnp") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1yxxj98y50xm2lj145flyiqp9wl6vy48a87ldqi7pz5q517skipq")))

(define-public crate-capnpc-0.4 (crate (name "capnpc") (vers "0.4.3") (deps (list (crate-dep (name "capnp") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1fyw42j92sgc2pf074ydjxp22v13kn9j08gqca56pgy56b150vs9")))

(define-public crate-capnpc-0.4 (crate (name "capnpc") (vers "0.4.4") (deps (list (crate-dep (name "capnp") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1h6jwckq1amjv5d6m427j54w4567ykv7x0dkvl6rg6cp9mi7bakm")))

(define-public crate-capnpc-0.5 (crate (name "capnpc") (vers "0.5.0") (deps (list (crate-dep (name "capnp") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "09pnai6hgddig793p39m7xhygaqs6nk8ss6p1ad00k8y7hv07lv4")))

(define-public crate-capnpc-0.5 (crate (name "capnpc") (vers "0.5.1") (deps (list (crate-dep (name "capnp") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "11xf4ldy2qijx1mygik2a1fbz575qrvsfj00k3zj3rk0c9vgc0v0")))

(define-public crate-capnpc-0.6 (crate (name "capnpc") (vers "0.6.0") (deps (list (crate-dep (name "capnp") (req "^0.6") (default-features #t) (kind 0)))) (hash "0xgfgb446lz9cz9vlp28irr7kq5bw7h5j26yahrvqy3px5cjqjxc")))

(define-public crate-capnpc-0.6 (crate (name "capnpc") (vers "0.6.1") (deps (list (crate-dep (name "capnp") (req "^0.6") (default-features #t) (kind 0)))) (hash "09zwm5hjpjcnq1lmv83nk6mhpv697bai8is5wwk2dpic3iwffgfy")))

(define-public crate-capnpc-0.6 (crate (name "capnpc") (vers "0.6.2") (deps (list (crate-dep (name "capnp") (req "^0.6") (default-features #t) (kind 0)))) (hash "10mrqbbndb07qyvbp7f6khc333q9rd0cmgx56bqrps0m79swblk6")))

(define-public crate-capnpc-0.7 (crate (name "capnpc") (vers "0.7.0") (deps (list (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)))) (hash "1f57z1azcxsr9xzx1c033n33gmsczazchfqqaww17iahvsyw3mmw")))

(define-public crate-capnpc-0.7 (crate (name "capnpc") (vers "0.7.1") (deps (list (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)))) (hash "0mw603ik7va2dm047i6iz2p9zacsb2pzdw1ay6crs2r75qjmcgla")))

(define-public crate-capnpc-0.7 (crate (name "capnpc") (vers "0.7.2") (deps (list (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)))) (hash "1dk23s8h6kifqal3a2z3lph416g4vd7naicj715d1cxyc8lxd240")))

(define-public crate-capnpc-0.7 (crate (name "capnpc") (vers "0.7.3") (deps (list (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hzp79796936sg1gpii993w8zw6rbv4l3zq2w2ndvzp3znlgk35p")))

(define-public crate-capnpc-0.7 (crate (name "capnpc") (vers "0.7.4") (deps (list (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)))) (hash "0g8xf5bcdxqcnd9ipxjlwy4g2q2p2x7qmq8xwqbbnal7x14jn8h6")))

(define-public crate-capnpc-0.7 (crate (name "capnpc") (vers "0.7.5") (deps (list (crate-dep (name "capnp") (req "^0.7") (default-features #t) (kind 0)))) (hash "0wbb6izh8fpmy5sv065v2d8i36gj32lrxb0bynql8ar707m0hg9l")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.0") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "0npqi41arxwa5yizpy191byb59g2mfqsr5xfb3w20v00vgjawjc1")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.1") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "0i6ndgkck0id0jd1ypqsq4l7bis3wzyvj3fyp0qhnszl8skbzys6")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.2") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "17zmmw5cyj58mf1n64rv12vyd4qpa6mv063xa7c8hhd5vbbr0y4c")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.3") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "036fyc7gvfmw694g4s65xa5pv7481vh9q9nmiv235f4xdcxai05k")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.4") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "191wfzyaxf4nsrfjdz9rqbvwnlcpjn965ki4f6zws966v311vg8i")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.5") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "0hfgziii3dk920jq0x8bn6hsp2zdfzqgbmkq3bip7dk89m7p27zx")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.6") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "1mqz8iz757li7g22llbijb2rnxcmql3a2713229wxpi4c21hbzkp")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.7") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "1qhqk4i1paf1c3hdpsf3i7ycdabv0hvvxn5si1nk3w93drjk1djy")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.8") (deps (list (crate-dep (name "capnp") (req "^0.8.13") (default-features #t) (kind 0)))) (hash "1kbyrlxl0rff1car11simjdv34wfhi9r4mb8bpyrm9w48kcycalk")))

(define-public crate-capnpc-0.8 (crate (name "capnpc") (vers "0.8.9") (deps (list (crate-dep (name "capnp") (req "^0.8") (default-features #t) (kind 0)))) (hash "0w98pgjx4fh392h31s1bhffvkkfmwplv3abvh607ywqmzyi5np90")))

(define-public crate-capnpc-0.9 (crate (name "capnpc") (vers "0.9.0") (deps (list (crate-dep (name "capnp") (req "^0.9") (default-features #t) (kind 0)))) (hash "0xafxc2qxpmlli7v9bf6xvz4ri7253zh0sxrjjkkrmnrswcbi1bg")))

(define-public crate-capnpc-0.9 (crate (name "capnpc") (vers "0.9.1") (deps (list (crate-dep (name "capnp") (req "^0.9") (default-features #t) (kind 0)))) (hash "1bhwsb2v8ni82h0364m57c3322jicpcj4s8pbhx16m8p9dccrxx2")))

(define-public crate-capnpc-0.9 (crate (name "capnpc") (vers "0.9.2") (deps (list (crate-dep (name "capnp") (req "^0.9") (default-features #t) (kind 0)))) (hash "1h5zpyvs85pb0ic1zdvnhr1f5j0i6n9xy08nd8pcfm0ynxi5dw4r")))

(define-public crate-capnpc-0.9 (crate (name "capnpc") (vers "0.9.3") (deps (list (crate-dep (name "capnp") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "08gdn6b4473pg6gxpbaxvp3qmff9yx0y250nznv8zs8alsmxrxyy")))

(define-public crate-capnpc-0.9 (crate (name "capnpc") (vers "0.9.4") (deps (list (crate-dep (name "capnp") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "0cbg7zdq7bnvq6nkagzrafx09bm12ip7y2kyx02ms6yga1464j32")))

(define-public crate-capnpc-0.9 (crate (name "capnpc") (vers "0.9.5") (deps (list (crate-dep (name "capnp") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "0mh0yvbf1yy3wp3jfjpfss5sgpnqbaz2d5wfr1067jfraaxljx09")))

(define-public crate-capnpc-0.10 (crate (name "capnpc") (vers "0.10.0") (deps (list (crate-dep (name "capnp") (req "^0.10") (default-features #t) (kind 0)))) (hash "0am2jbs09lcrnlj6yhy7789r9018s1yla00ngf02p0il97873wnz")))

(define-public crate-capnpc-0.10 (crate (name "capnpc") (vers "0.10.1") (deps (list (crate-dep (name "capnp") (req "^0.10") (default-features #t) (kind 0)))) (hash "00gbnni3wm8781zcq9xyilmv8fd1anpddv9hvf54w2gfspm2w6kc")))

(define-public crate-capnpc-0.10 (crate (name "capnpc") (vers "0.10.2") (deps (list (crate-dep (name "capnp") (req "^0.10") (default-features #t) (kind 0)))) (hash "1zxbmdkr0xfzkfq9p8zn7pp9jjq275qhr8fh9a0cc0ab37yfvbyj")))

(define-public crate-capnpc-0.11 (crate (name "capnpc") (vers "0.11.0") (deps (list (crate-dep (name "capnp") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0d4fc7cxj2s5z6py8axgpinfgxsb6nahay3yrrf1h6k6vc34wkkc")))

(define-public crate-capnpc-0.11 (crate (name "capnpc") (vers "0.11.1") (deps (list (crate-dep (name "capnp") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "08zvj1m9sf96x35wrh8n14095sjxvdv9z86za0jj22xah84y0klh")))

(define-public crate-capnpc-0.12 (crate (name "capnpc") (vers "0.12.0") (deps (list (crate-dep (name "capnp") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1vwf8d1yn727vxxmq232kxsjddsxwlagyzbgf4pk3yb8xf588pl0")))

(define-public crate-capnpc-0.12 (crate (name "capnpc") (vers "0.12.1") (deps (list (crate-dep (name "capnp") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1wy37wgmw54w6b2ysg1k7qgf2mf6aivrmdfd4qplddahpn0cfhz7")))

(define-public crate-capnpc-0.12 (crate (name "capnpc") (vers "0.12.2") (deps (list (crate-dep (name "capnp") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0j3b9hkld4x93khhvvmgf2iklxjzk7a1i3bcmvv8ip5z1ck0kfx6")))

(define-public crate-capnpc-0.12 (crate (name "capnpc") (vers "0.12.3") (deps (list (crate-dep (name "capnp") (req "^0.12.2") (default-features #t) (kind 0)))) (hash "1sahpbg32f487gyb17pa3wagydn5c192qfmi1haf346k71j4ccda")))

(define-public crate-capnpc-0.12 (crate (name "capnpc") (vers "0.12.4") (deps (list (crate-dep (name "capnp") (req "^0.12.2") (default-features #t) (kind 0)))) (hash "1hd8131asf8zck9dpcb53l49phqcag67p4fkylbbixw3dc4y35qh")))

(define-public crate-capnpc-0.13 (crate (name "capnpc") (vers "0.13.0") (deps (list (crate-dep (name "capnp") (req "^0.13.0") (kind 0)))) (hash "1h4ipzq7mxrc9qjsxqilpsvz3a391wkf18z3qds7nf4jkilpzcrm")))

(define-public crate-capnpc-0.13 (crate (name "capnpc") (vers "0.13.1") (deps (list (crate-dep (name "capnp") (req "^0.13.0") (kind 0)))) (hash "1hbm5xqpam3f0ha5ds39wjmpqpzdalpbrls9wlp7m3slh3p5r1c1")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.0") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "1lsarw7dzihzcqq9mm4nr5mwd5i5hxl45gshdjwqkvgyy4lrhy79")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.1") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "0pbiidgng5wn0gq7x3v6crmkj262chii5swmczwy7ab98igilrfp")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.2") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "07jjf01hhjprr3ifi3m339pm2dj1x92c48lspxf4xdjzc2qhxibn")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.3") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "0jgmcgr7s42z6pq0cahcpl6is5pf11z25137d10qd0dl2z9skj2h")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.4") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "1zhis7h4wbhd4d7zc3jnpgkjm4nj9dcfsipp71f8nlb2260wwyxl")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.5") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "0bwm6lnk39d0wh2p7bfz53snbpp54g7h3qn5zh3ivh0ad1x2lbv8")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.6") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "0169wj67r9s9lv3a1xcqj81mmbaixniwasgzkk0cpxbn4a5iwh4y")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.7") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "1df04gg3v5p7cc0sy67cbpxmdmrbki8brk18nfl03b4jyy09pvf7")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.8") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "0s086yix4fccf4816vsgr2d5n5ilm9zxsy8y12spslhyllg8ws0m")))

(define-public crate-capnpc-0.14 (crate (name "capnpc") (vers "0.14.9") (deps (list (crate-dep (name "capnp") (req "^0.14.0") (kind 0)))) (hash "1aisyk7rp1jxw2drf9a6c957d06vjy7nr8di0zq4yvb6hkfg3jdx")))

(define-public crate-capnpc-0.15 (crate (name "capnpc") (vers "0.15.0") (deps (list (crate-dep (name "capnp") (req "^0.15.0") (kind 0)))) (hash "1y93b611mcxcc2jw87mkbvdgh5ishkxr0v94xdjyv5qwkq2y1n8f")))

(define-public crate-capnpc-0.15 (crate (name "capnpc") (vers "0.15.1") (deps (list (crate-dep (name "capnp") (req "^0.15.0") (kind 0)))) (hash "041p17f564yinwkvmm0ykbz0byf7kqavd72lijq50xg1dr5rann7")))

(define-public crate-capnpc-0.15 (crate (name "capnpc") (vers "0.15.2") (deps (list (crate-dep (name "capnp") (req "^0.15.0") (kind 0)))) (hash "1s9y154pmdydwkj46gdwfbjjb1wdv8akmdbjkm7lbrcqha734ss7")))

(define-public crate-capnpc-0.16 (crate (name "capnpc") (vers "0.16.0") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (kind 0)))) (hash "1cajx8203wawa604hmbfvj5s12rry8is4wyma0y6s9gg5plvzv31")))

(define-public crate-capnpc-0.16 (crate (name "capnpc") (vers "0.16.1") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (kind 0)))) (hash "074457698fxghb5a35rm38iynhznqsscfjbgcxfzn3ljn0sps53l")))

(define-public crate-capnpc-0.16 (crate (name "capnpc") (vers "0.16.2") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (kind 0)))) (hash "080qcf07ill4dpw4z67nr3np4kd98dnwh27sh9b10313jcgs4sbj")))

(define-public crate-capnpc-0.16 (crate (name "capnpc") (vers "0.16.3") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (kind 0)))) (hash "1l2a1rk81fysw5xs1yli0vi48r6b6jx91fy35yjrpigyng0a0icb")))

(define-public crate-capnpc-0.16 (crate (name "capnpc") (vers "0.16.4") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (kind 0)))) (hash "1i7cwkid5hc1dp8jdjfdc188lfs8i0jd19kb78s07lac0l41dvhv")))

(define-public crate-capnpc-0.16 (crate (name "capnpc") (vers "0.16.5") (deps (list (crate-dep (name "capnp") (req "^0.16.0") (kind 0)))) (hash "031nm05wrn3qnnvwi9jvd5yjk9g02myp76d7402gva5h3b1jzdsm")))

(define-public crate-capnpc-0.17 (crate (name "capnpc") (vers "0.17.0") (deps (list (crate-dep (name "capnp") (req "^0.17.0") (kind 0)))) (hash "1k084c2cfhlk32mrgazky001cdnmchi2jsppxsg7gb14q8ajhvyr")))

(define-public crate-capnpc-0.17 (crate (name "capnpc") (vers "0.17.1") (deps (list (crate-dep (name "capnp") (req "^0.17.0") (kind 0)))) (hash "1myvyzc62brrjlrbrnraqvvc200kmg28x0axas8ahs34ymkavack")))

(define-public crate-capnpc-0.17 (crate (name "capnpc") (vers "0.17.2") (deps (list (crate-dep (name "capnp") (req "^0.17.0") (kind 0)))) (hash "0aq4ysmfpnqq111pg8rwamy7gy163bhixicwiqc3ariyzdikgg7v")))

(define-public crate-capnpc-0.18 (crate (name "capnpc") (vers "0.18.0") (deps (list (crate-dep (name "capnp") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0bcwyz7f2lah8xvavhdi4w0k7zx6ayd3w5ah67897nclxv4g6rsh")))

(define-public crate-capnpc-0.18 (crate (name "capnpc") (vers "0.18.1") (deps (list (crate-dep (name "capnp") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0cdi91h5j3bd89a51cd3xp7m6d917iclq0f0vh5yg1w1lymglhm6")))

(define-public crate-capnpc-0.19 (crate (name "capnpc") (vers "0.19.0") (deps (list (crate-dep (name "capnp") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1v49w7zsv4bkdn88dfmi2hk5dzv5pgs0qwgkq99jsn081w7a6ny7")))

