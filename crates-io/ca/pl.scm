(define-module (crates-io ca pl) #:use-module (crates-io))

(define-public crate-caplog-0.1 (crate (name "caplog") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0fyc002b6xbn25w8q6i03mvlgsr273cqb6glmnn86svmd8cvzg6p") (yanked #t)))

(define-public crate-caplog-0.2 (crate (name "caplog") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "04pcl5dqc67jwzigzblf2ik05chdr9pvgwahl1i6iaj1myzqw5s1")))

