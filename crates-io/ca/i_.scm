(define-module (crates-io ca i_) #:use-module (crates-io))

(define-public crate-cai_cargo-0.1 (crate (name "cai_cargo") (vers "0.1.0") (hash "1jya4qmp4m41r3hp7fj7qkimf0k57g6fbh5181yzn9arrvp70pda")))

(define-public crate-cai_cargo-0.1 (crate (name "cai_cargo") (vers "0.1.0-SNAPSHOT") (hash "0v6dqr450xazhlm8dww1pri8cafhh0mwf7g5xwp957l0lrpl3brc")))

(define-public crate-cai_cyclic-0.1 (crate (name "cai_cyclic") (vers "0.1.0") (deps (list (crate-dep (name "binfield_matrix") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mw8j37qf75jidzwvva3wpcjs92n9vgcd4xxbp4kk34mhb8lk64j")))

(define-public crate-cai_cyclic-0.1 (crate (name "cai_cyclic") (vers "0.1.1") (deps (list (crate-dep (name "binfield_matrix") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10y3kzxip7i9js9vvsbgw035w2yygr75rds8113mx3sl30ash0bl")))

(define-public crate-cai_cyclic-0.1 (crate (name "cai_cyclic") (vers "0.1.2") (deps (list (crate-dep (name "binfield_matrix") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "17i605x6wcz6r6brdflnynhz5rdqfw04mxx931jxh61y2z36irpa")))

(define-public crate-cai_golay-0.1 (crate (name "cai_golay") (vers "0.1.0") (deps (list (crate-dep (name "binfield_matrix") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0v26c3rwsyipsy36i9l0abrpv6v375r6mdxprl2164diq342d29c")))

(define-public crate-cai_golay-0.1 (crate (name "cai_golay") (vers "0.1.1") (deps (list (crate-dep (name "binfield_matrix") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0768n92z17lfaav1aski0q5r0l9ys5crpvcr2m99qg0rlma26rnb")))

(define-public crate-cai_hamming-0.1 (crate (name "cai_hamming") (vers "0.1.0") (deps (list (crate-dep (name "binfield_matrix") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1347kgpqhhmphh0l7yfsbfpxmm98dnfjhd3x1d3c06d3ifk5m6hc")))

