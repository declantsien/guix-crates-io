(define-module (crates-io ca dn) #:use-module (crates-io))

(define-public crate-cadnano-format-0.1 (crate (name "cadnano-format") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "14l3pgrgb6hwik44dcv5nyi8cnhq7y1rm06mpw2isb68h4qc6bpa")))

