(define-module (crates-io ca es) #:use-module (crates-io))

(define-public crate-caesar-0.1 (crate (name "caesar") (vers "0.1.0") (deps (list (crate-dep (name "openssl") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "0r5fg1hrg03fmjwnxndczznhfwx6wc9f07q8bbphgskmwnglpw6k") (features (quote (("tlsv1_2" "openssl/tlsv1_2") ("rfc5114" "openssl/rfc5114") ("default" "tlsv1_2" "rfc5114"))))))

(define-public crate-caesar-0.2 (crate (name "caesar") (vers "0.2.0") (deps (list (crate-dep (name "openssl") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "1mjskf14rpd7m7059q8h0wanvxxzgr2pncx2gdj1hmxy2ww1yj1g") (features (quote (("tlsv1_2" "openssl/tlsv1_2") ("rfc5114" "openssl/rfc5114") ("default" "tlsv1_2" "rfc5114"))))))

(define-public crate-caesar_cipher-0.1 (crate (name "caesar_cipher") (vers "0.1.0") (hash "1dfbfvr5c6rlr0zd1xm8xih3m594vqjck9zqcknw326m250yvc9f")))

(define-public crate-caesar_cipher-0.1 (crate (name "caesar_cipher") (vers "0.1.1") (hash "1kxhmi5msh9zv2j2rb66vyspf3aprf551agfx9fncipi6mvsdfy1")))

(define-public crate-caesar_cipher_cli-0.1 (crate (name "caesar_cipher_cli") (vers "0.1.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "1hv8xcgyxn9sbkfd803cq5qnbkc6daj6w2rz5l101ajq9ba0ljhd")))

(define-public crate-caesar_cipher_cli-0.1 (crate (name "caesar_cipher_cli") (vers "0.1.1") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0mdkcml6s48vlf3dm69rv06f7gxfg8c5kj2vfjp46hwqhfggiarw")))

(define-public crate-caesar_cipher_cli-0.1 (crate (name "caesar_cipher_cli") (vers "0.1.2") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0amndbw6rakxy7wcmzbwqrhas2980v1i4y3sgswhsdknfnz09zz2")))

(define-public crate-caesar_cipher_enc_dec-0.1 (crate (name "caesar_cipher_enc_dec") (vers "0.1.0") (hash "02iw377y3gz26hskd46086xldjdhjmw19yhjb2d2pmif22flx6yl")))

(define-public crate-caesar_cipher_enc_dec-0.2 (crate (name "caesar_cipher_enc_dec") (vers "0.2.0") (hash "0m5x3hsc04df1yl3k8466xwpjfmvk45x3xcfjari6mq20i2xjyxl")))

(define-public crate-caesar_cipher_enc_dec-0.2 (crate (name "caesar_cipher_enc_dec") (vers "0.2.1") (hash "08wr7fldi540wyfa0g4n930hvdsy6az92pi2xnpq97cch9y6snsx")))

(define-public crate-caesar_cipher_enc_dec-0.3 (crate (name "caesar_cipher_enc_dec") (vers "0.3.0") (hash "11pk3mqac8lb62njnpxl48glydfcbbzpl1gnxmbpvh4zl10kwkix")))

(define-public crate-caesar_cipher_enc_dec-0.4 (crate (name "caesar_cipher_enc_dec") (vers "0.4.0") (hash "190vaxwf67f7b8kgb9sajcsc07cmh4pwm3l3k2f1630bk0dd9h2v")))

(define-public crate-caesar_cipher_enc_dec-0.5 (crate (name "caesar_cipher_enc_dec") (vers "0.5.0") (hash "1wn2w3a0prj8yrhyqy1k62y7gpab5g5jw2kqg7nz7qp8jcrhd0vl")))

(define-public crate-caesar_cipher_enc_dec-0.5 (crate (name "caesar_cipher_enc_dec") (vers "0.5.1") (hash "0diicrs9b2502vs588qjwpsm28k367nv81mp5kj6907gd0hd0mn8")))

(define-public crate-caesar_cipher_enc_dec-0.5 (crate (name "caesar_cipher_enc_dec") (vers "0.5.2") (hash "0dxl7hnnch29s6vb397x3q35ahwaxvqy9b65qbn41x9vfkgl8pqn")))

(define-public crate-caesar_cipher_enc_dec-0.5 (crate (name "caesar_cipher_enc_dec") (vers "0.5.3") (hash "0mc45mp35i5xns9cnbdahz8k8rjixi1j1zk5ncf07ccvglbijsd7")))

(define-public crate-caesar_cipher_enc_dec-0.6 (crate (name "caesar_cipher_enc_dec") (vers "0.6.0") (hash "08vzgq4mwkw9cb9vk97hxf9y76xvnxbl5z3fzwhh5nc23kdcb65g")))

(define-public crate-caesar_cipher_enc_dec-0.6 (crate (name "caesar_cipher_enc_dec") (vers "0.6.1") (hash "17z3hh7x35pss29frw7v2hd3zfy6v0amm6kmd8g0j45qhsjj0xn7")))

(define-public crate-caesar_cipher_enc_dec-0.6 (crate (name "caesar_cipher_enc_dec") (vers "0.6.2") (hash "0570bvycc268vzjjpfnxqkl41bxripnfb0nixfn4fv5vkgvqwm92")))

(define-public crate-caesarcy-0.1 (crate (name "caesarcy") (vers "0.1.0") (hash "1ysl6m72rz0qhd6ysii99ji7xavwrbfrhwzqci17sf86f0zafjmi")))

(define-public crate-caesarcy-0.1 (crate (name "caesarcy") (vers "0.1.1") (hash "13s4fi8d5sj7h2n5mrg5w5aqi4njjb1lvdmcwlbl5pvw4ga0c4vd")))

(define-public crate-caesarlib-0.1 (crate (name "caesarlib") (vers "0.1.1") (hash "1r0p795fr182nfixaryzbr7avkbsws3nhrhpk60aix7v8m3cxvcf")))

(define-public crate-caesarlib-0.1 (crate (name "caesarlib") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1icyyrsk6zgn6ydvcyqjcx8w0xjl1d5nvcn31xhmm0ijry4gshp9")))

(define-public crate-caesarlib-0.1 (crate (name "caesarlib") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0dk4si4fvd5n87slfbmnci2pn17c250d9sa60qqasiw0b26ikmg3")))

(define-public crate-caesarlib-0.2 (crate (name "caesarlib") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1dhp3lw2q42pl6v5pss4a54mnwj2g5rrbp723hfygrxdspvmww5m")))

(define-public crate-caesium-0.1 (crate (name "caesium") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "router") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1pfak5krbd7ginqfs77mcajgj4zmzf9bvsab2qqb9v5gbg0f113q")))

