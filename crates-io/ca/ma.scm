(define-module (crates-io ca ma) #:use-module (crates-io))

(define-public crate-camarim-0.1 (crate (name "camarim") (vers "0.1.0") (deps (list (crate-dep (name "android_logger") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "syslog") (req "^5.0.0") (default-features #t) (target "cfg(target_os = \"ios\")") (kind 0)))) (hash "00xlx9p57km6m8kh2m0pn40k5v936avblv6fk9k41c0q9fhzb0v8")))

