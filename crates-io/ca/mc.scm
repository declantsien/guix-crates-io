(define-module (crates-io ca mc) #:use-module (crates-io))

(define-public crate-camctl_rs-0.1 (crate (name "camctl_rs") (vers "0.1.0") (deps (list (crate-dep (name "libusb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "usb-device") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0pp093p4nrf1dyfmi43wlp49xlw8cvz7gmm1mahq88kc9gpki4f4")))

(define-public crate-camctl_rs-0.1 (crate (name "camctl_rs") (vers "0.1.1") (deps (list (crate-dep (name "libusb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "usb-device") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ssmhh2n2pk3py8w3bnmdslk32mqsbahm2c9fp5schllfjnf5m2s")))

(define-public crate-camctl_rs-0.1 (crate (name "camctl_rs") (vers "0.1.2") (deps (list (crate-dep (name "libusb") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "usb-device") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "06w1zy7wnv6699sv8irdq2cfxckkh92qgcgn2xfjj1hpq47d9ghw")))

(define-public crate-camctrl-0.1 (crate (name "camctrl") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rbt3") (req "^0") (default-features #t) (kind 0)))) (hash "0si4ca756w76m0g1sfjs7xwwx8kdfrnm4ax59q1i6w5ysxbcq7n4")))

(define-public crate-camctrl-0.2 (crate (name "camctrl") (vers "0.2.0") (deps (list (crate-dep (name "glam") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rbt3") (req "^0") (default-features #t) (kind 0)))) (hash "0wv2nz1mvw4hxhs2qix2cwncsqbxw4r5j9j6zi424f6c8wqnf5bj")))

