(define-module (crates-io ca -r) #:use-module (crates-io))

(define-public crate-ca-rules-0.1 (crate (name "ca-rules") (vers "0.1.0") (hash "0bg9jskcv4fwyklfp4xjs9nv5padw5bd98yqgffz9fyzz7y4n444")))

(define-public crate-ca-rules-0.2 (crate (name "ca-rules") (vers "0.2.0") (hash "09f91swvsmyq55sxfwf2g1wrdd9y9w3s59cana6fbmzqqskk6ikd")))

(define-public crate-ca-rules-0.3 (crate (name "ca-rules") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1l5m328vyn0kiyiywpj4wy7i7w55qnhaq4sgqgjfgrbwarzxz6m3")))

(define-public crate-ca-rules-0.3 (crate (name "ca-rules") (vers "0.3.1") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0ixdb26apn559yv044j97948qz36jc2i90rdj68zb2pdpv9q58mj")))

(define-public crate-ca-rules-0.3 (crate (name "ca-rules") (vers "0.3.2") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "12iyj2ivwrzm650gpksjhvinspiy7317spsy96zw7llsrrsfqihy")))

(define-public crate-ca-rules-0.3 (crate (name "ca-rules") (vers "0.3.3") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1sc2hsxhbp6iv0x201n606crimc2sdrag60l916309bv7skg4a97")))

(define-public crate-ca-rules-0.3 (crate (name "ca-rules") (vers "0.3.4") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "17xqidfxqc4mnnpjb7jh4knjkc2ys0ilhm5igsmz175gx0ril2ki")))

(define-public crate-ca-rules-0.3 (crate (name "ca-rules") (vers "0.3.5") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0np5gfbwmw54birxrmk9zz2m7592zjnr4m0x0ddv8dx2rx0n85kd")))

