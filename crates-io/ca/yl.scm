(define-module (crates-io ca yl) #:use-module (crates-io))

(define-public crate-cayley-0.1 (crate (name "cayley") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1gm85b6n1icf45dlmgw7g63649cbqa5nvscv6f1b8pv4wslcl7y1")))

(define-public crate-cayley-0.2 (crate (name "cayley") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1wf6h42rjnm4y4w50w6dnknfif8h921ipv8awky19qwzphf9fazk")))

