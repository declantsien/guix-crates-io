(define-module (crates-io ca -t) #:use-module (crates-io))

(define-public crate-ca-term-0.0.1 (crate (name "ca-term") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1j88hin1n5zwa5dagxs2c91f3z0b5x54g6b9nm3xv2ip4jjsqncz")))

(define-public crate-ca-term-0.0.2 (crate (name "ca-term") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0jqnng879gd9akqlj6mzzg8ppz7qbddp0f8ji7avy9cx5ykcxs3p")))

(define-public crate-ca-term-0.0.3 (crate (name "ca-term") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1a11y30qmi57xsndxn6xs8wbkfw3ickj9l27y8xhz1fcvc8pv1pw")))

(define-public crate-ca-term-0.0.4 (crate (name "ca-term") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.0") (features (quote ("termination"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0xxzjwl2hjajinq8lk028dxlrp2rkpxmlc5agyp3ij50xky6mxb1")))

