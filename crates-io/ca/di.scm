(define-module (crates-io ca di) #:use-module (crates-io))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0gfnn8smnjbv7gpn0qja32ys8yrb5b1b5jfzmw8lh3fc404552k9") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "10djwkilk1bfxhf9d7zc1iayd6kl3akcmj5s1md86cdmd613cngf") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0djz8b512rhbvyv9lyyq9nq2hx2yk03lqhbss3ak7yr4ihn3qj6g") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0lbh8lbnjq56c6yjdrfki409684q3gashgmdsa9rdzxb1irj1nvd") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "03n7j0r96b2hb6sdgs2al6xb4141wwwf4vfpv0gxz42wjs56dwg2") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1i2k63hprx9flnvp5l6fphd5fnx5lm7caj4vqjvh5khzrkhx5k53") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.7") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0n5is3ln0088pi01755smk6xaym5xcv9zj1b3073yzdkw2s0clw1") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.8") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1qnz1zaxpz1kidncvvmkwschkm6fkvbjyambk65h7ssnsig365xa") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.9") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "08p7rsnh8cnw8cr7q049a0ssxl7w16nzk0yayi2jrcjcnvsizzw7") (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.10") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0ivxik17gjzsflabj78r3vjw3pcy2vk7vr1v8kgil3fyhqr8bw9y") (features (quote (("cpp-debug")))) (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.11") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "13922jwwdwbw8i599hsgwyplb36476y6y53m2j7lahb4l86wp22y") (features (quote (("cpp-debug")))) (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.12") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1sc7r38jp7k2clvzwg1rvm2p6i7gr1c79fmi4ih1b9qyviiz0mf0") (features (quote (("cpp-debug")))) (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.13") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1x8gpydxfi4grxghh904a1vlk6kafwspdkyj0l0ylljl0agyv6jh") (features (quote (("cpp-debug")))) (links "ccadical")))

(define-public crate-cadical-0.1 (crate (name "cadical") (vers "0.1.14") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1f2ifimpl2y2rr8npnd31hbb1lrkvplbaj2dwvfggcv8dh2d75jf") (features (quote (("cpp-debug")))) (links "ccadical")))

(define-public crate-cadir-0.1 (crate (name "cadir") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1vqsnnhzgjhm9nrvpmbbjayga70i9p3wyar7rjy1cfr7irdpdscr")))

(define-public crate-cadir-0.1 (crate (name "cadir") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1v8araizwwnx4n460q6icxvknbnswjpcj3ff117gdh9z5a326pz3")))

(define-public crate-cadir-0.1 (crate (name "cadir") (vers "0.1.2") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1qqjmf7jar1bvik4hnp5zk80wmsalpjaxg608mx6l0ggj7qj60yl")))

