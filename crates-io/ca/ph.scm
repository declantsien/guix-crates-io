(define-module (crates-io ca ph) #:use-module (crates-io))

(define-public crate-caphindsight_fft-0.1 (crate (name "caphindsight_fft") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1qd1z0n214ziipmz3vgdiblcng2676nmn565p8vv11b7d8n7qwb2")))

