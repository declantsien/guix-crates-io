(define-module (crates-io ca ct) #:use-module (crates-io))

(define-public crate-cacti-0.0.0 (crate (name "cacti") (vers "0.0.0") (hash "1kmiq31rdf85fjic2f4zfw04vmvr3vrh16qf1g4d0h7vryl3qc54")))

(define-public crate-cacti_cfg_env-0.0.0 (crate (name "cacti_cfg_env") (vers "0.0.0") (hash "1g6n87yr6gzrrn687vyibm3yx1f7kwkhp41b1z9gbv1l21qljhc3")))

(define-public crate-cacti_gpu_cu_ffi-0.0.0 (crate (name "cacti_gpu_cu_ffi") (vers "0.0.0") (hash "0mddd49lfjqmjmycy6w90bb0mfc06a61m8zq6rzf7ah3iz5wygmx")))

(define-public crate-cacti_smp_c_ffi-0.0.0 (crate (name "cacti_smp_c_ffi") (vers "0.0.0") (hash "0g6pp5mdzdaz8rz5j0lzv31zf8kmhn0mqw7gy0yl63g0xnw59k83")))

(define-public crate-cacti_weaver_protos_rs-2 (crate (name "cacti_weaver_protos_rs") (vers "2.0.0-alpha-prerelease") (deps (list (crate-dep (name "prost") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8.3") (features (quote ("tls"))) (default-features #t) (kind 0)))) (hash "0vfjp39a8iqmlzm5gnn9k3p31lnsanasdlsm71h5w9m9lnbaw95n")))

(define-public crate-cacti_weaver_protos_rs-2 (crate (name "cacti_weaver_protos_rs") (vers "2.0.0-alpha.1") (deps (list (crate-dep (name "prost") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8.3") (features (quote ("tls"))) (default-features #t) (kind 0)))) (hash "12ww4lf781fl05kxih9yscjgd51kqxnxskm9w39rqxl4ymg6sc02")))

(define-public crate-cacti_weaver_protos_rs-2 (crate (name "cacti_weaver_protos_rs") (vers "2.0.0-alpha.2") (deps (list (crate-dep (name "prost") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8.3") (features (quote ("tls"))) (default-features #t) (kind 0)))) (hash "1fs806c92lg5kd4rqfpway9mdryhi9rfqpqdv6nwb3pd2hgx3d0k")))

(define-public crate-cactive_hypixel_api-0.1 (crate (name "cactive_hypixel_api") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rf4f898pa8m85gpls58d2gpwfhqpb45bg1bqinsq6m5nn0j6zjf")))

(define-public crate-cactus-0.1 (crate (name "cactus") (vers "0.1.0") (hash "0ya2wg5a90p51b88qv7klz2kl45pqgha4klasrnbvqh60vmc1yaq")))

(define-public crate-cactus-0.1 (crate (name "cactus") (vers "0.1.1") (hash "104qkwkjmzim3kqmm102r7yw4npz2bng9i4f58kv71nbmqy7209m")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.0") (hash "1pjni13dqnsp7jlbwymmmi2yr3nl1kya3jw55v8hgjii9g17vxwk")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.1") (hash "11fad2ch80vycyj2124sqqbh9a3hhmc7r2bv92vwzzy1xdv3aqp7")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.2") (hash "123c4100ck6pdc0kn6kgn5f7rwwrmdcxmn7pgyslnjjgl73dswj5")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.3") (hash "15skgi5mshry8cqy1ij6vy8iwpc9nswdnmq8hbrcw1kp6ak5iqmv")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.4") (hash "10c59ss4abiw3zv486s0sqdnfrai8hf9mb4rz4qyrsrg1z999lp1")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.5") (hash "1sv86khqnyix12k18dr6glvvl876xrs7c5ck3b3f8cr7mkncj4q7")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.6") (hash "1xfwhzfp562qasmny3jqdccfi5dzh82qi7k1dlf036ninxjlf0yg")))

(define-public crate-cactus-1 (crate (name "cactus") (vers "1.0.7") (hash "0r05bx1632735gqndgawwpskrgq2jh5g2g9f8jvz87c75lw2dg5c")))

(define-public crate-cactusref-0.1 (crate (name "cactusref") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.8.4") (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("inline-more"))) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (kind 0)))) (hash "0ipsibwlm1s1c7p6dvla7rgccbn25kj77d19wb2x4pjhxkv367g4")))

(define-public crate-cactusref-0.1 (crate (name "cactusref") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.8.4") (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("inline-more"))) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1") (kind 0)) (crate-dep (name "version-sync") (req "^0.9, >=0.9.2") (default-features #t) (kind 2)))) (hash "1yxivqw6g1ks39nw4qiayn3a6i8291g9r2rr7bw94y3z9sqkhkkf")))

(define-public crate-cactusref-0.2 (crate (name "cactusref") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.0") (features (quote ("inline-more"))) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (kind 0)) (crate-dep (name "version-sync") (req "^0.9.3") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "0il4d7naavf6609hzd5n298zj18zzhvpqqc0xq0cb8mnm78vhxpj") (features (quote (("std") ("default" "std"))))))

(define-public crate-cactusref-0.3 (crate (name "cactusref") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (kind 2)) (crate-dep (name "hashbrown") (req "^0.13.1") (features (quote ("inline-more"))) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (kind 0)) (crate-dep (name "version-sync") (req "^0.9.3") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "0i0lqa66h1zabm5y4v5yz4f38w90n8izx24r5lcmq0q54raf08n0") (features (quote (("std") ("default" "std")))) (rust-version "1.56.0")))

(define-public crate-cactusref-0.4 (crate (name "cactusref") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.0") (features (quote ("inline-more"))) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (kind 0)) (crate-dep (name "version-sync") (req "^0.9.5") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "07dqnsaj20sp1cf20r3mzmk0qm8njbkks4g93gpawan9585i4y85") (features (quote (("std") ("default" "std")))) (rust-version "1.56.0")))

(define-public crate-cactusref-0.5 (crate (name "cactusref") (vers "0.5.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.0") (features (quote ("inline-more"))) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (kind 0)) (crate-dep (name "version-sync") (req "^0.9.5") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "1hsv5jnhx3ga5mp439sr5wk3156bcm797y2if1axg8rd6b260nii") (features (quote (("std") ("default" "std")))) (rust-version "1.77.0")))

