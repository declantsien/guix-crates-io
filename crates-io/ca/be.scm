(define-module (crates-io ca be) #:use-module (crates-io))

(define-public crate-caber-0.1 (crate (name "caber") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "1rf0gqqvhdqr31g8186k7vg0kh8ng7fxsf2w8rir9c92jl71c45v") (features (quote (("cli" "clap"))))))

(define-public crate-caber-0.1 (crate (name "caber") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "0md94ixsc8brh46d2zr9x9mdlaxa1k2ja5pqqdxkyl5qdn7w1hvz") (features (quote (("cli" "clap"))))))

