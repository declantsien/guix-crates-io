(define-module (crates-io ca ea) #:use-module (crates-io))

(define-public crate-caead-0.0.1 (crate (name "caead") (vers "0.0.1") (deps (list (crate-dep (name "aead") (req "^0.5") (kind 0)) (crate-dep (name "digest") (req "^0.10") (kind 0)))) (hash "0qgh2y0a9iffmfm7bxyx6h64zxrnpj30kqzkldbnyrkfwbbhdrd3") (features (quote (("std" "aead/std" "digest/std") ("alloc" "aead/alloc" "digest/alloc")))) (rust-version "1.68")))

