(define-module (crates-io ca os) #:use-module (crates-io))

(define-public crate-caos-0.0.1 (crate (name "caos") (vers "0.0.1") (hash "08nm067p6qraxw4vyf2i6y4z40mjmhpxvrdzh1654jxy34s68d2b")))

(define-public crate-caos-0.0.2 (crate (name "caos") (vers "0.0.2") (hash "0h311sw21xyc6yh0jlvnyrrk42chfcfhvk2wn5kn5aafishgsdf4")))

(define-public crate-caos-0.0.3 (crate (name "caos") (vers "0.0.3") (hash "163l1fnwadbvbdb2jdhqciwgpgrnq0ygncd700jv4alfhy8zrma2")))

(define-public crate-caos-0.0.4 (crate (name "caos") (vers "0.0.4") (hash "1ps7mrpvhnz1zg4ngczcy4smmjixys8ssn46m99m3jh31gilaqdb")))

(define-public crate-caos-0.0.5 (crate (name "caos") (vers "0.0.5") (hash "00gdhl3hhhhwp196gkpz7nx07jwpdg5gjpcl2n5q5a4w44zb4cfi")))

(define-public crate-caos-0.0.6 (crate (name "caos") (vers "0.0.6") (hash "1jj5xh36wxxsa3nakjwmmmb4vi5395zkbvqsqr6vk3lvhj5mdwgp")))

(define-public crate-caos-0.0.7 (crate (name "caos") (vers "0.0.7") (hash "1plksl6jhfz6i7an6297583hindbprm16ry97mm634x3vawyfb0h")))

