(define-module (crates-io ca -f) #:use-module (crates-io))

(define-public crate-ca-formats-0.1 (crate (name "ca-formats") (vers "0.1.0") (hash "0jmg09mb2wgcma2bq07qrng1v9rj6h2yvvhiwff5flr69gg933np")))

(define-public crate-ca-formats-0.1 (crate (name "ca-formats") (vers "0.1.1") (hash "0vfb3pp7mqj92rwapp9145jjnadr43fizxds9rm1mi8wjzqlgcdi")))

(define-public crate-ca-formats-0.1 (crate (name "ca-formats") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "15gii5qqqzfl2rh8as99d4n33ij47rr48myi9if1jg6s6jhmdg62")))

(define-public crate-ca-formats-0.1 (crate (name "ca-formats") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1nhraanhd07d77pf1yrs3dcci719j1k1r0qm9l2yrkvj65g053h2")))

(define-public crate-ca-formats-0.2 (crate (name "ca-formats") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "0c07r9km2nxvsrq6vpfgiba8jnhn8m8alc7czf1kmvf5lh8ivx68")))

(define-public crate-ca-formats-0.2 (crate (name "ca-formats") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "0n0nhjv55xdsrqny05m6mgmip2fa71fckk7qs7bgdvh4m2ip91xb")))

(define-public crate-ca-formats-0.2 (crate (name "ca-formats") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "04f67whbiln69qvxhqjhr75wlnjj8ihlf4k7f9vjn13dqqrpx0jw")))

(define-public crate-ca-formats-0.2 (crate (name "ca-formats") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1rlzy91bbhh105pl2lmgq123zx9jyggj8v0kb7mvxf82m3r4lqpz")))

(define-public crate-ca-formats-0.2 (crate (name "ca-formats") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1hks5an11064gpjfdzldvbk6gzpqx4pgxvx3siy24q7k9c3mjkpl") (features (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3 (crate (name "ca-formats") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0d5rd340i1zbl7a2j13bzxiln2csdhgvmvd4rqrqarc9qy6l8qlb") (features (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3 (crate (name "ca-formats") (vers "0.3.1") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "07ws7qphrrcjgji4xfl3w9m8di29498rnzkhwafgmaj8xx7nd58q") (features (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3 (crate (name "ca-formats") (vers "0.3.2") (deps (list (crate-dep (name "lazy-regex") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1fnqix02zqvr814m56sbgxyiv3a9hww82wviwwcj91427dgl4lv4") (features (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3 (crate (name "ca-formats") (vers "0.3.3") (deps (list (crate-dep (name "displaydoc") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0hgfpzcfagp5bjyi80c6cn2qg7vm6f2gadd5i7fl48152hj65ysc") (features (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3 (crate (name "ca-formats") (vers "0.3.4") (deps (list (crate-dep (name "displaydoc") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0rbhx6g6nw9xg8fhryd1wml8b593qjrr836k0z81xh8dvxsbmy23") (features (quote (("unknown") ("default"))))))

(define-public crate-ca-formats-0.3 (crate (name "ca-formats") (vers "0.3.5") (deps (list (crate-dep (name "displaydoc") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1rz43sx4yc61d23rrlqvgmmyixkn3pfmr4p1lm6d0w8bwww94gpx") (features (quote (("unknown") ("default"))))))

