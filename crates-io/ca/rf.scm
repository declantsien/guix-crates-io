(define-module (crates-io ca rf) #:use-module (crates-io))

(define-public crate-carfter126_guessing_game-0.1 (crate (name "carfter126_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0zi57ggsz1145hvgkkqw465cvf4lg3hr1b2r71zyc21zn2pmlg8p")))

