(define-module (crates-io bu sb) #:use-module (crates-io))

(define-public crate-busbus-0.1 (crate (name "busbus") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0knc9immwaxzzmlw2zlydkpwswazjfs5saxz5dlj9rqawrii3xlc")))

