(define-module (crates-io bu lb) #:use-module (crates-io))

(define-public crate-bulbb-0.0.1 (crate (name "bulbb") (vers "0.0.1") (hash "05g12apm1ys272bq0cq3gfd926744bbrcw4nl7s431qxh35mbjfa")))

(define-public crate-bulbb-0.0.2 (crate (name "bulbb") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.130") (optional #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "zbus") (req "^1.9.2") (optional #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "15wg7hkcpkaf8gyxdxr9ic2386vxgxfzvs3382si2bz9nf1vxaaa") (features (quote (("default") ("dbus" "zbus" "serde"))))))

(define-public crate-bulbb-0.0.3 (crate (name "bulbb") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.130") (optional #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "zbus") (req "^1.9.2") (optional #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0zpx267v19hf5q0lp2g1k91x3qqp1srqivs0vs8ra0cq7c9qp2mi") (features (quote (("default") ("dbus" "zbus" "serde"))))))

