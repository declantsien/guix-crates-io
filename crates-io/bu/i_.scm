(define-module (crates-io bu i_) #:use-module (crates-io))

(define-public crate-bui_basic-0.0.1 (crate (name "bui_basic") (vers "0.0.1") (deps (list (crate-dep (name "bui") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "jni") (req "^0.19") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 2)))) (hash "06lc7l378v02z80wilxksn0i5234skgckmn2f21w5a5z5y2jgzi9") (yanked #t)))

