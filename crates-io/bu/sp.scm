(define-module (crates-io bu sp) #:use-module (crates-io))

(define-public crate-buspirate-0.1 (crate (name "buspirate") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "18h3b2cb2ai3qqj0z1gcgmywl09pw69sdmg3rihsfkqsv51j5m4g")))

