(define-module (crates-io bu gh) #:use-module (crates-io))

(define-public crate-bughouse-0.0.1 (crate (name "bughouse") (vers "0.0.1") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1vs0l5mlndpy4hnj1qf6d36w03wghy79bvlmg2f9lnn9awr4nln3")))

(define-public crate-bughouse-0.0.2 (crate (name "bughouse") (vers "0.0.2") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0riycwwpaj14q127bk0fz43hlyskfm1wggy59177y8pc4ak2bflq")))

(define-public crate-bughouse-0.0.3 (crate (name "bughouse") (vers "0.0.3") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1xfbaiid066sys0a4q4lnjqf4slssk9qy8b8jrfk9kc66kqgcnzw")))

(define-public crate-bughouse-0.0.4 (crate (name "bughouse") (vers "0.0.4") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1igm61hic140fa0a4f6770dd554l5wc5bas4q72m8v7wgm62bwnx")))

(define-public crate-bughouse-0.0.5 (crate (name "bughouse") (vers "0.0.5") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1msdxk3344l0fnzp4wwn07lahm9dvka9919zvr69x2sql9w8h6n3")))

(define-public crate-bughouse-0.0.6 (crate (name "bughouse") (vers "0.0.6") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c6f1j246ibj74r7c565dylpal20sdri4iqp80a403gw6r05sjcf")))

(define-public crate-bughouse-0.0.7 (crate (name "bughouse") (vers "0.0.7") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bjami7lvyv50ixdnqvkm3zxclikkqns8w0wcmz0iy5mvsc6hlwx")))

(define-public crate-bughouse-0.0.8 (crate (name "bughouse") (vers "0.0.8") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l99n2idaagqrsbnkjq1h3vndzz6m4jzy9n08zgfmqd9hyyp419q")))

(define-public crate-bughouse-0.0.9 (crate (name "bughouse") (vers "0.0.9") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0im1isjk4a52sid7gicll2ysy0gskb17049iy8sir210r8wy5ycz")))

(define-public crate-bughouse-0.0.10 (crate (name "bughouse") (vers "0.0.10") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "065x3331zhg0hqyq46mbylvh5k0vlraljw89iv011xcdvnwacfal")))

(define-public crate-bughouse-0.0.11 (crate (name "bughouse") (vers "0.0.11") (deps (list (crate-dep (name "chess") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1iaw878rdgg2s5zyrd9nvxbry4zs5ymzkr3yc4cndf9al2v8vcis")))

