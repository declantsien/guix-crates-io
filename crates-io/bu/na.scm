(define-module (crates-io bu na) #:use-module (crates-io))

(define-public crate-buna-0.1 (crate (name "buna") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.4") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "02xj1031l4qw2j6j6w0d9ca5kvyj0hl10iv9cjy83mgnhichcn65")))

