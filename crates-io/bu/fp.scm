(define-module (crates-io bu fp) #:use-module (crates-io))

(define-public crate-bufpool-0.1 (crate (name "bufpool") (vers "0.1.0") (deps (list (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1l496b9mdbfw5irds8g3d1jjflb2kjclwpdd1s8d0hg3g4ng0zq5")))

(define-public crate-bufpool-0.1 (crate (name "bufpool") (vers "0.1.1") (deps (list (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1fcwz0jn1bgz3m4kkn2bfa0mn8l5pshr8mj18gbnjb46hffdw2z4")))

(define-public crate-bufpool-0.1 (crate (name "bufpool") (vers "0.1.2") (deps (list (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0n55lrl4cvsqnrvlzniiqayja18k065d6cj8bphcjqqyp4qgr2hv")))

(define-public crate-bufpool-0.2 (crate (name "bufpool") (vers "0.2.0") (deps (list (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0filfabz8bgs97c9ri6ih8bmy8rbgvmafifwg8mpk1jixjyfwgfl")))

(define-public crate-bufpool-0.2 (crate (name "bufpool") (vers "0.2.1") (deps (list (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "15rpidxbpgxcw3ilyw2kfr3avajvy131520ggb5di1620482hfdl")))

(define-public crate-bufpool-0.2 (crate (name "bufpool") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.142") (default-features #t) (kind 2)) (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1jng1islaha72z1ml0hvarnhij83d6nz5islp3qfnbmy4hishjbp")))

(define-public crate-bufpool-0.2 (crate (name "bufpool") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.142") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0gb373g52qc9nq4hnmsb4d67wmbfsah5xvn5bjxp1zsv7kdb3kx3") (features (quote (("no-pool"))))))

(define-public crate-bufpool-fixed-0.1 (crate (name "bufpool-fixed") (vers "0.1.0") (deps (list (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0g397rkhpzpnr9nvavjx8iss51v08fnrwi8yxw58xgzx9c6784ii")))

(define-public crate-bufpool-fixed-0.2 (crate (name "bufpool-fixed") (vers "0.2.0") (deps (list (crate-dep (name "off64") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "08qrnjh4r59f10ww3g2hfckjbvvd64fa72gx9fw4fkvcfbzdabw2")))

