(define-module (crates-io bu f-) #:use-module (crates-io))

(define-public crate-buf-list-0.1 (crate (name "buf-list") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "1jxps0v3mjh9gxnhm5flhp0a52ar1lqwsf3nddpda2wmy9k95mbp") (rust-version "1.39")))

(define-public crate-buf-list-0.1 (crate (name "buf-list") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "1kwfhavmarws3y06njgs57iwiqvjg07g546s7ywa244v424s6al4") (rust-version "1.39")))

(define-public crate-buf-list-0.1 (crate (name "buf-list") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "0af54sdj4wvqwsf47h0zi68qgqhm1fj1lqbc1d8akiq6c73n7vj9") (rust-version "1.39")))

(define-public crate-buf-list-0.1 (crate (name "buf-list") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "10sj4qfm5hccdglrmq0a1f0q2cdr1czs7n188kkk6xx8dami7cny") (rust-version "1.39")))

(define-public crate-buf-list-1 (crate (name "buf-list") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "0byhvhgv2s8f8x4b0qwkjhrflnfnlhfzgvvmsqkvzw41qsqayla8") (rust-version "1.39")))

(define-public crate-buf-list-1 (crate (name "buf-list") (vers "1.0.1") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "0j1c9sxmh2d4dvdi0zbvrchh1ggv9g5z36l5aqrl0iyv4xdy8qpl") (rust-version "1.39")))

(define-public crate-buf-list-1 (crate (name "buf-list") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 2)) (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "dummy-waker") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "futures-io-03") (req "^0.3.25") (optional #t) (default-features #t) (kind 0) (package "futures-io")) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "053ym9pkip1qsyzfzy7af65kadg351xbydq5s8aayv67hi9bhm1z") (features (quote (("tokio1" "tokio") ("futures03" "futures-io-03")))) (rust-version "1.39")))

(define-public crate-buf-list-1 (crate (name "buf-list") (vers "1.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 2)) (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "dummy-waker") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "futures-io-03") (req "^0.3.25") (optional #t) (default-features #t) (kind 0) (package "futures-io")) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("io-std" "io-util" "macros" "rt"))) (default-features #t) (kind 2)))) (hash "0flwfh22y6jx9p5lipyl185m1jz8rqgfn0nq7qm3wjlrbmld2szm") (features (quote (("tokio1" "tokio") ("futures03" "futures-io-03")))) (rust-version "1.39")))

(define-public crate-buf-min-0.0.1 (crate (name "buf-min") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0an3ndwpgldnps42y6c19h7czqkkmn5h4q11zq46vc80wk3vmwv0") (features (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.0.2 (crate (name "buf-min") (vers "0.0.2") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1ifcz0g0czzj9xybkkjb189qqbj5csshfzigbg61lxagzhp30fp5") (features (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.1 (crate (name "buf-min") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0zw9qay0drvx9gr5lb8pr9h24b7nspfdb1rpf4khvgxm4f6bajjc") (features (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.1 (crate (name "buf-min") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "194wvzn0np8y5v34q6iksa5p4l06y1qsc8kazvg7qz6hm9lp1bmn") (features (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.1 (crate (name "buf-min") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "17g5hmyj0yjz3hgrbd8l1xzdwmd49cggqy7lirg3ng8z50r9c2qj") (features (quote (("bytes-buf" "bytes")))) (yanked #t)))

(define-public crate-buf-min-0.2 (crate (name "buf-min") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0ja3xbyv9267k3xh5qaajzf1737bj3bjpbn9qvbl3yyhc57707l8") (features (quote (("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.3 (crate (name "buf-min") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "bytes-tokio3") (req "^0.6") (optional #t) (default-features #t) (kind 0) (package "bytes")))) (hash "09fp5ac8b2zsavn23pj8rlw98xgsny43n8hkjjjqsn8xxv8f9zn1") (features (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.3 (crate (name "buf-min") (vers "0.3.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "bytes-tokio3") (req "^0.6") (optional #t) (default-features #t) (kind 0) (package "bytes")))) (hash "09gdcsdmyyd8j8349m32bq0hm8c2faprr655cimvd70xaq8nypax") (features (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf" "bytes"))))))

(define-public crate-buf-min-0.4 (crate (name "buf-min") (vers "0.4.0") (deps (list (crate-dep (name "bytes-tokio2") (req "^0.5") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "bytes-tokio3") (req "^0.6") (optional #t) (default-features #t) (kind 0) (package "bytes")))) (hash "10za6b9lkd9h0wz8qpghyckd66c0wl07sxhq0nrnppbbylfal5zs") (features (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.5 (crate (name "buf-min") (vers "0.5.0") (deps (list (crate-dep (name "bytes-tokio2") (req "^0.5") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "bytes-tokio3") (req "^1.0") (optional #t) (default-features #t) (kind 0) (package "bytes")))) (hash "0rg54sc4z6p3kcbb208v8xc0vrhdwh2w8558qc43crh8z7a0qaij") (features (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.6 (crate (name "buf-min") (vers "0.6.0") (deps (list (crate-dep (name "bytes-tokio2") (req "^0.5") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "bytes-tokio3") (req "^1.0") (optional #t) (default-features #t) (kind 0) (package "bytes")))) (hash "111rrxgsqnqnl2gcp2x8amwj76kzb2sxh6rl1q62a6xj934gyk5n") (features (quote (("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.6 (crate (name "buf-min") (vers "0.6.1") (deps (list (crate-dep (name "bytes-tokio2") (req "^0.5") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "bytes-tokio3") (req "^1.0") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "ntex-bytes") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "13cfqwmajwdy41p6plldz6x9fizxxys6ppxgsbh99yz2ky51qlzl") (features (quote (("ntex" "ntex-bytes") ("bytes-buf-tokio3" "bytes-tokio3") ("bytes-buf-tokio2" "bytes-tokio2"))))))

(define-public crate-buf-min-0.7 (crate (name "buf-min") (vers "0.7.0") (deps (list (crate-dep (name "bytes") (req "^1.2") (optional #t) (default-features #t) (kind 0) (package "bytes")) (crate-dep (name "ntex-bytes") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1hyr2yicp4xlpada6ndwdr3nmh0dykhqg9zqp9hg99b8bazxd8rl")))

(define-public crate-buf-min-0.7 (crate (name "buf-min") (vers "0.7.1") (deps (list (crate-dep (name "bytes") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ntex-bytes") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "146wb3p4n62jbka6ym7sagziydcbixwhamw0cknl49w4ys66km92")))

(define-public crate-buf-rand-0.1 (crate (name "buf-rand") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0yp2sxjw7rf3ssmvb5188zzcwcyp2m4dbxds6plmsk9ckv8c3dcd")))

(define-public crate-buf-rand-0.1 (crate (name "buf-rand") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "042kvyn35afkywcrk8ms0siyyha8197fiz48qid8hgvs9n2bn2pr")))

(define-public crate-buf-rand-0.1 (crate (name "buf-rand") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1ipj9svh7bzrc71sc2bm96sz6v5zs6p4vllj9qrf6vyy23lygkar")))

(define-public crate-buf-rand-0.1 (crate (name "buf-rand") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0laxb30dcbclzbn78iwkr5yvmj2xjbas1mr6dfqk45b7nnn499ca")))

(define-public crate-buf-read-ext-0.1 (crate (name "buf-read-ext") (vers "0.1.0") (hash "0cvzcjc7ksd7nni347cf4zhxfcj2gp1w3yrf50yydns78qv9q2bj")))

(define-public crate-buf-read-ext-0.2 (crate (name "buf-read-ext") (vers "0.2.0") (hash "0b6y0jdz3dqz0gb8s00xaqfd35s803i36z1j987vaby3z9r40f6c")))

(define-public crate-buf-read-ext-0.2 (crate (name "buf-read-ext") (vers "0.2.1") (hash "062wzyr7bfrlyzvad545ygbhnmg519frgvy4sr0cvaj5xbgf33cx")))

(define-public crate-buf-read-ext-0.3 (crate (name "buf-read-ext") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1aqrlk1abbjaxxz0mdqm9qzqym8dlklwr8r55ba643vsdkpknaxx") (features (quote (("default" "async") ("async" "futures" "tokio-io"))))))

(define-public crate-buf-read-ext-0.4 (crate (name "buf-read-ext") (vers "0.4.0") (hash "0g57zyil58pdsfsbm9vf576abfzr0mp99b6g9vg69g2v9v272b1f")))

(define-public crate-buf-ref-reader-0.2 (crate (name "buf-ref-reader") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2") (features (quote ("libc"))) (kind 0)))) (hash "0wgfrr3dvrjx8pwkwsf9avcb9qaj3j7y2c7m7fxfl4f3jdhx0sp0")))

(define-public crate-buf-ref-reader-0.3 (crate (name "buf-ref-reader") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "copy_in_place") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (features (quote ("libc"))) (kind 2)) (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fsrj8r7nfdwilv5b09d8pn3bgacls8628zf45k7h1fnnqcyngzn")))

(define-public crate-buf-trait-0.4 (crate (name "buf-trait") (vers "0.4.0") (hash "1l8pmxr532wv9vp0pxnrdlpr975xmnlvx5gj1z4azvzdbz796px8")))

(define-public crate-buf-trait-0.4 (crate (name "buf-trait") (vers "0.4.1") (deps (list (crate-dep (name "zerocopy") (req "^0.7") (default-features #t) (kind 0)))) (hash "1d0pxqvynln4p58n7ajl95fk0x772xvgxjpsqgb77h78f33szsi1")))

(define-public crate-buf-view-0.1 (crate (name "buf-view") (vers "0.1.0") (hash "1z5azf6m9kgwfwqfj3xwkil32gbcvjwpxzd0m01180vz2k9g4dd6")))

