(define-module (crates-io bu si) #:use-module (crates-io))

(define-public crate-business-0.1 (crate (name "business") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)))) (hash "1765841x5gn87yx1yiix0zns5sqsjdvdcccm3z91zf10sxca11sw")))

(define-public crate-business_central-0.1 (crate (name "business_central") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pw9066lr1wf94sjvificaxrnh38cj2kh46gkg8j27nxvw1hdybf")))

(define-public crate-businessday_jp-0.1 (crate (name "businessday_jp") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "holiday_jp") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "09bas3y8aqzsgpdyf9h5a34jmmf2qz9p8k8xs9gy1xi67w5sma0v")))

(define-public crate-businessday_jp-0.1 (crate (name "businessday_jp") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "holiday_jp") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1r66a9j44lc3szfb7fbhw88kgjlkf0jdwrps56jyflkla1kjb1sg")))

(define-public crate-businessfn-0.1 (crate (name "businessfn") (vers "0.1.0") (hash "0q48xmdibpl78kzl51nxbhzgk3yp9ddi4fr8zapv1syhbw0gzskg")))

