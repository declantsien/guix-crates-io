(define-module (crates-io bu rd) #:use-module (crates-io))

(define-public crate-burden-0.1 (crate (name "burden") (vers "0.1.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "08q5pbfs0wxaw6x1psvx561w38llgcc695f7dci5bcl7jvpvpvk2")))

(define-public crate-burden-0.1 (crate (name "burden") (vers "0.1.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0s9minxlhl46lr350s7sl93hx3r92y3nhjym7a55sj28cbnf46mk")))

(define-public crate-burden-0.2 (crate (name "burden") (vers "0.2.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "12i0big8wxkzcsjsvf4a7ancchqcb4dvzdq7ynbrs6w804n9ac5l")))

(define-public crate-burden-0.2 (crate (name "burden") (vers "0.2.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1c1gm3zl2ppaxdw44vbv8qr8ykngkd8l0b9i43nw0r1f65ngnk19")))

