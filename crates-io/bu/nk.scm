(define-module (crates-io bu nk) #:use-module (crates-io))

(define-public crate-bunk-0.1 (crate (name "bunk") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "include-bytes-plus") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "16y9pfmvxrbpnpmajvadm2nr7a45aamkn26qnwbs830nwrwh9sja") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-bunker-0.0.1 (crate (name "bunker") (vers "0.0.1") (hash "0hxyxlhqn9ghkqs9x7vf49acag7jk4d1v8nlc8vk10cw25vwjkqh") (yanked #t)))

(define-public crate-bunkill-0.0.1 (crate (name "bunkill") (vers "0.0.1") (hash "1gsc1h7xhd6pn10lz1h8aw6z8anfpjilal1p3czfa62zkivhazrw")))

