(define-module (crates-io bu n-) #:use-module (crates-io))

(define-public crate-bun-cli-0.1 (crate (name "bun-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "19fxd0dvqhyfyvq9daiw5yiik2fg6781a2b64r1689c6b3jn0ki4")))

(define-public crate-bun-cli-0.2 (crate (name "bun-cli") (vers "0.2.0") (hash "0zji6askb6lbjg2wwgrgqjdhkwl2b8id8yhx7057553z15a0fp1j")))

(define-public crate-bun-cli-0.3 (crate (name "bun-cli") (vers "0.3.0") (hash "1rr1vlkis1w2aygk11i49s62644x26sflypc9kcvy41dn6bkihpw")))

(define-public crate-bun-cli-0.3 (crate (name "bun-cli") (vers "0.3.1") (hash "1q3jdngjhmc392dccj3xkffiiji5fnp3b31281pr7kslibd8pa3b")))

