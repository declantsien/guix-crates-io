(define-module (crates-io bu ru) #:use-module (crates-io))

(define-public crate-buruma-0.1 (crate (name "buruma") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.3.6") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wylq3f5y6jvnjbjd0ky9ndb9x5wnwl6nv5bid38412qbz16r2zw")))

