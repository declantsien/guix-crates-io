(define-module (crates-io bu sm) #:use-module (crates-io))

(define-public crate-busmust-0.1 (crate (name "busmust") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "busmust-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)))) (hash "109j7a5wzr8x8bycdwdpkix6pks138ygpnn25y44mb6f4kycy8jh")))

(define-public crate-busmust-0.1 (crate (name "busmust") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "busmust-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)))) (hash "1mffyiiwn43g0cq34vvy2cypyqymdrnagjyf6b94hma8crakjacg")))

(define-public crate-busmust-0.1 (crate (name "busmust") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "busmust-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (default-features #t) (kind 2)))) (hash "1jzkphmxy1d95mlb8bfkxlcpi0pm0ddnngx9grjbfnxymxsa44g1")))

(define-public crate-busmust-0.1 (crate (name "busmust") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "busmust-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (default-features #t) (kind 2)))) (hash "0ayi2phjl2h4286ibadycrll8waq2b8qrb8gjdf7vd5n75gjllz0")))

(define-public crate-busmust-sys-0.1 (crate (name "busmust-sys") (vers "0.1.0") (deps (list (crate-dep (name "bitfield-struct") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1mzwkqmywlf69xsz15gwd02pb7pmcxb4whsnk5brl1py0whaqcg5")))

(define-public crate-busmust-sys-0.1 (crate (name "busmust-sys") (vers "0.1.1") (deps (list (crate-dep (name "bitfield-struct") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1f4l8gpnq5ns6k0sw120kznaysyccmir4hf3afjg4m7ilnvxbp8v")))

(define-public crate-busmust-sys-0.1 (crate (name "busmust-sys") (vers "0.1.2") (deps (list (crate-dep (name "bitfield-struct") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1lbg6vq0ni7vahnpq1y6c0f150s9kbn1hml9hi5dyrdfl3avss4r")))

(define-public crate-busmust-sys-0.1 (crate (name "busmust-sys") (vers "0.1.3") (deps (list (crate-dep (name "bitfield-struct") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "08mnd0vm53cxryfj3q9yhpmj52mc43na4kysbhfj29kwab263kh0")))

