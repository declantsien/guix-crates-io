(define-module (crates-io bu gg) #:use-module (crates-io))

(define-public crate-buggy-0.1 (crate (name "buggy") (vers "0.1.0") (hash "1qd3zv5s9w8j92n1ipmwdir2w55zi2q7za2f8k4y27xcs2n1xrzz")))

(define-public crate-buggy-macros-0.1 (crate (name "buggy-macros") (vers "0.1.0") (hash "16k9f5hq29wnczd7y9l3dihmk621ls7bkppya2gfypdp9rd9b3np")))

