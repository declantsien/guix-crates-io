(define-module (crates-io bu lu) #:use-module (crates-io))

(define-public crate-bulut-0.2 (crate (name "bulut") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0nxlfy8ahvs215xfyram9df51vqbzkqqfz7nbwfr93p8dff7lkba")))

(define-public crate-bulut-0.3 (crate (name "bulut") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.43") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0c60292x7hv21gkhyx54856945y9pnbz9f6x85igs1w0avb45dll")))

