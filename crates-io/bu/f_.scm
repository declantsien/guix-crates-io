(define-module (crates-io bu f_) #:use-module (crates-io))

(define-public crate-buf_file-0.1 (crate (name "buf_file") (vers "0.1.0") (deps (list (crate-dep (name "raw_serde") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1ps7zg1b85vfs59ml6mv84v4hzl55pskn34lld471jhkasx4xwd8") (yanked #t)))

(define-public crate-buf_file-0.1 (crate (name "buf_file") (vers "0.1.1") (deps (list (crate-dep (name "raw_serde") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1f64860xyanrpygx28198nkilc737lwb9p5pajr8a0f5xyv23vjx") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.1") (hash "0answ6z26jbhrsbjz89ly7iw427qxy2m6qd7px6y3z9926w789b3") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.2") (hash "1iv9s2bfmjg4jz8mld52fbb0z1mrr998s3jmh6hpp06mhfaprjhv") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.3") (hash "1mf0qzndmn0l5hws1r821nfm2vqw0xix55vvivl68nd10ihdp1ff") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.4") (hash "18f2l5jbvb3nhkzwdrlwv6nz285ahi4c2ckj61f0lhy7s72hjnzl") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.5") (hash "0fvmqr63m8g515kgk64xxpyylnjvlxhkxzzn6akd2b9mfqb89d6r") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.6") (hash "19891qcm0gb23isyjhnvazci92bnw7hz73imym38s599y5gjcbvy") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.7") (hash "0gqq3lcm7asq50cfr5fgnknnqqlqkwhsyh9pv44ppf9x6a2f06y6") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.8") (hash "0fssqv52kxlyrmmsp00rmmr23vv8qngg4h308h2v0873m9p2n4yb") (yanked #t)))

(define-public crate-buf_file-0.2 (crate (name "buf_file") (vers "0.2.9") (hash "0c2hxrfdr2884szxkynxjnwzmybjfj3z74rmjhpsl8lw5fxvakb1") (yanked #t)))

(define-public crate-buf_redux-0.1 (crate (name "buf_redux") (vers "0.1.0") (hash "0m57iisw5wj8m52680cmlc1p3152h6rzv4i82i1y5nh54qn40r59")))

(define-public crate-buf_redux-0.1 (crate (name "buf_redux") (vers "0.1.1") (hash "0mqvijmfzvhh6n741nsgy70v7h40ndfbw6cgylalfj83c554rrs3") (yanked #t)))

(define-public crate-buf_redux-0.1 (crate (name "buf_redux") (vers "0.1.2") (hash "10vrg2bwaazqlh3ncy5irqdjfzbn7yyvxfvmnbp09r8jym2gskgy")))

(define-public crate-buf_redux-0.1 (crate (name "buf_redux") (vers "0.1.3") (hash "16chpd90zm92pk1cig3qkvbsxi22d72f2rzq1zw5i2y66ncvs5di")))

(define-public crate-buf_redux-0.2 (crate (name "buf_redux") (vers "0.2.0") (hash "1sn29r8b4c6f5dba4mmskxnk63im5calqplw2c0csfaalgp2d4n7")))

(define-public crate-buf_redux-0.3 (crate (name "buf_redux") (vers "0.3.0") (hash "0zgbaqwzl9533shciy2zdnly0gd1n060vla2f9j41jzmp4crs6w6") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.5 (crate (name "buf_redux") (vers "0.5.0") (deps (list (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jng7l78w2v2jv46zy2m00zvjcjjrb93sg6dswp28aksaz434hmx") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.5 (crate (name "buf_redux") (vers "0.5.1") (deps (list (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jsa0p0cdwj3ss0gxkl64ljkykmx4hi4h7xny628xskdndb27dz3") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.5 (crate (name "buf_redux") (vers "0.5.2") (deps (list (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "07dzj906dknzw73ki1x73jjdga74ybf2i13dc6jj7azgysiap6kb") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.5 (crate (name "buf_redux") (vers "0.5.3") (deps (list (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wajkh5n7vih5djdxfzj5qqn0hjv0sar0m1kmvqg1g7lr0hwaqis") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.5 (crate (name "buf_redux") (vers "0.5.4") (deps (list (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j8igbpxb9y96yzam4fryb0kwx7690rx3l0w9vh5rfqn3wdc546k") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.6 (crate (name "buf_redux") (vers "0.6.0") (deps (list (crate-dep (name "memchr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jm8x8bd6i051flcf2dyzr2z2kphax7lwwrdfspzqx0qlqazfpij") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.6 (crate (name "buf_redux") (vers "0.6.1") (deps (list (crate-dep (name "memchr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "0paqa586np6mxyg98cmv32q574i1kvv270bf7d417fiiq4s7cjg1") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.6 (crate (name "buf_redux") (vers "0.6.2") (deps (list (crate-dep (name "memchr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.1") (default-features #t) (kind 0)))) (hash "0c41wq8cbfzrj8xxxbd7mn0rc9hq36afgrj62w634rav0a198raz") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.6 (crate (name "buf_redux") (vers "0.6.3") (deps (list (crate-dep (name "memchr") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jgrn1qqlkrxd299kd8j9n2ps3g5xj1pi2dmbyq1dy4z6539c9xr") (features (quote (("nightly"))))))

(define-public crate-buf_redux-0.7 (crate (name "buf_redux") (vers "0.7.0") (deps (list (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.1.8") (optional #t) (default-features #t) (target "cfg(any(unix, windows))") (kind 0)))) (hash "0nnmhp30nq3ypljgr5ph6036ay5dnadiyqkcl7qnsk6qh7d9xs6r") (features (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.7 (crate (name "buf_redux") (vers "0.7.1") (deps (list (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.1.8") (optional #t) (default-features #t) (target "cfg(any(unix, windows))") (kind 0)))) (hash "1ah19a947mc449bicp80nfjx781rc703i05pjisrdkn94rx6iii0") (features (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8 (crate (name "buf_redux") (vers "0.8.1") (deps (list (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.1") (optional #t) (default-features #t) (target "cfg(any(unix, windows))") (kind 0)))) (hash "1ja92zpcvsqgqqxy77a3j4ilmb1inwpn4wbp8pw3ylmzmdkmrwkj") (features (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8 (crate (name "buf_redux") (vers "0.8.2") (deps (list (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.2") (optional #t) (default-features #t) (target "cfg(any(unix, windows))") (kind 0)))) (hash "0llpb7kb7qv3pqcjkm7xx9krjsn83njxd0r0xgrwamx8a0v0nsyb") (features (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8 (crate (name "buf_redux") (vers "0.8.3") (deps (list (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.2") (optional #t) (default-features #t) (target "cfg(any(unix, windows))") (kind 0)))) (hash "12n3vvj58dx4p4vskylcvq912cwcc2w35v83k5vm54w6s6jjhy1m") (features (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_redux-0.8 (crate (name "buf_redux") (vers "0.8.4") (deps (list (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "safemem") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.2") (optional #t) (default-features #t) (target "cfg(any(unix, windows))") (kind 0)))) (hash "0zy0p4vd2kjk7z5m4m8kdwhs5cmx1z02n7v36njhgfs8fs4aclxr") (features (quote (("nightly" "slice-deque/unstable") ("default" "slice-deque"))))))

(define-public crate-buf_stream-0.2 (crate (name "buf_stream") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.1.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "05hgvh7wah38rk1l8n5vvb4c7lxsinxh1yiyqcahpgz3cpwcd49x") (features (quote (("tokio" "futures" "tokio-io") ("default"))))))

(define-public crate-buf_stream_reader-0.1 (crate (name "buf_stream_reader") (vers "0.1.0") (hash "19lp54a0sz3qa7jdcmc5wmfwhljjh7n9ds4l82z80hrcvfx5x6fq")))

(define-public crate-buf_stream_reader-0.1 (crate (name "buf_stream_reader") (vers "0.1.1") (hash "0h0z26882b9arvrwsg1km4k8g0z8knyzyjac5wz70c30r4q4dl04")))

(define-public crate-buf_stream_reader-0.2 (crate (name "buf_stream_reader") (vers "0.2.0") (hash "14l7ynkwkx5ikb1icp3qgl7527804kpvmckp2vapsm23wsvb83lh")))

