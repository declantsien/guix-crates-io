(define-module (crates-io bu t-) #:use-module (crates-io))

(define-public crate-but-what-about-0.1 (crate (name "but-what-about") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (optional #t) (default-features #t) (kind 0)))) (hash "1r2ybfwn7x9qam1w7ipzfgj958dmbay8qc6l3slw68h8y75k1s25") (features (quote (("grapheme" "unicode-segmentation"))))))

