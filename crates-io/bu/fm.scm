(define-module (crates-io bu fm) #:use-module (crates-io))

(define-public crate-bufmut-netext-0.1 (crate (name "bufmut-netext") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0xr990a1jmck1fgpl24017khig5h3bp399s8kfvhx58fwz7pp7j1")))

