(define-module (crates-io bu gu) #:use-module (crates-io))

(define-public crate-buguc-0.1 (crate (name "buguc") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lib-rv32-asm") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0cxmb7lr2v7ixi3kmvx2friidsnsr2xns47bxfaa3baa080vh6pd")))

(define-public crate-buguc-0.2 (crate (name "buguc") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c01q0ybzglxg5c0abrpbwp0wvd5w09j4dh2abq93x206fjgx22g")))

(define-public crate-buguc-0.2 (crate (name "buguc") (vers "0.2.1") (hash "03ssi1cc680srk82pm8frwkj3767vhq2vd55q7w30a5ma3qsa5mj")))

(define-public crate-buguc-0.3 (crate (name "buguc") (vers "0.3.0") (hash "1xq9gph7hqc7sb5nn8407qgv794pvn4cgb9q237s0ls71w59x4cx")))

