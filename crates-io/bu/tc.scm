(define-module (crates-io bu tc) #:use-module (crates-io))

(define-public crate-butcher-0.1 (crate (name "butcher") (vers "0.1.0") (hash "134p5vkp2iyjvasbfnqs9xd1q0i7df0yqaf2kh44dp7vs9sfanyh")))

(define-public crate-butcher-0.1 (crate (name "butcher") (vers "0.1.1") (hash "0fnsy6gdsn8hlpq2djhzz19x852l7sd78pp0nqi9cx2r2n807ad3")))

(define-public crate-butcher-0.2 (crate (name "butcher") (vers "0.2.0") (deps (list (crate-dep (name "butcher_proc_macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "12d3fvyc2y0b72j8qks0x2v4g38i11hgxrzzrb1249l6k613py89")))

(define-public crate-butcher-0.2 (crate (name "butcher") (vers "0.2.1") (deps (list (crate-dep (name "butcher_proc_macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wr3yf08wpcyh82a0ql06myyk70b2jnwg4imw9h05kk78za1cl9n")))

(define-public crate-butcher-0.3 (crate (name "butcher") (vers "0.3.0") (deps (list (crate-dep (name "butcher_proc_macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fyiaasfghvhn7gah65ahj0mrg6r2q83jln4dpf7p6s715m3pr10")))

(define-public crate-butcher-0.4 (crate (name "butcher") (vers "0.4.0") (deps (list (crate-dep (name "butcher_proc_macro") (req "^0.4") (default-features #t) (kind 0)))) (hash "18l5v5p7v1yrj2b4nr86xr4zzz7lbh5wqb9x0f1h8kycfpfvf43b")))

(define-public crate-butcher-0.5 (crate (name "butcher") (vers "0.5.0") (deps (list (crate-dep (name "butcher_proc_macro") (req "^0.5") (default-features #t) (kind 0)))) (hash "12nwj3mhy4pls5k8l832rxhqsqirwjg6h7ddv4xd16njlq7bqn8i")))

(define-public crate-butcher_proc_macro-0.2 (crate (name "butcher_proc_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ikx2sjj408qzh7n3jwmsxirni16zgvz92h52fgl5kpyfslyg5zf")))

(define-public crate-butcher_proc_macro-0.3 (crate (name "butcher_proc_macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19962kswygs2mdidvii3pzfikb8plsbimmm9rk77dr198cx96xfc")))

(define-public crate-butcher_proc_macro-0.4 (crate (name "butcher_proc_macro") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "023yfvs6ld8944291v6nryjswd2djrx86pvh6wjndzgq2fdwp0nj")))

(define-public crate-butcher_proc_macro-0.4 (crate (name "butcher_proc_macro") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13zi9z1dhx6p0wzyr3rl9lb14sahri8wg5gjzqqbjj5rq7l0bflg")))

(define-public crate-butcher_proc_macro-0.4 (crate (name "butcher_proc_macro") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "15ji25gy5abvgiwhrfvadwz6c4zhmc5af5sdfd079a0rdngdibyb")))

(define-public crate-butcher_proc_macro-0.5 (crate (name "butcher_proc_macro") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ag0mj5jc6kdrxbzbkh0iqc6ln3ij7fhrw5py4a1c9bjdgz5h7ya")))

(define-public crate-butcher_proc_macro-0.5 (crate (name "butcher_proc_macro") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z53vavjjad0dhwyd4gzwddry5q6g1629d0fyam1lfavmanicq3d")))

