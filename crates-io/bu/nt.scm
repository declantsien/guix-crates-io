(define-module (crates-io bu nt) #:use-module (crates-io))

(define-public crate-bunt-0.1 (crate (name "bunt") (vers "0.1.0") (deps (list (crate-dep (name "bunt-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "0r1xpyp20jhiv3bfdfzf851w20hg65a3fdlrk525iyl4hbz0z8ff")))

(define-public crate-bunt-0.1 (crate (name "bunt") (vers "0.1.1") (deps (list (crate-dep (name "bunt-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1j406vxjsgry9cd4fawxcnc02qdxpvga1xah1bb78d4hxlw1dm3p")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.0") (deps (list (crate-dep (name "bunt-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1hid3ii2yc4nxlcrfcqi96pd4mwq1gzlj22hd8sxafss68bmrsgs")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.1") (deps (list (crate-dep (name "bunt-macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1qqzwyy1fcj1xi7m6hg9lb9n7bwgw0mjsrgs0m8hshf7vi65azk1")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.2") (deps (list (crate-dep (name "bunt-macros") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1ih014jh5fz4ymv126i0i627dpy80vcybi5dgagq779kgvz0cvzg")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.3") (deps (list (crate-dep (name "bunt-macros") (req "=0.2.3") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)))) (hash "1d4zbd4s3a809bpvc4n2bs4xn34dymdswm8h8y55ngyzl7sqi7i1")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.4") (deps (list (crate-dep (name "bunt-macros") (req "=0.2.4") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "03innvsdc2a33pvvv2wnbq0zfr3r92l2lzi80xwk4l17vngldbi5")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.5") (deps (list (crate-dep (name "bunt-macros") (req "=0.2.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "00fy5y87qsgbx1wflvrhn77giw3x29rn82v864jhbmqrb01d53lb")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.6") (deps (list (crate-dep (name "bunt-macros") (req "=0.2.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0lamzh452i27cafllq9g8fz6nvhmpj647r43nvz76hz02dnaqaqr")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.7") (deps (list (crate-dep (name "bunt-macros") (req "=0.2.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "08mrsdc523l5lcbf9lgc4xrhh4v3zccl17m63ng09ad8wlnlls1n")))

(define-public crate-bunt-0.2 (crate (name "bunt") (vers "0.2.8") (deps (list (crate-dep (name "bunt-macros") (req "=0.2.8") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1b3ndbycsa7w21qg19143xv7v4x54ad8f7x70pwhylqxyz7c9dzf")))

(define-public crate-bunt-logger-0.1 (crate (name "bunt-logger") (vers "0.1.0") (deps (list (crate-dep (name "bunt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "1d66rs9r58vdv1yzjd4hw1yahvy1dipkknwv6nlsw2ifcammx9xf")))

(define-public crate-bunt-logger-0.1 (crate (name "bunt-logger") (vers "0.1.1") (deps (list (crate-dep (name "bunt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "1zmmlfjdzw8m035wz7qlc2ypxyd9n8n1nqjlblxbsk0ndn1crc86")))

(define-public crate-bunt-macros-0.1 (crate (name "bunt-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "04s9n40kcdcdkn96p347fr8p12j0ay3jvk0c2iqj52nkbw8k7sg4")))

(define-public crate-bunt-macros-0.2 (crate (name "bunt-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13hcaa6ab62an6cajs5s4f4lv4z0072qg1cmmfc6pkmdwwdw8vh9")))

(define-public crate-bunt-macros-0.2 (crate (name "bunt-macros") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1kq9fxwxy8bh2y191xsm00828n1i1givk4lpsaw6dazj1law1dvc")))

(define-public crate-bunt-macros-0.2 (crate (name "bunt-macros") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0bji27nfifv2ydwjjvqjms14ap330qafp70bzi6pns5ghhsx7c59")))

(define-public crate-bunt-macros-0.2 (crate (name "bunt-macros") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "16v9dx2kz9jqkljyj8b5636v0shn7l4f4yj0d26kz4avpbjvbrrj")))

(define-public crate-bunt-macros-0.2 (crate (name "bunt-macros") (vers "0.2.5") (deps (list (crate-dep (name "litrs") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1056014hjn7ysscdqyjfa1xcvwiqv4x35h8dlx0ghilbpcdy66hq")))

(define-public crate-bunt-macros-0.2 (crate (name "bunt-macros") (vers "0.2.8") (deps (list (crate-dep (name "litrs") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1f2ay0i4b61k10gyzl4334ih2ph1myjp1v3llp2fmd5377cqki4a")))

(define-public crate-buntdb-0.1 (crate (name "buntdb") (vers "0.1.0") (hash "0ymw0vq92vk4i50ckpd9axzs5r4rdx8az1sqp15a3h1ah6dx1132")))

