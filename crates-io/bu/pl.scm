(define-module (crates-io bu pl) #:use-module (crates-io))

(define-public crate-buplib-1 (crate (name "buplib") (vers "1.0.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "09j4lmcszvrsbkggq25i51406zb73bf5134z2in3idz441czd1dl")))

(define-public crate-buplib-2 (crate (name "buplib") (vers "2.0.0") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1ziqqgaiarjfcx9n2q9skln6qpyq8bbmmqp7qxam5ass47jiwsfr") (yanked #t)))

(define-public crate-buplib-2 (crate (name "buplib") (vers "2.0.1") (deps (list (crate-dep (name "rodio") (req "^0.17.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "08g4mz2baay2s2vazff8nxwv95mfvf10i00ackqzv3i1vd9nj3ra")))

(define-public crate-buplib-3 (crate (name "buplib") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 2)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "0q6rpvwn03j2fhivqdkbqil918lfgkfjrgvxnjkvjl635sq1k5l7")))

(define-public crate-buplib-3 (crate (name "buplib") (vers "3.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 2)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "1m9z04vqwngsrd33j58fb0fc28li9nb1x6kw67h7wlf1msiswis7") (features (quote (("mut"))))))

(define-public crate-buplib-3 (crate (name "buplib") (vers "3.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 2)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "0pxnc3ypamkypw4j8kcqq30cyrpbva52j7rdk6iap6m7kp7mbdcl") (features (quote (("mut") ("future" "mut"))))))

(define-public crate-buplib-3 (crate (name "buplib") (vers "3.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 2)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "0gb27mkh313h3ckgcq64zl9vfy8xp25gbfgc83b1cyddgbyqc09k") (features (quote (("mut") ("future" "mut"))))))

(define-public crate-buplib-3 (crate (name "buplib") (vers "3.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 2)) (crate-dep (name "rodio") (req "^0.17.3") (kind 0)))) (hash "1vljhrxx88ya5j74zld7ig233iwz7ilgbg0j96xddlm745cqmg4v") (features (quote (("mut") ("future" "mut"))))))

