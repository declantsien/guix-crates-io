(define-module (crates-io bu te) #:use-module (crates-io))

(define-public crate-buter-0.1 (crate (name "buter") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0sc9h37yvxy4xi08bgw0vzl4ma25xm36lcsbzgp0bgfrz5gnxii8")))

(define-public crate-buter-1 (crate (name "buter") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "leak_slice") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lock_api") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1qmr9ll62sjjlkvs5lnkhdy4wvzwrqs86c26n6cibw9vq7g8qsl0")))

(define-public crate-buter-1 (crate (name "buter") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "leak_slice") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lock_api") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1f894m7h3n9yfssadcc8z49jak93dipaaz6q91vfj61dfvbwcw02")))

(define-public crate-buter-1 (crate (name "buter") (vers "1.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0jr551ihh6q3yxg0hmq7v6g2iwla438wm6jm8bcq69w0abprc410")))

(define-public crate-buter-1 (crate (name "buter") (vers "1.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "08yk6aqkphmar0lh8qm7zk9fazv3fbmvv3cvia4y0pwwxgnvl1cc")))

(define-public crate-buter-1 (crate (name "buter") (vers "1.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0b9mbmxwwdqjfyw2fh9zwv17fjdlrzpbv2jbkd57aw4dal2xapbs")))

(define-public crate-buter-1 (crate (name "buter") (vers "1.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0va7jy9pk7inxc2c7n2pahfbkx1kck7c9x9z8prfbcglfcrz58ar")))

(define-public crate-buter-1 (crate (name "buter") (vers "1.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "0nrlqjz69jl8i41rpspm253pif8xsx80q01xxwwjn38pifkd38yk")))

