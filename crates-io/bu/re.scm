(define-module (crates-io bu re) #:use-module (crates-io))

(define-public crate-bureau-0.1 (crate (name "bureau") (vers "0.1.0") (deps (list (crate-dep (name "cargo") (req "^0.78.1") (default-features #t) (kind 0)) (crate-dep (name "license") (req "^3.2.0") (features (quote ("offline"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "0vfvnwv1hdpn7f31g5ryqxc8s98ncsb8rn93lkwf6mw91pf5apfq")))

(define-public crate-bureau-0.2 (crate (name "bureau") (vers "0.2.0") (deps (list (crate-dep (name "cargo") (req "^0.78.1") (default-features #t) (kind 0)) (crate-dep (name "license") (req "^3.2.0") (features (quote ("offline"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "04mdkf1cxjhfls2y1b8npa98kqgpknh1l0cnn9k9zgff2ni17y7y")))

