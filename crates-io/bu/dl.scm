(define-module (crates-io bu dl) #:use-module (crates-io))

(define-public crate-budlang-0.0.0 (crate (name "budlang") (vers "0.0.0-reserve.0") (hash "0xi4s37dj5idbw40nainj251dc87r3hl3cwyljcjbdkyfr0r8ghq")))

(define-public crate-budlang-cli-0.0.0 (crate (name "budlang-cli") (vers "0.0.0-reserve.0") (hash "059zrk5fvzcmyvj1npbsmcsbkmgmhzlq8587vxmm4sn5id8cv9bf")))

