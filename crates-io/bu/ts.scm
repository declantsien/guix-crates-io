(define-module (crates-io bu ts) #:use-module (crates-io))

(define-public crate-butsuri-0.1 (crate (name "butsuri") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "bevy_prototype_lyon") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1fj58v2a05i60jx74x8bbry973z3rfa73abk6ln464j1x15i6wa3")))

