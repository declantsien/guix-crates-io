(define-module (crates-io bu dd) #:use-module (crates-io))

(define-public crate-buddhasay-0.1 (crate (name "buddhasay") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "06pcpfbc9d1r0wp1fsc0ssrmrbisssjj1n2plnmk4yywx8h1scf7")))

(define-public crate-buddhasay-0.1 (crate (name "buddhasay") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0z4hr04zr4w9085bfd42zyag0px67m5vmbbsh45svybypdg5c8fq")))

(define-public crate-buddhasay-0.2 (crate (name "buddhasay") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0xl9d6xypx8jsm64rqaaic6isay8am1v4arfclkww5g7b5wv4aja")))

(define-public crate-buddhasay-0.3 (crate (name "buddhasay") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1h2xafjw43rsqrd19mx34658nj9vyv62ynagq2sv9i8y2fh3g247")))

(define-public crate-buddhasay-0.4 (crate (name "buddhasay") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "1s94ky75fg16iik3cv6v7mmsz5gx3kyq31waqvali8fadkndbjd9")))

(define-public crate-buddhasay-0.6 (crate (name "buddhasay") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "13xh83izalynjfq161vdngp4va60zawqd2a3cbkpxwama7r26pwd")))

(define-public crate-buddhasay-0.6 (crate (name "buddhasay") (vers "0.6.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pvy379gpjxbawqhdwd9v5i5g3nhrscn19wh219cl4glgg9p22az")))

(define-public crate-buddhasay-0.6 (crate (name "buddhasay") (vers "0.6.2") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fk29kaadd0ihc4g8lrri8rhcm5ba7q6gq0ldrd6jljbgnf3n1ca")))

(define-public crate-buddhasay-0.6 (crate (name "buddhasay") (vers "0.6.3") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "0igd5di3h66xxqqahj2fql1xw7l0qqmggacx63l5gn9zfpkipyf5")))

(define-public crate-buddies-0.0.1 (crate (name "buddies") (vers "0.0.1") (deps (list (crate-dep (name "bitvec") (req "^0.11") (kind 0)))) (hash "019vsrpzihdk4w2zn4ps7bbligp1ln2908phzvy4gwhm09rf60bl") (features (quote (("default"))))))

(define-public crate-buddies-0.0.2 (crate (name "buddies") (vers "0.0.2") (deps (list (crate-dep (name "bitvec") (req "^0.11") (kind 0)))) (hash "0m8k9k7bvyq5b6x6han0axy9pinplyy875ygxbahxcv48rq9kyrq") (features (quote (("default"))))))

(define-public crate-buddies-0.0.3 (crate (name "buddies") (vers "0.0.3") (deps (list (crate-dep (name "bitvec") (req "^0.11") (kind 0)))) (hash "1q4p5322s7pm8xlvi3myx0vnmkrbq8sygm5fry2bmk30l5ym4aqj") (features (quote (("default"))))))

(define-public crate-buddy-0.0.1 (crate (name "buddy") (vers "0.0.1") (hash "1pmqkw1x2rmvjwvrlyk5f9pri6lc3awqx2l8z54i8w85bx3y2w4c") (yanked #t)))

(define-public crate-buddy-0.0.2 (crate (name "buddy") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.4.0") (default-features #t) (kind 0)))) (hash "0as3v37mjxhn3871b2r3ncj1c9mac9453xp6db3a8rjw8zinhns2")))

(define-public crate-buddy-alloc-0.1 (crate (name "buddy-alloc") (vers "0.1.0-pre.1") (hash "1bhz9hzq0cmbkrmhvcj5i88lb7iw9wll3b73g9218081679j1xpi")))

(define-public crate-buddy-alloc-0.1 (crate (name "buddy-alloc") (vers "0.1.0-pre.2") (hash "07dckhx6p0wx9jv4j18adb6iaar168b13s3wac4f8vmkrvy3k36z")))

(define-public crate-buddy-alloc-0.1 (crate (name "buddy-alloc") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bazr2j6s92k5wbvivks14b6kh34bl0297cp2bsmmf5qz2k9pbjw") (yanked #t)))

(define-public crate-buddy-alloc-0.1 (crate (name "buddy-alloc") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1wmzsj1d65nxrgvfnpv52gnb8ar2an6k1bn8zlvjibr3j1ahv7sf") (yanked #t)))

(define-public crate-buddy-alloc-0.1 (crate (name "buddy-alloc") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1jq716dhglbkhg5j3fv4x6ja3yppfz65w15ffg1n326b6fxhn8hn") (yanked #t)))

(define-public crate-buddy-alloc-0.2 (crate (name "buddy-alloc") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ik3palp8gxlhas23lp8694ly6qp08fhhbbkkgxyx1wq04i15pgd") (yanked #t)))

(define-public crate-buddy-alloc-0.3 (crate (name "buddy-alloc") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "03lyj4nkvidasgnhd37l7q2i2i21vk1ksklh13sdv7yzkym9xfpg") (yanked #t)))

(define-public crate-buddy-alloc-0.4 (crate (name "buddy-alloc") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "14az9i03g7z5mdz11x9y81sq84nrwp7hyfsfnrw29gqwcc6vp1as") (yanked #t)))

(define-public crate-buddy-alloc-0.4 (crate (name "buddy-alloc") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1m9z7wbiblh3fi2wg5510g59ddsmj85b9pmmwa2xn1k4k0wg7ycg")))

(define-public crate-buddy-alloc-0.4 (crate (name "buddy-alloc") (vers "0.4.2") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "029dri5jaaa08d3dq6isbq3acvp9j3741ga12sjsc3fg175s8h1j") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("default"))))))

(define-public crate-buddy-alloc-0.5 (crate (name "buddy-alloc") (vers "0.5.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1v5a0vq8drbq14hrhakz6yxra1l80pnrha8nbqfy706zhdindan6") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("default"))))))

(define-public crate-buddy-alloc-0.5 (crate (name "buddy-alloc") (vers "0.5.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "15dkp8ja06cfhzqvkccn6g0q63ym4ll8h907grd5v2ba9ak2s38z") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std") ("default"))))))

(define-public crate-buddy-allocator-0.1 (crate (name "buddy-allocator") (vers "0.1.0") (deps (list (crate-dep (name "alloc-wg") (req "^0.8") (default-features #t) (kind 0)))) (hash "0r1saj6szs7lxyy0q8c8mrk4x5zj1y8wc4jxa969xzapg23x8nys")))

(define-public crate-buddy-allocator-0.1 (crate (name "buddy-allocator") (vers "0.1.1") (deps (list (crate-dep (name "alloc-wg") (req "^0.8") (kind 0)))) (hash "1lni3fsqn67ywxls6nalz1z1cclyak10zwsaw08vqn3vfhbdbrf4") (features (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.2 (crate (name "buddy-allocator") (vers "0.2.0") (deps (list (crate-dep (name "alloc-wg") (req "^0.8") (kind 0)))) (hash "0vi4amcfx6anag8cndp3hcpwcy6zmxgqh7833dsknlny4xg89cxk") (features (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.2 (crate (name "buddy-allocator") (vers "0.2.1") (deps (list (crate-dep (name "alloc-wg") (req "^0.8") (kind 0)))) (hash "0mnmg658m7qvx021bl5bvhym13as6qnxslm42p0k49q3j95q8xgs") (features (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.2 (crate (name "buddy-allocator") (vers "0.2.2") (deps (list (crate-dep (name "alloc-wg") (req "^0.8") (kind 0)))) (hash "1ms4xfwchzkrc7bs0mmv5a8c5145l723x9b49fpii71rzjmlr5ps") (features (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.3 (crate (name "buddy-allocator") (vers "0.3.0") (deps (list (crate-dep (name "alloc-wg") (req "^0.9") (kind 0)))) (hash "10iifwbi1giy0a0iz9hdnqvbw3avsa9l0g968kli27m6sk8d5k3c") (features (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-allocator-0.3 (crate (name "buddy-allocator") (vers "0.3.1") (deps (list (crate-dep (name "alloc-wg") (req "^0.9") (kind 0)))) (hash "0z4kzcq016kx6d8n497vrsa098xs3bmm824dqgg8zl6gr51f1jy7") (features (quote (("std" "alloc-wg/std") ("default" "std"))))))

(define-public crate-buddy-link-0.1 (crate (name "buddy-link") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "solana-client") (req "=1.14.17") (default-features #t) (kind 2)) (crate-dep (name "solana-program") (req "=1.14.17") (default-features #t) (kind 0)) (crate-dep (name "solana-sdk") (req "=1.14.17") (default-features #t) (kind 2)))) (hash "065h1qirc3yahpq8wq1ibsxjcyw1857zd385aicby4bv4crr8a9f") (features (quote (("mainnet") ("devnet"))))))

(define-public crate-buddy-link-0.2 (crate (name "buddy-link") (vers "0.2.0") (deps (list (crate-dep (name "ahash") (req "=0.8.6") (default-features #t) (kind 0)) (crate-dep (name "anchor-lang") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "solana-client") (req "^1.16.5") (default-features #t) (kind 2)) (crate-dep (name "solana-program") (req "^1.16.5") (default-features #t) (kind 0)) (crate-dep (name "solana-sdk") (req "^1.16.5") (default-features #t) (kind 2)) (crate-dep (name "spl-associated-token-account") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1vikvvazgy0pjmz16rc4divmck4v2ykifhy9r3pxlk29c4dgw56b") (features (quote (("mainnet") ("devnet"))))))

(define-public crate-buddy-link-0.3 (crate (name "buddy-link") (vers "0.3.0") (deps (list (crate-dep (name "ahash") (req "=0.8.5") (default-features #t) (kind 0)) (crate-dep (name "anchor-lang") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "solana-client") (req "^1.16.5") (default-features #t) (kind 2)) (crate-dep (name "solana-program") (req "^1.17.33") (default-features #t) (kind 0)) (crate-dep (name "solana-sdk") (req "^1.16.5") (default-features #t) (kind 2)) (crate-dep (name "spl-associated-token-account") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1n1x4zs3sbzx8260izxxa02rb6yming2g471p4rzdq8ra7v7j85y") (features (quote (("mainnet") ("devnet"))))))

(define-public crate-buddy-system-0.1 (crate (name "buddy-system") (vers "0.1.0") (deps (list (crate-dep (name "generational-arena") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1jfr6lhq0b9r7kqqdmxcpcwm2y81pklz62j2z56g57is9yk4f7qw")))

(define-public crate-buddy-system-0.2 (crate (name "buddy-system") (vers "0.2.0") (deps (list (crate-dep (name "generational-arena") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1c34hkcaml9gpz2h947bdin036kn07p6hsl75zidl9yr4ylkyp3f")))

(define-public crate-buddy_system_allocator-0.1 (crate (name "buddy_system_allocator") (vers "0.1.0") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0mglzi53rpjmp2wa7icc2hwfg6q424j1h6j93fis2qjlj6a6w8xf") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.1 (crate (name "buddy_system_allocator") (vers "0.1.1") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0bahq4x3wsbxz779dppzsqiiqpc6b1nzkrr3wlsy6h4rmdqmp2gp") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.1 (crate (name "buddy_system_allocator") (vers "0.1.2") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "15f5dhmx4lg1ihjkzcjhklwcafnngyv7aqwdk4my7mi7wbqjin1f") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.2 (crate (name "buddy_system_allocator") (vers "0.2.0") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0cgvaxf18hz1xl0ab0gig5m6g287cbqxbrq3f2w1dd43qfp6q421") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.2 (crate (name "buddy_system_allocator") (vers "0.2.1") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1xl3p6zbdbsscha8p1vfjjglahg4sb65l40cpmpi28pypwab2z4q") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.0") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "04434a6sn3kinimjk3hhrxi8lhm06smh5hhpwv176b0cfndlg5ls") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.1") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0p2756xzq074l6gpv37g7m6l8irpwamb157c2q96h06ismdd9cb4") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.2") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "02p8rldgxnc8h99w89xicmkszdfmwibk2vr273wqy6hhkaa2nw3r") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.3") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1s82dz4pf38cby1mbd8q0r6irrpkkdr9p18a9y63yxdiwgap2wbc") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.4") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "14dfbbiy9rc447gszgm7z1xc3iyw82awnhsb5zds1zh778ac19ll") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.5") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0vs208ah5qk1747hxv35ck99xmpjcxdxg0826xwfx2b5appibnjr") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.6") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0aphl9zh2dz603fmkc14765pkh0jiy1z2zs4dvv04prhz155bafj") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.7") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0n707cvfz2i2c2f64g4v8v241rbwkffbw146fknd0san3qnj3j9z") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.8") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1jk337zk2qqa4zjxrcm59c0snyzikxiy8cp14b42h0l1kal2l1q9") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.3 (crate (name "buddy_system_allocator") (vers "0.3.9") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "13q19nhk1q06nrk9l8vha6ym9y55lambdmgan523mqc3inakp5cy") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.4 (crate (name "buddy_system_allocator") (vers "0.4.0") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0sx31311sj1x6h5vmsivjl184rrkj9zqbga6wbgpl7sgc0i4s5j2") (features (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-buddy_system_allocator-0.5 (crate (name "buddy_system_allocator") (vers "0.5.0") (deps (list (crate-dep (name "spin") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1bv1h6m8iv5kq0spgiqgbivks5ya8mfp34bcw2splh78zzhri3w7") (features (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.6 (crate (name "buddy_system_allocator") (vers "0.6.0") (deps (list (crate-dep (name "spin") (req ">=0.7.0, <0.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "0c5qzj5nnaahhdffjgs6wxx18v1wg5w6a8yi1fp4cnqh1rv5xs5l") (features (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.7 (crate (name "buddy_system_allocator") (vers "0.7.0") (deps (list (crate-dep (name "spin") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0r15da3fl1g1czqk223f6cy4rf2433zydzlzplmx0nhbgyfkkasj") (features (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.8 (crate (name "buddy_system_allocator") (vers "0.8.0") (deps (list (crate-dep (name "spin") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0j4apcx159gq65l2ns39g68nk44yvpi6mzwf2pk6q91cy32klw2m") (features (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.9 (crate (name "buddy_system_allocator") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ctor") (req "^0.1.23") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)))) (hash "0acnyb8xiq5mx05hf1rlbb46p97d00649aacr9iid7hcdddkdya3") (features (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddy_system_allocator-0.9 (crate (name "buddy_system_allocator") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "ctor") (req "^0.2.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.8") (optional #t) (default-features #t) (kind 0)))) (hash "1dc93fjy3zh142irl4zpb370l5hz6s4z83kxbv3i4wyimn65fkfl") (features (quote (("use_spin" "spin") ("default" "use_spin") ("const_fn"))))))

(define-public crate-buddyalloc-0.1 (crate (name "buddyalloc") (vers "0.1.4") (hash "1rrb5rca5z0qakx5nl60aih9bnlmjnz9rv5zs30sg8j03p1h1f1m")))

(define-public crate-buddyalloc-0.1 (crate (name "buddyalloc") (vers "0.1.5") (hash "0cav6bgvvxki0n9l9sw0wivg04jvmq76awnbfn0awbs2w9sdkpm5")))

