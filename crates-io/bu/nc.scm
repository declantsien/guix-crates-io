(define-module (crates-io bu nc) #:use-module (crates-io))

(define-public crate-bunch-0.1 (crate (name "bunch") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "1wj57dv2dy9hbp7pg68y780vgylqjdcc511yxckchh48rdy5frjm")))

