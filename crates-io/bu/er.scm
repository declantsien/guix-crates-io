(define-module (crates-io bu er) #:use-module (crates-io))

(define-public crate-buer-0.0.0 (crate (name "buer") (vers "0.0.0") (hash "0vgripfh8s2nsgggwkbq22wq4hpbnz5hgg6clc95msc6495f0hqv")))

(define-public crate-buerostatus-0.1 (crate (name "buerostatus") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9.6") (default-features #t) (kind 0)))) (hash "1s0g20h5sz5161dkzzwj06v2l4pj94fj0izfydj1iazcvkk3m57i")))

(define-public crate-buerostatus-0.1 (crate (name "buerostatus") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qrywaaf4bnzz7j7g4v7fpqrdn934zvrdwsvkcizjcn2aygsa792")))

