(define-module (crates-io bu sl) #:use-module (crates-io))

(define-public crate-buslogger-0.1 (crate (name "buslogger") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "06mzr9sfljqfrwrm6qqwc0vaiic2w27z6kpnmyms5dc7xi2kpslk")))

