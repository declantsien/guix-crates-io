(define-module (crates-io bu gl) #:use-module (crates-io))

(define-public crate-bugle-0.0.1 (crate (name "bugle") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.8") (features (quote ("compat" "thread-pool"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14hjqx7yrhphzkgd3gyikqlyfqcmh8m6yvghnkslfkcz378gw98g")))

