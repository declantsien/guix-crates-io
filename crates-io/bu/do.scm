(define-module (crates-io bu do) #:use-module (crates-io))

(define-public crate-budoux-0.0.1 (crate (name "budoux") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vf2qhh8p6qrk6idmc2rrssfrlligfhhdhvnzsjibkdlm8laa9w0")))

(define-public crate-budoux-0.0.2 (crate (name "budoux") (vers "0.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0dm8l32h447rsgrxvifpc5ramrblwm03ihc1bw8i7x6mgkfg8nhl")))

(define-public crate-budoux-0.0.3 (crate (name "budoux") (vers "0.0.3") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "178xdpvs3hjj7jjlbn4vvf3yhkai7pc8z6r1kj1ir2zcqkvylk75")))

(define-public crate-budoux-0.0.4 (crate (name "budoux") (vers "0.0.4") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1bf2cnkhwx80ip9rsynq7g9cdpl70imiban8j2ajzzidc0bl0h9d")))

(define-public crate-budoux-0.1 (crate (name "budoux") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1n5m7hw8ls2lnq36x5m0zivj91rinsnx4acjp6bxlqn6rh9kpdmd")))

(define-public crate-budoux-0.1 (crate (name "budoux") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1h3zwvzdsazyr9vkvasdvb9rnll7z3gg6xwvcm2s4pfmmfnda23a")))

