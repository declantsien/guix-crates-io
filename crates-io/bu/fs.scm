(define-module (crates-io bu fs) #:use-module (crates-io))

(define-public crate-bufs-0.0.0 (crate (name "bufs") (vers "0.0.0") (hash "1dkfcrxcf5587r6gx3lcnd7vr0nj24mjqacbd0lpi7kg6swsphlb")))

(define-public crate-bufs-0.1 (crate (name "bufs") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1k68y24syyk4rdmy7gxnfa24xbqpwnpcppwsanrba46grcck3v9g")))

(define-public crate-bufsize-0.4 (crate (name "bufsize") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)))) (hash "1vvq2jffy6q2z97w6wfmiw5qd5prfj8a786da9vrz2l0zima2cpw") (features (quote (("i128" "bytes/i128"))))))

(define-public crate-bufsize-0.5 (crate (name "bufsize") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "1y33qjrl0c2gab8pbdyhi5jrs8yip5s1gyhbsfykdqcamsn248zj")))

(define-public crate-bufsize-0.5 (crate (name "bufsize") (vers "0.5.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "1hbzdcmyaicsb5x49y794a37qgpbh5wmqhhl4fmjlzxr2pcy8pxv")))

(define-public crate-bufsize-0.6 (crate (name "bufsize") (vers "0.6.0") (deps (list (crate-dep (name "bytes") (req "^0.6") (default-features #t) (kind 0)))) (hash "015bfjpqvj7zihalmfwgzh85vcr4fmxvxk0ap39g6s1y8y1jalnm")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mlcxjl6kcfy3qzppxsrfajq9sdgrmmsgdfawsi88c9js4vrp627")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "00fjcymh2ghrh3c50pams4fpasnr8k98lsxz05jj2h5b0k31ypby") (rust-version "1.39")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.2") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kqq18356kyiyid38czb9gfadhbjnvnprj7p4laj7mxcm07qj7y8") (rust-version "1.39")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.3") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lcxpndydscppgld0xnsncpdyc5v6jim3mvf7c9b5n1y0m54n7n8") (rust-version "1.39")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.4") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zz19x2igdbxzg301dvw8y3167gdxzn48567cnba9lw11bapdk0c") (rust-version "1.39")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.5") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "0adm1vi4r1sx4h93yx3l5vgmqx5gb70qfi1ryf3v43w0r0dwdfsi") (rust-version "1.39")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.6") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s25ac260s37yqq8dks711qrd9nvvm0fglygrdlisbfix0dlyans") (rust-version "1.39")))

(define-public crate-bufsize-1 (crate (name "bufsize") (vers "1.0.7") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pff7dpr0m6lcscr1ggirl0bhrn7kkl3sg4p9nddk70052xayr3q") (rust-version "1.39")))

(define-public crate-bufstream-0.1 (crate (name "bufstream") (vers "0.1.0") (hash "0msz4rn55rz5xi3lyn9ad0gg4zad5c6rc7bj4xd289h84qgz6y1a")))

(define-public crate-bufstream-0.1 (crate (name "bufstream") (vers "0.1.1") (hash "1ci3bgnrdyg56b3k4bl7wqsdsgpa8m9dqdlf12bidmdfm3k3sjby")))

(define-public crate-bufstream-0.1 (crate (name "bufstream") (vers "0.1.2") (hash "0x6h27md1fwabbhbycfldj0wklrpjr520z9p0cpzm60fzzidnj3v")))

(define-public crate-bufstream-0.1 (crate (name "bufstream") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.1.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0clf8q4ivw3rr24g11x703xh5fmsjw20vh2cfindxfbn3rqq5wzj") (features (quote (("tokio" "futures" "tokio-io") ("default"))))))

(define-public crate-bufstream-0.1 (crate (name "bufstream") (vers "0.1.4") (deps (list (crate-dep (name "futures") (req "^0.1.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1j7f52rv73hd1crzrrfb9dr50ccmi3hb1ybd6s5dyg6jmllqkqs0") (features (quote (("tokio" "futures" "tokio-io") ("default"))))))

(define-public crate-bufstream-fresh-0.3 (crate (name "bufstream-fresh") (vers "0.3.0") (hash "1nsxfyb5q971dmhbmn30h1ibs9mszvbblfixa5i2d9djrw8n09gi")))

(define-public crate-bufstream-fresh-0.3 (crate (name "bufstream-fresh") (vers "0.3.1") (hash "0pq3l8jxdwg8rb15p4q7ak31rbj65581ydvc17svdkhf8mfiwhvw")))

