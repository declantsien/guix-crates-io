(define-module (crates-io lr pc) #:use-module (crates-io))

(define-public crate-lrpc-0.1 (crate (name "lrpc") (vers "0.1.0") (deps (list (crate-dep (name "lrpc-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04hvc22xb944s626fr72zvvsxddfjbrg8y8mqvsj885yw166qlvs")))

(define-public crate-lrpc-0.1 (crate (name "lrpc") (vers "0.1.1") (deps (list (crate-dep (name "lrpc-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pmk02hb9wnhxsidm95ciddjcn23znxnhvzng26wrfh86501vd0y")))

(define-public crate-lrpc-1 (crate (name "lrpc") (vers "1.0.0") (deps (list (crate-dep (name "lrpc-macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1hk4n9vl0sskrm4yybb93yjwcpm58fry79s7ndjmhvw3m1vmas67")))

(define-public crate-lrpc-1 (crate (name "lrpc") (vers "1.1.0") (deps (list (crate-dep (name "lrpc-macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1vv68zxicdygj16fxw9lg0an8ibjspi5jqmrafx8q68ck9q3hmbr")))

(define-public crate-lrpc-macros-0.1 (crate (name "lrpc-macros") (vers "0.1.0") (hash "00id51s7cb6whih8wxhj772akpj3khzxjbfyv0sbvgyyjlc1v3jw")))

(define-public crate-lrpc-macros-1 (crate (name "lrpc-macros") (vers "1.0.0") (hash "1vk4za0my0gby94yaw09n5sx7bniz9f5qlh7d9xxi4qcv0clkpxi")))

