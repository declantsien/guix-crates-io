(define-module (crates-io lr u_) #:use-module (crates-io))

(define-public crate-lru_map-0.1 (crate (name "lru_map") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1xby7imqskm9gsy194r2pd97xbdvcw51pzxc9w0c67j4nkal6r28")))

(define-public crate-lru_time_cache-0.0.1 (crate (name "lru_time_cache") (vers "0.0.1") (hash "06hksbyzrw5514a68y0az898x2gmkiym8w9vph3ky747asirc3za")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1inrjbzaci17fhmm3dqw8ag67sswm2mpccifkwd9kcyykfgsg02h")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1fx353ap3kvhph5apii13piw05fscyixr6ynsmymwvahjn990qqs")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0v0iwz58dsilql7pkp8kjm75awn7hd8lzhs7dhvk8q9w65asm31s")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1piyxk096p6lcmvbpkh3v6m8i95qwm8rapmncspqhalfwpfpl2mc")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1c2c0liqvcahj52zijp1glwgk2v4k7k0w2mzdzw9yy3p7liw3846")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1fgghki68ijfainxzsbyxk44b403dqlrb4w4hsj483b8pnyir53y")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0qlkpf3jgqlzmxarl6k45xpskkj8d1vs0brf5shcm8plf5x4gg3i")))

(define-public crate-lru_time_cache-0.1 (crate (name "lru_time_cache") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "09j9y390w6qnql6lk06alk791jngw4s4n7288nfv8ji3hacdc04w")))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "16r3rakm25gb89gkmgb5rp79sj7c02jld4l5knrm6f7pyhwqfqcn")))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0a8rm4pjdp2a5bi4s942lfh6rlqm1by107lc8vgqyvlcr7sdxj8q")))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "05v57pfbracxd4gs1svvqqj7ysl04jgyq7dn5kysspcgg4vddp6n")))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "~0.3.12") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)))) (hash "17dvxp84lwhivw4w3zw9sc7rs40dazapxp7c7pn844jx7ii2vnqs") (yanked #t)))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "~0.3.12") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)))) (hash "1p3h6y5x6g0qgnrsyq8qkbjwcrr90iin1rlj3azl9pw188i7cnnf")))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "~0.3.12") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)))) (hash "0s6iygszx4q6hw9rvgm5zlf8r8w7pn47807sq0d26i0njhb4hxjz")))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "~0.3.12") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)))) (hash "11xbcd2gw62a274wccydq64bas08myjhcly4pm1czf3mms39cv24")))

(define-public crate-lru_time_cache-0.2 (crate (name "lru_time_cache") (vers "0.2.7") (deps (list (crate-dep (name "clippy") (req "~0.0.45") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)))) (hash "14jiv62lrsnvgnbg264dg0bp5bpir9p1zgkvavygg6bvnv1xaycr")))

(define-public crate-lru_time_cache-0.3 (crate (name "lru_time_cache") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "~0.0.45") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)))) (hash "038biqyzzlwig7iz4m243q5rd9klgqkpym4wcb69jk04rxlmd9j8")))

(define-public crate-lru_time_cache-0.3 (crate (name "lru_time_cache") (vers "0.3.1") (deps (list (crate-dep (name "clippy") (req "~0.0.63") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)))) (hash "0269q6g1f9h2jj31qm6n0c8fkh51y2l3kn98xq9j8xpwnscmm6yj")))

(define-public crate-lru_time_cache-0.4 (crate (name "lru_time_cache") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "~0.0.68") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)))) (hash "1jxj6iya9givnzzd7ngrj1iyc72l4lwpqrg0hf4m5bj10jm52cbb")))

(define-public crate-lru_time_cache-0.5 (crate (name "lru_time_cache") (vers "0.5.0") (deps (list (crate-dep (name "clippy") (req "~0.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)))) (hash "01qbrbcnw7wivswgf6ac51biw729hgq36gzzsyr2w2y8v34qwiyx")))

(define-public crate-lru_time_cache-0.6 (crate (name "lru_time_cache") (vers "0.6.0") (deps (list (crate-dep (name "fake_clock") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.15") (default-features #t) (kind 2)))) (hash "0350yhm976i7sa1ry1k1mv969rgkaa2lkk6cq1y7ra7irmi7iwab")))

(define-public crate-lru_time_cache-0.7 (crate (name "lru_time_cache") (vers "0.7.0") (deps (list (crate-dep (name "fake_clock") (req "~0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.15") (default-features #t) (kind 2)))) (hash "1mm1kif25siqn8ci8asvcpch7w0cspd31dwxqsknr4s0mm48mwrh")))

(define-public crate-lru_time_cache-0.8 (crate (name "lru_time_cache") (vers "0.8.0") (deps (list (crate-dep (name "fake_clock") (req "~0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.4.1") (default-features #t) (kind 2)))) (hash "0ns52y62clslmj96g7jm0rv4k1qjm7w30id87wgrvnckz7fzclfi")))

(define-public crate-lru_time_cache-0.8 (crate (name "lru_time_cache") (vers "0.8.1") (deps (list (crate-dep (name "fake_clock") (req "~0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.4.1") (default-features #t) (kind 2)))) (hash "1i2kd715mpi5rj8gpnisnifvb7hs64rqhyfqfr09j1d9zks5zxc7")))

(define-public crate-lru_time_cache-0.9 (crate (name "lru_time_cache") (vers "0.9.0") (deps (list (crate-dep (name "fake_clock") (req "~0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)))) (hash "0q49rkrms6gg7f875rkg73bnl81mas3g1j2dws5ih42ibf7f0i5b")))

(define-public crate-lru_time_cache-0.10 (crate (name "lru_time_cache") (vers "0.10.0") (deps (list (crate-dep (name "fake_clock") (req "~0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)))) (hash "1axyk6q0r4zpfpkyjdflq26n35m8z2azqqskfn4bibjcbkgl3cmd")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.0") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0c5v0cf06vfyrqhw3mzr1x34zzzrzk8fgrf3r9g8mxc206pzsh5m")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.1") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jb7wqi2bixdsa5pm50mjyz5bhlrslgf7vd9cr36v5xrzn93522k")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.2") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "16p2y83b42kplcmxywx2jj7brx1gm50hga361kfxwfmdmw7hdb7b")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.3") (deps (list (crate-dep (name "rand") (req ">=0.6.5, <0.7.0") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req ">=0.4.0, <0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ld4jkkqs6c22s77iajmbyaszpxwf25m8s63iycdkprqd6rbxhic")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.4") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "02y88qawrq6i0g8sb9y3qpa7209g32ks5ypj9kzvslm0mb7ivax0")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.5") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1c8hbqnncimqd08wq3bgz8ckrpgnd5p3l6rangrkmib802amgybq")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.6") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1chc7y023a0wrk7zw6wd572rs1mc6c7dz6fawr2k3gasc4scwnd5")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.7") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "021r5s26qxg6q6c28f0sz4ckzg095yb1fl6d899853bz009skrvw")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.8") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1j7xnywmqbq73i739qvwwm3ssmg5k8fmwssdx06cif0dwhr3avns")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.9") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "19dsdmgan3lz8bjh1b5kwlmnhlc0xkl56yvfhzl4hafvsxz5qh78")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.10") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "18yby5k397yyffwnjqxp1lfhabwxvaxds456lhvin5kz1v7mh44r")))

(define-public crate-lru_time_cache-0.11 (crate (name "lru_time_cache") (vers "0.11.11") (deps (list (crate-dep (name "rand") (req "~0.6.5") (default-features #t) (kind 2)) (crate-dep (name "sn_fake_clock") (req "~0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pdssvc0q4df5v0dbr5y9wa5plkff3x9fbdvwmmqxm7z8zby21li")))

