(define-module (crates-io lr u-) #:use-module (crates-io))

(define-public crate-lru-cache-0.0.1 (crate (name "lru-cache") (vers "0.0.1") (deps (list (crate-dep (name "linked-hash-map") (req "*") (default-features #t) (kind 0)))) (hash "1snsvs02wq5f5z3da4xhwc85wnqk8mdv7cfwr2crdmbyis8ny4d5")))

(define-public crate-lru-cache-0.0.2 (crate (name "lru-cache") (vers "0.0.2") (deps (list (crate-dep (name "linked-hash-map") (req "*") (default-features #t) (kind 0)))) (hash "1jars4rd35wj71njp7hbkzqsshif722rf2sza0lkd7s7jgxw3q08")))

(define-public crate-lru-cache-0.0.3 (crate (name "lru-cache") (vers "0.0.3") (deps (list (crate-dep (name "linked-hash-map") (req "*") (default-features #t) (kind 0)))) (hash "1z27sdy2n6n0f49clm7w9lwpkjd0s5pacyxq3zca8y0b3m71s6c1")))

(define-public crate-lru-cache-0.0.4 (crate (name "lru-cache") (vers "0.0.4") (deps (list (crate-dep (name "linked-hash-map") (req "*") (default-features #t) (kind 0)))) (hash "1jyx52nk2y1wwyqli5dvzyxcrx089k81kk7wd4j0fh221ajhr7c8")))

(define-public crate-lru-cache-0.0.5 (crate (name "lru-cache") (vers "0.0.5") (deps (list (crate-dep (name "linked-hash-map") (req "*") (default-features #t) (kind 0)))) (hash "1wdxwv0m44dpbi8smvfwiw8by39r5r4bm581ia57ysmgcv6jvzkd")))

(define-public crate-lru-cache-0.0.6 (crate (name "lru-cache") (vers "0.0.6") (deps (list (crate-dep (name "linked-hash-map") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "1fh82crswby00fwlvprzfiv8apwsz5ryci9ka8mygw7wqm1b41s3") (yanked #t)))

(define-public crate-lru-cache-0.0.7 (crate (name "lru-cache") (vers "0.0.7") (deps (list (crate-dep (name "linked-hash-map") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "135z7hzvqnk5lj614klwfyf7rhy039z20hqh7gw5s54zbp5hvma2")))

(define-public crate-lru-cache-0.1 (crate (name "lru-cache") (vers "0.1.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dzxdmyagcg8i6lci67y58gyw0skdbzs6asr7h3g3g02rggs8vv5")))

(define-public crate-lru-cache-0.1 (crate (name "lru-cache") (vers "0.1.1") (deps (list (crate-dep (name "heapsize") (req "^0.3.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "08fwa341bpibpfgy1qaarmcvq44dp1n8f9z2ykjrqwkgy1zzy1jd") (features (quote (("heapsize_impl" "heapsize" "linked-hash-map/heapsize_impl"))))))

(define-public crate-lru-cache-0.1 (crate (name "lru-cache") (vers "0.1.2") (deps (list (crate-dep (name "heapsize") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)))) (hash "071viv6g2p3akwqmfb3c8vsycs5n7kr17b70l7la071jv0d4zqii") (features (quote (("heapsize_impl" "heapsize" "linked-hash-map/heapsize_impl"))))))

(define-public crate-lru-cache-macros-0.1 (crate (name "lru-cache-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12nlbzw97agvkb7yvh4smdy0w034rnqw8dvm4p4s0fkkf11l1938")))

(define-public crate-lru-cache-macros-0.2 (crate (name "lru-cache-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "085qdfnlj64x7kr3db5asz0y1zzn1sl0lggk1jrb94wz19zwldzc")))

(define-public crate-lru-cache-macros-0.2 (crate (name "lru-cache-macros") (vers "0.2.1") (deps (list (crate-dep (name "lru-cache") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11g5cl2hqs4dmjkq8vzpqnhxrnqhava824vwi711zhfm29lc1nkc")))

(define-public crate-lru-cache-macros-0.2 (crate (name "lru-cache-macros") (vers "0.2.2") (deps (list (crate-dep (name "lru-cache") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1w2y33fnj1hn82wf13kqqcin7wmjhnssgv0lfx2b36a19x0ba7aq")))

(define-public crate-lru-cache-macros-0.2 (crate (name "lru-cache-macros") (vers "0.2.3") (deps (list (crate-dep (name "lru-cache") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1r7lx440vljbs4v291gfkp2azzcg8d18q3ch7xnmv12c2j73zmkc")))

(define-public crate-lru-cache-macros-0.3 (crate (name "lru-cache-macros") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "lru-cache") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1z1x2vnpd34mnm9vlicf24f5g8gl3v86ziv9jv457rd2f2msv79l")))

(define-public crate-lru-cache-macros-0.3 (crate (name "lru-cache-macros") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "lru-cache") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1hdhkkqisb2683psbn2aaihijyl341mg27jpnj2rrphqyzga83mx")))

(define-public crate-lru-disk-cache-0.1 (crate (name "lru-disk-cache") (vers "0.1.0") (deps (list (crate-dep (name "filetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "0dazvgavjqdpp1r7w17in545vdil4sbpy0gbgjymxw53mcb7ibgd") (yanked #t)))

(define-public crate-lru-disk-cache-0.2 (crate (name "lru-disk-cache") (vers "0.2.0") (deps (list (crate-dep (name "filetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "1l234scphrks6s862m4fj59mx4ym0vfz2j05ry37bkwi1bq4jfn0") (yanked #t)))

(define-public crate-lru-disk-cache-0.3 (crate (name "lru-disk-cache") (vers "0.3.0") (deps (list (crate-dep (name "filetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "01n3himb10wvvx6m0kkx59p1y0gxifj4ry4rm1md43j040gz4148") (yanked #t)))

(define-public crate-lru-disk-cache-0.4 (crate (name "lru-disk-cache") (vers "0.4.0") (deps (list (crate-dep (name "filetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "0w02g205nd95vlfi09pnnq58azrff2iq8c55s7h234n4dplxf4gy") (yanked #t)))

(define-public crate-lru-disk-cache-0.4 (crate (name "lru-disk-cache") (vers "0.4.1") (deps (list (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "10sqnq9lh608293a1ygfypqdhmk8hv0a33bkxifsp6c7yqq95znr") (yanked #t)))

(define-public crate-lru-mem-0.1 (crate (name "lru-mem") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6") (default-features #t) (kind 2)))) (hash "0bygw6258bacxhr09sd1ldbvx2hivmkx5dmb7hk97niwzad86vi5")))

(define-public crate-lru-mem-0.1 (crate (name "lru-mem") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6") (default-features #t) (kind 2)))) (hash "0xijn9czg4zyxx0s7i4fpk1bq5fy5ykmp66wp7qh8fxysdqfixi6")))

(define-public crate-lru-mem-0.1 (crate (name "lru-mem") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6") (default-features #t) (kind 2)))) (hash "0zqwgbwsqd6nqf8f2s50xqnqr13q8y8jsamlvwqaig66apvpvkp5")))

(define-public crate-lru-mem-0.1 (crate (name "lru-mem") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1hrkxiaapvgyh9m0g2ba447z1p6j936l9j4fvzqdi98f6yi9ws6g")))

(define-public crate-lru-mem-0.1 (crate (name "lru-mem") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0gv9lib13gr8755pp8kwghaivy496xh22dv8nbd4d4kf92lpgm8q")))

(define-public crate-lru-mem-0.1 (crate (name "lru-mem") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1q31z7spk5ihavrzv2b7q86zj8mixqlpg5hn0339n8r8709vaak3")))

(define-public crate-lru-mem-0.2 (crate (name "lru-mem") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1rz8vigq6a7dmfqrvnhham5l1mqf7j2lwr8qg2fprxjy5hlwbzyq")))

(define-public crate-lru-mem-0.2 (crate (name "lru-mem") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "102j61qirlklbaarnjrc3flhq6wyvwn6xszw98s08p9b0wszbb26")))

(define-public crate-lru-mem-0.3 (crate (name "lru-mem") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1027fzirlbvyv2mddsrr272inkbslm01j5ycsj01r903v4k8qp6g")))

(define-public crate-lru-st-0.1 (crate (name "lru-st") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "02riz4f0lh23ifallwzk3zjfdz9gc8fd5j8d649q22kwnvqvdzn7") (features (quote (("sync"))))))

(define-public crate-lru-st-0.1 (crate (name "lru-st") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0zr712bkjbv2l62zr84nxnn39gcdfypy6nqcd37p9gd3fpblgsgk") (features (quote (("sync"))))))

(define-public crate-lru-st-0.1 (crate (name "lru-st") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0bzw4p0r387xhmnamc03sz81qj2298d017p7yakps23dwggh1xn2") (features (quote (("sync")))) (rust-version "1.60.0")))

