(define-module (crates-io lr cl) #:use-module (crates-io))

(define-public crate-lrclassifier-0.1 (crate (name "lrclassifier") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xr6i9514qxxd1addzzv7afz5fw7wklhzsh5gmnryk8mwlbim34i")))

