(define-module (crates-io lr df) #:use-module (crates-io))

(define-public crate-lrdf-0.2 (crate (name "lrdf") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "fastx") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "permutation") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1vin8sbhrlqi02zigdpy9mnbwzkzx3sf264f36k8vwm8m1yvr35z")))

