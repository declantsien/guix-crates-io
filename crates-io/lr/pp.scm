(define-module (crates-io lr pp) #:use-module (crates-io))

(define-public crate-lrpp-0.1 (crate (name "lrpp") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "0k0xrriqqbp6jsgi6jm9qxq5m8vg89fhgw86vwsf6prnjd782igh") (yanked #t)))

(define-public crate-lrpp-0.1 (crate (name "lrpp") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "1p5nkmwfbwp3b5rpiy4q3c69d8h7vgr7gqhbdc9jjgqmzrqpi9yf") (yanked #t)))

(define-public crate-lrpp-0.1 (crate (name "lrpp") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "09cs8q29qsv2ak823d4xzsnh0cchp7wir9l2i4gyy7q8600wsccp") (yanked #t)))

