(define-module (crates-io lr _p) #:use-module (crates-io))

(define-public crate-lr_parser-0.1 (crate (name "lr_parser") (vers "0.1.0") (hash "1v4dcgnl4pykwc4hk0qqbfhshx2vlrv3hbkg7gv9cbpcwxh8jfyg")))

(define-public crate-lr_parser-0.1 (crate (name "lr_parser") (vers "0.1.1") (hash "0ik68k6wng8y0ms35ni5mvyina9ky1y4pjqk70cqrhx29i29rikk")))

(define-public crate-lr_parser-0.1 (crate (name "lr_parser") (vers "0.1.2") (hash "0xc3fp0np5nhbh2lsim9r3aksvd3nj1jfasjrh4q9mrpidbwds5a")))

