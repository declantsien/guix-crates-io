(define-module (crates-io lr u2) #:use-module (crates-io))

(define-public crate-lru2-0.1 (crate (name "lru2") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "0.3.*") (default-features #t) (kind 0)))) (hash "12qcnlbmrda7ab921ixfyd58q01j7l8v0s8nw3zllraalv0fjs9x")))

