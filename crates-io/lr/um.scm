(define-module (crates-io lr um) #:use-module (crates-io))

(define-public crate-lrumap-0.0.0 (crate (name "lrumap") (vers "0.0.0-reserve.0") (hash "11ji2ip1cki2sl2aaidqfiycb1gmpb8k0q4l4hd5lq1vykmv0a66")))

(define-public crate-lrumap-0.1 (crate (name "lrumap") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13.2") (optional #t) (default-features #t) (kind 0)))) (hash "17cypwfp8ynz6wyrbxawd3k6910dqm5fgjvx287602criy7cwl8j")))

