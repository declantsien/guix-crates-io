(define-module (crates-io lr pe) #:use-module (crates-io))

(define-public crate-lrpeg-0.1 (crate (name "lrpeg") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w6ykrcqdsf6dk4mricdrdslmsv10ig8cpsqrh6jvhkpywvibv4l") (yanked #t)))

(define-public crate-lrpeg-0.2 (crate (name "lrpeg") (vers "0.2.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2") (default-features #t) (kind 0)))) (hash "188gx5pqf2fff0cynrq7l9p8594rz113ksc31gczcs75k1hfrafn") (yanked #t)))

(define-public crate-lrpeg-0.3 (crate (name "lrpeg") (vers "0.3.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xxmv0ylmbn2c5amygadbhcwfir7l280xyr9lkwgh021lrw8mzih") (yanked #t)))

(define-public crate-lrpeg-0.4 (crate (name "lrpeg") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z64jwf09hgbnhvmgd0q7d4shr41cwg6w4i8pzfl8i2glk15lav1") (yanked #t)))

(define-public crate-lrpeg-0.4 (crate (name "lrpeg") (vers "0.4.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jznjsjwlp1j4pg25p7k0jwfijh8d3iwhk3h1aiskcjag3vym4av") (yanked #t)))

