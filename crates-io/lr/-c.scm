(define-module (crates-io lr -c) #:use-module (crates-io))

(define-public crate-lr-cw-ownable-2 (crate (name "lr-cw-ownable") (vers "2.0.0") (deps (list (crate-dep (name "cosmwasm-schema") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "cw-address-like") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "cw-ownable-derive") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "cw-storage-plus") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "cw-utils") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0wpapsyipda9a4aya4q9msqb820xky4a5n1gf2fvz837wk0cv0p9") (yanked #t)))

