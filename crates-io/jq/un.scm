(define-module (crates-io jq un) #:use-module (crates-io))

(define-public crate-jqunit-0.1 (crate (name "jqunit") (vers "0.1.0-rc") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1c66zx0jljaf1m2gvz0m5jys3nindkcyqym76m4vivsdi2d9x0bk")))

