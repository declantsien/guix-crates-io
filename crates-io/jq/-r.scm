(define-module (crates-io jq -r) #:use-module (crates-io))

(define-public crate-jq-rs-0.0.0 (crate (name "jq-rs") (vers "0.0.0-alpha") (hash "1q38xhhrxdjl42356qbhp2i1s72qdfb7b71nycgi1n9qlmr9ac58")))

(define-public crate-jq-rs-0.4 (crate (name "jq-rs") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "jq-sys") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0yfp8924kp3vb3wvwx39hzgsgdk3dxcafjw59mzniilqzrswmc8s") (features (quote (("default") ("bundled" "jq-sys/bundled"))))))

(define-public crate-jq-rs-0.4 (crate (name "jq-rs") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "0.12.*") (default-features #t) (kind 2)) (crate-dep (name "jq-sys") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1r5ha5vh2lr8z5iprpxljvz1spxxgg65hxbqmckv55pg8apaxky9") (features (quote (("default") ("bundled" "jq-sys/bundled"))))))

