(define-module (crates-io jq oi) #:use-module (crates-io))

(define-public crate-jqoiview-0.1 (crate (name "jqoiview") (vers "0.1.0") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "100n3pkj4yfq84n531wscymbj23psc8x3wxmdry4kdpvikvz9kv3")))

(define-public crate-jqoiview-0.2 (crate (name "jqoiview") (vers "0.2.0") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "0cw4ry13rj3789qwzy70mny8c3lzqqm4vwzafhzc2il7xplpmimn")))

(define-public crate-jqoiview-0.3 (crate (name "jqoiview") (vers "0.3.0") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "1cqh5wm34kswsz0r34wwaa420qi6lp53xdy65zqsqhpf66ys43pn")))

(define-public crate-jqoiview-0.3 (crate (name "jqoiview") (vers "0.3.1") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "0q81fdcidwlsgpdqqg3b02j3571bbknmyxd8ch7nhb0a6kf868f6")))

(define-public crate-jqoiview-0.3 (crate (name "jqoiview") (vers "0.3.2") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "1bran4495alvinxl70ky7d40w3sicxfr8j2ypvq567bv36hi3izm")))

(define-public crate-jqoiview-0.4 (crate (name "jqoiview") (vers "0.4.0") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "17dwaj3vxz0s9ifpfcq29svpalyz6j09bj7qacjka38yvpjdkgdj")))

(define-public crate-jqoiview-0.5 (crate (name "jqoiview") (vers "0.5.0-alpha") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "0lcv1vn9af3jqgvd45c9ya876wk39ih3cnd9vk4l26kf3awq3mb5")))

(define-public crate-jqoiview-0.5 (crate (name "jqoiview") (vers "0.5.0") (deps (list (crate-dep (name "sdl2") (req "^0.35") (default-features #t) (kind 0)))) (hash "1yjjr9npdhxbg3lnpa0bfba9w1hh63wkkinxpqx0nmb3hj068ysk")))

(define-public crate-jqoiview-0.5 (crate (name "jqoiview") (vers "0.5.1") (deps (list (crate-dep (name "sdl2") (req "^0.35") (features (quote ("bundled"))) (kind 0)))) (hash "160sqhc1bxyk70m7kz2mxzybjyws320qqy6rh7bfmwkld41c1qmf") (yanked #t)))

(define-public crate-jqoiview-0.5 (crate (name "jqoiview") (vers "0.5.2") (deps (list (crate-dep (name "sdl2") (req "^0.35") (features (quote ("bundled"))) (kind 0)))) (hash "1wv6azbkgck2qrihbp35ghna54na7yv8sn9wi87mib4pkcqvdh57")))

