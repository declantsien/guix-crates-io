(define-module (crates-io jq -s) #:use-module (crates-io))

(define-public crate-jq-src-0.1 (crate (name "jq-src") (vers "0.1.0") (deps (list (crate-dep (name "autotools") (req "^0.1.3") (default-features #t) (kind 1)))) (hash "1pzic9qggdfcb8yyxrvwrxaqcsv5d7hkw0sk24az3absnrxk2v6r") (links "jq")))

(define-public crate-jq-src-0.2 (crate (name "jq-src") (vers "0.2.0") (deps (list (crate-dep (name "autotools") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0s2vy41003ybzv387d73nnh4rzpj9j3q8xg6gmxnxqjsxg2z67x5")))

(define-public crate-jq-src-0.3 (crate (name "jq-src") (vers "0.3.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wcb4vxr1nnp6kqpzam7pn1qq7lrrk8rgbdh9cgg7rfk3hpdiqm7") (yanked #t)))

(define-public crate-jq-src-0.3 (crate (name "jq-src") (vers "0.3.1") (deps (list (crate-dep (name "autotools") (req "^0.1") (default-features #t) (kind 0)))) (hash "0h344fvha165m4k2gli3yzwvyw1c44mzgrpp47v9k68yn896jsmw")))

(define-public crate-jq-src-0.4 (crate (name "jq-src") (vers "0.4.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xah35bv59jgll15yvm1vac5nkqczssw3xx8l3rgbawhwsp7k1fl")))

(define-public crate-jq-src-0.4 (crate (name "jq-src") (vers "0.4.1") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ddlcqn62g3x3iy5idk0f0pf7g0lscba0x1m46b5p9nxc70s08xf")))

(define-public crate-jq-sys-0.1 (crate (name "jq-sys") (vers "0.1.0") (deps (list (crate-dep (name "jq-src") (req "^0.1") (default-features #t) (kind 0)))) (hash "0d3aqs3v2w9xrd6dxyd07z4nraxkfz7sgyqkrmfrwf9qbl4h4jy8")))

(define-public crate-jq-sys-0.1 (crate (name "jq-sys") (vers "0.1.1") (deps (list (crate-dep (name "jq-src") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0gxn49567nq4vlbw6hw83vmx396hffa40j101cgxgb6iqr1w9709") (features (quote (("default" "bundled") ("bundled" "jq-src"))))))

(define-public crate-jq-sys-0.2 (crate (name "jq-sys") (vers "0.2.0") (deps (list (crate-dep (name "jq-src") (req "^0.2.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (optional #t) (default-features #t) (kind 1)))) (hash "1gv1ab1rx78psw778l05lylra7wr9qkqm81kzlr6xa8pw1vansrk") (features (quote (("default" "pkg-config") ("bundled" "jq-src")))) (links "jq")))

(define-public crate-jq-sys-0.2 (crate (name "jq-sys") (vers "0.2.1") (deps (list (crate-dep (name "jq-src") (req "^0.3") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (optional #t) (default-features #t) (kind 1)))) (hash "1ppzd0hzwwbliv46zvlxpw686wgngqqz0v4qcyqi222nk8j4xi86") (features (quote (("default" "pkg-config") ("bundled" "jq-src")))) (links "jq")))

(define-public crate-jq-sys-0.2 (crate (name "jq-sys") (vers "0.2.2") (deps (list (crate-dep (name "jq-src") (req "^0.4") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (optional #t) (default-features #t) (kind 1)))) (hash "0j2cp9gb7yb53zdmlnygnpd5hc8dyhcwzjr0rg4gybxxaiz5pdl1") (features (quote (("default" "pkg-config") ("bundled" "jq-src")))) (links "jq")))

