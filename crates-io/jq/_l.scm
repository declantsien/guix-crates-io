(define-module (crates-io jq _l) #:use-module (crates-io))

(define-public crate-jq_lang-0.1 (crate (name "jq_lang") (vers "0.1.0") (hash "1p5d6q6madpa8xbva3x00sq7xwmxajlnm76j122kfvn0l37zdlgk")))

(define-public crate-jq_lang-0.1 (crate (name "jq_lang") (vers "0.1.1") (hash "0a2sfia36jb92yd36h0cs01gjjqnh9hjr4604smr2ma6sdmzaj8x")))

(define-public crate-jq_lang-0.2 (crate (name "jq_lang") (vers "0.2.0") (hash "1nyclq5vvzxxxl420hmpbrg4gn0m6x4kadnk6jkhrn38nl4yf39q")))

