(define-module (crates-io jq ue) #:use-module (crates-io))

(define-public crate-jqueuers-0.1 (crate (name "jqueuers") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1phq7al6jz5wvqzaxq17mdsv1rhjsn40a8fvmv0385pksclfnb99")))

(define-public crate-jqueuers-0.1 (crate (name "jqueuers") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cn63pn36vmnxr4950f52x5hkfmpm4rr34lrp6nja37qvbclql7m")))

(define-public crate-jqueuers-0.1 (crate (name "jqueuers") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.23.3") (features (quote ("connection-manager" "tokio-comp" "aio"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4" "serde"))) (default-features #t) (kind 0)))) (hash "107bscdgfbwi0mskzmv2sy247jjs22y7yk655mh92lsq4li6dc0s")))

