(define-module (crates-io md #{2s}#) #:use-module (crates-io))

(define-public crate-md2src-0.0.1 (crate (name "md2src") (vers "0.0.1") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.7") (features (quote ("simd"))) (kind 0)))) (hash "19s8aaqc4svm3vpxrk93zrivk80k5nlkyca9bf5vms087srhjfzz")))

(define-public crate-md2src-1 (crate (name "md2src") (vers "1.0.0") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7") (features (quote ("simd"))) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "11wb94cfw4305jbh8wprwd53caw9j9n6yzgga7yrpx8q9y387czv")))

(define-public crate-md2src-1 (crate (name "md2src") (vers "1.0.1") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7") (features (quote ("simd"))) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1f76fk655dn6ybwrvcxqmssw0rn33bv5249fszd3kj0qkll2pk7a")))

(define-public crate-md2src-1 (crate (name "md2src") (vers "1.0.2") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7") (features (quote ("simd"))) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1na92vr0pi992mzr6n8qqrl89cgivyqzvqz6snnrg7i7c91da26v")))

(define-public crate-md2src-1 (crate (name "md2src") (vers "1.1.0") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7") (features (quote ("simd"))) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1qd7mkkw6g6lrqbmbr7dna0nha72i3f9sakxfm2dqqlvvpxbmw9x")))

