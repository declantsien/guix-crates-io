(define-module (crates-io md #{2t}#) #:use-module (crates-io))

(define-public crate-md2tex-0.0.1 (crate (name "md2tex") (vers "0.0.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "0sw59sm8mb2r6k24lgfwdwhrj74dmji2ipjwfqvbg303ingy8qch")))

(define-public crate-md2tex-0.1 (crate (name "md2tex") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.7.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "008phc2yzprr9ifrv97lmkq0wshkb4b2r1lw7kffyd0lfz4v9y6b")))

(define-public crate-md2tex-0.1 (crate (name "md2tex") (vers "0.1.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.7.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "1w8yhvc4wqls2z0v28mc34h4gaif959ccmhhysva2aaizk2hikzm")))

(define-public crate-md2tex-0.1 (crate (name "md2tex") (vers "0.1.2") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.7.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "1albraggm45zl9cszyrfarf2i03yfx9j92i3wvxpvxvgm7ril7h4")))

(define-public crate-md2tex-0.1 (crate (name "md2tex") (vers "0.1.3") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.7.0") (features (quote ("raqote-backend"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "1qpj6faamzw3l7hr4zmh1bhxjpi1h19kk0yjsk8sbqrqmzraswhx")))

