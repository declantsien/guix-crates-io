(define-module (crates-io md ta) #:use-module (crates-io))

(define-public crate-mdtable-0.1 (crate (name "mdtable") (vers "0.1.0") (hash "1kbnzq36rf9jra81k427r6dlwq119jmzpkv8yilx205mnlq7xg8x")))

(define-public crate-mdtable-0.1 (crate (name "mdtable") (vers "0.1.1") (hash "0a12p4nk93zwxlqy4kgbppvvnfw5q4bhxkyawxqs9iyywl66gjvf")))

(define-public crate-mdtable-cli-1 (crate (name "mdtable-cli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1pi225f91knb3w03gj3dwwvk5zwk03hqh4nzxa3wnlvbfadw24nz")))

(define-public crate-mdtable-cli-1 (crate (name "mdtable-cli") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "10bcwb5mm2i3137rl5xn3ddbh88rqi5gsm06h72lwpi9wgwx8bix")))

(define-public crate-mdtable-cli-1 (crate (name "mdtable-cli") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1xgrbmlbgs3ncxd7zsps9kk2iqaw4vmw64p895rdhd4387yd6jpq")))

(define-public crate-mdtable-cli-1 (crate (name "mdtable-cli") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "020vl90vjcbj3mhpgfxc2szwkjpj1kdihkscmga799pnaa2kgs6l")))

(define-public crate-mdtable-cli-1 (crate (name "mdtable-cli") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "easy-error") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "04xn1ja1nrhdyq5la4szsma1jal8fnzyycm84fcdw56zixnmnvvj")))

