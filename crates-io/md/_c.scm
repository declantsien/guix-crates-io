(define-module (crates-io md _c) #:use-module (crates-io))

(define-public crate-md_converter-0.1 (crate (name "md_converter") (vers "0.1.0") (hash "003yhdhix1qfgyw7047nfllqxs89zkzbsdjbpavlj9gppd5j8f2s")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.0") (hash "1bhskn23i77wq02mzjna4c0gd53xwsgprha5ihpf478v39h9b4bv")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.1") (hash "0awj3cv5bgikaybf4glvbp9sif9xgp6j9qk3wq15g2q8yyzfhl57")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.2") (hash "0m2nj9238phj4xj0jpqpgm8cps6px52ks3yk41a9kx6kz3dwvbr4")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.3") (hash "04nsznqf2a5zsla9719i3vlnhnynhsqn3va6k58csnqplxs67vy9")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.4") (hash "0j19kdzddpl6iqxn46zc632dzaz4pbsqwfm6g4qs8ggs9400zdvq")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.5") (hash "1s8hci2dscv6bmnnxi5j3lgwkqqvijgmmdvxjj6rvzapfvh02klb")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.6") (hash "18k70j3cpprjzz6451kqm4r1xcvk3g2wb2n3kz3pqv1wgm0n0isg")))

(define-public crate-md_converter-0.9 (crate (name "md_converter") (vers "0.9.7") (hash "1kvwbvwar0m845dxhananxrlxm9npmvlhkklj1qvq877rw1n5d9k")))

