(define-module (crates-io md rv) #:use-module (crates-io))

(define-public crate-mdrv-0.0.4 (crate (name "mdrv") (vers "0.0.4") (deps (list (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0") (default-features #t) (kind 0)))) (hash "0sb20g65lw5k6p8y689k5n82c6a64p8h9m5vg2ggjscqaz25303c")))

(define-public crate-mdrv-0.0.5 (crate (name "mdrv") (vers "0.0.5") (deps (list (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0") (default-features #t) (kind 0)))) (hash "0ixxhaddb8i8ghc9wv9wbik8bh7y8787s5jc3nrkxi1yvzr7m9lj")))

