(define-module (crates-io md c-) #:use-module (crates-io))

(define-public crate-mdc-sys-0.1 (crate (name "mdc-sys") (vers "0.1.0") (hash "11h4jd7p2dyhmxl0vb6442pya3fim6dcbi663gld139w4jhcv109")))

(define-public crate-mdc-sys-0.1 (crate (name "mdc-sys") (vers "0.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.57") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.34") (features (quote ("Element" "Window" "Document" "Event"))) (default-features #t) (kind 0)))) (hash "1cwiknisnlvp1s35a93rqj6jcnwx33kwqxbv84jk69023pm2k5dr")))

(define-public crate-mdc-yew-0.1 (crate (name "mdc-yew") (vers "0.1.0") (hash "080sn70sadkprs5j0kwh3xqsaxdijn45nb3lpkn1hpbg2smlhvpr")))

(define-public crate-mdc-yew-0.1 (crate (name "mdc-yew") (vers "0.1.1") (deps (list (crate-dep (name "mdc-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.10") (kind 0)))) (hash "0wp34hjr8917hfj7mickanrcwy43py45q7scjm7xrj4zb7x39wa8")))

