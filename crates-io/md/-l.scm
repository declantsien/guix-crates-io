(define-module (crates-io md -l) #:use-module (crates-io))

(define-public crate-md-localizer-0.1 (crate (name "md-localizer") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "18xy1xh1qsskwha1yfyav44pgd4l9nlc8q123l933cjnk1lkhjdk")))

(define-public crate-md-localizer-0.1 (crate (name "md-localizer") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1glh5dsvsg2r8h5zwx5a9qwz7lzblbd0jxjmyg4rqqq1whsvyvm0")))

