(define-module (crates-io md ur) #:use-module (crates-io))

(define-public crate-mdurl-0.1 (crate (name "mdurl") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0f31yz3c4qwjq7jxw46lqn2g12d60hv9s3w1l9vbipqyz6y886mj")))

(define-public crate-mdurl-0.1 (crate (name "mdurl") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req ">=0.2.0, <2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">=0.2.0, <2") (default-features #t) (kind 0)))) (hash "0rbl513h0sy2bfi4bf329mnnqa2mzh0i38cjm87c55y3p1fxhplj")))

(define-public crate-mdurl-0.2 (crate (name "mdurl") (vers "0.2.0") (deps (list (crate-dep (name "idna") (req ">=0.1.0, <0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req ">=1.0.1, <2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">=0.2.0, <2") (default-features #t) (kind 0)))) (hash "0fm4fdlnavfvlg81cq2wjjx2jkjg658rlhazkrawdycz0nw7p219")))

(define-public crate-mdurl-0.3 (crate (name "mdurl") (vers "0.3.0") (deps (list (crate-dep (name "idna") (req ">=0.1.0, <0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req ">=1.0.1, <2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">=0.2.0, <2") (default-features #t) (kind 0)))) (hash "0pl7mbpkvn2j3n62ya45qdsxxx330nqvbwl5jia79217ir23jnqh")))

(define-public crate-mdurl-0.3 (crate (name "mdurl") (vers "0.3.1") (deps (list (crate-dep (name "idna") (req ">=0.1.0, <0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req ">=1.0.1, <2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">=0.2.0, <2") (default-features #t) (kind 0)))) (hash "1lphpz6yc5151w23m5zcp8c8ll2yx26gi5x8k767r3xcpd2vldjp")))

