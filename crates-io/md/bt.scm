(define-module (crates-io md bt) #:use-module (crates-io))

(define-public crate-mdbtools_rs-0.1 (crate (name "mdbtools_rs") (vers "0.1.0") (hash "041ccjrzb2bw1czfjsaxjviwrbjdvra59kv5wjspnfadr5r63dn4")))

(define-public crate-mdbtools_rs-0.1 (crate (name "mdbtools_rs") (vers "0.1.1") (hash "1p48j2xy8a3v33c8fln4pzjfi0l731g2j9cwbdl38c2gx9jvzcdb")))

(define-public crate-mdbtools_rs-0.1 (crate (name "mdbtools_rs") (vers "0.1.2") (hash "15x45ix0qvrn096x1m0w6b9w9z55g7alcapdbzidc6ary7szc6yb")))

(define-public crate-mdbtools_rs-0.1 (crate (name "mdbtools_rs") (vers "0.1.3") (deps (list (crate-dep (name "version-sync") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "15l1bmf6fsyaqyqa6vjirnvwn52c6kfgcf2sq2l3hw8pbanr371i")))

