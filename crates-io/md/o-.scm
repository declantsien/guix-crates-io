(define-module (crates-io md o-) #:use-module (crates-io))

(define-public crate-mdo-future-0.1 (crate (name "mdo-future") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "mdo") (req "^0.3") (default-features #t) (kind 2)))) (hash "0mb017x094i87xhf8bb9d9ax3lw3c302a0pz2bhknvwzymlw0l9x")))

(define-public crate-mdo-future-0.2 (crate (name "mdo-future") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "0.1.*") (default-features #t) (kind 2)) (crate-dep (name "mdo") (req "^0.3") (default-features #t) (kind 2)))) (hash "0id8rgh12pgmmc6i8c9vyjykcwcsww70fdg5xjsw16isvh4a9qxk")))

