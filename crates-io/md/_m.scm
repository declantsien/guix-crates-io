(define-module (crates-io md _m) #:use-module (crates-io))

(define-public crate-md_match-0.1 (crate (name "md_match") (vers "0.1.0") (deps (list (crate-dep (name "md_match_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1462vcna1crpvyrz28087qvbj9chqy07z5ii8025fnmvdf6k0ryd")))

(define-public crate-md_match-0.1 (crate (name "md_match") (vers "0.1.1") (deps (list (crate-dep (name "md_match_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fwaw828217q6cm29ifcvbc9rzjy0sxlg9cg0hy1anqxcfkzw42q")))

(define-public crate-md_match_derive-0.1 (crate (name "md_match_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1b6p67sywzc5as2ppqz5z46z8y1q7x5b5kqxdi239d5icamvzba2")))

