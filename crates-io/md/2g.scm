(define-module (crates-io md #{2g}#) #:use-module (crates-io))

(define-public crate-md2gemtext-0.1 (crate (name "md2gemtext") (vers "0.1.0") (deps (list (crate-dep (name "gemtext") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8") (kind 0)))) (hash "1w3ygq3pdrp788gvw4a3xrnd42ixp1yb3q3zlw75aisz91y45lis")))

