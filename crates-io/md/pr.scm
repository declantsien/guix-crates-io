(define-module (crates-io md pr) #:use-module (crates-io))

(define-public crate-mdpreview-0.1 (crate (name "mdpreview") (vers "0.1.0") (deps (list (crate-dep (name "open") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.0.11") (kind 0)))) (hash "02b2pp35bkz2rvhcz0mhv3f9s8l3l54hkjbrzick3q10cbwkzqcx")))

