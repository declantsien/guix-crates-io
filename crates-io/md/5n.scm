(define-module (crates-io md #{5n}#) #:use-module (crates-io))

(define-public crate-md5namer-0.1 (crate (name "md5namer") (vers "0.1.0") (deps (list (crate-dep (name "md-5") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0mxx5r6487jbcfsh1xadzr1x3h1c2c6kx2lb78nlxbycynzcksk3")))

