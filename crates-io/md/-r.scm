(define-module (crates-io md -r) #:use-module (crates-io))

(define-public crate-md-rs-0.1 (crate (name "md-rs") (vers "0.1.0") (deps (list (crate-dep (name "sma-rs") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "15yk39kjp2l5kz8fi4jvhy7783yl0bpf8faqxlvnnp68mc72iq9q")))

(define-public crate-md-rs-0.1 (crate (name "md-rs") (vers "0.1.1") (deps (list (crate-dep (name "sma-rs") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "1i565awswi7ymk9y3jwqd0gvq5k25f2rng7z00325hw00ckgp130")))

