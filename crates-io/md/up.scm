(define-module (crates-io md up) #:use-module (crates-io))

(define-public crate-mdup-0.0.0 (crate (name "mdup") (vers "0.0.0") (hash "1mhgrs682hxy5ds012aphnyyxama8xlzyi3s9jw2zd8qzrx3lv09")))

(define-public crate-mdup-0.0.1 (crate (name "mdup") (vers "0.0.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0da7rfqc8yqfmnsyhdzz7rm1284fblllfcfpmn4j751ajfsqmga5")))

