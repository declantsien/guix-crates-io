(define-module (crates-io md #{5s}#) #:use-module (crates-io))

(define-public crate-md5sum-0.1 (crate (name "md5sum") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.165") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1v1pkxvxqvaq1vrhyh4f0wxzp1yqfsywxzjlzwf1wmcgbi0fxmh0") (features (quote (("default"))))))

