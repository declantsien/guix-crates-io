(define-module (crates-io md #{5-}#) #:use-module (crates-io))

(define-public crate-md5-asm-0.1 (crate (name "md5-asm") (vers "0.1.0") (deps (list (crate-dep (name "byte-tools") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crypto-tests") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "digest") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "digest-buffer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "generic-array") (req "^0.7") (default-features #t) (kind 0)))) (hash "11zdhanjq5sfy7n0n35hbms3adqaw1vb80jww9vx88d3kd3n5gz7")))

(define-public crate-md5-asm-0.2 (crate (name "md5-asm") (vers "0.2.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "generic-array") (req "^0.7") (default-features #t) (kind 0)))) (hash "16p62k077xsp7wapqdsishsfpcx90cwyg3wzx82pxw3qip9y27zy")))

(define-public crate-md5-asm-0.2 (crate (name "md5-asm") (vers "0.2.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "generic-array") (req "^0.7") (default-features #t) (kind 0)))) (hash "0my52myn4frqhjs3k3s0i7nsv80hhrlhi7098bp0lh5la7xsni45")))

(define-public crate-md5-asm-0.3 (crate (name "md5-asm") (vers "0.3.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "generic-array") (req "^0.8") (default-features #t) (kind 0)))) (hash "01w1p6hpiw7qc25d3nhmjx6pnc83wzlgkk1j1bwpxdjw21ly9b49")))

(define-public crate-md5-asm-0.4 (crate (name "md5-asm") (vers "0.4.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0yfvmi8mf6f6b8sc62qy0d9bpvd20fvxhmykcra5v9qj6g0in21a")))

(define-public crate-md5-asm-0.4 (crate (name "md5-asm") (vers "0.4.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "03xni4yyk80762mdvka8w7lk4lvspja3680lgy9ws7fivka2402s")))

(define-public crate-md5-asm-0.4 (crate (name "md5-asm") (vers "0.4.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0qajy90x4ycj10px2dl0csphaq7zlyr8fxgvspszlgxsqsin13iq")))

(define-public crate-md5-asm-0.4 (crate (name "md5-asm") (vers "0.4.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0gpk5647js1k084jc7pg2gji0cvl6hjkkbfia6lnpk8y4shyairv")))

(define-public crate-md5-asm-0.5 (crate (name "md5-asm") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ixmkg8j7sqy9zln6pz9xi2dl2d9zpm8pz6p49za47n1bvradfbk")))

(define-public crate-md5-asm-0.5 (crate (name "md5-asm") (vers "0.5.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0p1f9494s065i4sbnycj1dzwaavf1bpbnar81qs60p7yrp23plv1")))

(define-public crate-md5-asm-0.5 (crate (name "md5-asm") (vers "0.5.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1pz217kwlvrw4bj4hil5acyp3l7g37vwf25psdc210bxzkkqx6yi")))

(define-public crate-md5-core-0.1 (crate (name "md5-core") (vers "0.1.0") (hash "1w9mdz97wd3fz7iw6mabckwxbvms0s50kv3pg0wc1sx00ksx6qjn")))

(define-public crate-md5-core-0.2 (crate (name "md5-core") (vers "0.2.0") (hash "1pal54n78wq3i6n1nr85nlin6qjffq7x4ys984bkp731wv9hi3yq")))

(define-public crate-md5-core-0.2 (crate (name "md5-core") (vers "0.2.1") (hash "0bxghjl429xzlb7yqak2vbaz8faxw4fzmm176dkp645yfw30kw2y")))

(define-public crate-md5-crypt-0.1 (crate (name "md5-crypt") (vers "0.1.0") (hash "0wkbph7w68kjn0bqplv0xp0v31bdcgm30zg7f6qgjqhn96n3njkm")))

(define-public crate-md5-img-0.1 (crate (name "md5-img") (vers "0.1.0") (deps (list (crate-dep (name "bmp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0y4zsbmpv69rj1p7ky98bfj6rmcd2rgxfdmax2w2gl7521mhiapx")))

(define-public crate-md5-img-0.1 (crate (name "md5-img") (vers "0.1.1") (deps (list (crate-dep (name "bmp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "06ivdscdsbvw09r4k911wxm2i676jyqh4lpxi1kcr7h5sjmm0lcx")))

(define-public crate-md5-img-0.1 (crate (name "md5-img") (vers "0.1.2") (deps (list (crate-dep (name "bmp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0simvyk0ipjaz0nbccwykjdlviay5s6xds4n7a3d6qdsda5j7zc6")))

(define-public crate-md5-img-0.1 (crate (name "md5-img") (vers "0.1.3") (deps (list (crate-dep (name "bmp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1ddkmf8kfyk53c6c1i3585mph6xbfh2l9w8qnzn0zgyal5b8j6dh")))

(define-public crate-md5-rs-0.1 (crate (name "md5-rs") (vers "0.1.0") (hash "0g8rd2pi1xhd9hc4wyrari51s64ca7j547wgspwgbxl52c83k6hb")))

(define-public crate-md5-rs-0.1 (crate (name "md5-rs") (vers "0.1.1") (hash "1qz1r3wyrwxg3glxs0rcgspg6z85li8343q96bni2xhhkdxywizs")))

(define-public crate-md5-rs-0.1 (crate (name "md5-rs") (vers "0.1.3") (hash "0sc2bknzrzsmj2w637rfsjpsqbx3p2rmn01dq2s28hqrgbhrl8aq")))

(define-public crate-md5-rs-0.1 (crate (name "md5-rs") (vers "0.1.4") (hash "1fzlbzp2ipiyzb0zsibfm65j9679w33yn95dfp08rih3gynld45h")))

(define-public crate-md5-rs-0.1 (crate (name "md5-rs") (vers "0.1.5") (hash "1qgvb6acgwahx8zii784fpxr3w1bfjjbzxkm4j34fva1pipxf8g7")))

