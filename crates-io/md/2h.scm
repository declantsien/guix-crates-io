(define-module (crates-io md #{2h}#) #:use-module (crates-io))

(define-public crate-md2html-0.1 (crate (name "md2html") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "000055bh5rcca2j5ijssl53c0112p9d6wy4lmcysqiwhbvinqbwz")))

(define-public crate-md2html-0.1 (crate (name "md2html") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0y48b95p68b09f14n0w2g3dk2kgxv9m6z28xxy0kwp7pmn6vk3jq")))

(define-public crate-md2html-0.1 (crate (name "md2html") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1ca5bkqf9hrh197rwp4wgvz0d1gjshb29hqi1i6b54cqc47pznvg")))

