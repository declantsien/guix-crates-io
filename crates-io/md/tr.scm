(define-module (crates-io md tr) #:use-module (crates-io))

(define-public crate-mdtrans-0.1 (crate (name "mdtrans") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0gm7wr86wxkylm1bpgqk06mrallp103hvh10g238w8gaf0zc5s12")))

(define-public crate-mdtrans-0.1 (crate (name "mdtrans") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "03hvsmvc9dhp4zlxd7j6cs5vgwyjr9cn28nzpnmbnd80cm9qk0jk")))

(define-public crate-mdtrans-0.1 (crate (name "mdtrans") (vers "0.1.2") (deps (list (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1y4x045h639h8dviijqsb2wakzffrlm7rsbj3517kmdxlya832g0") (yanked #t)))

(define-public crate-mdtrans-0.1 (crate (name "mdtrans") (vers "0.1.3") (deps (list (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "17d3m0kh6c5pnxzklz1v2c3xbazl6a8b1d1hxp84s2pgymcbw7m4")))

(define-public crate-mdtrans-0.1 (crate (name "mdtrans") (vers "0.1.4") (deps (list (crate-dep (name "pest") (req "^2.7.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0xc6r3sjq3ciwvy41l6108p0j4v78l3ajslmcpadygnfi7c6hszp")))

(define-public crate-mdtrans-0.1 (crate (name "mdtrans") (vers "0.1.5") (deps (list (crate-dep (name "pest") (req "^2.7.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0hy719lwg3gp7nmhx52zrdnsbsh9ajg1wlg9rqckl2rn9ypcnkiq")))

(define-public crate-mdtransform-0.1 (crate (name "mdtransform") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0bqnxn1kmyrz84nqixdn68l5vfrbslpf9w2wfaj6bw6cc895www1")))

(define-public crate-mdtransform-0.2 (crate (name "mdtransform") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0wjkhnh3bc34vjmp9lmkhfy4fpxfr62shg04f79qk7n2v9kq1mzb")))

(define-public crate-mdtransform-0.2 (crate (name "mdtransform") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0iszsq91hm9ky0sjmsgkzwgn1pbs0m4gm3pg0dprckyv8nswnl0z")))

(define-public crate-mdtranslation-0.1 (crate (name "mdtranslation") (vers "0.1.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1nhvqfya5dh0lig2nl8nijyqix3rmd77mknlq9mxhlddimjbh8d5")))

(define-public crate-mdtranslation-0.1 (crate (name "mdtranslation") (vers "0.1.1") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "09b3zczkcfxy5fbb2vx09609lrj40saghyasn4h56qhb4kyrxij8")))

(define-public crate-mdtranslation-0.1 (crate (name "mdtranslation") (vers "0.1.2") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1q47j5h1bxwdil2hxk6r15z67pjfj75j2hisdlb8nlccyav0iif9")))

(define-public crate-mdtranslation-cli-0.1 (crate (name "mdtranslation-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "mdtranslation") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1fd6h7hj3qv5fpw3g4m6l9f513663cfy7c2nxlwignyqkmn7wvay")))

