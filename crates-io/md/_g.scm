(define-module (crates-io md _g) #:use-module (crates-io))

(define-public crate-md_gen-0.1 (crate (name "md_gen") (vers "0.1.0") (hash "1l5hv3fslwl4bna444q9faxc03wsf0zg5zar920d2wd06ikplfb6")))

(define-public crate-md_grid-0.1 (crate (name "md_grid") (vers "0.1.0") (deps (list (crate-dep (name "prettytable-rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1fmgj5j4cwa3a3f88cn1r4a94s1vdyqsqdhrinddiks16azn3550") (features (quote (("prettytable" "prettytable-rs") ("default"))))))

(define-public crate-md_grid-0.2 (crate (name "md_grid") (vers "0.2.0") (deps (list (crate-dep (name "prettytable-rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "04ib13m676ixdhchckv28icxw08mrwrp17q2i7m1znzgx5gf6h2n") (features (quote (("prettytable" "prettytable-rs") ("default"))))))

(define-public crate-md_grid-0.2 (crate (name "md_grid") (vers "0.2.1") (deps (list (crate-dep (name "prettytable-rs") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "15j5ns3ccjllkm5254mdis697sapxfmil7gnjinwzx1zhj0hnfnm") (features (quote (("prettytable" "prettytable-rs") ("default"))))))

