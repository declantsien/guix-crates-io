(define-module (crates-io md tg) #:use-module (crates-io))

(define-public crate-mdtg-0.1 (crate (name "mdtg") (vers "0.1.0") (hash "1whiccfb0hhxc9k2xp0hz30zyklf5spm64xkbx02ps86vylagjk1") (yanked #t)))

(define-public crate-mdtg-1 (crate (name "mdtg") (vers "1.0.0") (hash "1iyrnfpg1ciqavicvyj7pk8ixi6zjfjps7p1yywvpzrdn51f0fc7")))

(define-public crate-mdtg-1 (crate (name "mdtg") (vers "1.0.1") (hash "16qsbnb0p4xgxj8zy0zshccm9xirwh4pk4im33mfhnjaa7f4cq2x")))

