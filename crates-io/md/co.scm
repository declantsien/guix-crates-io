(define-module (crates-io md co) #:use-module (crates-io))

(define-public crate-mdconv-0.1 (crate (name "mdconv") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.7") (default-features #t) (kind 0)))) (hash "147kihkysmjbpsb18m7rfic8r38qszd3yv3cnmy487a9lvgabkqm")))

(define-public crate-mdconv-0.2 (crate (name "mdconv") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.7") (default-features #t) (kind 0)))) (hash "0nqp5dnihva19y00lg158glnvy0sw3wy29pxlq3s19a0zbbavyv5")))

(define-public crate-mdconv-0.3 (crate (name "mdconv") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "15lznp4cyx9hmmxjmvi8mj23hd92hbrb20gjy5bkfa1s0yc0262k")))

