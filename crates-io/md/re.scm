(define-module (crates-io md re) #:use-module (crates-io))

(define-public crate-mdread-donot-use-test-only-0.1 (crate (name "mdread-donot-use-test-only") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0j5b145f7yrnylwzf6ndwrq7mk45dzdcg8950prall15zkp9wfr7")))

(define-public crate-mdrend-0.1 (crate (name "mdrend") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0gfsrbw84wd5dqrfk96zwb90fqwml0jd3l5xfbcim2j36pmm0bv1")))

(define-public crate-mdrend-dont-use-lulz-0.1 (crate (name "mdrend-dont-use-lulz") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "09chc652va6v9fbijxhw9wwmmqp9wrbvkbz60vs1dqbaabm519yf")))

(define-public crate-mdrend-dont-use-my-version-0.1 (crate (name "mdrend-dont-use-my-version") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "03jv5gfpmjhxmvjdic0s58kxwc110bks3qd77nq6yja5l4kfdmj2") (yanked #t)))

(define-public crate-mdrend-dont-use-test-0.1 (crate (name "mdrend-dont-use-test") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "129wqb6g51pbpksnqji0mxwmdpfnhv16k4lm47q75f86hc21rwbn")))

(define-public crate-mdrend-dont-use-test-456-0.1 (crate (name "mdrend-dont-use-test-456") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1gm7pr074dz4kdpwcan1iq6r0yjc3c6zn5x05f0wffcfi8zi7b88")))

(define-public crate-mdrend-dont-use-test-jop0693-0.1 (crate (name "mdrend-dont-use-test-jop0693") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "04lszrxbkg9jn5nzdyglgnqr26jsqj1mr8nhgbrm52k8q3ccnicn")))

(define-public crate-mdrend-dont-use-test-senju-0.1 (crate (name "mdrend-dont-use-test-senju") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0hq7bm0waf3ia7r5jskarzp10r76lbd6g8dmnkz29wf7qhqj8g4q")))

(define-public crate-mdrend-dont-use-test123-0.1 (crate (name "mdrend-dont-use-test123") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1k40yw8j9ndzsbj8lilp22zm98bjl0s9ln723291dkfvccs27anv")))

(define-public crate-mdrend-dont-use-test123-maurilio-0.1 (crate (name "mdrend-dont-use-test123-maurilio") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1jsmpr0kddg9g26bw7g8q5s9qrl4r3sqfb9zwrwy3r8l6w297jsm")))

(define-public crate-mdrend-dont-use-test721-0.1 (crate (name "mdrend-dont-use-test721") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0akw39pczc956c3yba4w4lkn7xl94cfd0x95kzf3rss3dcp3hvgq")))

(define-public crate-mdrend-dont-use-tom-legrady-test-0.1 (crate (name "mdrend-dont-use-tom-legrady-test") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1dm8y9i1bxg471cs3pqxa8khq3n9wa7vq8hmw483q0zyv9lrdv15")))

(define-public crate-mdrend-jacky-demo-app-0.1 (crate (name "mdrend-jacky-demo-app") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1h1wwjxgiamz5b3f3hh74654jkcsqgpr1yf8qpwhia0jrhwdy81k")))

(define-public crate-mdrend-learning-project-not-for-public-use-0.1 (crate (name "mdrend-learning-project-not-for-public-use") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "07jr4fa70g3vpbrsrwvmxrj0mwvnwsjfm42wscidb4289821ny42")))

(define-public crate-mdrend-pedroigor91-0.1 (crate (name "mdrend-pedroigor91") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1glvzjxc23ashlas3yjbp3qjij6r558lw7kl90azj77xk4xbbxq1")))

(define-public crate-mdrend-test-dont-use-0.1 (crate (name "mdrend-test-dont-use") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1w8ynh9jc0iv6hysdagnsfigcvn5jx475xw02fk7vi1xb9yn6va3")))

(define-public crate-mdrend-test-markdown-123-0.1 (crate (name "mdrend-test-markdown-123") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0zfidzf0zz8ccay6dvnn743krqc9kik3a264g8bsmlaz6pzxv0lw")))

(define-public crate-mdrend-this-is-only-a-beta-demo-0.1 (crate (name "mdrend-this-is-only-a-beta-demo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1ngpmc3zc2f8vpzbf3hgnm7382pgbiqxk2d3y9z0pfqrnqm0mqk5")))

