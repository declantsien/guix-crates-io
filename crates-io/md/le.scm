(define-module (crates-io md le) #:use-module (crates-io))

(define-public crate-mdless-0.1 (crate (name "mdless") (vers "0.1.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "02yi686jkvdn1x9c8a6qz1rpdcrk9jvpndwl8cxawpiq5irq8xfw") (yanked #t)))

(define-public crate-mdless-0.1 (crate (name "mdless") (vers "0.1.1") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1r5v3dbxpic2k7w0g3ya6nvjsix1w09r6yvax6la5b6zb9cc1ikl") (yanked #t)))

(define-public crate-mdless-0.2 (crate (name "mdless") (vers "0.2.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1jlqb0kap7w812ilwy2i0kwdggb0ysfmvx54p6m6qm28rpnm8yhy") (yanked #t)))

