(define-module (crates-io md sh) #:use-module (crates-io))

(define-public crate-mdsh-0.1 (crate (name "mdsh") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1hq32fgyzgzhx97dfdf1lcrv199pildw93g4kz898z989xxxjm0p")))

(define-public crate-mdsh-0.1 (crate (name "mdsh") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1ighlx6hql4zw0kkz6chhyhm1mykmq460vshz4ydf0i1kpg87hp9")))

(define-public crate-mdsh-0.1 (crate (name "mdsh") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "17hipkdavbn04s9qlz71addppmzyia0hxkz6why2c992n0am280m")))

(define-public crate-mdsh-0.1 (crate (name "mdsh") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1swx76d22f2s2k5vvvpv9h9jzf0jpr3bpk1l32n6dg59izxazg2n")))

(define-public crate-mdsh-0.1 (crate (name "mdsh") (vers "0.1.5") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0dv8i4666xfl2s19s1rm9qfk8smrk2j1bbxnq8bx7zliyxj47kqj")))

(define-public crate-mdsh-0.4 (crate (name "mdsh") (vers "0.4.0") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "00d6ygqa4jxr0nkbsjlq3wsrppwxr7ibylyc4slbbfa0sdmz569k")))

(define-public crate-mdsh-0.5 (crate (name "mdsh") (vers "0.5.0") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1ip5c4ybv62xjhf9l4x1wnhgf5jnn0sijlip8lbv55nijav9lq7v")))

(define-public crate-mdsh-0.6 (crate (name "mdsh") (vers "0.6.0") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0jljhz7xd6h6jbxpb3jnfbpmy347qh6zblb5k50wdk3qwq5xlpcb")))

(define-public crate-mdsh-0.7 (crate (name "mdsh") (vers "0.7.0") (deps (list (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0nrasgh3q25mhzzfp82329prh24i652gsywr3f6rzgvrsy57va17")))

