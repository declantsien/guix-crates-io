(define-module (crates-io md #{2n}#) #:use-module (crates-io))

(define-public crate-md2nb-0.1 (crate (name "md2nb") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "pulldown-cmark") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "wolfram-app-discovery") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "wolfram-expr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wstp") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0j4mfn37agrrgj7dbky02j0d2rqskx5fhl4lf1h3bbaiypi5dh4l")))

