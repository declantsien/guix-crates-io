(define-module (crates-io md ma) #:use-module (crates-io))

(define-public crate-mdman-0.0.0 (crate (name "mdman") (vers "0.0.0") (hash "05siw7gwz7i0aqxwhncfm4ck5sc7h3ndnhqn0gimdagvwbhmz735")))

(define-public crate-mdmatter-0.0.1 (crate (name "mdmatter") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "stainless") (req "^0.1.6") (default-features #t) (kind 2)))) (hash "0zbhx5c9qhrqs7bfp3w2v4mp9rcikn26w23vlc729nsi5869bhqy")))

