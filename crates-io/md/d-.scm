(define-module (crates-io md d-) #:use-module (crates-io))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.0") (hash "1f4kz6v4b07x8aqi6032zwczgb0spllqzyz0jbxfd4xa6aak5gm6")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.1") (hash "0r7jrcf2s238cvfyv2ra7n2gbx837rv7grd3lh0vckg7kvvspxhm")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.2") (hash "14s6plbx9mip30qwz1cwcvnrkdfg0hrhcp4im71l6mdkangccshn")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.3") (hash "14nnw6frpy7prvzjq462p97l23a6mgacrmshw4cvr3qwwxl7khp1")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.4") (hash "0v2wamps03hnssl21jgfif334w4fygighva6l357zw2xls76pq1d")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.5") (hash "10xxjnyypw869kvcp63lyhhwrhf9ra057pz50ivh2b1wghk3nxmk")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.6") (hash "0fqh8lzzg967s7ay7hgr50zfzaycfzx86354s9pvfwy64csxf9d4")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.7") (hash "00vlgjw6hxmnsj5yl25vy923yq8yaal6ig4plazfsjcyj4i2g413")))

(define-public crate-mdd-lib-0.1 (crate (name "mdd-lib") (vers "0.1.8") (hash "1r8ci2pll2sxqcbx53y7pvkcl2fyjli1hnp1j6hgm0lqaf9ppi6i")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.0") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ci3v2862f10qhxqrd5m2l35gnvazz1m21kzr16i6pvfmzrs166x")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.1") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ia04d5fq5h6jv2m2gzx5mbfcfnpih8ljg5k24c30x4li7i191ml")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.2") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1h1c44s8fypd85zbajpjifyqwkmdiwy68by6bcirizp3j96h4ndl")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.3") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0d961gr4wp7zwlx64p99bzcjr59y15hnzspyd4gqrb5dz0vawga5")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.4") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1m8ghfpwpp0vg6il4m9flddbwbmwhp8w8qpvw6wn0m9bk196r5z1")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.6") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1i6vzbwxkdan2pidxrhm2yh5b1z75ba8bpdib5lvzahbqinkbbbg")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.7") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0mdzg3s10x08gayvylkzmd0zhbraw7mcvwglc2dh38v6nvlpvgg9")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.8") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "07zzk42p8cir844fbrqvng9zxa4bbhzqda07jfn6118v5k3b6nmg")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.9") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1kma2vf9sp619k7m4sjsgzh2bz73ldyp72bwcmxg5ngzmq3g4chn")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.10") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0zhhy22adxh34qy6yp37qiarp8iqgxaq3wp5m0nlif3dxyc26fba")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.11") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0j0k8p2rpjbll9r2cjjp8ckpsb4rwjpc5irpaifsxnk9amg5995w")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.12") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "18j1a3yv6cmhpgg7qjimjrvxmh7mdw65pys277b2v56gcli5y4n6")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.13") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "193n2wkdvkmas2afirjpv3888aczjf56v8c86kxgpxs1nbq0ks52")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.14") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "065rkz2dxhqr6frv8pnky24895mqb8cf4nznn7cwbbq5vr9plgjm")))

(define-public crate-mdd-soundcloud-0.1 (crate (name "mdd-soundcloud") (vers "0.1.15") (deps (list (crate-dep (name "mdd-lib") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1hv4ylbpwln14fv33xfdqlh20apq6wf7mhdl8pqb4g55gqs3km69")))

