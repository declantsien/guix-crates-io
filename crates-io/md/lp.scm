(define-module (crates-io md lp) #:use-module (crates-io))

(define-public crate-mdlp-rs-0.1 (crate (name "mdlp-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)))) (hash "1wg76y30lhszp8fxgnyb6nqjwvnxqv8x7vgy38m58ap0p2ppp6p6")))

