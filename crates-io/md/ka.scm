(define-module (crates-io md ka) #:use-module (crates-io))

(define-public crate-mdka-0.0.1 (crate (name "mdka") (vers "0.0.1") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0c9ql42f9b2vcacq43286cgjfsm82piymx38cd2krmfwzsl51c79") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.2 (crate (name "mdka") (vers "0.0.2") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05vvn2z7vqdcjdvz139w9ws8qksv6k9zh6h3ypsnjv3h4h9cg4sv") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.3 (crate (name "mdka") (vers "0.0.3") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1pzdpg6ahnva6dw8xisajrcmcsrx6qrqprp8hmrna4dm39835g24") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.4 (crate (name "mdka") (vers "0.0.4") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1awm5dwg1kq8510agyq2681y6sadkqg1yi1mmxkdh8qw1ih755ym") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.5 (crate (name "mdka") (vers "0.0.5") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "11az6kvdnl6rv3c2lfr9yp2c3cghlsyf8ixiy9mccak4zzp4d5f2") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.6 (crate (name "mdka") (vers "0.0.6") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05a4fb5y722wnvmjy0fxfb8x97jbqimvy3czq438hqmapp7giddh") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.8 (crate (name "mdka") (vers "0.0.8") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0y7cyvrjdxmd3skffdqyh1ch2l48gzv2iqqry9yjjfazmmvcs9vw") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.7 (crate (name "mdka") (vers "0.0.7") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0zskn6dcm37ry9r76iajjhh39mqsp5w5iijfz92crywv6dvhm637") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.9 (crate (name "mdka") (vers "0.0.9") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1nc97qrk2ryb66nd2bs5jqjchpmfcj7qqvdcmhncb2rwg471ysng") (rust-version "1.75.0")))

(define-public crate-mdka-0.0.10 (crate (name "mdka") (vers "0.0.10") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15kyrlnclxpnhvm7rr6ksp6mx66xkfw0p5qbsq3nc1n0ya36lbwr") (rust-version "1.75.0")))

(define-public crate-mdka-0.1 (crate (name "mdka") (vers "0.1.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "154gj62qm006cipi74c6351786v6fmv4wbxvr4whqnwk3rnr4qnl") (rust-version "1.75.0")))

(define-public crate-mdka-0.2 (crate (name "mdka") (vers "0.2.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0hzyw2fw68psjs9v4z8dxp2vm4hnzvyffys603fzns5nff57pcya") (rust-version "1.75.0")))

(define-public crate-mdka-0.2 (crate (name "mdka") (vers "0.2.1") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09v8wdr9rm5ir5yimm4paay9jp7bslilxzs58w0x2jjl8wzbq2m1") (rust-version "1.75.0")))

(define-public crate-mdka-0.3 (crate (name "mdka") (vers "0.3.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1bsllgqwhg6jjddvvdb5cl9ys0acjiz76rhd3h4mavaz1kc35060") (rust-version "1.75.0")))

(define-public crate-mdka-0.4 (crate (name "mdka") (vers "0.4.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "12y9zim9khkw0l6jldvd42gdlx8jymbd74ps39n4bba47m68iz01") (rust-version "1.75.0")))

(define-public crate-mdka-0.5 (crate (name "mdka") (vers "0.5.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15r2rrypi6njvyxvlk23w5172vggah5vccmq5zmf5afg9vplzn51") (rust-version "1.75.0")))

(define-public crate-mdka-0.5 (crate (name "mdka") (vers "0.5.1") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1n6jh3d197d6pnymkhyvj3ygd4w4h69b3x096z85abrcryvb7fyv") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1cvmq9pyw9hd973k911gqc46sgc0gr6dbnn32zm6k12f8v4phdji") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.1") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ib7l8fjdjymdkzk9js6ak0qdspj7cjvz0wmd0i9rx4xhsk00bik") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.2") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qa7hwa1a4d7kcbb6wyl3z8ldgkxxh5yhxg63191rry60pb5m79n") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.3") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0c3f8m0hm4n7w10yijbw7qfay2ddgwqm58nl0vfpmv6d5p9xchzl") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.4") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15h3cjkbzk46yc76irii0bx9czgx3fwa518pbaqr9shjxd23p2x2") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.5") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0689kaafg6kmg3lkvfd0sgcrrrp0rhivy8vd9fkpw0nxxh7idhg1") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.6") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "071mllhpq3cyj3jm7170d4wssi8x32q9ra762ksccmwbf4qs0z3x") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.7") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "19wx5va3hb8b8l7y8zv6h9zm1995g2g13pa9g4d0jrld2nl89hv4") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.0.8") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1igb22xlsgjvp24271jyz7q0yj9r59z1daacikkdasj5ydzkg8wg") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2") (default-features #t) (kind 0)))) (hash "161awhpm7m6sa7bh3bjnh686m94lkqmnsvciy459rak82rbpyphz") (rust-version "1.74.1")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iwnm8ba8cxvmz8fyh42x4yb585lcmjc07mx5zn1mifqm9x84mpp") (rust-version "1.74.1")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ynp9mzf5q4iv3qn8l7yikxlx4djx4rll2hlgjhmqpwf2m5aj9n6") (rust-version "1.74.1")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0153s2wb9m7sfjwic24d6f4bky6k5a6xjbi2gymxz53n88bxsp1c") (rust-version "1.75.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.2.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fh10j6xbw9abc9x28kyg1q8wfxzd349p1b9m0lsqq2qh0001kyv") (rust-version "1.76.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.2.3") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k7rrc9d72wfclrkcfk1gyxg8v6gc6r6ajklh38gf2c0375bdilh") (rust-version "1.76.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.2.4") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2") (default-features #t) (kind 0)))) (hash "1my9gnbpy7x5hpkzn4svm4lsfdwh1j550iccr1dwws2bkccxa3hb") (rust-version "1.74.0")))

(define-public crate-mdka-1 (crate (name "mdka") (vers "1.2.5") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "html5ever") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.3") (default-features #t) (kind 0)))) (hash "0n14w4iv7kpfwsa42ldnii4v85g9a75rfvp2wvrpjihxbxp88xmq") (rust-version "1.74.0")))

(define-public crate-mdka-rs-0.0.1 (crate (name "mdka-rs") (vers "0.0.1") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05xxv2pdqwcak2r5h9hxjfxyq8y0icsz9jlsjrl2ff7kwnmsjqr5") (yanked #t) (rust-version "1.75.0")))

(define-public crate-mdka-rs-0.0.2 (crate (name "mdka-rs") (vers "0.0.2") (hash "174mag92sff0q46hfsy88lv16hi53lxzsfxlj0rmz89a66x1h1pv") (yanked #t)))

(define-public crate-mdka-rs-0.0.3 (crate (name "mdka-rs") (vers "0.0.3") (hash "1ffrj6z3sn9j55jxh9km564y43dqh4v83b6aj1h8gj9m4kqx3rhl") (yanked #t)))

