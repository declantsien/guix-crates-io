(define-module (crates-io md -b) #:use-module (crates-io))

(define-public crate-md-bakery-1 (crate (name "md-bakery") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "0w10jhd8qs9a0f4ihg2l6xa2mcwhm7jlm3s5hvxvgnwc1ygcq5sc")))

(define-public crate-md-bakery-1 (crate (name "md-bakery") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "1pc5m6c7cqrrdzb22b3xj2w9cfh77h5ra5n8ckag7dzh3s4c4xd5")))

(define-public crate-md-bakery-1 (crate (name "md-bakery") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "1ghgli6j3yhhymwghf21c9qp1lhijg70z6d0qc52qza7lrpn7mr8")))

(define-public crate-md-book-0.1 (crate (name "md-book") (vers "0.1.0") (hash "1s95fwmrnzg79jy5cnv7mclb6w9y8dc0h1lzrrslxngh0vacbwd0") (yanked #t)))

