(define-module (crates-io md vi) #:use-module (crates-io))

(define-public crate-mdview-0.1 (crate (name "mdview") (vers "0.1.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0x2ymrb93f0pv47yjnmf88mxgqwyy44imbcmgr1d1n0fxaa4rhf5")))

(define-public crate-mdview-0.1 (crate (name "mdview") (vers "0.1.1") (deps (list (crate-dep (name "ispell") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "145nsfqplq4vizxh43xds80aks4632sik6sdd2cpbb0iw2hj8w8k")))

