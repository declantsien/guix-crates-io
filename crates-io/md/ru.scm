(define-module (crates-io md ru) #:use-module (crates-io))

(define-public crate-mdrun-0.1 (crate (name "mdrun") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "comrak") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "16qwpca0jvi62q775vyr00c0h81ibz590y0gi0dzvfm706c2d05k")))

