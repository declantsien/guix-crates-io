(define-module (crates-io md -u) #:use-module (crates-io))

(define-public crate-md-ulb-pwrap-0.0.1 (crate (name "md-ulb-pwrap") (vers "0.0.1") (deps (list (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-linebreak") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "16xssnh3fn8csjsjgxpaib44sx2rd7i062qmkmdnclx4ji50mnm0")))

(define-public crate-md-ulb-pwrap-0.0.2 (crate (name "md-ulb-pwrap") (vers "0.0.2") (deps (list (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-linebreak") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "19ph7qszz4xm79fw639kx8qmxg76dsdkncafx7dv7683m7b6mbjl")))

(define-public crate-md-ulb-pwrap-0.0.3 (crate (name "md-ulb-pwrap") (vers "0.0.3") (deps (list (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-linebreak") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1ry29b5vgiq24grijbv7kkaq1vc51v8afvsyb2ynhp8hag14bzna")))

(define-public crate-md-ulb-pwrap-0.1 (crate (name "md-ulb-pwrap") (vers "0.1.0") (deps (list (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-linebreak") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "13rk549livbmf7v37q1cw20vfggxc5pmsf9zsbd3bv7680k3w3hh")))

(define-public crate-md-ulb-pwrap-0.1 (crate (name "md-ulb-pwrap") (vers "0.1.1") (deps (list (crate-dep (name "rstest") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "unicode-linebreak") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wqpnaqla9i7bkj3ab479q0n411cxbfjqxkzyp5ikiyvpz3qwkz4")))

