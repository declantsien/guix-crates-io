(define-module (crates-io md to) #:use-module (crates-io))

(define-public crate-mdtoc-0.1 (crate (name "mdtoc") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "15g5yzf7ljywfhacp9dcz7mljf6hnk7k44a3c5wj0b5s9fw8cj2k")))

(define-public crate-mdtoc-0.2 (crate (name "mdtoc") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "14z70n2myddfwbr15kswq402a0qs7wikq2q7qvwbz400wbd0cm4z")))

(define-public crate-mdtoc-0.2 (crate (name "mdtoc") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kyq3jg4g11q6g03ahzvxgz9m9463iqg7819s2phc3z6fnjdfpmh")))

(define-public crate-mdtoepson-0.1 (crate (name "mdtoepson") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "11bdf9dn83dvplsrxz9k775qmdfdazgy7879j90jvhrsrjig05f4")))

(define-public crate-mdtoepson-0.1 (crate (name "mdtoepson") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "118x40cha0hb5ky2isann85w7d99sdwm5w1yl723xpzzccla1lcg")))

(define-public crate-mdtoepson-0.1 (crate (name "mdtoepson") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1qp4yvc1rv0wycq17r8y5fwlpmh0q3cka7lh428n2iwd1aqimcg4")))

(define-public crate-mdtohtml-1 (crate (name "mdtohtml") (vers "1.0.0") (hash "1czygzf0gn194y147frlxp1x9v7d264f3zanljwviwqfzp0ib1pz") (yanked #t)))

(define-public crate-mdtohtml-1 (crate (name "mdtohtml") (vers "1.1.0") (hash "0ami2byqklb87zwrahwn7bq95asg9qfmx8fs1c58l4zsmj8kf7fk") (yanked #t)))

(define-public crate-mdtohtml-2 (crate (name "mdtohtml") (vers "2.0.0") (hash "0c179kfnwpq2pjaqm66shljc8bcgkkfqhrsw38p7s8wx98nfclc8")))

