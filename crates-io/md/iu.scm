(define-module (crates-io md iu) #:use-module (crates-io))

(define-public crate-mdiu-0.1 (crate (name "mdiu") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0wbyzlpkikdkfgsgn430m4nys521x3pl3r0154060r5dbrgl7j1n") (features (quote (("markdown") ("html"))))))

(define-public crate-mdiu-0.1 (crate (name "mdiu") (vers "0.1.1") (deps (list (crate-dep (name "http") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1kcda8finmwqzkcrsh6a05sa1i831cx7vkblwfn6khvp9kz633j3") (features (quote (("markdown") ("html"))))))

(define-public crate-mdiu-0.1 (crate (name "mdiu") (vers "0.1.2") (deps (list (crate-dep (name "http") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1jzmyjfi2x1whiwa7li4s1cza7mk5a5kialbxc1kb0jz4zq77pgd") (features (quote (("markdown") ("html"))))))

