(define-module (crates-io md ba) #:use-module (crates-io))

(define-public crate-mdbat-0.1 (crate (name "mdbat") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "is-terminal") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "termimad") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "1yl33pdzlvay3fwbpkjzy59rdd1ad4gbmlg21v9k9rbv5jnnwwm3")))

