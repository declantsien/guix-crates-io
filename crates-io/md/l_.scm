(define-module (crates-io md l_) #:use-module (crates-io))

(define-public crate-mdl_monkey-0.0.1 (crate (name "mdl_monkey") (vers "0.0.1") (hash "0b3pnf3zb2qp0sxb7r3i90br3j6g8c84ksv2l9fl2rcyg9qlngx1")))

(define-public crate-mdl_monkey-0.0.2 (crate (name "mdl_monkey") (vers "0.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "15idhnm03cb56sk80sw010kzpcfxwb88a9il4zq70b3zs2a6bk52")))

(define-public crate-mdl_monkey-0.0.3 (crate (name "mdl_monkey") (vers "0.0.3") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0b921zrcw256l6sw3i2iqgkx91alz14wvkgl99fisgz794k33wkm")))

(define-public crate-mdl_monkey-0.0.4 (crate (name "mdl_monkey") (vers "0.0.4") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0bjlz69v857z7qiwlb8aa64zrbfpkb2qld10kwvjq75cb1y5nzjc")))

(define-public crate-mdl_monkey-0.0.5 (crate (name "mdl_monkey") (vers "0.0.5") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "042pf9x0csj9as4w6r1k47g5ljx08l97aqww4dbdy79dxn0j29ks")))

(define-public crate-mdl_monkey-0.0.6 (crate (name "mdl_monkey") (vers "0.0.6") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "12ya0nga207jpbrd8z66w3zl3dxj5bva339xvs85ddj63ig1398h")))

(define-public crate-mdl_monkey-0.0.7 (crate (name "mdl_monkey") (vers "0.0.7") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "152f52ml7lb0vbkmmrrfqs5szkwpdvpkszmkkwdyjpmwbr3ma935")))

(define-public crate-mdl_monkey-0.0.8 (crate (name "mdl_monkey") (vers "0.0.8") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1hij2vqrj6p1fmcbqfshq5sii3xp64sdxknv7hr7m2g6y69kz6xz")))

(define-public crate-mdl_monkey-0.0.9 (crate (name "mdl_monkey") (vers "0.0.9") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0nhfh84hnispd3mm2r7zavb012y64k16yqwikwm0c39hn6pbsl81")))

(define-public crate-mdl_monkey-0.0.10 (crate (name "mdl_monkey") (vers "0.0.10") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0lmv8v8p7sfpkxl1szn7q6izpchajy964x4mw6av6xaq1l44psz9")))

(define-public crate-mdl_monkey-0.0.11 (crate (name "mdl_monkey") (vers "0.0.11") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0pk7nqhzddbwkv3m26xipg199c0sq95pcqbgrpz4bpag1rbbc10w")))

(define-public crate-mdl_monkey-0.0.12 (crate (name "mdl_monkey") (vers "0.0.12") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0l5f9v8pzlvjiinw6gj58k1ylkdwaydxpj8nix6s96999qgj118c")))

(define-public crate-mdl_monkey-0.0.13 (crate (name "mdl_monkey") (vers "0.0.13") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0yf3h8j4clkzqy7r450y6cbg6kgmw3vjanas98r51a50x2kam196")))

(define-public crate-mdl_monkey-0.0.14 (crate (name "mdl_monkey") (vers "0.0.14") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1n4vrz2a84zqgnaf102s6wkpxgph6p5fs1rzjbpyyq2pnda2qi00")))

(define-public crate-mdl_monkey-0.0.15 (crate (name "mdl_monkey") (vers "0.0.15") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0zy4y8wanhyc2qpnh8bqdxrmhdlvh0r134vc0i7jh4w29s6wd493")))

(define-public crate-mdl_monkey-0.0.16 (crate (name "mdl_monkey") (vers "0.0.16") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0h9jzalkqzpknk8arnyv3xk8l76p381v744w2v1rc3133xpr1n8m")))

(define-public crate-mdl_monkey-0.0.17 (crate (name "mdl_monkey") (vers "0.0.17") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0gf8gx43z3snrq3z42p8f9kg38n2gslyzwfgag0yn7bz6404vi1l")))

(define-public crate-mdl_monkey-0.0.18 (crate (name "mdl_monkey") (vers "0.0.18") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0nd6ck03h0ghzghklzqagpfrpjankdxypby8ln6lm1iffiy3h52z")))

(define-public crate-mdl_monkey-0.0.19 (crate (name "mdl_monkey") (vers "0.0.19") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0sbn2awwprqmsjip0r5wgyryy8yky0y1wffn9gw6v6sfxvwfv6n6")))

(define-public crate-mdl_monkey-0.0.20 (crate (name "mdl_monkey") (vers "0.0.20") (deps (list (crate-dep (name "getopts") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0yr65dgaz6snc9ckw80r4l79p6n7z7wgzbf0grphvyn5vy963ghc")))

(define-public crate-mdl_monkey-1 (crate (name "mdl_monkey") (vers "1.0.0") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "0i4xf88q4kh2918w9s6ccy491a9h23gcjy1pq54gsbb5mlf4lph7")))

