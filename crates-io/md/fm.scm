(define-module (crates-io md fm) #:use-module (crates-io))

(define-public crate-mdfmt-1 (crate (name "mdfmt") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "09mwm79a480s8syldbcir476cvh3z9ib5kmp79c24wpiy642ah6k")))

