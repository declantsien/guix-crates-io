(define-module (crates-io md #{5m}#) #:use-module (crates-io))

(define-public crate-md5mix-0.1 (crate (name "md5mix") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0ci81n526ranigp4p8vmsj6gqd7zr0ppliw1yq65cdbr9xk7k7b1")))

(define-public crate-md5mix-0.1 (crate (name "md5mix") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "079slsq5d5r8ipafw8nmrpmhy0zxszaha3vwsry9vfgphszwi192")))

(define-public crate-md5mix-0.1 (crate (name "md5mix") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1r7ncs7wg9rn17lkybgy78b2llh4hrmk13jh64ph2xqk5vzb78pm")))

