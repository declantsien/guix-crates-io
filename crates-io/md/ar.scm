(define-module (crates-io md ar) #:use-module (crates-io))

(define-public crate-mdarray-0.1 (crate (name "mdarray") (vers "0.1.0") (hash "1lgy0krd5kx847wnrz6nqpxq4ahch48krns03j658m42jbr377kr")))

(define-public crate-mdarray-0.2 (crate (name "mdarray") (vers "0.2.0") (hash "1falssxj3aqvja7lmzp8c746sgz83rxd3yy1w9km2xnxkmncdcvz")))

(define-public crate-mdarray-0.3 (crate (name "mdarray") (vers "0.3.0") (hash "1ccm3nqpyanrfrxkqmkakn7g67wvd7dak5b56mnxczrprahv3mxa")))

(define-public crate-mdarray-0.4 (crate (name "mdarray") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0vrkm2bdbkc585v0qkx7zvkk4bi9yhha2blprjpx40lx7s6bzjx2") (features (quote (("permissive-provenance") ("nightly")))) (rust-version "1.65")))

(define-public crate-mdarray-0.5 (crate (name "mdarray") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0lkf7s219bl7w95f86sdw5pfb2j12z1i7lfprscyzsdcxk8946a2") (features (quote (("nightly")))) (rust-version "1.65")))

