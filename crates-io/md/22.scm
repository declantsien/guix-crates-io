(define-module (crates-io md #{22}#) #:use-module (crates-io))

(define-public crate-md22-0.1 (crate (name "md22") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0gc5jrc4zsy668z7b48821kl0s6vf0ia8a24bkrv7kdqq50ilv2g")))

