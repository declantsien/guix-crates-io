(define-module (crates-io md #{2p}#) #:use-module (crates-io))

(define-public crate-md2pdf-0.0.0 (crate (name "md2pdf") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "17zafh6wzalr5iawm10ps0yl736isp71464spinfl0k4l8hm23r5")))

(define-public crate-md2pdf-0.0.1 (crate (name "md2pdf") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "144x7chwfp9icqsrgc23dv30vw3z8j47wlcanc8k0k818jfb90an")))

(define-public crate-md2pdf-0.0.2 (crate (name "md2pdf") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.0.7") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1biacrr1n9k0z2n8jasb5rvx2w9328mfmzaq5b7nwjl596lzr988")))

(define-public crate-md2pdf-0.0.3 (crate (name "md2pdf") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.0.7") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.9") (default-features #t) (kind 0)))) (hash "0qxfahqg8rdggsf5wbq5l1bm645a27vxkx4hrqgf3qzfwi60nxld")))

(define-public crate-md2pdf-mdbook-0.0.1 (crate (name "md2pdf-mdbook") (vers "0.0.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "tectonic") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 0)))) (hash "1229phw2y1lhy56ny9v6xy48ihbp8b62mjmy7lrklavnwxncq8hh")))

