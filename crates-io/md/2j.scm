(define-module (crates-io md #{2j}#) #:use-module (crates-io))

(define-public crate-md2jira-0.1 (crate (name "md2jira") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0j40icw348hgli3ykam422i3x46d5p7wpqrvwvccvk6wa491jgzz")))

(define-public crate-md2jira-0.1 (crate (name "md2jira") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1h33hz1b5qd6wy47jrj8hkg1s9mygj08rk8i35bl5r1l1v4d4akd")))

