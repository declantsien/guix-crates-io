(define-module (crates-io md tc) #:use-module (crates-io))

(define-public crate-mdtc-0.1 (crate (name "mdtc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "02pfvi4z31vjzhcvmklhpb9y0brn09kdsw3n4bscp0lglrigggx5")))

