(define-module (crates-io md dd) #:use-module (crates-io))

(define-public crate-mddd-0.1 (crate (name "mddd") (vers "0.1.0") (deps (list (crate-dep (name "mddd-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ksm2ig66c6m1mbkr8xi3lx63ga03sd6zz5nm1j5ib0pc86c0fjn") (yanked #t)))

(define-public crate-mddd-0.1 (crate (name "mddd") (vers "0.1.1") (deps (list (crate-dep (name "mddd-macro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bhvnb4v4rs9jxrlgjww5j3k2pp2crqrm04624n1rz0l3c5jbk48") (yanked #t)))

(define-public crate-mddd-0.1 (crate (name "mddd") (vers "0.1.2") (deps (list (crate-dep (name "mddd-macro") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1j0sr2is1gblcjdq6xsqvxddlz8gc7nl758yf4rqi28v33md801p")))

(define-public crate-mddd-0.2 (crate (name "mddd") (vers "0.2.0") (deps (list (crate-dep (name "mddd-macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0xzp2kafp2ksnbfyr6ms0n3wni8kxzi2bkjhi4n08qp80nqrxjsx") (yanked #t)))

(define-public crate-mddd-0.2 (crate (name "mddd") (vers "0.2.1") (deps (list (crate-dep (name "mddd-macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02j26syhaj4av5hzwa69nlc9jd24nk57da67i1z4aljinmym7sy9") (yanked #t)))

(define-public crate-mddd-0.2 (crate (name "mddd") (vers "0.2.2") (deps (list (crate-dep (name "mddd-macro") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "00zadh994dnl9wy8qslz93jkl5wagbl4d9kh30px6pb77z5mhz64") (yanked #t)))

(define-public crate-mddd-0.2 (crate (name "mddd") (vers "0.2.3") (deps (list (crate-dep (name "mddd-macro") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "12wp6q4kzf4bdzjfyx03ya72dnnglm3pg9k702a7rqsvb7hkrwlr") (yanked #t)))

(define-public crate-mddd-0.2 (crate (name "mddd") (vers "0.2.4") (deps (list (crate-dep (name "mddd-macro") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0zh03y4qyvapnsviyl455miwr4288zq9b4pd65ppsmpc0g96bfg3") (yanked #t)))

(define-public crate-mddd-0.2 (crate (name "mddd") (vers "0.2.5") (deps (list (crate-dep (name "mddd-macro") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02rqwbbx2bfrisywmfqbyp76xm2hyarl2jclgv4pn3pf0wxg53yn")))

(define-public crate-mddd-0.3 (crate (name "mddd") (vers "0.3.0") (deps (list (crate-dep (name "mddd-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1a5316cgw2srv2pdg4iaaw6zd490s2ff41z4dpl7fk5671jqzn4p")))

(define-public crate-mddd-0.3 (crate (name "mddd") (vers "0.3.1") (deps (list (crate-dep (name "mddd-macro") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1ssk8mg93jq318d77gpx77gnmzm2wxwwlc1gva3cama0g7jzp7a2") (yanked #t)))

(define-public crate-mddd-0.3 (crate (name "mddd") (vers "0.3.2") (deps (list (crate-dep (name "mddd-macro") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "mddd-std") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "mddd-traits") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0kgwrb7lgp9r22044j06c9rxw6wa0rd6w1b44imlzazl8wflzs57")))

(define-public crate-mddd-macro-0.1 (crate (name "mddd-macro") (vers "0.1.0") (deps (list (crate-dep (name "mddd-traits") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1npn8apa0hw96lj2wh0a7584j71bw8a7y6h2iymw22blhrxzf1d6")))

(define-public crate-mddd-macro-0.1 (crate (name "mddd-macro") (vers "0.1.1") (deps (list (crate-dep (name "mddd-traits") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17f8pg1fs4rcdyls2gzp7qb3g1dbjcqfiyyn06sjr4xh30p6si6x")))

(define-public crate-mddd-macro-0.1 (crate (name "mddd-macro") (vers "0.1.2") (deps (list (crate-dep (name "mddd-traits") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lxkjl5l2wjfv23ngp9xnl92wczwn0z1sgs37dfyxsd3j79d8ngz")))

(define-public crate-mddd-macro-0.2 (crate (name "mddd-macro") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fkrrf2l4jyvb55qaycif7pw53qnq4zp1hvnadwh78fcfskzz0b4")))

(define-public crate-mddd-macro-0.2 (crate (name "mddd-macro") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10l53l4417m36c7ik0l0bf80nhg1zz36afw0s3224b65fynsh45k")))

(define-public crate-mddd-macro-0.2 (crate (name "mddd-macro") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11nmnwnc9kl06a3yshi8ly7wgqr1wxr0bsy2gdzvl7gksilvf8ck")))

(define-public crate-mddd-macro-0.2 (crate (name "mddd-macro") (vers "0.2.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "025gby3v8kwgz0cj9rwjyp035af38kbrcs5yymi54dwxyb5qdgx8")))

(define-public crate-mddd-macro-0.3 (crate (name "mddd-macro") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0k1skba32mhkcjjnv8iv3hgqka32sqr89vfbrnncdqb573llxw15")))

(define-public crate-mddd-macro-0.3 (crate (name "mddd-macro") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yrrvlfc9c7zr9px46aqs1gqhh2ys0vv0hdf4gww3ppvya25nxgl")))

(define-public crate-mddd-macro-0.3 (crate (name "mddd-macro") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xcmr8xc1qwqn2gcfgjka2f7sh1h92p66h639qb9yy7pwfw7dgph")))

(define-public crate-mddd-std-0.1 (crate (name "mddd-std") (vers "0.1.0") (deps (list (crate-dep (name "mddd-traits") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j1y7qbnd4ij0jy4i04vc2lfm933rqgccf8vf1j9z7vpgxik4nwm")))

(define-public crate-mddd-std-0.2 (crate (name "mddd-std") (vers "0.2.0") (deps (list (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hz949525w8cwdbnhlh46z7l535ka8n6993c73c27rqaa1in4vc1")))

(define-public crate-mddd-std-0.2 (crate (name "mddd-std") (vers "0.2.1") (deps (list (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1aa9ig0s6bx4gzzs3ff32jw723bp5pfrbsqnzfm7hy9h96k412yg")))

(define-public crate-mddd-std-0.2 (crate (name "mddd-std") (vers "0.2.2") (deps (list (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "166vhgpfg2dq66b1b8cq8h5a079syjnx9agp7y08gsqi8v31hqpw")))

(define-public crate-mddd-std-0.2 (crate (name "mddd-std") (vers "0.2.3") (deps (list (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n3zf9cplzqpcn0sxy8splqk0j3w9y6s044gjadkgk4j38inpsbv")))

(define-public crate-mddd-std-0.2 (crate (name "mddd-std") (vers "0.2.4") (deps (list (crate-dep (name "mddd-traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jzg95vl2swflqkpdkbp268nrqfgy50a6h0k47132yadqcisgpr6")))

(define-public crate-mddd-std-0.3 (crate (name "mddd-std") (vers "0.3.1") (deps (list (crate-dep (name "mddd-traits") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0lmkdkilah742pglcgvwsdx7wfbpz5jdkw8q5iw30m5fiw53qwi8")))

(define-public crate-mddd-traits-0.1 (crate (name "mddd-traits") (vers "0.1.0") (hash "1x0cgdq00jlxpdbk6pgqkn0y8wb35h8y4rrcr9ylyyipf33qmcpb")))

(define-public crate-mddd-traits-0.2 (crate (name "mddd-traits") (vers "0.2.0") (hash "1m9lp9wcb76lk5sv55yqmpyn4vlry7zh3pimch192crb4sh85yaj")))

(define-public crate-mddd-traits-0.3 (crate (name "mddd-traits") (vers "0.3.1") (hash "15c2g9nivfsvysxg96gw0mkhdg32by24myynbnnrghn9i42fxvbn")))

