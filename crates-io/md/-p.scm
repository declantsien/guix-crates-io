(define-module (crates-io md -p) #:use-module (crates-io))

(define-public crate-md-parser-0.1 (crate (name "md-parser") (vers "0.1.0") (hash "1n3j1ivpm9fw1dkcq13q1hmfxlrk4nvqdsnyfn16bqz09hmz6xsv") (yanked #t)))

(define-public crate-md-postprocess-0.1 (crate (name "md-postprocess") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0majbcfy0i2wqv9p8hiils4b8md8xldpvpiy89bq81ic26jc4sw9")))

