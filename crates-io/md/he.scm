(define-module (crates-io md he) #:use-module (crates-io))

(define-public crate-mdhere-0.1 (crate (name "mdhere") (vers "0.1.0") (hash "1s7qsq3yxnq2hxgb76wasb9zdy7bva97ajl1cw4xzl5n2iq9x1bd")))

(define-public crate-mdhere-0.0.1 (crate (name "mdhere") (vers "0.0.1") (hash "15pg6l5izcgnfnv40ihlxawbjaqkn2cvl901pzh02s33qg3j0nn0")))

