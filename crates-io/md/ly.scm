(define-module (crates-io md ly) #:use-module (crates-io))

(define-public crate-mdlynx-0.1 (crate (name "mdlynx") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9") (features (quote ("simd"))) (kind 0)) (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1813ca50l6w612x0myw366arghwp6k4nbn4mk8qi22i0sdw7xa8l") (features (quote (("default" "parallel")))) (v 2) (features2 (quote (("parallel" "dep:rayon"))))))

