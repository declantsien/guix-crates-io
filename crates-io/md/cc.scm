(define-module (crates-io md cc) #:use-module (crates-io))

(define-public crate-mdccc-0.1 (crate (name "mdccc") (vers "0.1.0") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1xnr772601wk2nmi2llhmzwr6jrr8ryna5mwjxags8nhg86szyyk")))

(define-public crate-mdccc-0.1 (crate (name "mdccc") (vers "0.1.1") (deps (list (crate-dep (name "pulldown-cmark") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1w4dhkp6bn928xrzv40ss5l38g96575pgh9vl4mmglxynpaw8x74")))

