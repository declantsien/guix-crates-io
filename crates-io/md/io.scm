(define-module (crates-io md io) #:use-module (crates-io))

(define-public crate-mdio-0.1 (crate (name "mdio") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0y95w7za0wh6jarqv5j1jn03h0xz43cwm7nr12y1hdm1b33mwq4z") (features (quote (("bitbang" "embedded-hal" "nb"))))))

(define-public crate-mdio-0.1 (crate (name "mdio") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0jc0ma7kv237z78rdx6zmnpbla53wl28fjhvcm99k8s61if6c1s9") (features (quote (("bitbang" "embedded-hal" "nb"))))))

(define-public crate-mdio-0.1 (crate (name "mdio") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "158yaxpj71awygf0qw7wzn7m1pc5w25xqvms7wcdmgczlvih5jp0") (features (quote (("bitbang" "embedded-hal" "nb"))))))

