(define-module (crates-io wk dr) #:use-module (crates-io))

(define-public crate-wkdr-0.1 (crate (name "wkdr") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "wikibase") (req "^0.1") (default-features #t) (kind 0)))) (hash "09qb98dap6m0x6rpdl34d7qxk55wdbmigpr30gaf5hwrzbnf505f")))

(define-public crate-wkdr-0.1 (crate (name "wkdr") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "wikibase") (req "^0.1") (default-features #t) (kind 0)))) (hash "0c3k15dd8g9ql9ai0b434z1smrw9g43zbjyl0s8nfgr2vhg4rk76")))

(define-public crate-wkdr-0.1 (crate (name "wkdr") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "wikibase") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0nrbqd2lmbgp4gkzb5zk13r6dl8nihmvd81nabr9fj465hs92zk1")))

(define-public crate-wkdr-0.2 (crate (name "wkdr") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "wikibase") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0b35w2189ghb6rckfvlk28yn9cb24a3sccjabwpg111k8xf5smy1")))

