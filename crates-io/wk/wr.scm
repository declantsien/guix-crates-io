(define-module (crates-io wk wr) #:use-module (crates-io))

(define-public crate-wkwrap-1 (crate (name "wkwrap") (vers "1.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18gp8x25d4nn97rg0hd570fxpkqf4axa9sqp59vjc3rnhnjfp2m6")))

(define-public crate-wkwrap-1 (crate (name "wkwrap") (vers "1.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cwzrh2p7djja79ns2ggr023awfnwqx5ysm9wfpmpgvi91m03shy")))

(define-public crate-wkwrap-1 (crate (name "wkwrap") (vers "1.6.0") (deps (list (crate-dep (name "lz4") (req "^1.23") (default-features #t) (kind 0)))) (hash "1xn61hhabrgc5qbs0p6pwzzfn6nr824scx3xkvards11w18i13m7")))

