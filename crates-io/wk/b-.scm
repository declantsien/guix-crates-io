(define-module (crates-io wk b-) #:use-module (crates-io))

(define-public crate-wkb-raster-0.1 (crate (name "wkb-raster") (vers "0.1.0") (hash "1l6da5xx3vss74zq0famsjjbg1j7vyhmn7l60biafnc7iqv646sd")))

(define-public crate-wkb-raster-0.1 (crate (name "wkb-raster") (vers "0.1.1") (hash "0jlmikcnrgnmnhm17g2ncazhs9x4kv93kj5w64g7k6536ixr9zci")))

(define-public crate-wkb-raster-0.2 (crate (name "wkb-raster") (vers "0.2.0") (hash "1qk0f9qvmch5ich95vqzcvzr4qzj9kr726c99lv6bw9vldyhcq13")))

(define-public crate-wkb-raster-0.2 (crate (name "wkb-raster") (vers "0.2.1") (hash "1qwg2n709lmxada7qa9q24pdj6wq0ihgk9a1339gdg205wvjaf4p")))

(define-public crate-wkb-rs-0.1 (crate (name "wkb-rs") (vers "0.1.0") (deps (list (crate-dep (name "geo-types") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qz568pv8g8n529hqqg8066dachmkr4q3hbqw409vgq9lhcyzcif")))

(define-public crate-wkb-rs-0.1 (crate (name "wkb-rs") (vers "0.1.1") (deps (list (crate-dep (name "geo-types") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "12khc76dgwf86qnj0415s0hap43g6kzp069vv3iccgkdwishpnxz")))

