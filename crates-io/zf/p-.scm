(define-module (crates-io zf p-) #:use-module (crates-io))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)))) (hash "036wgz80ngdvv8sr2fyhhrjha5xllp1i5hr568qdii0slf53a3wh")))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)))) (hash "02ak29cs44jw0b1mzyxp4pb2bk3fj9yd0xbzvyr6cq14y2irjqdr")))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)))) (hash "0v4vz65ygpkmi02vnd5whip7akhqf5bslsjnx9i52z3zr883q3n9")))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)))) (hash "0hn1ssk2nnsrq6mxzh7h7wf9m6yylriqkzp178sr4a0ss03iasn4") (features (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)))) (hash "0v4xz97bql5zspzaql1vwk69n2ry6jbh0jbci9dcm7jznqmgr625") (features (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0anvyj6736pd9zn9wr1p8hfqs0s6k0k762r68rp83fjrhh94zx1g") (features (quote (("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "049asb202bm6pvzrk6ligcq1l493y3crgdlhr3f0zy2g5i7f9fph") (features (quote (("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "12liz0fzwx98zzix3286bz0152lhfwz9mq8nlbvxvxr31d0xn637") (features (quote (("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "02qpr2icmd9wqa3jxjrry54dx1lp8cpnxrg05yj7dvwrh1npzmlr") (features (quote (("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.9") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0ilj1m01zihzyjc05r206mlr9iccj1ji8fvyfspz9ykj2l01lb6x") (features (quote (("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.10") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0h7s87anppi3dgb17np1psli2pvn52lvxk9qvw7ckhncj7d2q5ss") (features (quote (("static") ("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.11") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0fbbngc2vbz1p9zn7rfc1zmazgfjs2bavmh7q9bz4q2125ma2bpc") (features (quote (("static") ("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.12") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1l1c83h39rpzy5vznh6d371bdhb8zcsafrabcjw4nqsi4gly9dpv") (features (quote (("static") ("default") ("cuda") ("0_5_4"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.13") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1g5w0in5vk68sn93x5z8z59jfinkw7684f46dx0zkyakiqh7yi0s") (features (quote (("static") ("default") ("cuda"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.14") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "16d0acf2vjdk7mb567r7rkckgs93qwiincyaw2i3v22amg50hzx4") (features (quote (("static") ("default") ("cuda"))))))

(define-public crate-zfp-sys-0.1 (crate (name "zfp-sys") (vers "0.1.15") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1ijmaqq516z232gfa78llbvc9pwr54d4a2qk7f19l1n6xcx08sfg") (features (quote (("static") ("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1 (crate (name "zfp-sys-cc") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "11gjx5z0lyxagrr1dj1qd9rlpnhg3sn7hrwsi2jrnglhqxiggl9q") (features (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1 (crate (name "zfp-sys-cc") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1h4h7ykaslnlkv847b4zy6y3j7pf956m7b9l63zch5l9cqvsx5kz") (features (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1 (crate (name "zfp-sys-cc") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "06ck40hjqf2zqknzas31jjslhnfsnqfs8fhcmr35h5gs5m2m5lxj") (features (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1 (crate (name "zfp-sys-cc") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0dwcdyhb9h1klkndhi3b6nmhkhzlanfjqdcjwayay192smmsh2n8") (features (quote (("default") ("cuda"))))))

(define-public crate-zfp-sys-cc-0.1 (crate (name "zfp-sys-cc") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.66.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0vml50ya15macl3prqiyv87bhxns2vzzvv319n1mx3jgklpp32nz") (features (quote (("default") ("cuda"))))))

