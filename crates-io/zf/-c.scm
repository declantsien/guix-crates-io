(define-module (crates-io zf -c) #:use-module (crates-io))

(define-public crate-zf-cc-utils-0.1 (crate (name "zf-cc-utils") (vers "0.1.0") (hash "0ykv048v25gsasz09p52dpv1pmmy8fzjkshrpsv9s8wg8hmn0nm2")))

(define-public crate-zf-cc-utils-0.1 (crate (name "zf-cc-utils") (vers "0.1.1") (hash "0iwxvr4fn33mggwxc5bg5pf634y2wdxv6raq319g6a65wd577mbj")))

(define-public crate-zf-cc-utils-0.1 (crate (name "zf-cc-utils") (vers "0.1.2") (hash "0fyq0yyzq1k1d53jhzc69q3ikmqmgiss72adlyx9fawr1m05571g")))

