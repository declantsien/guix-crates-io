(define-module (crates-io zf il) #:use-module (crates-io))

(define-public crate-zfilexfer-0.0.2 (crate (name "zfilexfer") (vers "0.0.2") (deps (list (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "czmq") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "zdaemon") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1dr9m5vq8n5f62pdyswmdnqhswp7lhvmxw3lr05a228783i3fgja") (yanked #t)))

