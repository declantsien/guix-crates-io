(define-module (crates-io zf mt) #:use-module (crates-io))

(define-public crate-zfmt-0.0.0 (crate (name "zfmt") (vers "0.0.0") (hash "1krr2l08dhkr4jjlg81pvm16q6mavydlvrqjjwqps3l9m27rfz1p")))

(define-public crate-zfmt-decoder-0.0.0 (crate (name "zfmt-decoder") (vers "0.0.0") (hash "1l4rhb29vjdmf7z5wnqch1v9wbln4k5fha6n989b08zj8y66smnr")))

(define-public crate-zfmt-macros-0.0.0 (crate (name "zfmt-macros") (vers "0.0.0") (hash "1yp0541zx56h340hdha9fk3lgg4dkqr4hdk16vy33h418fl4fw0f")))

(define-public crate-zfmt-rtt-0.0.0 (crate (name "zfmt-rtt") (vers "0.0.0") (hash "1b5vzp2rjnmrxcqqqp7slmj81qi2ddws9dgh0af4l54d5690l3kp")))

