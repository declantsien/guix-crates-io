(define-module (crates-io zf la) #:use-module (crates-io))

(define-public crate-zflags-0.1 (crate (name "zflags") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "comn-pms") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "~0.1.21") (default-features #t) (kind 0)))) (hash "04ghkrqdfavcy572vd065kfh6c7bg22fq3xjxsfxr5j7ld94pj9y")))

(define-public crate-zflags-0.1 (crate (name "zflags") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "comn-pms") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "~0.1.21") (default-features #t) (kind 0)))) (hash "0kki8amjgaaqpww4pfmf54zr5i8amjgrldkpsr62wr2dwvqzyrd8")))

(define-public crate-zflags-0.1 (crate (name "zflags") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "comn-pms") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ctor") (req "~0.1.21") (default-features #t) (kind 0)))) (hash "03ysxqyla9m2l5fzgfvqp4wj1m5zvp4c9jlkiz29jrcnwdz9wplb")))

