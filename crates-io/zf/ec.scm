(define-module (crates-io zf ec) #:use-module (crates-io))

(define-public crate-zfec-rs-0.1 (crate (name "zfec-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1axgjgff4i2z0rmkml7cyj8dbw3zm8v0daa5l5bd3zc5y1kbqmih")))

