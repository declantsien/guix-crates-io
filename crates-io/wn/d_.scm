(define-module (crates-io wn d_) #:use-module (crates-io))

(define-public crate-wnd_macros-0.1 (crate (name "wnd_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12wjk1wsmjs901mmpn4k88pm5xjxrdav84p2p7wmfynspnlkkm53") (features (quote (("todo_attr") ("thread") ("full" "todo_attr" "thread"))))))

