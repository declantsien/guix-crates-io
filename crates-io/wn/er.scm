(define-module (crates-io wn er) #:use-module (crates-io))

(define-public crate-wnerkjnlvkeasfasef-0.1 (crate (name "wnerkjnlvkeasfasef") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "11200i8yggkg7c6hv7ymrlavhgs53ypfz54l95407ddd3hxf13yb")))

(define-public crate-wnerkjnlvkeasfasef-0.2 (crate (name "wnerkjnlvkeasfasef") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0gy20pc9ww99lw79wrzrb7jjfgcy5walsp1gnf5q0cyll2wmq3fr")))

