(define-module (crates-io wn g-) #:use-module (crates-io))

(define-public crate-wng-lib-0.1 (crate (name "wng-lib") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.17") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1lrzf3gqsrv9slajb53nq2v6h3v656jx5yr9l9j6xwkvgy1nwm80")))

(define-public crate-wng-lib-0.1 (crate (name "wng-lib") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.17") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0f1gbiyvc0bjdbdrzmb12by842k16lmjii9lan2ifbhhi1hcn111")))

(define-public crate-wng-lib-0.1 (crate (name "wng-lib") (vers "0.1.2") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.17") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0aa38r4y9n04n87lz5dy6148qsdm4hv2syzmdjdsvg4dqfgnxsr6")))

(define-public crate-wng-lib-0.1 (crate (name "wng-lib") (vers "0.1.3") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.17") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1w1pf53rdmgc0aj24x55y4k76caq5cqgyydf4f790dykdfn9ah1a")))

