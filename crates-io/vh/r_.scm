(define-module (crates-io vh r_) #:use-module (crates-io))

(define-public crate-vhr_datatypes-0.1 (crate (name "vhr_datatypes") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)) (crate-dep (name "vhr_serde") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hxy0zd50wbpvp1mq2ajmfx0mm8mh9wb3xnrkskcw4px058vs9nx")))

(define-public crate-vhr_serde-0.1 (crate (name "vhr_serde") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "1id6krjppfic1br5139rx7qb2r26hg2wxj8dlvqyyc3hpi8cbgzc")))

