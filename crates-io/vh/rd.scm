(define-module (crates-io vh rd) #:use-module (crates-io))

(define-public crate-vhrdcan-0.1 (crate (name "vhrdcan") (vers "0.1.0") (deps (list (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0fjk6cwzwr7q5wgnqswgvaic6k9pq2yij1y8yzk6hb58x7aj1hpk") (features (quote (("serialization" "serde"))))))

