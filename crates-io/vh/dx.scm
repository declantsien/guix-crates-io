(define-module (crates-io vh dx) #:use-module (crates-io))

(define-public crate-vhdx-0.1 (crate (name "vhdx") (vers "0.1.0") (deps (list (crate-dep (name "gpt") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "048h5xskw8bcr5bafby6y8l1mwkwj6niccdkqzpkjm4422m0irkw")))

