(define-module (crates-io xo r-) #:use-module (crates-io))

(define-public crate-xor-distance-0.1 (crate (name "xor-distance") (vers "0.1.0") (hash "13lzz0vc2nqm0i1nisj98m6ax4lk7lkna9i50kl33jdjf44s7xii")))

(define-public crate-xor-distance-0.2 (crate (name "xor-distance") (vers "0.2.0") (hash "1zmrpfpg56m7j5gfp93yc5x4nmmrnn9hrnqldb45zd2yn05k55ig")))

(define-public crate-xor-distance-exercise-0.1 (crate (name "xor-distance-exercise") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1jjy1l14gfj561ism04nyna70qqlkpw3mm6na8im93m5jnw8848k")))

(define-public crate-xor-distance-exercise-0.1 (crate (name "xor-distance-exercise") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0fcyz4wgf69j0d4sm0xg0z1qiw1ka9zcddlnzjsq1x5np6f73b1c")))

(define-public crate-xor-distance-exercise-0.2 (crate (name "xor-distance-exercise") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0h23izl9s6360rhgsl1jyywbazsfnsns0nd482wrn1a4g2lds9r5")))

(define-public crate-xor-distance-exercise-0.2 (crate (name "xor-distance-exercise") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0agwpyfg0ns64bl8b4d1fpy7hmn0vksp8hjxir07xq4diqn7fcfj")))

(define-public crate-xor-distance-exercise-0.2 (crate (name "xor-distance-exercise") (vers "0.2.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "14gn47jlbjjlkk9fywwsz8yb1vjgnk8jxg736mqq33510249dqf7")))

(define-public crate-xor-distance-exercise-0.3 (crate (name "xor-distance-exercise") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hlxyw81wkan2viyan7b1ias0mp166dr93rgynsm2x5l597xvjaj")))

(define-public crate-xor-distance-exercise-0.3 (crate (name "xor-distance-exercise") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0za3561fwh55l684ag1fk746d78kbll4nsjqlqcxgyq5l7ps4s35")))

(define-public crate-xor-distance-exercise-0.3 (crate (name "xor-distance-exercise") (vers "0.3.5") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0p6mbv1nbapq7cb12pqdyb7mxvrci4qpl6zlwmry9nd12kbl2ybh")))

(define-public crate-xor-distance-exercise-0.3 (crate (name "xor-distance-exercise") (vers "0.3.6") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0rxd7ql22yhqmwyaqk1cfvc1vn3fxjprqii8fmd2wfzshi040l8v")))

(define-public crate-xor-genkeys-0.1 (crate (name "xor-genkeys") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.24.2") (default-features #t) (kind 0)))) (hash "15bz70c6i8scxq50ib7w5307jw8d9fqx08s5v0w3x033q76klwk0")))

(define-public crate-xor-genkeys-0.2 (crate (name "xor-genkeys") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.24.2") (default-features #t) (kind 0)))) (hash "05aqc3x6djfzijjkgbg82sgfijn2dkphx9kq5wxivlf9ifxg89ck")))

(define-public crate-xor-genkeys-0.3 (crate (name "xor-genkeys") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.24.2") (default-features #t) (kind 0)) (crate-dep (name "xor-utils") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1j00mb2v0q0yf7yybgmp2ws6wdkpb7zw6pv1fbcz7977hj0f3krj")))

(define-public crate-xor-keysize-guess-1 (crate (name "xor-keysize-guess") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.24.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hamming") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1qdd01aybdhy1yg6yqjqv4lfddpilp47n1dxnc25576kmagbl4im")))

(define-public crate-xor-keysize-guess-1 (crate (name "xor-keysize-guess") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.24.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "xor-utils") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yfra27i821d1r8lf4zs1iwwlm4bn8lvyqbpnk8r0gi9b0g6xhbk")))

(define-public crate-xor-str-0.1 (crate (name "xor-str") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)) (crate-dep (name "xor-str-encode") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14r190g7q3222zc0z54xyza4zz5997x16bd42jdcbspi0snvq2m8")))

(define-public crate-xor-str-0.1 (crate (name "xor-str") (vers "0.1.1") (deps (list (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)) (crate-dep (name "xor-str-encode") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y2h54isbazw610qm8ck5j3w20ysfmwand4c7fi8rai84s92llw6")))

(define-public crate-xor-str-0.1 (crate (name "xor-str") (vers "0.1.2") (deps (list (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)) (crate-dep (name "xor-str-encode") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1k1shbm3cxw6h48dvmln3gzfq0f7lyirw7gv27dkpx6i275m4pvb")))

(define-public crate-xor-str-0.1 (crate (name "xor-str") (vers "0.1.3") (deps (list (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)) (crate-dep (name "xor-str-encode") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zdv87wcavlsilbzyk95w4w6qg9j6rawgh16ly86rhjvriapighn")))

(define-public crate-xor-str-0.1 (crate (name "xor-str") (vers "0.1.4") (deps (list (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)) (crate-dep (name "xor-str-encode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "004z29a7q8jsvl5p4b97njmvi8wxgac34z6qw1whlpk63c3wnfhl")))

(define-public crate-xor-str-encode-0.1 (crate (name "xor-str-encode") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)))) (hash "1nlihr8nlmpdqgp7kr6qdpvcdgwnwlswd97nzxm1vafifj76ddaj")))

(define-public crate-xor-str-encode-0.1 (crate (name "xor-str-encode") (vers "0.1.1") (deps (list (crate-dep (name "syn") (req "^2.0.64") (default-features #t) (kind 0)))) (hash "088zynzvz22dhb53r1zad0ilm0q6ppi2sh4mmdmazid4i6qqnqyr")))

(define-public crate-xor-utils-0.1 (crate (name "xor-utils") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1f9d174wn1ixwdkr9x3wdi46ir89n7f4y4rhmw481d4dr32771pl")))

(define-public crate-xor-utils-0.1 (crate (name "xor-utils") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1xxx85vwwjvpd6wrs0bf86lbvfmwhxfalj61nydyfyksj8nd9n6k")))

(define-public crate-xor-utils-0.1 (crate (name "xor-utils") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0ryng8fglmncvnbq6sm9jrdflzirsnkapdfs4asj1bz0fswvq5vp")))

(define-public crate-xor-utils-0.1 (crate (name "xor-utils") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0n6hfkq0ardqpdmcq489qld82wnyvxmlbv4616clj9z1bi4km0l2")))

(define-public crate-xor-utils-0.2 (crate (name "xor-utils") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0w12qb01zxacv8sgbxwybjciig9hppdr71vw6xky0kxy51jdiz7j")))

(define-public crate-xor-utils-0.3 (crate (name "xor-utils") (vers "0.3.0") (deps (list (crate-dep (name "hamming") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0sa8vhv9nzdcwijf36m82rhhxfl67sfid7h7kfx2z6n9m2347fxx")))

(define-public crate-xor-utils-0.4 (crate (name "xor-utils") (vers "0.4.0") (deps (list (crate-dep (name "hamming") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1nb131i67yjym24wwbz9r7b83hbsfs1bgh0qsbkd3fwds529z8w5")))

(define-public crate-xor-utils-0.4 (crate (name "xor-utils") (vers "0.4.1") (deps (list (crate-dep (name "hamming") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1wx6haijp88a6vb1br2asdcwd5pdwr1ffx5d8p8x6fhc0bxybwnc")))

(define-public crate-xor-utils-0.4 (crate (name "xor-utils") (vers "0.4.2") (deps (list (crate-dep (name "hamming") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1npf7b7063vqs8z8w42vi74akb5zim76hafwy94qxjv34r3414pl")))

(define-public crate-xor-utils-0.5 (crate (name "xor-utils") (vers "0.5.0") (deps (list (crate-dep (name "hamming") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1zcajk0cgarlgwcn643dmhrszd2rvbv5bx2gcdx9slxi03rinhpi")))

(define-public crate-xor-utils-0.6 (crate (name "xor-utils") (vers "0.6.0") (deps (list (crate-dep (name "hamming") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0br0p7ndsz3vpisjpcw64kxf71xk2niy21amm593xvyg469khg1w")))

