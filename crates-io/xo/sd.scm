(define-module (crates-io xo sd) #:use-module (crates-io))

(define-public crate-xosd-rs-0.1 (crate (name "xosd-rs") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "xosd-sys") (req "^2.2.14") (default-features #t) (kind 0)))) (hash "19065rlz4xbqxm2azxwvh7c6ixmvx46ny7xylzz7z6h2rxf6ljyq")))

(define-public crate-xosd-rs-0.2 (crate (name "xosd-rs") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "xosd-sys") (req "^2.2.14") (default-features #t) (kind 0)))) (hash "0slk03sjf9dk80kbqb24s5vxsjqg2jc6gx53xdps7dwzw7g84lhc")))

(define-public crate-xosd-sys-2 (crate (name "xosd-sys") (vers "2.2.14") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)))) (hash "07ha0gqd3qy4v6csycrip0smcqbscm00p7bz092c76gqna9v41bl") (links "xosd")))

