(define-module (crates-io xo -r) #:use-module (crates-io))

(define-public crate-xo-rs-0.1 (crate (name "xo-rs") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_graphics") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0sjl7mwy8n7fv3f7aair0gxdrd1xg8vwjz84vcm7qqiy35ynla39") (yanked #t)))

(define-public crate-xo-rs-0.2 (crate (name "xo-rs") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_graphics") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0zs8mx50f8pyb54jcjm7qpvb6hy8asgramkqbqdcdb9vy2p7fpn7") (yanked #t)))

(define-public crate-xo-rs-0.3 (crate (name "xo-rs") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_graphics") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "01vm7bq20llgix2mximgib0fxyc3fiwsc1cdw1i9682bmilgckd8") (yanked #t)))

(define-public crate-xo-rs-0.4 (crate (name "xo-rs") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1kmnd8hs08cnpicpjahycqhwcvdpf9hpm2l7r8s2wj4d42i4fcag") (yanked #t)))

(define-public crate-xo-rs-0.4 (crate (name "xo-rs") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1yb226cz63a7cg3bzqk9pb25f8xhqp0jjp18xsk7d6fqwbfslnca") (yanked #t)))

(define-public crate-xo-rs-0.5 (crate (name "xo-rs") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0ibahh3p34qx4ri6qr9p7pfb1d4z3c9jladv5pc6dp71b1nifjqz")))

(define-public crate-xo-rs-0.5 (crate (name "xo-rs") (vers "0.5.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1hdm0kfczwchsah2hgjc97w5sknj7ri3m95rbg0dxvvd9qhn2mw8")))

