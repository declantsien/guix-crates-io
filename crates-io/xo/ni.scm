(define-module (crates-io xo ni) #:use-module (crates-io))

(define-public crate-xonix-0.1 (crate (name "xonix") (vers "0.1.2") (deps (list (crate-dep (name "macroquad") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "macroquad-virtual-joystick") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1va6nmnsk6r13iycify5lhalh2aa6a03pb6lyj0i3306lfirfhw1")))

