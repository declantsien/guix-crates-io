(define-module (crates-io xo rs) #:use-module (crates-io))

(define-public crate-xorshift-0.1 (crate (name "xorshift") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1b31q3siyxv731y2bcw3583qjycrk31vr36hnvvk0452qr51za5r")))

(define-public crate-xorshift-0.1 (crate (name "xorshift") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "11vkawfbmdfmn3dqgqki42ymamfyx9pfcdydignjnpfzgdcnirbk")))

(define-public crate-xorshift-0.1 (crate (name "xorshift") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "029vm9p6w2qn6bcvdiwy66bxh4kl7cvd6r1ngfagn4mizy8zifwi")))

(define-public crate-xorshift-0.1 (crate (name "xorshift") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "1vd1xjnrli1qkx8s5i7gilq6rizx1nvwcg5jrbm0np6l9dal46fs")))

(define-public crate-xorshift128plus-0.1 (crate (name "xorshift128plus") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "1p0qv4dlrif7b783r8b662rj42bsm7fhhl16gpcmlz0f9yazwdzq")))

(define-public crate-xorshift128plus-0.1 (crate (name "xorshift128plus") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "0xxr7lqb0ygf2nxxahp5dza6fq3cidcr1frz9xkkax2fyil607bi")))

(define-public crate-xorshift128plus-rs-0.1 (crate (name "xorshift128plus-rs") (vers "0.1.0") (hash "0p35n5c5rry8gmpc8lrm42d0mb6b2clc9mdnx825xq3l04vc2jbx")))

(define-public crate-xorshift128plus-rs-0.1 (crate (name "xorshift128plus-rs") (vers "0.1.1") (hash "0ih5fhyzab3mcg2r521a4vvmvf7b8i6ny8hlqlvyw6zq2qq0ylyz")))

(define-public crate-xorshift128plus-rs-0.1 (crate (name "xorshift128plus-rs") (vers "0.1.2") (hash "0m10x9bx4742b0k16gqiphw6hqylflrziqddxw18mpi9yqvfbicb")))

(define-public crate-xorshift128plus-rs-0.1 (crate (name "xorshift128plus-rs") (vers "0.1.3") (hash "1ab4vhzb78j6rf1jlf7nxb9v44higsy2xby8i8iq6wffh3kfkav0")))

(define-public crate-xorshift128plus-rs-0.1 (crate (name "xorshift128plus-rs") (vers "0.1.4") (hash "18hkxsrxspf14j51v8g099y5lsgqdh3z58cwqs7pvmgz2kl2hlqa")))

(define-public crate-xorshift128plus-rs-0.1 (crate (name "xorshift128plus-rs") (vers "0.1.5") (hash "0jy61kdj83nxmkqjanwqcd2z6i02y4xdk7lvznzgqm3pciy7dply")))

(define-public crate-xorshift128plus-rs-0.1 (crate (name "xorshift128plus-rs") (vers "0.1.6") (hash "0cv6vvmkrhh6bxzv731vnwxqvmp3rsyd48fhmhp2k934x8gg7546")))

(define-public crate-xorstream-1 (crate (name "xorstream") (vers "1.0.0") (deps (list (crate-dep (name "base32") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.3") (features (quote ("paw"))) (optional #t) (default-features #t) (kind 0)))) (hash "01783fxz073p4r9w5mc4977rd6340zwawmh3g1cx1d81igj1rq5z") (features (quote (("default" "structopt" "paw" "base64" "hex" "base32"))))))

(define-public crate-xorstream-2 (crate (name "xorstream") (vers "2.0.0") (deps (list (crate-dep (name "async-std-stream") (req "^1") (features (quote ("unstable"))) (optional #t) (default-features #t) (kind 0) (package "async-std")) (crate-dep (name "async-std-test") (req "^1") (features (quote ("unstable" "attributes"))) (optional #t) (default-features #t) (kind 0) (package "async-std")) (crate-dep (name "base32") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (features (quote ("paw"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hq0n4f654ypgkx91hq3a25azlf7cz1s0qpqbj0j8svp1pajjbk8") (features (quote (("stream" "async-std-stream") ("std") ("default" "std") ("bin" "structopt" "paw" "base64" "hex" "base32")))) (yanked #t)))

(define-public crate-xorstream-2 (crate (name "xorstream") (vers "2.0.1") (deps (list (crate-dep (name "async-std-stream") (req "^1") (features (quote ("unstable"))) (optional #t) (default-features #t) (kind 0) (package "async-std")) (crate-dep (name "async-std-test") (req "^1") (features (quote ("unstable" "attributes"))) (optional #t) (default-features #t) (kind 0) (package "async-std")) (crate-dep (name "base32") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (features (quote ("paw"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wsnflmzyf0dy0s8prinsj3rga2y1zjdr2n0m26iqvlnca1gjqg9") (features (quote (("stream" "async-std-stream") ("std") ("default" "std") ("bin" "structopt" "paw" "base64" "hex" "base32"))))))

(define-public crate-xorstream-2 (crate (name "xorstream") (vers "2.0.2") (deps (list (crate-dep (name "async-std-stream") (req "^1") (features (quote ("unstable"))) (optional #t) (default-features #t) (kind 0) (package "async-std")) (crate-dep (name "async-std-test") (req "^1") (features (quote ("unstable" "attributes"))) (optional #t) (default-features #t) (kind 0) (package "async-std")) (crate-dep (name "base32") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (features (quote ("paw"))) (optional #t) (default-features #t) (kind 0)))) (hash "0d0p74k81m0h84fi0rc7y9p43qli2956lanvc55ssylygvjvr5an") (features (quote (("stream" "async-std-stream") ("std") ("default" "std") ("bin" "structopt" "paw" "base64" "hex" "base32"))))))

(define-public crate-xorstring-0.1 (crate (name "xorstring") (vers "0.1.0") (deps (list (crate-dep (name "xorstring-procmacro") (req "~0.1") (default-features #t) (kind 0)))) (hash "0c0hjdrv9scah6y3xadg3k5bnya1mv0dszzdf3md2jlpxsl4gz83")))

(define-public crate-xorstring-procmacro-0.1 (crate (name "xorstring-procmacro") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~0.15") (default-features #t) (kind 0)))) (hash "0zl7h6mq1xww9jlsr023kbjkpp19qqfnhnlkkx64adw2fsssljiq")))

(define-public crate-xorsum-0.0.0 (crate (name "xorsum") (vers "0.0.0") (hash "074034ywa3chx9jh6y3g57agb7m83r79ka1ml6hxjsfrmd7xx3jd")))

(define-public crate-xorsum-0.0.1 (crate (name "xorsum") (vers "0.0.1") (hash "1rj9v3f31pylbphv40dscrh8hjd2qbrmlscbgz8chdwdhpkh87rn")))

(define-public crate-xorsum-1 (crate (name "xorsum") (vers "1.0.0") (hash "1fq5ljyyygvgq1wyrwyp4nglp3k2ipa7485czrph19wbn0qrablp")))

(define-public crate-xorsum-2 (crate (name "xorsum") (vers "2.0.0") (hash "1irswwgil5fyy2b86xb9h8ljafds7ly0w4zxraqb7sj7fdq946g9")))

(define-public crate-xorsum-3 (crate (name "xorsum") (vers "3.0.0") (hash "1yhg8zvfw537j69r331sngf2ibj5p4cpki7izpg3malm2m8j36g8")))

(define-public crate-xorsum-3 (crate (name "xorsum") (vers "3.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0j723fcyrjnv6ldww0pdn570va3blb6lkivksj3ral8n2gk72dra")))

(define-public crate-xorsum-3 (crate (name "xorsum") (vers "3.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0a3zkz6dvv3cn6vcqi4bvz22wcx9r8drls20iaqi1j5z55hzdmmn")))

(define-public crate-xorsum-3 (crate (name "xorsum") (vers "3.1.3") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rb0m9n3sj9l1xqisxnh0mbkxjwikirxfxmy5fif27l69zky4a4p")))

(define-public crate-xorsum-3 (crate (name "xorsum") (vers "3.1.5") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1dj35lijnnbwsics35ihpwbbmchbgpd9ymq3h22k8xh178h4gvx4")))

(define-public crate-xorsum-3 (crate (name "xorsum") (vers "3.1.6") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06j2pxhxkr5v5yk4bs73x3rm0j9742lyy97mkqw2mnhgayz1338c")))

(define-public crate-xorsum-3 (crate (name "xorsum") (vers "3.1.7") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lzia6qrzhdnkb0ij83qmn3i71wscxb8sl77ns9zvxbz2sfnd6vj")))

(define-public crate-xorsum-4 (crate (name "xorsum") (vers "4.0.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xi3ysggq1cl6sbqzfif8nklc4yn1dbavcmc1vx7dvw5xki5a8rj")))

