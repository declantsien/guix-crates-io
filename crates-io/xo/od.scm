(define-module (crates-io xo od) #:use-module (crates-io))

(define-public crate-xoodoo-0.1 (crate (name "xoodoo") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.2") (default-features #t) (kind 0)))) (hash "01v1jbh6x2zpsq1ck2l57kj1vbizxxfaramnq35dxkx9p8ncdly1") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodoo-p-0.1 (crate (name "xoodoo-p") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0mz0f2dgk5jpf0cxrkfkskizmmfbbpkl3vkicfqmwpa6lggpkmy9") (yanked #t)))

(define-public crate-xoodyak-0.1 (crate (name "xoodyak") (vers "0.1.0") (deps (list (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1x1pis4w2h9ajvcr22w6dpsnf8wyvr2rc1yh6vsarmkmqy14yy9y") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.1 (crate (name "xoodyak") (vers "0.1.1") (deps (list (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0d4crxpj4145h7fpja4xdrjbsdcqgyjfixykdrb3vvl2nlzi4196") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.1 (crate (name "xoodyak") (vers "0.1.2") (deps (list (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ry70jng4pp17yll8i1l8z3jpb1hsrvqqfbvjs698wyf7fyrcdl7") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.2 (crate (name "xoodyak") (vers "0.2.0") (deps (list (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0a46ix8gj423n5ijsnnh1dr2v4hxnici151pnnf03y0f9b7qfrhw") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.0") (deps (list (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ibxqif8mvr0lvpf20ph3gnhbwvlc0nzkxdp11j0278xjsnnyvhm") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.1") (deps (list (crate-dep (name "rawbytes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0z770758d1bndq6j25vlzs04fg7vv096caa5365970jkm63nlqbr") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.2") (deps (list (crate-dep (name "rawbytes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0msxwbd2w6gd5qhzmw5a8j5cjvvycaz2h7yxb45grys02q9hz75s") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.3") (deps (list (crate-dep (name "rawbytes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1cjigk7ydnrdhg5awb7aqxl5wsd8176rvfhx9m5w3izfnmlh63z5") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.4") (deps (list (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "06cvrrvh2n7rq5s4yid1lzqrrvbjl6arg5wbw0bll713lh8hq4mi") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.5") (deps (list (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0wgwymxwq2l5g4nmyqhqwdyzb1c2lllzihv6m8yhxdwnmjsc0njl") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.6") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0s2mh71zg12l4fnslckzqbi6w8bzc4kj0zbhzz0zmqfh85nmvkx5") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.3 (crate (name "xoodyak") (vers "0.3.7") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0298wj1axm9qgxxk96cnvlnpyc3dj2f397xmf6arxfp21qgavjgl") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.4 (crate (name "xoodyak") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0j5ln3qlxvyids3qcksq2hch8slpfry4j2h12la3hy8fmzqi5141") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.4 (crate (name "xoodyak") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1fd7ihp30lsiw1543f1m1w1pdy59qywqxj527cq0br70zbsrkq33") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.4 (crate (name "xoodyak") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "05hjapd0kxj88jxf8p5r81gg11jhwbfhx5icqrmb51kjkph9fk97") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.5 (crate (name "xoodyak") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0bdilj2qm9wid9x1va132x4k9lwvj6jyxzjqkk0kv2bvfxyrmpr2") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.5 (crate (name "xoodyak") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "17ksfv9bpyqrka2fgphgw7z6pzk3dx13sv6l6cdqgp2l0azw9bkz") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7 (crate (name "xoodyak") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req ">=0.3.0, <0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req ">=1.1.0, <2.0.0") (default-features #t) (kind 0)))) (hash "10d4fj2g2vkxkkqdvbmd8fbkalzlhk9gg6c8hpn6qdrsbih8b74g") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7 (crate (name "xoodyak") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.2") (default-features #t) (kind 0)))) (hash "12nvq6v9ssyhw5xxx78v14mmwyzq0ihzwl0n59dvza8hs4jlykh9") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7 (crate (name "xoodyak") (vers "0.7.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.3") (default-features #t) (kind 0)))) (hash "0h5n0sswyi7mqnqhh8hgcxfnafanfizli7b8cyrh7ky9yhzb8gjw") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.7 (crate (name "xoodyak") (vers "0.7.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.4") (default-features #t) (kind 0)))) (hash "1vgfnpxbsarv78z81q41s0l2znqh2spq6cz3spf64j6ap6xjvdcl") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8 (crate (name "xoodyak") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (default-features #t) (kind 0)))) (hash "1dhwxjridyzpy3vn25ildimdcaylmwf4rwcnhfshds8khd3qibqy") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8 (crate (name "xoodyak") (vers "0.8.1") (deps (list (crate-dep (name "benchmark-simple") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (default-features #t) (kind 0)))) (hash "0wvr1wp31849npvkg3bspdl3bs9i9v77sbxq1f9l0z8pd0jpxigx") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8 (crate (name "xoodyak") (vers "0.8.2") (deps (list (crate-dep (name "benchmark-simple") (req "^0.1.7") (default-features #t) (kind 2)) (crate-dep (name "rawbytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6") (default-features #t) (kind 0)))) (hash "0xal85dj1lmc7kh2lcv4msd3xvgh0v7bdngmqa18y3rk826yinc2") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8 (crate (name "xoodyak") (vers "0.8.3") (deps (list (crate-dep (name "benchmark-simple") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "bytemuck") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6") (default-features #t) (kind 0)))) (hash "1q515cjyapgdhvl2bdri68zzdd07mxcr9122k78nvfrs7267ki49") (features (quote (("std") ("default" "std"))))))

(define-public crate-xoodyak-0.8 (crate (name "xoodyak") (vers "0.8.4") (deps (list (crate-dep (name "benchmark-simple") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "zeroize") (req "^1.6") (default-features #t) (kind 0)))) (hash "0szi326zgf66asmy2pmq1ni99syjdy3m2pqm8603kmaq40dpdb8d") (features (quote (("std") ("default" "std"))))))

