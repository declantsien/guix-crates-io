(define-module (crates-io xo sh) #:use-module (crates-io))

(define-public crate-xoshiro-0.0.1 (crate (name "xoshiro") (vers "0.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "039dfwwf2knmyvq94w2zabvd2fdzcabm578pya62cnrnshpyhhhw")))

(define-public crate-xoshiro-0.0.2 (crate (name "xoshiro") (vers "0.0.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "0d8nggam8d33kv81kydsbhscxpayjw4y6wcp5r1nqi5vnh57skxa")))

(define-public crate-xoshiro-0.0.3 (crate (name "xoshiro") (vers "0.0.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "01izwznmasar87kglpqna9knk926qwkgyxq95f7cv70nsxj78v9k")))

(define-public crate-xoshiro-0.0.4 (crate (name "xoshiro") (vers "0.0.4") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qnyr90h6r6w6cdg6arjgig37sxyr20adg6ahza4ymcqi68l1rxl")))

(define-public crate-xoshiro-0.0.5 (crate (name "xoshiro") (vers "0.0.5") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wwh4kxrsqxpzpd7y9c3pi9w0s9pp7qpsv4l265705kd4d12ic4q")))

