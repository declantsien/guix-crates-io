(define-module (crates-io xo ro) #:use-module (crates-io))

(define-public crate-xoroshiro-0.0.1 (crate (name "xoroshiro") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vsmi6b9fbw66svq0pxp73n4fmyq7fb2yz6vbl2pyv2c8mrqy23v")))

(define-public crate-xoroshiro-0.1 (crate (name "xoroshiro") (vers "0.1.0") (deps (list (crate-dep (name "aesni") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1l511c1bdfrxh92qmim2kwdpjcxgcbdbl26xcq8l98fzmblryv0w")))

(define-public crate-xoroshiro-0.2 (crate (name "xoroshiro") (vers "0.2.0") (deps (list (crate-dep (name "aesni") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hh1zs7a324qixid715fkzydag0gg71klzrs0m5r7wsr3c2fnfqv") (features (quote (("unstable" "aesni") ("default"))))))

(define-public crate-xoroshiro-0.3 (crate (name "xoroshiro") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qprykl0l9x5andaddyyn2j6v1n2vlnm90x1kc9gwkz2fyag1blf")))

(define-public crate-xoroshiro128-0.1 (crate (name "xoroshiro128") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "00dg9vdprvk8fg38adk5dvcbi20kfg3rv0kyqjpp606kwdx429l4")))

(define-public crate-xoroshiro128-0.2 (crate (name "xoroshiro128") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0yblkf84a9lzdddnmxxkimhskjm51c3da6kwq9jqjhgq49mb9dpf")))

(define-public crate-xoroshiro128-0.3 (crate (name "xoroshiro128") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kfzanis2v39r8q963q161izv0hmfsdxa11cxgqw8jgcp8sdmvp0")))

(define-public crate-xoroshiro128-0.4 (crate (name "xoroshiro128") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "0dd0qjdymxhzdgkxqn1w9vvmbxhjqsjx5lavc14vxz47v482v024")))

(define-public crate-xoroshiro128-0.0.0 (crate (name "xoroshiro128") (vers "0.0.0") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "1fxydvs4r7y9dzd5bwci6n23crkgxp91h644cyvkna8w71jpsasj") (features (quote (("std")))) (yanked #t)))

(define-public crate-xoroshiro128-0.5 (crate (name "xoroshiro128") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)))) (hash "0zll2cy4lw3h4hv17s9si5fina3zf2724w8f774011d9943hmw5l") (features (quote (("std"))))))

