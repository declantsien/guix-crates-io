(define-module (crates-io xo ps) #:use-module (crates-io))

(define-public crate-xops-0.1 (crate (name "xops") (vers "0.1.0") (deps (list (crate-dep (name "xops_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bc3lzbr3p3s571bx2v280j5kld82za9gwmbcgqpikl77szzrnsd")))

(define-public crate-xops-0.1 (crate (name "xops") (vers "0.1.1") (deps (list (crate-dep (name "xops_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0wk6zm4j28dix7l453jh3qvc23mhcfjp2mjlrhs9ha9sldk448vi")))

(define-public crate-xops_core-0.1 (crate (name "xops_core") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0zk9w3hlxxj8k6d9zann0wv2c73nkxlcaxv4v5jlydi1dggyry3r")))

(define-public crate-xops_core-0.1 (crate (name "xops_core") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1h0l2g3fm2iagqlcmd7sv539vcyzi9gnm4sy1lw4cbn7pczhx41d")))

(define-public crate-xops_macros-0.1 (crate (name "xops_macros") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "xops_core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03934rpkd0szbfrvnzn99wq6kkc5jivh84x1dsj92ifksk6b2fd9")))

(define-public crate-xops_macros-0.1 (crate (name "xops_macros") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "xops_core") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1fg1nkx2slrp60f4vyyi9d8cilhbrr4g2ksb4dlvn097pd11nqwz")))

