(define-module (crates-io lk _m) #:use-module (crates-io))

(define-public crate-lk_math-0.1 (crate (name "lk_math") (vers "0.1.0") (hash "1k3fflz5l608sspi1fwl69l34w8dkj84czjkrf44irsc7si6cmn0")))

(define-public crate-lk_math-0.2 (crate (name "lk_math") (vers "0.2.0") (hash "1dqvrbxp1gwqvz9p3r6mclm10z0y9ibp9lab0mh8lqx52l5kvmnc")))

(define-public crate-lk_math-0.2 (crate (name "lk_math") (vers "0.2.1") (hash "10r6dbvg2is7f42fr3bnmqcvqh40rr43i3spq51fjnpka9cbbjq8")))

(define-public crate-lk_math-0.3 (crate (name "lk_math") (vers "0.3.0") (hash "0a1gxd0jchbnhpmpqaj1g8iivvbrb6l221slabrcwza8cz9ynwm1")))

(define-public crate-lk_math-0.3 (crate (name "lk_math") (vers "0.3.1") (hash "1zmf89jnw0666q2fcaxnqynxv4cbp76dcp1pyf2fk33ksmh049s7")))

(define-public crate-lk_math-0.3 (crate (name "lk_math") (vers "0.3.2") (hash "0zzi28wqlpca29ys3dvnrs31ffhwf0ygbwf4qbndxrcypwadnyc3")))

(define-public crate-lk_math-0.4 (crate (name "lk_math") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0xj9w3rnjixi52rp5bwnjh3krzy1s8gzhcp3p5mhx1ffpcgxplar") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_arrays"))))))

