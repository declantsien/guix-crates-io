(define-module (crates-io rj in) #:use-module (crates-io))

(define-public crate-rjini-0.0.1 (crate (name "rjini") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "1k0cpgzlwvdh6pqk8dlv2idhjhm3jlkzddb06sqf3c8x9fd4rxcb")))

(define-public crate-rjini-0.0.2 (crate (name "rjini") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "05r3czqx50jwz08r51xcrz7y8sjxrwibh2jj2q33igk6si0jvbw9")))

(define-public crate-rjini-0.0.3 (crate (name "rjini") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0zcl0sirpqjqvdcd9r5f1dqwalaad09kar09dpq18lmi1bv9z1p6")))

