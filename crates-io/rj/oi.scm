(define-module (crates-io rj oi) #:use-module (crates-io))

(define-public crate-rjoin-0.1 (crate (name "rjoin") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.25.1") (default-features #t) (kind 0)) (crate-dep (name "csv-core") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1gxm91yhxg3i41vcmw3ciivw8nf5w6y78qrr0ssmdjjij9kncvz7")))

(define-public crate-rjoin-0.2 (crate (name "rjoin") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.25.1") (default-features #t) (kind 0)) (crate-dep (name "csvroll") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rollbuf") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kvz6n08glmqxcl11ix1q7p2s3wjy6mfln0p0bbbixhymrw9zxnh")))

