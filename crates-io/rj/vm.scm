(define-module (crates-io rj vm) #:use-module (crates-io))

(define-public crate-rjvm-0.1 (crate (name "rjvm") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)))) (hash "0l2hh6zf6np4qh1d0hczdx6aj0fn32pig7819s5fghrmp383kz08") (features (quote (("default" "decoder") ("decoder"))))))

(define-public crate-rjvm-0.2 (crate (name "rjvm") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)))) (hash "13278d039bbijnnbnh4kmnp5mjvxfqpyxhrmrynfxcv35qv6dxk4") (features (quote (("default" "decoder") ("decoder"))))))

(define-public crate-rjvm-0.3 (crate (name "rjvm") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "05xid62fry2r60179ajpkhcngf6q4a8x21xqia4riksq39bn8wa3") (features (quote (("default" "decoder") ("decoder"))))))

