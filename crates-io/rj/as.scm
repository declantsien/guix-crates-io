(define-module (crates-io rj as) #:use-module (crates-io))

(define-public crate-rjasmr-0.1 (crate (name "rjasmr") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "tokio1"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0phrgd0pyf3c27bd2zsix5vn9c16h03xpkg4gzbiqd9d6391lwra")))

(define-public crate-rjasmr-0.1 (crate (name "rjasmr") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "tokio1"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1vqa4iswh9m48csfbggmlmxayxr1wjpq91cxlijam5rmqwgrfi50")))

