(define-module (crates-io zy x-) #:use-module (crates-io))

(define-public crate-zyx-compiler-0.1 (crate (name "zyx-compiler") (vers "0.1.0") (deps (list (crate-dep (name "zyx-core") (req "^0.1.0") (kind 0)))) (hash "0by7ndzznb1mxhfjmvy9rl1sy7m633l98mqkkl9s864jh1cj8lis") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-compiler-0.1 (crate (name "zyx-compiler") (vers "0.1.1") (deps (list (crate-dep (name "zyx-core") (req "^0.1.1") (kind 0)))) (hash "06lmw96fnbsl9j6ix8xlxg70mf1wsm34ficf6xynf8pf9s2ma6rk") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-core-0.1 (crate (name "zyx-core") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (kind 0)))) (hash "1fxcwc0vvnyd6sw80x89681ladngmyhynbvpl50yk5avvg6xqsyw") (features (quote (("std") ("default" "std"))))))

(define-public crate-zyx-core-0.1 (crate (name "zyx-core") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (kind 0)))) (hash "04simpa76b9mlpqhq10crgq1l9fzfg78f68jc44288fcvb15xg73") (features (quote (("std") ("default" "std"))))))

(define-public crate-zyx-cpu-0.1 (crate (name "zyx-cpu") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.8.1") (optional #t) (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.0") (kind 0)))) (hash "04bc1yl6fqdrm94xqdf7m461mzrz86xa5xca6acb3nyv9dkn2cmd") (features (quote (("std" "zyx-core/std" "rayon") ("default" "std"))))))

(define-public crate-zyx-cpu-0.1 (crate (name "zyx-cpu") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.8.1") (optional #t) (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.1") (kind 0)))) (hash "1c8nlzs2f1v90kvjwbr9vjnibrnmybmgl0x22kbyykhcsa1dqgf1") (features (quote (("std" "zyx-core/std" "rayon") ("default" "std"))))))

(define-public crate-zyx-derive-0.1 (crate (name "zyx-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.49") (features (quote ("full" "fold"))) (default-features #t) (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ihd69zc3qrhldfv0iv0v64p0hbrm6iv1mz9f860fmybx19n50m7") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-derive-0.1 (crate (name "zyx-derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.49") (features (quote ("full" "fold"))) (default-features #t) (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.1") (kind 0)))) (hash "1v26jgdrchfpjk09zn3rk1y57py70bxpjapwv6zgiqd3fm17rcld") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-nn-0.1 (crate (name "zyx-nn") (vers "0.1.0") (deps (list (crate-dep (name "zyx-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kchq8awvyvw824lyabmv2qc919v6gi8lxlfmahcq8ynwbaancns") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-nn-0.1 (crate (name "zyx-nn") (vers "0.1.1") (deps (list (crate-dep (name "zyx-core") (req "^0.1.1") (kind 0)))) (hash "06c0pp0fvs1vbqviq4qyirlp02577fvv1girxg0x0q9j8m4i7i9j") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-opencl-0.1 (crate (name "zyx-opencl") (vers "0.1.0") (deps (list (crate-dep (name "opencl-sys") (req "^0.2.8") (kind 0)) (crate-dep (name "zyx-compiler") (req "^0.1.0") (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.0") (kind 0)))) (hash "1rk60pw655173j91a8qij9xdz93igxg94723x279in7r0j2j1qyv") (features (quote (("std" "zyx-core/std" "zyx-compiler/std") ("default" "std" "CL_VERSION_1_1" "CL_VERSION_1_2") ("debug1") ("CL_VERSION_2_1" "opencl-sys/CL_VERSION_2_1") ("CL_VERSION_1_2" "opencl-sys/CL_VERSION_1_2") ("CL_VERSION_1_1" "opencl-sys/CL_VERSION_1_1"))))))

(define-public crate-zyx-opencl-0.1 (crate (name "zyx-opencl") (vers "0.1.1") (deps (list (crate-dep (name "opencl-sys") (req "^0.2.8") (kind 0)) (crate-dep (name "zyx-compiler") (req "^0.1.1") (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.1") (kind 0)))) (hash "0wkyaqzsmazrvm3255dkwypfwk2p2yps9h6gywll4jgf3can8qpn") (features (quote (("std" "zyx-core/std" "zyx-compiler/std") ("default" "std" "CL_VERSION_1_1" "CL_VERSION_1_2") ("debug1") ("CL_VERSION_2_1" "opencl-sys/CL_VERSION_2_1") ("CL_VERSION_1_2" "opencl-sys/CL_VERSION_1_2") ("CL_VERSION_1_1" "opencl-sys/CL_VERSION_1_1"))))))

(define-public crate-zyx-optim-0.1 (crate (name "zyx-optim") (vers "0.1.0") (deps (list (crate-dep (name "zyx-core") (req "^0.1.0") (kind 0)))) (hash "102v68v0pfyv64ipr4zm4ipsbzv15zvs41v9ns953ncxk87zj5k2") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-optim-0.1 (crate (name "zyx-optim") (vers "0.1.1") (deps (list (crate-dep (name "zyx-core") (req "^0.1.1") (kind 0)))) (hash "1r6946ic5daqcxzc862vmca31izsg9f1s5dxp5423337whckk1hq") (features (quote (("std" "zyx-core/std") ("default" "std"))))))

(define-public crate-zyx-torch-0.1 (crate (name "zyx-torch") (vers "0.1.0") (deps (list (crate-dep (name "tch") (req "^0.15.0") (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.0") (kind 0)))) (hash "1jpmnj63qraxdwyyym63hrwpzgsxqfv8j59v9f58d613xhlysw0a") (features (quote (("std" "zyx-core/std") ("download-libtorch" "tch/download-libtorch") ("default" "std"))))))

(define-public crate-zyx-torch-0.1 (crate (name "zyx-torch") (vers "0.1.1") (deps (list (crate-dep (name "tch") (req "^0.15.0") (kind 0)) (crate-dep (name "zyx-core") (req "^0.1.1") (kind 0)))) (hash "05z1ai55rd72qgc7mf1297bwr763md17cdgqqi1chlncgx13zljj") (features (quote (("std" "zyx-core/std") ("download-libtorch" "tch/download-libtorch") ("default" "std"))))))

