(define-module (crates-io zy po) #:use-module (crates-io))

(define-public crate-zypo-lib-0.0.0 (crate (name "zypo-lib") (vers "0.0.0") (deps (list (crate-dep (name "lalrpop") (req "^0.17.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "17zbhz49ny01xfh9dr88l01h9dsic8k03vh4m6y4n5kr6n759g0l")))

(define-public crate-zypo-lib-0.0.1 (crate (name "zypo-lib") (vers "0.0.1") (deps (list (crate-dep (name "lalrpop") (req "^0.17.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "09qrx3v1m1275mprjpl9s26v4x6vi3h1w6qrz39pzg4vx77vyy5s")))

(define-public crate-zypo-lib-0.0.2 (crate (name "zypo-lib") (vers "0.0.2") (deps (list (crate-dep (name "lalrpop") (req "^0.17.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1pzigxssf7sjynj02hh6d26xhar9px6dnw2gkbrrj5ad4156qd7z")))

(define-public crate-zypo-parser-0.0.1 (crate (name "zypo-parser") (vers "0.0.1") (deps (list (crate-dep (name "lalrpop") (req "^0.17.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "12bhf26pyd2vf1l734y51q2f2p60l6hgcm0gqzn0c9jchirhcjph")))

(define-public crate-zypo-rs-0.0.0 (crate (name "zypo-rs") (vers "0.0.0") (deps (list (crate-dep (name "lalrpop") (req "^0.17.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "09ykr9321amllnhk1hm088jpcdyb4c8nw27a8i9nv2dqnsw4293r")))

(define-public crate-zypo-rs-0.0.1 (crate (name "zypo-rs") (vers "0.0.1") (deps (list (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "zypo-parser") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0rz85xmb5s5ihaf32hlicf760shbm8k23xlviifch8zwcpvwf7j3")))

(define-public crate-zypo-rs-0.0.2 (crate (name "zypo-rs") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.17.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (kind 0)))) (hash "12scri2jz1ckjs3b9pkqwy284g8x11qb30q8sbf2gfs5hbhlrp22")))

(define-public crate-zypo-rs-0.0.3 (crate (name "zypo-rs") (vers "0.0.3") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.17.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (kind 0)))) (hash "1niyvwxksawjclm7qvpzx37caxzdw7wjyzmpbq82nlzs23i1vdbw")))

