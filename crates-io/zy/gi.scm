(define-module (crates-io zy gi) #:use-module (crates-io))

(define-public crate-zygisk-v4-0.0.0 (crate (name "zygisk-v4") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)))) (hash "0zpk3xmg5zmwykf06anav4i1jpizcpj5yzr67jy2dpq83sazgglk")))

