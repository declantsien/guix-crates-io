(define-module (crates-io zy re) #:use-module (crates-io))

(define-public crate-zyre-0.2 (crate (name "zyre") (vers "0.2.0") (deps (list (crate-dep (name "zyre-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qnrd8d8xkwzh4lirrrxay0669d47rjk1nfwhwkf0mypihdh2b6w")))

(define-public crate-zyre-sys-0.1 (crate (name "zyre-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.20.0") (default-features #t) (kind 1)))) (hash "0kz936ygssg3jnqafcll9b3rism40xd2fm1jgk3c9mn9y1f7l02h")))

