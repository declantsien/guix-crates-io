(define-module (crates-io sa zp) #:use-module (crates-io))

(define-public crate-sazparser-0.1 (crate (name "sazparser") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "0nx80knfg9iw3vm6ichpmii1yjmyf1cihcg5c4sfl7ymgfzwn44w")))

(define-public crate-sazparser-0.1 (crate (name "sazparser") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ppyfwqj6c573rl7d51w48ablylddrpl7l7mjcnh3na3bawdq6yl")))

