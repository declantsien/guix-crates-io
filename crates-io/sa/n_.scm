(define-module (crates-io sa n_) #:use-module (crates-io))

(define-public crate-san_wrapper-0.1 (crate (name "san_wrapper") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.19.8") (default-features #t) (kind 0)))) (hash "0fh0jz5xsafmzahfrpggf0f6cv40m1i5vwqrw692rj5imnfjq2kl")))

