(define-module (crates-io sa dn) #:use-module (crates-io))

(define-public crate-sadness-generator-0.1 (crate (name "sadness-generator") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cmdl1c2h1majax8n9z4wzfrh7ldl3crlvmzcghpz4s3gv1qd6lz") (rust-version "1.59.0")))

(define-public crate-sadness-generator-0.3 (crate (name "sadness-generator") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rhr3rjrdg6rbnkyglz65gynk67k7wi5rb5n3qh2qiaa8ipzmzsq") (rust-version "1.59.0")))

(define-public crate-sadness-generator-0.3 (crate (name "sadness-generator") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bmbkk2xyjb4a5dh38r12kdxhvdiskn5si0wk993jzj36kghiw4g") (rust-version "1.59.0")))

(define-public crate-sadness-generator-0.4 (crate (name "sadness-generator") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "173gm9gjlgi2x26ibx88q30w0nary5lgbn5yp519swqldw0rhx3w") (rust-version "1.59.0")))

(define-public crate-sadness-generator-0.4 (crate (name "sadness-generator") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fnn6ffpg00yhajfd3i970gk3gv06dmzi5jpscf4vkj8bnl81c40") (rust-version "1.59.0")))

(define-public crate-sadness-generator-0.4 (crate (name "sadness-generator") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0az7zhbgmi13zjzgiapmvrxpn2lbhbk52dkkxgrl2ni2ngh79gsl") (rust-version "1.59.0")))

(define-public crate-sadness-generator-0.5 (crate (name "sadness-generator") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mwj8m4n60596058ipmw4gqxvjb0wwcv8r6q69fvnknisppgbnjw") (rust-version "1.59.0")))

(define-public crate-sadnessOjisan_hello-0.0.1 (crate (name "sadnessOjisan_hello") (vers "0.0.1") (hash "1r65msnf66z3x6ni6i6x2is33ywjq1iczzm1fm2z1b5snl4is3qh")))

