(define-module (crates-io sa i_) #:use-module (crates-io))

(define-public crate-sai_component_derive-0.1 (crate (name "sai_component_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0hjl4j7qnndnin8ikg6d9c5ib6cr94g4bcmcfihvy3l8avalc08d")))

(define-public crate-sai_component_derive-0.1 (crate (name "sai_component_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1f0vvbpn33mn027kj00h48j3xsw03i91rv234ng22izrz127baif")))

(define-public crate-sai_component_derive-0.1 (crate (name "sai_component_derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1klkfp6b4p1y5js92aw9q48cvc81izfrqai4h0z4i0dxcb3asqpa")))

(define-public crate-sai_component_derive-0.1 (crate (name "sai_component_derive") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "17b8k60w9via372lc3i6ibbgydp7z6jw23cpfk5cawmndk694r4y")))

(define-public crate-sai_component_derive-0.1 (crate (name "sai_component_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1c2w8wax3b384mlj6rac0dbnf53ms3dn0zn744afplhwky8x1vww")))

