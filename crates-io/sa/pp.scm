(define-module (crates-io sa pp) #:use-module (crates-io))

(define-public crate-sapp-0.1 (crate (name "sapp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "043w2j5clz945b0yam13dcps99izvy43sn9l0bgk7dsav9ph338x")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0svgmdjmrks5m863fgsaa53jrl3gm21zyjznzzqhki6380bixl06")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w5w3nlkk180n27qf0v7gzk90h60jh3mpvpih27c7lwmg727lm6m")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0ipnagfxdbg6ibln7684anqyr4bkbn1ggiqcnnzglsj0h73l1b3p")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "03vg936js69jcyl94sdmilmbka61p0vym1z1nwy5wm6mfwgkgf5p")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1xqr70hvmh1w6a62xdiy76wryc1f11p3ghzfqaamjb487w3x8iw0")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "14rirnvzqpfaj79dbzxj99fgcprma92mbsiik69bl3a5z4w35z1c")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1yc49x6zq02hakqsvh9076r2sry5bd1bq1v2y6qzw6hzpmiw6myb")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0wnnmhzpm4vq6iiwsg9zbv6ifa0bfkhkpfym47myqgglniwqha1d")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "090p7v059j5mq50cj6nsjhpxz2hkm9h8alkq74hq69xscbs82jjs")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1v07632rzawq6a05ivyzdy8di99q275q0f0c60njsrxhx7x8gmlj")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.10") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0h09dsp0czg6l2mcl5if9nrfmjv9bl29rb4kyqrwa09carphw3cw")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0794sfvlyq2hn4dfwphlqhd9jfgndm983k3qqhpqpbaf2avv47mj") (yanked #t)))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wkh7sywq4gsc2w0a3iy5bsjp8xfc1hld0ahrksqw0i2pn76gynb") (yanked #t)))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "06kina1x35l1xmdj12xriwcf2z1fv8hw747rf8djkw1z8l9lkjh5") (yanked #t)))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "17vainj9m6gws13mzmm38s7dpirmpa7kmjjw1r58ff7wv344vi97")))

(define-public crate-sapp-android-0.1 (crate (name "sapp-android") (vers "0.1.15") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndk-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0apkcxpz4hnjrgx3y20pwhhjbg62qp0dwi1cbxvh6ds50p75zscm")))

(define-public crate-sapp-cli-0.1 (crate (name "sapp-cli") (vers "0.1.0") (deps (list (crate-dep (name "sapp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zm3fhcblajjgx821jy0apzg8zrj446zfwjds4jsymflhwbss2ki")))

(define-public crate-sapp-console-log-0.1 (crate (name "sapp-console-log") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sapp-wasm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "11ws6q6qv7x1z2npfzam5irwnj31g5viiph6017can1z168r0a5c")))

(define-public crate-sapp-console-log-0.1 (crate (name "sapp-console-log") (vers "0.1.8") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sapp-wasm") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0ndmi2c2flqzdxv6rd08yn69k7drf9dis65rdqz7ay8cf8h3c37i")))

(define-public crate-sapp-console-log-0.1 (crate (name "sapp-console-log") (vers "0.1.9") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sapp-wasm") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1s8yx0hzqbkbq4i643lnxg6bcgrhnhhv98v1ili24q19z4nxhgsv")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ic4kkrq9jg71v7fmqs3mybxywj90fk5i11lwl9pg4r7nbdn03i5")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "000jkkdlkic5kr0w4jrq4lk9mrdps8crav83ga2jm0rfva0v361g")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wybzyr1ryr5w7g9vcr3cha15mp5wq6rmbcj31vdw0lk6x57c7h1")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "05gxiyiw1n87i0mfmk6gqbrdlypksihk2hb9rcpd6zl0xkzmxsl7")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "03ffvcd3x7xaspkagzilkp3yvqdfph7fn4b2ycvxw7pma9wqzmfx")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "061vvkqnj8ad20b7avab2kxpwc0zd9ji42mrsqm6q8rp56i3h3fg")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0caz2hdaygxyxlnn9fxg579qysxsdx6ykkgi2fm6hx1hbx2f4403")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "02aq1b1fc85l589kjskx5h43h93h201fw343p8ms9n9mvikli1zx")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.8") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0x732hc99gmgc3y19nl39mk3f2y2ai6zlxidz9737g46yilj6a7i")))

(define-public crate-sapp-darwin-0.1 (crate (name "sapp-darwin") (vers "0.1.9") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0pcrx03z9ks8krx7bwgwrwpq9adgplha3cb0wxfij5ys8gnkmkn1")))

(define-public crate-sapp-dummy-0.1 (crate (name "sapp-dummy") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x3188b4i3svz6wcfaj47jl7ngs3gd49caw9qh08hlzai2dann9k")))

(define-public crate-sapp-dummy-0.1 (crate (name "sapp-dummy") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1c6zvm5qf4smfvc8rj1k0dr1ilaxvclnv2paj25mp2xd07d77rvg")))

(define-public crate-sapp-dummy-0.1 (crate (name "sapp-dummy") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mng22mlaczh3pf45y2qz2ajxq1vv01ivy6nz2k5mi16c2bvmz4k")))

(define-public crate-sapp-dummy-0.1 (crate (name "sapp-dummy") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05zp28g3j6n46bsxckz99yxkvd63qpzvc2xg0jvb88699yq8ch44")))

(define-public crate-sapp-dummy-0.1 (crate (name "sapp-dummy") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "17cr4aaqa7f6bxf1khmfc7kvh4jqbm5nm1yggvscwq5jxdybfmx3")))

(define-public crate-sapp-dummy-0.1 (crate (name "sapp-dummy") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y542m92hmzd3l8cfam8k26jn04imawnvii7rawq5imnllkavwb6")))

(define-public crate-sapp-ios-0.1 (crate (name "sapp-ios") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "144h737qby3hcyl2kg4harxa2mys22lasw7fm8gqjqfflj5lr65h")))

(define-public crate-sapp-ios-0.1 (crate (name "sapp-ios") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1agazy859s4gq3iznr3qxgxx9q9rapjyf8273xlnl9360lrvwwyx")))

(define-public crate-sapp-ios-0.1 (crate (name "sapp-ios") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1adl50q9vrp7bhh9gj63bw39yhrvlmaaidkri69jxb69c596w7h8")))

(define-public crate-sapp-jsutils-0.1 (crate (name "sapp-jsutils") (vers "0.1.0") (hash "0cx0656mzm1x6dsji654960w2dcvxgc37n1cknn4hy926jprq83d")))

(define-public crate-sapp-jsutils-0.1 (crate (name "sapp-jsutils") (vers "0.1.1") (hash "1d9nv1fgz09nfjmbyyk77vkzh5mc0wv5qjf5mvr9imababk5h69g")))

(define-public crate-sapp-jsutils-0.1 (crate (name "sapp-jsutils") (vers "0.1.2") (hash "11n54vq5xljpyvsls0h7wkc2rf3gvmwds2kainfgf25sshqadhzp")))

(define-public crate-sapp-jsutils-0.1 (crate (name "sapp-jsutils") (vers "0.1.3") (hash "02i8xh3vrramgkb71wbn3277qs6b0d562sqfmwzpfn5kz3h7cdrq")))

(define-public crate-sapp-jsutils-0.1 (crate (name "sapp-jsutils") (vers "0.1.4") (hash "0r8dvkkkjf7bri1li6xlzz99cs31mhj7xh4qzdk213hz6n557dlm")))

(define-public crate-sapp-jsutils-0.1 (crate (name "sapp-jsutils") (vers "0.1.5") (hash "1m11pd33k3mshf489japwcfmsyz7wyzli0hp19nglcblhsxbm2mv")))

(define-public crate-sapp-kms-0.1 (crate (name "sapp-kms") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k63fj30r7zzsjgqfj882x1idf696aklqbc0nk674m47a6dbilap")))

(define-public crate-sapp-kms-0.1 (crate (name "sapp-kms") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qx2y1aknrsn6c3q1h4b5ffg8jyvpa1pjn8v95ai9xyaql5w65pk")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kkqp23zh3n4l3xl1m4jk4l6jc9bx5by4aspxfliq0h5hyp5v6ph")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gf151rbr39i8x13kw0ly76mqw70yq8ncwxhh9mx4n3f2hkg22v2")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yivb05zd1mb8y4q6nzcffzsb3mwagvrcbklbf8vvz54gqm4yi2c")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fbsin3qbdb9nh9k1vb5d4kwjyv3cp73bx047h14xn48a6rv0iwa")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "168n577zhw6d10j2a34d6018b4y2bb85l7k4nasvis9vwm7p4dyw")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vj72y1h0xkn9zvagvgfg7knwnlwh1ins4vkmj0i6i3b2bil7i8l")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1604nx746bdfwgrl09hls2sf7pq401c8v86ycgkx0irn0df0nxjq")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qfdbc63sdc0j31ap29h2ba4ndd8crbn95qik4nyadw9bqn0c3h0")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s9symxw2wqvvy2v0znb43syp4g9h02l3gswgjr6lsa05pj5qdqz")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m28fngkdw312rsclkr00fsiapk7kc13xg3dzqf09p66yjm06j5f")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.10") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jgiwg4ikpq0ahpydxknffmq3g6b55jmsqbm7c73jmg7nidq1bv3")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j6qn5vpia35m34fwm4rwlhk6dyaw070igamssv6il28wgkjhhq1")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14my6y2n9yqwmbsizb6kfjm1jjjd963234cp17c26a7vxyknzdi2")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hpi5awi25mqa8fl3mqbs088hy1y6dc6lqly9ma64p4m2602znxv")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gaw94ab4p976472rxa3mm38b3icd0br2sh4b0fz7n41fw552wfw")))

(define-public crate-sapp-linux-0.1 (crate (name "sapp-linux") (vers "0.1.15") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18n8pj1wi7a6pphlzxkyc8mc47jrszyd9qgjvy9jdf5wjnkq0vm6")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.0") (hash "1spxmq0ix0hv0gfi45jlqgkga259a5g6z5gyfj6yg7qbxspgrxj7")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.1") (hash "0lvlndlq3vvzdlwimqnzi23jgn7qiwjyqnk3jgf1jmcwgb40qqk5")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.2") (hash "07rbn5r1bc8kcflczf586rr9ax91a6xjjj16kymg0m6d0d2d9si1")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.3") (hash "0v3m1ka2wrnd8pij5hvl0b6sgk3azfzx4mnhxb8m2fc2qph6pwhv")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.4") (hash "0kax0i7id9ly5aw1wgl0jms7c77ma1ndq639mziqi93cc0143hf3")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.5") (hash "1vssmix7bk49xb315dq6r3d1kzp49jqz9mlcinj7np9y2d2jlsc8")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.6") (hash "15wjawdn85qaf7hvwq8gyws3lknmd122c68n9cbscc0n08q1lvmg")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.8") (hash "1ixnfwm7zap04pg9nqm03ca94rnx7jri4iqb81wq3gax1n4s3h74")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.9") (hash "1ya3ndzpd5ah7q7fq8rqzl2d6cx4mzw1hprh3kv7j3fqrncc33l7")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.10") (hash "0d6wypiyarc5wvq50byg8b75j74h1y8yhryx2svkpbfz0wd27i97")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.11") (hash "1ambnvp8c2x8il02m6f5yzgwljzq4i3pkpcsv03nww0i699brsw6")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.12") (hash "0japac43dyv16zmcxl270hdrmwcc29bg5xhm68k8yqxl9qa3s648")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.13") (hash "0r3n4ddr0rj0f3y55gk8ap8g627nxh84jrxpmhzwhv56w08p00pz")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.14") (hash "1zd61qig6vp5fw36cy2jgfadih5ibfgrpyv9yqqibbzrq5dyzkxh")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.15") (hash "0gnxayp9fzkmhw8bwjrsrn8l84zx9izxp1hdpf48k0mq7mab2583")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.16") (hash "1b51dmilg8l333d20ym0wqz138zj9gamqzw2n0cz8z7zripg34w9")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.17") (hash "12afk4c505gb4vha2kjssz9g5q7y81y712j86zh613n2c93cqxan")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.18") (hash "129ncmaavjakxw10zbnrcwdmh5h41qnsi19iizpn4ghrwp0lpg54")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.19") (hash "05c3c9n3b42pswiib6s5q75jpmi6l2kq3cny1k90is98glx39180")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.20") (hash "0a7sb6mjjryp5nrpd04198y7i92kdq7jc6qk0i76lfcn36vwzc1q")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.21") (hash "0h9g204y5dnhm1dyav8wdk58vfn7x6544k20pjqxlh9n46s39fqv")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.22") (hash "1lbkghcxfal5w7z6mxh3nri4r7hkffhdr6jb882x9c8cc8ifv8sn")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.23") (hash "1r3qafijl7a6bh2gps200s819ckw0s2ajwf9hsb45093f5axhck6")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.24") (hash "0z73ih80shih0lnvb1lir60rac3w3r6r90wd1ajx3fdrx311ip26")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.25") (hash "1shyv43vq59g2frsja99mcq7py42ks2iqf7wwkwq5vjr5cajfjqh")))

(define-public crate-sapp-wasm-0.1 (crate (name "sapp-wasm") (vers "0.1.26") (hash "1fq8ymh8fd62xb5vq88vy82l33j3hfwblh6dms2wnfssckl5ks00")))

(define-public crate-sapp-windows-0.1 (crate (name "sapp-windows") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vgbs9405qnh8gn8dhaaxy7n0vpvr6hynzksvjal1xrqwh3gip25")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "01nvhr9ck1sg8ja0n8mdbgbqc0i5v2sp2sv8bmfyq91m8sr0k8cq")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r0icfhpq4b26fgcs0f8g3sq0hbp6chi5gs14fzmhyksia72wdi1")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "007bg7il10594szjs1n2a9a3yrn060v5di2108w147mf0xhzk8r7")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zf5l2lq6a9icapgdmndvda5yzmdjrxinhg0mibasl5j7fr1506x")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hl3dsnwiz1bcl9qi7lvwlwkfk91pzcl1w39qdv5qxdy0ljp3wr5")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k4034ivz54vycv9zjv4dxmwg6vz7k0rwjm653f6fmdd8mgd9adg")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x27raxkj7wh36fnbyy4wagvk7s4m599dl4fi1wbv7vfb415sipq")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b3lcwd9f4h9zz2d6951889xfwb42h40z8jlasrc4z67izdg86xr")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.8") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k0nicx7d7x2bc1pqc3l4jad2vwj5458iw2js2ba10p9drhwhyki")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.9") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gv25n35l983f7da5j8nmfaxfz0xwhwpmfbzlk0m1ir1pgisrrv8")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.10") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx"))) (default-features #t) (kind 0)))) (hash "1ipf53p6s6s6h78ch53h1i0rlkmhqacwk9jbqxb1z0g2ran6k904")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.11") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (default-features #t) (kind 0)))) (hash "1nvzsh8kc6m2kl66qad0vlk53wqbp6bqfcryp68z6spy3m8gwvml")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.12") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (default-features #t) (kind 0)))) (hash "11y5r4fc67rjmnz1wbifd6cshc8n96sn1h4fbcccpdb2np5fypy7")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.13") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (default-features #t) (kind 0)))) (hash "02i1hks5ahax0k2nf9c0ifbk1ap72l2w31617gkqzxlbqslkl0w4")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.14") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase"))) (default-features #t) (kind 0)))) (hash "1jxy68415clhmkfqg08sg5815y0jgw33s2fmrlkib404clv2rgvl")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.15") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (default-features #t) (kind 0)))) (hash "031hkz5qkrj80794k7z6pjkv3j0y3wpv3rssnz7y2ypdl6zndi7a")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.16") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (default-features #t) (kind 0)))) (hash "0mhfjkpiqhy8mbcqbhcpg17qsly64xrin769ghwzhxhzchgjvynq")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.17") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (default-features #t) (kind 0)))) (hash "1lk44nck5hxh5l7x4fwf3lgjb2dgamrfx2y5yymqnpbz5c9b46r5")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.18") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (default-features #t) (kind 0)))) (hash "1yhr0yy1gyhdb7jhmkd9zfwvldkcmq5gq5cqablyak5m7jcfrynq")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.19") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (default-features #t) (kind 0)))) (hash "171wavh0blw0491hir6z669lfwskqx27py5cpgy2yxc28sms9sgk")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.20") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage"))) (default-features #t) (kind 0)))) (hash "13hkm1118gqmdv9m5wl8d4d6lfr0wmxj65ywybsspp0zb4dr53q8")))

(define-public crate-sapp-windows-0.2 (crate (name "sapp-windows") (vers "0.2.21") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("wingdi" "winuser" "libloaderapi" "windef" "shellscalingapi" "errhandlingapi" "windowsx" "winbase" "hidusage" "shellapi"))) (default-features #t) (kind 0)))) (hash "1x9in6mgkjwiaz9s0kpnmgn2daqfgmy10w6qdl88mzsxrlldd5mq")))

(define-public crate-sapper-0.1 (crate (name "sapper") (vers "0.1.0") (deps (list (crate-dep (name "conduit-mime-types") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "17adfa8rj8wfhbamj5mynnd2n87n049yi20k4l232dp1x38sr3dd")))

(define-public crate-sapper-0.1 (crate (name "sapper") (vers "0.1.1") (deps (list (crate-dep (name "conduit-mime-types") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0xv14kfk7wz6rjwmkaf9hnb0afypx889ksjr1qr7krzl1z8fi5xm")))

(define-public crate-sapper-0.1 (crate (name "sapper") (vers "0.1.2") (deps (list (crate-dep (name "conduit-mime-types") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1gjqqljxk28yfchpy11gsf94n9vvl1h9zci63b0xgrirnlmiaqwd")))

(define-public crate-sapper-0.1 (crate (name "sapper") (vers "0.1.3") (deps (list (crate-dep (name "conduit-mime-types") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0r18sc32fy5imahgs4il71j993qaw4xcdq5jb6rz10n8qc0k80yp")))

(define-public crate-sapper-0.1 (crate (name "sapper") (vers "0.1.4") (deps (list (crate-dep (name "conduit-mime-types") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1vqxabpqrpypsp1xm5ds146z8xfpjpjkv1ajy4mnb351m3wqimh7")))

(define-public crate-sapper-0.1 (crate (name "sapper") (vers "0.1.5") (deps (list (crate-dep (name "conduit-mime-types") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1nyv8kh4wcax5fmalmx7zx1f22j2jg4hqv01kqvny02nvp21y82c")))

(define-public crate-sapper-0.2 (crate (name "sapper") (vers "0.2.0") (deps (list (crate-dep (name "conduit-mime-types") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3") (default-features #t) (kind 0)))) (hash "01ck5gpylma58svg4b6mfnzs38hzrwnqvysrn0fmzlkdm25wrzha")))

(define-public crate-sapper_body-0.1 (crate (name "sapper_body") (vers "0.1.0") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "1gn41l9sa8vnwgyznp3hlgsy5my9h2ai580v7drpyxvhv9fg9v38")))

(define-public crate-sapper_body-0.1 (crate (name "sapper_body") (vers "0.1.1") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "07qf79s4gjphrxf8yrdkaii5v9s5m4kmcmdlv0bc0ipvhn0vxkg3")))

(define-public crate-sapper_body-0.2 (crate (name "sapper_body") (vers "0.2.0") (deps (list (crate-dep (name "sapper") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "1k295c9s120112fy4vk6qrx0j60fhs33bphng9qvh93f7ywy655j")))

(define-public crate-sapper_logger-0.1 (crate (name "sapper_logger") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1a1myjb157cgbmnkj793l9dcik8s503fh4ajcl685qwr5xsrrrgq")))

(define-public crate-sapper_logger-0.1 (crate (name "sapper_logger") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "12gan5pwr01m2l0nzgi22x55ppqxcgzm60b3byncyh1y1ncqldwl")))

(define-public crate-sapper_logger-0.2 (crate (name "sapper_logger") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sapper") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kvc6rdzwa7yvapbcfwkjdwx031scd4lf8hhm1n91f8mgbvgscix")))

(define-public crate-sapper_query-0.1 (crate (name "sapper_query") (vers "0.1.0") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "10yfxniyqc512w10yzkj5y5v5z7ac8mbhwgvh9a275pb4hsnhs8x")))

(define-public crate-sapper_query-0.1 (crate (name "sapper_query") (vers "0.1.1") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0va8ynxdv65snks4dndcvz9xlck7wlpifjab5c4c62qy7y3zhvd9")))

(define-public crate-sapper_query-0.2 (crate (name "sapper_query") (vers "0.2.0") (deps (list (crate-dep (name "sapper") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "0fwyxvzxnb3bxsqq91rhdaici6wr5czmcag26nbwcykaq1c4hjg8")))

(define-public crate-sapper_session-0.1 (crate (name "sapper_session") (vers "0.1.0") (deps (list (crate-dep (name "cookie") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vxws1jm02fvq4h3h4wfg18141r7pi1bm4kcabjlylsw4zgzg085")))

(define-public crate-sapper_session-0.1 (crate (name "sapper_session") (vers "0.1.1") (deps (list (crate-dep (name "cookie") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qghz8jysvva1dskx5k73fdxkp202nfm5ndy6j4vwzdr6rbx6jfc")))

(define-public crate-sapper_session-0.1 (crate (name "sapper_session") (vers "0.1.2") (deps (list (crate-dep (name "cookie") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "146xqgdhw7j356z62zl0rpr1cywjc94mp9gv77f35gzvrl3w0c93")))

(define-public crate-sapper_session-0.2 (crate (name "sapper_session") (vers "0.2.0") (deps (list (crate-dep (name "cookie") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "sapper") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z2d5ild97pa0sd2izjv4yc139gaj3fc9m9fw1zz706x0dqgniga")))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.0") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11fr6mq20j7yhf3l5vbh3nm5cb7w01jmpj53a27f5clj0w9sajvz")))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.1") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v1x89wr4c1knzyvw8qcah19iz0jr9aq4m2085wkfi6pra8l4dhd")))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.2") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "04x3lhdl7mvj93sv6121v80dirxp37gl12f34s1w50fwka9cypzk")))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.3") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fg5yd6bz79gh142rn1wscrhp4035d66v8k9fhdqy969llpai4gi")))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.4") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hvn4sd4xx377w18rq1q6114f8hqk7xbck2xbamm565dyav0qnki")))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.5") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bkg1ffsc2bg756mrszmzbdrbhsvhx49qvg8mgdlbsvng0b1y4pg")))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.6") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "00pya6haa766z3p6f4819ipd9dr4wchjmxffd7rd9i4ify2iz9ys") (features (quote (("monitor" "sapper_tmpl/monitor") ("default"))))))

(define-public crate-sapper_std-0.1 (crate (name "sapper_std") (vers "0.1.7") (deps (list (crate-dep (name "sapper") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "12n32bdz9rpq8s0lyyqra8bjlm112qi4wpr9igycm02j18fqhyir") (features (quote (("monitor" "sapper_tmpl/monitor") ("default"))))))

(define-public crate-sapper_std-0.2 (crate (name "sapper_std") (vers "0.2.0") (deps (list (crate-dep (name "sapper") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11ad75jsnw6asq8mfshnaic9j1haymym8ymb4mkr3a30gwbs8gm1") (features (quote (("monitor" "sapper_tmpl/monitor") ("default"))))))

(define-public crate-sapper_std-0.2 (crate (name "sapper_std") (vers "0.2.1") (deps (list (crate-dep (name "sapper") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_body") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_logger") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_query") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_session") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sapper_tmpl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l5bir5qabsf46ymzzfbg8c27fxkmj6blgbqyj93dkqyimkmjlwq") (features (quote (("monitor" "sapper_tmpl/monitor") ("default"))))))

(define-public crate-sapper_tmpl-0.1 (crate (name "sapper_tmpl") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "053842d5mcm44l0hpi54i5dzq246z9ky1d6h1jh54qm04s4kpa1n")))

(define-public crate-sapper_tmpl-0.1 (crate (name "sapper_tmpl") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0asy9mpyakppzrqjr752wqcll1alaqfh38v59nbg9sp1h82psjpq")))

(define-public crate-sapper_tmpl-0.1 (crate (name "sapper_tmpl") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.10") (default-features #t) (kind 0)))) (hash "0bp2phkh5pcihlj1dbgfi294z7i45biz791iaskvpwymljvkyrf5") (features (quote (("monitor" "notify") ("default"))))))

(define-public crate-sapper_tmpl-0.2 (crate (name "sapper_tmpl") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ijgs7m774wp1c5wrpfzjvpmlv00h6pmfzk4bwmmwkabc6pplrm0") (features (quote (("monitor" "notify") ("default"))))))

(define-public crate-sapphire-0.0.0 (crate (name "sapphire") (vers "0.0.0") (hash "0q2pqmxy8zvay52lr52j25qygqms7vnhxjgki8mx7ich4fwzak1k")))

(define-public crate-sapphire-hash-0.1 (crate (name "sapphire-hash") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sapphire-hash-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "1spf9czpl2qnkn23rgz2j5mih3nbn260wpbrx53964rg2jw74hgm")))

(define-public crate-sapphire-hash-0.1 (crate (name "sapphire-hash") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sapphire-hash-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "043dqqkbsq1w680s79lb2yrpv6kll4fl99caj3m2nqbghbrjw0pd")))

(define-public crate-sapphire-hash-0.1 (crate (name "sapphire-hash") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sapphire-hash-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "spinoff") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "1pqpvch4riyikhg7zylrkv5l1n3zki7fqdhxgvx8jslppx9zpb63")))

(define-public crate-sapphire-hash-core-0.1 (crate (name "sapphire-hash-core") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "14fr15hjyfnhy1n6m8h6xdphfxl1d3h5m1j98frnqbpwfqmjixxd")))

(define-public crate-sapphire-hash-core-0.1 (crate (name "sapphire-hash-core") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "1gkpvmmv1nqx5fg6skqk0vx3m06cmkxkg1hcscfwv577c8n5k3kz")))

(define-public crate-sapphire-hash-core-0.1 (crate (name "sapphire-hash-core") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "0nr1c3rx786q3qffia70bnp8m4y6k8vgs2mdg8rw0a5sji7zycxd")))

(define-public crate-sappho-0.0.0 (crate (name "sappho") (vers "0.0.0") (hash "017n8nc3cnfn03g903y4zkysagd5dyl5irx969cxa76h84gbmxrm")))

(define-public crate-sappui-0.0.0 (crate (name "sappui") (vers "0.0.0") (hash "0rjc7yhyjwngswqy8lwsslb8l2brzxh4ix2ana3jy2a8bwgl9cbh")))

