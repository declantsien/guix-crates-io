(define-module (crates-io sa el) #:use-module (crates-io))

(define-public crate-saelient-0.1 (crate (name "saelient") (vers "0.1.0") (deps (list (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "14jh3cgdki0pxq6y47fjjgy87wrdzk02l1nkkxjliaylxz39y298")))

(define-public crate-saelient-0.1 (crate (name "saelient") (vers "0.1.1") (deps (list (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "12sgk3qz2mndhjacmxs6ckdzxny1ykwdy7205r46s75bnah2kyji")))

