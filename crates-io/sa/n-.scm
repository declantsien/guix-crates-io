(define-module (crates-io sa n-) #:use-module (crates-io))

(define-public crate-san-rs-0.1 (crate (name "san-rs") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)))) (hash "0bv3jp6pfc2nvygvv5rwvmdkmggl72526nwi14nx6lq060wwgcq9")))

(define-public crate-san-rs-0.2 (crate (name "san-rs") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)))) (hash "1jw9lz9hsijkbicsv70k0apms9c2njkjwf1ngxqmgcrvjp7p6840")))

(define-public crate-san-rs-0.3 (crate (name "san-rs") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)))) (hash "02r1md9kikz4pxcxwyrf102p86dj938hqga9w0j5kq7qkczz8rpg")))

(define-public crate-san-rs-0.3 (crate (name "san-rs") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)))) (hash "1b7asqpscm8cfhzhgdk9s11fbznsjw3nx6v53pqwn00zwglxvyix")))

