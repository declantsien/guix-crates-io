(define-module (crates-io sa cr) #:use-module (crates-io))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.0") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)))) (hash "1kahihj7qgh4nhm1s0dcbrmzqp2n91f40055f2q3wcnspz3kx92k")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.1") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "10xc7340wxx2hqvqkqvrvidqh2sw8k96vyw86n7744zwp8p19pyq")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.2") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "0rv3armqs0f81g28g0qkj1p6ckh7g25d2pxpj72h27c5hvgnbiqi")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.3") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "1g78ixxbjn9azhggz55hwjd65jf8bc519984f4g0qxfr37p702f7")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.4") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "0mrlv6579g09llq4ada5lzkkbwymg0zg1shq1nrab41cgqj9hqil")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.5") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "0yxc8f656m1pjxplldwv0b698c75hi25rbram902xizwkz7px9gv")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.6") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "02xywkzkdx1k6p8pp58da2aq99x98gai6mi21sxpkdki2ingax28")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.7") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "0j795hhc07i87ma88m1c9pzhfkapz1ddbh9fcd74ap0mv40v6vrk")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.8") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "04zf5x9swdazwxwarw6hwz21hwjqi0vf1c29m0s38yyn2f32rfy2")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.9") (deps (list (crate-dep (name "pgn-reader") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "005nfbxyvdiri02i96pcmbyv8rrfjwd00i6xmwm98glrdam7rdpd")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.10") (deps (list (crate-dep (name "pgn-reader") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "1yjq9pf384cbksy56iv1253ibbrvymh6mfwwldspwyydhz0mx8mk")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.11") (deps (list (crate-dep (name "pgn-reader") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "09v91v4j7c0xs759jw7sglfwflrcxbcpbs6ic4kd1ybahjyipzjp")))

(define-public crate-sacrifice-0.1 (crate (name "sacrifice") (vers "0.1.12") (deps (list (crate-dep (name "pgn-reader") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "1nzs8d5xkmqyc72k8m2pm0vxhkbykk59sskg4vfixaq3jywx15yx")))

(define-public crate-sacrifice-0.2 (crate (name "sacrifice") (vers "0.2.0") (deps (list (crate-dep (name "pgn-reader") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.24") (default-features #t) (kind 0)))) (hash "14f9bxrwj36712pcwsnxpmgy1ilr3lri60dsis2khdzwgxjchiv3")))

(define-public crate-sacrifice-0.3 (crate (name "sacrifice") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "pgn-reader") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.26") (default-features #t) (kind 0)))) (hash "0vfd5rdzwzwn488hbnm6psa2g7sh8kka9klfpsa96f6nq321456g")))

(define-public crate-sacrifice-0.3 (crate (name "sacrifice") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "pgn-reader") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.26") (default-features #t) (kind 0)))) (hash "1l45cags5z6pnfi53kdzkkwiidql3c4jz64g49ifgwd8ffbi6ljh")))

(define-public crate-sacrifice-0.3 (crate (name "sacrifice") (vers "0.3.0-alpha.3") (deps (list (crate-dep (name "pgn-reader") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "shakmaty") (req "^0.26") (default-features #t) (kind 0)))) (hash "0h9kcgfvd31a9dwxnkcf37h4sc3b81gp1r48pc74mwl8kp0vklna")))

