(define-module (crates-io sa ys) #:use-module (crates-io))

(define-public crate-saysky_rust_learn-0.1 (crate (name "saysky_rust_learn") (vers "0.1.0") (hash "0i3s0gvdhra2z0zkbgi1gbcxlfmn97kgvi6lrncql7pg6jrn2rs4") (yanked #t)))

(define-public crate-saysky_rust_learn-0.1 (crate (name "saysky_rust_learn") (vers "0.1.1") (hash "1gzqzm2bmpf4n3f1qdali60zzp7xvy2fk9k4r99hmsf1qq76cpvf") (yanked #t)))

(define-public crate-saysky_rust_learn-0.1 (crate (name "saysky_rust_learn") (vers "0.1.2") (hash "1444g6944l43z3jclrjany87wz6a0yp8gs7iw15b60bnvs7wlr3y")))

