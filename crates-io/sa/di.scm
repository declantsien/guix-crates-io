(define-module (crates-io sa di) #:use-module (crates-io))

(define-public crate-SadieFish-0.1 (crate (name "SadieFish") (vers "0.1.1") (deps (list (crate-dep (name "aes") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "shaderc") (req "^0.6.2") (default-features #t) (kind 1)))) (hash "1s4bjvhcj1zpa57cps187gi66a3rz26f5zy61spls3hwg8ps8971")))

(define-public crate-sadikkuzu-0.1 (crate (name "sadikkuzu") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1j3zlpxdwl8a3p843vk4rxws9km93ri2x6cy87fzd0344qw755np")))

(define-public crate-sadikkuzu-0.1 (crate (name "sadikkuzu") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16kbk25nvcjnjxj27xnsnzwgfx418zpsz4klr7k829w9llrjn9g2")))

(define-public crate-sadikkuzu-0.1 (crate (name "sadikkuzu") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wbxswhy0jn4qd63h4avqzkvfzczf5bfp41j3ljlba8n97qfvs5l")))

(define-public crate-sadikkuzu-0.1 (crate (name "sadikkuzu") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1yg75ig8g0sbffbnw06m2rh4lnryr4z1wz6x9bkl6j2xs50v2w3z")))

(define-public crate-sadikkuzu-0.2 (crate (name "sadikkuzu") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19rx159n74byx6w1vn22n20ipw2imy1fj41mmhgi3ylgzj99gx09")))

