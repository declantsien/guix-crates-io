(define-module (crates-io sa nt) #:use-module (crates-io))

(define-public crate-santa-0.1 (crate (name "santa") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0") (features (quote ("color" "derive" "cargo" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-enum-str") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.23") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "tabular") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09p0fw1hpjvnp5y373y839qfdyw36z95wwrm4az04vl83krhd2nv") (rust-version "1.56.0")))

(define-public crate-santiago-0.1 (crate (name "santiago") (vers "0.1.0") (hash "1bqnhw1rr9bd2zcq0zsikmh4kayqm682nmsgn1ky1gi7jmc9vg1i")))

(define-public crate-santiago-0.1 (crate (name "santiago") (vers "0.1.1") (hash "1znb5iv504lwfwhdk3dd6c816gaahk21zc76q49ndbisidhqj1c9")))

(define-public crate-santiago-0.2 (crate (name "santiago") (vers "0.2.0") (hash "03858jbgx4hh73a8q4pjq0d5wq65pdh3kymmhsysffbh0y0prvs4")))

(define-public crate-santiago-0.3 (crate (name "santiago") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1hk11kw7ams9d08g7azz5wbix873qsmdv2mqmlz83xm6mlsy7gmw") (features (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.3 (crate (name "santiago") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "123b61wz1fvk654nikvmfm7abrp0c6wrjvqwk86gsjhkszd1rfk2") (features (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.4 (crate (name "santiago") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1vsnf763b7yxcgcz3ny772x6sjjcaw19x9njspn4rj5467zk2h1l") (features (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.5 (crate (name "santiago") (vers "0.5.0") (deps (list (crate-dep (name "regex") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "17zxqqmrdjmqqpyn1k1j8c9w0i6jn6873hibazlij7225f34hyq7") (features (quote (("regular-expressions" "regex") ("default" "regular-expressions"))))))

(define-public crate-santiago-0.6 (crate (name "santiago") (vers "0.6.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (optional #t) (default-features #t) (kind 0) (package "regex")))) (hash "1602qin2zpy4ilx702anrmmjjyj43ncqqjbsp7icv9075nflzaan") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.7 (crate (name "santiago") (vers "0.7.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (optional #t) (default-features #t) (kind 0) (package "regex")))) (hash "02dqbjqbfrmv5898ynqw7s41bbaa05snn5kvg6vhzq522had31sd") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.8 (crate (name "santiago") (vers "0.8.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (optional #t) (default-features #t) (kind 0) (package "regex")))) (hash "003wy7kvqc8sgshsvvpv5jqjnln1g7vfyf9nsg79wwvvw2cakkmy") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.9 (crate (name "santiago") (vers "0.9.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (optional #t) (default-features #t) (kind 0) (package "regex")))) (hash "13l476ghzf79sjjl6kz6p4fmj2xmm27zdpig9pns46f95q6qkyc1") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.10 (crate (name "santiago") (vers "0.10.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (optional #t) (default-features #t) (kind 0) (package "regex")))) (hash "07s20112ylk5iarby8an94fcaissjp668vpinn0qxlpp2dgrml9y") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.11 (crate (name "santiago") (vers "0.11.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "09fwraxdsnqp801zs8l1m15yjzc9qg2rrlspyykrh62qnrn42bqz") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.12 (crate (name "santiago") (vers "0.12.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0bk1pymjvs1x6hl4imqx9605za4d72jlmjfqr91n0kxiqpf23h51") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.13 (crate (name "santiago") (vers "0.13.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0vym4hm7zz9lms6q2bzh19mkh1mihg723gf0r5yjx7zd70x3a3kn") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.13 (crate (name "santiago") (vers "0.13.1") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0rfz1l8qy7dbdlpizy2nhd82xazd82gmh9nq0yakw5wi2cyxsb4r") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.13 (crate (name "santiago") (vers "0.13.2") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "153mzh76bb1rxjxvdnhy0hx6pv0aw4wk17wlhpw5yfak14bnhcs9") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.14 (crate (name "santiago") (vers "0.14.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1dkb02xik18n5l3k2cd6kaijnxfm9knlxwyblkf857vszd2fjvpp") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.15 (crate (name "santiago") (vers "0.15.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "04ijq1jr81lm6s824f0n0nyykdk4l3sik1zrvg5fs3hzsrf6bp3w") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.16 (crate (name "santiago") (vers "0.16.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0iz5vpr1ymgdxdv6adzr8ys095w3bhwldm88ncdrdimj5f7wnk9c") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.17 (crate (name "santiago") (vers "0.17.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0lrxniihks1yx72a1gbd6ncf5h2y94bz2gn02azqhnf4snikc8nl") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.18 (crate (name "santiago") (vers "0.18.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1l9ldk5mla98bh7wv8kxm4w6f1vnpg8h89nx56fqixxpxcww887h") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.19 (crate (name "santiago") (vers "0.19.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1q1psvn7gy9n1v5ifh5y9z66rmv2aq8crw818wp7v8gfg56kalwr") (features (quote (("language_nix") ("default" "crate_regex" "language_nix"))))))

(define-public crate-santiago-0.20 (crate (name "santiago") (vers "0.20.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1fkvk7r07ghcrqlkmga6h1hi4vxm0bvwa05hf1q73dq8gq7hd6dy") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-0.21 (crate (name "santiago") (vers "0.21.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "03kfnzj4jgp7l9f04fj1l59pgy0xpyrj50p2fldyja8azvgcfikk") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-0.21 (crate (name "santiago") (vers "0.21.1") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1l3a0diqsvmrkb8vd8dc2fmkpjasg18rf3nh2hqld3178n8zwb3k") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1 (crate (name "santiago") (vers "1.0.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0m1dj37lbkpv80l9m6ahj2m7dzx5pcdjwdf1r13zf4zw16xkabgd") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1 (crate (name "santiago") (vers "1.0.1") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0lsk0s5cclhmkqpc8k0zz6cyyvvmwgz4kgfwq0sq0y1h4xjq2nwp") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1 (crate (name "santiago") (vers "1.0.2") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "16djjv4snsj40kr523dbp8gbzxh7iaafv02k2qy3jlrww31ihfv8") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1 (crate (name "santiago") (vers "1.1.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "0pdiw6scjvxc6h5p530jpycl752h057hfqsjb2aj8iny80vhwffl") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1 (crate (name "santiago") (vers "1.2.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1ysbhwc9max6kbi8ip7g0pr7864lv5bnx55asikcwl7ifia9z0yz") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1 (crate (name "santiago") (vers "1.3.0") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1p8w712igvfimslmir3j406nbd2mhajakf2favslnr1r4cdcdxmr") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santiago-1 (crate (name "santiago") (vers "1.3.1") (deps (list (crate-dep (name "crate_regex") (req "^1") (features (quote ("std" "perf-dfa" "perf-inline" "unicode"))) (optional #t) (kind 0) (package "regex")))) (hash "1v47xcgl4k0zf4h8hiwv8pj7ad7g1x3gmgsmizmqc85wj8i04dny") (features (quote (("language_nix") ("language_calculator") ("default" "crate_regex" "language_calculator" "language_nix"))))))

(define-public crate-santoka-1 (crate (name "santoka") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "174z7nci6s18kk6sj06xhanimdqz3yzbi0k2lrzwz3g62ll3zs1m")))

(define-public crate-santoka-1 (crate (name "santoka") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "07996x1xjhnd70x10bm83h2l6096gbk9mvdcx6a0pjbkg7lnscah")))

(define-public crate-santoka-1 (crate (name "santoka") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "0pvdih1vj6j39bjwvflq4y1dl71zfc8wkfq6f5p6gi1ykz28jspn")))

