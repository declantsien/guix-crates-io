(define-module (crates-io sa si) #:use-module (crates-io))

(define-public crate-sasin-0.1 (crate (name "sasin") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.199") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.199") (default-features #t) (kind 0)))) (hash "0i5mz1l26mjzdr602j350y3p9qnr3ispq0dzqr4z0pvn5hpx5h8k")))

