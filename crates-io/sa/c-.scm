(define-module (crates-io sa c-) #:use-module (crates-io))

(define-public crate-sac-base-0.0.1 (crate (name "sac-base") (vers "0.0.1") (deps (list (crate-dep (name "graphlib") (req "^0.6.1") (features (quote ("no_std"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1jw9x1v9vmjy8cshhqd830262az6z0sp5zyxfnn64cwb4nhxg72f") (yanked #t)))

(define-public crate-sac-base-0.0.2 (crate (name "sac-base") (vers "0.0.2") (deps (list (crate-dep (name "graphlib") (req "^0.6.1") (features (quote ("no_std"))) (default-features #t) (kind 0)))) (hash "1j21bc844jj3cj7hv7vxxrvdgqpxy1sfi3r8gm419x464x73szvn")))

(define-public crate-sac-base-0.0.3 (crate (name "sac-base") (vers "0.0.3") (deps (list (crate-dep (name "graphlib") (req "^0.6.1") (features (quote ("no_std"))) (default-features #t) (kind 0)))) (hash "0ypw85mhg8k488b297rm9h4y1vvgalnls3sbzgv6vgw9gfavc09v")))

(define-public crate-sac-base-0.0.4 (crate (name "sac-base") (vers "0.0.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "graphlib") (req "^0.6.1") (features (quote ("no_std"))) (default-features #t) (kind 0)))) (hash "0m76hmmzicrg322xlvfs2v57yqsb0swvyyymy7yjw658ki1zlm19")))

(define-public crate-sac-base-0.0.5 (crate (name "sac-base") (vers "0.0.5") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "graphlib") (req "^0.6.2") (features (quote ("no_std"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (kind 0)))) (hash "16rk8d4xb640xpgdn2j0kslm4652arj3hzrfp9wy57cr1nz3lkrl")))

(define-public crate-sac-base-0.0.6 (crate (name "sac-base") (vers "0.0.6") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "graphlib") (req "^0.6.2") (features (quote ("no_std"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (kind 0)))) (hash "1v97fg2hhv70ifayvg571ap7bsgjqrw7wn1rwkgf670zz5mrdbnl")))

(define-public crate-sac-base-0.0.7 (crate (name "sac-base") (vers "0.0.7") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "graphlib") (req "^0.6.2") (features (quote ("no_std"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (kind 0)))) (hash "0fxznjfaryppzh11ngif2mbmcz8s164gfpr3mayi6q0qsl5b3y04")))

