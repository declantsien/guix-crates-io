(define-module (crates-io sa mi) #:use-module (crates-io))

(define-public crate-samira-0.1 (crate (name "samira") (vers "0.1.0") (hash "00ymhlg9qig62pbqr6wpwrhz97lmimw6jvryr6j9m202ihpvacpr")))

(define-public crate-samira-0.1 (crate (name "samira") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "1rkl2nm1hafkyiakl7l7343nk9j3rpnqhj3k6znil8z61vcbprzn")))

(define-public crate-samira-0.1 (crate (name "samira") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "15vj06xab0n1kws5vfvgzl5jddfzqy6ak0vqvb3r7k8rvkn7y0jq")))

(define-public crate-samira-0.1 (crate (name "samira") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "034j61szp9bmy2mjgx955d4hv50fiafjvd8qypzdx2nvb4i5zk6h")))

