(define-module (crates-io sa f-) #:use-module (crates-io))

(define-public crate-saf-httparser-0.1 (crate (name "saf-httparser") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wcs5dy6ilx7ry8rpxd8xyd76f5y3zfgy49rwfmch2hrn5v301ki")))

(define-public crate-saf-httparser-0.1 (crate (name "saf-httparser") (vers "0.1.1") (deps (list (crate-dep (name "http") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1797aaj85abkpxnvfbfac9s3brgncwnlc1v16zkiiflynsg74kx7")))

