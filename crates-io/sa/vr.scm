(define-module (crates-io sa vr) #:use-module (crates-io))

(define-public crate-savr-0.1 (crate (name "savr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pam") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "x11rb") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "xkbcommon") (req "^0.4") (features (quote ("x11"))) (default-features #t) (kind 0)))) (hash "0mncwzjzqcwmcj5ja7qkg978l7l9262hclmkigffig0qc4vhsw46")))

