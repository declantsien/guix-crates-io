(define-module (crates-io sa nn) #:use-module (crates-io))

(define-public crate-sann-0.1 (crate (name "sann") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "vectors") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0ykk1fpgl1ilkrxjgfln895pdkgw25z6qsskg3c17cmrr7yf948a")))

