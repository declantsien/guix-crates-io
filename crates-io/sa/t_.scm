(define-module (crates-io sa t_) #:use-module (crates-io))

(define-public crate-sat_lab-0.1 (crate (name "sat_lab") (vers "0.1.0") (deps (list (crate-dep (name "bool_vec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)))) (hash "0p4cl5g1v7q00k4wfk9ph8im9i25nbf2hj0db7yxadf7zy6851lr") (v 2) (features2 (quote (("rand" "dep:rand"))))))

(define-public crate-sat_toasty_helper-0.0.1 (crate (name "sat_toasty_helper") (vers "0.0.1") (deps (list (crate-dep (name "splr") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "16jay2b550sasj5mk1wdp2qv1dqq2s95cz2fqx7kqib7zk07ljjl")))

(define-public crate-sat_toasty_helper-0.0.2 (crate (name "sat_toasty_helper") (vers "0.0.2") (deps (list (crate-dep (name "splr") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0dipjnygm3hs31yimy85dia0amqbwfbghp5x42xfv3dssb86b29n")))

(define-public crate-sat_toasty_helper-0.0.3 (crate (name "sat_toasty_helper") (vers "0.0.3") (deps (list (crate-dep (name "splr") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "1aldwazp75klnaipz2zb0axwmcikckz0iwwn5kw4rzxsmrbp3vcx")))

