(define-module (crates-io sa sc) #:use-module (crates-io))

(define-public crate-sascha-10-post-message-input-0.1 (crate (name "sascha-10-post-message-input") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0.19.3") (default-features #t) (kind 0)))) (hash "1kwlwkh96l9y5wmqdyvq2hcgqisdhspyq1il6v0a4kcbx2fslq2h")))

