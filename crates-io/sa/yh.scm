(define-module (crates-io sa yh) #:use-module (crates-io))

(define-public crate-sayhello123-0.1 (crate (name "sayhello123") (vers "0.1.0") (hash "0pgh91nfnp3wfx8ki0q59kz788gpvvdj3aqwhk0rz0zr0ll48vp0") (yanked #t)))

(define-public crate-sayhello123-0.1 (crate (name "sayhello123") (vers "0.1.1") (hash "1cclhv4lrkhzlcixnf8fv0lg9xcf57bdn31ncydzhhsy1x62gama") (yanked #t)))

(define-public crate-sayhi-0.1 (crate (name "sayhi") (vers "0.1.0") (hash "19ysrc1vcb9pgjgsprn3j3l0yfh2fl18i1q7xgj6vvs944hdqkzr")))

