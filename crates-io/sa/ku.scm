(define-module (crates-io sa ku) #:use-module (crates-io))

(define-public crate-saku-0.1 (crate (name "saku") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.6") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)))) (hash "0ipvb4ghsrv3pmjn5s88sql0izwqd56h8j7r5pp54q3dxp9dpc8v")))

(define-public crate-saku-0.1 (crate (name "saku") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.6") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)))) (hash "15z6r2d2jjkhz4ss5jx9lws5wkwjxcgqb49hiz6wx82ay4zqr0nh")))

(define-public crate-saku-0.1 (crate (name "saku") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.6") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1csva8rpk3qazys2mbf1s4di98ffp2f662hr3ri9s17862nll8hy")))

(define-public crate-saku-0.1 (crate (name "saku") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.6") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1jyw9mh54773q8cnw4bp0579yxplpnqbzayn39gqs4v5cn57m8ji")))

(define-public crate-saku-0.1 (crate (name "saku") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.6") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0h0p4sxy5fwgj05hhw7wv41hinni7y06p5xb8x3fhwikqlacjz43")))

(define-public crate-saku-0.1 (crate (name "saku") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.6") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)))) (hash "12li03v0pplf21g06d4mjbk3gcbg3bpwkx7mz796wgjrvqq0wpws")))

(define-public crate-sakuin-0.1 (crate (name "sakuin") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.6.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "19586mh0n2f8ik8fqj0ababwcgqf6vdj58w10ckgcadkkiv9m0fl") (yanked #t)))

(define-public crate-sakura-0.1 (crate (name "sakura") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "04jniq7a8ks53jb06mp3vvciwjkmjw1dc5s4ajkiam6s6bifsvn0") (yanked #t)))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.13") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p8jpvl74x5pnlnmhbr2x8qwb3j43wa2zjy1jbaxwx4azk6drr14") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.14") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w7s3ljhy2mpd6v7na8dralwz1ac0zmnvrxcgafw5x33fcdaqrj8") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.15") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p0lqkdpqvqrd2rvhfb69bfp8k08mv07a6g8sbngam96smqyilac") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.16") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yif78rsklacps79rw1dvf832lng6xsnk2sllp8bdng54ss4rqlw") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.18") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ldimr33rcxbh0cdirhh5lyvzpycnvsdi96dcam0ggjfpgza9b74") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.21") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y02c84gyx3r2z8znz85qwbaqdxaih5xg1g08c9j3x1297lqhfi8") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.22") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jzkm5lll4arn1i0m74ya7ddzfcsf478cdm9xc5jk2pkzqfxh7rp") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.23") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0by4gyhgrvsji4p70nx4j38s2d86pf6msix4cpy6xghz7s22ksnk") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.24") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rm4j35vd67jkxqyhwjnkqf5k6fdh55s3ml9p55gcnfjqjp3ca4b") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.25") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0a937fi7dx4015c82ysbpnf1yjw1sks4dc73z0dvghqn4jkc93lm") (rust-version "1.64.0")))

(define-public crate-sakuramml-0.1 (crate (name "sakuramml") (vers "0.1.26") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ygfd701izm5w4hns1k7ydrgvbnkxyvl4kil9d7ay22da95a5xaz") (rust-version "1.64.0")))

