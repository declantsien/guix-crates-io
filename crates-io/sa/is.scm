(define-module (crates-io sa is) #:use-module (crates-io))

(define-public crate-sais-0.0.1 (crate (name "sais") (vers "0.0.1") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1i404vdxmay9hjpqp14nrjrqcak1752mww3dl4y4g88krr9hspw6")))

(define-public crate-sais-0.0.2 (crate (name "sais") (vers "0.0.2") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "03lz9dkis7izc0yk2w5bnf8lqipsrlqc0wz7zkdihq478d2gg0sc")))

