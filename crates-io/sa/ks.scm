(define-module (crates-io sa ks) #:use-module (crates-io))

(define-public crate-saks-0.1 (crate (name "saks") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req ">=0.11.0, <0.12.0") (default-features #t) (kind 2)))) (hash "18zv8w8b7wdaskcwvspcwba6wbivh2g9dv5ar0kjm0h5misblry7")))

(define-public crate-saks-0.1 (crate (name "saks") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1h0drm0mvq0wrqq9mzchfsw93sc2czkh22xy8y2rs47i4gwsiyjn")))

