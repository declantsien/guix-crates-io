(define-module (crates-io sa as) #:use-module (crates-io))

(define-public crate-saasaparilla_notification_common-0.1 (crate (name "saasaparilla_notification_common") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("json" "gzip" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "035fv21d2fw6dywfnyp4110kbcc0b68gbng0ddih8r7rfkr0hvmq")))

(define-public crate-saasthings-0.1 (crate (name "saasthings") (vers "0.1.0") (hash "0f7j5d9gyk9208039pwlkfg46n7bbg43qxdj692rjq28c9y386a4")))

