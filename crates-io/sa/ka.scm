(define-module (crates-io sa ka) #:use-module (crates-io))

(define-public crate-saka_grep-0.0.1 (crate (name "saka_grep") (vers "0.0.1") (hash "18f4pgl9vj7rz2bqrzr0k6mpj53g34hxgy2fkva35ivlqma4mgia")))

(define-public crate-sakaagari-0.1 (crate (name "sakaagari") (vers "0.1.0") (deps (list (crate-dep (name "git2") (req "^0.13.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "15lsa1p6cf8zpm08srpg8rzi33ansy2vx2kpk2b8fxv2qvda074w")))

(define-public crate-sakaagari-0.1 (crate (name "sakaagari") (vers "0.1.1") (deps (list (crate-dep (name "git2") (req "^0.13.12") (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (kind 0)) (crate-dep (name "toml") (req "^0.5.7") (kind 0)))) (hash "0f1v1826gc66w9ph8iwy2zphmqmn8lzb9r3nvnc5b078a0gv1qna")))

(define-public crate-sakaagari-0.2 (crate (name "sakaagari") (vers "0.2.0") (deps (list (crate-dep (name "git2") (req "^0.13.12") (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (kind 0)) (crate-dep (name "toml") (req "^0.5.7") (kind 0)))) (hash "1aiqiczwxqr2a8byv7bdqpssq7khg72nk8x5msy5in7rhpd7znbb")))

