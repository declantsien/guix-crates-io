(define-module (crates-io sa ts) #:use-module (crates-io))

(define-public crate-sats-0.0.0 (crate (name "sats") (vers "0.0.0") (hash "1bar94r74y6055xphlp7nmzsx4rn6fm1ypkrdwja00d22byhgxhy")))

(define-public crate-satsuma-0.1 (crate (name "satsuma") (vers "0.1.0") (hash "10glw99rwx8bj6zdiyi30mv23ns5xsk09pgcwdglpvg5y2plh08d")))

