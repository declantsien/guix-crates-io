(define-module (crates-io sa d_) #:use-module (crates-io))

(define-public crate-sad_machine-1 (crate (name "sad_machine") (vers "1.0.0") (deps (list (crate-dep (name "convert_case") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zap9vf0a869qpzvl2xz9zmy0vaglyf425a34hmximbwapi9038p")))

(define-public crate-sad_macros-0.1 (crate (name "sad_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0czn3ldyhcg9gr9xh6zfaaw9cjsnlw4f571sr9nypcydf40k8536")))

(define-public crate-sad_macros-0.2 (crate (name "sad_macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0cbs6rirbv5c2c5hb0g2sdwc7kycgs0kkj0rqapn5dwxamplzmq2")))

