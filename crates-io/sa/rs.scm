(define-module (crates-io sa rs) #:use-module (crates-io))

(define-public crate-sars-0.1 (crate (name "sars") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bio") (req "^0.41") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hmzn2wzwm42xffxkcvg296s125k0m756006f8485f4gas8b0xql")))

(define-public crate-sarsh-0.0.0 (crate (name "sarsh") (vers "0.0.0") (deps (list (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "1hch1gi5cv31gzgxq3g37vh9pdjwgma5fx6z3h17g80h2hs0f7h5")))

