(define-module (crates-io sa tl) #:use-module (crates-io))

(define-public crate-satlog-0.0.1 (crate (name "satlog") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0q1mzwd9ya01b06223bfcz7nn57kisrkf0kf323kakpbfglhlglz")))

(define-public crate-satlog-0.0.2 (crate (name "satlog") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1xlr1k6424m5wyiacziwhha4ywgfk82r98xdhjdsbz8swmc39r7p")))

(define-public crate-satlog-0.1 (crate (name "satlog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0p9ixdkrvgbzda8m9cp28krisdplxi0655x7yqy5yjsq7kzmhf5l")))

(define-public crate-satlog-0.2 (crate (name "satlog") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0liaqal68v50f1zrx2ndcvrbsxz2pi45q5y71j61fpp1dq8yxk7b") (features (quote (("default" "color") ("color" "colored"))))))

(define-public crate-satlog-0.2 (crate (name "satlog") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "16xz8h2cgg4p9vdbg2hf32h6vgix0abpiv8dg9m8p6hx90i0mh58") (features (quote (("default" "color") ("color" "colored"))))))

