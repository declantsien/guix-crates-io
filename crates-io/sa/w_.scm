(define-module (crates-io sa w_) #:use-module (crates-io))

(define-public crate-saw_mcr-0.1 (crate (name "saw_mcr") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "11imcbqqkcjb1d8njh14mcg241mk79z09lpyckba6sccacvrqrid")))

(define-public crate-saw_mcr-0.2 (crate (name "saw_mcr") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "01prw9jagzk9kcvwbi1llfyxl2w6jmllpny7ava93lh7a5s6mpp4")))

