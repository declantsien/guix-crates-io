(define-module (crates-io sa y-) #:use-module (crates-io))

(define-public crate-say-hallo-0.1 (crate (name "say-hallo") (vers "0.1.0") (hash "15sf3yygslr0r7fbl3bwi3vklly583m18yswv4irhf1p4k37ca4j")))

(define-public crate-say-hello-ikoafianando-0.1 (crate (name "say-hello-ikoafianando") (vers "0.1.0") (hash "1hs55072b7v36qd2jp8c1h5a3ns3g6fvping3ffkd0zpkpvx700f")))

(define-public crate-say-hello-ikoafianando-0.2 (crate (name "say-hello-ikoafianando") (vers "0.2.0") (hash "1ifc8q0id1qg6grsni1rgdl79lkjwvfja79l1lcrbzvjshhy25lf")))

(define-public crate-say-hello-ikoafianando-0.3 (crate (name "say-hello-ikoafianando") (vers "0.3.0") (hash "1d3aka9ivc6ffhby2lb2b7klq1lg0cak01yjalp5bkwihshidi62") (features (quote (("hello") ("default" "hello") ("bye") ("all" "hello" "bye"))))))

(define-public crate-say-hello-ikoafianando-0.3 (crate (name "say-hello-ikoafianando") (vers "0.3.1") (hash "01qig22iaprdnmn6mzh7mn7m9wdj1kzxygnlv0asf014a505drd2") (features (quote (("word") ("hello") ("default" "hello") ("bye") ("all" "hello" "bye" "word"))))))

(define-public crate-say-hi-0.1 (crate (name "say-hi") (vers "0.1.0") (hash "1fg9hz4k306hl0m8wm15bssxskhapyvvc871ph9xcazs2p6qrvwf")))

(define-public crate-say-hi-0.1 (crate (name "say-hi") (vers "0.1.1") (hash "1ibaikhk4mifm7nz373s5hg7q5hvaawby9hx0b59lv189slvmx09")))

(define-public crate-say-hi-0.1 (crate (name "say-hi") (vers "0.1.2") (hash "05mgdpdmrvq99ncs24zk8wzblv4cdai6m0g9mbshfyszznqhbygg")))

(define-public crate-say-hi-cli-0.1 (crate (name "say-hi-cli") (vers "0.1.0") (deps (list (crate-dep (name "say-hi") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1spljnvlizgc45z5d1y34w9lrj5aplpvgrdw3jxbdqc3vg3gvcmg")))

(define-public crate-say-number-0.1 (crate (name "say-number") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0944qv99b35ljp0jz84gmcypcsamk6aa0vlrji2fpcwvdb4hv1bq")))

(define-public crate-say-number-0.1 (crate (name "say-number") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "1y7g2z3ybbcf6g7bg1a721prx9qnvakvv9zwhash47vx0h282m5c")))

(define-public crate-say-number-0.1 (crate (name "say-number") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "1rrcigmz519sk1c16xjwv7hz15sjmx1ia7lvcm2gy0zad3jwgidl")))

(define-public crate-say-number-0.1 (crate (name "say-number") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "1g3lpfk790j4z1pmk6vvyb7c2bbvpvfwah3cqy0xdc0ibm0icf49")))

(define-public crate-say-number-0.1 (crate (name "say-number") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "1pdd3grc9rqvx8v2is8bzsgh8flalsgx6a7fk44hdd4zp5dxgznl")))

(define-public crate-say-number-0.1 (crate (name "say-number") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "1lainfh2axxj5pay0kq9k0c0436hfplzdxhbzrryyqch2a974lbx")))

(define-public crate-say-number-0.1 (crate (name "say-number") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "1r7hxny4b0xcvmi2rgk3z68bxwngy9p35ap9kkx7rkfnxlc5l9ba")))

(define-public crate-say-number-1 (crate (name "say-number") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)))) (hash "1skf7lnskc4d22ay1znf8r558vmkzhmxyz400p08l21v6589f00f")))

(define-public crate-say-rs-0.1 (crate (name "say-rs") (vers "0.1.0") (hash "01cv4m5qfxyzd502mz3xz5pnm0brd75638lmjx4jljq3pcqzkrvv") (yanked #t)))

