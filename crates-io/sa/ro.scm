(define-module (crates-io sa ro) #:use-module (crates-io))

(define-public crate-saros-0.0.1 (crate (name "saros") (vers "0.0.1") (hash "11g98ima8lb66k4qpz8dypcr53xqv9rqrvr3gd3afwvgxinpkizm")))

(define-public crate-saros-0.2 (crate (name "saros") (vers "0.2.0") (deps (list (crate-dep (name "biometrics") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "buffertk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "indicio") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "one_two_eight") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tatl") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zerror_core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "zerror_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kxcqajc1m9jdna9ss906fsps9xh160wdml4sy8hvxid2i15xqdr") (features (quote (("default" "binaries") ("binaries"))))))

