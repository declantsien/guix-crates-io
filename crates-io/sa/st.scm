(define-module (crates-io sa st) #:use-module (crates-io))

(define-public crate-sastrawi-0.1 (crate (name "sastrawi") (vers "0.1.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0x4njbxflw7j2xw17mvnlfw3w2qqf96213dkirxjzm7sj1i27mss")))

(define-public crate-sastrawi-0.1 (crate (name "sastrawi") (vers "0.1.1") (deps (list (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snailquote") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "13rd9z80hjn383dk4kbr6na1dsnkp9cfhwq8w12zjmwmxk3yf7zh")))

