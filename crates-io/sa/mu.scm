(define-module (crates-io sa mu) #:use-module (crates-io))

(define-public crate-samuel-0.1 (crate (name "samuel") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.3") (default-features #t) (kind 0)))) (hash "1q12kljdpmqmbvzzdi46m46xi5va5j154hbf26m3cys1h6d8z7sz")))

(define-public crate-samuel-0.1 (crate (name "samuel") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zg0w9hr2nxwp1zbp03whf5sd4lmgax200w8w05093mhlnq38qzl")))

(define-public crate-samuel-0.1 (crate (name "samuel") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "try_from") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jzszid2ii7bkhs52k9wry9d555zn0psmimr1qffp70iynar6syr")))

(define-public crate-samurai-0.1 (crate (name "samurai") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "11062k5y41biyp4wnfbv5m12qmlswyyqi9x95d2v9nbk58mpicvy")))

(define-public crate-samurai-0.1 (crate (name "samurai") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "01wy3xy4wp0zcg95qw4ng08d8iybr3cdis4hnmj62cayq17i2wdm")))

(define-public crate-samurai-0.1 (crate (name "samurai") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "00ysnp1h553g0kfxrdclmchc9dgy5yggwz600p6pwjva5ln3dbp3")))

(define-public crate-samurai-0.1 (crate (name "samurai") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1rqzfgmfp9vl3xldm18shh413c2gi2ijy0d4v933svlpmirvlfx1")))

(define-public crate-samurai-0.1 (crate (name "samurai") (vers "0.1.4") (deps (list (crate-dep (name "get_if_addrs") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "igd") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mgbvjwi9fn4v26znbpaynl22x8x05j85qwkcsimxl385nbjzvb1")))

(define-public crate-samurai-0.1 (crate (name "samurai") (vers "0.1.5") (deps (list (crate-dep (name "config") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "get_if_addrs") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "igd") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f0pj5d9a9i8agv7g2lx84ag7nk5nlbww8anq1c4l6z4c0xrd48z")))

