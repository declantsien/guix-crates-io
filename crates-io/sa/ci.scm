(define-module (crates-io sa ci) #:use-module (crates-io))

(define-public crate-sacio-0.1 (crate (name "sacio") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "flinn_engdahl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "geographiclib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04599438k1cw6km54pmyajqasw8nx1026d1i635507w2kb1882wf")))

