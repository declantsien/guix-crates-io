(define-module (crates-io sa mc) #:use-module (crates-io))

(define-public crate-samc-0.1 (crate (name "samc") (vers "0.1.0") (hash "046h4n1vndg1xnziyn6v0x63yqv9f9janvdyrsp1skng7q69f1x4") (yanked #t)))

(define-public crate-samcomp-0.1 (crate (name "samcomp") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1dz27c9cgvxiq46cq72n4psc3bbk8pxppwjs3xkzcdglrx5c3701")))

(define-public crate-samcomp-0.1 (crate (name "samcomp") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0864j479h6iw50hms0k3rkl4ffn8n35i5cvb9yg01dvhyy3a5izz")))

(define-public crate-samcomp-0.1 (crate (name "samcomp") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qla00adsi198yhl2rnrz72wfnxfardhqyynydar76f2l1qhvm1c")))

(define-public crate-samcomp-0.1 (crate (name "samcomp") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w79fzfbqsq08yjp19cmkjcgy93cr2lwlaj25q88xyh35mlwr77y")))

