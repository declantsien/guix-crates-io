(define-module (crates-io sa us) #:use-module (crates-io))

(define-public crate-sausage-0.1 (crate (name "sausage") (vers "0.1.0") (deps (list (crate-dep (name "indenter") (req "^0.3.3") (kind 0)))) (hash "1a4xvn0dxfwf6h8k5773s07azx36zw5k36hkhrxsnb4i2s71ww4x")))

(define-public crate-sauss-cramer-0.1 (crate (name "sauss-cramer") (vers "0.1.0") (hash "1m2r9pcxgz3srqpjp6kpinxczbn4wq46665cnjkry6y6s33glb4n")))

(define-public crate-sauss-cramer-0.1 (crate (name "sauss-cramer") (vers "0.1.1") (hash "0mfn47vmqzm95cq24wgv51p6vwjxnywg4p1zncl352fkvyyw3dzf") (yanked #t)))

(define-public crate-sauss-cramer-0.1 (crate (name "sauss-cramer") (vers "0.1.2") (hash "0qcd72x3s6yzs2lk2z13xvgvl3m4l760man6mcxxw8776hbdpxn6") (yanked #t)))

(define-public crate-sauss-cramer-0.1 (crate (name "sauss-cramer") (vers "0.1.3") (hash "05h9pbhqb5zdxy355b22az7bigrl1ygywrk3n96xm2zpbj0yq3ns") (yanked #t)))

(define-public crate-sauss-cramer-0.1 (crate (name "sauss-cramer") (vers "0.1.4") (hash "19y6p6c42dqa3li02kvkp363zkpqb6bl0a2ic7r21g4dwn0vjgzm") (yanked #t)))

(define-public crate-sauss-cramer-0.1 (crate (name "sauss-cramer") (vers "0.1.5") (hash "1pjv72da06hfqsi9rqhkpidd7qnmx5474ffdxjdlb4101rja1wfg") (yanked #t)))

(define-public crate-sauss-cramer-1 (crate (name "sauss-cramer") (vers "1.0.0") (hash "1pln9g1f89rpm2zs5kn4ayhs51aib8lj9gyfnklmbmxyxlb4dwrx")))

(define-public crate-sauss-cramer-1 (crate (name "sauss-cramer") (vers "1.0.1") (hash "0khp3vr4y5dj79s1s2vd1hhi2wk8n40brxddpkdbkh4sfx5n9kxm")))

(define-public crate-sauss-cramer-1 (crate (name "sauss-cramer") (vers "1.0.2") (hash "11jrcq8i5h8z2h5wqny1dxmwkdr9027sj3mhbq3pm73ka20xizsg")))

