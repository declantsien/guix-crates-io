(define-module (crates-io sa nk) #:use-module (crates-io))

(define-public crate-sankaku-0.1 (crate (name "sankaku") (vers "0.1.0") (hash "1bmpdig2d76hjdn6117y96d2n5s3jizbpn9prdg1f9j6fvajx8vh") (rust-version "1.77")))

(define-public crate-sankey-0.1 (crate (name "sankey") (vers "0.1.0") (deps (list (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0f7k459b63mxd2hss6b141yzjxh749lv2zcygsrcpaqky30hykrj")))

(define-public crate-sankey-0.1 (crate (name "sankey") (vers "0.1.1") (deps (list (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "069zac5883133np2n1bcimrshmxjn2p6gjg246xd0svzry5q83id")))

(define-public crate-sankey-0.1 (crate (name "sankey") (vers "0.1.2") (deps (list (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1cxxyxk60s22lcq51yvmj4mvxlrnrlv1phnds5a5f4jaz5rbmxqp")))

