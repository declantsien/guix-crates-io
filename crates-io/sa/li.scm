(define-module (crates-io sa li) #:use-module (crates-io))

(define-public crate-salign-1 (crate (name "salign") (vers "1.0.0") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "extabs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0wq8b41yh5nv1rz28bf1zwppip0ix1dvp0021rx7sxw1z4lxdjhr")))

(define-public crate-salign-1 (crate (name "salign") (vers "1.0.1") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "extabs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0a0an1d2vjqgxlnnmpl7swbhfafvici23hm32xl0m7qvpskllkws")))

(define-public crate-salih-0.1 (crate (name "salih") (vers "0.1.0") (hash "0v69c3fvyz3gdcg53snvjjybasr3i09fs7qs8dx41r2cnl48v71a") (yanked #t)))

(define-public crate-salih-0.1 (crate (name "salih") (vers "0.1.1") (hash "0c5r5069jn3a8yb2f03nip4357mmx84s4z41h8vgm4xcx71ivmjm") (yanked #t)))

(define-public crate-salih-0.1 (crate (name "salih") (vers "0.1.2") (hash "0gziqzqj81h29anrncaa694xfh51nmzc6dq2g9knkb6srbv60yd8") (yanked #t)))

(define-public crate-salih-0.1 (crate (name "salih") (vers "0.1.3") (hash "13kalgzixg5nnar6wb7hjdzlvpssgz8whyi27478cqkbq5584w2z") (yanked #t)))

(define-public crate-salix-0.0.0 (crate (name "salix") (vers "0.0.0") (hash "1rjxs66ig2dsdr7qd28c9b351fwm9brw8yyf5sfryhw885sbqghz")))

