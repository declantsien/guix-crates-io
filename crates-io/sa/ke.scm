(define-module (crates-io sa ke) #:use-module (crates-io))

(define-public crate-sake-0.0.1 (crate (name "sake") (vers "0.0.1") (deps (list (crate-dep (name "thrift") (req "^0.13") (default-features #t) (kind 0)))) (hash "0sx7ivcpx1f3x8ir1hbvclx5qg1nzls0rcmk1ihyrxw9cblwf8y5")))

(define-public crate-sake-build-0.0.1 (crate (name "sake-build") (vers "0.0.1") (hash "0jgllhh4i3rkrkysad3iz6fn329z813l9zav3zx26dydvg5k4kzx")))

(define-public crate-sake-derive-0.0.1 (crate (name "sake-derive") (vers "0.0.1") (hash "134nhagnj7v3yj6xs52yvqpimnmb3ajawnqbdd2q70sjkhwzls40")))

(define-public crate-saker-0.0.0 (crate (name "saker") (vers "0.0.0") (hash "1c1kwdi5mzz3f2n71vpq4jx45748124n9md5zjj6r4w5j88pwg8r")))

