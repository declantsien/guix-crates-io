(define-module (crates-io sa y_) #:use-module (crates-io))

(define-public crate-say_hello-0.1 (crate (name "say_hello") (vers "0.1.0") (hash "1j4ab0gbb71znz522jvyjrc550hzhcskvgss1jfi8mk14nixl4hm")))

(define-public crate-say_hello_useless-0.0.0 (crate (name "say_hello_useless") (vers "0.0.0") (hash "03vaivimb9v5psvsmilvv450bn9fkdcswv5ylgpsgl4zsrsq25kr") (yanked #t)))

