(define-module (crates-io sa xa) #:use-module (crates-io))

(define-public crate-saxaboom-0.0.0 (crate (name "saxaboom") (vers "0.0.0-alpha.1") (deps (list (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)))) (hash "10vv2lq0ylsixwp3sx1nssd6baz94q7kdhfxjxwfwcm6alfzbc1s")))

