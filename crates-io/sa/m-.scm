(define-module (crates-io sa m-) #:use-module (crates-io))

(define-public crate-sam-cli-0.1 (crate (name "sam-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xd9nq1imrfjv9yp9jyfjsngw9sybc0ribcb45kiig06dz4n2fyw")))

