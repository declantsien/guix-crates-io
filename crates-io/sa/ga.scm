(define-module (crates-io sa ga) #:use-module (crates-io))

(define-public crate-saga-0.0.0 (crate (name "saga") (vers "0.0.0") (deps (list (crate-dep (name "actix-web") (req "^3.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.125") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "09dy9ij3f7l05zb0j3845x6rs3kpxfdirfqi1w9b8vnqc1ns9596")))

(define-public crate-sagan-0.0.0 (crate (name "sagan") (vers "0.0.0") (deps (list (crate-dep (name "abscissa") (req "^0") (default-features #t) (kind 0)))) (hash "03x7n4134zw4pfmrrhqwyh62r4h3c87hz98l6wnpv3zi2n1i5kjx")))

(define-public crate-sagasu-0.1 (crate (name "sagasu") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (features (quote ("wrap_help" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1v29cggr1ipyp12fwjs32sp4svwd2b2vlcfw3k4j2fwayv8xgls7")))

(define-public crate-sagasu-0.2 (crate (name "sagasu") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (features (quote ("wrap_help" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1c41cbs9wlfb199mwcqm48r7v3lspm1d9vfhxdm2nj7504kgkk0g")))

(define-public crate-sagasu-0.3 (crate (name "sagasu") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (features (quote ("wrap_help" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "196gv42yiracnsbfpm00cj7f37wflz1ka5gm7dcz0j58q4rpp6rc")))

(define-public crate-sagasu-0.3 (crate (name "sagasu") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0wrhw4j20j7976f1jb2jcdm2ri5gl4j7d0asllf6i1z1yrlsp9wb")))

