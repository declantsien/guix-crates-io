(define-module (crates-io sa sh) #:use-module (crates-io))

(define-public crate-sash-0.1 (crate (name "sash") (vers "0.1.0") (deps (list (crate-dep (name "axum") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.25") (features (quote ("axum"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0sgpxbm0l0clrq9v2w3l1vwcj1rv4w2c0bki8ahvkg58wnb0nj2w")))

(define-public crate-sashay-0.1 (crate (name "sashay") (vers "0.1.0") (deps (list (crate-dep (name "erasable") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1b5hvcq0n87gywff04qfh4zvsa7vipdpy35p52n7j12z8r30dpxj") (yanked #t)))

(define-public crate-sashay-0.1 (crate (name "sashay") (vers "0.1.1") (deps (list (crate-dep (name "erasable") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0vawm03sfxsmh661j0sp6kj7z9zzpc0irj3jrxc5ghgih4rffzyh") (yanked #t)))

(define-public crate-sashay-0.2 (crate (name "sashay") (vers "0.2.0") (deps (list (crate-dep (name "erasable") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1w1r8g3ng57mf1r5i5m3ac4x20dnspv0hdv06rbr8md6z05911jj")))

(define-public crate-sashay-0.2 (crate (name "sashay") (vers "0.2.1") (deps (list (crate-dep (name "erasable") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "100as8pv0m6zxj90il8nwdqa1klxrpf0klrnspn58ns80nj51hym")))

(define-public crate-sashay-0.3 (crate (name "sashay") (vers "0.3.0") (deps (list (crate-dep (name "erasable") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0rwdi0q9vcab6lfj74zniw7p9fk748lyjdhza7gsa8yq9bjw7gsl")))

(define-public crate-sashay-0.3 (crate (name "sashay") (vers "0.3.1") (deps (list (crate-dep (name "erasable") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0z7wvh0lifnqipwwy8kpksbv7sa2nqg26pidr91z2d04a5qjq6ip")))

(define-public crate-sashay-0.3 (crate (name "sashay") (vers "0.3.2") (deps (list (crate-dep (name "erasable") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1lmnwfg6shc98jbrq05pwdq7wrfhhbzpapdj53g4bal0yfmy0y9s")))

(define-public crate-sashay-0.4 (crate (name "sashay") (vers "0.4.0") (hash "0y2jhybdzisf75ks3wna3h1vi1q3vhfag44dvskiq819zqg0ykf4")))

(define-public crate-sashay-0.5 (crate (name "sashay") (vers "0.5.0") (hash "1bkr0h21x8sd3xza3p2w09ig9b1vbwl9knnbxpff9k45xbpwyg68")))

(define-public crate-sashimi-0.1 (crate (name "sashimi") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0glz4dg3zgpwlcl6g04n5wqa3v21v38qr2kf72ifimf40di3ly9v")))

(define-public crate-sashimi-0.1 (crate (name "sashimi") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "10qpl9xqlfxbp9zx7l45pyhvjn7p1p84s83m8k8ajhg0mqhdhi3j")))

