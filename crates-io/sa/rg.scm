(define-module (crates-io sa rg) #:use-module (crates-io))

(define-public crate-sarga-0.1 (crate (name "sarga") (vers "0.1.0") (hash "07czs6qdjjiv9jfvv2j6dfiz9s73iis2f7kzn0zhs5fhgdzhj0li") (yanked #t)))

(define-public crate-sarge-2 (crate (name "sarge") (vers "2.0.0") (hash "0x1001r90cnp68g8lycnr0xh34fp5g0d635w4nlg4qgsw7hqr6i6")))

(define-public crate-sarge-2 (crate (name "sarge") (vers "2.0.2") (hash "0yydxn5vmbj892y5alrf89jhb2ddb1wbvinlc85vwcwqh5nilhxv")))

(define-public crate-sarge-2 (crate (name "sarge") (vers "2.0.3") (hash "1imh2yhph3qcwg801pr4l80ramw9n3h0xyj15g2wr716l9v90kng")))

(define-public crate-sarge-3 (crate (name "sarge") (vers "3.0.0") (hash "0kv6gjvpp5ja01zlb4l5bzx6igsmcn4p65wmllvb9x2c83lkxks2")))

(define-public crate-sarge-3 (crate (name "sarge") (vers "3.0.1") (hash "14n8q847fk3l6chwisdkd9kppx7dpghgdlv6y365b3v4mkz89ifa")))

(define-public crate-sarge-4 (crate (name "sarge") (vers "4.0.0") (hash "1klp0vjnwkrvnifwvd7npd0cpcpmyz0bcj0fjjhhbds08gvfy96v")))

(define-public crate-sarge-4 (crate (name "sarge") (vers "4.0.1") (hash "01vkwn1gi01245m8p6qrs622c7gb54ahh0k1llf345j47v4hys3h")))

(define-public crate-sarge-4 (crate (name "sarge") (vers "4.0.2") (hash "0h34kwbpswv7g4f9fxiry5q11gaz4wadxq48qdf2sw9pd2afksca")))

(define-public crate-sarge-5 (crate (name "sarge") (vers "5.0.0") (hash "1g3w6hl2493fbny2gxd5jns5xcy2h8xqwwr9ygn2m8wwpqd36ic2")))

(define-public crate-sarge-5 (crate (name "sarge") (vers "5.0.2") (hash "1z6idmj7cikra432j86l0jljsil4n9j6sr9d8filzpgi4yfdm8by")))

(define-public crate-sarge-6 (crate (name "sarge") (vers "6.0.0") (hash "1qb5hpflqwharm3k6w8w3czvl6airz9kw0j68xmr74pmi9by7jk3") (features (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-6 (crate (name "sarge") (vers "6.0.1") (hash "03ck9rgcy79j68rqwyqsz3xm7l0rvvxvi13gm5gz2sjkz8shsjk2") (features (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-6 (crate (name "sarge") (vers "6.0.2") (hash "0hjgk8wndpwdpij1br5j4g0473mg0ydnyvrl3h7qh1idck2fp7q4") (features (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7 (crate (name "sarge") (vers "7.0.0") (hash "0sx6dyifzyaf3bbqhb6gy1gj4xw2sri1pwaa4hazaa9calp5a92l") (features (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7 (crate (name "sarge") (vers "7.0.1") (hash "1gyn47sgzjrkfjyc6vh093qk824338pa2nqdmq9fyzq28aqx1gig") (features (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7 (crate (name "sarge") (vers "7.0.2") (hash "157nz9fl14kz9l76zim7qg32ck2mp8sv0smcwn1xwzdm6azgxy3q") (features (quote (("macros") ("default" "macros"))))))

(define-public crate-sarge-7 (crate (name "sarge") (vers "7.2.0") (hash "0xc4cvlvn4ryvb6ky4kggblbhaxcgfhdj1d5agp556s43wzn8byp") (features (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sarge-7 (crate (name "sarge") (vers "7.2.1") (hash "1qnrcbg4743w9lzaykdria71m9h6lnpxnhh4hd3xsbyaj17iv08g") (features (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sarge-7 (crate (name "sarge") (vers "7.2.2") (hash "09z5x1ahp2knwbb63klm9l3sd7v4nd4p65h3m48ll8f63rm5cm55") (features (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sarge-7 (crate (name "sarge") (vers "7.2.3") (hash "0j40zrfyd9frwbdyhdbr2a2cb11c1zfsharxffm2vpi944wcb3a1") (features (quote (("macros") ("help") ("default" "help" "macros"))))))

(define-public crate-sargo-0.0.0 (crate (name "sargo") (vers "0.0.0") (hash "13aj46yan770s50diyqnzgbpq402p08vqqnhara0fvmnpqza4qk6")))

(define-public crate-sargparse-0.1 (crate (name "sargparse") (vers "0.1.0") (hash "0rpszqczw84jm92l4m6da1i6nzlw5mxfgd55l0f34zkdq75zd4lv")))

(define-public crate-sargparse-0.0.2 (crate (name "sargparse") (vers "0.0.2") (hash "1kdjhbabbgigp5saq6qh0s4q4r4ic073hg4qgrlbr8dckwgznx2y")))

(define-public crate-sargparse-0.2 (crate (name "sargparse") (vers "0.2.0") (hash "0r0rdaqdnxk8874w864imwjhzs0nk3793svg999gil5nyzmnvw95")))

(define-public crate-sargparse-0.2 (crate (name "sargparse") (vers "0.2.1") (hash "0qdmzl537dp18w35y8k2zj8idhyv9l5iq2i6ss6194ibazsp49ra")))

(define-public crate-sargparse-0.2 (crate (name "sargparse") (vers "0.2.2") (hash "07jgzlymayy6n4bq3n9m9xs33xh5ml5fc78fca1n5bfky3dq6zjx")))

(define-public crate-sargs-0.1 (crate (name "sargs") (vers "0.1.0") (hash "0iaj61r8zsw3rwgrby8lzy919dfsm20f39vz0bc0slk98j07d2a9")))

(define-public crate-sargs-0.2 (crate (name "sargs") (vers "0.2.1") (hash "0s1ikbqrq0xsm0m0bl2bdd9151cwpbpg60zii9945dm301fggxkx")))

(define-public crate-sargs-cmd-0.1 (crate (name "sargs-cmd") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bwv7q5ybr4q3lghllh5j3zs4mlajyslqp1snvgr3fpsryabwg5v")))

(define-public crate-sargs-cmd-0.1 (crate (name "sargs-cmd") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1saah5l6j998b3xkb0hhz44dqzxbb3zis5jmpxzqiphikhpsswb2")))

(define-public crate-sargs-cmd-0.1 (crate (name "sargs-cmd") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bksd76ra877lxrz4l5vnly4pd57422ljzvzfc6cxnlyw7zjih9l")))

