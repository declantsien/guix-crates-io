(define-module (crates-io sa qi) #:use-module (crates-io))

(define-public crate-saqib_fun-0.1 (crate (name "saqib_fun") (vers "0.1.1") (hash "1d7d03s9gk5hx2f9v7j4v03dsdbdjrdvbmphi53yzxa96xqgchlk")))

(define-public crate-saqif-0.1 (crate (name "saqif") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "druid") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "druid-widget-nursery") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "headless_chrome") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "1xf2fhscnqx33whphqj28hxh483zqzpaw6qlmj8lf89kly4drgg7")))

