(define-module (crates-io sa lu) #:use-module (crates-io))

(define-public crate-salus-0.0.1 (crate (name "salus") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.51") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.15") (features (quote ("server" "http1" "http2" "runtime"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1jv5d4iblrhkxhdbcrgfm7c43g1brlqn4pldn6y0814hfgw1fk3r")))

(define-public crate-salut-0.0.0 (crate (name "salut") (vers "0.0.0") (hash "0clpbpp35mbz77jiqlbz7pvi83b28ylx0nqbz3lqmnhf766dwq6f")))

