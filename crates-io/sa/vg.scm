(define-module (crates-io sa vg) #:use-module (crates-io))

(define-public crate-savgol-rs-0.1 (crate (name "savgol-rs") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "lstsq") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "polyfit-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "subprocess") (req "^0.2.9") (default-features #t) (kind 2)))) (hash "0w677ixgif956mqhrmnprsgvpy90wm2k2dmh2lz20vcn5qb4dgvd")))

