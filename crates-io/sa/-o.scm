(define-module (crates-io sa -o) #:use-module (crates-io))

(define-public crate-sa-ord-0.0.1 (crate (name "sa-ord") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "built") (req "^0.5") (features (quote ("git2"))) (default-features #t) (kind 1)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "16dn0pngnkh35xxzcg662r9r6ikgv80zavzdrxdh9xaj0pbnahxg")))

