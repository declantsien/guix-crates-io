(define-module (crates-io sa ca) #:use-module (crates-io))

(define-public crate-sacabase-1 (crate (name "sacabase") (vers "1.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "13f4z1gs900b9shxp2wby05yvvvx4p0iz2i11sql43idwfjmdy55")))

(define-public crate-sacabase-2 (crate (name "sacabase") (vers "2.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "113s4s39s043w2misq7y7ac5yyqzpkw051lh9nsqpmz3dhyzr0wq")))

(define-public crate-sacana-1 (crate (name "sacana") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.4") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "websocket") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "01g2lgrxjk72ia19991i7i72446sgz1znflcglkrhsjmrwnn9336")))

(define-public crate-sacapart-2 (crate (name "sacapart") (vers "2.0.0") (deps (list (crate-dep (name "divsufsort") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "sacabase") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0w9r099cx9ybzd7c17f3b02x5hb41ylgx16s636k00rhgwzv1l9m")))

