(define-module (crates-io sa xx) #:use-module (crates-io))

(define-public crate-saxx-0.1 (crate (name "saxx") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "10d1jqfxm73s0282nyf6qa91wa4xqfy61y1haim086iw266kgmna")))

(define-public crate-saxx-0.1 (crate (name "saxx") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ra8gri535kbkjxccga60mpnsj0gb4xn1jvjhy6akdfg62framlz")))

(define-public crate-saxx-0.1 (crate (name "saxx") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pkn47lzvx4wg8kwr5cii1fqdwngc24qq2d2h7a8yiigscp7rfzb")))

