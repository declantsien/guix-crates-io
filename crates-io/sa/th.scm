(define-module (crates-io sa th) #:use-module (crates-io))

(define-public crate-sath-0.1 (crate (name "sath") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (default-features #t) (kind 0)))) (hash "1ygiqa80d6f9wcr5y5b9s66qvdbnfrdis9i3ccrym9hh78hdqd7g") (features (quote (("use-f64"))))))

