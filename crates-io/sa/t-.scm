(define-module (crates-io sa t-) #:use-module (crates-io))

(define-public crate-sat-rs-0.0.1 (crate (name "sat-rs") (vers "0.0.1") (hash "0i1g3zhw55g7hj7l6pzbk8xhyi34vqxha1fpl7kxphzisikfcr0g")))

(define-public crate-sat-rs-0.0.2 (crate (name "sat-rs") (vers "0.0.2") (hash "1jc1xlgfjx3a2dr1715ss8xvwmzxbcjb5jjqb8p7kc2jkdip1ycx")))

(define-public crate-sat-rs-0.0.3 (crate (name "sat-rs") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bz3b9vk5fzm4rpg1mqyck9xsak8lbz61282vvb3i7zj790chnkh")))

(define-public crate-sat-solver-0.1 (crate (name "sat-solver") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "08y5djxri5722jwzkb77m8i95q2ivsri621aw60mc9fwfb91p9sp")))

(define-public crate-sat-solver-0.1 (crate (name "sat-solver") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "priority-queue") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0pxw7b1jjhm7nkd4z0aw8ks4k8h8bvk1mz50q05497kiszsskcda")))

