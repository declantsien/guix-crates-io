(define-module (crates-io nr f2) #:use-module (crates-io))

(define-public crate-nrf24-rs-0.1 (crate (name "nrf24-rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "08svprlpaz4j86hgn2p9lcixrp8bjfpl1h5n2x66m8jra2jywnzp") (features (quote (("micro-fmt" "ufmt"))))))

(define-public crate-nrf24-rs-0.1 (crate (name "nrf24-rs") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1hk4ha5rn0wgfz3s1a2v870j7zadl3hy1jzddvlrgbzlkghk3080") (features (quote (("micro-fmt" "ufmt"))))))

(define-public crate-nrf24l01-0.1 (crate (name "nrf24l01") (vers "0.1.0") (deps (list (crate-dep (name "spidev") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sysfs_gpio") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "06a6380mnjfyqvbwxk4mrihv5xgzj8l44rqsinirnchzsp5m75pj")))

(define-public crate-nrf24l01-0.2 (crate (name "nrf24l01") (vers "0.2.0") (deps (list (crate-dep (name "rppal") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "spidev") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sysfs_gpio") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)))) (hash "07zgg2kgnnr3f8ds5nv76gbzdvl718xypfifbwk8phgp631fn9q6") (features (quote (("rpi_accel" "rppal") ("default" "sysfs_gpio"))))))

(define-public crate-nrf24radio-0.1 (crate (name "nrf24radio") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "0c489mkbqmwxvjzwg2bqka1903mjgdlfk87729p7fy1pw037bp7j") (features (quote (("log" "defmt") ("default" "log"))))))

