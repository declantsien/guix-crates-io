(define-module (crates-io nr bf) #:use-module (crates-io))

(define-public crate-nrbf-0.1 (crate (name "nrbf") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "const-str") (req "^0.5.7") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.34.3") (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "17yk2yi8zryp1drcl2r69grkvx79c6i7hv0vj0sjlkpb7rc1l28k") (v 2) (features2 (quote (("serde" "dep:serde" "rust_decimal/serde"))))))

