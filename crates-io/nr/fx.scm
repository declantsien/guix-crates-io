(define-module (crates-io nr fx) #:use-module (crates-io))

(define-public crate-nrfxlib-0.1 (crate (name "nrfxlib") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nrf91") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nrfxlib-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1g707lr47pa51sjz49840dbgn03wcwpl0a3jlig21n4ksd4gixz8")))

(define-public crate-nrfxlib-0.2 (crate (name "nrfxlib") (vers "0.2.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nrf91") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nrfxlib-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bch9gv1fyap9wkgkxn5bi8ygyv0lqa76y5mgdknz2vc5r80d0rs")))

(define-public crate-nrfxlib-0.2 (crate (name "nrfxlib") (vers "0.2.2") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nrf91") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nrfxlib-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ibgkfbll447a3bfcv837d8v0xkq5wn0lkphwri3p1z6f7d17s6b")))

(define-public crate-nrfxlib-0.3 (crate (name "nrfxlib") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nrf91") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nrfxlib-sys") (req "^1.1.0-rc2") (default-features #t) (kind 0)))) (hash "1y9r6f2lfapahk4xxfqqf5pchmx161x9xcbk3awmlyw3kh5hx1cb")))

(define-public crate-nrfxlib-0.4 (crate (name "nrfxlib") (vers "0.4.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nrf91") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nrfxlib-sys") (req "^1.1.0-rc2") (default-features #t) (kind 0)))) (hash "1dxs1i3n1c2vm2s5bqb2y9ahycs0q86fibq32hn2bw68x7gnjk9y")))

(define-public crate-nrfxlib-0.5 (crate (name "nrfxlib") (vers "0.5.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nrf9160-pac") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "nrfxlib-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0d4sg89mnavpz7pj5dld8sbaw7k9n7mf16kmvpfrs17qq3nzylc7")))

(define-public crate-nrfxlib-0.6 (crate (name "nrfxlib") (vers "0.6.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "linked_list_allocator") (req "^0.9.0") (features (quote ("use_spin"))) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nrf9160-pac") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "nrfxlib-sys") (req "=1.5.1") (default-features #t) (kind 0)))) (hash "1lvmp8hprbaixsn1aa5aypdbj330abhcb6q778q8z4pan1wx8k4i")))

(define-public crate-nrfxlib-sys-0.1 (crate (name "nrfxlib-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "1gcvj94z7ckqw1s21fx0j80bh93vngyzvlcg6daqvqa4c7g26vjs")))

(define-public crate-nrfxlib-sys-0.1 (crate (name "nrfxlib-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "0ck5a3cwaqws5lnwyxddv6lf2kdyyhl6809kh4z02nv805hiax41")))

(define-public crate-nrfxlib-sys-0.1 (crate (name "nrfxlib-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "0s5v2lljfdmdd5cxv9136zmfj2h6gkkm3rc498p2l0skv6fgfjgm")))

(define-public crate-nrfxlib-sys-0.1 (crate (name "nrfxlib-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "15y955j9f0fkca00rzf1nhnn6slm78h516bkd7mprz429pcwvajk")))

(define-public crate-nrfxlib-sys-0.1 (crate (name "nrfxlib-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "0ihp3hj2fm6j9zhk9zhywc9wm5flx8kldbwhic0a46s0w988y8f1")))

(define-public crate-nrfxlib-sys-0.1 (crate (name "nrfxlib-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "13p9s3vr6rx2rnb9xkc1lqlhc9n1dmzk85fy052g8ilazbc6cxbd")))

(define-public crate-nrfxlib-sys-0.1 (crate (name "nrfxlib-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "110dnz9p2wx0zn8fmp66fwx83qrcnfy5plbpk9vrvhzrfplkq9ly")))

(define-public crate-nrfxlib-sys-0.2 (crate (name "nrfxlib-sys") (vers "0.2.0") (hash "029n2n51znqfynzryzhkm33a9xrvsahk6iwa93blvswaziwcfpcl")))

(define-public crate-nrfxlib-sys-1 (crate (name "nrfxlib-sys") (vers "1.1.0-rc2+rel1") (hash "0x0wg665zwswnhdd3m7az7mw14qyiisflvbvrcgp0dhhicbj4b5k")))

(define-public crate-nrfxlib-sys-1 (crate (name "nrfxlib-sys") (vers "1.1.0-rc3+rel1") (hash "0vmkdgrl7a71nx0zrzls76niqkxa7mbzn4wm51qy5rqnkf4xap69")))

(define-public crate-nrfxlib-sys-1 (crate (name "nrfxlib-sys") (vers "1.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1v32xdfyp1d9gh7an4sy11qbgigcj8cw68w4q5816zvq9ji3d097")))

(define-public crate-nrfxlib-sys-1 (crate (name "nrfxlib-sys") (vers "1.4.2") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0ywvpsdc8x75xmy72if7b2qblr8pnj5gbrfcxvv5khrq38irzp93")))

(define-public crate-nrfxlib-sys-1 (crate (name "nrfxlib-sys") (vers "1.5.1-rc1") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0r5j86hq2i513171wzdrl1x69dihys2bvcckawyz47zkfk1x9wbj")))

(define-public crate-nrfxlib-sys-1 (crate (name "nrfxlib-sys") (vers "1.5.1") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1jcby1ay512wwiy257745pyq1ncdd8qrf698cjhccqabqrd45s7h")))

(define-public crate-nrfxlib-sys-2 (crate (name "nrfxlib-sys") (vers "2.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0gqfl77s9lmp0kk5whc2mbc4hssz0r1p8sdpazddzxy53mki3c0f") (features (quote (("llvm-objcopy") ("default" "llvm-objcopy") ("arm-none-eabi-objcopy"))))))

(define-public crate-nrfxlib-sys-2 (crate (name "nrfxlib-sys") (vers "2.4.2") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "llvm-tools") (req "^0.1.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1mzdk4hd74gd308zxrnjcy2ynsg5cbv6j0q2v8s5hzdab2g2492m") (features (quote (("default" "llvm-objcopy") ("arm-none-eabi-objcopy")))) (v 2) (features2 (quote (("llvm-objcopy" "dep:llvm-tools"))))))

