(define-module (crates-io nr s-) #:use-module (crates-io))

(define-public crate-nrs-commit-0.1 (crate (name "nrs-commit") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1ys1rplw07gbmiqsjvd3ai9dvj90yhyi6gn07id110h0095vxzrn")))

(define-public crate-nrs-commit-0.2 (crate (name "nrs-commit") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0kc949ayry01x5qpyi95lzpc7ppmahsd7n0s30siqvnmcsrdmz15")))

