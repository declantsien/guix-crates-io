(define-module (crates-io nr c-) #:use-module (crates-io))

(define-public crate-nrc-protobuf-2 (crate (name "nrc-protobuf") (vers "2.8.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1xqrlgfdyknrq5amj44ahis12z1wa2szc5j16r5lwcgzw7d0zh89") (features (quote (("with-serde" "serde" "serde_derive") ("with-bytes" "bytes"))))))

(define-public crate-nrc-protobuf-codegen-2 (crate (name "nrc-protobuf-codegen") (vers "2.8.0") (deps (list (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "= 2.8.0") (default-features #t) (kind 0) (package "nrc-protobuf")))) (hash "110kjn3y7hdh7m0x1qhz5li5fnqcr5xwy2an0npafsdn1w7mvns5")))

