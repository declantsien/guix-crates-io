(define-module (crates-io nr fi) #:use-module (crates-io))

(define-public crate-nrfind-0.1 (crate (name "nrfind") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "0mbghfz48171ycb09kyqn8b8ky5ds4yld31ddpdq5af3qmgxbnzb")))

(define-public crate-nrfind-0.1 (crate (name "nrfind") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "1ld5mvd4kpd9zrjp53zdj7m32qkcnk712lgxyp94y3wc85wyl8y3")))

(define-public crate-nrfind-0.2 (crate (name "nrfind") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "1c8afjx8yjxafkf09s8pfrrdsvr400q85fc26xrsbzi5g4kg1rkz")))

(define-public crate-nrfind-0.2 (crate (name "nrfind") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "0cg15jfh5jl0r5rqrknmvjv50s5h18nsbaq6vg5g42gh1jzkvl2a")))

(define-public crate-nrfind-1 (crate (name "nrfind") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0vlmx98j9jhb63i8523mnbwri2z1qsrynzbmb7v6b86m9l9v2gb5")))

(define-public crate-nrfind-1 (crate (name "nrfind") (vers "1.0.1") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0kp610xzy0kp3nrws3ldvwhj9r6wdlllw7m9g9i4scw9cs70v4k0")))

(define-public crate-nrfind-1 (crate (name "nrfind") (vers "1.0.2") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "11wazcqz16npqr6070nnr2wpnk4p96pdqw2rjwazvcj12nzqarnd")))

(define-public crate-nrfind-1 (crate (name "nrfind") (vers "1.0.3") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "18kzlfl4gmbyfncsqyl217clbbacjnm8yf8bp9wgh52sq6ddy1bk")))

