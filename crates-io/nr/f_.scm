(define-module (crates-io nr f_) #:use-module (crates-io))

(define-public crate-nrf_dfu-2 (crate (name "nrf_dfu") (vers "2.0.1") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0wnh5f98ddj3i76svxslac9hk4s3sygj6dc0sxks4csi5ssiv9w9")))

