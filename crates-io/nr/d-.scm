(define-module (crates-io nr d-) #:use-module (crates-io))

(define-public crate-nrd-sys-0.1 (crate (name "nrd-sys") (vers "0.1.0") (deps (list (crate-dep (name "sysreq") (req "^0") (default-features #t) (kind 1)))) (hash "19nxfgfkwj1nirdxkkrfvb9sc2zys981g8az2rn9qfxl30pwn7am")))

(define-public crate-nrd-sys-0.2 (crate (name "nrd-sys") (vers "0.2.0") (deps (list (crate-dep (name "sysreq") (req "^0") (default-features #t) (kind 1)))) (hash "153fb2pd9biqwcrmflcfxkdy83rrhvg4nmlriwf4jdjmzs3bjvn2")))

