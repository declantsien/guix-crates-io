(define-module (crates-io ks ch) #:use-module (crates-io))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0mxhd2w8x1k0qmlbz57bvdcv7n7yw9vk9rmzvqmwfx5fswakmwmz")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.1") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1ghc7ld9vz4pal1raa6mx6mf9mr2pzsgb9bzgxgqzdws098a94y8")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.2") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1pmjc97v74vxvxjixr08pz6p1jy1dbx4dzv7simnqlhvsnjsmms4")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.3") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0j1z9cfjnnrxaghyl8yvk6p86sydd79zlaz37pm58npmm0adxjpg")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.4") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1agkc1qshshqngaxs0bdghndi4bbp519r3mpb0bqbk99a9grys42")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.5") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0crfw79cdnl04xqrm5ilxgn4ka73hcs35ia8xmak35qllbbzwg0a")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.6") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0z94zv8dv2s61654d8wzyd6m7ajg5kdj6smh93ylzfp7pi1hxyz0")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.7") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1l9144kjifw5b9p2lad073ki229gzvz60lc0vj1nsp4pbq6ypdpj")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0-alpha.8") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0bpkg8ysxzqyb8wl2xfb2d0vn0ra17nwildii5nna9d29sapvrfa")))

(define-public crate-kschoonme-identity-pb-1 (crate (name "kschoonme-identity-pb") (vers "1.0.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0nmax5jmfv5z092f9x3ngvga699zs4x7q42bblm43qbz01nns22f")))

(define-public crate-kschoonme-notifications-pb-0.1 (crate (name "kschoonme-notifications-pb") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "06vig2vvh7l2lb29ls2vvgl8zmlw9p34kk16ypvsq0s0y9nd2364") (yanked #t)))

(define-public crate-kschoonme-notifications-pb-0.1 (crate (name "kschoonme-notifications-pb") (vers "0.1.1") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "106f1zd6glir5yc2giw6a9ybvxrbcqhldb86rb6a41dkl1akg0zp") (yanked #t)))

(define-public crate-kschoonme-notifications-pb-0.1 (crate (name "kschoonme-notifications-pb") (vers "0.1.2") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1wm3a8mf42as9s7i3as22d5vn74d2ppibvr9lmf530l01jjr4jpw") (yanked #t)))

(define-public crate-kschoonme-notifications-pb-1 (crate (name "kschoonme-notifications-pb") (vers "1.2.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0dax4c6w4g9m9jwxaj3m7cfa7gm0911b9y8jypx2mwx581q43yd2") (yanked #t)))

(define-public crate-kschoonme-notifications-pb-1 (crate (name "kschoonme-notifications-pb") (vers "1.3.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "16j4ga5h7b9h29xmr8pnwz6qxfrkibpg0lanv896g71rf9vlda07") (yanked #t)))

(define-public crate-kschoonme-notifications-pb-1 (crate (name "kschoonme-notifications-pb") (vers "1.0.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1i10bsqps3ndjrz9lp6l4kj8c1smla1sdy7p98ynkm1ngazn9305") (yanked #t)))

(define-public crate-kschoonme-notifications-pb-1 (crate (name "kschoonme-notifications-pb") (vers "1.3.1") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1ldg232cxj3yiar5hc4f9f9ald4ich1bm9d9vgk7y6bpdj3rzpmh")))

(define-public crate-kschoonme-notifications-pb-2 (crate (name "kschoonme-notifications-pb") (vers "2.0.0-alpha.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1fqq8f0wcyx388dvcdphmpy593lpc75j6dlfqq9v3kak28qk947q")))

(define-public crate-kschoonme-notifications-pb-2 (crate (name "kschoonme-notifications-pb") (vers "2.0.0-alpha.1") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "0yscflp1fd619ddxq80zc7rfsa1qbqrwiz0jzpwkggshn38dz797")))

(define-public crate-kschoonme-notifications-pb-2 (crate (name "kschoonme-notifications-pb") (vers "2.0.0") (deps (list (crate-dep (name "prost") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.3.0") (default-features #t) (kind 1)))) (hash "1lapzwq49mxxggw3xs8dvimpim09f699nnq1irqzc06q94x5px3x")))

