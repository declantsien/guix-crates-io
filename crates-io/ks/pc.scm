(define-module (crates-io ks pc) #:use-module (crates-io))

(define-public crate-kspconfigtool-0.1 (crate (name "kspconfigtool") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "string-builder") (req "^0.2") (default-features #t) (kind 0)))) (hash "0101l5v31ijnhl2yfwkx1bvdaq1bb528vc7b7da1dp7f611way3p") (rust-version "1.68.2")))

