(define-module (crates-io ks pl) #:use-module (crates-io))

(define-public crate-ksplang-0.1 (crate (name "ksplang") (vers "0.1.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1w8fvsd1r28739b24nk95d9p4jz322hm4n59lpqwa4ndnp6walwz")))

