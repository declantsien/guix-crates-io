(define-module (crates-io ks ni) #:use-module (crates-io))

(define-public crate-ksni-0.0.0 (crate (name "ksni") (vers "0.0.0") (deps (list (crate-dep (name "dbus") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.3") (default-features #t) (kind 1)))) (hash "1wi54wfcniq1yz72mb9rnphbzafbs9hjyvcws3s2nnl7fxhp3b9c")))

(define-public crate-ksni-0.1 (crate (name "ksni") (vers "0.1.0") (deps (list (crate-dep (name "dbus") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.4") (default-features #t) (kind 1)))) (hash "0bzs6q3w4zd2p9hq27cf0q69dnxirqzk948mzy9qk3wf11pspri0")))

(define-public crate-ksni-0.1 (crate (name "ksni") (vers "0.1.1") (deps (list (crate-dep (name "dbus") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0md8j1g7bjb5ky8dca0qa7yg778jpjrvvk67f7ibzcgihh6n73mc")))

(define-public crate-ksni-0.1 (crate (name "ksni") (vers "0.1.2") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "dbus-tree") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i1gwz7li7whxkcsf1w60whzk3asy84yg93rgs0201c6mgisfizp")))

(define-public crate-ksni-0.1 (crate (name "ksni") (vers "0.1.3") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "dbus-tree") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "19xk6y1zfgwh8piv0514g54bkkp4qgwdjl9dh20fmi5a4mqn4zbc")))

(define-public crate-ksni-0.2 (crate (name "ksni") (vers "0.2.0") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "dbus-tree") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w3k14kk7izypczg8wsrnw6ycgaahcd8x7981806lmvbd8a8dds8")))

(define-public crate-ksni-0.2 (crate (name "ksni") (vers "0.2.1") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "dbus-tree") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "030x4w6xqcm6f7b8gl7w4vsh0yzqii9l82wh176jplmwhvs9q6xk")))

(define-public crate-ksni-0.2 (crate (name "ksni") (vers "0.2.2") (deps (list (crate-dep (name "dbus") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "dbus-codegen") (req "^0.9") (default-features #t) (kind 1)) (crate-dep (name "dbus-tree") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ny832wm7nd2kaaxhmdrq5cd0vy1q1dd7f42air5avh1vl5k2d29") (rust-version "1.58")))

(define-public crate-ksni-dummy-0.1 (crate (name "ksni-dummy") (vers "0.1.0") (hash "0drgb816yj5wgxpsi2gzgpfyw0fsa71h6gn3p20k0kv0x77halvh")))

