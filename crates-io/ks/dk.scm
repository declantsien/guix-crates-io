(define-module (crates-io ks dk) #:use-module (crates-io))

(define-public crate-ksdk-0.0.1 (crate (name "ksdk") (vers "0.0.1") (hash "15pzn53pwix532nk2s8690hwx03mlxd9nh056k7nfzb83b9gh480")))

(define-public crate-ksdk-core-0.0.1 (crate (name "ksdk-core") (vers "0.0.1") (hash "12hp2hhsq0kq753jk1p0jd14jpfi54lavkq3kvamk8mgzzw7y3gh")))

