(define-module (crates-io ks td) #:use-module (crates-io))

(define-public crate-kstd-0.0.1 (crate (name "kstd") (vers "0.0.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0yhswmxld9vc9f9bcfn71jwz5bbn0fi1da2gx2ni10xnp3q29fwv")))

(define-public crate-kstd-0.0.2 (crate (name "kstd") (vers "0.0.2-rc.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0adfkjay00jbqlh376kjjl02l6zz6h8b2sh0wy0laiqqq1fm82r5")))

(define-public crate-kstd-0.0.2 (crate (name "kstd") (vers "0.0.2") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0fdws402pjibmp47743s7ych7nighqgh95dlbm471dmsx4f3q87c")))

(define-public crate-kstd-0.0.3 (crate (name "kstd") (vers "0.0.3") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "05md35dg55j45xh3hk0cx3z6hh5sxn6xpssdnglmpavzqbi5d1m9")))

(define-public crate-kstd-0.0.4 (crate (name "kstd") (vers "0.0.4-rc.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0hk9833fd19vqd0kcc2q5fh0d07rl3xjfxmrm0hz0plisnx6hvqf")))

(define-public crate-kstd-0.0.4 (crate (name "kstd") (vers "0.0.4-rc.2") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1hcn75hw700qaym7ahn7l87h24hpbf2a820ac8p0wq5gqb5m6imj")))

(define-public crate-kstd-0.0.4 (crate (name "kstd") (vers "0.0.4") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0mgr46xcyic1ys5blk89wfrq9v45sq44pw1xpk2zvvqljzb1vbll")))

