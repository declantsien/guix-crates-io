(define-module (crates-io ks to) #:use-module (crates-io))

(define-public crate-kstool-0.1 (crate (name "kstool") (vers "0.1.0") (hash "1hxcxpcjp0i19xzlly141j6fa08274bh0nxx1mxhqv55yzsjiqic")))

(define-public crate-kstool-0.1 (crate (name "kstool") (vers "0.1.1") (hash "0a0zgl0pki8ixdsnvx8b8mysyrcqiv3a5d9rkli112xqj5ny7rdg")))

(define-public crate-kstool-0.1 (crate (name "kstool") (vers "0.1.2") (hash "071lrmbaypljp2pwx9bsmfmr6m1mwswygyb87hrpil2sk1yfdidm")))

(define-public crate-kstool-0.2 (crate (name "kstool") (vers "0.2.0") (deps (list (crate-dep (name "sqlx") (req "^0.7.0-alpha.3") (features (quote ("sqlite"))) (optional #t) (default-features #t) (kind 0)))) (hash "01h063famm4msfpa329fzkshspfj7pk0r2ijam0wj7x0qifgfal9")))

(define-public crate-kstool-0.2 (crate (name "kstool") (vers "0.2.1") (deps (list (crate-dep (name "sqlx") (req "^0.7.0-alpha.3") (features (quote ("sqlite"))) (optional #t) (default-features #t) (kind 0)))) (hash "0jk1dcxnd9i3ksfdqngzddkpk0y8iwq2v2m0jw71srybzvijl0nk")))

(define-public crate-kstool-helper-generator-0.4 (crate (name "kstool-helper-generator") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1i1x88kacfx3cqq8ikzc1z3dc922kba7zzfahy7z90ys1xzx1kr3")))

(define-public crate-kstool-helper-generator-0.4 (crate (name "kstool-helper-generator") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0za5xy9rmgcczah3wk7y38s2hrmwiha6py0r37inq1izq708wwhs") (yanked #t)))

(define-public crate-kstool-helper-generator-0.4 (crate (name "kstool-helper-generator") (vers "0.4.3") (deps (list (crate-dep (name "fancy-regex") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0655zxpsl7ndj92y781ypl3qh3i0fhjlpj36fnihymvsbz07p93a")))

(define-public crate-kstool-helper-generator-0.4 (crate (name "kstool-helper-generator") (vers "0.4.4") (deps (list (crate-dep (name "fancy-regex") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1ypsqj90wj9s8b9ivyn626kbn1c7bldpndcl37z1ivw33s2gryrk")))

