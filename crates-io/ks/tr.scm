(define-module (crates-io ks tr) #:use-module (crates-io))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hnn4rbs3mzd0d995129y426wqc8zyjc4ghx6blxbzpy8kb3abig")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0df0wvhjz0nqc5nbrsnw7p0b8lj5krwgi44cb5475mbxgf7mf7ba")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gqmrg8030hyjyg0qkb3d1z1dm0c6kmn758p7avkznlg48qk5m04")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ghlf5cn676xz9ng55jbjbqn5mfqv76s37bk9ks9y7byl8ifv6ys")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.6") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nliy1496d4qaqn1swg0kr3ywalrcpwp7pwwyn8nqvfj34ihb7fa")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.7") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "19822qqkgjg1i4zd1d5h5p2i0pfxk362hqv9k3bwh7bxyncdh6fa")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.8") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sb5bs6fawhgg8gax81accfbi3l9r1by169gcp4wnyjrqx45g688")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0-alpha.9") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "06cfcgsyydn2rxf9i40vgyq8i2wbkcb8z3b0nprjp495ab0pzs1q")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "02fnfl4hgmvl69xp53fvfq81ajn9413n7dihj0a2xlhg219xz0k3")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "13mm31pnnkjak3lcv965wng355mspix5r4vf6kvdsmhdp2z31g7v")))

(define-public crate-kstring-0.1 (crate (name "kstring") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "019lla1dp2gplck8rp86frgr4cnsqg44n2ynwwg644a1wbc33b0y")))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d5l025g1g0sdh4k1yfi80ibch7jl44b871j8n4ihpvqdm3hay65")))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "059j6kcibdrmc2my4hagy62955ivr7vdwrzrfjynfc5lhn1khrqi")))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0716xg4imyskaylrk2lw3dh12qicqm4wd40p6h965547vf42fvjf") (features (quote (("default" "serde"))))))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0h5jwjz7lz743yx1clj1k1ypvp07zmmr176g6lzdd4rqdlyxyxm9") (features (quote (("default" "serde"))))))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.0.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1yp72jlb8x657wdl1qw9rb5gdsv2frrq9fjrqdz6zjff54mxpwhf") (features (quote (("default" "serde"))))))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.0.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.1.18") (default-features #t) (kind 2)))) (hash "08b2cwmr5a0214l4qf7sdvdgkrq0qjyxapyvr1w91k1q56cpx3bf") (features (quote (("max_inline") ("default" "serde") ("bench_subset_unstable") ("arc"))))))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.0.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smol_str") (req "^0.1.18") (default-features #t) (kind 2)))) (hash "09j5xb3rnjd3kmc2v667wzsc4mz4c1hl1vkzszbj30fyxb60qccb") (features (quote (("max_inline") ("default" "serde") ("bench_subset_unstable") ("arc"))))))

(define-public crate-kstring-1 (crate (name "kstring") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "document-features") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1r4n9fa5scikqvl736nxghcfa6s3b07xz61w43hyzs2qb3wmd3nk") (features (quote (("unstable_bench_subset") ("unsafe") ("max_inline") ("default" "unsafe" "serde") ("arc")))) (yanked #t)))

(define-public crate-kstring-2 (crate (name "kstring") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "document-features") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0isp7kmk4q0qxpcd877q77ykgb3ryfbmj18djmnwv8c210sncc7c") (features (quote (("unstable_bench_subset") ("unsafe") ("std") ("max_inline") ("default" "std" "unsafe") ("arc"))))))

