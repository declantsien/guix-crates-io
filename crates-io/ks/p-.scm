(define-module (crates-io ks p-) #:use-module (crates-io))

(define-public crate-ksp-commnet-calculator-cli-0.1 (crate (name "ksp-commnet-calculator-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ksp-commnet-calculator-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1bkyz9zgvwldgqz1xb2aix4bvdqpi1pj6zinaqqxcgi7gd77wal4")))

(define-public crate-ksp-commnet-calculator-cli-0.1 (crate (name "ksp-commnet-calculator-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ksp-commnet-calculator-core") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0kd4dmgdryj116jj6gk76vwk7g7f8jiq996b18cfb45klfcfrdcp")))

(define-public crate-ksp-commnet-calculator-cli-0.1 (crate (name "ksp-commnet-calculator-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ksp-commnet-calculator-core") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0vxnhxm0zf0yhp7pswizlpiqfyxkwlahrz167cvk7183j6f0rlab")))

(define-public crate-ksp-commnet-calculator-core-0.1 (crate (name "ksp-commnet-calculator-core") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0f6h973s55q551xk3b5q1dwlj9vvm9n7p5x02017bd7nvw8cbva2")))

(define-public crate-ksp-commnet-calculator-core-0.1 (crate (name "ksp-commnet-calculator-core") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0nsk1q7g5gb01z7mvpblighz8r44zn8dw2fy4rv4zxhj0fcfa9s2")))

(define-public crate-ksp-commnet-calculator-core-0.1 (crate (name "ksp-commnet-calculator-core") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "00lhk8jp7gm2p4xqh8vcjwb6l1badxyjzybvhcdzyzgy1fkq5gk3")))

(define-public crate-ksp-commnet-calculator-core-0.1 (crate (name "ksp-commnet-calculator-core") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "1p6vlrx8934xq741g28xv4c6vlmcv7jfcb3kkxllhzplp9dbchm7")))

