(define-module (crates-io ks ta) #:use-module (crates-io))

(define-public crate-kstack-0.1 (crate (name "kstack") (vers "0.1.0") (hash "0j7ga1ybzlrnbmax9jjnrbmv28cm0z7qpb8d80mmpnpyd9kmriaj") (yanked #t)))

(define-public crate-kstat-0.1 (crate (name "kstat") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jjvhkp0hkxpnfh9z9490m624aicdn7far22jgfl5avw48mi3spy")))

(define-public crate-kstat-rs-0.1 (crate (name "kstat-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "05043cp4gwwh8b37nz8b42zy9b502w0h2dmi6sg8p5i52g79r2s9")))

(define-public crate-kstat-rs-0.2 (crate (name "kstat-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0x9rsm97bvkz5i0vvzd50m0rc00zjrbqvr7lhwmq85p9sw865ly1")))

(define-public crate-kstat-rs-0.2 (crate (name "kstat-rs") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1h5ra0dkgd4fc39d7pl4qx0ys179dy9z87qlid11w77rd9pcx7la")))

(define-public crate-kstat-rs-0.2 (crate (name "kstat-rs") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0rvdzyi7xsj791q7nik04rgm1sk472nxn4j0q3q7qx9gj33i7izw")))

