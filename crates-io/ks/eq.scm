(define-module (crates-io ks eq) #:use-module (crates-io))

(define-public crate-kseq-0.1 (crate (name "kseq") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)))) (hash "1bkkyh598wb8ph67qjazdkxidbways9j9wps8vvlb0xmk72dkr29") (yanked #t)))

(define-public crate-kseq-0.1 (crate (name "kseq") (vers "0.1.1") (deps (list (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)))) (hash "195cvxjv7fc8mzmwi8snmnafjar73ghcvhgs1m26c9q83bcji9bn") (yanked #t)))

(define-public crate-kseq-0.1 (crate (name "kseq") (vers "0.1.2") (deps (list (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)))) (hash "0fhpxm3frfjs0yvplnqhwlvqx2m6hrndkgmxg9fg25xc224rg70r")))

(define-public crate-kseq-0.1 (crate (name "kseq") (vers "0.1.3") (deps (list (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)))) (hash "15yrg5jx1kzvi178zqyv85904vg07b5bx3dyb83dp3cy06xawriy")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.0") (deps (list (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)))) (hash "07b83pj7yshk6za5paw6vpp7n36z8f2j3654m8cd5mrznrdja3dv")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.1") (deps (list (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)))) (hash "04r0pzrynnidr4904mkw50j55ac7d0d78fa9qlgy31g62bpbqn9z")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastq") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "0kdsj2shp57603fh4dnv8hydkyf9c1ms71gwrcrdxh195viqv8nl")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "02k6zxwl2h408p5dxbzb3qgj0pllnrq9z3kdq3ip5hymkyhf2d4a")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "00g8xc8kygnmi00bjpnyf4bh4wqxl4krjlkz5i57azg1gj6pyriv")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.5") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "0nzpziqa4gjzcir9ji79fip0fqmssx3acmfb9ivp6qz25hdsdqwh") (yanked #t)))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.6") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "185y8xv3adcjg8ypx3pysbwz5d2ydfcl1cj6pw9x8r2l6g1hgx67")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.7") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=0.2.18") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "0s5dnz14h1ha32x3l12yp46rzkjqcbx2yw2djw7qszrszx9bql4y")))

(define-public crate-kseq-0.2 (crate (name "kseq") (vers "0.2.8") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "1c0cs0vyc83srh8q109hkqaj08zzj83vx5a8zajm95q6sp63ahcx")))

(define-public crate-kseq-0.3 (crate (name "kseq") (vers "0.3.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "048p3mlxfxrm8n8213k4gmx78z9x41d9048kyhscx90ihg41wc0k")))

(define-public crate-kseq-0.3 (crate (name "kseq") (vers "0.3.1") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "01bmjf284i7jz3scninl3hi9qg10992jw94cwk8dwz0s34xr5r89")))

(define-public crate-kseq-0.3 (crate (name "kseq") (vers "0.3.2") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "09d9cr2w9r5z3jrya57c2l8nhmcbskbgnm1r5a4ma8pwjcizm58g")))

(define-public crate-kseq-0.4 (crate (name "kseq") (vers "0.4.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "1b5sz8wpyz755x5p7ywz37f5j33811b75lkwfnryp3iny4qhdf58")))

(define-public crate-kseq-0.5 (crate (name "kseq") (vers "0.5.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "memchr") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "10jqqim8p3imddxiljxvq3vkn7pa8jzfq3fs2a2zkq79gmwh24j2") (yanked #t)))

(define-public crate-kseq-0.5 (crate (name "kseq") (vers "0.5.1") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "memchr") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "02pjlrq28hnj65zqp3z47b90flm5rha20dm70j6fs9rv7k6mrqd3") (yanked #t)))

(define-public crate-kseq-0.5 (crate (name "kseq") (vers "0.5.2") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "memchr") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "0vx85pxi8kmwn7fbiis5hnp58b86k5jbzcr9gjfj9zfd75zqsby9")))

(define-public crate-kseq-0.5 (crate (name "kseq") (vers "0.5.3") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req ">=1.0.17") (features (quote ("zlib-ng-compat"))) (kind 0)) (crate-dep (name "memchr") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "needletail") (req "^0.4") (default-features #t) (kind 2)))) (hash "0ryg2sfm5mwgx6jngmsnc9lxjwzab4cwy5fvgs4m3nms17cgrpaj")))

