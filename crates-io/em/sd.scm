(define-module (crates-io em sd) #:use-module (crates-io))

(define-public crate-emsdk-0.1 (crate (name "emsdk") (vers "0.1.1") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking"))) (default-features #t) (kind 1)))) (hash "1adqvhq4il5n3z21kwawlrpk3bbyr8awdv77h1spvv0lv2nj9v51") (rust-version "1.60.0")))

