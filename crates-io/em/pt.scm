(define-module (crates-io em pt) #:use-module (crates-io))

(define-public crate-empty-0.0.1 (crate (name "empty") (vers "0.0.1") (deps (list (crate-dep (name "void") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "023p31siypnnd7ryyy5hkasp2ngp7khn74krkjvjd4v800mlr6f6")))

(define-public crate-empty-0.0.2 (crate (name "empty") (vers "0.0.2") (deps (list (crate-dep (name "void") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "12ff8k3kyv238h0dm1dvldgg7xbynjqwny4yhk845ar90148japx")))

(define-public crate-empty-0.0.3 (crate (name "empty") (vers "0.0.3") (deps (list (crate-dep (name "void") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0zgcpdig3k1j1fq5lg53019vbsn2df8g8lb08h1sxb3hnhwx4pz6")))

(define-public crate-empty-0.0.4 (crate (name "empty") (vers "0.0.4") (deps (list (crate-dep (name "void") (req "^1") (default-features #t) (kind 0)))) (hash "1c0k8v0vblfwz92141px90398p4wfi3s8n4jafprvw9fzdxalbcy")))

(define-public crate-empty-box-0.1 (crate (name "empty-box") (vers "0.1.0") (hash "0lxjl65dmv2q6lq9r1imhnsqaia8q85lslfmm3d6p5wmxyz2piz3")))

(define-public crate-empty-box-0.1 (crate (name "empty-box") (vers "0.1.1") (hash "17p0bxm82dcrnw7ndc4j0jd07azp9qb184ig1v2q6g6jxr2l8jmd")))

(define-public crate-empty-library-1 (crate (name "empty-library") (vers "1.0.0") (hash "030knf4j2xf4x5kxhxnjhrv9xsvx3ynjad24l6d77s90k1xsmwh9")))

(define-public crate-empty-option-0.1 (crate (name "empty-option") (vers "0.1.0") (hash "1wxxid9ff8k78zw0awy0l01knphxz0y84cxwjj1x7djhiji9zir5")))

(define-public crate-empty-option-0.1 (crate (name "empty-option") (vers "0.1.1") (hash "0jz03ckvir6b48sv6sqg9zfmdh523ivvd9hn6p58815m7knslkd1")))

(define-public crate-empty_package_one-0.1 (crate (name "empty_package_one") (vers "0.1.0") (hash "0f8lg7qf77pjg2n3fh98sld4a9qzpmb22172s7k63h1mcbs7rzz2") (yanked #t)))

(define-public crate-empty_package_one-0.1 (crate (name "empty_package_one") (vers "0.1.2") (hash "1xzzpwq7fr9pc3z5cb0r9a6bh9lmzf75dvckj98wcgkc5d9mxx4g") (yanked #t)))

(define-public crate-empty_package_one-0.1 (crate (name "empty_package_one") (vers "0.1.1") (hash "1vrxzjzqpwjxcqzs126rjn6r6y37fm5qng25kgwp97gfvmas6vxg") (yanked #t)))

(define-public crate-empty_package_one-0.1 (crate (name "empty_package_one") (vers "0.1.3") (hash "10psxcxyhrydzl2bbb3lzlkghpagbcg4scb1yf61mwm9gn7zbrnm") (yanked #t)))

(define-public crate-empty_package_two-0.1 (crate (name "empty_package_two") (vers "0.1.0") (hash "191pb7gdz2mjg2zlcwwzxyfs90bgpr6pq4y267mvgvhm2136hvcf") (yanked #t)))

(define-public crate-empty_type-0.1 (crate (name "empty_type") (vers "0.1.0") (deps (list (crate-dep (name "empty_type_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "empty_type_traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00yf2j5k9w72l1f5mpml28j0n90zw2zbpgvzxisd6jg2nzbbshpj") (features (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

(define-public crate-empty_type-0.2 (crate (name "empty_type") (vers "0.2.0") (deps (list (crate-dep (name "empty_type_derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "empty_type_traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1nlwfisqv8i8z4np81hld8iybf68c94y10x3c0jn2lsw63kywpa4") (features (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

(define-public crate-empty_type-0.2 (crate (name "empty_type") (vers "0.2.1") (deps (list (crate-dep (name "empty_type_derive") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "empty_type_traits") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "13ysmzb40sjv10qwsvbwrx99fpb0lzrri4n7gwc66zw07fkzqzhl") (features (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

(define-public crate-empty_type-0.2 (crate (name "empty_type") (vers "0.2.2") (deps (list (crate-dep (name "empty_type_derive") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "empty_type_traits") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0rv91pasg5gzpriz0zpv3zspfcdf9ghhyw94w9xzbj9vmwz7kx43") (features (quote (("serde" "empty_type_traits/serde" "empty_type_derive/serde") ("derive" "empty_type_derive"))))))

(define-public crate-empty_type_derive-0.1 (crate (name "empty_type_derive") (vers "0.1.0") (deps (list (crate-dep (name "empty_type_traits") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gqnwbwg3wwdnqi3lprmafaccr97143fnnn9prfvvljx7mnmfc5n") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive" "empty_type_traits/serde"))))))

(define-public crate-empty_type_derive-0.2 (crate (name "empty_type_derive") (vers "0.2.0") (deps (list (crate-dep (name "empty_type") (req "^0.2.0") (default-features #t) (kind 0) (package "empty_type_traits")) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1mk3fjynwn6dzp6wplxybv9wc721gj9sr99x4h236j0ybfd9ps6z") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive" "empty_type/serde"))))))

(define-public crate-empty_type_derive-0.2 (crate (name "empty_type_derive") (vers "0.2.1") (deps (list (crate-dep (name "empty_type") (req "^0.2.1") (default-features #t) (kind 0) (package "empty_type_traits")) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1in487h8s2k07rp56zkwnmyph5z91vr0268g7rww2ggxbzar1dis") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive" "empty_type/serde"))))))

(define-public crate-empty_type_derive-0.2 (crate (name "empty_type_derive") (vers "0.2.2") (deps (list (crate-dep (name "empty_type") (req "^0.2.1") (default-features #t) (kind 0) (package "empty_type_traits")) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0xf5k67hj0x846d23w86z4abr0hr6xvfcd9b7f6zivxds0dy3l05") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive" "empty_type/serde"))))))

(define-public crate-empty_type_derive-0.2 (crate (name "empty_type_derive") (vers "0.2.3") (deps (list (crate-dep (name "empty_type") (req "^0.2.1") (default-features #t) (kind 0) (package "empty_type_traits")) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0dckxvm94jvavhqa0gw37rpnvpfac6yqjqw80fcx5zxn7al1479x") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive" "empty_type/serde"))))))

(define-public crate-empty_type_traits-0.1 (crate (name "empty_type_traits") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "07nqyi7dm8v4iqprfwcj4d76ibmw8hr2ysmfc6vv7pkvmp8jjrnq") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-empty_type_traits-0.2 (crate (name "empty_type_traits") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "04amw8p1dh48dcgmc94hh4qix18ivbsy3nx0jqlfn2gdrn35qx8z") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-empty_type_traits-0.2 (crate (name "empty_type_traits") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "14hdk0jpnwpjx0xbwpp4f1hbv3ij0hqslz81hrpxb4nr4m2a6iss") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-empty_type_traits-0.2 (crate (name "empty_type_traits") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "06ag2fs1nn2hbp11pxljs86n4l66dm7q1b9g5d7vxdcf20nsp757") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-emptypipe-0.1 (crate (name "emptypipe") (vers "0.1.0") (hash "0jh131d5jgz4cfbphs0s1dqywzjjrd4rsjaw50vgrns31dif8aqs")))

(define-public crate-emptypipe-0.1 (crate (name "emptypipe") (vers "0.1.1") (hash "1lrg7rbvg9qq5kd8sqb6dbi1fjifpyl11x977dz5h9c9ixr8b0gd")))

(define-public crate-emptypipe-0.2 (crate (name "emptypipe") (vers "0.2.0") (hash "0jp1g615xfg94slzw41mmj8vb1bw4nf7121q7066651rfj246yjf")))

(define-public crate-emptypipe-0.2 (crate (name "emptypipe") (vers "0.2.2") (hash "0szfqimjbmizrhq8v8mdvmd8lbd2wk4i70mdf80apz1nj8yc7nnk")))

