(define-module (crates-io em br) #:use-module (crates-io))

(define-public crate-embree-0.0.1 (crate (name "embree") (vers "0.0.1") (hash "0g8shraz6pk9w5398ghzjym833a03pr03h137i9d8wwgk90bf8x7") (yanked #t)))

(define-public crate-embree-0.3 (crate (name "embree") (vers "0.3.6") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0wxxgbr0k4fc5w29516ydwyhx6f9z7m0993a912p0wbrwzr8zvyz")))

(define-public crate-embree-0.3 (crate (name "embree") (vers "0.3.7") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "10g8adjpqkqaalxk9ji9jzvf1wzbq7zb8839mlqxxw9f6x4iwiiz")))

(define-public crate-embree-0.3 (crate (name "embree") (vers "0.3.8") (deps (list (crate-dep (name "cgmath") (req "^0.18") (default-features #t) (kind 0)))) (hash "069j1x265l1m3b2wigfkg65ib3m9n1f6i382jq2mcqj700rcn2hj")))

(define-public crate-embree-rs-0.1 (crate (name "embree-rs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "17k9wmr67yxb4rlkrrpj0824frccdawi3zvfz70qdsg85wq7j8ps")))

(define-public crate-embree-rs-0.1 (crate (name "embree-rs") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qap0ip1yi4n7jllwxngbxrp9k2hpy1j78l7ia2y5r22jw5h1mrr")))

(define-public crate-embree-rs-0.2 (crate (name "embree-rs") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.189") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hg0s67kwz8p6z42q4458xgskr3p9xb889zkvff94ld8fi0fs97g")))

(define-public crate-embree-rs-0.3 (crate (name "embree-rs") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)) (crate-dep (name "cgmath") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.189") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08qlbmxbp3b3npvrqf9gcrr9xl0hhbd7mbhh09mq4avz9fz09vzs")))

(define-public crate-embree-rs-0.3 (crate (name "embree-rs") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.37.0") (default-features #t) (kind 1)) (crate-dep (name "cgmath") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.189") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x9b988p98kr21na8my47dva716l6ry835kizjl9wpr6in5q8sma")))

(define-public crate-embree-rs-0.3 (crate (name "embree-rs") (vers "0.3.2") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "0iisvndjvaa76jl2vgaa1mqbvvbv14c79akcdb5xzxwb8cfrzjj0")))

(define-public crate-embree-rs-0.3 (crate (name "embree-rs") (vers "0.3.4") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0v32kanfc2289mrxsb2rrmnl1g14aczar59qbw8wmvnxj788lhh2")))

(define-public crate-embree-rs-0.3 (crate (name "embree-rs") (vers "0.3.5") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1n2mcckvr8cgbj9gdk22iivxh0p6cr17f1nxz29v2h653nlbri1w")))

(define-public crate-embree-rs-0.3 (crate (name "embree-rs") (vers "0.3.6") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0bz33b4dmxxnac80mvhx32pmppwm6nf3mg366cwra30ccfaybwpn")))

(define-public crate-embree3-sys-0.1 (crate (name "embree3-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "0067akpr90iymmnz9a02rkf8wbfssngk7224r243ixl59xsmr8xs")))

(define-public crate-embree4-rs-0.0.1 (crate (name "embree4-rs") (vers "0.0.1") (deps (list (crate-dep (name "embree4-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0rjk2a8mcsl0s9khx0p8b8r219cx67rnncnwaz032zml8zpbzlgr")))

(define-public crate-embree4-rs-0.0.2 (crate (name "embree4-rs") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1hc46p3ndawpwn1sdvsr9pmgap1pdiki58ial07gafa60hc382ml")))

(define-public crate-embree4-rs-0.0.3 (crate (name "embree4-rs") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1aqdrwv4mc98n0c6i9582rnfpas3ccvqjdvyg0hprqx33i34w4ly")))

(define-public crate-embree4-rs-0.0.4 (crate (name "embree4-rs") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0i7bnv08w7c5lys0l30208vixnj1v3140aqqwszg3379hyc6jidj")))

(define-public crate-embree4-rs-0.0.5 (crate (name "embree4-rs") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1jmq5l0124y6scv2hq4fzn73ffpji66f45i2k2nih7a5jp98kifn")))

(define-public crate-embree4-rs-0.0.6 (crate (name "embree4-rs") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (default-features #t) (kind 2)))) (hash "1xrs9ha2lqll7whx569yj3barylnap7g27g28ynd7mbk3k4hnmgi")))

(define-public crate-embree4-rs-0.0.7 (crate (name "embree4-rs") (vers "0.0.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1gmcmbaq9awh11zx0gnk0kva8i57r5wcscmbpcjyi9h5h0v2h4f0")))

(define-public crate-embree4-rs-0.0.8 (crate (name "embree4-rs") (vers "0.0.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1gkhyzbry1b2cwjrblx8f3wz28hpj0qccklc8kpljs0h60q87k3p")))

(define-public crate-embree4-rs-0.0.9 (crate (name "embree4-rs") (vers "0.0.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1a6gxgh7cf264lv1a6070wnk2ak2bb25ysmag5d6rgb26bwi9dak")))

(define-public crate-embree4-rs-0.0.10 (crate (name "embree4-rs") (vers "0.0.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1vi462k5fpy2i71799dzzji0nxl5g3hnbn7zf4jp14833dbbrlav")))

(define-public crate-embree4-rs-0.0.11 (crate (name "embree4-rs") (vers "0.0.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "18y2w601n3zz49sja7ry8f90lrd1ncn1bdq9vmfmrdsc7ccvb174")))

(define-public crate-embree4-rs-0.0.12 (crate (name "embree4-rs") (vers "0.0.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "11hcja2iv8ag3wqafjchz6sar2knb5yhnnhvmjzl95z2y45af7bw")))

(define-public crate-embree4-rs-0.0.13 (crate (name "embree4-rs") (vers "0.0.13") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "07qwq6n8q4i9z7ijnb7ybx4dsmnkwip4qz2axxch50sd319lcksa")))

(define-public crate-embree4-rs-0.0.14 (crate (name "embree4-rs") (vers "0.0.14") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1yr6bn94f83x95wn7p2m84jvga09q56mm3ldpysqjmggck2hdw4x")))

(define-public crate-embree4-rs-0.0.15 (crate (name "embree4-rs") (vers "0.0.15") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "19zf243343qfj0pjbskmc55qg6ihxilllmwi5vcii6c2xq24jnj3")))

(define-public crate-embree4-rs-0.0.16 (crate (name "embree4-rs") (vers "0.0.16") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "embree4-sys") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24.2") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0zc2a6lbpc9xgzkpca73cr4j7pz8dm16vlar37djir91scj1jd3y")))

(define-public crate-embree4-sys-0.0.1 (crate (name "embree4-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1clj5qn5x5113yc5jn5ikpiib2y5hcj62j0s5a68h3b1gqgabxqx")))

(define-public crate-embree4-sys-0.0.2 (crate (name "embree4-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1mqnzm3zn6nhhl7ljyp8bsyan3jjg8rp1p44nw2v0k9f6gm1ygpd")))

(define-public crate-embree4-sys-0.0.3 (crate (name "embree4-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1q8k5l3mvi6qqs0l08fz99fqn53ffsz07kaf2bz7rxfnly47xy4l")))

(define-public crate-embree4-sys-0.0.4 (crate (name "embree4-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "0qlcsj6jvdq8p2lwjlqyi1k5y5zrvv27lxjmidy3h69yh93x2r86")))

(define-public crate-embree4-sys-0.0.5 (crate (name "embree4-sys") (vers "0.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "01hd1z6fhh4kd5xaz4bzipn2hcs2lwd2g6m5ppz9f8f24brghy4x")))

(define-public crate-embree4-sys-0.0.6 (crate (name "embree4-sys") (vers "0.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "09smy61v9yj6v8bwa8bwkr0ncdbpj0531clhzi4ndkjnvcz9hfx4")))

(define-public crate-embree4-sys-0.0.7 (crate (name "embree4-sys") (vers "0.0.7") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "0qa7dk1k1vigaik8s9lnrkchxbg7z1i920y938pfx4czi0rx49in")))

(define-public crate-embree4-sys-0.0.8 (crate (name "embree4-sys") (vers "0.0.8") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "0457vg9jwczy7vhijr49yq6spfff4iwsg97cy52bc27k8ihsax0l")))

(define-public crate-embree4-sys-0.0.9 (crate (name "embree4-sys") (vers "0.0.9") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1kza9k1gq7czvmfhdd9v9ci46mlkl7wsi206sjahs93illrqni83")))

(define-public crate-embree4-sys-0.0.10 (crate (name "embree4-sys") (vers "0.0.10") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "0izylzc2i62ql45gdpnxg6kbc0da737aa9mcn9ykjqxpcfvbbx5w")))

(define-public crate-embrio-0.0.0 (crate (name "embrio") (vers "0.0.0") (hash "1681j8w4wmhrgylzly4aki04d2vk2j23asz8322mqgizz65clih9") (yanked #t)))

