(define-module (crates-io em v_) #:use-module (crates-io))

(define-public crate-emv_parser-0.1 (crate (name "emv_parser") (vers "0.1.1") (deps (list (crate-dep (name "emv_tlv_parser") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0657ll4kmn1p9q1lrfprn9sq08gywjkylqsvy59vgwfxjq70f27n") (yanked #t)))

(define-public crate-emv_parser-0.1 (crate (name "emv_parser") (vers "0.1.2") (deps (list (crate-dep (name "emv_tlv_parser") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1s77yzlnc3wvsk0yqsapyh36lah6w1w0m7k87cknrfbihfy0y4mg") (yanked #t)))

(define-public crate-emv_parser-0.1 (crate (name "emv_parser") (vers "0.1.3") (deps (list (crate-dep (name "emv_tlv_parser") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0an3fg6vqixmwz2r255n651dgfl3fplg1rqf73nclkfzcgh5x168") (yanked #t)))

(define-public crate-emv_parser-0.1 (crate (name "emv_parser") (vers "0.1.4") (deps (list (crate-dep (name "emv_tlv_parser") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0sjg7nhmc5kig3aayqfq273sv3cbbawg7vkc2x138pa1327mmygs") (yanked #t)))

(define-public crate-emv_parser-0.1 (crate (name "emv_parser") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "emv_tlv_parser") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0yw3lzxvj48lh5n2znpy6aw7fzzxdqrk87zgpjpmzicydkqg0zz0") (yanked #t)))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1z0aaxkfqrkkjjpij65jalbf77pviyi5vnzqyclb9i4w4aid4xp0") (yanked #t)))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "04prx96j3l1g61lcw11a8akkrz5a8iv264m9a3wc9agx5ii5y1av") (yanked #t)))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "120xjs6mwr5rskj0hssva52rg0l5x25xqrwgp89myc68y2phv8p1")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.3") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0nzzd6c5f1zq3qcg8m1s80f7ym18crih1mpgvs3iir02nx40ybnm")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.5") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1yc1wpn832jx4hk1yph6qc5irpkixwvfs3vipzdqf61k0hqpn1b7")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.6") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "11kfjs7p2jhyn5n24n0cb3fmnx0prqx4aisg229kkl62rs2lnxjq")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.7") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0zhfz307f4l6y6ygnyy3mv6nh2m1v7a9b6g5yh3mznh13gcpna94")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.8") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0fv3pj225avf3ymy2pbwdj0wgjjwhxnqy97xaclca8nqm53iqz27")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.9") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "03ng9fqbs4wrgdh541l5k7s90yghvhwsihvkndrg1rycssnqb5k4")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.10") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "09wwc2ris1k97pj308d3ss3f08l1kkyi4rsj8vzxlxjdzc68c2vz")))

(define-public crate-emv_tlv_parser-0.1 (crate (name "emv_tlv_parser") (vers "0.1.11") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0bckkfr6w8kiyrl8cfxvsvzx4xafmb2g6nw7pvss8fndxsl7hil8")))

