(define-module (crates-io em bo) #:use-module (crates-io))

(define-public crate-emboss-0.1 (crate (name "emboss") (vers "0.1.0") (hash "0x49f3x7z9k9qjajdr4v1hppd5kylfp3xidwyd9fw3h8903fj78n") (yanked #t)))

(define-public crate-emboss-0.2 (crate (name "emboss") (vers "0.2.0") (hash "0sgb5c6dyw7jw3hz9g8h2d2s8sqmpn5j0pa3gxx8c9k7ldnfl0hs") (yanked #t)))

(define-public crate-emboss-0.3 (crate (name "emboss") (vers "0.3.0") (hash "1wz13qzl6m1m8g1fb7i9pdfawjw58axfj5c0s2970wcrzz99d7cw")))

(define-public crate-emboss-0.3 (crate (name "emboss") (vers "0.3.1") (hash "127p6bf37rq3iigjxg2v1v35h3hp9fd996ay9dsmk8s4yfqk74s0")))

