(define-module (crates-io em or) #:use-module (crates-io))

(define-public crate-emorand-1 (crate (name "emorand") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.1") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0i1m8azq75rlpvrvfdxmbysz55x1n9wczcqkjs0fyif62i1f984s")))

(define-public crate-emorand-1 (crate (name "emorand") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1fl09icfnxly3dyypi0vlqcfvxfpmcjmbl5zmainr1bjm9xvxcpn")))

