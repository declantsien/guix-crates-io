(define-module (crates-io em sc) #:use-module (crates-io))

(define-public crate-emscripten-functions-0.1 (crate (name "emscripten-functions") (vers "0.1.0") (deps (list (crate-dep (name "emscripten-functions-sys") (req "^3.1.44") (default-features #t) (kind 0)))) (hash "0zd8v2caqs53m7mpxn7idk8f3xkm8a452klm11cifizbq3av837a")))

(define-public crate-emscripten-functions-0.2 (crate (name "emscripten-functions") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "emscripten-functions-sys") (req "^3.1.44") (default-features #t) (kind 0)))) (hash "0rhs7v51xvd8rwi9a3iyvcgfbk0g50yyssawmcq3lqvnwf65r906")))

(define-public crate-emscripten-functions-0.2 (crate (name "emscripten-functions") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "emscripten-functions-sys") (req "^3.2.46") (default-features #t) (kind 0)))) (hash "01arfdg0bm542miwmpzg002w5dwnyiz9yb1fzjvh10kvgpdvl896")))

(define-public crate-emscripten-functions-sys-3 (crate (name "emscripten-functions-sys") (vers "3.1.44") (hash "1i00svs4y9i77w8hkmfpg0yad4svq4jnagfa55flpyxw2aifsd3i")))

(define-public crate-emscripten-functions-sys-3 (crate (name "emscripten-functions-sys") (vers "3.2.46") (hash "1j6s4myipp8ggz2r1dw3gp681p3njkd9p1cxcmzn14797289nwqs")))

(define-public crate-emscripten-sys-0.1 (crate (name "emscripten-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)))) (hash "19jsabfwyzh08091hwq1v2v517crpbqg94iij9i9gv1v249fvq4p")))

(define-public crate-emscripten-sys-0.2 (crate (name "emscripten-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)))) (hash "1z9xf2q519qgsm3yps4l6kkw3lc8znvj9pj4syhl30mbqzh460j9") (yanked #t)))

(define-public crate-emscripten-sys-0.3 (crate (name "emscripten-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)))) (hash "15n1rm43kikirqwpm1cyd24nywg6bkb21cwpa8mvggd20b785kxa")))

(define-public crate-emscripten-sys-0.3 (crate (name "emscripten-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.24.0") (default-features #t) (kind 1)))) (hash "1mwrnk2chqv91mmg60pyf08163647gdvns80vk94nzsg88hlby6f")))

(define-public crate-emscripten-sys-0.3 (crate (name "emscripten-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.24.0") (default-features #t) (kind 1)))) (hash "0f9nfcs9db2zbi40lxfz006d41ikcssjgx1nm2wwiah9prfjjfb0")))

(define-public crate-emscripten_main_loop-0.1 (crate (name "emscripten_main_loop") (vers "0.1.0") (hash "0qivvmmriaxx3a0b504ph3r0njvsghwp91lchp7av69lc5ixlhbc")))

(define-public crate-emscripten_main_loop-0.1 (crate (name "emscripten_main_loop") (vers "0.1.1") (hash "10mnd5b3ikc7aix9h8i7jy4qv8bqwqmig82ypslpn6745y5x5yjs")))

