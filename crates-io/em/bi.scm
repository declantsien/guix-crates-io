(define-module (crates-io em bi) #:use-module (crates-io))

(define-public crate-embi-0.0.0 (crate (name "embi") (vers "0.0.0") (hash "180zv0rpxzlr22jfl3wi3irlga6nmb3jdxmmx3422q3k7xc9k950")))

(define-public crate-embin-1 (crate (name "embin") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1b57379j76c15smjmrnpi6r2171pv4rwmk3scbic4bfb7i1y0ndg")))

(define-public crate-embin-1 (crate (name "embin") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0ifwzk9pz8hvc1v60vnrkn0vy1ldcwzp66r42qvf9w2q7g2frc8w")))

(define-public crate-embin-1 (crate (name "embin") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "147pr829spv5dfiib7mbzr2mymx6xfi8v742l1d1qnmfix6gp1kr")))

(define-public crate-embin-1 (crate (name "embin") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "10mk3db93ccza1ga8qjm5aqk4xr6yz255qnbl3qn30kv5xdn20b8")))

(define-public crate-embin-1 (crate (name "embin") (vers "1.1.3") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1lwykmap6wbzx9gx0d6s3zns04f1gbdaswvpql8lb11hzlm8rfyc")))

(define-public crate-embin-1 (crate (name "embin") (vers "1.1.4") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "11ib46ndhadr858jj01fb5lpz5flj1nxrpd098wziw8x3kj46cqx")))

