(define-module (crates-io em bt) #:use-module (crates-io))

(define-public crate-embtk-rotary-encoder-0.1 (crate (name "embtk-rotary-encoder") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.0") (kind 0)))) (hash "022dsw0xd4ip6ns4sxma9z4qk68swzfcpamc257pkj5q7108zjm8")))

