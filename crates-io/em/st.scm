(define-module (crates-io em st) #:use-module (crates-io))

(define-public crate-emstr-0.1 (crate (name "emstr") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (optional #t) (kind 0)))) (hash "1s4g83xgzd99k654g3k4p5q9xgy25mg3cj0xk1w78zw5gfw24rm1") (features (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-emstr-0.1 (crate (name "emstr") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "03f2v7vszrlwqzqr21sgmngvgwyyhl4ny31ny5vk8hhrqm9kcj7n") (features (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-emstr-0.1 (crate (name "emstr") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0bidrsx6wsjyi70r4g1hcm0fsbdywwwzki3gd5vfc6bld3yf65wz") (features (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-emstr-0.2 (crate (name "emstr") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1db70rwfllf9k96pql11l9hs9j9nvr0hm43rv5kai7j2vq752jrc") (features (quote (("std" "thiserror") ("default" "std"))))))

