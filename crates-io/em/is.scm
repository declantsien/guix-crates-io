(define-module (crates-io em is) #:use-module (crates-io))

(define-public crate-emissary-0.1 (crate (name "emissary") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.27") (default-features #t) (kind 0)))) (hash "05pn2yd82lgw2lgf451a12r30hkb3rkbgf16cgdjxdmicmr9xpgg")))

