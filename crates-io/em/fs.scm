(define-module (crates-io em fs) #:use-module (crates-io))

(define-public crate-emfs-0.0.0 (crate (name "emfs") (vers "0.0.0") (hash "18a6ig2wv2j36p6hih7k07zy5l8hvny4ggb1v9cisppi8f8mgiqf") (yanked #t)))

(define-public crate-emfs_derive-0.0.0 (crate (name "emfs_derive") (vers "0.0.0") (hash "0r8904xd8ds8xwhcsbv7jdk2dabjf23lcrglvd5aaw1ivacr0zj1") (yanked #t)))

