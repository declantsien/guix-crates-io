(define-module (crates-io em he) #:use-module (crates-io))

(define-public crate-emheap-0.1 (crate (name "emheap") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0s83wrmmdbi671svcrp8www762m3d3c26p1b94a94d77xlpq47jl")))

(define-public crate-emheap-0.1 (crate (name "emheap") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0d9kx0z2057667jyfqa5dv3mzyqzjfcb87k9xk5zv03v9dra8rhd")))

