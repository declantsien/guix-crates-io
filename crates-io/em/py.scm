(define-module (crates-io em py) #:use-module (crates-io))

(define-public crate-empy-1 (crate (name "empy") (vers "1.0.0-alpha") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "chlorine") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f3ksz7macafn1hbr85fjid55hbr8wgz9xfbgvy7fdjv6jf46dlv") (features (quote (("std") ("simd") ("nightly-docs") ("mp1-mp2") ("default" "simd"))))))

(define-public crate-empy-1 (crate (name "empy") (vers "1.0.0-alpha2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "chlorine") (req "^1.0") (default-features #t) (kind 0)))) (hash "032dsj0mxcc8bm6v8hm1xzzgp8hzaa97psi7iq00ziyn263fn3c1") (features (quote (("simd") ("mp1-mp2") ("default" "simd"))))))

