(define-module (crates-io em li) #:use-module (crates-io))

(define-public crate-emlib-0.0.1 (crate (name "emlib") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cortex-m-rt") (req "^0.3.12") (features (quote ("abort-on-panic"))) (default-features #t) (kind 0)))) (hash "1yg59avxhmyaabkm3b8fdk2zz61hcjy7n8fh5k1sfhy8gj6xmf6b")))

