(define-module (crates-io em lx) #:use-module (crates-io))

(define-public crate-emlx-0.1 (crate (name "emlx") (vers "0.1.0") (deps (list (crate-dep (name "plist") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.29") (optional #t) (default-features #t) (kind 0)))) (hash "07p7s1lvd6q9nd56q1z28c9safw50762mzyvabxlaj7namvxjhaz") (features (quote (("use-tracing" "tracing") ("default"))))))

