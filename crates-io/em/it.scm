(define-module (crates-io em it) #:use-module (crates-io))

(define-public crate-emit-0.1 (crate (name "emit") (vers "0.1.0") (hash "1l4s0imnnhkyh1154h5ppqiwr2jwr7sjll321lfv16zs47lfdid0")))

(define-public crate-emit-0.2 (crate (name "emit") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "1hb31abba3lxg2bcm02h8y8k2kjh8rwysah77w5qhz62s6q2asnm")))

(define-public crate-emit-0.2 (crate (name "emit") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0gymrgrji02accdzrm5kmw3grcqdqk36x13x7q1xxrbxbfjmbvqz")))

(define-public crate-emit-0.2 (crate (name "emit") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "121b1w4wwaclx5bls4mvr9s1cqyc3wb469bzc2xlisdaya0fgzl5")))

(define-public crate-emit-0.2 (crate (name "emit") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "10qa5adb99c2ng75r1kzmmjfx3i03fzrs8gpxfx1j43bypcl9l0f")))

(define-public crate-emit-0.2 (crate (name "emit") (vers "0.2.4") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "1dbbxnb4822fq12h1n53is08xw2hkq9qiqa92gs85vg5xamrj3lh")))

(define-public crate-emit-0.3 (crate (name "emit") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "1i6hd12sysfz37m0i1mpamrng7nl8w5l448za40z9ja8c3bj9rpx")))

(define-public crate-emit-0.3 (crate (name "emit") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hqac9h7l066bl8xfivc78wa3kam9n078bc7gmjgc1b3niqddf61")))

(define-public crate-emit-0.3 (crate (name "emit") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0sh89hy9ffs3a3rz2v66ichcipv96946bv1ryals506inxxazwpk")))

(define-public crate-emit-0.4 (crate (name "emit") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "14y0d4hwnn5rybmf7a0491gsx7wg5qsxk8psxgyq7c7h7wx36n4d")))

(define-public crate-emit-0.5 (crate (name "emit") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "1kl9qr5894i47pi59n28bqj4sqqpm9a8fik649c337mix9z6a409")))

(define-public crate-emit-0.5 (crate (name "emit") (vers "0.5.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0m20d9d3p8ld7sjg6d0jfn317rg6qw654wl6ap68362pff8fz197")))

(define-public crate-emit-0.6 (crate (name "emit") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "1l7ih28x916ba3y6igw72b15b1bmj3fn3hvnr894q7nxr63p334m")))

(define-public crate-emit-0.7 (crate (name "emit") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0m7k2gynz07hwbbrs7w2pp9dg3akvry38lvihlmyi4f25i6f4bfg")))

(define-public crate-emit-0.8 (crate (name "emit") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0higzwblm6zn8i2ms7bkis0vfnmbs73nyxpvccrhry4ssmjgvgqd")))

(define-public crate-emit-0.8 (crate (name "emit") (vers "0.8.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "1dicy92x1m31lvf23f72wrzr1kard9fx6g17q6k270asgds56gn7")))

(define-public crate-emit-0.8 (crate (name "emit") (vers "0.8.2") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "19adv0sn7qdq8jkbzcq1bv20xgwkiy5qv0zs5w2zj4qhqnp728hd")))

(define-public crate-emit-0.9 (crate (name "emit") (vers "0.9.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hrfjnhmhby6ch3ycsbyivj2vp25q97vxrgi5sz8hn1ppf5kgxhd")))

(define-public crate-emit-0.9 (crate (name "emit") (vers "0.9.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "14x995a3fffqzc73l1bzb30vzy60vwqh2f07a7adzxwl83hzamb3")))

(define-public crate-emit-0.9 (crate (name "emit") (vers "0.9.2") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "1y1gm600qagrj1yb5wkjw3mk8jy481y39b50sfr8ifsnr7ak8fid")))

(define-public crate-emit-0.10 (crate (name "emit") (vers "0.10.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "04ih605mxzl4qfmz8dirp4n2xr85wcnylvdmr4fxziig3qppkbx5")))

(define-public crate-emit-crash-0.1 (crate (name "emit-crash") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0b88rlqa1mb48pyx0svlmn8gr4xj8pr9syv52x48smwrs7rl2p52")))

(define-public crate-emit_ansi_term-0.1 (crate (name "emit_ansi_term") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "emit") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "08k21z0wrrb3sp4pdncl8knbic41nz2xjadk4y43dzv4s8cy6wki")))

(define-public crate-emit_ansi_term-0.2 (crate (name "emit_ansi_term") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "emit") (req "^0.10") (default-features #t) (kind 0)))) (hash "1ds8ml0caxshsm6zj6ihbzhrp35r35fdmvxfz37485jp7dfabkvs")))

(define-public crate-emit_seq-0.1 (crate (name "emit_seq") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "emit") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "19qw3zry5y4l8jx0ffzz91cgqir907x8fp76fz6z7zl44ag25nvj")))

(define-public crate-emit_seq-0.2 (crate (name "emit_seq") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "emit") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "008h4nrlgar6f6ychk86hxps0618wpafcqa129k322wjq5qb5nv0")))

(define-public crate-emit_seq-0.3 (crate (name "emit_seq") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "emit") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7") (default-features #t) (kind 0)))) (hash "0xrfmjqwzaq8vkbvz917djlyrh5h47q8y0lbik7v2gq4dgivyx4k")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ingvlbvn73dk4qqwls770j3kc8hmaac1fcy25hpm3wsv8s55yh0")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)))) (hash "199ybhw253n937yv4cmmxjdnrg3i9q1fp6v2mimmlnsaic8zxc0v")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.2") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zpwp1f3bsryv5iixk1lgjbgvx2vf2vy1anf97dfi730ajv6s49i")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.3") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)))) (hash "132h5sqkyh5xldk52kzcw4jagjzmi2qncnn27fnw1x9jjz2kss5r")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.4") (deps (list (crate-dep (name "hashbrown") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "103y8sxrl1fpk3gkpy684gx4l0v9ha0sglih7kr4aqy0ri406p23")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.5") (deps (list (crate-dep (name "hashbrown") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "1mj1pdhc67axyr7fdbxy97g94dgy8370dnyip3wgq32xg39mdz9r")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.6") (deps (list (crate-dep (name "hashbrown") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0jqk8bksiwgfvjzyarmih786i8i5j1hawcdvl9fdv248lrxhahjf")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.7") (deps (list (crate-dep (name "hashbrown") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "1k70qkcpxza9lp8pxyf1dkzl1f9hwvki1pq43zklhcywzq85dvsm")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.8") (deps (list (crate-dep (name "hashbrown") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0vr83qpvwjjx3d0iyg0g2xzw3mv61dw7aj62qpj2zi8ds5dmqikj")))

(define-public crate-emitbrown-0.1 (crate (name "emitbrown") (vers "0.1.9") (deps (list (crate-dep (name "hashbrown") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0c4mv4zz401d1cv7q6jwlyba8hwcg1dj77kpshw2zq5xkcjn42wd")))

(define-public crate-emitter-0.0.1 (crate (name "emitter") (vers "0.0.1") (hash "103my74ldnvrd3a8ypw5p8f867bkkddkhc069qdi4r760xfza9n3")))

