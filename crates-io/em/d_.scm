(define-module (crates-io em d_) #:use-module (crates-io))

(define-public crate-emd_earcutr-0.1 (crate (name "emd_earcutr") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 2)))) (hash "11z1c5k389jm7m29j5m1d63ff9s14ma31z607zyprf0clhd5xcna")))

