(define-module (crates-io em bd) #:use-module (crates-io))

(define-public crate-embd-0.1 (crate (name "embd") (vers "0.1.0") (deps (list (crate-dep (name "embd-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "145x4p0zdq8x8pzw9kzdqj23yvkpw7aqgdijlbc30wb6pd18mbjg")))

(define-public crate-embd-0.1 (crate (name "embd") (vers "0.1.1") (deps (list (crate-dep (name "embd-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "05hgsx9knk45zjxm9x745wncw4zlfrfrm56rxdf563rhwkalpx3k")))

(define-public crate-embd-0.1 (crate (name "embd") (vers "0.1.2") (deps (list (crate-dep (name "embd-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jwgxqvw1ranfa1nhxw9508jdvfaz8vg43hyg6prwqk9p84dy3r5")))

(define-public crate-embd-macros-0.1 (crate (name "embd-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0ngf7vk1frsgsm14i4a7wly9zf7j0a75l8ljvz69i9ympn2qxsxk")))

(define-public crate-embd-macros-0.1 (crate (name "embd-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "03vvfv3kl8dyzakwmw9n9wmspxg35vxyyw6gfx6q6d56305zz2kd")))

(define-public crate-embd-macros-0.1 (crate (name "embd-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "19n39hy30ikp3bj478aa41qicsaa3qj9qn8fgqkxd8if8888vsly")))

