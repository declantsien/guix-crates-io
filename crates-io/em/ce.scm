(define-module (crates-io em ce) #:use-module (crates-io))

(define-public crate-emcee-0.1 (crate (name "emcee") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1w2977hs7zmidymvmib4d4q1fcn6qsq8gil7kq8kya45pqwm70hp")))

(define-public crate-emcee-0.1 (crate (name "emcee") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "090k6rcr9439hx2p4kyp0dflm6q5858x0r6ghqilkmxskd9rlw0q")))

(define-public crate-emcee-0.2 (crate (name "emcee") (vers "0.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "03nbjsk7bqqj0wrwrzi2hviskpbjb96icq27hbmlzl6a767h7rpd")))

(define-public crate-emcee-0.3 (crate (name "emcee") (vers "0.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1ya5axmzl3aqamicrzfsd4nqjmh7pr63znwqral6jy7791dvc0yr")))

(define-public crate-emcee-1 (crate (name "emcee") (vers "1.0.0-alpha.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "13f0ni9dr3kx8r24bkigc2hvy9f73h0f8ka7khnqwwk1k0whga72")))

(define-public crate-emcee-1 (crate (name "emcee") (vers "1.0.0-alpha.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1bdgvcby4q102wdsc1lwas18dwqp7j6lcxlibm2iv52wssh71ncd")))

(define-public crate-emcell-0.0.0 (crate (name "emcell") (vers "0.0.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.7") (optional #t) (default-features #t) (kind 0)))) (hash "19nznnj6dnjys32lkflbb81mq07200chcf84igdrvzkn52sp8db8") (features (quote (("rt-crate-cortex-m-rt" "cortex-m") ("default" "rt-crate-cortex-m-rt") ("build-rs"))))))

(define-public crate-emcell-macro-0.0.0 (crate (name "emcell-macro") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.57") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0x0qm3jis8ksh7q1c6lc4y7r93sxhi3zymv8aj3lx0ljr39g903f") (features (quote (("default"))))))

