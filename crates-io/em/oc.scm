(define-module (crates-io em oc) #:use-module (crates-io))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.0-alpha.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1k5hwbfw5k153b14kdlj21g5ibk3yph8lhyahnnwdj1cd34wh1lx") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.0-alpha.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0q38lrc9sx88hhbr4jz5j0l9ac6wmb25fwn76md2r5k9k5ca0iz0") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.0-alpha.2") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0rw1f6mkbj2myag2bjjbyi3cgig1apzhcr34zhngpmcjcvq7vqmi") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.0-alpha.3") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1jp7h69nvm6pd6i10g39v1dfw38kfmf0bwq31rblnqsp381c46yj") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "192pwian17bfxn2ildl9nch23kswqh3z6drk1gi1j5kmgck0bp4n") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "02qmnank4hhxq0saggaafk5yd0z137l5p1ds9ifz6npibcpcly4l") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.2-alpha.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "05v7ch3m5cbmaw8wqd31vbjc9h2r2gr7axwg5xapnfvr47ggpqmv") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.3") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "1qan62yk10y8q4jqa5v2l03k5civmziphm9sj8g324fapw1cpihd") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.4") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "0v11jj5rg2gb37xxjx4l4w33lahm5gcqd3hr3c1i3sclfk1hd022") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.5") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "1qd5aqm8incx1apipjziskal38nrcd9drdf4misb9ipak1vj3g2k") (yanked #t)))

(define-public crate-emocli-0.8 (crate (name "emocli") (vers "0.8.6") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "1m38dydsg0b81bzwzm2zqiwnlklvrbfs2i52r7c3rh2dyx3jv4bq") (yanked #t)))

(define-public crate-emocli-0.9 (crate (name "emocli") (vers "0.9.0") (hash "06slpq40s4ilwk19h2vzhwxw9gzhq23anszlwia5zfkhcc4hd7md")))

