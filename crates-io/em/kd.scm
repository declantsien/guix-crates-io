(define-module (crates-io em kd) #:use-module (crates-io))

(define-public crate-emkdir-0.0.0 (crate (name "emkdir") (vers "0.0.0") (deps (list (crate-dep (name "cplogger") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "die") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "io") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0rl70cn46h0kg0rfplzb68dc2bzxgq51j1n9inzqvxk99hviwh1c")))

(define-public crate-emkdir-0.0.1 (crate (name "emkdir") (vers "0.0.1") (deps (list (crate-dep (name "cplogger") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "die") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "17fn62wralpb2j6gz92rhl177f15yvnc1dghpgcpv63mg7am6mfs")))

(define-public crate-emkdir-0.0.2 (crate (name "emkdir") (vers "0.0.2") (deps (list (crate-dep (name "cplogger") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "die") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02myrh9z76fcicb2sp1sw8x7ykiwqvji580a9r04r43ixw55jmx4")))

