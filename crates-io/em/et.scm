(define-module (crates-io em et) #:use-module (crates-io))

(define-public crate-emeta-0.1 (crate (name "emeta") (vers "0.1.0") (deps (list (crate-dep (name "miniserde") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1a5yd7m8g4wr9j5nkgrvakk3prhxzfgvxk0wwi0qhgzbz79vhrmf")))

(define-public crate-emeta-0.1 (crate (name "emeta") (vers "0.1.1") (deps (list (crate-dep (name "miniserde") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "speedy") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0s91xvxp9pnbak8sbrr6g87d7p70znpd0jaf17wh8aqhz55chnib")))

