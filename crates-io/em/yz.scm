(define-module (crates-io em yz) #:use-module (crates-io))

(define-public crate-emyzelium-0.9 (crate (name "emyzelium") (vers "0.9.2") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "15yaxj1z8igv2xma3c992gzk0nzvjhp36q78shsphnx7q209jh6i")))

(define-public crate-emyzelium-0.9 (crate (name "emyzelium") (vers "0.9.3") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1khvhqlmf64i8dnjwvraj7mj6hcrfnh0ps46dbhbh3i63y4grrdk")))

(define-public crate-emyzelium-0.9 (crate (name "emyzelium") (vers "0.9.4") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1dza8alrg06rbsi52rnq1sry0g5r8bxdzvpqpi84zc513wf40p3q")))

(define-public crate-emyzelium-0.9 (crate (name "emyzelium") (vers "0.9.6") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0lwx8s8s55i0c2ix68r2ifyq5jjxpnj1df1p373pi5cmdl2maq6l")))

(define-public crate-emyzelium-0.9 (crate (name "emyzelium") (vers "0.9.8") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1hccpwlff1pv3nn2vpzj31zxhnx9fwylwz2wvrh33rl4p1gxflrf")))

(define-public crate-emyzelium-0.9 (crate (name "emyzelium") (vers "0.9.10") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1n6mhgdxb32qsrwqacfs6kg25h8wl0apbamqhjhli694vdr912hc")))

