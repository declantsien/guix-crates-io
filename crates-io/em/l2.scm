(define-module (crates-io em l2) #:use-module (crates-io))

(define-public crate-eml2html-0.1 (crate (name "eml2html") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eml-parser") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "17a83c8gw9q4y7y8b50yx29yndd0xc9my3q2mb69z932ih5zj33p")))

