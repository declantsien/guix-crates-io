(define-module (crates-io ny li) #:use-module (crates-io))

(define-public crate-nylisp_eval-0.1 (crate (name "nylisp_eval") (vers "0.1.0") (hash "0pvafbs9cy1l2as9pgwrm68wrvkvwpps9c9wsgyqmjyb3bppm5bp")))

(define-public crate-nylisp_eval-0.2 (crate (name "nylisp_eval") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "18qhykwwx4g1pb0pxnlbsfqb1xg97dicxgh461587f1lkp5r5msl")))

(define-public crate-nylisp_eval-0.2 (crate (name "nylisp_eval") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "1zs8pka3w5dr9wd3pq8dc744a9w07byb20ys88jf13hklc0m6inv")))

(define-public crate-nylisp_eval-0.2 (crate (name "nylisp_eval") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "04k09h97gfwdwpg28if3cd5m32wipdgjyy0jhcwg5wzf9xizfhpn")))

(define-public crate-nylisp_eval-0.2 (crate (name "nylisp_eval") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "1y1zqx9mayabbsg15ccg9y5fab3dk2g8lixpaiijh2khr76b8cxh")))

(define-public crate-nylisp_eval-0.2 (crate (name "nylisp_eval") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "08gr41bkrrayxvjdq31i0g23v1ayj8hih59al99a7xcdf4wfrinq")))

(define-public crate-nylisp_eval-0.2 (crate (name "nylisp_eval") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "0269qakhzm0dasif4vfwnqbsaagab3wd662lmyh4invvx0lcl7lf")))

(define-public crate-nylisp_eval-0.2 (crate (name "nylisp_eval") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("wasm-bindgen"))) (default-features #t) (kind 0)))) (hash "155y2c4m69aqis0q1zccc3wjljkrlf8wxdsbmmw29wvwaz1phih1")))

