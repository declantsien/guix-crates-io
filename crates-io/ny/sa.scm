(define-module (crates-io ny sa) #:use-module (crates-io))

(define-public crate-nysa-0.1 (crate (name "nysa") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "17v357bwqag8iqz5vp495y7q5m914hbgmmkdgzd1w154yxzpd6n5") (features (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.1 (crate (name "nysa") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "01ch7djxax1k8gqvsg8a9w6jd96dx22sw1adydkzp2wd5pa36dzn") (features (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.1 (crate (name "nysa") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ji1jk74m3n97pzr6gys9ffh9xkrgdyjfwdbx72fsfalwsyrzgyf") (features (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.2 (crate (name "nysa") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "17h92qkp2jgkkn98pvlvp0gzrsg83n0b3cl6z34gmjxi2dc7zlvn") (features (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.2 (crate (name "nysa") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "102cw00s1s48n6y63i6fhzycvy0100sd28q6aynz8bkicih76sll") (features (quote (("global" "lazy_static") ("default" "global"))))))

(define-public crate-nysa-0.2 (crate (name "nysa") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0g0jgxmlcjg379g1fxpz1gwfm2i8ymbzpagvcw14rk21j8lgg6dj") (features (quote (("global" "lazy_static") ("default" "global"))))))

