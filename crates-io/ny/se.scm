(define-module (crates-io ny se) #:use-module (crates-io))

(define-public crate-nyse-holiday-cal-0.1 (crate (name "nyse-holiday-cal") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "0qkidf0hnrhx9iy2fgj0bz9n4a3y9p0wcfl95nyjs6qyvq3wcy6k")))

(define-public crate-nyse-holiday-cal-0.2 (crate (name "nyse-holiday-cal") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.20") (optional #t) (default-features #t) (kind 0)))) (hash "1nlmgp7s8j5phql37bpgj47b4xaqx4phkrm4yqblb83lb2wpy0r6")))

(define-public crate-nyse-holiday-cal-0.2 (crate (name "nyse-holiday-cal") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.20") (optional #t) (default-features #t) (kind 0)))) (hash "1wzciia44951766l0ar8l2qqzcrys5qils5x61g90ws222m1z60j")))

