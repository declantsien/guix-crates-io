(define-module (crates-io ny lo) #:use-module (crates-io))

(define-public crate-nylon-0.1 (crate (name "nylon") (vers "0.1.0") (deps (list (crate-dep (name "core_affinity") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(any(target_os = \"linux\", target_os = \"macos\"))") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0kjs2s39nvp8zz9xbjxvbg8znnrs508kpjqw77iq3a1zh3nm4iy1")))

