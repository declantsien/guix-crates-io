(define-module (crates-io re it) #:use-module (crates-io))

(define-public crate-reiterate-0.1 (crate (name "reiterate") (vers "0.1.0") (deps (list (crate-dep (name "elsa") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1b58d5m8zdb4ansvbbw9vz0jzip27vyma1zr8j0divdpjyr5blhp")))

(define-public crate-reiterate-0.1 (crate (name "reiterate") (vers "0.1.1") (deps (list (crate-dep (name "elsa") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "17s0r2nf8v5adf5j2ahwf325qm6pr346pdjx1kpd84sivliz9qic")))

(define-public crate-reiterate-0.1 (crate (name "reiterate") (vers "0.1.2") (deps (list (crate-dep (name "elsa") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0px4mwnj7mrm1mc474w2n5scgsj8j55sa2m8ggzx1r87c6hxvvs9")))

(define-public crate-reiterate-0.1 (crate (name "reiterate") (vers "0.1.3") (deps (list (crate-dep (name "elsa") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0ql27xghr49425k5bxs8s71346ijxaz45vd7b667mis1584g0bbh")))

(define-public crate-reiterator-0.1 (crate (name "reiterator") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "0ybi6ylslkkcifyps87qhgsq3f7czin0b6b3h4mb1mxgzm7p1agw")))

(define-public crate-reiterator-0.1 (crate (name "reiterator") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "1g2zmn7mb43vf7qpa1xbahc5idfl2rir5ar47vp1qxmqc2jm8q3h")))

(define-public crate-reiterator-0.1 (crate (name "reiterator") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "1bf388k4adw6vpkliy6chpx6fx565lpvs5lafcdc7gklyvjp6d23")))

(define-public crate-reiterator-0.1 (crate (name "reiterator") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "1adbdlf23y693hrfvq08j2mll8jgxr74lcjxffdixdgbnswvh05k")))

(define-public crate-reiterator-0.3 (crate (name "reiterator") (vers "0.3.0") (deps (list (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)))) (hash "01a67qjk793yz5sm2mf88dq3gjw7hwhm6435jwa7bwlmx2nj72q2")))

