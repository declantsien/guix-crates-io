(define-module (crates-io re he) #:use-module (crates-io))

(define-public crate-rehexed-0.1 (crate (name "rehexed") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1krfxgij9jbsgdflpzk0lwvax2cwd0qzbrnf6d4dvl6izyzndc2b")))

(define-public crate-rehexed-0.1 (crate (name "rehexed") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "03k6qnmsk1bwm2js0m3nnbdymljl3gzmxxg5ban3mi4p4w55h0vp")))

