(define-module (crates-io re mt) #:use-module (crates-io))

(define-public crate-remtools-0.0.0 (crate (name "remtools") (vers "0.0.0") (hash "0lrnpbbiwh6vsw8avfjlb0938lfhiry9agczwqw0qyyv9wai43fm")))

(define-public crate-remtools-1 (crate (name "remtools") (vers "1.0.0") (hash "0dlvjk0gr2xkq35mncyx4x0q4gsklqm049hmmfhmchiji3h6cxz1")))

