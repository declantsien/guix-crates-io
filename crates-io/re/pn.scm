(define-module (crates-io re pn) #:use-module (crates-io))

(define-public crate-repng-0.1 (crate (name "repng") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "16p6ikcz6k2af9sm3bfa71xkbj8fah432xnds3w8x926wy291c3q")))

(define-public crate-repng-0.2 (crate (name "repng") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0c12miziix3nh9k2l340jnd3f1l4z6g28rz209gdgijcsf4rnq7b")))

(define-public crate-repng-0.2 (crate (name "repng") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "1zx79dix3298wfnzhqcbn5jznjmh9m04kr0zv9cqqrmhrwqhf2cl")))

(define-public crate-repng-0.2 (crate (name "repng") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "1nmvba099zkidyba8vbn2g03nbx3wmadc928xfrrkijwrg97rm8d")))

