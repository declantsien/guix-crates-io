(define-module (crates-io re nl) #:use-module (crates-io))

(define-public crate-renls-0.1 (crate (name "renls") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lw7ghjc01gpzdng09yxs9sls51z24c0pnlqgdv879fqrq6bgq59")))

(define-public crate-renls-0.1 (crate (name "renls") (vers "0.1.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16d3dq6x9b4mas6znr0y5wmsbivr2n4s8vqj7cnrhx4myazg90v6")))

