(define-module (crates-io re v_) #:use-module (crates-io))

(define-public crate-rev_bits-0.1 (crate (name "rev_bits") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0nvfgqb3di9zf2iz3xipq66wdlhp8z9qljlcl9v5k32mmvvrrb74")))

(define-public crate-rev_bits-0.1 (crate (name "rev_bits") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "18q0wqr9b45hqfkp57whiz6wqqk0jmv56ijim2ix7riq5dwhsf7g")))

(define-public crate-rev_bits-0.1 (crate (name "rev_bits") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0glghw40s8mmina7wcgww79d12aanywsbn2w448mrynj0nq8z0m5")))

(define-public crate-rev_buf_reader-0.1 (crate (name "rev_buf_reader") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "03pnkfrm8bqzpjghma07wbg31qxxydlrirx91x3q9z0frfq860xk") (features (quote (("read_initializer") ("iovec") ("default"))))))

(define-public crate-rev_buf_reader-0.1 (crate (name "rev_buf_reader") (vers "0.1.1") (deps (list (crate-dep (name "memchr") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "177l0alryc1fvm071qy0qzgnhkiw6ni9vcba9y0mzbmacjcrb5qa") (features (quote (("read_initializer") ("iovec") ("default"))))))

(define-public crate-rev_buf_reader-0.2 (crate (name "rev_buf_reader") (vers "0.2.0") (deps (list (crate-dep (name "memchr") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "0hgplkmyg59yych6l33ssxl4yfbh3kdw8hyginhls8as87wfmrsv") (features (quote (("read_initializer") ("default"))))))

(define-public crate-rev_buf_reader-0.3 (crate (name "rev_buf_reader") (vers "0.3.0") (deps (list (crate-dep (name "memchr") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "047rfp65hwi5wmxj9157s603g2kjhhbjwvl2b44r4a8fw13jw3wc") (features (quote (("read_initializer") ("default"))))))

(define-public crate-rev_lines-0.1 (crate (name "rev_lines") (vers "0.1.0") (hash "061ci18bp1rqq8k6ishlk0rajl0hbcjnz517krpmkvcyyzlabg4v")))

(define-public crate-rev_lines-0.2 (crate (name "rev_lines") (vers "0.2.0") (hash "0bandfl4fqa77x6dg698ln5k4p2wx7xfczjmyrr5qg7pn89p4dcq")))

(define-public crate-rev_lines-0.2 (crate (name "rev_lines") (vers "0.2.1") (hash "1cw8mf0gpskm1wxsnsjaq1gnzp1vi3jcgjkg2d9i0csdcsv55sqq")))

(define-public crate-rev_lines-0.2 (crate (name "rev_lines") (vers "0.2.2") (hash "0k9hd109aabx13k2pi6cgjd91fyxapzqwsm9vv9fxvl0fbw9g0mg")))

(define-public crate-rev_lines-0.3 (crate (name "rev_lines") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0pzh5g31ljhkzinpahg8zq05c0gz54q307ayz89vzk55qxm92qpd")))

(define-public crate-rev_slice-0.1 (crate (name "rev_slice") (vers "0.1.0") (hash "0dglpq7w6y18ai34wqmf352ipfxx361mf9bpqxv6vbvcwp08jnlr") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-rev_slice-0.1 (crate (name "rev_slice") (vers "0.1.1") (hash "012sa9i2rc3qh835vpwp90b6r9jzgfdadwdz7ychqvm7hb3n22gb") (features (quote (("std") ("default" "std"))))))

(define-public crate-rev_slice-0.1 (crate (name "rev_slice") (vers "0.1.2") (hash "195hc5hmckvxpn31xk7a64mq3hgk2b4abhab2bb6bs6gsbd6049s") (features (quote (("std") ("docs_rs_workarounds") ("default" "std"))))))

(define-public crate-rev_slice-0.1 (crate (name "rev_slice") (vers "0.1.3") (hash "1s6jd70lyjvi8vg1430skzbynbscip6khbkw5ihl5fk8sa461yjs") (features (quote (("std") ("default" "std"))))))

(define-public crate-rev_slice-0.1 (crate (name "rev_slice") (vers "0.1.4") (hash "1rphlva8imqcc8gzi5k85q0q451zdp0943h30mkgin9264p09f97") (features (quote (("std") ("default" "std"))))))

(define-public crate-rev_slice-0.1 (crate (name "rev_slice") (vers "0.1.5") (hash "0m1r9sybn1wavn57bbigmz1sj2mvhjphnz38qg5z1aanbblh5n3f") (features (quote (("std") ("default" "std"))))))

