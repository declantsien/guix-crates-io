(define-module (crates-io re dy) #:use-module (crates-io))

(define-public crate-redyq-0.1 (crate (name "redyq") (vers "0.1.0") (deps (list (crate-dep (name "redis") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)))) (hash "0q4ymcslmrmnrddyhmc9y34mc0vfj21c631kji3yln90aa3is0py")))

(define-public crate-redyq-0.1 (crate (name "redyq") (vers "0.1.1") (deps (list (crate-dep (name "redis") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)))) (hash "1idip1i85w6f22jyh74xmpzhkpn6azhsjqr0g9akp76dbn9px4dh")))

