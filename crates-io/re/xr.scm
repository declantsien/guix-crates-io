(define-module (crates-io re xr) #:use-module (crates-io))

(define-public crate-rexrocksdb-0.3 (crate (name "rexrocksdb") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "librocksdbsys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0wzh2gx1qn7jcla4g9skjpz60x5lbrhv9d4z2ipfxffdplg1bgc0") (features (quote (("valgrind") ("sse" "librocksdbsys/sse") ("portable" "librocksdbsys/portable") ("default"))))))

