(define-module (crates-io re pa) #:use-module (crates-io))

(define-public crate-repac-cli-0.1 (crate (name "repac-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "repac-lib") (req "^0.1") (default-features #t) (kind 0)))) (hash "0phfz4l3s3by2lrq7iiqrivp5ah4cfi0anylbknjdsw6hlrfbqmb") (yanked #t)))

(define-public crate-repac-gui-0.1 (crate (name "repac-gui") (vers "0.1.0") (deps (list (crate-dep (name "repac-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "slint") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "slint-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0i8170y8p8cfagmcigd5x5rwj3aim6fx5hgyfj3k52hx1470rz4s") (yanked #t)))

(define-public crate-repac-lib-0.1 (crate (name "repac-lib") (vers "0.1.0") (deps (list (crate-dep (name "libflate") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "03a6yc09hwpjynqhmgvs45nc64jwmyzs2b4l1pjaj7jva17ripy3") (yanked #t)))

(define-public crate-repackage-0.1 (crate (name "repackage") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cargo_toml") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.3") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "tar") (req "^0.4.32") (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0gmka4ykkbvgwcvgd18636ln0l5ik97srvmfnm5v8qxm57yq8yvf")))

(define-public crate-repackage-0.1 (crate (name "repackage") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cargo_toml") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.3") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "tar") (req "^0.4.32") (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1yd7ynkm527wbq0mgb9j5gmila4plbvqb4k9r9hyzp83jkyxsja9")))

(define-public crate-repair_json-0.1 (crate (name "repair_json") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "05da09qk8qg1l6ylra5ay843jkjq4l71cx4vnlnffpqjhkhr3qay")))

(define-public crate-repak-cli-0.1 (crate (name "repak-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "repak-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1319k34n4d3rqbl9vwqisjcqdd7f18kypnvjypkbrf41avz5q1wd") (yanked #t)))

(define-public crate-repak-gui-0.1 (crate (name "repak-gui") (vers "0.1.0") (deps (list (crate-dep (name "repak-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kj33p1sb6nmvmyw9h5l22jf6mmkyfa3jcwn1qpw922gdfjqi2l2") (yanked #t)))

(define-public crate-repak-lib-0.1 (crate (name "repak-lib") (vers "0.1.0") (deps (list (crate-dep (name "libflate") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "version-compare") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0115s2pzmr0a0zxz6f8n8b0n8c9swp5d2rmdgfxm6m76raadajgw")))

(define-public crate-repak-tui-0.1 (crate (name "repak-tui") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "repak-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0faixgw1gdahp38krny2qhl6icc96ig42ds6mm649ky7v0wz6b5c")))

(define-public crate-reparse-0.1 (crate (name "reparse") (vers "0.1.0") (hash "0h9jnb2g06npjj6w0bp21jhri37hprrpvnby1xjl6vx6nvygywyy") (yanked #t)))

(define-public crate-reparse-0.1 (crate (name "reparse") (vers "0.1.1") (hash "0jy3mb25jfr281zllwapy8ipmdrkiq6zkgi5kjk9m8cq3g41qw5b") (yanked #t)))

(define-public crate-reparser-0.1 (crate (name "reparser") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop-util") (req "^0.19.5") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "17wnmyfxjzqj7qhzvr82nm9a4ldfwddg2f1sgwfi9g7c0d221lpj")))

(define-public crate-reparser-0.1 (crate (name "reparser") (vers "0.1.1") (deps (list (crate-dep (name "lalrpop-util") (req "^0.19.5") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "1vj8j1q8szcm5dn77706dhfivc5iv1v92w30j9f88w1hlhdr1b83")))

(define-public crate-reparser-0.1 (crate (name "reparser") (vers "0.1.11") (deps (list (crate-dep (name "lalrpop-util") (req "^0.19.5") (features (quote ("lexer"))) (default-features #t) (kind 0)))) (hash "0m3a4jic8g384z0kj3kkz2inq0ibhjzzv7g82x2cv44rf7k63fa7")))

(define-public crate-repatch-0.1 (crate (name "repatch") (vers "0.1.0") (deps (list (crate-dep (name "anstyle") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^1.9.0") (features (quote ("unicode"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.16") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "diffy") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "grep-matcher") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "grep-regex") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "grep-searcher") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 0)))) (hash "1apxvpx8c5nk1vk8pm0jg5rs5yax983iqk9mys5paps42aslka1a")))

(define-public crate-repay-0.1 (crate (name "repay") (vers "0.1.0") (deps (list (crate-dep (name "ramn-currency") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0db9501k8gsf9bsk9fkii4v13scm3sixgx4aa1gm2c5r2dh3p2x2")))

(define-public crate-repay-0.1 (crate (name "repay") (vers "0.1.1") (deps (list (crate-dep (name "ramn-currency") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "051nrw76cr3iv03qx7wgqffvpm2mfjxj0r7b3zvlibnx6bdxg0pq")))

(define-public crate-repay-0.1 (crate (name "repay") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nb5x8vx5gc8kwxsfb6057pzb5p6c32k2kn5gbapi4d15b938ksh")))

(define-public crate-repay-0.2 (crate (name "repay") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1q5bgwz6cb9rza6k5m0qyd4mp8zd2waav5njh862k3dhqdayhfki")))

(define-public crate-repay-0.2 (crate (name "repay") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1j18n9qr6f5a753wy8dhgqpj45jpnbc5xc1b07qz88c4g80xba02")))

