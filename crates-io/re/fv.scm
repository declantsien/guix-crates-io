(define-module (crates-io re fv) #:use-module (crates-io))

(define-public crate-refview-0.1 (crate (name "refview") (vers "0.1.0") (deps (list (crate-dep (name "refview_derive") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "015a6crxb74s09zlpyhdbnar4flq5g9z5gbyl33xbnzigyhjspbw")))

(define-public crate-refview_derive-0.1 (crate (name "refview_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wbvgzlyfwzrshh3pc4a12wcx43zrvs3n2znmpw6q9nnnrhzk9ja")))

