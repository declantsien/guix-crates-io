(define-module (crates-io re ik) #:use-module (crates-io))

(define-public crate-reikna-0.5 (crate (name "reikna") (vers "0.5.0") (hash "1swwfayb4ywn4d1gsqc0y3hnbssz1srjkx777wcai4a8rxamyc4r")))

(define-public crate-reikna-0.6 (crate (name "reikna") (vers "0.6.0") (hash "0cc1vik4asc2pr1ncyhbgbwf7y6pyapp64l0s6bamsy3kbsfj4xa")))

(define-public crate-reikna-0.10 (crate (name "reikna") (vers "0.10.0") (hash "1h51mivxxncy24rmy987v22ih6c4zzv18g4sdw12b1fjakb3qi3j")))

(define-public crate-reikna-0.12 (crate (name "reikna") (vers "0.12.3") (hash "1jhfk2frdiffclkn557jvasw9qxzbi9ifa0rv1b9v0i5qw92nzsq")))

