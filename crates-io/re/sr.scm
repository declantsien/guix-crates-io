(define-module (crates-io re sr) #:use-module (crates-io))

(define-public crate-resrap-0.1 (crate (name "resrap") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wzlimyvrpq141qbcn36n3pbbjvpd92cxhvaik1jg6k29412lkbs")))

(define-public crate-resrap-0.2 (crate (name "resrap") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "077zsj3nkfrs8jn5jrjq5shym2zlgg43qn3zp019rsxx16dawqz5")))

(define-public crate-resrap-0.3 (crate (name "resrap") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "01ack4hdnr53483jgbzssxd97ixssziz3ki6937ph47k5ljihacm")))

(define-public crate-resrap-0.4 (crate (name "resrap") (vers "0.4.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "16dcxnn4r541isdphg3l2swr78f0cbh9diwbmgmqpfagklw982cb")))

(define-public crate-resrap-0.5 (crate (name "resrap") (vers "0.5.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0yy0ikmfpqww4znirk51gh33vn1dh8d0z7v4s3xc37bga5h3pry7")))

(define-public crate-resrap-0.6 (crate (name "resrap") (vers "0.6.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "05y06n61r638z6dgdpgvsyj1swvsbk3mbn4j1ag3dx6g49v82qi4")))

(define-public crate-resrap-0.8 (crate (name "resrap") (vers "0.8.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1d1r39ihgpdxx4anim4qqxadasmfq0vkm57v7yx8ws4wn5qgda2d")))

(define-public crate-resrap-0.9 (crate (name "resrap") (vers "0.9.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1b1j98lkdmsw3d3256waxzliqsqrc5a9h3hhk0sajrm5kswwli75")))

(define-public crate-resrap-0.10 (crate (name "resrap") (vers "0.10.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0d9nhz59y4did6q1hlbpzglq6cm4pxykdsimbypvmn77hxw4m04a")))

(define-public crate-resrap-0.11 (crate (name "resrap") (vers "0.11.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0p0v8qsq704ln1j5r5xs2y3i3phial1y3lqybyq2pyvsabfmmiqv")))

(define-public crate-resrap-0.12 (crate (name "resrap") (vers "0.12.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1s7m376pn6whhih77s3i9i7xljfcgb21krczdq9ms0ihpnhm6g4f")))

(define-public crate-resrap-0.13 (crate (name "resrap") (vers "0.13.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0p7jm5l25di3spg88cjxllcrfz0h5n85837vjivh8m4hh105jjmk")))

(define-public crate-resrap-0.14 (crate (name "resrap") (vers "0.14.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "18grmnz964k88fp8vj7iffdl8jcz3gqqxr0g4vz94mqwimjhqi4l")))

(define-public crate-resrap-0.15 (crate (name "resrap") (vers "0.15.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "17syb9vdm63qqfnghvlpaic7wpr56z6srwldy4lfykmz6ci4k5bp")))

(define-public crate-resrap-0.16 (crate (name "resrap") (vers "0.16.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1dhca2wgzm3i76q4588qf8jlis3lfalmcjmdkkw9dlbzimcfg5r9")))

(define-public crate-resrap-0.17 (crate (name "resrap") (vers "0.17.0") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "resrap_proc_macro") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "138hc5iz2ry33mgnd265716ppg6qfzlmsapaaf647f2pcr2r7i2i")))

(define-public crate-resrap_proc_macro-0.1 (crate (name "resrap_proc_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s3fgj5dclrghzrfhm121v0sjnmg0rs6rf6bmp0ss4ki9qddj11l")))

(define-public crate-resrap_proc_macro-0.2 (crate (name "resrap_proc_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qs9s8d8n5r4yf7n4cc8w775iacahvar3qrr73xqdh6klx6fyaj1")))

(define-public crate-resrap_proc_macro-0.3 (crate (name "resrap_proc_macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bdia1qr59sjygbm9iqykrb31mmbsbgg09vsk1d37k83k4kb738s")))

(define-public crate-resrap_proc_macro-0.4 (crate (name "resrap_proc_macro") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0nywsn69l0r2aj9knxrqf45kfvra2j8dpkq0fy684p2qz0ynbgs0")))

(define-public crate-resrap_proc_macro-0.5 (crate (name "resrap_proc_macro") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0nvk24qxxy2rg7rp01rjf00d469nsb4h6h82bawn3qrzpy95xwhi")))

(define-public crate-resrap_proc_macro-0.6 (crate (name "resrap_proc_macro") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0a1nas1n1b7wri1q9dphsj6z9xdnzrlaaysvh4l6q9r743kndfcg")))

(define-public crate-resrap_proc_macro-0.7 (crate (name "resrap_proc_macro") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1m9jx2jkhwrb624pqzhnn65nm6q8x0fsmgbwx26i4r36pkisrzpq")))

(define-public crate-resrap_proc_macro-0.8 (crate (name "resrap_proc_macro") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14nwl56ss99y3w39cx316fwksgbz45kqv8idln8l2v14fnvv8jys")))

