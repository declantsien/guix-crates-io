(define-module (crates-io re kt) #:use-module (crates-io))

(define-public crate-rekt-0.1 (crate (name "rekt") (vers "0.1.0") (hash "1xp2mvd393pqn44k6n52mwqrpxw1mm68z21178xsrd6i1l3czcq3")))

(define-public crate-rekt-common-0.1 (crate (name "rekt-common") (vers "0.1.2") (deps (list (crate-dep (name "cbindgen") (req "^0.26.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "17m59b9185kbkf1rzkr6c8mnsfbl9w3r3dbbqlyidfg7y9a6qlz7")))

(define-public crate-rekt-protocol-common-0.1 (crate (name "rekt-protocol-common") (vers "0.1.0") (hash "1avr00lv7w3fkjghms47ykfb2zggj9kmfxnpiffyfxbxlqjcqblg")))

(define-public crate-rekt-protocol-common-0.1 (crate (name "rekt-protocol-common") (vers "0.1.1") (hash "0vqpl5ki9nziss6czk18wjzjxban1cciwa6v7kma9y2k73y2r85m")))

(define-public crate-rekt_lib-0.1 (crate (name "rekt_lib") (vers "0.1.3") (deps (list (crate-dep (name "cbindgen") (req "^0.26.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0kxkfns1n4pxkrgs71i8rcqqbznml01gsq4g2ks5cajq3lysiz4f")))

(define-public crate-rekt_lib-0.1 (crate (name "rekt_lib") (vers "0.1.4") (deps (list (crate-dep (name "cbindgen") (req "^0.26.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0p3024p75dg16a5y4lzj8q6clf61ljkwk1j74d10jx9qv1gjrd0l")))

(define-public crate-rekt_lib-0.1 (crate (name "rekt_lib") (vers "0.1.5") (deps (list (crate-dep (name "cbindgen") (req "^0.26.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0ph7xix61nwcni79mcpwi1f6cc7cggflbq15sb9h4r9k1b9jh7bc")))

(define-public crate-rekt_lib-0.1 (crate (name "rekt_lib") (vers "0.1.6") (deps (list (crate-dep (name "cbindgen") (req "^0.26.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "1fi59f2zvzlrcx0cmrl4d15553yms2ihlvk9k7wlrl2w1jidfawn")))

