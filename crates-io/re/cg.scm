(define-module (crates-io re cg) #:use-module (crates-io))

(define-public crate-recgen-0.1 (crate (name "recgen") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "recgen-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1y5fzmpv7qqhpdc087h90xsr7ax93cabcp8rrmym8dm7b1mqql5w")))

(define-public crate-recgen-0.1 (crate (name "recgen") (vers "0.1.2") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "recgen-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mqsf5bkv72vgpc5l45q7ckk563wqz16qqiyffwjzq38jaibig04")))

(define-public crate-recgen-sys-0.1 (crate (name "recgen-sys") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1j7syqljsg1c4s3j2i3rlmdbs8vm12rwla9gbl77dl35qkh7vc7f")))

(define-public crate-recgen-sys-0.1 (crate (name "recgen-sys") (vers "0.1.2") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ky2lbbw7s046bxniqbw0gjys4588rl2mhr347j6yqxkf7wrpynf")))

