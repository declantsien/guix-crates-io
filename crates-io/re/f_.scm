(define-module (crates-io re f_) #:use-module (crates-io))

(define-public crate-ref_arena-0.1 (crate (name "ref_arena") (vers "0.1.0") (deps (list (crate-dep (name "brunch") (req "^0.5.2") (default-features #t) (kind 2)))) (hash "0l6ry1vxx7ml55pzv83c8f3xi22zqh2all78c41fhz0rx839r0ww")))

(define-public crate-ref_arena-0.1 (crate (name "ref_arena") (vers "0.1.1") (deps (list (crate-dep (name "brunch") (req "^0.5.2") (default-features #t) (kind 2)))) (hash "083gm8vnfazs2995d33ci87iplxgr6ikrc6cvgmslp5chkm8xrs0")))

(define-public crate-ref_clone-0.1 (crate (name "ref_clone") (vers "0.1.0") (hash "1x6mwrcxh4i7h13qrzg7aw8kjl4j508akqdgfpwvh3qqw07rnz5q")))

(define-public crate-ref_clone-0.2 (crate (name "ref_clone") (vers "0.2.0") (hash "00di3yzbq9l09hvkgqy4whajrgxpjk147wnnxw68wan82qdr9aq2")))

(define-public crate-ref_clone-0.3 (crate (name "ref_clone") (vers "0.3.0") (hash "0hm7lpmmz0flsdzajnpf4hl75a8mj1392i5y546gkx3w63256wld")))

(define-public crate-ref_clone-0.4 (crate (name "ref_clone") (vers "0.4.0") (hash "0kbcj4rhfyciyqmc2h462gknk2b5a8wsvng6fbnb8fskp98ky3h9")))

(define-public crate-ref_clone-0.5 (crate (name "ref_clone") (vers "0.5.0") (hash "1bq5v5iz5facml0mhn4xdck9nfa59s4v6ghrdxi47q3nd8qdbsam")))

(define-public crate-ref_clone-0.6 (crate (name "ref_clone") (vers "0.6.0") (hash "1k6rjl1aa0hcdafv12s1w9bssf3ixj50c8dvmppi23z0p33fpk0w")))

(define-public crate-ref_clone-0.7 (crate (name "ref_clone") (vers "0.7.0") (deps (list (crate-dep (name "ref_clone_derive") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0pah2lww7aqm93n6fmjyjllkvqand94xmaz35b415yy85jk1zpbx")))

(define-public crate-ref_clone-0.8 (crate (name "ref_clone") (vers "0.8.0") (deps (list (crate-dep (name "ref_clone_derive") (req ">=0.7.0, <0.8.0") (default-features #t) (kind 0)))) (hash "1yz7ybqbbf9517yvkzwpk94r5mg99j1yc5yccf5s13h9i0120r0h")))

(define-public crate-ref_clone_derive-0.1 (crate (name "ref_clone_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "ref_clone") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "13603j0if0ghfcaf6jnzrlw8prqi3v948cxylbl05xrcsxgh640z")))

(define-public crate-ref_clone_derive-0.2 (crate (name "ref_clone_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "ref_clone") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0dp4gh06ypxnd27rmiqw7fkw8bzqp5fsa4hjqqpk2p7yp2qlih49")))

(define-public crate-ref_clone_derive-0.3 (crate (name "ref_clone_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "ref_clone") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0j0vccpdg6vxdd7x8ifb22m99nxpb6a55a4sbc3ziv3d2zi3v3zw")))

(define-public crate-ref_clone_derive-0.4 (crate (name "ref_clone_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "ref_clone") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1da7xixp0fcikv85wb4ahhq1v27wx1kkvir7f6sr0ig4qcd7z9r5")))

(define-public crate-ref_clone_derive-0.5 (crate (name "ref_clone_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "ref_clone") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "108qv06g3gr8gzma1xq2bchwgdh96hj21spc030fvvcrv170q439")))

(define-public crate-ref_clone_derive-0.6 (crate (name "ref_clone_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1p24219mrkzszpx1iq7n84inc8xydb44704jh2nyyjdm7z72vmdw")))

(define-public crate-ref_clone_derive-0.7 (crate (name "ref_clone_derive") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "13klkgdxxzax5j6nr6lnd4qprz8sidn6mkk8f8k4qpp40qns13s8")))

(define-public crate-ref_count-0.1 (crate (name "ref_count") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.7.1") (features (quote ("futures" "checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("sync"))) (default-features #t) (kind 2)))) (hash "123sh2klsigzwppp95zkmykxdm42c22wklv6ghag6ynvilklk59c") (features (quote (("std") ("alloc"))))))

(define-public crate-ref_count-0.1 (crate (name "ref_count") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.7.1") (features (quote ("futures" "checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("sync"))) (default-features #t) (kind 2)))) (hash "02haljp2xipznw78gjmvwzjw78mf2c7f7jr2dblkbmbm9izpha2f") (features (quote (("std") ("alloc"))))))

(define-public crate-ref_count-0.1 (crate (name "ref_count") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.7.1") (features (quote ("futures" "checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("sync"))) (default-features #t) (kind 2)))) (hash "1nazk0p91fvcf2rqwfbpb0s3ns54ydwss326mammwfgi0bv703ib") (features (quote (("std") ("alloc"))))))

(define-public crate-ref_eq-0.1 (crate (name "ref_eq") (vers "0.1.0") (hash "0hrpf311nxji87gxcxx7b6xhxq1pz3hh86285lxx29wk2iv9da14")))

(define-public crate-ref_eq-1 (crate (name "ref_eq") (vers "1.0.0") (hash "1ndbgisjhpqrppqdwzjw5bmj3bzn9anjygyvi6b6drdb1ic0bgmm")))

(define-public crate-ref_filter_map-1 (crate (name "ref_filter_map") (vers "1.0.0") (hash "0gr1x1j5zrpnp04ndljy6wxxcdbh8bx1x3xa2kg0p9i432jm1xsi")))

(define-public crate-ref_filter_map-1 (crate (name "ref_filter_map") (vers "1.0.1") (hash "1vdm5xmm1kzabdf98f111b8gsr4zniga28pd854dl2a01s2fnp1b")))

(define-public crate-ref_kind-0.0.0 (crate (name "ref_kind") (vers "0.0.0") (hash "1f12vfshgqridg2366zbfakqfzs2cd7s3dqnyfk1bxmdv6bx0jin") (yanked #t)))

(define-public crate-ref_kind-0.1 (crate (name "ref_kind") (vers "0.1.0") (hash "0bi2mcv950fjrsxzkywillf8fvg67y5ffcgksfp7p7d3kddzgg2b")))

(define-public crate-ref_kind-0.2 (crate (name "ref_kind") (vers "0.2.0") (deps (list (crate-dep (name "bumpalo") (req "^3.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "1rig7334dj657sp5jz1zn7r9ny3qdf8dcw32csblw18ycn3rx6cj") (v 2) (features2 (quote (("bumpalo" "dep:bumpalo" "hashbrown/bumpalo"))))))

(define-public crate-ref_kind-0.2 (crate (name "ref_kind") (vers "0.2.1") (deps (list (crate-dep (name "bumpalo") (req "^3.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "1cljj74y9q5nf8jk8pl2f780433jfiqbcr7k6cldkkcxgb1544jr") (v 2) (features2 (quote (("bumpalo" "dep:bumpalo" "hashbrown/bumpalo"))))))

(define-public crate-ref_kind-0.3 (crate (name "ref_kind") (vers "0.3.0") (hash "1wghav4751napqk1zxgi40zk2mff4d5mc5alw4q6977qif51f8aa")))

(define-public crate-ref_kind-0.4 (crate (name "ref_kind") (vers "0.4.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12.3") (optional #t) (kind 0)))) (hash "1m93653l8rjwscq3d6my771cpp6fz1c9g0ypxaa05hm02ybkz3by") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.4 (crate (name "ref_kind") (vers "0.4.1") (deps (list (crate-dep (name "hashbrown") (req "^0.12.3") (optional #t) (kind 0)))) (hash "07x3kk0vfgk9hdj6ds3idv4d2gfxaigs609s4imd07zw7rsxvs66") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.4 (crate (name "ref_kind") (vers "0.4.2") (deps (list (crate-dep (name "hashbrown") (req "^0.12.3") (optional #t) (kind 0)))) (hash "1hwnakxgrwdz7v55vwzpv39zvvsynwlxjq5h12jbwlm6pnvin8wq") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.5 (crate (name "ref_kind") (vers "0.5.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14") (optional #t) (kind 0)))) (hash "0h7sw9ipais3852v28si427nw7z5cqyrldwfyb9dhqcqccd6a7a4") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_kind-0.5 (crate (name "ref_kind") (vers "0.5.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14") (optional #t) (kind 0)))) (hash "009494zglm1bqgbfaqmadz1xzjjl05cl82jsdvwvj09yd1kapq74") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("hashbrown" "dep:hashbrown"))))))

(define-public crate-ref_slice-0.1 (crate (name "ref_slice") (vers "0.1.0") (hash "1riz17lj5mdy0fb0v6s6d58qaar01y7frspwca3jaaaaj62m7nyc")))

(define-public crate-ref_slice-1 (crate (name "ref_slice") (vers "1.0.0") (hash "1wmsfkc4r180bqp9f0jpramqa69vyn7gjb8i4l2pzhq3i67izj94")))

(define-public crate-ref_slice-1 (crate (name "ref_slice") (vers "1.1.0") (hash "1b9dczvky84bvys9lkrhfydax5zajy03qnyc68kj6py8j6mb8ssl")))

(define-public crate-ref_slice-1 (crate (name "ref_slice") (vers "1.1.1") (hash "09qmyw14dx6i0bd36chm9r1h7g5agr3zdyp74lqd9dwpf42l0mw2")))

(define-public crate-ref_slice-1 (crate (name "ref_slice") (vers "1.2.0") (hash "0s5csx2w2arg1ya3pdi8apv4052i55w0ab7k11maf802h1w7h6sf")))

(define-public crate-ref_slice-1 (crate (name "ref_slice") (vers "1.2.1") (hash "1adpmza9cnfsms81riry1p60q1isadlyyams86wakswjzdrivvgl")))

(define-public crate-ref_thread_local-0.0.0 (crate (name "ref_thread_local") (vers "0.0.0") (hash "1rnvd1ka04c95s73kgwcp90g0i8bqaislg7lx944lxq05qmh44yq")))

(define-public crate-ref_thread_local-0.1 (crate (name "ref_thread_local") (vers "0.1.0") (hash "154d16z1kcxyv4am7j17jwpndg7v9vjnm8cjhrbkb536ix07gv8k")))

(define-public crate-ref_thread_local-0.1 (crate (name "ref_thread_local") (vers "0.1.1") (hash "1w7zw42bzywh9jfhlj5dswdh79dk7bvh4mmsamw9jy40lrh1dmd0")))

(define-public crate-ref_wrapper-0.1 (crate (name "ref_wrapper") (vers "0.1.0") (hash "07ncihh7kzdy9w3zni19yswf7273bf10jb3818nwlsh0pkqya8qf")))

(define-public crate-ref_wrapper-0.1 (crate (name "ref_wrapper") (vers "0.1.1") (hash "1fdc3gn9py681ngq79pkrcw3hzshms1zwg487byb1x1d7n2i4lcp")))

(define-public crate-ref_wrapper-0.1 (crate (name "ref_wrapper") (vers "0.1.2") (hash "1g5hmw3qflxpkr8z9nnxrq3w2wbif4g3r26c8qav6bb6d8adqqzp")))

(define-public crate-ref_wrapper-0.1 (crate (name "ref_wrapper") (vers "0.1.3") (hash "1bjnpx7q6yhq4xgaqvls2nbjaipjrsfmj729l7i440d5iw38wh8q")))

(define-public crate-ref_wrapper-0.1 (crate (name "ref_wrapper") (vers "0.1.4") (hash "1g6ydr80ra2py1nhv6byhba112h2r4h4xm2fwfrg990ipadbhpml")))

(define-public crate-ref_wrapper-0.1 (crate (name "ref_wrapper") (vers "0.1.5") (hash "1lq040shwq1wc4j6myhi0x4z7gy2zyg516q33bxf1fqqmg41hb6s")))

(define-public crate-ref_wrapper-0.2 (crate (name "ref_wrapper") (vers "0.2.0") (hash "1rbx3dp31dvks26l3nd6d5v860cr61p4k1azw03frrlxcdwq3w7w")))

(define-public crate-ref_wrapper-0.2 (crate (name "ref_wrapper") (vers "0.2.1") (deps (list (crate-dep (name "drop_tracer") (req "^0.1") (default-features #t) (kind 2)))) (hash "017pncsh1z0qq957gv905w85k71qyw8jzsa5pdfa1rwklb7jfrk6")))

(define-public crate-ref_wrapper-0.2 (crate (name "ref_wrapper") (vers "0.2.2") (deps (list (crate-dep (name "drop_tracer") (req "^0.1") (default-features #t) (kind 2)))) (hash "1x37qw9raz4a7mgjl49wqa0c3yznlabhr8i1wnyz6jdfj86nqvzw")))

