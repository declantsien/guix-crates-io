(define-module (crates-io re ag) #:use-module (crates-io))

(define-public crate-reagent-0.1 (crate (name "reagent") (vers "0.1.0") (deps (list (crate-dep (name "ramhorns") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "14c36g062g4kwczpr41hm0whdvv5rp43c1j23sl12vd0wzrlj0j2")))

(define-public crate-reagent-0.1 (crate (name "reagent") (vers "0.1.1") (deps (list (crate-dep (name "ramhorns") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1mpc08shd4nj281ki312jl39hfg2b89fkwkjn600l98rv90vwhy9")))

