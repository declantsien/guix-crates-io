(define-module (crates-io re gp) #:use-module (crates-io))

(define-public crate-regplace-0.1 (crate (name "regplace") (vers "0.1.0") (deps (list (crate-dep (name "globset") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "109i00w220k58144831dl2bpm95a318j20l6bjrvsdmrqsfimy7r")))

