(define-module (crates-io re dz) #:use-module (crates-io))

(define-public crate-redzone-0.1 (crate (name "redzone") (vers "0.1.0") (deps (list (crate-dep (name "soloud") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "18bdjm38ha4njmv7487lizg17nk4646clj18d17ryqg0mhp1h8ln")))

(define-public crate-redzone-0.1 (crate (name "redzone") (vers "0.1.1") (deps (list (crate-dep (name "soloud") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1drlsqnl2wnr5g472i8phws7fq956ffsrym4vw64favxcj0fv8kq")))

