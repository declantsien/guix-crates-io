(define-module (crates-io re cs) #:use-module (crates-io))

(define-public crate-recs-1 (crate (name "recs") (vers "1.0.0") (hash "0xwllfzx16g980f9dhhkc38wjiy34dv4zsjsfmc6ajlab3nbv5f0") (yanked #t)))

(define-public crate-recs-1 (crate (name "recs") (vers "1.0.1") (hash "0dxxx4qzhsc69lnnl3hf8yn7gbs4i5sb7k0808ach4irbl77y7b5")))

(define-public crate-recs-1 (crate (name "recs") (vers "1.1.0") (hash "0dx78dmqnmaalzxq8hgail303jjmbs72b7l0bnzjw6s1hh8435sd")))

(define-public crate-recs-2 (crate (name "recs") (vers "2.0.0") (hash "17kxdrxib65hjwhxggcablqrl30arpma1k1inzvlc8abx3rgzhlp")))

(define-public crate-recs-2 (crate (name "recs") (vers "2.0.1") (hash "1nk0ygj13rgybq00gs5bgc3mjj8hqp0nqw9jxvlp5pzpy4hvjzxf")))

(define-public crate-recstrm-0.0.1 (crate (name "recstrm") (vers "0.0.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "testtools") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 2)))) (hash "1cx1l3qbz52msgks8rbizv49n2yvr0yyqpzjpg2kag7psp5ynxp2") (rust-version "1.56")))

