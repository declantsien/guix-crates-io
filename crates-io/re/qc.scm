(define-module (crates-io re qc) #:use-module (crates-io))

(define-public crate-reqchan-0.5 (crate (name "reqchan") (vers "0.5.2") (hash "0qma08la1m6n7bx2s11ch28d8ww8zhhiqi9aly2q9l0ml4sm96zx")))

(define-public crate-reqchan-0.5 (crate (name "reqchan") (vers "0.5.3") (hash "0c1x9csm1sni5y2754rlqpm79d9jz3pd4wxq0r2ii4f6vr9wqw07")))

(define-public crate-reqchan-0.5 (crate (name "reqchan") (vers "0.5.4") (hash "16abqqzvg0vin9zqxf0akgq80kjd42yqjhp6ahisjfkypdbp09iy")))

(define-public crate-reqchan-0.5 (crate (name "reqchan") (vers "0.5.5") (hash "0jn6khfkxgj16yj85s4zrnkqmrvafd75ax4z168w5zqhglkyi6a2")))

(define-public crate-reqchan-0.5 (crate (name "reqchan") (vers "0.5.6") (hash "0flfsvy9vwmr10gh8f6kgx77h6pzkq777fzjs0yxl3zl13zk6mrr")))

(define-public crate-reqchan-0.5 (crate (name "reqchan") (vers "0.5.7") (hash "1fp26hm0q99m8p3vy4g6j09jv7k2j7dw2vgirvaq156p2xw3r8f5")))

(define-public crate-reqchan-0.5 (crate (name "reqchan") (vers "0.5.8") (hash "0z51c4dls5fd3k2jlzahin6b2fqzhvnrjadi7pd17zrmw9f5cg8h")))

