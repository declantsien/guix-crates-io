(define-module (crates-io re dp) #:use-module (crates-io))

(define-public crate-redpallas-0.0.0 (crate (name "redpallas") (vers "0.0.0") (hash "0qzbqwvwa2ssfs5gkz0mdx1zxl39q31jg9xwvmfq2inwn9f58rss")))

(define-public crate-redpanda-0.3 (crate (name "redpanda") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rdkafka") (req "^0.29.0") (features (quote ("zstd" "tracing" "cmake-build" "zstd-pkg-config"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "0px7k5zd7izsx1kphvm0mh486v6nnk8dph5q4j9d717b22l0ks46")))

(define-public crate-redpanda-0.4 (crate (name "redpanda") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rdkafka") (req "^0.29.0") (features (quote ("zstd" "tracing" "cmake-build" "zstd-pkg-config"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "1fgbnamccj1bd9hzhp4r78kfx6i5adn3f0bplppdp6pmnq7q9xc3")))

(define-public crate-redpanda-0.5 (crate (name "redpanda") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rdkafka") (req "^0.29.0") (features (quote ("zstd" "tracing" "cmake-build" "zstd-pkg-config"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tracing-test") (req "^0.1") (default-features #t) (kind 2)))) (hash "1prad2db668byp1xn2r9ww38vr08rmwdn3i77gbm3dmpdx5z57pp")))

(define-public crate-redpanda-http-0.0.1 (crate (name "redpanda-http") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "simplehttp") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0q9324a0x1s06c5wpc40kmpc1fmbhpfs5dl2i9j2lmlcgvn3814i")))

(define-public crate-redpanda-http-0.0.2 (crate (name "redpanda-http") (vers "0.0.2") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "simplehttp") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0zk83q7scqgnwh3ncdxhig1zxa1rdhqxcy0nhqbdsmdbf6881iji")))

(define-public crate-redpanda-http-0.0.4 (crate (name "redpanda-http") (vers "0.0.4") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "simplehttp") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0sa23lzgbfa78z7737cvksqv84hsd316v0f4q6mf3bwr3xpfq8sb")))

(define-public crate-redpanda-http-rust-0.0.1 (crate (name "redpanda-http-rust") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)))) (hash "1q2ilrhi4dg8qbvc2bszlg651ysrnxdz7pmk7qiqb13arhi909yr") (features (quote (("defaults")))) (v 2) (features2 (quote (("reqwest" "dep:reqwest"))))))

(define-public crate-redpanda-transform-sdk-0.0.1 (crate (name "redpanda-transform-sdk") (vers "0.0.1-dev+edeac7a3ef") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "apache-avro") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=0.0.1-dev") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=0.0.1-dev") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "1pfq6f98bvyv5wylh1ppnrbs4rvk6z4h8kqgdr3mw4mhfvln6p8c") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.1-rc4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.1-rc4") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.1-rc4") (default-features #t) (kind 0)))) (hash "1lx2q9dss5qq174qkkhyahig4zfba4fwa9gn9jjqp4q1dm5z26bh") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.2-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.2-rc1") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2-rc1") (default-features #t) (kind 0)))) (hash "0awbd1ryqi2g3lm0h9f9vvnl92bamh0x8wdd57ix83sgxvwny8rm") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.2-rc2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.2-rc2") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2-rc2") (default-features #t) (kind 0)))) (hash "1ady8fq8ram0d99z4wr1aj80mjyk4rppdf6396i7gix7ib8zyzy1") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.2-rc3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.2-rc3") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2-rc3") (default-features #t) (kind 0)))) (hash "11p7vc5aspwgrxj9wvy7ja585z1xr55w7652clnkl5zs2prd2pq5") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.2") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2") (default-features #t) (kind 0)))) (hash "0jjv5z1h5b7ixcd8pqj2viv3p2c99y2r00x6xlnjc7pmpq5xcy2r") (yanked #t)))

(define-public crate-redpanda-transform-sdk-0.1 (crate (name "redpanda-transform-sdk") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "002vy1qcjz179mi6qv6p40a6rlamslgscli1w0hmn8hd1gf8lsjm")))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.3-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.3-rc1") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.3-rc1") (default-features #t) (kind 0)))) (hash "1h91w329sslxvza1maziym7d5wfpz9jkvyqc6liksk478s4waf88") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.3") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.3") (default-features #t) (kind 0)))) (hash "1iv4a9g9d8lbyscncrh48blmq1i6cd09ypwx9c3zsgzm8wbg2k7q") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.4-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.4-rc1") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.4-rc1") (default-features #t) (kind 0)))) (hash "1cahksi6q29nbk5srrmb2yddvl35q34z8n4svmshjc5m4j389zbl") (yanked #t)))

(define-public crate-redpanda-transform-sdk-23 (crate (name "redpanda-transform-sdk") (vers "23.3.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=23.3.4") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.4") (default-features #t) (kind 0)))) (hash "1pixvzs5ggzk9vpvf1xvab7d7kjp2l3pgqbdpr6vwhblna6hkmc2") (yanked #t)))

(define-public crate-redpanda-transform-sdk-0.2 (crate (name "redpanda-transform-sdk") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 2)))) (hash "134358ba9a04ggav2vnyviyik4sfdjjq14rydkkdxxgqcjxqnb51")))

(define-public crate-redpanda-transform-sdk-1 (crate (name "redpanda-transform-sdk") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 2)))) (hash "1b7vl4rplbx1jq45j7rlfmzvkf52w2f8d297mib4r8fvjmcgy5h3")))

(define-public crate-redpanda-transform-sdk-1 (crate (name "redpanda-transform-sdk") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 2)))) (hash "1akk4cqfz2xmgmzqi7aw24p5hdq7ssjg6fnk5624bib2qlsdc3dw")))

(define-public crate-redpanda-transform-sdk-1 (crate (name "redpanda-transform-sdk") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "apache-avro") (req "^0.16.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-sys") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-types") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 2)))) (hash "03i3k5cqj43i7fgifrqhal902ln5s5s5069dm9j4bzfxzwl6j3bv")))

(define-public crate-redpanda-transform-sdk-sr-1 (crate (name "redpanda-transform-sdk-sr") (vers "1.0.0") (deps (list (crate-dep (name "redpanda-transform-sdk-sr-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-sr-types") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0r0kb13hjqk8mvy03s76pqhnk8krlqh4g6x2ah2qhr0ba5nkmsgy") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-1 (crate (name "redpanda-transform-sdk-sr") (vers "1.0.1") (deps (list (crate-dep (name "redpanda-transform-sdk-sr-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-sr-types") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "18zys73q0fgd81wdmzzhsqf64lbd3cy1k030klgzq4n66g978bq3") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-1 (crate (name "redpanda-transform-sdk-sr") (vers "1.0.2") (deps (list (crate-dep (name "redpanda-transform-sdk-sr-sys") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-sr-types") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1x8lcfmmgg3s89kp6vl7kzkfsqfawncbhj3xbxam12bmngra60fk") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-sys-1 (crate (name "redpanda-transform-sdk-sr-sys") (vers "1.0.0") (deps (list (crate-dep (name "redpanda-transform-sdk-sr-types") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0gfiyjkmmizpilqnclw888zdmrmgbxw4kkwpk2dgdmrqlk52cwf2") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-sys-1 (crate (name "redpanda-transform-sdk-sr-sys") (vers "1.0.1") (deps (list (crate-dep (name "redpanda-transform-sdk-sr-types") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1bw1vpi4zfvxpmwc2ww68j0cqqik3d5w3rx8vwwy076cglkd7vdw") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-sys-1 (crate (name "redpanda-transform-sdk-sr-sys") (vers "1.0.2") (deps (list (crate-dep (name "redpanda-transform-sdk-sr-types") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "10g32phmqbvdhm7pi2w8dn275j587nb3w5fbi7xl03lzp4lsz2vv") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-types-1 (crate (name "redpanda-transform-sdk-sr-types") (vers "1.0.0") (deps (list (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16y36k7hfa7anl7bs92a5p39dqfr3nk6awz9mf7skpgfz3p4z1y8") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-types-1 (crate (name "redpanda-transform-sdk-sr-types") (vers "1.0.1") (deps (list (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "16bbfg3vhfb26ynw21v47c0qv76qycc5n8044hdj8c2dl0xpr1j7") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sr-types-1 (crate (name "redpanda-transform-sdk-sr-types") (vers "1.0.2") (deps (list (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "17iqw99c3fbz3gqxvkrpcv16pkgnwxy22hgkc3x1ww49nfdp0n01") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-sys-0.0.1 (crate (name "redpanda-transform-sdk-sys") (vers "0.0.1-dev+edeac7a3ef") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=0.0.1-dev") (default-features #t) (kind 0)))) (hash "1hn7bxvvxgrbqyvh1lrfl31ykd2kn69qwf3fsa6p6bfyps2cgcjd") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.1-rc4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.1-rc4") (default-features #t) (kind 0)))) (hash "1gkb9as1dwc8dgv67apkx0lpgf1asxqz626xgfjv9z1mny3nx6fv") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.2-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2-rc1") (default-features #t) (kind 0)))) (hash "1bpai5k0sjyvwdpmqifh1ppad9m1wwbmnimh5wpggr9z58ndcb1s") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.2-rc2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2-rc2") (default-features #t) (kind 0)))) (hash "05rbx1rybc8s2yrwafap2y7qbqiwc5wsa2war3vlihnbpzrnzcva") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.2-rc3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2-rc3") (default-features #t) (kind 0)))) (hash "15klcw184sdskmppw29hc3pqy3djzlqa1xxi1ad0fmvjl794h6ma") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.2") (default-features #t) (kind 0)))) (hash "0fdl9dppbb7sp95bxia3j7jixgbwpkn4lzg2idhsr58zsm0hwlb5") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-0.1 (crate (name "redpanda-transform-sdk-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "1sq2hjmyi2hrmw1qrk442zkl0rgkx9x10vyib5p1qad9v28b94vv")))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.3-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.3-rc1") (default-features #t) (kind 0)))) (hash "1s21al2vpd4wbl64maxwrjm95qhzhfs1nsp2zq3r8vabi66np0ha") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.3") (default-features #t) (kind 0)))) (hash "1gs3p9c1i0r0sx7dw4lk2ana82kb9cpkrcrhclmb45gppdxv4rsi") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.4-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.4-rc1") (default-features #t) (kind 0)))) (hash "15353nlc51037pjmaksqx0zx5r30b36vccfk2jjk8w6zw5cbjf17") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-23 (crate (name "redpanda-transform-sdk-sys") (vers "23.3.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=23.3.4") (default-features #t) (kind 0)))) (hash "0hs42m27ssmb1v2jwnb4z8zilk94bcqyn5ha9f2xr254l983sypg") (yanked #t)))

(define-public crate-redpanda-transform-sdk-sys-0.2 (crate (name "redpanda-transform-sdk-sys") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "078w9n9mrhhkmqnxpxx3hb2vc96j4vqqs9dwkml5p8f3h5hzc2b2")))

(define-public crate-redpanda-transform-sdk-sys-1 (crate (name "redpanda-transform-sdk-sys") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0g7nlh810l8h5r03qc54b8bpxmsiazqn2zjy7kzlw9a709rkcxfy")))

(define-public crate-redpanda-transform-sdk-sys-1 (crate (name "redpanda-transform-sdk-sys") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0fc3hp3ka0rdvwhq2a1g92yaddnn7lmlnwgyf66sygcgd8djmkhp")))

(define-public crate-redpanda-transform-sdk-sys-1 (crate (name "redpanda-transform-sdk-sys") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "redpanda-transform-sdk-types") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "redpanda-transform-sdk-varint") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1mcchp1sdbwbn417r7p3vl68br3hy9hi89rg5v4ma3gpaid6mkm2")))

(define-public crate-redpanda-transform-sdk-types-0.0.0 (crate (name "redpanda-transform-sdk-types") (vers "0.0.0-dev+6956980246") (hash "0370047gc3k2ix1izyfxfk2rv2lnn398x97pwj6j3a8kx9zdpli3") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-0.0.1 (crate (name "redpanda-transform-sdk-types") (vers "0.0.1-dev+edeac7a3ef") (hash "1rcxmi1srinqdyvkch546kbbk0amwrpf4zrnaabgfq4j0kbp5vq8") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.1-rc4") (hash "084q7wr1zssg705d45nkg7ksl847r3yh4lrlynqr3a4cxsmz3z90") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.2-rc1") (hash "191chhm672yca0jwycg0m8y0b1l538a76jw6n0lqwjshpqj8473h") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.2-rc2") (hash "17l7s65rpkgxa0x2514p2r997phgdc8ayavcz7zqhfciy787gp1b") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.2-rc3") (hash "1pqk6c1rg45s8a7qscp7pnss3jimwfmsn447hsdyrxq2lrzmhfbv") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.2") (hash "08rjmimjhk0wgwa29cp6gbl562964cha3p6r796j05mlfja3ms9p") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-0.1 (crate (name "redpanda-transform-sdk-types") (vers "0.1.0") (hash "1g75pl89q10dqxw43lb683v14h1a504pxx993xf2c4a2sfrpfn9d")))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.3-rc1") (hash "168p4if8qvdh7316lsnxn5f7xhyw2xi4nz147imnh814ngdxw9sw") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.3") (hash "1bj534z8aqkjbqd6fq94aj11xdxfpqfpan97za2x1w7xcx1fp2cc") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.4-rc1") (hash "0fd3my5pw3450qjmijr1ad1vnrjvcr6a8gyv1jy0p7vswzd6k7cl") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-23 (crate (name "redpanda-transform-sdk-types") (vers "23.3.4") (hash "0syjp2y2x7s5g9zv3ldz8y56b98ipkrsl0qfdjnx1pxxwkaaahys") (yanked #t)))

(define-public crate-redpanda-transform-sdk-types-0.2 (crate (name "redpanda-transform-sdk-types") (vers "0.2.0") (hash "0fqi7ln1nszhsxk6xwg03yqjjb9r06scfsglq15w33rh00f43dr2")))

(define-public crate-redpanda-transform-sdk-types-1 (crate (name "redpanda-transform-sdk-types") (vers "1.0.0") (hash "0s3g7i3h2rryfkj22g4ixvly87fff7q9drka35kdf7jm1sd3kr3v")))

(define-public crate-redpanda-transform-sdk-types-1 (crate (name "redpanda-transform-sdk-types") (vers "1.0.1") (hash "0k9s0vwcisc2l2mjg15yddl4ak78rx27af8jf7v9ifi6mnwwrgv8")))

(define-public crate-redpanda-transform-sdk-types-1 (crate (name "redpanda-transform-sdk-types") (vers "1.0.2") (hash "1gzybw9w6lg7rm7svvz5yq0d4b16plczshy1l4gwdk6ka795vz9s")))

(define-public crate-redpanda-transform-sdk-varint-1 (crate (name "redpanda-transform-sdk-varint") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)))) (hash "1aa1x2v1ch3d8plzhmlhlv5vm42lg5rsxss54hbhriciwssv4g6v") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-varint-1 (crate (name "redpanda-transform-sdk-varint") (vers "1.0.1") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)))) (hash "0k121rajk2dxhw7dkh3s7pi28nj4ndi16y507mv7m6ll0b79xzcn") (rust-version "1.72.0")))

(define-public crate-redpanda-transform-sdk-varint-1 (crate (name "redpanda-transform-sdk-varint") (vers "1.0.2") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)))) (hash "027bdac4341drflw8v5hzp3jyny0ba9q8wkxwyxfgid908yvxwzf") (rust-version "1.72.0")))

(define-public crate-redpatterns-0.1 (crate (name "redpatterns") (vers "0.1.1") (deps (list (crate-dep (name "pomsky") (req "^0.11.0") (default-features #t) (kind 1)))) (hash "1y1brdry9bgy8iax7vrzvlnqrzmarn0g444wwaqk3v5866anam9g")))

(define-public crate-redpatterns-0.1 (crate (name "redpatterns") (vers "0.1.2") (deps (list (crate-dep (name "pomsky") (req "^0.11.0") (default-features #t) (kind 1)))) (hash "1rka5plxp8r0wsyxzx2sw624gmmsmvbw1byd5dbqa327sw5fdwi8")))

(define-public crate-redpatterns-0.2 (crate (name "redpatterns") (vers "0.2.0") (deps (list (crate-dep (name "pomsky") (req "^0.11.0") (default-features #t) (kind 1)))) (hash "12jp5mm5awkgmiigq27ii5rvn2vkq50g2xylkzbc4fbysmm6xw00")))

(define-public crate-redpen-0.1 (crate (name "redpen") (vers "0.1.0") (hash "1l6yfj05pa72rd14zfb1ixxn1cxd56gagy9q0v3s2z0a3phwcg4v")))

(define-public crate-redpen-linter-0.1 (crate (name "redpen-linter") (vers "0.1.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.10") (features (quote ("tmp"))) (default-features #t) (kind 2)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)))) (hash "1dpqyspx8zlsbrwbi4di6pk1gxba2h6rb25shqd4x9k9vgkybm3j") (rust-version "1.72.0")))

(define-public crate-redpen-linter-0.2 (crate (name "redpen-linter") (vers "0.2.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.10") (features (quote ("tmp"))) (default-features #t) (kind 2)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)))) (hash "0zm410mzbdiw75cpfkbh4c9q4bgic63lswr2q09mhv0gdjz4kn7i") (rust-version "1.72.0")))

(define-public crate-redpen-linter-0.3 (crate (name "redpen-linter") (vers "0.3.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.10") (features (quote ("tmp"))) (default-features #t) (kind 2)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)))) (hash "1h7gy1h9njnkzja4zv5wq90jdwdi0jsqzcp4xkp0z0d3dcwz4057")))

(define-public crate-redpen-linter-0.4 (crate (name "redpen-linter") (vers "0.4.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.10") (features (quote ("tmp"))) (default-features #t) (kind 2)) (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)))) (hash "17gjw3q7hj7a7cq888bjkwpva0907px9cak7bwrfr7yzm5sgbf4h")))

(define-public crate-redpen-shim-0.1 (crate (name "redpen-shim") (vers "0.1.0") (hash "02z60vyx8lqvx9blysl7pnxqdi9h6v61xmj1kx0hh547c23gvgcy")))

(define-public crate-redpen-shim-0.2 (crate (name "redpen-shim") (vers "0.2.0") (hash "05s905lc53ignh1xrxb4zrwsf45hq4j0biz7b3wl0qkgif88x5gg")))

(define-public crate-redpen-shim-0.4 (crate (name "redpen-shim") (vers "0.4.0") (hash "0zji92s4k6c4kdcp1jwh60zfq1xjssqizmai514l5vxs67fys3jk")))

(define-public crate-redpill-0.1 (crate (name "redpill") (vers "0.1.0") (hash "1wjcp9jv9ddgbfzb32kzj8y4mdp002fh4a9x62jdfbalkf1wzicx")))

(define-public crate-redpine-0.1 (crate (name "redpine") (vers "0.1.0") (deps (list (crate-dep (name "core_affinity") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "polling") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1") (default-features #t) (kind 0)))) (hash "1ybz3b4aj3xxhqh1kn83l2p16v3yj3zmwrwv1jq7hd57wvvzhj1c")))

(define-public crate-redpine-0.2 (crate (name "redpine") (vers "0.2.0") (deps (list (crate-dep (name "core_affinity") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "polling") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1") (default-features #t) (kind 0)))) (hash "1kg1qd6hacdw2nvy74yhair71i6p6cfnq7rdnj0yck3177m2w9cx")))

(define-public crate-redpine-0.2 (crate (name "redpine") (vers "0.2.1") (deps (list (crate-dep (name "core_affinity") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "polling") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1") (default-features #t) (kind 0)))) (hash "123k2mv9cfw5fdi8cwx55pwxrgjma2klk0f0dl32a6rrc63gcsli")))

(define-public crate-redpitaya-0.0.0 (crate (name "redpitaya") (vers "0.0.0") (hash "1a4xf9yfrapdc4adq771cgjqdf62c5cliwc3kp1l5cbhk93w3690")))

(define-public crate-redpitaya-0.1 (crate (name "redpitaya") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zn5cfpqsnf6fgn55sfbxv5gf10bb78zvpiw7r8fk48y27dl54dl")))

(define-public crate-redpitaya-0.2 (crate (name "redpitaya") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.4") (default-features #t) (kind 0)))) (hash "1f57ibdxsx361zpcd12v4pvzanri7v5ivcix0l3n2cjb11cwky55")))

(define-public crate-redpitaya-0.3 (crate (name "redpitaya") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.6") (default-features #t) (kind 0)))) (hash "09f80bj3l1kqjxpnib3asvj54rjfndjrblg8s4l7134c8lzi11ik")))

(define-public crate-redpitaya-0.6 (crate (name "redpitaya") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.9") (default-features #t) (kind 0)))) (hash "0i667vpx21iyayx74v9amras95j5acihhy53zdzia7gwnp8mr997")))

(define-public crate-redpitaya-0.8 (crate (name "redpitaya") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.9") (default-features #t) (kind 0)))) (hash "0xb16vlppacddizjjkz76c1r45likb1h9w1q1vz72zkjb0w3275y")))

(define-public crate-redpitaya-0.8 (crate (name "redpitaya") (vers "0.8.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.10") (default-features #t) (kind 0)))) (hash "09wxkjiwjaammpnhxzsb0x4l2r59yiwhsbj8gxkdrvqqwp97rdvw")))

(define-public crate-redpitaya-0.9 (crate (name "redpitaya") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.11") (default-features #t) (kind 0)))) (hash "1ga2cim1dh65xv76ljhf0g55raa5wxcj5mkz4107rd7ql1dv13g3")))

(define-public crate-redpitaya-0.10 (crate (name "redpitaya") (vers "0.10.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.13") (default-features #t) (kind 0)))) (hash "0gx00c1hh3dyzwwhm6x1chli61bszs5xyli5xg1z75899hjai6h7")))

(define-public crate-redpitaya-0.10 (crate (name "redpitaya") (vers "0.10.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.13") (default-features #t) (kind 0)))) (hash "1yrlbaf5prnwc4hgghi9pyq48sm9l1aanqwn3m766i8jyf7q73sp")))

(define-public crate-redpitaya-0.11 (crate (name "redpitaya") (vers "0.11.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.14") (default-features #t) (kind 0)))) (hash "1x72rnhkrghhcjwpl793kvbmv6mliw60implf0ml2l3j8s6v04xy")))

(define-public crate-redpitaya-0.13 (crate (name "redpitaya") (vers "0.13.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.16") (default-features #t) (kind 0)))) (hash "14vdfyk1r1pfkd5asp7rcgq2xgslgv20n2w791iibr64nikbdpm8")))

(define-public crate-redpitaya-0.14 (crate (name "redpitaya") (vers "0.14.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.17") (default-features #t) (kind 0)))) (hash "0fz3b4xlvh8ygg3xyvbrkprj4ds4ih5agz8nx0nc07swddcib09q")))

(define-public crate-redpitaya-0.15 (crate (name "redpitaya") (vers "0.15.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.18") (default-features #t) (kind 0)))) (hash "08qqxkjga2fw5dplfx0bn8ssy3d9ga8i3haw1p3l57iny227bk8g")))

(define-public crate-redpitaya-0.16 (crate (name "redpitaya") (vers "0.16.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.18") (default-features #t) (kind 0)))) (hash "0lrfvbhk9h2laj390jv3k2shr95f6pqggp4c7pl7nbgzv84qsjyq")))

(define-public crate-redpitaya-0.17 (crate (name "redpitaya") (vers "0.17.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.18") (default-features #t) (kind 0)))) (hash "0hsz3sfw7ai0ckhv35zkw62bna26l997ph8il1kc6gcggaf9qji2")))

(define-public crate-redpitaya-0.18 (crate (name "redpitaya") (vers "0.18.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.19") (default-features #t) (kind 0)))) (hash "0qah5f1qz8csa9gjsn6kpcqb8kayi8bv9bafgkpfgrkn0k0vb6xb")))

(define-public crate-redpitaya-0.19 (crate (name "redpitaya") (vers "0.19.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.20") (default-features #t) (kind 0)))) (hash "03rcvf8ds5djmq07ypq0slrj3b7xndf0hqzqxd74r588gck6qfdm")))

(define-public crate-redpitaya-0.20 (crate (name "redpitaya") (vers "0.20.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.20") (default-features #t) (kind 0)))) (hash "0l12h050z3msa5vpki7valirfckgjawx8hch498lqfba18q2148c")))

(define-public crate-redpitaya-0.21 (crate (name "redpitaya") (vers "0.21.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.20") (default-features #t) (kind 0)))) (hash "1sl56gd5cpxri2cfd6hk6sy8nklp603848bcfchlqjjhrbx6a4vr")))

(define-public crate-redpitaya-0.22 (crate (name "redpitaya") (vers "0.22.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.20") (default-features #t) (kind 0)))) (hash "0fy0n1fww7ybx0s7a6jm64pa85q9vf8hdaq2qr01jxi3gkgvx2p9")))

(define-public crate-redpitaya-0.22 (crate (name "redpitaya") (vers "0.22.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.20") (default-features #t) (kind 0)))) (hash "1jf3y4jj8ybbh2745g08vslx2vcyzxa523l0kikv3mnvj133iv5j")))

(define-public crate-redpitaya-0.22 (crate (name "redpitaya") (vers "0.22.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.20") (default-features #t) (kind 0)))) (hash "1z858904mfhp1rpnngipcxsf9wj31sxnjsi2i7164ql40kdf6q8g")))

(define-public crate-redpitaya-0.22 (crate (name "redpitaya") (vers "0.22.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.20") (default-features #t) (kind 0)))) (hash "04npi74kvifzivc3925dil01a1yszi3ar36hyzax71k13j5vri61")))

(define-public crate-redpitaya-0.23 (crate (name "redpitaya") (vers "0.23.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.21") (default-features #t) (kind 0)))) (hash "1sgn26z1k1q9y4fjpllaq2b23dj417vk9f0g7z7yhl5ja94sl9s9")))

(define-public crate-redpitaya-0.23 (crate (name "redpitaya") (vers "0.23.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.21") (default-features #t) (kind 0)))) (hash "1pmyhyqq4pilj2kkrl6kwdps9cns78hgcj569r5xcghcr87fqgvg")))

(define-public crate-redpitaya-0.23 (crate (name "redpitaya") (vers "0.23.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.21") (default-features #t) (kind 0)))) (hash "1shyp4wxgiyzfchqlyklsmka2mvzag5lc40zdja9ryr10z3viy63")))

(define-public crate-redpitaya-0.24 (crate (name "redpitaya") (vers "0.24.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.22") (default-features #t) (kind 0)))) (hash "1sxs70zka9bbnkb7w78gxi9nhscsxh8bng4bfmxir1vh7fszgrzi")))

(define-public crate-redpitaya-0.25 (crate (name "redpitaya") (vers "0.25.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.23") (default-features #t) (kind 0)))) (hash "0v60vicfbn1hv2d5yril5jb54xxfv93swh8c03cl6mscc1kmiih6")))

(define-public crate-redpitaya-0.26 (crate (name "redpitaya") (vers "0.26.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.24") (default-features #t) (kind 0)))) (hash "1rijk62269zbdw1lznrgbrnw779rwbw41yqb6g630qf91nycl3wj")))

(define-public crate-redpitaya-0.27 (crate (name "redpitaya") (vers "0.27.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-mock") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.25") (optional #t) (default-features #t) (kind 0)))) (hash "01c7ih74186lnhkblnp4lwn0db9mzk3ranj1y7aaa2f0zfnb5ri1") (features (quote (("mock" "rp-mock") ("default" "rp-sys")))) (yanked #t)))

(define-public crate-redpitaya-0.28 (crate (name "redpitaya") (vers "0.28.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.26") (default-features #t) (kind 0)))) (hash "0j86vsf6bq4584i7prww07jd8rxh1a64kz50006i3ns5lx8f1czm") (features (quote (("mock" "rp-sys/mock") ("default"))))))

(define-public crate-redpitaya-0.29 (crate (name "redpitaya") (vers "0.29.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.27") (default-features #t) (kind 0)))) (hash "02xk54pgj8bbyryfa1w1v1zrpc6gcy0c3q860qiz82njhz64i42q") (features (quote (("mock" "rp-sys/mock") ("default"))))))

(define-public crate-redpitaya-0.30 (crate (name "redpitaya") (vers "0.30.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.27") (default-features #t) (kind 0)))) (hash "0hghci1dfjzq8vbh906ph2im5sml388vpwhg75x8ir7p34bc4d4s") (features (quote (("mock" "rp-sys/mock") ("default"))))))

(define-public crate-redpitaya-0.31 (crate (name "redpitaya") (vers "0.31.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rp-sys") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "1vwlx2qd22v4lpk17yg7jxjrlpbbp3xzaiszn8zwpgvibgb682k6") (features (quote (("mock" "rp-sys/mock") ("default" "rp-sys"))))))

(define-public crate-redpitaya-scpi-0.1 (crate (name "redpitaya-scpi") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1141cis3p5kzqk7yjh16i1bim4dw2mc1ivxb4097zgdhvvpzi1ld")))

(define-public crate-redpitaya-scpi-0.2 (crate (name "redpitaya-scpi") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vdfr75h9fk7bgisgp60vmbs743sz6xmzq1vwh7322iqprdjnqq0")))

(define-public crate-redpitaya-scpi-0.3 (crate (name "redpitaya-scpi") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vna7zmx7i4vq63kjllnnw5g16bkg61qw4p5m4qwph67vvmxzqps")))

(define-public crate-redpitaya-scpi-0.4 (crate (name "redpitaya-scpi") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kp0xdg3zk8mknhsqvkmzf31prnycb5s13qgi55lcpwy1hpr8kl2")))

(define-public crate-redpitaya-scpi-0.5 (crate (name "redpitaya-scpi") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rd43qff9qdn6ww168d90x34f6av98cmnl0w1h617g407r0jsspy")))

(define-public crate-redpitaya-scpi-0.6 (crate (name "redpitaya-scpi") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jxmxmpzisf3s63sy9v8p8iaa38qwcyrcb94ds8fchjikvw6w3yf")))

(define-public crate-redpitaya-scpi-0.7 (crate (name "redpitaya-scpi") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "129x2xjm71100k5vhn4af747hrah2w7hkgdbfilk2s1d9l3hfaki")))

(define-public crate-redpitaya-scpi-0.8 (crate (name "redpitaya-scpi") (vers "0.8.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0sfm37kp33iv8n8xld64ql8jgki26608yxf7cp7bgah4vc0w9153")))

(define-public crate-redpitaya-scpi-0.9 (crate (name "redpitaya-scpi") (vers "0.9.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "10qpf0fdhbh16k8ykv9k2jhpmakb3cb0mlwg2s1yv2qmqm539bsw")))

(define-public crate-redpitaya-scpi-0.10 (crate (name "redpitaya-scpi") (vers "0.10.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vl2vcp80r0yi2x47zmq9dmqzx4k9s8lppgf566jgi8fki2k53yv")))

(define-public crate-redpitaya-scpi-0.10 (crate (name "redpitaya-scpi") (vers "0.10.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mjs6rk5d1k6jfigkcypbj6ppqkw1isj204jyc8mq7hrfww6vy80")))

(define-public crate-redpitaya-scpi-0.11 (crate (name "redpitaya-scpi") (vers "0.11.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0fm87li2gvzl3d33rpmf158wf3szz3l5c8v0hkp83phphyzpy0rc")))

(define-public crate-redpitaya-scpi-0.12 (crate (name "redpitaya-scpi") (vers "0.12.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jsjb9hm9az333gz4pw84l58xcawhksjgr3waxl7qmi92k5ac87m")))

(define-public crate-redpitaya-scpi-0.13 (crate (name "redpitaya-scpi") (vers "0.13.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "12m9179p05bl4v5m2a55smix00mnksy60zwqsx21sh4ml1ly1rg9")))

(define-public crate-redpitaya-scpi-0.14 (crate (name "redpitaya-scpi") (vers "0.14.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0i6llv4bbsjq4lyqphglaxb4fxk9xlxq9d93im4p1sphp96sq9zz")))

(define-public crate-redpitaya-scpi-0.15 (crate (name "redpitaya-scpi") (vers "0.15.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1swcg4akaygp13i82cysq621bjv3ifp8a4g816abbny1fjd84067")))

(define-public crate-redpitaya-scpi-0.15 (crate (name "redpitaya-scpi") (vers "0.15.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zg2jh14a5gxipbmiacif9856gfsafqvbjlij8znydy0g7fwl3zy")))

(define-public crate-redpitaya-scpi-0.16 (crate (name "redpitaya-scpi") (vers "0.16.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0rhqh7p351s3kn9p47s7xv134w4qg6ys4bq9nvdhrm95bir2ysky")))

(define-public crate-redpitaya-scpi-0.17 (crate (name "redpitaya-scpi") (vers "0.17.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dd03sdv703gln5vvhslp9bhhskbyivfmdk5xwlz55h92ph2vnms")))

(define-public crate-redpitaya-scpi-0.18 (crate (name "redpitaya-scpi") (vers "0.18.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ll5x8d2kccwdhq5kga6i501mhlq4jfp56jhjahdj18yqlgs6cly")))

(define-public crate-redpitaya-scpi-0.19 (crate (name "redpitaya-scpi") (vers "0.19.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1iqs9qfk80qnhxnmhn0zaads2fi8xar0k4szr40w7fv9jjd8jdpp")))

(define-public crate-redpitaya-scpi-0.20 (crate (name "redpitaya-scpi") (vers "0.20.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pqnn0bh18ymkvavpyisv93ckzjfk35hjgyikhk1s4jq5whwhbh9")))

(define-public crate-redpitaya-scpi-0.21 (crate (name "redpitaya-scpi") (vers "0.21.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "12kfpq2npc2wc9hjbj9rmxzyhyi8lmlng9izg18mh3bwf1gnrkxw")))

(define-public crate-redpitaya-scpi-0.22 (crate (name "redpitaya-scpi") (vers "0.22.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "09n0d2yjkdya4065zmhwr3aq9qbr6r8wpspxbjla6sxpyhykfjsk")))

(define-public crate-redpitaya-scpi-0.23 (crate (name "redpitaya-scpi") (vers "0.23.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1br7v8401v96qdk2k6s5sm9s47g9fmcbc93d6f7vwssnrx5cpbha") (features (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.23 (crate (name "redpitaya-scpi") (vers "0.23.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hkb0m0bj7iry5zq27j01i6ghix8lkn7dfcf7939xlyx203sf1dy") (features (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.24 (crate (name "redpitaya-scpi") (vers "0.24.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zjd5m2ckqxrgndfipmg29xis6r17aq5hfd3wvffz99r4clnnzhj") (features (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.25 (crate (name "redpitaya-scpi") (vers "0.25.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rqq33fbi4dv8xvgq9kxb50fb1fzjcgazqy1vf8s04yczs8vp712") (features (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.26 (crate (name "redpitaya-scpi") (vers "0.26.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1sk7rrr3fp72a7prkwjzxmffkp44nh05m7664b6angw8d31cznpc") (features (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.27 (crate (name "redpitaya-scpi") (vers "0.27.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "16p095njbxcdfi068zm22vw123nvnq759wp71vlsm89z3x1bbxi9") (features (quote (("mock") ("default"))))))

(define-public crate-redpitaya-scpi-0.27 (crate (name "redpitaya-scpi") (vers "0.27.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0w7xrw6jvhm4ci6lppslv32rgkqk1vq8g2m72g89zc09v1xrr4mr") (features (quote (("mock") ("default"))))))

