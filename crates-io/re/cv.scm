(define-module (crates-io re cv) #:use-module (crates-io))

(define-public crate-recv-dir-0.1 (crate (name "recv-dir") (vers "0.1.2") (hash "1dg5p8k1vh473m0r9z6pchg503scilh56ncnallc299d111l7gix")))

(define-public crate-recv-dir-0.1 (crate (name "recv-dir") (vers "0.1.3") (hash "0lbfpg13kflmwrwv2m2c9hvy7fll8fghnlqm7869msgi0r6j1g1s")))

(define-public crate-recv-dir-0.2 (crate (name "recv-dir") (vers "0.2.0") (hash "1k8jsim1r70hyj8hsc2gk4azfx5xbks8fhdvsmh9fi9y2gd9fs80")))

(define-public crate-recv-dir-0.2 (crate (name "recv-dir") (vers "0.2.2") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0crdql6nx2yqqfwdanxppswcsj10m6s3zslv397vxl56qfpi9gcq") (features (quote (("default") ("async" "tokio"))))))

(define-public crate-recv-dir-0.2 (crate (name "recv-dir") (vers "0.2.3") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "09f0v4qqxb60hhrdk2di8j405rl0v43gd9rjrdfl34hlkzml21pg") (features (quote (("default") ("async" "tokio"))))))

(define-public crate-recv-dir-0.3 (crate (name "recv-dir") (vers "0.3.0") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0iwxj7v1j4a5y2rcx5f8xqd29p8cn24szdv44wkwv0hgah2g44dh") (features (quote (("default") ("async" "tokio"))))))

(define-public crate-recv-dir-0.3 (crate (name "recv-dir") (vers "0.3.1") (deps (list (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1hh627vbrr3r0yn5ii72s38iz5x8dm0jfj066pfwj203ahpnbq4z") (features (quote (("default") ("async" "tokio"))))))

(define-public crate-recvmsg-1 (crate (name "recvmsg") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.150") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("rt" "net" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "windows-sys") (req "^0.52.0") (features (quote ("Win32_Networking_WinSock"))) (optional #t) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0xa173gbg1cx8q7wyzi6c4kmcsz5rka68r4jb6kg14icskax9vfk") (features (quote (("std_net" "std" "libc" "windows-sys") ("std") ("default")))) (v 2) (features2 (quote (("tokio" "dep:tokio" "std_net")))) (rust-version "1.60.0")))

