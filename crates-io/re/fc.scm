(define-module (crates-io re fc) #:use-module (crates-io))

(define-public crate-refcapsule-0.1 (crate (name "refcapsule") (vers "0.1.0-beta1") (hash "1jcb72hvmjqplwsjqnqn97pd8gwx0af02admzdymzmf2sw6r1flj") (rust-version "1.64")))

(define-public crate-refcell-lock-api-0.1 (crate (name "refcell-lock-api") (vers "0.1.0") (deps (list (crate-dep (name "cfg_aliases") (req "^0.2.0") (default-features #t) (kind 1)) (crate-dep (name "lock_api") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "05hixbxgnx5s1b4f93ll7059bla6hmx95flwsaaly9ir8k2sxxdf") (features (quote (("default") ("debug-location-release" "debug-location") ("debug-location"))))))

(define-public crate-refcell2-0.1 (crate (name "refcell2") (vers "0.1.0") (hash "0n0scyyhd88g68i9sqjiiah0218frz2si5n0d3gs5qwi4rggzv74") (yanked #t)))

(define-public crate-refcell2-0.1 (crate (name "refcell2") (vers "0.1.1") (hash "0w1l92k02n6gy9pn16wmml6w4zh9lq8122acgkymyx0amn1qpvc1") (yanked #t)))

(define-public crate-refchannel-0.0.1 (crate (name "refchannel") (vers "0.0.1") (hash "0jwp44lhpsjg9k0g9yn3n13dpvblncnlnzrfjpqc49vfy4r96821")))

(define-public crate-refchannel-0.0.2 (crate (name "refchannel") (vers "0.0.2") (hash "1im2f8f58j64w6rw2d1lykazgb7ia0xcki4f637lp7567advjq6i")))

(define-public crate-refcount-interner-0.1 (crate (name "refcount-interner") (vers "0.1.0") (hash "17wdmj6vjj37mdw6lxn0bbjrkxzwpjyrggg21d08r54x7sbr63qh")))

(define-public crate-refcount-interner-0.2 (crate (name "refcount-interner") (vers "0.2.0") (hash "1fikbc1119k0jz0lix8nami9l3c2g9c9gfk9qvpda4xn9cy2lkvd")))

(define-public crate-refcount-interner-0.2 (crate (name "refcount-interner") (vers "0.2.1") (hash "1l9cpa0zc45ys0wypwg8yzzfd6jlhgbgrj48g7cxs1vaxdya0ksg")))

(define-public crate-refcount-interner-0.2 (crate (name "refcount-interner") (vers "0.2.2") (hash "1j3mwx81jj71nvlkfj68n9v6jxrqlc69dfncriwpnjn5lx9vbmxb")))

(define-public crate-refcount-interner-0.2 (crate (name "refcount-interner") (vers "0.2.3") (hash "0233iplfv2b9acqqncr4ly783k9c7c296lld71a4angn0li2n1js")))

(define-public crate-refcount-interner-0.2 (crate (name "refcount-interner") (vers "0.2.4") (hash "0f84b0hm0bdd0gazvin3wxj0746063bzyd11c6356v997njpjj7z")))

(define-public crate-refcounted-0.1 (crate (name "refcounted") (vers "0.1.0") (hash "0cvlbyghfsnyg85dxvpc402d2ya72pmab0if75gyp1p8svsl8ssa")))

