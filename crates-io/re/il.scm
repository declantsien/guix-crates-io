(define-module (crates-io re il) #:use-module (crates-io))

(define-public crate-reilly-0.0.1 (crate (name "reilly") (vers "0.0.1") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.21") (features (quote ("dtype-slim"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6") (default-features #t) (kind 2)))) (hash "1fa67zl2kbnb3nv14fphadid1yjlpf2qz9wb9n1awxs7xx3p6r3c")))

