(define-module (crates-io re ee) #:use-module (crates-io))

(define-public crate-reeename-0.1 (crate (name "reeename") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^10.0.0") (default-features #t) (kind 0)))) (hash "0pdgvflb1gk0kp5sac86fiqqw3xdnnafp754kfyzfngrhqal4n6a")))

