(define-module (crates-io re or) #:use-module (crates-io))

(define-public crate-reord-0.1 (crate (name "reord") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "0kan9zyxmf2bm548vwasxkcv7gyxs7mxkskfgrxxw3n643h6jps3") (features (quote (("test"))))))

(define-public crate-reord-0.1 (crate (name "reord") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "1rrkvynjcdp73j5i08icay0hw683jar2cazcwwaa45wa9s6fh7jl") (features (quote (("test"))))))

(define-public crate-reord-0.1 (crate (name "reord") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "0h4lifclw6wgdah9ws2lddi0j6li74c2xwwgv33k9wsb3c1vbqy3") (features (quote (("test"))))))

(define-public crate-reord-0.1 (crate (name "reord") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "038simz8ja2kyrpycxyj74l1n2k5ny6qp3j73d7lk952qq3bl5ch") (features (quote (("test"))))))

(define-public crate-reord-0.1 (crate (name "reord") (vers "0.1.4") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)))) (hash "0zpxy1p8ll5srm4cbz4ga38a361qhy5y2q7gky1swinp98wqi1zk") (features (quote (("test"))))))

(define-public crate-reord-0.1 (crate (name "reord") (vers "0.1.5") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "1ns5vmhp6f2fjdxg0jjv8ay0q0m9bcbzcjhmiwkq35lfgiy25gq3") (features (quote (("test"))))))

(define-public crate-reord-0.2 (crate (name "reord") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "08cl368c890qqmizhccl3sf36z5v645xvl2y4zqsfskdz461a584") (features (quote (("test"))))))

(define-public crate-reord-0.2 (crate (name "reord") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "13vma3ln2ng5sjsrxwxsc5rzf4ixzqz6yh7408crg6wqz0rc78bm") (features (quote (("test"))))))

(define-public crate-reord-0.2 (crate (name "reord") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "0laiw551fc87jsprlb2my3bkri9s1hs7djcyny22wvn8008cihmb") (features (quote (("test"))))))

(define-public crate-reord-0.2 (crate (name "reord") (vers "0.2.3") (deps (list (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("rt" "sync" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "1jbnvfzmwxx03pvcnd0r7jc86a79567rkl1rv9s1r5bwblql1h9i") (features (quote (("test"))))))

(define-public crate-reorder-1 (crate (name "reorder") (vers "1.0.0") (hash "087zb7dzb9k57ly51x4iqbmhz2fywki4n1jsl8s2jd520ggxr81w") (yanked #t)))

(define-public crate-reorder-1 (crate (name "reorder") (vers "1.0.1") (deps (list (crate-dep (name "unchecked-index") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "07ydjg6ly6b88s5bimsf0xf68s9kzq5vw6c17fwajj1hqdj58mn5") (yanked #t)))

(define-public crate-reorder-1 (crate (name "reorder") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "unchecked-index") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "07bnpfny6a49pwilgsmvvzwdmd2wwwpcgcxz4n5rq9n5r240wmch") (yanked #t)))

(define-public crate-reorder-1 (crate (name "reorder") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "0sba8gvql62yq0bdqk4078v82gpy3bsh545lp4aacd6v52iwk45y") (yanked #t)))

(define-public crate-reorder-1 (crate (name "reorder") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "1hbhb9y38f1hgjr7jwrsjimhciwdiyw0nf6hcs2h9z9ddvpqakmx") (yanked #t)))

(define-public crate-reorder-1 (crate (name "reorder") (vers "1.2.0") (deps (list (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "013747j61n0wibn3ybg7v0ncv2bkb6jpb2l5j873s70wc50xrq9p")))

(define-public crate-reorder-2 (crate (name "reorder") (vers "2.0.0") (hash "0095i4bvnhnxfk1dfybs7s5wpby6f0xq5y9n65ic1ys83qa76whn")))

(define-public crate-reorder-2 (crate (name "reorder") (vers "2.0.1") (hash "125ddgzkrzkxzng7qsxkcg24vb0a099pyc88h6wdww6xr3qr8k4c")))

(define-public crate-reorder-2 (crate (name "reorder") (vers "2.1.2") (hash "00kr1aq3vqk3fjy2xk59q3ynb10j42is2dih5ikcvrrrg653b5n7")))

(define-public crate-reorder-2 (crate (name "reorder") (vers "2.1.3") (hash "0vdmqmmw8hv0x703idcnk7i7aigc1salvgnqkf1mpp073q1p4rnd")))

(define-public crate-reorg-0.1 (crate (name "reorg") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "116df5g4q9yln6ayarj2fnay7xf9872ap18ka7avb5wk0wyw0k4v")))

(define-public crate-reorger-0.1 (crate (name "reorger") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "007wv6fyrbb6vvs54ri63scflaxa5kj1k8x1gzmdc9b0m6z4p6qx")))

(define-public crate-reorger-0.2 (crate (name "reorger") (vers "0.2.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1s3qkf4s2m9az3fpy1q5mgi539hcnxcn5bwydx25cskj44shfqp3")))

(define-public crate-reorger-0.3 (crate (name "reorger") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0yk5avnn3qb4nssw8q0b2n15yhb1k5lg4jpsmib2ny29z8s5c24j")))

(define-public crate-reorger-0.3 (crate (name "reorger") (vers "0.3.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0v74x78cp5jm1ff1f67bvd940ffmc7d81qc0z8sjvi37cvlr8bqc")))

(define-public crate-reorger-0.5 (crate (name "reorger") (vers "0.5.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0ph5laksfqwdxzjsd1l1xykaq0vxj93y1xd54slm9nwqgg87r5k8")))

(define-public crate-reorger-0.5 (crate (name "reorger") (vers "0.5.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0zrsf6pn6sdd5knzdhqpr59nqnqk93pbjg0g071lragzmrhfkvvq")))

(define-public crate-reorger-0.5 (crate (name "reorger") (vers "0.5.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1k6qxkl8bpxy0pmdb97gzqyaa2zsb0zbg2k3zk9alp2wh9i29l7m")))

