(define-module (crates-io re qi) #:use-module (crates-io))

(define-public crate-reqif-rs-0.1 (crate (name "reqif-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0ns9hhdywpx1x63z7laj7zzsix721km2zhs5agrrvh6hgq12kxkj") (yanked #t)))

(define-public crate-reqif-rs-0.1 (crate (name "reqif-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1x1ng2j6rikc18gn3m580mc85s5vfi2zdayhrvyn09nin9g3lla0")))

(define-public crate-reqif-rs-0.1 (crate (name "reqif-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "yaserde") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "yaserde_derive") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1wfncra3zh3gaxmdljjwx3hwhgcrik2bds0jynjmrdd422bckncl")))

