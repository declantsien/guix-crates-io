(define-module (crates-io re fa) #:use-module (crates-io))

(define-public crate-refac-0.1 (crate (name "refac") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.154") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1g8ilar8k5ynnh9rbhkk58r6j1ynpgxly7arwlwh247vypn49p28")))

(define-public crate-refac-0.1 (crate (name "refac") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.154") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0y94wiy6q9iww60nh01y87hbdplk2hixl0w36dxw94wrvnn1a0wc")))

(define-public crate-refac-0.1 (crate (name "refac") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.154") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "004ndhmwvf6szvrdgcx3r2h05679nrd5jw58gr3pam07rwr1zlqw")))

(define-public crate-refactor-0.0.0 (crate (name "refactor") (vers "0.0.0") (hash "03pmzcsj270yv3bgs35yrd7gvali0a3g74aad5xp44wijyvqsba9")))

(define-public crate-refactory_string-0.1 (crate (name "refactory_string") (vers "0.1.0") (hash "06n43y8sy76yxhf855kpl0vhvvimc8gp9j35wddpdh6kfrxz8lj3")))

(define-public crate-refactory_string-0.1 (crate (name "refactory_string") (vers "0.1.1") (hash "1823nmnvymqkc5c0g3wq2sfx8x7b06s3iv9w1y8ixzcw2534ifga")))

(define-public crate-refactory_string-0.1 (crate (name "refactory_string") (vers "0.1.2") (hash "1382f3faa6kiviz7b0ipw0fpbbs6kf7dfmra0islxnnd8m3l12zy")))

(define-public crate-refactory_string-0.1 (crate (name "refactory_string") (vers "0.1.3") (hash "1x9hygbpnyid4ak9x3243g82fc90wf19brrdxi76yn2vj7vxidmq")))

(define-public crate-refactory_string-0.1 (crate (name "refactory_string") (vers "0.1.4") (hash "108w2nbl2q14i1dd8abg8yklk6hxzxlgj5zwhfbramir7cvn92dm")))

(define-public crate-refalizer-0.1 (crate (name "refalizer") (vers "0.1.0") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0dfs6slqgvqrhlr01az9k0z6sdk8vz25w6yw7fkczdzgxdv36jh8")))

