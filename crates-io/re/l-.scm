(define-module (crates-io re l-) #:use-module (crates-io))

(define-public crate-rel-ptr-0.1 (crate (name "rel-ptr") (vers "0.1.0") (hash "1v2nxkzfpndj23b69mblw71aw05i0f2yw6q9j8avii2a43g06vrp") (features (quote (("std") ("default" "std"))))))

(define-public crate-rel-ptr-0.1 (crate (name "rel-ptr") (vers "0.1.1") (hash "1vfni7wibaf7jiz56yz1kll9ziz2lv2mma3vfb8da972rpqwhd4d") (features (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.1 (crate (name "rel-ptr") (vers "0.1.2") (deps (list (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)))) (hash "07fdf95csnsnfh6zspxhjclkib2kzm4lm8p0d61pik56zzjl7hnl") (features (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.1 (crate (name "rel-ptr") (vers "0.1.3") (deps (list (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)))) (hash "182ncznv33vvmq9001myp3g3z4ah0kkmnjxc08blldkfw54l6sri") (features (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.1 (crate (name "rel-ptr") (vers "0.1.4") (deps (list (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)))) (hash "1fz48s2q979hp59k9xq6hha35w9w4vy6dan9cvcsczxrgf57mi8l") (features (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2 (crate (name "rel-ptr") (vers "0.2.0") (deps (list (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)))) (hash "1lw05dxza9dhvvz1kia4mnihf2x6miw1c3a58d8r1dx28g1fmg0y") (features (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2 (crate (name "rel-ptr") (vers "0.2.1") (deps (list (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)))) (hash "0gjw7w5ns2v6hhiwx0gjx722f43bnkpvz17y4m2fhrnkl6p4a2jg") (features (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2 (crate (name "rel-ptr") (vers "0.2.2") (hash "03wljdml7zfd08g7pv6xxk869yzv4sb0w7lziayplaq01pic50h5") (features (quote (("no_std") ("nightly") ("default"))))))

(define-public crate-rel-ptr-0.2 (crate (name "rel-ptr") (vers "0.2.3") (hash "1wipiji77jr902ykhpiqy2dy2raf4nrszgsnj9mklnfsawznxax5") (features (quote (("no_std") ("nightly") ("default"))))))

