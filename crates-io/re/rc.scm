(define-module (crates-io re rc) #:use-module (crates-io))

(define-public crate-rercon-0.1 (crate (name "rercon") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0qvp8gxifw5m0x1q5gmd7wp87rwcvi42pn8jds5w3nqimm6qdjhf") (yanked #t)))

(define-public crate-rercon-0.1 (crate (name "rercon") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0pi8sgwzb52c28ha3565jk6k4z3h5n1317j2j80h3z9ncz4966xd")))

(define-public crate-rercon-1 (crate (name "rercon") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "18cj55cf4rcs4slflqbcmx2mwl7q3arpzdx3zj34x3cn5da650nz")))

(define-public crate-rercon-1 (crate (name "rercon") (vers "1.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)))) (hash "190kmcg81cc7wdqdvwdcnliq5wlbwszmfy7ypkq3k8adm289hhd9") (yanked #t)))

(define-public crate-rercon-1 (crate (name "rercon") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "06pc1kvyp32mwbwmcnp0bnzq4zm74a47vb4irzx7pxb6y8bgjrai")))

(define-public crate-rercon-1 (crate (name "rercon") (vers "1.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0mi9r5hzajqnrm06zag6gmh51cqvilnzwcxzfs6srfhfp480lq28")))

(define-public crate-rercon-1 (crate (name "rercon") (vers "1.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)))) (hash "0l6hvmfirhd84bndjk2k6791j6zs0ncj68adgjcyafxdpa7z3zin")))

(define-public crate-rercon-1 (crate (name "rercon") (vers "1.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.10") (features (quote ("io-util" "time" "macros" "net" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "08rbyvidc4qg3s90jclp0lvvk4zy3ynlw41g7rlf3pbd7y7b1wb9") (features (quote (("reconnection" "tokio/sync") ("default" "reconnection"))))))

