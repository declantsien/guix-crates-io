(define-module (crates-io re ho) #:use-module (crates-io))

(define-public crate-rehost-0.2 (crate (name "rehost") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1cfp6kbqp0nhgg9l32mpwhxqf01jnmbb1slbrd9fs2gzix8nab8b")))

