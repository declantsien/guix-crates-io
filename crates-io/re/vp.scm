(define-module (crates-io re vp) #:use-module (crates-io))

(define-public crate-revpfw3-0.1 (crate (name "revpfw3") (vers "0.1.0") (deps (list (crate-dep (name "enum-ordinalize") (req "^3.1") (default-features #t) (kind 0)))) (hash "0lw4kmapsv4hq8jxjhvcknrsb8bsrd7ls006c011rsfnq1hrqbk4")))

(define-public crate-revpfw3-0.1 (crate (name "revpfw3") (vers "0.1.1") (deps (list (crate-dep (name "enum-ordinalize") (req "^3.1") (default-features #t) (kind 0)))) (hash "0diw9s9kf9x2sqrn8n3blarqmhjcck51g9hczm92k4i7r5lp547p")))

(define-public crate-revpfw3-0.1 (crate (name "revpfw3") (vers "0.1.2") (deps (list (crate-dep (name "enum-ordinalize") (req "^3.1") (default-features #t) (kind 0)))) (hash "0i9n2752nwn61l3wslwxpl7lj1a0nxp9387a71lkklavg0xmhish")))

(define-public crate-revpfw3-0.2 (crate (name "revpfw3") (vers "0.2.0") (deps (list (crate-dep (name "enum-ordinalize") (req "^3.1") (default-features #t) (kind 0)))) (hash "0ps57csjfmsrkzxi5yrcs1lsxjwczx4cr5s8fv3cb71ghs9p20af")))

(define-public crate-revpfw3-0.3 (crate (name "revpfw3") (vers "0.3.0") (deps (list (crate-dep (name "enum-ordinalize") (req "^3.1") (default-features #t) (kind 0)))) (hash "0rzx7di0zmasa7bv637k3zllb5hm3y1lxw15as6smhkmqxldpkpz")))

(define-public crate-revpfw3-0.3 (crate (name "revpfw3") (vers "0.3.1") (deps (list (crate-dep (name "enum-ordinalize") (req "^3.1") (default-features #t) (kind 0)))) (hash "03a6vq8pxy2bcql6rwdd88pd7i0kxs0hmxzrr281jfac2zxwplsy")))

(define-public crate-revpolnot-1 (crate (name "revpolnot") (vers "1.0.0") (hash "1457j7dqb37wcsyknxnbc2v48nilsm0w3w790j42igp75w3p5jgh")))

(define-public crate-revpolnot-1 (crate (name "revpolnot") (vers "1.0.1") (hash "1l9qv3snkqlhlwkjhy11vh9v3jyzyfrrjimk6jqqn833bivg5rhm")))

(define-public crate-revpolnot-1 (crate (name "revpolnot") (vers "1.0.2") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0a84pizwxaa0f77r39vcrsjhnimf3d0dm4widzlvwb0mr51ihmwf")))

