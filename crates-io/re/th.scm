(define-module (crates-io re th) #:use-module (crates-io))

(define-public crate-reth-0.1 (crate (name "reth") (vers "0.1.0") (hash "0bv1bj4p5kz7jkk1kc6nkyynq5iijm8h0s5z42lysxjk4wi4xk6l")))

(define-public crate-rethinkdb-0.1 (crate (name "rethinkdb") (vers "0.1.0") (deps (list (crate-dep (name "json_macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "110iv1891440gp9cm2amwnljmf7ki5bhf5ryzigj888dvl8lj2ci")))

(define-public crate-rethm-0.1 (crate (name "rethm") (vers "0.1.0") (hash "1kgs3ahhkabq8cgylkgnv5kk4dsnxagr899i42rifmxs3i914zyq")))

(define-public crate-rethmx-0.1 (crate (name "rethmx") (vers "0.1.0") (hash "16x7r06xi1mrafxmf29rv6b32370fh9zjmal29hqv6ml1lgxw8g0")))

