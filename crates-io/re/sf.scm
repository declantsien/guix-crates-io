(define-module (crates-io re sf) #:use-module (crates-io))

(define-public crate-resfetch-1 (crate (name "resfetch") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libmacchina") (req "^6") (default-features #t) (kind 0)))) (hash "09y5y7v6ci4vgsx9f3a0g44hy1zbgl0dz9rzffpmk07dvm2h0jp6")))

(define-public crate-resfetch-1 (crate (name "resfetch") (vers "1.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libmacchina") (req "^6") (default-features #t) (kind 0)))) (hash "1g16vsf3fc2qfn934014mndidxhc96irjkrlznnms7kh6m64l78h")))

(define-public crate-resfetch-1 (crate (name "resfetch") (vers "1.0.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libmacchina") (req "^6") (default-features #t) (kind 0)))) (hash "14iw27xwfrbfv3h0ic9x0pn3rh41766fs3y28f742rg2kdjxki6l")))

(define-public crate-resfetch-1 (crate (name "resfetch") (vers "1.1.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "libmacchina") (req "^7") (default-features #t) (kind 0)))) (hash "0x6wn6bl6xsb9lx83lnnnsx84vf4q46cwnpk05dvkvva564619w0")))

(define-public crate-resfetch-1 (crate (name "resfetch") (vers "1.1.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "libmacchina") (req "^7") (default-features #t) (kind 0)))) (hash "0djybq72fkdvh8izhv3y2grw9fpysdbxz9km6hm2ikg618llclic")))

(define-public crate-resfetch-1 (crate (name "resfetch") (vers "1.1.2") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "libmacchina") (req "^7") (default-features #t) (kind 0)))) (hash "1pch245ajxq4rm0b85dmmf24fxp81cf7p4pq75r4d2vlcyyc0r5s")))

