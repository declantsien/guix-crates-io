(define-module (crates-io re fs) #:use-module (crates-io))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.0") (hash "1xykqb32a223cal7b1c2wqbjvky19a50d3yr2swh3wnlnnmxmr2d")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.1") (hash "0qcwzsin9pj1pqn44mavy388ylpm7ba3gnhlx1wy64r3w0ai31zh")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "12s6h9r0k9amxkzqvag26qv80jrjrpkdbx42djg0wy8v34mpl6wh")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1vp1rigg22kvv3jxaj6wclgbw2fp8l57m2ci7p4i4zzsn97r7hcx")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "093ma3q3w87x2zgpqxqxpjjngqj179cbcga3f318imdq2l6wrk7h")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.5") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "07bf30aa2h6gpgwif2q7g30v6pi6x4fqpvyivz624jj5llh1zzh6")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.6") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0sbgfdjc3nkldr6qpgqdkgnhfkskhk0qdd2gxfanzsj8zp5a3yfy")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.7") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1g54q3way12i2w3i0h8xyzg0v3drv6xpz7jp207dd5hlr5h6kzxx")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.8") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "05vdhiq4ym96460ym8yi6rx6rws01xs9rk0gi6chq2xms3zz59a1")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.9") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0ab80qynmny3w5hc9acg3v4xz4w8q7k3h518r0hb8bbv5lkjjnr2")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.10") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1rrnz9i2zbmfkh7hszhafyz2qxb7vg0c78s2mji5f4nybg6p97bc")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.11") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1wq98zvd4mzdz3l5wc20v03s35k9w13ad3sfakrqgijibs93zd2a")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.12") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1gvycfwl9f8d9nfpy1lfa36scmxaya49p0iqcr9ib008k8xsm4c7")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.13") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "107nm7ala2m76jm16wvm3lkn93xmq5am8j3fi1c573gcqg5bc92p")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.14") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rtools") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0jzqf27r0qmfk1y38h7fynykyx9fyc196bi0zn8bw5bf9ripjx1q")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.15") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0adysc39r8lxz4pxffikykcc8bq0k64kwy2yc98qhnxl2fs9crya")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.16") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1fy8yi5hid7h50r45i2crp50bbdvz28kz9p5ncq87p4c3pxd5fin")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.17") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "035kjxv1xnn40k4a2cys1q6rfy7psp55156z8ly083pak4xs7ldv")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.18") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0fqzv5fqh7bh2c6dzz44kc9xxg6rysk4rq0cqdi0gmibam85061b")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.19") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0rd2ysbm8ic0dpdlffnm10gfzj0xxwpa6bavr3fhlb69fk7146bb")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.20") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0r1rmgh4gqp2k51wxxghvx7ipi7wvxb9v5l5fx439s8bqi34942n")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.21") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "10kqnlbp5angmgi2q7c31c1bd4md1kfirxdyhx32w1yy90jidjvr")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.22") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "01b680hpxm97vnysf567lf8h1l03cx2f7bsg9ykhmdairrbijrq3")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.23") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0w3ian6gpqq8mb26kbzizv25brh4c4xpxrhix2w5wfyiwdbszvnp")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.24") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "088k9gq1bmj34zwszy806s869p6ggsv18iyw257p0fjd0x7lddhs")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.25") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0b34awprni525v3fj9qfmljjxj5j2avn7ndmqb1jpnwmccd275wl")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.26") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1hc5jd0b438jyhj07k0blj5nqmwyj24ph4dzv82rgvjr7c4d4pip")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.27") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1kx5r849a8dbkwswq9k4b1bxizjbf50m5iafjafvf0dn540fb6xl")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.28") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1qiw0sy4j1pvk7m2y5vy6r5w51sc8scj933bkg6f89ympjcrv3jv")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.29") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "08kc6hshnyhynm37cjqhrxkm300b47yy9639m0b76gr9w4gq9vhl")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.30") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0bzy3ffpiywayhyk8qxcd2ycdg0iyjw270jgldxxzipqcbw126ki")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.31") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "09zv7w3daqkpr3qjn7n1nnlm33xhkr7z3k7mhkhlzjvh36xpkh0a")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.32") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "10v31af4fn8l9hz2v6brcvd4mls6l93p1484fvk02q1pl23h6za2")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.33") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "062bwgc2hynj9lsmmja5pp588pzv9hpds8s5a9wf4wy8caqdvfaj")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.34") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1lqrwq9vmf9fwm2mcpqadi8jyd48a6r1dnd6h617n7byz781pwhr")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.35") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "03p060iavs5fgqnkh44407kvcc65l0aalv9ci7sgvxzrmrr6c4zm")))

(define-public crate-refs-0.1 (crate (name "refs") (vers "0.1.36") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1gmivx0bnagx09mwx8mnwmbwh703ks4xvv7iq6lh9sm6lgv2lcp0")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0x9ch23z4cx2kv7hgqcg9yf6dljkx28rbpa65ncpvl4kwfqc05h2")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1mwxfr2rqcf3nmsy8pwmcnwm5mz3lfnx8g4b84nrrwc8myrhqjkf")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "017154ll1cra865mx8z3pv1gg71spmbq9dvkzjp9cw5nj65fi21m")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "18x4fdj4m2qlf46ycqpgv2qnqd229v79dsk1qlcfkks1ph6y56df")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.4") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0hrk4y0cia5cf4cb0948gf93wzn46ybb22bk6cwnfgvnvbb9zbcw")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.5") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "109ynz9x5rf90qq6c7gw3nyg7ix46j9y0x26z2gfcx9j6mkcmfmq")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.6") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0iq1xr3m85awz34d2wg8jc0r0q7m4s1jcs2fy05wn38day58nnm8")))

(define-public crate-refs-0.2 (crate (name "refs") (vers "0.2.7") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "063bh3q82ixq3wigml4qg88pmk8jw9hwhaqcgfiqdz1bz0dmgx24")))

(define-public crate-refs-0.3 (crate (name "refs") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1k9r6ci6cl63x1pbf1fmpydqj8m6hfxmns5s8yba87y2f0fy12i3")))

(define-public crate-refs-0.3 (crate (name "refs") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "00y32zxmaqx1vsh83f72sglg4mhihc292is7m0p81kgwi35riy0i")))

(define-public crate-refs-0.3 (crate (name "refs") (vers "0.3.2") (deps (list (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0zfspsny79c60yyl0b8fp5zs8qvrgbnv77fqx5fmd188h8xcw20k")))

(define-public crate-refs-0.3 (crate (name "refs") (vers "0.3.3") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0yfxmr3x86grpp450ryyn0l2rzwv82aydmajy1zz91lnx18pmi97")))

(define-public crate-refs-0.4 (crate (name "refs") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "09ymp0msb0z64n15by2h1pyhrmfrp8g9wbinh8wi4a4fhzsv3208") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.5 (crate (name "refs") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0g6b3anv33ich8wpcszx58gbhj51ja9g4lc70dk5qsb6scjbp0df") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.5 (crate (name "refs") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1zfvrvh8ilrq9rxh14d6qg2dzx3i3wgl7rkrgixv4dhvs8a6mbv3") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0yb4iaqfy8hg52d7z0bwdr3bm6mpqjq4z2y6167jwd7n6hab4i75") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "19z8scs3xws252fqikcj6769mbjxickbfzsc6wxwlq6lwj27mrp2") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.2") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0zp8qxv7dx79x3mxfck9r4ww9zpsglkcnhpy22b28k11cj7l5xsq") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.3") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "13yv7w7vwvh2g9vfw8xiqbb8y0156nd9m5kr0ac4h3ifpzlfz4nx") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.4") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1vsahp0d02f686frl98nakh8byz3rh1agzzr9pk9mhiqy87401f2") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.5") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "17k451w5p4yhgabaarcwm89vvf5rzp8gd3p8am7dp91cpj8pgvp6") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.6") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1sqx5z7fqcmfwi0zjy0kym29acqlcckjjgjmiig8a5givhn3jk8r") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.7") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "17w19r1vk11j0g51w2hxi188277cjvvxfkzbq5c1nkj7nz18jjah") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.8") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1z2fagmnf1b4mfq3f5112a0mvw811rw6q3ccw3dd05li6fxbq0j6") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.9") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0n5mj1l94spznwlgkcphig15q0dichc2pzkgaiacvkvh011h64fz") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.10") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "14q7s7pc8dc10kf9iqg7zj4hivpbv0lfibmq24sgrw7mwr15xqls") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.11") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "020mvgkx090k0g5mbh7krfxb8sjpqydi9dxv2qz0fi55mrlcvy2m") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.12") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1zyabcfry2y48g18jdsvhpc6zrdn32cqz13w7ih5d219r9ry8gvh") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.13") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1y51za8dfm1za7y4sj1bddr1jdh0mkdg6b9dfpl50nwgf8dh77ys") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.14") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1nq17gz783gimpnjk53i5iqpnpzvfs1z3dl459cpj5ccbkafslhv") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.15") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "1dncj3siy8j8pylq1gqyywwlyhl5q17nfm5kmh6fdrj3k5pzjwmr") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.6 (crate (name "refs") (vers "0.6.16") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.4") (default-features #t) (kind 2)))) (hash "03qk8l1c2vkr15vwscgkjkqhsfjc1zn6ph9lll6f5h18mcpq909j") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0ja1a98v21ziyp7sq8jz0x154qp273mqpsyqbk9gj920yw4iwc0g") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "1r7m8h9n5nyv42k17lf7lq9ziggjm3pbgdi3p8v9g2jlyidvqx2w") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0gb626vrq8sjcf0f7ixq5f858hvc6mspmbk7vg08k466d035k7hm") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0arqxkxs4rz5855blk6gn7xp7xh8a9nf88n4sdyv61dbad2m5fwd") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0rav73lljvxllrdsx5s66jlcxx9rvqbsqcma9p5wfc8nlkc8z1d5") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.5") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "1d1w6f07plqv8wsvyrlb6xab2zcbvc8iasky0cca3bqnfh3k0bdv") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.6") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0y6xrmaiq1s2fhsnvsvzqw1pi28c5ffhdwgzlyfgal49pd4262bq") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.7 (crate (name "refs") (vers "0.7.7") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0lcjslfbzsxzgwpvwnl5qxnfqx7gcqx5y3a0lmzbmkvrrq7pcb22") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.8 (crate (name "refs") (vers "0.8.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "1g0xs0mzhc8agc1g4kal0acz1aymjg8vinnh6g4814xgy1gch8bw") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.8 (crate (name "refs") (vers "0.8.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "1r9alzh80m8rgkv6gm9czx45wbcgckhnm31digz3k2jasqag3f6y") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.8 (crate (name "refs") (vers "0.8.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "1wx8r4xni6jz5wqis0j91lmrlmp7zgpkp6pbrdpmzvaz4phh17n8") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.8 (crate (name "refs") (vers "0.8.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0jrznjb13mkp1sxb4v5fjh1br415al34p013y1v2cdhs9m3ih1ki") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refs-0.8 (crate (name "refs") (vers "0.8.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "vents") (req "^0.5") (default-features #t) (kind 2)))) (hash "0dd1n2spjd8llfhicrdn6hym5c1zjv2dym2ycisj40x72b56mxck") (features (quote (("default" "checks") ("checks"))))))

(define-public crate-refset-0.1 (crate (name "refset") (vers "0.1.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)))) (hash "1dysax7vy702mdcfmsq47ncjnii0li5p6ycmyiz8g0l5snqvyw7q")))

(define-public crate-refset-0.1 (crate (name "refset") (vers "0.1.1") (deps (list (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)))) (hash "0lvlax91dpxynwzh3qi6zij41drlrb7wpcwpnnj75975rpa62dnr")))

(define-public crate-refset-0.2 (crate (name "refset") (vers "0.2.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "smallmap") (req "^1.1.6") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)))) (hash "054vm8z1ksfpf9xk01gdqid29696zdkwy57zhib2xz68kc5pp3v6")))

(define-public crate-refstruct-0.1 (crate (name "refstruct") (vers "0.1.0") (deps (list (crate-dep (name "toml") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v4byjva3m30rl73d0s6fxsqbyj9szhaq04c68dizy7bzj247dxd")))

(define-public crate-refstruct-0.1 (crate (name "refstruct") (vers "0.1.1") (deps (list (crate-dep (name "toml") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kz6jl45ra21f84fj3s6rsx1l7ixpmrq8kiap4d664z11f6gdwy9")))

