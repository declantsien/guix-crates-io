(define-module (crates-io re gl) #:use-module (crates-io))

(define-public crate-reglex-0.1 (crate (name "reglex") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0c07kkkrbmnnxncxns5s6gl7bd0ijbibv2jjxswpmwssv5lylypl")))

(define-public crate-reglex-1 (crate (name "reglex") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1v7njb1m0vnwj58lq507zv1i50x7ngv91abyjrapz1s4aw8x09jd")))

(define-public crate-reglex-1 (crate (name "reglex") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1a3i7pcaf0kpbgchd2nils6pix780a1gqrsxl08473m4k1pcp0af")))

(define-public crate-reglex-1 (crate (name "reglex") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "08c36kihy7k7bbixj9wvf3xaqarmw4zb9gmz7737k02i86sq9v2s")))

(define-public crate-reglex-1 (crate (name "reglex") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0mfayq65s8qi8xjalm8swv1w2m79kb92kh9nfxllr70fdv85wwc0")))

