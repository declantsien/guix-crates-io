(define-module (crates-io re fb) #:use-module (crates-io))

(define-public crate-refbox-0.1 (crate (name "refbox") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1a8k7cfs42yj17fbpfxpa10p4knh9lf2qmr9yz9z659y8bl7hxac")))

(define-public crate-refbox-0.2 (crate (name "refbox") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "15k2m3s42yb74apy4azyq25n6dv95w443n0249g6zb77p834gkps")))

(define-public crate-refbox-0.3 (crate (name "refbox") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "038pwm95nnif5r1rprihm29q7n1bmvd8cclybk6vdgn2crf1620z") (features (quote (("cyclic"))))))

