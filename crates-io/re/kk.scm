(define-module (crates-io re kk) #:use-module (crates-io))

(define-public crate-rekker-0.1 (crate (name "rekker") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.23.5") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "1l5p5s7689alva77vp6c805sgfnszr1m7nr7il7576npf4fg405s")))

(define-public crate-rekker-0.1 (crate (name "rekker") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.4") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.23.5") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "1iwc10hby7b9s7vmdkp2ri6z1h21bjsl3f7ikds122ni2snp8jyr")))

