(define-module (crates-io re dc) #:use-module (crates-io))

(define-public crate-redc-0.1 (crate (name "redc") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.13.0") (features (quote ("integer"))) (kind 0)) (crate-dep (name "twoword") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "077dxl1f3hb7bc5fx5d9kmx0qr1mhqhz7l55661bj46zdfd9wxfa")))

(define-public crate-redcon-0.1 (crate (name "redcon") (vers "0.1.0") (hash "0kvkjzv1qac22fmq92mkh82dp9ssnp9wzvhwdr5665z344k2fjmr")))

(define-public crate-redcon-0.1 (crate (name "redcon") (vers "0.1.1") (hash "0givdg984zxy3v8bnapa0dyz0j313xisyp139wcnijmdainykbdn")))

(define-public crate-redcon-0.1 (crate (name "redcon") (vers "0.1.2") (hash "19zdsm0h51khdrx0ypqhqbzjjg4s5d5id3km0i4m59k37vw8r1di")))

