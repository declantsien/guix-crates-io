(define-module (crates-io re nt) #:use-module (crates-io))

(define-public crate-rent-0.1 (crate (name "rent") (vers "0.1.0") (hash "0567bgvwvbp4bikp36nnskprw2257xyw22xf2xp0k7fb225yd6cn")))

(define-public crate-rent_to_own-0.1 (crate (name "rent_to_own") (vers "0.1.0") (hash "1ci51hyxlrddc7h5ysmwhzb4xf6sd0867cg6i7x11iy5n791m985")))

(define-public crate-rent_vec-0.1 (crate (name "rent_vec") (vers "0.1.0") (hash "1bsr7fj7bb165xid71v2yargl55sri3zgjdv6f0gnv3ml1vlpmpq")))

(define-public crate-rental-0.1 (crate (name "rental") (vers "0.1.0") (hash "1i9d5jdhsa35qfg33ncms247imzlqlxy3mpm6zbkyv8zg46783jh") (yanked #t)))

(define-public crate-rental-0.2 (crate (name "rental") (vers "0.2.0") (hash "0yvw5sjqddk3qf26y5cypknrrhlfb7dkg8sacnb6ivda5s3bgclv") (yanked #t)))

(define-public crate-rental-0.2 (crate (name "rental") (vers "0.2.1") (hash "115762w77vawffbjiczmddn7027n967csdaqz3apkxmx6ighk61n") (yanked #t)))

(define-public crate-rental-0.2 (crate (name "rental") (vers "0.2.2") (hash "1a9w7j694vnl8ihr0p7y7rdj1ihgsxsa3kdkw16pvjg4nf02fsp6") (yanked #t)))

(define-public crate-rental-0.2 (crate (name "rental") (vers "0.2.3") (hash "0avs1ib77ydzz1qqb89g6a7qh0ibjw0dfq6q0y42ackg93bswl6y") (yanked #t)))

(define-public crate-rental-0.2 (crate (name "rental") (vers "0.2.4") (hash "1hgc9bil535j5r6h5838djf85sfvkkmkjbsp34mb5rbaq89l8ls2")))

(define-public crate-rental-0.3 (crate (name "rental") (vers "0.3.0") (hash "1x4vpzc8i7kri62nxc5y9qqpryd0m0x0f7qyz38yf0mf7w3kgdsd")))

(define-public crate-rental-0.3 (crate (name "rental") (vers "0.3.1") (hash "0kvlsv3phikqga446q2pqy3jnf1khijxxyx4988sfiqmkvkdmrn6")))

(define-public crate-rental-0.3 (crate (name "rental") (vers "0.3.2") (hash "0nwp3j2s5xg96gzsqsq9lzqabzp5pgfsqba1n7wzbl323yc498pa")))

(define-public crate-rental-0.3 (crate (name "rental") (vers "0.3.3") (hash "0gbsgpgf8wicpha594d2apgsx1hhaivnicabb1rwk2x5cpyi5qpc")))

(define-public crate-rental-0.3 (crate (name "rental") (vers "0.3.4") (hash "0x9771yka0zn886yqvfkjpnmqab50l1jnqg7yd11ifas1ajc90zn") (features (quote (("std") ("default" "std"))))))

(define-public crate-rental-0.3 (crate (name "rental") (vers "0.3.5") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1ppavh8i0cnzx3r3adshg45xigxhycbii556hrjdnv7qyhzy7aqy") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.3 (crate (name "rental") (vers "0.3.6") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1nhl4sbb7ws3izrzfri561lnqjj6ybbx8dl9yd7gmbsvr96kqq5n") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.0") (deps (list (crate-dep (name "rental-impl") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0d7cscnfypi7x6vfwiwkhyf1qy90ahsdr8sraqhr71qlv70516sc") (features (quote (("std" "stable_deref_trait/std") ("default" "std")))) (yanked #t)))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.1") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.1") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1xdyapzwwqykv92lfy48nvf3ix3g8p4bwx8plzai819vczywx4iy") (features (quote (("std" "stable_deref_trait/std") ("default" "std")))) (yanked #t)))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.2") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.2") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1rphcbd160w51ks714az8vni8272rh73i28024bpg8fyp55sys99") (features (quote (("std" "stable_deref_trait/std") ("default" "std")))) (yanked #t)))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.3") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.3") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1x7ca30kxb3i2kj5jli5vjivvdqb6vncqki60zr32xs00l41hdxv") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.4") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.4") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1zqlhddyvm7zsk6hdbdzmghidrxvir6zwjfm03h9jr1calw2hc59") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.5") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.4") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0ac0mc4hxw1rjnv6p03pfjzrw6i27k59nsdc9mwmsbl9lqdqning") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.6") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.6") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1ljmzi8bfh9fwbaf3lzjh2j9sc6mrn3mg1xalgikw37a9xn56723") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.7") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.7") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "07d6nkg3nilifzy3b3r98a1vcyvwx9bgjaw4072ly0bjy8vw52sw") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.8") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.8") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0fcx26n7wfm685wqkl0n0fv9z2g9ysd4abk6i2kqc7fi66q5ql39") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.9") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.9") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0240my0sqs7ivykfcpws8293yqbnv5r39hp7isyyaq8q3z42xc2z") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.10") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.10") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0d7c05gdfi39hff7gy1pqv481jl3q0wci44ha5bzimzrpkfvpfzj") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.11") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.11") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1rm1qkwpby404rggdwpa8vf9nzhqvmpbpxxb8f8bkw9jz9dni2w7") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.12") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.12") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0qydslfs3gzhi2yvjm3agj0cq6i7wg9vp1xxf97qf7c7vjxvqq5g") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.13") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.13") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "123aai3qlbgx9hj2l95jmif0skj5dx3jwdqqldavhni1k9ybd43f") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.14") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.14") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "038ky7mjjk0m6apjkmc4f4ksbmlw9pyzmaqhsmjjg56yrkysnqlr") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.15") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.15") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0qybh3dx8mm13q33q73d68bl1wbp15h0sffxddbanj530cgyh9vx") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.4 (crate (name "rental") (vers "0.4.16") (deps (list (crate-dep (name "rental-impl") (req "= 0.4.15") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0vh3pm4aykrn12la7nxfs53lpbsd4h4nxjljz6x2kff5rfzxb67h") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5 (crate (name "rental") (vers "0.5.0") (deps (list (crate-dep (name "rental-impl") (req "= 0.5.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0yaik4kwdyf5iimv00w8iqakxcam5sllxqr0mp1d2cljdrprdvkp") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5 (crate (name "rental") (vers "0.5.1") (deps (list (crate-dep (name "rental-impl") (req "= 0.5.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0blqq2hmzj59zcs605mx69r7mrqaz1zm2mkl665h6f7b48xhn7y4") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5 (crate (name "rental") (vers "0.5.2") (deps (list (crate-dep (name "rental-impl") (req "= 0.5.2") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0m7pnlq4ywmfygmca0158gc9601sk7pvifzib6rhppz3k2dvy96a") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5 (crate (name "rental") (vers "0.5.3") (deps (list (crate-dep (name "rental-impl") (req "= 0.5.2") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "1501c9inwsa00c0bp6b6rgppyaazsncqzja734wnfbmhfpr24xfm") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5 (crate (name "rental") (vers "0.5.4") (deps (list (crate-dep (name "rental-impl") (req "= 0.5.4") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "07ry2b2y7mj2rk3i8bj0lcnbpxa7zai455fwlmw1ks62kyynx481") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5 (crate (name "rental") (vers "0.5.5") (deps (list (crate-dep (name "rental-impl") (req "= 0.5.5") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0zw4cxyqf2w67kl5v6nrab0hg6qfaf5n3n6a0kxkkcdjk2zdwic5") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-0.5 (crate (name "rental") (vers "0.5.6") (deps (list (crate-dep (name "rental-impl") (req "=0.5.5") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0.0") (kind 0)))) (hash "0bhzz2pfbg0yaw8p1l31bggq4jn077wslf6ifhj22vf3r8mgx2fc") (features (quote (("std" "stable_deref_trait/std") ("default" "std"))))))

(define-public crate-rental-impl-0.0.1 (crate (name "rental-impl") (vers "0.0.1") (deps (list (crate-dep (name "procedural-masquerade") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0v0xvrf6kpklk352klr9xy7zkwkqclk8h3hdws73czxx2li5aw1i")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.0") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "081l2xqsgy6prklddrdgh5c6bxr3xxznlcq7w88knivb8rxnw4i3") (yanked #t)))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.1") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "113bmlmjfhjhqqr1bl4vcgf3yphv4r5iivyv3aagp4f4dlcvz4y6") (yanked #t)))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.2") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "11s1223g5jiz53hfqs6ybrrmvjgl3l4ysh791cknc8ppksnwd25y") (yanked #t)))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.3") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1lyl0pmp6nwbvda4cgsvy81mvvpsh5dc7qw9199slap0y8amr4i4")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.4") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0j9vc019g41i2izczlmdb1ly43b66n4s92jacgfnp9p98n04wvq1")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.6") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "11pc769y2nwjy7xwazx5yfswk17666hclf4xxkxkzsyn42ha1c5y")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.7") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "10km7m5qx6v6dbjb52mc50zfi7s0f5fbxpv8k00k80hsgsq031w7")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.8") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1yis4sb87wn2xf8b1pq8hkgy3sc1g4xlypd63rcnlma1aggq4in2")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.9") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1dv903vkbw2n0v1qkv0l0977b39f5k6cwrqdm7bppbx8diwlzni9")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.10") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1a2f1533grq8lm6bbwjxxsamn34m7s5qmfdqkbym00kvdqr8k6ln")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.11") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0xrk999hz7j61grq1ygxqs1689qf54qc38rzd0ngps6011qlb73n")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.12") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1p5799rlxfnyixbyfki59d3r8wssr2x9i9l9n1mxjz9p482ykxra")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.13") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1k19ys4g5n8hz9s9sxlmix9x8q01a1im6vb7pfang536ghykb8zx")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.14") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0yx0kz9j581l4qz0153rwjpr3ps2djq8nf25f5hn64b6a5h70k3c")))

(define-public crate-rental-impl-0.4 (crate (name "rental-impl") (vers "0.4.15") (deps (list (crate-dep (name "procedural-masquerade") (req "= 0.1.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.10") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1wbk9agjy9mq5bibcqsi6p0rlflxpws5ra2f2mjb5mzx6imzn96b")))

(define-public crate-rental-impl-0.5 (crate (name "rental-impl") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13.1") (features (quote ("full" "fold" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1721kbjpvrh9lxr8pshw94krc7hpsxjqxv4yyrxwxram6b70wras")))

(define-public crate-rental-impl-0.5 (crate (name "rental-impl") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.1") (features (quote ("full" "fold" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fl0fhrp1v3l37rpjn8q8rfl9k348nv523i6914amfwkkcx56sd2")))

(define-public crate-rental-impl-0.5 (crate (name "rental-impl") (vers "0.5.4") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.1") (features (quote ("full" "fold" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1djsnbzw57fkspjgjfm159qaw7z8k1y7w7qnvw49dgicrxa0s9l2")))

(define-public crate-rental-impl-0.5 (crate (name "rental-impl") (vers "0.5.5") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "fold" "visit" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1pj0qgmvwwsfwyjqyjxzikkwbwc3vj7hm3hdykr47dy5inbnhpj7")))

(define-public crate-rental_rod-0.0.1 (crate (name "rental_rod") (vers "0.0.1") (deps (list (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0qcqw9w0x7v2lcx1i7di3zhaqh8yyhqv4wrj1jnm4y45mxbnr95k")))

(define-public crate-rental_rod-0.1 (crate (name "rental_rod") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1b5s5myc4s8l16ckiynyfid1k0vpm0qbiwqjf2560pqwyd4m9s96")))

(define-public crate-rental_rod-0.1 (crate (name "rental_rod") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1z2dc1hnz39cplmq8dnk97h1cwaj13ldivwk3ayhi2vqaf1c2qrd")))

(define-public crate-rental_rod-0.1 (crate (name "rental_rod") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "19xik10n71vpja4rda2vkahm3apgdhyy7zg6a9c1zr71xmk510kr")))

(define-public crate-rental_rod-0.1 (crate (name "rental_rod") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1r60j38vpq9108ziwjhapx7zvzrmghr4pai25pikjkd5h16icahv")))

(define-public crate-rental_rod-0.1 (crate (name "rental_rod") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0mnqcbyrk3b1dmwn56zd0zfg45xrzqj05rciqpsrc515w0mq8snd")))

(define-public crate-rental_rod-0.1 (crate (name "rental_rod") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1q9cz3hck6m0j9cqgvzvm6xgr47da94430k82pjgk5w59n4kjf4f")))

(define-public crate-rental_rod-0.2 (crate (name "rental_rod") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1h5pbhc9z1jmh3li6r2ql59vasv0h8gx4063sc7w8vdq356x2z48")))

(define-public crate-rental_rod-0.2 (crate (name "rental_rod") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0pdsybw2s82x3879fx916ksp5nzllqpds41zjk58yzhwaa2pmcgj")))

(define-public crate-rentr-0.1 (crate (name "rentr") (vers "0.1.0") (deps (list (crate-dep (name "notify") (req "^5.0.0-pre.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0ymdzbhcj8wdm5mlpnfc10ysv14cgncj8y0ibrzndxilvgr16975")))

(define-public crate-rentr-0.1 (crate (name "rentr") (vers "0.1.1") (deps (list (crate-dep (name "notify") (req "^5.0.0-pre.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0qi7ykfdvws8kkrqrs2dx28dhpgh14z03px511jns6c3qv112ddk")))

(define-public crate-rentr-0.1 (crate (name "rentr") (vers "0.1.2") (deps (list (crate-dep (name "notify") (req "^5.0.0-pre.13") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "1mxdgf5w0klcpyk58shhn6kk5vh2sr9lqqw3dp3fyp77rjixngb4")))

(define-public crate-rentr-0.1 (crate (name "rentr") (vers "0.1.3") (deps (list (crate-dep (name "notify") (req "^5.0.0-pre.13") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "1cj0608yswn6a7ambxz7p8svq2xxn272a1c6mrfzdkv0v5izjvd0")))

