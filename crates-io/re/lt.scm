(define-module (crates-io re lt) #:use-module (crates-io))

(define-public crate-reltester-0.1 (crate (name "reltester") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1z484gq3lqf0a2fq1hlr5lz08amh3vcgc6cr29jp7abl0d7z43s1")))

(define-public crate-reltester-1 (crate (name "reltester") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "18xvkiwdl6s20n79fka2szjd6q4awba0rq5y9n29nk0yavzwav5s")))

(define-public crate-reltester-1 (crate (name "reltester") (vers "1.0.1") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0mkpwbjf2ff3mm77lwcbhjziy2sbav6n2rkzbkxsj40gwaqdhp7z")))

(define-public crate-reltester-2 (crate (name "reltester") (vers "2.0.0") (deps (list (crate-dep (name "proptest") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07qd8vzdg71zfzp2v6gy3v01z5dw1v3r9vwqcaw5la0n770nm4nd") (rust-version "1.56")))

