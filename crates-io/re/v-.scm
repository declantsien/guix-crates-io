(define-module (crates-io re v-) #:use-module (crates-io))

(define-public crate-rev-11-1105-rs-0.1 (crate (name "rev-11-1105-rs") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1g3b89yf8m1jd30pw40grh2l1v8430xl2pal5fzv5bbjldvngiba")))

(define-public crate-rev-up-your-harley-0.1 (crate (name "rev-up-your-harley") (vers "0.1.0") (deps (list (crate-dep (name "unic") (req "^0.7") (default-features #t) (kind 0)))) (hash "0yp4wl3rbhyqh8biqnbw46js909g7vrbypr0n4i2f4m8r00pnj8j")))

(define-public crate-rev-up-your-harley-0.1 (crate (name "rev-up-your-harley") (vers "0.1.1") (deps (list (crate-dep (name "unic") (req "^0.7") (default-features #t) (kind 0)))) (hash "0q6q17ml9ijrxjj4gx4yf6g3sxz5y3hq3wxsmwnzlwa2ifz82lkn")))

(define-public crate-rev-up-your-harley-0.1 (crate (name "rev-up-your-harley") (vers "0.1.2") (deps (list (crate-dep (name "unic") (req "^0.7") (default-features #t) (kind 0)))) (hash "1kxazxhplq94djd6r9i733zd2h19pcap21fy3nmigvq2s7jyl36l")))

(define-public crate-rev-up-your-harley-0.1 (crate (name "rev-up-your-harley") (vers "0.1.3") (deps (list (crate-dep (name "unic") (req "^0.7") (default-features #t) (kind 0)))) (hash "168x4a0p0mmm7hng9c00ddb1nv7w7mjf9pyb5032djgbncmh451i")))

(define-public crate-rev-up-your-harley-1 (crate (name "rev-up-your-harley") (vers "1.0.0") (deps (list (crate-dep (name "unic") (req "^0.7") (default-features #t) (kind 0)))) (hash "0vgsk4vdg56azwwvdaxpvbjy5s2s7x8m05scz4b8wbb94qlapsz4")))

(define-public crate-rev-up-your-harley-1 (crate (name "rev-up-your-harley") (vers "1.0.1") (deps (list (crate-dep (name "unic") (req "^0.7") (default-features #t) (kind 0)))) (hash "1jz5cggk4q3gvrbic71f26wpjwx050hdy8akf17l5rsdsdgamqcq")))

