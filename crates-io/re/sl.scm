(define-module (crates-io re sl) #:use-module (crates-io))

(define-public crate-reslab-0.0.0 (crate (name "reslab") (vers "0.0.0") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sharded-slab") (req "^0") (default-features #t) (kind 0)))) (hash "1r4326czs8bmr4s8lmqx5gr3dq6q5pigdf02w4xvi1hjh4xm7hgv")))

