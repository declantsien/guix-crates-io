(define-module (crates-io re jo) #:use-module (crates-io))

(define-public crate-rejoin_slice-1 (crate (name "rejoin_slice") (vers "1.0.0") (hash "1mijrvqqzxn9g2v60z8ry0aha0d1xn5df6c4g470dab7xasvzd64") (yanked #t)))

(define-public crate-rejoin_slice-1 (crate (name "rejoin_slice") (vers "1.0.1") (hash "0rg3i7snmkqlkjlqjp5728qygl9z4jllaiv3y9d3vsc8n8wdnb9p") (yanked #t)))

(define-public crate-rejoin_slice-1 (crate (name "rejoin_slice") (vers "1.0.2") (hash "0rzhv3ms1fczyg52bqzcfi80rpxi7j7kbawaaxgs26lgk8j7w3sd") (yanked #t)))

(define-public crate-rejoin_slice-1 (crate (name "rejoin_slice") (vers "1.0.3") (hash "15cz5378zldsbqs0ldbhvcm7wi0r69wa7m762dmkjqm9gz79zf4y") (yanked #t)))

