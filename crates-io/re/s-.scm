(define-module (crates-io re s-) #:use-module (crates-io))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "ress") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "0h245sjy51jc55yxi5g389118xfkfjwfd9qrgasw0fv7cz4ayyj3")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "ress") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "1p1wm11z4aazlas6yiyz26j9l3ik8ki6z5prl0kycc7kfds1k9cn")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.0-beta.3") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "ress") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "18q6gziggapdfi6y2c9wdk6nnbf6x1k33sk5brga9jgqc95jlgmx")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.0-beta.4") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "19z66rr8qhh4rf72mvlbvplkqz867mvs580vap35mmf5mvci47in")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "139dmrlwklnb44cmgf36pym5zasa543mwnvyhmrv9vmlbx8n9kma")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "1wjhmihwvpp9sf2r7ws87xhkyszmnrddrv1zvlk1xj75y21p85kv")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "10dhmzd0iqha6xzrnjpyhc1z0ifk0jxa3ma9115lhdb8q7pkl747")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "0453zh95v3r87ixyd2xx8g8prs7i2d01jj766ph7f5rhbx40vim4")))

(define-public crate-res-regex-0.1 (crate (name "res-regex") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "unic-ucd-ident") (req "^0.9.0") (features (quote ("id"))) (kind 0)))) (hash "1ca7ib8hn3qcqrlyqyvbh6zny584qpydia5g64zzkxi856v7z5vv")))

