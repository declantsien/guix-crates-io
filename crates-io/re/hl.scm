(define-module (crates-io re hl) #:use-module (crates-io))

(define-public crate-rehl-1 (crate (name "rehl") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "07m747nnjnlnys4f9hzx4cbp68zv8ls5v95pmb5dcjhy98mzwx6i")))

(define-public crate-rehl-1 (crate (name "rehl") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1n55v4cb8nvpmab77yarhhwydkvndpc6mzgqzmz31x86smskxmjg")))

