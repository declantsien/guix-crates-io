(define-module (crates-io re gn) #:use-module (crates-io))

(define-public crate-regnum-0.1 (crate (name "regnum") (vers "0.1.0") (hash "05xih37cdr0sxa6vbn2hy6rlmpjkpvcv5g4v1i6gs0klmqaw5fw9")))

(define-public crate-regnumassets-0.1 (crate (name "regnumassets") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0k5rrlzg1wj4ach3spkqqn2an8y3v9225qri6y2mb1m48vglp64a")))

