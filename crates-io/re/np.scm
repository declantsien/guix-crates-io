(define-module (crates-io re np) #:use-module (crates-io))

(define-public crate-renparkn-0.1 (crate (name "renparkn") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1wndm3zblyidjhc9x4m3q3zhsb4b8wsb9y8j65yyirr23ww29bq0")))

(define-public crate-renparkn-0.1 (crate (name "renparkn") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)))) (hash "0mjpbv8z5lq0w8rw81vg4wvw4snv24v684x23qyspbp0l8agczz2")))

(define-public crate-renplex-0.0.1 (crate (name "renplex") (vers "0.0.1") (hash "1vxs7n7mmargjhww493ws60any5n10gb9vff1h8hcx0bxjzn5x1n")))

