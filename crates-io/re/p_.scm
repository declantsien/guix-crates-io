(define-module (crates-io re p_) #:use-module (crates-io))

(define-public crate-rep_derive-0.1 (crate (name "rep_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07gp3z35ribn485xl18m19r16qwwczkn3vj6sc32xisi3ff3cv5k")))

(define-public crate-rep_derive-0.2 (crate (name "rep_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ksn5y27a2kj91nmxssg83g58v0sakmmdf8w1z4an5gqxgdhviqq")))

(define-public crate-rep_derive-0.3 (crate (name "rep_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gydy4d1r0s08pndx0wj5nxy7vch7q3snfzi8s6095b7kwqlc5ki")))

