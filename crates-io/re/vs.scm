(define-module (crates-io re vs) #:use-module (crates-io))

(define-public crate-revshell-1 (crate (name "revshell") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1bpkv53i75h7q9w3shik7aaxl4sahmvnk13hjmfr9izvyab5kapp")))

(define-public crate-revstr-1 (crate (name "revstr") (vers "1.0.0") (hash "0v2v456fgkc6ildrd568nmmyyr9mm8pmc56xhhcn7k3d7mjq85vi")))

(define-public crate-revstr-1 (crate (name "revstr") (vers "1.0.1") (hash "088323qznzllv37hcrxailbrzq602gkq323qnbgk1ksckb3hdp56")))

(define-public crate-revstr-1 (crate (name "revstr") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05z47drwk207xjalifkng67a368nk7059zyzpydy9fscviph8g9m")))

