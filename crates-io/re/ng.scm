(define-module (crates-io re ng) #:use-module (crates-io))

(define-public crate-renge-0.1 (crate (name "renge") (vers "0.1.0") (hash "1agrch3v9bykxazq2ipd9919h1aqw7sj2zk95v1gsamiil3dyjbd")))

(define-public crate-rengine-0.0.0 (crate (name "rengine") (vers "0.0.0") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "1m0916acnvc20q4kf14b4kklh7azswcc7hypcmi8q3f0klymq4qd") (yanked #t) (rust-version "1.66")))

(define-public crate-rengine-0.0.1 (crate (name "rengine") (vers "0.0.1") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "13wswy5cdgi2cdqk0qyhmsla8whggpdxxhnc5szkhbnm5vh9kyra") (yanked #t) (rust-version "1.66")))

(define-public crate-rengine-0.0.2 (crate (name "rengine") (vers "0.0.2") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (default-features #t) (kind 0)))) (hash "10xpq746qkf9v4c6bq6j1dlpqjxbxmdzv0iazidx6fcw6ikdqhv5") (yanked #t) (rust-version "1.66")))

