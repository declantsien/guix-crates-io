(define-module (crates-io re ih) #:use-module (crates-io))

(define-public crate-reihendorf-0.1 (crate (name "reihendorf") (vers "0.1.0") (deps (list (crate-dep (name "sled") (req "^1.0.0-alpha") (default-features #t) (kind 0)))) (hash "12ydbm5kxp2fhqkfq45hi70n040sk1i6dhkd7836hqqmzacmyckz")))

