(define-module (crates-io re n3) #:use-module (crates-io))

(define-public crate-ren3-0.1 (crate (name "ren3") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1sbhh703q1m2yw3cinprj8ipv8vyhbfa38r84a8clnzvc0qvjm0c")))

