(define-module (crates-io re s2) #:use-module (crates-io))

(define-public crate-res2br-1 (crate (name "res2br") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)))) (hash "0mc7939q6k898qpr31f3k8b648i6aihsv3w47lr7w66qbkc5hqpn")))

(define-public crate-res2br-1 (crate (name "res2br") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)))) (hash "11m1l8dlpav7pilczxfap9lkdqp82slk8qiarbj69a503d29g9jf")))

(define-public crate-res2br-1 (crate (name "res2br") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1zcgbm1m56dgaf0zap9is4x5rbcn29kdxqcgcyl59lrqa3y8pvd5")))

(define-public crate-res2br-1 (crate (name "res2br") (vers "1.3.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04m6fmgcvfajjxj3lcrikp8qn3p35pbpcqa854998a0m3s8cfngp")))

(define-public crate-res2br-1 (crate (name "res2br") (vers "1.3.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_mangen") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "16cgjc223y6ksipx4dac436i1hj40sq6s70pfim3145681wfmr87")))

(define-public crate-res2br-1 (crate (name "res2br") (vers "1.3.3") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_mangen") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1j6lpwigmb9pyvpsv1q37amnmwr8z14mink7zadbxa7q4axsqh12")))

(define-public crate-res2br-2 (crate (name "res2br") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_mangen") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1kfbbsmy1r2mbmliy2xhpxi8s9sbyxnfr63h9ixzy4zrgrd9xj8h")))

(define-public crate-res2br-2 (crate (name "res2br") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_mangen") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1j9nah8m6sa5vpxmhalaq00y2r2lj5c48jpd7ijkf36yz4fjz11f")))

(define-public crate-res2br-2 (crate (name "res2br") (vers "2.0.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_mangen") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "19a69zkhyhrnxf6xzg33d2x7bir3i2mfsmq1lz2x7ni7wilsq1jh")))

(define-public crate-res2br-2 (crate (name "res2br") (vers "2.0.3") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_mangen") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1ikslnfw9bgh7hbb9kz5fdcm5va85r7788cg9wbssii4r1hpdsl8")))

