(define-module (crates-io re -p) #:use-module (crates-io))

(define-public crate-re-parse-0.1 (crate (name "re-parse") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "re-parse-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 2)) (crate-dep (name "serde_urlencoded") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1l2j32njfss099ndsxhallqrzbbpgbrha6h2lihq869cyrr7gwxg")))

(define-public crate-re-parse-macros-0.1 (crate (name "re-parse-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0lkl3wpdcwf36my8gs136gldv6drzanfdlqn4kd6j74lj9fr8r89")))

