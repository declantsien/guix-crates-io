(define-module (crates-io re t_) #:use-module (crates-io))

(define-public crate-ret_ty_attr-0.1 (crate (name "ret_ty_attr") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "1w87dfwf70fh8i17ishb735jywksd7f6j0nmf6l6y1acm75wxfdp")))

(define-public crate-ret_ty_attr-0.1 (crate (name "ret_ty_attr") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "1s1achdjzy7zrmb0c0hvc9l8y6gbwynxmhia6z07z81jfb149jbp")))

(define-public crate-ret_ty_attr-0.1 (crate (name "ret_ty_attr") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "1rzqww800kwqck09pic3bdhssdyaz85x0si1wlsxqwx0nmd2yphk")))

