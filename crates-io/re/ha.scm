(define-module (crates-io re ha) #:use-module (crates-io))

(define-public crate-rehashinghashmap-0.1 (crate (name "rehashinghashmap") (vers "0.1.0") (hash "0ghxdgb32yx14kr9qx28zf4ilchbay9ayyhyky8yd2j6z8jz3s9i")))

(define-public crate-rehashinghashmap-0.1 (crate (name "rehashinghashmap") (vers "0.1.1") (hash "1rylfh92624fxwnbm09pd0jqvr12gdjgdwsmkgkcpl408d86amaz")))

(define-public crate-rehashinghashmap-0.1 (crate (name "rehashinghashmap") (vers "0.1.2") (hash "0lr1v24c1kr1alrh4dx2gdjh6k37v8r0k3rcf8p4g97q1z5c1gxp")))

