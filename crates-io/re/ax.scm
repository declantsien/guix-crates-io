(define-module (crates-io re ax) #:use-module (crates-io))

(define-public crate-reax-0.1 (crate (name "reax") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0lcmiksf48k6x6xkbzc1vx426yyjlw489lmw0nqvvwlin7pg797y")))

(define-public crate-reax-0.2 (crate (name "reax") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bl9ynwrnlq42idpp80xnsp6653xzqw5ip0hg2q8davyndflmkp6")))

