(define-module (crates-io re li) #:use-module (crates-io))

(define-public crate-relic-0.0.2 (crate (name "relic") (vers "0.0.2") (hash "1k6nbdvn2p3k3jmspayyivll8r5s4g78nbx6gz75680h835d9fgg")))

(define-public crate-relic-0.0.4 (crate (name "relic") (vers "0.0.4") (deps (list (crate-dep (name "skellige") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0wbrbxxaqmzry4c1v84hw89l75vwywxhl31y8xa49mymc386zfvs")))

(define-public crate-relic-0.0.13 (crate (name "relic") (vers "0.0.13") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "librelic") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "witcher") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "04bcgm303gilsdi0skyc5b2nk3gbjy3zk3azpwzvpzy2647sxwfz")))

(define-public crate-relic-0.0.14 (crate (name "relic") (vers "0.0.14") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "librelic") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "witcher") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "03lkw4lgclxvz67kld27paq223xikc5bsbgk5cxhlkxlxayzgq2m")))

(define-public crate-reliudp-0.1 (crate (name "reliudp") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)))) (hash "13z7sbjfa774hw25lf2hl15sq7hz88jyd06ysnrasfn36h8wr0cy")))

(define-public crate-reliudp-0.1 (crate (name "reliudp") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)))) (hash "061lahq5h97ix9m3h1r1a2fz8schlc2x66q1vj5bsxxqxzrpy22a")))

(define-public crate-reliudp-0.2 (crate (name "reliudp") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0f80qm7nfz7h2xhlmj8rbqb0ns6ny71s58lcja5bwfwyp1yf9cnc")))

