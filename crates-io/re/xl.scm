(define-module (crates-io re xl) #:use-module (crates-io))

(define-public crate-rexl-0.0.1 (crate (name "rexl") (vers "0.0.1") (hash "1yx8fw1ppvhz4gbbskhxm7y1idavka3qigz37brv76xaly0bivj2")))

(define-public crate-rexl-0.0.2 (crate (name "rexl") (vers "0.0.2") (hash "1ff9pcxnfn97lc6281glw7rhrd8rlig9c0pacskq82qkvg2ya06q")))

(define-public crate-rexl-0.0.3 (crate (name "rexl") (vers "0.0.3") (hash "1lijcplqi5wd40pvqmnsgrqhykzy7r29ympjl39clv4vimr31jxk")))

(define-public crate-rexl-0.0.4 (crate (name "rexl") (vers "0.0.4") (hash "0ygaimis761r6bwznnyc484cpb5yff1km6rvqbq254rixx94dr6y")))

(define-public crate-rexl_math-0.0.2 (crate (name "rexl_math") (vers "0.0.2") (deps (list (crate-dep (name "rexl") (req "^0.0.2") (kind 0)))) (hash "0axd5spww2aiiq6vn34b7x33vnsxq319kzriky7scd6s15wm07fh")))

(define-public crate-rexl_matrix-0.0.2 (crate (name "rexl_matrix") (vers "0.0.2") (deps (list (crate-dep (name "rexl") (req "^0.0.2") (kind 0)))) (hash "1b8d1xmksywfjlc934rwq1k9c1hwl909drqz9g3kq8hjqx3nah7h")))

