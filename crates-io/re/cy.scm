(define-module (crates-io re cy) #:use-module (crates-io))

(define-public crate-recycle-0.1 (crate (name "recycle") (vers "0.1.0") (deps (list (crate-dep (name "replace_with") (req "^0.1") (default-features #t) (kind 0)))) (hash "03vl7rzz7rzjlh1dnggq83pgxck46qnka5hh1mnr7h8dk7nd313l")))

(define-public crate-recycle-box-0.1 (crate (name "recycle-box") (vers "0.1.0") (hash "1zf0y8a2m6czppix77vbcw0p9c4frmv513wr1kf7068vhj4hdgnj") (rust-version "1.56")))

(define-public crate-recycle-box-0.2 (crate (name "recycle-box") (vers "0.2.0") (hash "0i66zlzbxdlc92q3kxlgsywp9418dzdh2an2j9fs10v11wf0n605") (rust-version "1.56")))

(define-public crate-recycle_vec-1 (crate (name "recycle_vec") (vers "1.0.0") (hash "0a5qaryypk0n6kpvrww1c6cwhxbs1f48hpr50933swm4vsc96vmb")))

(define-public crate-recycle_vec-1 (crate (name "recycle_vec") (vers "1.0.1") (hash "18nc7x9haipn8g038m7b0s2j1mr6lr8bz8wzqawf174ybyl2mn2w")))

(define-public crate-recycle_vec-1 (crate (name "recycle_vec") (vers "1.0.2") (hash "0b6ariyc4xjkbb31n7r75nv5c422dax2k9i47k9r66jy98867dj1")))

(define-public crate-recycle_vec-1 (crate (name "recycle_vec") (vers "1.0.3") (hash "07dwjhidzq5m7xj8z56s7gyj08l4ywj2nvwx8zgpwqg6vqp24i14")))

(define-public crate-recycle_vec-1 (crate (name "recycle_vec") (vers "1.0.4") (hash "0g0kaac9wb2hb88lgr9dwa1zbml59dwqr49d5b3ib5k2zz01ngy9")))

(define-public crate-recycle_vec-1 (crate (name "recycle_vec") (vers "1.1.0") (deps (list (crate-dep (name "trybuild") (req "^1.0.89") (default-features #t) (kind 2)))) (hash "0922qy5nyb5dc7hynab344gn8x1q108qkwj03f69vq9d64mnbfg4")))

(define-public crate-recycle_vec-1 (crate (name "recycle_vec") (vers "1.1.1") (deps (list (crate-dep (name "trybuild") (req "^1.0.89") (default-features #t) (kind 2)))) (hash "1i3fq3frvmpbgrvnc4h7n73njwl2vabcymy979sfigmwf204m41d")))

(define-public crate-recycler-0.1 (crate (name "recycler") (vers "0.1.0") (hash "1zfib9s33880axjvgqybql9sg2nbsvi9ffvrjdfdlf86xc3p0xg9")))

(define-public crate-recycler-0.1 (crate (name "recycler") (vers "0.1.1") (hash "1dq4mg7lprp12n62qd0wrh33mvqb7ix09syv76hibfapnx3rvmbx")))

(define-public crate-recycler-0.1 (crate (name "recycler") (vers "0.1.2") (hash "1fh2mdbrf87ry70amwr93l5ngipwyg5vqw2n5hh6m63wf8izxyma")))

(define-public crate-recycler-0.1 (crate (name "recycler") (vers "0.1.3") (hash "0w215ybplrky4bjm7466n3b9hc0a9v6xfg003h9vfvs1rzjamdx0")))

(define-public crate-recycler-0.1 (crate (name "recycler") (vers "0.1.4") (hash "1yll0sqswy6afk9ik7r22djqafa3wfgvgdzqqh7jbczyiqr2gp4q")))

