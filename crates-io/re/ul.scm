(define-module (crates-io re ul) #:use-module (crates-io))

(define-public crate-reuler-0.1 (crate (name "reuler") (vers "0.1.0") (hash "132gmf89v0swr35alg5iicq3j1s28h2lypgyybp69l3r9lfzkjin")))

(define-public crate-reuler-0.2 (crate (name "reuler") (vers "0.2.0") (hash "0s8z6gw97qdw4mdpk9h4bpa08vwrs1w47js7qy3fi36j5scp4kih")))

