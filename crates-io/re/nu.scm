(define-module (crates-io re nu) #:use-module (crates-io))

(define-public crate-renum-0.1 (crate (name "renum") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "1604kpw3d15hxigky92mm05y3q4r6h5m0j6b1lfyzcq7910clwm2")))

(define-public crate-renum-0.1 (crate (name "renum") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "1si40s3jna7i3y8bp1fgyinn8dnm2m6b578lfiw67xjvydrph6cx")))

