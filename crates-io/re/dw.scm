(define-module (crates-io re dw) #:use-module (crates-io))

(define-public crate-redwm-0.0.1 (crate (name "redwm") (vers "0.0.1") (hash "1ar7k4b5xfa6vp20ppn2n2qn8fvqynarmdd72sgzczxg327li779")))

(define-public crate-redwm-0.0.2 (crate (name "redwm") (vers "0.0.2") (deps (list (crate-dep (name "x11rb") (req "^0.8.1") (features (quote ("all-extensions"))) (default-features #t) (kind 0)))) (hash "0g7cqi3480q1l2rn67dhfalg60gf0p2aasrrhghmb14nsh9d3b9a")))

(define-public crate-redwood-0.1 (crate (name "redwood") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.17") (default-features #t) (kind 1)) (crate-dep (name "crossbeam-utils") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0wa19faz2nnig51h1pbqgvyr0pcz8iky7h74wadvrdnhb1qpsjkd") (features (quote (("default" "asm") ("asm"))))))

(define-public crate-redwood-wiki-0.1 (crate (name "redwood-wiki") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (kind 0)) (crate-dep (name "rusqlite") (req "^0.24.1") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^4.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1a0a25y80l25i3zdwfzcjijz380lj60q3y4s4xqla9135dfwflkk")))

(define-public crate-redwood_cli-0.1 (crate (name "redwood_cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "redwood") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0npnw5y5wajfs4fxl4h39c6pm3i7y28q82lssn7lhqykrzsmxy90")))

