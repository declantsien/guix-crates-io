(define-module (crates-io re sx) #:use-module (crates-io))

(define-public crate-resx-0.1 (crate (name "resx") (vers "0.1.0") (hash "1r3s7fkh99ynjmdfq4kvk7nc6x2w244gkfj702jgwmkz96hdkk8n")))

(define-public crate-resx_derives-0.1 (crate (name "resx_derives") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "resx") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0qhh477j0vfyr86wnhlrwgmri8z6wkld1nswi4gdc2xyrqcy1nk4")))

(define-public crate-resx_derives-0.1 (crate (name "resx_derives") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "resx") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ri8mdp2ssra14d9x6sfq4pv63by3wspm2dlidc89j80f69qxlm4")))

