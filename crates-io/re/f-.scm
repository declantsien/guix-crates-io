(define-module (crates-io re f-) #:use-module (crates-io))

(define-public crate-ref-cast-0.1 (crate (name "ref-cast") (vers "0.1.0") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "1474lhc1zif901321nirp3zcfswzxq07w34pvh3m54qvhxizxzn6")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.0") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r3mvbnp60b7lynpvvv6gggijjbisxwn2434q2ws42ihk61651gz")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.1") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w3pbihc2dxd6q5b7chvjv8x3rgcwhn31pdl2h4wk48khg621viz")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.2") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "168nv0vcj7lbz202pwhfz5lq0391jd9c0r7vxhlcz70saalm5cra")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.3") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "02b2d2r5c43sqvdb6hcb3f08xvr6953b0cb5bpyx9d13in7f4cqb")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.4") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "01m40ip334bx248h9bxyxcpvgfihwa7h5jb2jp8gh8rcwy5sd9vh")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.5") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bd8q0vi6fxydbaxfkfq94fjz7zw7w13qi7a6kp6jwqf7xfsa081")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.6") (deps (list (crate-dep (name "ref-cast-impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jgj1zxaikqm030flpifbp517fy4z21lly6ysbwyciii39bkzcf1")))

(define-public crate-ref-cast-0.2 (crate (name "ref-cast") (vers "0.2.7") (deps (list (crate-dep (name "ref-cast-impl") (req "= 0.2.7") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fcbpfb7xhr992qvyfg9hr5p63xqykjp48pm3f7a1q21vmhzksvv")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.0") (deps (list (crate-dep (name "ref-cast-impl") (req "= 1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vy378bdzb4kcz13kh96c5n5qw1jinhfrya5j4bf9rxz65x1jzq7")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.1") (deps (list (crate-dep (name "ref-cast-impl") (req "= 1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.19") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1lfdmiay4fchzncpdw4zdhazcm09zh7cidwdc70kzdp1fmw4q88a")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.2") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.2") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.19") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "08r6qz7228k55nlyl5v7ykdzxrasnawgzmb1jrbfbnkx2s3ifp3l")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.3") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.3") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.32") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1kmyva8qifql967b6n5fqv1ybkr8dsk7541ppy25pwxwyjr2cxp1")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.4") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.4") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.32") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1g8akn898qfckrzc90v7npiizxl7m2zvqqpsbfnawhl7ph9iqgyx")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.5") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.32") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0pgrgmr72dmb7xh0w0nssi316jih1yj894mm24y8jf6xfwy8ljz8")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.6") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.6") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.32") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1nj1wp530yd1rx545qisvllvp7nbv9x00iax57p391w0bn1jl3rh")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.7") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.7") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1arjfjs0b52wa4z43p6v2rmppnssyr5pr8l8rkj86avcbdi5hpb8") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.8") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.8") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "12l29aj44iwkdg69w538dkf0d4yk3hfi3yckdbv67x8c8d08jv3p") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.9") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.9") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0arkgnl28zalp4r1h8h415rh1lji2qlr0r59014v8jj9079bq4zd") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.10") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.10") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1haf4wjh5vjyv39hyi81zgh9mj993x4cy87n6d8z6az3ycrgdsxq") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.11") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.11") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "059k3pqyyhw6pfgh479kqsn9igljj0vxpg4dmflm29r75y4r5lvh") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.12") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.12") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1hj9mzzashgvkykm38jb63imb76ggxlzhj7y2d4rp4kcfkqk79qj") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.13") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.13") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0i1ca3rwgvrd0pzp2ii1yci2jidjmgvrxjnqgiv0vmprnkmmvcak") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.14") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.14") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1vdryz081qiyi1d10vny209g5shcjp5b9rzwys7d9g4kja6gny4c") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.15") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.15") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1m9xrvanqh7mmy2l2g3lj856cxkzbc4q0pjijw0623pqkvq2rbx9") (rust-version "1.31")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.16") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.16") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0b5iyxdxwrmysi36injr2zi7mlv9m2473sbhxr0nicy8n68slgzl") (rust-version "1.56")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.17") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.17") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.52") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1hv7cqpqn3i5x1kqv7pkfgzim5sr8b4piaam8a7m9d8nbwd7pl45") (rust-version "1.56")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.18") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.18") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.81") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0sxzy3f0zmclsmi1z17n16xbjbp99d5c6nh7592yy6f3fya82h8n") (rust-version "1.56")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.19") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.19") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.81") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1w9x0ris7ds9krfqg5rr181i1100z1a5116gn7fl46c4x0c7xvv1") (rust-version "1.56")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.20") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.20") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.81") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "102j2yqipkizkc85p2k469l35j0rv7p88nrb1yh9viz9fg85ipmc") (rust-version "1.56")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.21") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.21") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.81") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1ir9gd0mkpp0rnr0wqsz19cmz8ksqwx2qij3zgpsx1i6y74kwcak") (rust-version "1.56")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.22") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.22") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.81") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0gs7m6rikdf1k0vk6irnf9g0vwpf4ilzg2pg7cd1nwnia166v164") (rust-version "1.56")))

(define-public crate-ref-cast-1 (crate (name "ref-cast") (vers "1.0.23") (deps (list (crate-dep (name "ref-cast-impl") (req "=1.0.23") (default-features #t) (kind 0)) (crate-dep (name "ref-cast-test-suite") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.81") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0ca9fi5jsdibaidi2a55y9i1k1q0hvn4f6xlm0fmh7az9pwadw6c") (rust-version "1.56")))

(define-public crate-ref-cast-impl-0.1 (crate (name "ref-cast-impl") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0vv563kgj0p7gqcrh2g3xid1f28yb27zhhb35y1wkh7h91lrh8n6")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1dwx22fd4cy2d24p6c9f72vzggiq1d4jq9yqkmigyc48z8dn587j")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (default-features #t) (kind 0)))) (hash "0sra3jwar40q28h5cvgkjzq615wzw4dpp4wz82l0yl4yx8vbsdpb")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.13") (default-features #t) (kind 0)))) (hash "1i0xxj98i7am3b087sm22ss9lv2h2x8nypd2p590c0r2kwspxa7q")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.3") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "0xdlfrda137i8pf0qs1yn4hmiwqw59jy4a4sk5zh5kpvh5ba897d")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.4") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0w5z7d0k46pflwaz4gnymdcjv0bw0w576hb6lgn5dzsxm2vhl84y")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.5") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0q91br61nr890skwyq5w0g293cpwwnhyy51p8acjkfv5fxd37jcb")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.6") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0hw0frpzna5rf5szix56zyzd0vackcb3svj94ndj629xi75dkb32")))

(define-public crate-ref-cast-impl-0.2 (crate (name "ref-cast-impl") (vers "0.2.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0av43xxjlinfqklb67rpj217cmaxfjsf8151gs0hbs4hnr5664ck")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "07rc752npmkyc5b8xcqk2ydbl3gxi1n4fzrq0wx9wz5qd4mvavn3")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1j7fyfv7safs51871pfwh91b7lh1pivgnhaqqapbkz36vafbabk0")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i1i3an8si070aqg2mvz6yqc6y2pl9zhd6dd2piz17l7mdsv88bx")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wjgpcabdpa0da5lag214xc07lrp1csrm128i4lb1i4axp53qlhc")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0c81jh5mih29lvs7mmq29rbzg0r0h7sp1r8la96iwnfpnhdn5rf2")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1l5w1fqx9wm8lg8wq5j2r8nc7323kpbag353cd9mk9ksq0zigmcr")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1lndalzfwhrpknibjs027zjcfjvincxqhyv3b4wwn89brnpf6f2c")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "07zs83c6nj07h1b1jssmqi6p5xa3xp03l66598vnjhf955784hx0") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0m0zy2d298n5wr96kxxahsygwdyig3xs2bpnpw1n2a308iqc89jz") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1ykqlp3jc0vvyq8gdhddr10hafh4mjvb2lrvj0r5x2i5cdhcsd2j") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.10") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1dh1halb6igad1x3kjbjipksypmi8r553khj0pa99qjwlzx9iaya") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.11") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1yrqw58psbfwg4x3r19xmwnaxkpx21awxjsa0jql2dlwx98jjg11") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.12") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0s03qqqw3kglg5kya14qwxsp5s762dhvwqqlhqhx5bxc055dx1sq") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.13") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0dlg5b3qs3mgh298454qvibda7p0036x78w5cpgd6kz9x48qbymb") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.14") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0pzfrixs4vnhcrr70jp71aycb2h65vz82qhh0r44wr03my90r74z") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.15") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "06w8kkb1qn2pbcn5gmcwd7kh0vxfsvkmvi9v8dsy50ir740i4l4w") (rust-version "1.31")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.16") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1rzrfk7hl28gvc44ws06d5yywra8c131lk1nlyr0l1c3njm7a8ld") (rust-version "1.56")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.17") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "14psk784nnvh70mmgza88drgfsmp0vs7zfqp8f1ybzpgv48b0c59") (rust-version "1.56")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.18") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "05kjg9g9akzb0yns255a5zvdkbwk0wa9kp1cf8k6h25nv7d57gv8") (rust-version "1.56")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.19") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "1yn91c66nrl94ypljc6m85mi3gfmqrdiyygkjz3pcqknbg4g1yid") (rust-version "1.56")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.20") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "09a9cscyfpdmbcmk5klz39hcnmclb4b3w3nxjf0h146grz176x3z") (rust-version "1.56")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.21") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "1i4x0wdbbgq9q06w1mip567vmmgwblzkq1177glc5wj5d2zw8ri5") (rust-version "1.56")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.22") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.46") (default-features #t) (kind 0)))) (hash "1k5w51zyy06kcnr0vw71395mx1xrlxlpma35zjx2w2lvv7wb9paz") (rust-version "1.56")))

(define-public crate-ref-cast-impl-1 (crate (name "ref-cast-impl") (vers "1.0.23") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.46") (default-features #t) (kind 0)))) (hash "1rpkjlsr99g8nb5ripffz9n9rb3g32dmw83x724l8wykjgkh7hxw") (rust-version "1.56")))

(define-public crate-ref-cast-test-suite-0.0.0 (crate (name "ref-cast-test-suite") (vers "0.0.0") (hash "0cx15bhs8zq99qidrhjmjhc5krlfclcaq0y5c68pq48vlds72idl")))

(define-public crate-ref-extended-0.1 (crate (name "ref-extended") (vers "0.1.0") (hash "04p8jcrm9kprsxbnhaz0fjarlvcy38kzd19cj94wi5pi4pf57cxw") (yanked #t)))

(define-public crate-ref-extended-0.1 (crate (name "ref-extended") (vers "0.1.1") (hash "0sxgcihczqd5w329kcjksmrph2yj468h302cdykqpi0yh6is03sx") (yanked #t)))

(define-public crate-ref-extended-0.1 (crate (name "ref-extended") (vers "0.1.2") (hash "1xzr325smfayzg8w73hmv4yxawjjjr5c0kf9xisnd0rgz51bvcvx") (yanked #t)))

(define-public crate-ref-extended-0.1 (crate (name "ref-extended") (vers "0.1.3") (hash "02qf90ks74dyqpab864x3819cb9ixyahkay3r6zxch8nprdvjywj") (yanked #t)))

(define-public crate-ref-extended-0.2 (crate (name "ref-extended") (vers "0.2.0") (hash "041264x022748fv8h1sh3n3fynk3y11k6f2zqgzd25ddsyllazd1")))

(define-public crate-ref-extended-0.2 (crate (name "ref-extended") (vers "0.2.1") (hash "1z7bwi9bkhxbxkncmxb7sjnqndnxhv41q4m5psd3sn7npk6pvf78")))

(define-public crate-ref-into-0.1 (crate (name "ref-into") (vers "0.1.0") (hash "08nrdja78dx2r85iy0wnwn18fzgbnz9xnn7sb2x9im026djwzna6")))

(define-public crate-ref-kman-0.0.1 (crate (name "ref-kman") (vers "0.0.1") (deps (list (crate-dep (name "atomic-wait") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1x4q4nam8fqa5mvqh3zx90kz6h6692wydi7bjn3syikfjx59vrx2")))

(define-public crate-ref-kman-0.0.2 (crate (name "ref-kman") (vers "0.0.2") (deps (list (crate-dep (name "atomic-wait") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "17n680baw18fjzj1nhc8fy3qpzd3z08pvn255v0c098yqndfhggn")))

(define-public crate-ref-map-0.1 (crate (name "ref-map") (vers "0.1.1") (hash "1bgb8ljwp9svlgdyvnsq5cpqdf0y86wsmd0n9s47fvv2gqk46wg0")))

(define-public crate-ref-map-0.1 (crate (name "ref-map") (vers "0.1.2") (hash "0pgajd5lak3bq2j4qh9ihxibb79gdxbpdbv3rr0jlh993z9q2ckh")))

(define-public crate-ref-map-0.1 (crate (name "ref-map") (vers "0.1.3") (hash "0xbhpw62xajvb0h4h6y94bjk0yqif3a8mq558l22d7rnbsc76ayj")))

(define-public crate-ref-mut-n-1 (crate (name "ref-mut-n") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.88") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0jh1mzh1jnh21pcvqp58101a92s9rjb8cn4d4rv2vsybaasgxd5i")))

(define-public crate-ref-mut-n-1 (crate (name "ref-mut-n") (vers "1.0.1") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.88") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1yl7ph9fig4xrb2wzk1sw4077rs6srvvxsh0s2czlyd918g0yj3d")))

(define-public crate-ref-ops-0.1 (crate (name "ref-ops") (vers "0.1.0") (hash "1746z1z5fd58nl1b35ma4i5w6bx9j8maa7h1244ya85ii1dpvcf6") (yanked #t)))

(define-public crate-ref-ops-0.1 (crate (name "ref-ops") (vers "0.1.1") (hash "0lbzi7zdpyfd6py155y9gvwy58b3cskqkn2l2djmhd0c2k283lkp") (yanked #t)))

(define-public crate-ref-ops-0.1 (crate (name "ref-ops") (vers "0.1.2") (hash "0gispkwk14js319kmp70crm6cdcjinj0wnnva6v5igqb882y52rp") (yanked #t)))

(define-public crate-ref-ops-0.2 (crate (name "ref-ops") (vers "0.2.0") (hash "0h2hfja2mmqrr1placy91crnw5bnh5r9phasidvvgi5d1pwi7wqw") (yanked #t)))

(define-public crate-ref-ops-0.2 (crate (name "ref-ops") (vers "0.2.1") (hash "00km6qqzf75iwh4k0fd2lbzfblkbwkcbxby0whn3l5apl1clsg0l") (yanked #t)))

(define-public crate-ref-ops-0.2 (crate (name "ref-ops") (vers "0.2.2") (hash "0fqmdkwqn5zkfjyaqwaidxy5y5wpj982pkb6kchlc5j8wqc7ad05") (yanked #t)))

(define-public crate-ref-ops-0.2 (crate (name "ref-ops") (vers "0.2.3") (hash "047rplr064zciv47igfz42ljvz5siidfrfq9y6xlcxmpwmdi9fwb") (yanked #t)))

(define-public crate-ref-ops-0.2 (crate (name "ref-ops") (vers "0.2.4") (hash "0jszd2qq2pgrj97wfpi4v2bmfw1f4bz4ix0lsiws939jwd8086b8") (yanked #t)))

(define-public crate-ref-ops-0.2 (crate (name "ref-ops") (vers "0.2.5") (hash "00yi4gkwga6pp1d3rnnx5lzaxd1ic1zn58q33411hvscmag2chx7")))

(define-public crate-ref-portals-1 (crate (name "ref-portals") (vers "1.0.0-beta.1") (deps (list (crate-dep (name "assert-impl") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "wyz") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0a1871nwrs2idd5fw2pz05y54y16i6dd4y63wyfmsvh3f4ajr3gs")))

(define-public crate-ref-portals-1 (crate (name "ref-portals") (vers "1.0.0-beta.2") (deps (list (crate-dep (name "assert-impl") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "assert-panic") (req "=1.0.0-preview.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "wyz") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lmq1gmrfgcdhh1mbvrkwgqki08vialb14c00nilddhkrd2fcv73")))

(define-public crate-ref-stable-lru-0.1 (crate (name "ref-stable-lru") (vers "0.1.0") (hash "0gi9s5ya254nqqqyqs2v7s7nm144nzz5hlqk2vf5f7ppb203ich2")))

(define-public crate-ref-stable-lru-0.2 (crate (name "ref-stable-lru") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("executor"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1k3mr8yz0prsw6kzm6mc839y02c03a4b8fx5fhbsx3rdmi9k3xkk")))

(define-public crate-ref-stable-lru-0.2 (crate (name "ref-stable-lru") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("executor"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1alri9ni9z1iw06nyavjjbrm4c425c68s9rs3i1g6qnrhh3k3c7h")))

(define-public crate-ref-swap-0.1 (crate (name "ref-swap") (vers "0.1.0") (hash "1k6gcnf9kjx9l98l9zx4x534dz1g5pgk8wglsfzrbh5i6dj4lvmh")))

(define-public crate-ref-swap-0.1 (crate (name "ref-swap") (vers "0.1.1") (hash "1sr69h63zalkbmgm7v8fa3wic95iqzgw66dh0ysi1pv7drprxssr")))

(define-public crate-ref-swap-0.1 (crate (name "ref-swap") (vers "0.1.2") (hash "0g7v535w9vqm08gvar3i2ah92p25lc5dbmc8y05b9rgyvxa0rhq9")))

