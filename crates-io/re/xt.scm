(define-module (crates-io re xt) #:use-module (crates-io))

(define-public crate-rext-0.1 (crate (name "rext") (vers "0.1.0") (hash "127z0p7ind3qh2qml07qg809c86w0ik0wya3v3glsa7gmhbypxqd")))

(define-public crate-rextc-0.1 (crate (name "rextc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "021v6hkcq7ib2xmyc1lxmxz64m9fi68vf0prz334wv07bkbam37m")))

(define-public crate-rextc-0.1 (crate (name "rextc") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0y7hs2wxd1bly9bznncafn2c0cr6na7ab38azjdhqjw1k26giv5q")))

(define-public crate-rextc-2 (crate (name "rextc") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "03lglaph03kr9asc3lkizncgv209ajczgsxv49i65p9mgp8l1yd0")))

(define-public crate-rextc-2 (crate (name "rextc") (vers "2.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rlrb7vpqrifclpkain2l40rlw021p231r2flhba79ybvgaa3q95")))

(define-public crate-rexturl-0.1 (crate (name "rexturl") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1yrplplhgxz4lpnzzg9npaq0dqb8cbav1kg48gvb3cgxldqqf8j4")))

(define-public crate-rexturl-0.1 (crate (name "rexturl") (vers "0.1.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0cf518vb529wwwp7cjkf949bqdxqfmp0vzf36rzdsyr3v2cvgn0m")))

(define-public crate-rexturl-0.1 (crate (name "rexturl") (vers "0.1.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1syjifgvyx8nih8k4hqsm027hfkbp87h55wpck232mgc99lxnsg6")))

(define-public crate-rexturl-0.2 (crate (name "rexturl") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "19f67ng1as9nql0y8nxccdw3k2ifpxl42hvwr4dgasq09jga3gd2")))

(define-public crate-rexturl-0.2 (crate (name "rexturl") (vers "0.2.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1vhjqwx597i9a5yrc5m500xir44637j810jzyn91vgg7l5p9ji3l")))

(define-public crate-rexturl-0.2 (crate (name "rexturl") (vers "0.2.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1n1l2ivk9magrkv8fc4y9c3ghal9ha53r7d9z74k46jr19fmyl66")))

(define-public crate-rexturl-0.3 (crate (name "rexturl") (vers "0.3.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo" "env" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0s3w3bv7p9wspyrnra3xz6m5zrxkcqmdk4dh6vglhl3hksd4rr8c")))

