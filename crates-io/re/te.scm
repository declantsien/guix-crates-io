(define-module (crates-io re te) #:use-module (crates-io))

(define-public crate-rete-0.0.1 (crate (name "rete") (vers "0.0.1") (hash "15ijpbhs4wjifsnf78619q58176r5fccmr1zrjzwvsfiixc3ny65")))

(define-public crate-retentive-lender-0.1 (crate (name "retentive-lender") (vers "0.1.0") (hash "0068g35hzc3jj4ljfivjv8l2z3qhrhymcs111mdpb2y63db83ig1")))

(define-public crate-retest-0.2 (crate (name "retest") (vers "0.2.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 0)))) (hash "1szzxb2h2bbcs4fms0cx8qr33v9hql3dhs03j65bhfa460j744a1")))

