(define-module (crates-io re dr) #:use-module (crates-io))

(define-public crate-redro-0.0.1 (crate (name "redro") (vers "0.0.1") (hash "1zarf995k3x22lvlcilwgfydxw620267qpynz19mpp1w0ga10gcg") (yanked #t)))

(define-public crate-redro-0.0.2 (crate (name "redro") (vers "0.0.2") (hash "04qbnxhkicdvp7xpr43bpyqznxp1pwrnff0r453kqqfh0hmdsxs0") (yanked #t)))

(define-public crate-redro-0.0.5 (crate (name "redro") (vers "0.0.5") (hash "12s5kykimd2nb9swhqnsw6cnrg97fmp7w2npw41q2n6r83nway5z") (yanked #t)))

(define-public crate-redro-0.0.7 (crate (name "redro") (vers "0.0.7") (hash "03dnjgvk269g8zx42wi55ka78r902c3wxawgads25faxabh3g1h1") (yanked #t)))

(define-public crate-redro-0.1 (crate (name "redro") (vers "0.1.1") (hash "1n72kr2p5p0ni2nk176qk2q9adr5dqgqawm9ks8fz624zq79x0dc") (yanked #t)))

(define-public crate-redro-0.1 (crate (name "redro") (vers "0.1.3") (hash "07jkmxbir3y3cq8dvp14036bppp66ci2dp681ga64bxcn897wjfh") (yanked #t)))

(define-public crate-redro-0.1 (crate (name "redro") (vers "0.1.4") (hash "1l12sp2x2fqrbvqifabn963j4r6p40b7r0s245hyrs52jvq0mkww") (yanked #t)))

(define-public crate-redro-0.1 (crate (name "redro") (vers "0.1.77") (hash "18lz7kg1b5p3v2izjs6awvfdyzrb3d2h7s4ssb4agcs1x8xr4ghb") (yanked #t)))

(define-public crate-redro-0.77 (crate (name "redro") (vers "0.77.0") (hash "05vgcfcwp6pb6d0x5gpmm18aa7my51mxxzcykz9lrk5rmdlskjlw") (yanked #t)))

(define-public crate-redro-7 (crate (name "redro") (vers "7.7.18") (hash "18jnk253qic67pv9i4nv99ywwqz7ihvj7nbvxpardzy6yafrcdmx") (yanked #t)))

(define-public crate-redro-2018 (crate (name "redro") (vers "2018.7.7") (hash "186wx6jx187iv7dis79vgw281m1yp051d386zny207aqwn8fvsz1") (yanked #t)))

(define-public crate-redro-2019 (crate (name "redro") (vers "2019.12.13") (hash "1k0isg9mkiy5g4spwrvzj0y5nh16c1a7x3j9x8di6v5amd7cgrxv") (yanked #t)))

(define-public crate-redro-9999 (crate (name "redro") (vers "9999.999.99") (hash "0cs5pvyamj17v4qxbqbbzk38r4nv625qzxzjin5pr62mz6kw40jr") (yanked #t)))

(define-public crate-redro-9 (crate (name "redro") (vers "9.9.9") (hash "0y1g9a8bjn2pa96z29asbvdwbyr9kl5nx56vwazlhjpdsjxaqkqx") (yanked #t)))

(define-public crate-redro-99999 (crate (name "redro") (vers "99999.99999.99999") (hash "1qzd8scc6hjqg899mxjpicy6gn6a2zwkbdl70h6ii6r6waz963kd") (yanked #t)))

(define-public crate-redro-9999999 (crate (name "redro") (vers "9999999.9999999.9999999") (hash "1i3ab8671fwxhd1q9j5r7wxhmxq85caygagn9lmvs4wlin7vw5bb") (yanked #t)))

(define-public crate-redro-999999999 (crate (name "redro") (vers "999999999.999999999.999999999") (hash "188a7gksfsgg8k0zs6j872mk1pbqyy6ydz5fzr2vaykpwkcyzqf3")))

(define-public crate-redrock-0.1 (crate (name "redrock") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.84") (default-features #t) (kind 0)))) (hash "0ls04g2djfhl7rcc60wisvq9mi5cafy2qw07chbfqcms8qnccx1f")))

(define-public crate-redrock-0.2 (crate (name "redrock") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.84") (default-features #t) (kind 0)))) (hash "0scwcvbnq6xplamch7kxahijakrkga8xlvmxamdmqkxdw8999jr3")))

(define-public crate-redrock-0.3 (crate (name "redrock") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.84") (default-features #t) (kind 0)))) (hash "0c52mp9l5rq8y11vfd7f84vdn0m1gnmbbn6kj85z771yyvgv1qml")))

(define-public crate-redrock-0.4 (crate (name "redrock") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.84") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.84") (default-features #t) (kind 0)))) (hash "164s0n30yqvqv23wnxk9pkcr6r6qsfscnq4aj0xxzbvzn1hxkpha")))

(define-public crate-redrock-0.5 (crate (name "redrock") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "002q7w9afcmk71pmylgcg4k2s99gsvamq9ny19qgawzf9dlrwfqv")))

(define-public crate-redrock-0.5 (crate (name "redrock") (vers "0.5.1") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "0d83g0c04c0v85j0sp2769w58gay2yd7bwnmjll575ndxb1rylqp")))

(define-public crate-redrock-0.6 (crate (name "redrock") (vers "0.6.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "0kiaqpj26mzq2r4mgr90l4iwmhychblfch9kldd42yhm5x8741sz")))

(define-public crate-redrust-0.1 (crate (name "redrust") (vers "0.1.0") (deps (list (crate-dep (name "c2rust-bitfields") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1hrn7bsci9sh1v208yfgq3clvshmyaysff6dfbgmzw0b51gg720y")))

(define-public crate-redrust-0.1 (crate (name "redrust") (vers "0.1.1") (deps (list (crate-dep (name "c2rust-bitfields") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "040i4pr7970fircvwyiwxx0233qlza42nbn7sjc2rsnr76qp7jps")))

