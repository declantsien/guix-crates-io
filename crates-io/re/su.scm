(define-module (crates-io re su) #:use-module (crates-io))

(define-public crate-resufancy-0.1 (crate (name "resufancy") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "filesystem") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "pug_tmp_workaround") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wkhtmltopdf") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vr7gz5yif904hr2xvkq79ywkz0bbgrnl6lv872d0rnkn62imlm8")))

(define-public crate-resufancy-0.1 (crate (name "resufancy") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "filesystem") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "pug_tmp_workaround") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wkhtmltopdf") (req "^0.3") (default-features #t) (kind 0)))) (hash "1yg971d9325c03w4c6rpmar6lng6hmjrd6zbsrzzs8x232v0qcpp")))

(define-public crate-result-0.0.1 (crate (name "result") (vers "0.0.1") (hash "0b30xrxig3qxk8rqcna8acx310clcs17x6n63cmvzyhlsmz8zhlp")))

(define-public crate-result-1 (crate (name "result") (vers "1.0.0") (hash "0q2mslk9mvpdrl5zr1yvlb8ikmynpq5786c8ybn1wpa03rcqwk8r")))

(define-public crate-result-ext-0.1 (crate (name "result-ext") (vers "0.1.0") (hash "0989y4n7xzhxw6h5s7gh1hdfpwy8cxxxpizcq0ji3db0wlgyik43")))

(define-public crate-result-ext-0.2 (crate (name "result-ext") (vers "0.2.0") (hash "1hnw75m67m5kvr39g9iakdjbz191y1fbjkiv3vkxb59bgqla89ak")))

(define-public crate-result-extensions-1 (crate (name "result-extensions") (vers "1.0.0") (hash "1jg55dgqvqvgcgpkpwlmkzq97v6mhhk6v9k6pcyh9vahh54nrn7h") (features (quote (("library"))))))

(define-public crate-result-extensions-1 (crate (name "result-extensions") (vers "1.0.1") (hash "01rlj9is308wnajlc75zl66d23kn4rgafw9bxwy95wnj5gfcb0mh") (features (quote (("library"))))))

(define-public crate-result-extensions-1 (crate (name "result-extensions") (vers "1.0.2") (hash "0szx3i1zpdal7m0lm3nrlpiig7df12ym52prni1kzqvxlwknk8g6") (features (quote (("library"))))))

(define-public crate-result-inspect-0.1 (crate (name "result-inspect") (vers "0.1.0") (hash "11kgw2zrdnh59wp1m7z95i7wcbp6vc2czlb27k23pf854xjv893n")))

(define-public crate-result-inspect-0.2 (crate (name "result-inspect") (vers "0.2.0") (hash "1dq6xlmbr7cbz178r1fv2fwq3df0nq01ksfqk6l33n04rkg926h2")))

(define-public crate-result-inspect-0.3 (crate (name "result-inspect") (vers "0.3.0") (hash "0q5iinq0k1dryxqzfyzn4qwmjbj11agrgvazqfp2j87sxkw51adp")))

(define-public crate-result-like-0.1 (crate (name "result-like") (vers "0.1.0") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "15h1sfrcs0xgq1ymi85rpf6nyfac29x49h7wzsdvcnay62jsfi2k")))

(define-public crate-result-like-0.2 (crate (name "result-like") (vers "0.2.0") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jgxi0ggna7x2ggd5n3i47750db2nxz7j5h0qbb3jb4xrdprzjhn")))

(define-public crate-result-like-0.2 (crate (name "result-like") (vers "0.2.1") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "16zb9qzi0hsrx8k5a5nzi9knq3js8xdxsqpmih19j3m47mdlqsk5")))

(define-public crate-result-like-0.3 (crate (name "result-like") (vers "0.3.0") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bcgcrfn82aqkff1702k18cbdsvrjh0wgbxfqwi5icy7j4p1aghd")))

(define-public crate-result-like-0.4 (crate (name "result-like") (vers "0.4.0") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0w8qr7c07y1saki9dj1722i56wk553fbxjl5m6cwjif1d3979mn2")))

(define-public crate-result-like-0.4 (crate (name "result-like") (vers "0.4.1") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1dn6j1jyfyy2pcmlm7zzipxnpr9vl9i6n3zw213ayvkxsnak9ay5")))

(define-public crate-result-like-0.4 (crate (name "result-like") (vers "0.4.2") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0halw8i62c9gv89a1hqhlwqzv3p62mbpdfr40400a1hl5r6307rf")))

(define-public crate-result-like-0.4 (crate (name "result-like") (vers "0.4.3") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0g074wl3vs77zxjdxkk15ckql2nxcmhfh42v7splwf7sx5yr4pgr")))

(define-public crate-result-like-0.4 (crate (name "result-like") (vers "0.4.4") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1vdja7h018g1a8pfxi7b4fhr7i30g8gkrqhm8sl6v9iswvis4591")))

(define-public crate-result-like-0.4 (crate (name "result-like") (vers "0.4.5") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0a2486vhzvidv17fhv4qndxihykrdcr5h1g27f8rcnkrjq1gx03v")))

(define-public crate-result-like-0.4 (crate (name "result-like") (vers "0.4.6") (deps (list (crate-dep (name "is-macro") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "047qjh9d78hrxhyvkvvmd1lvnk10rmw5bs1hg98rhf636mjcxiyc")))

(define-public crate-result-like-0.5 (crate (name "result-like") (vers "0.5.0") (deps (list (crate-dep (name "is-macro") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "result-like-derive") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1px9d3n4mfpk5ycg95qnsmi7a9khcn16rgr6bimhazbaxwpigxxb")))

(define-public crate-result-like-derive-0.4 (crate (name "result-like-derive") (vers "0.4.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0g8bz7f4rrhr248z369p4y3y2xwy3y9m5rjxwymw4hyh6gj3h39z")))

(define-public crate-result-like-derive-0.4 (crate (name "result-like-derive") (vers "0.4.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-ext") (req "^0.3.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nms70nmbmw49d8n93qh4invrmnlnpaam0qjra7p4zqmr1lzgynw")))

(define-public crate-result-like-derive-0.4 (crate (name "result-like-derive") (vers "0.4.2") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-ext") (req "^0.3.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1alnx9nsmmk3v6bpbcgwf6hgkazxi7bkn51nazhjmxzqdi3bpsx0")))

(define-public crate-result-like-derive-0.4 (crate (name "result-like-derive") (vers "0.4.3") (deps (list (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-ext") (req "^0.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rmrm2qwgq04hrcpykan1w9mjl5jhc6v8q2j93nb990l19ar3b3d")))

(define-public crate-result-like-derive-0.4 (crate (name "result-like-derive") (vers "0.4.4") (deps (list (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-ext") (req "^0.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "019kfzpk27b94pbyir4jiqm4chn4f0il6lhhzcsl9fa216i5ynv6")))

(define-public crate-result-like-derive-0.4 (crate (name "result-like-derive") (vers "0.4.5") (deps (list (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-ext") (req "^0.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h9mgdr264r71n50qc0hy8r6j38ya0xjd6dqp3fg2fbqmjjcha9a")))

(define-public crate-result-like-derive-0.4 (crate (name "result-like-derive") (vers "0.4.6") (deps (list (crate-dep (name "pmutil") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "syn-ext") (req "^0.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hlgg3q79wsdmhcgalvknpf7m8x8l54gcj8dqml1qwagwnig1aqz")))

(define-public crate-result-like-derive-0.5 (crate (name "result-like-derive") (vers "0.5.0") (deps (list (crate-dep (name "pmutil") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1r8g5pi0lgvbm4q5pdcgajdbrvcgsvjq3ingf1ixd5780965gmm8")))

(define-public crate-result_float-0.1 (crate (name "result_float") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (kind 0)) (crate-dep (name "num-traits") (req "^0.2.4") (kind 0)) (crate-dep (name "quickcheck") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.66") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.66") (default-features #t) (kind 2)))) (hash "1ip23cbn5vcl9wh0l1p01xaqzqnbi0zmh4jw48nj00qqc4f51556") (features (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-result_float-0.2 (crate (name "result_float") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.4") (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.66") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.66") (default-features #t) (kind 2)))) (hash "1igb8zqrh90v18qyrya44kh4i8n6lr58krv6n9f7fvf9l8ciswas") (features (quote (("std" "num-traits/std") ("default" "std"))))))

(define-public crate-result_iter-0.1 (crate (name "result_iter") (vers "0.1.0") (hash "0sadwyxgs9f1qi38615z0sv1gxz6q3d98fdilvx8sb8xrxva48xj")))

(define-public crate-resultit-0.1 (crate (name "resultit") (vers "0.1.0") (hash "0x6idncjbpz2am324nwqmm392m31535bxqnw4sxm7rn8qrzlbvf6")))

(define-public crate-resumable-io-0.0.1 (crate (name "resumable-io") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("sync" "time"))) (kind 0)))) (hash "0qm05w4kcy9hyzz8kzll27lq1r78ys6kdypl164kxzynmvmy5qlg")))

(define-public crate-resume-0.1 (crate (name "resume") (vers "0.1.0") (hash "1mrqacsn6bdvpjdlakch3zn776s0fk331rmfddz1ppgihc15mhl7")))

(define-public crate-resume-0.1 (crate (name "resume") (vers "0.1.0-1") (hash "1apdbnhlwqg5nkjc08ig1x6sh11izbm8np3qxywwgddfkqklg9xm")))

(define-public crate-resume-0.1 (crate (name "resume") (vers "0.1.0-2") (hash "0jjr2g1k7z01qwgk8dv6g9fb9pjaxpda3a9bg050xl25bbh6r84q")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.0") (hash "0j6079llzwh42kknnbsljq3ahdy1kfb9i68qjyz6npwllf01cnji")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.1") (hash "0mmj1a65wh767xsvxvj3s09vn38z0czvni5nklv7wq9kv2yzpl1h")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.2") (hash "0sfyp14wf327aza1cfdanvx7phc178bmy903dz2ka7p33r269r3i")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.3") (hash "1jv47nydy1cawj1r2n5gkwbkyb0h7xd1dfq6qxp6gqf68mwv905x")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.4") (hash "03rgqwsagw63snzckfbp9mvhxf178pcjbm9zxymsv60x4g20gpcf")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.5") (hash "1wmc9ma4w1cc94vxccgf6hq2iwly521yp6v3lgaj66kgrp7x0jsz")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.6") (hash "0x32cycwibg148ikkx6nq5ff8lqr9q8kbb44s60syszf1y6w9sar")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.7") (hash "176y74l1l0ksjg2mv5gw698k91pyls6lyi2fybn4byfc1gwg2adc")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.8") (hash "0x2bp0q6w2njm4y6rqx200q9pqhpa4ah6ds7aqx9c4nggi2yr2ji")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.9") (hash "132fccivp56ixwfb5mg85r4vc7ah8mab5zhm55ms6cg8ml5jlfl3")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.10") (hash "1v5x24d4wjnphkrzqm792m3w1qpxr00v9px1i3xw6szl9zqldaax")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.11") (hash "0v0rfdwmhjk0lsb097d1di1ayyl8nfyxnwz3zh4a8pm486rv9vvn")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.12") (hash "0ysq6smfphxq0v34jfi6icrrmwdnd46qs6j9b8bsvgx35sirnfv8")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.13") (hash "0fb78ms86ibjs9i1jr6fblrxmkbqi860nzk0fiw156fcmycwqhgz")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.14") (hash "073gppfgxsvzjkh9j5jp7l81ys21gg8k79dp778kmyc4kxck0wwj")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.15") (hash "1jxkwpjgm18mkarg9915dadkm2ayqqzayvncby4a34h3qbv7iqka")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.16") (hash "1d7nxxw0xwxmrrsmnf5smq6cv16rkk111nmfdy6phjpc2fkwghrd")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.17") (hash "1257paff4i20c308cm337djlhbj3whfi1cg7lpnvn9qs2d31nyjl")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.18") (hash "0hyjikyiwkyawai001439jy4safqkqi0020j1z89r2k906ns5c0h")))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.19") (hash "0jf665jz02x9a0hb2gw4v8q7hpsyz5q3nkjqrsbcwj5gkz2l3nz4") (features (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro") ("bar"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.20") (hash "0jh79pi1wr3b158km78z5qkjv2dsily9rdg794mp0mplpakg6x0w") (features (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro") ("bar"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.21") (hash "10zkfvarxz2wasz6spksfpa33gq5hdkwiyiswf6fvkbk4g8slhv4") (features (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro") ("bar") ("arch"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.22") (hash "1fxdxs00byqrkv37iz9qv0nc976ynah1z3d19pqha3clnny2wfdq") (features (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro" "arch") ("bar") ("arch"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.23") (hash "09cd1ds0m6kchv76h0lbgfb9ly0dm0n2g2kfiyl7hz48d4rhq5s7") (features (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro" "arch") ("boo") ("bar") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.24") (hash "1pkb3zyiwm7yrig4mf8qlhj8jyfl5hdkzxhnjgl5jr6m4y731j17") (features (quote (("span-locations") ("proc-macro") ("nightly") ("foo") ("default" "proc-macro" "arch") ("boo") ("bar") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.25") (hash "0lhm5dhzh10kcmza2hmivx2k1yky4gmgv5i4zvb9qkis5hczss3p") (features (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.26") (hash "18d6rbp48p9zr5ckbmsbl27ka2f7s7dygwa79v86dqm4asgww9f6") (features (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.27") (hash "07qzisbdsh63fn7ja4n30x3hpwbprh9m17xm0mawib0i3nj3cxwn") (features (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.28") (hash "1d2fiknpxga84r95zqsrk9s7pnvnkb79cwqm4chapvqk7rigmyk8") (features (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.29") (hash "00mvw2qsx6if395zs7cq96r6lhg8q8g3n7cr21v010zaik5c1asn") (features (quote (("span-locations") ("proc-macro") ("nightly") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.30") (hash "0z20wybmdsq2hjzsdwmwbqbvwd0xar2si405mc06k914xd5adp3y") (features (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.31") (hash "0mgf7h20k5z01s0303ncp058qcxh3a8saspgwzqdj5ydmsknql23") (features (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.32") (hash "1vfaq3ilmpz6i95xhmimanssq14cishf11i4468vapzkc9axb8m6") (features (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-0.2 (crate (name "resume") (vers "0.2.33") (hash "0fw84wdkd397nn0d2h0j09hxphh26m3sjijdwrpyfc4155615dag") (features (quote (("span-locations") ("proc-macro") ("nightly") ("magic") ("linux") ("foo") ("default" "proc-macro" "arch" "linux" "archlinux" "magic") ("boo") ("bar") ("archlinux" "arch" "linux") ("arch") ("Boo"))))))

(define-public crate-resume-builder-0.1 (crate (name "resume-builder") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "soup") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros" "sync" "parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rayon") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1c3yz0af4a48n3rlwpzh6ar16zp2af7aqymhxfas88fhpghpq78g")))

(define-public crate-resup-0.2 (crate (name "resup") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.21") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "01sq6ll4qn2ifgx1jsj12v0is60bm875kgz9n76may8w7mbg6ng4")))

(define-public crate-resup-0.3 (crate (name "resup") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.5.0") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "1388h08c7cifaj70q5xyib8lms10pb23m74izzdl26q44ylz9jad")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.0") (deps (list (crate-dep (name "smartstring") (req "^1.0.1") (features (quote ("proptest" "serde"))) (default-features #t) (kind 0)))) (hash "186lgcivdvq1vn951bg74ids7gsslcih2knz9xbgirdfv5gdricc")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.1") (deps (list (crate-dep (name "smartstring") (req "^1.0.1") (features (quote ("proptest" "serde"))) (default-features #t) (kind 0)))) (hash "1h8cjsw0h7fvpr24q1a0xa9alc4dzs2qf5f6gpqj11942rhpk1mg")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.2") (deps (list (crate-dep (name "smartstring") (req "^1.0.1") (features (quote ("proptest" "serde"))) (default-features #t) (kind 0)))) (hash "0y1y0y6bfd9dasgffcmijgc7lag0nl59gs3bzqmdh7w1y86wpb0v")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.3") (deps (list (crate-dep (name "smartstring") (req "^1.0.1") (features (quote ("proptest" "serde"))) (default-features #t) (kind 0)))) (hash "1n52mi59im86k08djwwqgfadb6sva07ky4dxamhfg43hp13vy51q")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.4") (deps (list (crate-dep (name "smartstring") (req "^1.0.1") (features (quote ("proptest" "serde"))) (default-features #t) (kind 0)))) (hash "0knwn4hmi4l4j3j3pz3fr3bxnnxjbgc85abk56vs1vy721xg2w3a")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.5") (deps (list (crate-dep (name "smartstring") (req "^1.0.1") (features (quote ("proptest" "serde"))) (default-features #t) (kind 0)))) (hash "0lac56zp3c36lma0b9gxkifbvlpdyhya8skkp6mwymp80yslpw44")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.6") (hash "0539dzrdxkrzv1zfsv8fwlcvc0wnpysyjazhjmyzgvliphfi8ijl")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.7") (hash "0dv54pmcxrpcc5c3gippmhk14jpfn3372pivvqppx46cbq4papgz")))

(define-public crate-resurgence-0.1 (crate (name "resurgence") (vers "0.1.8") (hash "1jsvdx5zg8nd5izrfiqxfclkr0c8jy26i2rgff20hqndivlpbcvb")))

