(define-module (crates-io re ck) #:use-module (crates-io))

(define-public crate-reckless-ci-0.1 (crate (name "reckless-ci") (vers "0.1.0") (hash "1zf83p2dd39b0z8r861mij8gf0ncw4ip8ssg31y2gqk22abprlac")))

(define-public crate-reckon-0.1 (crate (name "reckon") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "0khrzbgswy34qa0r769jn1y7krqrwyj4j6lzmk6r6cvqjwgbijix")))

(define-public crate-reckoner-0.1 (crate (name "reckoner") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "imath-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "060k2w9jgl4yd3cvlplh43c0d3sj8y8g7y1ivixqdingvmfx0x2h")))

(define-public crate-reckoner-0.1 (crate (name "reckoner") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "creachadair-imath-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1605mp3gvb4yfrvjk3a7jy6320sfagwjvpyacaag3imnzbgimjca")))

(define-public crate-reckoner-0.1 (crate (name "reckoner") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "creachadair-imath-sys") (req "^0.1.2-alpha.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "08c2inmcri9lgyjg9cg7af64akjksw9mwg1y932ir21qfqqzx423")))

(define-public crate-reckoner-0.2 (crate (name "reckoner") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "creachadair-imath-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1dp636hnjgl55fka5gz85a9y4wbzblfqvcv3kw3v3drfgii8r416")))

