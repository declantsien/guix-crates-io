(define-module (crates-io re #{0-}#) #:use-module (crates-io))

(define-public crate-re0-core-0.0.0 (crate (name "re0-core") (vers "0.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0q820ifpg2ybw3vz94dlzdcfc1k91f3gz51d4x4wdbix1nv81h3m") (features (quote (("default"))))))

(define-public crate-re0-pest-0.1 (crate (name "re0-pest") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_generator") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 2)))) (hash "19zc05i8601ycjbysi9188c4i8d21y3nrm1qk519dbl9a83fgfxa")))

(define-public crate-re0-pest-0.2 (crate (name "re0-pest") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_generator") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 2)))) (hash "1j7myks83ympxkcgr43cdjngphxk5lfgixn14nrv3kg7535gcx8f")))

