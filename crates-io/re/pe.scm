(define-module (crates-io re pe) #:use-module (crates-io))

(define-public crate-repeat-0.1 (crate (name "repeat") (vers "0.1.0") (hash "0dn8r10dhpb0psipwdl7bjwgm2svp41bjx5z5w96xb0izir7d2hq") (yanked #t)))

(define-public crate-repeat-command-0.1 (crate (name "repeat-command") (vers "0.1.0") (hash "0p10axx2cfq6w7ikv7sg48qi8lb443f15pgaidjc3nla22nb0kkx")))

(define-public crate-repeat-command-0.1 (crate (name "repeat-command") (vers "0.1.1") (hash "14dvi7zmk5c6nnrc90w0a6g5gbr660vjdhm658864v36y6rqcrli")))

(define-public crate-repeat_finite-0.1 (crate (name "repeat_finite") (vers "0.1.0") (hash "1qxx02b9k4kv1j1mpc6zqc62zkbd2z9x9xy0pv9sfv443wcqjgz4") (features (quote (("nightly") ("default")))) (yanked #t)))

(define-public crate-repeat_finite-0.1 (crate (name "repeat_finite") (vers "0.1.1") (hash "0c50j6n0zsfpl94161my4hgjcqxf3x8a7fqfhfwz75p4z0p21f6c") (features (quote (("trusted_len") ("default"))))))

(define-public crate-repeated-0.1 (crate (name "repeated") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10zabn773yfyni8dr5c292wjxm6z1p4kvjavnnx1sx876a806jfa")))

(define-public crate-repeated-0.1 (crate (name "repeated") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kkv1c48kgk6jgps0qfinvsgka8k9mgl45vm2v6d5lvjkg0m5s09")))

(define-public crate-repeated-0.1 (crate (name "repeated") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16cis8d7nwr080k4jj88vypzqazrl054wvhsz86sd9pywk35x5v2")))

(define-public crate-repeated-assert-0.1 (crate (name "repeated-assert") (vers "0.1.0") (hash "12kvdm9qsvmng25phmdq6jf08wjmnipjiibbi0nmd6g9a06jby7v")))

(define-public crate-repeated-assert-0.1 (crate (name "repeated-assert") (vers "0.1.1") (hash "0vk70niyrnidqqrzf8fqfag32spp3qap0f6pmr4b02nwlrpd6xp4")))

(define-public crate-repeated-assert-0.1 (crate (name "repeated-assert") (vers "0.1.2") (hash "1yc6x4v2r6lnqfnwr4nkjdkyj8bjym5f71cyw50l62bjpiwkfp8q")))

(define-public crate-repeated-assert-0.1 (crate (name "repeated-assert") (vers "0.1.3") (hash "0fdqq4ir6hb4hmhl5acji2q3cv7qnz8l2krn9yn8dd2bfgfg2zsi")))

(define-public crate-repeated-assert-0.2 (crate (name "repeated-assert") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0r9dbrh4710mqxz7rjlrp6is1d5kzzmrwinksqgj1if7j446ygz7")))

(define-public crate-repeated-assert-0.3 (crate (name "repeated-assert") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0znr3dc73a8zfqgcayny8mdx2fc98l1l06dd6wfcmwl2nh86kwmb") (features (quote (("async" "futures" "tokio"))))))

(define-public crate-repeated-assert-0.4 (crate (name "repeated-assert") (vers "0.4.0") (deps (list (crate-dep (name "futures") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("time"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1kpfqwslrq6syxlwy2k743axwfym683s6sm2fz72xykkd6cpx3a7") (features (quote (("async" "futures" "tokio"))))))

(define-public crate-repeated-lines-remover-1 (crate (name "repeated-lines-remover") (vers "1.4.0") (hash "0brk9pqzlh4pj4zc6ji8axnrlrpnx54i8km0gcd111lkxzjzfzks")))

(define-public crate-repeating_future-0.1 (crate (name "repeating_future") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 2)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^1.0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.10") (default-features #t) (kind 2)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rumqttc") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("rt-multi-thread"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0l8q6v87xnfzn76mpxmw7q9ggh24cqbgsxhw3alpn5cxc0xhzipz") (features (quote (("streams" "rumqttc" "tokio" "anyhow" "pin-project") ("default" "streams"))))))

(define-public crate-repeatrrrr-0.1 (crate (name "repeatrrrr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0wq1pflhaxdyzqa423ph8zszdqnqga49j6xzl2v17ar2cia69rfc")))

(define-public crate-repent-0.1 (crate (name "repent") (vers "0.1.0") (hash "05kp9jvryg8wycsv5sxcv9w93wv3v206yzwcrbfb8g1j4cc8czaw")))

(define-public crate-repertory-0.1 (crate (name "repertory") (vers "0.1.0") (hash "1821iiz557asjm4qf6zwsnndaxywvn27fdal2m4zxhh8f88c0jf5") (yanked #t)))

(define-public crate-repertory-0.0.0 (crate (name "repertory") (vers "0.0.0") (hash "03nbpb9jsi7v5ar41cilg4pilyibd2ghhv6i5paajw0zbn7qx2qa")))

(define-public crate-repetend_len-0.1 (crate (name "repetend_len") (vers "0.1.0") (deps (list (crate-dep (name "gcd") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "1l9ww24hblvqj5v0bajw2gmvyq207s83avbm6a7m1mbpwgqalbxb") (yanked #t)))

(define-public crate-repetend_len-0.2 (crate (name "repetend_len") (vers "0.2.0") (deps (list (crate-dep (name "gcd") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "1sms3lc46srajm5p2mqr98l10xi80qzxahgzzyqsssqdvfimgfcs")))

