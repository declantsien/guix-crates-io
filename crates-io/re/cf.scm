(define-module (crates-io re cf) #:use-module (crates-io))

(define-public crate-recfiles-0.1 (crate (name "recfiles") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19mrnl6fcx4x456ca12141j9dj65ylv78gh1dpkshm4443xy6xln")))

