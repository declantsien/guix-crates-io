(define-module (crates-io re im) #:use-module (crates-io))

(define-public crate-reim-0.0.0 (crate (name "reim") (vers "0.0.0") (hash "00j58qqmn1gh3hxnm3bxq5g32w629jlplrf2h08yfpjfjwadzvkv")))

(define-public crate-reim-0.0.1 (crate (name "reim") (vers "0.0.1") (hash "1zc5bpd2811nir9ppbq4i3pqam9wkcn9acszb50ma8g07nzjy219")))

(define-public crate-reim-0.0.2 (crate (name "reim") (vers "0.0.2") (hash "1wjbffnvp2jfsnpi7j4q9xs6x1dd1c39pw333bzzmh5xq90q3m8i")))

(define-public crate-reim-0.0.3 (crate (name "reim") (vers "0.0.3") (hash "11k12vn56la4rfgxm01s3n2sr1z71l7dvansldcj90xx5iprlbxy")))

(define-public crate-reim-0.0.4 (crate (name "reim") (vers "0.0.4") (hash "07z2kxbi6wi6wvvdsa5w2idxwzr9i4qa78qida93rph0rds8h96z")))

(define-public crate-reim-0.0.5 (crate (name "reim") (vers "0.0.5") (hash "0wvf8h1ykh7sgf3sa1i67p5x95fwhxx4nyi4hx2v3v8hpkqr0xd5")))

(define-public crate-reim-0.0.6 (crate (name "reim") (vers "0.0.6") (hash "1slq6fi2lh8dw1d82s36ammvbmk2aph8dk8pnhlvr1ng7nchl5y4")))

(define-public crate-reim-0.0.7 (crate (name "reim") (vers "0.0.7") (hash "1l5w5dvgzc2yqkpxyvrzx1mgz56dwx9hyr5cpdyj90i3h1nzsj17")))

(define-public crate-reim-0.0.8 (crate (name "reim") (vers "0.0.8") (deps (list (crate-dep (name "disp") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0") (default-features #t) (kind 0)))) (hash "1nbxxdqv455l3fxg06p3nl3n1bgl0zn7dsl8hc4kkrb1m39bk6zz")))

(define-public crate-reimu-0.0.0 (crate (name "reimu") (vers "0.0.0") (hash "0r123iimr6c6q7l8g1x05ga7yma7a4p4v52pc6546zinz5v3xf3y")))

