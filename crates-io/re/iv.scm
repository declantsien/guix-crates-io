(define-module (crates-io re iv) #:use-module (crates-io))

(define-public crate-reivilibre_fork_cylon-0.2 (crate (name "reivilibre_fork_cylon") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("async_futures"))) (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3") (features (quote ("io"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1vmqnk1zxq5i11l2dkg2yh0c0dhdxsp4nhh3x7h92gcn63hdhbfm") (features (quote (("crawl-delay"))))))

