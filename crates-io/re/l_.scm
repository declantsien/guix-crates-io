(define-module (crates-io re l_) #:use-module (crates-io))

(define-public crate-rel_alloc-0.0.0 (crate (name "rel_alloc") (vers "0.0.0") (hash "19ywyjr0ngdcp4rxvnbhlimfbm5mirhfb6l4ifkqqp88hbna0jni")))

(define-public crate-rel_alloc_derive-0.0.0 (crate (name "rel_alloc_derive") (vers "0.0.0") (hash "0l94ba7jfqv3j8svj1n7cahnj8n98ymv547dkpkl8cp3hg3j932v")))

(define-public crate-rel_core-0.0.0 (crate (name "rel_core") (vers "0.0.0") (hash "0k9fg6ddx3svg6a6v5k787zan3k7v1b38iqxnbr834lj718ywqwz")))

(define-public crate-rel_core_derive-0.0.0 (crate (name "rel_core_derive") (vers "0.0.0") (hash "04sa9l1a1h865c5gkrgy7x716iv623rj3y0qcghikx0ks7j0gzwp")))

(define-public crate-rel_std-0.0.0 (crate (name "rel_std") (vers "0.0.0") (hash "187zyjyrilykszagdw80sm973g3bl3zg1nxy9zv4z2rx0c5qs6df")))

