(define-module (crates-io re us) #:use-module (crates-io))

(define-public crate-reusable-0.1 (crate (name "reusable") (vers "0.1.0") (deps (list (crate-dep (name "macro_state") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r1pyamw23f587gydmw6ia0n7rv0ajp2awisjc6xr4qx26l5d1hp")))

(define-public crate-reusable-box-future-0.1 (crate (name "reusable-box-future") (vers "0.1.0") (hash "1w8bsszx3axs8wgxg4rv9ifhf9vcn0hfvgyzgl54bpk04i8i0h8c")))

(define-public crate-reusable-box-future-0.2 (crate (name "reusable-box-future") (vers "0.2.0") (deps (list (crate-dep (name "futures-executor") (req "^0.3.13") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1fcv62q81x2ahb9ccqcaq24d608acrsvfrwkpmgxipgv476n23hy")))

(define-public crate-reusable-fmt-0.1 (crate (name "reusable-fmt") (vers "0.1.0") (deps (list (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)))) (hash "11s06cw9imd3a1v81kra4jnxfayafp30yda0iwi4kxlp708hrygs")))

(define-public crate-reusable-fmt-0.1 (crate (name "reusable-fmt") (vers "0.1.1") (deps (list (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)))) (hash "1gxpkd326mfpsnr5fix6ak22psnirs11mbklb1vzabcc6m0qi8cl")))

(define-public crate-reusable-fmt-0.1 (crate (name "reusable-fmt") (vers "0.1.2") (deps (list (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)))) (hash "012vixi3dq4294frjdls6fpl9n0f9jkvp56p29r2iaxsml6dgc7h")))

(define-public crate-reusable-fmt-0.2 (crate (name "reusable-fmt") (vers "0.2.0") (deps (list (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)))) (hash "0gf8r7sl04mfn9qh04jf8lj42xny0vxgw31m7dhfp7hryi2061q8") (features (quote (("std") ("default" "std"))))))

(define-public crate-reusable-id-pool-0.1 (crate (name "reusable-id-pool") (vers "0.1.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.6") (optional #t) (default-features #t) (kind 0)))) (hash "0hlpk1qn5aws38ljnlvv31lzizkfl83cah253zgzyimnnzismshb") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:linked-hash-map"))))))

(define-public crate-reusable-id-pool-0.1 (crate (name "reusable-id-pool") (vers "0.1.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.6") (optional #t) (default-features #t) (kind 0)))) (hash "0ikp7g3b637d6x1hs7ndh962z48idma2j2dxrr0705bcprahcrfg") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:linked-hash-map"))))))

(define-public crate-reusable-id-pool-0.1 (crate (name "reusable-id-pool") (vers "0.1.2") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.6") (optional #t) (default-features #t) (kind 0)))) (hash "1p2d781vaw4blpy819w9863iash6qy7wmba1knljyy21vd5iqqlb") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "dep:linked-hash-map"))))))

(define-public crate-reusable-memory-0.1 (crate (name "reusable-memory") (vers "0.1.0") (hash "19374997am6zh0q515mxp1c7x21ss2xf091pq642m7q1nfmivgdv")))

(define-public crate-reusable-memory-0.1 (crate (name "reusable-memory") (vers "0.1.1") (hash "1pdyrpgvraspbnhf24dslssgcvr2mk1jpgpq33pwmr9aqdqvlqkx")))

(define-public crate-reusable-memory-0.2 (crate (name "reusable-memory") (vers "0.2.0") (hash "0dnx8j2g7hh8m29dn86apwiicmalpblibzj3cd1l8aixvf7xgr5a")))

(define-public crate-reusable-memory-0.2 (crate (name "reusable-memory") (vers "0.2.1") (hash "1mzc5l421bign615vnrrzp8cqmc41scyg6jbwqayxm3ngi1xpqw8")))

(define-public crate-reusable-vec-0.1 (crate (name "reusable-vec") (vers "0.1.0") (hash "0i3djh1sfld53fqvf7sh6f64yj7jba7s26vcp3yzg629ck1iq6dx") (yanked #t)))

(define-public crate-reusable-vec-0.1 (crate (name "reusable-vec") (vers "0.1.1") (hash "0ig0dz30lg4dxjq5d22qmj9qnfm25xad27pavgg5dknxyll4s4l1") (yanked #t)))

(define-public crate-reusable-vec-0.1 (crate (name "reusable-vec") (vers "0.1.2") (hash "14v665zg8vvn1q8ax8flv2ddm8kxqbab8vs64zxx9pwlppc2n7cs")))

(define-public crate-reusable_derive-0.1 (crate (name "reusable_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "086srqs09gh3c118bncllbbxahv5phyx8776297iq8z43qnvkk5i")))

(define-public crate-reusable_lexer-0.1 (crate (name "reusable_lexer") (vers "0.1.0") (hash "06i9xk97mblvfvdr1cb6jfa1fpjpmy37msb4cwmijczswiikjh50")))

(define-public crate-reuse-0.1 (crate (name "reuse") (vers "0.1.0-alpha") (hash "075x8rd687fly49iga6hbgzwhfg3q0yrs7ga963ivwfbihl39hd6")))

(define-public crate-reuse-notifications-0.1 (crate (name "reuse-notifications") (vers "0.1.0") (deps (list (crate-dep (name "notify-rust") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1pv3j7gn9ys57c3slxsa3hjv1069929x7sjk4cfrh9kn5wxv7ixg")))

(define-public crate-reuse-notifications-0.1 (crate (name "reuse-notifications") (vers "0.1.1") (deps (list (crate-dep (name "notify-rust") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "126s3vvkh6qarkrfccwr8bqw6k0wyn6xnjgsppm1rx3l3ran2d4b")))

(define-public crate-reustmann-0.1 (crate (name "reustmann") (vers "0.1.0") (deps (list (crate-dep (name "colorify") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j619c2cbhqppxc4lkjmrp5vlpzic8s72pxdy6kwgi9rfy1n3xwq")))

(define-public crate-reustmann-0.1 (crate (name "reustmann") (vers "0.1.1") (deps (list (crate-dep (name "colorify") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^0.2") (default-features #t) (kind 0)))) (hash "06vzhyp4g0l3yqg5iqzb3aplr5hrf8piqsdxjv9sx0aj03n6i7bp")))

(define-public crate-reustmann-0.1 (crate (name "reustmann") (vers "0.1.2") (deps (list (crate-dep (name "colorify") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^0.2") (default-features #t) (kind 0)))) (hash "1la3jgbpm13xzkmcajqs3ijwmygddardvbiczmlykbxr49573yls")))

(define-public crate-reustmann-0.2 (crate (name "reustmann") (vers "0.2.0") (deps (list (crate-dep (name "colorify") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j3vkrg7gs3h4jrfszdifnyy0gsha6jkqv1y826hwlhf2al5kgvl")))

(define-public crate-reustmann-0.2 (crate (name "reustmann") (vers "0.2.1") (deps (list (crate-dep (name "colorify") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lsvrkzcwy6k2r70j1vgxnwh2gx9wrmjvjk3z2640gcxlknm4xac")))

