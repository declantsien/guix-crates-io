(define-module (crates-io re fu) #:use-module (crates-io))

(define-public crate-refuel-0.1 (crate (name "refuel") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1hdw56adpnpy3vpxpvnm9vwr32bcnjjm6mwgyvb8f7w3iwzz8f4w") (yanked #t)))

(define-public crate-refuel-0.1 (crate (name "refuel") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1amp0n99x0q1v5yhdx3vlvn3pf78cg7my4apm06vq53ld1ij4ipz")))

(define-public crate-refuse-0.0.1 (crate (name "refuse") (vers "0.0.1") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "intentional") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kempt") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1h2m1mlvsawr0x2ag1fx6lx0vk9hkxngkjcg4agpn5qrsg1q2l6r") (rust-version "1.73.0")))

(define-public crate-refuse-0.0.2 (crate (name "refuse") (vers "0.0.2") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "intentional") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kempt") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "refuse-macros") (req "=0.0.2") (default-features #t) (kind 0)))) (hash "1bnyd5madwhnqk9bjda2wdzkymzzxh8bshc4164pq28cs7mrwbpd") (rust-version "1.73.0")))

(define-public crate-refuse-0.0.3 (crate (name "refuse") (vers "0.0.3") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "intentional") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "kempt") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "refuse-macros") (req "=0.0.3") (default-features #t) (kind 0)))) (hash "00jynp75838jfhlr6cgzqfa8ai10anf27p234qzbg8vdilh1p8z2") (rust-version "1.73.0")))

(define-public crate-refuse-macros-0.0.2 (crate (name "refuse-macros") (vers "0.0.2") (deps (list (crate-dep (name "attribute-derive") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "manyhow") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (default-features #t) (kind 0)))) (hash "1zg3n2y8cfymkj7i3qpip23f4q1n4jq27m1qbz9jscsfvycjhr6l") (rust-version "1.73.0")))

(define-public crate-refuse-macros-0.0.3 (crate (name "refuse-macros") (vers "0.0.3") (deps (list (crate-dep (name "attribute-derive") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "manyhow") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (default-features #t) (kind 0)))) (hash "0gj7q7ihc717hydm237c0zknzf5vp437gpahs134l7a883mj6hg5") (rust-version "1.73.0")))

(define-public crate-refuse-pool-0.0.2 (crate (name "refuse-pool") (vers "0.0.2") (deps (list (crate-dep (name "ahash") (req "^0.8.11") (features (quote ("runtime-rng"))) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "refuse") (req "=0.0.2") (default-features #t) (kind 0)))) (hash "1x25fw7bvvwg481lqq1kdd5y0h0ha3gb419ls5d3wqais4zrh9zl") (rust-version "1.73.0")))

(define-public crate-refuse-pool-0.0.3 (crate (name "refuse-pool") (vers "0.0.3") (deps (list (crate-dep (name "ahash") (req "^0.8.11") (features (quote ("runtime-rng"))) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "refuse") (req "=0.0.3") (default-features #t) (kind 0)))) (hash "1birp98c8062bn07dvrhnlas49rv8kbwhraxx6103p7qcadfs88s") (rust-version "1.73.0")))

(define-public crate-refute-0.1 (crate (name "refute") (vers "0.1.0") (hash "1knj6c8vp6d1nldacazwn9nv8s9yy60zyj6bs5bvfy3cjgnxfrwp")))

(define-public crate-refute-1 (crate (name "refute") (vers "1.0.0") (hash "04wdy6xbv54z96ncdbjmgark2k1qp5yn0v440bgibfi4hlxfbizy")))

(define-public crate-refute-1 (crate (name "refute") (vers "1.0.1") (hash "0y01vc3yj29bjzlph0yx745y9m4g42injd1cdkxmk437vxm8g7c2")))

