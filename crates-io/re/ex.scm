(define-module (crates-io re ex) #:use-module (crates-io))

(define-public crate-reexport-1 (crate (name "reexport") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00jwzb3wv2l4k8dfww382890nmjrk22i01nf3lhb1y2zd5sfw210")))

(define-public crate-reexport-proc-macro-0.1 (crate (name "reexport-proc-macro") (vers "0.1.0") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "144zdd8zhdm40drsqghz3ywkxf57rxz7kslm41fx90578n2wwa9c")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.0") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "1lsmx3612wygdbqr9q0pqdbskdkh6hlb9fr9y2dm7c5yfrsg1kfs")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.1") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "1vcbbwz228hpcl621n4rvdrrsz22arwjr4ggmigf90d7byc71swb")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.2") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "1w033kxccv5d0cww66wamrv8kybx3c7xy6dc5im0yvrqxfan5029")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.3") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "1iggicmaagl2km0js2x79g4c4x2qk5sl88c2ll3j8fjxrs3m18mf")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.4") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "0laqn44i6gb75xl0153hv2ia62vvjif7ifrm7y2whlpz7i9ayg9c")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.5") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "091kwqzh9n94xi7s0zrhcql976x4mlkls2z3k2pmp8gdf0vyd3s3")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.6") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "0xgmn6gg0k0mrkhzk3jky0rkibj5rind1dk8siij85ckyqbw83mr")))

(define-public crate-reexport-proc-macro-1 (crate (name "reexport-proc-macro") (vers "1.0.7") (deps (list (crate-dep (name "enum-map") (req "^0.2.24") (default-features #t) (kind 2)) (crate-dep (name "enum-map-derive") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.36") (default-features #t) (kind 2)))) (hash "1x67w18dvr0hifz4vp0z24gzkapq144q25xhv1184lixd2an3zf6")))

