(define-module (crates-io re sn) #:use-module (crates-io))

(define-public crate-resname-0.1 (crate (name "resname") (vers "0.1.0") (hash "0sgcnmpsm14bv87h8njxna9iqvr457ss4ashxxnicq1wb83igsn7")))

(define-public crate-resname-0.1 (crate (name "resname") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)))) (hash "1q7sakfmdbszdxc5ch5njm8xw6jfradhz373zzmjcmv89kypcj7w")))

(define-public crate-resname-0.1 (crate (name "resname") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)))) (hash "1nnzy32lyxlqbl18h079hzs25k51rsq9y40hkg7i6bsidi7h167k")))

