(define-module (crates-io re mu) #:use-module (crates-io))

(define-public crate-remu-1 (crate (name "remu") (vers "1.0.0") (hash "0d1jzfbapwyj0czmqk6kc9j0zfh9j1g48l899i3yb22alivm78r6")))

(define-public crate-remu-1 (crate (name "remu") (vers "1.0.1") (hash "04gr7w7v0zfm78yzz20ms3axwm9hlzxf08qgfd8y0kbf1a6r67b8")))

(define-public crate-remus-0.0.0 (crate (name "remus") (vers "0.0.0") (hash "0n64j3nxlargqydk5xpyvh4bklgi0ljk8wjdkg75waqwkmvs036l")))

(define-public crate-remutex-0.1 (crate (name "remutex") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0lha8gz589vqxx17w00bfkfh8yr9m3hj9hbwy0x2d99f0fb69hj3")))

(define-public crate-remutex-0.1 (crate (name "remutex") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0didiri8v2ypckp1j78cf883j3pglrjpclk79nvpzcmk4hdrr3rg")))

(define-public crate-remux-0.0.0 (crate (name "remux") (vers "0.0.0") (hash "1z9zm36pyqikrxak6a9pm3dmpinqd24fn9dfkc7hwcgzlk54zy99")))

(define-public crate-remux-0.8 (crate (name "remux") (vers "0.8.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "nohash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("tcp" "rt-threaded" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-util") (req "^0.3") (features (quote ("compat"))) (default-features #t) (kind 2)))) (hash "1i51gpv1n6wlyj3y3hsdmc8mcbgqzhv21v64yfirwmv3cmd7bbmb")))

(define-public crate-remux-0.9 (crate (name "remux") (vers "0.9.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "constrained-connection") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "nohash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("net" "rt-multi-thread" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("compat"))) (default-features #t) (kind 2)))) (hash "14zf9vpz1wnhnk6aj43xhdhgrali65r7711iyskcjn505205j6i0")))

