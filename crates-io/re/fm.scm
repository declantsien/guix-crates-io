(define-module (crates-io re fm) #:use-module (crates-io))

(define-public crate-refmove-0.1 (crate (name "refmove") (vers "0.1.0") (hash "1fdcvz559n92180vbv6zy6rlcgmgb2g9zln9q9z04fi7wfxnn8jb") (features (quote (("std") ("default" "std"))))))

(define-public crate-refmove-0.1 (crate (name "refmove") (vers "0.1.1") (hash "0c9j4gcw4xb5yz34vaaqb72j132qqir3xz2sj98z9xd70skrdskv") (features (quote (("std") ("default" "std"))))))

(define-public crate-refmove-0.1 (crate (name "refmove") (vers "0.1.2") (hash "0vc152z04xmwjcdyqgarv3bsn2bnipymd4shdnjmafag628m7858") (features (quote (("std") ("default" "std"))))))

(define-public crate-refmove-0.1 (crate (name "refmove") (vers "0.1.3") (hash "1q0nb4lif2crxn6lmhf98ps9lb1ihsanmhkkffas940c2q0c3q5g") (features (quote (("std") ("default" "std"))))))

