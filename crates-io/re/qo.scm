(define-module (crates-io re qo) #:use-module (crates-io))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "06fw3jy24y26x39fv5a0bzmd71rhmncx1dssa56p5aivwv457szh")))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1fan1vqd6fqjwnpv3kzxmafl89dh5j55jv3z0rdrnrqy8j01bgq1")))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "19kkqbrl10r2vbmqis228iyi7z4fm4g5vi8f220prfv5wx9fkzg4")))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "08wvxs2ygfnxq70chy9wm7aliabmhwii52fkwjdwrprfwzfn0b5z")))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1vad17gkwhr4mr64wg0wdgmpfnc7f2h52hngxrnxfk2sd7yq19xw")))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0lhp2nbri6qs28mwlb2p2jymarklddirv00689lz3lpkfgz303nb")))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "15xlbmcg50xs3znma6yhf4hl8i4n3vr1sd3mkzag842ldzxphg3m")))

(define-public crate-reqores-0.1 (crate (name "reqores") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0v0z2a4mvrx9zmfvscc7nkw3l2q33314nk1gs6yhlca8n08w5jky")))

(define-public crate-reqores-client-surf-0.1 (crate (name "reqores-client-surf") (vers "0.1.0") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2") (default-features #t) (kind 0)))) (hash "00k0x1g5m3sdm9baf6v1f5jr1q46a2n2kwbrfxps592g5sj488sa")))

(define-public crate-reqores-client-surf-0.1 (crate (name "reqores-client-surf") (vers "0.1.1") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2") (default-features #t) (kind 0)))) (hash "0hbxrn0zk59qrwpzmw44m229hswal072x6wsp2lw12cr98aagyga")))

(define-public crate-reqores-client-surf-0.1 (crate (name "reqores-client-surf") (vers "0.1.2") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2") (default-features #t) (kind 0)))) (hash "1kx8iyfs7y2c0834ppsybpwzgkjkbbn8dg77924gy8h7lvdg6mcz")))

(define-public crate-reqores-client-surf-0.1 (crate (name "reqores-client-surf") (vers "0.1.3") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2") (kind 0)))) (hash "0p37pigijf551sva601d20sbqp12a2jipa87ky57zi0r0vr1rmi6") (features (quote (("wasm-client" "surf/wasm-client") ("middleware-logger" "surf/middleware-logger") ("hyper-client" "surf/hyper-client") ("h1-client-rustls" "surf/h1-client-rustls") ("h1-client" "surf/h1-client") ("encoding" "surf/encoding") ("default" "curl-client" "middleware-logger" "encoding") ("curl-client" "surf/curl-client"))))))

(define-public crate-reqores-universal-cf-worker-0.1 (crate (name "reqores-universal-cf-worker") (vers "0.1.0") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "worker") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0b7qyhgw787c7n2yisfy4q5i818y0np83lb2si2lrhshyjwbp3b8") (features (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1 (crate (name "reqores-universal-cf-worker") (vers "0.1.1") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "worker") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0h4ra8ai0imp73k8vs45jf2c2wr4mwhmidcya09icwis9whqyw59") (features (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1 (crate (name "reqores-universal-cf-worker") (vers "0.1.2") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "worker") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "01as2gbdi03lpa1bf7nh7d1vl1vc1b6kbd09kmdjv9n6f4pq36n6") (features (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1 (crate (name "reqores-universal-cf-worker") (vers "0.1.3") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "worker") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "0pmd8azpvy7rkfrwwgn812b5800b36d8755889wcxlvhqwh96qx4") (features (quote (("server") ("default" "client" "server") ("client"))))))

(define-public crate-reqores-universal-cf-worker-0.1 (crate (name "reqores-universal-cf-worker") (vers "0.1.4") (deps (list (crate-dep (name "reqores") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "worker") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "12lfz8washh1xig0991ziq5hl0imxv6xxzac8cvyn4aa2fpag05p") (features (quote (("server") ("default" "client" "server") ("client"))))))

