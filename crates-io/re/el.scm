(define-module (crates-io re el) #:use-module (crates-io))

(define-public crate-reel-0.0.0 (crate (name "reel") (vers "0.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "reef") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0ggg8xvx5559vrg1gi78sjbaz300jgjjxgllp4m4qji0hz7a412l")))

(define-public crate-reelpath-1 (crate (name "reelpath") (vers "1.0.0") (hash "14aww8c2zvralfn41n91qfzmwk7g4xfsxwsmkkrs83gaw1bsq9y5")))

(define-public crate-reelpath-1 (crate (name "reelpath") (vers "1.0.1") (hash "1aqk5zpsqqvjh92crfq6py0f6cd0k3w163a9f9f16yx3br1j5i0g")))

(define-public crate-reelpath-1 (crate (name "reelpath") (vers "1.0.2") (hash "1dp7x751nd60kxqgbqnsn3m9cfvq3iqq6qxjhyg6w6vygckqcknr")))

(define-public crate-reelpath-1 (crate (name "reelpath") (vers "1.1.0") (hash "1ksw7g3y3hjfcnl7l0m5vpgi2f4nlfdfxp9jwl3hyq56dhw9jlxb")))

(define-public crate-reelpath-1 (crate (name "reelpath") (vers "1.2.0") (hash "0d6xa265ik6ngs5bklbjqw9wdagv1fzbxd7bj0zkc2bqfmpxjjzj")))

(define-public crate-reelpath-1 (crate (name "reelpath") (vers "1.2.1") (hash "0mjqa9pmgl318ydyw847vmvqszh7lpnl86ajcp1a0pxjf5xlrsfr")))

(define-public crate-reelpath-1 (crate (name "reelpath") (vers "1.2.2") (hash "13rd8263ymj718m1zbk5wq2c7fs1a27jqdklbyv6kk1jzk0zp7bq")))

