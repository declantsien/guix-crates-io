(define-module (crates-io re pc) #:use-module (crates-io))

(define-public crate-repc-0.1 (crate (name "repc") (vers "0.1.0") (deps (list (crate-dep (name "repc-impl") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "1121dlb9k4kdkhxvin0w7fnadx7xmw81d5gr3zkwdzp71gyj2a60")))

(define-public crate-repc-0.1 (crate (name "repc") (vers "0.1.1") (deps (list (crate-dep (name "repc-impl") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "1rlqzrf4yzd1v5y7xh3sbcxxm58vn29l13x4ywqb66z8s7hrfm7x")))

(define-public crate-repc-impl-0.1 (crate (name "repc-impl") (vers "0.1.0") (hash "038zk8fwdad8kh6hpyrsjlrrwg5y4x3ajcz2gmq92659k6hh8n2j")))

(define-public crate-repc-impl-0.1 (crate (name "repc-impl") (vers "0.1.1") (hash "0s431dgymmpvppch9qfyxhzc41d94sk44v0p07lc4d8kb9yhn4cy")))

(define-public crate-repcon-0.1 (crate (name "repcon") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("multipart"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0asy3hymlch3cfddc2sqx6rkd26gjmfr1y6l48rykrlnn64gmz1a")))

(define-public crate-repcon-0.1 (crate (name "repcon") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("multipart"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0iib0n2863zijkahh347ikdcgklb3mm5jyn68vswk0mjndjzlsgv")))

(define-public crate-repcon-0.1 (crate (name "repcon") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("multipart"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qn9zfnazvidblp6bb7rlprdhjjwfl5pn25byghqkwi6mxkvbzwq")))

(define-public crate-repcon-0.1 (crate (name "repcon") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("multipart"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fm72fac0zc4hmhlxqfmphy4pvacnc2xw5h84vcz4194qz3cfvnf")))

