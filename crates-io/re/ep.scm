(define-module (crates-io re ep) #:use-module (crates-io))

(define-public crate-reep-0.1 (crate (name "reep") (vers "0.1.1") (deps (list (crate-dep (name "bit-set") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "router") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2") (default-features #t) (kind 0)))) (hash "18fcbwir174q0ykkxb6irrsfxdkr7w2vm4f8wiwxhhdnb20nlc2b")))

(define-public crate-reep-0.2 (crate (name "reep") (vers "0.2.0") (deps (list (crate-dep (name "bit-set") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "router") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "1.*") (default-features #t) (kind 0)))) (hash "07y1aa636i8x6c46qraizwy4dihyq9vgjzgd19ars5gjj2qdg17p")))

(define-public crate-reep-bodyparser-rustcdecodable-0.1 (crate (name "reep-bodyparser-rustcdecodable") (vers "0.1.0") (deps (list (crate-dep (name "bodyparser") (req "*") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reep") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1pvikcnngxh2p6znacmlx3zdvg8ndjg64xc3bdmzj6cwxqpdqyqz")))

(define-public crate-reep-bodyparser-rustcdecodable-0.2 (crate (name "reep-bodyparser-rustcdecodable") (vers "0.2.0") (deps (list (crate-dep (name "bodyparser") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reep") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)))) (hash "00myb29f414kqpgqf1six4ns5gfy1yban32acpmcaz9a3ajvmk19")))

(define-public crate-reep-id-string-0.1 (crate (name "reep-id-string") (vers "0.1.0") (deps (list (crate-dep (name "iron") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reep") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1blw8rgzivlfg9r1g2h7ly4qxf3mgyldvksp316dvcvmzmc0gqf5") (features (quote (("serde-support" "serde") ("rustc-serialize-support" "rustc-serialize") ("default"))))))

(define-public crate-reep-id-string-0.2 (crate (name "reep-id-string") (vers "0.2.0") (deps (list (crate-dep (name "iron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reep") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0gqj9dqwxlmpa6sg7mky01ir5z9dxxyfi899vqrb5wb7w4dxnfj1") (features (quote (("serde-support" "serde") ("rustc-serialize-support" "rustc-serialize") ("default"))))))

(define-public crate-reep-optionparser-urlencoded-0.1 (crate (name "reep-optionparser-urlencoded") (vers "0.1.0") (deps (list (crate-dep (name "iron") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reep") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "urlencoded") (req "*") (default-features #t) (kind 0)))) (hash "0cfjii1baj6aq34c518clx9y9pzks2cdyj6g558gxlh0g97y3giz")))

(define-public crate-reep-optionparser-urlencoded-0.2 (crate (name "reep-optionparser-urlencoded") (vers "0.2.0") (deps (list (crate-dep (name "iron") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reep") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "urlencoded") (req "^0.4") (default-features #t) (kind 0)))) (hash "18qxjs4p50w762kbp53bwjyzllq4izl48yynkl0df6xqnjkvfys4")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1b02asmr25c0gdgi9i56pb3w54awqk8v97qaw982cgmal6kqq1y3")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0klfb8wkgvac4pd96j6ysb5wi54vf6wr014r8mx4vhq2vkbcs3fp")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17yc6ijqwfhj2hdhljf8zwnhdb2vc8y9dw0aslkgrzc2nrfk2gzs")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19msbzplbknl4swzqb67niiqjh82k6im5pacrhnz8rv5wxslf6ah")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mc4r3l0vv9icppv0h8xnya7nrsjxa9yxpvs1whxjpl0mgcrwm01")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1z92adrdvzhgwfya3ay9bq52953sjxancsfjlrqg1lr73y1vmhg9")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0324s8218xaxqg8g3990x8q9p90vm97dk6a29b7kzl6j2lbcfv4w")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1959zhqq29qsl1rfflkln5a55nw4l4pxyjv9j05jmgz9gk5hkvg2")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1n7blfc35v15ibzry8xq4m3zg0v1yqjf7hsw9zr9vy7y1dxc6fs6")))

(define-public crate-reepal-0.1 (crate (name "reepal") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qhcb1dr97javjqi7r2rlixg4a23parlid1ivb39743w1vfcaadh")))

(define-public crate-reepal-0.2 (crate (name "reepal") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.20") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dd8rm66v6gh1398gj5y3bdym0m28a8wjxiy398c403yi3k2kh5l")))

