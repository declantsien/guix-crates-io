(define-module (crates-io cn cs) #:use-module (crates-io))

(define-public crate-cncs-sm2-gen-0.1 (crate (name "cncs-sm2-gen") (vers "0.1.0") (deps (list (crate-dep (name "cncs-sm2-kit") (req "^0.1") (kind 0)))) (hash "14b8av5iavb0zr1qlk1ccffkiyjqpv3687wk90kssiw9dc831yc1")))

(define-public crate-cncs-sm2-kit-0.1 (crate (name "cncs-sm2-kit") (vers "0.1.0") (deps (list (crate-dep (name "gmsm") (req "^0.1") (kind 0)) (crate-dep (name "libsm") (req "^0.4") (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "05p5bpaxq1y11g2hlc1c41gn852kxbsahyvnqyps4gyzl24m9ljp")))

(define-public crate-cncs-sm2-kit-0.1 (crate (name "cncs-sm2-kit") (vers "0.1.1") (deps (list (crate-dep (name "gmsm") (req "^0.1") (kind 0)) (crate-dep (name "libsm") (req "^0.4") (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "038bv0ama8w5gxy6kb878q8j65955cnxjf2ym7i2b3mbxyp59m9x")))

(define-public crate-cncs-sm2-kit-0.1 (crate (name "cncs-sm2-kit") (vers "0.1.2") (deps (list (crate-dep (name "gmsm") (req "^0.1") (kind 0)) (crate-dep (name "libsm") (req "^0.4") (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "16y8m78981xdcz2pc3csm9203hzmq7pm5brxqc439rv44vairggk")))

(define-public crate-cncs-sm2-kit-0.1 (crate (name "cncs-sm2-kit") (vers "0.1.3") (deps (list (crate-dep (name "gmsm") (req "^0.1") (kind 0)) (crate-dep (name "hex-simd") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "libsm") (req "^0.4") (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0yid912k36vw4ldrwhg0y1hi0xx1wrihn63ds6c4c2jj01n21r4n")))

(define-public crate-cncs-sm2-php-0.1 (crate (name "cncs-sm2-php") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13") (features (quote ("std"))) (kind 0)) (crate-dep (name "cncs-sm2-kit") (req "^0.1") (kind 0)) (crate-dep (name "ext-php-rs") (req "^0.7") (kind 0)))) (hash "03npfyrj86i7kwsq0f3j113d2asfp3wnz41j5xzhbmz618rak1m5")))

(define-public crate-cncs-sm2-php-0.1 (crate (name "cncs-sm2-php") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13") (features (quote ("std"))) (kind 0)) (crate-dep (name "cncs-sm2-kit") (req "^0.1") (kind 0)) (crate-dep (name "ext-php-rs") (req "^0.7") (kind 0)))) (hash "04jb9xgm7ndjvfcl6xwkl7pn165v234rh8paicclga79fa0d5g1l")))

(define-public crate-cncs-sm2-php-0.1 (crate (name "cncs-sm2-php") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.13") (features (quote ("std"))) (kind 0)) (crate-dep (name "cncs-sm2-kit") (req "^0.1") (kind 0)) (crate-dep (name "ext-php-rs") (req "^0.7") (kind 0)))) (hash "1xdbnhilcmjdzrd9bprcdp947ms677xrnz2pcsgrmr4rhzkk6r8i")))

