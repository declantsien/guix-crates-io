(define-module (crates-io cn fg) #:use-module (crates-io))

(define-public crate-cnfgen-0.2 (crate (name "cnfgen") (vers "0.2.1") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "itoap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qbjdkz277fja9x0y1lvi98i231ym6hlcccxby5fi303a9w5d4w0")))

(define-public crate-cnfgen-0.2 (crate (name "cnfgen") (vers "0.2.2") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "itoap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k60nj9ayipzj2bxw6xamrr48lgx512k0sk6z096xccc562q6yg3")))

(define-public crate-cnfgen-0.3 (crate (name "cnfgen") (vers "0.3.0") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "itoap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hqvhzbf14z2pav6l98psnsqlnlwrhiqhgvjjz4bdx8biarcd84l")))

(define-public crate-cnfgen-0.3 (crate (name "cnfgen") (vers "0.3.1") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "itoap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q39mdz84fdivw675afqv39gff2jfbgkhwy2qfy1c3hnv1r9h8ad")))

(define-public crate-cnfgen-0.3 (crate (name "cnfgen") (vers "0.3.2") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "itoap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b0bxpanxjdap4kp8savaxzkia3n7ag3l72xcr805s0z5bykwrbf")))

(define-public crate-cnfgen-0.3 (crate (name "cnfgen") (vers "0.3.3") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "itoap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c47rh90pyyyzcbdfavk0zgb8knd6qhvdh17y3n5zbl1ybgx2vqq")))

(define-public crate-cnfgen-nand-opt-0.1 (crate (name "cnfgen-nand-opt") (vers "0.1.0") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "exec-sat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "00j0ba8fppm7dlz57k9xn5jwd2wydavjdip650syd4bfcgsgixl8")))

(define-public crate-cnfgen-nand-opt-0.1 (crate (name "cnfgen-nand-opt") (vers "0.1.1") (deps (list (crate-dep (name "cnfgen") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "exec-sat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0phh40warnyzd0sfqxcqi7a738ccmfldqmf9ybpfndlsfzy0mc7m")))

