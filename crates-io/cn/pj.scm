(define-module (crates-io cn pj) #:use-module (crates-io))

(define-public crate-cnpj-0.0.1 (crate (name "cnpj") (vers "0.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (optional #t) (default-features #t) (kind 0)))) (hash "1669i76k8hmr844gbdfmffrdwmk1h2wlvkp6ac54rq8pxpbxc5ad") (features (quote (("default"))))))

(define-public crate-cnpj-0.1 (crate (name "cnpj") (vers "0.1.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1xrvaa9dw1fv0lwjbdnvixln7jlz0acbsjrccg07cgkjwdd8xk56") (features (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cnpj-0.2 (crate (name "cnpj") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "06k4z22xr8fxgl5xa73vyx0dcbkankvzmlgccss1s3v59gvcpxss") (features (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cnpj-0.2 (crate (name "cnpj") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("std" "std_rng" "small_rng"))) (kind 2)))) (hash "05ddx3qixqbi8wp84gnicfz4cb9kjw42jmgp45wzk60nch2vmgh7") (features (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cnpj-util-0.1 (crate (name "cnpj-util") (vers "0.1.0") (hash "0366gyn7almgh6yqwg5binmkgqiwlnshbgji3pd4ck0mlv58mywb")))

(define-public crate-cnpj-util-0.1 (crate (name "cnpj-util") (vers "0.1.1") (hash "119iszx990gplsjwb4g138hpn4w510m0jiirdn54vq80m51cl4v6")))

(define-public crate-cnpj-util-0.1 (crate (name "cnpj-util") (vers "0.1.2") (hash "0bijb2iwr8a3a9ky2incca0fcskxr30jlzjjdk1d8cj845cwkxk1")))

(define-public crate-cnpj-util-0.1 (crate (name "cnpj-util") (vers "0.1.3") (hash "1ql4gbvrw3vxxh5qmdgbbgvj4v1jl9lnlsj3gkz2dldwa3bqikh2")))

(define-public crate-cnpj-util-0.1 (crate (name "cnpj-util") (vers "0.1.4") (hash "1jdpc5d5wx0641a8n5r334979zp69wps43gicy9lghxqvm8k7vjm")))

