(define-module (crates-io cn tk) #:use-module (crates-io))

(define-public crate-cntk-0.0.1 (crate (name "cntk") (vers "0.0.1") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0nkgl2wb41757ir0jig3czdjpflnr14n4fp93iqnxvdl6ck7r8mw")))

(define-public crate-cntk-0.0.2 (crate (name "cntk") (vers "0.0.2") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "18w4df2m0078cm160qwikdhrmvdca2js5044mq56a79iks269av7")))

(define-public crate-cntk-0.0.3 (crate (name "cntk") (vers "0.0.3") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "04f0g7avn4xzn1kdwzmdr3np6zxjdfy1p920av2clxfahyji2pbq")))

(define-public crate-cntk-0.0.4 (crate (name "cntk") (vers "0.0.4") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1flr8rwnzp8sc1i02qv8rj64fwnyklnprxbs0n0mixb8n0ls4b1k")))

(define-public crate-cntk-0.1 (crate (name "cntk") (vers "0.1.0") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "mnist") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "05sxy246w0sm7fivnx6yqphpkfgysk7y3aymbvaxb14ajp22d26i")))

(define-public crate-cntk-0.1 (crate (name "cntk") (vers "0.1.1") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "mnist") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "1hyl3d1c3y4sxmxc57wy8cnwf1sj409phmk0w6g81q2d7vzkhnah")))

(define-public crate-cntk-0.2 (crate (name "cntk") (vers "0.2.0") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "mnist") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "0hqg4fp6697z1fd6c9a02azxhpl8l7czqd8z8gdsmgh0bw0zki9s")))

(define-public crate-cntk-0.2 (crate (name "cntk") (vers "0.2.1") (deps (list (crate-dep (name "cpp") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.3.2") (default-features #t) (kind 1)) (crate-dep (name "mnist") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "1iiqc4h4qfzfx0rssicj85aclkbwk4fm3739jvilnjf6yi1pc624")))

