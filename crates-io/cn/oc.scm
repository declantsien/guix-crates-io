(define-module (crates-io cn oc) #:use-module (crates-io))

(define-public crate-cnocr_rs-0.1 (crate (name "cnocr_rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "opencv") (req "^0.70.0") (default-features #t) (kind 0)))) (hash "0p7axibzb33ba87mx4kd299nyg3q9xq4lr1byg68jgl58412gxvz")))

(define-public crate-cnocr_rs-0.1 (crate (name "cnocr_rs") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "opencv") (req "^0.70.0") (default-features #t) (kind 0)))) (hash "0a62nqsz1rc47sznbfjzm62ws49xvsckrn5f0fllsn3kz9dvc3f9")))

(define-public crate-cnocr_rs-0.1 (crate (name "cnocr_rs") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "opencv") (req "^0.70.0") (default-features #t) (kind 0)))) (hash "094ir8icqvajpaxjm98prlqqqxgpw7fprxah3fd6hayfsh0wkgwj")))

(define-public crate-cnocr_rs-0.1 (crate (name "cnocr_rs") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "opencv") (req "^0.70.0") (default-features #t) (kind 0)))) (hash "09601p2n0n4aspys35i453ab5mckcs8k75flhnia0f5kf9cg1lbp")))

(define-public crate-cnocr_rs-0.1 (crate (name "cnocr_rs") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "opencv") (req "^0.70.0") (default-features #t) (kind 0)))) (hash "055lskpcf18x29cr2p5b55bqlhlrnc9kf1yvjp5llzpvqd1l7myi")))

