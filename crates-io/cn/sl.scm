(define-module (crates-io cn sl) #:use-module (crates-io))

(define-public crate-cnsl-0.1 (crate (name "cnsl") (vers "0.1.0") (hash "044yyw6a5jl5ag8jdzx8427bpl17zr570zpbah5m6282psnivxf0") (yanked #t) (rust-version "1.60.0")))

(define-public crate-cnsl-0.1 (crate (name "cnsl") (vers "0.1.1") (hash "14x9k6rbfjkffkq0ac0gjmv5wdgx3a8ihx2rcrj7vm4q5c8h3c1v") (rust-version "1.60.0")))

(define-public crate-cnsl-0.1 (crate (name "cnsl") (vers "0.1.2") (hash "1xm0qmbagfv8dapdaqc4kpg419xja3mhqcdyj69qppgari7sgpwa") (rust-version "1.60.0")))

(define-public crate-cnsl-0.1 (crate (name "cnsl") (vers "0.1.3") (hash "0ylqiyn4yajn8bcg8ysy0di2pbk7wfk6yy0rrf08g0k817kjimwn") (rust-version "1.60.0")))

