(define-module (crates-io cn pr) #:use-module (crates-io))

(define-public crate-cnproc-0.2 (crate (name "cnproc") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p7dwzhy8fm3sfbbrf76ha80gbb29asf9vh1lfyyg2na4m7da3p8")))

