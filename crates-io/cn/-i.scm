(define-module (crates-io cn #{-i}#) #:use-module (crates-io))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1cjcnk6xlw0sk8gv5ldd2xvcpi4y0in8vcpbr4zvk2s0k1gcrjjw")))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1mlfqw1bmxspk3d49xic4p6m9phasnaib66w5wn6j9m7i2nilv78")))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1d4bj8m4l77f4aalbzg5ypb9gx1g9g1xg15va6hfc514jamyjir4")))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "179jcdffzvpih9m6ilf3rlc45dayizv22h0gkf6fn49c02msm709") (features (quote (("region" "lazy_static") ("default" "region"))))))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1sgkdsjin70gss6bvnndhcbgr7gaq7ybbbs1ackadlxim30lp1wp") (features (quote (("region") ("default" "region"))))))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xzd7vfbbdsgjl4kc2dxqzm3rnj10q12khjx6id8dy1rphfqr25v") (features (quote (("region") ("default" "region"))))))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0ky60mci1859qh0plyyqlwk27va2bblkb11nasvkhac4baw1770y") (features (quote (("region") ("default" "region"))))))

(define-public crate-cn-id-card-0.1 (crate (name "cn-id-card") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0j2snk0g7zxjppg8hrqkqqfba15hw4abrpw1hxc98hhb98r4rv17") (features (quote (("region") ("default" "region"))))))

