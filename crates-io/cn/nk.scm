(define-module (crates-io cn nk) #:use-module (crates-io))

(define-public crate-cnnks-0.1 (crate (name "cnnks") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1igxyikcp1w2adx1r795z8v9x3pfjicsdras1ggq6dymdrp54nb2") (yanked #t)))

(define-public crate-cnnks-0.1 (crate (name "cnnks") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xk99rf1jc0yn2kmws3x547v4pawqnicbwrjnx1yb2cjybrxn7cn") (yanked #t)))

