(define-module (crates-io cn i_) #:use-module (crates-io))

(define-public crate-cni_format-0.4 (crate (name "cni_format") (vers "0.4.3") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0n517jy3382pfdscqrj1qdmijzg9w7b1yzdgh55kq01fi50xbgd5") (features (quote (("more-keys") ("ini") ("default" "ini"))))))

(define-public crate-cni_format-0.4 (crate (name "cni_format") (vers "0.4.4") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1n8hmn2sd254b9xf6n3pvmsbrspqy8cp0v1mjq5l6x5kphwqsa18") (features (quote (("more-keys") ("ini") ("default" "ini"))))))

(define-public crate-cni_format-0.5 (crate (name "cni_format") (vers "0.5.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "utf") (req "^0.1") (default-features #t) (kind 2)))) (hash "0aq0sa99s1hjwz2wj0mwih5ak0my2xrhgqryiv841l4zkkvxn441") (features (quote (("serializer") ("more-keys") ("ini") ("default" "api") ("api"))))))

(define-public crate-cni_format-0.5 (crate (name "cni_format") (vers "0.5.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "utf") (req "^0.1") (default-features #t) (kind 2)))) (hash "1ki138mh61bii1cyv5vh847f9b4gnhk85fabr5f4nn4jjv1sirvz") (features (quote (("serializer") ("more-keys") ("ini") ("default" "api") ("api"))))))

(define-public crate-cni_format-0.6 (crate (name "cni_format") (vers "0.6.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1w3v3h53p2662yh7fxh7q2y9xbqzw3p455llfim72f431s7ig9jr") (features (quote (("serializer") ("default" "api") ("api"))))))

(define-public crate-cni_format-0.6 (crate (name "cni_format") (vers "0.6.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1y6hfc0rziyhp2db6502h62f0j3igq6rzp3bfjx2yifh3fm7r9g6") (features (quote (("serializer") ("default" "api") ("api"))))))

