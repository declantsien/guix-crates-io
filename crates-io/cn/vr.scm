(define-module (crates-io cn vr) #:use-module (crates-io))

(define-public crate-cnvrt-0.1 (crate (name "cnvrt") (vers "0.1.0") (hash "19g6m28kjj1vgdhabg2376z4nbp8pgf3f7r920j34sr45bfampcv")))

(define-public crate-cnvrt-0.2 (crate (name "cnvrt") (vers "0.2.0") (hash "0ilkf729ig8j600p0aycyh2bnlqg9p67p79d0gdfi8gqh2hn268m")))

