(define-module (crates-io dk vs) #:use-module (crates-io))

(define-public crate-dkvs-0.0.1 (crate (name "dkvs") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 1)))) (hash "1ra11hg12nrg7jvwq38jrg8if0g63m4fw8xdnhbz6hw6ji4p3p19")))

