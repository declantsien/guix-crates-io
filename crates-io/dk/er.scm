(define-module (crates-io dk er) #:use-module (crates-io))

(define-public crate-dkernel-0.0.0 (crate (name "dkernel") (vers "0.0.0") (deps (list (crate-dep (name "deskc-ast") (req "^0.0.0") (default-features #t) (kind 0) (package "deskc-ast")) (crate-dep (name "deskc-hir") (req "^0.0.0") (default-features #t) (kind 0) (package "deskc-hir")) (crate-dep (name "deskc-ids") (req "^0.0.0") (default-features #t) (kind 0) (package "deskc-ids")) (crate-dep (name "deskc-types") (req "^0.0.0") (default-features #t) (kind 2) (package "deskc-types")) (crate-dep (name "dkernel-card") (req "^0.0.0") (default-features #t) (kind 0) (package "dkernel-card")) (crate-dep (name "mry") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "salsa") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 2)))) (hash "0z70mhl5rvcmb2vx83jnn9yj0qcyii6pxxhlv7lv9yfrdg11v401")))

(define-public crate-dkernel-card-0.0.0 (crate (name "dkernel-card") (vers "0.0.0") (deps (list (crate-dep (name "hir") (req "^0.0.0") (default-features #t) (kind 0) (package "deskc-hir")) (crate-dep (name "types") (req "^0.0.0") (default-features #t) (kind 0) (package "deskc-types")) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "0fpbjsa7vam019aw5d4lbffa25jfrid6m2zmhyn09adq2n6skylg")))

(define-public crate-dkernel-firestore-0.0.0 (crate (name "dkernel-firestore") (vers "0.0.0") (hash "1flvn6i0cgbz5byrcpx3iynxcaziz49zf7rvbqjjadcgq49l7n7w")))

(define-public crate-dkernel-in-memory-0.0.0 (crate (name "dkernel-in-memory") (vers "0.0.0") (hash "1xdpk1ask8fjcbnvpfp0vs3b5jc4di27kfalp0kv009w95wp0mn8")))

