(define-module (crates-io dk b-) #:use-module (crates-io))

(define-public crate-dkb-rs-0.0.1 (crate (name "dkb-rs") (vers "0.0.1-alpha") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.28") (default-features #t) (kind 0)))) (hash "1yirsn5gg073wi2hqj589p24ankj4277sck2m44660p14ivysc3b")))

(define-public crate-dkb-rs-0.0.2 (crate (name "dkb-rs") (vers "0.0.2") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.28") (default-features #t) (kind 0)))) (hash "0x0660c1avsqnk77skw9qvzkr6siw4b3145abm7nc3dfsck1n7hz") (yanked #t)))

(define-public crate-dkb-rs-0.0.1 (crate (name "dkb-rs") (vers "0.0.1-alpha.2") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.28") (default-features #t) (kind 0)))) (hash "08xb4cjyj06i9nqbawrda59h7nvjn0s208b0lqz8v386nvyf3cb7")))

