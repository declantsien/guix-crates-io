(define-module (crates-io l- sy) #:use-module (crates-io))

(define-public crate-l-system-fractals-0.1 (crate (name "l-system-fractals") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "1w9y2zr8hrfw7a4h8gg8khrfl707s5g1hpc0ayfq2cqqzmi1jqsr")))

(define-public crate-l-system-fractals-0.1 (crate (name "l-system-fractals") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "1rw68xlxxprfa17hwha7qxi6c4s7lk5q0z9ppzrwdwnbab97q5k7")))

(define-public crate-l-system-fractals-0.1 (crate (name "l-system-fractals") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "1bpqpf48qymhjfzb64ikp8k2pcjbq9sgy7dlj6n98lxmjlcgybbb")))

