(define-module (crates-io fg -n) #:use-module (crates-io))

(define-public crate-fg-notation-0.1 (crate (name "fg-notation") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "01w71smc06ydkby3svx5vy8binv5f9y85nsnr6w1lv6bqafs3csa")))

