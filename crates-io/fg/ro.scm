(define-module (crates-io fg ro) #:use-module (crates-io))

(define-public crate-fgrok-1 (crate (name "fgrok") (vers "1.1.0") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^5.0") (default-features #t) (kind 0)))) (hash "1dapgg9a7n4sh75rz3kjpw82r6ycsqa3iiipm9jg0n0n084s0fx9")))

