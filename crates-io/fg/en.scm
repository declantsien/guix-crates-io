(define-module (crates-io fg en) #:use-module (crates-io))

(define-public crate-fgen-0.1 (crate (name "fgen") (vers "0.1.0") (deps (list (crate-dep (name "cbindgen") (req "^0.6.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.5.10") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.42") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "tera") (req "^0.11.8") (default-features #t) (kind 0)))) (hash "1f1kaigxyx0n7v105ghmdcx3ls41w569srkp3czzwr4i79m5kinh") (features (quote (("default") ("cc" "cbindgen" "libc"))))))

(define-public crate-fgener-0.1 (crate (name "fgener") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "09qljgvrjmlbkvx2irj71cbzjs4g78g80sc4pcdhpzw9jxzwvf0z")))

(define-public crate-fgener-0.1 (crate (name "fgener") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vxbr1wcq07p18mys1s690na5g506qhgzny17gc8wc90ssq74ari")))

