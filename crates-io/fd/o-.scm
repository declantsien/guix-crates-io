(define-module (crates-io fd o-) #:use-module (crates-io))

(define-public crate-fdo-client-linuxapp-0.0.1 (crate (name "fdo-client-linuxapp") (vers "0.0.1") (hash "1jpqbbpqlsmcdzp1s9zg192gwh1ab3qbh7i2d022v2xdzdn6i7bc")))

(define-public crate-fdo-data-formats-0.0.1 (crate (name "fdo-data-formats") (vers "0.0.1") (hash "0f3db3q20s516pr5qb89ggvkw9g8x066wbvyi72g3jhxch6kh8af")))

(define-public crate-fdo-http-wrapper-0.0.1 (crate (name "fdo-http-wrapper") (vers "0.0.1") (hash "0vdblaq0a46xi9xljbgwq3cdn3nqzbg8xni9k9xsn5lz9ak45rl6")))

(define-public crate-fdo-manufacturing-client-0.0.1 (crate (name "fdo-manufacturing-client") (vers "0.0.1") (hash "10vb8429124sn8sj5a65xpp5pih2j5l8p4jhkdyfnrs49qkzc61s")))

(define-public crate-fdo-manufacturing-server-0.0.1 (crate (name "fdo-manufacturing-server") (vers "0.0.1") (hash "002qyddw2jchx091vd1a59k5i1z7cr3wgqh012f54h1q21xywq09")))

(define-public crate-fdo-owner-onboarding-server-0.0.1 (crate (name "fdo-owner-onboarding-server") (vers "0.0.1") (hash "1f6416wif3p2g87nszkmcr8048n377wmqyn79xji0v0kx94vschs")))

(define-public crate-fdo-owner-onboarding-service-0.0.1 (crate (name "fdo-owner-onboarding-service") (vers "0.0.1") (hash "1i9dz9a5d0qk9a1w78011dgam6f8icamcqvzl5lwcm2z4qav89p0")))

(define-public crate-fdo-owner-tool-0.0.1 (crate (name "fdo-owner-tool") (vers "0.0.1") (hash "1p7zmqvgv17lkcvjjq5y4z65j76gqhlv4svw42065h0qinff9nsm")))

(define-public crate-fdo-rendezvous-server-0.0.1 (crate (name "fdo-rendezvous-server") (vers "0.0.1") (hash "1cmyn4nhknnfw331ryigy2v6i6w4s0j57isvc7ibnkz665v6ahj8")))

(define-public crate-fdo-store-0.0.1 (crate (name "fdo-store") (vers "0.0.1") (hash "089nk6k40vbrw7pwarymzcvjvggx9x93vawyzxd10fjmfk56dnxn")))

(define-public crate-fdo-util-0.0.1 (crate (name "fdo-util") (vers "0.0.1") (hash "1ac43xq8v3np6h16kcnargs2f3zmkz0s0pzd076snscc6k7sjk2f")))

