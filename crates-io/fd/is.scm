(define-module (crates-io fd is) #:use-module (crates-io))

(define-public crate-fdisk-0.1 (crate (name "fdisk") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.12") (kind 0)) (crate-dep (name "fdisk-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "0i29gplfisk0bg6yp7zd7q8apcq4chkcnpw6dxy7js3rh2ihad0j")))

(define-public crate-fdisk-0.1 (crate (name "fdisk") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.12") (kind 0)) (crate-dep (name "fdisk-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "1kk8qsqb5cxgb5m94s2cm01xlby7lx8c32x6768y8psmqbh1qxv2")))

(define-public crate-fdisk-0.1 (crate (name "fdisk") (vers "0.1.2") (deps (list (crate-dep (name "error-chain") (req "^0.12") (kind 0)) (crate-dep (name "fdisk-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "05w4zrnaas4fcmi6gprgywl45rpwa2sdy2pfszrkng9pa87a2hk5")))

(define-public crate-fdisk-0.2 (crate (name "fdisk") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "fdisk-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.1") (default-features #t) (kind 0)))) (hash "1xkyp6mq521d1zxy8cdvn4i9m9kznvi21r3skn4xfpnih0p3j41b")))

(define-public crate-fdisk-sys-0.1 (crate (name "fdisk-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "1qygk2j8b69bpgyjzfkghgcl5lci1kavvbhiz5mq6dm2ncl2207q")))

(define-public crate-fdisk-sys-0.2 (crate (name "fdisk-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)))) (hash "012k5cgbs5gi9b27qvhasrdxs7an883kw5mk9ry7x94l8h5h7c2s")))

