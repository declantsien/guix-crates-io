(define-module (crates-io fd b-) #:use-module (crates-io))

(define-public crate-fdb-gen-0.2 (crate (name "fdb-gen") (vers "0.2.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "08w6d0a5x4qwhbm069yssz55bkf8bx958ir96i7zm07ddbffblqf") (features (quote (("fdb-6_3") ("default"))))))

(define-public crate-fdb-gen-0.2 (crate (name "fdb-gen") (vers "0.2.1") (deps (list (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "132dann7izgj8gg60928ipb9bhiis2iylaxdpy95qdxa5sybx97l") (features (quote (("fdb-6_3") ("default")))) (rust-version "1.49")))

(define-public crate-fdb-gen-0.2 (crate (name "fdb-gen") (vers "0.2.2") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1hj01rkpv6sq8jvhp6ck65jdl3lz3l2mk7n4f82qpzbpc75y4qwx") (features (quote (("fdb-6_3") ("default")))) (rust-version "1.49")))

(define-public crate-fdb-gen-0.3 (crate (name "fdb-gen") (vers "0.3.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1b88305m3nam83ni5ai3h74a4442g86fylxvspsmpqd5i4gshsbm") (features (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (rust-version "1.49")))

(define-public crate-fdb-gen-0.3 (crate (name "fdb-gen") (vers "0.3.1") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0xlzdp2ixi5zfch7jzqd9bg8z8105icpx849j6hgm9kyp2518pd0") (features (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (rust-version "1.49")))

(define-public crate-fdb-rl-0.1 (crate (name "fdb-rl") (vers "0.1.0") (hash "0wdfbykgr3i8wwr9qp0ah5xrg7i9vk2qvqk1iq4y5ksqzgixsns2")))

(define-public crate-fdb-sys-0.2 (crate (name "fdb-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.0") (default-features #t) (kind 1)))) (hash "17g4c1fw7r6hsps0xm0hd75rz0s2xqcwj2zhsy4ggzqzaxc1s4bm") (features (quote (("fdb-6_3") ("default"))))))

(define-public crate-fdb-sys-0.2 (crate (name "fdb-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.0") (default-features #t) (kind 1)))) (hash "1cbbdh1f9asg4gq82qsgm0xlsl8rhjpy3i7q9r0xz00gd0wpdd2a") (features (quote (("fdb-6_3") ("default")))) (rust-version "1.49")))

(define-public crate-fdb-sys-0.2 (crate (name "fdb-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0kkskf1j83fns6ggdcvnsgvgr8ig2bv6k7l7sjgr5fn5x6p1ij1y") (features (quote (("fdb-6_3") ("default")))) (rust-version "1.49")))

(define-public crate-fdb-sys-0.3 (crate (name "fdb-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0y6xny8j1vkmyz6sl26wacfpdln79s48bj1hrk5f6ny98a85x5yc") (features (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (rust-version "1.49")))

(define-public crate-fdb-sys-0.3 (crate (name "fdb-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0pw1hhmq0sbz29k2dnfia6qh2cqx9ppcvzz5h83f02wicsrl4nsv") (features (quote (("fdb-7_1") ("fdb-6_3") ("default")))) (rust-version "1.49")))

