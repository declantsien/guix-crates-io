(define-module (crates-io fd ns) #:use-module (crates-io))

(define-public crate-fdns-gamenetworkingsockets-sys-0.1 (crate (name "fdns-gamenetworkingsockets-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2.15") (default-features #t) (kind 1)))) (hash "1jvaw0mwfbbd7kiszgksd6fqj8kxnc4yhq6arj7cwqdxn8nzy70p") (links "gamenetworkingsockets")))

(define-public crate-fdns-gamenetworkingsockets-sys-0.2 (crate (name "fdns-gamenetworkingsockets-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2.15") (default-features #t) (kind 1)))) (hash "1iasqk1sb7s9v8gq1s5hagqph05zhzz46436pfcv8d6saxdzxpzd") (links "gamenetworkingsockets")))

(define-public crate-fdns-gamenetworkingsockets-sys-0.3 (crate (name "fdns-gamenetworkingsockets-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2.15") (default-features #t) (kind 1)))) (hash "0mryy9hj4v202mm6r037286pl22s12izv304ikbq30qgsgvrkm02") (links "gamenetworkingsockets")))

