(define-module (crates-io fd ri) #:use-module (crates-io))

(define-public crate-fdringbuf-0.0.1 (crate (name "fdringbuf") (vers "0.0.1") (deps (list (crate-dep (name "nix") (req "*") (default-features #t) (kind 2)))) (hash "0i5gn00ckqwyjgszlvqfzqrmv5pjjl69qsq5m715kbdavgfs616c")))

(define-public crate-fdringbuf-0.0.2 (crate (name "fdringbuf") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "*") (default-features #t) (kind 2)))) (hash "0h4cn7h7xz9l0q7x75hsaq0gd9yg7hrb8nmic4qxbrinmnss7x1k")))

