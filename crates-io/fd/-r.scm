(define-module (crates-io fd -r) #:use-module (crates-io))

(define-public crate-fd-reactor-0.1 (crate (name "fd-reactor") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)))) (hash "1y28n292h329qk91x62hwspcllm5zrg3d3z1crxrf3s3phsh40ws")))

