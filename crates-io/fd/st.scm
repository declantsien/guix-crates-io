(define-module (crates-io fd st) #:use-module (crates-io))

(define-public crate-fdstream-0.0.1 (crate (name "fdstream") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "19fxdipxnmp13fm4dpci9ag4xfbj2zhkmawgcfs7ql69dwslkc9g")))

(define-public crate-fdstream-0.0.2 (crate (name "fdstream") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1bqsb9f9fh7qn7mypdjil4gssyj09rw1w72g27x2lnzq3xa8ib4q")))

(define-public crate-fdstream-0.0.3 (crate (name "fdstream") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "13f1d4w10z12yf9imlhmdcjga8igzx2qfrfnjpkfcn8cvh88wqy0")))

(define-public crate-fdstream-0.0.4 (crate (name "fdstream") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "11qf8q28wxp4dbsmicq7vnvvribg2q3dzx3cl3fqhqm6hbsmpr1k")))

(define-public crate-fdstream-0.0.5 (crate (name "fdstream") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)))) (hash "1qi22qvmk89j4lkkn89dm0kpvpq3067lssmis20dcip0iq2m7kca")))

