(define-module (crates-io fd k-) #:use-module (crates-io))

(define-public crate-fdk-aac-0.1 (crate (name "fdk-aac") (vers "0.1.0") (deps (list (crate-dep (name "fdk-aac-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hcg8q5n661k40rbb80sd2xm28bbhzvb05b8spvrmmwikd0dllz6")))

(define-public crate-fdk-aac-0.2 (crate (name "fdk-aac") (vers "0.2.0") (deps (list (crate-dep (name "fdk-aac-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1k0a4kbjjfvain9cbkfrziz0z56nkz15vmcrgs6wizzkjhghmkrn")))

(define-public crate-fdk-aac-0.3 (crate (name "fdk-aac") (vers "0.3.0") (deps (list (crate-dep (name "fdk-aac-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1x460wf4pdyvidiqph1pxrrwkmsd2603cjygzr60lcls7gqn9dvy")))

(define-public crate-fdk-aac-0.4 (crate (name "fdk-aac") (vers "0.4.0") (deps (list (crate-dep (name "fdk-aac-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1nxhfv9d9zjnz9a57hj8df3sx0nv7rpy225brsyv16lpg9hrbwm8")))

(define-public crate-fdk-aac-0.5 (crate (name "fdk-aac") (vers "0.5.0") (deps (list (crate-dep (name "fdk-aac-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0v3i7gkzcs3iq46wjipr0525zprsdppn8yxbvclaf521za4gkwyk")))

(define-public crate-fdk-aac-0.6 (crate (name "fdk-aac") (vers "0.6.0") (deps (list (crate-dep (name "fdk-aac-sys") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jf6q9ph8f92vkl46cryj6mapr2bbglvdlxaqlk87kiwarv3hy42")))

(define-public crate-fdk-aac-sys-0.1 (crate (name "fdk-aac-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0qmlqmprd91yd3ys9qmc6zr5gz2f1znwbkqwnryrhsc1g2fbca9b")))

(define-public crate-fdk-aac-sys-0.2 (crate (name "fdk-aac-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0zimnkin1ky30cmaqmzahfhi3bf3v8rbkh6xq7fk6i1r59756yhr")))

(define-public crate-fdk-aac-sys-0.3 (crate (name "fdk-aac-sys") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1c8syly1iypsi1nndd9vlcy64a06mqi4dq50kxvjzgamzdbnia1x")))

(define-public crate-fdk-aac-sys-0.4 (crate (name "fdk-aac-sys") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1z0piq2dxs57bi5qk7rby7qiavim8zcg0bmn6wpsi2kslygfzk8c")))

(define-public crate-fdk-aac-sys-0.5 (crate (name "fdk-aac-sys") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "10yaa4nb59ikzkk1773bf3i9zbvbbz3ssm9mhfqmqvah24k6sl94")))

