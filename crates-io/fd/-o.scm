(define-module (crates-io fd -o) #:use-module (crates-io))

(define-public crate-fd-oxigen-1 (crate (name "fd-oxigen") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "oxigen") (req "^2.1") (features (quote ("global_cache"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)))) (hash "0wv3jqz3m9c4x9iwladrc3fyd41i857wi3p8al79kwasplbn1k9b")))

