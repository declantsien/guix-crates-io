(define-module (crates-io fd _b) #:use-module (crates-io))

(define-public crate-fd_bs58-0.1 (crate (name "fd_bs58") (vers "0.1.0") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0q41iqn7d1w9qljwq4vk5l3g4yxcj47rkbkr59bf8pgr1yrw3apx")))

