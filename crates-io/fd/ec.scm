(define-module (crates-io fd ec) #:use-module (crates-io))

(define-public crate-fdec-0.1 (crate (name "fdec") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1qmj4786fx5xqr2xmknrzlv6w95skss3003slldz17vvbvplnfcy")))

(define-public crate-fdec-0.2 (crate (name "fdec") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1fj04am7v6qzq4rnw68awb351p9gj21lw6af777g7s6z82sz6l06")))

(define-public crate-fdec-0.2 (crate (name "fdec") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "07mhzlcs4dbbcizwim7f9apskfil2za7nnrlraa1lk5awjg2zifl")))

(define-public crate-fdec-0.3 (crate (name "fdec") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1qchzlzynb6s1cvd2d4x54nmjw9539wwbmygnnb4jlnpy9x56bnz")))

(define-public crate-fdec-0.3 (crate (name "fdec") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0npl8p7mg0w0w27rf5rjnbysrbmb8agapi6k6yx60fj16n9hkz5r")))

