(define-module (crates-io fd -p) #:use-module (crates-io))

(define-public crate-fd-passing-0.0.1 (crate (name "fd-passing") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1zckhshqf47ik9kv69lpivr3j2s3v8nx1pbgzf44b91xk96a1lsp")))

