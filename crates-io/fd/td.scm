(define-module (crates-io fd td) #:use-module (crates-io))

(define-public crate-fdtd-0.1 (crate (name "fdtd") (vers "0.1.0") (hash "1fd50mciia1f48rqvfvl22apix27z7j7ka6xlhmgmabvrx3vz2iy")))

(define-public crate-fdtdump-0.1 (crate (name "fdtdump") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "fdt-rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "unsafe_unwrap") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vyg0h7gpbbwp36k7yymcdxwy46klkrnnzyfiasp7d6d293bf1ch")))

