(define-module (crates-io fd p-) #:use-module (crates-io))

(define-public crate-fdp-sys-0.1 (crate (name "fdp-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "1imcjbyrb0pjglfyn5qa98flcxva796shjz2bqq6pl5pa38zgxz8") (links "FDP")))

(define-public crate-fdp-sys-0.1 (crate (name "fdp-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "1hzwsyyvmfldhrrlgicb9r9v1n8ak1m7jjdcij8blczinr7f7p5v") (links "FDP")))

