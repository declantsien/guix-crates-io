(define-module (crates-io fd b_) #:use-module (crates-io))

(define-public crate-fdb_tuple-0.1 (crate (name "fdb_tuple") (vers "0.1.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)))) (hash "1i9vvshldp0m88yxkm8c3mn1ynmjyp1fa0y2b2pym53xlx87larr") (features (quote (("default")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.2 (crate (name "fdb_tuple") (vers "0.2.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)))) (hash "15c3r8pkw0mcw6wv4sl2d37a7fk8a34xcwqxb3vxhbjb4n9y90j1") (features (quote (("default")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.2 (crate (name "fdb_tuple") (vers "0.2.1") (deps (list (crate-dep (name "seq-macro") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)))) (hash "0zdxaqbh8l9flmgca0han2s63h21fnfx8v22bh9laagpadj5pgk1") (features (quote (("default")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.3 (crate (name "fdb_tuple") (vers "0.3.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)))) (hash "0294zcx3v9dawrz8c55ssvprc44x1li5dzcx69ya3svizqs662by") (features (quote (("default")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

(define-public crate-fdb_tuple-0.4 (crate (name "fdb_tuple") (vers "0.4.0") (deps (list (crate-dep (name "seq-macro") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)))) (hash "004nkmkvww6269vm6gby0m3pazydh3473f7j7vgc7vq7lj0hcyp2") (features (quote (("default")))) (v 2) (features2 (quote (("uuid" "dep:uuid"))))))

