(define-module (crates-io fd ir) #:use-module (crates-io))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.0") (hash "0r82jpc0l6d7swk4kbj2cm844wzb5w8k3pr4wpw78awndpkjxhdl") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.2") (hash "0m30x0xpwnsr36xpjcc85d1bkiyzjiifc3qv39kf4skkl0qkvj12") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.3") (deps (list (crate-dep (name "axum") (req "^0.6.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1d18ji7sja474k040sfzhifg489d855hq9krpxzn6j31dgx253n1") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.4") (deps (list (crate-dep (name "hyper") (req "^0.14.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "05vxvaa682sppnx0dwg16w52affrbb2s5jj542c87qf99hnvjsr7") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.5") (deps (list (crate-dep (name "hyper") (req "^0.14.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "15g5iaa13vwq94riqywm637bmkn4lsqciak4rb1gpjz62crjsx5c") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0xalvgks0rly55scpxsav4ivf281sn04g3dkyyf0b20ys3lkfrkg") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.6") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1b81jrx9ymy34xwgs46i9zfgazh014nscbsxwdfj3wjx8z6h2xzc") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.7") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0vwg1q94k373nfbl5sk44smvpz7p0qhmxwqdbf9zdf0alw6s0lai") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.8") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "1b9805simhz1sxridslx8i3kpxl9x4k4kl3c92vdrl0d5x4j0gdb") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.9") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "1q05jgy4wiqq5fjjiz9yy07wpis04rig35qzyh1n83ysfy8wih6r") (yanked #t)))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.10") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "0hpc2jfm66h5158aiqc510bq3c5sf5wfi89khqji5zndk7jyfixp")))

(define-public crate-fdir-0.1 (crate (name "fdir") (vers "0.1.12") (hash "0msqhgl9vh6sav8ja325ysdi2p36ziwyyzjb4jsjqn1b8gs3kfrl")))

