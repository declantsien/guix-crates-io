(define-module (crates-io fd pa) #:use-module (crates-io))

(define-public crate-fdpass-0.1 (crate (name "fdpass") (vers "0.1.0") (deps (list (crate-dep (name "fd") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (kind 0)))) (hash "0n30h0q5w07skkj3im8cfkm1d6gna2z3jf9mvfgm2psq5j7ljzr2")))

