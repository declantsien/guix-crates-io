(define-module (crates-io fd li) #:use-module (crates-io))

(define-public crate-fdlibm-rs-0.1 (crate (name "fdlibm-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "00yafl0kn8g3hq8736ghqnn3kpnp4kk8jd888n5r38g2rfnmlb3d")))

(define-public crate-fdlimit-0.1 (crate (name "fdlimit") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vrdljpggwqsrslxshahvrk0psj63pwyx51dpwr394jzf7qi0635")))

(define-public crate-fdlimit-0.1 (crate (name "fdlimit") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1alvrp7p7a9cmq1g63q8bcj3nwgad1q1axr8f6rq0m8f0nkibvmi")))

(define-public crate-fdlimit-0.1 (crate (name "fdlimit") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l9mk9xbmqyc9qyfhjg6hvpdkrqg6a4dnxl96ab19yvfnxdwb14h")))

(define-public crate-fdlimit-0.1 (crate (name "fdlimit") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vambf8fhkcinmlmgnrka7z1wdfalbjnvwzi04hpx86dgi7ydfwa")))

(define-public crate-fdlimit-0.1 (crate (name "fdlimit") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g30d6gqkrwy8ylwdy7pqm443iq0p5dmnpz4ks41pirl7dclm98d")))

(define-public crate-fdlimit-0.2 (crate (name "fdlimit") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l3qi8yjqsf215zjmvffndv27192dp8s31fb1ayv4jc35ci6xg27")))

(define-public crate-fdlimit-0.2 (crate (name "fdlimit") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06rzmk7f60ifwc549hq6d3dzj7q3k1mx4rsvrbj3nnizci1rwk1c")))

(define-public crate-fdlimit-0.3 (crate (name "fdlimit") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1rbz4wpf6a2csjfwxlv2hg22jiq8xaxmy71mczpxjwzgqbdzg0p1")))

