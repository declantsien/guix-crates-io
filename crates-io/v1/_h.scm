(define-module (crates-io v1 _h) #:use-module (crates-io))

(define-public crate-v1_hello_crates_demo_test_crate_do_not_use-0.1 (crate (name "v1_hello_crates_demo_test_crate_do_not_use") (vers "0.1.0") (hash "00ac9fwn7kz3w0mhm1x7h6ai8g4rdz04f80iqrk2xyyhjsfm6n43")))

(define-public crate-v1_hello_do_not_use-0.1 (crate (name "v1_hello_do_not_use") (vers "0.1.0") (hash "1f1par4w0zs39fvlzxgsf108y1jp9xz4bvbwm79s6l06ylqvxdl9")))

(define-public crate-v1_hello_do_not_use_pedromsluz-0.1 (crate (name "v1_hello_do_not_use_pedromsluz") (vers "0.1.0") (hash "1y8qn6m1ywscc5bd1dzj9mrz1pwgsvx9ar1zc2y3gg8g8pw9wccc")))

(define-public crate-v1_hello_hello_donotuse-0.1 (crate (name "v1_hello_hello_donotuse") (vers "0.1.0") (hash "09d3lm0rrgx0byiw7pyn7x0nk7sx83prs6329dadqi6pxnfkvzgg")))

(define-public crate-v1_hello_world_biomathcode-0.1 (crate (name "v1_hello_world_biomathcode") (vers "0.1.0") (hash "0svjwxx0c5y6qkapn58igiaa6h8c1pfl3rzzdv7gnj8sy920sl97")))

