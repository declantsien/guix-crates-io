(define-module (crates-io v1 #{1_}#) #:use-module (crates-io))

(define-public crate-v11_macros-0.0.5 (crate (name "v11_macros") (vers "0.0.5") (deps (list (crate-dep (name "procedural-masquerade") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syntex") (req "0.58.*") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "0.58.*") (default-features #t) (kind 0)))) (hash "06xj0dv0rfml1dhmm5zlhg39l84kqnd2rj2kazz2qdhycychrymb")))

