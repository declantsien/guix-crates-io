(define-module (crates-io gp oi) #:use-module (crates-io))

(define-public crate-gpoint-0.1 (crate (name "gpoint") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gzmssdacrjhd35qlqfmzha5q2f89sdymgy9qzi02dzs6ka80b3r")))

(define-public crate-gpoint-0.1 (crate (name "gpoint") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "04fwxfsblv74fkw4nyqfwpsvxpa78fm7mql5rldzpl878pnp11ih")))

(define-public crate-gpoint-0.1 (crate (name "gpoint") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fgd6j9sv5szw9350s52rd8fnlacqhwhffz3qpij47mnwqwqbvb8")))

(define-public crate-gpoint-0.1 (crate (name "gpoint") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rfb95f47fvaj7v0ybkrs5zzw4zl9lvmy2vk2xqf0kj7g2vq0kpa")))

(define-public crate-gpoint-0.1 (crate (name "gpoint") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i6wfcgk4333nhvh7ychjbj3jjdy8rl4qbqknjgnnkyyf8rgsp60")))

(define-public crate-gpoint-0.2 (crate (name "gpoint") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vkhhscn6pbr2dnfsdisdjw8bqn30gdiwfgni5xm2k63s6y42jkq")))

(define-public crate-gpoint-0.2 (crate (name "gpoint") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v7gy5z7fxyj7345wva0hi1fqib6nw99xpbim04q2h2p5pbg200w")))

