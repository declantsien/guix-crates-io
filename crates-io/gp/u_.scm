(define-module (crates-io gp u_) #:use-module (crates-io))

(define-public crate-gpu_rand-0.1 (crate (name "gpu_rand") (vers "0.1.0") (deps (list (crate-dep (name "cuda_std") (req "^0.1") (default-features #t) (target "cfg(target_os = \"cuda\")") (kind 0)) (crate-dep (name "cust") (req "^0.1") (default-features #t) (target "cfg(not(target_os = \"cuda\"))") (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "18lw351s94i9vzirxn10qjkfs1c97yxaqw7wrblhblgcm8nzxxwm")))

(define-public crate-gpu_rand-0.1 (crate (name "gpu_rand") (vers "0.1.1") (deps (list (crate-dep (name "cuda_std") (req "^0.1") (default-features #t) (target "cfg(target_os = \"cuda\")") (kind 0)) (crate-dep (name "cust") (req "^0.2") (default-features #t) (target "cfg(not(target_os = \"cuda\"))") (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "03wlhs9153s9wdyrsqirvmpvfycb03xnx74axlz2fl9ssbwzxbxb")))

(define-public crate-gpu_rand-0.1 (crate (name "gpu_rand") (vers "0.1.2") (deps (list (crate-dep (name "cuda_std") (req "^0.2") (default-features #t) (target "cfg(target_os = \"cuda\")") (kind 0)) (crate-dep (name "cust") (req "^0.2") (default-features #t) (target "cfg(not(target_os = \"cuda\"))") (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "108afdnvhkc4cagdgpxvkgcl9sgbsacbxbpd4w41qjb7yxbr2gyz")))

(define-public crate-gpu_rand-0.1 (crate (name "gpu_rand") (vers "0.1.3") (deps (list (crate-dep (name "cuda_std") (req "^0.2") (default-features #t) (target "cfg(target_os = \"cuda\")") (kind 0)) (crate-dep (name "cust_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "0748p5wmyrwifgyjgczh8apkvq0djs6jq03xy7rkvgshm4003k2r")))

