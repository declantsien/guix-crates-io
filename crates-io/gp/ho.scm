(define-module (crates-io gp ho) #:use-module (crates-io))

(define-public crate-gphoto-0.1 (crate (name "gphoto") (vers "0.1.0") (deps (list (crate-dep (name "gphoto2-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "14l8lcl3r5zzjwvsfb7iv9knppm1wv4n8bvjq2q88y93bf7ndycf")))

(define-public crate-gphoto-0.1 (crate (name "gphoto") (vers "0.1.1") (deps (list (crate-dep (name "gphoto2-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cbm780jfq6i9kxam051a0kislbhasbqkacdqvn35as6hpg3d35w")))

(define-public crate-gphoto-0.1 (crate (name "gphoto") (vers "0.1.2") (deps (list (crate-dep (name "gphoto2-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g0kg7qf0caaix5d7ym475qhxaiwr59782n2cxzp755n3gdyhhym")))

(define-public crate-gphoto2-0.1 (crate (name "gphoto2") (vers "0.1.0") (deps (list (crate-dep (name "libgphoto2_sys") (req "^2.5.10") (default-features #t) (kind 0)))) (hash "00ki2l1i88i4w4fpvzc47y3naq8n8qys287m5vcjbjkjk321zna1")))

(define-public crate-gphoto2-0.1 (crate (name "gphoto2") (vers "0.1.1") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0fj6h2m52g5lk1wbjsbwang38lr91zcmc395cjsz6h36ivv97dq1")))

(define-public crate-gphoto2-0.2 (crate (name "gphoto2") (vers "0.2.0") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10l70jw6vxx7skx0aknrkh5k40bzhsym55aybj2bg7iim9x19wil")))

(define-public crate-gphoto2-0.3 (crate (name "gphoto2") (vers "0.3.0") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0asa6xxg26nppizw294gz63ipm8n4nzn5x3vi244swnigs6gaxmn")))

(define-public crate-gphoto2-0.3 (crate (name "gphoto2") (vers "0.3.1") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "139nycbrci4i8gkqpn6f9q0154vihql712nqv2gnab1y2kndf0x8")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.0.0-rc1") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "18ywy182w7l8apx9pyrc3h2k0mqyc3nl3v68fif7p96jzmgkkbk3")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.0.0") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0yzhyyz18y2blyyaz7pga972hj0fr4f0ngzhiibdrm9jizrgydri")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.0.1") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "015bd76mfs4ilzczx9p4gyczvn4fpfkwyzl9qll4kyg0zz57gnj4")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.0.2") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0j18vxj56v0xm0gnacbmb84c9wbnm6hv2w1lc8vw169a25r6sv50")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.1.0") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "17hq13yj92s57xxds3a1dfkx5zjzb0ng17cjds6x3n4zzbzg9rb9")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.1.1") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1cvrckzdac9l17bbfcapszn69ahs5ngcm92s3dbbw6q42x98dcdx")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.2.0") (deps (list (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "09bdqk78aalccz4x05rvs7h4jyphnmklylmpq0wa0azy6d3rvanw")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0vq1mk0dwdyj8q5dpydqh8pfn6g2v21hkxnaajrqr25wgq8ka2r0")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0g93bdyphpywhhk9j4qjw8432kqqplb4116l0g770af6nxyhp9nx")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1f744cagicm6bshs593gdc45xhw55k5fgrb18x59h1i6flp319p1")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.3.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "10a64y2lagjdm5v2y82gmlhwrgwpprbrb18q7lyjnwlirnqdih23")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.3.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0r68a80wjjgk4pmg1107rxvvz8w36q7g89cw6kl0ak2yx5qg4m27")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)))) (hash "18z390afh1f6xm5njjh92wygqxqsnx5vnfbk1j8n77svf41wc66c")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)))) (hash "0308ywcvq31063j70vb7a22mmn7aaaiinf5ysr8pc4lqyzlaz3g8")))

(define-public crate-gphoto2-1 (crate (name "gphoto2") (vers "1.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)))) (hash "1d9jdwacanjjwxzgnldx7fms2kwf5v0b92vg5p5lnhgk69002w19")))

(define-public crate-gphoto2-2 (crate (name "gphoto2") (vers "2.0.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1hg3csj046ip70r5ldh9q9173lbz0myar38rp7vk9nb5a204d1ha") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-2 (crate (name "gphoto2") (vers "2.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0w04d3qdx4l8h5g3pk4dpcc988xqzv1fhm0yw47917lxxfnfbw8z") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0g3lyqmphxbxja10mhzdmjvmjxfba4zgqlnwnq8qr934xnaxavwy") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0a6lqg2zh6bz49y05r496hbkdyrvsr6aazkjcmmck0jn5c54mwcy") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qqljcg5mky6l5fmn0yalx2k57wjl79b49qhv8cdm0r04qgbdjvd") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0yjjyg2gq2haqyz33dipmvmcbghb8a7nnzchpabv5i7rbgvyxbkg") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pdidrhrzzwkkjmwfp8qxqr1jcrws23rh85bsnkbq9rr5mkavsqv") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.2.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "1y7pk6lrw5i5cncw4ixk659l62dgzak4gihbv87qipxp9rvrkca2") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.2.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qs4nldpnjagxdgb18p72kx8mbwh7s5idiiwc7gyd79cg3qcvpi1") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-3 (crate (name "gphoto2") (vers "3.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^1.20.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libgphoto2_sys") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1gdhp3nbj37arpyh5wyc0322df1yxbvwmyfq72gfl0gmf53j7kz8") (features (quote (("test" "libgphoto2_sys/test") ("extended_logs")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gphoto2-sys-0.1 (crate (name "gphoto2-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "17wkc8qyr2spw2p060cc4i2l68d28jb81yjydj9bfyh4vwrf0n4q")))

(define-public crate-gphoto2-sys-0.1 (crate (name "gphoto2-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "18445knq48pbr81bypsv8y6spkzpjd2lylwzvzp1176gmxdcxxp1")))

(define-public crate-gphoto2-sys-0.1 (crate (name "gphoto2-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "167qsw8qanbj020rjij11vmpwl8lxp0ni176c9vx2nvns45janmy")))

(define-public crate-gphoto2_test-1 (crate (name "gphoto2_test") (vers "1.0.0") (hash "0dxzbrz50sfq5pp2k8vfxjaaikmkd67v3h8k3bi5kf6rq2ap6gk8")))

