(define-module (crates-io gp s_) #:use-module (crates-io))

(define-public crate-gps_axel-0.1 (crate (name "gps_axel") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kpkmivjx1315q8cnankpsvwcfrpfgxwd0bli03bxkw1zps6mpnn")))

(define-public crate-gps_data_collector_kim-0.1 (crate (name "gps_data_collector_kim") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d4bxl7ygkgjiri7f6z3p396j3zilw5i5bk0fjhksg21kcqqdrjb")))

(define-public crate-gps_data_reader_bjarki18-0.1 (crate (name "gps_data_reader_bjarki18") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00hmplxvlid2zh04wivsa2n5mdarz4wmwc5a9vd3va7l3arm5hwa") (yanked #t)))

(define-public crate-gps_data_reader_bjarki18-0.1 (crate (name "gps_data_reader_bjarki18") (vers "0.1.1") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "063fskgw4chz2wpvz65l5i35lv5jqf1rdblqwflxy2yi3mjhigvm") (yanked #t)))

(define-public crate-gps_data_reader_bjarki18-0.1 (crate (name "gps_data_reader_bjarki18") (vers "0.1.2") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hlmgjgrdghr68lwr0iyrw98cd64m2nijczc7pr8n7nx739fk212") (yanked #t)))

(define-public crate-gps_data_reader_bjarki18-0.1 (crate (name "gps_data_reader_bjarki18") (vers "0.1.3") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1f5im4nwskcpqr91s78zyrcq677rkmsabqwqk9sv2icdzcd991v3") (yanked #t)))

(define-public crate-gps_data_reader_bjarki18-0.1 (crate (name "gps_data_reader_bjarki18") (vers "0.1.4") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06pzmxg5kl57yh3h3l8y6jkiq1scc4vrrw7y15hwlngbnxvd1yp4")))

(define-public crate-GPS_datacollector_dagur-0.1 (crate (name "GPS_datacollector_dagur") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1v3zi2bx4y5rqhsy891pgp87vwwvd2a1s9giisrkd7qvwrjklbc2")))

(define-public crate-GPS_datacollector_dagur-0.1 (crate (name "GPS_datacollector_dagur") (vers "0.1.1") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0c9lmqvba0nn3cz9mmy9inks5aqipvkxz2gmjybgvszh9gafldxm")))

(define-public crate-GPS_project_aisha-0.1 (crate (name "GPS_project_aisha") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xh5aqdnnjcbr5hi3iw66vml86icq3q8y0a9ld5wapnwmx841fbi") (yanked #t)))

(define-public crate-GPS_project_aisha-0.1 (crate (name "GPS_project_aisha") (vers "0.1.1") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08akzxmvcs0801sdpb33lf9jqf1vrgv7mf0khkllsd74nfhg4aw7") (yanked #t)))

(define-public crate-GPS_project_aisha-0.1 (crate (name "GPS_project_aisha") (vers "0.1.2") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p55gfk1p47qrg8zs7rr1i67k4lxll151f8r2xdyx3vinyj3ik4c") (yanked #t)))

(define-public crate-GPS_project_aisha-0.1 (crate (name "GPS_project_aisha") (vers "0.1.3") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xvjsdcrzwjxr0nq7lqkkg51bklvw30zl1l4408a57c9y8ml61cy") (yanked #t)))

(define-public crate-GPS_project_aisha-0.1 (crate (name "GPS_project_aisha") (vers "0.1.4") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1i515da2dznbxhyby4g1zaamdr34637rixvx61vyilf24s3kslkn")))

