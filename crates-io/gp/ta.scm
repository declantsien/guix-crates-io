(define-module (crates-io gp ta) #:use-module (crates-io))

(define-public crate-gptask-0.1 (crate (name "gptask") (vers "0.1.0") (deps (list (crate-dep (name "bat") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "06sqfrj4j41kl4yjkfm764n3h7nadcs69hf3kgb61d0z4iq4483v")))

(define-public crate-gptask-0.2 (crate (name "gptask") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "02f7c2g8klqbziilmi8l06grzbq8wvwn9xkjsvjshy3fh2i23z2h")))

