(define-module (crates-io gp s-) #:use-module (crates-io))

(define-public crate-GPS-Gunnlaug-0.1 (crate (name "GPS-Gunnlaug") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05kzljnns28xmjnxf9j1nw4hqpfncn3wxxsd470bqchbis9zahgz")))

(define-public crate-GPS-Gunnlaug_18-0.1 (crate (name "GPS-Gunnlaug_18") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gbw8dbzcfz3klf896dbrbspan4d72p69rynyvdkhzis0nmdmsxn")))

(define-public crate-GPS-Gunnlaug_18-0.1 (crate (name "GPS-Gunnlaug_18") (vers "0.1.1") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mbkrvkz3d977jq16ynmif9d9x3rdhi5qf81fyg8rgjcpbb5na2q")))

(define-public crate-GPS-Gunnlaug_18-0.1 (crate (name "GPS-Gunnlaug_18") (vers "0.1.2") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1dkn90xy41k6zkch73s3vsr7c5syd0drfvvvjsczhy7bvnqw5sn7")))

(define-public crate-GPS-Gunnlaug_18-0.1 (crate (name "GPS-Gunnlaug_18") (vers "0.1.3") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z6g039zm0ak0xfbl9r3v8ys4gr8w9s3dj7zmy7ch3njwss80kki")))

(define-public crate-GPS-Gunnlaug_18-0.1 (crate (name "GPS-Gunnlaug_18") (vers "0.1.4") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a83qxgxr5l2p9f2m6d6xl7zl7lar467w0b2shj71xaywrm737kk")))

(define-public crate-GPS-Gunnlaug_18-0.1 (crate (name "GPS-Gunnlaug_18") (vers "0.1.5") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14qahy5qx3k7nraah9msqsdy0zg0jd6492dfsm50va8wb8blxs2b")))

(define-public crate-GPS-Gunnlaug_18-0.1 (crate (name "GPS-Gunnlaug_18") (vers "0.1.6") (deps (list (crate-dep (name "rpi_embedded") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0h4cgxzvl0mrpcaddlw2l1gxzagxrd9zj1cxhdb0ghvxm62865an")))

(define-public crate-gps-share-0.3 (crate (name "gps-share") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.23.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "libudev") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "zvariant") (req "^2.6") (default-features #t) (kind 0)))) (hash "0rbvpvx6xflig07lwvrz8b0jg32gyqp2y0gdk4vj8yrf40fakfik")))

(define-public crate-gps-share-0.3 (crate (name "gps-share") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.23.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "libudev") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "zvariant") (req "^2.6") (default-features #t) (kind 0)))) (hash "1fd6dlm3ldkwmbsk7mjr1wb475j6c0jvj3wabf6jqq29plzyw71k")))

