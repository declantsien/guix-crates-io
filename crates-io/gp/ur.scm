(define-module (crates-io gp ur) #:use-module (crates-io))

(define-public crate-gpurs-0.1 (crate (name "gpurs") (vers "0.1.0") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "156mjsyay67x8da0aj990k6gm5x0k70301s6ap03nzb3synhjk85") (yanked #t)))

(define-public crate-gpurs-0.1 (crate (name "gpurs") (vers "0.1.1") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1hd1a5qhqr9pqj8pvq8rwcgrhg1mr0459xdcbgja9ycrxdl6x8ir")))

(define-public crate-gpurs-0.2 (crate (name "gpurs") (vers "0.2.0") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0srf8dmcmzs83jf8sap7xnx6zyhj2sd8d9xq4mfyvjihdyl6vqvs")))

(define-public crate-gpurs-0.3 (crate (name "gpurs") (vers "0.3.0") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)))) (hash "10i8l8v9sjwxbch2zcjh3h4l3k0mdzqc212l803hfwhpn1q7sxg7") (features (quote (("default" "gpu_accel")))) (v 2) (features2 (quote (("gpu_accel" "dep:opencl3"))))))

(define-public crate-gpurs-0.3 (crate (name "gpurs") (vers "0.3.1") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)))) (hash "02f90l9svjcixnbrrk989y0vqb0xpi33xgy7l26v4751y2lpz0k7") (features (quote (("default" "gpu_accel")))) (v 2) (features2 (quote (("gpu_accel" "dep:opencl3"))))))

(define-public crate-gpurs-0.4 (crate (name "gpurs") (vers "0.4.0") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)))) (hash "070paxx8bkyq9752rjjp5wnd1jqjdgp1s39qfma05fhwqdnqghpx") (features (quote (("default" "gpu_accel")))) (v 2) (features2 (quote (("gpu_accel" "dep:opencl3"))))))

