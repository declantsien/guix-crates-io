(define-module (crates-io gp #{2y}#) #:use-module (crates-io))

(define-public crate-gp2y0e02b-0.1 (crate (name "gp2y0e02b") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.3.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1zr38gas1dllbiip4331f92nq11xnnlsk63hizbl72klj60kc4lz")))

(define-public crate-gp2y0e02b-0.1 (crate (name "gp2y0e02b") (vers "0.1.1") (deps (list (crate-dep (name "cast") (req "^0.3.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13li8b68jxzzypsl085sad4wrsicsli5gz0zy4hj42425wdhqdnk")))

(define-public crate-gp2y0e02b-0.2 (crate (name "gp2y0e02b") (vers "0.2.0") (deps (list (crate-dep (name "cast") (req "^0.3.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0gz4ir7mi4qigjbrl5nvypgcd76wp49kbd3ppsfh796gg5hh6ais")))

(define-public crate-gp2y0e02b-0.2 (crate (name "gp2y0e02b") (vers "0.2.1") (deps (list (crate-dep (name "cast") (req "^0.3.0") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1d0phzbypv6kh6i2y10ahj8aaq7nkrsp84phqxfay5nwb6qz5sji")))

(define-public crate-gp2y1014au-0.1 (crate (name "gp2y1014au") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "0.2.*") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "1.0.*") (default-features #t) (kind 0)))) (hash "0x97hiby1n9khkwh4c405ijczqw60z888zrizl390fh65rfsr7iz")))

