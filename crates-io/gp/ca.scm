(define-module (crates-io gp ca) #:use-module (crates-io))

(define-public crate-gpcas_arschitek_zero-0.1 (crate (name "gpcas_arschitek_zero") (vers "0.1.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.1") (default-features #t) (kind 0)))) (hash "1s06049278mm3frb49m04is5sw96glwq6b2xz9kpvl2gjx1dvabv")))

(define-public crate-gpcas_arschitek_zero-0.1 (crate (name "gpcas_arschitek_zero") (vers "0.1.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rd9smyv9xav13a5nqk6340yx6847dsz9ibpa9cgjb10nx8vqbj5")))

(define-public crate-gpcas_arschitek_zero-0.1 (crate (name "gpcas_arschitek_zero") (vers "0.1.2") (deps (list (crate-dep (name "gpcas_base") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.1") (default-features #t) (kind 0)))) (hash "0sanx5hniv0np77l8pp0w9gwladqklyv92ciz55c6yrcgw55flh9")))

(define-public crate-gpcas_arschitek_zero-0.1 (crate (name "gpcas_arschitek_zero") (vers "0.1.3") (deps (list (crate-dep (name "gpcas_base") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.1") (default-features #t) (kind 0)))) (hash "1lbzx53j1qrw4cwyd377x2798yqzp6x8jlvqxawd3a9m60fv395q")))

(define-public crate-gpcas_arschitek_zero-0.1 (crate (name "gpcas_arschitek_zero") (vers "0.1.4") (deps (list (crate-dep (name "gpcas_base") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1l52lpcs3cf8nlgkrlid0lkx989w4pkawg7dqr68gby9x2kblsnc")))

(define-public crate-gpcas_arschitek_zero-0.2 (crate (name "gpcas_arschitek_zero") (vers "0.2.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "006228qcwfh958gzcsv9q5qp9gbj5s63jmgnz7rmkqlgqlvb4555")))

(define-public crate-gpcas_arschitek_zero-0.3 (crate (name "gpcas_arschitek_zero") (vers "0.3.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pdvpmr0gkhxjdkqqjms64j6m6p6lqbhiy6ls2s9frgn2z677028")))

(define-public crate-gpcas_arschitek_zero-0.3 (crate (name "gpcas_arschitek_zero") (vers "0.3.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.3") (default-features #t) (kind 0)))) (hash "1w9x9f97wqywgdwmvfghah17a7ww9sf0rblb8yqrg79i03gqymz6")))

(define-public crate-gpcas_arschitek_zero-0.4 (crate (name "gpcas_arschitek_zero") (vers "0.4.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jv7fnpfa7l106h0b6y05ipc34p3rmrfgmwwjvb6gryz3axaznkm")))

(define-public crate-gpcas_arschitek_zero-0.4 (crate (name "gpcas_arschitek_zero") (vers "0.4.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.5") (default-features #t) (kind 0)))) (hash "1l5lqmwncp39l6b8y060zf86ikp62gkpw5mg4pds3pv9j03ww8gn")))

(define-public crate-gpcas_arschitek_zero-0.5 (crate (name "gpcas_arschitek_zero") (vers "0.5.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hn1al78vv1d1wik9n2x1bykzzband0977pr6lyzda5zd4y6nsgs")))

(define-public crate-gpcas_base-0.1 (crate (name "gpcas_base") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1gjmwg1y54q8pspyb1qriivf90pxcw8g4r3pl6ahqai7g73n0fbg")))

(define-public crate-gpcas_base-0.1 (crate (name "gpcas_base") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "08yadk2r5z45kpj2ly0hnmpfd9ldshbhzwcc439v7d6q4z911al9")))

(define-public crate-gpcas_base-0.2 (crate (name "gpcas_base") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1b7q8g8zkyf5f20qnwx8ifh2qz585a7ds9myq8l5j0ix2iq1r20q")))

(define-public crate-gpcas_base-0.3 (crate (name "gpcas_base") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0fm2kypjw7rpa0qxn80v778n70nri6d8znn0bvpfajk2vrz2cjlc")))

(define-public crate-gpcas_base-0.3 (crate (name "gpcas_base") (vers "0.3.1") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "02r8cdr47wibxrpjgg4fi9n0ln1zp7b4r8agnglvgpdbgcqjygva") (yanked #t)))

(define-public crate-gpcas_cpu_model-0.1 (crate (name "gpcas_cpu_model") (vers "0.1.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1j53bq4abpdixrf73wnp91p06sxsy4jchh7qkk8hnq2ns9ckihy9")))

(define-public crate-gpcas_cpu_model-0.1 (crate (name "gpcas_cpu_model") (vers "0.1.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0z0rv8nrjpjhwlpx8lcmm6f046ap4n3f3wh9jzy7lah1ris3gjbx")))

(define-public crate-gpcas_cpu_model-0.2 (crate (name "gpcas_cpu_model") (vers "0.2.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cjlvnc54yldhj1p8phm4dr5x3hghkz3s183cfcrdamzxk63vzk8") (yanked #t)))

(define-public crate-gpcas_cpu_model-0.2 (crate (name "gpcas_cpu_model") (vers "0.2.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0aa91gf51f9q4w8nfh9y40ixmsb102jfds569rijjc0ravzc25y0")))

(define-public crate-gpcas_cpu_model-0.3 (crate (name "gpcas_cpu_model") (vers "0.3.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "07k5knf724230j8hjjh5gr0zp0qdjxhnwgh6fq06r694wjm27w8k") (yanked #t)))

(define-public crate-gpcas_cpu_model-0.3 (crate (name "gpcas_cpu_model") (vers "0.3.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1a77bayrr67wn48qiwd26v07z5rqq4m4ycv2kmdd9h8jsimw803b")))

(define-public crate-gpcas_cpu_model-0.3 (crate (name "gpcas_cpu_model") (vers "0.3.2") (deps (list (crate-dep (name "gpcas_base") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rqkmg9hkkywdj8nkh48jhcymjqgb62zlvzxarxr5vs1d50jqdxm")))

(define-public crate-gpcas_cpu_model-0.3 (crate (name "gpcas_cpu_model") (vers "0.3.3") (deps (list (crate-dep (name "gpcas_base") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sa1d0bvn7z7z13h63h5bzm4661fg3k2ixbjfjqkivxhylysprkg")))

(define-public crate-gpcas_cpu_model-0.4 (crate (name "gpcas_cpu_model") (vers "0.4.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1djh360028kxck5qmh6kps0lq4g8gbyslk5d9hflndplizapwpid")))

(define-public crate-gpcas_cpu_model-0.4 (crate (name "gpcas_cpu_model") (vers "0.4.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0j7sfqyp7g48mvf6l8miwika5ymyqh1rcbh2cgpc0f5xzs8da9fr")))

(define-public crate-gpcas_cpu_model-0.5 (crate (name "gpcas_cpu_model") (vers "0.5.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0whd7snahip1182b1wgw15kml3nhwz51j5jkhwb2x96rysk50h4j")))

(define-public crate-gpcas_cpu_model-0.5 (crate (name "gpcas_cpu_model") (vers "0.5.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0g2hzg8h3qws1f4d2rw8fxmhw3fkm7xpf679zyj13k7smi2ns3c5")))

(define-public crate-gpcas_cpu_model-0.6 (crate (name "gpcas_cpu_model") (vers "0.6.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13gmxbxg1p3vcixa8f66xjvz3bldi4b2qvjy8njs6dsh6jf15psg")))

(define-public crate-gpcas_cpu_model-0.6 (crate (name "gpcas_cpu_model") (vers "0.6.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ip8f4lzvhrv738gpl8q3rw96ybxjh1nnvi5pidh335mrkq2rn14")))

(define-public crate-gpcas_cpu_model-0.7 (crate (name "gpcas_cpu_model") (vers "0.7.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "193yvipzqgzygyvfgyzq0hafvxd448nx674bdlih1siz3fxy44iy")))

(define-public crate-gpcas_cpu_model-0.7 (crate (name "gpcas_cpu_model") (vers "0.7.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19gv1xj8pyqbhw0prrv7falf65gyxrfnf3qw1kiifrwjgq9jkx2y")))

(define-public crate-gpcas_forwardcom-0.1 (crate (name "gpcas_forwardcom") (vers "0.1.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "142ly1a3iykbva3lpc55c43zm6c8wiwzlajgirmnq7clr6qbmk5j")))

(define-public crate-gpcas_forwardcom-0.1 (crate (name "gpcas_forwardcom") (vers "0.1.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "17l47ddg6pf1d84m39qw9xm9kpnb8bm5scgsl2l1x2sla0kfn7pm")))

(define-public crate-gpcas_forwardcom-0.1 (crate (name "gpcas_forwardcom") (vers "0.1.2") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "15vmr02x7yyb2jbw2gid9w07ppp98yzkrmzjgwr2n77yhy6qg4v0")))

(define-public crate-gpcas_forwardcom-0.2 (crate (name "gpcas_forwardcom") (vers "0.2.0") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "167jdanlabic92rbiprgagbazlyd78pqzdy9mv87yrnwkpfcva9r")))

(define-public crate-gpcas_forwardcom-0.2 (crate (name "gpcas_forwardcom") (vers "0.2.1") (deps (list (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "04lb95l633swwdf1by43dspi6hywa7a1141vnb7s9q0w15p54vm2") (rust-version "1.67")))

(define-public crate-gpcas_isa-0.1 (crate (name "gpcas_isa") (vers "0.1.0") (hash "0yqi6bjrl2m83fb14hz14l6rxm11b2r4784mscr7nxjl0amv4xjg")))

(define-public crate-gpcas_isa-0.1 (crate (name "gpcas_isa") (vers "0.1.1") (hash "1cc0rg8frq1v7ylq3z6mjzx4fwqx9kfx8mc8srdry1lbjm1nda8i")))

(define-public crate-gpcas_isa-0.1 (crate (name "gpcas_isa") (vers "0.1.2") (hash "00n5sd0v3llhw04cp9956v9py28z5sxcpxv8i690v4vfpshjhdn3")))

(define-public crate-gpcas_isa-0.2 (crate (name "gpcas_isa") (vers "0.2.0") (hash "0ccmvs61yvrnq8d1zxs6pbkb16fpav3va4kfkgrpr194awa3wwvg") (yanked #t)))

(define-public crate-gpcas_isa-0.2 (crate (name "gpcas_isa") (vers "0.2.1") (hash "1ysfmr7hw3s1jgbk8w6irmfrnwwf783blgn3l8m5f0jvs707z9v5")))

(define-public crate-gpcas_isa-0.2 (crate (name "gpcas_isa") (vers "0.2.2") (hash "0vlvk0ds4d7xky6ykjpjw8jlh7zpdvkw5i9c71vyzsnq76wgn41v")))

(define-public crate-gpcas_isa-0.3 (crate (name "gpcas_isa") (vers "0.3.0") (hash "0ngv3n0c6azddlqs4cjg7fm93yy355hh30pc70jah2izi9jg8py6")))

(define-public crate-gpcas_isa-0.4 (crate (name "gpcas_isa") (vers "0.4.0") (hash "0hgdix5g0j01aky3i78wl7bqgxpd13q91j5lifmqh4qhza881yqb")))

(define-public crate-gpcas_isa-0.5 (crate (name "gpcas_isa") (vers "0.5.0") (hash "096hgavcjk9cm1s07dmi2wclva8hpcfmbpdlzsdj8cfwwyv3jpfs")))

(define-public crate-gpcas_isa-0.6 (crate (name "gpcas_isa") (vers "0.6.0") (hash "1siyp7fv28w9f19ghzdnn3lsszjbyphl8al9sii1dxmm2rgfa7ng")))

(define-public crate-gpcas_isa-0.6 (crate (name "gpcas_isa") (vers "0.6.1") (hash "1f1l7pma679x5cy7lhhyid0f8jb5r811lsj97if633g7r32p2qrg")))

(define-public crate-gpcas_simulator-0.1 (crate (name "gpcas_simulator") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "gpcas_arschitek_zero") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_cpu_model") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "gpcas_forwardcom") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "num-format") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ring_buffer") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)))) (hash "1z6bzddd6bzm8g5b9lbaj2b2jf9l5h1n893a0wdq5gxz6bybx4x2") (rust-version "1.63")))

(define-public crate-gpcas_simulator-0.1 (crate (name "gpcas_simulator") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "gpcas_arschitek_zero") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_cpu_model") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "gpcas_forwardcom") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "num-format") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ring_buffer") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)))) (hash "1ys1qlxxf32ffli1myk3wm5xcc97lcnh6w86anyanddab1ni19yr") (rust-version "1.63")))

(define-public crate-gpcas_simulator-0.2 (crate (name "gpcas_simulator") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "gpcas_arschitek_zero") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_cpu_model") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "gpcas_forwardcom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "num-format") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ring_buffer") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)))) (hash "136ks3xrdjkgh245z7bgjj2xk4srk4qmysk8fqv7pm5c1xlfagyr") (rust-version "1.63")))

(define-public crate-gpcas_simulator-0.2 (crate (name "gpcas_simulator") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "gpcas_arschitek_zero") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "gpcas_base") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gpcas_cpu_model") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "gpcas_forwardcom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gpcas_isa") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("max_level_trace" "release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "num-format") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ring_buffer") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)))) (hash "0g9r14ssbjpy2j2zcy4yvymgria498md14j5a4l34gbps4bmqf7x") (rust-version "1.67")))

