(define-module (crates-io gp -e) #:use-module (crates-io))

(define-public crate-gp-externalities-0.13 (crate (name "gp-externalities") (vers "0.13.0") (deps (list (crate-dep (name "codec") (req "^3.2.2") (kind 0) (package "parity-scale-codec")) (crate-dep (name "environmental") (req "^1.1.3") (kind 0)) (crate-dep (name "sp-std") (req "^5.0.0") (kind 0)) (crate-dep (name "sp-storage") (req "^7.0.0") (kind 0)))) (hash "0wsz74p4krqv917sahlqlfb1naajckgx6rxycgls6jffq1ki70dk") (features (quote (("std" "codec/std" "environmental/std" "sp-std/std" "sp-storage/std") ("default" "std"))))))

