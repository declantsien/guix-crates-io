(define-module (crates-io gp ar) #:use-module (crates-io))

(define-public crate-gparity-wasm-0.45 (crate (name "gparity-wasm") (vers "0.45.0") (deps (list (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mb1any62r0qklafjkc1ina2xqg4wy5sm00fgcjgax47ji251v2k") (features (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("multi_value") ("default" "std") ("bulk") ("atomics")))) (rust-version "1.56.1")))

