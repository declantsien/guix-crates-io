(define-module (crates-io gp er) #:use-module (crates-io))

(define-public crate-gperf-sys-0.1 (crate (name "gperf-sys") (vers "0.1.0") (deps (list (crate-dep (name "autotools") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "1jqq0fwgz1ws5wkc84hkib0nyzbsivh0rnl7la6a08p13n0z6xxz") (yanked #t)))

(define-public crate-gperf-sys-0.2 (crate (name "gperf-sys") (vers "0.2.0") (deps (list (crate-dep (name "autotools") (req "^0.2.1") (default-features #t) (kind 1)))) (hash "0s23jf0831blhfxnz3c7l8ihwz6p46w9hp1d9jd4zx3imaf098yb") (yanked #t) (links "gp")))

(define-public crate-gperftools-0.2 (crate (name "gperftools") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "17dw7sc2zdfl01sw0y3zay5j11h4j349hsgw5333w8mi31cgr8r0") (features (quote (("heap") ("default"))))))

(define-public crate-gperftools-static-0.1 (crate (name "gperftools-static") (vers "0.1.0") (deps (list (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "gperftools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)))) (hash "09nxrvlkxxbdvm11jqkpza8m47p1gap9gq25j87gs5q9j6qkbw20") (features (quote (("static_unwind") ("static_gperftools") ("default"))))))

(define-public crate-gperftools-static-0.1 (crate (name "gperftools-static") (vers "0.1.1") (deps (list (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "gperftools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)))) (hash "0y6s2fggrrm4sc2mhqkh174l38zpc0h6y2kg670yw5xh3xwy426y") (features (quote (("static_unwind") ("static_gperftools") ("default"))))))

(define-public crate-gperftools-static-0.1 (crate (name "gperftools-static") (vers "0.1.2") (deps (list (crate-dep (name "fs_extra") (req "^1.1.0") (default-features #t) (kind 1)) (crate-dep (name "gperftools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)))) (hash "05idhpzhkmbisn9pssfgkd4bhmr4v0r2w2qpzqyalj8ld4phrw3p") (features (quote (("static_unwind") ("static_gperftools") ("default"))))))

