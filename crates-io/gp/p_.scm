(define-module (crates-io gp p_) #:use-module (crates-io))

(define-public crate-gpp_decrypt-0.1 (crate (name "gpp_decrypt") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.35") (default-features #t) (kind 0)))) (hash "0pnx2fvszz0z0sfaw41hqia8lgrzwdflx4wqf7m22dikw5a4vdlx")))

(define-public crate-gpp_decrypt-0.1 (crate (name "gpp_decrypt") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.35") (default-features #t) (kind 0)))) (hash "0zmajmq6fb7ibjx9idmw9s9q1ywimd0ni6bjndx5v91vff0rah0r")))

