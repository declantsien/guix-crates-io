(define-module (crates-io gp ac) #:use-module (crates-io))

(define-public crate-gpac-sys-0.9 (crate (name "gpac-sys") (vers "0.9.0") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "0qihddmr92pbjkcbashwayxrql0kf53l9m1cz7kr53v34zab8fdl")))

(define-public crate-gpac-sys-0.9 (crate (name "gpac-sys") (vers "0.9.2") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "10hqj2wwfgj223j26wwhrvh90dcw5g3x564n7izmg7ak6wr5hmcb")))

(define-public crate-gpac-sys-0.9 (crate (name "gpac-sys") (vers "0.9.3") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "17k0rp92ax27js9kyp7i1vygrnzix82ff1y8hvkiq9h2sxli22vi") (features (quote (("static"))))))

