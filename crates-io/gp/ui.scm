(define-module (crates-io gp ui) #:use-module (crates-io))

(define-public crate-gpui-0.1 (crate (name "gpui") (vers "0.1.0") (hash "1bwpm127z9k9ci8bqaspzzb8fsfliqhqi7q3n1f935am6xvzc2da")))

(define-public crate-gpui2-0.1 (crate (name "gpui2") (vers "0.1.0") (hash "13lnsiv5gdhr1m8wkz5qkydf91jsm1b9563l4zk95yyy7vn4m9mk")))

(define-public crate-gpuinfo-0.1 (crate (name "gpuinfo") (vers "0.1.0") (deps (list (crate-dep (name "nvml-wrapper") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1bya9gb2q1glzwxcbsfci18vci4x8fpdrsjmjbsq16si7gxbhghr")))

(define-public crate-gpuinfo-0.1 (crate (name "gpuinfo") (vers "0.1.1") (deps (list (crate-dep (name "nvml-wrapper") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "15lmcwjycyqlyqdfp9npg13sjhm84p52kfl0j071976m316a7gk7")))

(define-public crate-gpuinfo-0.1 (crate (name "gpuinfo") (vers "0.1.2") (deps (list (crate-dep (name "nvml-wrapper") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1r35b1sm2grb2hazjic7v6gm14965zwnrwv2r22k4xndbjcfzdkr")))

(define-public crate-gpuinfo-0.1 (crate (name "gpuinfo") (vers "0.1.3") (deps (list (crate-dep (name "nvml-wrapper") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "nvml-wrapper-sys") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0k88m6nn418if50qyxf7cnkrc8d5c0wfpxfi1ny8b3mf7cgbgnm8")))

