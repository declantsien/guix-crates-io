(define-module (crates-io gp tx) #:use-module (crates-io))

(define-public crate-gptx-0.1 (crate (name "gptx") (vers "0.1.0") (deps (list (crate-dep (name "gptrust_api") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vh098llc1s3v59ql3rlhhig35dd0nqv9cqa47mmm3c27rk404fi")))

