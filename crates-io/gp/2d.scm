(define-module (crates-io gp #{2d}#) #:use-module (crates-io))

(define-public crate-gp2d12-0.1 (crate (name "gp2d12") (vers "0.1.0") (deps (list (crate-dep (name "adc-interpolator") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0zdfkwsh119lxn5n20829xv85i5dga2dxa37nhgq9ajcrijkcsw2")))

(define-public crate-gp2d12-0.2 (crate (name "gp2d12") (vers "0.2.0") (deps (list (crate-dep (name "adc-interpolator") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0zkpsa9rif6rfdl6028pfmf58r96h0r38p86149flavyrjcqchik")))

(define-public crate-gp2d12-0.3 (crate (name "gp2d12") (vers "0.3.0") (deps (list (crate-dep (name "adc-interpolator") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.6") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "00ngi6q6rjrzkkl4d3a3wmh4rf1y7racqpsr7ccsllcj955as8qp")))

