(define-module (crates-io sk ew) #:use-module (crates-io))

(define-public crate-skew-forest-0.1 (crate (name "skew-forest") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "debug-tag") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1pa5y0252fyvc4ixs424fjxg2ffl24bx0rl803xxr7a56kxsxsxl")))

(define-public crate-skew-heap-0.1 (crate (name "skew-heap") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0d060msir0jqacg5dqjy6n6akl5q0sp2mcsy3jciz7aphwcgwk3q") (features (quote (("specialization") ("placement"))))))

(define-public crate-skew-heap-0.2 (crate (name "skew-heap") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1w096z2km01h1jlsm4lxxy61rx175yg2pw34ylzvwrmy7wwzldkc") (features (quote (("specialization") ("placement"))))))

