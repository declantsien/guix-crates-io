(define-module (crates-io sk sp) #:use-module (crates-io))

(define-public crate-skspring-rs-0.1 (crate (name "skspring-rs") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "10bx5w3l5cbsj6f8khkw7zdvky15wzpx468ldvmas0gglm70jxfh")))

