(define-module (crates-io sk ei) #:use-module (crates-io))

(define-public crate-skeid-0.1 (crate (name "skeid") (vers "0.1.0") (hash "1h3q4wa3sqn53q11qr43wf0gbv7wswzx6i2a49h3980j8qnw5lqm")))

(define-public crate-skeid-0.2 (crate (name "skeid") (vers "0.2.0") (hash "0mq39hx2ni3lzwwwba5nfsa2s3any76cz3mh41axnrr7vp9j40k5")))

(define-public crate-skeid-0.3 (crate (name "skeid") (vers "0.3.0") (hash "0fwn3ff5pijjbgm2frdslfqwysf3z3cm7yamqy53h2dp12v30xyl")))

(define-public crate-skeid-0.4 (crate (name "skeid") (vers "0.4.0") (hash "14z4qp9hjnks02rlg6mcks4vymp75gy1n9hd4g986kahs9rrj1cr")))

(define-public crate-skeid-0.5 (crate (name "skeid") (vers "0.5.0") (hash "15fpxrmpbmrznmis7p3h2ddl1lzy0z39bizpyv74c4wlbzjr0qag")))

(define-public crate-skeid-0.6 (crate (name "skeid") (vers "0.6.0") (hash "1g5qszwvhr8pk2iz4saqippbazsdkk96ljrz5v3ra92y9awwsf9x")))

(define-public crate-skeid-0.7 (crate (name "skeid") (vers "0.7.0") (hash "1951r61q0nzwspwqvz2gy499sbk3vnxwg8zwy7daasmxwy47fggl")))

(define-public crate-skeid-0.8 (crate (name "skeid") (vers "0.8.0") (hash "1l9r2ydbhpk0adl6y0331q2i5zcj2blkxcvljy6s80xv43gaz3hm")))

(define-public crate-skeid-0.9 (crate (name "skeid") (vers "0.9.0") (hash "00gxmwi65phff0a3w8n9a66qds2blyina4dnc6hkq0354iffyfpx")))

(define-public crate-skeid-0.10 (crate (name "skeid") (vers "0.10.0") (hash "01ijnghs7wv901w7ibyr9jjfbsxid35v71qs7cva710ikysaz09y")))

(define-public crate-skeid-0.11 (crate (name "skeid") (vers "0.11.0") (hash "194fnka4lj37zz7pq0jb758dip0s86qbni551aadlyn3qsjw4yq0")))

(define-public crate-skeid-0.12 (crate (name "skeid") (vers "0.12.0") (hash "054w5dann3zbhqhqlh7v5n9njnw8dnvv4v69b4rndcn5a1pvqzwv")))

(define-public crate-skeid-0.13 (crate (name "skeid") (vers "0.13.0") (hash "1nvgz1w9506m6c53pfvnp8107m3c07f95b8y7ays7gagqv3965lp")))

(define-public crate-skeid-0.14 (crate (name "skeid") (vers "0.14.0") (hash "0j4h6h0jaq23dbnq45rzqlmdbjimr11w2yhgqpg6j5zaj4b11j45")))

(define-public crate-skeid-0.15 (crate (name "skeid") (vers "0.15.0") (hash "0lfp22sml3janflplqx2ch37r32vsfi5n1llrjps6pg3kzvjg9y4")))

(define-public crate-skeid-0.16 (crate (name "skeid") (vers "0.16.0") (hash "1ajvkqwafcxn1pdq24f4sc4rh0hnhq2aq4vmc3250ard2qwfc25y")))

(define-public crate-skeid-0.17 (crate (name "skeid") (vers "0.17.0") (hash "1kfkrkqdm7bjr3jsl731f1b16s007vsfsnn869wasfpbjalimikk")))

(define-public crate-skeid-0.18 (crate (name "skeid") (vers "0.18.0") (hash "0y53hka6hln73sjbdqrpnw0d390lad8bmpvy9n7cafwbnn6aym30")))

(define-public crate-skeid-0.19 (crate (name "skeid") (vers "0.19.0") (hash "12xpj630irlnvic5vs9l6lzd411khf9vwabky9lid2zmslc3dkg7")))

(define-public crate-skeid-0.20 (crate (name "skeid") (vers "0.20.0") (hash "135mcqpav78n50sq1j7z6bab8jzbaz220jfpqss5jsnir1328hiv")))

(define-public crate-skeid-0.20 (crate (name "skeid") (vers "0.20.1") (hash "0im59nymf07z6l30chwymzzvdbcwy5yab9992dp7z5g14k297656")))

(define-public crate-skeid-0.20 (crate (name "skeid") (vers "0.20.2") (hash "0f1nx73fsmra0v2j282m0lk5b6xpli725wvmvsla7ydlnkhhhmgl")))

(define-public crate-skein-0.0.0 (crate (name "skein") (vers "0.0.0") (hash "10414nxpv9jba41pm48lv5rdkbggh8wfqsj2f8yym9aki0bi7hcy")))

(define-public crate-skein-0.1 (crate (name "skein") (vers "0.1.0") (deps (list (crate-dep (name "digest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "threefish") (req "^0.5.2") (kind 0)))) (hash "1kr5c5kb5pxzkgdfskyrhndww3nxcgg5qsxzjj8bcx7s2y92f4pl") (rust-version "1.57")))

(define-public crate-skein-ffi-0.0.1 (crate (name "skein-ffi") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "1r6m9rn3d6y1k7d469r462hzbcyx6n7ac6ccc21jmhqkbpxak3db")))

(define-public crate-skein-ffi-0.5 (crate (name "skein-ffi") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0.26") (default-features #t) (kind 1)))) (hash "1il7ynbpsjyggzzqy8pmpdpfgh8scv478qa488j3jq96rpgkxfyp")))

(define-public crate-skein-hash-0.3 (crate (name "skein-hash") (vers "0.3.0") (deps (list (crate-dep (name "block-buffer") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "block-padding") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "threefish-cipher") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1jnzqqmk9gh6wh75m97zrapj7m3dxq138a8v0yi1397690a9jr6d")))

(define-public crate-skein-hash-0.3 (crate (name "skein-hash") (vers "0.3.1") (deps (list (crate-dep (name "block-buffer") (req "^0.9") (features (quote ("block-padding"))) (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.9") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "threefish-cipher") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qiabpxa259g178l0dvxk8z64hakx0rg3c7kcbyg5i9s2fkgh9jp")))

