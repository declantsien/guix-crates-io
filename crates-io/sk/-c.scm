(define-module (crates-io sk -c) #:use-module (crates-io))

(define-public crate-sk-cbor-0.1 (crate (name "sk-cbor") (vers "0.1.0") (hash "0x2rz15r88vq1l0qhpsiglh24qzk976rywkrazmd017dawywr0cr")))

(define-public crate-sk-cbor-0.1 (crate (name "sk-cbor") (vers "0.1.1") (hash "1dxx198f0xgf7sm99qcq85axj2vziymhk6ivz47mg8ibyk61z8hp")))

(define-public crate-sk-cbor-0.1 (crate (name "sk-cbor") (vers "0.1.2") (hash "0yl4g3xwp8hjjrgrs9bq9a9cri3js77lbwwisrjnxfisg6d8g53y")))

