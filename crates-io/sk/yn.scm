(define-module (crates-io sk yn) #:use-module (crates-io))

(define-public crate-skynet-0.1 (crate (name "skynet") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0p66187xv599wfwc4asc6wfd6x2wzbph50h84dnrm232qchg3qpk")))

(define-public crate-skynet-0.1 (crate (name "skynet") (vers "0.1.1") (hash "0gzm9a6pjpl6ccwp3l6kzglz4ngm89jfqb6yzsrc3g2n18ny515m")))

(define-public crate-skynet-rs-0.1 (crate (name "skynet-rs") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("client" "http1" "http2"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mime_guess") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "textnonce") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1zarwzvrp705zws6h0nz34nid4ywgh9ybcw95mz5wwwxc385y6nl")))

(define-public crate-skynet_ai-0.0.1 (crate (name "skynet_ai") (vers "0.0.1") (hash "04pr6iqvqsc8j686a02n5k5mfz8cq2wmhdlkx3h1fvmlf7rd19rk")))

(define-public crate-skynet_ai-0.1 (crate (name "skynet_ai") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ibck7ghg55sxa3klk4x6l2sgphm3jm6pilsd8fhmd92012jh8qj")))

(define-public crate-skynet_ai-0.1 (crate (name "skynet_ai") (vers "0.1.1") (deps (list (crate-dep (name "flume") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "1mpm3qhx0qk0f174ksmm34zfjp8dmlnl8h2p6ff25j4j99903g88") (features (quote (("parallel" "rayon" "flume"))))))

(define-public crate-skynet_ai-0.1 (crate (name "skynet_ai") (vers "0.1.2") (deps (list (crate-dep (name "flume") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)))) (hash "1phnrl3086ivqycpky33zbg4y7ifx5p8sjfjg9yhzn9fqv58j45y") (features (quote (("parallel" "rayon" "flume"))))))

