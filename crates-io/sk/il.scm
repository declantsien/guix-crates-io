(define-module (crates-io sk il) #:use-module (crates-io))

(define-public crate-skill-rating-0.0.1 (crate (name "skill-rating") (vers "0.0.1") (hash "0v4qdm1i5w3iq8xiavkc9qc5kkd9cn3gx0w7ryzyaa55ghi469n4")))

(define-public crate-skill-tree-0.1 (crate (name "skill-tree") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1lijib5m9n6ccspy0108ghq0x7ji9d90jqnff04rbvnnqg70xrzp")))

(define-public crate-skill-tree-1 (crate (name "skill-tree") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0k1yczdyhzb4jzjjrvw30743v41xkbsd41hp0xkk4bqfl6pgfsf4")))

(define-public crate-skill-tree-1 (crate (name "skill-tree") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "04p325k84apnz7yl7my3h4fqjw3lxhhlkslddw69p3xr8amf9x9y")))

(define-public crate-skill-tree-1 (crate (name "skill-tree") (vers "1.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0zlw1sc0idyhgdxiryvd0029awhv5bnrsivpmsk8vh117iv9vzn2")))

(define-public crate-skill-tree-1 (crate (name "skill-tree") (vers "1.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "01hvlhlb8k91allq24xpq1sj7ld7kn5rsz729ab19i53bk2b5b3n") (yanked #t)))

(define-public crate-skill-tree-1 (crate (name "skill-tree") (vers "1.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^5.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "061m5rkj5dbd3wv1qqi50q2z1vml18ayanp0zgrlda4nch5d3ax5")))

(define-public crate-skill-tree-2 (crate (name "skill-tree") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0zq1s1a30w58v4dhvaxnvw6pwi4msidsjhx9gjb9449qxfvjfbnn")))

(define-public crate-skill-tree-3 (crate (name "skill-tree") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "prettydiff") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0li2956l8wa3shh19iq4kvv88b2p3r5c106iprhiax5khr9jq50z")))

(define-public crate-skill-tree-3 (crate (name "skill-tree") (vers "3.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "prettydiff") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "18xm2dz3rdsdchsw66jqzna25r4n39jvi099zxlq17pvvji1gx7z")))

(define-public crate-skillratings-0.1 (crate (name "skillratings") (vers "0.1.0") (hash "1fpm6k8w2d914c4pvd44fmkbzpmbnrr13can4g2s120911zaz7jl")))

(define-public crate-skillratings-0.2 (crate (name "skillratings") (vers "0.2.0") (hash "0bxr3m2id6cxqxvwpf6pywvgmcnl9am3g6dv7r9jwqk46s5a0ivi")))

(define-public crate-skillratings-0.3 (crate (name "skillratings") (vers "0.3.0") (hash "0ip2ggiv5lw6a09a6slwhccxzg6prz47mc1pzmkp057p8cg516lx")))

(define-public crate-skillratings-0.3 (crate (name "skillratings") (vers "0.3.1") (hash "1xlqvxpnyw4l1ijg54gan378k8q3m6gv9bl9k08gp9isycjhibz5")))

(define-public crate-skillratings-0.4 (crate (name "skillratings") (vers "0.4.0") (hash "1f9wwynsziy0vyam9117dz85fak61ays67fqpwj7a0fhlpmxpx39")))

(define-public crate-skillratings-0.4 (crate (name "skillratings") (vers "0.4.1") (hash "1mwzpjkcgw2f6fb6da0nyhsqjbalwplmmgjlkx0pgnyf26yicz4v")))

(define-public crate-skillratings-0.5 (crate (name "skillratings") (vers "0.5.0") (hash "1zikaydq634qi2lg5rjmjx0im9w25rcgmilkmp9xbmi97zpxcg8j")))

(define-public crate-skillratings-0.6 (crate (name "skillratings") (vers "0.6.0") (hash "09x23m4i98k2ljaxb0ym2h4ziskxwgp02jh169mxhbbpfnr1wmxb")))

(define-public crate-skillratings-0.7 (crate (name "skillratings") (vers "0.7.0") (hash "1kjkkw3vq8ag4kcrisgnv3sl7g8sazz5j6cbsi5ih1ahpii3sdyq")))

(define-public crate-skillratings-0.7 (crate (name "skillratings") (vers "0.7.1") (hash "17h839pq2ks04g07bzp9aq2zmhiqq6cwv87n2s7yciks5dbd16jq")))

(define-public crate-skillratings-0.7 (crate (name "skillratings") (vers "0.7.2") (hash "00kqxm69qbjq9w7csf3bljxnc3gqq878bipka20q7dmymq9zcjc7")))

(define-public crate-skillratings-0.8 (crate (name "skillratings") (vers "0.8.0") (hash "131wsj2yn3ksfhqiqx9jcksyaxpiwczc729nxsck0131rsryx8q2")))

(define-public crate-skillratings-0.9 (crate (name "skillratings") (vers "0.9.0") (hash "0bl36dqg711wigh8hjdpxkzdxhcp2nlm58b92fcnkzqas15mv6q2")))

(define-public crate-skillratings-0.9 (crate (name "skillratings") (vers "0.9.1") (hash "0bh88ak2qydlyihbflwihvvgxpdcm14fpknbgbpkbnby99qjr6jm")))

(define-public crate-skillratings-0.9 (crate (name "skillratings") (vers "0.9.2") (hash "1m9v60f5avyf3s9bcllzqxiv89sg0fja65vis01n9splx1qgy9lb")))

(define-public crate-skillratings-0.10 (crate (name "skillratings") (vers "0.10.0") (hash "03i28lry76kk8zbspxr05f3255wilyyrw60cnqq49s266spmfwcy")))

(define-public crate-skillratings-0.10 (crate (name "skillratings") (vers "0.10.1") (hash "1j2l9rg7ldy1gfal0lbcccnw7bsd0sllfhn1sj6zg1xc5scgiqiv")))

(define-public crate-skillratings-0.10 (crate (name "skillratings") (vers "0.10.2") (hash "05wc9c5w1rsn0kc3x5czbih7yif8jyx7kk6rr5n22zgsf9ksxhki")))

(define-public crate-skillratings-0.11 (crate (name "skillratings") (vers "0.11.0") (hash "0awdic0yccs9l9snirsbxhkdp30s0hb1y72vcbxkvvv4n3vssgbz")))

(define-public crate-skillratings-0.12 (crate (name "skillratings") (vers "0.12.0") (hash "04jxsy98h62nswj3dzw7f4sgc8liyhmcyc9a89zr29y4lwasddih")))

(define-public crate-skillratings-0.13 (crate (name "skillratings") (vers "0.13.0") (hash "05hbz7wp22ggi638lqn415vpczgl3k31lg5k6wqfcsfs7y6vd7sr")))

(define-public crate-skillratings-0.13 (crate (name "skillratings") (vers "0.13.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1ciygp1vfvqgpkw448i4x2jyn74mf3xmww40r2hs3qjgbg1fz7ck")))

(define-public crate-skillratings-0.13 (crate (name "skillratings") (vers "0.13.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1hbgxi03xka8bqy5fdk1i98bsr8z1q4nijh834jnwr1djqrjp5ya")))

(define-public crate-skillratings-0.13 (crate (name "skillratings") (vers "0.13.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1l3517slhw8vz93376hml76b9y1kdgg09l550bs9ib08k2rmvkq4")))

(define-public crate-skillratings-0.13 (crate (name "skillratings") (vers "0.13.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "00b2586ikdz9p88whghw6vgz60v2iy7w55k382f4xwrlsbjmggv2")))

(define-public crate-skillratings-0.14 (crate (name "skillratings") (vers "0.14.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0l6r3q9d6xix5al87z9apcyvxpcyd3q5hxa96grs69h8j41mp68s")))

(define-public crate-skillratings-0.15 (crate (name "skillratings") (vers "0.15.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0qkkgad02wwgypqiffv12lcnrc9wlc9j78c7gn8080h64f4iann0")))

(define-public crate-skillratings-0.15 (crate (name "skillratings") (vers "0.15.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1gp6pd3g4j0x1h6ishgpp8r62jlzifjb5n7paz5ivr3kpfyskvvb")))

(define-public crate-skillratings-0.15 (crate (name "skillratings") (vers "0.15.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0ncg476ks2dz2wsg2cr5l524ablfjnxi3aqcpw9l7m2ky3paw3cz")))

(define-public crate-skillratings-0.16 (crate (name "skillratings") (vers "0.16.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "03p1dr9a2cyj81ahq8ljxxahw65w77vyzq5fjcrx2sqamnhvwypz")))

(define-public crate-skillratings-0.17 (crate (name "skillratings") (vers "0.17.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0kr2wr31i9as5wbd7p1vdgqsigw5ysyz97jvcx96g4w8wpl2grrz")))

(define-public crate-skillratings-0.18 (crate (name "skillratings") (vers "0.18.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "054n8290aq71f6sm691bv9v1n5zgnsi2scadcf17p2x6fsp26sdw")))

(define-public crate-skillratings-0.19 (crate (name "skillratings") (vers "0.19.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0flcbfdmj9i1563ladc478d0szszvw88grmmp6ara4w434wii3m5")))

(define-public crate-skillratings-0.19 (crate (name "skillratings") (vers "0.19.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1z4brxha7wnpkzpbimx3vbz9w6kfz9hl39hiafc3lnnsl5j1cmyg")))

(define-public crate-skillratings-0.19 (crate (name "skillratings") (vers "0.19.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0lp6gpf8jw8207jziglpy26jbamq219vj05bnrwldrd9zm0j8m93")))

(define-public crate-skillratings-0.20 (crate (name "skillratings") (vers "0.20.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0f79z771hb0yniir1cfjjzwx4hpjhf4aspsf0jdx1rbznsigvd1y")))

(define-public crate-skillratings-0.21 (crate (name "skillratings") (vers "0.21.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "041qakz4gbax9mnfffjaqqnyrkp0izpi3dmjqx7h54di8zrp62cg")))

(define-public crate-skillratings-0.21 (crate (name "skillratings") (vers "0.21.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wfsjpbln89jzy0hsb0d3j3r5d791xakag2jfa0q518nr5b2jy9d")))

(define-public crate-skillratings-0.22 (crate (name "skillratings") (vers "0.22.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1zrjk3d9wvhw7cmzm30y7fx1zbn89z74sx63vg12bwq6b0k1ym1q")))

(define-public crate-skillratings-0.23 (crate (name "skillratings") (vers "0.23.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1i9byw18dsydjs46bjjhx3j7z46dm79i7w0r7zq9v7faqfjsik69")))

(define-public crate-skillratings-0.24 (crate (name "skillratings") (vers "0.24.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mq5hazsb6mlb6q4i1bws1zqyyyahzjff26v69aqvmf9b4h214ya")))

(define-public crate-skillratings-0.25 (crate (name "skillratings") (vers "0.25.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1alr4m82kspjl4pvdc593qjpci92w7mpxyk6dkcgdl9gw0583ac2")))

(define-public crate-skillratings-0.25 (crate (name "skillratings") (vers "0.25.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1sj0qasbzxf2r9djmdlikq3zv4m5bcnfnaz1z5yfm83smqrc79g8")))

(define-public crate-skillratings-0.26 (crate (name "skillratings") (vers "0.26.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0viz10ihx6a65j3f9h7zp2d131jif7zs6ajzdsasxyry2rp325h2")))

