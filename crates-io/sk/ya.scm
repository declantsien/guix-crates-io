(define-module (crates-io sk ya) #:use-module (crates-io))

(define-public crate-skyangle-0.1 (crate (name "skyangle") (vers "0.1.0") (hash "1anzncapzij2ilbg9w5d7mpxvv284sl1idm0h72ssxs6kndabpcp")))

(define-public crate-skyangle-0.1 (crate (name "skyangle") (vers "0.1.1") (hash "1zdywsks91mqrdgqf7qrwyqa7bm19hqmjf518jpi627jxc956ms4")))

(define-public crate-skyangle-0.1 (crate (name "skyangle") (vers "0.1.2") (hash "0i350vm2prxcc0s8ai4zas5apk3v6cn19bviisqqkwr643hz1w0l")))

(define-public crate-skyangle-0.2 (crate (name "skyangle") (vers "0.2.0") (hash "0k6nzhm9sdrbi9w4vq8jqvgjibkxhm1cr9627q6x4798dyv1j329")))

(define-public crate-skyangle-0.2 (crate (name "skyangle") (vers "0.2.1") (hash "125fbkz2hgwswx2didgf1w7wkwahcf9ny4pf3f260q2sjrnsry7i")))

(define-public crate-skyangle-0.2 (crate (name "skyangle") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 2)))) (hash "1zmw307fh3cy37rac5m37ijdh73cl283p4pxj5wsgp0swifx9hlr")))

