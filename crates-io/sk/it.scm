(define-module (crates-io sk it) #:use-module (crates-io))

(define-public crate-skit-0.1 (crate (name "skit") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0b93b3bp2xfbzwbkpl09pfmsrylhm1gy2b177sph5fcnb6dn0rmy")))

(define-public crate-skit-0.2 (crate (name "skit") (vers "0.2.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "185ppkkhhvvsjflaan1kc6gmglpa6rjflx2wkh99j0xp91r302lg")))

(define-public crate-skit-0.2 (crate (name "skit") (vers "0.2.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1m6xm926n5ins0jkhqsfyk95h65djriy0w8khg7pgl0fqn93wn3s")))

(define-public crate-skit-0.3 (crate (name "skit") (vers "0.3.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "1ad3xz5kwxkdpqlqm8432v9gpj24kxw9shbif6kpz7mq89nf2lgr")))

(define-public crate-skitter-0.0.0 (crate (name "skitter") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1jpd2p435h2sdmnd36h3bjjjxsvil1zpir1jh10jsgi9nxm5j9i0")))

(define-public crate-skittles-0.1 (crate (name "skittles") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0zhqhdrrr0css7cqahnxhcic7721inrlz34j2x7113w5gq7mxxnd")))

(define-public crate-skittles-terminal-0.0.1 (crate (name "skittles-terminal") (vers "0.0.1") (hash "1jbymb7ch3jg41qc5iymka6cg3sv6d4l9hf56gm5k754a2ajn7g6")))

(define-public crate-skitty-0.1 (crate (name "skitty") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1q82h21gh39j4bfbwgz4gp6r516fv6f6v9r9m8fgwnv44bxqqvfb")))

