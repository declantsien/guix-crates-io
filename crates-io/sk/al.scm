(define-module (crates-io sk al) #:use-module (crates-io))

(define-public crate-skald-0.1 (crate (name "skald") (vers "0.1.0") (hash "1jkdf71ax28wvyk9bm0krli3zar432hqizg65gybxybniwly70n5")))

(define-public crate-skale-0.1 (crate (name "skale") (vers "0.1.0") (hash "17cgmzs9scw09dd0fjggnmf6v0d5558a7nkqsjfypdikfb334czz") (rust-version "1.64.0")))

