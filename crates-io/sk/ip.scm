(define-module (crates-io sk ip) #:use-module (crates-io))

(define-public crate-skip-0.0.0 (crate (name "skip") (vers "0.0.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0vq1mnqfcwrd4pwm12k3bbig11mq407bnnaj407xyfghb4x9lf53")))

(define-public crate-skip-0.2 (crate (name "skip") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1c3kscvfcy3yrwwzmq5j2w5kr69cqfvm9bc6z4537z3178hxiss2") (rust-version "1.74.1")))

(define-public crate-skip-fail-1 (crate (name "skip-fail") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0gc5skrwq62930wh1k7k9zlxvfz5mw2m8871xbfnhpihk95mmh4p") (yanked #t)))

(define-public crate-skip-linked-list-0.1 (crate (name "skip-linked-list") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0ysmn7b4920zla835l0a9ymlrp2r075vkhv32br241sflwzgypq5")))

(define-public crate-skip-linked-list-0.1 (crate (name "skip-linked-list") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1j29yz5gyracfidnwl152zlicz0d0yawynbv357ijfv2rhm2la93")))

(define-public crate-skip-list-0.1 (crate (name "skip-list") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0skm06z7n13w1dfbkj7xya30650pjrhi1zxdyw60ngmig99bb3al")))

(define-public crate-skip-list-0.1 (crate (name "skip-list") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wzd9bv85xs10qgvffxrwn17m2cq1afq41a8r7578xz0gm4d1dkh")))

(define-public crate-skip-list-0.1 (crate (name "skip-list") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02m9bn447myg1a2llzsp2xmrc3d166jh8lyjrrlas8a7fymm9vir")))

(define-public crate-skip-list-0.1 (crate (name "skip-list") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16s6rsc0km1hv5ychvajv9pz4wcs1avfwfpwawdpy6z08xsx570v")))

(define-public crate-skip32-0.1 (crate (name "skip32") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1v8zhdkf2nfyblwcrcdrxb3gjmdqvcwr2ji9syfxccg7rhqkpyba")))

(define-public crate-skip32-0.1 (crate (name "skip32") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0w0575pzb6ybbxfk3xz4iva2a88xz22qw9g0ijj0qmm17fx256rb")))

(define-public crate-skip32-1 (crate (name "skip32") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "05vjj1cd5jp7yi8dmxab5h8wwbxkx0mvkn58f5rqr8gwrs7m2aj0")))

(define-public crate-skip32-1 (crate (name "skip32") (vers "1.0.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "16kzhiffh2p4incrrpbc4m3niq8w04sx9akr1kx4hnr6s7yq68h9")))

(define-public crate-skip32-1 (crate (name "skip32") (vers "1.0.2") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "17v3r50m9fi8p88f7ighpsw44if260q9qjcaj5jzfv0zix1bv7xq")))

(define-public crate-skip32-1 (crate (name "skip32") (vers "1.0.3") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "15yy1bxy3q1k85jljc57s0nmyfsgyq470d7jmv0d9p9bcmzmap7c") (yanked #t)))

(define-public crate-skip32-1 (crate (name "skip32") (vers "1.0.4") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "1r5dzxgv98ciz54s2bn4lb90n1ivdq5d1rf0nqrm2qamj4h3xscr")))

(define-public crate-skip32-1 (crate (name "skip32") (vers "1.0.5") (deps (list (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0xj7z2niy5qf2cl51h6c0m82w5vi9qy4qwr4z1isvibw9w4n15vg")))

(define-public crate-skip_bom-0.2 (crate (name "skip_bom") (vers "0.2.0") (hash "158myg5sww0frsy113yc11mmagp4zhy4knx02qgr764h90alsczj")))

(define-public crate-skip_bom-0.2 (crate (name "skip_bom") (vers "0.2.1") (hash "0jd1y37km1bni1w9sl4mx0ridfl5fr7hjl5a0i7lghfifs0by09g")))

(define-public crate-skip_bom-0.2 (crate (name "skip_bom") (vers "0.2.2") (hash "0051hr7w8pn45d6vsc146vdv14jyz304vhilr1xagr3cyi9ckjby")))

(define-public crate-skip_bom-0.2 (crate (name "skip_bom") (vers "0.2.3") (hash "1fzphwfiq2bmsf20z9sx48gds2lqlyksx3wb5bwh4k8llv8lfc28")))

(define-public crate-skip_bom-0.3 (crate (name "skip_bom") (vers "0.3.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "1z0c60y786cb809713fwka7c3zscdh8mhk6q45qm1ph6j0f0bz0i")))

(define-public crate-skip_bom-0.4 (crate (name "skip_bom") (vers "0.4.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "0mgya4cdiq7rg8dhj5krpgbg1jchlb4sswi82g23vf9wgrhcxhpr")))

(define-public crate-skip_bom-0.5 (crate (name "skip_bom") (vers "0.5.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "10347x6y21hvi93d4fizy8j5dai1i33b5gcxsdn1bal5iq18c548")))

(define-public crate-skip_bom-0.5 (crate (name "skip_bom") (vers "0.5.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)))) (hash "0jf5x1jfd0n2p9jql8ipsxjld9ifhz179bpgsk9g5xgmv3wssrdn")))

(define-public crate-skip_do-0.1 (crate (name "skip_do") (vers "0.1.0") (hash "073rrfb3j187wpvm5bkk8dz3d9z7grf7v5cp7kpd3saql3h0b6gl")))

(define-public crate-skip_error-1 (crate (name "skip_error") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "173q8lnacr4saa22sxpqr2f5ilp8vrfr80g16ljyky5srfj82k5l")))

(define-public crate-skip_error-1 (crate (name "skip_error") (vers "1.0.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "022a59pc5a6g5hz52k2v42b0ayhh2b45kzqil98dnki6z2jgxx7i")))

(define-public crate-skip_error-1 (crate (name "skip_error") (vers "1.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0yajy2c9p7qihz7s1f3swngglhkah4ayak70b2blhx92v95vkvcn")))

(define-public crate-skip_error-2 (crate (name "skip_error") (vers "2.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "testing_logger") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (optional #t) (default-features #t) (kind 0)))) (hash "102wjb3vckinppl5cihkzqs2z7ysab4qws7rqjagzffp7gvk3i8k")))

(define-public crate-skip_error-3 (crate (name "skip_error") (vers "3.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "testing_logger") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jlvkpsjcka4sdwxw73xdqfsqkzg9xzrsi69zshjc0lbln5bqsk0")))

(define-public crate-skip_error-3 (crate (name "skip_error") (vers "3.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "testing_logger") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (optional #t) (default-features #t) (kind 0)))) (hash "12avaci6873jzvpbhqgan6c0rywi4kn64702v91zynyshw1hfhv4")))

(define-public crate-skip_error-3 (crate (name "skip_error") (vers "3.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "testing_logger") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("log"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wsi1mbq8pkda1h33zbbbswrll8jnjjrrxsyi302hkxr3j0bn5qa")))

(define-public crate-skip_it-0.1 (crate (name "skip_it") (vers "0.1.0") (hash "060azx4wmnkpg8yn1zai87jp8amkmsqpjcg4h907wmy7mp6ba1w4")))

(define-public crate-skip_it-0.1 (crate (name "skip_it") (vers "0.1.1") (hash "0kgi7v57gnsjf2f8qfi9sd5ds345gq41gf3gjj4v5arq65bgrz47")))

(define-public crate-skip_ratchet-0.1 (crate (name "skip_ratchet") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "=0.13.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "=0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "=0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "=0.10.0") (default-features #t) (kind 0)))) (hash "0n9a4nqx4fjbhclrfva2ywcsvb8m8i845sppqy0i818b5rrb74jd") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-skip_ratchet-0.1 (crate (name "skip_ratchet") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "=0.13.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "=0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "=0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "=0.10.0") (default-features #t) (kind 0)) (crate-dep (name "test-strategy") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1ylqrzx4c72b97170qylcxwx8z7a1phdsb34axz9m0a0pk0g7067") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-skip_ratchet-0.1 (crate (name "skip_ratchet") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "=0.13.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "=0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "=0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "=0.10.0") (default-features #t) (kind 0)) (crate-dep (name "test-strategy") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0c1cgky3qksbmchdm0ivr3h03ll0vgmxjb7hdc1wkylnjnzf9vvk") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-skip_ratchet-0.1 (crate (name "skip_ratchet") (vers "0.1.6") (deps (list (crate-dep (name "base64") (req "=0.13.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "=0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "=0.6.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "=0.10.0") (default-features #t) (kind 0)) (crate-dep (name "test-strategy") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0yamlabkr5skaxch1scx8krp6qbskxfrrk63303gw4vgm3w43vhn") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-skip_ratchet-0.2 (crate (name "skip_ratchet") (vers "0.2.1") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libipld") (req "^0.16") (features (quote ("dag-cbor" "derive" "serde-codec"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "test-strategy") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wf26hna98rqjkfhf3y2rv9ca94i5g736iaqvax1gf3i0sp6smwx") (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_bytes"))))))

(define-public crate-skip_ratchet-0.3 (crate (name "skip_ratchet") (vers "0.3.0") (deps (list (crate-dep (name "blake3") (req "^1.4") (features (quote ("traits-preview"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libipld") (req "^0.16") (features (quote ("dag-cbor" "derive" "serde-codec"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-strategy") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ds1j71flksk2793r1n1hda515p3yc3nx0cvalg0lv36rgw42prr") (v 2) (features2 (quote (("serde" "dep:serde" "dep:serde_bytes"))))))

(define-public crate-skipchannel-1 (crate (name "skipchannel") (vers "1.0.0") (hash "0ym6ahf4l0sc161g6kqgf73nvx5hlvyq9847wgvgp6cqkcjrz3xv")))

(define-public crate-skipchannel-2 (crate (name "skipchannel") (vers "2.0.0") (hash "14d9wyryj9j961j5v8xc2s0844kr2xx6gyr6ccqd4da916s6xmry")))

(define-public crate-skipchannel-2 (crate (name "skipchannel") (vers "2.0.1") (hash "19h26i92zdj0lbc0k68b07crlgp7nn8r7i4jfc4y6psmdz0vyhka")))

(define-public crate-skipdb-0.1 (crate (name "skipdb") (vers "0.1.0") (deps (list (crate-dep (name "cheap-clone") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "skipdb-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "txn") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "wmark") (req "^0.1") (default-features #t) (kind 2)))) (hash "11r6wkcl32dax1l458sbzas1dxn7dc808567v1hjddxpn0pibpfy") (features (quote (("default")))) (yanked #t) (rust-version "1.75.0")))

(define-public crate-skipdb-0.1 (crate (name "skipdb") (vers "0.1.1") (deps (list (crate-dep (name "cheap-clone") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "skipdb-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "txn") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "wmark") (req "^0.1") (default-features #t) (kind 2)))) (hash "0y2pgx6likrbw2w39b03jr9p730pddh0qjg6wlbwyv0xzcwgg1ic") (features (quote (("default")))) (yanked #t) (rust-version "1.75.0")))

(define-public crate-skipdb-0.1 (crate (name "skipdb") (vers "0.1.2") (deps (list (crate-dep (name "cheap-clone") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "skipdb-core") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "txn") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "wmark") (req "^0.1") (kind 2)))) (hash "0jcd0czsr2yz99c7cns53kjjl35hnp249xqcxd43yi1ghc45m4m0") (features (quote (("default")))) (yanked #t) (rust-version "1.75.0")))

(define-public crate-skipdb-0.1 (crate (name "skipdb") (vers "0.1.3") (deps (list (crate-dep (name "cheap-clone") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "skipdb-core") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "txn") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "wmark") (req "^0.1") (kind 2)))) (hash "062hrw81rzwbpgm6qwmqrrvssc84dlng8f0m57sylszvaadkq18a") (features (quote (("default")))) (yanked #t) (rust-version "1.75.0")))

(define-public crate-skipdb-0.1 (crate (name "skipdb") (vers "0.1.5") (deps (list (crate-dep (name "cheap-clone") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "skipdb-core") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "txn") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "wmark") (req "^0.1") (kind 2)))) (hash "0jkqg8099ldaqdcb3jg9fb4vn2nrir4ji2phrzb8gy10qwqksals") (features (quote (("default")))) (yanked #t) (rust-version "1.75.0")))

(define-public crate-skipdb-0.2 (crate (name "skipdb") (vers "0.2.0") (deps (list (crate-dep (name "cheap-clone") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "skipdb-core") (req "^0.2") (features (quote ("default"))) (kind 0)) (crate-dep (name "txn") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "wmark") (req "^0.1") (kind 2)))) (hash "0r173b16alkwh9v59iagxnmfm78wdxsa2aa94z2gvx6crack8hvz") (features (quote (("default")))) (rust-version "1.75.0")))

(define-public crate-skipdb-0.2 (crate (name "skipdb") (vers "0.2.1") (deps (list (crate-dep (name "cheap-clone") (req "^0.1") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "skipdb-core") (req "^0.2") (features (quote ("default"))) (kind 0)) (crate-dep (name "txn") (req "^0.1") (features (quote ("default"))) (kind 0)) (crate-dep (name "wmark") (req "^0.1") (kind 2)))) (hash "0497qs21i5d9m1jzsgv30k7z0jvxrnqwyg7xgqfmbyzcpdphlc0j") (features (quote (("default")))) (rust-version "1.75.0")))

(define-public crate-skipdb-core-0.1 (crate (name "skipdb-core") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smallvec-wrapper") (req "^0.1") (features (quote ("const_new"))) (default-features #t) (kind 0)) (crate-dep (name "txn-core") (req "^0.1") (kind 0)))) (hash "08afnyiv4wf2czjk5mb89b6sfys3fjrfgm526w48jf8kf55v4hj4") (rust-version "1.75.0")))

(define-public crate-skipdb-core-0.1 (crate (name "skipdb-core") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smallvec-wrapper") (req "^0.1") (features (quote ("const_new"))) (default-features #t) (kind 0)) (crate-dep (name "txn-core") (req "^0.1") (kind 0)))) (hash "1zp4f1g19iy60mfq7xxlhmw7wxk018h5q5jhykcz23l3wavqgd1v") (rust-version "1.75.0")))

(define-public crate-skipdb-core-0.1 (crate (name "skipdb-core") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smallvec-wrapper") (req "^0.1") (features (quote ("const_new"))) (kind 0)) (crate-dep (name "txn-core") (req "^0.1") (features (quote ("alloc"))) (kind 0)))) (hash "0sg0zzyyhjnmc9aw64q9y22l9k796jspsqi7cx7wqi3v5i3d7mfq") (features (quote (("std" "smallvec-wrapper/std" "crossbeam-skiplist/default" "txn-core/std" "either/default") ("default" "std")))) (rust-version "1.75.0")))

(define-public crate-skipdb-core-0.2 (crate (name "skipdb-core") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smallvec-wrapper") (req "^0.1") (features (quote ("const_new"))) (kind 0)) (crate-dep (name "txn-core") (req "^0.1") (features (quote ("alloc"))) (kind 0)))) (hash "071z98aqlx1wabfs0k54mh5b1df3skaywvjsxa23xafx59qn04df") (features (quote (("std" "smallvec-wrapper/std" "crossbeam-skiplist/default" "txn-core/std" "either/default") ("default" "std")))) (rust-version "1.75.0")))

(define-public crate-skipfield-0.1 (crate (name "skipfield") (vers "0.1.0-alpha") (hash "1x4szv2rqmavmj49369rqhcrizm4ixkwp9mv3cd1z2589d15bf6i")))

(define-public crate-skipfree-0.1 (crate (name "skipfree") (vers "0.1.0") (deps (list (crate-dep (name "guacamole") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1fkmjmhbw1n5q69m6afq5dp2czj9vksdmwynmz36gczf517liskh")))

(define-public crate-skipfree-0.2 (crate (name "skipfree") (vers "0.2.0") (deps (list (crate-dep (name "guacamole") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0l4afy9ncwnixpcxnpr4rqrmv6kr34cr0jw8s53qr0kfnz8xdr1v")))

(define-public crate-skipjack-0.1 (crate (name "skipjack") (vers "0.1.0") (hash "0pmw1spjgxz5jaqvp850iwcd88knpb7wm0syyr3sx38wj7byj1qj")))

(define-public crate-skipjack-0.2 (crate (name "skipjack") (vers "0.2.0") (hash "0mxsf075j6ymv71bjvq8k6vl9dak4riigh23175pj9bk5gznri1i")))

(define-public crate-skipjack_rs-0.1 (crate (name "skipjack_rs") (vers "0.1.0") (deps (list (crate-dep (name "cipher") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "criterion-cycles-per-byte") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.14.5") (default-features #t) (kind 0)))) (hash "079xf4bdmbmzabqzwf40s89djr3vrl2qjh065kw00l09hmhklrmy")))

(define-public crate-skiplist-0.1 (crate (name "skiplist") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "07ynk94mgcvhad536plyl21lwnj8iw7f7gykr51w5kn2rivhg8p3")))

(define-public crate-skiplist-0.1 (crate (name "skiplist") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0d62hrlffgirx6ncjv2pmyzzzkchff9wz5l8k11fiyc0pns51a9b")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0xbn9jqrqawpbsfvs0cj45amswmrj69lxhgdb1dv9i77ibvmfry7")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1iq46d5x67z55crffs0s6dvkpbkbvdblj59spk0l7b3x0r3w321a")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0wsj2aw5yrypm2w9avl6akndwd5bi1xkrdcy6ziz8h0fd7wrq924")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "185p4sdsnwa4h5r3shazivl53gjxqj6d920pzvl729fvjbrz9w9f")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0m8pd84kq1q3x61l60rdibm9lz4z6j7qs5zdqvh2jxsf21r3wa54")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "09j9klq3wa09qqq57ym4b8c3kmdwd8cnnml51lsg2wwliw91pl34")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "12678pbzriwxnrd7gchql9y1cfh1b0i9qflm23f2v9y9nnlggvk7")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.7") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1lzgp0n3bf8g6ajcz69nxisxxzdm01asxjb3arh36phrflwbzf66")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.8") (deps (list (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "1rb7z44wg385mx17g936m9ig8zhcy7hy0988ln75vnj7lfg2xhz0")))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.9") (deps (list (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "088zx61yggjmljajkdw25q6jy5vk2np0j6x5887rda484b9iq2l0") (features (quote (("unstable"))))))

(define-public crate-skiplist-0.2 (crate (name "skiplist") (vers "0.2.10") (deps (list (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "157b91jdww4rw86bki4v83mny8dr7cz4r362x9gm52g7yvi7wwxp") (features (quote (("unstable"))))))

(define-public crate-skiplist-0.3 (crate (name "skiplist") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0msgscnn87q325j0gm0h02rva1igh8vi1dswarsgc2jw028yb0l7")))

(define-public crate-skiplist-0.4 (crate (name "skiplist") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0ny9qjxd22090clhvbf1mlnx9k1ncvqmbzis25fpc2a65dx38dzs")))

(define-public crate-skiplist-0.5 (crate (name "skiplist") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "16xa2mg3n82iqsj5gdizv4pzj8nhq2w4q6czvfhxhqqljms3dkdx")))

(define-public crate-skiplist-0.5 (crate (name "skiplist") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1xn3rjz322f65vk7i0qsx0dy0n0v1dwc527k09gdrz33cks2bv0f")))

(define-public crate-skiplist-rs-0.1 (crate (name "skiplist-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0ji8ddz7jxm631bbv5zlw03ds0awk15hih62c7b49hi3462a58aq")))

(define-public crate-skiplist-rs-0.1 (crate (name "skiplist-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0g5mwbc3djpqkrc7m51ar0w82q4xbv4k8r7j4q9qq9a5fdyd00k1")))

(define-public crate-skiplist-rs-0.1 (crate (name "skiplist-rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0p768j43gb8s7dj6z3cwy9589s03xhhjf3b6j3bv227c3gp7k2g7")))

(define-public crate-skiplist-rs-0.1 (crate (name "skiplist-rs") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0r69f6l1zav6wwc4q4zi3rksmyy5pj0zarrcga7yd8gbwv4dwzdz")))

(define-public crate-skiplist-rs-0.1 (crate (name "skiplist-rs") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "17hhdnrdf1h6c9g895j86pfsaqjj2w483dqbjvpx3rc5lzwyyghf")))

(define-public crate-skiplist-rs-0.1 (crate (name "skiplist-rs") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0xylan63x79svzb5rk5ijz4li740c2699b4fhgq0mspx4i2dcccz")))

(define-public crate-skiplistdb-0.0.0 (crate (name "skiplistdb") (vers "0.0.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1hqbl09g35s60w3dhzy51sml75fc10gpih1ai14fzylwlrnz9s45") (features (quote (("default"))))))

(define-public crate-skipngo-0.0.0 (crate (name "skipngo") (vers "0.0.0") (hash "1blxwz7hili1m96kykk2frg0wagmfjbl9i0akakv4srac5dxzvcs")))

(define-public crate-skippable_map-0.1 (crate (name "skippable_map") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "198lcbii3mh6svi63mkf0rxfq69zk5yj59yjyxa56slxg0zlxb46")))

(define-public crate-skippable_map-0.1 (crate (name "skippable_map") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "1qh03j4jnjr26arcfvwavnv2f4qhainnys251i9kwqhb848l3m9x")))

(define-public crate-skipper-0.1 (crate (name "skipper") (vers "0.1.0") (hash "04vkshpfy49ri1yxwm4mkw42cf1yjb0zmxfxjg0l9zx19qmcwi4a") (yanked #t)))

(define-public crate-skipping-search-0.1 (crate (name "skipping-search") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.7") (default-features #t) (kind 2)))) (hash "1cbzqxj8bavi88hpfvd8s1azlrrd5jmh07ag7mg0dfwyanp9imlx")))

(define-public crate-skippy-0.0.0 (crate (name "skippy") (vers "0.0.0") (hash "1cx56kf0921gy3vyipid4xw21yf5agy9204h3id8pksbvbiq3s9r")))

(define-public crate-skippy-rs-0.0.0 (crate (name "skippy-rs") (vers "0.0.0-alpha.0") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wrqw3bsbhx5q0crgj0apc28yvsii1qjq736179jql3m26fjq49b")))

(define-public crate-skippy-rs-0.0.0 (crate (name "skippy-rs") (vers "0.0.0-alpha.1") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1a43dknjnd9xhw3j6qwwfbj3hycrkzvcvh6wqq4rciph5wqm12vs")))

(define-public crate-skippy-rs-0.0.0 (crate (name "skippy-rs") (vers "0.0.0-alpha.2") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "07lw3nvlj0j1db2xa7wr1g86amwfcvc28kqnpk3r7s31djaj1052")))

(define-public crate-skippy-rs-0.0.0 (crate (name "skippy-rs") (vers "0.0.0-alpha.3") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0chxyy213w5wyfn6fn9jcfgx705fy5lcs9dfscj8yr9ldp2kck4x")))

(define-public crate-skippy-rs-0.0.0 (crate (name "skippy-rs") (vers "0.0.0-alpha.4") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "haphazard") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1a9iah56acplag1anw2jdp3ngb4sbq9n5qqdla96g266fkbs63gf")))

