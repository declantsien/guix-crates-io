(define-module (crates-io sk #{68}#) #:use-module (crates-io))

(define-public crate-sk6812_rpi-0.1 (crate (name "sk6812_rpi") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.13") (default-features #t) (kind 0)))) (hash "1yvxmhd7jw3iv92na328zvsg1dm9r6gpzmnk1yc8s03fichbdiyn")))

(define-public crate-sk6812_rpi-0.1 (crate (name "sk6812_rpi") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.13") (default-features #t) (kind 0)))) (hash "16py1bkp45jnzmmna8zq42cfvdp4zrw0c09j0ki5m5c0d55rycfn")))

(define-public crate-sk6812_rpi-0.1 (crate (name "sk6812_rpi") (vers "0.1.2") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.13") (default-features #t) (kind 0)))) (hash "0q3vca2ymk58bfvbkjs5vkra7901hcx1za0sga6nvkc1m746j93v")))

