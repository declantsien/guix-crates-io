(define-module (crates-io sk ni) #:use-module (crates-io))

(define-public crate-sknife-0.1 (crate (name "sknife") (vers "0.1.0") (hash "11vfqidw64l6rhpbpr5kicjri4nm8sl0bj61p3wn7f6ja7qkpziz")))

(define-public crate-sknife-0.2 (crate (name "sknife") (vers "0.2.0") (hash "0qbp1yd519kdhdgcga6m1c87c8lbrc1a2p0d7givqgvgkyig2k39")))

(define-public crate-sknife-0.2 (crate (name "sknife") (vers "0.2.1") (hash "0rncmv290zl4q4aa0l31arc7rz4d5p7q4z3df6s1j79kb2vkjbgp")))

(define-public crate-sknife-0.2 (crate (name "sknife") (vers "0.2.2") (hash "1pzwqvam2d6zjj3jvkq5ibsrd6azbmpppsmk5imdmbckd90jrjm0")))

