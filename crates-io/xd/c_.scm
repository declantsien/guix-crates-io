(define-module (crates-io xd c_) #:use-module (crates-io))

(define-public crate-xdc_macros-0.1 (crate (name "xdc_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0yz3c27n92mhwcap9fv5zl8f4ir215iw52wg2wg7gdannx0jc72y") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

