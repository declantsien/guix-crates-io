(define-module (crates-io xd rk) #:use-module (crates-io))

(define-public crate-xdrk-1 (crate (name "xdrk") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fcm7rs8izs2magx6h8mj17k8i1z7h70an9fk5wvrv14c9cc6xzn")))

