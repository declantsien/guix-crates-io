(define-module (crates-io xd ir) #:use-module (crates-io))

(define-public crate-xdir-0.1 (crate (name "xdir") (vers "0.1.0") (deps (list (crate-dep (name "home") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1s6zshd8h9mb1h8m6vn5znxviqicx2a9j67xbxmsz3c17xqs02wi")))

(define-public crate-xdirs-0.1 (crate (name "xdirs") (vers "0.1.0") (deps (list (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs-sys-next") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("knownfolders"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0x6akk7di4vjj0b0g97wx9nh9dxvnjdhd96fi79irwgapdhiys20")))

(define-public crate-xdirs-0.1 (crate (name "xdirs") (vers "0.1.1") (deps (list (crate-dep (name "dirs-next") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs-sys-next") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("knownfolders"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "08ajay2cxwgvzshx1v2ig27pzw780969i7x9r8lb8szxgnrpr4x0")))

