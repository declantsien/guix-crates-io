(define-module (crates-io xd oc) #:use-module (crates-io))

(define-public crate-xdoc-0.0.0 (crate (name "xdoc") (vers "0.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (optional #t) (default-features #t) (kind 0)))) (hash "1j762p5g02ms3cvjsnl168kq2cbgfb64wbh42qhib5b0cf74ldcx") (features (quote (("default"))))))

(define-public crate-xdoc-0.0.1 (crate (name "xdoc") (vers "0.0.1") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (optional #t) (default-features #t) (kind 0)))) (hash "0hznadid8c3i4sw8sy7gwykd2a5xxygi9g7j3m8m5ssbxq4vbxjg") (features (quote (("default"))))))

(define-public crate-xdoc-0.0.2 (crate (name "xdoc") (vers "0.0.2") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 1)))) (hash "1g9waqfdmafzaxiac921i31nlns8i8pz4d9nrw29g62vq3lkkc00") (features (quote (("default"))))))

