(define-module (crates-io xd pi) #:use-module (crates-io))

(define-public crate-xdpilone-1 (crate (name "xdpilone") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "spin") (req "^0.9") (features (quote ("rwlock"))) (kind 0)))) (hash "16b3jy27wi96r02q622xiyrzjpkjvcbiqfaihh9pxha82pn2i3m2") (yanked #t) (rust-version "1.65.0")))

(define-public crate-xdpilone-1 (crate (name "xdpilone") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "spin") (req "^0.9") (features (quote ("rwlock"))) (kind 0)))) (hash "0ff2hs476qry76lsshr7cdfd564hddw2hi7rkgwi4rwnmxvxjr35") (rust-version "1.65.0")))

(define-public crate-xdpilone-1 (crate (name "xdpilone") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "spin") (req "^0.9") (features (quote ("rwlock"))) (kind 0)))) (hash "1pp7mkp1b63y283a9k6p4z79wzprl3v20z35jf8qynkl3jv7nafi") (rust-version "1.65.0")))

(define-public crate-xdpilone-1 (crate (name "xdpilone") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "spin") (req "^0.9") (features (quote ("rwlock"))) (kind 0)))) (hash "0564kiy5dsdppy5d8lyfib113z4hbb1nvfsr9hcwyql7z434s4yp") (rust-version "1.65.0")))

