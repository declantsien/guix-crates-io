(define-module (crates-io xd r_) #:use-module (crates-io))

(define-public crate-xdr_extras-0.1 (crate (name "xdr_extras") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "xdr_extras_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g9iaj1rcicd634is0m2ghv8dhvhi0lhvssr832x0anyh89mjbf0")))

(define-public crate-xdr_extras_derive-0.1 (crate (name "xdr_extras_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde-xdr") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19an0pw4f3mf726pkb3h2ffhf2vlfnfglwrihv6kw7h07d0j5w2w")))

(define-public crate-xdr_extras_derive-0.1 (crate (name "xdr_extras_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde-xdr") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "xdr_extras") (req "^0.1") (default-features #t) (kind 2)))) (hash "06sv3ddxh5qb4y1yi9clp0c43ibrklfn2hya7jq9dswdahrjjyms")))

