(define-module (crates-io xd lo) #:use-module (crates-io))

(define-public crate-xdlol-0.0.0 (crate (name "xdlol") (vers "0.0.0") (deps (list (crate-dep (name "clippy") (req "^0.0.169") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.9") (default-features #t) (kind 0)))) (hash "02xalkyhhkqnv4p9fks0784rggzcad0qi3avn649zamk7jnp8b49")))

