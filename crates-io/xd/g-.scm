(define-module (crates-io xd g-) #:use-module (crates-io))

(define-public crate-xdg-basedir-0.2 (crate (name "xdg-basedir") (vers "0.2.0") (hash "1g6cchq490pic0nkh0alkgv6vql7h14i2jxm7xysbq8lca0pjxw5") (features (quote (("unstable"))))))

(define-public crate-xdg-basedir-0.2 (crate (name "xdg-basedir") (vers "0.2.1") (hash "1c5ah0xsr14fvypyjs15g5a7dzg135a9iqkhv1g3vbcr51mw14qf") (features (quote (("unstable"))))))

(define-public crate-xdg-basedir-0.2 (crate (name "xdg-basedir") (vers "0.2.2") (hash "1zaq3bnx083ckg2fxgrvz5p0jxva3g1rgjlnhdxbzypalpsiqdvd") (features (quote (("unstable"))))))

(define-public crate-xdg-basedir-1 (crate (name "xdg-basedir") (vers "1.0.0") (hash "1z5vzk6a7qxinqn3q70wzkh9y2ngjigb8y56dv3rskwwjdvd1580") (features (quote (("unstable"))))))

(define-public crate-xdg-desktop-entry-0.1 (crate (name "xdg-desktop-entry") (vers "0.1.0") (hash "06vncsxrmmdsskgh8wrh750xfl6a6jp2246kcyhlbj6w8i5i8y8z")))

(define-public crate-xdg-desktop-portal-0.1 (crate (name "xdg-desktop-portal") (vers "0.1.0") (deps (list (crate-dep (name "derive_setters") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.180") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^3.14.1") (default-features #t) (kind 0)))) (hash "100zqr0dpb1xhqs6d1yxc5616y0c9ig08h50m9panfnpdyi595ya")))

(define-public crate-xdg-directories-0.1 (crate (name "xdg-directories") (vers "0.1.0") (hash "1x50zmaak8a89c2800mrnj19fkzp6d0l1yx2wljrryds77ivbjgh") (yanked #t)))

(define-public crate-xdg-directories-0.0.0 (crate (name "xdg-directories") (vers "0.0.0") (hash "0x8skch2d4bcrr1dj6hh6p168r7w6k4bq08qxfxxis2rdxy0am5q")))

(define-public crate-xdg-dirs-0.1 (crate (name "xdg-dirs") (vers "0.1.0") (hash "18ngwqqwjxdhlj7gz85hlj3vlq4kf3ailvkym0lyxj720hsqv2af") (yanked #t)))

(define-public crate-xdg-dirs-0.0.0 (crate (name "xdg-dirs") (vers "0.0.0") (hash "1r5apms0kxs3kp28pjpds6j71g08wdssjmd67y2siwnwrzbz1lr5")))

(define-public crate-xdg-home-1 (crate (name "xdg-home") (vers "1.0.0") (deps (list (crate-dep (name "nix") (req "^0.26.2") (features (quote ("user"))) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winerror"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1kbd1ks8bvpsay6lgk60yaf1w13daaf75ghmslan031ss4y20s97") (rust-version "1.60")))

(define-public crate-xdg-home-1 (crate (name "xdg-home") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winerror"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "13mkcfgngnc1fpdg5737hvhjkp95bc9w2ngqdjnri0ybqcjs7r91") (rust-version "1.60")))

(define-public crate-xdg-mime-0.1 (crate (name "xdg-mime") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "1x5fw66cxb98bhd4ggd70vhwwn357lcd27cn2dxd447n0h139rn0")))

(define-public crate-xdg-mime-0.2 (crate (name "xdg-mime") (vers "0.2.0") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "1gn14fqyhjhh6rk67l1y23sjy7lsh7cfql1vxgdl02rf11ipyw9w")))

(define-public crate-xdg-mime-0.2 (crate (name "xdg-mime") (vers "0.2.1") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "11sm1yl7xamnh1kdc66azq6jmdv02rrx0iaqkfi8zv1ph3jkg0cz")))

(define-public crate-xdg-mime-0.3 (crate (name "xdg-mime") (vers "0.3.0") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "10khmfyjlg86h65bp15aki0lh7k9rpl6p5k0ckdq3jsgxpaq09rn")))

(define-public crate-xdg-mime-0.3 (crate (name "xdg-mime") (vers "0.3.1") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "1iiz4xnags2k6nan4b2gcx74lgky7m3sz1swl587sbyxdbv5xyrq")))

(define-public crate-xdg-mime-0.3 (crate (name "xdg-mime") (vers "0.3.2") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "0x9qsjy1hscvygyacpv90p2cz5ay6brs4n453zfsabjslnsfzglx")))

(define-public crate-xdg-mime-0.3 (crate (name "xdg-mime") (vers "0.3.3") (deps (list (crate-dep (name "dirs-next") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "1alplz0ki5zdxmi6s9w04p1yiwrxkpi7nikfldq8sn2hpdlppgw7")))

(define-public crate-xdg-mime-0.4 (crate (name "xdg-mime") (vers "0.4.0") (deps (list (crate-dep (name "dirs-next") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "1j8yx29crbwwy3dv0nf55syngz5c37q2dfbfln21kywkrb82blsq")))

(define-public crate-xdg-rs-0.0.1 (crate (name "xdg-rs") (vers "0.0.1") (hash "1hdpj1nzfpnh1f649ax4g5msirpk26br2g6jzrphypjis84wgriw")))

(define-public crate-xdg-rs-0.0.2 (crate (name "xdg-rs") (vers "0.0.2") (hash "0k40aa8n00l358zja5jzb0a6lcwc8d52xxgqxwczrslmli4xyy02")))

(define-public crate-xdg-rs-0.0.3 (crate (name "xdg-rs") (vers "0.0.3") (hash "1iwxypdvdp6cghyjk4fj4zn9dj3vrqishfyy82wyzra5ayl1nwwp")))

(define-public crate-xdg-rs-0.0.4 (crate (name "xdg-rs") (vers "0.0.4") (hash "0b8c0dn2lyqgys82jrfv35spsjnxj9irbwslr8zvwm0yv8547acv")))

(define-public crate-xdg-rs-0.0.5 (crate (name "xdg-rs") (vers "0.0.5") (hash "1lywnvfk16849ns3jih460i9v3dcx62igwlmlyk5y478jicxcd3a")))

(define-public crate-xdg-rs-0.0.6 (crate (name "xdg-rs") (vers "0.0.6") (hash "1cz5crll6yni7ikxixfanpld5gcv4af162by3j1pj481jrxrilrv")))

(define-public crate-xdg-rs-0.0.7 (crate (name "xdg-rs") (vers "0.0.7") (hash "06m4y7zrc6r7qrs1jjgjxdygrp7xvx7kg5l9g7zblb525llqj59a")))

(define-public crate-xdg-rs-0.0.8 (crate (name "xdg-rs") (vers "0.0.8") (hash "0q8lvai89v57y7zzgchsi4s19m6c3lbjglzrs0mx7i5ixmiihccn")))

(define-public crate-xdg-rs-0.1 (crate (name "xdg-rs") (vers "0.1.0") (hash "19kz5h49xfw0hx7iw1w7gh75885pmkklgcil1269fkii6rn7q088")))

(define-public crate-xdg-rs-0.1 (crate (name "xdg-rs") (vers "0.1.1") (hash "1zgjcgzjh0hdlisyhvddgkyhvacdjbxc8759jf3vxkp4sg9y9dhy")))

(define-public crate-xdg-rs-0.1 (crate (name "xdg-rs") (vers "0.1.2") (hash "1j41fawh447w6l5lfx6gfp4zndwsk96k2izxc8ra3fs08cal33g5") (features (quote (("nightly"))))))

(define-public crate-xdg-rs-0.1 (crate (name "xdg-rs") (vers "0.1.3") (hash "19iy0hgwxja2xmbmr3a5hw8bpa8wnvmw9nw187vff6kbzic7kznx") (features (quote (("unstable"))))))

(define-public crate-xdg-rs-0.1 (crate (name "xdg-rs") (vers "0.1.4") (hash "0samlr0bkf3anwyd5yj6sjdvhrji9m99ys1fnnqv3l4aag64bdcl") (features (quote (("unstable"))))))

(define-public crate-xdg-sys-0.1 (crate (name "xdg-sys") (vers "0.1.0") (hash "08n086zdsjwrzp3zi2qa2ycivch6kyraqlch9fqw270c9096q3zy") (yanked #t)))

(define-public crate-xdg-sys-0.0.0 (crate (name "xdg-sys") (vers "0.0.0") (hash "0mbiilvcm009kjnzr3mslv7ql9z67q5739f7hy6p19q4p4azzzjm")))

(define-public crate-xdg-user-0.1 (crate (name "xdg-user") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (features (quote ("std"))) (kind 0)))) (hash "1lynj3ibqhmvx6ah0vdg582x50ajjc1piqsvlmxbnc8d764xdh4p")))

(define-public crate-xdg-user-0.1 (crate (name "xdg-user") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (features (quote ("std"))) (kind 0)))) (hash "03p37ywswd3y1w7vzphnn7arja3i1l47qpmbvjg6wawxnlihihv9")))

(define-public crate-xdg-user-0.1 (crate (name "xdg-user") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "146xx7jq060nyr9ah7rspsfawwlyzl4pdzzv732rpvczhfr3sy5c")))

(define-public crate-xdg-user-0.2 (crate (name "xdg-user") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "02b8wlrvrk55lxr91rn992k1jg7yaq5gyav0ccw18s8g47f4zw25")))

(define-public crate-xdg-user-0.2 (crate (name "xdg-user") (vers "0.2.1") (deps (list (crate-dep (name "home") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03lzkwgyvyki3ffizj5ydfhc161949c9h4lbii45mki87y0cvmd1")))

(define-public crate-xdg-user-macros-0.1 (crate (name "xdg-user-macros") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "1fmm1xnm5hqys6cpcd0lmyhjmh6jdxr04qxkdssllrh3ghx5cfi3")))

(define-public crate-xdg-utils-0.3 (crate (name "xdg-utils") (vers "0.3.0") (hash "0gmffrmjlzzr4bs8q0ssjv8l5j2fz1m8yi76vd8m5cisidxa0jjb")))

(define-public crate-xdg-utils-0.3 (crate (name "xdg-utils") (vers "0.3.1") (hash "1knkvmnmdp36zcq9l862kf2sadxsprz7gpn4ln9z6ja2izdpbhnn")))

(define-public crate-xdg-utils-0.3 (crate (name "xdg-utils") (vers "0.3.2") (hash "1nfi4n325p69kv1q95assk06zva4qcd0pw15d50n88rdmd4clwzq")))

(define-public crate-xdg-utils-0.4 (crate (name "xdg-utils") (vers "0.4.0") (hash "19nvj2afc67xgdzdddi2lh6wq089frm9wlpw5hg74sar5pkfz7yv")))

