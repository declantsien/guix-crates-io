(define-module (crates-io xd el) #:use-module (crates-io))

(define-public crate-xdelta3-0.1 (crate (name "xdelta3") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 1)))) (hash "04hzp24zx5q5mhw6vifygizd9yajkk32v186g0dib8y0bjmqrb58")))

(define-public crate-xdelta3-0.1 (crate (name "xdelta3") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 1)))) (hash "179plqnj32b0fzmhk64hflz6dbrbrhs3fqkh0ph3m9g2wp0r1myy")))

(define-public crate-xdelta3-0.1 (crate (name "xdelta3") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 1)))) (hash "1sdg6zrk3n05c4iyz899lkisk1x0w3jfppdsdpmns8mqpms67arc")))

(define-public crate-xdelta3-0.1 (crate (name "xdelta3") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 1)))) (hash "0ybfiqy42x42nqfir5m4pvbcpai2aarrx92ww83iw18psyp4akpi")))

