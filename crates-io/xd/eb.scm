(define-module (crates-io xd eb) #:use-module (crates-io))

(define-public crate-xdebug-0.1 (crate (name "xdebug") (vers "0.1.0") (hash "1a9l5jsxbchwjkbc98rkrqv17873qkdlz0mbxhcr581gn73v8d7q")))

(define-public crate-xdebug-0.1 (crate (name "xdebug") (vers "0.1.1") (hash "1yinpzpyw3aj4dmg1y4dd975pw5l03pd8zvpg4kaf9vvaxm39z9i")))

(define-public crate-xdebug-0.1 (crate (name "xdebug") (vers "0.1.2") (hash "0nqz9p2jnnmis2idlcq8wj6aygi90jqlhasaizkg89zx1r2ykjzk")))

(define-public crate-xdebug-0.1 (crate (name "xdebug") (vers "0.1.3") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jqf7mnfxcibppfcf585s3znxzjjwqplrq37bx65dwc2imi4s4fi")))

(define-public crate-xdebug-0.2 (crate (name "xdebug") (vers "0.2.0") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bi1iprl7qxriif24g66v364f3y8q1qqgfnq45dpzpg7fm7i2ps5")))

(define-public crate-xdebug-0.2 (crate (name "xdebug") (vers "0.2.1") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ggp8lxb2vqdpxvryd43gfvxdr6zjj305l1b62d4fyq8hyahf4in")))

(define-public crate-xdebug-0.3 (crate (name "xdebug") (vers "0.3.0") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize" "overlapped-lists"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nrm44cbfp3x4i8v3axfda702jh2f2q2c28g3yb9c75sbsifliad")))

(define-public crate-xdebug-0.3 (crate (name "xdebug") (vers "0.3.1") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize" "overlapped-lists"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08az63xdndbsgg89w3mfnliiw3qkd2jk2pmj285f75sjzvz3cldy")))

(define-public crate-xdebug-0.4 (crate (name "xdebug") (vers "0.4.0") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize" "overlapped-lists"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hralplisp4xsqghlb1b9yaxxnxzd4d7b2mni0xacjnf1n6m5sq9")))

(define-public crate-xdebug-0.4 (crate (name "xdebug") (vers "0.4.1") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize" "overlapped-lists"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0swkbv7hhkpcw0r50njv93dq7k6hqmc5mnw0i50js7wd8krnla62")))

