(define-module (crates-io xd rc) #:use-module (crates-io))

(define-public crate-xdrc-0.1 (crate (name "xdrc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10qlwiznidrjadnxlncyr9w3wgcadx1iappx276xki7038vabnza")))

