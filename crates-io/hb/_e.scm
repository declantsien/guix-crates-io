(define-module (crates-io hb _e) #:use-module (crates-io))

(define-public crate-hb_error-0.1 (crate (name "hb_error") (vers "0.1.0") (deps (list (crate-dep (name "hb_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09ipa3j1vm68hwwdaihjb6r761cavpwk7r6kb0g98s56s73g14ig") (features (quote (("dont_use"))))))

(define-public crate-hb_error-0.1 (crate (name "hb_error") (vers "0.1.1") (deps (list (crate-dep (name "hb_macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0pqhrfms1hy3173b0nqmc2k06dk7j3shg8r7h4050hqxq0ardzpj") (features (quote (("example_only"))))))

(define-public crate-hb_error-0.1 (crate (name "hb_error") (vers "0.1.2") (deps (list (crate-dep (name "hb_macros") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "126xi50lhsd650r4z46wxsx81dxscr4mninxklnswr68kdz82wdr") (features (quote (("example_only"))))))

