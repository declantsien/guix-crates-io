(define-module (crates-io hb -s) #:use-module (crates-io))

(define-public crate-hb-subset-0.1 (crate (name "hb-subset") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0dny89l4xp8rbzn15933wh1h37zw667925m3l0af2lsfax1r9w5y") (yanked #t)))

(define-public crate-hb-subset-0.2 (crate (name "hb-subset") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "09sjqrf9d72myvrli7b1xhyji0f3nfcfripjipy1y5kswvxybwbw") (features (quote (("bundled"))))))

(define-public crate-hb-subset-0.2 (crate (name "hb-subset") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1in2q2avv99fn5wvxfjdaihy391s696rdmp6d09rnnbbg0dm4v2x") (features (quote (("bundled"))))))

(define-public crate-hb-subset-0.3 (crate (name "hb-subset") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "11rrhslyr6s5zphqbamh7q63psfxpkx57gpd0x1jah6w0d1hnd67") (features (quote (("bundled"))))))

