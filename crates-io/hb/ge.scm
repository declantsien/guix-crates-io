(define-module (crates-io hb ge) #:use-module (crates-io))

(define-public crate-hbgen-0.1 (crate (name "hbgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^3.0") (default-features #t) (kind 0)))) (hash "1jnc3cnll2gv72dafx396dn511kb9b7r8zqpb330lzbkqhxf5a8p")))

