(define-module (crates-io hb _m) #:use-module (crates-io))

(define-public crate-hb_macros-0.1 (crate (name "hb_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1iiqci66fzjv09xplf2zavcrcmp1f6r1i8pdgsvb4k5pkkqvip9a")))

(define-public crate-hb_macros-0.1 (crate (name "hb_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0cifhh256s6x4024vlf44irfsy6hmrdkn0gfh8ckbzzrqi1wzgz8")))

(define-public crate-hb_macros-0.1 (crate (name "hb_macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0wcjliphvzx4fiaaaqnpnyzhwbl3nnadv20cfw707k1kq6qrq751")))

(define-public crate-hb_macros-0.1 (crate (name "hb_macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0bwsd6wy5lwvkll03w4d2abqkh0rih9kbs4ra327bmh6hm2f4yp0")))

