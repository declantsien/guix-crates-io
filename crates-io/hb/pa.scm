(define-module (crates-io hb pa) #:use-module (crates-io))

(define-public crate-hbpasta-rs-0.1 (crate (name "hbpasta-rs") (vers "0.1.0") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pasta_curves") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "005rywax00kh3cm2lzx8knp085bisyaak1yvlp0kzkrjfiibw3ww")))

