(define-module (crates-io ld sc) #:use-module (crates-io))

(define-public crate-ldscript-parser-0.1 (crate (name "ldscript-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^2.2") (default-features #t) (kind 0)))) (hash "03dwlayazg3mlgli40x54cp94d0a14zndmq2s9d59j3q405bgwvz")))

(define-public crate-ldscript-parser-0.2 (crate (name "ldscript-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^3.1") (default-features #t) (kind 0)))) (hash "0pb07h3adh1b66x424xw4c8yd4kfnya08s3fq61gh7sizv65wvxs")))

(define-public crate-ldscript-parser-0.3 (crate (name "ldscript-parser") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1023xk8bhjqbv6gnn1w3fxjsn84b4c5q9ac7wpbvamfgm60gzkm6")))

