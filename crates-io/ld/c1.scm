(define-module (crates-io ld c1) #:use-module (crates-io))

(define-public crate-ldc1x1x-0.1 (crate (name "ldc1x1x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "freebsd-embedded-hal") (req "^0") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 2)))) (hash "0q2szjn6qh9wpnzn9260vsry7g04j3dkij9nigh4jgnqk1bkrp86")))

(define-public crate-ldc1x1x-0.1 (crate (name "ldc1x1x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "freebsd-embedded-hal") (req "^0") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 2)))) (hash "1xsbimqfp611lzblbqhn77n217lkcn34cps6rng7vvlscq280hha")))

(define-public crate-ldc1x1x-0.1 (crate (name "ldc1x1x") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "freebsd-embedded-hal") (req "^0") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 2)))) (hash "1c918mnilighd04yx4i3gmqjv6cwkjrln75mb5rg3abv02jadqx1")))

