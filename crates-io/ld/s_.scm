(define-module (crates-io ld s_) #:use-module (crates-io))

(define-public crate-lds_simple_view-0.1 (crate (name "lds_simple_view") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "lazy_diamond_square") (req "^0.1") (default-features #t) (kind 0)))) (hash "12b4gxkx2hhbja787i65q1sy0610kjyirdd70byb7f7f2sr31vlf")))

(define-public crate-lds_simple_view-0.1 (crate (name "lds_simple_view") (vers "0.1.0-1") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "lazy_diamond_square") (req "^0.1") (default-features #t) (kind 0)))) (hash "04dz2iak1i6fqy8r2nvcrfm2x1p5q9ryhs3y0xdxzs621ivb9adi") (yanked #t)))

(define-public crate-lds_simple_view-0.1 (crate (name "lds_simple_view") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "lazy_diamond_square") (req "^0.1") (default-features #t) (kind 0)))) (hash "00mcwxsxdhvik7lwfal6sw18qspinlsckl8la7zc8shizfmqfp9a")))

