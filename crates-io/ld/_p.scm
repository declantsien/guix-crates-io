(define-module (crates-io ld _p) #:use-module (crates-io))

(define-public crate-ld_preload-0.1 (crate (name "ld_preload") (vers "0.1.0") (hash "0d475n1aas5v45fwdb97binbcchiq18v7wzsmiab1xbd51i3z198")))

(define-public crate-ld_preload-0.1 (crate (name "ld_preload") (vers "0.1.1") (hash "1zjmb929jsvddr9vqjpsmrqbbfndiqasha5a27727649ca5vq3lj")))

(define-public crate-ld_preload-0.1 (crate (name "ld_preload") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1wj3aa2k0v23ihg7ms5s9fl2bfzxhgynrn6xyzwhprqp2rl6faqz")))

(define-public crate-ld_preload_helpers-0.1 (crate (name "ld_preload_helpers") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qf5rr2bknngljnqkf24w7sgljv33cbm6i907nqq34firr0lh41y") (features (quote (("std") ("default" "std"))))))

(define-public crate-ld_preload_helpers-0.1 (crate (name "ld_preload_helpers") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jnlgirgjiia608nw8x63w4d032vi35wzqan5szd49545w36ppiy") (features (quote (("std") ("default" "std"))))))

