(define-module (crates-io ld -m) #:use-module (crates-io))

(define-public crate-ld-memory-0.1 (crate (name "ld-memory") (vers "0.1.0") (hash "0y8rl1rpakkdjfh3b2937fnkz8njinsji14l32qk9nd0b1liqnx1")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.0") (hash "1b1ra7lgih10r3frll6kk4hdkf88hslq6v5nnx7r1y5a0np5b0ap")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.1") (hash "0i51jddxkd6lgkn8y52pbnsifwi8h4p7gpk1ygdxiflkyw5hzg04")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.2") (hash "12f0rl1a61vibzf6lmjx0j7di4pib0bia5zy0lsbjp2qnxk4sraq")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.3") (hash "0cm4cg7pyzb6mbqzr9crv8wsvi8nkmsrh9plsxmg53n9i7h928j2")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.4") (hash "0g3nrgfr1mlp7kpgcaysplxskpbvljqf74zz211mfx2yivd8aizr")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.5") (hash "05agfq06w8ck8xd812xdr5xz4vr76ri3l26qg00wajmq10lrv71w")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.6") (hash "1hba1lzm5l0flbkmx7aadxs63hnig1q00nlfdzx98y48ddzjkxrb")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.7") (hash "0z72mgrndk2qac8s0hwnjsg7x09pc0yjxm4wcr0crn6f7pnnkmy9")))

(define-public crate-ld-memory-0.2 (crate (name "ld-memory") (vers "0.2.9") (hash "034z7qwygk755xckz9zskir6bc9j4693x7v83aa3635ga53pq60n") (features (quote (("build-rs"))))))

(define-public crate-ld-memory-cli-0.2 (crate (name "ld-memory-cli") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "evalexpr") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "ld-memory") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "114jkwi9qi9pmk9fza8n7vjzc1lwkwwyx9m901h019gf4pmph54m")))

(define-public crate-ld-memory-cli-0.2 (crate (name "ld-memory-cli") (vers "0.2.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "evalexpr") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "ld-memory") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0x9vgf2qlink88zhjbgdlicdknp1qr83s8sfk7j51pzwq7dfixpr")))

(define-public crate-ld-memory-cli-0.2 (crate (name "ld-memory-cli") (vers "0.2.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "evalexpr") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "ld-memory") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0hcbj9p3y4p834jaii2arkji1vm1v5dn29dh04n3xcgs2g9nvjjq")))

(define-public crate-ld-memory-cli-0.2 (crate (name "ld-memory-cli") (vers "0.2.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "evalexpr") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "ld-memory") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "013rj8hhi09wjvq9p6qd9np8i5cijnslsr62riidfa45i891ym4p")))

