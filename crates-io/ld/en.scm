(define-module (crates-io ld en) #:use-module (crates-io))

(define-public crate-ldenv-0.0.1 (crate (name "ldenv") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "17wmsk5s5jicv6n5cz0d2pfg908w82y3554iq9cg94sxi5f8a757")))

(define-public crate-ldenv-0.0.2 (crate (name "ldenv") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1mmbn49fcfwgaajk1hgd8gfgx7bnx3lr0apsa11ga11108z3wpwk")))

(define-public crate-ldenv-0.0.3 (crate (name "ldenv") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "131i1ns58qhlh73hgh0x3bg3m70imq4gd5xd0x90q9h9s40ybzhq")))

(define-public crate-ldenv-0.1 (crate (name "ldenv") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0pmza01blr2jg36c6992ibwfszlhcrcxyc6v5ic0p2hwl7zzm5nb")))

(define-public crate-ldenv-0.1 (crate (name "ldenv") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "02lknccl6brqvmlwzhm5cdh6bxzf6wm13nmvwvk105p1f07qrf1b")))

(define-public crate-ldenv-0.1 (crate (name "ldenv") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1bbck5nf17za00z98k78cnc2yx54f7d3z2sqkylfpq2a0m5xx2cr")))

(define-public crate-ldenv-0.1 (crate (name "ldenv") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1ys9gzga2hc6mj8nc8n8jjxql5zzbf98mv5kpz11yb4224cwnnkw")))

(define-public crate-ldenv-0.2 (crate (name "ldenv") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1c9n8ycsfy5n2ql4vp6ps60f2z25nr0m1zdcqyf3qpq96wf2p2i3")))

(define-public crate-ldenv-0.3 (crate (name "ldenv") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1a4061d3snxn6jmmyq0qh06bzk1aaqvag35q5as5imqwxj32ghlc")))

