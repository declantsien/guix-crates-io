(define-module (crates-io ld wm) #:use-module (crates-io))

(define-public crate-ldwm-0.1 (crate (name "ldwm") (vers "0.1.0") (hash "14n3pn8ng664q4j52jry1ab84i94pj6xv1grg3v8vmm5hfib1rrl")))

(define-public crate-ldwm-0.2 (crate (name "ldwm") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)))) (hash "1dl4x08cwr0g60pbpw4vi2nsm72ns0q62q084km4y5bd0vyn4djz") (features (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2 (crate (name "ldwm") (vers "0.2.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)))) (hash "0jx1h63dd7ya1g3j66ah0ry6z7pcn6jx1chqkvppmbnzkgi28rck") (features (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2 (crate (name "ldwm") (vers "0.2.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)))) (hash "04cy3k8xxhwgfi7g7wi18gnxziqc1nmhja5ic285mpbz1rx4gc59") (features (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2 (crate (name "ldwm") (vers "0.2.3") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)))) (hash "0bjcryihjwabcqxs5h6f84x382ckds8s31p6zqjk8sm2hg9w5gcm") (features (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.2 (crate (name "ldwm") (vers "0.2.4") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)))) (hash "0pm5v6s3n4dawln2szy1g79k4n3iy6c305xn1ywphnkz1n598ryn") (features (quote (("verify") ("default" "verify"))))))

(define-public crate-ldwm-0.3 (crate (name "ldwm") (vers "0.3.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (kind 0)))) (hash "1k3vvdsxs4j7mld17qq959khalfd5w0i488j81wz4b847sglwhw6") (features (quote (("verify") ("sign" "std") ("default" "verify" "sign" "rayon")))) (v 2) (features2 (quote (("std" "dep:rand") ("rayon" "dep:rayon"))))))

