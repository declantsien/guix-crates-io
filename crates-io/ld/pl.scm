(define-module (crates-io ld pl) #:use-module (crates-io))

(define-public crate-ldpl-0.1 (crate (name "ldpl") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0rk9ywjc8548dc48k27sxbwy8p92kx57fm6lh123v5il8c0qpfgq")))

