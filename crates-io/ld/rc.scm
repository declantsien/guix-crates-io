(define-module (crates-io ld rc) #:use-module (crates-io))

(define-public crate-ldrc-0.1 (crate (name "ldrc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "038sfmqh98wd17rs4mbx8l30mpcp1z6v3xpj65z8fpqf7zi7s70l")))

(define-public crate-ldrc-0.1 (crate (name "ldrc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1420bz6jqz00xngmjr1a9ynqj01xr4jcza3jfhihgigdxmy0kll1")))

