(define-module (crates-io ld ra) #:use-module (crates-io))

(define-public crate-ldrawy-0.1 (crate (name "ldrawy") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.20.5") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 0)))) (hash "1lzv6gb0zv084hiqxg2zn0hbcp0xjwxvyw1vgvrppj84ifh2jz92") (yanked #t)))

