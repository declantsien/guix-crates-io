(define-module (crates-io ld dt) #:use-module (crates-io))

(define-public crate-lddtree-0.1 (crate (name "lddtree") (vers "0.1.0") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "137vq3f8na346gadmrvp698wgkc7n2iyc750dcd0d8l1ffggib5n")))

(define-public crate-lddtree-0.1 (crate (name "lddtree") (vers "0.1.1") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0vz7nzkq9yginwi1h61pkd78pnqqvwn993gyz1svbw0jr4gkwd5c")))

(define-public crate-lddtree-0.1 (crate (name "lddtree") (vers "0.1.2") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1gckn86s08m4m6nhw3wc484fdy62gsh9wi3z5kzw18yp1jillx8z")))

(define-public crate-lddtree-0.1 (crate (name "lddtree") (vers "0.1.3") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1mpbhmbxjgqgj0mbi3jpdjf7zi2zgp0d84m1s3zlaasw4rqdl6gi")))

(define-public crate-lddtree-0.1 (crate (name "lddtree") (vers "0.1.4") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0rzlhw9y1bx8xaxavn604lpva2glz83l7imfbb6p49dvph0iapbg")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.0") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "01yz1z4spybwhygcjhl2zivd0rxy0z6sal8l0l0mmgqmhs8x4c65")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.1") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "07ajc90zdm95aiknv8p33kajh6gnjiygaka4k6i88lc3cvgn8x1i")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.2") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1am38i9ik9m8w8drnc7awpzmy4f33c0qx92f4q34kw2y7slxx30h")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.3") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0vb5chlz82b7f6qknapvnw316pym6f32q4gi8bh2rkn1cpkpmshx")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.4") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0667v51dcjw7gjzf4hq47p3w6rzbjfprqqsszr702j64asxcr3kk")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.5") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1kfid6jlpj4cg4jz78yxk7avfdyizcdsjnv3zhfz7hf514vsqymn")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.6") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1vm3qhlw9cxabi835d7bnzmbrcfwrlv3f9r6r6bnyqk77s07i6a3")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.7") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "13ss3f6smfpsyrr504vwjr2fj4svxq8ddj827x9ssj6636cszpbk")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.8") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1q7miwkjcldwdmppfqz8rwd547hmxz3m1p6ldzy9ys1pwb914j42")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.9") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0wlckszl78rk3z6373r73v8ar1rhch6yqh4r6ps072hd45pn9nb2")))

(define-public crate-lddtree-0.2 (crate (name "lddtree") (vers "0.2.10") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "04wl1j6as98jfr88z25k4vrvvqpiidb459r7f17xx8y503p1sspi")))

(define-public crate-lddtree-0.3 (crate (name "lddtree") (vers "0.3.0") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "02flf198mfc0jgfw2a3j6gqwqnhgfdr6880wdyshlprapw2dgllj")))

(define-public crate-lddtree-0.3 (crate (name "lddtree") (vers "0.3.1") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "15627v085jyjh41iw62m606vn97g5afwmwps0j75rqws124kl6hr")))

(define-public crate-lddtree-0.3 (crate (name "lddtree") (vers "0.3.2") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0s99hg1zm990skyih3rid5maa9s4wzkaqyibqw39xk0ff0ns103m")))

(define-public crate-lddtree-0.3 (crate (name "lddtree") (vers "0.3.3") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "15f24jcglphqdjxmzafp8fnzslczzr6q1rls36akmb9hd32gwnrg")))

(define-public crate-lddtree-0.3 (crate (name "lddtree") (vers "0.3.4") (deps (list (crate-dep (name "fs-err") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "19ny0nbsxylqm5j3jj08354j9h7m9hb8qsbxr7lyv194dn3r72pq")))

