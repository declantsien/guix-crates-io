(define-module (crates-io ld s-) #:use-module (crates-io))

(define-public crate-lds-color-0.1 (crate (name "lds-color") (vers "0.1.0") (deps (list (crate-dep (name "lds-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "03pl9zya749y6l0x4grnap2qv98lhiip3b19nh0w9k2h9x5kq615")))

(define-public crate-lds-rs-0.1 (crate (name "lds-rs") (vers "0.1.0") (deps (list (crate-dep (name "approx_eq") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "09zi7jdrsnvc3j56gbz3yj739kj3q3z937jyn7mlk0qm1pprji60")))

(define-public crate-lds-rs-0.1 (crate (name "lds-rs") (vers "0.1.1") (deps (list (crate-dep (name "approx_eq") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "0jk877fw545ycl7p5sbsh918rdd1rnn1c85k7sx0di2cy8h818gs")))

