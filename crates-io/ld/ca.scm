(define-module (crates-io ld ca) #:use-module (crates-io))

(define-public crate-ldcache_rs-0.1 (crate (name "ldcache_rs") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07a5wfzajs653aqfi8mxq0xsfapx9lfm5khl7wwjvbpzgr5q20q1")))

