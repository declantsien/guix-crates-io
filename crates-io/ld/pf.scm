(define-module (crates-io ld pf) #:use-module (crates-io))

(define-public crate-ldpfuse-0.1 (crate (name "ldpfuse") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.24.1") (default-features #t) (kind 0)))) (hash "14cix2bi33llczxi6avz432f051v9d0p7rl5lf9nvj8pkjw4pwn5")))

