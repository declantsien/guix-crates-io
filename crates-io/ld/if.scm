(define-module (crates-io ld if) #:use-module (crates-io))

(define-public crate-ldiff-0.1 (crate (name "ldiff") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "114dxbz6pwrlmrnvaaakming9grs7wm49hzq0p4rmlzx8n99sz0s")))

