(define-module (crates-io w- pg) #:use-module (crates-io))

(define-public crate-w-pgn-0.1 (crate (name "w-pgn") (vers "0.1.0") (hash "1s9wb24bg9rfiywlqksc5wkjnv6b9m73q1scz8dmh52j861dhiv6")))

(define-public crate-w-pgn-0.1 (crate (name "w-pgn") (vers "0.1.1") (hash "1v9pjway280w1y129ikwd0mj0bm1080l2w0apkfi2mz2rrficcfi")))

(define-public crate-w-pgn-0.1 (crate (name "w-pgn") (vers "0.1.2") (hash "1xba7swixd4d740lfiaaicm3xagidq98wrchjyy6z1vgfiv9zaix")))

