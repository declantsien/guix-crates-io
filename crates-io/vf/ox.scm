(define-module (crates-io vf ox) #:use-module (crates-io))

(define-public crate-vfox-0.1 (crate (name "vfox") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "mlua") (req "^0.9") (features (quote ("lua54" "vendored"))) (default-features #t) (kind 0)))) (hash "0zwxx03qli0xd3bpsnp93v4cqhyrqdchq39f7zk6hqzs560s8llh")))

(define-public crate-vfox-0.1 (crate (name "vfox") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "mlua") (req "^0.9") (features (quote ("async" "lua54" "macros" "serialize" "vendored"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0mmwrsfh5gcymj9w006aa29i8zn1g5y9zyhyf0f3xr199dbqrihz")))

