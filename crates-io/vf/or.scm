(define-module (crates-io vf or) #:use-module (crates-io))

(define-public crate-vfork-0.1 (crate (name "vfork") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.148") (default-features #t) (kind 0)))) (hash "191f841j6lggyfrhfx98capfaf9m97ml9i7c5i6aalxlvypy6jfm")))

