(define-module (crates-io vf so) #:use-module (crates-io))

(define-public crate-vfsops-0.1 (crate (name "vfsops") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "kstat") (req "^0.1") (default-features #t) (kind 0)))) (hash "1s9asracnxdw9351pgs8nlp5qmsp8y19m0g6f74g52pq9wnnf0pv")))

(define-public crate-vfsops-0.1 (crate (name "vfsops") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "kstat") (req "^0.1") (default-features #t) (kind 0)))) (hash "13cmh7xvrdrmwrr8c6nds65an1bs8njlpd5qny2j5mlgxij4s4bp")))

