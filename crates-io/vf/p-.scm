(define-module (crates-io vf p-) #:use-module (crates-io))

(define-public crate-vfp-cli-0.1 (crate (name "vfp-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "vfp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0k9mxzqdy3d1xvkxkphgv2vk39i1f5w6zxs6m59hy6r5kx57v3az")))

