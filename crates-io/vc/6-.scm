(define-module (crates-io vc #{6-}#) #:use-module (crates-io))

(define-public crate-vc6-sys-0.1 (crate (name "vc6-sys") (vers "0.1.0") (hash "1jfqsc6zhy709fvbdc99rwkqb43yxbamknhqxspzxms6nynvk1ah")))

(define-public crate-vc6-sys-0.1 (crate (name "vc6-sys") (vers "0.1.1") (hash "06c4xgj2pyi7n1s85z4is49zq7gyidn2sfdqai15ycza9inar9qp")))

(define-public crate-vc6-sys-0.1 (crate (name "vc6-sys") (vers "0.1.2") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")))) (hash "11a34hrx074gj84nlhmzw1vrabh8lxk9bqlwyqc5im6b1basrcx5") (features (quote (("rustc-dep-of-std" "core" "compiler_builtins/rustc-dep-of-std"))))))

