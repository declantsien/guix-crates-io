(define-module (crates-io vc g-) #:use-module (crates-io))

(define-public crate-vcg-auction-0.1 (crate (name "vcg-auction") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "secrecy") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1h48l8n1m3xphr9zbxwm8wrfj2c0ny2r2qb24fi6lx6gdqa9nba8") (features (quote (("default" "rand")))) (rust-version "1.56.1")))

