(define-module (crates-io vc sg) #:use-module (crates-io))

(define-public crate-vcsgraph-0.1 (crate (name "vcsgraph") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1xr8n8dpajwzqv5m672233nks1s7qg36yk2j3qyy9gkwjbp7hiyf") (features (quote (("cli" "structopt"))))))

(define-public crate-vcsgraph-0.2 (crate (name "vcsgraph") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1xqy6pp6njyywdnaaqz9hhpfrm7rflw226bw798gfx953qiqrdjc") (features (quote (("cli" "structopt"))))))

