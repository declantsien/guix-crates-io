(define-module (crates-io vc s-) #:use-module (crates-io))

(define-public crate-vcs-classic-hid-0.1 (crate (name "vcs-classic-hid") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.2.5") (kind 0)))) (hash "1a117ppa4s42b73vf31hirrvyypn88wiqyyzb5v8lyrm1da6x6p2") (features (quote (("linux-static-libusb" "hidapi/linux-static-libusb") ("linux-static-hidraw" "hidapi/linux-static-hidraw") ("default" "linux-static-hidraw"))))))

