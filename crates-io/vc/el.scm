(define-module (crates-io vc el) #:use-module (crates-io))

(define-public crate-vcell-0.1 (crate (name "vcell") (vers "0.1.0") (hash "03bswmj8gzr2z3nbbqasl9g5k4lkx6arvzxiib88r4mnmzq9ghj5") (features (quote (("const-fn"))))))

(define-public crate-vcell-0.1 (crate (name "vcell") (vers "0.1.1") (hash "0mz6fwn6533qn60x2wwvsa619qz8wym1hdjvwbdgpfcyf9fgfl7a") (yanked #t)))

(define-public crate-vcell-0.1 (crate (name "vcell") (vers "0.1.2") (hash "0g2b99b6zam4idgc8p9inrig75qi775zg579i513lmpympf34vl7") (features (quote (("const-fn"))))))

(define-public crate-vcell-0.1 (crate (name "vcell") (vers "0.1.3") (hash "00n0ss2z3rh0ihig6d4w7xp72g58f7g1m6s5v4h3nc6jacdrqhvp") (features (quote (("const-fn"))))))

