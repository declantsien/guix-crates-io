(define-module (crates-io vc -l) #:use-module (crates-io))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.1") (hash "1yah7cc66cfgj4npkhpc7xxvdgrzpivyvp04h52gv88lndx4j33b") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.1-r2") (hash "0a6x3wk3cyg2icm49dvjsmjws857jl7lkjis8w0wxbzss5h1rdjp") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.1-r3") (hash "1qz1krrcccji73w38s59mad5dglifrmrgss3z2gzqswxxiaq7r6h") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.1-r4") (hash "0sqgpm4l78qim8b2ydbvy2pid20mk56x9zfjy1sfppmyznvg4fz3") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.1-r5") (hash "19mswv6km16jf802r9gpgxzi60lbiqa7zzcnrgp28h07r8zqf6s4") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.2") (hash "1c4d2ims1m0f0in9n96cf5xf6jabk6mbr22gv8hqjzkrbnngxbn4") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.2-Beta1-r7") (hash "1c7ikvsr9swlfnh3whpcln6gvk8a1cfylqdvc9kqkgq6c3wn4lc4") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.2-Beta2") (hash "1yx1my3c4qln1w0629aklark6cckp4f2bgzapwpmha6p2xrzvfww") (yanked #t)))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.3-Beta1") (hash "15scdxwkd8pz08smp0vl5pfz4jpsfi2dx5k4dkaka5vad88jrvkd")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.3") (hash "139ymq3gs748wkwpmcg45nnr2f53dbnm65376x31k696hm2120ij")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.4-Beta1") (hash "0qg57ksk8qg36a0gjp65nfg3vazd99hlyic2a506a96nwmhyyil0")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.4-Beta2") (hash "0qvanrw9vzw266qa6b1xy6z8cidfi2vw8gc4y1c42rdjkiac0lkm")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.4-Beta3") (hash "0jbv7bqh4jk4lbgfk80zs11398p0wkbc5dyzmhsvgx3c582xl9iw")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.4") (hash "1xil9436x1nzjqnfdfycsk7yq8asc0wv901r3lx16cxls37sv894")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.5-Beta1") (hash "0na4mzas0k1f9cxjwsqh7igcs40yinvx7iic9i6bwl531jpsbk7g")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.5") (hash "195da9ksi2bi8hjmmwckczrla8aphjs0n4am5l6hhpdrr8pfv655")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.6-Beta1") (hash "1hpavmvzip7hlxdhqxxy0wpz8mpfgq240gbkx33sanwjkx29fldw")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.6-Beta2") (hash "1jbfw0ar538bssdrjjnva7ylzyjxlq8rm4rsxvm7rmxnlkb0fjzs")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.6-Beta3") (hash "19cxqskh3sgvnpv1v5s2hdi65z6a35plgr8hybgq119bn4zg15q5")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.6-Beta4") (hash "150rzbgmkhh2km324zzrifvp65hidjxa9n7smldlvx2jiphidfi2")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.6-Beta5") (hash "15ik8cxafwyp20w4fihk37xdh7jsysz63rxbq4yivqx1p8nkfgx8")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.6") (hash "1h80b8a6jrpd6rd5yh6nnzbr37hygm728p82c8ygiilqyd4igfpl")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.7") (hash "16p5p75wgkhqw19dyfnd6g46vzh38lyqz13gy9wbyp56i48lhxac")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.8-Beta1") (hash "11a4k4c0im9cbdnkvjrfnwq5sbsdspxghpsnhc6a7fppwn6dlyr6")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.8-Beta2") (hash "0agvpxpp4w4x3sb7bj481k1m65pakrka45rqpff060dbmryvfa12")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.8") (hash "098dp0vim7d6a0bhnhka35dvzsi0pnzv11kmhqaa6h8z93iqmsc7")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.9") (hash "1kxmr4qcakhdn05sqzv2a7lc0444w9mkzhih85hmir72r90fwfds")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.10-Beta1") (hash "1l3hpixkzrydd8g73014a398g22vb2zc59kfrzsy5k69k623zm4y")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.0.10-Beta2") (hash "115jfgg6par5cnw2r57y1bq2vsidv0jsfva6zihws5p8s31hnmnv")))

(define-public crate-vc-ltl-5 (crate (name "vc-ltl") (vers "5.1.1-Beta1") (hash "1v8724gwskm9ssa7fvibk9i7s60npgvf0ba97r1yzlv8bakiy32g")))

