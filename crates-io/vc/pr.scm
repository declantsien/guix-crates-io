(define-module (crates-io vc pr) #:use-module (crates-io))

(define-public crate-vcprompt-0.1 (crate (name "vcprompt") (vers "0.1.0") (hash "09yjlkplaxqd6gx8qv9knb8ahfw3vbdwn1ifc82akfl89m5r4i3v")))

(define-public crate-vcprompt-1 (crate (name "vcprompt") (vers "1.0.0") (hash "0dys33mqqpzrqkxjvpaa4zppvvzfmvkzmnyd7v151751yz2czwmp")))

(define-public crate-vcprompt-1 (crate (name "vcprompt") (vers "1.0.1") (hash "18xg7ssvikzc9halbsf4sawdc534cr8mb33cnplmc091i7klyapv")))

(define-public crate-vcproof-0.1 (crate (name "vcproof") (vers "0.1.0") (hash "1cyksas5b2l3ih0r9idbmmbm4q53k51fsvg79zlhrh932s902np4")))

