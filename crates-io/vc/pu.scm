(define-module (crates-io vc pu) #:use-module (crates-io))

(define-public crate-vcpu-0.1 (crate (name "vcpu") (vers "0.1.0") (deps (list (crate-dep (name "enumflags2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fidfacv42pj5qnak31qy8xx7f22r862krkija1gchq99rpx376w")))

(define-public crate-vcpu-0.2 (crate (name "vcpu") (vers "0.2.0") (deps (list (crate-dep (name "enumflags2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "12h0303p16id9bpxvsp1qrlnqmbspm6vrl0p6plj4njl82q8i6gr")))

