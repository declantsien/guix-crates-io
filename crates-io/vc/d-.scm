(define-module (crates-io vc d-) #:use-module (crates-io))

(define-public crate-vcd-ng-0.1 (crate (name "vcd-ng") (vers "0.1.0") (deps (list (crate-dep (name "atoi_radix10") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "compact_str") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "linereader") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0n4igp5ncag75r33mmp45g9m46wljbvkz1qfvdx4w0cv9v4hly46")))

(define-public crate-vcd-ng-0.1 (crate (name "vcd-ng") (vers "0.1.1") (deps (list (crate-dep (name "atoi_radix10") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "compact_str") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "linereader") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "15hwhlrxlmrrm22ivrrkfayh9dmadbbg4sxvknli0sjwm89jzc6a")))

(define-public crate-vcd-ng-0.1 (crate (name "vcd-ng") (vers "0.1.2") (deps (list (crate-dep (name "atoi_radix10") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "compact_str") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "linereader") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0am5rcdwkz9haqcfmhlgs1bkhw1dksqhk12wj9gafg63miwk3xn2")))

