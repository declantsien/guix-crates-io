(define-module (crates-io vc md) #:use-module (crates-io))

(define-public crate-vcmd-0.0.1 (crate (name "vcmd") (vers "0.0.1") (hash "1k5m6bivc3rsmv53s5mvrxz48i57bj2cz4qq6wwaqlv43zx2zigg")))

(define-public crate-vcmd-0.0.2 (crate (name "vcmd") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xh678iycwqc39lshnrs63s6wzvpiky5shln1j3v8qx8isgqmzra")))

