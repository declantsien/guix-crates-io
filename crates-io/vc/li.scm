(define-module (crates-io vc li) #:use-module (crates-io))

(define-public crate-vcli-0.1 (crate (name "vcli") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "1qhawhx3q665spkdlp1sigk19xp89pd8jzpp0x2prkm6q7213gpg")))

(define-public crate-vcli-0.1 (crate (name "vcli") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "1zip8zndnbi0apszwhm1xmvnrmc71ilvs686yn01g0brncr1sn2y")))

(define-public crate-vcli-0.1 (crate (name "vcli") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "1gpkcmcxcllipqvy57lg90alaqcm6mcqllwc6fdrc9gdh33yljlg")))

(define-public crate-vcli-0.1 (crate (name "vcli") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "0dxgk762v4rc8i077sggqizva0kdad3dzj7664y1l3i122ad2wfl")))

(define-public crate-vcli-0.1 (crate (name "vcli") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.3.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "11i18l0gawz8vmpz387jyzwj8f9ghc2v8r75qy79plsb2nsk45ky")))

