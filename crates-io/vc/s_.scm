(define-module (crates-io vc s_) #:use-module (crates-io))

(define-public crate-vcs_version-0.1 (crate (name "vcs_version") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.34") (features (quote ("formatting" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "which") (req "^6.0.0") (default-features #t) (kind 0)))) (hash "1z8jl7n9s6pwlw37n38654j1zc3sk9xl1sg0kh6bjndx9fbr7161")))

