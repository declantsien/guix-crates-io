(define-module (crates-io vc st) #:use-module (crates-io))

(define-public crate-vcstatus-0.1 (crate (name "vcstatus") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "01q4vyvrhvlq0lgxcd2mf32jd5f2wf2iyr4drvikhr96hjl02nvy")))

(define-public crate-vcstatus-0.1 (crate (name "vcstatus") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1") (default-features #t) (kind 0)))) (hash "1b3rwlvnqnaka6bnhy1ymrqc5jvaxv3abn715jmczlgg7cc7zq45")))

