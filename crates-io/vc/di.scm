(define-module (crates-io vc di) #:use-module (crates-io))

(define-public crate-vcdiff-0.1 (crate (name "vcdiff") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "open-vcdiff-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rn563nin0fanlfd306hdg63xag6j620v6ilmdrg9l3fm5dh5q3b")))

(define-public crate-vcdiff-common-1 (crate (name "vcdiff-common") (vers "1.0.0") (hash "1lfpy0cl00z1if0k8sajmy9crrz39inn3pax5k90zysilf9rnsg2")))

(define-public crate-vcdiff-decoder-1 (crate (name "vcdiff-decoder") (vers "1.0.0") (deps (list (crate-dep (name "vcdiff-common") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vcdiff-reader") (req "^1") (default-features #t) (kind 0)))) (hash "1nshfs3hjy58vg2v0rf9iafbq7kxc0wh24lq2zx7n3kvzxd9krgf")))

(define-public crate-vcdiff-merger-1 (crate (name "vcdiff-merger") (vers "1.0.0") (deps (list (crate-dep (name "vcdiff-common") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vcdiff-reader") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vcdiff-writer") (req "^1") (default-features #t) (kind 0)))) (hash "19njaj1ww9bk1klygkxq6im27kism7vqw0slvzqsh4an5xgnj9py")))

(define-public crate-vcdiff-reader-1 (crate (name "vcdiff-reader") (vers "1.0.0") (deps (list (crate-dep (name "vcdiff-common") (req "^1") (default-features #t) (kind 0)))) (hash "1g5dijrlmz2xx6a6bzfx262jzcqm77sfr4fzzp8rskchrdy13nr7")))

(define-public crate-vcdiff-writer-1 (crate (name "vcdiff-writer") (vers "1.0.0") (deps (list (crate-dep (name "vcdiff-common") (req "^1") (default-features #t) (kind 0)))) (hash "1l1jp5x55dvy6vi65jmba68gpv64kvzdln62j06c93h26g5dlp16")))

