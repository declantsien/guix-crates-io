(define-module (crates-io vc fv) #:use-module (crates-io))

(define-public crate-vcfverifier-0.1 (crate (name "vcfverifier") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "faimm") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.39.5") (default-features #t) (kind 0)))) (hash "038wiza80hz6qvvy8w2yjplfizm1sbi469h1c7jdqrl9239jqd3m")))

(define-public crate-vcfverifier-0.1 (crate (name "vcfverifier") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "faimm") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.39.5") (default-features #t) (kind 0)))) (hash "1za82pamwch42gw4snznql76x2h800l8xwmjcrgwk99gygrpsrrg")))

