(define-module (crates-io vc pk) #:use-module (crates-io))

(define-public crate-vcpkg-0.0.1 (crate (name "vcpkg") (vers "0.0.1") (hash "1px2r0cpjh4vahpq9brm97k1f96qz36yx44dr3dsmd5r43ahpl1q")))

(define-public crate-vcpkg-0.0.2 (crate (name "vcpkg") (vers "0.0.2") (hash "03ijdcv3m3a9drwzpsh98pwm0grnmlkrl75l020fda6vrs8ll30h")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.0") (hash "0ff55nhh024nfp0vg472iq80i4ddjyl7421bf9znkf9k1l9wvbqf")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.1") (hash "1pjxxab8ivkwh6z8xlz4flz9i7vmb23mk5jqxc6vnm17qd241d6h")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.2") (hash "1b0nfr7qxk10fg6hwgzv0crrd1dig1nzgj65b3kcwz4y4hma5fw1")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.3") (hash "1gl6c75lr9f9gxlzjs1r2184ghxv5nfs9zrvq6qak1r1ij3sl49p")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.4") (hash "12kaaf7hxn614v9iqhbvjjgmvbw9rwd31azhm08xwgs9095fxyil")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.5") (hash "16j0kg14jldryks4dw8pcyja9p0hjb78x9bn211rk58x2l29ib4j")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.6") (hash "0y82lf5z8x73qkxrycvzsxhhkqi5p0f4ndai9lpkj2v0fjqs59d9") (yanked #t)))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.7") (hash "1p3hcjk7912f2smpvijif6gvpa0adfz3h6cwnbx441w0140cmr7b")))

(define-public crate-vcpkg-0.1 (crate (name "vcpkg") (vers "0.1.8") (hash "0r18j0bqlj2vkbgj1fsn0mmrz06y54q4wvrz84l0gmp5blp24n6x")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.0") (hash "1k2gzrbsdkx5j3rdw6xmb6s78qvakbw40rp0cxzn1nffw9qgyx6z")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.1") (hash "1bdcylhr5v828xz5vm02lwr433cpiws7h44mvc8cgw2m5bz87h2f")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.2") (hash "02vx93alz6qp31vxd2ms2jdd17naxrkd969127xshy1ixn5ps2ly")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.3") (hash "1003wv63wjg112xk3kyikf3aj5i189sx3jf1pd0wm1cakiwgdl3y")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0x1410z41pjydhjlchwv81034pdk32l6bpnbwg0085w173hk7rfb")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.5") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "1k3iqxswcs77v0z6ghdrmxpz1dvc5p26gf77m1xjzvd51sa7a555")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.6") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0g8l2gpc889bl4ja7fx0syr4sgj0w6zq63kx5hdkf4ivxg9rdwny")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.7") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "15dzk1b96q946v9aisbd1bbhi33n93wvgziwh1shmscn1xflbp9k")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.8") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0s1ijdrsg6917imja2hb07l0z4vbx7ydm8m2i1n9g62fg7r3ki1z")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.9") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0k6p226prl1g8rns3xi5ak44aki0xx6hmnr3wbqhf7hjaqfy9lam")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.10") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "132hlmsc4maava91vl4lh677sl1c7vr8ccl53fnr5w41y6dh4m34")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.11") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "1yvrd2b97j4hv5bfhcj3al0dpkbzkdsr6dclxqz3zqm50rhwl2xh")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.12") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0p9ypqvv55cq2rl0s7g5ca2bkvwn62cdrdb0hm8j0hd2crigznyb")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.13") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "1ajg5vqyhd4n72y6048bpdrmb3ppvsaabhavipjhf6by005f8p02")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.14") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "169kihgyij8jff0ymql2ss8c60q3xgql7r4j19cbzsglzpr5sibh")))

(define-public crate-vcpkg-0.2 (crate (name "vcpkg") (vers "0.2.15") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "09i4nf5y8lig6xgj3f7fyrvzd3nlaw4znrihw8psidvv5yk4xkdc")))

(define-public crate-vcpkg_cli-0.1 (crate (name "vcpkg_cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.23.2") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "13da98cyz3k5wlb4svssfssy21js7q1kdf5lx61jw7fx1wy7zz12")))

(define-public crate-vcpkg_cli-0.1 (crate (name "vcpkg_cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.23.2") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1ymd6arsmkx7d31nixf0kbmc0xzfcgy5yidcw8xhc3fv3f7r9nmx")))

(define-public crate-vcpkg_cli-0.2 (crate (name "vcpkg_cli") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.23.2") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qyjkkyji9gwzky7ckxc93qyklzmh7937qgs72mj411bpyykx2fw")))

(define-public crate-vcpkg_cli-0.2 (crate (name "vcpkg_cli") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1syk3d9xlxny2awk9zlxv2wv74r9vw2cd63mxwh2ibfrxkcvpprm")))

(define-public crate-vcpkg_cli-0.2 (crate (name "vcpkg_cli") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0ajm3999f4x24wwhnhwg2whx10j8wp98kr412pkhdv2q0fiswq8b")))

(define-public crate-vcpkg_cli-0.2 (crate (name "vcpkg_cli") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "05hyjdqmsd6mjk0fvza4zixyr267wjqxmxc909gll0qd85mswsa8")))

