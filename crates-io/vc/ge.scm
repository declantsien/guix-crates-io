(define-module (crates-io vc ge) #:use-module (crates-io))

(define-public crate-vcgencmd-0.1 (crate (name "vcgencmd") (vers "0.1.0") (deps (list (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1kdja8ylj9cv1pz9jfj04msnv5zzwk171m95np3bw7af7rnqiyj8")))

(define-public crate-vcgencmd-0.1 (crate (name "vcgencmd") (vers "0.1.1") (deps (list (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0pmgsvrl79sm0rnx8r9478k8fskbml1q1b59y4mzvs1cmvz09b2x")))

(define-public crate-vcgencmd-0.1 (crate (name "vcgencmd") (vers "0.1.2") (deps (list (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "06y68mzdp607kdnn3ivxj47v6333bz5ggp3q3qx2ydqpv2jpvrwq")))

(define-public crate-vcgencmd-0.1 (crate (name "vcgencmd") (vers "0.1.3") (deps (list (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "04ki4581d6llxkvhp7k2qwkjlh9ab7fvz2v8f5vwpj0lax2hp1w4")))

(define-public crate-vcgencmd-0.1 (crate (name "vcgencmd") (vers "0.1.4") (deps (list (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0q01rp05wvy1dqk4asvn3kk6w7j49psr8bd8rqn4kh8rfxyfqpgr")))

(define-public crate-vcgencmd-0.2 (crate (name "vcgencmd") (vers "0.2.0") (deps (list (crate-dep (name "bitpat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0mf8gf249bf0ychgif8rk6jgr2051p0s2xq5msakrwm7zpc8qy5x") (features (quote (("serde_support" "serde") ("default"))))))

(define-public crate-vcgencmd-0.2 (crate (name "vcgencmd") (vers "0.2.1") (deps (list (crate-dep (name "bitpat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1fby2jhhg9ivvzkyj23dkl8zpp9667rvpkcyfjxli95yl79afmbj") (features (quote (("serde_support" "serde") ("default")))) (yanked #t)))

(define-public crate-vcgencmd-0.2 (crate (name "vcgencmd") (vers "0.2.2") (deps (list (crate-dep (name "bitpat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "08h8kslgdqq88m32sw67h89341aip14nzykfp086bdq2zwp148wv") (features (quote (("serde_support" "serde") ("default"))))))

(define-public crate-vcgencmd-0.2 (crate (name "vcgencmd") (vers "0.2.3") (deps (list (crate-dep (name "bitpat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "19iif4v5iixwk2vmd386hfpjvg2vlj7xkjyivdvdw1f8zgp9vqrk") (features (quote (("serde_support" "serde") ("default"))))))

(define-public crate-vcgencmd-0.3 (crate (name "vcgencmd") (vers "0.3.0") (deps (list (crate-dep (name "bitpat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0njxb7fz9yg85al32s2rj9kz1784g8ivgdi0cjnghmpx2vd3cbxk") (features (quote (("default"))))))

(define-public crate-vcgencmd-0.3 (crate (name "vcgencmd") (vers "0.3.1") (deps (list (crate-dep (name "bitpat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0lr7aj4q3nz1zwgm4k0g2w46bsi3fvx2cy41qadphfk8s99cshx2") (features (quote (("default"))))))

