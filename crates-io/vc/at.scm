(define-module (crates-io vc at) #:use-module (crates-io))

(define-public crate-vcat-0.1 (crate (name "vcat") (vers "0.1.0") (deps (list (crate-dep (name "better-panic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vfile") (req "^0.1") (default-features #t) (kind 0)))) (hash "0x3cq9sr8wql2qnrkbaiwfjlacdab3wh8wmsg07v7zcp956zyxhn") (yanked #t)))

(define-public crate-vcat-0.8 (crate (name "vcat") (vers "0.8.0") (deps (list (crate-dep (name "better-panic") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vfile") (req "^0.8") (default-features #t) (kind 0)))) (hash "1kxjgwzmxc1yjq4dmafvy79zl6vi0j5ly0y6j2n6pp3db183crbj") (yanked #t)))

