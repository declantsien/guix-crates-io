(define-module (crates-io vc he) #:use-module (crates-io))

(define-public crate-vcheat-0.1 (crate (name "vcheat") (vers "0.1.0") (hash "0n2mbairii3bh4jfmi75cgsrzjxvbjqdy7vkcjxgpdnwlf2nfpgv") (yanked #t)))

(define-public crate-vcheat-0.1 (crate (name "vcheat") (vers "0.1.1") (hash "0igxp75b2bzkr5z9ifby3i6ry5rmgh5mzpnpwv1dclznam0bdvv7") (yanked #t)))

(define-public crate-vcheat-0.1 (crate (name "vcheat") (vers "0.1.2") (hash "1636dy1gc0zmc1bg9kz3m9gl4h2ljb1qrpmfs3z820nj26pgfzfb") (yanked #t)))

(define-public crate-vcheat-0.1 (crate (name "vcheat") (vers "0.1.3") (hash "1x09yqpd2skdj77gb97wkm62y2llh580barfly1glh3nipkakczl") (yanked #t)))

(define-public crate-vcheat-0.1 (crate (name "vcheat") (vers "0.1.4") (hash "1g7v5qv6b9an5v50gwfd6qrm29gsx4nl8kx04zf88jahrp9x9y5w") (yanked #t)))

(define-public crate-vcheat-0.1 (crate (name "vcheat") (vers "0.1.5") (hash "1fxsxpgwpy74hpikg13xq5ai750y3bfnhdnbgx37cjmi93pdnsdl") (yanked #t)))

(define-public crate-vcheat-0.2 (crate (name "vcheat") (vers "0.2.0-alpha") (hash "1agv8qw954606l8ilny1fd8jpzz2qn239k5dx0z1caslz4x5afbf") (yanked #t)))

(define-public crate-vcheat-0.2 (crate (name "vcheat") (vers "0.2.0-beta") (hash "1qgqksg3yjp5rcf59ypsz74va87p8nnm7dv645rjyn9vn7rn71a4") (yanked #t)))

(define-public crate-vcheat-0.2 (crate (name "vcheat") (vers "0.2.0") (hash "17vaa25q4gpdiws9dpl4j8vikmijlb28a84557zjm4d95h63xn48") (yanked #t)))

(define-public crate-vcheat-0.3 (crate (name "vcheat") (vers "0.3.0") (hash "0ks0k4hf62v258gjr9b6xk9i5a6ki16lwfhm4qwi52ixxw2w3ljp") (yanked #t)))

(define-public crate-vcheat-0.3 (crate (name "vcheat") (vers "0.3.2") (hash "1nd60p8sdijnp8brgw5hwcnb10mhgc6xlrrg1jlh43d70ksc9rk7") (yanked #t)))

(define-public crate-vcheat-0.4 (crate (name "vcheat") (vers "0.4.0") (deps (list (crate-dep (name "unsafe_fn_body") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p6xs1ba61y3ww03wn57dnlh51by9sz71dcdm4zhj2b4rw4gq5hc")))

(define-public crate-vcheat-0.4 (crate (name "vcheat") (vers "0.4.1") (deps (list (crate-dep (name "unsafe_fn_body") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sqb0wa1znizrf3hsxc4rh1n3g8g796ln6fd975d23zbffcsam11")))

(define-public crate-vcheat-0.4 (crate (name "vcheat") (vers "0.4.3") (deps (list (crate-dep (name "unsafe_fn_body") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qzvq9jj5dibyysimw265qnwjz92w2m2mw8y6a9kq3dc9f50d0a0")))

(define-public crate-vcheat-0.5 (crate (name "vcheat") (vers "0.5.0") (hash "1jb423xqc7lx9lg3q44jzn9371m1w4l8v7pcd5zy6ya85mpr8aza") (yanked #t)))

(define-public crate-vcheat-0.5 (crate (name "vcheat") (vers "0.5.1") (hash "06jjq72fq3jj93j8fv2mq8fidhiwpn2dh1xh7d0qa3gnj2bbxgrr")))

(define-public crate-vcheat-0.5 (crate (name "vcheat") (vers "0.5.2") (hash "1nwcnvsnc6xj75dvfaxz34f9wv7bq16ff9fgy3zyf1nxm1cykaf9")))

(define-public crate-vcheat-0.6 (crate (name "vcheat") (vers "0.6.0") (hash "1qznb83x4zv2pqqyxc6xdin9q6f6il8ibk11v1dn7qr6h3cz6h5i")))

(define-public crate-vcheat-0.6 (crate (name "vcheat") (vers "0.6.1") (hash "0zcia04xbf02c0g4589zgh0scdy639i8l7mc15kilacpjxwqqi2l")))

(define-public crate-vcheat-0.6 (crate (name "vcheat") (vers "0.6.2") (hash "13dqzangc1bkwy68p25k49m55gyfwqn4ih5nbzsjzf5nc2q3ggqq")))

(define-public crate-vcheat-0.6 (crate (name "vcheat") (vers "0.6.3") (hash "12l89ix7y4s8vs61r2ivbxlmcs93v3i57kh018mrywr37wl88qi9")))

(define-public crate-vcheat-0.6 (crate (name "vcheat") (vers "0.6.4") (hash "1wsdijy20h00ivz07s00j8ax07wbac3vyvlaanlszm2kv8qz295i")))

(define-public crate-vcheat-0.6 (crate (name "vcheat") (vers "0.6.5") (hash "0b9c9kcf7cpnjl0qighc4v0q0mdnch5nxyhfcydpcif8nzzy94vg")))

