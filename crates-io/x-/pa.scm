(define-module (crates-io x- pa) #:use-module (crates-io))

(define-public crate-x-path-0.1 (crate (name "x-path") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "markdown-includes") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows-sys") (req "^0.45.0") (features (quote ("Win32_UI_Shell" "Win32_Foundation" "Win32_Globalization" "Win32_System_Com"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0cmz7g9iaz96b8w6di97nhq1v8wcf52vxvnd9wmj96wa2k63bx2k") (features (quote (("strict"))))))

