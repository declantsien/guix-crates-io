(define-module (crates-io x- gi) #:use-module (crates-io))

(define-public crate-x-git-hooks-0.0.1 (crate (name "x-git-hooks") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "19xqc6z0zwpq57al09yapmgycvgnacc7nz0q5idhfq0f8lkclx1g")))

