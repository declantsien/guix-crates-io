(define-module (crates-io x- la) #:use-module (crates-io))

(define-public crate-x-launcher-0.1 (crate (name "x-launcher") (vers "0.1.0") (deps (list (crate-dep (name "shell-words") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0syr320r6pha9a99nwjixl436dy7cd1hkb3v6hy3xb80bl81wclw") (yanked #t)))

(define-public crate-x-launcher-0.1 (crate (name "x-launcher") (vers "0.1.1") (deps (list (crate-dep (name "shell-words") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1rkj7yb98qdya9b8wzy741qkvkfm3k9zxwgk85gibn43ijrk5xhl") (yanked #t)))

(define-public crate-x-launcher-0.1 (crate (name "x-launcher") (vers "0.1.2") (deps (list (crate-dep (name "shell-words") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "059r7jway0iwzf9xsk28wy09m4p3b7c3r6pc0rbvvm89azm0jniq")))

