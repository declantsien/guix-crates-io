(define-module (crates-io x- me) #:use-module (crates-io))

(define-public crate-x-media-0.1 (crate (name "x-media") (vers "0.1.0") (deps (list (crate-dep (name "core-audio-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "core-media") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "core-video") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "x-variant") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yhwzza6h805k97dwfi3cb9f9p408a2ykghyk3nwlic78a5yaw3f")))

