(define-module (crates-io x- bo) #:use-module (crates-io))

(define-public crate-x-bow-0.1 (crate (name "x-bow") (vers "0.1.0") (deps (list (crate-dep (name "observables") (req "^0.1.0") (kind 0)) (crate-dep (name "x-bow-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15phgl5ybmnk36vd12g2zcb8bgwgwcqbbm3b1yhrj7xcf1ffm7wm")))

(define-public crate-x-bow-0.2 (crate (name "x-bow") (vers "0.2.0") (deps (list (crate-dep (name "async_ui_internal_utils") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.13.0") (default-features #t) (kind 2)) (crate-dep (name "nohash-hasher") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.3.0") (features (quote ("macro"))) (default-features #t) (kind 2)) (crate-dep (name "x-bow-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1b41h6gkwb0nhkxclh88b5dlbjhsym88qk6z5y13byngkg0frgmd")))

(define-public crate-x-bow-macros-0.1 (crate (name "x-bow-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fg6b19zkh9icmji7jw2fscf74h62agnrhs6ac59nih4fqd7wyf5")))

(define-public crate-x-bow-macros-0.2 (crate (name "x-bow-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0nhy6khhcsnprqbkh7j1prgpr5f906a8p8466zjgr3544d42xz58")))

