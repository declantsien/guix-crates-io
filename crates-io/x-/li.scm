(define-module (crates-io x- li) #:use-module (crates-io))

(define-public crate-x-lib-0.1 (crate (name "x-lib") (vers "0.1.0") (hash "0ymgw0nrbi2lbv3kkpzxhq9wfkijzgmf7xyl9xhp4i07rgsyimc1")))

(define-public crate-x-lint-0.1 (crate (name "x-lint") (vers "0.1.0") (deps (list (crate-dep (name "camino") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "guppy") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "hakari") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "x-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05zcn7nlijmq63n7yrsn7fgxh864n6yz94m697jz61czkqj6ybiq")))

