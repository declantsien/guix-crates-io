(define-module (crates-io x- ui) #:use-module (crates-io))

(define-public crate-x-ui-0.1 (crate (name "x-ui") (vers "0.1.0") (deps (list (crate-dep (name "x-base") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "x-media") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "x-variant") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1cavwhzprbx5acdgwd6r0lfrzfjprjg3wy0s671y8ln9zz82li3b")))

