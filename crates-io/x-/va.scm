(define-module (crates-io x- va) #:use-module (crates-io))

(define-public crate-x-variant-0.1 (crate (name "x-variant") (vers "0.1.0") (hash "0m0q9swxm0cvnlnzr2micjsy246ainf7g83s55crf43snycx259h")))

(define-public crate-x-variant-0.1 (crate (name "x-variant") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0fb2v9wkianp7v18l3a18d2n6955da4w2jq7a13r9p72c0c03f5p") (features (quote (("serde-prefix-nums") ("default" "serde"))))))

