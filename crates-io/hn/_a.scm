(define-module (crates-io hn _a) #:use-module (crates-io))

(define-public crate-hn_api-0.1 (crate (name "hn_api") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1clag28r41mcc97sb4k56s1hpn7f5r9skw397v6pmq23ysljqy6n")))

