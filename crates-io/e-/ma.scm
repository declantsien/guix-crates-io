(define-module (crates-io e- ma) #:use-module (crates-io))

(define-public crate-e-macros-0.1 (crate (name "e-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "1v99azc5hgsdn2bxxf0bygybhs0nbaqczcl4w21v9wyf2fdvhyqw") (yanked #t) (rust-version "1.76.0")))

(define-public crate-e-macros-0.1 (crate (name "e-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0wgbhjck307f7apzh4f3bf0ypg8im6cnzb0y6w0db194jw972r8w") (yanked #t) (rust-version "1.76.0")))

(define-public crate-e-macros-0.1 (crate (name "e-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "1bgg15kcg8nsbp5c5hsgkrfhmhli4vjz9g3wk41cddifbi85d0qi") (yanked #t) (rust-version "1.76.0")))

(define-public crate-e-macros-0.1 (crate (name "e-macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "123xn2zm1rd8ljgjiaxm27bysvydganw0vhv1kc5n2dyff0yjkdg") (yanked #t) (rust-version "1.76.0")))

(define-public crate-e-macros-0.1 (crate (name "e-macros") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0j86qgjlv304xkzwm57wa517jlbrpzrjpc4ycb35y2rm8frs0pgf") (yanked #t) (rust-version "1.76.0")))

(define-public crate-e-macros-0.1 (crate (name "e-macros") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "16amhfkwgk6hrmjcdjb4yl8iybq3amvyn8xz65zvxg8xzy08hfiz") (rust-version "1.76.0")))

(define-public crate-e-macros-0.1 (crate (name "e-macros") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0zkcf7nwpz8b959v00rv8s520r5s5jp3i6vzcmybg615pd7xy7y3") (rust-version "1.76.0")))

