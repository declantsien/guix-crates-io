(define-module (crates-io e- ri) #:use-module (crates-io))

(define-public crate-e-ring-0.1 (crate (name "e-ring") (vers "0.1.0") (hash "169vpqg0nkac5zvhzlphvzdi81z6kmv411h8f44dbc47yw4q707g")))

(define-public crate-e-ring-0.2 (crate (name "e-ring") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-graphics") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)))) (hash "0md21ihgvkqzica5nzx8axr29748smmvbms6fv63kdvdmi9bbk23") (features (quote (("hist" "embedded-graphics"))))))

(define-public crate-e-ring-0.3 (crate (name "e-ring") (vers "0.3.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-graphics") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0425ksm3q0ii9766nhp0gzqb9mdmqwfsrzp4hglqjd7csz9xn2b6") (features (quote (("hist" "embedded-graphics"))))))

