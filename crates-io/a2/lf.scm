(define-module (crates-io a2 lf) #:use-module (crates-io))

(define-public crate-a2lfile-0.8 (crate (name "a2lfile") (vers "0.8.0") (deps (list (crate-dep (name "a2lmacros") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hfbm0mqkvigx377dxrpkzzlb3q1dm0h3xsy9xs0l6y7yf56rhdk")))

(define-public crate-a2lfile-0.8 (crate (name "a2lfile") (vers "0.8.1") (deps (list (crate-dep (name "a2lmacros") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1bnfznlpy05aizxrqj7n5cf6734alxjivd4nq3q9d457fi96rpya")))

(define-public crate-a2lfile-0.8 (crate (name "a2lfile") (vers "0.8.2") (deps (list (crate-dep (name "a2lmacros") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "116349sbvfk2nb6q3qp9vr02xzqxqqnf38dw5b3icqpvpvspy3d6")))

(define-public crate-a2lfile-0.9 (crate (name "a2lfile") (vers "0.9.0") (deps (list (crate-dep (name "a2lmacros") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "02fd44ivji194sbc0dcr9b243k9svr9v8z3abagmdnlj33qw8aw1")))

(define-public crate-a2lfile-0.9 (crate (name "a2lfile") (vers "0.9.1") (deps (list (crate-dep (name "a2lmacros") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "182baq295ayp29lkhpgl6w9bbq07mimfnzzsc8n30452nprh7d8d")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.0.0") (deps (list (crate-dep (name "a2lmacros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0vb85awj3xiz64ngqcq59sg3g8bfsbgdiwsrgfpp4n69nniylpsi")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.0.1") (deps (list (crate-dep (name "a2lmacros") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1d9c27rxmb5cly5lk9vav5w8qf32mafi21rvcbgkxi4lgrrjdn77")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.1.0") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "112il66fxplz6crss5n4bgf31d5pafm9kpdsgvcgq29bjzzdxigc")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.1.1") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0s5x1qls2cqlkwxj264v0xgc0dpq70f1jf949fjljpr7svpjr9gl")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.2.0") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0g3gn3gpa2vrymb8wfipwdw88xgf4vvpbmnw73n18ghw0dwgg0r4")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.3.0") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "13ps2sw6ix9p4kca9wb29svy31xihllpm4j37dn10dgzsb5nlrh8")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.3.1") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0ip2q9r2glpgycl5phgqyfgnzqvmxih6b0ng4h85g9l9bg31irrc")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.3.2") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0x4i42axmhvv3z3pcj9313lvmx026lkjf1a5r8gzh6sp7xp620c8")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.3.3") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1rbzizmsw9la75yyllhqrzd8ylbz4bqgqf78hk9agqjg8xz4757v")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.3.4") (deps (list (crate-dep (name "a2lmacros") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "19j6zxxhabdzz98r3rrhrj48bdjxpgmh2niqdxk5nbb8fr5ar7l9")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.4.0") (deps (list (crate-dep (name "a2lmacros") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1dkjls6wxa1rrw5rrmkfq2vgsbmjjcgfyr52fc7l3nh3q189wa0z")))

(define-public crate-a2lfile-1 (crate (name "a2lfile") (vers "1.5.0") (deps (list (crate-dep (name "a2lmacros") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0bmg64skja3glkh6ck1b3q761x544pca2vik8n9k9cbl9pipggr2")))

(define-public crate-a2lfile-2 (crate (name "a2lfile") (vers "2.0.0") (deps (list (crate-dep (name "a2lmacros") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1dfcpajbsv38lx5qqp9mgmmlpx22ijg2933kkhf63450j8k88rhs")))

