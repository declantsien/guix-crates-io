(define-module (crates-io a2 -m) #:use-module (crates-io))

(define-public crate-a2-memory-map-1 (crate (name "a2-memory-map") (vers "1.0.4") (deps (list (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "08ak6mvxflp768rlr12jl7j8zx09fapqb7dw43952y4bl4qf824f")))

