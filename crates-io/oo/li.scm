(define-module (crates-io oo li) #:use-module (crates-io))

(define-public crate-oolisp-0.1 (crate (name "oolisp") (vers "0.1.9") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kd0f509ffjdwnpzalsvm1d3xa0jy0f9imiyajbvqb6f0g9vkrrv")))

