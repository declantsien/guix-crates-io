(define-module (crates-io oo dl) #:use-module (crates-io))

(define-public crate-oodle-safe-0.1 (crate (name "oodle-safe") (vers "0.1.0") (deps (list (crate-dep (name "oodle-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pq1c6p175vdc3m6rg2480c6rkfmdpp4gmgvgkapgg95q29vrk1d") (features (quote (("bindgen" "oodle-sys/bindgen"))))))

(define-public crate-oodle-safe-0.2 (crate (name "oodle-safe") (vers "0.2.0") (deps (list (crate-dep (name "oodle-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1fb85gkl05zf5hn6ljrd6afxh1rjslp56svbrrljjm01rzazsk92") (features (quote (("bindgen" "oodle-sys/bindgen"))))))

(define-public crate-oodle-sys-0.1 (crate (name "oodle-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (features (quote ("runtime" "which-rustfmt"))) (optional #t) (kind 1)))) (hash "0r57qg0sg5gazwk5h12prd6wcgjlr9rcvk1ffgc2m7ldhwpa3a2z") (links "oo2corelinux64")))

(define-public crate-oodle-sys-0.2 (crate (name "oodle-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (features (quote ("runtime" "which-rustfmt"))) (optional #t) (kind 1)))) (hash "1k5qg8a8fdmwm2f4wr6qn7g62kh00d4a117ysx844iv35s0mq95p") (links "oo2corelinux64")))

