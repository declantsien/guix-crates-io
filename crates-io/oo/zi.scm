(define-module (crates-io oo zi) #:use-module (crates-io))

(define-public crate-oozie-0.1 (crate (name "oozie") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "10zs36ajj3sq2v6vbm4msjgj429yrv6i63g21zb5ldfi0a17r1zl")))

(define-public crate-oozie-0.1 (crate (name "oozie") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "129m0qfpvviq2kslfwvnidghczszmqffw7k7wgrw535d0kdwmc55")))

(define-public crate-oozie-0.1 (crate (name "oozie") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0vs6g5kxpraj755lrrgj0l3pgvz6w977xrj93h7r92sbrfspxwlh")))

