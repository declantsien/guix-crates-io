(define-module (crates-io oo p_) #:use-module (crates-io))

(define-public crate-oop_in_rust-0.1 (crate (name "oop_in_rust") (vers "0.1.0") (hash "09m82369jnggrkd2qi5wad7cpqkgn5d01dh55kyaqgw63x75cblq")))

(define-public crate-oop_inheritance-1 (crate (name "oop_inheritance") (vers "1.0.0") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "oop_inheritance_proc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1jpx4dg5w0sgvih573jzqyb89fb3l71vrijggi64csp6ca6fm34k")))

(define-public crate-oop_inheritance-1 (crate (name "oop_inheritance") (vers "1.0.1") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "oop_inheritance_proc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "08s5kdifwpwxzj5vxd4c9bnv144v71cqfinbmif2vl2fizxy7k8v")))

(define-public crate-oop_inheritance_proc-1 (crate (name "oop_inheritance_proc") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1l6qf2zrwgg6am5pmhfl99hlr5ag20sha812p0ydzxascqdpw2cc")))

