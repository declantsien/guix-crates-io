(define-module (crates-io oo lo) #:use-module (crates-io))

(define-public crate-oolong-0.1 (crate (name "oolong") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)))) (hash "1f6xkihgvsl0h1nchv4hj72khnzb2y1hvli65f1p4albbwz4vp3y")))

