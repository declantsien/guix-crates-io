(define-module (crates-io oo mf) #:use-module (crates-io))

(define-public crate-oomfi-0.1 (crate (name "oomfi") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1kkvwnxnkf4ygfxd9kpy3mmhmylyh68amaxpw8frf5fdspc99imi") (yanked #t)))

(define-public crate-oomfi-0.1 (crate (name "oomfi") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0x15xxs5p0b1731zq0p6yahy0njapzciq2nm3375c18gsgf09dxi")))

(define-public crate-oomfi-0.1 (crate (name "oomfi") (vers "0.1.2") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1xqrw79bb4mrrlq9pqgdy7h3v02c8kw8nh4xkpxqwn2r9g6j6sjj")))

