(define-module (crates-io oo #{7-}#) #:use-module (crates-io))

(define-public crate-oo7-cli-0.3 (crate (name "oo7-cli") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("alloc" "clock"))) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "oo7") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "rt-multi-thread"))) (kind 0)))) (hash "1hq2kg862d5gamw4aj3cbwxdf9dizryz7l9150cm52kflmygabnf") (rust-version "1.75")))

