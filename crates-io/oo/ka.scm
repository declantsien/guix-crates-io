(define-module (crates-io oo ka) #:use-module (crates-io))

(define-public crate-ookami-0.1 (crate (name "ookami") (vers "0.1.0") (deps (list (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "0ryryqxqcg45dvbm11fws3a84bx1xl8kcyzpvcd74jsr1d4jpgvx")))

(define-public crate-ookami-0.2 (crate (name "ookami") (vers "0.2.0") (deps (list (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "1kha1fnqbxdpi0l5kgcicvnjqya23yy091xmmb169igl7k19p59x")))

