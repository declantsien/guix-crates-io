(define-module (crates-io oo p-) #:use-module (crates-io))

(define-public crate-oop-macro-0.0.0 (crate (name "oop-macro") (vers "0.0.0-alpha.0") (hash "1bs7mnmwdkc7wskhyrw7ik7psd55rw5b2vhh90rwgac0r8vlvf32")))

(define-public crate-oop-macro-0.0.1 (crate (name "oop-macro") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("default" "full"))) (default-features #t) (kind 0)))) (hash "1krxqk3kvzych4gnsd2sjd0c4m6g3sk5z4rpgphy3yli69i1z4z1")))

(define-public crate-oop-macro-0.0.2 (crate (name "oop-macro") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("default" "full"))) (default-features #t) (kind 0)))) (hash "115z26zqqkjk0c03cgs0mcnafrq8cijv1xqnjwm8bgl8rwyh94z4")))

