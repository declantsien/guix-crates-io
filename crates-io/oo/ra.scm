(define-module (crates-io oo ra) #:use-module (crates-io))

(define-public crate-oorandom-0.1 (crate (name "oorandom") (vers "0.1.0") (hash "1f9ba3jv6599kgk27p4llkv617zkd0s1mpi10qg8l96p3fzbbwx3")))

(define-public crate-oorandom-9 (crate (name "oorandom") (vers "9.3.0") (deps (list (crate-dep (name "randomize") (req "^3.0.0-rc.2") (default-features #t) (kind 2)))) (hash "14dchijxmrhk3fg0wgyrpc2cgrsd91lnka8iz5k1xcsd6sgvi552")))

(define-public crate-oorandom-9 (crate (name "oorandom") (vers "9.3.1") (deps (list (crate-dep (name "randomize") (req "^3.0.0-rc.2") (default-features #t) (kind 2)))) (hash "1gbb9140p1006i19fp1cjys0mck404y99zcn47mkc12670n1v6y9")))

(define-public crate-oorandom-9 (crate (name "oorandom") (vers "9.3.2") (deps (list (crate-dep (name "randomize") (req "^3.0.0-rc.2") (default-features #t) (kind 2)))) (hash "18i0dhznnlv8l3gyiqw1ih48sh3dkflidw7s7df0yssbk6snvlzn")))

(define-public crate-oorandom-9 (crate (name "oorandom") (vers "9.3.3") (deps (list (crate-dep (name "randomize") (req "^3.0.0-rc.2") (default-features #t) (kind 2)))) (hash "1hk6qcp6k5sqx9zh19y5hjmxyxcagv7jhlipflzmdzcnpvzdm9if")))

(define-public crate-oorandom-9 (crate (name "oorandom") (vers "9.3.4") (deps (list (crate-dep (name "randomize") (req "^3.0.0-rc.2") (default-features #t) (kind 2)))) (hash "1ql14gk13c3a4mcqd8zln3a8ndr8hpy12ar37fchzpw0qfi6h950")))

(define-public crate-oorandom-9 (crate (name "oorandom") (vers "9.5.9") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0-rc.2") (default-features #t) (kind 2)))) (hash "1vhv65lwakvphkggagdlfaydrfzdmm29gbi9j5s75dqmx6772zgn")))

(define-public crate-oorandom-10 (crate (name "oorandom") (vers "10.0.0") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "073w1117glzj50rqrnpzkyn3s63wmw6lq590rg52bqmj870wrfiv")))

(define-public crate-oorandom-10 (crate (name "oorandom") (vers "10.0.1") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "1zzqj53v28mm69wrb25yjddjwz17w9dijb10wgsv2iiab8h4f1da")))

(define-public crate-oorandom-11 (crate (name "oorandom") (vers "11.0.0") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "1n2108s1y1rsjf06x5kiylbyqb7wls97sfpz5fn2z2fyzhks8kdx")))

(define-public crate-oorandom-11 (crate (name "oorandom") (vers "11.0.1") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "random-fast-rng") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "0ji0xjfqp8bgvzancjdjs2sphnhn4nppghqagcwxqz5lihr2mqq9")))

(define-public crate-oorandom-11 (crate (name "oorandom") (vers "11.1.0") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "random-fast-rng") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "01clxfnz1zwg4maynvbgj09wlkj5m3c8kjqfrp3sqp59qb4wgkpb")))

(define-public crate-oorandom-11 (crate (name "oorandom") (vers "11.1.1") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "random-fast-rng") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "0n33kc6bci43762knisghpi23alasj2ckqp43ccn0zrwqddk5bwl")))

(define-public crate-oorandom-11 (crate (name "oorandom") (vers "11.1.2") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "random-fast-rng") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "0b0bmi76bfywdwllnrpc2ksmvcw05ykqbnz4jbm0i811h2ywww51")))

(define-public crate-oorandom-11 (crate (name "oorandom") (vers "11.1.3") (deps (list (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "random-fast-rng") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "randomize") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "0xdm4vd89aiwnrk1xjwzklnchjqvib4klcihlc2bsd4x50mbrc8a")))

