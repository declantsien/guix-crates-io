(define-module (crates-io oo zl) #:use-module (crates-io))

(define-public crate-oozle-0.1 (crate (name "oozle") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0lv64kwlr6f0529lxj5vzwpygn4n79q6ik6yw5bg00ihgqxmxxw1")))

(define-public crate-oozle-0.1 (crate (name "oozle") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1sh9yyfgmixd1irybk2r3dal16wbp0a598i4z9v4crlcwk2jgd2n")))

(define-public crate-oozle-0.1 (crate (name "oozle") (vers "0.1.2") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1vyryb8m0nqp2nn78byvw44msjx9s1gz2jknn3yfxwxbrmyn6x7k")))

(define-public crate-oozle-0.1 (crate (name "oozle") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1p7nib2hnb52f8i66d924w8kw4js31d40sy6n9plgj5cxs3jrpb4")))

(define-public crate-oozle-0.1 (crate (name "oozle") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "00nrvkl6zi7v6hdbvcyskbybfyaqkm6lqlw30p88wvr5nbzznv7m")))

(define-public crate-oozle-0.1 (crate (name "oozle") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0bi81n53s0spyaspjgighxi23q44n6bvdbiv4zdcjmid6a5fayzw")))

