(define-module (crates-io oo la) #:use-module (crates-io))

(define-public crate-OOLANG-0.1 (crate (name "OOLANG") (vers "0.1.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1hpsiklwl6ir54jcbipbpk6sdz5hqpvvl4ff45rik53pxwh1j8iv") (yanked #t) (rust-version "1.58")))

(define-public crate-OOLANG-0.1 (crate (name "OOLANG") (vers "0.1.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1nf6cnn8bkp46fz4mxc0fhbz0bjs1m06alrg2rm9p1xr5acvsfn7") (rust-version "1.58")))

