(define-module (crates-io oo z-) #:use-module (crates-io))

(define-public crate-ooz-sys-0.1 (crate (name "ooz-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)))) (hash "13ijvvk1xm6hw6pb0kvlsgrsapddm9kj8p3g7fz2d0r0hgfsr7qq") (yanked #t)))

(define-public crate-ooz-sys-0.2 (crate (name "ooz-sys") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)))) (hash "0247ypkd04b6zr2bz71f7hafdnnrwch8sabflr89gj7zzpna7843") (yanked #t)))

