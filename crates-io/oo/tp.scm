(define-module (crates-io oo tp) #:use-module (crates-io))

(define-public crate-ootp-0.0.1 (crate (name "ootp") (vers "0.0.1") (deps (list (crate-dep (name "hmac-sha1") (req "~0.1.3") (default-features #t) (kind 0)))) (hash "1k7wah3102pw7mdjh7zm6ilm15k4yzhda93v33jaiwqd94bp2pl6")))

(define-public crate-ootp-0.0.2 (crate (name "ootp") (vers "0.0.2") (deps (list (crate-dep (name "base32") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "hmac-sha1") (req "^0.1") (default-features #t) (kind 0)))) (hash "1shjrhqwl5sks27ha906625j4msdlwqd63hvh9ysw11w6vn52l45")))

(define-public crate-ootp-0.0.3 (crate (name "ootp") (vers "0.0.3") (deps (list (crate-dep (name "base32") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "hmac-sha1") (req "^0.1") (default-features #t) (kind 0)))) (hash "14izl4285ldknb2aprr0h846kzm3xyqfa9gxwn374569y7xnn39j")))

(define-public crate-ootp-0.0.4 (crate (name "ootp") (vers "0.0.4") (deps (list (crate-dep (name "hmac-sha1") (req "^0.1") (default-features #t) (kind 0)))) (hash "0iniasbhbspkkkl7qlbpv4kpzh5chgj1mx58kwv4xxc0qpzyfa7f")))

(define-public crate-ootp-0.0.5 (crate (name "ootp") (vers "0.0.5") (deps (list (crate-dep (name "hmac-sha1") (req "^0.1") (default-features #t) (kind 0)))) (hash "1iwq38vyw7gc0b99303kj3c7d4j6lcsag5wmh02v1n96rvgi7wpa")))

(define-public crate-ootp-0.0.6 (crate (name "ootp") (vers "0.0.6") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "hmac-sha1") (req "^0.1") (default-features #t) (kind 0)))) (hash "01l9q6la7p8jqybx0wvyc1qihf3gmg7slxg4awhh5axpsbarf67a")))

(define-public crate-ootp-0.0.7 (crate (name "ootp") (vers "0.0.7") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "hmac-sha") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dd6rzww96qk5qvn4l3gwk3h0b7z5zxs456bava4zz420zq0x8vp")))

(define-public crate-ootp-0.1 (crate (name "ootp") (vers "0.1.0") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "hmac-sha") (req "^0.4") (default-features #t) (kind 0)))) (hash "18q85l2ss43yr97w3avdw7cafc7lj8if2gvdkypr4zdpyzldagkm")))

(define-public crate-ootp-0.1 (crate (name "ootp") (vers "0.1.1") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "hmac-sha") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0946y0zndzj2bvcnj5j9gy5m631vsn25871y0yj81yqz3aw9lsj5")))

