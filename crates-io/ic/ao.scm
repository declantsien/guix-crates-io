(define-module (crates-io ic ao) #:use-module (crates-io))

(define-public crate-icao-isa-0.1 (crate (name "icao-isa") (vers "0.1.0") (deps (list (crate-dep (name "icao-units") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j0p62hd3j0sk32m4d10iclfc59dv1i1j9q8x4yq05ncq0v9ngy4")))

(define-public crate-icao-isa-0.1 (crate (name "icao-isa") (vers "0.1.2") (deps (list (crate-dep (name "icao-units") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "04ibfagqgwqmfxhfrzfdjwafwvkdhc0wdj3xdm2b5fh050vkpp9d")))

(define-public crate-icao-units-0.1 (crate (name "icao-units") (vers "0.1.0") (hash "0fjd9wm80cznxb5mz1fby529xflmikasfv6355538n4x19dwn6lp")))

(define-public crate-icao-units-0.1 (crate (name "icao-units") (vers "0.1.1") (hash "0a3rwlwywgxry9yi2zccp3lh2ylqs76qxaswfcm1xslyigysxs9d")))

(define-public crate-icao-units-0.1 (crate (name "icao-units") (vers "0.1.2") (hash "1lr2l4aavrmadzc6agmv81r8gbrj4ngc1g8gy67kgbiyjl9r2hh3")))

(define-public crate-icao-units-0.1 (crate (name "icao-units") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0cpchvw44zda8l029bgj0lzhrqs5129525xcc9iksshzjkqxdyyx")))

(define-public crate-icao-wgs84-0.1 (crate (name "icao-wgs84") (vers "0.1.0") (deps (list (crate-dep (name "angle-sc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "icao-units") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unit-sphere") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gnliay6q84b9akzzj81iralam98srw91vzzp66qz9b3ikxijn72")))

(define-public crate-icao-wgs84-0.1 (crate (name "icao-wgs84") (vers "0.1.1") (deps (list (crate-dep (name "angle-sc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "icao-units") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unit-sphere") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mzan2dg5wc6gqswd0n9dm4xssy0ip1a6n9j1rq3vcd5zrayw9n1")))

(define-public crate-icao-wgs84-0.1 (crate (name "icao-wgs84") (vers "0.1.2") (deps (list (crate-dep (name "angle-sc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "icao-units") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unit-sphere") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ik1cdvdcs3qspq2qpawfi347r56jn6zmxdp3b3kc86qmwkqa05q")))

