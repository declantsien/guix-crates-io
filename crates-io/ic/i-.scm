(define-module (crates-io ic i-) #:use-module (crates-io))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lkbg5lnb8qfn1dzckmjg3pc7fg9s16dd5qqs8wb9firgmh8w54d")))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pl7xanwlj84aslcjryk5p47l60chnz105lfgflblh6if5dsmprs")))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "18ks5z7g9ma3ff6ff0zzklv734wlycirm7fvlgnfpr4yb90ljb0m")))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kskf3jkma33p2lpkmfns0l72l59h17pb0091939f27hwlhv1gyy")))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "15wifp7yac9yhjpfip7w96xn4k4bwbwmzkrjxx9kblydmc8fl4hl")))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.5") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0swjfzggh51lqvk85vvprph09anah0z38mbjzcx0ysa0iphw23fk")))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.6") (deps (list (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0fz5v1kh09wxqjkzc3605pmlqd0ylc074xin4nbasyk6l3fq926g")))

(define-public crate-ici-files-0.1 (crate (name "ici-files") (vers "0.1.7") (deps (list (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "02w704qc8750bnky64wmm9yjqvdxb62pbs7sq8jgyxhkhcb9k785")))

(define-public crate-ici-files-0.2 (crate (name "ici-files") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1f54mp1aggnpnlr7dy76926wngqlqgv8lpy7667rxy2574i1jwzf") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ici-files-0.2 (crate (name "ici-files") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1xyr9gh2lwk8xp9jsl6xgz6s8gvng0fzvmnsd01lp564gqvrxca7") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ici-files-0.2 (crate (name "ici-files") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0hna098lkwa0hbms30mx12vwqghx0xrrbaz7abvjg5zig1dkjyy8") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ici-files-0.2 (crate (name "ici-files") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0zhyaa37r0mzr66nylmxmd5fpnlb5f45gsnkbkrfc1iqqmjj8kxv") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

