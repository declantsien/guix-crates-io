(define-module (crates-io ic mp) #:use-module (crates-io))

(define-public crate-icmp-0.1 (crate (name "icmp") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0pqnikzq1ya64n9ivdb64h3r3x3a74iw01rs2wplnbv6c6sh0fr8")))

(define-public crate-icmp-0.1 (crate (name "icmp") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1r05dvs64xn2kmbk7s723ama8a7vy9xx4y3v04n5cziixmvm6svl")))

(define-public crate-icmp-0.1 (crate (name "icmp") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0l9cswppcdf7qc1rjhm7inlfnhljlz10a9z8cssafprgwfd1qvhq")))

(define-public crate-icmp-0.1 (crate (name "icmp") (vers "0.1.3") (deps (list (crate-dep (name "clippy") (req "^0.0.95") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0nhaxqvdwmwvllpg9agrkwykrc6fi8lx6283yaqksd70wimbckh2")))

(define-public crate-icmp-0.1 (crate (name "icmp") (vers "0.1.4") (deps (list (crate-dep (name "clippy") (req "^0.0.95") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0pbgib5mhdy3pzpz4ansxxibkgd8hrycnccqhra4bff7lrdgr776")))

(define-public crate-icmp-0.1 (crate (name "icmp") (vers "0.1.5") (deps (list (crate-dep (name "clippy") (req "^0.0.95") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "142ic08x4mm9fbsdxim8r6nybhg1k32zh76y9sfp0n67c911qldj") (features (quote (("default"))))))

(define-public crate-icmp-0.3 (crate (name "icmp") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "05ifpc98k5hc2xrmr3sydgsb3gbf2aiqcs5ligyqpgayh2yyjwxc") (features (quote (("default"))))))

(define-public crate-icmp-client-0.0.0 (crate (name "icmp-client") (vers "0.0.0") (hash "1a7rki0k41smmyz5a5jkw4rz0fa5x8qj94y55qb3169rdpw5qyz1") (yanked #t)))

(define-public crate-icmp-client-0.1 (crate (name "icmp-client") (vers "0.1.0") (deps (list (crate-dep (name "async-io") (req "^1") (optional #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (kind 0)) (crate-dep (name "icmp-packet") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "socket2") (req "^0.5") (features (quote ("all"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "134y9np1nrwmlflqzjmisdsp0lk84z9sg9mpgq099qsvdri0dipy") (features (quote (("impl_tokio" "tokio") ("impl_async_io" "async-io") ("default" "impl_tokio"))))))

(define-public crate-icmp-client-0.1 (crate (name "icmp-client") (vers "0.1.1") (deps (list (crate-dep (name "async-io") (req "^1") (optional #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (kind 0)) (crate-dep (name "icmp-packet") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "socket2") (req "^0.5") (features (quote ("all"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0b6mljg2xywv0d9994nml1hlx5bm1ggcqwji48wp80nng3nq42w2") (features (quote (("impl_tokio" "tokio") ("impl_async_io" "async-io") ("default" "impl_tokio"))))))

(define-public crate-icmp-client-0.2 (crate (name "icmp-client") (vers "0.2.0") (deps (list (crate-dep (name "async-io") (req "^1") (optional #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (kind 0)) (crate-dep (name "icmp-packet") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "os_info") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "socket2") (req "^0.5") (features (quote ("all"))) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0pw4ralaj1xl4l611qlz44dhl56bqjfpmw1xcy8nilw0i5dcm04v") (features (quote (("impl_tokio" "tokio") ("impl_async_io" "async-io") ("default" "impl_tokio"))))))

(define-public crate-icmp-packet-0.0.0 (crate (name "icmp-packet") (vers "0.0.0") (hash "0c1z2lr7lmcq5mr47rsgviyd127f8wva10fazg5d85s70qwkgb58") (yanked #t)))

(define-public crate-icmp-packet-0.1 (crate (name "icmp-packet") (vers "0.1.0") (deps (list (crate-dep (name "pnet_packet") (req "^0.33") (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("std" "std_rng"))) (optional #t) (kind 0)) (crate-dep (name "wrapping-macro") (req "^0.2") (kind 0)))) (hash "1mnpx27j79xvh5b43lfdvgr7lnfhv32xpwn0ygdlg7my34zdl6m1")))

(define-public crate-icmp-socket-0.1 (crate (name "icmp-socket") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "1m0i38vyl74ppkdkchkkys400gnji7pjayhcr9crlak2032vwjys")))

(define-public crate-icmp-socket-0.1 (crate (name "icmp-socket") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "1y7c0bzyd4vb9dlwcz7w4c5ixdgma5wkfrh7sax16c994drv70wc")))

(define-public crate-icmp-socket-0.1 (crate (name "icmp-socket") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.4.4") (features (quote ("all"))) (default-features #t) (kind 0)))) (hash "08rfm9fx5dixwq84ipmv7i8980jy5q2i77g6iiifwqz2ymjfc6hx")))

(define-public crate-icmp-socket-0.2 (crate (name "icmp-socket") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.4.4") (features (quote ("all"))) (default-features #t) (kind 0)))) (hash "01gnga88lzfjpck4giwvymqk80p1fmzw29wlf012ysydhapkvg4q")))

(define-public crate-icmptunnel-rs-0.1 (crate (name "icmptunnel-rs") (vers "0.1.0") (deps (list (crate-dep (name "chacha20poly1305") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "pnet_sys") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "tun-tap") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.0") (features (quote ("reusable_secrets"))) (default-features #t) (kind 0)))) (hash "1qvaxsq2jaa73rb5gzh4q85gl8ycsjzn8aik1kdmxp8r49qwpxh1")))

