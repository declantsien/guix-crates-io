(define-module (crates-io ic ec) #:use-module (crates-io))

(define-public crate-icecast-stats-0.1 (crate (name "icecast-stats") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1apmvj4ym7107svhd7ykcq8rvips1lj7mi7kyifkka0ivndiix9g")))

(define-public crate-icecast-stats-0.1 (crate (name "icecast-stats") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.5") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0lij7wmd9yyb10j92lwxgn1d7hjjasc9cgz709y8fi2a9va0xm53")))

(define-public crate-icecast-stats-0.1 (crate (name "icecast-stats") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "17dmgk84g4q06sjvx58mfajcg4yqrnksxhsl21k432sj52wwkg3p")))

(define-public crate-icecream-0.0.1 (crate (name "icecream") (vers "0.0.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xqmi39zfxrgw94nzq6gkff8ysx5qn50057dd3yd89jxyfw42i8v")))

(define-public crate-icecream-0.0.2 (crate (name "icecream") (vers "0.0.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ivqwjia28jnb8p2ya0h71d9dlzg721ghq670q2pkamm8848113z")))

(define-public crate-icecream-0.1 (crate (name "icecream") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gag") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ganydvvvwvza37rqrqwknhmqa4nz6i55brlga8svd1hhv75zl9c")))

(define-public crate-icecream-rs-0.0.1 (crate (name "icecream-rs") (vers "0.0.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "07diihny9iwy9f62q4n6zn6v9fl22ipr9z6m7szrvk6hrxbzq5il")))

