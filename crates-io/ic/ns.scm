(define-module (crates-io ic ns) #:use-module (crates-io))

(define-public crate-icns-0.1 (crate (name "icns") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.5") (default-features #t) (kind 0)))) (hash "1qp2s0494pspfacfyp2kay2svn62yd6k60gw29zl49g44wcjxjpi")))

(define-public crate-icns-0.2 (crate (name "icns") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.5") (default-features #t) (kind 0)))) (hash "149mss6jhljc0m1109ak6i387fc0wsaa0l0sdr2c499vxh9vfnj2")))

(define-public crate-icns-0.2 (crate (name "icns") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.6") (default-features #t) (kind 0)))) (hash "0maf3010n0cxlg9wdyyvjvl9j8izx1ia384ip1v1qrg3fyrjn80n")))

(define-public crate-icns-0.2 (crate (name "icns") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.11") (default-features #t) (kind 0)))) (hash "0s4fsgv4srnik5h8i2x4jfcdjbf5ycgshs2r6z3lapinjfgz295k")))

(define-public crate-icns-0.3 (crate (name "icns") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.13") (optional #t) (default-features #t) (kind 0)))) (hash "0bb7p3dwgx95wnj4rp45yk9g2znhj726jga4f6zj62yagg9c4r5f") (features (quote (("pngio" "png") ("default" "pngio"))))))

(define-public crate-icns-0.3 (crate (name "icns") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16") (optional #t) (default-features #t) (kind 0)))) (hash "0h4slnysg38bpaqa6iaxrqjk3ndglna4k4lanjjp1nh8gsnzpk55") (features (quote (("pngio" "png") ("default" "pngio"))))))

(define-public crate-icns-rs-0.1 (crate (name "icns-rs") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "1wpanjg4bl0w7mq2a7r9nxxfn86frq2sy6lw4l596b51qx9iccwn")))

(define-public crate-icns-rs-0.1 (crate (name "icns-rs") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "02fq8a06jgrxli2vqxhairzix14h7sffx7mk2s279xvy44ic1004") (yanked #t)))

(define-public crate-icns-rs-0.1 (crate (name "icns-rs") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)))) (hash "0lm44xwf2x3jrhh8wcpx9cgwkakfc3mvwlq6dkpfhy8mq246j8gf")))

(define-public crate-icnsify-0.1 (crate (name "icnsify") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "icns") (req "^0.1") (default-features #t) (kind 0) (package "tauri-icns")) (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "0dl8fzyvfwajlis4wga48ambp9x783i50rni2f8gjys6p3q0wn0q")))

(define-public crate-icnsup-0.1 (crate (name "icnsup") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "icns") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "0cap44ycy5ji7gqnv75l8kfz5msvn0p7ha853vvrcfiz9qlvbz1s")))

(define-public crate-icnsup-0.2 (crate (name "icnsup") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "icns") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "05ba6jjjljq12shkv3y7jhbgwns4fxsczn3bhqklwywn1dk31fsv")))

