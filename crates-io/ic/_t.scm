(define-module (crates-io ic _t) #:use-module (crates-io))

(define-public crate-ic_tx-0.0.1 (crate (name "ic_tx") (vers "0.0.1") (deps (list (crate-dep (name "candid") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1rksdnyl12yh4diirjvx5n369vz2gyxjcap0v85rnxx1z7cvm81k") (features (quote (("default")))) (v 2) (features2 (quote (("candid" "dep:candid" "dep:serde"))))))

