(define-module (crates-io ic o-) #:use-module (crates-io))

(define-public crate-ico-builder-0.1 (crate (name "ico-builder") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (features (quote ("ico"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0f03p93r60akwjy9w47x458lvm0sxw7p2lndcwq1c5xp7zxw2kak") (features (quote (("tiff" "image/tiff") ("jpeg" "image/jpeg") ("gif" "image/gif"))))))

