(define-module (crates-io ic ic) #:use-module (crates-io))

(define-public crate-icicle-core-1 (crate (name "icicle-core") (vers "1.3.0") (deps (list (crate-dep (name "ark-ec") (req "^0.4.0") (features (quote ("parallel"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ark-ff") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ark-poly") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ark-std") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "icicle-cuda-runtime") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0v7x6gkfg7a7imvg9mk3kgd4vmqbb1phpppwkpysamnl76l2ihis") (features (quote (("g2") ("ec_ntt") ("default") ("arkworks" "ark-ff" "ark-ec" "ark-poly" "ark-std"))))))

(define-public crate-icicle-cuda-runtime-1 (crate (name "icicle-cuda-runtime") (vers "1.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1i5gh21zvhj15w3p47mpvmsng7rvfz37hh82i0nbwcsvqlihgljb") (rust-version "1.70.0")))

