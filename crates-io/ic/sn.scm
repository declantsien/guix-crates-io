(define-module (crates-io ic sn) #:use-module (crates-io))

(define-public crate-icsneo-0.1 (crate (name "icsneo") (vers "0.1.5") (deps (list (crate-dep (name "libicsneo-sys") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.17.3") (features (quote ("extension-module" "abi3" "abi3-py37" "anyhow" "chrono"))) (optional #t) (default-features #t) (kind 0)))) (hash "10r083fdnsp9fxrp4aqgds9v6cxzrvz793r9qy5p7a60l3h7mbvs") (features (quote (("test_zero_devices") ("python" "pyo3") ("default" "test_zero_devices")))) (yanked #t)))

(define-public crate-icsneo-0.1 (crate (name "icsneo") (vers "0.1.6") (deps (list (crate-dep (name "libicsneo-sys") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.17.3") (features (quote ("extension-module" "abi3" "abi3-py37" "anyhow" "chrono"))) (optional #t) (default-features #t) (kind 0)))) (hash "0k70m6fzxx5lm4g7jg7sisfv0h5jv48hs1hsrjh4184r4kbqrhnq") (features (quote (("test_zero_devices") ("python" "pyo3") ("default" "test_zero_devices")))) (yanked #t)))

(define-public crate-icsneo-0.1 (crate (name "icsneo") (vers "0.1.7") (deps (list (crate-dep (name "libicsneo-sys") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.17.3") (features (quote ("extension-module" "abi3" "abi3-py37" "anyhow" "chrono"))) (optional #t) (default-features #t) (kind 0)))) (hash "081m8g63nx6fpyvm13kh3h8v61vqnzrkf77cwydkb9div62j60ws") (features (quote (("python" "pyo3") ("default")))) (yanked #t)))

(define-public crate-icsneo-0.2 (crate (name "icsneo") (vers "0.2.0") (deps (list (crate-dep (name "libicsneo-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "18gm1amznn51n8cxfhvy15ykrlzmifs40k1xmvjjac2m71x5rdg3") (yanked #t)))

