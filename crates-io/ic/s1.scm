(define-module (crates-io ic s1) #:use-module (crates-io))

(define-public crate-ics12-proto-0.1 (crate (name "ics12-proto") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.2") (kind 0)) (crate-dep (name "ibc-proto") (req "^0.35.0") (kind 0)) (crate-dep (name "prost") (req "^0.11") (kind 0)))) (hash "03zw6jg3281blwym7kg5gdy051jlszhvxrxa9k6m29wr9qd9cgv7") (features (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

(define-public crate-ics12-proto-0.1 (crate (name "ics12-proto") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.2") (kind 0)) (crate-dep (name "ibc-proto") (req "^0.35.0") (kind 0)) (crate-dep (name "prost") (req "^0.11") (kind 0)))) (hash "0f30bzm6hjybvd4fbp7rpw0dbjfggj99d4ig19x4fhhd39yd46by") (features (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

(define-public crate-ics12-proto-0.1 (crate (name "ics12-proto") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1.5.0") (kind 0)) (crate-dep (name "ibc-proto") (req "^0.38.0") (kind 0)) (crate-dep (name "prost") (req "^0.12") (kind 0)))) (hash "0iiqwsca3ark4bhvkqywqxw3xs6vk4dkxdjpmbx9yg6pnq2p8bxc") (features (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

(define-public crate-ics12-proto-rs-0.1 (crate (name "ics12-proto-rs") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.2") (kind 0)) (crate-dep (name "ibc-proto") (req "^0.35.0") (kind 0)) (crate-dep (name "prost") (req "^0.11") (kind 0)))) (hash "17wb84mdfqcjid8kxn14s8g14nfh3hvgxvsshi68nxx5kjdf1axp") (features (quote (("std" "prost/std" "bytes/std" "ibc-proto/std") ("default" "std"))))))

