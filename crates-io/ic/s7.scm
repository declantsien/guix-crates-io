(define-module (crates-io ic s7) #:use-module (crates-io))

(define-public crate-ics721-types-0.1 (crate (name "ics721-types") (vers "0.1.0") (deps (list (crate-dep (name "cosmwasm-schema") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^1.5") (features (quote ("cosmwasm_1_2"))) (default-features #t) (kind 0)) (crate-dep (name "cw-storage-plus") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "cw721") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "01g11ms7043b5qy1y8q6vvjx81lr6v9f0g5v8apbs3gdbdf2a87l")))

