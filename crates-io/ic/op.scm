(define-module (crates-io ic op) #:use-module (crates-io))

(define-public crate-icopng-1 (crate (name "icopng") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.8") (default-features #t) (kind 0)))) (hash "1iqh306zx1n5vm1l04b1h1w904dbg9rdjwjc0b7hc3lh18v7w4jn")))

