(define-module (crates-io ic -m) #:use-module (crates-io))

(define-public crate-ic-metrics-encoder-1 (crate (name "ic-metrics-encoder") (vers "1.0.0") (hash "18lxij2vqbibb9499hh9dpd5fbh48cx0fps198jbia7bapa01vwa")))

(define-public crate-ic-metrics-encoder-1 (crate (name "ic-metrics-encoder") (vers "1.1.0") (hash "0p9v5nmm2ylf4kzdck8ljggqrrn8qjpfmphr4gb693c2f7jj3cvw")))

(define-public crate-ic-metrics-encoder-1 (crate (name "ic-metrics-encoder") (vers "1.1.1") (hash "1y7ig5zq2bfq68h012i59nmfb2s6fj00y4v1lk6swmy3x8l7cp4b")))

(define-public crate-ic-mu-0.1 (crate (name "ic-mu") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.13.1") (default-features #t) (kind 0)))) (hash "18lf5d59qmlyic5a0zg422a6h8877ylnrvw2m30vqscxqnfb6fi4")))

