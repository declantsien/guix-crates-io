(define-module (crates-io ic _h) #:use-module (crates-io))

(define-public crate-ic_helper-0.1 (crate (name "ic_helper") (vers "0.1.0") (deps (list (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19") (default-features #t) (kind 0)))) (hash "0gyz48xzfx68dsngfsddk8b50v24g98fi3lvxwzg5r7zlav0w0zx")))

