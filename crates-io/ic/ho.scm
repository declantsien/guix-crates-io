(define-module (crates-io ic ho) #:use-module (crates-io))

(define-public crate-ichor-0.1 (crate (name "ichor") (vers "0.1.0") (deps (list (crate-dep (name "ctor") (req "^0.1.21") (default-features #t) (kind 2)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.6") (features (quote ("default-tls" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0zwzxq5k1w22ndrdjbvvk5fhp0qgw995n66i64ah1zrg6168wa63")))

