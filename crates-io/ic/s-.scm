(define-module (crates-io ic s-) #:use-module (crates-io))

(define-public crate-ics-chrono-tz-0.1 (crate (name "ics-chrono-tz") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "chrono-tz") (req "=0.6.1") (default-features #t) (kind 0)) (crate-dep (name "ics") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3") (default-features #t) (kind 1)))) (hash "1yxdz6zqiq2hbclxzkbjb7yzbj9rk6na1d60vmpl07w0pkrxxhrc")))

(define-public crate-ics-chrono-tz-0.2 (crate (name "ics-chrono-tz") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "chrono-tz") (req "=0.8.6") (default-features #t) (kind 0)) (crate-dep (name "ics") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3") (default-features #t) (kind 1)))) (hash "0k1vh9vzj2g57sbz03sl7hq974mhz2j3m0mp4j6904mn8mfjhdlc")))

