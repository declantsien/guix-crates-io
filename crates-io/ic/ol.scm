(define-module (crates-io ic ol) #:use-module (crates-io))

(define-public crate-iColor-0.1 (crate (name "iColor") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "05ihvcaf1id0qnhqv67xdhwjcw0mynpzqrlny0yd19c16bzdhb3c")))

(define-public crate-iColor-0.1 (crate (name "iColor") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1j3kgrm4safziwlhg6hh3g5wkvilqj519ksvp90kn28by58xadra")))

(define-public crate-iColor-0.1 (crate (name "iColor") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0i1r1h0mlmwqyhbn1ahwmagk07dl56nlykks1ql3sy56i2qkr9ns")))

(define-public crate-iColor-0.1 (crate (name "iColor") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0ahfm2pzxky6f7shiywhhznr0375fssa71ji72gw0bw3cnprl2yw")))

(define-public crate-iColor-0.1 (crate (name "iColor") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "16x926yasss7k7b10zqk3lrqwp5jydv16w9j1kybyy1vrkq65z25")))

