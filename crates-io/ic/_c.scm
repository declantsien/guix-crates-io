(define-module (crates-io ic _c) #:use-module (crates-io))

(define-public crate-ic_candy-0.1 (crate (name "ic_candy") (vers "0.1.0") (deps (list (crate-dep (name "candid") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.160") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)))) (hash "1q9nnfh3b2fspxyzm7zc2pq547yi5z2brr9lxyqfmfzh9znm8n9k")))

(define-public crate-ic_canister_backup-0.0.1 (crate (name "ic_canister_backup") (vers "0.0.1") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "11lp23jvxmqq02711ccqfkp7xirb2yhxnv6mg2pzfhwqxkh6q1zx")))

(define-public crate-ic_canister_backup-0.0.2 (crate (name "ic_canister_backup") (vers "0.0.2") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "08kvjj1nxb1qsrvnipgiyyg45rdj1snrj7slqhi6vl5l5f0r6286")))

(define-public crate-ic_canister_backup-0.0.3 (crate (name "ic_canister_backup") (vers "0.0.3") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0fqa8pp9wmq3ji2j4z7a9shdv697ragddz9f72nhwmvia932r683")))

(define-public crate-ic_canister_backup-0.0.4 (crate (name "ic_canister_backup") (vers "0.0.4") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1grdsdpcinywwx015mf8vli7c5v3zxp2ksn6qj4g9qipmxqgnzp5")))

(define-public crate-ic_canister_backup-0.0.5 (crate (name "ic_canister_backup") (vers "0.0.5") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "15452ps38kac87dm9v0qqaik2yiyjlcr5abl0idmvrq1gql7zyan")))

(define-public crate-ic_canister_backup-0.0.6 (crate (name "ic_canister_backup") (vers "0.0.6") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "11p5wxchqp2ld473d08p26gs6j4bl3rdxj7cb57hacyp7gbkpk8k")))

(define-public crate-ic_canister_backup-0.0.7 (crate (name "ic_canister_backup") (vers "0.0.7") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "074ma8g6gp9irnhdx9c51y7mqn0vwxwg0k3pahkkyyqnn27ykr8x")))

(define-public crate-ic_canister_backup-0.0.8 (crate (name "ic_canister_backup") (vers "0.0.8") (deps (list (crate-dep (name "ic-cdk") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1521p5s01bpxzrly5j2s9r4cd8azj9149xsqwn3br42aknhasc2c")))

(define-public crate-ic_canister_backup-0.0.9 (crate (name "ic_canister_backup") (vers "0.0.9-beta.0") (deps (list (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "06fidifx5qrlbr48ar5d2jrpsnqm5i741a418s4gfggiwplpc9gw")))

(define-public crate-ic_canister_backup-0.0.10 (crate (name "ic_canister_backup") (vers "0.0.10") (deps (list (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "14jfaqcxp5jsr6bl9kvri7wgi8hal8z13mgkzhcq7c7p9042lbik")))

(define-public crate-ic_catalyze_notifications-0.0.1 (crate (name "ic_catalyze_notifications") (vers "0.0.1-beta.0") (deps (list (crate-dep (name "candid") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p58y0ar868rixsvh27rl90ilqs1fk89py2xh3zckm549nh0kirm")))

(define-public crate-ic_catalyze_notifications-0.0.1 (crate (name "ic_catalyze_notifications") (vers "0.0.1-beta.1") (deps (list (crate-dep (name "candid") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g1r3rakrybhx6pldfybff71sci4bsiij0j9jag1fm2dc4mwshj8")))

(define-public crate-ic_catalyze_notifications-0.0.1 (crate (name "ic_catalyze_notifications") (vers "0.0.1-beta.2") (deps (list (crate-dep (name "candid") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "13aybf6vkmzxmhl5fg0gqcwhazgmk4zsc9my16majah2mhczf4ff")))

(define-public crate-ic_catalyze_notifications-0.0.1 (crate (name "ic_catalyze_notifications") (vers "0.0.1-beta.3") (deps (list (crate-dep (name "candid") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "18jx0f1cm4nb627i1a0w7fxd5pnvvncpkzhlj1kgsy1gq331zzzf")))

(define-public crate-ic_catalyze_notifications-0.0.1 (crate (name "ic_catalyze_notifications") (vers "0.0.1-beta.4") (deps (list (crate-dep (name "candid") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "04xw683a68qzaa29gv62c9jpqn0xmhnp6gp8whk8wq3s9vxg0yms")))

(define-public crate-ic_catalyze_notifications-0.0.1 (crate (name "ic_catalyze_notifications") (vers "0.0.1-beta.5") (deps (list (crate-dep (name "candid") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "199mb2hl1k863fc8hc1mpgw49hxdhaqv9h8g419869pr2ninvpb1")))

(define-public crate-ic_catalyze_notifications-0.0.1 (crate (name "ic_catalyze_notifications") (vers "0.0.1-beta.6") (deps (list (crate-dep (name "candid") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "ic-cdk") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0allpma0ibj03vbkrpwii33bri24c2g90dgjwn1gz33isxlz996w")))

