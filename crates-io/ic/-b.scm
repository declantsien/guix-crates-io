(define-module (crates-io ic -b) #:use-module (crates-io))

(define-public crate-ic-btc-interface-0.1 (crate (name "ic-btc-interface") (vers "0.1.0") (deps (list (crate-dep (name "candid") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "ciborium") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 0)))) (hash "1dh2bdn3rgiz874cn45pyrfj35b753m6qzihmcwjnzy8r7myzx78")))

(define-public crate-ic-btc-interface-0.2 (crate (name "ic-btc-interface") (vers "0.2.0") (deps (list (crate-dep (name "candid") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "ciborium") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 0)))) (hash "1b43pkzppdcsmwhz42sibr861rcsk3hac7xjmav05kazvgpd3hj6")))

(define-public crate-ic-btc-test-utils-0.1 (crate (name "ic-btc-test-utils") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin") (req "^0.28.1") (features (quote ("rand"))) (default-features #t) (kind 0)))) (hash "1p92qq1n2acrxz057mzpvphdc3ab646ldy2hj4ivm22i2i1hm3my")))

(define-public crate-ic-btc-validation-0.1 (crate (name "ic-btc-validation") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin") (req "^0.28.1") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)))) (hash "0q97360j2scyljxc41k6jwlmh2zgj91vs1j7nvfx6m5r03sbawry")))

