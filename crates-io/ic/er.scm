(define-module (crates-io ic er) #:use-module (crates-io))

(define-public crate-iceray-0.0.9 (crate (name "iceray") (vers "0.0.9") (deps (list (crate-dep (name "iceoryx-rs") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18") (features (quote ("termion"))) (kind 0)))) (hash "12qan4331wccgdw6zwprpzgvdpnshrz4yp84wjipyywcs50ni9b5")))

(define-public crate-iceray-0.1 (crate (name "iceray") (vers "0.1.0") (deps (list (crate-dep (name "iceoryx-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18") (features (quote ("termion"))) (kind 0)))) (hash "16vpsflm9vpnzlm7rx77q4fb48p5z8r2k3kp0szdqc15zg24242k")))

(define-public crate-icerpc-0.0.0 (crate (name "icerpc") (vers "0.0.0") (hash "0bpvdgiinr86clxfw58a45j48nml4682idylpdqv35s0ffd61ba4")))

