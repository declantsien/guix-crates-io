(define-module (crates-io ic ha) #:use-module (crates-io))

(define-public crate-ichain-0.1 (crate (name "ichain") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "1jjm11d988878v2prvm02z5d12jqf46q678miwp1whw06fy6pacb")))

