(define-module (crates-io ic p_) #:use-module (crates-io))

(define-public crate-icp_2d-0.1 (crate (name "icp_2d") (vers "0.1.0") (deps (list (crate-dep (name "lstsq") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)))) (hash "0xv6bisl6x5k7skjv7c84shlrwdr528ixjl85fz0z1qvg9pdz9hq")))

(define-public crate-icp_2d-0.1 (crate (name "icp_2d") (vers "0.1.1") (deps (list (crate-dep (name "lstsq") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)))) (hash "106jqgf79hnpc6lmm63c3z5661m8im9vbfybpw8icaviawadfvyv")))

(define-public crate-icp_2d-0.1 (crate (name "icp_2d") (vers "0.1.2") (deps (list (crate-dep (name "lstsq") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)))) (hash "0dn0jypr9ph2d1rwqa812c3h8ak91k30v0v7wfzl9cg1xyklg8rn")))

