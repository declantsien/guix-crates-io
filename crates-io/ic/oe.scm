(define-module (crates-io ic oe) #:use-module (crates-io))

(define-public crate-icoextract-0.1 (crate (name "icoextract") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "utf16string") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0811vv4i31yvvx0rsmzp3xbcknmwiqlmb4ya1vgh1bl6qk0y4mk9") (yanked #t)))

