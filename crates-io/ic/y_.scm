(define-module (crates-io ic y_) #:use-module (crates-io))

(define-public crate-icy_sauce-0.1 (crate (name "icy_sauce") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1zkzcm24lixbrs5f49s37g41wij1nxwrbk39fmhbsnpikbvycvyv")))

(define-public crate-icy_sauce-0.1 (crate (name "icy_sauce") (vers "0.1.1") (deps (list (crate-dep (name "bstr") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "01h21f28b0avas4bnprjzslrav0d8yfzs8wqinjv7rx3d4jnr49c")))

(define-public crate-icy_sixel-0.1 (crate (name "icy_sixel") (vers "0.1.0") (hash "15rkd33f1nn1f6q93hsxbs5vlmclzcxfvgn5ypgnlf7s6w5sbv7a")))

(define-public crate-icy_sixel-0.1 (crate (name "icy_sixel") (vers "0.1.1") (hash "0pilrnhbs175f16y471naia7l8dxn96wgim461vj9z632q1d7i0x")))

(define-public crate-icy_sixel-0.1 (crate (name "icy_sixel") (vers "0.1.2") (hash "0nqmq5c3hawvpqdl6rskyc93qjf36pa0mjzgzmn5jk9803l8m1c6")))

