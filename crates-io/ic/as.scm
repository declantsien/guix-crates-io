(define-module (crates-io ic as) #:use-module (crates-io))

(define-public crate-icasadi-0.1 (crate (name "icasadi") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (kind 0)))) (hash "0lwxjrsgcfqwfvvz7h030d2i8j850plp3sb5aav3gkp7llv9zx10")))

(define-public crate-icasadi-0.1 (crate (name "icasadi") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (kind 0)))) (hash "167anj4bs7fs8axf7zpsw1q1s2dyyg8ixfisgfq85k1dl5kgnqxp")))

(define-public crate-icasadi_test-0.0.1 (crate (name "icasadi_test") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (kind 0)))) (hash "12ljcxciw5zidwzv1vjy33iqll9z9dz8p3pg7sr2vwf0id60pjnl")))

(define-public crate-icasadi_test-0.0.2 (crate (name "icasadi_test") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (kind 0)))) (hash "0dlcw62hrjwqcpm2yknbxkfrqpydk5vs6yskz0y3pmidr5rvbn85")))

