(define-module (crates-io ic ew) #:use-module (crates-io))

(define-public crate-icewal-0.1 (crate (name "icewal") (vers "0.1.0") (hash "0117jb0xnf6740yiwyiy9nfyrbkv5jpzw5fk9wfsrkwa70lh3n7g")))

(define-public crate-icewrap-0.1 (crate (name "icewrap") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "033ibscgs719h5khl57zg8biq4rcvl46nkzfillx2cahyb7w747r") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-icewrap-0.1 (crate (name "icewrap") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "03xm1x47nz29yacjwj7mhqaf5v40isdg9gaxbjvfr3r1x78v7qp6") (features (quote (("std") ("default" "std"))))))

