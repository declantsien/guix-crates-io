(define-module (crates-io ic ar) #:use-module (crates-io))

(define-public crate-icarus-0.0.1 (crate (name "icarus") (vers "0.0.1") (hash "1hgfz9f9iimkssk4lgqa2qvsmf5cn6901c3203q5fh3bdn53kd6c")))

(define-public crate-Icarus-nrf9160-bsp-0.0.0 (crate (name "Icarus-nrf9160-bsp") (vers "0.0.0") (deps (list (crate-dep (name "nrf9160-hal") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "0sbw766rjsih29abags8wg1kjvfvfy3wy55xvcm2qn1a49n6q373")))

