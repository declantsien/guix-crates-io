(define-module (crates-io ic ub) #:use-module (crates-io))

(define-public crate-icub3d_combinatorics-0.1 (crate (name "icub3d_combinatorics") (vers "0.1.0") (hash "1gs0drfj8bcbfm4a36mhkzvigqanmj7rishhlhmbs45yk5gdvpq9")))

(define-public crate-icub3d_combinatorics-0.1 (crate (name "icub3d_combinatorics") (vers "0.1.1") (hash "1lg65rgi2ha7cfwi1g3h197yga478r9bcy1yd0jf150dg87j3lmx")))

(define-public crate-icub3d_sudoku_solver-0.1 (crate (name "icub3d_sudoku_solver") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1hhzmf8jjhs2h2zi9fk8ic4cl702hxkf8fk4l8gba8nhscc01z8b")))

(define-public crate-icub3d_sudoku_solver-0.1 (crate (name "icub3d_sudoku_solver") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0iwwwrfl6rz0csr0imzkzgcncanyld0n3n8f0h8d0727ay52i6gx")))

(define-public crate-icub3d_sudoku_solver-0.1 (crate (name "icub3d_sudoku_solver") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "16lscgnb576488c6cn4k259f7fh8c1wnychs1vdkqizvnnsmh9fa")))

(define-public crate-icube-sdk-sys-0.1 (crate (name "icube-sdk-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 1)))) (hash "16chdl1v9rpzjxszlq167dnb6smp3pb9h730sj8ma9ivnj1i66y0")))

