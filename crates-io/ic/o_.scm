(define-module (crates-io ic o_) #:use-module (crates-io))

(define-public crate-ico_math-0.1 (crate (name "ico_math") (vers "0.1.0") (hash "0db1s5p7c9yzr87c3glbzhk0fbkwkqq9ww10q9iih65p5cwmpg4h")))

(define-public crate-ico_math-0.1 (crate (name "ico_math") (vers "0.1.1") (hash "07fcf9addkf0iz1ci4bp4jhidxwqfs6pbib0r1k1py1zl5khxwaj")))

(define-public crate-ico_math-0.1 (crate (name "ico_math") (vers "0.1.2") (hash "1dqriz8mnvlqhdrbwahcccwc9mm0ckwrc4v0r0w8400p4595b2w8") (features (quote (("use-std"))))))

(define-public crate-ico_math-0.1 (crate (name "ico_math") (vers "0.1.3") (hash "08m977vdyg7lcvdvxr8437x33rhmkaj85ypqvk0jcbgjvd3fr22z") (features (quote (("use-std"))))))

(define-public crate-ico_math-0.1 (crate (name "ico_math") (vers "0.1.4") (hash "10nyzajqnlw296255bs1sw3iiik9vg4xhgmvn25xfwh6kx6f7lk4") (features (quote (("use-std") ("default" "use-std"))))))

(define-public crate-ico_math-0.1 (crate (name "ico_math") (vers "0.1.5") (hash "083a9z8667ky1pw7nv12wd3aqcg65qy4rbg95yjmwpps1ygd3v79") (features (quote (("use-std") ("default" "use-std"))))))

(define-public crate-ico_math-0.1 (crate (name "ico_math") (vers "0.1.6") (hash "07gvxzlyr4g5gfsir911by1n6npb33x4cbj5k4ilqfb8ji4c57xq") (features (quote (("use-std") ("default" "use-std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "03mm4kvgvc415kiq8ps24pc4g3jmxyy940g6vbx9d5bajyq04702")))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "16hvgg98fpw1rmn3vn50sl1dhzcaixnvp8g0hybrgdp3rd75pz2g")))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "066jr8rs5q2mwc3gx334f0qmvvl2p8znasj8kjqlbwgz3hmvmbp5") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0abi3wbxp8jil1zzv9l0zis45aag1gxfh1a0ds7v6g51pk6q3y41") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0cll4hj3f56x6b50410w73794f77h3d3dg692vmmns447qkay7j8") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0hz6k4kdmh167gaawb24mb70l1mah3yvnv7drm4w8h6rbb2c82kk") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "08laz690gcgi0dk8b2032fgyih4pnz581qnr96cjjgl6fg28rlsp") (features (quote (("std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "185sy3fcy8559alfr92zmc6skfkb0mcih3di5rr2xi061iwmjlv1") (features (quote (("std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "02rslkr6ffm475lnrdbf3i5zykqnqdxh0ijgqj6yz8674hr8aar8") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "1xqkfh8c175yy0mqvnjjvn4dcib4crg22853q5m754s9bydnsb0q") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.10") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0ajdhriy0dy1p53srzpvhhkw328zbw5906ldzf6rg9s2rsz192i9") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.12") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0xbb4gw86rlsxh81gngwz08rvxyrx1ywcdxdm5sk8amx5agcjajc") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0gq05rf8mfbjrah8zbm8xxgmpbmbvfckdzs15mv8326r7ipv5sy9") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0bsk8f51v82329yb67fkjq7d9bgb8hpwzh5v0r0hp0s40ykfppic") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.15") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0yg0ikbpyiyksj2a2jv6zkp1pdzd9f13cvccn50zkvgfqfq5r1l6") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.16") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "1am0awrjwgmljlm5y5pldiy53i2c9p8ikx81wxb1s9hg4gzraw6k") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.17") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0l7iysvib5lmgbd6583j5zdj0hh0smp3ymkh6z0gl9qk3jrjkwh7") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.18") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0zaj2aj8l7pl6v1l6s5l739ip4gr715ixfirifz1sxpbwy1anvm0") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.19") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0mmg8qk3aihcy25k32vbwk1h6dkkrb1g7cpnwbx74q639bgh4yw8") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.20") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "1afizgfgxi5wx74ar1z0hzqlgqidinz6a1frj3w3vvz7bix3njh5") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.21") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "1brdybin9b4b9n22zmm7l1zb8wps27xyz0gdkj4r8079fij0z4kh") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.22") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "075952g7244fb18qrbvmf2pka6sz7qzrfjx0s3rzfk9gj8znx5d6") (features (quote (("std") ("default" "std"))))))

(define-public crate-ico_memory-0.1 (crate (name "ico_memory") (vers "0.1.23") (deps (list (crate-dep (name "libc") (req "^0.2.61") (kind 0)))) (hash "0pxbqh5qj68z5sknvl6bja53smyhcnm3dj8zlmli7ncdba17vdq5") (features (quote (("std") ("default" "std"))))))

