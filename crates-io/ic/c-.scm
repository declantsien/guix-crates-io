(define-module (crates-io ic c-) #:use-module (crates-io))

(define-public crate-icc-profile-0.0.2 (crate (name "icc-profile") (vers "0.0.2") (deps (list (crate-dep (name "bin-rs") (req "^0.0.7") (features (quote ("util"))) (default-features #t) (kind 0)))) (hash "1980w321say6b04mswxfazx8qpw7dpfj62i36v0bvahic88jkvpk")))

