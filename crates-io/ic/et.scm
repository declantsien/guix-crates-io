(define-module (crates-io ic et) #:use-module (crates-io))

(define-public crate-icetea-0.1 (crate (name "icetea") (vers "0.1.0") (hash "1wr6rix2xy1w8gw6ndihgh1v1232g7mrgc7qf7nf4yk52bhcmqpb")))

(define-public crate-icetea-0.1 (crate (name "icetea") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j2aa89q3qdx3gbgm7rg7va79p52da0p9iq6f2ib28dca0xhhwvn")))

(define-public crate-icetea-0.1 (crate (name "icetea") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v2q06pmnk4q71a4pcs1cr94855s7249kbji5b7s6mb68jrhihq1")))

