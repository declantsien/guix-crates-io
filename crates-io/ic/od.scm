(define-module (crates-io ic od) #:use-module (crates-io))

(define-public crate-icodec-0.1 (crate (name "icodec") (vers "0.1.0-alpha") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ikon") (req "^0.1.0-beta.11") (default-features #t) (kind 0)))) (hash "04h5fl83n45gd6a3h8xayw0n73hypx9h0s0ry04rdxy6q1hh0kgm") (yanked #t)))

(define-public crate-icodec-0.1 (crate (name "icodec") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ikon") (req "^0.1.0-beta.11") (default-features #t) (kind 0)))) (hash "0n6pznfmj2dmpjnsx6mdsjzcjp3i4zgrm1n4apxw4661vi3xy2x0") (yanked #t)))

(define-public crate-icodec-0.1 (crate (name "icodec") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ikon") (req "^0.1.0-beta.12") (default-features #t) (kind 0)))) (hash "1ri6iz7jy2xf7bc48rvqblsgf8birkgaqm7841yr91yvjz3nl30y") (yanked #t)))

(define-public crate-icodec-0.1 (crate (name "icodec") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ikon") (req "^0.1.0-beta.16") (default-features #t) (kind 0)))) (hash "03z21v6bdxsfc9x329nhziqnqghmmvrwix3jh0rjcw5pc5w3bbzm") (yanked #t)))

