(define-module (crates-io ic -x) #:use-module (crates-io))

(define-public crate-ic-xrc-types-0.8 (crate (name "ic-xrc-types") (vers "0.8.0") (deps (list (crate-dep (name "candid") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)))) (hash "0z7akg8xv65ka0jlg1f1mzbd2vv0miv10s7finrvjjydh36367lb") (yanked #t)))

(define-public crate-ic-xrc-types-1 (crate (name "ic-xrc-types") (vers "1.0.0") (deps (list (crate-dep (name "candid") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)))) (hash "1x49mr309nzpnzickiqh0qxvzmgh4vcji6dil8mn74mqkkw1g94h")))

(define-public crate-ic-xrc-types-1 (crate (name "ic-xrc-types") (vers "1.1.0") (deps (list (crate-dep (name "candid") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)))) (hash "1smnb6axwgbfzs1vpd9wkygavk28cjds1n28a6ijvfnxrihniaq5")))

(define-public crate-ic-xrc-types-1 (crate (name "ic-xrc-types") (vers "1.2.0") (deps (list (crate-dep (name "candid") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)))) (hash "18mmb3izhay6k947yyh7bnjnwnvsgwq2l6fipd51y81jlgmb58mi")))

