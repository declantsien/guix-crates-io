(define-module (crates-io ic kl) #:use-module (crates-io))

(define-public crate-ickle-0.1 (crate (name "ickle") (vers "0.1.0") (hash "1qwc3dg0fgpp4j2nz6lppid0w2zqha04rb5vwlpl9fl8ll0s7qay")))

(define-public crate-ickle-0.1 (crate (name "ickle") (vers "0.1.1") (hash "1whibl5afzj2l3p9wl5cqi8lina6xk4gzjfxr4vf9234hfm5dmch")))

(define-public crate-ickle-0.2 (crate (name "ickle") (vers "0.2.0") (hash "0p02qqj070rj8qr3xhlbc86g4i46d6a42jjcz5kifpnwxvafbfyz")))

(define-public crate-ickle-0.3 (crate (name "ickle") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0flyzi21mzlckfkl544lsgmkhkxyg7c2akxah4n8czz913hjz8x8")))

