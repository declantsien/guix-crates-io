(define-module (crates-io ic ap) #:use-module (crates-io))

(define-public crate-icaparse-0.1 (crate (name "icaparse") (vers "0.1.0") (hash "041wgzwgzix4sm16kzv8p8w2wd6wdllwws88r9c2awgvv4l8adhq") (features (quote (("std") ("default" "std"))))))

(define-public crate-icaparse-0.1 (crate (name "icaparse") (vers "0.1.1") (deps (list (crate-dep (name "httparse") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "00gzjrgw08h1yvznd5yzcjk97fcjpdhm4z9y2lvp0xicmyx1zs51") (features (quote (("std") ("default" "std"))))))

(define-public crate-icaparse-0.2 (crate (name "icaparse") (vers "0.2.0") (deps (list (crate-dep (name "httparse") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "1cyvcllgpih7blqxhblfifjb9ghcvjv9qp7jc8a3w0xdl6c299x9") (features (quote (("std") ("default" "std"))))))

