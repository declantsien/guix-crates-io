(define-module (crates-io ic ef) #:use-module (crates-io))

(define-public crate-icefall-0.1 (crate (name "icefall") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jf32sv9ixgwgfrlcfyd96z4j9rcjcpd84h2ln2w4zkwfs6mq2q5")))

(define-public crate-icefalldb-0.1 (crate (name "icefalldb") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xpnp6c0300sv0g1v7lycrb7b1hid4dvljkw5n3ndfqw2fgxxkjr")))

