(define-module (crates-io ar gw) #:use-module (crates-io))

(define-public crate-argwerk-0.1 (crate (name "argwerk") (vers "0.1.0") (hash "0szp95d023z6s805fczpl4czkg1dc5q0x2dqdbchyms2vd7dhmx0")))

(define-public crate-argwerk-0.2 (crate (name "argwerk") (vers "0.2.0") (hash "0ljzalp3wyk0f04w4w0fnw51bln3xpnl36c7p972mj3slkklbxcv")))

(define-public crate-argwerk-0.3 (crate (name "argwerk") (vers "0.3.0") (hash "0h7sdw3c302x4dci8f7qfp3ds4i36xmy1jqql0k0mh12r0gim6rl")))

(define-public crate-argwerk-0.4 (crate (name "argwerk") (vers "0.4.0") (hash "19ax37mnvdaymcx50nplwmknq0gv6s49bp4xcaqli8kmks3b8xb4")))

(define-public crate-argwerk-0.5 (crate (name "argwerk") (vers "0.5.0") (hash "0w69yl3ab2rd744msbjhdm42c73v6vi59c4ibis5vaka4i5f96l0")))

(define-public crate-argwerk-0.5 (crate (name "argwerk") (vers "0.5.1") (hash "05w3mbwhm74xz4ficv5f0z8a6g0hn2ckxvhxgprng2yb6j6ndlrm")))

(define-public crate-argwerk-0.6 (crate (name "argwerk") (vers "0.6.0") (hash "04wsmih18gky4xlvh6c33vpvx94d8rnsqcr11xphl1454lc0aqjr")))

(define-public crate-argwerk-0.7 (crate (name "argwerk") (vers "0.7.0") (hash "0crpkza3c424ajqg97d11p056x18h1w8lbiz1r7kmqdb8pcsdm7l")))

(define-public crate-argwerk-0.8 (crate (name "argwerk") (vers "0.8.0") (hash "0lmgcdllrs9p75xqv41ybldr7a5wbm3nxnkwj9wdabdjpc63nc5k")))

(define-public crate-argwerk-0.8 (crate (name "argwerk") (vers "0.8.1") (hash "0lwa393r8nld6hb64wzw7784flkfpn5qyn37k8wl58xg7kgrnviz")))

(define-public crate-argwerk-0.9 (crate (name "argwerk") (vers "0.9.0") (hash "0ih3nhqccyfbvd237crpsnacj034k2lrs268fjg19hi7rqgshpy1")))

(define-public crate-argwerk-0.10 (crate (name "argwerk") (vers "0.10.0") (hash "0yl3lqx3jch5d70ip4apisz10l3lmlq3jr11kq2m5lhzl040lxqn")))

(define-public crate-argwerk-0.11 (crate (name "argwerk") (vers "0.11.0") (hash "1xqwyp8sl5jkkgm15clg72h182qb7v54xdwxn1h90wb16gbhpgla")))

(define-public crate-argwerk-0.12 (crate (name "argwerk") (vers "0.12.0") (hash "00a9jw18pap2klk196a2m7sl7knys1j4krnakzlyyqh09pnls262")))

(define-public crate-argwerk-0.13 (crate (name "argwerk") (vers "0.13.0") (hash "1r419gxpysv7n8cy3q1mlzf6q6qmkxkw84k02awp1y11xxan98jk")))

(define-public crate-argwerk-0.13 (crate (name "argwerk") (vers "0.13.1") (hash "16p6vi3i9nl8hd7hgpcpb9116prnpasns09ddfbkz295v6599piz")))

(define-public crate-argwerk-0.14 (crate (name "argwerk") (vers "0.14.0") (hash "1ich4cghham7mzprwfzsyr994v9rz3im65b6vibvjh6z88hfk5dk")))

(define-public crate-argwerk-0.15 (crate (name "argwerk") (vers "0.15.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1jl8sjznxri383gy5pldlns7bpajpk2z95bny6hz8n4vf8gi3vvx")))

(define-public crate-argwerk-0.16 (crate (name "argwerk") (vers "0.16.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1nnxrdq95aj3rzfl5mxfk8pyfqhbkrmffd98v6lk6krkbwm0fy2b")))

(define-public crate-argwerk-0.16 (crate (name "argwerk") (vers "0.16.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1187pa7sfwf36ajr2rq2fcjjcnv7qkgw852zyygcvvvzh0n9whl6")))

(define-public crate-argwerk-0.17 (crate (name "argwerk") (vers "0.17.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1n72xrkwjljgm651qbpsndfiq603kay8yy4vjgsdhrijxwb328qi")))

(define-public crate-argwerk-0.18 (crate (name "argwerk") (vers "0.18.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1pr54b4izvvh4l1mpc5jg0dldz4mmmx99wd450mrnmfywjysd5jj")))

(define-public crate-argwerk-0.20 (crate (name "argwerk") (vers "0.20.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1abaayix7wx3najf8pirw3i7w1qbl11amh58lslrg6air8krhkj9")))

(define-public crate-argwerk-0.20 (crate (name "argwerk") (vers "0.20.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0xap7fqslqs07k77ssslpqz4sd197sp30prdzs95dqi29cazp2jw")))

(define-public crate-argwerk-0.20 (crate (name "argwerk") (vers "0.20.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0qz4m31r3r3s203v4iz6hlvxqxka6ig7bb9lhadb562bgkpqn0mc")))

(define-public crate-argwerk-0.20 (crate (name "argwerk") (vers "0.20.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1waaz353s9cy9lzx9z352nlbw7i3y3icv1lfjrl3gd03vzr20378") (rust-version "1.56")))

(define-public crate-argwerk-0.20 (crate (name "argwerk") (vers "0.20.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0v4rmscjyrh83lykfmn5gxhri3dg2zpjgnk1gqawzqxr18pkglf3") (rust-version "1.56")))

(define-public crate-argwerk-no-std-0.20 (crate (name "argwerk-no-std") (vers "0.20.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "core-error") (req "^0.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "064rd4w155mfhr9w277rh5rc1g8k8y1bji89cxn7mfi42030ca5b") (yanked #t)))

