(define-module (crates-io ar gr) #:use-module (crates-io))

(define-public crate-argrs-0.1 (crate (name "argrs") (vers "0.1.0") (hash "0fd9vkyc2d5vsi12hkvhidff2whfbz7igqhhri47zj48ik3y2w5g")))

(define-public crate-argrs-0.1 (crate (name "argrs") (vers "0.1.1") (hash "1z7lj7sh1pkg1yhbh46r0zc8lpabxanvbqdpds3qqxfy9h25s7ja")))

(define-public crate-argrs-0.1 (crate (name "argrs") (vers "0.1.2") (hash "1cbr7k5gw33frvnafxaflb5nk56asrzb9m0pza64qbvidkbnh3qw")))

(define-public crate-argrust-0.1 (crate (name "argrust") (vers "0.1.0") (deps (list (crate-dep (name "rustypath") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ba2x5rizvks9g8agm5hcmib4yfy9jp0apgswwlpjrf7h18shigy")))

