(define-module (crates-io ar qo) #:use-module (crates-io))

(define-public crate-arqoii-0.1 (crate (name "arqoii") (vers "0.1.0") (deps (list (crate-dep (name "arqoii-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)))) (hash "0mh84m9zwam5p3fw1br6az89ai3l3qi2n6sq3isfddkk9ndc8jl9")))

(define-public crate-arqoii-0.2 (crate (name "arqoii") (vers "0.2.0") (deps (list (crate-dep (name "arqoii-types") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 2)))) (hash "15m8yrjw5l3fv1ssm8pknqm8s6vkarp55pg6m6p3j9yx5c1jiiy0")))

(define-public crate-arqoii-0.3 (crate (name "arqoii") (vers "0.3.0") (deps (list (crate-dep (name "arqoii-types") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.12") (default-features #t) (kind 2)))) (hash "1401i00w1d6kpfy9v7drsbm3jibs3dk3mj354k23q3vpg164i3ax")))

(define-public crate-arqoii-cli-0.3 (crate (name "arqoii-cli") (vers "0.3.0") (deps (list (crate-dep (name "arqoii") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.12") (default-features #t) (kind 0)))) (hash "11h77cgp613lfyiadz72knmky2ygsb8y2ljxv8wb9n79xhfzy0rf")))

(define-public crate-arqoii-types-0.1 (crate (name "arqoii-types") (vers "0.1.0") (hash "038jhjbdy6a9jj6pb1s2bkw3b5qzmyza9xqyrb976jqzvz1i1b69")))

(define-public crate-arqoii-types-0.2 (crate (name "arqoii-types") (vers "0.2.0") (hash "1yn8vm758a4kkpgr6ahx9fl5iaiqy9c9ngh9fl44zr6i8mgs29n7")))

(define-public crate-arqoii-types-0.3 (crate (name "arqoii-types") (vers "0.3.0") (hash "0z626ica9cyxkmbqblh77rdavr2kahkgizkylq57a38q0zb3mmpa")))

