(define-module (crates-io ar ko) #:use-module (crates-io))

(define-public crate-arko-0.1 (crate (name "arko") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.115") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1j1g5j116c9l6crxfj2l64n7gsbqdhnllz8ww0hv9c1yr120wp3j")))

(define-public crate-arko-0.1 (crate (name "arko") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.115") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1rmam44hwdc31wsx543r9k1mhpb5aas82inzdwsjwprr0p3j3i19")))

(define-public crate-arko-0.2 (crate (name "arko") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.115") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0q8vizjdn2pd246r2zhcl1vb1f53fc3crfjbynp1vkfhrh9z0hx8")))

(define-public crate-arko-0.2 (crate (name "arko") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.115") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "16d3xsk6lif97yxaj2kvmirx7qj2ssgashdfm4vlqfbgq7ddqkls")))

(define-public crate-arko-0.2 (crate (name "arko") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0438r0qw1mvxydramsv5d6rf02vp3ylyrp9515fhwsf1y3x82ih7")))

(define-public crate-arko-0.2 (crate (name "arko") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xvya0qgpnj1672gigr3y6x7974121a1319j53ygj5g2d7nmb10v")))

