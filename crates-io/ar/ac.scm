(define-module (crates-io ar ac) #:use-module (crates-io))

(define-public crate-arachne-0.0.1 (crate (name "arachne") (vers "0.0.1") (hash "0rlgvi2dw43h4rsm8cfxf19ahsmqdy03ix8cmvjxf5r6334mmy5l")))

(define-public crate-arachnid-0.0.0 (crate (name "arachnid") (vers "0.0.0") (hash "0s476ri0wj151whv8hnilx5xfk9zldpganmcdj8w24avynlijgm1") (features (quote (("full") ("default"))))))

