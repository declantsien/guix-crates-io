(define-module (crates-io ar r_) #:use-module (crates-io))

(define-public crate-arr_macro-0.0.1 (crate (name "arr_macro") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qv4kp3hvx3syn0iwdxlbhlwwgg26pkvbgipr1ycx3hmwf4lcc4x")))

(define-public crate-arr_macro-0.1 (crate (name "arr_macro") (vers "0.1.0") (deps (list (crate-dep (name "arr_macro_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0m298c6g2s4756jm2adpg085c53af71ghfir90y61kgnb047q8wa")))

(define-public crate-arr_macro-0.1 (crate (name "arr_macro") (vers "0.1.1") (deps (list (crate-dep (name "arr_macro_impl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1x90cppfwaczsv8kgrkh9my7j4l83zdr3369506lg91pg6zwkf1c")))

(define-public crate-arr_macro-0.1 (crate (name "arr_macro") (vers "0.1.2") (deps (list (crate-dep (name "arr_macro_impl") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "15rsvpwdqgchh6ycwpvfv22xy0s38k6n9ryn99aj2cap5wzvhqnj")))

(define-public crate-arr_macro-0.1 (crate (name "arr_macro") (vers "0.1.3") (deps (list (crate-dep (name "arr_macro_impl") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "199086q8zva66lbg9bpz6fa67s81ra7yfa8148cwy1w7lkymn43a")))

(define-public crate-arr_macro-0.2 (crate (name "arr_macro") (vers "0.2.0") (deps (list (crate-dep (name "arr_macro_impl") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-nested") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zp2fad99xrbiz84pqk3k8n60wqvdai03i0yar7qfk9rhsqpsfny")))

(define-public crate-arr_macro-0.2 (crate (name "arr_macro") (vers "0.2.1") (deps (list (crate-dep (name "arr_macro_impl") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-nested") (req "^0.1") (default-features #t) (kind 0)))) (hash "14p9mx9z5562nhfgz2kyh8q5s6lw9p1rjbvsl6nfhapscbh3d4y4")))

(define-public crate-arr_macro_impl-0.1 (crate (name "arr_macro_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0r80k7djzky7x6xzn8l96il1apla8l1zxyssvl7bbv7dvf5v01pl")))

(define-public crate-arr_macro_impl-0.1 (crate (name "arr_macro_impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y0ff1bgxb18qlf16py11d5v8nbfhsy5dcbz6gfmdqww52zs9dgd")))

(define-public crate-arr_macro_impl-0.1 (crate (name "arr_macro_impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z5axgkv2r7ikp0882gdqqb1dswwhb8fb4cd4929x4zczybvxv4d")))

(define-public crate-arr_macro_impl-0.1 (crate (name "arr_macro_impl") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lbjilz3pvwav72dfkcbz99rsq7m04xbdpqh8g3yvx3jsn5wf286")))

(define-public crate-arr_macro_impl-0.2 (crate (name "arr_macro_impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wmrfpcgzwaw383j18a2gk260b67hv7fdm3g95mj27n2faiwnzzf")))

(define-public crate-arr_macro_impl-0.2 (crate (name "arr_macro_impl") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pkfgwbyd5ylj069bdjmaalb0dwl1jp2f0wir81w8vjwmvwnhqww")))

(define-public crate-arr_ty-0.1 (crate (name "arr_ty") (vers "0.1.0") (hash "1iq3q64lsaj4saiwiyfyf7z2dmilgrlqk6lsy2iiqmzq34r0fkwp")))

(define-public crate-arr_ty-0.2 (crate (name "arr_ty") (vers "0.2.0") (hash "1sn1zm57cm4iajrdbcs0pr2rllp59s8jnbhzzbvxqpah6qr5w44h")))

(define-public crate-arr_ty-0.2 (crate (name "arr_ty") (vers "0.2.1") (hash "0403qipzdbj3q5n0g5jgmg1yqswmn83z9h9arr2by63dwwf5pm5r")))

(define-public crate-arr_ty-0.2 (crate (name "arr_ty") (vers "0.2.2") (hash "0rd2dqphqb9ga1nirmfhahjp696zzmshanb10bnnc7ycqqnxzg2s")))

