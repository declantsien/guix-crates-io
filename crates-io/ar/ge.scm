(define-module (crates-io ar ge) #:use-module (crates-io))

(define-public crate-argent-0.1 (crate (name "argent") (vers "0.1.0") (hash "0sl2z7f82z4yig3wggb5xjpw85jx7dw1x28vv3yjjmjljhdpi4yi")))

(define-public crate-argentum-0.1 (crate (name "argentum") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14r5whlbhd70djqgsakflqwzqm259fpdjmc7c8clkwh86l1zi0ip")))

(define-public crate-argentum_encryption_business-0.1 (crate (name "argentum_encryption_business") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "02yjrscrdwmdsnk51i09ygpw73xhwz2shb0ibvckdq3m042yvdn3")))

(define-public crate-argentum_encryption_business-0.1 (crate (name "argentum_encryption_business") (vers "0.1.0-dev") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mm0vs59i5sbqnc4nkv1d0yjxk3x3x6xy748ny261pn87pix0a4l")))

