(define-module (crates-io ar ou) #:use-module (crates-io))

(define-public crate-around-0.1 (crate (name "around") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qx3iffj9dkcx7qkr7zwpahy3lgiivpa78zkls29gmxf88jgfcm0")))

(define-public crate-arouse-0.1 (crate (name "arouse") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.57") (default-features #t) (kind 0)))) (hash "04rw8fn5lzrawqvfd3278kms4h3wfym319jik3s6jnwr3gsnhs6r")))

