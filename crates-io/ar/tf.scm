(define-module (crates-io ar tf) #:use-module (crates-io))

(define-public crate-artful-0.1 (crate (name "artful") (vers "0.1.0") (hash "1cbsx6d707zcnimyfr3lxh0a6hg1ni6ybf3s0z9ra6xswnk7yv3h") (features (quote (("simd"))))))

(define-public crate-artful-0.1 (crate (name "artful") (vers "0.1.1") (hash "0378vaxcn6cgibw1rr6n2nqsixxdmi0lk6xx3x04fsj1678fvf7k") (features (quote (("simd"))))))

(define-public crate-artfuldolphinaddsone-0.1 (crate (name "artfuldolphinaddsone") (vers "0.1.0") (hash "1k6jdhfafal9wzxnidz9k4yalg8418vmx1gld25jxf2645a0gpkp")))

