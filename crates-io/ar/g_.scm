(define-module (crates-io ar g_) #:use-module (crates-io))

(define-public crate-arg_attr-0.1 (crate (name "arg_attr") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "0nh336rbizcmr22l31aiyp23k6v8qax19wdmgnwnh91n1s45gk7i")))

(define-public crate-arg_combinators-0.1 (crate (name "arg_combinators") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qvn3s4s7sydpwafxa7smzna9jfma0jspnarqfdm9hngsgd9gk8d")))

(define-public crate-arg_combinators-0.2 (crate (name "arg_combinators") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h7wmga5774y7dwly9yhda7s1a8j2cz488wypdynw0kcgnrsxjgf")))

(define-public crate-arg_combinators-0.3 (crate (name "arg_combinators") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yqzhpnsqpz3ijh3h9pk8k3b43h6pkvvpc63f5ykz1vqd4zi5apw")))

(define-public crate-arg_combinators-0.3 (crate (name "arg_combinators") (vers "0.3.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b6pzdkjpci8m58svv12fz3iz5k9f2ljnzafwgblpld9ki8kxgzx")))

(define-public crate-arg_combinators-0.4 (crate (name "arg_combinators") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dfhi25iy0cjyy163mdllsdy6ix0g69v2s1vqnm8dcr8qp8x88mv")))

(define-public crate-arg_combinators-0.5 (crate (name "arg_combinators") (vers "0.5.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "09fbpfv2f989iwc8faliqn43lsdw9bf9j4ckwky99nsrldqca96f")))

(define-public crate-arg_combinators-0.5 (crate (name "arg_combinators") (vers "0.5.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0d5rr09z9qzphh6lhn4sx2fk8kkr8ymbr53hy6valydan3w7f910")))

(define-public crate-arg_combinators-0.6 (crate (name "arg_combinators") (vers "0.6.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "13ry43gaaqkgd3l6m0c74ld7gicmp313garf51ddf3ifz508kk9l")))

(define-public crate-arg_dot_h-0.1 (crate (name "arg_dot_h") (vers "0.1.0") (hash "11qsiq56azhnz86537cyh0hjv9k9501dk7fkl8z7wpc795qvcpfv")))

(define-public crate-arg_dot_h-0.1 (crate (name "arg_dot_h") (vers "0.1.1") (hash "0nwmqai635vmslkf94z3nijhgs806sxbaj1gcfydagxshwksn2ig")))

(define-public crate-arg_dot_h-0.1 (crate (name "arg_dot_h") (vers "0.1.2") (hash "029fw9m8f0l87xid50yni3icpcd04pqn1ylzg5lfaggsa2b06ik5")))

(define-public crate-arg_enum_proc_macro-0.1 (crate (name "arg_enum_proc_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1dis2z8pc2x3mf44zj3vahqxl8f1038f27x859r6z4inlw20f29j")))

(define-public crate-arg_enum_proc_macro-0.1 (crate (name "arg_enum_proc_macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "03b9ls8wy7wqxfj0vjryp0n5dqvkw2pj36yd07m74c9b4760f6qx")))

(define-public crate-arg_enum_proc_macro-0.2 (crate (name "arg_enum_proc_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0mzf00lc902ys1n8hm1734pd1mp7lryls7855ffnkn21ckf70vlh")))

(define-public crate-arg_enum_proc_macro-0.3 (crate (name "arg_enum_proc_macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "021rr6j3n031ynfbm7kwb3j3bxvbsz40n0nqi78k47d3p92rihcv")))

(define-public crate-arg_enum_proc_macro-0.3 (crate (name "arg_enum_proc_macro") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qcjnkl08krm2cs3dikr8m4zxqi3pmcag55kzq6316sfc93033kw")))

(define-public crate-arg_enum_proc_macro-0.3 (crate (name "arg_enum_proc_macro") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ww7gl2yn779dq7fqigrvy64z71gr8z3n0ydhw2mjm46xr1rphnp")))

(define-public crate-arg_enum_proc_macro-0.3 (crate (name "arg_enum_proc_macro") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0q7g30bk6nx3hacihh2f0wsmqj11crbg96mwplqswwf9p3mv2qj0")))

(define-public crate-arg_enum_proc_macro-0.3 (crate (name "arg_enum_proc_macro") (vers "0.3.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1sjdfd5a8j6r99cf0bpqrd6b160x9vz97y5rysycsjda358jms8a")))

(define-public crate-arg_fn-1 (crate (name "arg_fn") (vers "1.0.0") (hash "17faa5hlvandy9a16x3gb6m32km1bq52ik6qdal54crjq8150f2h")))

(define-public crate-arg_input-1 (crate (name "arg_input") (vers "1.0.0") (hash "0gx16qm3vmz3gx4srrqsaf1ykh77h8x3s1cvn7a3776hp34ckdbs")))

(define-public crate-arg_input-2 (crate (name "arg_input") (vers "2.0.0") (hash "13nwghsl62g7nyj45qri62sg05iq5xwdaj88bvwiqgjznk7v3l38")))

(define-public crate-arg_input-2 (crate (name "arg_input") (vers "2.0.1") (hash "1i3cvk1l70ddzdyhxna24m8ycqm0inia18a7cyfxp2as4bng3xky")))

(define-public crate-arg_pars-0.1 (crate (name "arg_pars") (vers "0.1.0") (hash "15ma7j0jj51ljmvr3aagqjrzlx9n856r55yqldz99yqrc978qc1n")))

(define-public crate-arg_pars-0.1 (crate (name "arg_pars") (vers "0.1.1") (hash "03cvhzppdplbqrg788cxnyfjdc6l1d3ppbmzs45xq9zissz9lcr6")))

(define-public crate-arg_pars-0.1 (crate (name "arg_pars") (vers "0.1.2") (hash "1jm4fyibjl6nlcylylbnj2lnsryc5niajbjzsrz125c5b5fc4q8v")))

(define-public crate-arg_pars-0.1 (crate (name "arg_pars") (vers "0.1.3") (hash "02bw1yjxhqkc7707w8k5x5j5wsvsqr7gwsihwyyshai3b3gvhc6b")))

(define-public crate-arg_parse-0.0.9 (crate (name "arg_parse") (vers "0.0.9") (hash "1ppgjs3lz4bbsv5z7nkvcxazd12rsxqnnc4kjlwipyf1ns3kmpqd")))

(define-public crate-arg_parse-0.1 (crate (name "arg_parse") (vers "0.1.0") (hash "00mm9xh1vzi8dgazimry1w00dpxgw10n4b95fis1b3iyw72q6d90")))

(define-public crate-arg_parse-0.1 (crate (name "arg_parse") (vers "0.1.1") (hash "04d7y6b4l6035p0ih4wxzfqjfs16bacb1nhq7bichy0cpmj7p0wk")))

(define-public crate-arg_parse-0.2 (crate (name "arg_parse") (vers "0.2.0") (hash "1krdbh10sm8hwqfg1dk5vyvkv3a9zibvk1shcj074fx0nmdchr3i") (yanked #t)))

(define-public crate-arg_parse-0.2 (crate (name "arg_parse") (vers "0.2.1") (hash "09qdvq7i68kzybfr0khxz909dmrdakg4vv4ivqzsfynzpwmkf56r") (yanked #t)))

(define-public crate-arg_parse-0.2 (crate (name "arg_parse") (vers "0.2.2") (hash "1x1xlmmafvhspzki7nhsx3bfvvj30ly28qk79y4klkddxa9dn80z") (yanked #t)))

(define-public crate-arg_parse-0.2 (crate (name "arg_parse") (vers "0.2.3") (hash "0qc44vxc1c2xz26c1aq7bq7x5vky0rvr9dlz6k4c9zw00mkyw394")))

(define-public crate-arg_parse-0.3 (crate (name "arg_parse") (vers "0.3.0") (hash "07c0kwih5i6i3pmfk9ws3qkmcgvrllr4s9xl9xkd0cp5736y8z7k")))

(define-public crate-arg_parser-0.1 (crate (name "arg_parser") (vers "0.1.0") (hash "1mv82bkl4ym503h2b0cizbnc74rsrj3x72kwza50vsm4hn8xzg79")))

(define-public crate-arg_ripper-0.1 (crate (name "arg_ripper") (vers "0.1.0") (deps (list (crate-dep (name "mockall") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0xqkxj1pijip298w9rkbvby3c5l6c5zinm94s08yy9k9kkf14l28")))

