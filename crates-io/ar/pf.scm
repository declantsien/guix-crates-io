(define-module (crates-io ar pf) #:use-module (crates-io))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.0") (hash "11zr8nmarbirxrrc2gx52wxsjss5q6jcn9gpa2pwbxlp4j5ij4r8")))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.1") (hash "0q5hpjf4z3k3yxpipn5kdgdm4bar1hfm9kmh9r83rgj667rglckx")))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.2") (hash "150vjaza63shqgsyxf4xpkifc090iwhn6afcmxgh1qpsy18zsgci")))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.3") (hash "1viw4xvizj674gv9y0k4dm4r17jfia5w05irwd112zls9jgzmgw5") (features (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.4") (hash "1ynbh9yc5gqdvv45kwyhj7hx4ccfmyrxf7mi29m6f3yvvnl34l1i") (features (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.5") (hash "18rpzavwaq045xf7pclcb6d9c29cd0kirgvqm93piskaiclgk1xd") (features (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.6") (hash "0l12c93m3cm2dy7pbkvzhfrxmj010bxlgg226hb0d3mcm2n6j74k") (features (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.7") (hash "0bzzd4grjjl0b2sn618q6yz9xv6i5mjg3yj6vnj69q3vzj1wn9aq") (features (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.8") (hash "0liwrp4fbccni4ph0r17khr6p2sqdr0dz2s04p87m4bikypyzlys") (features (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.9") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0cdw2v0j6wmw0xa2544piw5bxzy586kv8lw3hmlznh4j0rxmjxvb") (features (quote (("std") ("default" "std"))))))

(define-public crate-arpfloat-0.1 (crate (name "arpfloat") (vers "0.1.10") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1qkrk2nixwjxghwnhk62k7slafbf3yjiys0xhrwvn2z981aj6mww") (features (quote (("std") ("default" "std"))))))

