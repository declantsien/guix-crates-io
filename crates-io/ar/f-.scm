(define-module (crates-io ar f-) #:use-module (crates-io))

(define-public crate-arf-strings-0.1 (crate (name "arf-strings") (vers "0.1.0") (hash "1z4df4cj4rfgsjizk7xl0ba81fr7372vlhx2spp037lpfjj5fg9i")))

(define-public crate-arf-strings-0.1 (crate (name "arf-strings") (vers "0.1.1") (hash "062baav35xjj00vp45nsfhydml9c3pyv10c28ha5256ykdjybhvc")))

(define-public crate-arf-strings-0.1 (crate (name "arf-strings") (vers "0.1.2") (hash "0l4qsyn9mfs3lwgfzafdfdx1jq3rad49k8vvhhil0qkmhns917h2")))

(define-public crate-arf-strings-0.2 (crate (name "arf-strings") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1gwl5v5dn13zqa6p2hmxg63j2pmik9y3dk9r4z7jj84bsvrzbmrb")))

(define-public crate-arf-strings-0.3 (crate (name "arf-strings") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "138r2jv389dqlyqybishg87qfsacibr5kmcwhmdlhra3dqvzsiwv")))

(define-public crate-arf-strings-0.4 (crate (name "arf-strings") (vers "0.4.0") (deps (list (crate-dep (name "rsix") (req "^0.18.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0l5gsjx8dz57cdk0bd0b591qw9g30crl0pmgn2hymswz72cxw5hq")))

(define-public crate-arf-strings-0.5 (crate (name "arf-strings") (vers "0.5.0") (deps (list (crate-dep (name "rsix") (req "^0.23.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0b84wx7g6jgsgx6jk8hxdv7iil6zkvjzqdrki5kbgpdw63251z2z")))

(define-public crate-arf-strings-0.5 (crate (name "arf-strings") (vers "0.5.1") (deps (list (crate-dep (name "rsix") (req "^0.24.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "11b0cap9ian1knbz4x70ncivz504313g3d1p6ygka1jq0gxd4qjg")))

(define-public crate-arf-strings-0.5 (crate (name "arf-strings") (vers "0.5.2") (deps (list (crate-dep (name "rsix") (req "^0.25.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0svlivpyih8pcamvkkm7wqy4z55sg978ad1bgs1c6i3z8aalfr68")))

(define-public crate-arf-strings-0.5 (crate (name "arf-strings") (vers "0.5.3") (deps (list (crate-dep (name "rustix") (req "^0.26.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0mcs2wbmaikahwaih9f95andzsp3z713f0pj3fjp1l342bs0xsyy") (yanked #t)))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.0") (deps (list (crate-dep (name "rustix") (req "^0.26.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0rk94gf4aiqllx3ggk002szn2lfws8zgi9kylk202dl6vxc4r9q8")))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.1") (deps (list (crate-dep (name "rustix") (req "^0.26.2") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1r3lh0inkgz72v3rmr5834619mz16l2xnx0nzf8vwhm3sg20lgj1")))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.2") (deps (list (crate-dep (name "rustix") (req "^0.27.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1hs55ry9mhvl4pqwv0kwp9flk73n6pq12bfxhvq80bhryzh2rvi4")))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.3") (deps (list (crate-dep (name "rustix") (req "^0.31.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1rmgn9ljd3gi115xkg72ywy04ap4hph8yjvhvdgw5sibc5s28831")))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.4") (deps (list (crate-dep (name "rustix") (req "^0.32.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1wk8c9diic4676gr6vbxay7k1bnhj7y8n1gmaddbsmd7k6i6gdgm")))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.5") (deps (list (crate-dep (name "rustix") (req "^0.33.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0ccp7a2chwasjwdfsvn7r2axvdfk1p27ra8kq11v3afa8j1h1g6x")))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.6") (deps (list (crate-dep (name "rustix") (req "^0.34.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0g4pyh440ig6h9wwj14j8h711lz93vzfkab0ppagsm7jva5i225d")))

(define-public crate-arf-strings-0.6 (crate (name "arf-strings") (vers "0.6.7") (deps (list (crate-dep (name "rustix") (req "^0.35.6") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0n8r1vx984brv14cjwz9si6qvaap6b13s2fvglgbcbc8fc4vd075")))

(define-public crate-arf-strings-0.7 (crate (name "arf-strings") (vers "0.7.0-rc1") (deps (list (crate-dep (name "rustix") (req "^0.36.0-rc1") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0bdqmhclqgshi90i913i397y7h0fk166gs1bn0ihbv7fydfq3d5j")))

(define-public crate-arf-strings-0.7 (crate (name "arf-strings") (vers "0.7.0") (deps (list (crate-dep (name "rustix") (req "^0.36.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1y48h6yidi5y16bny8x631apb1429h7jbx1s0njcrrxkkk3qakp2")))

(define-public crate-arf-strings-0.7 (crate (name "arf-strings") (vers "0.7.1") (deps (list (crate-dep (name "rustix") (req "^0.37.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1nmwsbdhc8cpl9pv64qy97xdky45yw7hsrgy24fjxmdbmby2ynxp")))

(define-public crate-arf-strings-0.7 (crate (name "arf-strings") (vers "0.7.2") (deps (list (crate-dep (name "rustix") (req "^0.38.0") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "0ncy11j7bk0ji94yb8nafyr5wvfg90q6vcbzcanpdbgyk4cy9szx")))

