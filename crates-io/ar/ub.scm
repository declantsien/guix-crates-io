(define-module (crates-io ar ub) #:use-module (crates-io))

(define-public crate-aruba_ripple-0.0.1 (crate (name "aruba_ripple") (vers "0.0.1") (deps (list (crate-dep (name "hyper") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.5") (default-features #t) (kind 0)))) (hash "0rjhlx35iw4h7l4bkrzi0laxj7sw20ch3vfcyhryn5wirxsshyg8")))

