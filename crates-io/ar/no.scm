(define-module (crates-io ar no) #:use-module (crates-io))

(define-public crate-arnold-rs-0.1 (crate (name "arnold-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1c4d28nsv73mipfr847yhipffjdd7r0f425l4gz4yiqcsmqc9198")))

