(define-module (crates-io ar t0) #:use-module (crates-io))

(define-public crate-art008-0.1 (crate (name "art008") (vers "0.1.0") (hash "0aij1a45j8g376y7b1fvzg332pnhhkxwviifvgramsahmsjxijk6")))

(define-public crate-art008-0.1 (crate (name "art008") (vers "0.1.1") (hash "0i8nbpm326a63ybdwrrqx2dvfykafky7zsbhp1vsxcnsi24p6x9b")))

(define-public crate-art010-0.1 (crate (name "art010") (vers "0.1.0") (hash "0phw62b96kqacfmaqz45l0ffqvscf0bmpypwdxwq529hlkkkavfj")))

(define-public crate-art0101-0.1 (crate (name "art0101") (vers "0.1.0") (hash "0afhid245ac7j42c5x9arr1b31f2gsjypzrs2zxc13zs7899wv6l") (yanked #t)))

(define-public crate-art0101-0.1 (crate (name "art0101") (vers "0.1.1") (hash "0c3ppy7jj3xsh31p3kxgp5s1dd4brg8w2zcqjl28jjq6ciqw87az")))

