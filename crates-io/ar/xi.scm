(define-module (crates-io ar xi) #:use-module (crates-io))

(define-public crate-arxiv-0.1 (crate (name "arxiv") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.3.20") (features (quote ("macros" "parsing"))) (default-features #t) (kind 0)))) (hash "07ik0aqph0lan7rnglkw9gx1mgn35x9cvphm909a9g1fm4ywc61z") (rust-version "1.63.0")))

(define-public crate-arxiv-0.2 (crate (name "arxiv") (vers "0.2.0") (deps (list (crate-dep (name "time") (req "^0.3.20") (features (quote ("macros" "parsing"))) (default-features #t) (kind 0)))) (hash "043120ywnlvknmgcydzhr7dc308bny6sw40sysfkmihcbyk3pgrn") (rust-version "1.63.0")))

(define-public crate-arxiv-rs-0.1 (crate (name "arxiv-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "03v8kglbqs5af44461w01jsjxllv3g9fhn017kabnxgs0xpnf50z")))

(define-public crate-arxiv-rs-0.1 (crate (name "arxiv-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0dn8g6priq70z0nvr69gid290255vjkc1dmk818vcxabzsmyswar")))

(define-public crate-arxiv-rs-0.1 (crate (name "arxiv-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "08xsj59zzh8qpxia8aq7mk5r85n9176asjkjrx47rhd54cfvsw4s")))

(define-public crate-arxiv-rs-0.1 (crate (name "arxiv-rs") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.3.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0565km563fvw6jgn3620k264x5yxcfacf7i8lfsbpnpcb2iw1x8c")))

(define-public crate-arxiv-rs-0.1 (crate (name "arxiv-rs") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.3.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "009zf6bfmi5n7x8ry9a5qpjqh0akfqbj4yjvlji3fs21rik9v3ll")))

(define-public crate-arxiv-rs-0.1 (crate (name "arxiv-rs") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.3.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "xml-rs") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "00j16whi0nqj3bw579ff1x2f0b639zdji78vzk6h2cv56svbj38j")))

