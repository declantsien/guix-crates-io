(define-module (crates-io ar al) #:use-module (crates-io))

(define-public crate-aral-0.0.0 (crate (name "aral") (vers "0.0.0") (hash "19x12dgypa1rd8j69fbf2q4b2vn7i43sbh29kzfgjpsq2whwhza5")))

(define-public crate-aral-0.1 (crate (name "aral") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("fs" "io-util" "net" "time" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qhc9llg0fiaaq4fayrb4zgy74w303wd1055n2p3cb8cvn83vzra") (features (quote (("runtime-tokio-multi-thread" "tokio" "tokio/rt-multi-thread") ("runtime-tokio-current-thread" "tokio") ("runtime-async-std" "async-std"))))))

(define-public crate-aral-0.1 (crate (name "aral") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("fs" "io-util" "net" "time" "rt"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ingv6zy8njmw3mmg0rqhnnkaj3466jcx3v0g8dzdl5b9r2j4dm1") (features (quote (("runtime-tokio-multi-thread" "tokio" "tokio/rt-multi-thread") ("runtime-tokio-current-thread" "tokio") ("runtime-async-std" "async-std"))))))

(define-public crate-aral-0.1 (crate (name "aral") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "aral-runtime-async-std") (req "^0.1.0-alpha.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aral-runtime-noop") (req "^0.1.0-alpha.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aral-runtime-tokio") (req "^0.1.0-alpha.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1zy17kjbxjsp9ci2b1sn7yhj017mgfrphdnjdifcripnqq5069ga") (features (quote (("runtime-tokio" "aral-runtime-tokio") ("runtime-noop" "aral-runtime-noop") ("runtime-async-std" "aral-runtime-async-std") ("default" "runtime-noop"))))))

(define-public crate-aral-0.1 (crate (name "aral") (vers "0.1.0") (deps (list (crate-dep (name "aral-runtime-async-std") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aral-runtime-noop") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aral-runtime-tokio") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0j3zfd5sma682r21p5bnfsnif3lsdpyh062mwqs2d6v6dd6qnmh8") (features (quote (("runtime-tokio" "aral-runtime-tokio") ("runtime-noop" "aral-runtime-noop") ("runtime-async-std" "aral-runtime-async-std") ("default" "runtime-noop")))) (rust-version "1.75")))

(define-public crate-aral-runtime-async-std-0.1 (crate (name "aral-runtime-async-std") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "19fcfbw77qhlgb9xjhl4wn698mwvzfyrrjf8m72fmwdh2nz4wg2i")))

(define-public crate-aral-runtime-async-std-0.1 (crate (name "aral-runtime-async-std") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "1fsy3sbmjhz25nd651amfmk825ramw7jk0cz28kc98zvg8v92sj2")))

(define-public crate-aral-runtime-async-std-0.1 (crate (name "aral-runtime-async-std") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "12krxsnb6rdbizsiyjsq7rnzm0mwkxc2ysnywfjbm9rh5gp2jlj6") (rust-version "1.75")))

(define-public crate-aral-runtime-noop-0.1 (crate (name "aral-runtime-noop") (vers "0.1.0-alpha.4") (hash "07l8sjdar9axgav06kjrpjz8s2cd9azsc9x3lbz756h0ygsg0247")))

(define-public crate-aral-runtime-noop-0.1 (crate (name "aral-runtime-noop") (vers "0.1.0-alpha.5") (hash "173r7jsv44z08d416hv61yp6zhfxn175sa7nwqnnhrmwbcvnsalr")))

(define-public crate-aral-runtime-noop-0.1 (crate (name "aral-runtime-noop") (vers "0.1.0") (hash "0h5342fk21n6h3kzbifscyyyjs6jx8fv5pw8iis8px5plpq4fg54") (rust-version "1.75")))

(define-public crate-aral-runtime-tokio-0.1 (crate (name "aral-runtime-tokio") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("fs" "time" "rt" "io-util" "net"))) (default-features #t) (kind 0)))) (hash "0ilh7xz90q2907w75185f8lj97pynmw60fvicg4ipabslp7p0agj")))

(define-public crate-aral-runtime-tokio-0.1 (crate (name "aral-runtime-tokio") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("fs" "time" "rt" "io-util" "net"))) (default-features #t) (kind 0)))) (hash "1s7gsvha6fpd8z5i2vr4iqpmrn2kbakrv1qf0wbrlqxmd8fby0f4")))

(define-public crate-aral-runtime-tokio-0.1 (crate (name "aral-runtime-tokio") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("fs" "time" "rt" "io-util" "net"))) (default-features #t) (kind 0)))) (hash "16lflrdysws30zxpy3jw8avxd7d136dl68ag6srpi0xsci63glf1") (rust-version "1.75")))

