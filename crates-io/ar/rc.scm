(define-module (crates-io ar rc) #:use-module (crates-io))

(define-public crate-arrcat-0.1 (crate (name "arrcat") (vers "0.1.0") (deps (list (crate-dep (name "core_extensions") (req "^1.5") (features (quote ("const_default" "const_val"))) (default-features #t) (kind 2)))) (hash "1cn486ihjshw1yc37ypwsds68jmf5w529d6m6qs9gwcx7czzivld")))

(define-public crate-arrcat-0.1 (crate (name "arrcat") (vers "0.1.1") (deps (list (crate-dep (name "core_extensions") (req "^1.5") (features (quote ("const_default" "const_val"))) (default-features #t) (kind 2)))) (hash "19bdag1dsjxln1yixirdiaclhrmw8hgwx1i7br2b86cl7zszd8lq")))

