(define-module (crates-io ar mc) #:use-module (crates-io))

(define-public crate-armc-1 (crate (name "armc") (vers "1.0.0") (hash "1jzwgag2bmbbgcaq1qci2jnl7s0psvddmvr4r92mbn9vvrsfs4xx")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.1.0") (hash "0ibbsshy66s7vihbqhw7h3fnlf1qw6k83pm2abxdkyxw0m8m9llh")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.1.1") (hash "19889ka6if41i0a036fbqcn6w20mjl4haixjpq7qb5kjj23yggph")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.2.1") (hash "0pzg7cxagmacqsh7nkxs8hl1wq802y7mqn0n053b3ggx30yw8z7x")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.3.1") (hash "0f7wdn699041wbnm12wvg66yi7ksalkazvj61p7qqsj95cm6injb")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.3.2") (hash "0yg7jwn374891lia8jdwh1kzdkb0djr3yi9l9dymm4d5lqhxivww")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.3.3") (hash "0qgzl8pqb5dq84l2r3rndhs2xvvr3gb9yq86g1bncgnsks2ppyzq")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.4.3") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "10gqz839si1h9szx62rgx4w9779garzl2ksclgp9grjds86f4fb2")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.4.4") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "09fpnwbihd725iwjjvwjp8hm49x18l1n6i4sv0yk9qsx0sraxxf0")))

(define-public crate-armc-1 (crate (name "armc") (vers "1.4.5") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "1vq13zb2xdx0jcnk37a9wjnqg8hnw6kv5924gacbm2nk61lmi97l")))

