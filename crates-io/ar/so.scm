(define-module (crates-io ar so) #:use-module (crates-io))

(define-public crate-arson-0.1 (crate (name "arson") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0dx2y10kjkfm9siy5crc5bklkl6m40hyz9yvr9aa1iylikmlg63k")))

(define-public crate-arson-0.2 (crate (name "arson") (vers "0.2.0") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1snh24rcyfgwdpiph4a78nrz0zqrmrl8z8ha0380973xygy7q3lc")))

(define-public crate-arson-0.2 (crate (name "arson") (vers "0.2.1") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0hr5fb7yxk7mjfmd3qk12hyw191n51kba83z8f2llcgh191bypkr")))

