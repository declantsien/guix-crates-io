(define-module (crates-io ar ip) #:use-module (crates-io))

(define-public crate-ariprog-0.1 (crate (name "ariprog") (vers "0.1.0") (hash "10qgpzapcfxvh0ww78dl8bln783shj2njpsjaxwm5mr66xdgcpxd")))

(define-public crate-ariprog-0.1 (crate (name "ariprog") (vers "0.1.1") (hash "1akfm925rcqk9ildjqgcjq9b9j63vrs0108zsl0d07gifymdqkb4")))

(define-public crate-ariprog-0.1 (crate (name "ariprog") (vers "0.1.2") (hash "0i36ncx6m0z6iqpkfd6g8pili5rjk0jrqa25w0m98b4km7zbs412")))

(define-public crate-ariprog-0.1 (crate (name "ariprog") (vers "0.1.3") (hash "086ln9d82gxvxj9iacy223izhm9zd16k9dv0s9y00l20h75cbvip")))

(define-public crate-ariprog-0.1 (crate (name "ariprog") (vers "0.1.4") (hash "18w8mqsv22cl0wxg9jmxwbzzgbi60cfvih6vr0bfcbs1gkz4mglc")))

