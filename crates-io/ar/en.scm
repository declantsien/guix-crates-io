(define-module (crates-io ar en) #:use-module (crates-io))

(define-public crate-aren_alloc-0.1 (crate (name "aren_alloc") (vers "0.1.0") (hash "1gg2wwqjdd7n1x8nmcs01zqdicsa73ban7rjhrzdkw71r9mmk0gy")))

(define-public crate-aren_alloc-0.2 (crate (name "aren_alloc") (vers "0.2.0") (hash "1g8cyqpikb2ng9kyxjcc7dwyyazcv9ywmdc1ivrqnjb846yg7a1d")))

(define-public crate-aren_alloc-0.2 (crate (name "aren_alloc") (vers "0.2.1") (hash "1aqw3b0rmg1l3x107n02av7rfbzly903aakxp3b741p55rh5391k")))

(define-public crate-aren_alloc-0.3 (crate (name "aren_alloc") (vers "0.3.0") (hash "072mn4y7b8fkj0b5dgijm686yw98bkqxylaw75d2dnf06ihfvxjz")))

(define-public crate-arena-graph-0.1 (crate (name "arena-graph") (vers "0.1.0") (deps (list (crate-dep (name "typed-arena") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0g40ysa2kp6wk4vlfyc43g20i9djjpa6lgy6m4w8k0w1738vyylv")))

(define-public crate-arena-rs-0.1 (crate (name "arena-rs") (vers "0.1.0") (hash "0nlfq70w6q5fckzgwwjjynhdhfnrkpaihm0kl1j9yidcjk9564y0") (yanked #t)))

(define-public crate-arena-rs-0.1 (crate (name "arena-rs") (vers "0.1.1") (hash "0cnss28bk0h96pyxsvb1swv1jq7rfd4m13i7678ch7if48a082im") (yanked #t)))

(define-public crate-arena-tree-0.1 (crate (name "arena-tree") (vers "0.1.0") (deps (list (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zm9yj9lqvmrab07lc3bbbqhhsqh5lm2kqzdxh1dzh482nz5d58h") (yanked #t)))

(define-public crate-arena-tree-0.2 (crate (name "arena-tree") (vers "0.2.0") (deps (list (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 2)))) (hash "0lya50526yp3k6qkshzscl50q66g68h69j6j61na6f4sg82pk4lk") (yanked #t)))

(define-public crate-arena-tree-0.3 (crate (name "arena-tree") (vers "0.3.0") (deps (list (crate-dep (name "typed-arena") (req "^1.0") (default-features #t) (kind 2)))) (hash "0q5dyp26bkg6v9pxs0im4zwfknzwzvhpwv5gby3n4a1xpgx22nxf") (yanked #t)))

(define-public crate-arena-voxel-tree-0.0.1 (crate (name "arena-voxel-tree") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "010jr0zhmfb7bd1551hs3ldz8r2w90j8rjwjndr7wz2ywi4g6iw7")))

(define-public crate-arena64-0.1 (crate (name "arena64") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1wshr5f1i85w0hyw5fgjlng3wg6xms7m4lmi217vap902yq577kf") (yanked #t)))

(define-public crate-arena64-0.1 (crate (name "arena64") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1n74mdikcy2schjs8swq3map3mp63ypx18g68ji0c1yax6f8vfj8") (yanked #t)))

(define-public crate-arena64-0.1 (crate (name "arena64") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1k66nlw63pafd2ss1fdb1crf17ypa3x8iph7d4mhg2nf4awlvnyc") (yanked #t)))

(define-public crate-arena64-0.1 (crate (name "arena64") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0agza2j2pg4r18nvq11drpd10pzb7a31z32chav3rhpqpkpyzl7d")))

(define-public crate-arena_system-0.0.1 (crate (name "arena_system") (vers "0.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bca44cbk2fxamxixcsn0g54wkvh5v8jxjbm7gnldr2ipvfpbprv")))

(define-public crate-arena_system-0.0.2 (crate (name "arena_system") (vers "0.0.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z7b2zhiy5gvvasx82rv2dm1mj70la98bpm7l0s6kr9q38r6axkz")))

(define-public crate-arena_system-0.0.3 (crate (name "arena_system") (vers "0.0.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1") (default-features #t) (kind 0)))) (hash "02s14m57abb90110xwraxbslam2qms8v34p5mlyi42c3s1yil48j")))

(define-public crate-arena_system-0.0.4 (crate (name "arena_system") (vers "0.0.4") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1") (default-features #t) (kind 0)))) (hash "195cvnkdybg0wdvjqx13mx5nm0mbc5m508f7df9bqbspjfb2f0vw")))

(define-public crate-arena_system-0.0.5 (crate (name "arena_system") (vers "0.0.5") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1") (default-features #t) (kind 0)))) (hash "03a2bwzgl2zs7zd9d8dd44p8jpxrss1cbf2bnns02q9gbj3a0iv9")))

(define-public crate-arena_system-0.0.6 (crate (name "arena_system") (vers "0.0.6") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jhd35yar9gywlsdrqf20pbba422qciqg9yjsc53zlwjczgf7w8c")))

(define-public crate-arena_system-0.0.7 (crate (name "arena_system") (vers "0.0.7") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1") (default-features #t) (kind 0)))) (hash "00glywi01pqcxli1dir8qd00mk3z6f9afl7041v3im39gxar9q6l")))

(define-public crate-arena_system-0.0.8 (crate (name "arena_system") (vers "0.0.8") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1n8k1a43f71np4whkj9cnn10ddc3hcvk6nl1rl337a795hg8wjb7")))

(define-public crate-arena_system-0.0.9 (crate (name "arena_system") (vers "0.0.9") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "146kcmvqi3wxlc74w8v1bhm3iry6dswdclvlvjd198lhk3cs30ic")))

(define-public crate-arena_system-0.0.10 (crate (name "arena_system") (vers "0.0.10") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0snavh6zbar5frxmbb00jqxka9ca39zl8flvajkkmp47x2379ail")))

(define-public crate-arena_system-0.0.11 (crate (name "arena_system") (vers "0.0.11") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1xh5vz4r4wk4j8j7px05il37yy51dpsljhkjg56959xqdw3c67wi")))

(define-public crate-arena_system-0.0.20 (crate (name "arena_system") (vers "0.0.20") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_cell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1i6rgjbgv1wqg98b37nmwc9j7yhi8bxixcxbqqm7x1ww6lhmnbi0")))

(define-public crate-arenalloc-0.0.0 (crate (name "arenalloc") (vers "0.0.0") (hash "1lh75basdz3dzrcb3mkq5gmpibx429dcqn13pzgayz69ris8lafl")))

(define-public crate-arenas-0.1 (crate (name "arenas") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "19zzz9m3dirxwy1v4fgyxl4fjp3kvawp7lw8jgk0z6kh9nbymlfw")))

(define-public crate-arenas-0.1 (crate (name "arenas") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xvbwzk6d6cyaxypnlq1sp9rlacq58scpl1yazqx53mk1c026ylf")))

(define-public crate-arenas-0.2 (crate (name "arenas") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0siyx8710pg0f3483452swsz3jpxr2dlcygqpz1ra6gr1p9d6ngw")))

(define-public crate-arenatree-0.1 (crate (name "arenatree") (vers "0.1.0") (hash "08z4r9g1pf7r7czhj9s14y1c1ndz5k448nkwz0mdyqlvrqcxy8nh")))

(define-public crate-arenatree-0.1 (crate (name "arenatree") (vers "0.1.1") (hash "1g0iix3pi62ziry3yd0zrvlb3hyzshjjnp1sbnl4ykfx0hhmx9q4")))

(define-public crate-arenatree-0.1 (crate (name "arenatree") (vers "0.1.2") (hash "11ddl0fxc6gyi74xjdvblrp76dlw56inihjdkajgh5827hfycmpk") (yanked #t)))

(define-public crate-arenatree-0.1 (crate (name "arenatree") (vers "0.1.3") (hash "1akpina7ma43x2h895vnbgg3qrh1cvscrjagd0570x5xdyqhrhhj")))

(define-public crate-arenavec-0.1 (crate (name "arenavec") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "proptest") (req "^0.9.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("basetsd" "memoryapi" "minwindef" "sysinfoapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0nvpz6sap85lvhpcy762cxiglcykfjwmxlqnxn9rl4553laqs29b")))

(define-public crate-arenavec-0.1 (crate (name "arenavec") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "proptest") (req "^0.9.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("basetsd" "memoryapi" "minwindef" "sysinfoapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0cvnf4ddhinpg8nsphzzn45qk3j9akdd1jk1aqzcanvi2nlnfgka")))

(define-public crate-arendur-0.0.4 (crate (name "arendur") (vers "0.0.4") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.14") (features (quote ("eders"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.24") (default-features #t) (kind 2)) (crate-dep (name "copy_arena") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "flame") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "flame") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tobj") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dcywiinrph2zahpj7p49cvd0j7a7lfzlmm6f6sr19vz5dinq0zn") (features (quote (("default"))))))

(define-public crate-arendur-0.0.5 (crate (name "arendur") (vers "0.0.5") (deps (list (crate-dep (name "aren_alloc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.14") (features (quote ("eders"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.24") (default-features #t) (kind 2)) (crate-dep (name "copy_arena") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "flame") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "flame") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tobj") (req "^0.1") (default-features #t) (kind 0)))) (hash "1m7svcpcrxzfgdp8fsb8vcniic6zrrh4rdx6qkh3v4jdwvwa6nqh") (features (quote (("default"))))))

(define-public crate-arenta-1 (crate (name "arenta") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.5.3") (features (quote ("date"))) (default-features #t) (kind 0)))) (hash "1drryri4yfnqnivqsyydxqk9qf3sxfzgxdcsc12pvm1abnmfixi3")))

(define-public crate-arenta-1 (crate (name "arenta") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.5.3") (features (quote ("date"))) (default-features #t) (kind 0)))) (hash "0680yhwrj1hgf2ihvms88k2jcbxp8s8ajiww9p6lh3c8va272jmn")))

(define-public crate-arenta-1 (crate (name "arenta") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.5.3") (features (quote ("date"))) (default-features #t) (kind 0)))) (hash "100cshr0nis7zc065y25m9f9z1qmh0xv6xr09gv94kfw4xrx6ks7")))

