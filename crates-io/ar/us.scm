(define-module (crates-io ar us) #:use-module (crates-io))

(define-public crate-arush-0.0.1 (crate (name "arush") (vers "0.0.1") (hash "0q9sgjcyjjfb3451ad4fcl17w2xdx0b2ry5mrv9jgdkffalsiwl2") (yanked #t)))

(define-public crate-arush-0.0.5 (crate (name "arush") (vers "0.0.5") (hash "1ihghwys2r968vzb5cgw17p5869adhcmmkq33dkf1pafcclj7hx4")))

