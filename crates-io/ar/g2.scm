(define-module (crates-io ar g2) #:use-module (crates-io))

(define-public crate-arg2stdin-0.1 (crate (name "arg2stdin") (vers "0.1.0") (hash "09fbgpyjim2md575z99g87kd4hvdm3i4dgckraihgm5p2wc1r0n5")))

(define-public crate-arg2stdin-0.1 (crate (name "arg2stdin") (vers "0.1.1") (hash "1d0bvf93ydqr849rnb1v6wgvs69d0lmjbgk6ys1hbpzv9sw5wqs2") (yanked #t)))

(define-public crate-arg2stdin-0.1 (crate (name "arg2stdin") (vers "0.1.2") (hash "1mmp7xzvqb1llbmi9nklxg98gfkk4j6hyvbw544a8bhgmm08nkfn") (yanked #t)))

(define-public crate-arg2stdin-0.1 (crate (name "arg2stdin") (vers "0.1.3") (hash "1jp8mzlxmzmh42999mzs628lagyg1znh2rgyfjakplq2f571b9va") (yanked #t)))

(define-public crate-arg2stdin-0.1 (crate (name "arg2stdin") (vers "0.1.4") (hash "1cvjqqqv3g70vwn0085cp12r7j4byqqfvqm2hd21h4693rga6idl")))

