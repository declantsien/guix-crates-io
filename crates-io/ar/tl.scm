(define-module (crates-io ar tl) #:use-module (crates-io))

(define-public crate-artloop-0.0.0 (crate (name "artloop") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0mffv1f5jiyjyhna3n6srgkqgm46as4y5r4f8144v3fqb8z2lxmc")))

(define-public crate-artlr_lex-0.1 (crate (name "artlr_lex") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "148dgpcwl0xr3xrx1ccm364r4ha8ddz2y30b3yk48m9sys05zm7y")))

(define-public crate-artlr_lex-0.1 (crate (name "artlr_lex") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0vqcc47m975y2rypc3w9nykkzgxhmi3fi2i4rkx5km51gb7ds8hv")))

(define-public crate-artlr_lex-0.2 (crate (name "artlr_lex") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0bqfyap28pn4psn3hnrpqy0vvnk1ziqspbm77p0wkdl9malsqwi8")))

(define-public crate-artlr_lex-0.2 (crate (name "artlr_lex") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0z9q3xlfgqck480b5pml27higyvhjia41xii1ry59vmgm6djr4ql")))

(define-public crate-artlr_lex-0.2 (crate (name "artlr_lex") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1lwcmmrajfa1s57si9kk7wfhlmpmgbaxhv0kdpyhlmm172d84y9l")))

(define-public crate-artlr_syntax-0.1 (crate (name "artlr_syntax") (vers "0.1.0") (deps (list (crate-dep (name "artlr_lex") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1w6grq6vq9z0170xlllw0r2vhiindd40v4zx47zz47m0820s2dwi")))

(define-public crate-artlr_syntax-0.2 (crate (name "artlr_syntax") (vers "0.2.0") (hash "0pmbqd02122xm2cadf59ada4pmq4z2d5k8kvyn4krd5fvc64na64")))

(define-public crate-artlr_syntax-0.3 (crate (name "artlr_syntax") (vers "0.3.0") (hash "0lap7gisv7h6s94dg2vwvbmhzfa7cwri4dkhb53r73fdj838ibnn")))

