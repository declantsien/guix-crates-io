(define-module (crates-io ar cm) #:use-module (crates-io))

(define-public crate-arcm-0.1 (crate (name "arcm") (vers "0.1.0") (hash "1mvdm116ghg94l87izl4i94v1nmdvxkjz3ays4p0c200z18yizwf")))

(define-public crate-arcmut-0.1 (crate (name "arcmut") (vers "0.1.0") (deps (list (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1pgxb8kijzn2wx87ng9sm0x0appk4r53412frhmcznr4bi11igxz") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

(define-public crate-arcmutex-0.1 (crate (name "arcmutex") (vers "0.1.0") (hash "1g6sqavlpq69hydakncfkl5qnzwlbhl5z4my9v8z7595sn5v5apf")))

(define-public crate-arcmutex-0.1 (crate (name "arcmutex") (vers "0.1.1") (hash "18mzaimhgwv9zflwvzn3z3k9n5rjz1amjv70pddvk7h87ig72z41")))

(define-public crate-arcmutex-0.2 (crate (name "arcmutex") (vers "0.2.0") (hash "1cixys988frxal8ssh4yca3cv03pnf33swwcnllsn5hbxw985hbc")))

