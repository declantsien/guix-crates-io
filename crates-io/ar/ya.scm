(define-module (crates-io ar ya) #:use-module (crates-io))

(define-public crate-arya-0.0.1 (crate (name "arya") (vers "0.0.1") (hash "0zbdlsjz10jhb33wbpk2pkb83km9169mgizdvmacqdd33jasx6qx")))

(define-public crate-arya-0.0.2 (crate (name "arya") (vers "0.0.2") (hash "0s5bqiwh8swgywzz0nmhc5rnpddhfdcwbqf7rhdrzr0lyzlidpws")))

(define-public crate-arya-0.0.3 (crate (name "arya") (vers "0.0.3") (hash "1s6ris72h78ay218lminl8imljbf69bddimz56g32pgzn13w88ws")))

