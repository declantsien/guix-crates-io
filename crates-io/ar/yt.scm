(define-module (crates-io ar yt) #:use-module (crates-io))

(define-public crate-aryth-0.0.1 (crate (name "aryth") (vers "0.0.1") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.5") (default-features #t) (kind 0)))) (hash "0v8csfm1g9cibiin02pgzg36k999z5q93rg7gj7c4q18qis4np9g")))

(define-public crate-aryth-0.0.2 (crate (name "aryth") (vers "0.0.2") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.5") (default-features #t) (kind 0)))) (hash "14c7hvb4qnppm20aw2nhqp16jxqnjipxsd8r4da35hqk30h5xkkg")))

(define-public crate-aryth-0.0.3 (crate (name "aryth") (vers "0.0.3") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.8") (default-features #t) (kind 0)))) (hash "01j601ra3mhbaipsl120136sr8hffxb4088s7zxysqkfnmjr9bpc")))

(define-public crate-aryth-0.0.4 (crate (name "aryth") (vers "0.0.4") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.8") (default-features #t) (kind 0)))) (hash "0r4c2z544l95nv87facpzim8yg04smp61ds7mphwrkshmbap9hp7")))

(define-public crate-aryth-0.0.5 (crate (name "aryth") (vers "0.0.5") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.8") (default-features #t) (kind 0)))) (hash "0bikygkkyg6p4rv1gj8d1zbl66m6yb23fspdn88cbjzc0qmmxal4")))

(define-public crate-aryth-0.0.6 (crate (name "aryth") (vers "0.0.6") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.8") (default-features #t) (kind 0)))) (hash "0wqplq330qjinsdncvba2ksh7jjjp81c9yclazr8fq24l8cfv0si")))

(define-public crate-aryth-0.0.7 (crate (name "aryth") (vers "0.0.7") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.9") (default-features #t) (kind 0)))) (hash "0bkmydj4avff0liwcy8p5wfrlccdixdbx6l0ilv2ps49bavj9gpr")))

(define-public crate-aryth-0.0.8 (crate (name "aryth") (vers "0.0.8") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.2") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.15") (default-features #t) (kind 0)))) (hash "00rx2z76gfx1gc0vx8p9m72nlbhznia8xnzjsj0fy12rijx5rhnh")))

(define-public crate-aryth-0.0.9 (crate (name "aryth") (vers "0.0.9") (deps (list (crate-dep (name "num") (req ">=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.2") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.16") (default-features #t) (kind 0)))) (hash "1sfs670n5r16vpqr80mjy64yakpsx5hq305fla60d56d0aw5w8xx")))

(define-public crate-aryth-0.0.10 (crate (name "aryth") (vers "0.0.10") (deps (list (crate-dep (name "num") (req ">=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.6") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.19") (default-features #t) (kind 0)))) (hash "0x6lj32igz34fazfa4llq5s2p43bgxskiwkiqaw0piq366s1nsy5")))

(define-public crate-aryth-0.0.11 (crate (name "aryth") (vers "0.0.11") (deps (list (crate-dep (name "num") (req ">=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "texting") (req ">=0.0.7") (default-features #t) (kind 0)) (crate-dep (name "veho") (req ">=0.0.20") (default-features #t) (kind 0)))) (hash "19gv5hr7gh9pj97z38h1kv89r8fckcyqplz75jij6c2r9sl1p7aw")))

