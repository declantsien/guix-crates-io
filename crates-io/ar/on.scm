(define-module (crates-io ar on) #:use-module (crates-io))

(define-public crate-aronia-0.0.0 (crate (name "aronia") (vers "0.0.0") (hash "1gpdj6cvk1bg3z9pdnsii7xsqd0zgz9i59f2awabn9zsvn9y7x17")))

(define-public crate-AronIS_GPS_Crate-0.1 (crate (name "AronIS_GPS_Crate") (vers "0.1.0") (deps (list (crate-dep (name "rpi_embedded") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0c06qf7plpdjl7dccs22vrrwsk5bvfyy29zcnpc7p9smjhs7cqd1") (yanked #t)))

(define-public crate-AronIS_GPS_Crate-0.1 (crate (name "AronIS_GPS_Crate") (vers "0.1.1") (deps (list (crate-dep (name "rpi_embedded") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1wc3naj0mid975dz3m203ryiikybggz9wghx88g13mimvhd2xb97") (yanked #t)))

(define-public crate-AronIS_GPS_Crate-0.1 (crate (name "AronIS_GPS_Crate") (vers "0.1.2") (deps (list (crate-dep (name "rpi_embedded") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0zmqfcygpmqykkr8q6pdjic3rnq0p245xvyrr3sbzsq8hbvmy8zd") (yanked #t)))

(define-public crate-AronIS_GPS_Crate-0.1 (crate (name "AronIS_GPS_Crate") (vers "0.1.3") (deps (list (crate-dep (name "rpi_embedded") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0d0kj0bsv67k11fb9jyjw99npp6imfbwbskxy43b5rizlbz6pa45") (yanked #t)))

(define-public crate-AronIS_GPS_Crate-0.1 (crate (name "AronIS_GPS_Crate") (vers "0.1.4") (deps (list (crate-dep (name "rpi_embedded") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1m4c33giywv3xcrfllkrn4x0kbsyfiy9pww3mi54jw7c9pbplzvf") (yanked #t)))

(define-public crate-AronIS_GPS_Crate-0.1 (crate (name "AronIS_GPS_Crate") (vers "0.1.5") (deps (list (crate-dep (name "rpi_embedded") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1b50a407p7kanginan4c86p0jb4cx2x9s6kq8gsrazx1p2bqbzfk")))

