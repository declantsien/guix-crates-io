(define-module (crates-io ar c4) #:use-module (crates-io))

(define-public crate-arc4-0.1 (crate (name "arc4") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "0iwk6jm3xi0d1cfgbf5a4p8w37fh8dg4kyxiz5i6ngqxqagd0g62")))

