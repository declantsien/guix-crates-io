(define-module (crates-io ar cf) #:use-module (crates-io))

(define-public crate-arcfinder-0.1 (crate (name "arcfinder") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1x4dcsmjxb5pz0pbp67kqblsc2fxggh160b1sa4b6i3s8hqq4s9g") (yanked #t)))

(define-public crate-arcfinder-0.1 (crate (name "arcfinder") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "0wx033l0pfd1930qbf2drsww4bv8002bsxmhx3fwfkj36wyvk6nk")))

(define-public crate-arcfinder-0.1 (crate (name "arcfinder") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "1p6b820azqb2zp79gkfvkaz7pqw8gcxc01lmy6g9hb1j5d481h3m")))

(define-public crate-arcfinder-0.1 (crate (name "arcfinder") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1cpmxjhf4qirpj6rjpjkpvq4xlrrn9nmcfc39kf21pi11x5fjzgj")))

