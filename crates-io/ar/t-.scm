(define-module (crates-io ar t-) #:use-module (crates-io))

(define-public crate-art-crate-io-test-0.1 (crate (name "art-crate-io-test") (vers "0.1.0") (hash "059zx4miswkvh2jj8662rl0p94lrs2cb7nlw6caj3nd1jkgj6xv6")))

(define-public crate-art-dl-0.0.1 (crate (name "art-dl") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0x0ijjj2vnbf2ny8997j9jh54ji72i29gc99mzi28y3jdal2zs9f")))

(define-public crate-art-example-cekingx-0.1 (crate (name "art-example-cekingx") (vers "0.1.0") (hash "1wqjhs15qfy5q11r6v8l289hdjd7cqy93waqy6mwlavnydclsq2m")))

(define-public crate-art-fns-0.1 (crate (name "art-fns") (vers "0.1.0") (hash "0g7x0wddyxmajs5nrr0mp2fy0bzh5pggy9q76dhjm8qiq9zy5z81") (yanked #t)))

(define-public crate-art-jlam55555-0.1 (crate (name "art-jlam55555") (vers "0.1.0") (hash "05c05496frafay3fjkvy3qn1yhgzwm02vi5wc77igij4cvps6iyz")))

(define-public crate-art-mk-0.1 (crate (name "art-mk") (vers "0.1.0") (hash "15vdb6jjmr9lfspshbbrjkqv9kc7ap1fcgrbmcinpfbaj0s5palw")))

(define-public crate-art-mk-0.1 (crate (name "art-mk") (vers "0.1.1") (hash "10y9ky8kndhgg2sslxblxzc0frfidz382sy47iddh0v3i0iyrn4p")))

(define-public crate-art-prompt-webhook-1 (crate (name "art-prompt-webhook") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0z1m2kgirg6cnwwxhyz3hahk669xgs56bw2hd56sk8qq69dcd11m")))

(define-public crate-art-prompt-webhook-1 (crate (name "art-prompt-webhook") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0vv3ngrj3r97dnng7j3k77n1lldl48mxwjcz51w0jvi8aikyallm")))

(define-public crate-art-some-0.1 (crate (name "art-some") (vers "0.1.0") (hash "07dmf8axvbdgfxbsyq35g0axyqqcwrwdyyhhqw567a881lsrp47s") (yanked #t)))

(define-public crate-art-stamps-0.0.1 (crate (name "art-stamps") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "1k6dhxipq8n0ms7byqr3yvz5vg1fl81d0ss9qar4rvljs6xw28cb")))

(define-public crate-art-stamps-0.1 (crate (name "art-stamps") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.98") (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "19h8gm0x7xq45clax0pija6k01xarf7rxlnc4xc2r292xfy4hxk2")))

(define-public crate-art-test-0.1 (crate (name "art-test") (vers "0.1.0") (hash "19pw96pvjqfl4l7kmycjlirkw6zbia2h7xzcgn54sm90m2l82psf") (yanked #t)))

(define-public crate-art-test-7-0.1 (crate (name "art-test-7") (vers "0.1.0") (hash "0k51m88ngp6k1q6arqgwwkb6hyy7b8ifp4amk7k5cadn70ppvzsx") (yanked #t)))

(define-public crate-art-test-8-0.1 (crate (name "art-test-8") (vers "0.1.0") (hash "1m7p8l7fc790h0q8fxvy5bv43w1qp4cyiza2sqi91vgw5nyk0x2r")))

(define-public crate-art-test-kerwin-0.1 (crate (name "art-test-kerwin") (vers "0.1.0") (hash "18931q13xyx0n3nnqkf3a3d05j1sh2x8z8nqrshlp170l65mmmai") (yanked #t)))

(define-public crate-art-tree-0.1 (crate (name "art-tree") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0xjvjphvqwpdarwgvndff5yr9afcbjzzkawgc9z93d8pmcnadpnj")))

(define-public crate-art-tree-0.2 (crate (name "art-tree") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1wpiwdmg5hdcnn3ld8mlhw67a7cqvn65zpbxzrv5x5rjhnv2d15s")))

(define-public crate-art-xornotor-0.1 (crate (name "art-xornotor") (vers "0.1.0") (hash "020fppdqs59wayzb0xlnyrqddjkv3yx58rl6q1k854ac4j6c8xy4") (yanked #t)))

