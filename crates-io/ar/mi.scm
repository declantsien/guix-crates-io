(define-module (crates-io ar mi) #:use-module (crates-io))

(define-public crate-armistice-0.0.0 (crate (name "armistice") (vers "0.0.0") (hash "0gsrdp106ahbwzdq55cd7pv6pvwy6hr9xpxbn7x8cpc8qph1rq7y")))

(define-public crate-armistice_core-0.0.0 (crate (name "armistice_core") (vers "0.0.0") (deps (list (crate-dep (name "armistice_protos") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0k3nvk5v73qda6410ypfx47rnj9fpmgjhwnbm719lrdpg5swqi5k") (features (quote (("std" "armistice_protos") ("default" "std"))))))

(define-public crate-armistice_protos-0.0.0 (crate (name "armistice_protos") (vers "0.0.0") (deps (list (crate-dep (name "prost") (req "^0.5") (default-features #t) (kind 0)))) (hash "07kswwavvd3dw3j2lc1cq9lwayi8cc5gsx7rmi22f7b7x6n2d7cs")))

(define-public crate-armistice_usbarmory-0.0.0 (crate (name "armistice_usbarmory") (vers "0.0.0") (deps (list (crate-dep (name "armistice_core") (req "^0") (default-features #t) (kind 0)))) (hash "1bv68vzpkm1w6441jpx8ai40i8g2w7gyw7klj8sd2wwv0g1dmfcy")))

