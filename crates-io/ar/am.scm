(define-module (crates-io ar am) #:use-module (crates-io))

(define-public crate-aramid-0.1 (crate (name "aramid") (vers "0.1.0") (hash "01k89kmdymp0p0hwnr4h48ilf0w5jpmv18p8kp34if3lfknxlvzi")))

(define-public crate-aramid-0.1 (crate (name "aramid") (vers "0.1.1") (hash "1qx5mrbv65px7m2sk6m8w5n40k6q71crcjgd5lf02ncrdw5ygsmk")))

(define-public crate-aramid-0.2 (crate (name "aramid") (vers "0.2.0") (hash "0l9rrpalcg8zj5ydrfwml3lj7qlamlzgm1vvjf1ywm724ddcdpl0")))

(define-public crate-aramid-0.2 (crate (name "aramid") (vers "0.2.2") (hash "1fxb5hqlk8n7p4pnl3xcjnq8p42n2b61gl9k4dgzky9g04py82fn")))

(define-public crate-aramid-0.2 (crate (name "aramid") (vers "0.2.3") (hash "1b2k4dj819xikyd8385ds39id2hc3phznznvsh4n54nsx6b98mwx")))

(define-public crate-aramid-0.3 (crate (name "aramid") (vers "0.3.1") (hash "1m7s10al7yi2vps6dwd2imp477asl03m1r5nifghhriqkb7b8vy2")))

