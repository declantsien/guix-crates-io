(define-module (crates-io ar _a) #:use-module (crates-io))

(define-public crate-ar_archive_writer-0.1 (crate (name "ar_archive_writer") (vers "0.1.0") (deps (list (crate-dep (name "object") (req "^0.29.0") (features (quote ("std" "read"))) (kind 0)))) (hash "1yzhs2skyqqcl92873f02wmjqxnsxa3vk9awfipvn947lyaady0n")))

(define-public crate-ar_archive_writer-0.1 (crate (name "ar_archive_writer") (vers "0.1.1") (deps (list (crate-dep (name "object") (req "^0.29.0") (features (quote ("std" "read"))) (kind 0)))) (hash "0cdsm6g3rfw2gb5sppri4pkqybjdgrkgq7xai3msvzan0nc82s17")))

(define-public crate-ar_archive_writer-0.1 (crate (name "ar_archive_writer") (vers "0.1.2") (deps (list (crate-dep (name "object") (req "^0.30.0") (features (quote ("std" "read"))) (kind 0)))) (hash "1764k396k3dx3zh6n57p2yy3kcyw2cz42gpl5kxfpg1fw3689if2")))

(define-public crate-ar_archive_writer-0.1 (crate (name "ar_archive_writer") (vers "0.1.3") (deps (list (crate-dep (name "object") (req "^0.30.0") (features (quote ("std" "read"))) (kind 0)))) (hash "1fy9mk81xx78fb1vcjsbnrijl5ycd23xra5w3iyik8qpzm0r8qxh")))

(define-public crate-ar_archive_writer-0.1 (crate (name "ar_archive_writer") (vers "0.1.4") (deps (list (crate-dep (name "object") (req "^0.31.0") (features (quote ("std" "read"))) (kind 0)))) (hash "0k09gf5d6ipq2ryp9a9gq5c2sf5l41mv0pyb68i157m5h2cb7kvl")))

(define-public crate-ar_archive_writer-0.1 (crate (name "ar_archive_writer") (vers "0.1.5") (deps (list (crate-dep (name "object") (req "^0.32.0") (features (quote ("std" "read"))) (kind 0)))) (hash "0w9ypimlfrmqss1s6c502mwmbl3i0sd76lz49xzpwg8plmyd74lp")))

(define-public crate-ar_archive_writer-0.2 (crate (name "ar_archive_writer") (vers "0.2.0") (deps (list (crate-dep (name "cargo-binutils") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "object") (req "^0.32.0") (features (quote ("std" "read"))) (kind 0)) (crate-dep (name "object") (req "^0.32.0") (features (quote ("write" "xcoff"))) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "16ikh370y20xivq73hjzq9dz6zw4pysnkkx0mpbykrbg9f4nkhph")))

(define-public crate-ar_archive_writer-0.3 (crate (name "ar_archive_writer") (vers "0.3.0") (deps (list (crate-dep (name "cargo-binutils") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "object") (req "^0.35.0") (features (quote ("std" "read"))) (kind 0)) (crate-dep (name "object") (req "^0.35.0") (features (quote ("write" "xcoff"))) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0cvpyryv40w4p2andg230w4yilamxprk49asp9n3aqq6d4njlhgq")))

