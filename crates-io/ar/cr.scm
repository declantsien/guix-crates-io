(define-module (crates-io ar cr) #:use-module (crates-io))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.0") (hash "1gfcxp95484wfj80rci6b2q3i2rpgdds1dmjvny8da2vfbcyk1lc")))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "pyo3") (req "^0.12.3") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1v2pra3bf6ibw6hjpc6nijbgw9qa2n5c4vmq4nra6f4s2bv85zgi")))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.1-alpha.2") (deps (list (crate-dep (name "pyo3") (req "^0.12.3") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1qdb42gzw18pcf4wql6qfk023dng3cs06vhcnk9cbbbfywkm8hvz")))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.1-alpha.3") (deps (list (crate-dep (name "pyo3") (req "^0.12.3") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "04fr9wmg838lxa5jqyn306w2vm5zm88p757rwzs0rj4fdka8knj1")))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.1-alpha.4") (deps (list (crate-dep (name "pyo3") (req "^0.12.3") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1c61mk0wh2iivir0zhlrsj56pn1imb02rw73q9h0ckil969m5jly")))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.1-alpha.5") (deps (list (crate-dep (name "pyo3") (req "^0.12.3") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "0fpij488cib956binxjn1lqy5awingysfl8kkq723vf38vbr4f15")))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.1-alpha.6") (deps (list (crate-dep (name "pyo3") (req "^0.12.3") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1a0yaizg7ss6z0d0cm8514lhd2b1qwl2x81lyxgmflbzmb1i53as")))

(define-public crate-arcrs-0.1 (crate (name "arcrs") (vers "0.1.1-alpha.7") (deps (list (crate-dep (name "pyo3") (req "^0.12.3") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "0bq118qmg2xsl79an3c42vsfd50hmn68iz6mldp72mrxnp63arky")))

