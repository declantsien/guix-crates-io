(define-module (crates-io ar rr) #:use-module (crates-io))

(define-public crate-arrrg-0.1 (crate (name "arrrg") (vers "0.1.0") (deps (list (crate-dep (name "arrrg_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "07vsbf1j8ll63cnifx3z4dkf5ky2rlzrz0irb4r1jpq9pjwr4b4g")))

(define-public crate-arrrg-0.1 (crate (name "arrrg") (vers "0.1.1") (deps (list (crate-dep (name "arrrg_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gixcw3am0acf75mp0sdpcfcign4gnzvhw1zi51hy8s84mjzv3ms")))

(define-public crate-arrrg-0.1 (crate (name "arrrg") (vers "0.1.4") (deps (list (crate-dep (name "arrrg_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sg8gvqymzqbrbdpjjfz7zlzahg663b9ybiwscvmpjxhaldisdn2")))

(define-public crate-arrrg-0.2 (crate (name "arrrg") (vers "0.2.0") (deps (list (crate-dep (name "arrrg_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1g4zjpajswkl8y9knxdq94laqlq9ihbc7achl15bvxmzf08wp05d")))

(define-public crate-arrrg-0.3 (crate (name "arrrg") (vers "0.3.0") (deps (list (crate-dep (name "arrrg_derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1savjjjy0xf5rppd8h17jws965xm8b9j6ykpln2f1g7zf7ggdrgl")))

(define-public crate-arrrg_derive-0.1 (crate (name "arrrg_derive") (vers "0.1.0") (deps (list (crate-dep (name "derive_util") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "055yzqrr233qa8xzbgzd12xw675lsipnagd17173dbm6clznvsrj")))

(define-public crate-arrrg_derive-0.1 (crate (name "arrrg_derive") (vers "0.1.1") (deps (list (crate-dep (name "derive_util") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "158iiiriqxrj60krn6v1ggi8lynlzm428lr2f6id2y7qwdcrn8b7")))

(define-public crate-arrrg_derive-0.2 (crate (name "arrrg_derive") (vers "0.2.0") (deps (list (crate-dep (name "derive_util") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "10fj1jlrg6q0c08zvaqy8i5698pjijp4zc4f3nmxrq8qf2zf14zq")))

(define-public crate-arrrg_derive-0.3 (crate (name "arrrg_derive") (vers "0.3.0") (deps (list (crate-dep (name "derive_util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vj688kgx1q3yhg8n8k8vama9inr3zcymhs7hwacfr4g9xg9fz81")))

