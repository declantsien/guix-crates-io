(define-module (crates-io ar b_) #:use-module (crates-io))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.0") (hash "12m67fm10zsbfg4vysgkqy9aldxfa1aksa7v4a59m6yk4awrc6fr")))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.1") (hash "10w803cg8hv316fwp55wj58n6x4ipc9ylrgi9aqr7rqgs0bgb4qm") (features (quote (("hash") ("digest" "hash") ("default" "hash" "digest"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.2") (hash "1ahkrld4gpxgr5zhg9x9k164fjsanmkpvia5j6gqwyvm295yijsv") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.3") (hash "1xpfijqpl73vvd3gj8hlbjj3s0c4xcr7jwn8xbp7vy18fg4ynld2") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.4") (hash "1gng4248q2a7yls98m83d02lw1hjz17w3b8y4jgf96qcmr0qlavk") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.5") (hash "10zy60lfh2a8bc4cb7jhwxhg57f2794cydhy2bczqzg84w886gnd") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.6") (deps (list (crate-dep (name "rayon") (req "^1.5.2") (optional #t) (default-features #t) (kind 0)))) (hash "1zpp0hrhx22r6mi2qh362xhbq5x2wi90n4rha9xkjfzjlsx8nydd") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block")))) (v 2) (features2 (quote (("parallel" "dep:rayon"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.7") (deps (list (crate-dep (name "num_cpus") (req "^1.13.1") (optional #t) (default-features #t) (kind 0)))) (hash "1s90b1knzxvvlwdrp0mmrgprh9fr8aw9f37hsn6089faqi8y01mg") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block")))) (v 2) (features2 (quote (("parallel" "dep:num_cpus"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.8") (deps (list (crate-dep (name "num_cpus") (req "^1.13.1") (optional #t) (default-features #t) (kind 0)))) (hash "1d3qqs1c5kk9w78jbrhaankwhkdrjxj6h63c5726zkwvc66p93hw") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block")))) (v 2) (features2 (quote (("parallel" "dep:num_cpus"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.9") (deps (list (crate-dep (name "num_cpus") (req "^1.13.1") (optional #t) (default-features #t) (kind 0)))) (hash "1fx0q35d38nlmxwbg5kcb1p20j43spja2w10fipcwwh655v3616n") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block")))) (v 2) (features2 (quote (("parallel" "dep:num_cpus"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.10") (deps (list (crate-dep (name "num_cpus") (req "^1.13.1") (optional #t) (default-features #t) (kind 0)))) (hash "0zg24lc2f3dm9l95kxdyk36nvz4qfgfajymzhn6zqpy75qs08gcl") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block")))) (v 2) (features2 (quote (("parallel" "dep:num_cpus"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.11") (deps (list (crate-dep (name "num_cpus") (req "^1.13.1") (optional #t) (default-features #t) (kind 0)))) (hash "1kajv8x0rlrdvfxcmx76jc1fh887yaf1a1f5045r90fvk0hq5fvv") (features (quote (("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block")))) (v 2) (features2 (quote (("parallel" "dep:num_cpus"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.12") (hash "11h7xv18ag8aagpgc2yx2mwc11sy4bqy53lx9fxgrjp4gxizx6il") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.13") (hash "0rk2d5yil4f7j1rm063x5191x6g1x7bk4km9h1wr9gbnb6sc4d0j") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "hash" "digest" "block" "parallel") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.14") (hash "1593xrmprg5lccdjbwysc91ci01g9xinlxrn5b9k08xdswqjs28s") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("counter") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.15") (hash "13f8vd7v2q2s7amycnjf6l2cl24hdspx69a7lsg2m610ajabn5vx") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.16") (hash "0wya6m2n6aw8jzfirnj5kk6a6wfnfqjvz2rwm9rvkwl6sxzqmxqf") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.17") (hash "10ksx9nbzfvi3r33flll6hkw0mbjipxq77k4sy2j3c87yfik3cnq") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.18") (hash "0m37fr4wc4f79mnd8jqr7zlw9i8x64hh4qixqf94mxpz0mnzdg9a") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.19") (hash "1kfn5rzaakmhc4zwqgx2gy3vnwq4mm172fvvzw3dmys3wcilyibv") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.20") (hash "06b4mhmxycxqslxfmrizihh7yg137qwvkwd58vl5v0v9xi9m4aly") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.21") (hash "1fqjx32v7hrl0jy7s5rbkb910ag0yplbgps0ywsbl6xik6lw84ay") (features (quote (("parallel") ("hash" "block") ("digest" "hash" "block") ("default" "block" "digest" "parallel" "hash") ("block"))))))

(define-public crate-arb_hash-0.1 (crate (name "arb_hash") (vers "0.1.22") (hash "119w3qbfyikly1h5d7bcv3yckvcsr4yvw96sg92iqxcqrn61rvq7")))

