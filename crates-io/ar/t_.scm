(define-module (crates-io ar t_) #:use-module (crates-io))

(define-public crate-art_04104bdd-0.1 (crate (name "art_04104bdd") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0xcmcav5vv2azyw7cinmzd33qqj5jnzas4bi6x59n5h4im5fapin")))

(define-public crate-art_112144-0.1 (crate (name "art_112144") (vers "0.1.0") (hash "0njhl0frzqqqwb2hzbq6m5x286578piwyacgjfzxwk5ag46amrwj")))

(define-public crate-art_1978-0.1 (crate (name "art_1978") (vers "0.1.0") (hash "00rbb3ha6wcnzh2bgz5l7lb37h4qlfmrq766g2k5ywywkzx6b9ry")))

(define-public crate-art___-0.1 (crate (name "art___") (vers "0.1.0") (hash "0xjf0a0zq86p2i38nx4cyvvpds9460c1hcpwmxfl1md49j71pgw3")))

(define-public crate-art___-0.1 (crate (name "art___") (vers "0.1.1") (hash "1ynlk53v1yqyppii643zw4zbpldr93s6j872256vww9qb3hdbkz9")))

(define-public crate-art_benchmarks-0.1 (crate (name "art_benchmarks") (vers "0.1.0") (hash "0q0j2wd1k40wakijy7lhv8rnxdrdkc3974s04p4idl5f9dd0nyzp")))

(define-public crate-art_benchmarks-0.2 (crate (name "art_benchmarks") (vers "0.2.0") (hash "0356yy10kkhzg34gd2gzhk1zzbi1rlqzk9wdjarbap76qk0k47b0")))

(define-public crate-art_benchmarks-0.3 (crate (name "art_benchmarks") (vers "0.3.0") (hash "1igvpny76h0k71dgm9a4iw79lpz5akdw8r0vpm7rn5vi3gyd5zkm")))

(define-public crate-art_bs-0.1 (crate (name "art_bs") (vers "0.1.0") (hash "10qm9j1llg3qh1jj1dck7kcrc06wvzvvc1150xwz0b8h731qq7h0")))

(define-public crate-art_color_mixer-0.1 (crate (name "art_color_mixer") (vers "0.1.0") (hash "1ylcpc1xqqnlkp1jb2dvy8vpzznvdkzq9p9jsyp7gkpmzmqf5aiv")))

(define-public crate-art_concepts-0.1 (crate (name "art_concepts") (vers "0.1.0") (hash "1bby2dcrw822sk7ak1hki7cyjxnrv68ixxiqz4hazizs6mhqysnf")))

(define-public crate-art_cqmyg-0.1 (crate (name "art_cqmyg") (vers "0.1.0") (hash "1vf8c24mf6zzji21l3r9z9bpzpp55c7ysv653qn1n1jpng365z6p")))

(define-public crate-art_crate_substantial-0.1 (crate (name "art_crate_substantial") (vers "0.1.8") (hash "08qax8jk1hm0jy1s48lnlvhz1kf7kif814v50xzly56x00pc5525") (yanked #t)))

(define-public crate-art_crate_substantial-0.1 (crate (name "art_crate_substantial") (vers "0.1.9") (hash "1gzvhrlrl44jbijl9r1ms39rn2kllr7lnkb6az2i45y39dnill24")))

(define-public crate-art_crate_test-0.1 (crate (name "art_crate_test") (vers "0.1.0") (hash "1wsz1lv5zjrha9cg66pwy6ph8zwn6jcgmk7kxbm47kmaxv8hh4s8")))

(define-public crate-art_demo_dummy-0.1 (crate (name "art_demo_dummy") (vers "0.1.0") (hash "13s94qb7jnfgcygycia810dz41a2sl3ac9wf65mdkhpdqprd16gs")))

(define-public crate-art_demo_dummy-0.1 (crate (name "art_demo_dummy") (vers "0.1.2") (hash "1m6la51z17ngd84hash8ginmahdw9w28cylfl5bx0jbn6bj16pdl")))

(define-public crate-art_demo_dummy-0.1 (crate (name "art_demo_dummy") (vers "0.1.3") (hash "1k5h5cx6da8hrkm2z23yngl9awpfid3cfxxgsd9rkq03p0xpdn2n") (yanked #t)))

(define-public crate-art_dewjjj-0.1 (crate (name "art_dewjjj") (vers "0.1.0") (hash "1a9yl2l1bcfy3c9g5j61ndqizw20nb8ap993f467vvkyg59q66kk")))

(define-public crate-art_dewjjj-1 (crate (name "art_dewjjj") (vers "1.1.1") (hash "1pkjvz7c9hjyxqk2gp9iizgq5wwgi8gy4967h8agk9r1b1al36bf")))

(define-public crate-art_dewjjj-1 (crate (name "art_dewjjj") (vers "1.1.8") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1c7l1l239nmvr30drmg82zfrlz90wxi6kkplvqnch0csxzvc1wid")))

(define-public crate-art_dice-0.0.1 (crate (name "art_dice") (vers "0.0.1") (hash "1i6arx5chrwzzf7img1gr41682c9bncxcm17bhwprpl23c09p7bl")))

(define-public crate-art_dice-0.0.2 (crate (name "art_dice") (vers "0.0.2") (hash "1gs4pqljyi12cpv8p7cp5zkd93c29w9dcdmbs96g9i0zvi8dx9sg")))

(define-public crate-art_dice-0.1 (crate (name "art_dice") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1d9hp37ha4f6h61aq32n3gvcbvfv5d73gxssk04xx2rjrglrg78i")))

(define-public crate-art_dice-0.2 (crate (name "art_dice") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0i3hgj1wq202l0wpflddfjyy01myzvbz8vg90fxia9fdp9h2nq1n")))

(define-public crate-art_dice-0.3 (crate (name "art_dice") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0r7rzb815s0qpnajzn8qjinf0jhf42ypji1bixbcfgydvn414n2y")))

(define-public crate-art_dice-0.4 (crate (name "art_dice") (vers "0.4.0") (deps (list (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1ywlan3lksmm1qhd583qxsfmdnjlivb5sb2hq7jycalfwps52dvz")))

(define-public crate-art_dice-0.4 (crate (name "art_dice") (vers "0.4.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "04bqg4j6aw2c0pb80ax5n95w92i5xkrhqsx74jg9rchxsr2qfa55")))

(define-public crate-art_dx-0.1 (crate (name "art_dx") (vers "0.1.0") (hash "0jf1fy7jlwz9mql46wbjyzlm76vmy0w4n9yzm4y2lll8z317njpm") (yanked #t)))

(define-public crate-art_dx-0.2 (crate (name "art_dx") (vers "0.2.0") (hash "0s8h9w8gc7a6n5p6zfkhvpf8yrp12yy30q1yg4p59q2lb93i3zm7")))

(define-public crate-art_exercise_2019-0.1 (crate (name "art_exercise_2019") (vers "0.1.0") (hash "0yanjm89qd8hd2s692s35aqfa07apiixhgzizn2yb23j7lxry215")))

(define-public crate-art_game-0.1 (crate (name "art_game") (vers "0.1.0") (hash "09nqavy5bshz0p4mp0xn61x1akfgcw6y7rfmblcwibq2blgrg31s") (yanked #t)))

(define-public crate-art_gujinmal-0.1 (crate (name "art_gujinmal") (vers "0.1.0") (hash "1axx95i3h312c643rjdgg4gx8pqsl06x7z02zbdd1sd3rh5hxzkh")))

(define-public crate-art_gujinmal-0.1 (crate (name "art_gujinmal") (vers "0.1.1") (hash "0pwlk2wkc99wknhxj1dmvbz6d5hck9lrlmms38k74l7nsqmaf4lr")))

(define-public crate-art_hhh_phtbx-0.1 (crate (name "art_hhh_phtbx") (vers "0.1.0") (hash "030j0m59vksv745yzyhknr9j6wpc0yj1axxz98dmgsx8plava1p7") (yanked #t)))

(define-public crate-art_hieu90-0.1 (crate (name "art_hieu90") (vers "0.1.0") (hash "1fbi071s1q5z4lqn381k3yn2x3q6af95viakwanxx08802dzj8zx")))

(define-public crate-art_hieu90-0.1 (crate (name "art_hieu90") (vers "0.1.1") (hash "05nba83vf8n27vyajlkrbx8b2am89wmmmr41w6rzp0xnmv21qcrb")))

(define-public crate-art_koi-0.1 (crate (name "art_koi") (vers "0.1.0") (hash "1r5fafcksd2xwgr28i5kslk85qg8xir9j97m0srlcq8dp9830z7x")))

(define-public crate-art_lib-0.1 (crate (name "art_lib") (vers "0.1.0") (hash "18s4372zx8cci8jid4bf71yf32xfxgf1dcmppzd0nm89habz4k1r")))

(define-public crate-art_lib-0.1 (crate (name "art_lib") (vers "0.1.1") (hash "1zdga97c6ispw0ycr0bcy8xy4wzm8yvaf5g9q3pyak35vayhga1w")))

(define-public crate-art_lib-0.1 (crate (name "art_lib") (vers "0.1.2") (hash "0w7iajs175kb0dxhx4za57q8h08n26y2j93bqmilfdsjx6wh5x2p")))

(define-public crate-Art_lib_test-0.1 (crate (name "Art_lib_test") (vers "0.1.0") (hash "07llcj26jwiqq76zrbc4841n6gnm10xjr4zxjcrf4242y7fnggkk") (yanked #t)))

(define-public crate-art_mix_color-0.1 (crate (name "art_mix_color") (vers "0.1.0") (hash "1a9nr67p1wqf98b0h98bvyr06rhfk2l2zin4fbw47hpijmca7gcc")))

(define-public crate-art_mix_color-0.1 (crate (name "art_mix_color") (vers "0.1.1") (hash "03jsm7jpyfbn38yn4qyca31c7rhc4a50h5syfi4jahn9i7bqd20l")))

(define-public crate-art_nier-0.1 (crate (name "art_nier") (vers "0.1.0") (hash "1pp0hfkwbl6r9m27g17arr0661sxfd7wj7yzcg1inmc67hcj3bf0")))

(define-public crate-art_nkowne63_learning-0.1 (crate (name "art_nkowne63_learning") (vers "0.1.0") (hash "1zh3gzkk2sqzsggssr1amqwjaiz2d8n3263v2gr7646m57qw0h53") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.0") (hash "18f23f6ynlbnw3zjfdrbrxi6sxdkk05k1f5pqq9djfb06ll3ijks") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.1") (hash "0lipma8d1mnn9jz3x2kybczxw12552ss54ppw4r3hz3zi72fx28k") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.2") (hash "1mb54vvsnv2v3s8dg6p9mxqymrd7cbma36yjzpsg9zgmhnzms848") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.3") (hash "07f555vlsdfpr6n07qsi7ksc8vq6mkf3jbc2ffmqgzmhqy9yzvxn") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.4") (hash "1fx4jl4csdsvm2byy1k8b0s1w8jvwk3nrnz34kfcg7s324j9w9m3") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.6") (hash "1qf5cr11222w54kl1p5w8i80zy9by4xavgks67bbgw73ydl0adb2") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.7") (hash "0gdwgis53y6hgvvsdy21xpls5m9jdvycaahcv21c2mhj8v41h00l") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.8") (hash "0gda8hsykz300qn6dr7vrfmbh2pfda4ixamsiwz2pfchdnpf7143") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.9") (hash "1gfff19skzj0573s4fqwdc4awhz8c5xx1jf7193023cj8ys1wcrd") (yanked #t)))

(define-public crate-art_p256-0.1 (crate (name "art_p256") (vers "0.1.10") (hash "1qvkxsd2x9fd6dvyqsv6qxwzd2dc26z1g4a3hsk31k2avaggfrbv") (yanked #t)))

(define-public crate-art_p256-0.2 (crate (name "art_p256") (vers "0.2.0") (hash "14vamyw81nmfj52ykfwqjnvv50sxc8yw3f5k4sv3g1rdwmp60lkr") (yanked #t)))

(define-public crate-art_p256-0.2 (crate (name "art_p256") (vers "0.2.1") (hash "145q8p9bz376ghjv1f3pflmba1fj52jv7hzp0vaxnrkcq55xaf6i")))

(define-public crate-art_pk-0.1 (crate (name "art_pk") (vers "0.1.0") (hash "1cnnxw7qnipkq1z9zic05g3s49qcwz1avqx27s4gf8qjbvrkbbcz")))

(define-public crate-art_rust_book-0.1 (crate (name "art_rust_book") (vers "0.1.0") (hash "094hy72yjrdiy2hv950gv8wgarklbv919mrj8hw04rqsi470say6") (yanked #t)))

(define-public crate-art_test_ppppppp-0.1 (crate (name "art_test_ppppppp") (vers "0.1.0") (hash "03lk25whz8bmzd7l7kw7220mz6rdwzkdng0gw1mpiki9y8h5vybd") (yanked #t)))

(define-public crate-art_test_ppppppp-0.1 (crate (name "art_test_ppppppp") (vers "0.1.1") (hash "0shz6cysms39kfk9rdmgad0hnhhv29wwcd55aa259fnbxsaxl69h")))

(define-public crate-art_test_ryououki-0.1 (crate (name "art_test_ryououki") (vers "0.1.0") (hash "01xkm7kjafvwpq46ymrmyqr2iyf6z6kngpzdds0hmj9n0dnzglv7") (yanked #t)))

(define-public crate-art_test_smp-0.1 (crate (name "art_test_smp") (vers "0.1.0") (hash "1biwlqjklm6ma6i0mmza212xm09f2avn1669xqbrm3ps4jqzb7s3")))

(define-public crate-art_testing_adannup-0.1 (crate (name "art_testing_adannup") (vers "0.1.0") (hash "1zaxzx9ni4i7svr53rkn9x9dg2cw63jvyp3h3ahadirri3pwdw6j")))

(define-public crate-art_unhumble_ben-0.1 (crate (name "art_unhumble_ben") (vers "0.1.0") (hash "01zqa5i5m40jmyjpsq2c6y033nrfn0sl5f9yj6nmxdzfh97lgl28")))

(define-public crate-art_xl-0.1 (crate (name "art_xl") (vers "0.1.0") (hash "18qa1dm39a67y2aa6pbsysbrmdfs397g83cjc6x0b6k65azcn2z8") (yanked #t)))

(define-public crate-art_xl-0.2 (crate (name "art_xl") (vers "0.2.0") (hash "0f2r9y9ypigpk8md7d3klix7cdpyl8q7niiszq5nsgxl730z39d9") (yanked #t)))

(define-public crate-art_zhang-0.1 (crate (name "art_zhang") (vers "0.1.0") (hash "04csq2sqdmyaads8rglbfrk2in3ha4xn0q2ldphdv73ilkzkk8zk")))

(define-public crate-art_zhang-0.1 (crate (name "art_zhang") (vers "0.1.1") (hash "0bqf40wbqdzq8i2ds7359xj3ybjj6chz8yb71gdlrpqskkzi3anf")))

