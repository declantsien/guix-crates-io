(define-module (crates-io ar in) #:use-module (crates-io))

(define-public crate-arinc_429-0.1 (crate (name "arinc_429") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1mbmqd3387dz9r1c0fl1bfvlwiygwzshrmis4n2r8r9a6xw4527k") (features (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1 (crate (name "arinc_429") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0hijhw7y8w685ziswxxrc8ngvfyr9s4m3gd1246n2gcqf61nr700") (features (quote (("default"))))))

(define-public crate-arinc_429-0.1 (crate (name "arinc_429") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "15jgvqw5k96zzalpspxpj9vh8w1a01dnwynjm5hqkrjng82wv7yq") (features (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1 (crate (name "arinc_429") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "10zrjxgjmsf7y1kpq892qs32r7zd9hqwnbqcrsm0pxggqf158dkn") (features (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1 (crate (name "arinc_429") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0fnvfq2h8a8d857igzqrnyp3rvvqild965nmacj2g2g8igky3g8r") (features (quote (("std") ("default" "std"))))))

(define-public crate-arinc_429-0.1 (crate (name "arinc_429") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0bzyh09y5dhwn369sja20iyb582pis0jhf9xpjk202qyjkskq8d2") (features (quote (("std") ("default" "std"))))))

