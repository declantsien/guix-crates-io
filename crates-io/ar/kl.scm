(define-module (crates-io ar kl) #:use-module (crates-io))

(define-public crate-arkley-0.0.1 (crate (name "arkley") (vers "0.0.1") (deps (list (crate-dep (name "arkley_traits") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1ssyyaw0x9skn8s52nnaln04yr5rmdwiw2fwqyaydrd6a54530wr")))

(define-public crate-arkley-0.0.2 (crate (name "arkley") (vers "0.0.2") (deps (list (crate-dep (name "arkley_numerics") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "arkley_traits") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "18gpgmly87w2valri9y9cmjllwr885zhxbxl3bhyr4bysi1wvscv")))

(define-public crate-arkley_numerics-0.0.2 (crate (name "arkley_numerics") (vers "0.0.2") (deps (list (crate-dep (name "arkley_traits") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0bcs5nxg3yhf6lzs95c6pz69vzqmpji94wvmm7gfl7whlmm4lb88")))

(define-public crate-arkley_traits-0.0.1 (crate (name "arkley_traits") (vers "0.0.1") (hash "0s1v1rbkv8wz8k6jlw1slfv9nwc7h2q70w0jmgjpfn30yiirxyzj")))

(define-public crate-arkley_traits-0.0.2 (crate (name "arkley_traits") (vers "0.0.2") (hash "0wdb4mdvsw7l24xw18zzqs1r1zcsip39wjzczc5dq13g6201yvy2")))

(define-public crate-arkllm-0.0.1 (crate (name "arkllm") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "12mjfdvvv9a3r8w6fz9bdazxxp9hq1d5pwyfv9qhm0pm2pfh2miz")))

