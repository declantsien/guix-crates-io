(define-module (crates-io ar gd) #:use-module (crates-io))

(define-public crate-argdata-0.1 (crate (name "argdata") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "0y0xfg3bs1gav98zl9hqnrkyx72pang8a4yww030c603m6ihay6q")))

(define-public crate-argdata-0.1 (crate (name "argdata") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0ygps5gc36ibfbzi613fbc0marmxxws1y9i9h8jfyn1wyc5yqwp5") (features (quote (("nightly"))))))

(define-public crate-argdata-0.1 (crate (name "argdata") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "1yphhxbfjdl0lyvmz35q9xfzh9jbfqz4pxbj3mc0jw32j4ldawwj") (features (quote (("nightly"))))))

