(define-module (crates-io ar oo) #:use-module (crates-io))

(define-public crate-aroon-rs-0.1 (crate (name "aroon-rs") (vers "0.1.0") (deps (list (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "06s192kwpdghq299vpdgjg76b51dpi03i0rvm21304gmh830qi5k")))

