(define-module (crates-io ar ae) #:use-module (crates-io))

(define-public crate-arae-0.1 (crate (name "arae") (vers "0.1.0") (deps (list (crate-dep (name "loom") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1ybs31xzrwxy51sbwn1qrcwc8bb005m4vv5mr1ahpvdvlzhnrg1r") (features (quote (("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-arae-0.2 (crate (name "arae") (vers "0.2.0") (deps (list (crate-dep (name "loom") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1qg2ywjgdz8ky83pgl10jyp91jifk90qrqbwrcbcn9pj3pa76y6g") (features (quote (("default" "atomic") ("atomic"))))))

