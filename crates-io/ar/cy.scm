(define-module (crates-io ar cy) #:use-module (crates-io))

(define-public crate-arcy-0.1 (crate (name "arcy") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16") (features (quote ("macros" "rt-multi-thread" "parking_lot" "sync"))) (default-features #t) (kind 0)))) (hash "04a5xzs1l3z887s41l3r3wf5g9bfqski89c5sqm5w53xkfcgsvsr")))

