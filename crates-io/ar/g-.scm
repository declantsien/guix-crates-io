(define-module (crates-io ar g-) #:use-module (crates-io))

(define-public crate-arg-derive-0.1 (crate (name "arg-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "19q6nd5x13zm16jaq47c3zqz0wgsdwr3p3i8069zxbymafg18lqf")))

(define-public crate-arg-derive-0.2 (crate (name "arg-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "0vlb2sfi3zf59g3hsn933nbry2547ba482w3qb5sdrbv72bzhyv3")))

(define-public crate-arg-derive-0.2 (crate (name "arg-derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "1qz411is3an491ixigdmi8svmkyzdndx840r9540p1fkm8vx88a3")))

(define-public crate-arg-derive-0.3 (crate (name "arg-derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "1ikv3gajqpqpz9rh86jg4p5yx18mag3wi5pky95vjbhj02kxq0fh")))

(define-public crate-arg-derive-0.3 (crate (name "arg-derive") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "13gjcpk8ay46h3b63hbw5kza2hzx1myia54jc2sa6p238jycjyc6")))

(define-public crate-arg-derive-0.3 (crate (name "arg-derive") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "1jci9k1yjh1hp9wzc2cg0xypq704kzzq2y5r22nbd1asvdgkg9ak")))

(define-public crate-arg-derive-0.3 (crate (name "arg-derive") (vers "0.3.3") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "0jkynk7a5nxickic9x8mwa5v27m2nkzh1114ck187ywzm4yga7sk")))

(define-public crate-arg-derive-0.3 (crate (name "arg-derive") (vers "0.3.4") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "0sxhbkp7hgchx37x6a0yp0yx9awp33a12rk827fpv37yip9zdx7m")))

(define-public crate-arg-derive-0.3 (crate (name "arg-derive") (vers "0.3.5") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "0m0v9n3grl98wy50b6lhwcysrcw98wwy9ls24z69bglxsyl1agcb")))

(define-public crate-arg-derive-0.4 (crate (name "arg-derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "18ibyh3jwijsz5rzwc2247v2q8yfxhm0p8ydn3w92hxylxi22l3y")))

(define-public crate-arg-derive-0.4 (crate (name "arg-derive") (vers "0.4.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "0q1cki2awsvcx1w53z5vxfvg621ayq1ffkxz27lirr865hbgr7l4")))

(define-public crate-arg-soup-0.1 (crate (name "arg-soup") (vers "0.1.0") (hash "1d92r5khcafdhgyaz04kc6g62ds6mfa0xrxyrbxgbwzsg1vzgfby")))

(define-public crate-arg-soup-1 (crate (name "arg-soup") (vers "1.0.0") (hash "1n4w0qkfq9ws2jq40qgqidhl5i1a8aikbzmaws91rj423bgsd4mk")))

(define-public crate-arg-soup-1 (crate (name "arg-soup") (vers "1.1.0") (hash "16k76c51g00rplbkl8j5nh0x5blhmvpxy0xwgmn00j1j6kc628lc")))

(define-public crate-arg-soup-1 (crate (name "arg-soup") (vers "1.1.1") (hash "0vvc41zs4jr76shqzns948n6y11jah9gy7si1wrr2za0ah6jynzx")))

(define-public crate-arg-soup-1 (crate (name "arg-soup") (vers "1.2.0") (hash "16hdszci4wgxiqnxg7iy2zdfkmzrgyhhq33m4h8bzyq468z40mfy")))

(define-public crate-arg-soup-1 (crate (name "arg-soup") (vers "1.3.0") (hash "0llcj9fv5xxcpkcy7r2hhgg58c0sljh5xcnm2b1qvz6lgy9psclv")))

(define-public crate-arg-soup-2 (crate (name "arg-soup") (vers "2.0.0") (hash "0a3f8c7ymj2xgh3fc4prkm0yx1yfnbz851ry1f17394nkgygf7hp")))

