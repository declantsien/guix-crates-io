(define-module (crates-io ar m-) #:use-module (crates-io))

(define-public crate-arm-gic-0.1 (crate (name "arm-gic") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1vwlakhxc2dsphvy52mgmyi80af46c3g8d7fa5573la13zbhas6i")))

(define-public crate-arm-memory-0.1 (crate (name "arm-memory") (vers "0.1.0") (deps (list (crate-dep (name "coresight") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "enum-primitive-derive") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "jep106") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "13ld6mipc12hgzk9k8kckqz4ais6lw7pj61zwxrfks59yn4qpqz0") (yanked #t)))

(define-public crate-arm-semihosting-0.1 (crate (name "arm-semihosting") (vers "0.1.0") (deps (list (crate-dep (name "cstr_core") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0kq3l9nlzp0z60cl5i5x1q9qq4d3c77r3fib31mggnxha0la1prg") (features (quote (("default"))))))

