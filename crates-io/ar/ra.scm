(define-module (crates-io ar ra) #:use-module (crates-io))

(define-public crate-arrange-0.1 (crate (name "arrange") (vers "0.1.0") (hash "0vb9blfgs06j34b6g5albrj1zw3kg5hm3hirr1w4dcdbpx49xz3n") (yanked #t)))

(define-public crate-arrange-0.1 (crate (name "arrange") (vers "0.1.1") (hash "16na78dc47i0g4ng7jv74r0i2vrhxl5qa1qac8xfpnwd2h77rn0k") (yanked #t)))

(define-public crate-arrange-0.1 (crate (name "arrange") (vers "0.1.2") (hash "1ps8sw1dcdrcpmn7cj0l8656cj5kg0ylnp16z1vd352v8jfmf6lc") (yanked #t)))

(define-public crate-arrange-0.1 (crate (name "arrange") (vers "0.1.3") (hash "09zhvhmpq88b5lr9p8fbqvjxm0ivy97m5x00psi345jxxkw0sb37")))

(define-public crate-arranged-0.1 (crate (name "arranged") (vers "0.1.0") (deps (list (crate-dep (name "arith_traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "assert2") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3.64") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1sxaqybkbvwikaw2ffgaj9bgpfnq5pd3kqm4fnyz4a2ybpvjyxv7")))

(define-public crate-arranged-0.1 (crate (name "arranged") (vers "0.1.1") (deps (list (crate-dep (name "arith_traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "assert2") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3.64") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1cn9p1fdm9z5kqigvlam4c12m1bgd7hcz553d60yn1qmxmxmin2z")))

(define-public crate-arranged-0.1 (crate (name "arranged") (vers "0.1.2") (deps (list (crate-dep (name "arith_traits") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "assert2") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3.64") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0sabw0xwa36qfjlcmdd7ycs95gscx1n11z2wsv7yxpdi5rpyp5d7")))

(define-public crate-arrav-0.1 (crate (name "arrav") (vers "0.1.0") (hash "08wiidi6jm423nyxbqjxcwyb5jzsplg3b7pkqj89s5144dy3yc3d") (features (quote (("std") ("default" "std"))))))

(define-public crate-arrav-0.1 (crate (name "arrav") (vers "0.1.1") (hash "1zmsmx5is8jkb3ib029hi14xgv661aypj7hswpwk77aba98h1k8x") (features (quote (("std") ("default" "std"))))))

(define-public crate-arrav-0.1 (crate (name "arrav") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "packed_simd") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0hivljhr2d44vjd5rh6l1sr3sbzz397frsjln9g7dz21q6sk5069") (features (quote (("std") ("simd" "packed_simd") ("default" "std" "simd"))))))

(define-public crate-arrav-0.1 (crate (name "arrav") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "packed_simd") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1wg0s3ql6pcigbmq5c7ralb2ckzsdjyvc57xlcsszrnl7l92jsgm") (features (quote (("std") ("simd" "packed_simd") ("default" "std" "simd"))))))

(define-public crate-arrav-0.1 (crate (name "arrav") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1c75w775cz1mr7syfil8bksi51zppigbn9y00ij87wk6g9c8ghvh") (features (quote (("std") ("specialization") ("simd" "specialization") ("default" "std" "specialization"))))))

(define-public crate-arrav-0.2 (crate (name "arrav") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1pw0hhqyrydgvhb4arikygvmx5awxg322975l4fn43rlqk6lbyzn") (features (quote (("std") ("specialization") ("default" "std" "specialization"))))))

(define-public crate-array-0.0.0 (crate (name "array") (vers "0.0.0") (hash "0xivrmd1vakh16mhj5if6qmn545858jjwkl7vb8k20sbnpc0fwmh")))

(define-public crate-array-0.0.1 (crate (name "array") (vers "0.0.1") (hash "04gskadl6z0vbavsmk1n65yksvdd6jj3kmpsb9yjj02hwwg0a9xz")))

(define-public crate-array-append-0.1 (crate (name "array-append") (vers "0.1.0") (hash "1v5z43gr47v7avkw166rfgwi2fbjqvb6sxbs4g9rxfdmmfsk2mvj") (features (quote (("std") ("default" "std")))) (rust-version "1.62")))

(define-public crate-array-as-struct-0.1 (crate (name "array-as-struct") (vers "0.1.0") (deps (list (crate-dep (name "array-as-struct-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xwqipd6b4lflcp9mbigmiflpkwii82y1ikd7khmn6sz84yc5vfx")))

(define-public crate-array-as-struct-derive-0.1 (crate (name "array-as-struct-derive") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-crate") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing" "printing" "proc-macro" "derive" "clone-impls" "extra-traits"))) (kind 0)))) (hash "0vbdvadr67mkag453smybnq4rpq80ihi0qqsrv9f8hzb37zvmmrn")))

(define-public crate-array-bin-ops-0.1 (crate (name "array-bin-ops") (vers "0.1.0") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1najn97c6vjplrmsjzsn2grzyf30n4qj5akkjvwzbk4pqb0wl1m8") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-array-bin-ops-0.1 (crate (name "array-bin-ops") (vers "0.1.1") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "06vmrl5axfxmgjlri1j6ayd4dkpqzl7w3cwqibc6phjfpbgg7j01") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1 (crate (name "array-bin-ops") (vers "0.1.2") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0i5n2yd9s6kgbapygcvx84sq1a4dnvgikm79d2wxjfzkmwci9d8z") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1 (crate (name "array-bin-ops") (vers "0.1.3") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "048bl2apm1nl2641bs0vbad7aaq2ib76pxx4mdh5fy7ydi3438xs") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1 (crate (name "array-bin-ops") (vers "0.1.4") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1cmlkximkravijp0873mbm4fs27dv2l71q7k4dnaylmr2lx1il22") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1 (crate (name "array-bin-ops") (vers "0.1.5") (deps (list (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1y3y4s9yx8gzgb6jnxgawpk47v14pxkg3wbgz8f0xivmm79kn97i") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-bin-ops-0.1 (crate (name "array-bin-ops") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "mockalloc") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1xbvqr2ya3mfqwm1x689yipb09lcx8mqwz0cckq90vmi9msp0q1x") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-box-1 (crate (name "array-box") (vers "1.0.0") (hash "0r4wqsxmgqzn8ld9r3yjsscxw9gwbxl37jyy04dyx288s47wnd52")))

(define-public crate-array-box-1 (crate (name "array-box") (vers "1.0.1") (hash "1mx8gchzbdcf6zbadhmly5sl0r267shlglxl5zh4sqrdasjqvkpr")))

(define-public crate-array-box-1 (crate (name "array-box") (vers "1.0.2") (hash "0ylm9pdddip30117ixvw6jl435l0phykfcdxasxlkvq1m5f74amy")))

(define-public crate-array-bytes-0.1 (crate (name "array-bytes") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req ">=1.0.34, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req ">=1.0.22, <2.0.0") (default-features #t) (kind 0)))) (hash "1v33dvhsdhg3mrziw3whwxccg63gxxprhrwr8vy0p63l1482rn3k")))

(define-public crate-array-bytes-0.2 (crate (name "array-bytes") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "0dq605ww6a7zi34w40mpp0nvb1vzxrqw7226kxklfa1l8rsla6vw")))

(define-public crate-array-bytes-0.3 (crate (name "array-bytes") (vers "0.3.0") (hash "1pjikb8d0djc43s3c7ylns4ah0sfcx4qm95plrcpd8r9ril9yxa3")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.0.0") (hash "1amjyz3m839i61cyb4wi76kdby3m6s7ma3bkbipdfbhjmnf1kpx8")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.1.0") (hash "1dfmm6qaglx6i7xppi73bzv16aj7dcyh89s9w01ckjyfjlas22fp")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.2.0") (hash "1x5aflm92zjm8s785s4h302pvc0f7cww41sxlkkzwi1ywl1h9vfr")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.126") (optional #t) (kind 0)))) (hash "19nqh652jqq821rhc7mhc0whylzlgj186nbbvpgpjv3pdij1wvc0")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.126") (optional #t) (kind 0)))) (hash "0yzbwzc9fxdg5dgnvhkimi7m56vk0icr1sly1c6qhsz44bym82v8")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.3.2") (deps (list (crate-dep (name "serde") (req "^1.0.126") (optional #t) (kind 0)))) (hash "12bhf8y4f1jhfgc2psqv1fva8v5fvw7iwymahhwfb7rmdr0ghr83")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.3.3") (deps (list (crate-dep (name "serde") (req "^1.0.126") (optional #t) (kind 0)))) (hash "0xi7r8bmhlc6vhbxnvx078ghxcv4l6bfrin5pn5jw0dhnan44rdp")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.130") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 2)))) (hash "036jpqzaxr83lnc04ml117k1ilv301pqy7cflcf4npd1amxjkyy8")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "132bxyrkrqqcak08d4yz7d14gx7b42jf8k889zxz44swnjp89lls")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0hrw94c9n9a3wqhwamw65gk3cmkpr8j3yi5dyn1zsq34738hg2yz")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.5.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0wyrg2zbrh0njv07g7vj4s82qz00fk4rrl5fz3ik0n240p6a654s")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.5.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1avv52dfipgn11r966jg58yn3rgprcqzf2ahbn9a1ad47y6wg2xl")))

(define-public crate-array-bytes-1 (crate (name "array-bytes") (vers "1.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0m38j84ywvfrdiklp3d1yj0aj5jcb873jiciqlxdvg7yxgy11cn5")))

(define-public crate-array-bytes-2 (crate (name "array-bytes") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ly8fh4ml0693nc02mimh63f23yzkyvz9lmcbllhj83l167zw78q") (yanked #t)))

(define-public crate-array-bytes-2 (crate (name "array-bytes") (vers "2.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0vgvj7lcckd82mj2w72524z7dqwlldss3r97p5qva4gk3lh5swih")))

(define-public crate-array-bytes-2 (crate (name "array-bytes") (vers "2.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "00h3z1dfh8axqblxbffxy4ifsrljihq4y424mli4nnrawd6a5xxp")))

(define-public crate-array-bytes-3 (crate (name "array-bytes") (vers "3.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "158ysj629w43b349kpw5kbkx4y5c5kr95gxrw0x7wdbxaxspikfw")))

(define-public crate-array-bytes-4 (crate (name "array-bytes") (vers "4.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0kl2hsg7ia44y7llk4y5v7hjmw1hv7493npxr9r5l4xxhnmnsh9b")))

(define-public crate-array-bytes-4 (crate (name "array-bytes") (vers "4.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0j89jm87pz62r0m7x99ip9wfmswh1psra9q78nvyc8n9n0rkd4ba")))

(define-public crate-array-bytes-4 (crate (name "array-bytes") (vers "4.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1imnmw7cp9xj9il63w1klmhqj94qdavwiajynfj1csiiq72n6bzm")))

(define-public crate-array-bytes-5 (crate (name "array-bytes") (vers "5.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1af536fx5f395s2hhllb3mfay0lqzlrvbfl8lq52bf08z7ir1qyk")))

(define-public crate-array-bytes-5 (crate (name "array-bytes") (vers "5.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0vyhzyhrm41i9w4iqadqzsfn8ygvjp92wfdf8h7am1ly0mk2s8c5")))

(define-public crate-array-bytes-6 (crate (name "array-bytes") (vers "6.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "06fd0c02i6dh7w6dddrb566d523v34h0nizag03whjxcdyfjxxr2")))

(define-public crate-array-bytes-6 (crate (name "array-bytes") (vers "6.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1z80q9q12brg68xinzxycsd4xfzibjmr9gfzv2msac7ch6jcbcfr")))

(define-public crate-array-bytes-6 (crate (name "array-bytes") (vers "6.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0b1q62v2bxazfna7zwm845hmvffswbj4v9x1k76cbn2ajccsj5yy")))

(define-public crate-array-bytes-6 (crate (name "array-bytes") (vers "6.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "11na5k0qq2zrqsbwj5fa8x6glkk3zy3j0jzinx0nrzir596qv4if")))

(define-public crate-array-bytes-6 (crate (name "array-bytes") (vers "6.2.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1q4s8mz6a29jf0bnwil4spjrqnvdrvjjc354gvhwbksv36vhz13g")))

(define-public crate-array-bytes-6 (crate (name "array-bytes") (vers "6.2.3") (deps (list (crate-dep (name "const-hex") (req "^1.11") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc-hex") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "14w23czh8s1b5ic1vfczymf674jn1jwxkcmv0blijhfk3c3dwpax")))

(define-public crate-array-concat-0.0.0 (crate (name "array-concat") (vers "0.0.0") (hash "1i7labpggzb14gc2y6gdlky54npkkfy08saxn7n694a1qnfgxwa9")))

(define-public crate-array-concat-0.1 (crate (name "array-concat") (vers "0.1.0") (hash "1f66cxsj2a4lyb38lxjvz9qlrl82wf6si277nifrhghqcmncaqd8")))

(define-public crate-array-concat-0.2 (crate (name "array-concat") (vers "0.2.0") (hash "12anhzjp782d27yg81fb848xhkfwvvw7h7gig1yavvf471dwfl6j")))

(define-public crate-array-concat-0.3 (crate (name "array-concat") (vers "0.3.0") (hash "1zlc9pwgh160y1h1rwpz4dhf0lbmd22n9qsm0qyj94ii9pvhw6pc")))

(define-public crate-array-concat-0.3 (crate (name "array-concat") (vers "0.3.1") (hash "15bjjgcf86grk4q10vxrgv4nf4wbbakv1cbq4jwala0swmflpy1i")))

(define-public crate-array-concat-0.4 (crate (name "array-concat") (vers "0.4.0") (hash "1i2ki32i0ii5nd33dx04dl7r8krdaxq07l2baz1khw5zm4achnwn")))

(define-public crate-array-concat-0.4 (crate (name "array-concat") (vers "0.4.1") (hash "0v71p20adq2qsz5ci57n62whd84ifmq5wx40if6n7binxpa8y8fv")))

(define-public crate-array-concat-0.5 (crate (name "array-concat") (vers "0.5.0") (hash "1snppmqd642fwjsgqpp0jwl77prlwxajray06x2igkwfnlfslgyg")))

(define-public crate-array-concat-0.5 (crate (name "array-concat") (vers "0.5.1") (hash "0d4il92m2navq0niw8y7916gsh0ylyh330g5n720zx818gjgprvs")))

(define-public crate-array-concat-0.5 (crate (name "array-concat") (vers "0.5.2") (hash "0zbz9slqnaf70bmgi7nwsn8199vcz2xshzzgwp7hgg6cfbmhy679")))

(define-public crate-array-const-fn-init-0.1 (crate (name "array-const-fn-init") (vers "0.1.0") (hash "1v1cjlnr13r1kfipaxqgn60x521xgjwfr1lspi1vzbjbqpp5q73k")))

(define-public crate-array-const-fn-init-0.1 (crate (name "array-const-fn-init") (vers "0.1.1") (hash "1yxgsvzl2ijvw7sw1brjxwa7fa5sa2vldzzslrzl0pf093jqbjwb")))

(define-public crate-array-fu-0.0.0 (crate (name "array-fu") (vers "0.0.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1z9l9vycsdrssfws7wcq9y40nnkashk2lz95fqmbnfpwfariipvz") (yanked #t)))

(define-public crate-array-fu-0.0.1 (crate (name "array-fu") (vers "0.0.1-alpha") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0lqyc99mmnrlcrgrkfklg873v0wpqhym5vnrl2xg2papp2swmiqy")))

(define-public crate-array-helpers-0.0.1 (crate (name "array-helpers") (vers "0.0.1") (hash "04a04kp4jhfh7ngqhz3a2pmmnia0x4sa9nv5h7w5gyy8z437rjcm") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-init-0.0.1 (crate (name "array-init") (vers "0.0.1") (deps (list (crate-dep (name "nodrop") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "121ys9l8hyy6x9ylw0m3adfxrs32cj8547jnfjxqca42y4jrhy14") (features (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.0.2 (crate (name "array-init") (vers "0.0.2") (deps (list (crate-dep (name "nodrop") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0salmmba3wfz0l4270kpgflh83xrkcnbapqk1asr4s116n6m0fsd") (features (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.0.3 (crate (name "array-init") (vers "0.0.3") (deps (list (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)))) (hash "1wszqzaxwcfxcp5vz929i71lh5b079l89qsrdz3si0dfs1b89k63") (features (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.0.4 (crate (name "array-init") (vers "0.0.4") (deps (list (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)))) (hash "0wndg0qry887r61bk898irz61316a13q6y0j1wx0sikbhv5rwn13") (features (quote (("nightly" "nodrop/use_union"))))))

(define-public crate-array-init-0.1 (crate (name "array-init") (vers "0.1.0") (hash "0d3n40d0mhwq76brh2gy0k6x0c1l3d9j06rdbyzxmgkz5lsz0cd2")))

(define-public crate-array-init-0.1 (crate (name "array-init") (vers "0.1.1") (hash "1nv5qxchqrsgxaxrbx8mkyxdx9g4j68iyylcpmapy49xbqpvw2zk")))

(define-public crate-array-init-1 (crate (name "array-init") (vers "1.0.0") (hash "15fcfwi3c40pb2lyaizr26dvsfx4pnl5mrcily9ihsdnclyrh755") (features (quote (("const-generics"))))))

(define-public crate-array-init-1 (crate (name "array-init") (vers "1.1.0") (hash "09rxkf6ghawzjf4vdz561x9irqjy3rd9wzinkmddhf9x905r3v00") (yanked #t)))

(define-public crate-array-init-2 (crate (name "array-init") (vers "2.0.0") (hash "1ilfd4hja0i8gjm4n2p29ydcsaicin3w54750bkcavqp49acqib9")))

(define-public crate-array-init-2 (crate (name "array-init") (vers "2.0.1") (hash "15pmzssi4nxkqdb65pjmfgj0wc60imffwvj9qw4af8nw0l8dgdmz")))

(define-public crate-array-init-2 (crate (name "array-init") (vers "2.1.0") (hash "1z0bh6grrkxlbknq3xyipp42rasngi806y92fiddyb2n99lvfqix")))

(define-public crate-array-init-cursor-0.1 (crate (name "array-init-cursor") (vers "0.1.0") (hash "0w8g1vyl59vnk9v8abllm5j7fzyz38z6nd5l9651kz8gz8daw8gw") (rust-version "1.57")))

(define-public crate-array-init-cursor-0.2 (crate (name "array-init-cursor") (vers "0.2.0") (hash "0xpbqf7qkvzplpjd7f0wbcf2n1v9vygdccwxkd1amxp4il0hlzdz") (rust-version "1.57")))

(define-public crate-array-linked-list-0.1 (crate (name "array-linked-list") (vers "0.1.0") (hash "0yc0an48hlzqlh29g0hn5qznkzrn2hdbnajjxjsy1ynvxs5xm8np")))

(define-public crate-array-lit-0.1 (crate (name "array-lit") (vers "0.1.0") (hash "1dd8kgdx9j8f2c8bc53314sah5my0hgkz24x41njxvmr571s54g7") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-lit-0.2 (crate (name "array-lit") (vers "0.2.0") (hash "0bzha8db0j0dgmhjg4s1gnrw53d54r02dsb0akvswjiir8ar7pqk") (features (quote (("std") ("default" "std"))))))

(define-public crate-array-macro-0.1 (crate (name "array-macro") (vers "0.1.0") (deps (list (crate-dep (name "array-macro-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "16gv392zg04ai613ryn5jh5bp6d1dxyy9hglwgnkrpjcz80jhb8m") (yanked #t)))

(define-public crate-array-macro-0.1 (crate (name "array-macro") (vers "0.1.1") (deps (list (crate-dep (name "array-macro-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0awb5gc95dh5nizxs4x11h6a00028hfhalw9sqrk7gaf247j3bn0") (yanked #t)))

(define-public crate-array-macro-0.1 (crate (name "array-macro") (vers "0.1.2") (hash "1im2wg49h10d5kr9vqpqybw5n0g6n3yd1i57l81wix9bglkkcv5c") (yanked #t)))

(define-public crate-array-macro-1 (crate (name "array-macro") (vers "1.0.0") (hash "11z9jfmxyjd2s41qsrgi263vfir6f9dijgk6zpsnhlqz6r2gwmpg") (yanked #t)))

(define-public crate-array-macro-1 (crate (name "array-macro") (vers "1.0.1") (hash "11mrcgw5pd367dgfsc1xv26i9x5xsak8vmyhz2ak19dqadaxz5jj") (yanked #t)))

(define-public crate-array-macro-1 (crate (name "array-macro") (vers "1.0.2") (hash "1vs0v010rpcgq24434x52v0pbn29q8yl4l76q0n9ypi3vq01l6wb") (yanked #t)))

(define-public crate-array-macro-1 (crate (name "array-macro") (vers "1.0.3") (hash "1ljjd2iqja2k4229kyrhra5mb1g6vph9nfgxxhgjli7v4mxg6kvw") (yanked #t)))

(define-public crate-array-macro-1 (crate (name "array-macro") (vers "1.0.4") (hash "1pn41nrd6sbbgzw5nmkx6ga9pj7d3na00m692k1svrylfvflw0vx") (yanked #t)))

(define-public crate-array-macro-1 (crate (name "array-macro") (vers "1.0.5") (hash "19mdx2xlppnqwl6rhsbzylx61a0kkp2ql8q16195b7iga977ps86")))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.0.0") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "1sy5x4jdq33hf4qa7svnak3bqv9r8hxzifk2fp55hc60v1mlw7wp")))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.0") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0wac5di1npwm23fgldpn2ml76j55zb5ynpjfv6hi3401h3qwh22a") (yanked #t)))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.1") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "1cj2mys4am91gshphmdzg3618v40pbl13lyq5fpwp6ww8xv034g3") (yanked #t)))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.2") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0dggpz0z4j7la48qqx90fw99dcljflzr7g1wm9pjclyq8b3pp40i") (yanked #t)))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.3") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "18yqz8vfw4267vajm3smya8y254lc0dcarxc77jif1242ajcbp6p")))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.4") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0by1fy9fswwawi896wh9l5yhqzcar890rlfj43hzajdas5s0rkmj") (rust-version "1.56")))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.5") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "1x3wya9qxhmyv90r8fmq4c2x5047lyl00564sxrhphqnczx62xj5") (rust-version "1.56")))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.6") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0hk2kjkvpqwi6x67wr46f4fbscnvphdmj6f9byikgqrjs0hy0xmw") (rust-version "1.56")))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.7") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0ismw7vcf8d2g8kha051yxbbzxwps5hk8v8f4a1b7y1n3r79078c") (rust-version "1.56")))

(define-public crate-array-macro-2 (crate (name "array-macro") (vers "2.1.8") (deps (list (crate-dep (name "tokio") (req "^1.0.1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "169wzpzzxh71jpdip2a7mbika77zzr6ykb0f3pjfyrmli9hjq2i2") (rust-version "1.56")))

(define-public crate-array-macro-internal-0.1 (crate (name "array-macro-internal") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1r0qwspxw7vw0s65hskckzj82xz86rwrjz3v492vbv4n8jmn5730") (yanked #t)))

(define-public crate-array-macro-internal-0.1 (crate (name "array-macro-internal") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "03y1haaxrhk0dz34i82aig62ck9a5iylj1fjkzvavcwfdrxkcc1r") (yanked #t)))

(define-public crate-array-macro-internal-0.1 (crate (name "array-macro-internal") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "18r43d4yvy0slnvc1z10dirb6rq615v4lg9rjxada3j7psb7hrh9") (yanked #t)))

(define-public crate-array-matrix-0.1 (crate (name "array-matrix") (vers "0.1.0") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "19wgzrgqlh35ggsk7vk09p6j6rffhmza01ws7ii8v0d0v8n9nqqh")))

(define-public crate-array-matrix-0.1 (crate (name "array-matrix") (vers "0.1.1") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1figl75skabcc6bwxgj2zxn475zqkzf7593lr84lzk1d1qkamzhb")))

(define-public crate-array-matrix-0.1 (crate (name "array-matrix") (vers "0.1.2") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "19ny52fp7badivx8nddnaacvc4qsf735fhfvqmr8bjg48pgiv6bn")))

(define-public crate-array-matrix-0.1 (crate (name "array-matrix") (vers "0.1.3") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0h8vvd688rlbzkd652rahwf4wkhmsswach713y9ypmzqasfy6vi2")))

(define-public crate-array-matrix-0.1 (crate (name "array-matrix") (vers "0.1.4") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1jxcpmn59aqggwxrkhjhfvwbrcampf32r3vgfajz0pdlw0alrf5c")))

(define-public crate-array-merge-0.1 (crate (name "array-merge") (vers "0.1.0") (hash "1jq37qvv4z5w5rlpxpffxx7l1lm5v53sln68xxn2gpiyg146khg5")))

(define-public crate-array-merge-0.1 (crate (name "array-merge") (vers "0.1.1") (hash "05im9bj5c3ngkskxy9c3g21cgd7l90mqfwkrf3mbjw98amm75mqx")))

(define-public crate-array-ops-0.1 (crate (name "array-ops") (vers "0.1.0") (deps (list (crate-dep (name "rand_core") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1jn7l9gppp6kr9kgsy22sw5p9cn1jazmqxy296gjwiwz5d11i4fc")))

(define-public crate-array-queue-0.1 (crate (name "array-queue") (vers "0.1.0") (hash "017dg6q66r6nlmwz6xf4lpc74k6pz0dww9s87j3s89xmwibhb4vx")))

(define-public crate-array-queue-0.2 (crate (name "array-queue") (vers "0.2.0") (hash "0kl7qiw7ih29rqsgsk9khwqvb34vvqvaksms14x76k726wg0gcs7")))

(define-public crate-array-queue-0.3 (crate (name "array-queue") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)))) (hash "1m6azwm1skv6y0fidvy4cpill4wgwkr5hip3sbmqbqr9ppkr0f6g")))

(define-public crate-array-queue-0.3 (crate (name "array-queue") (vers "0.3.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d2ww8xii7gm2aglkwhqg9qaz1dbv4v918lqz1dgn5m2m8dbycc1")))

(define-public crate-array-queue-0.3 (crate (name "array-queue") (vers "0.3.2") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wa1rap2i4nv9r313s0dvkd2l4xs9z0wj82fgjdlarx4fkdflp4w")))

(define-public crate-array-queue-0.3 (crate (name "array-queue") (vers "0.3.3") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)))) (hash "088ww18ykrryd4sk4s9i3n1jsmb33k2fw1v6irkx95d2x9fd7nb7")))

(define-public crate-array-section-0.1 (crate (name "array-section") (vers "0.1.0") (hash "0cccwb68lishmh4q9c7rc9fi2drjv0bhbi1lpbwp0rl8bh3c37sd") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1 (crate (name "array-section") (vers "0.1.1") (hash "00hq18wraj0hyqc7mp5bgqzq93fbn0fylny45f4vf1m2xl34wjvp") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1 (crate (name "array-section") (vers "0.1.2") (hash "1c16grca66wzmgh3wh37x4graffc010n1gyd7ry581xwiahmi1rz") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1 (crate (name "array-section") (vers "0.1.3") (hash "05hccrv8j7vxkbhvvlspa8vs60v8n137p8nqxjkypdhzbjryliy3") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1 (crate (name "array-section") (vers "0.1.4") (hash "1m18kpxiqb4h3i09lmqq1wgp62n62wx60acyx50bny5mqc5b21jy") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-section-0.1 (crate (name "array-section") (vers "0.1.5") (hash "1cqan7grjw6g9s14w8rh983y1sbcvziaa017cdfibcanb12ajg8q") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-array-tools-0.2 (crate (name "array-tools") (vers "0.2.4") (hash "1j17kdvf4apwzkbw8bsdn7v3syjr0pc5d28dgd7llac395sb7bix")))

(define-public crate-array-tools-0.2 (crate (name "array-tools") (vers "0.2.5") (hash "0y4jr3rwbszb5bzrgywvgbizzix5m7sbp2gk8bgwbld67jmy0adb")))

(define-public crate-array-tools-0.2 (crate (name "array-tools") (vers "0.2.6") (hash "13hc2jqz8gdbypw7ji10a1x0jm52c210pj431wp1kifzpcyjqbmz")))

(define-public crate-array-tools-0.2 (crate (name "array-tools") (vers "0.2.7") (hash "0j1gw0inyx8pv4r7jahqdq6pqpknb7xv5g7273c7c21kshdvbfnq")))

(define-public crate-array-tools-0.2 (crate (name "array-tools") (vers "0.2.8") (hash "1gacfy7sj4x73kb626jp3qc5805j4nsmhrzph5p2hvlwxwwbliyz")))

(define-public crate-array-tools-0.2 (crate (name "array-tools") (vers "0.2.9") (hash "17mgybf8wjbq8adr97qcnnm1qm1sqw36ds4jkqyx7h8mk3ppwcfr")))

(define-public crate-array-tools-0.2 (crate (name "array-tools") (vers "0.2.10") (hash "1v9i7dlz8304qhcsziifhdb1rdh56j1xialvkighr5xkls3vhgf6")))

(define-public crate-array-tools-0.3 (crate (name "array-tools") (vers "0.3.1") (hash "01shhgzj2wpg598pns6py0092ys4775g45clrdg3ip868aklv35g") (yanked #t)))

(define-public crate-array-tools-0.3 (crate (name "array-tools") (vers "0.3.2") (hash "1dpldba2j03hlr6gaj9k01nfvzlxv71i9pzjbw5p5jr36cf3nq25")))

(define-public crate-array-util-1 (crate (name "array-util") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ycyck5b35w839kfsxnrhcljdr3v1wxdcx8gv7dvixxxqxywpdks") (rust-version "1.71.1")))

(define-public crate-array-util-1 (crate (name "array-util") (vers "1.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)))) (hash "19rnc5n0zkcyzf9my1bpx3abqsq1vbf9f8gmx6qaih8kkw9xak9w") (rust-version "1.71.1")))

(define-public crate-array-util-1 (crate (name "array-util") (vers "1.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)))) (hash "1mifri4zgy8av8pkvqb02c3z95b65d56hi1l5h5bj2cgvr29hl3y") (rust-version "1.71.1")))

(define-public crate-array-utils-0.0.0 (crate (name "array-utils") (vers "0.0.0") (hash "1yriza7pnqck6ykhh3ya8dshqnfywr450992nvdnds3hm05yxzb0")))

(define-public crate-array-utils-0.1 (crate (name "array-utils") (vers "0.1.0") (hash "1k145a9xsdbs9bm1cf5av0yk4gnd6gmmpr52snysi7hwchhh5dg4") (features (quote (("superimpose") ("splice") ("slice") ("resize") ("join") ("initialize") ("drift") ("default" "initialize" "drift" "slice" "splice" "join" "resize" "superimpose"))))))

(define-public crate-array-utils-0.1 (crate (name "array-utils") (vers "0.1.1") (hash "047qcqh1f3yz62waidchp4h30088krla2581vwp1kip98zb2gb62") (features (quote (("superimpose") ("splice") ("slice") ("resize") ("join") ("initialize") ("drift") ("default" "initialize" "drift" "slice" "splice" "join" "resize" "superimpose"))))))

(define-public crate-array-vec-0.1 (crate (name "array-vec") (vers "0.1.0") (hash "16vmbrq5hd0cl7r0rzb5r4p0p5kbnm7s13j1d93n7q3y6h5ryxg4")))

(define-public crate-array-vec-0.1 (crate (name "array-vec") (vers "0.1.1") (hash "0i958hgjzx9i9ss97ahbxlym0vyyg7frcdg7m1wvl90cd2j8qb8d")))

(define-public crate-array-vec-0.1 (crate (name "array-vec") (vers "0.1.2") (hash "1cyp7h8xq8ih976lx5qa55cxjsmgzcad6pb15sa682i3zkx5csrs")))

(define-public crate-array-vec-0.1 (crate (name "array-vec") (vers "0.1.3") (hash "0zwgjfrpplzk08wqqsl9xp7vhf8jlrf4lgyq40gjkmpy3k6lzr6v")))

(define-public crate-array2d-0.1 (crate (name "array2d") (vers "0.1.0") (hash "0j7vdcrqllc8rv8d2z3ck8z6z7dba1h588mqxqya3q2yvb8paz07")))

(define-public crate-array2d-0.2 (crate (name "array2d") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0n9c2nfwkrhyc84s6ls0m97l40fab9iirm1sby0wfzpli8nl415i")))

(define-public crate-array2d-0.2 (crate (name "array2d") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1s69ryvwpjysh0na22qdh4wcws76dq3qdc87if60dpg7sfwjsjr2")))

(define-public crate-array2d-0.3 (crate (name "array2d") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dy9pbn5wfi5prkslhpjwqhzfqbqbfadsx5df4n87ix9s0qks2br")))

(define-public crate-array2d-0.3 (crate (name "array2d") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rf0nf9h1rpbna9xxaipd93vmma7iy5nq8q7cm4rpzxip9155bcl")))

(define-public crate-array2d-0.3 (crate (name "array2d") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0x4zi30jv548ac7r0a23bampvy4whnmrbalps06pqnmzq6r9rcyq")))

(define-public crate-array2ds-0.1 (crate (name "array2ds") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0lci1cq1gw89crb78r9shbdz7ja2s60g006lnmdaym7b83g0ha3c")))

(define-public crate-array2ds-0.1 (crate (name "array2ds") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0qwbqs5gx08ps44pbhcmz9xqk64zhgvdi67ayjsq0pgq0zimasd8")))

(define-public crate-array2ds-0.1 (crate (name "array2ds") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0lxqljl2jl509x9h6s2gbfr69s3w23vrsx9d65zm8wa01f9zphhj")))

(define-public crate-array2ds-0.1 (crate (name "array2ds") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "14a1rcgi2hg4qlmgky4dkqj36xl8rvc4c6vj6647x2arlnb118w8")))

(define-public crate-array2ds-0.1 (crate (name "array2ds") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0jbnr32cavjfwjy39zn58iicdyc5aq6pzqckz706700drc8scxwq")))

(define-public crate-array2ds-0.2 (crate (name "array2ds") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1lfxcvpxkqlhwrmll1j94x88k0vkl32hhz1v401gzvd63r0f2mqg")))

(define-public crate-array2ds-0.2 (crate (name "array2ds") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1mz3r2gql163cp0d75dgaqiwi7mpdkmzmk72qii40hl95d08d89m")))

(define-public crate-array3d-0.1 (crate (name "array3d") (vers "0.1.0") (deps (list (crate-dep (name "lininterp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00w7r15n9cid1vcpvjvllddwv81jyz50xgdyw95745r0jc5zpdda")))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.0") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z6n93hrgpyaxp3v88ws1ip0cm27mxz555hg0z9flqdrws54dpqd")))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.1") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mahm95jb9xl1xs8hn3q83q3n3hddqwy6dd53rs6qxpy6jz1glcc")))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.2") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qkpyb1v9qgpjkv8bqvdhaldsz6d7x2s13ywrrgs21sl8h2x8wrj")))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.3") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hmgc5mj8lgwx79sx086233lw0nvxppvz1f98mg9zi8pvsa216bc")))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.4") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p8pa5b5ff37433pd3fi89k4jfdpmwi8ygkzzc16893ycxvvmf6w")))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.5") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "144lzq5ahjvi1sk14y8qbjxzg6yky8j2g58r9a2wbi884zkkf800") (features (quote (("std") ("default"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.6") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0l6cm6cq5nxhbs51l06k27cmrx39cw9lr7nxqn38pwswfvmf33cl") (features (quote (("std") ("default"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.7") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0x7yi4km0zmm7q53b71lb9k5bwvv3zcxvmdmi09s1zzi4z5bz8a6") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.8") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1x5k6blpk3lhsyhailw1k9x1alrsd24wrf7knv2rjjq7c2a6y4dx") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.9") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1b398qsb3b1hqpfhmc705jh77w0sag8da5m7dy045gincglvjk2f") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.10") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0qxdvdz8lpzyl7r16l6lfbpi5vw33jjwab4yzv7sbiizllwlcab1") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.11") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "05ahw8abys59grrgwzkdj4j5767vsmzm9zih04bjhcdhr3d2l3g0") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.12") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "18xixfvihflqrzzlckmmnzzbxql1mnx3cs543qrjrh53c9ifclfd") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.13") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "050ii8drb3m4vap8h881ba8lpmrjdwryjzj780jssx46shkni1gw") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.14") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "041ary3f1zpvnqkdrr35d2p1gp9gbvp4kwxilhx7b703b22k7f1f") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.15") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0fxahwzjg216n8194qklavay82m7h0sc7dv2sd23v9xka7lg0h5r") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.16") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1a6vqr04wrbqg14a6zmcmm1jzxa5ra2zs7mqsjl8jcfabk2fs27d") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.17") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1lbbihahd26n9yj7q6xdplldc66p5s9j3g927spwj2nphnr97yam") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.18") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "14im6bzc7a31anx3fps8mnz3qakmwk0pq2rqkyx6vajxjmsajah2") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array__ops-0.1 (crate (name "array__ops") (vers "0.1.19") (deps (list (crate-dep (name "array_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0pjxxa2r85nqv5xslac92lvqp9sl4bhp1ncmcvkzvgm467dj1mcq") (features (quote (("std" "slice_ops/std") ("default" "std"))))))

(define-public crate-array_builder-0.1 (crate (name "array_builder") (vers "0.1.0") (hash "0bzqjxlgmcciil81rkvjb1g35vq0ps3f0r06ziaj7djpv74ch9pg")))

(define-public crate-array_builder-0.1 (crate (name "array_builder") (vers "0.1.1") (hash "0wr53l5d8x26cf82d50i5fdh9sipgr89h0hqc2790q7mv6kq0a17")))

(define-public crate-array_builder-0.1 (crate (name "array_builder") (vers "0.1.2") (hash "1k0is4g1jf775a9hsp15m65xgm06yjq6qmw1i6g6y5l6p8648121")))

(define-public crate-array_builder-0.1 (crate (name "array_builder") (vers "0.1.3") (hash "0gr2xhgxxqkzydz8xs81m8v8irxpx97avk8dn2wphqm729yqpr88")))

(define-public crate-array_builder-0.1 (crate (name "array_builder") (vers "0.1.4") (hash "1j3wb6d0jng4llsviixcxqfax23ff2n84n2s7s0x3i5k96a8id21")))

(define-public crate-array_cow-0.1 (crate (name "array_cow") (vers "0.1.0") (hash "19nkwzr9dpgmx4c8msfa3pk6j3q4a3jkn7iv11cz14dzlx5ahwm2") (yanked #t)))

(define-public crate-array_ex-1 (crate (name "array_ex") (vers "1.0.0") (hash "1qnmcpq06pci9980djdcfyardyzpyq8pdy10h8avpr1v2lr21lpl")))

(define-public crate-array_ext-0.1 (crate (name "array_ext") (vers "0.1.0") (hash "10ayrpbpcm6wfwa6sl14fr74454h0hk8g86yw3y73nmi1pa7w41q")))

(define-public crate-array_ext-0.2 (crate (name "array_ext") (vers "0.2.0") (hash "1p9d2ljp3micj0qyx46gimjnmlif7ih4vaq7x5wdy7g7n17817zw")))

(define-public crate-array_ext-0.3 (crate (name "array_ext") (vers "0.3.0") (deps (list (crate-dep (name "seq-macro") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0q2bmhjxwv5m3rmf64rv6x6cx6p1lw5xqg5sbq66mwyi99dm4aag")))

(define-public crate-array_ext-0.4 (crate (name "array_ext") (vers "0.4.0") (hash "179yy5iyhzvzpy1mh1agv7k86r9rphq5gmzmi4rrrgr7ipv5pzbf") (features (quote (("nightly"))))))

(define-public crate-array_final-0.1 (crate (name "array_final") (vers "0.1.0") (hash "1qqgi4kc8dgp35cz5z9linv20x5rjr3x68rw292jrmr4vgn0w9yb")))

(define-public crate-array_init_macro-0.1 (crate (name "array_init_macro") (vers "0.1.0") (hash "1y6ldv6mcs4ia0mlv6ffm7bw331vwlwnf0bln48lh38j74c1s1vg")))

(define-public crate-array_init_macro-0.1 (crate (name "array_init_macro") (vers "0.1.1") (hash "0i7b2yxnk9xm4nwvml7cf18mnz8dylpf73jzxm9j59jxahiflbl7")))

(define-public crate-array_init_macro-0.1 (crate (name "array_init_macro") (vers "0.1.2") (hash "1jfp5dnw14bs7jn16901vsx0j4a3y13agzv234hv72gj813zw3s0")))

(define-public crate-array_iter_tools-0.1 (crate (name "array_iter_tools") (vers "0.1.0") (deps (list (crate-dep (name "array_builder") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1kj7q9i1i3d72a6crrpqhdm1q1hj028ippghi7q874x2wly9slk4") (features (quote (("drop" "array_builder") ("default" "drop"))))))

(define-public crate-array_iter_tools-0.1 (crate (name "array_iter_tools") (vers "0.1.1") (deps (list (crate-dep (name "array_builder") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0a1b7axnjfxjifbpx5yvcify98n1jd9id9vmwlrgqwmf6s8ddgkc") (features (quote (("drop" "array_builder") ("default" "drop"))))))

(define-public crate-array_iter_tools-0.2 (crate (name "array_iter_tools") (vers "0.2.0") (deps (list (crate-dep (name "array_builder") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "07hrgcb4ixrrfb75d6mxxm5iqb7d9wgbzwav0zcy6khgac7l19pc")))

(define-public crate-array_iterator-0.1 (crate (name "array_iterator") (vers "0.1.0") (hash "0rsdzzyyym45r4mfdf4pajigxh4mm6f7k1vfgjr0sph8w1rw5jkc")))

(define-public crate-array_iterator-0.2 (crate (name "array_iterator") (vers "0.2.0") (hash "116wxqz1d5dipb2bsmzrr6c6h1477mx2vdaa0vlhc868vhv82spd")))

(define-public crate-array_iterator-0.2 (crate (name "array_iterator") (vers "0.2.1") (hash "1wf3h6fkm99dvf6yz6b67qbj7jxlvpz8b7976qbqj3lcncvgs9dp")))

(define-public crate-array_iterator-0.2 (crate (name "array_iterator") (vers "0.2.2") (hash "08h9vp1g4qv06mhfxpw7481nwvxd0kjnrkrc4qalj7pyvdw5xgx3")))

(define-public crate-array_iterator-0.2 (crate (name "array_iterator") (vers "0.2.3") (hash "0nq48hsa5fz0jarpmc5wpcy5l998v38ab3gpal6qgk4hypa1kfam")))

(define-public crate-array_iterator-0.2 (crate (name "array_iterator") (vers "0.2.4") (hash "1plny6m13h3g1m5zfqnl9j92fjz7rss365fzkw62685v4mybjph1")))

(define-public crate-array_iterator-1 (crate (name "array_iterator") (vers "1.2.0") (hash "1j01x7lwgpb656vw0qawacpkgz8lfl6ylnqhz50ivz4jf439393l")))

(define-public crate-array_iterator-1 (crate (name "array_iterator") (vers "1.3.0") (hash "1v2rd0qs465ll7ay5lqik5zncz1zw78wn59b3jbqk87fcag3yg0n")))

(define-public crate-array_iterator-1 (crate (name "array_iterator") (vers "1.4.0") (hash "1kvyzxzs195fhi24a6zcjn4r3jsbpfw5hryxrvr00zqjh7bsjhzf")))

(define-public crate-array_iterator-1 (crate (name "array_iterator") (vers "1.5.0") (hash "0i6vxq4s4lbj49zw7yh7rz4nljgp2p3cp3s5jw3hf6qrvd34fwsq")))

(define-public crate-array_iterator-1 (crate (name "array_iterator") (vers "1.8.0") (hash "1qb62mypw9vb66sy1gvj7nx8zfa81qyaxzgx1x0rmbdgq9agg6kn")))

(define-public crate-array_manipulation-0.1 (crate (name "array_manipulation") (vers "0.1.0") (hash "19gbikr9l6zdhshq1n4w4i7k00jg0gjsax5wgdbzf45mkxqx2nmr")))

(define-public crate-array_manipulation-0.2 (crate (name "array_manipulation") (vers "0.2.0") (hash "12ls238f3jyba9jbgw08ydxvsycy2lcn864b4iar3gj6v3yq6qcv")))

(define-public crate-array_manipulation-0.3 (crate (name "array_manipulation") (vers "0.3.0") (hash "1qmkwq45qg7pjadr3pzxzfnav490fy63g550wyf6bdwfk5yhvl44")))

(define-public crate-array_manipulation-0.4 (crate (name "array_manipulation") (vers "0.4.0") (hash "01hvwv7ga1j74lay27r00ivp2jd7mgyfbhw1l9nib2zsp88nvbvk")))

(define-public crate-array_manipulation-0.4 (crate (name "array_manipulation") (vers "0.4.1") (hash "0pyvdsj88qk4z35ivlmnpm5m1768qfahm79r77pdgfdni1l7zr99")))

(define-public crate-array_map-0.1 (crate (name "array_map") (vers "0.1.0") (deps (list (crate-dep (name "array_map_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0rkagy4yibgnmyx0q78n78bf04fn1ljavawxwbqcmxrfk48i0sgq") (features (quote (("derive" "array_map_derive"))))))

(define-public crate-array_map-0.2 (crate (name "array_map") (vers "0.2.0") (deps (list (crate-dep (name "array_map_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1j5v56pny99n9saaq3b8sk8mkyiwky3428hv1qg6k9c8aiyf45fq") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.2 (crate (name "array_map") (vers "0.2.1") (deps (list (crate-dep (name "array_map_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0w6mxgzhj3rr04s2n7v9c5sc3r62zcwkl3z9m3vx9cl8f7z6i725") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.2 (crate (name "array_map") (vers "0.2.2") (deps (list (crate-dep (name "array_map_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "19m7r3mg542vfjc6mr4kwcmdfm3q7nz7dnma1rcsa5sc2xzs0iak") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.2 (crate (name "array_map") (vers "0.2.3") (deps (list (crate-dep (name "array_map_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0xhwnwnfjka2wrqp1kz7k9vpv8fkbcja6jzgfn3fg71inwgaa25k") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.3 (crate (name "array_map") (vers "0.3.0") (deps (list (crate-dep (name "array_map_derive") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1g17p2pwa9g0rlnmsap3jf8xc2dbf8wqij04q80kky72v31sc3kg") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.3 (crate (name "array_map") (vers "0.3.1") (deps (list (crate-dep (name "array_map_derive") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0a1wkr8g1jdca3rldh0y4ghzgplc8pv9xjz0aandj5pr6qjqv8fr") (features (quote (("std") ("derive" "array_map_derive")))) (yanked #t)))

(define-public crate-array_map-0.3 (crate (name "array_map") (vers "0.3.2") (deps (list (crate-dep (name "array_map_derive") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_map_derive") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0yzxl94j2bgphkvksm9yfyc613sjcqm6907kpva2kz06kjs6g00f") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.3 (crate (name "array_map") (vers "0.3.3") (deps (list (crate-dep (name "array_map_derive") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_map_derive") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "18lzya9apq1amphlhqifdb6qrmscj44b0phrr8wgp7isjb9nhn2q") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.3 (crate (name "array_map") (vers "0.3.4") (deps (list (crate-dep (name "array_map_derive") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_map_derive") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0yppypvrkzkvfa4xk9pcmxz7h04mj3sl8swx20lwd3kkid6vibq5") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map-0.4 (crate (name "array_map") (vers "0.4.0") (deps (list (crate-dep (name "array_map_derive") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_map_derive") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0vx7c3paxygd83rncl5rpjpq1zwb1n624c5rvndgkkrfkzv8m4fq") (features (quote (("std") ("derive" "array_map_derive"))))))

(define-public crate-array_map_derive-0.1 (crate (name "array_map_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("parsing" "derive" "proc-macro" "printing"))) (kind 0)))) (hash "03wg675djdmp9vfxvsq9l1nbz0n6clcda8ycdm1jg93yay6v0nrv")))

(define-public crate-array_map_derive-0.2 (crate (name "array_map_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("parsing" "derive" "proc-macro" "printing"))) (kind 0)))) (hash "0dnx4lingynhnhaf4slvixvmpvxmig8w5a0s8vk0ygp735r0p1hj")))

(define-public crate-array_map_derive-0.3 (crate (name "array_map_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("parsing" "derive" "proc-macro" "printing"))) (kind 0)))) (hash "0ls44zvdsmlj6fril19xadrgpmqp94li6gwb7lmgvbal2sp08z82")))

(define-public crate-array_map_derive-0.4 (crate (name "array_map_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (kind 0)) (crate-dep (name "quote") (req "^1") (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("parsing" "derive" "proc-macro" "printing"))) (kind 0)))) (hash "1innhalfamgwzvvyis41w8265c5fysccyxc9k0c8qn65bvlgb01d")))

(define-public crate-array_math-0.1 (crate (name "array_math") (vers "0.1.0") (deps (list (crate-dep (name "array_trait") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "float_approx_math") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "09mnnxzzy4f9n8mwsfkpv4y80hdqwf8j2392sibqcaj0qz17sxbh")))

(define-public crate-array_math-0.1 (crate (name "array_math") (vers "0.1.1") (deps (list (crate-dep (name "array_trait") (req "^0.6.16") (default-features #t) (kind 0)) (crate-dep (name "float_approx_math") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rfksx2wafcv7hxawyq2i98zd3n59vaaxgv00a34mn18x8hdk12f")))

(define-public crate-array_math-0.1 (crate (name "array_math") (vers "0.1.2") (deps (list (crate-dep (name "array_trait") (req "^0.6.16") (default-features #t) (kind 0)) (crate-dep (name "float_approx_math") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vf00ndh2389gqvb1ddwkh5588ya26zck1icz7kxdgd9709j8xzj")))

(define-public crate-array_math-0.1 (crate (name "array_math") (vers "0.1.3") (deps (list (crate-dep (name "array_trait") (req "^0.6.17") (default-features #t) (kind 0)) (crate-dep (name "float_approx_math") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1cs06znxrizrrmjrs7wrxc0hkvcwhmpj69nkbdax4gaz5lf1gcbx")))

(define-public crate-array_math-0.1 (crate (name "array_math") (vers "0.1.4") (deps (list (crate-dep (name "array_trait") (req "^0.6.18") (default-features #t) (kind 0)) (crate-dep (name "float_approx_math") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11zgv0gqvw0m1c94561b3g0my1qqrhk490w1kd4s7w6112i13jbr")))

(define-public crate-array_math-0.1 (crate (name "array_math") (vers "0.1.5") (deps (list (crate-dep (name "array_trait") (req "^0.6.18") (default-features #t) (kind 0)) (crate-dep (name "float_approx_math") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rn8i73wiq095paid05hhmw9cia6jmx7mara4zynlqv3jqby2ql8")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.5") (deps (list (crate-dep (name "array__ops") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "float_approx_math") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mqvlyaiim624a1naq1w17mpi87plk1fri88yxx7n4jsfnmncnv4")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.6") (deps (list (crate-dep (name "array__ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "086g17h9kifqcgly686hkkyhffznn69amzcxq03hb2f1hyfdpfjv")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.7") (deps (list (crate-dep (name "array__ops") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16lqki8dv47shdj7fsjvl18qzj3qp65fybgdbww10kcd5z52nw3r")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.8") (deps (list (crate-dep (name "array__ops") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19kcc3xawwf7pk5syqhgj0na8ax4s4ric5kgj6adyzl6qz0yqvzl")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.9") (deps (list (crate-dep (name "array__ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num_identities_const") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yzalqi9bdv4jc60z5s7h4ckx8j1xigyf89y5kmi5p9sxs342rjn")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.10") (deps (list (crate-dep (name "array__ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1rybw4zxdzxrksilqj37hw6h1rbzp78357wpm8lb3i36y30hwm1d")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.11") (deps (list (crate-dep (name "array__ops") (req "^0.1.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0hy30bagy36yl0pmkrsnk94wwjzri9pbxdbkzmni26xz4lvpk54w")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.12") (deps (list (crate-dep (name "array__ops") (req "^0.1.8") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "slice_math") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0mnjzlcdrjfcy3m63vvfgvbxzjv3zbdj6qv4kg0kc43svi7h1v6c")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.13") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "slice_math") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "00sdkhv8p7kv0rg52n37bnszsw499yfhdk1qk1j5hpgdybvgfh86")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.14") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "linspace") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0dda9rjdnmi806iwssbn20235n4nchv1n1s9hdc29ayyx24vijn7")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.15") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "173fv9lq10hns3dssbfy7g6bm7yxan7lgwsxp1kn2bqqalqz8fm7")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.16") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "16pkr0ffcz2819khb7y7hiyfp0kjhq3n201wj1m85ylfp9zymaik")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.17") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1mfyk25q69z9s10zqqnz36aqgircn0a9cs8hlvmi66g65798vj1r")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.18") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0c96q0lmqlq166ypdvn33205f5vl9mcs9nidk76rcivx8p8bdxmg")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.19") (deps (list (crate-dep (name "array__ops") (req "^0.1.11") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "054vjj1iva2vgbsz7n42bvgvgyf38s7c93zznlv9z1ffjjcgypc8")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.20") (deps (list (crate-dep (name "array__ops") (req "^0.1.12") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0f93b2m90md992llhf475qkfxirr6a5vpknd7bj91czilgnhzwbp")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.21") (deps (list (crate-dep (name "array__ops") (req "^0.1.13") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "10rc9b78kws5km2f1hhh06fizxn0i8dxj0jhn66f75ngcsf6yahx")))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.22") (deps (list (crate-dep (name "array__ops") (req "^0.1.14") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1i1frrdxp9qalba0vajvw4sq79ahph4axdvn3ha1b618f7vji0s3") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.23") (deps (list (crate-dep (name "array__ops") (req "^0.1.14") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "111m6jzkx53cx02l05s8lirkyyb7chy89d2hawjb5zwlbp0azasj") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.24") (deps (list (crate-dep (name "array__ops") (req "^0.1.14") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0d7za4l16hv6k7cix1nsm30p1q8mx71n0hd0hfpsvcdpwblkwqj4") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.25") (deps (list (crate-dep (name "array__ops") (req "^0.1.14") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "03f58qksfnjgv64258gv12riv0hly98xdyk92sdqycv1x7b1k9kr") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.26") (deps (list (crate-dep (name "array__ops") (req "^0.1.14") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "05gi7ljd1dry2jx853yc898npsw8pzraqgyz4r6whch1cc943g0a") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.27") (deps (list (crate-dep (name "array__ops") (req "^0.1.16") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0abclc3x8v6szxdp6v1wvjhb45v2vypbjqaqhn56mbwzb8ix657a") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.28") (deps (list (crate-dep (name "array__ops") (req "^0.1.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1hgzfva8iz34azxx27pdz19f55ys60rqc8vcv80zdc6bnfz30922") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.29") (deps (list (crate-dep (name "array__ops") (req "^0.1.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0y0vyarsgsg77y4fw3ailr5gxgkg423ckkkpvwhymh8a684mybfw") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.30") (deps (list (crate-dep (name "array__ops") (req "^0.1.18") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "00cm50ydk3mkl8fmjsi33v940q09d1s4g2wagz4qn9p4395anw23") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.31") (deps (list (crate-dep (name "array__ops") (req "^0.1.18") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "02lhjw6fv8s10sfldkaa1xdbhcbj370yd9iz0l7908x7iwgaz6ga") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.33") (deps (list (crate-dep (name "array__ops") (req "^0.1.18") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "1r87n5pcxwzwb3vvincv26jqik8iyq6jh33yz0lqk2kyccqbjqlx") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.34") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "19mx5cixhywq6qn99vadvaz5x3c067c60hjx2wnjkwmzy3fz0x94") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.35") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1zc7cndld5acdx20z4vsdqs5a8531l7wsbhxdns04cbhflg2ynhx") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.36") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.23") (default-features #t) (kind 0)))) (hash "0pz6ld0nr99gildz795dcyjw805j0z4rfl3dg1x677124v0klwkx") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.37") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.24") (default-features #t) (kind 0)))) (hash "0ym1ljkrqgfh6d3d6vzklq206icn5ayq118pfxs6yk77qmgg1z1c") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.38") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "02lh8anb7hc7i5k63m48ipmjavpcrc8mq366pgpmsm7731aq268b") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.39") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.26") (default-features #t) (kind 0)))) (hash "11fdv67virybsi5mnnf501j4rzjlqps02c0hwr926xy5y71yk1wi") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.40") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "0p890xgfxa8pdfg2lpan2f2pcj9s35w6pl6z6536qz4yivbv18gm") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.41") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.28") (default-features #t) (kind 0)))) (hash "1idwdwddbvx6ibh0a1g01jba7a67ligabh83a2dprl5pzc4jkrzy") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.42") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.28") (default-features #t) (kind 0)))) (hash "1pv4ad1vd53xi4bzc6vj322qnwpdv7p70v3hnx870dkj6zk7cqkx") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.43") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1kximp62dd3alm5476gkf5ryzd8i4lw9wfi6zcya45k5cfkmg7wm") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_math-0.2 (crate (name "array_math") (vers "0.2.44") (deps (list (crate-dep (name "array__ops") (req "^0.1.19") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_math") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "0iywfafc6pwcwlificvrbidj0fndclvfr8xb4vrlbdf3nslcdcv5") (features (quote (("ndarray" "slice_math/ndarray") ("default"))))))

(define-public crate-array_of_base-0.1 (crate (name "array_of_base") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "~1.0.148") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0.148") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "~1.0.89") (default-features #t) (kind 2)))) (hash "18xww4q7ki4mcdy5mfhax87p54jgp4y0268sh5b65vbyih9axmaj") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-array_par_map-0.1 (crate (name "array_par_map") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "10fswr0ginl290ygvrf47fwvlgn4wihndf62wrn7523dlxkqf1m2")))

(define-public crate-array_range_copy-0.1 (crate (name "array_range_copy") (vers "0.1.0") (hash "0kgsr163cbcwfwcd38a8a5wfs1va5zk0ywlh50h0c5gv1fi2vq3y")))

(define-public crate-array_segments-0.1 (crate (name "array_segments") (vers "0.1.0") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1mclkqqpf0qm2a498wsj6f7rmmn9v62q8rbvykf8k8pyb8xagzyj")))

(define-public crate-array_segments-0.1 (crate (name "array_segments") (vers "0.1.1") (deps (list (crate-dep (name "array__ops") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1mysh4gj86xybrrmsh235zc7ijq3832nbfw6sc9g244mymfnxk2a")))

(define-public crate-array_segments-0.1 (crate (name "array_segments") (vers "0.1.2") (deps (list (crate-dep (name "array__ops") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "16b50iihlim6c29110xlwcs1rk7a1jpb3w7x1frrs1dagarpg182")))

(define-public crate-array_segments-0.1 (crate (name "array_segments") (vers "0.1.3") (deps (list (crate-dep (name "array__ops") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0xs88qpqadjjcmga2niy8zgx6fis0g78wqciy4fxk2jl0hfym8i5")))

(define-public crate-array_stump-0.1 (crate (name "array_stump") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1fk9r6y9jvi47rl72l70jcmjbihbhrzk01b8l0smjjpwzbxns31c")))

(define-public crate-array_stump-0.2 (crate (name "array_stump") (vers "0.2.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1hmx5hih8hnllczj4vw5vxgfp5gqj6vax2df9gan1sjcs8708j8v") (features (quote (("indextrait"))))))

(define-public crate-array_stump-0.2 (crate (name "array_stump") (vers "0.2.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "09ym0z9lc3s35p25b392hvp0qga0kn6vj6d33axgakk6n037z2wv") (features (quote (("indextrait"))))))

(define-public crate-array_tool-0.1 (crate (name "array_tool") (vers "0.1.0") (hash "1d7z930r7km53az3gfxlaqi588lds9rmvdpwfm320bd2k4i25xpx")))

(define-public crate-array_tool-0.1 (crate (name "array_tool") (vers "0.1.1") (hash "133z4r52jxbxc5y5zv8hpdgznkzy1hndifgkxh3wwyb602kdxsbr")))

(define-public crate-array_tool-0.1 (crate (name "array_tool") (vers "0.1.2") (hash "002laqvbvh71m0ba0hfip0y5xqb5cfnz7v2cjlv6n0zdjnjvi2qj")))

(define-public crate-array_tool-0.2 (crate (name "array_tool") (vers "0.2.0") (hash "1pb2a86qic0gzz20b6lw00fdys67xav31f6nb9l819rqs5vr201s")))

(define-public crate-array_tool-0.2 (crate (name "array_tool") (vers "0.2.1") (hash "0gykcl7mq28j6r8hk5v32rc7mr3g2gkrx9pg7ipci7bwdl4mskga")))

(define-public crate-array_tool-0.2 (crate (name "array_tool") (vers "0.2.2") (hash "087imkq7n626w74qcg2w939l7czpgcrl2ma6vxix9ga8pc91v689")))

(define-public crate-array_tool-0.2 (crate (name "array_tool") (vers "0.2.3") (hash "10l08r16n6ifq5kx4905yhzyv50z7hqmkind3vw0b22nfzaki74c")))

(define-public crate-array_tool-0.2 (crate (name "array_tool") (vers "0.2.4") (hash "0l9bmny6qba4fqlk96bb0vqfk3f19ndinwrbn1yv46r7i292k1ga")))

(define-public crate-array_tool-0.2 (crate (name "array_tool") (vers "0.2.5") (hash "13syqmrd21zkkf65w5z5p3gpqhy1yk11l45fdkgzmqkak06077si")))

(define-public crate-array_tool-0.2 (crate (name "array_tool") (vers "0.2.6") (hash "03v70zfmynbqabr998ig61ks1q3hv0p6rq3k7k76vf6knqbl568l")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.0") (hash "0v5wiwbr37p5n99jc6dg4ldnxv2pvlln10ihc2v1l9zy7bffy26j")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.1") (hash "1kfnmkdhjlyzyj0hcsnsnrbh0clvgxdh81w49l4z6ahzxgapn9kx")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.2") (hash "16zpklsc8jkb93587r2xx9vzv600hb6bm5v611lv8gdy7lz3zv56")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.3") (hash "1s4n15l9r0639nfyq6dyzfky4mpikagdpgw2rx4n3l5qb6s8l9xl")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.4") (hash "0hhk17bqyk3d4qwyqh4fkz00jq845hp91hgw9lxr46gyfycnxzxa")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.5") (hash "10iq9x3p2ayyh5wrjjd387sm302aviiba0bf0ws3l4zy241fln3l")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.6") (hash "0r01fris4npafiqdpn0nz0rw6p1y280r51wxnz8008d22842xyik")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.7") (hash "0rv7lcfm06zhymkxsiavrgsvvfiajxl6wjm4ij8fsrz1248kv94z")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.8") (hash "0qiyns38cwqk8ikxkhi75lp9visw67lv9nz3lz0sd87nghq6pvqh")))

(define-public crate-array_tool-0.3 (crate (name "array_tool") (vers "0.3.9") (hash "0r8dywmfw2g461mi2azcb02s737sl2f3ix9mi5vs54nj824wvhl1")))

(define-public crate-array_tool-0.4 (crate (name "array_tool") (vers "0.4.0") (hash "1jkygd4l0q4ni1kjqk5053c8a0sr9ksrq056r9ycrl8z4zs2gs6l")))

(define-public crate-array_tool-0.4 (crate (name "array_tool") (vers "0.4.1") (hash "0hj5p3sksaplrsb21w2xvfvwsghvlydyk674k7d5q7k1gjrcq1qs")))

(define-public crate-array_tool-1 (crate (name "array_tool") (vers "1.0.0") (hash "0q8yr8dka959mx26jzd0v8ig67bnksybxqszv70yqf1ahmilml64")))

(define-public crate-array_tool-1 (crate (name "array_tool") (vers "1.0.1") (hash "16v2pd1k5n3wgx1adfwhzwr9jm0cfy4xsd2v05pkg8nq0qgbi53q")))

(define-public crate-array_tool-1 (crate (name "array_tool") (vers "1.0.2") (hash "0j3ll7qgi9p68d4l71f8wzmnkc8zifzyrdl67dbqqmrd1gacm8m6")))

(define-public crate-array_tool-1 (crate (name "array_tool") (vers "1.0.3") (hash "0wg208lp6y061wndx35djszc9018zy69f92g7j36lr7b2kcbb34g")))

(define-public crate-array_trait-0.1 (crate (name "array_trait") (vers "0.1.0") (hash "10s0shllqcp2nzwq039iwqa6my9bw9afhf5qa6w0lcya90akp8s1")))

(define-public crate-array_trait-0.2 (crate (name "array_trait") (vers "0.2.0") (hash "029j7j9bwq92i41n65n3xrnnpm3dmr5sa6m8b28v2gpcnn8c0h5s")))

(define-public crate-array_trait-0.2 (crate (name "array_trait") (vers "0.2.1") (hash "0cbcn9ik2rbmclp2njjkz5n7gz0f71iigagdbfj8q33zjxv7jh8n")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.0") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kss6rjh3ydmd1f1vqzhl1mpswy5m9d5lmgmmfpqs8s5gq784m8i")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.1") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kg129iqhni4mj8c6bzrpcmc6xqrlcqjlccghmk8p2zmah3y3yhh")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.2") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gqzq4apd9qzdb5lpmzc2sx4yq8nf54pzrl7sncyzlp7gnzq74ja")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.3") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15pap9dc1jl8lvxr0m4hnlfbmvw5kr876k55gp5v1i9b484wjl0m")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.4") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17s858liv395q4w7yibk1adjia38k6dxvqdgxd416ip0fw4m2kfi")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.5") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0k4fdwagv9a7w23pg67q7ddpf5zri4fdq4jszb3m62ganjh1b1nf")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.6") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xbhqyqakhhm190rsrl56f7q7bbiwvg2nfwrd0lm6br138w9qmkx")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.7") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vva9vvj0jh2f2m4s178dxdabhaz5i9li5mg2riigaj6d8l1967m")))

(define-public crate-array_trait-0.3 (crate (name "array_trait") (vers "0.3.8") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y6syvvknb3a0d0ayashiwzzp7l3vd1n4qvn4biys30sij6kimyr")))

(define-public crate-array_trait-0.4 (crate (name "array_trait") (vers "0.4.0") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bfkz8ykdjn5pa4l50la1a5n80kcvpkl4iizqq1d6xcrc7izxzkx")))

(define-public crate-array_trait-0.4 (crate (name "array_trait") (vers "0.4.1") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0139yrdh14dff14s5ir9hh0arrkgrfylarajkaik4yfm10hal8wn")))

(define-public crate-array_trait-0.4 (crate (name "array_trait") (vers "0.4.2") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hp45p08dvs5vscgwc261xivxc8yg3kswppx39y1d5gqj7zixsy5")))

(define-public crate-array_trait-0.4 (crate (name "array_trait") (vers "0.4.3") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1s38q88af4qs4r0siivqlsavjybzwkfwwxl4yw3pizhzl546a96x")))

(define-public crate-array_trait-0.4 (crate (name "array_trait") (vers "0.4.4") (deps (list (crate-dep (name "moddef") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1r0vlv9s93i9rqzcc62wq0pjdckn6rvpym48pszs4wjjvjpqivgq")))

(define-public crate-array_trait-0.4 (crate (name "array_trait") (vers "0.4.5") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1bf82ydvia1545bp68xv9dy33lyydmps32msabjg3gv9ilhnrk7s")))

(define-public crate-array_trait-0.4 (crate (name "array_trait") (vers "0.4.6") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "13rf1hqxqk0fgdchdh2npb728qkyjz15x81rq08niz0kagcwi4a5")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.1") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1aydqxa0g3yvi3zd05dq9d390b0rgz13iccnqznfyl6125d17kiy")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.2") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "17w9hp32rigilj7xqa75fh21fd498ifaf839ypg4y58dxilpd9rl")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.3") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1byxjbs7iwz1am8ln74r6lgcijirvwm8l0byif08rj9gak1d1xp9")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.4") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "09cq43fqg1jpfhml781069chlynxnjdmds16h02pk1cwb1x9jxzn")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.5") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "10xr2mmi2j1yvdc1ypqz8f9rw4kcarkhch5cmz2silbvn7d24205")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.6") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0g79yn7zm43js9l58lrjr9cga7cmsx678kkgj4blb4s83pqafrzg")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.7") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1lh8sn429k992ipy5b3f3vrq4gxbhiy39c8rqfqhncn5hg062fym")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.8") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0kwp5zb04dfcr12qap1hvssjaw67fskbwvafx49kkhimcyhdfpd3")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.9") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02s0hbign3g3rqn8j0gih8w2vlf976zf1l0z6dk2a656bfp5p81p")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.10") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1indyg2g9jhkcgr1778lcwsv4c8gwvsgs0723q5qrhahdvpngvgv")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.11") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "03ks5m72cgnqi05lah0la5wf9b285n1p478bm5c1h34cc9rhni6x")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.12") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0i9pkii4cs8jbn9f8b6chfpz79kk41hhi15kbhsh94rk9mdlx74y")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.13") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "05dl2di7zgzxx68irfbsyd27v4w4016lraqgzj8iby19jb9crqav")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.14") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0nvxn79izkdblnlw5gb9hnaj9w2yxyhrf3lilzmdmjmca0i75yl0")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.15") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1ldd50snwv9da95lsv7fi4p6hcaaxzmgxr6wzbyq4din5naxlrmd")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.16") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0sf9mhrn9qs7qj3p2jrryyii420rq2rlq4ifjkjjxjw2kpfnlclx")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.17") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0lmkf1hy9fiallp78q0g83534sqadh4pl6jcig0xbiphic8r725x")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.18") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1hn3rxfsnq243w65xhayp6dwdaq218597al5cvb95qh4zznh4gs9")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.19") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0da8v1nha0qh9k3rdivs6hlxb9hdacbaidf2vzxvx0i3c36ram22")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.20") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0syqpzka29zvmjjls0f2n0a0yd60vavcg9jb619v5zvgv6rg7s70")))

(define-public crate-array_trait-0.5 (crate (name "array_trait") (vers "0.5.21") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1311ic3p8f5w98sa9x4szicd6jx7fc1n1gk2sr0vl13y7mscndjj")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.0") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0vc4m2kz5dppj0mzmxjiayc4s32izwsw977w12zgqxj1rz5dsxyy")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.1") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0nwbajkip73w3gsadwj7xvs4gsmz61yiqsipcmgm5a2cp69ifh2n")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.2") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02rkhxhdydcgcr5m8p7kvsz3g1k9z51xza0x9lc40d0fmbwlzk03")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.3") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "03msyzxln1q5rw3bipx0fi87ccgmdi5kkj91znplnkk5973s6rp5")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.4") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0lqrj5lxyvl7brycvib5wq3r0cpn2dawwfp2ygjq64ffhxnk4074")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.5") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "06g75aabjvy4a5h1kvmyirnrfzlxgps5xsjv5mwilx2941n9bcyk")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.6") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1fm8rcn32925918kcvhwfx1vr1xy5prk022x0rg0x102jx7b9p5k")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.7") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "110jr6pl6xa6l2ji0prfl9saph8qs8gq0mndx98r5jjzpdspfh4f")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.8") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "017xazpf63kvf7q5qx6b1hy9qmv8s8xi1scc35mh5z9d8bvg58n3")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.9") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "16fgwjyg7jgmx91pab47xa1p3cv221nc1mkap4qn74c5lil524b3")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.10") (deps (list (crate-dep (name "float_approx_math") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1hdyv32bwrxwdf9yh69virhx7pjjyiwmjw2fxcj27jyznsdy4cc6")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.11") (deps (list (crate-dep (name "float_approx_math") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0aayan0x4zglywvil4sa1c6mljxl4mdapckc1y6vi1wlzlqv228n") (v 2) (features2 (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.12") (deps (list (crate-dep (name "float_approx_math") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0ysfiffhdcd35bpq6gbzx6hx5d23209n2rvzqicfg7v7kqlxmrc0") (v 2) (features2 (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.13") (deps (list (crate-dep (name "float_approx_math") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1zifc8nhnmx1bghmqq6k8l31sqvs70wgd0yxqklj2cvflix3ssgq") (v 2) (features2 (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.14") (deps (list (crate-dep (name "float_approx_math") (req "^0.1.2") (features (quote ("sqrt"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "121lxja8w9fy04v7xk5md1cc9xn8j1p6pdx68a5bp562y8nj2b1h") (v 2) (features2 (quote (("float_approx_math" "dep:float_approx_math"))))))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.15") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0ghzn39360xyby11ka469sndaadfkdjm4fvys1r2kis5v3gbj1x0")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.16") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1zcsl5m2rq74bnn5kc6b6c40prw0viyzjcbly5agm1ig0sakn8vq")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.17") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0nk83yblm1x56y6pqj24c0vf0p9ryypbycmfpj88lijnfyls6rjg")))

(define-public crate-array_trait-0.6 (crate (name "array_trait") (vers "0.6.18") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0qg3axymc5maw78c5iplkd3cizbmw72y3mr0bin913ass8n6a49c")))

(define-public crate-array_trait-1 (crate (name "array_trait") (vers "1.0.0") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ybqbpvplbkfymrf7ybi7x0dz95i71d5a2z5bikd7z3yfxmbk6xz")))

(define-public crate-array_try_map-0.1 (crate (name "array_try_map") (vers "0.1.0") (hash "0as5v5nhq8f1wmayi8195n3r413dhjw0dj57gmq1s0i6j5vscmvl")))

(define-public crate-array_windows-0.1 (crate (name "array_windows") (vers "0.1.0") (hash "12lgg673jc6slg2cmmi37wg4crpgbjfscdx4fy07laa9mj27v5al")))

(define-public crate-array_windows-0.2 (crate (name "array_windows") (vers "0.2.0") (hash "0v3xm7l92b7ifhq4iw3bqz4xxmc5cwk7bgf13q888jkl120n0fwh")))

(define-public crate-arraybox-0.0.1 (crate (name "arraybox") (vers "0.0.1") (hash "08m8wskav3sxpnm2ll1yv0y7xr5c1vxgwayymj5f6j7s82n0k5lh")))

(define-public crate-arraybox-0.1 (crate (name "arraybox") (vers "0.1.0") (deps (list (crate-dep (name "const-default") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ah2scjmc00y7z4jwj5grf3rg8fimc258csg4k6dh15z1zzb4vqi")))

(define-public crate-arraybox-0.1 (crate (name "arraybox") (vers "0.1.1") (deps (list (crate-dep (name "const-default") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1kikb85iihrn6518krvr4cvmssj40vlxz38aipkakgpj06m945iv")))

(define-public crate-arraybuf-0.0.0 (crate (name "arraybuf") (vers "0.0.0") (hash "1gl9s039z5yip9iq0gkwnk9dgawjndjm04ahbk6hjjxffq0y9a4a") (yanked #t)))

(define-public crate-arraydemo-0.1 (crate (name "arraydemo") (vers "0.1.0") (hash "0vgjhja2573vfb5qmxfs53pi7r7gs7hyrxnzk1yc1dvbzbqg03kp")))

(define-public crate-arraydeque-0.1 (crate (name "arraydeque") (vers "0.1.2") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "08q4z4sv6nh15il9a383fgj77mrlnqypj047rh0bjs00xvi5an44") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.1 (crate (name "arraydeque") (vers "0.1.3") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "1qrnwy236pir4gv743h8vp0qkys7hhrwjkadq5sj6mnds0i8lm9m") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.2 (crate (name "arraydeque") (vers "0.2.1") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "0kfip65zk45iy4kjwf4zscs2bh49mm7sfqmz3s26hnq7g6hdbmgb") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.2 (crate (name "arraydeque") (vers "0.2.2") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "17g7116ri1r2lprzgrmj5v9i46zsbv5gcsx7zq9xsmbxl53290hc") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.2 (crate (name "arraydeque") (vers "0.2.3") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "05x7i5s6144lbrhpk2m532wh561zg6cnf3184m929hi4vg579rwn") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arraydeque-0.3 (crate (name "arraydeque") (vers "0.3.1") (deps (list (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "0zs2f9piz52gli2cb47xwi0i44gz86lfpxr6r5b88yb92hykhqdj") (features (quote (("std" "odds/std") ("default" "std")))) (yanked #t)))

(define-public crate-arraydeque-0.4 (crate (name "arraydeque") (vers "0.4.1") (hash "13gvaxm521ahc56na9ll7f951l6ha7vsw8p0kk89fcb3j52y11p0") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-arraydeque-0.4 (crate (name "arraydeque") (vers "0.4.2") (hash "048zn2vq0amsi8shbfjmrh6gwklfvhg0cajkvnqlz7wbb6l9l1dw") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-arraydeque-0.4 (crate (name "arraydeque") (vers "0.4.3") (deps (list (crate-dep (name "generic-array") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)))) (hash "1imcdj47z71r1ixmy08s3gpwf6hksjr2icnc3z4gy1mqfdq34073") (features (quote (("use_generic_array" "generic-array") ("std") ("default" "std")))) (yanked #t)))

(define-public crate-arraydeque-0.4 (crate (name "arraydeque") (vers "0.4.4") (deps (list (crate-dep (name "generic-array") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)))) (hash "107mg6ijwl3qpz04368r99q4dbwcqjbdg8d2lbaak97qjh7alj6x") (features (quote (("use_generic_array" "generic-array") ("std") ("default" "std"))))))

(define-public crate-arraydeque-0.4 (crate (name "arraydeque") (vers "0.4.5") (deps (list (crate-dep (name "generic-array") (req "^0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1n4appvjfrmxkc4x0v8ivpzwqf1z6pqx2caxk98116fqkgbd7zzh") (features (quote (("use_generic_array" "generic-array") ("std") ("default" "std"))))))

(define-public crate-arraydeque-0.5 (crate (name "arraydeque") (vers "0.5.0") (hash "19h01cp55giz3064prdfr4p823j98jwjpljyms6c6pr5xw3j932w") (features (quote (("std") ("default" "std"))))))

(define-public crate-arraydeque-0.5 (crate (name "arraydeque") (vers "0.5.1") (hash "0dn2xdfg3rkiqsh8a6achnmvf5nf11xk33xgjzpksliab4yjx43x") (features (quote (("std") ("default" "std"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.2.0-rc0") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 1)) (crate-dep (name "time") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "11s66ig8xq9pqgwa9kkhzqnvn77a7riwsixnrg66383an01nvcwd")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.2.0") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 1)) (crate-dep (name "time") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "1y2jajsj4v9qq8hwhvbp5rbjlfrlh13g2fniqkqpqrwd06c7cinl")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.3.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "time") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "02lbhad07iyhl1sqsb2v5nkw0wqlcyhx8wkzif6cvw4q3dkkv2i6")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.3.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "0ciw5n61lw6j83kb3q8c81mrq9zpfa299aaq9c430bxlzxwll402")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.3.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "0s7whhgcqfq0fb34y7ccv2jc7xh1hf176s1w24fnkrndz0qx6f3w")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.4.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "005miih3z8m38wd0si8kg4mjqn2dkf1151yzwlv6hhg3ndg84c0z")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.4.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "07x13whqv87hyprkndjg7n9hx3d36mq9ck2i0d81b32fjy4afh3a")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.4.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "12bi2fynn3218sv0inkc5s2qvmyr2km2mcjfkg64145gwh3ljfi4")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.4.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "12qxd0ac8nwccsa8yzygn3d0055iy0i9qhq1kcyrwhcyhfayrhwp")))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.5.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0.1") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 1)))) (hash "0y75zq4sb99qcz4ss7bmsqdpny29w7rm15n79r5hawzjqgjf5f7c") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.6.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "0p7g6p5rjdr3g44f0qn2qkjjq7rfcfqrjimxl4c2ps3vdh93yz89") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "core" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("core") ("blas") ("arithmetic") ("algorithm"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.6.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "0axc2n49blzhcz7yl3a73bd8rzwnzkzf04wax2yqlr2av3r1wayr") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.6.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "1pcpbqjq9bdjxj9cva1hxl2azzaf8bdc07dqqags6xhbwf1ifha0") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.6.3") (deps (list (crate-dep (name "float-cmp") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "0242rx7zpvxn3a8vmahnwh3982fbzfygmmnqr8f5mxs8kpqy60zs") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.7.0") (deps (list (crate-dep (name "float-cmp") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "00xv53hvgd02a92sl3f46350i7rxwbskjmy5j6j48daa7cwfwz82") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("macros") ("machine_learning") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "machine_learning" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.7.1") (deps (list (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "0flzllqf6z676zgid78cyhw7byf3bv0wr7w58k3hygkajha7zjrg") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("ml") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "ml" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm")))) (yanked #t)))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.7.2") (deps (list (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "1c62qmix5q0pdrkrlwzc5s8gbxrw5vvgm56vm108l4xsc15v24bh") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("ml") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "ml" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm"))))))

(define-public crate-arrayfire-3 (crate (name "arrayfire") (vers "3.8.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0fsx1nk8f2z6aw9m16wdvlqf0p0mi3cdbp56wiqrpnd160n86bf0") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("ml") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "ml" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm") ("afserde" "serde"))))))

(define-public crate-arrayfire_fork-3 (crate (name "arrayfire_fork") (vers "3.8.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "half") (req "^2.3.1") (features (quote ("num-traits"))) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.3.1") (features (quote ("num-traits"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mnist") (req "^0.5.0") (features (quote ("download"))) (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.3.3") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0smvil1jzyykn4mz8krkkpzi5zv5g3xq4bksc5bapp3lwl7n6y2q") (features (quote (("vision") ("statistics") ("sparse") ("signal") ("random") ("ml") ("macros") ("lapack") ("indexing") ("image") ("graphics") ("default" "algorithm" "arithmetic" "blas" "data" "indexing" "graphics" "image" "lapack" "ml" "macros" "random" "signal" "sparse" "statistics" "vision") ("data") ("blas") ("arithmetic") ("algorithm") ("afserde" "serde"))))))

(define-public crate-arrayfire_serde-0.1 (crate (name "arrayfire_serde") (vers "0.1.0") (deps (list (crate-dep (name "arrayfire") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.18") (default-features #t) (kind 0)))) (hash "08vjb9sc1gcivycl50d3p532812wnah4d86zapd3d7al4z8vrr94")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1hflqi7rq6sb52vm637d8hhqpdqjjhy2i58p62mcxd8qbsnkjpnw")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1va5315x232z3hdw5lzx9qf0l4gn0z425vnjljp2s5wldz61v93b")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1dlgvbjqccp4rhqsw1kkj5vdsasf6vs4hnlkmzyic2hzjrrzxz0b")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1f9pghh7n07l70yzq6bicxi5gv60ywrha25asgmbfffrlfbpnwj7")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "09i2mkr6my4iib7mn7d8s35brw02jpn942839yfambzxgfg4mgr5")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1x2imfyl9vhsfj6j66xinf9xfhhlyxa2kcb5sq5jbjc8rcnmqp86")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1da4vhrim2shp8c6xg3m1pvy21my1vq769rivmzadbr27399s0gw")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.7") (deps (list (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0pzx68ims0yfy6nwp2qws4mw6ii4drk53075nr193mhwvgxa14jr")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.8") (deps (list (crate-dep (name "arraygen") (req "^0.1.8") (default-features #t) (kind 0) (package "arraygen-docfix")) (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0bbwph8fps640135vv6ipllnfc0hffpynx79bmw6bpqbl48s7yma")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.9") (deps (list (crate-dep (name "arraygen") (req "^0.1.8") (default-features #t) (kind 0) (package "arraygen-docfix")) (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0ja1al5hhjf99fjlq9mwfk5xr18xasa3k2nc8d89l7li57sfqlq4")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.10") (deps (list (crate-dep (name "arraygen") (req "^0.1.8") (default-features #t) (kind 0) (package "arraygen-docfix")) (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1mj1qkjsrbhrw4hmgn11k624qr4dfiy62bkj2hwiawswxxscjclk")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.11") (deps (list (crate-dep (name "arraygen") (req "^0.1.8") (default-features #t) (kind 0) (package "arraygen-docfix")) (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1slp1vdg9pzlixpv2bvphvqyajyj8blwvfgmw335kb0b94ih2s37")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.12") (deps (list (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1dv5wki20n0a8fbc5nxhaa36lp1q5nnkmv1xd0kl319jqznrml8w")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.13") (deps (list (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "18bbbqng4xhh678bzlf7dig84xqkhz7af3q0xxqc44cjq4imnxfc")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.14") (deps (list (crate-dep (name "compiletest_rs") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0qp513pkkr1sqjq31klfsla6j6pvnqvvh6fhpyaq4n2jbxmy06js")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.15") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "0cr4gl82y2vvzxb71vxw031r0ivl5k1nb5vbl6afaavswwa74pjx")))

(define-public crate-arraygen-0.1 (crate (name "arraygen") (vers "0.1.16") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "1kycd9w3hpfab6213y6j1im1x2f1jiqh0kzkqn35kd0m2rsnqfjl")))

(define-public crate-arraygen-0.2 (crate (name "arraygen") (vers "0.2.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "0v0k8rdlzx31xs1hlh87ma25ijyp6m65xxss992ws6r3rnzw7ja8")))

(define-public crate-arraygen-0.3 (crate (name "arraygen") (vers "0.3.0-RC0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "16nhxcmkg0cb74rsqshkcrfsmsxqf1d99696yjvgzg7y8f0k2mwn")))

(define-public crate-arraygen-0.3 (crate (name "arraygen") (vers "0.3.0-RC1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0mi07mksadsrz2v9filgnw3a3sfjzivdr0w2hq8f5hzyfd0m009p")))

(define-public crate-arraygen-0.3 (crate (name "arraygen") (vers "0.3.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0mpwdx6p9z82n9jc0361l7q2rra6xmqqlf4k81gqxapgkagrja9z")))

(define-public crate-arraygen-0.3 (crate (name "arraygen") (vers "0.3.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1nh8w0hrwyj0rdwl8izv1za9maarfffjbxl1py92sypz6fblxwmz")))

(define-public crate-arraygen-0.3 (crate (name "arraygen") (vers "0.3.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1238askrzc2ginkbsfgv3vjj933qbiwaib7pjsky0mzxi7ivi3yi")))

(define-public crate-arraygen-docfix-0.1 (crate (name "arraygen-docfix") (vers "0.1.7") (deps (list (crate-dep (name "arraygen") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0yll0fgav7pfw8cfcghhdmlas737n77af0iryxqpbpasa6msp5br")))

(define-public crate-arraygen-docfix-0.1 (crate (name "arraygen-docfix") (vers "0.1.8") (deps (list (crate-dep (name "compiletest_rs") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0avlisqdykcz9gpns1qr7pdp3z3dcyrvlxn668wkpkk81flvgf3x")))

(define-public crate-arrayinit-0.1 (crate (name "arrayinit") (vers "0.1.0") (hash "0pwhjh9qzj673hngchm2p6j7qw2k4w2lilwpmg9y38fjqaxc729l")))

(define-public crate-arrayinit-0.1 (crate (name "arrayinit") (vers "0.1.1") (hash "0cfr3pyqfyk52967dqcmqadb3vpkv53cncp01jm22b02ak3948jd")))

(define-public crate-arraylib-0.1 (crate (name "arraylib") (vers "0.1.0") (hash "15ajzbky3qc64j9375damkqjm7fl22fkxycm5f2w69icnc4pygbz") (features (quote (("nightly") ("array-impls-33-128") ("array-impls-129-256") ("alloc")))) (yanked #t)))

(define-public crate-arraylib-0.2 (crate (name "arraylib") (vers "0.2.0") (hash "15b44vw8qkww7q7d3zvfrfv639n86ypcppi317vsq585spk3p080") (features (quote (("nightly") ("array-impls-33-128") ("array-impls-129-256") ("alloc")))) (yanked #t)))

(define-public crate-arraylib-0.3 (crate (name "arraylib") (vers "0.3.0") (hash "1gr81zh800vqx5fbamkf3p7rv28cmwql87qbqladsxws4gaj2a49") (features (quote (("nightly") ("array-impls-33-128") ("array-impls-129-256") ("alloc"))))))

(define-public crate-arraylist-0.1 (crate (name "arraylist") (vers "0.1.0") (hash "1hc9dlwrs53m3sj2qhz2jb9yyn0kp1mqip2azpfinfdyjwqwd1yx")))

(define-public crate-arraylist-0.1 (crate (name "arraylist") (vers "0.1.2") (hash "1q8ygc832h1qq778gzzdqpjb3c73gq5jsbf5n8ff51mm6p477vzz") (yanked #t)))

(define-public crate-arraylist-0.1 (crate (name "arraylist") (vers "0.1.3") (hash "0h297hm70zfk2ksazkp979ly794zaz54xw8dhwr1k3wcyr0dg33i")))

(define-public crate-arraylist-0.1 (crate (name "arraylist") (vers "0.1.4") (hash "0wa9fgyjik8z6pjy8nmgq53z5ch1kbj71bx1lk6giknf8h5mbdc1") (yanked #t)))

(define-public crate-arraylist-0.1 (crate (name "arraylist") (vers "0.1.5") (hash "1kmp5rzd58x38r24fm90286vx8nn2np17nmhmjbh5igkl4nvgqgv")))

(define-public crate-arraymap-0.1 (crate (name "arraymap") (vers "0.1.0") (hash "1j5kpcyadh7hhp92w0yvvzfpqhzdq4y44arj4qz184a80jlrplqg")))

(define-public crate-arraymap-0.1 (crate (name "arraymap") (vers "0.1.1") (hash "05m4xygsbajbicy10ghqp3rbd8y3yas6frnp67fa2giz1n563827")))

(define-public crate-arrayref-0.1 (crate (name "arrayref") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1jmhgx1byfsp38f8jhvmmngsbh81ls1hfzl9ln5gb3r21v4gasdv")))

(define-public crate-arrayref-0.1 (crate (name "arrayref") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0ng0jhl5pbynjf5ryjlwpvplym0xd559yjmh3g82pkckq2w1l56z")))

(define-public crate-arrayref-0.2 (crate (name "arrayref") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0vgishy80cwvgxwrsf7d9awb136y6khh15fm5vqaj55s5sdd4kkl")))

(define-public crate-arrayref-0.2 (crate (name "arrayref") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0bsp6y482m74vgia0824q6bfhdjfmyqjkfkxj6xcasbzmpb6lp5i")))

(define-public crate-arrayref-0.2 (crate (name "arrayref") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1ylcy2v1h0ci593vcn9n61fygbj92i38qcrbk1qm53d2gl2rrz6h")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "01c8i780d8vi5m2r4bza4nvqrn961gkwl7b0g9r130zscidgc904")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "18xzgc89887chahspgx3aq0bpmv3mx3dp1fdn5160z3aba1hi8r9")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.2") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1yaqimjxdbwccr4n0szf8lalsr1v2vn6djaw1fdjwkz3sjd7403m")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.3") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "1r8xqn116z7343fc21f8qi9rdvggz3kk5zk0rdvsdxvr487snnxi")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.4") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "13r5hyyarq53bnbqqp7jj9m6jbcjjg1bbwszsgdilr19gjdlgl8g")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.5") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1vphy316jbgmgckk4z7m8csvlyc8hih9w95iyq48h8077xc2wf0d")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.6") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0i6m1l3f73i0lf0cjdf5rh3xpvxydyhfbakq7xx7bkrp5qajgid4")))

(define-public crate-arrayref-0.3 (crate (name "arrayref") (vers "0.3.7") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0ia5ndyxqkzdymqr4ls53jdmajf09adjimg5kvw65kkprg930jbb")))

(define-public crate-arrays-0.0.0 (crate (name "arrays") (vers "0.0.0") (hash "1x1p8zp3rkgh90k0b54gq518ys8j30mayfmsmggbkk3zhjzf5r0w")))

(define-public crate-arrays-0.1 (crate (name "arrays") (vers "0.1.0") (hash "15v6axidwg1rl9z4bbvvqyccsslj164r8zds8q74jm4wdvv0bsfm") (rust-version "1.56")))

(define-public crate-arrays-0.1 (crate (name "arrays") (vers "0.1.1") (hash "1zh8v9a9m6vg3vjdsr6q50mdrwpcn815h4mqqr3hcvnjfwlbpg9d") (rust-version "1.56")))

(define-public crate-arrays-0.2 (crate (name "arrays") (vers "0.2.0") (hash "1qgizk31qgy77kbydxfsh97swibnqjji4gkjrk0sf7cijbc1161w") (rust-version "1.56")))

(define-public crate-arrayset-0.1 (crate (name "arrayset") (vers "0.1.0") (hash "05j3mfg37amc4vfy4kvz7s5i74g5sg1hb73dbkpj7xd3wpvac55x") (yanked #t)))

(define-public crate-arrayset-0.1 (crate (name "arrayset") (vers "0.1.1") (hash "17h7nnwdj12r7vws9h4sp2cvkgvkq2116zl4jdwa7y62kxkxsh9y") (yanked #t)))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.0.0") (hash "1c9alr65s6ik8iq0hc4qmglyzyzjhmngxsvbyy4syjq2qlz8c9wy") (yanked #t)))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.0.1") (hash "1fkbdi95pmiha6cy15lp8qxq16n4bn39zn35hl2n00fihkgyr890") (yanked #t)))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.0.2") (hash "1smc8ysqszkgmhny69vr5lijmwiich8dg2y0kda0m3nisk8b7sqv") (yanked #t)))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.0.3") (hash "0yqbwri4cn64h93n81b57nzkd58r521dg7ksmx5161fbjsqyc1xl") (yanked #t)))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.0.4") (hash "0p4iyksj92caw2mpk4ra0y3xz67blp4xkb5g46h1lbfs912k2gsl")))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.0.5") (hash "1cspznhaybr4idaia666g0hsk5f9q080f68kgbdvkjc0pkm70hjy")))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.1.0") (hash "1n6r6xvyhf8a5my6gfflf91ra7fircn26ifm8ysmi04wfjd4apx3")))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.2.0") (hash "0sv1xqz8ifk7rh8n45pwmljmjvbrq10ivba5a9q55i5jqaf6vczh")))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.3.0") (hash "1f98fdgmasd0b8c9fcmivpacwmxml9pm0s2llbkchhq00ibfyhsl")))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.4.0") (hash "0s7hsi27hqlvzrm785130vgx31yq9592klrmz7sbkc5zcrks4n5g")))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.5.0") (hash "0s6lici8hss0mmznzyzbmlp36ayp5179kj72h91xlwznwqnsyxna")))

(define-public crate-arrayset-1 (crate (name "arrayset") (vers "1.5.1") (hash "1x319zdqb0sklvlryc41vvn0l60jql2y8sd4g25m4gkbci9wjjal")))

(define-public crate-arrayset-2 (crate (name "arrayset") (vers "2.0.0") (hash "0dywj6g8zardg047kk5ahxz9ik7ddrpsxg3gsmc3iyk7xzndmvz8")))

(define-public crate-arraysetcell-0.0.1 (crate (name "arraysetcell") (vers "0.0.1") (deps (list (crate-dep (name "default-option-arr") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "194drks3pp855aq99piiq1vp2ijlam8jysb1v4wff4aa81lir3bn") (rust-version "1.68")))

(define-public crate-arraystring-0.1 (crate (name "arraystring") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "05v1a40jdp6kg0l6mh135cpigi5xkfpw4xifb3s4bbdllln1bw5q") (features (quote (("std") ("serde-traits" "serde") ("nightly") ("logs" "log" "env_logger") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraystring-0.1 (crate (name "arraystring") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "diesel") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ixp9rvjjmppp7k8i23509kx3zzcyc86rli95ab10wznr54yp747") (features (quote (("std") ("serde-traits" "serde") ("logs" "log" "env_logger") ("ffi") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraystring-0.2 (crate (name "arraystring") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1a019crl84gbxa5hf8z9k26syk2l7i7jnqq6g4llamh5qgkm3n74") (features (quote (("std") ("serde-traits" "serde") ("logs" "log") ("default" "std"))))))

(define-public crate-arraystring-0.2 (crate (name "arraystring") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "diesel") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.3") (features (quote ("sqlite" "postgres" "mysql"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "0n2bgl1dhnwqd2ah6yr08g1vzwr374z8mr013rxq2wcyx70ixpbr") (features (quote (("std") ("serde-traits" "serde") ("logs" "log") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraystring-0.2 (crate (name "arraystring") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "diesel") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.3") (features (quote ("sqlite" "postgres" "mysql"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "1nzcq8vd30xlglkk0md4svwfab8manlsfjnbs6mm1ss0yxzpz8sb") (features (quote (("std") ("serde-traits" "serde") ("logs" "log") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraystring-0.2 (crate (name "arraystring") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "diesel") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.3") (features (quote ("sqlite" "postgres" "mysql"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "0ybgv00krnqcyzbgk2wmn32qhqdbnfi75lc827ispjx4gzw14k2m") (features (quote (("std") ("serde-traits" "serde") ("logs" "log") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraystring-0.2 (crate (name "arraystring") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "diesel") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.3") (features (quote ("sqlite" "postgres" "mysql"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "1yv7b5pz2ql23j1abd7f67c3pldrybl0dz5dxfr97x17nsz6zlzy") (features (quote (("std") ("serde-traits" "serde") ("logs" "log") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraystring-0.2 (crate (name "arraystring") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "diesel") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.3") (features (quote ("sqlite" "postgres" "mysql"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "0kah1nj4w0j8yndp3p9nwqnzlrzaw6135j094s0rvsbwvcif4az5") (features (quote (("std") ("serde-traits" "serde") ("logs" "log") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraystring-0.3 (crate (name "arraystring") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "diesel") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.3") (features (quote ("sqlite" "postgres" "mysql"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "inlinable_string") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "smallstring") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "1181rr76jqlldrpy586cj2bzgxvzhn8crj2vg75diq8pf537qlad") (features (quote (("std") ("serde-traits" "serde") ("logs" "log") ("diesel-traits" "diesel") ("default" "std"))))))

(define-public crate-arraytools-0.1 (crate (name "arraytools") (vers "0.1.0") (hash "1gmzj8rw9g156gwl8ix2f4gfx7g6z8h3xwaq0q7x0bwavq12sy8r")))

(define-public crate-arraytools-0.1 (crate (name "arraytools") (vers "0.1.1") (hash "17sfslnpbzmpnkc5c100v94721q85a9y1ik2djh7ywf35gi6ydgz")))

(define-public crate-arraytools-0.1 (crate (name "arraytools") (vers "0.1.2") (hash "1d220mk1alfkdi1b9afy44lwnxqc2088fzhcdnr4pvn3p84bcn51")))

(define-public crate-arraytools-0.1 (crate (name "arraytools") (vers "0.1.3") (hash "0p3f59z6r1vjhbfylypshrdnlcjkns794gf0f88bh7gm2vqsxin7")))

(define-public crate-arraytools-0.1 (crate (name "arraytools") (vers "0.1.4") (hash "086x5whs4yixx5qz44l1jvl9sq8ivw4jiw6ywc6v3clfhkd2zwc7")))

(define-public crate-arraytools-0.1 (crate (name "arraytools") (vers "0.1.5") (hash "0dzl752gd56xsg0z1bdy12xmxj3lf5ykqsig5ydq17cdjccpjdm5")))

(define-public crate-arrayvec-0.1 (crate (name "arrayvec") (vers "0.1.0") (hash "104nbdkfyipwhx1ncjqphv9n7cw27kmqdn5q6wqmlx0wy98izzzp")))

(define-public crate-arrayvec-0.1 (crate (name "arrayvec") (vers "0.1.1") (hash "109va9a7k1kf97fk02l0sd3pxxmcavs4cxrg2993qkq7pq2byk7j")))

(define-public crate-arrayvec-0.1 (crate (name "arrayvec") (vers "0.1.2") (hash "0rxapl0jf35kfw4023wszpjarnvlyx9jkvzvn19w1lx4dzhwfzvg")))

(define-public crate-arrayvec-0.2 (crate (name "arrayvec") (vers "0.2.0") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "053a0vlz7ca1p88mpdp35jddhylm8bqxni3qpj4dm3jqn87m2lbh")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.0") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "1adzqxwsjp2zaf095x9n7flnm7kndk1daz6cvdb0947v0lbdgg5i")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.1") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "0m1avcjhw8c0zyj9lhj9c3jixrlwij4lnbmpk3zzh79a17922qhj")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.2") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "09jw4dsjxjq82vf0lzdqhcvxx6fj10yqygilfkmfb111b58lwmq5")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.3") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "1q92xvqyf2spfp5wilw3x96scahgpk5nl7bv4kjvjjjhk44s8ris")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.4") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hfppqxi7wa2ak16byy93fbgcbdss3fcp57hh29a1hwv19k2vf0v")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.5") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.1") (default-features #t) (kind 0)))) (hash "03al12cs1h5ppl3m9iapri7q7svgk3dv7q5yfgmdslpda8ql5way")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.6") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "1n7f5rjncy50nn8y8fmryw8aml2wa2zrwfp72hq2wbr2jcrhcnis")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.7") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "04b9gppg625lx753r6xrxd9vyjmd3p7gfmwnpk47a2p86gadblhj")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.8") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "099czprg3vj21f041n3gqsbp4vzzqbrkmm8pd4iigzv6gmdqfdva")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.9") (deps (list (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gd07z35b34bn14ln75661hlcxb198743j2smm350ylhibcw5np1")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.10") (deps (list (crate-dep (name "nodrop") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "0adqk0p1y4jzlg84m3x3mlc9099ivx3grkcbplcgal3dmdd6cxxd")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.11") (deps (list (crate-dep (name "nodrop") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xi4g7fmfi6k7clgyxhsz4afkbdm8y36cgiycpqhx0jvwahvyx70")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.12") (deps (list (crate-dep (name "nodrop") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "063xjdl1nfwjbh2gl5kswdr55hlma500pwv3qpn1d64cgqj0hc5y")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.13") (deps (list (crate-dep (name "nodrop") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y1djhqrsawlccv8b6wv5a89j6qfllsw9292jn6yxf6jmlz6nhx3")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.14") (deps (list (crate-dep (name "nodrop") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "0szkv5bpm14w9v9fcz20p0cafiw0r7fjlx6p8vxp0njbb2hs2y32")))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.15") (deps (list (crate-dep (name "nodrop") (req "^0.1.6") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "1axia8yrz3hxxw3jblxqvj46567jxcd0nsj8qzzdjznq304hzk2z") (features (quote (("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.16") (deps (list (crate-dep (name "nodrop") (req "^0.1.6") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "0x996nf2mwpwa736nasb55akivgqgjlmjpcphl1cwfjbynrbvqqn") (features (quote (("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.17") (deps (list (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "0q68hnrwgwvscmcqfpvc88ap3m7h265djqy0hxrwx4if5qwkg8c0") (features (quote (("use_union" "nodrop/use_union") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.18") (deps (list (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "1dycl8gry7m2ikbdcrfamx86hax4202fbfrcazi6i0wffgyh23c9") (features (quote (("use_union" "nodrop/use_union") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.19") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "0nkmm4fya8l2vna0skcdr02p3mjhdf98cqa2z9s3ss8jh77vnni9") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.20") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "15mhzb03q8hlyjnnz3axmk6a58c2s74g12kpjyvvaw124h71p7yq") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.21") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)))) (hash "1d0qxd4q01456wc3mz41alx1kigqpjq6rvgqk8i9gq50b7svhk0n") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.22") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)))) (hash "1vdsfjjwbnzy28c4alh6sqp4yqj2r9k0rb2sxyfhhd814q3ykh9m") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.23") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)))) (hash "0rywr2q86719d6vjx9srkrnhs3bqnwl1psxmqgl1gmvr7fln77k9") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.0") (deps (list (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "185j6rk1cy0fl1z23891lcdsbwi31wwf5gpdji8sk31ddhgll5fi") (features (quote (("use_union" "nodrop/use_union") ("std" "odds/std" "nodrop/std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.1") (deps (list (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "05hw95vibwp22ggvwxsjcnc7arhk2ndb0254ihas6jjf389nbiqv") (features (quote (("use_union") ("std" "odds/std" "nodrop/std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.2") (deps (list (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "09w40y4h96i79nk66x9175j635jwvx438ncqccam176fs13m5l11") (features (quote (("use_union") ("std" "odds/std" "nodrop/std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.3") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0bnsv7rw18a805nq7gy1nns1icwb7i48ix2s5g8mn2nrmbgbd6vg") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.4") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1dhpxmv5l7z3vl9x2rgbhj7w3a1jva4b0psjvx9n6c8p7dlm00hw") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.5") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1yvjg3j4lk5arn1jf4pxcbj5nbc4sbzbb09h0bbvr4gcwq12qgpn") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.24") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)))) (hash "1qk5kw9y04k1r8shrxmwbk9i8i8p4kmahbnzr07kmi71w3vcn0z0") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.6") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1qmvf115sa0xviyv8j8sb2glc7f77p4ii2ci3p4s0680halz83ig") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.3 (crate (name "arrayvec") (vers "0.3.25") (deps (list (crate-dep (name "generic-array") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nodrop") (req "^0.1.8") (kind 0)) (crate-dep (name "odds") (req "^0.2.23") (kind 0)))) (hash "0krkhgiwsd54r11sqnx8xymgy8xd70h8rllhrpx7ifq60ghrzx86") (features (quote (("use_union" "nodrop/use_union") ("use_generic_array" "generic-array") ("std" "odds/std" "nodrop/std") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.7") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vsscw2jzkhn2lqjgz3xcp48ilj0vamh6ddlzj1q2n2dwbwn9sd1") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.8") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1k2062b7by0p2p15x6q4y1c9bx80pkwsvhlgdi7pi2yd456cq1gl") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.9") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "09lagldfg8fyf9k49m63rgy6vck6vi9mq72i932630idgjbi71fi") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.10") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0wcch3ca9qvkixgdbd2afrv1xa27l83vpraf7frsh9l8pivgpiwj") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.11") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fmhq4ljxr954mdyazaqa9kdxryl5d2ggr5rialylrd6xndkzmxq") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.5 (crate (name "arrayvec") (vers "0.5.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "05ia5hrr78x2fi7dwf0pkd8wwfbgg1dzja50l0gcjfw4p2y03n7a") (features (quote (("std") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.4 (crate (name "arrayvec") (vers "0.4.12") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1.12") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fdiv5m627gh6flp4mpmi1mh647imm9x423licsr11psz97d97yd") (features (quote (("use_union") ("std") ("serde-1" "serde") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.5 (crate (name "arrayvec") (vers "0.5.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1f5mca8kiiwhvhxd1mbnq68j6v6rk139sch567zwwzl6hs37vxyg") (features (quote (("std") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.5 (crate (name "arrayvec") (vers "0.5.2") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "12q6hn01x5435bprwlb7w9m7817dyfq55yrl4psygr78bp32zdi3") (features (quote (("unstable-const-fn") ("std") ("default" "std") ("array-sizes-33-128") ("array-sizes-129-255"))))))

(define-public crate-arrayvec-0.6 (crate (name "arrayvec") (vers "0.6.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "05rh2a04k9fdffgprybkvaq21d8zxs4jp4m83ncn9wkh2ayqhbv8") (features (quote (("unstable-const-fn") ("std") ("default" "std"))))))

(define-public crate-arrayvec-0.6 (crate (name "arrayvec") (vers "0.6.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1jghi4x1j0gybnhcdsrnk8kqs06wd9rpq7pqhympqfimd1g0z796") (features (quote (("unstable-const-fn") ("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7 (crate (name "arrayvec") (vers "0.7.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1xza6jbs8x51yhh4qnwjw1crm33bhl975r965fpq1hqhpfq5hbss") (features (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7 (crate (name "arrayvec") (vers "0.7.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1kcgsfp0ns0345g580ny7hhl9y9a6l3m0pykfa09p9pz65qw0kdy") (features (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7 (crate (name "arrayvec") (vers "0.7.2") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mjl8jjqxpl0x7sm9cij61cppi7yi38cdrd1l8zjw7h7qxk2v9cd") (features (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7 (crate (name "arrayvec") (vers "0.7.3") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "17684msnzvh4nx064x4cqqijmimqv5lyajm7kl3qpa6fz2gz0s48") (features (quote (("std") ("default" "std"))))))

(define-public crate-arrayvec-0.7 (crate (name "arrayvec") (vers "0.7.4") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "zeroize") (req "^1.4") (optional #t) (kind 0)))) (hash "04b7n722jij0v3fnm3qk072d5ysc2q30rl9fz33zpfhzah30mlwn") (features (quote (("std") ("default" "std"))))))

