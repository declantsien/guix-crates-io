(define-module (crates-io ar mv) #:use-module (crates-io))

(define-public crate-armv4t-0.1 (crate (name "armv4t") (vers "0.1.0") (hash "0sgd96s4fkjbhr9nkgzj3gjgcp4z150a23w90q7fglzgymlz5kzm")))

(define-public crate-armv4t_emu-0.1 (crate (name "armv4t_emu") (vers "0.1.0") (deps (list (crate-dep (name "capstone") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde-big-array") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "17v80mmg9cmkj7q50b93ndw0pngimg1s7399qjqfndhf2h3k7xsx") (features (quote (("serde-serialize" "serde" "serde-big-array") ("default") ("advanced_disasm" "capstone"))))))

(define-public crate-armv5te-0.1 (crate (name "armv5te") (vers "0.1.0") (hash "106wf0wlabj2izj6hnf0gpxymfif3kw30idrjf890a2jcxs3jq67")))

(define-public crate-armv5te-0.2 (crate (name "armv5te") (vers "0.2.0") (hash "1hz533bn4g1mkmg2mbv7ivvhx390vy72mc503nw2g5k2iply49bl")))

(define-public crate-armv5te-0.3 (crate (name "armv5te") (vers "0.3.0") (hash "012z0g1fhakc2pr49wfnmzhw46ds3mpwjhqs070vxv9lmangk07x")))

(define-public crate-armv6-m-instruction-parser-0.1 (crate (name "armv6-m-instruction-parser") (vers "0.1.0") (deps (list (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1is36g9shml872s2k1qg0hr5d2hyi9s723jna2gysmy340vx45qw")))

(define-public crate-armv6-m-instruction-parser-0.2 (crate (name "armv6-m-instruction-parser") (vers "0.2.0") (deps (list (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0k0a8yl9jsgpqbqscjc7xrzs6w9n05v4b39my9kqhf5aj50zsi6g")))

(define-public crate-armv6-m-instruction-parser-0.3 (crate (name "armv6-m-instruction-parser") (vers "0.3.0-rc1") (deps (list (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "04l5bghfm5hb1mrqpplh5fs8r3kmllwkbqx1k6hxcg7sgq5x3jrr")))

(define-public crate-armv6k-1 (crate (name "armv6k") (vers "1.0.0") (deps (list (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)))) (hash "0dyvmrwjkvkr0r2cp78d2nypv133sjv6wvxvsdb1apnyk9gbjd2x")))

(define-public crate-armv7-0.1 (crate (name "armv7") (vers "0.1.0") (deps (list (crate-dep (name "register") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0s9c3pqnjyixn69b41dzrgzxwja1ykjqh7j6h32kkghlx3flyya4")))

(define-public crate-armv7-0.2 (crate (name "armv7") (vers "0.2.0") (deps (list (crate-dep (name "register") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0i894b74ffyg4nfppg28ddp78gl31jlb64vl0fszlxw7n6wxwq8w")))

(define-public crate-armv7-0.2 (crate (name "armv7") (vers "0.2.1") (deps (list (crate-dep (name "register") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1sj1w25bcy49agzalwyxark6w9in4a34qkkh51v8jx01lxmhrmjl")))

(define-public crate-armv8-0.0.1 (crate (name "armv8") (vers "0.0.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "063cczfqjmv2bi1pfvlpgzrkq4697vzripdqc4hkafx5sl3gqd2z")))

(define-public crate-armv8a_panic_semihosting-0.0.1 (crate (name "armv8a_panic_semihosting") (vers "0.0.1") (deps (list (crate-dep (name "armv8a_semihosting") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "17yk5djvgj1hwap6ldinbmsm667w7k257q2lr10rbr2k0cs481dx") (features (quote (("info") ("default" "info"))))))

(define-public crate-armv8a_semihosting-0.0.1 (crate (name "armv8a_semihosting") (vers "0.0.1") (hash "0rx3v6sl77g6fx92hb5a65xhqlli48qb17559ybv4l0yj2r17h8x")))

