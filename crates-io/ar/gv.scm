(define-module (crates-io ar gv) #:use-module (crates-io))

(define-public crate-argv-0.0.0 (crate (name "argv") (vers "0.0.0") (hash "01ha0jvb6yxyz87ylfpcqax6sv6lk8gh3n8ipknlmjzhd9z8ixw2") (yanked #t)))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.0") (hash "0f5l2njcwqig2zfrvyb5fi2vzqr8ik3sxxmk57gp55b7gdbrsqp2")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.1") (hash "083q7a8ryajivmc4p15jlmc51cs8591gvcv2mrm6vw28d3cfc619")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.2") (hash "078f4xci8p86shhh3c9hgr0ijvq5a3yw0dzjsyvg35rffny8pd47")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.3") (hash "0nsqkp6978fanwk5ydvbgjcrzyira9dl8jxrdfgymypxisc585nz")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.4") (hash "1ndqb9lvb0hvcghgyy7qx6g75qk9la69hvrb4d47vj6qi5bwdzp9")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.5") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "138ms6vm0gxkvnr5kxbvfyamw73z644y61sx5pap7mcfz0366fsr") (rust-version "1.33")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.6") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0cj2cjbzmi3lb9p058akvmsz2n2r764j2pnc5wx6ly5p0vfhafss") (rust-version "1.33")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.7") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0m0bjbc75mkvvb7zly46azpdgfihh5c4nxpxhp6b8pn793n9sscw") (rust-version "1.33")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.8") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "14s8cxkm1nqlkdyb0n7bk7b8b2gy8fsqdggjq944q8icnmfhbwdj") (rust-version "1.33")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.9") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ql3jhyvliyz723kwswzlz45gm7ndw8hx2vxks35vhvrpiryvfli") (rust-version "1.33")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.10") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "target-triple") (req "^0.1") (default-features #t) (kind 2)))) (hash "07qnlf510b8q2jb3mvi8fhkqmfhavxhw311p4cf4gvryi5s0igd2") (rust-version "1.33")))

(define-public crate-argv-0.1 (crate (name "argv") (vers "0.1.11") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "target-triple") (req "^0.1") (default-features #t) (kind 2)))) (hash "0y66i1xs9lppq7g0wi78lch7axpkjcr614lq3imkzi4xrljh5jay") (rust-version "1.52")))

