(define-module (crates-io ar bt) #:use-module (crates-io))

(define-public crate-arbtest-0.1 (crate (name "arbtest") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "16dj9nyjlf15sk37p4n86wp75k762iqq87ffz0141jyxhdzpz953") (features (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.1 (crate (name "arbtest") (vers "0.1.1") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1b689y26j4a5q43941jgj9rzw5ir9pc6cd0s3v2jwmspi659204r") (features (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.1 (crate (name "arbtest") (vers "0.1.2") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "110fckpjs48qw73b3vxf7g2fk7xffdlx4zlk46rml9xnlq54dn0c") (features (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.2 (crate (name "arbtest") (vers "0.2.0") (deps (list (crate-dep (name "arbitrary") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1avmdhqsm4w64f9r5qgcy940859ahmvxrx5b46s0b13b2nafx36l") (features (quote (("derive" "arbitrary/derive"))))))

(define-public crate-arbtest-0.3 (crate (name "arbtest") (vers "0.3.0-pre.1") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.5") (default-features #t) (kind 2) (package "regex-lite")))) (hash "179lkvdlzghdi7lzzk5qp92khpa37njlwwn6s6wqd39871cjra7y")))

(define-public crate-arbtest-0.3 (crate (name "arbtest") (vers "0.3.0") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.5") (default-features #t) (kind 2) (package "regex-lite")))) (hash "0qzx35dm0bx0wg4vsa8d2bqvnp9rfy2yhgmzzn1kjxbadxqzgwsn")))

(define-public crate-arbtest-0.3 (crate (name "arbtest") (vers "0.3.1") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.5") (default-features #t) (kind 2) (package "regex-lite")))) (hash "1p1z3450v617axpmh539qhpd47g4vd3yi1y8ljlc5yhpnmgrv413")))

(define-public crate-arbtree-0.1 (crate (name "arbtree") (vers "0.1.0") (hash "0g2kygqdw64z12aqy6nv5aijpnxlpkki48nfbbawbk9ahjln5nx9")))

(define-public crate-arbtree-0.1 (crate (name "arbtree") (vers "0.1.1") (hash "0xsf2kh5s1jav0c3pva0mfpngvhw5ga45za5jzb5515r6dmqb2fx")))

(define-public crate-arbtree-0.1 (crate (name "arbtree") (vers "0.1.2") (hash "124i463cfgy8f9gq5pqla8k95d0ivrs9cr4zp0qkjr22gxb3imkk")))

(define-public crate-arbtree-0.1 (crate (name "arbtree") (vers "0.1.3") (hash "0s8i3dz849a5akqfrcrhzxzp53gmnfa1jlrs4mb81czp8angj93a")))

(define-public crate-arbtree-0.2 (crate (name "arbtree") (vers "0.2.0") (hash "1xax4s68y54f550j76ph0zvqnmgpgmkysp1lm0ccqn87lvqgr4cf")))

