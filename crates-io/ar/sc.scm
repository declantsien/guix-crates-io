(define-module (crates-io ar sc) #:use-module (crates-io))

(define-public crate-arsc-0.1 (crate (name "arsc") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fagxi8m4i8yp8kxbsjs4nss6bq8pm3xzjz1pfag8lbhasv07zif")))

(define-public crate-arsc-0.1 (crate (name "arsc") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "10s813kwmyzi21fv83057vxkkvmqvzsk14s135rn0d6kxa414hr4")))

(define-public crate-arsc-0.1 (crate (name "arsc") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "01m0vdvqncc315nrfnca68lvwy0snqlx7a47yb6hw7rszx2xc2dj")))

(define-public crate-arsc-0.1 (crate (name "arsc") (vers "0.1.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lb05qfyvhia6l69da9qrcvqdj2xp6vbkkii4kr64f8f67sfywhs")))

(define-public crate-arsc-0.1 (crate (name "arsc") (vers "0.1.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 2)))) (hash "1i4najzav1nywlxbl58qxfsn9yd6798jrn7q8442dq8nzc8fbym4")))

(define-public crate-arsc-0.1 (crate (name "arsc") (vers "0.1.5") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 2)))) (hash "1wb113cc32kaj3wkzq60wiv2y5sjikka3rrr772m2l2f95j15iiw")))

(define-public crate-arsc-rs-0.1 (crate (name "arsc-rs") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memoffset") (req "^0.8") (default-features #t) (kind 0)))) (hash "0lc4xfplx05a46f00s80i963g9kfb4xsa53501h5adrnh6w6kzg1")))

