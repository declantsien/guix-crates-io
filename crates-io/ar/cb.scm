(define-module (crates-io ar cb) #:use-module (crates-io))

(define-public crate-arcball-0.1 (crate (name "arcball") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.108") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "0mfvk182h7g1cga5zgh7nnz9zk3krfnrn4ncbay82bgzsnicbrw4") (features (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.2 (crate (name "arcball") (vers "0.2.0") (deps (list (crate-dep (name "cgmath") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.129") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1z8ha56rm0xgzwsc99wjkchs5rq52vp4xpxma20jmkzqg5jcrmc4") (features (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.3 (crate (name "arcball") (vers "0.3.0") (deps (list (crate-dep (name "cgmath") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.148") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1nhqm3vylyjmf9g8b0c343rkb4bhp5bjqkm4skfjzfr4xqyiqgcf") (features (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.3 (crate (name "arcball") (vers "0.3.1") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.148") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1q00fqy6s0a5kck9jvjvznak3cx8g5wllw6p8gb62mllh6la4ma0") (features (quote (("unstable" "clippy"))))))

(define-public crate-arcball-0.3 (crate (name "arcball") (vers "0.3.2") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.23.0") (default-features #t) (kind 2)))) (hash "0y4kfn7gsahzkr1jnvi3fjrdz7ppncg4xbmx3brcz73n8b8hwkl1")))

(define-public crate-arcball-0.3 (crate (name "arcball") (vers "0.3.3") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.23.0") (default-features #t) (kind 2)))) (hash "1iy74ya7hv25064vg1ka0y56p6hjn00f18pp6sjk7570rzwgxj2p")))

(define-public crate-arcball-1 (crate (name "arcball") (vers "1.0.0") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.21.0") (default-features #t) (kind 2)))) (hash "1bls6nb82vx4pml3rwvhc1vav04pb6c1lkqzl63yhr3yqcz47qvm")))

(define-public crate-arcball-1 (crate (name "arcball") (vers "1.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "glium") (req "^0.28.0") (default-features #t) (kind 2)))) (hash "0d92hsv6nwh4jy0j5qbwzcnrn6rn53wrg3021jlhxl49ix0f2cjv")))

(define-public crate-arcball-cgmath-0.1 (crate (name "arcball-cgmath") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0q3majhj87p21gd1xxpkilbixyry2k4a18jslhzicx8zknbc08af")))

(define-public crate-arcball-cgmath-0.2 (crate (name "arcball-cgmath") (vers "0.2.0") (deps (list (crate-dep (name "cgmath") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1sh748ps87n8wxxxbak8dzyvblx0l6x0k3nyj9xcfq3pdl2d57wn")))

(define-public crate-arcball-cgmath-0.2 (crate (name "arcball-cgmath") (vers "0.2.1") (deps (list (crate-dep (name "cgmath") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "10yw2gamzsidcsv0diz3mx8lgaqrbkrjibww6wy5hi5jn3kxgkdk")))

(define-public crate-arcball-cgmath-0.2 (crate (name "arcball-cgmath") (vers "0.2.2") (deps (list (crate-dep (name "cgmath") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0anxxf401f15484qvpmd3lqm6i378paq5q2hi5k1hrcgbspwgcgm")))

(define-public crate-arcball-cgmath-0.3 (crate (name "arcball-cgmath") (vers "0.3.0") (deps (list (crate-dep (name "cgmath") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pnqsxldk13nczbzwhgqkn8s93f9ql1m21awliyj8dn88s4d28wz")))

(define-public crate-arcball-cgmath-0.3 (crate (name "arcball-cgmath") (vers "0.3.1") (deps (list (crate-dep (name "cgmath") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dpgdw2ifmcpnyiq1dw4lp5zj1prs4yy1s4vs6z2mgarivx3jaxy")))

(define-public crate-arcball-cgmath-0.3 (crate (name "arcball-cgmath") (vers "0.3.2") (deps (list (crate-dep (name "cgmath") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rmvx1amc7dr3l8fg81kdhd1swi5zd4i0q3wf6pclmg8m3hp6xgw")))

(define-public crate-arcball-cgmath-0.3 (crate (name "arcball-cgmath") (vers "0.3.3") (deps (list (crate-dep (name "cgmath") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qmhws9i1ndj91dm9r1ln8w5pnrvklfv12abirbkh3h9k0sipb0a")))

(define-public crate-arcball-cgmath-0.3 (crate (name "arcball-cgmath") (vers "0.3.4") (deps (list (crate-dep (name "cgmath") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "17frkq7bqrmcll2vyc5iazdgpj6iazxb8asid51n24d6vy85vmxw")))

(define-public crate-arcball-cgmath-0.4 (crate (name "arcball-cgmath") (vers "0.4.0") (deps (list (crate-dep (name "cgmath") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "0b3dcx03c6w754573020wfn75vqsqfafgj36ljrslz2n0w0x6ing")))

(define-public crate-arcboot-0.1 (crate (name "arcboot") (vers "0.1.0") (hash "0zb2mn1yzwpbsli28j03jdvmicyl9n40lc0llwxm8wmkf191cnhb")))

(define-public crate-arcboot-0.1 (crate (name "arcboot") (vers "0.1.1") (hash "0zy5f6d1ngaarrjfnbs8hwx8mr9hb6i5g1pb58729lmzwz04j7pz")))

(define-public crate-arcboot-0.1 (crate (name "arcboot") (vers "0.1.2") (hash "13f063mi8vvh0zahxkw2xd72yv0rjvbhrq8m3c18wmw8x93fda29")))

(define-public crate-arcboot-0.1 (crate (name "arcboot") (vers "0.1.3") (hash "1sa5qxgb75v25cfj9w7bi0fap2m4ayz8bs2cbvh85s14li7apckn")))

(define-public crate-arcboot-0.1 (crate (name "arcboot") (vers "0.1.4") (hash "1rp4f19f1j3pn6sd6czq0jj3r1nn4acrhs60xplm9b7z7l80av3c")))

