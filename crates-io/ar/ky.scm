(define-module (crates-io ar ky) #:use-module (crates-io))

(define-public crate-arkyo-0.1 (crate (name "arkyo") (vers "0.1.0") (hash "1ikyc4mf3409pxvjfkf24cmxxpif1vfpfgmp8qsf53hds7pzspah") (yanked #t)))

(define-public crate-arkyo-0.0.1 (crate (name "arkyo") (vers "0.0.1") (hash "0vrcdjj838r0zx0nwzm8j87fp7zs8yz5nkm2c5mwvf744niysbkh")))

(define-public crate-arkyo-0.0.2 (crate (name "arkyo") (vers "0.0.2") (hash "0839vys9a5jh6x0wj5rd2pfzg3wzvpvp7lzgwp5q4v9rg1f3mfmz")))

(define-public crate-arkyo-0.0.3 (crate (name "arkyo") (vers "0.0.3") (hash "165gjr1ngpd06l9c63m4kkc55ggkhpmd7993xmw35dvdv7rrsz7y")))

(define-public crate-arkyo-0.0.4 (crate (name "arkyo") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)))) (hash "0byxaln1gxg69scxvlgh02b6pjwbkxqmx26cxnvikxbzr7mq6c90")))

(define-public crate-arkyo-0.0.5 (crate (name "arkyo") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)))) (hash "161dk9kwkylxcrwv1cn3yqasrjrgrjprapfw4y1lgyz0ym7g5xy4")))

(define-public crate-arkyo-0.0.6 (crate (name "arkyo") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)))) (hash "0cskxjnpdaf0dkijzwqy6g4sy42mwf8nfxvl6pmghs4vdx8bg291")))

