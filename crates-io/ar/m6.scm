(define-module (crates-io ar m6) #:use-module (crates-io))

(define-public crate-arm64_virt-0.1 (crate (name "arm64_virt") (vers "0.1.0") (deps (list (crate-dep (name "simink_entry") (req "^0.1") (default-features #t) (kind 0)))) (hash "02hzb13zcafwsc2ipvpafp2k6gca05gv93js5r26kp1s44h6gmnd") (rust-version "1.71")))

(define-public crate-arm64utils-0.1 (crate (name "arm64utils") (vers "0.1.0") (hash "0crl6j7bxsmd2zjdq0k2ldl6pnrj63dpszmvmmqyzy3ids1jx8pa")))

(define-public crate-arm64utils-0.1 (crate (name "arm64utils") (vers "0.1.1") (hash "1lifjb7yjy7qgnpnq18p7gmcbilhy0na9aj95gj7jxmix5sqzx8i")))

(define-public crate-arm64utils-0.1 (crate (name "arm64utils") (vers "0.1.2") (hash "0hc1zv3cglx1ih39n23g70nhhfws0vd228q3ikyr0c6gslbjbbbm")))

(define-public crate-arm64utils-0.1 (crate (name "arm64utils") (vers "0.1.3") (hash "1z8szpmz10hdxfqny3hhpwdy76h3gcb2p3zaxgh2i3hlqdz8vzzq")))

