(define-module (crates-io ar gl) #:use-module (crates-io))

(define-public crate-argle-0.0.0 (crate (name "argle") (vers "0.0.0") (hash "0x6mw9r6qjvaf4lnyym0gbsf75zgpxqjxk2iiymf2pij0hrwpgvv")))

(define-public crate-argle-0.1 (crate (name "argle") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "14vvsv405zkm7vzz6rpwjqyy1gx89g6zssr7v9snqn58fvy6j459")))

(define-public crate-argle-0.1 (crate (name "argle") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bmq0vc43fkm7a8rx00m5kk6w9nnddlhk2r55h1k4rr7bz4n4387")))

(define-public crate-argle-0.1 (crate (name "argle") (vers "0.1.2") (deps (list (crate-dep (name "either") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pwrnzh5is2f405zpqs92p3g8nc5rvmhpwzgfg8i8973r16lk638")))

(define-public crate-argley-0.1 (crate (name "argley") (vers "0.1.0") (deps (list (crate-dep (name "argley_macro") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1859rb65505ammi40ldhl669g0hmllig591k1lrk51x0rrnk9krd") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:argley_macro")))) (rust-version "1.60.0")))

(define-public crate-argley-1 (crate (name "argley") (vers "1.0.0") (deps (list (crate-dep (name "argley_macro") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "022ggp6zrmsfbzkcs0a0ki16arzhpsknwmdvw234h6fx571pkf6f") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:argley_macro")))) (rust-version "1.60.0")))

(define-public crate-argley-1 (crate (name "argley") (vers "1.1.0") (deps (list (crate-dep (name "argley_macro") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1n5rlsd2rafc8czlx2jwwjbq8wlzc67r0fsjcxy2jrn34933svsi") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:argley_macro")))) (rust-version "1.60.0")))

(define-public crate-argley-1 (crate (name "argley") (vers "1.1.1") (deps (list (crate-dep (name "argley_macro") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1155xrb4s2zfaqfbbvai13392kjp40hhigcrl0hwl99rjiax7y99") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:argley_macro")))) (rust-version "1.60.0")))

(define-public crate-argley-1 (crate (name "argley") (vers "1.1.2") (deps (list (crate-dep (name "argley_macro") (req "^1.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "11q0p83b6a8y1zjzwjcdhpfh3f26vbsagvn7ijlsg8fxx93y2xcr") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:argley_macro")))) (rust-version "1.60.0")))

(define-public crate-argley-1 (crate (name "argley") (vers "1.3.0") (deps (list (crate-dep (name "argley_macro") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1") (features (quote ("unstable"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("process"))) (optional #t) (kind 0)))) (hash "0ay1y11nqlgslx9b2aqw2314c8qgv5i667rfbg9xkh3zb01xhvcd") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("derive" "dep:argley_macro") ("async-std" "dep:async-std")))) (rust-version "1.60.0")))

(define-public crate-argley_macro-0.1 (crate (name "argley_macro") (vers "0.1.0") (deps (list (crate-dep (name "delegate-display") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0p1cm2pg29my7dg5wvln0iy4ydrlz89fnc0rnpqrcgjifbi27fxh") (rust-version "1.60.0")))

(define-public crate-argley_macro-1 (crate (name "argley_macro") (vers "1.0.0") (deps (list (crate-dep (name "delegate-display") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0bh1pgmd6jd0xakhsmwd6ivbphn36h3f7qfqwcxcmvy7nvj8rm57") (rust-version "1.60.0")))

(define-public crate-argley_macro-1 (crate (name "argley_macro") (vers "1.1.0") (deps (list (crate-dep (name "delegate-display") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "001nd5m0pd5px5xxqlmhb4kj6qxgy89hgviys52cics9li6bb035") (rust-version "1.60.0")))

(define-public crate-argley_macro-1 (crate (name "argley_macro") (vers "1.1.1") (deps (list (crate-dep (name "delegate-display") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dw7n103h92sj4rv419bl6m2b1mdmf01b9jr85rrcyf3b9z841sx") (rust-version "1.60.0")))

(define-public crate-argley_macro-1 (crate (name "argley_macro") (vers "1.3.0") (deps (list (crate-dep (name "delegate-display") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0a2x3l0jrmm7kkkjvi8nz5an1ryd7q04k0zswr0zrhw3yb6qfqf5") (rust-version "1.60.0")))

