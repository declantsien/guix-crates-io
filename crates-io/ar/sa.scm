(define-module (crates-io ar sa) #:use-module (crates-io))

(define-public crate-Arsalan_Crate-0.1 (crate (name "Arsalan_Crate") (vers "0.1.0") (hash "1846msm240f487ff5mqq9ad5mgw1isgprxvcn2cxhjn8ggbchrsn")))

(define-public crate-arsalan_fun-0.1 (crate (name "arsalan_fun") (vers "0.1.0") (hash "0nqniawgam4540ayvnycj03bwll22yx6sm348nh35s74rf4sk6x9")))

(define-public crate-arsalan_function-0.1 (crate (name "arsalan_function") (vers "0.1.1") (hash "03dz7gm0dq0qzd1fm10j27a6qpbi5hzksf87vhjnhbgijjry1yrr")))

