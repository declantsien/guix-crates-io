(define-module (crates-io ar px) #:use-module (crates-io))

(define-public crate-arpx-0.0.0 (crate (name "arpx") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "03mfqqbi1ns8wa61bqbrcdasi5gsw8iwa3bvb86px22dx88gpb0v")))

(define-public crate-arpx-0.0.1 (crate (name "arpx") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0vhi7czx2597vhr6a9if2z4nl8j2413r85jjmnhp401s2knn3lh7")))

(define-public crate-arpx-0.0.2 (crate (name "arpx") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0gpsnhb1ybc3a1d80jdxnqw0990vxd1d9rvc5qlgv309if1yw6k4")))

(define-public crate-arpx-0.0.3 (crate (name "arpx") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0jmggjpd8n5hxsrlgc8r94rfjfv8i3yg9z8cw9cqkxi2ck0gqiyx")))

(define-public crate-arpx-0.1 (crate (name "arpx") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "08qzqvryx51ljnw8dg3kx5kjhlgnjlrpp360j6chq3zcbgyshix0")))

(define-public crate-arpx-0.2 (crate (name "arpx") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0dbn9xha06yjjbayqkc90pkpv0cxhrz5whyaq1gcahsf348ch6zv")))

(define-public crate-arpx-0.3 (crate (name "arpx") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0j1si7zl7nj1rjxj79f0wafbvqnflpx79k90xsa3ajikxhvhnxl0")))

(define-public crate-arpx-0.3 (crate (name "arpx") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "1kjnrqsdyc8dpcfg5yvx75k0pvbgksc4pq76fls9hdi5hhb69bmh")))

(define-public crate-arpx-0.4 (crate (name "arpx") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "06ax50r3yvv639xks5aiv34xdkjl5q3wz5hfi54aghz6rdlny1sp") (yanked #t)))

(define-public crate-arpx-0.4 (crate (name "arpx") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "09xdwyywhyjzddrnisnhiggf0whd4c278in2mlc7w2ddqlx0f8bj")))

(define-public crate-arpx-0.5 (crate (name "arpx") (vers "0.5.0-beta.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.55") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "arpx_job_parser") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.14") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.23") (default-features #t) (kind 0)))) (hash "0ky7k2kg1vdi4fwa1sbjld6gg19ribsmrzfmhha3s2qjw14qyz0r")))

(define-public crate-arpx-0.5 (crate (name "arpx") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.55") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "arpx_job_parser") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.14") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.23") (default-features #t) (kind 0)))) (hash "1dhkqmivbhi0ziynl8706xp07bx326ab33lskljq2pzlv85qwxfl")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.0") (hash "0710ds1pkjdbrb72kkbavbjskf8dn502xk0jqnx3m6k36d594xvh") (yanked #t)))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.1") (hash "1vbiifxa0gp1akgk78sjx21djx8b7qq6c2q6m5ffkhg3249lm0nv") (yanked #t)))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.2") (hash "0cdxnqpal6s63gn3jqmi1rcq2zr8rpsqrpr8d8hglzy7722i6017") (yanked #t)))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.3") (hash "0r4xfsmm2csj973p20bjlf8y42ikqfaanzv3n6rvffn58c5m1xgd") (yanked #t)))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.4") (hash "1k9cfgafv0kbvh86q3rhhszid6m93wddwl1pcizrsdk3p61ii47p")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.5") (hash "0aw8ihj68x3p091wpw2kwi1j2gddbndi61fmiyf38jwa6spmkgwh")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.6") (hash "07gx8hcv4vlsmk2yrcn63sf9min2ndcygxlqnrp76hlxdaa77myl")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.7") (hash "1w04k9ggip3w9wnp7yg04m3w2iwn0lsa58nnpqkz896wswiaf1p9")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.8") (hash "0zv00xgkjfcaphd276dz8p7y26d7d5rnfsm4v04yznqgdq2rkl0z")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.9") (hash "1pr1s6vxxk4mvx2zlhwj6zc2q0r1d4acwcbg5x84s8akxvrkgpq1")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.10") (hash "1zjb0gw2ir4vn4nsnc152jn6vv6jm0hqm62giiy629llnh23f3hn")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.11") (hash "01shz9d54skkkmn7di4ayz2ghyrrc5ypnpv35ij650f0yw639y7m")))

(define-public crate-arpx_job_parser-0.1 (crate (name "arpx_job_parser") (vers "0.1.12") (hash "0ls4q9xzzvhl25gi3mzsl9xp3rnjxdn0cfc3sy2606zc27ahgdy5")))

