(define-module (crates-io ar ts) #:use-module (crates-io))

(define-public crate-arts-0.1 (crate (name "arts") (vers "0.1.0") (hash "013sywfrffb0q9vvjiq1fphksja1j3hz80jvpyac5mpq0zf19jv3")))

(define-public crate-arts-1014-0.1 (crate (name "arts-1014") (vers "0.1.0") (hash "134d672x7dw1qqqcjjxbvgpf2zrjmbjhdipjm6hrdbjkhjyv3g9m") (yanked #t)))

(define-public crate-arts-1014-0.1 (crate (name "arts-1014") (vers "0.1.1") (hash "1gb1cn5fg26z3zkbpag68mkvsszj838569drbxxi6yalmj07ak0y")))

(define-public crate-artsy-0.1 (crate (name "artsy") (vers "0.1.0") (deps (list (crate-dep (name "take_mut") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "05p6qw08424995gc5hn3xm5vvfh4xhvj0m0fhp7ri1fhf1s8z37f") (features (quote (("node48") ("node4") ("node16") ("no-simd") ("default" "node4" "node16" "node48"))))))

(define-public crate-artsy-0.1 (crate (name "artsy") (vers "0.1.1") (deps (list (crate-dep (name "take_mut") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1v6znx6znks81w4qg52jmjwd8d42pmxgagv110am5kmwsl15migv") (features (quote (("node48") ("node4") ("node16") ("no-simd") ("default" "node4" "node16" "node48"))))))

(define-public crate-artsy_bitsy-0.1 (crate (name "artsy_bitsy") (vers "0.1.0") (hash "110wr4jzilx7gmyvj50sw4r45ljvz2zvsvbwz627ib50ybbk1hdz")))

