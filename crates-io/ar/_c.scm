(define-module (crates-io ar _c) #:use-module (crates-io))

(define-public crate-ar_cuil_cuit_validator-0.1 (crate (name "ar_cuil_cuit_validator") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0mp1hfmnq0hyg8kxldwrs1iaj2516ll3f47ibyli2wf502midkyx")))

(define-public crate-ar_cuil_cuit_validator-0.1 (crate (name "ar_cuil_cuit_validator") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12603y68igfzg66gw1n48pvvkb4pnf1givj767xcf3ya53ik0jlx")))

