(define-module (crates-io ar ff) #:use-module (crates-io))

(define-public crate-arff-0.1 (crate (name "arff") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d90lwrmz7clsgxggbwrb8rcbl2asrml8piizfwf5zplv9ws3hlc")))

(define-public crate-arff-0.2 (crate (name "arff") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1an5p9j51aicp3rwr4dakrg2i3smgdiwh1g5kdrr8rpkny64jhi8")))

(define-public crate-arff-0.2 (crate (name "arff") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "00wg7ksc87wj8xkbk2v39gji47qfnz4f8njp3mv02qk2mlfxpphz")))

(define-public crate-arff-0.3 (crate (name "arff") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "0k8xicsbbv82wmadarwmz9d8ramx89yxafnrs0ws50ddxlxcvz7z")))

