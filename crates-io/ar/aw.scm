(define-module (crates-io ar aw) #:use-module (crates-io))

(define-public crate-arawasu-0.1 (crate (name "arawasu") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.90") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0x712mdidvscf5g2j7mmzj9kks66by1js8bqy2acld39h52bk061")))

