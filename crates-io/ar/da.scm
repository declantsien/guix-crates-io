(define-module (crates-io ar da) #:use-module (crates-io))

(define-public crate-ardaku-0.0.1 (crate (name "ardaku") (vers "0.0.1") (deps (list (crate-dep (name "devout") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "pasts") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^2.0") (features (quote ("universal" "cranelift"))) (kind 0)))) (hash "0zrpm1nvn3s2qr49svr70k13qgn6nw6rpxwdqpmngzgi5i6cwn4s")))

(define-public crate-ardaku-0.1 (crate (name "ardaku") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.19") (kind 0)))) (hash "0j5rqij2i0wv66vzbcvk0ldlh0zvk8zvmf5nzfilmgfjzbr7g8j7")))

