(define-module (crates-io ar w_) #:use-module (crates-io))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.0") (hash "1ny0kd3w7pqcrc4v5cpjpzv60fb953qibj4nn5f9x9a0i4560x0r")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.1") (deps (list (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "1ryaglnyl8ja7pspd74faznxs2hvpy8kdl299633apaagf2nrbxq")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.2") (deps (list (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "1k4mgx0c9in7rjxv9ckr9q283d1plcl74i37k9pfhqh821xcx6n0")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.3") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "060zab66ya7z1zwpnq12i5cyicnjgqbvb0wlhcx7970c3l3rfhax")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.4") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "011a4jng8qsiis2bi17qali3xl9wclms8ihj1c037k5ci5w26hya")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.5") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "0f4rks8i7c7k6ahxxrxdg8i4p2r1229z73xygam5qhgmkhysvz6q")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.6") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "0xgivcf4zlkcrwm595kifk94crwn8yrz77vg7fn6bwhzlcypqb8v")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.7") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "18gpsmnx82q53acrz0js84r1jkn69h0h87r26xi9z72vvy2a0m9w")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.8") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "0b6bxp6a8vrhfmk76zpy9x70dm1v15kplbzwpgqgkn5bp1bx045j")))

(define-public crate-arw_brr-0.1 (crate (name "arw_brr") (vers "0.1.9") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.27.2") (default-features #t) (kind 0)))) (hash "1gx5c3ndwzgilm4ajbh9nl5hmfz573r0b0gg1sikn3py26zr80mv")))

