(define-module (crates-io ar gh) #:use-module (crates-io))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.0") (hash "1am8inhq45dyw1lcylz6zifm3wp7dj703i72ngcdyl5wwn24yphl")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.1") (deps (list (crate-dep (name "argh_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1h34raci62111chgbgyg9qk2pdcws8m45bgs2av7l9xdvdafzw57")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.2") (deps (list (crate-dep (name "argh_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "017l7zc1ggc9y7rrfd4f933jdq3c5xgx4ps7z11h4y1fldmmwphq")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.3") (deps (list (crate-dep (name "argh_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0m01hbnbxj2h7wb6flv3qmy4ky64c00nw1k9sh0dgb7c9ki7f66a")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.4") (deps (list (crate-dep (name "argh_derive") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1gi2qp6mfc9gz2sy3w1fq8fpz3d1bvx1gmmiryicgkc7iw42yyci")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.5") (deps (list (crate-dep (name "argh_derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0gij9511j75wkvilhw2nq8d81an6c9pbnwlyilkwa5xw96jifwrf")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.6") (deps (list (crate-dep (name "argh_derive") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1gy9y69d38q7f5147kj823swgggc3m30x7z2z1lrjpwpsxncf8zh")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.7") (deps (list (crate-dep (name "argh_derive") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0n0r1xbhrn86r94zph2np4djdr65cp127c2sp5nbkyidv62ivd6v")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.8") (deps (list (crate-dep (name "argh_derive") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 2)))) (hash "0qks2z9qvxj72wjg3fvmfm8sjcr3nkydlq877h17wx20gsmf9rx7")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.9") (deps (list (crate-dep (name "argh_derive") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "1qxb1jn3y1c3p6by1l0lhy96yjz5nrh6hf9irknxax10zpnfsxf3")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.10") (deps (list (crate-dep (name "argh_derive") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "0gprrqzjigbibhvbhy7szaf6lqv4xnsif0kga9svz5llxfbpc9db")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.11") (deps (list (crate-dep (name "argh_derive") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "0s9r05i25kpj5bqqcinxjc67hjkb2k8gkm05yqc3ym6my3z7c7rh")))

(define-public crate-argh-0.1 (crate (name "argh") (vers "0.1.12") (deps (list (crate-dep (name "argh_derive") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "argh_shared") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "06fjmac07knqw7vahra9rkbfrrsv31yrqhf7wi623xvzjq3bmxbs")))

(define-public crate-argh-demo-0.0.0 (crate (name "argh-demo") (vers "0.0.0") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g4llbshr7n0zaffdwzjy2v31ys4s4nj865bybc79yj2kvqq4jw4")))

(define-public crate-argh-demo-0.0.1 (crate (name "argh-demo") (vers "0.0.1") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wg8bdja3mq97cy7s8dkinaap1ahwmw02g50wbsn9xrfxbv8cy9i")))

(define-public crate-argh-demo-0.0.2 (crate (name "argh-demo") (vers "0.0.2") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)))) (hash "1x51m8rknpyqza4mxa0vgccircqn5fkjsghvilr98gvkq17c0r15")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.0") (deps (list (crate-dep (name "argh_shared") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0azgr1crq5jqcjlixkw4awdkcbnrdh1yn4g4bi80hq5xdd04l770")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.1") (deps (list (crate-dep (name "argh_shared") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r7pbqn397vabahcsfkyaa7yrlvrnb10h5w0rcmr7z231x71jhp7")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.4") (deps (list (crate-dep (name "argh_shared") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rxhd359d4gpac9524xx8qz13wiq3vilqnn95m0pgm0a2860rsy4")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.5") (deps (list (crate-dep (name "argh_shared") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g4d7fcbz963r9rzdv9p28n41b52rfq38i1m5r2fjlak6x19r530")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.6") (deps (list (crate-dep (name "argh_shared") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13qz9i9frdjl1v9aqw5b2cs7wn3h34x2xkpsi9wcl1hcpjd23ba8")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.7") (deps (list (crate-dep (name "argh_shared") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1df3ipaznhr76cw7i5m72xzqysxcqsavsl0s6fmxcza9yl7gfsdy")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.8") (deps (list (crate-dep (name "argh_shared") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g5l39simwnkrrkig00ksnxm8r74p84xa6sjwps18r7dyrzvvwk9")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.9") (deps (list (crate-dep (name "argh_shared") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "00vszcr90w66hrnj48b9ycbdf4q01wml2fd006zs2281p1wk80da")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.10") (deps (list (crate-dep (name "argh_shared") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19jg69pr5bjr92q2rmiaavcb17vc23dy369ky0qk61cf539xp0mk")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.11") (deps (list (crate-dep (name "argh_shared") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "08cwgrmrpn3s504xl2zz6l63m5xrc2ya636z1y18ihqzi7fqid8y")))

(define-public crate-argh_derive-0.1 (crate (name "argh_derive") (vers "0.1.12") (deps (list (crate-dep (name "argh_shared") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0ynq2f2f05ybhmvg5y4m1kdfihw4jsq3bnq6gp32yykbvzp0mpsn")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.0") (hash "0havs0w9ilvhq9xsmqmb3dwl3xjgcvssp66h5wvmn57pj72h0nhj")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.1") (hash "1x8zc5ckp7iqd54hbln8fyhz724q4s53833awf8qaxva4zs6ifp1")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.4") (hash "1cacdyl980sarjfx32v6d57snk76c7dvak3mp7fvlvc2r5n367vq")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.5") (hash "006aa509w15dyhbkr5bxicbfkfz61q9i57ybcb8ibx5qkh0ynqca")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.6") (hash "0crzkzr4mq9gyys3m0idgsfwwrwd4dk70scp7rspvb2fmgd01piq")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.7") (hash "1b1f9g3mm881bhy4anmr3cjbfjs7jq0pznbw20v1pai8za0c7y76")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.8") (hash "1npc0k84i7n0dvs5fymmmdhsqiqz9wcmjcngw6iwgbvdk2c3n9a7")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.9") (hash "1adnxljnfvgiqhrvzbizq7nz59zry1l5m1g0ca1629qqxjxpb7ql")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.10") (hash "0vshhn6znb1xywvfaj5x6gfpkpf3h9dwrrxvzwvksplnblar9jv4")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.11") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1h02zg29i18j5s8idr2w43dp7jwhrgc98v4xvy95rla33zgf120f")))

(define-public crate-argh_shared-0.1 (crate (name "argh_shared") (vers "0.1.12") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cbmf3n5fd7ha014m303f4bmsmj0v84an4a1rh77d9dx868z74sn")))

(define-public crate-arghsh-0.1 (crate (name "arghsh") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0j12mlwzzw04pcb1ljk4cknz7kkg7azskfq9dack37d62y88y62z")))

(define-public crate-arghsh-0.1 (crate (name "arghsh") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1025mfspcqnn77ilfhrryqq2y1cjwps7r49k4nrdlwqhbangac6c")))

