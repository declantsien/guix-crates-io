(define-module (crates-io ar m7) #:use-module (crates-io))

(define-public crate-arm7tdmi_aeabi-0.2 (crate (name "arm7tdmi_aeabi") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ja9ksggi90ns36wv4rkfqdirzm9mdsisnbsk2bzk43ah52lvwbh")))

