(define-module (crates-io ar is) #:use-module (crates-io))

(define-public crate-arise-0.1 (crate (name "arise") (vers "0.1.0") (hash "07k19q3x1hj4z8r92chfxcsb31fh36scxy4gfkmcdnbg5szzwm9b")))

(define-public crate-arise-0.1 (crate (name "arise") (vers "0.1.1") (hash "140pyx3sa8pkxq3i74zgg16haqainkdp14ca0h3bga0sp3zhii7p")))

(define-public crate-arise-0.1 (crate (name "arise") (vers "0.1.2") (hash "069c2a49v8vvxb1c26jcrw660jdj5776mglf84n4pv6afrvzdkg0")))

(define-public crate-aristeia-0.1 (crate (name "aristeia") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0lwib70amhrwpmksym6dphqandg4kizjcmfg967bvj9rl2ynkgza")))

(define-public crate-aristeia-0.1 (crate (name "aristeia") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1dvfgikjcf3f9wf8frk4r1jczw38106g6g0m3mbx996457qy6wcw")))

(define-public crate-aristeia-0.2 (crate (name "aristeia") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1hqqnv678kb62y292danndj4qgk3jbhxw9r6xc21872kz04minqd")))

(define-public crate-aristeia-0.2 (crate (name "aristeia") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0gvhc1fx6mas1cssgay7d9xxkklv7hbqm4p9jf47xql3gv4gmhbw")))

(define-public crate-aristeia-0.2 (crate (name "aristeia") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0qx3f6wy1lb757safp1b03rsaa9hfhbblayg39cpb68kczx72ra0")))

(define-public crate-aristeia-0.2 (crate (name "aristeia") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0cx4frs07mz9njfzyk2ib3fa8wwkmcvxg7pxbh697dcllp0pisrw")))

