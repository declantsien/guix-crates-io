(define-module (crates-io ar di) #:use-module (crates-io))

(define-public crate-ardia-0.1 (crate (name "ardia") (vers "0.1.0") (hash "1y73wq984bnkpfl7gf58gllmhpzn7fadr0h9g8kfyjc9dd4rbsn7")))

(define-public crate-ardia-cli-0.1 (crate (name "ardia-cli") (vers "0.1.0") (hash "0y6v2s0cclyixmavjfxznx779zadnq3fng44lav2a6xvxs2ry50i")))

(define-public crate-ardia-tools-0.1 (crate (name "ardia-tools") (vers "0.1.0") (hash "1f35vqnn59fb9km3gjfvbihgki3wqay5gq9g14pal9lhnc3syclx")))

(define-public crate-ardic-0.0.0 (crate (name "ardic") (vers "0.0.0") (hash "17l35h7pxijz98jrwp3m5g5pc3x8wgwrid02lfiqwg0vynrjqrnf")))

(define-public crate-ardic-core-0.0.0 (crate (name "ardic-core") (vers "0.0.0") (hash "106nzk5fppyz30dmiy6pacb0zlfkzfl6fa99q0n83fzhnnwvd8fy")))

(define-public crate-ardic-macros-0.0.0 (crate (name "ardic-macros") (vers "0.0.0") (hash "1bj902pkjxwb6zx3bbbnfavkw9vncczzb21pdvdf01pcjk68d0hx")))

(define-public crate-ardic-macros-0.0.1 (crate (name "ardic-macros") (vers "0.0.1") (hash "0rnv7yii2gm87any6miksdqmbdidyy12ck1iap08kbz33ll9igp4")))

