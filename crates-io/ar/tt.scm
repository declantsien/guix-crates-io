(define-module (crates-io ar tt) #:use-module (crates-io))

(define-public crate-artt1-0.1 (crate (name "artt1") (vers "0.1.0") (hash "0ycrlxc89d2lnqagr46hwvrvzy13cv9cvl2vgj72i31diaqnc15a") (yanked #t)))

(define-public crate-arttestevghedulib-0.1 (crate (name "arttestevghedulib") (vers "0.1.0-alpha") (hash "0ynlviffihppqz2y0qbfr8xf8skn4yxj9g07hfzf4x9jvcx4yymc") (yanked #t)))

(define-public crate-arttesting123-0.1 (crate (name "arttesting123") (vers "0.1.0") (hash "1gnamsy8m7lcpzizyz0zxcfx2jz9ff0xhjn2nz4fw2ac40rzpg6f") (yanked #t)))

