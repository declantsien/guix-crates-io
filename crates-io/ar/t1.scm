(define-module (crates-io ar t1) #:use-module (crates-io))

(define-public crate-art112233-0.1 (crate (name "art112233") (vers "0.1.0") (hash "1g281n3i1ly8gcqmsyi58156kjl5ij05fc87jsygnywprd1i63d7") (yanked #t)))

(define-public crate-art123-0.1 (crate (name "art123") (vers "0.1.0") (hash "1k3zqf9ra869np5328aq42ypzl9z4jr9jx5kzpaqx5gsqqiz81ds") (yanked #t)))

(define-public crate-art1545-0.1 (crate (name "art1545") (vers "0.1.0") (hash "0sj7j0xcpz09lna6cx6w91jhfsd0c1mh6x08iikkqkxzzv5r7lmn") (yanked #t)))

(define-public crate-art1545-0.1 (crate (name "art1545") (vers "0.1.1") (hash "0hc42d5qa8mmbhb2fg85qyxmfz009p6krrdc1gzryfb5q3r9gbis") (yanked #t)))

