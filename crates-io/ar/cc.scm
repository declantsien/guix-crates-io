(define-module (crates-io ar cc) #:use-module (crates-io))

(define-public crate-arccell-0.1 (crate (name "arccell") (vers "0.1.0") (hash "00w86lc5yr2ww5gkm0nyc6x4sdahflsi3sj6cmjykm7kw5wm2apa") (features (quote (("experimental")))) (yanked #t)))

(define-public crate-arcconfig-0.2 (crate (name "arcconfig") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1nx70i89s7lj3zcrjn93pj50gx1ws0c0pcv6hdn4dqjcsjvy15yj")))

(define-public crate-arcconfig-0.2 (crate (name "arcconfig") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0619l3762kwplc3dmk9swz44yk0wav697gpfbisnv2kbhz30gsj6")))

(define-public crate-arcconfig-0.3 (crate (name "arcconfig") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "111mfpzniwhgvzqqq1f39zd8mbhbqp6bzi556dymszgmqhwsirjb")))

(define-public crate-arcconfig-0.3 (crate (name "arcconfig") (vers "0.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1899fzajc3llyn2q36rwz424kb5rhhpxz5np3lv6nvjg2dgwq4bx")))

(define-public crate-arccstr-0.1 (crate (name "arccstr") (vers "0.1.0") (hash "1abiddvzcgaslf1f2p8r1v7kjdya0wyc3crysi527y3lmzkliwk1")))

(define-public crate-arccstr-0.1 (crate (name "arccstr") (vers "0.1.1") (hash "0nfmyz7vy0m5d3bz32jhszwlssi3n0f6kkj6khgsklkl1qzp4i35")))

(define-public crate-arccstr-0.2 (crate (name "arccstr") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^0.9.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^0.9.10") (default-features #t) (kind 2)))) (hash "0caxn56mbn7fxfj8vs8av886w980vigg8jhi59vbww4pagyxl7nq") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-0.3 (crate (name "arccstr") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^0.9.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^0.9.10") (default-features #t) (kind 2)))) (hash "0hqxg1g1k8njnv253h1g4nlxlb8y2f3957d9srjc3zqjqp4dpmfp") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-0.3 (crate (name "arccstr") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^0.9.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^0.9.10") (default-features #t) (kind 2)))) (hash "04lzfhygiigb249nqcirmjbhgva4f3838rfjh6y7739brbbbj2h6") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-0.3 (crate (name "arccstr") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^0.9.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^0.9.10") (default-features #t) (kind 2)))) (hash "0z44n4hlph7apgqsmcfh6rib84rxk6kihlddiaywf20l1jhc2qvw") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-0.4 (crate (name "arccstr") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0kmp17hrljrwp3ga0z2wqpirnz4raypd225ypbyd9jm03fwxmhrg") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-0.4 (crate (name "arccstr") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0lb4w7kd801ip10ypwxrbbzl31j0cdw092k48if8m31bv4dd5ha2") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-0.4 (crate (name "arccstr") (vers "0.4.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "05wqh371mm8c9bmgf083j7wd5zrpbrwirx0f3xdrqz7g7vsrfzps") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0xw181gnckdz881b4z1vr7r6wmm8mb8fff3yxfl31xaa776znkv6") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1840xicmwcm76p0h5i602r9mllrjcim7qkwp5m07kwrdv9h1mqx0") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0454h0zyh0fn3k32i3zwram3s8jqxv6ag51bf5a3r5m3bzjd097s") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "190pxh054gyvc164l8q3jr9iihylic4j52pqn76y3graiqrddf3i") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.0.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "17lpd7l876nkfgrckwc4ngymwvjnm941flp3xlm622qpv00y7xfh") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "15np53xvar8s7ipxpsi2nfq9x1qihh8azvb16wvbsksp0c13wkdl") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1x35ya05ysdklbqfi4fm8vwzxlykw4qxdm6ykkixb3jlmzfjwylm") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mmcf85gnaxlcfwgj77hax80b893w4n2n5zcw8gfp00dy3y5rikp") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0v2plz52nwm71jvd328kilqnwdgv57wmdp9ybx0dxw6xx99l269s") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "12ldr6i5qwyx2d5kr0b2kw00l7rcfp2kiwsfcl0rl6j0sfi4qpbz") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qbfgc6r385ipyrn0rkl8gdmsvfjz75pkl7f3nr1s0xab7b1siw2") (features (quote (("default" "serde"))))))

(define-public crate-arccstr-1 (crate (name "arccstr") (vers "1.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "02c42x7mdymigy8bgz4fr4mx2f30m5nc0qaf8jq0xbv913admh6n") (features (quote (("default" "serde"))))))

