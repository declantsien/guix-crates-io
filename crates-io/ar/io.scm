(define-module (crates-io ar io) #:use-module (crates-io))

(define-public crate-arion-0.1 (crate (name "arion") (vers "0.1.0") (hash "0gy9g5rj32ywmgy57hjc89ppzqlmb4qs6ypqq70cq7n0pg9ykvmv")))

(define-public crate-arion-0.2 (crate (name "arion") (vers "0.2.0") (hash "148rbwla9j1nc0skj9bydl2a45kdjzka6g8v89pi2a8fn8c3b4pq")))

(define-public crate-arion-0.3 (crate (name "arion") (vers "0.3.0") (hash "0bv1ifp5h6ck2zkrijn9rpcnv0b40wd5bsvsq1hhsla8x0m3y5qj")))

(define-public crate-arion-0.4 (crate (name "arion") (vers "0.4.0") (hash "1vwx2gc8nbzgh9qldd5r8sph55fkxxvqsvxfvmwxafkf702kfx7r")))

(define-public crate-arion-0.5 (crate (name "arion") (vers "0.5.0") (deps (list (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1kc88lygf7s3ckkiw633nixha1yvk40n1k8n8n0857lmc7fph8lq")))

