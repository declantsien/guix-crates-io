(define-module (crates-io ar kn) #:use-module (crates-io))

(define-public crate-Arknights-0.0.0 (crate (name "Arknights") (vers "0.0.0") (deps (list (crate-dep (name "ndarray") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1gr5qgwfxvah68ivlnj3k3lr07l9wa9c3ymxs91w79r96zar739z") (features (quote (("default"))))))

(define-public crate-Arknights-0.1 (crate (name "Arknights") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "01wlfqy6v2rx20d8j1bh4shnc1wal2jkmfa9mq1sjj2rl8kax1kj") (features (quote (("default"))))))

(define-public crate-Arknights-0.1 (crate (name "Arknights") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0r70032j7hpjb7biwf0cdkknrbim23p51ymfnkkrn48myw6rwg1j") (features (quote (("default"))))))

(define-public crate-Arknights-0.1 (crate (name "Arknights") (vers "0.1.2") (deps (list (crate-dep (name "ndarray") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1kljliilg32lanp0lm4ck19545idlkaqslndrabcrn27w88mjgfx") (features (quote (("default"))))))

