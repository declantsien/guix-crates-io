(define-module (crates-io ar gs) #:use-module (crates-io))

(define-public crate-args-0.1 (crate (name "args") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kqaxhxdvm1xgb85hxn6ncg31h71v0jb14s7f2lhxh8cwxp0rdlq")))

(define-public crate-args-0.1 (crate (name "args") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ki2js3a25qy95gxwpwrvccjhc3bdrfrng72xvf0nd0lc9vdi1xz")))

(define-public crate-args-1 (crate (name "args") (vers "1.0.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "01198dbkibaxxldjzsk42fkkmb5hc741xmmvp483fx6pmmsfbyjr")))

(define-public crate-args-1 (crate (name "args") (vers "1.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1770g7cmiw3cnjbs093kymnfss808az5q9bzggqri0s2p3am22i3")))

(define-public crate-args-1 (crate (name "args") (vers "1.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lizrzbhqwna2dz8qw1vqxl5abdmc2s7aqbyykp0s9av414657ms")))

(define-public crate-args-2 (crate (name "args") (vers "2.0.0") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "0hapw98skbldy2drwkx0kna40jisdmc1sjm5mzczfm0by0p135vy")))

(define-public crate-args-2 (crate (name "args") (vers "2.0.1") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "1z2wy8mnq5nm3z22q2xg21qhhdx7kj3ww1r47l9zxp4ws6v4qpn5") (yanked #t)))

(define-public crate-args-2 (crate (name "args") (vers "2.0.2") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "0fvbgqly3hrc1xkv36412qbx4gikn58clmidgzrihphdv2p69iza")))

(define-public crate-args-2 (crate (name "args") (vers "2.0.3") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "1bx71zng8v6bsk9kaf1kxkxp1i6bk6m29gdbwrk99j06jh500ki5")))

(define-public crate-args-2 (crate (name "args") (vers "2.0.4") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "0xv9ps04sxylz5yy17n1y9qf7zgw63ph5ningv2z8hwdb80g7b95")))

(define-public crate-args-2 (crate (name "args") (vers "2.1.0") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "1rwjqv7r0q4kg3mbs3j7844w8ascg35imrjmglkprpd0h1f2j44w")))

(define-public crate-args-2 (crate (name "args") (vers "2.2.0") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "02wpdlzdkqh0qg49yiv8jj9pwh4dvlhf0mid0df8syqpcln47dyl")))

(define-public crate-args-functional-0.1 (crate (name "args-functional") (vers "0.1.0") (hash "0vrfz9y8p1k6313vw4kj690f8s44rnajim13dpar50fyfj41i8x7")))

(define-public crate-args-functional-0.1 (crate (name "args-functional") (vers "0.1.1") (hash "0fc09yqrxyj5ak809ax2zf32badf6xf6ji4x2blxg92jrj1pihsp")))

(define-public crate-args-functional-0.1 (crate (name "args-functional") (vers "0.1.2") (hash "1pllah8dw734d3468bgp1bfa23qa63snhq2ws1bkan343fnmf233")))

(define-public crate-args-to-json-1 (crate (name "args-to-json") (vers "1.0.0") (deps (list (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0j29b2x7p0551c471qnb1pp4lmxa1wfkqr1ivnk0cms1bviqraj6")))

(define-public crate-args_flags_1-0.1 (crate (name "args_flags_1") (vers "0.1.0") (hash "00phvr2fcwicyjbc6ca3ffj0cl6cpr69wnv6f8n0c3vmxdlnndlb")))

(define-public crate-args_flags_1-0.1 (crate (name "args_flags_1") (vers "0.1.1") (hash "1n9l843nycqx99bwjlwmn8m9k2imzwb67kbx8bzi1lmd1v2a17sh")))

(define-public crate-args_flags_1-0.2 (crate (name "args_flags_1") (vers "0.2.0") (hash "1xjck6k2lsc23kmahzl70hylryp7p5l6wcy12i4dc2xa6vga6lrp")))

(define-public crate-argser-0.1 (crate (name "argser") (vers "0.1.0") (deps (list (crate-dep (name "argser-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1c62k9m4z45qyw88nbmn63h1wk2mbjxc3yq6rh11462k79xqdd8c")))

(define-public crate-argser-0.2 (crate (name "argser") (vers "0.2.0") (deps (list (crate-dep (name "argser-macros") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yfa9avrfrh3f4l4zxr0fd42n5l5xlg93qy91vbpixr37w2hqpk7")))

(define-public crate-argser-0.2 (crate (name "argser") (vers "0.2.1") (deps (list (crate-dep (name "argser-macros") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0px1lpxfp6wydckadx6j32151yy7f362z0nwcw4anaff4adx0mxl")))

(define-public crate-argser-0.2 (crate (name "argser") (vers "0.2.2") (deps (list (crate-dep (name "argser-macros") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "12kvcdmh3ni4nsgpff2c37rmzc3skjakqfm4gk3s98jkavq7yn2m")))

(define-public crate-argser-0.2 (crate (name "argser") (vers "0.2.3") (deps (list (crate-dep (name "argser-macros") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1ypvlwxrnm9cn6ryh14hzy1r7b1n9237mw336d9q3xv50y60s9br")))

(define-public crate-argser-macros-0.1 (crate (name "argser-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gf6z50rzj2wdjqprbxh6bp4vs240wjpx551gq6dbsyg209ix66n")))

(define-public crate-argser-macros-0.2 (crate (name "argser-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lz03z7qpgyfgvfqsgv4mcb08yyi453m05pjah705vv47ffkysbw")))

(define-public crate-argser-macros-0.2 (crate (name "argser-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1l6b14s5xcvca7f3yw1fpm54lgncachnjwckidwc975mgqifyy21")))

(define-public crate-argser-macros-0.2 (crate (name "argser-macros") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13p7yk9zr9hsz467ywssd5rjx92zz21jb0sgl69hg3yx8a40jjj1")))

(define-public crate-argsplitter-0.4 (crate (name "argsplitter") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1ra1701bga2y2jyzp6wj8lfv87krfykglklbhgvdpclds9f01d1z")))

(define-public crate-argsplitter-0.5 (crate (name "argsplitter") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1ylcpi6brmc4p7dgd8jrqx2d7kjkx9yyznk5fiw14lm7z19linzg")))

(define-public crate-argst-0.1 (crate (name "argst") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "11mrgrp6a3sjfqcgzql6c5pdxdimqk7nwqxwb7jgrzgyglmxsqhq")))

(define-public crate-argst-0.1 (crate (name "argst") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1h4s29p8l2apzxmfnxv3a78qyrv4zs9ql1l23cmf859py4dfyzwy")))

(define-public crate-argster-0.1 (crate (name "argster") (vers "0.1.0") (deps (list (crate-dep (name "argster-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1h1kp4dagwlggpr7jk09bwzpl7zzzcf9fsd77x3zb204pqis9vy6")))

(define-public crate-argster-0.1 (crate (name "argster") (vers "0.1.1") (deps (list (crate-dep (name "argster-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1lym1bag7krhp2jkqp4icyqb0bw8kxy68fhmx7cnm06kbdncjmwj")))

(define-public crate-argster-macros-0.1 (crate (name "argster-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02alb89azfd6d2ibzig8hp51q835ggnsd04zvmj28m73hf6kww63")))

(define-public crate-argster-macros-0.1 (crate (name "argster-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0d1v3iv3527nccvic66q6bpjn88m4l7b6jxc116d4a0c6f60p3pq")))

(define-public crate-argsyn-0.1 (crate (name "argsyn") (vers "0.1.0") (hash "0cwp147sbmpg4fvl4nc9z8yg0421vva4c1c1lqq15xb9zkhqcbbf")))

(define-public crate-argsyn-0.1 (crate (name "argsyn") (vers "0.1.1") (hash "05gdlc8zl0s3gsv5rv2dsdxnpyppqd80c6l3861s6wmlqxrisgdx")))

(define-public crate-argsyn-0.1 (crate (name "argsyn") (vers "0.1.2") (hash "0i05x3f1w219155d28cz7gcpajny35a1qywpkbwadk3qhlhdjmz4")))

