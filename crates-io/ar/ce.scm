(define-module (crates-io ar ce) #:use-module (crates-io))

(define-public crate-arcell-0.1 (crate (name "arcell") (vers "0.1.0") (hash "0dllan0yn4qxflv0kh3kbn2gf71kvq17py5ydf6gqn9mav6h106j")))

(define-public crate-arcell-0.1 (crate (name "arcell") (vers "0.1.1") (hash "16mp1dz9vlqcrqbccpwm7rddi61lxs47mjjcnzkslqdai2wzjfps")))

(define-public crate-arcell-0.1 (crate (name "arcell") (vers "0.1.2") (hash "1kq75hvf8k9p1szshkzna6h542n5f43279y6xrz9n4g19rbsmmw9")))

(define-public crate-arcen-0.1 (crate (name "arcen") (vers "0.1.0") (hash "07phr2nwpg0j3dxdhpzgj1ppfrnzrdh1r1ww9rgxf74cip2bb3hc")))

(define-public crate-arcerror-0.1 (crate (name "arcerror") (vers "0.1.0") (deps (list (crate-dep (name "arcstr") (req "^1.1.4") (kind 0)))) (hash "0wzrz21xl1figh8aqad27cqjs35wbhyvwixnhvfg364p20m4zpzf") (yanked #t)))

(define-public crate-arcerror-0.1 (crate (name "arcerror") (vers "0.1.1") (deps (list (crate-dep (name "arcstr") (req "^1.1.4") (kind 0)))) (hash "07lqqs6844vv87kaxdrgmhykcjyzq10ffl53blbikl3mljac15j9") (yanked #t)))

(define-public crate-arcerror-0.1 (crate (name "arcerror") (vers "0.1.2") (deps (list (crate-dep (name "arcstr") (req "^1.1.4") (kind 0)))) (hash "02al2wb8sfla36w16g3adiys34pcq16g9k3kkrqz8vy7mhdcn6h8") (yanked #t)))

(define-public crate-arcerror-0.1 (crate (name "arcerror") (vers "0.1.3") (deps (list (crate-dep (name "arcstr") (req "^1.1.4") (kind 0)))) (hash "0dhnfndpvr6pvka8x3rl75pa0sf0avzfslln00n0z1ib5icrhgrl")))

(define-public crate-arcerror-0.1 (crate (name "arcerror") (vers "0.1.4") (hash "0q3m7dqflxf5fd2wlnhv755wcjsiq23j6hh6lx8l38fqs99lrp3q")))

(define-public crate-arcerror-0.1 (crate (name "arcerror") (vers "0.1.5") (hash "1yig8zsmwrpgppn71xlbzmmfdhin8w39w1p1vzr0sa85a9dj89qy")))

