(define-module (crates-io ar cu) #:use-module (crates-io))

(define-public crate-arcu-0.1 (crate (name "arcu") (vers "0.1.0") (hash "09k4dw70s1fa2idd2309w64cyczgc821ljsk7g8mnzs4ck7yvj7w") (features (quote (("thread_local_counter" "std" "global_counters") ("std") ("global_counters" "std")))) (yanked #t) (rust-version "1.65.0")))

(define-public crate-arcu-0.1 (crate (name "arcu") (vers "0.1.1") (hash "17w5j6ddkvslwikfxi5f6j8p5jzdhvn7ki8iixf60dn4nh7pqwpq") (features (quote (("thread_local_counter" "std" "global_counters") ("std") ("global_counters" "std")))) (rust-version "1.65.0")))

(define-public crate-arcus-0.1 (crate (name "arcus") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0rndd484g38amqf06ampwvl5v867gjsgs4xz39mf81vbxzzn3cp6")))

