(define-module (crates-io ar qu) #:use-module (crates-io))

(define-public crate-arquery-0.1 (crate (name "arquery") (vers "0.1.0") (deps (list (crate-dep (name "xml-rs") (req "^0.7") (default-features #t) (kind 0)))) (hash "0y3vn9d2qyrvp8iazi66sg8n00hnmwz06b9hk1kqdrb2nkmsz9nm")))

