(define-module (crates-io ar ig) #:use-module (crates-io))

(define-public crate-arigato-0.1 (crate (name "arigato") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "tracing" "sync" "net" "rt"))) (kind 0)) (crate-dep (name "tracing") (req "^0") (default-features #t) (kind 0)))) (hash "13zp97c3i44cvc3bfigs2i2ppii4l8k8y318xc2jxdrc1sw6i6py")))

