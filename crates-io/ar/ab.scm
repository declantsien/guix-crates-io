(define-module (crates-io ar ab) #:use-module (crates-io))

(define-public crate-arabic-glyphs-0.1 (crate (name "arabic-glyphs") (vers "0.1.0") (hash "1l719yz71iy2ac22145vd28cyzzak4zql14j1p991lhbknarf6ba") (yanked #t)))

(define-public crate-arabic-glyphs-0.1 (crate (name "arabic-glyphs") (vers "0.1.1") (hash "0j2nygv5q017ra0xz3cxldjpmqv7bd2ss47y43wciap4d5k74j5j") (yanked #t)))

(define-public crate-arabic-glyphs-0.1 (crate (name "arabic-glyphs") (vers "0.1.2") (hash "0dy6vq6mfj66d3w9klrg5rrjai5ih1pwzl7qvpnfhikjsvh52n0c") (yanked #t)))

(define-public crate-arabic-script-0.1 (crate (name "arabic-script") (vers "0.1.0") (hash "17ymv5zda5852pzcwcf5yybm07rc9m9fk9p1ylpxg11w445hqnps")))

(define-public crate-arabic_reshaper-0.1 (crate (name "arabic_reshaper") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "17xzxl4ma1dxbm2sgmc4bb2d0vhgi09fa7c23r6vr4ny2r620wdp") (yanked #t)))

(define-public crate-arabic_reshaper-0.1 (crate (name "arabic_reshaper") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0aik3crnj4wnpsyab08ym1cmib5drdhx2dz3cilfcmq627kyfc03") (yanked #t)))

(define-public crate-arabic_reshaper-0.1 (crate (name "arabic_reshaper") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0c9x959q3gwmv8nqksmpbshla2b9j6p3livmdvq4jg00ndrpx54l")))

(define-public crate-arabic_reshaper-0.1 (crate (name "arabic_reshaper") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1pslbzx97jw2gx8gawfvdryazlnbybn9340za4di539byjd36kl5")))

(define-public crate-arabic_reshaper-0.1 (crate (name "arabic_reshaper") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1y5kibk8dxx9wrmwsxdr1zdgw9p37q6m068jrph1g4mlhzxdl6xx")))

(define-public crate-arabic_reshaper-0.1 (crate (name "arabic_reshaper") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0cy639pi6mssfhi5czwff1x6x8zpsnhf9g07dix9kx3ply4l0v5s")))

(define-public crate-arabic_reshaper-0.1 (crate (name "arabic_reshaper") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1m4n5m0iaxjvp0ljrjrlkddhd8xmixp6zhdm1bk81p2r09vkdabb")))

(define-public crate-arabic_reshaper-0.2 (crate (name "arabic_reshaper") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (default-features #t) (kind 0)))) (hash "16k3jj5kxh0gqgdjrsz0673jzzdih85rdl97sabik9wy4w2hrgjz")))

(define-public crate-arabic_reshaper-0.2 (crate (name "arabic_reshaper") (vers "0.2.1") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)))) (hash "113wgjmyvpay9vkr57vm3ln34z6jjgd3pp3gl7zp2nm663jvsw22")))

(define-public crate-arabic_reshaper-0.3 (crate (name "arabic_reshaper") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)))) (hash "1kladwf8yjk7zsrz5l6xcvdzyq2qiwhmrwgk28rfn4kimgna35ba")))

(define-public crate-arabic_reshaper-0.3 (crate (name "arabic_reshaper") (vers "0.3.1") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)))) (hash "1brak2bnv1jji02y493cjy7l0kg6db9vf7v25kzh2457nsn7yc08")))

(define-public crate-arabic_reshaper-0.4 (crate (name "arabic_reshaper") (vers "0.4.0") (hash "1gflnv9i8aw74dj30m6jpcjh2h1s3zbgmyrmr50j2acylpq30kzj")))

(define-public crate-arabic_reshaper-0.4 (crate (name "arabic_reshaper") (vers "0.4.1") (hash "0b4az5kkdr0l04gnvsb0ss9zd0h7y4j4zqxn5h3hir8ydcb7j81a")))

(define-public crate-arabic_reshaper-0.4 (crate (name "arabic_reshaper") (vers "0.4.2") (hash "0nsl3724qnjmnmmhkhklgpn7cc9sq0d843ddd678icv2ph8k95pb")))

