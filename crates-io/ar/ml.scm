(define-module (crates-io ar ml) #:use-module (crates-io))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.0") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "radio") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "02p6sv3np5xh29xcc5jcasvq7mrfbxy0q9cf94maf9fp5slh27yp")))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.1") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "radio") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "1iymlwpnk0z8w3b14g4qqzw5ir1bq7jd49xrlf5mnx2lv6h7xxb6")))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.2") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "1nszgi350mg94mkbgfjivqp0i7z06mgnb6csp8avpz8yv2y33hxb")))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.3") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "08if597chyn30djvdk3h6nh2ynr676l5rzk4ixzx3xs1j23fkyw6")))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.4") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "0c8qp4m7fd6fs2ll1wzg3kvx0gi366fv4pm96nal4p67qhm689yn")))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.5") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "0f6ixxz9wy7qbv5q56lz7j2ckq9drp25g365cqn1yn6g6byqh52c") (features (quote (("i2c"))))))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.6") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "035gm0kmd1jwzbsfh1ylg57msw5n2bks0p7kllb1f6afijlhfk5q") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.7") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "0d5bcy2bnffg1305bya859jd3nb7dc30rahvcjnv0m4pdxwswpsv") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.8") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "1h6w6gfm6bb3m65q04c5ci861q9bii6yfp4cpw8kwzqpbvq30x2z") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.9") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "0lzdj3a4mki92vc8bggcck44snj5l6r76b6brpxsmwrxi6jhj9hs") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.4 (crate (name "ArmlabRadio") (vers "0.4.10") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "1x8jyxbiw9s4f0hrkxlbis1fhrdbiky2qg8ib1zz852ynidkr74w") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.5 (crate (name "ArmlabRadio") (vers "0.5.0") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "138l5zhijm5n74rzgqgnjdpqfrg8yihv9sfmsmzpww5xpyxipl1m") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.5 (crate (name "ArmlabRadio") (vers "0.5.1") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "1b7ffyfb2ygyba6ska08j8srdair7khhslsmh043rdsp4jqwsfss") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.5 (crate (name "ArmlabRadio") (vers "0.5.2") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "0bmrjn0pkl883h4c0sirp56mv3w40149iz5vc0bjvr0bb17jcdrk") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6 (crate (name "ArmlabRadio") (vers "0.6.0") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "0jvr2mbg1i2444yrwh1d0sccl053bfkrlg9l647i1p4avpys6276") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6 (crate (name "ArmlabRadio") (vers "0.6.1") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "0s71i4khzzw9x1rmkch32z5kqj4kqrz34907mdnizximx93h3l57") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6 (crate (name "ArmlabRadio") (vers "0.6.2") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "150flh5bnw095xkn2lmgmh4zslb99lmvxrjljadzvjya4lxj2yk7") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6 (crate (name "ArmlabRadio") (vers "0.6.3") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "1xig79ffxkijyza2x9sibiqmpxd0a8szr3lplh58qidhr9ydknpr") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6 (crate (name "ArmlabRadio") (vers "0.6.4") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "08xm1r1j4l5nfandlq2h86f275zai5a31nyp6zni3qbn9whwmz19") (features (quote (("i2clib"))))))

(define-public crate-ArmlabRadio-0.6 (crate (name "ArmlabRadio") (vers "0.6.5") (deps (list (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.2") (kind 0)))) (hash "17b2gsva0drfax2dcya1fa77k3g5zn32qvkgadb2k4gj9n6njv8b") (features (quote (("i2clib"))))))

