(define-module (crates-io ar gi) #:use-module (crates-io))

(define-public crate-argi-0.1 (crate (name "argi") (vers "0.1.0-beta.1") (hash "0srr54iga2z19dx190aja4zzsybjh99va06hn3jzws4gwxm47npv") (yanked #t)))

(define-public crate-argi-0.1 (crate (name "argi") (vers "0.1.0-beta.2") (hash "0dlhnw54y65ccsczwg9l87a1j12p84snh2hqq1bs2wcizg4rir2r") (yanked #t)))

(define-public crate-argi-0.1 (crate (name "argi") (vers "0.1.0-beta.3") (hash "163n89395s5avrhcjkblfnx64g76vv825fxs5k14fyb0ckx9225i") (yanked #t)))

(define-public crate-argi-0.1 (crate (name "argi") (vers "0.1.0-beta.4") (hash "14682fjsa1cxaxrkbfjbv1jbwnrj4hvnws30r6x42m3yc1nyhgq8") (yanked #t)))

(define-public crate-argi-0.1 (crate (name "argi") (vers "0.1.0-beta.5") (hash "0i02gw9dvycw8az8xb0vxrbs6mfwidaqamqg4lkh9c790sf1lvjr")))

(define-public crate-argin-0.1 (crate (name "argin") (vers "0.1.0") (hash "0lgbi0yhjb200m0a66r7yapwfin3y9q3kd1nahm1y90674p034gh")))

(define-public crate-argio-0.1 (crate (name "argio") (vers "0.1.0") (deps (list (crate-dep (name "proconio") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0jgrxs4xsxxb9znsdy0m14lkpm3dmq6l8sk09x98bsdg860yks7n") (yanked #t)))

(define-public crate-argio-0.1 (crate (name "argio") (vers "0.1.1") (deps (list (crate-dep (name "proconio") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0vva4g3y1s8n7g9v6xk82vcb40gb4dj7yz14x68p2c4yin0hf2l3")))

(define-public crate-argio-0.1 (crate (name "argio") (vers "0.1.2") (deps (list (crate-dep (name "proconio") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "093w93162hd47ll59iwzwmdlnff9ap0vfqlhb6yicg9q5i190qfi")))

(define-public crate-argio-0.1 (crate (name "argio") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "proconio") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.35") (default-features #t) (kind 2)))) (hash "1lr4blhmqhhnynq7c3ivxpcgd53ypyfj60yz311n4sbj91x0vfvj")))

(define-public crate-argio-0.2 (crate (name "argio") (vers "0.2.0") (deps (list (crate-dep (name "argio-macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proconio") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "14v3h4bfc138f48hclcshkd735pdl25mjdyp3yx4p6zhic0wbkzd")))

(define-public crate-argio-macro-0.2 (crate (name "argio-macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)))) (hash "17pjng0wbd2jsg1pallyrwpcai2y7avvpcwqpj9p70q62sq9vzh2")))

