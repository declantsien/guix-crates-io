(define-module (crates-io ar ba) #:use-module (crates-io))

(define-public crate-arbalest-0.1 (crate (name "arbalest") (vers "0.1.0") (hash "13glpa99agx71zlijfppfs87inz7hwrjzncskgs2ggnibzqs089b") (yanked #t)))

(define-public crate-arbalest-0.1 (crate (name "arbalest") (vers "0.1.1") (hash "128ims12xn1xgxbl0ayr0ggya62fpbdcby1gyiwc3amlv1mhpmip")))

(define-public crate-arbalest-0.2 (crate (name "arbalest") (vers "0.2.0") (hash "078h9lxb95wmffmgamcmwxf51h0qf6vmx9ahmxmwpr47arm0dqqz")))

(define-public crate-arbalest-0.2 (crate (name "arbalest") (vers "0.2.1") (hash "0gg0bpxlys9fvfplq9wca63vd2h0am6djladir4vhsyz5pbnlp0y")))

