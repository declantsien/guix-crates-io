(define-module (crates-io ar sl) #:use-module (crates-io))

(define-public crate-arslan_functions-0.1 (crate (name "arslan_functions") (vers "0.1.0") (hash "1p6mhxb2rbbh0rcn10hbsi4576vgv8iwdiw6jajjiyzqars7fsl6")))

(define-public crate-arslan_loops-0.1 (crate (name "arslan_loops") (vers "0.1.0") (hash "1ss890fyzbf8gk6pr95ldjskkzbc5nkfj30bv055ha4wg0rm3lvg")))

