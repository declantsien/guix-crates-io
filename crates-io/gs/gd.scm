(define-module (crates-io gs gd) #:use-module (crates-io))

(define-public crate-gsgdt-0.1 (crate (name "gsgdt") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0swz6laryh72kdrjd942qaxwcn02hwga7xy9a48p45mqg62lwklq")))

(define-public crate-gsgdt-0.1 (crate (name "gsgdt") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "095brv8srdij29y9kjdmyinzz4bjgjcdipbjca86ag6w7hqs7d4w")))

(define-public crate-gsgdt-0.1 (crate (name "gsgdt") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "09bqc425rz45ln09bm14h828d6pzlqfm669a58k9dpv2fb77dn50")))

(define-public crate-gsgdt-0.1 (crate (name "gsgdt") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0q4da0lx2kb39wfw5d0nqswqi3vn6zqkcd22yp93f7wppcwq35fb") (yanked #t)))

