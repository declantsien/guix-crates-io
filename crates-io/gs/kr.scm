(define-module (crates-io gs kr) #:use-module (crates-io))

(define-public crate-gskrm-0.1 (crate (name "gskrm") (vers "0.1.0") (deps (list (crate-dep (name "gskrm-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y9xmk9792v2nzcb2i9k1w6bzbqkq754ijm9cxy1sgdk8fl4jcaz")))

(define-public crate-gskrm-0.1 (crate (name "gskrm") (vers "0.1.1") (deps (list (crate-dep (name "gskrm-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wi9c3pa3589ymlnqk7zb6r8hkv77ihchjkfzk8wmkl3gc85pvmq")))

(define-public crate-gskrm-0.1 (crate (name "gskrm") (vers "0.1.2") (deps (list (crate-dep (name "gskrm-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "19lczw8cnpx6zqk5v3k5xnvvsg411jfs2agq5p02anj2kdbg4nc2")))

(define-public crate-gskrm-0.1 (crate (name "gskrm") (vers "0.1.3") (deps (list (crate-dep (name "gskrm-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q220n8flsyrq5rinfwjg8r5z90m2dj95yfa0c4jc8ayks5lq2fn")))

(define-public crate-gskrm-0.1 (crate (name "gskrm") (vers "0.1.4") (deps (list (crate-dep (name "gskrm-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p7yg1i9yr739db8ryhvazjzfwjdjijymh5avlcnrwm902qv2kwk")))

(define-public crate-gskrm-sys-0.1 (crate (name "gskrm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "03al0l959sj72qmdha7ighpx38r1i482v9z5bhf326ajcmn3lf8w") (links "gskrm")))

