(define-module (crates-io gs ca) #:use-module (crates-io))

(define-public crate-gscaler-1 (crate (name "gscaler") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02qss3rhd4fly14jjsmbjakvb79kmv8n8fpyfzs5q1q24ly8gkal")))

(define-public crate-gscaler-1 (crate (name "gscaler") (vers "1.3.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1zk7s8lrpd2s76ppijhjnn218k8vzsc16c4ps5k4wafdafxmm5j7")))

