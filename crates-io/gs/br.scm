(define-module (crates-io gs br) #:use-module (crates-io))

(define-public crate-gsbrs-0.0.1 (crate (name "gsbrs") (vers "0.0.1") (deps (list (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "0.2.*") (default-features #t) (kind 0)))) (hash "04zn7a4xvbdmfirwgc55p4kq9li4h0abs1r36cyzkw44flbmy88j")))

(define-public crate-gsbrs-0.1 (crate (name "gsbrs") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1lrqvixbydrprrsxgwkcwk14kqm12kw05clqqbqdgxmqb4ql2fi1")))

(define-public crate-gsbrs-0.2 (crate (name "gsbrs") (vers "0.2.0") (deps (list (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1h9yf3dlwfkn0s2pny028fw26nbzn2yqmr0h52q9az0gy7bf3i1y")))

(define-public crate-gsbrs-0.3 (crate (name "gsbrs") (vers "0.3.0") (deps (list (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1fds5iq2nwz8qqr13pqg79km6dhsah3zwdyd5570cxgc8sraflx2")))

(define-public crate-gsbrs-0.4 (crate (name "gsbrs") (vers "0.4.0") (deps (list (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0i16ank3cvic8kayi5dr7ih4dhbir7xpfz8h8brsdjagayzbkxch")))

(define-public crate-gsbrs-0.5 (crate (name "gsbrs") (vers "0.5.0") (deps (list (crate-dep (name "hyper") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0syzr9rj3fs3k0mi87lhp8rsjav5909mp15niq1vdq1j27k082xb")))

(define-public crate-gsbrs-0.6 (crate (name "gsbrs") (vers "0.6.0") (deps (list (crate-dep (name "hyper") (req "0.7.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "0.5.*") (default-features #t) (kind 0)))) (hash "1v51smwdg96q5v1xy94kfd453in3kbi7dxrdiwnd28vgcl7q3xzv")))

(define-public crate-gsbrs-0.7 (crate (name "gsbrs") (vers "0.7.0") (deps (list (crate-dep (name "hyper") (req "0.7.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "0.5.*") (default-features #t) (kind 0)))) (hash "0n5kmbvfyhpb4mfqx1ri8bqjrzr91bhimzcrzxwa185n6lq5v13a")))

(define-public crate-gsbrs-0.7 (crate (name "gsbrs") (vers "0.7.1") (deps (list (crate-dep (name "hyper") (req "~0.10.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "url") (req "0.5.*") (default-features #t) (kind 0)))) (hash "0wjsjk68k9dvplmi4nbfpz0k2j2abqc6afzqzbb8r4zgfgp83i37")))

