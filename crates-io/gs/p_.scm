(define-module (crates-io gs p_) #:use-module (crates-io))

(define-public crate-gsp_gpi-0.1 (crate (name "gsp_gpi") (vers "0.1.0") (hash "1afiwnb6lx0yb32hr9lrd4n51fcyhrya2mxy9hjxsry4m1d2zjfw")))

(define-public crate-gsp_sgr-0.1 (crate (name "gsp_sgr") (vers "0.1.0") (hash "147ng6ka0g69jxvhj4righlrakwd6h5lngycgl9sxsly8c7nkxb9")))

(define-public crate-gsp_system-0.1 (crate (name "gsp_system") (vers "0.1.0") (hash "03am9mr52bd6gs21j6wgzzsb77j8aszsaaklmn66msgcfp8qgm5y")))

(define-public crate-gsp_system-0.1 (crate (name "gsp_system") (vers "0.1.1") (hash "1xphahn18i9gn53c0zsjw3ps1mgz75ja8fiv2k12l50bhkwncsrq")))

(define-public crate-gsp_system-0.1 (crate (name "gsp_system") (vers "0.1.2") (deps (list (crate-dep (name "objc2") (req "^0.5.1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "windows-sys") (req "^0.52.0") (features (quote ("Win32_Foundation" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging" "Win32_System_LibraryLoader" "Win32_Graphics_Gdi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1rqi77h9d6rwvi84irdw8crp032nwz29hrwmlklw44clry8ccy5g")))

