(define-module (crates-io gs rs) #:use-module (crates-io))

(define-public crate-gsrs-0.1 (crate (name "gsrs") (vers "0.1.0") (deps (list (crate-dep (name "typed-arena") (req "2.0.*") (default-features #t) (kind 2)))) (hash "10n6c71xabrkcys8d4dzq3r3kwrxmzl5xxv8q8z42df713rwps8h") (yanked #t)))

(define-public crate-gsrs-0.1 (crate (name "gsrs") (vers "0.1.1") (deps (list (crate-dep (name "typed-arena") (req "2.0.*") (default-features #t) (kind 2)))) (hash "0jnaqwxgdqrwh11wf54cgb9ab1c332j0xfwh659wkhf9k4pzvfy6") (yanked #t)))

(define-public crate-gsrs-0.1 (crate (name "gsrs") (vers "0.1.2") (deps (list (crate-dep (name "typed-arena") (req "2.0.*") (default-features #t) (kind 2)))) (hash "0qj42r7r4gppar2p8y3vqfcmnyhwznjkal0bkg6n3mxcnkfyb214") (yanked #t)))

(define-public crate-gsrs-0.1 (crate (name "gsrs") (vers "0.1.3") (deps (list (crate-dep (name "rustversion") (req "1.0.*") (default-features #t) (kind 2)) (crate-dep (name "typed-arena") (req "2.0.*") (default-features #t) (kind 2)))) (hash "1akbd1gs5s0b0xv8dygw01jx60xdhk826zgzcsclza0ia56bb1rk")))

(define-public crate-gsrs-0.1 (crate (name "gsrs") (vers "0.1.4") (deps (list (crate-dep (name "rustversion") (req "1.0.*") (default-features #t) (kind 2)) (crate-dep (name "typed-arena") (req "2.0.*") (default-features #t) (kind 2)))) (hash "0qs35av7zc9pjfjw0x39si27gb2a873893spylck2c675r2rc6xi")))

