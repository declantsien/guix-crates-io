(define-module (crates-io gs m7) #:use-module (crates-io))

(define-public crate-gsm7-0.1 (crate (name "gsm7") (vers "0.1.0") (deps (list (crate-dep (name "bitstream-io") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7.24") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0bhlddm0xqmcf4x4vzfrsym5565hwn2p9k9k6k0c7h4b5xh42ghz")))

(define-public crate-gsm7-0.1 (crate (name "gsm7") (vers "0.1.1") (deps (list (crate-dep (name "bitstream-io") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0lkp490w7fw7xswr354pw7sbjynw2pq6nivf3c41q828n8q86xy0")))

(define-public crate-gsm7-0.3 (crate (name "gsm7") (vers "0.3.0") (deps (list (crate-dep (name "bitstream-io") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "028bwg5c9bihj3jarqkwdy2xa34hpj4i3rkb9bnkb7gsyjqx14y2")))

