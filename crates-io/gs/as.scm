(define-module (crates-io gs as) #:use-module (crates-io))

(define-public crate-gsasl-sys-0.1 (crate (name "gsasl-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "1fap00mrr575kbr9v5r4rhi89lgxayckzdclvfxmxf3vdaxi5c5w") (links "gsasl")))

(define-public crate-gsasl-sys-0.2 (crate (name "gsasl-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)))) (hash "0cxw6a0irm89sgkiqprgm0pwqa2chjkhpr3svb40jvz8k20lc8ws") (links "gsasl")))

(define-public crate-gsasl-sys-0.2 (crate (name "gsasl-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)))) (hash "0gdbila0a93nq299c29bci0nfgz29pg1b3hq9kjzgylkilz8cgrd") (links "gsasl")))

(define-public crate-gsasl-sys-0.2 (crate (name "gsasl-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)))) (hash "1ir8x97yb0h8jnxch5msx7h4l68nffbmxg9fkk4bqb7b74im1l4h") (links "gsasl")))

(define-public crate-gsasl-sys-0.2 (crate (name "gsasl-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.55") (optional #t) (default-features #t) (kind 1)))) (hash "0ddqdcxdwyg5xdfn8jkrwvq6kpnf71sg27n8vl66jmi3gjr954fq") (features (quote (("default" "build_bindgen") ("build_bindgen" "bindgen")))) (links "gsasl")))

(define-public crate-gsasl-sys-0.2 (crate (name "gsasl-sys") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.55") (optional #t) (default-features #t) (kind 1)))) (hash "1gaxpnv1s2kms3rl1vx9w8jawxbsj2lnnsjc3dcn3rh7c0h7ydyd") (features (quote (("default" "build_bindgen") ("build_bindgen" "bindgen")))) (links "gsasl")))

