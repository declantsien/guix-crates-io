(define-module (crates-io gs sa) #:use-module (crates-io))

(define-public crate-gssapi-0.0.1 (crate (name "gssapi") (vers "0.0.1") (deps (list (crate-dep (name "colored") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_primitive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gssapi-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dwr5aaacc96p02q3ml4qjc96j39j0mi3pw2hi63wn947z8jfi69")))

(define-public crate-gssapi-sys-0.1 (crate (name "gssapi-sys") (vers "0.1.0") (deps (list (crate-dep (name "krb5-sys") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1jglhhzh06was5gcr4wj906gpkf9vm7i6vrvmdhqmylvxznbps5g") (features (quote (("om_string") ("krb5" "krb5-sys") ("default" "krb5"))))))

(define-public crate-gssapi-sys-0.2 (crate (name "gssapi-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.37") (default-features #t) (kind 1)))) (hash "0i6r7r9vwbv2vidfgnkpcxzcdczkzj72pizrx49m4x0c96wzjmby") (links "gssapi")))

(define-public crate-gssapi-sys-0.2 (crate (name "gssapi-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.37") (default-features #t) (kind 1)))) (hash "0nqi8zhyrdjbb6z42fqah8mzhx5dys8w0p5n0ykgmhh314jmiwsf") (links "gssapi")))

