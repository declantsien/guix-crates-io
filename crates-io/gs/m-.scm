(define-module (crates-io gs m-) #:use-module (crates-io))

(define-public crate-gsm-sys-0.0.1 (crate (name "gsm-sys") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jr756a5drm5pxpid6s834jpqzz0rnyv2ah1csh62axbjvfdkyas") (links "gsm")))

(define-public crate-gsm-sys-0.0.2 (crate (name "gsm-sys") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m28j7gyia47ar6pfr6dgwh9snyl067x3fflsdwih21gz543hmn1") (links "gsm")))

