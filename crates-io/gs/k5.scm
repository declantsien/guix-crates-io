(define-module (crates-io gs k5) #:use-module (crates-io))

(define-public crate-gsk5-0.0.0 (crate (name "gsk5") (vers "0.0.0") (hash "12ax1vhdrdnrmkvc1v7wwwxp5jq7f2d087srzgscdwaddr9w9ny7")))

(define-public crate-gsk5-sys-0.0.0 (crate (name "gsk5-sys") (vers "0.0.0") (hash "11k8vmwjxbdi762y8gjrxi659q6v2djs4wsa1n7nbd1gdizjw8pr")))

