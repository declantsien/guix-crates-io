(define-module (crates-io gs ys) #:use-module (crates-io))

(define-public crate-gsys-0.0.0 (crate (name "gsys") (vers "0.0.0") (hash "1bzzxy98sp93695v7rdja6gmq6z3yda3vcxl8j8i3h8z2hz8ik57") (yanked #t)))

(define-public crate-gsys-0.1 (crate (name "gsys") (vers "0.1.0") (hash "1569afwqaddvignk1z9w2w93rx4rf0sapr6d5ms6p5fsx09y79vf")))

(define-public crate-gsys-0.2 (crate (name "gsys") (vers "0.2.1-alpha.0") (hash "0brclw3ywbjcanc0xjb42lkg636s132l1y3hb8mryn0bndmzy27v")))

(define-public crate-gsys-0.2 (crate (name "gsys") (vers "0.2.1-alpha.1") (hash "1vyn6xg6l7ssbbfhxl5ax3x4mxfl6jwal6yiphm5cwxg60sc9wzb")))

(define-public crate-gsys-0.2 (crate (name "gsys") (vers "0.2.1-alpha.2") (hash "0iqv10ls1vk0s0sgf2nsbc82krjgf7cf249llnl8lmgqgi5vnc1w")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.1-alpha.0") (hash "1iaanq4v08p0rm8xzmnanbw0rhj9dqbj5cpd5sz58dgaqgrg5zii")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.1-alpha.1") (hash "0nv0pa4q847402jspk8yjvaqjy5idvvk87zfy26ni74ayqvz5sm0")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.1-alpha.2") (hash "1laqkdmc720z47dx91pa5xgw7w0man5b5c3ccc7gjx5l4pib008h")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.2") (hash "0fynqpx56lxn87spwk3jrhml2kldil80834hd9nzxc01f9xa3pbq")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.2-alpha.1") (hash "1wrmk6hkpyzzi7r7ics3cvch92n3jbizrn8kvpmp9i96jbkwyar0")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.2-alpha-2") (hash "1ph4jh3pdc3v51n8pd3pznkll0l94hl6davviyri6qmxffh2l0ly")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.2-alpha.3") (hash "04g69hfnz4rzhy13d1kh63z2ii0p92404sixcryi7b22ysdwcpal")))

(define-public crate-gsys-0.3 (crate (name "gsys") (vers "0.3.2-alpha.4") (hash "1fnrqz9s356vq5kav09fjhfhfxwfk51m50q6a75awb8ppfvn421z")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.0") (hash "11nwbsgfz56dxqj5jy8wxksdsjfdbkrfx96rgkknbjs6cmipzq4q")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.2") (hash "0b6nwk36b62b0drb4gg5s8h7zqjj31qgkvm719xcw0msnrqhj1kk")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.3") (hash "1m059wa0s3nkrics06nblpc087svnvyd434pmq65394a3yanpjna")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.4") (hash "0kw80h3d9lp57a1sd9y8vzah3n5swf2v4igpd68d489mwl4gf0sv")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.5") (hash "1hpg1cjbbl4dbcqh4lxgbv83k2jjd6cxsb8dfvbl6ig9kbhsibgc")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.6") (hash "0l476jz07j7z0gm0p4ky5m7qgyihc3qla2lqw7hjaaxiyzc5msa3")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.7") (hash "0bddr7v1r5xfyddqfgwmb3dnpd843cpbpskp9xc69zw24s7chd65")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.8") (hash "0638y9jafyv1fqch1sgq9bbblzdbk9lc4ri8z0jnjs7bm2yzk119")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.9") (hash "0sxcsdllh6k6nd4lbkgmmpmr0ph529c21kxr5v2cwj8dmh1jfaxp")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.10") (hash "0k2r8r32zgrilr26ba00r17van03pd85k7qrwsixvkizdqscidhg")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.12") (hash "1lm51cq2hihrv2y6kl0dzgwncy6sbzph9s3jw0q1wa8qisiay1wi")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.13") (hash "1599s3cmkl9yykhckrjbz0rr7r92b3k102cnyph2x0nysxd6yb14")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.14") (hash "0449j4jb09340yfpx545f3db332rdajwy78k0mr7kq6wd7wvip52")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-pre.15") (hash "0kf4pww12slbxi97yml2jn55qxpwpc24apqwnqhk9fvzp4xrbb67")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.3") (hash "0hj9amr33q5fy0s45ly59s1rxd1nz7fiqlzcwilg16vd2nkv6mx4")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.4") (hash "0c6m0x199r33ssxhc0wb0nv5r471gf2dwbz4sbdr453d7qj2d4mv")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.6") (hash "0w5xzc4i6z8ndbc57a4kf2v5mnycnmf1n0l6jb26ak9wwp78nhl0")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.5") (hash "0qzvdj08v6qqn0nyvrxz6g6p682pjhdv1x7ksby381j04ncdl27z")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.7") (hash "0jpjv37pz6mla56z24xw6kxxrq3abprvi4cz7svik01snb4x5kjr")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.8") (hash "0b2a5i1x868acq91m6h0ghiz0xhh51p5gqbk9ry0h6q9gnakfcsb")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.9") (hash "068ba3756hxwd6k13v56kyqw904h8jy8v0d4pgcgkwi6m2943i70")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-354d660.10") (hash "1gyw0kcyqmjf0ab4148ps9v0g3s6ppl34sksrxqrpaqxwqabzncv")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2") (hash "0w4g56yy220vmax90787jj79a91wif47mk8sq7azdc6y2y55l884")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-gtest-dev") (hash "0w0h1r88ghzd0v3rnxkb5ij10xnvaggvdlvf6givrg7j47v4k8rq")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.2-dev.0") (hash "0kgna3n6a41qacn8hqxaz0k5xshha3dpy8zqdnxr1v4yxwkv6riz")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.3-dev") (hash "1wj8rx66mqdmhyvl5kp9cs6mnxn8rsnkqcdj48bmb1n0iaff91y4")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.3-dev.0") (hash "0b73db61ayg86q74wzf50nwf5d7cz4jn0k9syxb0f3p285nv9gy1")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.3") (hash "1iv0m8ccjcbmnjppyprcaaslkg3y79xmkl7fqzriggag3slawmg2")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.4-rc.0") (hash "1i1yzz7pw871wkc240grsg3n92nckpqf8s0a38sbj2fz3ymrsn1f")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.4") (hash "0aaz4s2mlnisnb1z1as4yicax4y4xl8vb2p716r7vxvyz3fdv0k0")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.0.5") (hash "15ymxh3msfhbgkvg5dfnckcl3pwlql9gnj5fyn263bc37w7ivb36")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.1.0") (hash "0s76ijqk8f1df47jh3b9ys3g2v2s153m3kdqqnzzjkgvxpvf2ym5")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.1.1-rc.0") (hash "1mlfd8xj5v1chfqjig05np1ml4ad16kjsisb41fl0hq1zfcs9q04")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.1.1") (hash "1njb5zvi3w752hy4zwd4fxsb66x48libjjjkd29i3kvymnnd7zp4")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.2.0-pre.2") (hash "0hbrnmqw5wdycfzpbgdz33zrh58k4dwh3xadrrcibs9a1ly1xddn")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.3.1-pre.2") (hash "1idpl047hv91gqss8714h86ggf7afhvz73kgw5vcvwqxp5cxrgjf")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.3.1-pre.3") (hash "1w467sajl7ln0wssrfm07in7cm8k80v0cmxzk359w04lww01gv0v")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.2.0-pre.3") (hash "0k6vvc33ifmgn9svz06ps3pbiali6ki7pm8gn1jdyh3kpkkfcgx6")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.2.0") (hash "1lrd3zfx0vpri5ga7k0y3szj5kslsrx1jkkxhn53fc8jxag0vipc")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.2.1") (hash "0qn11363kplahvm3435kw39m8r23qnrs1b68dbnjwqbh6a3x5gj0")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.3.0-pre.1") (hash "09nlzb8vvf2jbjmqxiy2rjmzkq7yiv62x1c81wxsp5jijsayxlni")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.3.0") (hash "1cgjapqv0jh45rcj7qjvn75w49x1746a5d3mwq0cpgf49micjzcg")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.3.1") (hash "0v67gdm1b6v7xwns5yv8fbzx9cwbrw2jfcjiafi9js5dk7cwf63b")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.4.0") (hash "08khl3iqyzkfgb5h2fkfbblvy56agf6k79b6d35jyaa7ag5lvywk")))

(define-public crate-gsys-1 (crate (name "gsys") (vers "1.4.1") (hash "0qi417yx4vdmc2w8khl29znglgzqcygim0j9xbdr7lckwn2y89ky")))

