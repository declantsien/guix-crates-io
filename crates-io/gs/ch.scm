(define-module (crates-io gs ch) #:use-module (crates-io))

(define-public crate-gscholar-0.1 (crate (name "gscholar") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kzk9z3b72pzri35i75vi355wcsr46a4krx6rn2g3g0vvgxz6plv")))

