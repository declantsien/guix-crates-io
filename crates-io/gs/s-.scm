(define-module (crates-io gs s-) #:use-module (crates-io))

(define-public crate-gss-api-0.0.0 (crate (name "gss-api") (vers "0.0.0") (hash "06f467gawrjmqh2qdy8r702dm2jxvhhgkjakk53d731adilhbznj") (rust-version "1.56")))

(define-public crate-gss-api-0.1 (crate (name "gss-api") (vers "0.1.0") (deps (list (crate-dep (name "der") (req "^0.7") (features (quote ("oid" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "der") (req "^0.7") (features (quote ("oid" "pem" "alloc"))) (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "spki") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "x509-cert") (req "^0.2") (kind 0)) (crate-dep (name "x509-cert") (req "^0.2") (features (quote ("pem"))) (kind 2)))) (hash "0nmannc0r8h5vx1mka6v2pnwhnl6yi5bwdkhgwkmr4yyslpkdmzf") (features (quote (("rfc2478")))) (rust-version "1.65")))

