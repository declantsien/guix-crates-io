(define-module (crates-io gs v-) #:use-module (crates-io))

(define-public crate-gsv-culture-ships-0.1 (crate (name "gsv-culture-ships") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1zm282aaqmiaj2dz0klqqyb0sgx1c0pfbpj5s06fz28b1av4hdlp")))

(define-public crate-gsv-culture-ships-0.2 (crate (name "gsv-culture-ships") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03hjgjvrs7010ahq3xwyfg7k7cv03mhcnz7vbiy9np6ipzaxlff3")))

