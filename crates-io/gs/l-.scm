(define-module (crates-io gs l-) #:use-module (crates-io))

(define-public crate-GSL-sys-2 (crate (name "GSL-sys") (vers "2.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "01l2n3j6hl15s4z6qzfn1348bw4igqiri2s2nwhfqqk7jsmcb4y0") (features (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

(define-public crate-GSL-sys-2 (crate (name "GSL-sys") (vers "2.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0rblf63ik5phadc09ngahyfhb67vi463ipmqv6asmph8h9627l0y") (features (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

(define-public crate-GSL-sys-2 (crate (name "GSL-sys") (vers "2.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1rdcipahi4ncm26bv19jjlpfwb7p1xc0mlpaw9fnhsxscg9lrx7v") (features (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

(define-public crate-GSL-sys-3 (crate (name "GSL-sys") (vers "3.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "17dx066l1pbjwp9syjkzqb6fiajyb4wc814zqdfrj807rh6nfxs5") (features (quote (("v2_7" "v2_6") ("v2_6" "v2_5") ("v2_5" "v2_4") ("v2_4" "v2_3") ("v2_3" "v2_2") ("v2_2" "v2_1") ("v2_1") ("dox" "v2_7"))))))

