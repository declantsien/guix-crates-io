(define-module (crates-io gs li) #:use-module (crates-io))

(define-public crate-gslib-0.1 (crate (name "gslib") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "google_auth") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.6") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1n61svjfaziqhb8hhlwarqqf7m316c4cw2n0d97bpp6hm3cc8d7m")))

