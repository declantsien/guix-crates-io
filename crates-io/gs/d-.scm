(define-module (crates-io gs d-) #:use-module (crates-io))

(define-public crate-gsd-parser-0.1 (crate (name "gsd-parser") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.24.1") (default-features #t) (kind 2)) (crate-dep (name "pest") (req "^2.5.2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.2") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (kind 2)))) (hash "01q5q1nikn17ymwwwr3c5j1xc0k75pzsqhywhqkm2ml78qhgjvm6")))

(define-public crate-gsd-parser-0.1 (crate (name "gsd-parser") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.24.1") (default-features #t) (kind 2)) (crate-dep (name "pest") (req "^2.5.2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.2") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (kind 2)))) (hash "1sa4b8bk8y7c1adm6gj2iqina1bdhmhlhv226942aq973mjx8rhn")))

(define-public crate-gsd-parser-0.2 (crate (name "gsd-parser") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.24.1") (default-features #t) (kind 2)) (crate-dep (name "pest") (req "^2.5.2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5.2") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (kind 2)))) (hash "148m10dyk8yhzqwsyh80f3bvl4rgnrggfl5klbm32w8bnrmcj48j")))

