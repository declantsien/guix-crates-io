(define-module (crates-io gs hh) #:use-module (crates-io))

(define-public crate-gshhg-reader-0.1 (crate (name "gshhg-reader") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "1rgid3sfwvrdlmq4pfjcijjb82jfp6v5lwcy6wmqy39iwba1rq03")))

(define-public crate-gshhg-reader-0.2 (crate (name "gshhg-reader") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "0iyh8b5s0cghcyc1pqlngzl227w5ldy83lv162l5mz201ri8w3hw")))

(define-public crate-gshhg-reader-0.2 (crate (name "gshhg-reader") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "19vysx7fdihj271akcigiiaqwnw2sa33xmh1gz129pcby0hqnn3n")))

