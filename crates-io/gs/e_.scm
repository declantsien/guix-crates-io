(define-module (crates-io gs e_) #:use-module (crates-io))

(define-public crate-gse_do_not_disturb-1 (crate (name "gse_do_not_disturb") (vers "1.0.0") (deps (list (crate-dep (name "dconf_rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vg16rlzn198jb2xc2ddx2ihkrnn2vba332lb5bxrav4jndh3s28")))

(define-public crate-gse_do_not_disturb-1 (crate (name "gse_do_not_disturb") (vers "1.0.1") (deps (list (crate-dep (name "dconf_rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1lnvrfdh3xx14jrfhdf0s72rqj7rzb812817kxmh7yjvs53zlzxs")))

(define-public crate-gse_do_not_disturb-1 (crate (name "gse_do_not_disturb") (vers "1.0.2") (deps (list (crate-dep (name "dconf_rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p77bd2da6q3ic7ja5fzb6hm042rlcad6qqjyx4pc1h9fsyvrcg9")))

(define-public crate-gse_do_not_disturb-1 (crate (name "gse_do_not_disturb") (vers "1.1.0") (deps (list (crate-dep (name "dconf_rs") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0gahwxpblykd6vwvdaxh5mpkwghg3v3wggdwxwcjv6w3r6i649cl")))

(define-public crate-gse_do_not_disturb-1 (crate (name "gse_do_not_disturb") (vers "1.2.0") (deps (list (crate-dep (name "dconf_rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0gjd3d4mljjiwcnm31h2g2xxpzw90r8slgxkzv9fcnabfqzmd096")))

