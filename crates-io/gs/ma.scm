(define-module (crates-io gs ma) #:use-module (crates-io))

(define-public crate-gsmarena-0.1 (crate (name "gsmarena") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cal49l4isxd4ghky10qivi292k1ch2sxxhx1nlk6ngphfn0gm5m")))

(define-public crate-gsmarena-0.1 (crate (name "gsmarena") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nxhji00aybcpfkcb3v0xpmhk8zr17wqf72y01vyr57a2pk8hwbk")))

