(define-module (crates-io jg et) #:use-module (crates-io))

(define-public crate-jget-0.1 (crate (name "jget") (vers "0.1.0") (deps (list (crate-dep (name "jget_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jget_derive") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nhr550yajgrb5dpnk1563g3a8756hdhs3zqcbs73fb0znjh24w5") (features (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.2 (crate (name "jget") (vers "0.2.0") (deps (list (crate-dep (name "jget_derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jget_derive") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z8ikbwgxj9pzbbh8cvdgy0l3ygq860rmgh45palsy377dwwhlsc") (features (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.3 (crate (name "jget") (vers "0.3.0") (deps (list (crate-dep (name "jget_derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jget_derive") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gflhhajfzhn0jqklzpjs0lksb9cd5wi7zpmwbwnpc11hx0n6fxr") (features (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.4 (crate (name "jget") (vers "0.4.0") (deps (list (crate-dep (name "jget_derive") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jget_derive") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vc3vic0fxhn4vkrqsbz1hbb0i20zqn68qzddn23sbkih1kj5d8z") (features (quote (("derive" "jget_derive"))))))

(define-public crate-jget-0.4 (crate (name "jget") (vers "0.4.1") (deps (list (crate-dep (name "jget_derive") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jget_derive") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wsmscjzy7gqfsjw5xc1w0f1xprzw87byldk45l0lkyxs7ay69p1") (features (quote (("derive" "jget_derive"))))))

(define-public crate-jget_derive-0.1 (crate (name "jget_derive") (vers "0.1.0") (deps (list (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "1n6li9fybqj07jskzfk8kza4jlpri59gbb2vmczxpzhf7nrps525")))

(define-public crate-jget_derive-0.2 (crate (name "jget_derive") (vers "0.2.0") (deps (list (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "16r503nsnxll7445g1hscki9xrscbz4v9lz5bj3y5p92bdhsjzr0")))

(define-public crate-jget_derive-0.3 (crate (name "jget_derive") (vers "0.3.0") (deps (list (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "1rdy47w9hlv4g55s58i2x9l2717cb8bbmf8mg36hxdn3f0x30bc5")))

(define-public crate-jget_derive-0.4 (crate (name "jget_derive") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "03ki7prriym2cwb5va8vw8clycj2mah8k55qrlzbixp31j2hc0ny")))

(define-public crate-jget_derive-0.4 (crate (name "jget_derive") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "0j7ikxwjh7mc32wrrvlvxflm3c29r7k2ws62p8q9n0vipq7sc5yz")))

