(define-module (crates-io fs co) #:use-module (crates-io))

(define-public crate-fscommon-0.1 (crate (name "fscommon") (vers "0.1.0") (deps (list (crate-dep (name "core_io") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "13rn2a302igiq9wh3c5xqbk35mdc8qs97mahlrkmfw2h5n9lqjx8") (features (quote (("std") ("default" "std"))))))

(define-public crate-fscommon-0.1 (crate (name "fscommon") (vers "0.1.1") (deps (list (crate-dep (name "core_io") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0c9m21083hi8yvqbcj988rj00paagps6whvy7rdcrpd5mj2ycp1i") (features (quote (("std") ("default" "std"))))))

