(define-module (crates-io fs tr) #:use-module (crates-io))

(define-public crate-fstr-0.1 (crate (name "fstr") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "1zym6i2afqn49lji9wkwlw9q5p6b9c2ys8jhnxpcyi0iyj3zd86l") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-fstr-0.1 (crate (name "fstr") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "1664gxxwwajiwgj92cprkc53akn322mmm7945il7pmgk87727kif") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.1 (crate (name "fstr") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "0igmwrdrp9pgmsff9h5xfhwxwys8xch5wdgzs0gj6a8v6wkh6dxc") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.1 (crate (name "fstr") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "0p2kahik7585h7fj18aggrv9c0wxga3ic9yxms7gw5bnxn8p7n1i") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.1 (crate (name "fstr") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "11v4rb5xc857z3vr0vw1fgsmsg7ichhrdqvpjqmly1pyb9b2zrfb") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.1 (crate (name "fstr") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "1w9a33k0f3wk0d1bnvgbbzxlz21khmmvcq4db2rmkp32mvbcz725") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.1 (crate (name "fstr") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "1fjkrhz5y07jcsdzl6qjax6w1vzzq0lb3r08gr9flrach74yd01d") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "15jmbl1v16mjs7qm2hnv5p5fallyiqyc755y20ipxckzmpvd4n93") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "0m6mzczjndbpnilyapa7ljvzhzk8mpd9jyhb135bgivjbbj0vhi1") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "07369h76i8qawz7avhlymbqrgp5fqq409lfn86vmz6bqwvibz5d8") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "04wynj2f7jg253h9fv901i1lwcw7lwh7ml0nmk5jk6q8093h8fph") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "13pn3b7m2n416rmsjrif8v91q4hj39j464ndmvsqn3k9i5ys3dwj") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.5") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "08gv88xyxb54hscp9s0ppn6yag3id3mfr1wx6gn50bihcnxmmhdx") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.6") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "10vw9nk5lyl670la2dss1la4xk5qj2ny43mcvqxp5gmkc49ib25l") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.7") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "099acmpfk9x7ay4alx15gf70zf97vqlqc9nqfgy359jc9ag4pxai") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.8") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "0nrb9jz76r2km56f6v90j4x26rg2cd6xxndrjbdyvz9k6gbmsfcw") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.9") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "0mc3iwx662gybajfpjhzavcmd5laym84myv90njkc7kpa5vj6sr1") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-fstr-0.2 (crate (name "fstr") (vers "0.2.10") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (kind 2)))) (hash "11b9rwbcz2qaa1sabv51sm6jh2lc4ga53ck7sjhqbxpnxxzxllbv") (features (quote (("std") ("default" "std"))))))

(define-public crate-fstream-0.1 (crate (name "fstream") (vers "0.1.0") (hash "1vg73843g6spd17y5c0y1kl8ksh8f5n566daci0c490xw0v9y99k")))

(define-public crate-fstream-0.1 (crate (name "fstream") (vers "0.1.1") (hash "1fwq0dq04a5imly67mcbbxi9rp9zf16vb4m37ww9af0xam6j9kn4")))

(define-public crate-fstream-0.1 (crate (name "fstream") (vers "0.1.2") (hash "15p57mhy81ifsl4frqd1s9q0bmw48am0zcyan1xx2gcc6kdzhfzx")))

(define-public crate-fstrings-0.0.1 (crate (name "fstrings") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro") (req "^0.0.1") (default-features #t) (kind 0) (package "fstrings-proc-macro")))) (hash "0vs96p78mns48psla7r3237h7jsi889iw80pzsk8x7azhyldwn9j") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions"))))))

(define-public crate-fstrings-0.1 (crate (name "fstrings") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro") (req "^0.1.0") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "13266pbzgc3v5rklcfzd2ii4k2nn6khb2i9axjdgppy02lxki6kd") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1 (crate (name "fstrings") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro") (req "^0.1.1") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1q7w5fhj9fvba79685lcav523ycanypq2jnfh17v93hgrjkzkns6") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1 (crate (name "fstrings") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro") (req "^0.1.2") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "09318lslwi9pkm37vzkq6pqam06ladmq8dgka6nrs7bqnmanhi61") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1 (crate (name "fstrings") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro") (req "^0.1.3") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "01jnccslh96x2gkba9rafzv8kplgwp60sa206nbv024pq5gw1cjn") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.1 (crate (name "fstrings") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro") (req "^0.1.4") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "03vvc84w98s1b9v803xcybxd5rdh6jn4l2hjqigza9gllwvhg9f1") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2 (crate (name "fstrings") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro") (req "^0.2.0") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "1w35cjcqx6arlnrn3lry9xvlxvjwcrdfd19j19w5fbx8zxchb3m1") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2 (crate (name "fstrings") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro") (req "^0.2.1") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0b0gr4gvsrlgzlz2l5fpks7la0svw1i9zkyrd3ayjm3a1jly1ksp") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2 (crate (name "fstrings") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro") (req "^0.2.2") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "098bc6jj7a9c3lxxm418hk6ixlxqr9jcyxc0phpka8rddgz8945z") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2 (crate (name "fstrings") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro") (req "^0.2.3") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "1riaazfvqnr6193wa6afsfwghmxb5mhqc15dp8vaq1d5bpqs0ibq") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2 (crate (name "fstrings") (vers "0.2.4-rc1") (deps (list (crate-dep (name "proc-macro") (req "^0.2.4-rc1") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "00rsl9j7vd6c3kqkb3yx1xqm9ylh89rlb68frw3hq9xa4y07kl1d") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-0.2 (crate (name "fstrings") (vers "0.2.4-rc2") (deps (list (crate-dep (name "proc-macro") (req "^0.2.4-rc2") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "19vc95imdi5fwxf585d7cg51y645gk5sfn2rz63w8cxmklmkfg3k") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-proc-macro-0.0.1 (crate (name "fstrings-proc-macro") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.42") (default-features #t) (kind 0)))) (hash "1mf24lz5295nxm1x0hiw3swvmxdbbglfzkyzmf4zfyid241qn7hr") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1 (crate (name "fstrings-proc-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.42") (default-features #t) (kind 0)))) (hash "0h4cqlr917v9p3jzj3m7ffwzavg8r47g2qyyzwy4w6lpp1w56113") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1 (crate (name "fstrings-proc-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.42") (default-features #t) (kind 0)))) (hash "037xh4kcmaf04dapqnszmzk3irjg5sp4gki0f1i0dqp05afk0qz6") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1 (crate (name "fstrings-proc-macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.42") (default-features #t) (kind 0)))) (hash "1m0fcwkc7xajb69lp9lzw9zly7m28x6bcbzn263i1ahxjfiqx2d0") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1 (crate (name "fstrings-proc-macro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.42") (default-features #t) (kind 0)))) (hash "0194a40yx16mxgy7dkjbl3k6igkmm17lm2hfz0ncnd9n4xqcxggc") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.1 (crate (name "fstrings-proc-macro") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.42") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17nkvqah5709l8fqzf4q5wdvykl62s7fpmwak0l1bxa66rwcldgx") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2 (crate (name "fstrings-proc-macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rz8jc9s6zr6kk4pim39x98lv5nqa7h5zp8m9vyd7cw7gdsh9n8s") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2 (crate (name "fstrings-proc-macro") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pp7w4k4fknwqdmdyjmmrrkqvf76xmbcmbif6z37zrvp88sf1y2z") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2 (crate (name "fstrings-proc-macro") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hzi2929njw7xabs1apf2nvq7pb03xs0awddwnndsb0qy74vdsb1") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2 (crate (name "fstrings-proc-macro") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "179dr49s3c3zlhl6gqlb0a63pbp5rf192a9ji93k7p41fl78rdb3") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2 (crate (name "fstrings-proc-macro") (vers "0.2.4-rc1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19mwj8hv1p7i08spajv5kx0sn8lgk0ih5lr7wwypv11b3parscri") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-proc-macro-0.2 (crate (name "fstrings-proc-macro") (vers "0.2.4-rc2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "130ch104d8brxim4bia1f2b4l66bcam9x8fi13vknzv3ad69fb96") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

(define-public crate-fstrings-rust-0.2 (crate (name "fstrings-rust") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro") (req "^0.2.2") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0crnbk3d2cy2gn22aibzhk8sbg2w79959ciq124p072mvq6a8dzr") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-rust-1 (crate (name "fstrings-rust") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro") (req "^0.2.2") (default-features #t) (kind 0) (package "fstrings-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "1kg0h10x3jlhs2lhmri9sd3k5vrhjxr8payqgnz12xsf5m67ih4q") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-rust-1 (crate (name "fstrings-rust") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro") (req "^1.0.0") (default-features #t) (kind 0) (package "fstrings-rust-proc-macro")) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0ahlczmh3s8xsy076kcqxnzi6wsvbgy9gha20ns6jgddvnkd9sl2") (features (quote (("verbose-expansions" "proc-macro/verbose-expansions") ("nightly") ("default"))))))

(define-public crate-fstrings-rust-proc-macro-1 (crate (name "fstrings-rust-proc-macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "1.0.*") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "1.0.*") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1j18dlam7bcf59f6672pp5vlbkjf6jrdx43b58kgja8n15qssaqi") (features (quote (("verbose-expansions" "syn/extra-traits"))))))

