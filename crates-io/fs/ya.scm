(define-module (crates-io fs ya) #:use-module (crates-io))

(define-public crate-fsyaml-1 (crate (name "fsyaml") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0w2zbjddiznnx4lxqd46pjqq0ifkc4yk5mwa065j8yl0rmsy6yxi")))

(define-public crate-fsyaml-1 (crate (name "fsyaml") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0x3nkr5zig7m9ghlxiyy9hbrj993vffw0b2hx1pxwmxyymszl6nk")))

(define-public crate-fsyaml-1 (crate (name "fsyaml") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "16xlmmkh0aja7qrh7jlqnaz1bhaszzjz6fjqi1pp60s4x3s67a89")))

(define-public crate-fsyaml-1 (crate (name "fsyaml") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0vh8amdmbahlvk872wxqk7zlhkfxqrwp4qcmrkgljshqz6fjqlvw")))

(define-public crate-fsyaml-1 (crate (name "fsyaml") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1a1lfbrqq3l9w0k64mqa25fn0pp3143nsvby8h04ka089k1ccqza")))

(define-public crate-fsyaml-1 (crate (name "fsyaml") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1rzapqrfbmjxz77qr1a39fjjwxqgvdgvsq16pd3q5vpbv5dq4kd2")))

