(define-module (crates-io fs mo) #:use-module (crates-io))

(define-public crate-fsmonitor_watchman-0.1 (crate (name "fsmonitor_watchman") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.156") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (default-features #t) (kind 0)) (crate-dep (name "watchman_client") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "094gs14p55w37398c1j311x1pj2k7fkwc3x88rslbxdm9d0sk6pw") (yanked #t)))

