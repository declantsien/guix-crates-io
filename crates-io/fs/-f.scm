(define-module (crates-io fs -f) #:use-module (crates-io))

(define-public crate-fs-flatten-0.0.0 (crate (name "fs-flatten") (vers "0.0.0") (deps (list (crate-dep (name "diagnostic-quick") (req "^0.3.0") (features (quote ("walkdir" "globset"))) (default-features #t) (kind 0)))) (hash "0k18i9bzy2xkxgf246pxmlz4xdjwdi6mgmzd6byk4arsx3c300cv") (features (quote (("default"))))))

