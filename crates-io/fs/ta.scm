(define-module (crates-io fs ta) #:use-module (crates-io))

(define-public crate-fstab-0.1 (crate (name "fstab") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "~0.3") (default-features #t) (kind 0)))) (hash "1rmyw9ps97cqh1sim72akkf4blhmp4r79xh3rh51xh512zpks95m")))

(define-public crate-fstab-0.2 (crate (name "fstab") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "~0.3") (default-features #t) (kind 0)))) (hash "11h64qq71c1ahf7s602f7h6pimqn64vdc7aww5gpafi8sh8sz9pi")))

(define-public crate-fstab-0.3 (crate (name "fstab") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "~0.3") (default-features #t) (kind 0)))) (hash "0n4rs427x7dz9wx9md26xrpvlxq5yx73ln0r7ldn9pq26mqyy7hb")))

(define-public crate-fstab-0.3 (crate (name "fstab") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "~0.3") (default-features #t) (kind 0)))) (hash "0lynlc2x7dnmmrxni0kyqb5lfmg5vqvka3vqax2sn38niaxk7q7k")))

(define-public crate-fstab-0.4 (crate (name "fstab") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)))) (hash "083qi649064jxphskggg8dnc2xg1qirqcqgl8nmi5v0nhwij8c62")))

(define-public crate-fstab-generate-0.1 (crate (name "fstab-generate") (vers "0.1.0") (deps (list (crate-dep (name "disk-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "partition-identity") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1mvrf2v5nlwyw8s6wx4hphwj8lrjq86x9z0mx47pmb60sm95637k")))

(define-public crate-fstab-generate-0.1 (crate (name "fstab-generate") (vers "0.1.1") (deps (list (crate-dep (name "disk-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "partition-identity") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0znhydy4cc33kjnx66mlwakdp7k85r3f1wk0l13pp7mfp6v8d2ny")))

(define-public crate-fstab-generate-0.1 (crate (name "fstab-generate") (vers "0.1.2") (deps (list (crate-dep (name "disk-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "partition-identity") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0nsldyg3a3n4l315symv42bi29gm5l9vwy3dg1ijjn4gx622qwp6")))

(define-public crate-fstapi-0.0.1 (crate (name "fstapi") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0kxnh78w8qx94i0ga14myfyy2p7wm80xjd65xaqqn65c25nfibby")))

(define-public crate-fstapi-0.0.2 (crate (name "fstapi") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "102szzr4np9drpgw5kp7kvhgy0fjj7j99qz75qkqfymbjbmspcvm")))

