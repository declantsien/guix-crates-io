(define-module (crates-io fs ay) #:use-module (crates-io))

(define-public crate-fsays-0.1 (crate (name "fsays") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.25") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ferris-says") (req "^0.2") (default-features #t) (kind 0)))) (hash "04gy6dbk3n1nxyxfcp6vd6drsz6gqlc7wr57fasbjazv99sc7vf7")))

(define-public crate-fsays-0.3 (crate (name "fsays") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.25") (default-features #t) (kind 0)) (crate-dep (name "ferris-says") (req "^0.3") (default-features #t) (kind 0)))) (hash "1j7dz2m296fig6crsp8b6sb3ggvg10k1yypvcpg1a2910xfj3lgx")))

