(define-module (crates-io fs dr) #:use-module (crates-io))

(define-public crate-fsdr-blocks-0.1 (crate (name "fsdr-blocks") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)) (crate-dep (name "futuresdr") (req "^0.0") (default-features #t) (kind 0)))) (hash "10j6ydbgsxidgmjsb7yhy0868502imin46969lpz5vj015a3ryr4")))

