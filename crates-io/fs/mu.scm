(define-module (crates-io fs mu) #:use-module (crates-io))

(define-public crate-fsmulator-0.1 (crate (name "fsmulator") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "1z4bi70mwbrqxc1r76y5v42rsg1jy8djv2d9680d07r1y0sig0kv")))

