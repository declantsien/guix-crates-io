(define-module (crates-io fs um) #:use-module (crates-io))

(define-public crate-fsum-0.1 (crate (name "fsum") (vers "0.1.0") (hash "0g6yc4v190avgkzsx3xa0x3b4kss68dbimvi6zhj6fqjklpgq19z")))

(define-public crate-fsum-0.1 (crate (name "fsum") (vers "0.1.1") (hash "1lzrzdmvcgjp50029i3m3bl41dqmqgvab2hnddlrpryiya0falgr")))

(define-public crate-fsum-0.1 (crate (name "fsum") (vers "0.1.2") (hash "12gmqicpxq45y983xx3jblvngy5wif4sx4pq9qfvvi2511rpyn9s")))

