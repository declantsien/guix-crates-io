(define-module (crates-io fs to) #:use-module (crates-io))

(define-public crate-fstore-0.1 (crate (name "fstore") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "~1.0") (default-features #t) (kind 0)))) (hash "0m6brk6qg0f7pjs5fhdh52c5dmzb3kfsw4smsn6zy74nx1c5bizy")))

(define-public crate-fstore-rs-0.1 (crate (name "fstore-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "edit") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob-match") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "opener") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0zl0707lqxw8ba6v12pbz03xfx99hd4zkglmn80p4hwbwiq1dlkq") (yanked #t)))

(define-public crate-fstoy-0.1 (crate (name "fstoy") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.17.7") (default-features #t) (kind 0)))) (hash "023542lzbd27w9lcmg3wck0qwnv33s9fh55lyrn84r4zdrcmp7vs")))

