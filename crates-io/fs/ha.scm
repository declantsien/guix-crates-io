(define-module (crates-io fs ha) #:use-module (crates-io))

(define-public crate-fshamer-0.1 (crate (name "fshamer") (vers "0.1.0") (deps (list (crate-dep (name "number_prefix") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "03bvfkfg1id5ljjyrsd7x56ds1g1ycdzwbrv4548kxzbihh3rzcm")))

(define-public crate-fshamer-0.1 (crate (name "fshamer") (vers "0.1.1") (deps (list (crate-dep (name "number_prefix") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1757j8dhfbmfyl3cl0bb1hyghnbdvm20qxh6sx3hmnpw45cm86f2")))

(define-public crate-fshamer-0.1 (crate (name "fshamer") (vers "0.1.2") (deps (list (crate-dep (name "number_prefix") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1948si51sf3v066laq6ab5zdra67fj5gsn0m24iwc3n0di0gp3w5")))

