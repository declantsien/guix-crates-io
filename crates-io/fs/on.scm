(define-module (crates-io fs on) #:use-module (crates-io))

(define-public crate-fson-0.1 (crate (name "fson") (vers "0.1.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "pest") (req "^2.7.4") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.4") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "15gk7mqzs3dy0k4lzs7gfkg0cfzd9cadl5fh890rp8x655xsazv0")))

