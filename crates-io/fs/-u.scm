(define-module (crates-io fs -u) #:use-module (crates-io))

(define-public crate-fs-utils-0.1 (crate (name "fs-utils") (vers "0.1.0") (deps (list (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "19aa48qiamaznxk9f4dh5szxqfss93qvcslaa0af5hpyn44mgkjw")))

(define-public crate-fs-utils-0.1 (crate (name "fs-utils") (vers "0.1.1") (deps (list (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bmf9hpnsxsr2w3b7kkjakmm56dk44z016sj5lxkcrbq174dng5i")))

(define-public crate-fs-utils-1 (crate (name "fs-utils") (vers "1.0.0") (deps (list (crate-dep (name "quick-error") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1xmzrg22i835hcj3w7gdz58xgw8c4276khh3n2l9vasnqwyxclxy")))

(define-public crate-fs-utils-1 (crate (name "fs-utils") (vers "1.1.0") (deps (list (crate-dep (name "quick-error") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "0yipx40h4mz0mivfifgaqxy7w8yimxdd1i5lwizf2y3ysx40dcgq")))

(define-public crate-fs-utils-1 (crate (name "fs-utils") (vers "1.1.1") (deps (list (crate-dep (name "quick-error") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "0q6fxabdyhqdb48p5d1ks5mp9hph06gkr02h7afgrvhvxbq6j3iv")))

(define-public crate-fs-utils-1 (crate (name "fs-utils") (vers "1.1.3") (deps (list (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "0jiwczipi8hp30mc08y0aymr6r9li8firvxc1icc6bv1i6wq9ib2")))

(define-public crate-fs-utils-1 (crate (name "fs-utils") (vers "1.1.4") (deps (list (crate-dep (name "quick-error") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)))) (hash "14r5wl14mz227v0lpy89lvjzfnxgdxigvrrmm6c4r52w03fakivg")))

