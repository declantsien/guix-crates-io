(define-module (crates-io fs _u) #:use-module (crates-io))

(define-public crate-fs_util-0.1 (crate (name "fs_util") (vers "0.1.0") (deps (list (crate-dep (name "generic_error") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bxz0hbpsi7nqh19c7xxdzdd35fg3azplfxvmik3lpig7qrx24i0")))

(define-public crate-fs_util-0.1 (crate (name "fs_util") (vers "0.1.1") (deps (list (crate-dep (name "generic_error") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15jrg7xgjmhgx44x4y7plkddj0rxvmjp5ghfmdsxym3gbnawwy20")))

