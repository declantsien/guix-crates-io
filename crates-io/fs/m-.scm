(define-module (crates-io fs m-) #:use-module (crates-io))

(define-public crate-fsm-macro-0.1 (crate (name "fsm-macro") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "13kr7fl0jkn7mgk6qz3nayaik0p2qnbfg604yxhx3843fx77xc96")))

(define-public crate-fsm-rust-jb-0.1 (crate (name "fsm-rust-jb") (vers "0.1.1") (hash "1irik393ln8c32qciyrlcvm1rqwf0yc35wb7d78g7cxavw1l6lwx")))

(define-public crate-fsm-rust-jb-0.1 (crate (name "fsm-rust-jb") (vers "0.1.2") (hash "17min9r0xkbvbf70dhix9ygv9v6v7wwyz73ygvgh3z5dqb6vsjg5")))

(define-public crate-fsm-rust-jb-0.1 (crate (name "fsm-rust-jb") (vers "0.1.3") (hash "0z7ms2rhlrgjjp4vskqvqnyghh68fyf1sizv1m95f1l8nnkisdpg")))

