(define-module (crates-io fs d_) #:use-module (crates-io))

(define-public crate-fsd_interface-0.1 (crate (name "fsd_interface") (vers "0.1.17") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1n60wgjxf71v1389hbkyyga5l85vbzzq38jwgx475ap6z2zw6hn1")))

(define-public crate-fsd_interface-0.1 (crate (name "fsd_interface") (vers "0.1.18") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0p5fi8haj6sfbprc5d24mg87s0laq3d1pxwkqzan3n8g3apnhccv")))

(define-public crate-fsd_interface-0.1 (crate (name "fsd_interface") (vers "0.1.20") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0zx9sm73ypqw3ginj03bck1gnhj0pkvnbma8b0jw8xb83ysys63b")))

(define-public crate-fsd_interface-0.1 (crate (name "fsd_interface") (vers "0.1.21") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "13m71xlyjnbpmkcwgq1n08aah1rmsg3jb7liyjan8h1z2v8iy3vc")))

