(define-module (crates-io fs iz) #:use-module (crates-io))

(define-public crate-fsize-0.1 (crate (name "fsize") (vers "0.1.0") (deps (list (crate-dep (name "build-helper") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1i2yjdhjhdwhgbl67j429liw4yvlfy321a02ci3cm3ymzwn34ndk")))

(define-public crate-fsize-1 (crate (name "fsize") (vers "1.0.0") (hash "0hnankyw8lsid0i4mvd4izh03agydhzn51vi6rydwa1y948x869v")))

(define-public crate-fsize-0.1 (crate (name "fsize") (vers "0.1.1") (hash "05fb2fwp1zp4h6a1a760k6fpnva0nfmakw7n1a4fi3ai1jfc3njm")))

