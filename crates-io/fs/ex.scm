(define-module (crates-io fs ex) #:use-module (crates-io))

(define-public crate-fsextra-0.0.0 (crate (name "fsextra") (vers "0.0.0") (hash "0cr1z8vfbi1msxn54x3268zxsa6wprv60lral970lyq132kv2qdg") (yanked #t)))

(define-public crate-fsextra-0.0.1 (crate (name "fsextra") (vers "0.0.1") (hash "03r8cz6q5snp75v5w1sgdpy6y0p0pc5lwiahmqcxx59dj310rcbx") (yanked #t)))

(define-public crate-fsextra-0.1 (crate (name "fsextra") (vers "0.1.0") (hash "03ygm6mplmydmj931pprff0yzzbfyvdcc70238ipvnssglg593xf")))

(define-public crate-fsextra-0.2 (crate (name "fsextra") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.16.20") (optional #t) (default-features #t) (kind 0)))) (hash "1bhfmfphbc89c59gzzjmdz41yi1a05ixl5a7gf6d0m4hxavwql8h") (features (quote (("crypto" "ring"))))))

(define-public crate-fsextra-0.2 (crate (name "fsextra") (vers "0.2.1") (deps (list (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.16.20") (optional #t) (default-features #t) (kind 0)))) (hash "1a7rzj6dh89vvwl6z6qg20x14arjvha37rfpvym9zawm2x2p3la8") (features (quote (("crypto" "ring"))))))

(define-public crate-fsextra-0.3 (crate (name "fsextra") (vers "0.3.0-alpha.0") (deps (list (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.16.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ll5k2xaqpvngl7z3q4m2xhjiclp7m1qhnb28wwqk6a0h0p3kirn") (features (quote (("crypto" "ring"))))))

(define-public crate-fsextra-0.3 (crate (name "fsextra") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.16.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1wlz6qncpvmc3v52hdf2k0bvhrv0lswnfhpl1m55rhlby00mbcym") (features (quote (("crypto" "ring"))))))

