(define-module (crates-io fs #{3d}#) #:use-module (crates-io))

(define-public crate-fs3ds-1 (crate (name "fs3ds") (vers "1.0.0") (deps (list (crate-dep (name "vfs") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0yv2blkbcm3h3dp071s5x1zczky9ri0g0ipam1g3gjqfimbskp1s")))

