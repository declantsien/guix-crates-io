(define-module (crates-io fs ui) #:use-module (crates-io))

(define-public crate-fsuipc-0.1 (crate (name "fsuipc") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "05djiaq7f6m15xp73zzls048fv6i61841s9d8ags8yh1jgkd0iz8")))

(define-public crate-fsuipc-0.2 (crate (name "fsuipc") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "095kxdan40l9dnfmz5hwxxk5smgkm1jqr8y8kj8dbggfpa5d04b1")))

(define-public crate-fsuipc-0.3 (crate (name "fsuipc") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "08lgcnx0mhgivxvbvbigr97p8ncvbbf28af8pf3nla81aqm96h1k")))

(define-public crate-fsuipc-0.4 (crate (name "fsuipc") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cv60jhi1kxcvy2y8h5vil987qf8ycnzsqxj31c0qx3ii4lnhnrz")))

(define-public crate-fsuipc-0.5 (crate (name "fsuipc") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "14nr4gg5zka8ylsbxq73fx0wrxp67k8g98pjvd037yr1d4ag8ds6")))

