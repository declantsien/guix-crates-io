(define-module (crates-io fs -c) #:use-module (crates-io))

(define-public crate-fs-chunker-0.1 (crate (name "fs-chunker") (vers "0.1.0") (hash "0jsy5gvq3g5qfpvyykj3p5j6wm020hnkam9f3kldyxhl4kp8p02g")))

(define-public crate-fs-chunker-0.2 (crate (name "fs-chunker") (vers "0.2.0") (deps (list (crate-dep (name "sha256") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "02f6dk0x9bvmvr6b7a25nr0d9mzlb68grw4qr22wf0qr77xx0319")))

(define-public crate-fs-chunker-0.2 (crate (name "fs-chunker") (vers "0.2.1") (deps (list (crate-dep (name "sha256") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1rr7128zh88kw2jgsgl9pamjkx4l5nv65hd4067fxla261hhiha4")))

(define-public crate-fs-chunker-0.3 (crate (name "fs-chunker") (vers "0.3.0") (deps (list (crate-dep (name "sha256") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0x9gbjcqagkypjivma4zslj3y7d6hawzqgdydbx675dvmhb680qi")))

(define-public crate-fs-chunker-0.4 (crate (name "fs-chunker") (vers "0.4.0") (deps (list (crate-dep (name "blake3") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1x66dx1kkpkbzmdzf9jh9jqlap9nbijfbzl76cn9grm230k2ggaf")))

(define-public crate-fs-crypto-0.1 (crate (name "fs-crypto") (vers "0.1.0") (deps (list (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0c5lm4fdj0xkr8wy574kq5q5cbgfzq8xwb1yyswfqb63p3abkxv2")))

