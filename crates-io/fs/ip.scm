(define-module (crates-io fs ip) #:use-module (crates-io))

(define-public crate-fsipc-0.9 (crate (name "fsipc") (vers "0.9.1") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^3.0") (default-features #t) (kind 0)))) (hash "09z68w3h7cyzdn6md91l24b54cjid25x4pjyim9j1d75qyhygy3c")))

(define-public crate-fsipc-0.9 (crate (name "fsipc") (vers "0.9.2") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^3.0") (default-features #t) (kind 0)))) (hash "00cay5ljhk49b7xijw1yadvxr7gvkadnq0an0290bb0i586w1vzk")))

(define-public crate-fsipc-0.9 (crate (name "fsipc") (vers "0.9.5") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^3.0") (default-features #t) (kind 0)))) (hash "0rfrhl7qaya08i00vbyvrwxrhs14jjyk7c2n3yiqags3bdxy4sk1")))

