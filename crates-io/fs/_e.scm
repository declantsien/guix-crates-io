(define-module (crates-io fs _e) #:use-module (crates-io))

(define-public crate-fs_eventbridge-0.1 (crate (name "fs_eventbridge") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.77") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "0hz0g3x9l27ac4j48b55141x5kzjwh5r86xfhf4278m7l32r5a23")))

(define-public crate-fs_extra-0.1 (crate (name "fs_extra") (vers "0.1.0") (hash "1r3gkyr8pvj7drs4ysdhyg70jwrwz97rrqhs2a0wybax3my3dap9")))

(define-public crate-fs_extra-0.2 (crate (name "fs_extra") (vers "0.2.0") (hash "14g7a3nk6j7yqz6rd7dgrs50np0y6fcdj6bw8inmgl6n9d4rryqz")))

(define-public crate-fs_extra-0.2 (crate (name "fs_extra") (vers "0.2.1") (hash "0h1jhzzlimpwif17hsgdm388p7aaf2isq1m5rji23dnsc756hbgm")))

(define-public crate-fs_extra-0.3 (crate (name "fs_extra") (vers "0.3.0") (hash "1xppsdlrgwhdrzjjsvk820549vw2nzvwc99x8zkm0c0kyvigvi5n")))

(define-public crate-fs_extra-0.3 (crate (name "fs_extra") (vers "0.3.1") (hash "0w88044samf0387zxm0ivpgym6iq72f67c62119x4g959a6pc0hh")))

(define-public crate-fs_extra-1 (crate (name "fs_extra") (vers "1.0.0") (hash "0k4xghm4l9smf1c8rnbyc0gp2k57g10grj319snkf2ln3bxbs4nq")))

(define-public crate-fs_extra-1 (crate (name "fs_extra") (vers "1.1.0") (hash "0x6675wdhsx277k1k1235jwcv38naf20d8kwrk948ds26hh4lajz")))

(define-public crate-fs_extra-1 (crate (name "fs_extra") (vers "1.2.0") (hash "151k6dr35mhq5d8pc8krhw55ajhkyiv0pm14s7zzlc5bc9fp28i0")))

(define-public crate-fs_extra-1 (crate (name "fs_extra") (vers "1.3.0") (hash "075i25z70j2mz9r7i9p9r521y8xdj81q7skslyb7zhqnnw33fw22")))

