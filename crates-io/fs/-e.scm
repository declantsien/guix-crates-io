(define-module (crates-io fs -e) #:use-module (crates-io))

(define-public crate-fs-encrypt-0.1 (crate (name "fs-encrypt") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "0.10.*") (default-features #t) (kind 0)))) (hash "1ch7w5xqiwq8wvinmgk4xk8g0f5fy96vmr2k25p1p8lvzdx3jdna")))

(define-public crate-fs-encrypt-0.1 (crate (name "fs-encrypt") (vers "0.1.1") (deps (list (crate-dep (name "aes") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "0.10.*") (default-features #t) (kind 0)))) (hash "0v2r5yn94q4admz19mwx8icsisrvas3wmbk2akn4kv19834d03iv")))

(define-public crate-fs-encrypt-0.1 (crate (name "fs-encrypt") (vers "0.1.2") (deps (list (crate-dep (name "aes") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "0.10.*") (default-features #t) (kind 0)))) (hash "0zj5y5fz7950lkfy608chgmwlgkp4m0c7299rhmg7gywij9gjp15")))

(define-public crate-fs-encrypt-0.1 (crate (name "fs-encrypt") (vers "0.1.3") (deps (list (crate-dep (name "aes") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "0.12.*") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "0.10.*") (default-features #t) (kind 0)))) (hash "0f2i3b33gvmvbnyi5q8bmw48xb0q1zv2razk93jakfm0k9yvpgjj")))

(define-public crate-fs-err-0.1 (crate (name "fs-err") (vers "0.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "12mh73qxwardcbw947khws0c3zhfd2sib43i46p465wxfadbwqq2")))

(define-public crate-fs-err-0.1 (crate (name "fs-err") (vers "0.1.1") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1w1vawa2rnry3aa4cd77srkdp6x4580c135q14x000iilsf6df1s")))

(define-public crate-fs-err-0.1 (crate (name "fs-err") (vers "0.1.2") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "17v6sm1bfaw210fjlkfhymj1chgng196js1d6gdqgwd9x96ya4ak")))

(define-public crate-fs-err-1 (crate (name "fs-err") (vers "1.0.0") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "06l5fsnnlq1ay0jhp7nlalhx64ki2hncjwjrddcvbpqbk9hsv9pj")))

(define-public crate-fs-err-1 (crate (name "fs-err") (vers "1.0.1") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0lfnldyij12biwbgmc147pinsnars8zxagr9macvvqia2vfc6gy3")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.0.0") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0i38w512vqhrrlidv87rmdkn43wmzbgnwhj2yc538ff74jvgzj10")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.0.1") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0r36l7q50w3ii1l1pvjv9l6cr17fxnzwq8q0hfsl60az077rzmf2")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0szvkqw3d0r9v5gg6vp7v36dwnyd6md24zaxm5ppvyar7p2dvs87")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.2.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1gpk71df1n4lpljg22wdx649f032vqp6nyj6hz1fb61rxbflhz4w")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.3.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "06i4gk13kv9ppch6gmp5749c9ys9ivip9akv3x9vxvsqf65iz9f1")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.4.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0dill0fjh1ncw537vkckh883r2mwqvm2mf0372hvid3fa2gvpjb4")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.5.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0c9l4lzy7rca87zir4dbmmf1751h9nh6cvg21ai75nlbwhx1dldw")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.6.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "1q5z8d6q8310fgzg78bpmhyfigz7fkgp1bbmhcxq85k1ml23bgay")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.7.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)))) (hash "0bk5fmwyk8b3lgfl5mi133s743hwq3z6awgvi6pd75d48nirzmsv")))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.8.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)))) (hash "1d4c4v5ss9qq9r2a36aqxabivn4dlr7najnwyf3lhasrny36wkgv") (features (quote (("io_safety"))))))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.8.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)))) (hash "0l2wxmfsy2sngyc7szcg9cj7y3zisn46fdm68cpndw3054k3xnv4") (features (quote (("io_safety"))))))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.9.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("fs"))) (optional #t) (kind 0)))) (hash "0ha5ysh5jz2hxlhmydc82pjcycps6ips4jyni41jy8cr48jzli88") (features (quote (("io_safety"))))))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.10.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("fs"))) (optional #t) (kind 0)))) (hash "17yjwbsim0bsny08lxryyz79gj8vq2c19dcmsg5qf44bpsydjpzv") (features (quote (("io_safety"))))))

(define-public crate-fs-err-2 (crate (name "fs-err") (vers "2.11.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("fs"))) (optional #t) (kind 0)))) (hash "0hdajzh5sjvvdjg0n15j91mv8ydvb7ff6m909frvdmg1bw81z948") (features (quote (("io_safety"))))))

