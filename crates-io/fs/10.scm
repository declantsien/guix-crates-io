(define-module (crates-io fs #{10}#) #:use-module (crates-io))

(define-public crate-fs1027-dg-hal-0.1 (crate (name "fs1027-dg-hal") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1c0sg1dkii3r39hf31c7z7nc103kja95fkc2yp22hna6vvxqa8iy")))

(define-public crate-fs1027-dg-hal-0.1 (crate (name "fs1027-dg-hal") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0wvsgb3vprds9a53xd804f264p314gac361ps48r31y7s1l360rq")))

(define-public crate-fs1027-dg-hal-0.1 (crate (name "fs1027-dg-hal") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0clk7bdwfvf19clhjm74wv82ndzz46aylwqdhpk4w3hmp5b0f6b8")))

(define-public crate-fs1027-dg-hal-0.2 (crate (name "fs1027-dg-hal") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1wmgfmk4mv920lvbz8n2jqkldgbbnjmr49bavdxvd2blmrsl9zi5")))

