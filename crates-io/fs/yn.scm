(define-module (crates-io fs yn) #:use-module (crates-io))

(define-public crate-fsync-0.1 (crate (name "fsync") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0hq9x8q9y540n33lqwa7bnx5zdadqifr1kkcabvpswnbfyj1pry6")))

