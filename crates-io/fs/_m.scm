(define-module (crates-io fs _m) #:use-module (crates-io))

(define-public crate-fs_metadata-0.1 (crate (name "fs_metadata") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "1wsrxxwwf9pzq5f2vh5i1vxbg63kbpgd8jax7kcjp8pmy48np9gg")))

(define-public crate-fs_metadata-0.2 (crate (name "fs_metadata") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "1d2y4fld7lkzh65lraqggjcjiqyicxslmzi2m57qmy0ngvmf0vci")))

(define-public crate-fs_metadata-0.3 (crate (name "fs_metadata") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "faccess") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0jk48hgmykx1z574vwny0n9y1s0bqp9b1b48cmg51jqb7l2qlhkv")))

(define-public crate-fs_metadata-0.3 (crate (name "fs_metadata") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "faccess") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "04vvlfkmzy11g7wngfkakrh5b5gbkqcc5wd1ja4xdcr8glgvysdc")))

