(define-module (crates-io fs cx) #:use-module (crates-io))

(define-public crate-fscx-rs-0.1 (crate (name "fscx-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1b26zn8psx68virx2w9z9gjzjpmzr6digwyfbwqy800hndvhnwnh")))

(define-public crate-fscx-rs-0.1 (crate (name "fscx-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0ymr82c6cs5gqr9w1cbnccpf80f7a366d2w89i0xjh47z0y7rb8f")))

(define-public crate-fscx-rs-0.1 (crate (name "fscx-rs") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "122f707kx79bmjvir8mlmgn7xfbm7v5kvsmgapqnjfs8wq74hzrl")))

(define-public crate-fscx-rs-0.1 (crate (name "fscx-rs") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "146a57i69h3zy87gvba2gcz7kbdlaj7f5jrq5wzrgkr9znl4gcpc")))

