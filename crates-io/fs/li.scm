(define-module (crates-io fs li) #:use-module (crates-io))

(define-public crate-fsling-0.0.1 (crate (name "fsling") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "0hb4yrd4rd9py3nyijvvh8jm5sq8jgccdzn91yda06dhzf2idxl9")))

