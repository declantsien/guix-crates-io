(define-module (crates-io fs -t) #:use-module (crates-io))

(define-public crate-fs-tail-0.1 (crate (name "fs-tail") (vers "0.1.0") (hash "144hk86yvrq179jzsvk45ld5if9r0jxk6m7y2wm2b3kl2bic5w8d")))

(define-public crate-fs-tail-0.1 (crate (name "fs-tail") (vers "0.1.1") (deps (list (crate-dep (name "memchr") (req "^2.3.4") (default-features #t) (kind 0)))) (hash "1q3q53wz89079d0wv6qzcin1k1ah45rx41cnnpkpzjr6zmjxc660")))

(define-public crate-fs-tail-0.1 (crate (name "fs-tail") (vers "0.1.2") (deps (list (crate-dep (name "memchr") (req "^2.3.4") (default-features #t) (kind 0)))) (hash "13il7vr8fh7xidlrb5jiz0dmnnrxag6aspdnm3zyljbfb50k9px7")))

(define-public crate-fs-tail-0.1 (crate (name "fs-tail") (vers "0.1.3") (deps (list (crate-dep (name "memchr") (req "^2.3.4") (kind 0)))) (hash "0867rm9pzhqd0yqf9qkha4ipl6gc6d16mws9vlis3m3s34kpbdaw")))

(define-public crate-fs-tail-0.1 (crate (name "fs-tail") (vers "0.1.4") (deps (list (crate-dep (name "memchr") (req "^2.3.4") (kind 0)))) (hash "1lkadcslflg779p1w14h092w0xm35ibxhslpm9wnvbdjjj3fxj3k")))

(define-public crate-fs-tool-0.0.0 (crate (name "fs-tool") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "diagnostic-quick") (req "^0.3.0") (features (quote ("globset"))) (default-features #t) (kind 0)) (crate-dep (name "fs-flatten") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "14msmn1ak2606za285fn094hwf3ny4qk2haia7yd1w8443f4xdky") (features (quote (("default"))))))

(define-public crate-fs-tracing-0.1 (crate (name "fs-tracing") (vers "0.1.0") (deps (list (crate-dep (name "color-eyre") (req "^0.5.10") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "tracing-error") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.15") (default-features #t) (kind 2)))) (hash "1d26758vd2882f3iw2j3673w7p083g4z6cbpa4acwj26ymrc36yw")))

(define-public crate-fs-tree-0.1 (crate (name "fs-tree") (vers "0.1.0") (hash "0b07h6wgz6r6pbfi1fv6k4yjq4k2683kmwsd0caaffj73m7ndb0x")))

(define-public crate-fs-tree-0.1 (crate (name "fs-tree") (vers "0.1.1") (hash "0zm5729dl39lqfnz1s009qjdbm4cyi0ip0ld1lah3hvww5akm5cd")))

(define-public crate-fs-tree-0.1 (crate (name "fs-tree") (vers "0.1.2") (hash "08fryaggp7fxhp62r9yzzikzp0mz0dkpy53lyqfrxshaiwflkqcp")))

(define-public crate-fs-tree-0.1 (crate (name "fs-tree") (vers "0.1.3") (hash "1cwww67cafcs5fv174p2psiygpcdw6adlmhi5c2prjrkzww7c5ry")))

(define-public crate-fs-tree-0.2 (crate (name "fs-tree") (vers "0.2.0") (deps (list (crate-dep (name "file_type_enum") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ifsv75nzwpim17ccaghvivyfh01fyjn22sk5yixas13fri94jn1") (yanked #t)))

(define-public crate-fs-tree-0.2 (crate (name "fs-tree") (vers "0.2.1") (deps (list (crate-dep (name "file_type_enum") (req "^0.11") (default-features #t) (kind 0)))) (hash "1gfh0rxmx5ycsih6zwyk1a3c3kk660dazrf7ixdl0vnxmn1mai5j") (yanked #t)))

(define-public crate-fs-tree-0.2 (crate (name "fs-tree") (vers "0.2.2") (deps (list (crate-dep (name "file_type_enum") (req "^0.11") (default-features #t) (kind 0)))) (hash "1wzj1xhawmjks8gsnw2j8w539vs4184msf7nyzpkp4kwri8rz0bs")))

(define-public crate-fs-tree-0.3 (crate (name "fs-tree") (vers "0.3.0") (deps (list (crate-dep (name "file_type_enum") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)))) (hash "0wivrbxp2bc8bgmp92gi6iay73glkvbkvi9cqbm9jahx5xv1fhnq") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.4 (crate (name "fs-tree") (vers "0.4.0") (deps (list (crate-dep (name "file_type_enum") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "14pn7ykap5injm5sa1m5250dw5ig90yarfwp8mn2cq8g6q88758i") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5 (crate (name "fs-tree") (vers "0.5.0") (deps (list (crate-dep (name "file_type_enum") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1nfqfn1madh5n7c591sh1c3hg8lgjm35mr94z4kpqi1rr9s5491a") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5 (crate (name "fs-tree") (vers "0.5.1") (deps (list (crate-dep (name "file_type_enum") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0mam5hqj2vapzk2qgjrxl092kh0a56vy90ic8qqv2z5l4x9y0z0s") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5 (crate (name "fs-tree") (vers "0.5.2") (deps (list (crate-dep (name "file_type_enum") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0kwd5ykxnwh1qrndhdzf9h5fsf9l60215bsbc8k7fia2vv54gbcy") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5 (crate (name "fs-tree") (vers "0.5.3") (deps (list (crate-dep (name "file_type_enum") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "17q69cb05370fv08vfmbw4jpiqb1z6h8kvpxhd0gh291nqwgmb6z") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5 (crate (name "fs-tree") (vers "0.5.4") (deps (list (crate-dep (name "file_type_enum") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 2)))) (hash "1qijh6v25k9d3p053yzy8b4c5yc4r2d06ry8iiv0a9wp6dyw3bmj") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-tree-0.5 (crate (name "fs-tree") (vers "0.5.5") (deps (list (crate-dep (name "file_type_enum") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 2)))) (hash "17lmm6vx04kqw2vbnwkrpxnh4ikkds11rbk08l6gf8xj6mw9l5xh") (features (quote (("default" "fs-err")))) (v 2) (features2 (quote (("libc-file-type" "dep:libc") ("fs-err" "dep:fs-err"))))))

(define-public crate-fs-trie-0.1 (crate (name "fs-trie") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.1.6") (default-features #t) (kind 2)))) (hash "0iz0s0alx0wsn4hd523pqbn0fy3vs7r0xh4sy7rfj8am9nv14k29")))

(define-public crate-fs-trie-0.1 (crate (name "fs-trie") (vers "0.1.3") (deps (list (crate-dep (name "bincode") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.1.6") (default-features #t) (kind 2)))) (hash "0fm66jskjd980xmi460yaw8wppg999p9rj0wf77bmbf42cfpl92f")))

(define-public crate-fs-trie-0.1 (crate (name "fs-trie") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.1.6") (default-features #t) (kind 2)))) (hash "1nx9rblkv29qvjccw72fa8i26091z58vvxpjbm2sq7ivzz4c1xac")))

