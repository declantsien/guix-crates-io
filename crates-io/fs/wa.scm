(define-module (crates-io fs wa) #:use-module (crates-io))

(define-public crate-fswallet-0.1 (crate (name "fswallet") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "base58") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "hdwallet") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "hdwallet-bitcoin") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.14.6") (default-features #t) (kind 0)) (crate-dep (name "ripemd160") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "07fn9g34161kzx69p59q4nb5fj245vpmzw5xw5gnwsw82wfj174s")))

(define-public crate-fswatch-0.1 (crate (name "fswatch") (vers "0.1.10") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fswatch-sys") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0m98qa4w6wnmlrbiwmhxph5q2p48qbg4ii8cik6vq98myi2zvxf7") (features (quote (("use_time" "time") ("fswatch_1_10_0" "fswatch-sys/fswatch_1_10_0") ("default"))))))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "046znhb1wsvnm3f0fa8fmicx6d2py47ayb85fz96drw8bz82jkag") (yanked #t)))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f66b72j0w89c4lyi461y9mpmmnwgp65m2bd99bkqnilj5xzqqqa") (yanked #t)))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03v0wrhwq4cxf566ak7m86vhqbcdiyzlz5kag2mjvkxx81gvlqzn")))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x4jdixkk9kznfmkzcifyw8w5vxgah5y3p3nws90i1xv24q6hq3w")))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14j79am3ncfijk5w3g4faxfdhyszin2s4sic4w47bwljfmcy6ffm")))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hnccmxklxp4gd485s0h0zf8q4n7q0k86fav3pgmnhf1ngxsqna9")))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1f3b41pqwjx05jy1dpr6cnpfw8fap0h3adgq694bsrlhmqh3mr6g") (features (quote (("use_time" "time") ("default"))))))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s44mqg7cfxz56sgqa5q0nw41y2fl7laa9bqdkl29zgz8qxdkg1d")))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.9") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mcsc5s86mb2dl3br8k3rhxgxnqv5gkmy3hb8fzaz51rfaclfycm") (features (quote (("fswatch_1_10_0"))))))

(define-public crate-fswatch-sys-0.1 (crate (name "fswatch-sys") (vers "0.1.10") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rphdp6z1rbcp5rl4wr83qh27yq1akm0y64qrcj6yyw6mwq28kvj") (features (quote (("fswatch_1_10_0"))))))

