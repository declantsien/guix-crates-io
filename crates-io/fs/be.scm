(define-module (crates-io fs be) #:use-module (crates-io))

(define-public crate-fsbex-0.0.0 (crate (name "fsbex") (vers "0.0.0") (hash "0hfbijj03cbic0x31j3c76a995gc89m2s1751a1pqvcrs12rzqj3") (yanked #t)))

(define-public crate-fsbex-0.1 (crate (name "fsbex") (vers "0.1.0") (deps (list (crate-dep (name "bilge") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "lewton") (req "^0.10.2") (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "vorbis_rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "16b72xg7i1xkn6dfmpljjx11sycswdama898f4fhirh0v5ag9z6r")))

(define-public crate-fsbex-0.2 (crate (name "fsbex") (vers "0.2.0") (deps (list (crate-dep (name "bilge") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "lewton") (req "^0.10.2") (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "vorbis_rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0nzwvzhbryan03l03rldia4fv8wa9f5vv8629cjgfdqnz1bws840")))

(define-public crate-fsbex-0.2 (crate (name "fsbex") (vers "0.2.1") (deps (list (crate-dep (name "bilge") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lewton") (req "^0.10.2") (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "vorbis_rs") (req "^0.4.0") (features (quote ("stream-serial-rng"))) (default-features #t) (kind 0)))) (hash "0ai97i32gd8s47vpjy1bwrzq7w5kla4w5412sj10pcvk63z6zghc")))

(define-public crate-fsbex-0.2 (crate (name "fsbex") (vers "0.2.2") (deps (list (crate-dep (name "bilge") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lewton") (req "^0.10.2") (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "vorbis_rs") (req "^0.4.0") (features (quote ("stream-serial-rng"))) (default-features #t) (kind 0)))) (hash "01rkn9jlj01xg42g8lxzj2xn9iyrlc9mb6mwz3lvkl7l9y4i1w46")))

(define-public crate-fsbex-0.3 (crate (name "fsbex") (vers "0.3.0") (deps (list (crate-dep (name "bilge") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lewton") (req "^0.10.2") (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "tap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "vorbis_rs") (req "^0.5.0") (features (quote ("stream-serial-rng"))) (default-features #t) (kind 0)))) (hash "1k3xr8ij5bi7bg1p6hylqsfhy4k41g5p52i3djbdk3mr8w75fbin")))

