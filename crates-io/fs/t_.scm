(define-module (crates-io fs t_) #:use-module (crates-io))

(define-public crate-fst_stringstring-0.0.1 (crate (name "fst_stringstring") (vers "0.0.1") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)))) (hash "15izyk2nrn53z58zcm48bs2xgx3sd2iqclkn5l1yzlr1i6fjhy9z")))

(define-public crate-fst_stringstring-0.0.2 (crate (name "fst_stringstring") (vers "0.0.2") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)))) (hash "1amrz1l1a2n1z7hdyba5ph9p79jjsklnamkpi359kzq7503qvnar")))

(define-public crate-fst_stringstring-0.0.3 (crate (name "fst_stringstring") (vers "0.0.3") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)))) (hash "1b8wsav9m7rhjv4kwnjh18rn16fj4swry3bzda2a294wkdsvpaa4")))

(define-public crate-fst_stringstring-0.0.4 (crate (name "fst_stringstring") (vers "0.0.4") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "1qliwl4jxihjpk1h1f1pi6ianm3afdsspjr2kif8yaj21bzy03ym")))

(define-public crate-fst_stringstring-0.0.5 (crate (name "fst_stringstring") (vers "0.0.5") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0z1n4p7lsr7hd5cl32i5x43a46zv15c832lwyib9sq4hm8xmy102")))

(define-public crate-fst_stringstring-0.0.6 (crate (name "fst_stringstring") (vers "0.0.6") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "13d1yds2shlhjglz6yln402zbj2ifll0ilpyns1x58g21v0a3mlm")))

(define-public crate-fst_stringstring-0.0.7 (crate (name "fst_stringstring") (vers "0.0.7") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0n4l8msgra06lk5xjjpz976g292zjxsdxc4h839mc7vghvxqsxjq")))

(define-public crate-fst_stringstring-0.0.8 (crate (name "fst_stringstring") (vers "0.0.8") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "1dyxsm8403njqpr6c981sb2ijkmi66vw41si8vx0idi1hrxk0dw9")))

(define-public crate-fst_stringstring-0.0.9 (crate (name "fst_stringstring") (vers "0.0.9") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0w90a0wq3xz4125lvgpszharyi58ycahmhrsrfyapga6qkp4fawc")))

(define-public crate-fst_stringstring-0.0.10 (crate (name "fst_stringstring") (vers "0.0.10") (deps (list (crate-dep (name "fst") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0vjf61bc3wh5n4832vgh8ih87da0jfsnxgcz705q8kcibxha78gj")))

