(define-module (crates-io fs -v) #:use-module (crates-io))

(define-public crate-fs-verity-0.1 (crate (name "fs-verity") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.81") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "parse-display") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0plhqw53ajh62jpyq8mygya6hyyrr8f31g2q83jrfbn1bqc9lq66")))

(define-public crate-fs-verity-0.2 (crate (name "fs-verity") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "parse-display") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1b1a786bv4dfm9sdjrwij6fav7c47s9qlydhjabr40k16382a3jh")))

