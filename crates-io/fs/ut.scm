(define-module (crates-io fs ut) #:use-module (crates-io))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0ncd17ca2g3wjnn178i6p42c05brrwwz76yy96dqrgm5xxqgkh91")))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0yc5abk56b3mby38b4ab2kbv9jbr8qxcdfq6ivkfdr844zz1wssv")))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1bca2parq8802bhxmlc1ydb50rf6cbb6z2s5ss15yklr381r58ld")))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1b1dlk354d3255vn5ax2iscdz4izzw161jkclsjjpw4gawwk6892")))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.4") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "01f18gwvsbjr5z1yzxlx897jmabcchzms4nhjvqyqirdjfy1nkbb")))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.5") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "18ljlsdiy3lbvhrln1laxvv59s8gv2l8m3h4434r93nmhnmwm59i")))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.6") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1iv96idzzplxpiaajf5dmygx899adsxw7z2cp6n8rk7kk8b57hv0")))

(define-public crate-fsutils-0.1 (crate (name "fsutils") (vers "0.1.7") (deps (list (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0j48ljin6vrid30fzsd2dak9jr7wa8x0vy94c9sqyd6pkpzca9gh")))

