(define-module (crates-io fs td) #:use-module (crates-io))

(define-public crate-fstd-0.1 (crate (name "fstd") (vers "0.1.0") (hash "0n4ygs0mv5zbp38ldd89gf5zblr75sdy5q28ppgi5qs90v5zdgmj")))

(define-public crate-fstd-0.1 (crate (name "fstd") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0i4y5cd8n8zbpqvbzg0965w1w4cljrnkfbnpbmi6ais12n5i4hg9") (features (quote (("rand_nobias"))))))

(define-public crate-fstd-0.1 (crate (name "fstd") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "08nkbbmi2yyp5kw38fzs4h7hj1fbgsyqmdwh3ymdidi6wdc7sia4") (features (quote (("rand_nobias"))))))

