(define-module (crates-io fs g_) #:use-module (crates-io))

(define-public crate-fsg_guessing_game-0.1 (crate (name "fsg_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "133grv3yrbpn5aavkzrc3vljr7vkd0kvwg0ygh9kpgln4fdfby55") (yanked #t)))

(define-public crate-fsg_guessing_game-0.1 (crate (name "fsg_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "08jgx829csiiw6hb3c9j7hlvwa130za5diwqim0npww9xsrfklw5")))

