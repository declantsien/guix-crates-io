(define-module (crates-io fs #{-i}#) #:use-module (crates-io))

(define-public crate-fs-id-0.1 (crate (name "fs-id") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("std" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0mlf5dx83hd71n3azr8i3ia4ya26pd5c97pq0mqs7q5szkwhdwvw") (rust-version "1.66")))

(define-public crate-fs-id-0.2 (crate (name "fs-id") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "wasi") (req "^0.11.0") (default-features #t) (target "cfg(target_family = \"wasm\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("std" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0f7r7adnm5z7z0h4v1k3a60xgnyrph7yvhdmc4nh93g9i4dqqr7i") (rust-version "1.66")))

