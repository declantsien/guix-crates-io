(define-module (crates-io fs #{30}#) #:use-module (crates-io))

(define-public crate-fs3000-0.1 (crate (name "fs3000") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "interp") (req "^1") (default-features #t) (kind 0)))) (hash "1bmm69ryg1vxbi98gvj5alv731kvsnvzfwgwvv8vy59ibp1xdsdj")))

