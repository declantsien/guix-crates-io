(define-module (crates-io mz #{16}#) #:use-module (crates-io))

(define-public crate-mz16-0.1 (crate (name "mz16") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0jcb282ji42jy9nyfqaij2ihga0zpsbdwm0mxqfy3ksy2cbblmc6")))

(define-public crate-mz16-0.1 (crate (name "mz16") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "0lh6lwwzvwr7ach8phm30r2d2r1irmfdsaizdabh7wwpx804d8hi")))

(define-public crate-mz16-0.1 (crate (name "mz16") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "143q133jxrcln72xslhjw2ph86rv1n3rd8sfqxnkxx3hcn3hz22k")))

