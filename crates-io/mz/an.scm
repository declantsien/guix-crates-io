(define-module (crates-io mz an) #:use-module (crates-io))

(define-public crate-mzandvliets_hello_world_crate-0.1 (crate (name "mzandvliets_hello_world_crate") (vers "0.1.0") (hash "0qx8mxjz8h1jn032v1l4nnyx4pjpgj8h091a2600620yhhyak70a")))

(define-public crate-mzandvliets_hello_world_crate-0.1 (crate (name "mzandvliets_hello_world_crate") (vers "0.1.1") (hash "0xa5k5y3iqcbg0mphzz4xgq5qvyxj7ybj007pbc04z5psrqv6c7j") (yanked #t)))

(define-public crate-mzandvliets_hello_world_crate-0.1 (crate (name "mzandvliets_hello_world_crate") (vers "0.1.2") (hash "1dr76my1ysg7zcqr7cid3ig61mj6d30l7rqb2rdpws39mfqkdl7b")))

