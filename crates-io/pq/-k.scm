(define-module (crates-io pq -k) #:use-module (crates-io))

(define-public crate-pq-kem-0.1 (crate (name "pq-kem") (vers "0.1.0") (deps (list (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rac") (req "^1.0") (default-features #t) (kind 0)))) (hash "08wklcb4k200jmnd06wzr413gz1sq2y32vn4k5swya0vlx2zazs4")))

(define-public crate-pq-kem-0.2 (crate (name "pq-kem") (vers "0.2.0") (deps (list (crate-dep (name "rac") (req "^1.1") (default-features #t) (kind 0)))) (hash "07gysp9iws6ncgj3b8l22fn1y0sg6c67xc5l19i11m9ckrql2wcd")))

(define-public crate-pq-kem-0.3 (crate (name "pq-kem") (vers "0.3.0") (deps (list (crate-dep (name "digest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rac") (req "^1.1") (default-features #t) (kind 0)))) (hash "1b5dwhcb2qpca49qabw8g9kkrzlgl3jw5yf4jzjwpp50q035gac1")))

(define-public crate-pq-kem-0.4 (crate (name "pq-kem") (vers "0.4.0") (deps (list (crate-dep (name "rac") (req "^1.3") (default-features #t) (kind 0)))) (hash "0gva35c4h56c968g6n3c8l6nwy8w1ylq9ip9lp9psq4m1xixqp1s")))

(define-public crate-pq-kem-0.5 (crate (name "pq-kem") (vers "0.5.0") (deps (list (crate-dep (name "rac") (req "^1.3") (default-features #t) (kind 0)))) (hash "1wydgfc96l4is0n0za7g5c8ky0hjxsfyxgy6visr2s98vh1rl0an")))

