(define-module (crates-io pq ca) #:use-module (crates-io))

(define-public crate-pqcat-0.1 (crate (name "pqcat") (vers "0.1.0") (deps (list (crate-dep (name "polars") (req "^0.39.2") (features (quote ("lazy" "parquet" "dtype-full"))) (default-features #t) (kind 0)))) (hash "0npl80vbmckxrw9m01lyy1q3clgl0vvgmsskbffj4vh9xkg0qv75")))

(define-public crate-pqcat-0.1 (crate (name "pqcat") (vers "0.1.1") (deps (list (crate-dep (name "polars") (req "^0.39.2") (features (quote ("lazy" "parquet" "dtype-full" "timezones"))) (default-features #t) (kind 0)))) (hash "0jih9kgr0r09ddz525hm1y6plw8mdf5ii8r38cs7ppyd4ahblh9y")))

