(define-module (crates-io pq gr) #:use-module (crates-io))

(define-public crate-pqgrams-0.9 (crate (name "pqgrams") (vers "0.9.0") (hash "0b8p8li4gbcnm6g5cjrzxd1ff2v1yyyxywjy4q32y0bcaavxj3ik")))

(define-public crate-pqgrams-0.9 (crate (name "pqgrams") (vers "0.9.1") (hash "0jfwprxlnb615rgpbx4cmy208q3dsczi86js9k1lnag03qca78cf")))

(define-public crate-pqgrams-0.9 (crate (name "pqgrams") (vers "0.9.2") (hash "11n65hbp61jnj7i2l774j8jgdj7d3sjq80fycblsymsbxhvpdyfd")))

