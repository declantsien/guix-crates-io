(define-module (crates-io pq -s) #:use-module (crates-io))

(define-public crate-pq-src-0.1 (crate (name "pq-src") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-src") (req "^300") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (features (quote ("vendored"))) (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "0qg4z6v1pcf08ny7yvb1q4qhwx6iym1fxbjixdwr03kssycq7j2p") (features (quote (("with-asan") ("default"))))))

(define-public crate-pq-src-0.1 (crate (name "pq-src") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-src") (req "^300.0.0") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (features (quote ("vendored"))) (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "0qnix76hnjj7h9jpi3pqk7dx9p31indqknysbslnr89jizwz4ys2") (features (quote (("with-asan") ("default")))) (links "pq_sys_src")))

(define-public crate-pq-src-0.1 (crate (name "pq-src") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-src") (req "^300.0.0") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (features (quote ("vendored"))) (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "0s8sfc54z2pm7b1d71bqbgbyr2amaw60h07s9sfvhzsmvjbzsa0y") (features (quote (("with-asan") ("default")))) (links "pq_sys_src")))

(define-public crate-pq-src-0.1 (crate (name "pq-src") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-src") (req "^300.0.0") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (features (quote ("vendored"))) (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "1b9i9mkshm58gwj3jrb5wrj56jrmh54a26mqp2b52j236wj9ngqz") (features (quote (("with-asan") ("default")))) (yanked #t) (links "pq_sys_src")))

(define-public crate-pq-src-0.1 (crate (name "pq-src") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-src") (req "^300.0.0") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (features (quote ("vendored"))) (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "1qv7jr687y1jng3l0hnhc0mgyqdhvgjc5lp2ij0s1l32aywmm9jk") (features (quote (("with-asan") ("default")))) (links "pq_sys_src")))

(define-public crate-pq-src-0.1 (crate (name "pq-src") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-src") (req "^300.0.0") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (features (quote ("vendored"))) (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "1qc06kq3siclzsl8y3g862prkm2cj7f8mg5kxa7k1pkdrxynqyxl") (features (quote (("with-asan") ("default")))) (links "pq_sys_src")))

(define-public crate-pq-src-0.1 (crate (name "pq-src") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-src") (req "^300.0.0") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (features (quote ("vendored"))) (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "1fgz8m973wr9jd1xbxfbsybwfd51mvqb2hxfb3cjzf6dw2jsb9a4") (features (quote (("with-asan") ("default")))) (links "pq_sys_src")))

(define-public crate-pq-src-0.2 (crate (name "pq-src") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.93") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)))) (hash "07dgq76xdvp0y247km67apf3vfzqbhlpai8rz2lp7g95wkdmwxhc") (features (quote (("with-asan") ("default")))) (links "pq_sys_src")))

(define-public crate-pq-sys-0.1 (crate (name "pq-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)))) (hash "054n9lnrs59yjna3p1pcfwfslwq2p0x6nzxkmcgbq7zwg1agdah1")))

(define-public crate-pq-sys-0.1 (crate (name "pq-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1q60pi4imkzjl3qyphlrj2n0j5hxbhpc5savl5yhvpjsssl4xdwr")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "10aa2hsl19k1kpwig655zm4i0xxg9z97g38gpi5qafigsp2pbfyq")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0cb8dmbkl3rlkhmlvs60jhixgb7plwvy9hlmxmjl47lkwx8w66ff")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1kspqp3ydh2p07jls23dkqf2lyzy2qznqbk6wqlcvvpsjxqyynr1")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0ss3a6c6s6g494nprzdv56xh13m17dbwzkr9viyrrq7hlvd9q36m")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "074i98y6d66wvn2claw79nqk7s5x7ainw1dxlziss312lmlrmydy")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0kdzsmms28583lq26v0r2vyv4486mcq109rbb94rl9fsf0qxkh7g")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.6") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "~0.3") (default-features #t) (kind 1)))) (hash "176w4m7r418yfbk67l7ra7526bafzb9chrlda8x0566r4x7d80cl")))

(define-public crate-pq-sys-0.2 (crate (name "pq-sys") (vers "0.2.7") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "07a8ifw92jbb70sqh5n51293mxjgckmx7nn6mpq43b1xrmsnvk9k")))

(define-public crate-pq-sys-0.3 (crate (name "pq-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "1m32p4g2yh96p2bfns7hj6inlp20j17s01y7sqj92chah0ddl5m4")))

(define-public crate-pq-sys-0.3 (crate (name "pq-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "1si2i4rh5lkcfm22ans38hmqjdbz5gkz6xwxs3qfbscl2076knsx")))

(define-public crate-pq-sys-0.3 (crate (name "pq-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "0rfic8z0015v2rc9njcnh1v173nlfx97v7xwxx2qm2sbwi4zagv7")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "1yabh283ncajahkq60ysgnc0n4iw077jxlay2jqmqcj67464x409") (yanked #t)))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "17ih6m4jcr9w8rg09033azcrhqwv4xfx8azzkm80s7y6x9pzj8c7")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "1v7hmlh0yak5ggkambsjqpg7jwxhzgjf7cixrgai7w664i431fdq")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.3") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "00xrcn7zlk52laccmci06c5x71db9dv9xx48yx6iymyq8bc6nf3g")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.4") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "0m4smnd7dplm469ywz7kpxrlykjxgjx4f79q59xinfprbrbmxysd")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.5") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "1w6j8a30rankd2pmcy55gdmghiv2my3vm00gsis13i2g937ac8wk") (links "pq")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.6") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "1npz9756283pjq3lcpwss8xh1rw4sx8f6dz8cxdg90h5bbp5xhka") (links "pq")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.7") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "1l8nl1b4dd2hah3lds21fi7dcgwmd2nqlaf5l9rgjm65irnmv11v") (links "pq")))

(define-public crate-pq-sys-0.4 (crate (name "pq-sys") (vers "0.4.8") (deps (list (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "1gfygvp69i5i6vxbi9qp2xaf75x09js9wy1hpl67r6fz4qj0bh1i") (links "pq")))

(define-public crate-pq-sys-0.5 (crate (name "pq-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pq-src") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "0fl7pzbn8j43s5kmjh57qgplbi3lpn7jxvp2x177hqfgsgnlm5gb") (features (quote (("default") ("bundled" "pq-src")))) (links "pq") (v 2) (features2 (quote (("buildtime_bindgen" "dep:bindgen"))))))

(define-public crate-pq-sys-0.6 (crate (name "pq-sys") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "pq-src") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcpkg") (req "^0.2.6") (default-features #t) (target "cfg(target_env = \"msvc\")") (kind 1)))) (hash "0jscim24a922vcmlr8sahsj4llaq6kw1y8gphl9agq9qhzxf6xjm") (features (quote (("default") ("bundled" "pq-src")))) (links "pq") (v 2) (features2 (quote (("buildtime_bindgen" "dep:bindgen"))))))

