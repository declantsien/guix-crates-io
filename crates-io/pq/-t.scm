(define-module (crates-io pq -t) #:use-module (crates-io))

(define-public crate-pq-tree-0.1 (crate (name "pq-tree") (vers "0.1.0") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "enum-map") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0q4dw105fa3dc6lmigx73wwngffmja78yr5gfqm330s1dz08m22q")))

