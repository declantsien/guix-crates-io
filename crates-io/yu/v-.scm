(define-module (crates-io yu v-) #:use-module (crates-io))

(define-public crate-yuv-sys-0.1 (crate (name "yuv-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "06yji71raly07liraq6nh53k0djvkwj00szyv6p5aavdchs3df4h")))

(define-public crate-yuv-sys-0.2 (crate (name "yuv-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1p5narrhisc9ms1fqw7lwyd1ahyyx86pv5328w250wg6ppmdxi54")))

(define-public crate-yuv-sys-0.2 (crate (name "yuv-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1m00dm4v7k2fspn0zcp3hyl1i8bicml61j4hl7jzmk1gn76sj1p3")))

(define-public crate-yuv-sys-0.3 (crate (name "yuv-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0q4sj1avinlygy7ngiv2qv3g74hdilq2lzlvpnfjklq87cx1y747")))

(define-public crate-yuv-sys-0.3 (crate (name "yuv-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1zk540q3zbrdl4hfi43l72b80gf01nmfmzr5lw0ig7s7jcd8m6hv")))

(define-public crate-yuv-sys-0.3 (crate (name "yuv-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "16hcx0sy0p14f0v38w43nb15g75wcg6gcxby08hhr7yc16amw6dh")))

(define-public crate-yuv-sys-0.3 (crate (name "yuv-sys") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0ppdmsgnk70yjq785w78hby5s9kn86qcs5d742mcy6w282v6bmbi")))

(define-public crate-yuv-sys-0.3 (crate (name "yuv-sys") (vers "0.3.4") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1gd1zgjky7x2zg66q96rp4k0aq633k4ddfnn9ifzwpss3602c04r")))

(define-public crate-yuv-sys-0.3 (crate (name "yuv-sys") (vers "0.3.5") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0bn0h524n92k6iq3zf21f0cgndk1bxlcj18y4bv52b6ss0yw3pwl")))

(define-public crate-yuv-sys-0.3 (crate (name "yuv-sys") (vers "0.3.6") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.8") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0l3in0wzchn21hh3kr08527p81k01lg6xvldrlsm353cfy4zb8w4")))

