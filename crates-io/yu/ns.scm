(define-module (crates-io yu ns) #:use-module (crates-io))

(define-public crate-yunser-hello-0.0.1 (crate (name "yunser-hello") (vers "0.0.1") (hash "19yfy0jdkc9asnd7q5nn1wzxk372c7npky3dn2mf233k2j8prqx9")))

(define-public crate-yunser-hello-0.0.2 (crate (name "yunser-hello") (vers "0.0.2") (hash "1j8r8vghr4694n8dczaizzxdbrb4vnjn1i55fklwgcv8b7k0bg3z")))

(define-public crate-yunsin_crate_demo-0.1 (crate (name "yunsin_crate_demo") (vers "0.1.0") (hash "0vkrygrd3nq2jjpngw014b69glhcdw7f3gih0jkixs9kfs490k5x")))

