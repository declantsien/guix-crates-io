(define-module (crates-io yu ge) #:use-module (crates-io))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.0") (hash "0m2jbkxk2zk7f78b5ncl2f0i16y8m9qvvsfipycccdz7hm0gaqx0")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.1") (hash "0v069aln63f8vhc0x7w2l416hnffwdczn7x0a7lm73bbm708h7k2")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.2") (hash "0qpq2dd81dwsja69m46d2anv6b287lkagh7cilnafn3s2l4kmrqs")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.3") (hash "0mgvk9msf65vk45jwz5y976sp9mb89d09yfl9d0rfjh39pd2n43m")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.4") (hash "1k4pjgpqb417qycncxr8nq9fyc37849dwbgxygrf9cisl7jsy90f")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.5") (hash "0pxx6v57np7afkvy5bj9w47nqliy6gjl4d4fw3m5lkjri4bzshw2")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.6") (hash "0nbdrh0a4p9shqixzp3z79p3n2hfqj13wn4b714si17jk4zm14dz")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.7") (hash "1m365cnkv0887jwxq7simqmgg946x23g1r5jsmi4zjrkb3x86060")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.8") (hash "0qvnljxw8p6671i0ig1y6fxgan3zcyjvqnky97fnqiv09654710l")))

(define-public crate-yugen-0.1 (crate (name "yugen") (vers "0.1.9") (hash "0p68jjki9c0khll0xz12jv46kcq3gm8ldyyqcl8kh2ckh8022mnb")))

