(define-module (crates-io yu ta) #:use-module (crates-io))

(define-public crate-yutani-0.0.0 (crate (name "yutani") (vers "0.0.0") (deps (list (crate-dep (name "ahash") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "syslib") (req "^0.0.0") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0p84zxldsdg2bwabslm8sbamq6dp8fsv80ny6yw9xzfzdca0hafq")))

(define-public crate-yutani-codegen-0.0.0 (crate (name "yutani-codegen") (vers "0.0.0") (deps (list (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1sqbyv3x11qk258l9vp63nnq74xsbasa4c711j3v473pcn4f5gk7")))

