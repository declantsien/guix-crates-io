(define-module (crates-io yu v2) #:use-module (crates-io))

(define-public crate-yuv2rgb-0.0.1 (crate (name "yuv2rgb") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yxprpbp5zgjps73qiwnagwxxxl5yw9xi0dgqlbph4xs3gm3mjh4")))

(define-public crate-yuv2rgb-0.0.2 (crate (name "yuv2rgb") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vf67p043db4pg85mfjif8nvgjl6acnag9arbw79gzwaw0szp6ad")))

