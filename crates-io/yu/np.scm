(define-module (crates-io yu np) #:use-module (crates-io))

(define-public crate-yunpan-0.1 (crate (name "yunpan") (vers "0.1.0") (hash "16c9r8zzpxgh9axvlaw1qhk8bhnqi07vmmmcgalbsf3c0kx4y9g9")))

(define-public crate-yunpian-0.1 (crate (name "yunpian") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v9b58kxfr8y1n0nhnx8rmp8kvg5wxcy9sx2ad3vm3vkw5kksaha")))

(define-public crate-yunpian-sdk-0.1 (crate (name "yunpian-sdk") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02ln9hpx8x3m9hn6lhc5qd2cfkws4q69zrv3i8l16igv40gqpdh7")))

(define-public crate-yunpian-sdk-0.1 (crate (name "yunpian-sdk") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z9svysq5yf7j903rm6saf9fa4r6y36igp0san4c4j1f48f222nj")))

