(define-module (crates-io yu an) #:use-module (crates-io))

(define-public crate-yuan-0.1 (crate (name "yuan") (vers "0.1.0") (hash "1mz3mxzqchq75knywycy4bq8y341f5737zd5r228511vk54qrql4")))

(define-public crate-yuan-grep-0.1 (crate (name "yuan-grep") (vers "0.1.0") (hash "0cndq9blcx3cjwwvjbgqpi9zz14vy2gcb5vgaaijl5pd8abr5hqc")))

(define-public crate-yuan-shen-0.0.0 (crate (name "yuan-shen") (vers "0.0.0") (hash "17xab90dfzla0y0lpxrkcmwmr20x30p10wcnn9k6ps9lhqmwa6xp") (features (quote (("default"))))))

(define-public crate-yuan-shen-0.0.1 (crate (name "yuan-shen") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_builder") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "ys-core") (req "0.0.*") (default-features #t) (kind 0)))) (hash "0pry1b3a6dzgvjf3n3gqchbpnmqrgfvjfypbyyk5mxpjrhi6f4pv") (features (quote (("default"))))))

