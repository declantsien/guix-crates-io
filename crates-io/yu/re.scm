(define-module (crates-io yu re) #:use-module (crates-io))

(define-public crate-yurei-0.1 (crate (name "yurei") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "14nkqsnmmfyj4db9j0vbfvinvv038l17krl1bbz7z50kaadfkz2r")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.0") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "136zmls6mffm0lalw4cvpcqdqq4qw84175605pr4xgcafzrxbv3b")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.1") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "02bvvd7zfk0r6y76ckybdcv06i09y2726nznkm0pcwhg7dm7nl5c")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.2") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1sk0saxp8m1y71sg7y392afac57ldh0q5j96yr6lllvpga86v0pz")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.21") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0ay348javy4crvd31z6hzksbabngba9m17mq72vpk7bzd3cch5rp")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.22") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0vknn7xp0w0mx05fk9vd74yga3d3vjrpcd2s8574vfzn74hpf9l6")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.23") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1ib3395x7dyaq2xlvc9dllcra6c7hv9zri0wahrdjv26f9f7jck1")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.24") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1cbwf0nhxwxnachcgnqfjypd341rrqrz3f7c6lsyqssvgg1x4iy3")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.25") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1dg1lyphqam43n448fs8fpkb7h49yfv3vs43jkldzhip00vha33p")))

(define-public crate-yurei-0.2 (crate (name "yurei") (vers "0.2.26") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bevy_rapier3d") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1qyhpy1kcx6c5rxiri9027i4hv9dng76a7rcnsrvg2if58nbcxrr")))

