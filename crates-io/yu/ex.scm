(define-module (crates-io yu ex) #:use-module (crates-io))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.0") (hash "0ql396hr0spv4xbbx3y0cvbc4n7f10mvk662kxfp98xfgxf9pz2a") (yanked #t)))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "04ka7c5riznmk8kchs3awklsrg85m0szc9axiv96skdgji0b7263") (yanked #t)))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.2") (hash "1kld921yix8lqa636zajhg02sbg86k5y1ighkb3j7pg0yc94094m") (yanked #t)))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "~1.6") (default-features #t) (kind 0)))) (hash "1k6yi348lrnzznf3i9z45ixrai7y2wlxh8fxdprqnpznf4xqa0v7") (yanked #t)))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "~1.6") (default-features #t) (kind 0)))) (hash "1pjpcrg67hpr6kh6hmapvp3lw96l3m7qihmqm7ycwmhiq3iqbi16") (yanked #t)))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "~1.6") (default-features #t) (kind 0)))) (hash "0xq011alrkq4vh9zhl525n6snqsivzx9kr4rqf4cnx886gnc0ls7")))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "~1.6") (default-features #t) (kind 0)))) (hash "107sayv48xsq377jam74dvnr0c9lgsg4bsx61ssphd9qp3dwbxp5") (rust-version "1.56")))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "~1.6") (default-features #t) (kind 0)))) (hash "1nsnk7p5h0b6hpdl6hkvwfqp2n512zc665a734dndqca2ihlwwn6") (rust-version "1.56")))

(define-public crate-yuexclusive_foo-0.1 (crate (name "yuexclusive_foo") (vers "0.1.8") (hash "19ygnihm4f57ydy0mp77927xpx28fl95g8d6bvzx8znck5vdf5mv")))

