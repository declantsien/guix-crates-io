(define-module (crates-io yu rt) #:use-module (crates-io))

(define-public crate-yurt-0.0.0 (crate (name "yurt") (vers "0.0.0") (hash "082fpg9f4kxcaay3fn68ixjiq0i9x9qq8nz396ipnrv54icnas55")))

(define-public crate-yurt-lsp-0.0.0 (crate (name "yurt-lsp") (vers "0.0.0") (hash "03rn7cx29j2asncs5izsrdqd6gjdgsasb09hs698139028saxrcw")))

(define-public crate-yurt-pm-0.0.0 (crate (name "yurt-pm") (vers "0.0.0") (hash "1nh273f1h90jcalyncxbw0wrm3xmp1q1h0pk6lm1pwjf69xb8yd0")))

(define-public crate-yurtc-0.0.0 (crate (name "yurtc") (vers "0.0.0") (hash "08r7537pr5g6wqag7ihb6535py6l5r83xk2hd7280zfya5nrs0c9")))

(define-public crate-yurtfmt-0.0.0 (crate (name "yurtfmt") (vers "0.0.0") (hash "0pfccfrnjpw3py920k7rkhdqdyx1jyj9g57fpq818q5ahxnyadz4")))

