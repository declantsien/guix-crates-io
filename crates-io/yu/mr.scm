(define-module (crates-io yu mr) #:use-module (crates-io))

(define-public crate-yumrepo-0.0.1 (crate (name "yumrepo") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (kind 0)) (crate-dep (name "syaml") (req "^0.8") (kind 0) (package "serde_yaml")) (crate-dep (name "xml") (req "^0.17.1") (features (quote ("serialize"))) (kind 0) (package "quick-xml")))) (hash "1jcac95s9id1m3i1aikm4i0xjl5xn34mgc9bfdjcqwd2nyy83s8l")))

