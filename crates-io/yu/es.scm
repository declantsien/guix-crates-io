(define-module (crates-io yu es) #:use-module (crates-io))

(define-public crate-yuescript-src-0.13 (crate (name "yuescript-src") (vers "0.13.6") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 0)))) (hash "1d4n8hbw855wh8pjv5n4v0bagpq9rlfwnl4lxhpykmsqhsihj489")))

