(define-module (crates-io yu vi) #:use-module (crates-io))

(define-public crate-yuvimg-0.1 (crate (name "yuvimg") (vers "0.1.0") (deps (list (crate-dep (name "conv") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24.4") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0x7vmgyz5jgqv2d3im43a7ym6ry3rcrvxlf8hprynxz0wpfsg7k1")))

(define-public crate-yuvimg-0.1 (crate (name "yuvimg") (vers "0.1.1") (deps (list (crate-dep (name "conv") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24.4") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "1c2f2pvxqmvan2xz6yx8zv2fdy77vpx1bcr2hqmgv32jbzdg00hm")))

