(define-module (crates-io rs tx) #:use-module (crates-io))

(define-public crate-rstx-0.0.0 (crate (name "rstx") (vers "0.0.0") (hash "0d5icvqddfj0wg025gmb7nkqk7ia6k2xnwfrw727ig395wm5i6dx")))

(define-public crate-rstx-cli-0.0.0 (crate (name "rstx-cli") (vers "0.0.0") (hash "01vf8yqwx4ax88f2pa2m8vc7zylh9yjx1n5s36k3qvjhyshwba1x")))

