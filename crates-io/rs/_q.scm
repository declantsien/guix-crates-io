(define-module (crates-io rs _q) #:use-module (crates-io))

(define-public crate-rs_quickcheck_util-0.1 (crate (name "rs_quickcheck_util") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "1g2fvggi5b558xbnlwqdsxh5hhlin1v6a0flqjcasq0vsy9a2yk7") (rust-version "1.56")))

(define-public crate-rs_quickcheck_util-0.1 (crate (name "rs_quickcheck_util") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "03d43500q6c8axfg6770zb38ja6jnq2l004hzb5qgy47r30z8liz") (rust-version "1.56")))

(define-public crate-rs_quickcheck_util-0.2 (crate (name "rs_quickcheck_util") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "15wmh7ayk96r62lyv0wlr3acv7s3akzdhdgzkaqgpm09y9b8dwhh") (rust-version "1.56")))

