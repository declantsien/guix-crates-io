(define-module (crates-io rs pw) #:use-module (crates-io))

(define-public crate-rspw-0.1 (crate (name "rspw") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19gn4jhmbdhgj2k167dyamvh02i7lhh65csfkl41s0g4y6p1nnsb")))

(define-public crate-rspw-0.1 (crate (name "rspw") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "038frzdwipdvsn3r8s4dxmxwx84ci5wjwda9kfa57i3ckcply12x")))

(define-public crate-rspw-0.2 (crate (name "rspw") (vers "0.2.0") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02lnw86nvn30kj3y3xlxix96rjfsqd0rs1rpq7d0kprnyp7llg86")))

(define-public crate-rspw-0.2 (crate (name "rspw") (vers "0.2.1") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1glmaa5cx248jzbnbn32yj60d8fzw1xx6lf59x81q84rr2sxz32l")))

(define-public crate-rspw-0.2 (crate (name "rspw") (vers "0.2.2") (deps (list (crate-dep (name "arboard") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "13vbqldp0sw520783y0rvj6fs95wvp02wj4qjrgzi603cccwaqqg")))

