(define-module (crates-io rs gg) #:use-module (crates-io))

(define-public crate-rsgg-0.1 (crate (name "rsgg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.6") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dipn2wv8ijwzxchgw2gwc120qlii8qvjr263w25fybxbndvssi3")))

