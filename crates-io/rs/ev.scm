(define-module (crates-io rs ev) #:use-module (crates-io))

(define-public crate-rseven-core-0.0.0 (crate (name "rseven-core") (vers "0.0.0") (hash "1bhq7s2fscw61r0qjccmfh4wdfvf88kvljwv6r8y4bjvlxpd30mc")))

(define-public crate-rseven-parser-0.0.0 (crate (name "rseven-parser") (vers "0.0.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1i8hx712vi9c56wy53giz6nhbiq4g23kak3ca4h8dh65phwyxl4j")))

(define-public crate-rseven-parser-0.0.1 (crate (name "rseven-parser") (vers "0.0.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1spcc8j459a6fgbqrcvjz06f2a0klz53wqlcs0az5686mvs65zaj")))

(define-public crate-rseven-parser-0.0.2 (crate (name "rseven-parser") (vers "0.0.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1b9rhnkni49kqjj9qzn8pd0jib50sjhhhg64gnhmp1nlbmnh7fd2")))

(define-public crate-rseven-parser-0.1 (crate (name "rseven-parser") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_ascii_tree") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.23") (features (quote ("std" "log"))) (kind 0)))) (hash "19nc37hdki8xgx6673vxdnf44arskzmvivxm9pnxqv1a1s92xjxa")))

(define-public crate-rsevents-0.1 (crate (name "rsevents") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1f3233vdfklma0r5hhby1c67w9rp1pmflqcfj1hx8nbg5ajgx80h")))

(define-public crate-rsevents-0.1 (crate (name "rsevents") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mcbm0mk95660n7a0jfd167bzlmypfg64l5ky09g82k9hqarxic6")))

(define-public crate-rsevents-0.2 (crate (name "rsevents") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1m7mv9fw5cl8yyb00jp76qs498sairky8wp6cy46y2xvr0wa9l4w")))

(define-public crate-rsevents-0.2 (crate (name "rsevents") (vers "0.2.1") (deps (list (crate-dep (name "parking_lot_core") (req "^0.7") (default-features #t) (kind 0)))) (hash "0cy9p3byq22hw82kqswk8m3r60jqqcd0c2895sk5ddwhvmxq809y")))

(define-public crate-rsevents-0.3 (crate (name "rsevents") (vers "0.3.0") (deps (list (crate-dep (name "parking_lot_core") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-rng") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0szlxxys3w4a90zf35pfrnpw7nbcgqdg1v8fvv9kwbvv05ifjq22") (yanked #t)))

(define-public crate-rsevents-0.3 (crate (name "rsevents") (vers "0.3.1") (deps (list (crate-dep (name "parking_lot_core") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-rng") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0lf6n05ymzs47w3pym6z2lj3ib3s6f16v1vdw1c4s6d0zmlrgqp8")))

(define-public crate-rsevents-extra-0.1 (crate (name "rsevents-extra") (vers "0.1.0") (deps (list (crate-dep (name "rsevents") (req "^0.2") (default-features #t) (kind 0)))) (hash "0llh0325m31pxw2287y3ldqhnpd900hwm9bk4vsc1qvmm1hwrklz")))

(define-public crate-rsevents-extra-0.2 (crate (name "rsevents-extra") (vers "0.2.0") (deps (list (crate-dep (name "rsevents") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1kfz3sq25i1lfvhq16g3lqm4wfak11dknplcl0ax8dnmj65cyq67")))

(define-public crate-rsevents-extra-0.2 (crate (name "rsevents-extra") (vers "0.2.1") (deps (list (crate-dep (name "rsevents") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0qv8qi1gg9h90cf23xasgf2amah46c168knb284f028kjm4cnmsx")))

(define-public crate-rsevents-extra-0.2 (crate (name "rsevents-extra") (vers "0.2.2") (deps (list (crate-dep (name "rsevents") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1r1nx447m20n7kgnbrjgxaf9sd2xzlvpm0xlwh1ki22i0y2lxxlg")))

(define-public crate-rsevmasm-0.1 (crate (name "rsevmasm") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "1cc4diqm4x4qx2ikjccvpmqxhc4ry71zz3w9y4zafw61psk0x7br")))

(define-public crate-rsevmasm-0.2 (crate (name "rsevmasm") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "1xxjalz23daqq4c0hij1iknqg64f6s0vqfd9yyyz77k0mf5n82i5")))

(define-public crate-rsevmasm-0.2 (crate (name "rsevmasm") (vers "0.2.1") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "1zvbvh5rnqk1xlwp73bbz2zmnlm11j7inqjbgs3nlhnhp48b42f2")))

(define-public crate-rsevmasm-0.2 (crate (name "rsevmasm") (vers "0.2.2") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "0p8dq611wbld6ng7q1af0c10rxnw2fhm9bgb33gy8z5h6b0ar9pl")))

(define-public crate-rsevmasm-0.3 (crate (name "rsevmasm") (vers "0.3.2") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "0jqq5dgm1n7vym08r2rf1giz2a8rsk8zvbvj4pyzadmbcampbwqw")))

(define-public crate-rsevmasm-0.4 (crate (name "rsevmasm") (vers "0.4.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "123k82q8h1wjvcjqaj1ipawzbkvxq5vknaifzvbqncxan4nxlq15")))

(define-public crate-rsevmasm-0.4 (crate (name "rsevmasm") (vers "0.4.1") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0vs9ihzw55zdv0qaq9rm3jmw6i1lqmi0fm22xp94idgklhqarw82")))

(define-public crate-rsevmasm-0.4 (crate (name "rsevmasm") (vers "0.4.2") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "1c98vfqp6ihmcb0g5yyc4hbhwrl3s4cwzy7lvz22z39r0icdcbdk")))

(define-public crate-rsevmasm-0.5 (crate (name "rsevmasm") (vers "0.5.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 2)))) (hash "1b87vr3578ngs4ikn8ky8hjsm48q60awndmd2zjnpg77bqmqg7p4")))

