(define-module (crates-io rs re) #:use-module (crates-io))

(define-public crate-rsre-0.1 (crate (name "rsre") (vers "0.1.0") (hash "1df0kw0inym5h6h99vp892mjw938pwqs6dl9y6kn8b9mr10xkr0g")))

(define-public crate-rsre-0.1 (crate (name "rsre") (vers "0.1.1") (hash "13ayxr1cb2n0dn5xcd33s8jb9lnj32qjj825vzd66lxp7hcklr8n")))

(define-public crate-rsre-0.2 (crate (name "rsre") (vers "0.2.0") (hash "05r8grkbi3wylwcy1bikv4afjr2y0ll0figz1s0i33v50a5qq45q") (features (quote (("debug"))))))

(define-public crate-rsre-0.2 (crate (name "rsre") (vers "0.2.1") (hash "1n6mc5bippld7wzmnpp00wqhjs62ycr6xvf7g8x2crprzwhq6dxp") (features (quote (("debug"))))))

(define-public crate-rsre-0.2 (crate (name "rsre") (vers "0.2.2") (hash "0rslmlyhxwkchb7xgqa0kdd2rfwn7a2ba8mv916i3qikkd6c9imp") (features (quote (("debug"))))))

(define-public crate-rsre-0.2 (crate (name "rsre") (vers "0.2.3") (hash "03krzffj6db9wcm751xzkaryixpygs7xv36q0ivnykp3jzfazpja") (features (quote (("debug"))))))

(define-public crate-rsre-0.2 (crate (name "rsre") (vers "0.2.4") (hash "0zwmdfxz8ml30wk9x3fql4dqw3400r7iggkyx7af1mr1y4npjl1y") (features (quote (("debug"))))))

(define-public crate-rsreddit-0.1 (crate (name "rsreddit") (vers "0.1.1") (deps (list (crate-dep (name "curl") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "webbrowser") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1c31s0rcj1qssvvxw5gzn2gs7q09zh0mxgn4wwc5nhi1gxanpakr")))

(define-public crate-rsreddit-0.1 (crate (name "rsreddit") (vers "0.1.2") (deps (list (crate-dep (name "curl") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "webbrowser") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1c0jdjqhm6r1c0bvw16b76cjy3758i31js7vd376kcsk9ndkqvga")))

(define-public crate-rsreddit-0.1 (crate (name "rsreddit") (vers "0.1.3") (deps (list (crate-dep (name "curl") (req "^0.4.29") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "webbrowser") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0ivjwka4jzprp566k14j7y0bbg7x2djvm67yk503rbq8ppi44ns0")))

