(define-module (crates-io rs gr) #:use-module (crates-io))

(define-public crate-rsgrad-0.1 (crate (name "rsgrad") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1qjzj0bz8p5z0krdm469ykhf0x36sf29ng2m5ypgzq20j0lrgrhg")))

(define-public crate-rsgrad-0.1 (crate (name "rsgrad") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1lgfz4jrrzgjh5z3i76yin6jrgli3lizl9xxghp30lz4fcpmr6yi")))

(define-public crate-rsgraph-0.1 (crate (name "rsgraph") (vers "0.1.0") (hash "10l1c3pvb2vhgza37f7qb89qxc6ikl9qm9dx69r7y8shzhw4s3fb")))

(define-public crate-rsgraph-0.1 (crate (name "rsgraph") (vers "0.1.1") (hash "18ai66qxkcp4kmb6p5xhrwbrxbyvd8al7bayz8spndnl3163bl5c")))

(define-public crate-rsgrep-0.1 (crate (name "rsgrep") (vers "0.1.0") (hash "00rxcwksr2a8gr7xr9nsp3293a7nm0inp2ds8nz7hy6mayc0rx05")))

(define-public crate-rsgrep-0.2 (crate (name "rsgrep") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0q466m4vy017nq1892c9fb3biia1ds53wsbbkrcd4gfwhfr8bm9l")))

(define-public crate-rsgrep-0.3 (crate (name "rsgrep") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1waqpymjm1bcdbdqwq691i74nm9y9d4r97nk1zr1zjzbyqf4l869")))

(define-public crate-rsgrep-0.4 (crate (name "rsgrep") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0gqqgaywdhqffbrg4q2hnppbis4iqiafss2bhm1nqry9j73bxip8")))

(define-public crate-rsgrep-0.5 (crate (name "rsgrep") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "content_inspector") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1nc8a5l11330ww1z8zdvx4bl1xa52xksvnkbnrfa219j8dbgr22s")))

