(define-module (crates-io rs co) #:use-module (crates-io))

(define-public crate-rscode-0.1 (crate (name "rscode") (vers "0.1.0") (deps (list (crate-dep (name "rscode_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rscode_ui") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0g3a0liaf7iax8cldxi5qpw85q41sh60ggig2pcgm1qnqb62q6n7")))

(define-public crate-rscode_core-0.1 (crate (name "rscode_core") (vers "0.1.0") (deps (list (crate-dep (name "iced") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1ihx31xhj81zpm2bdvmmincd6pq4k63c0996iigpi9h1lqqazwg5")))

(define-public crate-rscode_ui-0.1 (crate (name "rscode_ui") (vers "0.1.0") (deps (list (crate-dep (name "rscode_core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02hkdzvmbp605aafzdk84jha49846wi3w36aw81ak6j70idamy1j")))

(define-public crate-rscolorq-0.1 (crate (name "rscolorq") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("jpeg" "png"))) (optional #t) (kind 0)) (crate-dep (name "palette") (req "^0.5") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("std"))) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (optional #t) (kind 0)))) (hash "1gszpsa5qihdwn0j4m378j6q6ww8xg5dfciqhmmslmyn2ixpmxds") (features (quote (("palette_color" "palette") ("default" "app") ("app" "image" "palette_color" "structopt"))))))

(define-public crate-rscolorq-0.1 (crate (name "rscolorq") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("jpeg" "png"))) (optional #t) (kind 0)) (crate-dep (name "palette") (req "^0.5") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("std"))) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (optional #t) (kind 0)))) (hash "02naanr3v1as4r38c12k5h5raxvnlh6mlyl98hmiawf9221q26q3") (features (quote (("palette_color" "palette") ("default" "app") ("app" "image" "palette_color" "structopt"))))))

(define-public crate-rscolorq-0.1 (crate (name "rscolorq") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("jpeg" "png"))) (optional #t) (kind 0)) (crate-dep (name "palette") (req "^0.5") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("std"))) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (optional #t) (kind 0)))) (hash "12v3z96pa5l0yvjyplldazsgxyg4cdvraa44ascir8c1fhza01ng") (features (quote (("palette_color" "palette") ("default" "app") ("app" "image" "palette_color" "structopt"))))))

(define-public crate-rscolorq-0.2 (crate (name "rscolorq") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23") (features (quote ("jpeg" "png"))) (optional #t) (kind 0)) (crate-dep (name "palette") (req "^0.5") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("std"))) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (optional #t) (kind 0)))) (hash "1gz7d2f972apb175gzj7prgzk19kg6v1pji2lsfdic2c1rn5450h") (features (quote (("palette_color" "palette") ("default" "app") ("app" "image" "palette_color" "structopt"))))))

(define-public crate-rscompress-0.1 (crate (name "rscompress") (vers "0.1.0") (deps (list (crate-dep (name "rscompress-approximation") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-checksums") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-encoding") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-transformation") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0manbwlp5apmxn5n1pmrk8sf97gw21610q4vpraa31192qjyarxx")))

(define-public crate-rscompress-0.1 (crate (name "rscompress") (vers "0.1.1") (deps (list (crate-dep (name "rscompress-approximation") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-checksums") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-coding") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-transformation") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0ma76kq8k0zqmi3j8yiiwzln26qkxhkragplam015abs9qdb8c1j")))

(define-public crate-rscompress-0.1 (crate (name "rscompress") (vers "0.1.2") (deps (list (crate-dep (name "rscompress-approximation") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-checksums") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-coding") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-transformation") (req "0.2.*") (default-features #t) (kind 0)))) (hash "01kpacy2gjy0y6m62z02jgy6jghj8p623wqp2iqxp1lfljnvxd0x")))

(define-public crate-rscompress-0.1 (crate (name "rscompress") (vers "0.1.3") (deps (list (crate-dep (name "rscompress-approximation") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-checksums") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-coding") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rscompress-transformation") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0y41zdp9v7110sazngb790bcb5yf9bvf8wnppw0zasjhpcrdsvx4")))

(define-public crate-rscompress-approximation-0.1 (crate (name "rscompress-approximation") (vers "0.1.0") (hash "1sjk1wqca80ljdrayjlm0q3d8nkszry6k7incr35gz34xprm98nl")))

(define-public crate-rscompress-approximation-0.1 (crate (name "rscompress-approximation") (vers "0.1.1") (hash "0dakikgapsqsiwfrbv8nc8z6hy24nxji20dyw24z1kdpwh287k88")))

(define-public crate-rscompress-checksums-0.1 (crate (name "rscompress-checksums") (vers "0.1.0") (hash "1fwj91vwz4n14f3l73vh4bpp6bfx2gxr9sjnyiv975716nzlcfy3")))

(define-public crate-rscompress-checksums-0.1 (crate (name "rscompress-checksums") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)))) (hash "1p8a1mgj5r2jjyc1pzbbsiyjammqm2788564nyv3ndiliw9yzxnr")))

(define-public crate-rscompress-checksums-0.1 (crate (name "rscompress-checksums") (vers "0.1.2") (deps (list (crate-dep (name "crc") (req "1.8.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)))) (hash "19givg2lk2z5j7fvlrly6v34j3mxjv8l3c7m2yij51wg60c3k4sg")))

(define-public crate-rscompress-checksums-0.2 (crate (name "rscompress-checksums") (vers "0.2.0") (deps (list (crate-dep (name "crc") (req "1.8.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)))) (hash "0wzkf84b224hw0nsa7bqr8g2s5bv63ag80ccgni2ksaa09id0iq7")))

(define-public crate-rscompress-checksums-0.2 (crate (name "rscompress-checksums") (vers "0.2.1") (deps (list (crate-dep (name "crc") (req "1.8.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)))) (hash "14q5iq0szb9hq8lvadd3lh02hj639fq8m6n7j17gsnxmllxaw1xj")))

(define-public crate-rscompress-checksums-0.2 (crate (name "rscompress-checksums") (vers "0.2.2") (deps (list (crate-dep (name "crc") (req "1.8.*") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)))) (hash "0gfk5fk7z6m51kbc0vfbnxrnlqbpi3a29hd7bnsxnym788r7j4d9")))

(define-public crate-rscompress-coding-0.1 (crate (name "rscompress-coding") (vers "0.1.0") (hash "0vkqhank5jykhdd353sqd23inc5j1yc0ym32yvhbdlncmmgvsfgh")))

(define-public crate-rscompress-coding-0.1 (crate (name "rscompress-coding") (vers "0.1.1") (hash "1b53f0lar9g06lrbdamfk4ifrd9fwc23cc4mcjcwyyqzrzq8q32i")))

(define-public crate-rscompress-encoding-0.1 (crate (name "rscompress-encoding") (vers "0.1.0") (hash "0rsgcn16zz1plg6rw7jl3q7mrk93yf194h7g9534pv44kr6s9hvp") (yanked #t)))

(define-public crate-rscompress-transformation-0.1 (crate (name "rscompress-transformation") (vers "0.1.0") (hash "0qfi97c35sqcdb57zdkjxx1bjs42qidw8rk3gfla7scx00lxrw8q")))

(define-public crate-rscompress-transformation-0.1 (crate (name "rscompress-transformation") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)))) (hash "0gyyh71n6kdwbj4pjvknhd6b0rfca9cj1k8qb006rxxirlagsb2b")))

(define-public crate-rscompress-transformation-0.2 (crate (name "rscompress-transformation") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)))) (hash "13a2f8slg63s66nqipd7bp837bzd3cqg9nrkb0994vk0ml49rmrf")))

(define-public crate-rscompress-transformation-0.2 (crate (name "rscompress-transformation") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)))) (hash "0wczj87m0jdsh0m9bc2fqng916qbqsh5wz3d8678nkzrhnb90pyh")))

(define-public crate-rscompress-transformation-0.2 (crate (name "rscompress-transformation") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)) (crate-dep (name "suffix_array") (req "0.5.*") (default-features #t) (kind 0)))) (hash "1n9jc83inwn4ss2rdf09wksx9rymv1ikybsk5h1b2ws4123zv9g7")))

(define-public crate-rscompress-transformation-0.2 (crate (name "rscompress-transformation") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "0.3.*") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "0.8.*") (default-features #t) (kind 2)) (crate-dep (name "suffix_array") (req "0.5.*") (default-features #t) (kind 0)))) (hash "0mghj78q0j36m508gp9mlghp3wgzg36r77vwjzw4bvvj13qlbprh")))

(define-public crate-rscon-0.1 (crate (name "rscon") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive" "cargo" "string"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "figment") (req "^0.10") (features (quote ("toml"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5") (default-features #t) (kind 0)))) (hash "0sxrpmj8lsbvbc7300k2y0lvj6pp6q3akydxwg6p7z5gm7kdr9w9")))

(define-public crate-rsconf-0.1 (crate (name "rsconf") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 2)))) (hash "1p1a51h0x7ggjgnqa54jarf4w1k22qz93q8vzx1arr6wfrv8id6x")))

(define-public crate-rsconf-0.1 (crate (name "rsconf") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 2)))) (hash "0xnmfb9xx5cnypglayw8mdr87zbh2ln2vslhniivrwcpzzva7a9q")))

(define-public crate-rsconf-0.1 (crate (name "rsconf") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 2)))) (hash "0v8yf8li9lpfsydqfrzhqvgbj8nybr5550k3b33icm091v0575kv")))

(define-public crate-rsconf-0.2 (crate (name "rsconf") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 2)))) (hash "1k1ns755cc2kp0zrzqsv9f8yrfpknby96h1649mf1w39sfg8a8as") (yanked #t)))

(define-public crate-rsconf-0.2 (crate (name "rsconf") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 2)))) (hash "1lbjv1k2mrglb1xqw1qfscy1ljkhndpqfibgfj55yz98hx6svss7")))

(define-public crate-rsconf-0.2 (crate (name "rsconf") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 2)))) (hash "1p5w6qiskg43f5qhsh6slpc3klmhhyf76xvmzkkh215gy5czhamx")))

(define-public crate-rsconfig-0.1 (crate (name "rsconfig") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sphvc55p0h7ijvapfrn9cxalhxnwjgn7r5xndzlsdsmf1l4gnrd")))

(define-public crate-rsconfig-0.1 (crate (name "rsconfig") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1szir3qxnd1xcaggq17iifsm4rcmh4ipx65kxklkniwppcxyqymj")))

(define-public crate-rsconfig-0.1 (crate (name "rsconfig") (vers "0.1.2") (deps (list (crate-dep (name "rsconfig-macros") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qffhcawrn3141741frws6ar2nyfh1flsiidb58pqlyqr79ih3r6")))

(define-public crate-rsconfig-0.1 (crate (name "rsconfig") (vers "0.1.3") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0msd1v4r3ip8gpcapmpgy05q3c84wyg96cnmmb8hpix1iafqp8gs")))

(define-public crate-rsconfig-macros-0.1 (crate (name "rsconfig-macros") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rsconfig") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dxbysj25dl03i28w4xk01kpcprr5gwzvidw67c35r4q4cby05fn")))

(define-public crate-rsconnect-0.1 (crate (name "rsconnect") (vers "0.1.0") (hash "0m224c20zk9vdrdllagzlcc4nr4hb9wkhv89kfxga6m30wjzarhr")))

(define-public crate-rsconnect-0.2 (crate (name "rsconnect") (vers "0.2.0") (deps (list (crate-dep (name "rsconnect_macros") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "10kc5gn7yc0v4nsxb68d4h07pgy6v8fjkmay4a66104g2wjzg2qg") (v 2) (features2 (quote (("macros" "dep:rsconnect_macros"))))))

(define-public crate-rsconnect-0.2 (crate (name "rsconnect") (vers "0.2.2") (deps (list (crate-dep (name "rsconnect_macros") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "1z24xvkxszfz4ndzgnlz6zgnd7ib5w7p1hkv2l9vs4bhkclh4xv9") (v 2) (features2 (quote (("macros" "dep:rsconnect_macros"))))))

(define-public crate-rsconnect_macros-0.2 (crate (name "rsconnect_macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (features (quote ("full" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0pd1wa3j4rvway4awi17l2d5gkxh8s3d3khv5q1js6whsjvb4881")))

(define-public crate-rsconnect_macros-0.2 (crate (name "rsconnect_macros") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (features (quote ("full" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0rq382h51450pcwgmd6g9yn3wd3k3pb80b2kyf316r07zrfzm6q4")))

(define-public crate-rscontainer-0.1 (crate (name "rscontainer") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1gjgxg7bkk2awwdjwland947lf9r47q34ixr14il1j8d6xcq3y5h")))

(define-public crate-rscope-0.0.1 (crate (name "rscope") (vers "0.0.1") (hash "0y4l54fwvc0ji5ivkaawcgx17wq2x7j55lj7rzbhccqndaj0qng6")))

