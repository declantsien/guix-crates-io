(define-module (crates-io rs om) #:use-module (crates-io))

(define-public crate-rsomeip-0.1 (crate (name "rsomeip") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("net" "rt" "sync" "macros" "time"))) (default-features #t) (kind 0)))) (hash "17f0fldc89ndbi9m2v5sagnss8qj9v1pgl95kgc368pdr6pzczb3")))

