(define-module (crates-io rs t-) #:use-module (crates-io))

(define-public crate-rst-common-1 (crate (name "rst-common") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "argon2") (req "^0.5.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.77") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^2.1.0") (features (quote ("pkcs8" "pem" "rand_core"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "erased-serde") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper-util") (req "^0.1.3") (features (quote ("tokio" "server-auto" "http1"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.17.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)) (crate-dep (name "table-test") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "^0.4.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "^0.5.1") (features (quote ("trace" "timeout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.0") (features (quote ("static_secrets"))) (optional #t) (default-features #t) (kind 0)))) (hash "1a454sg8y69hss2h0bbln4pslvc9z1szvmajqg00c5kmd0wi4l9g") (features (quote (("standard") ("full" "standard" "with-tokio" "with-tracing" "with-logging" "with-errors" "with-tests" "with-http-tokio" "with-cryptography") ("default" "standard")))) (v 2) (features2 (quote (("with-tracing" "dep:tracing" "dep:tracing-subscriber") ("with-tokio" "dep:tokio") ("with-tests" "dep:mockall" "dep:table-test") ("with-logging" "dep:log" "dep:env_logger") ("with-http-tokio" "dep:tokio" "dep:axum" "dep:tower" "dep:hyper" "dep:hyper-util" "dep:tower-http") ("with-errors" "dep:thiserror" "dep:anyhow") ("with-cryptography" "dep:rand" "dep:rand_chacha" "dep:hex" "dep:chacha20poly1305" "dep:blake3" "dep:argon2" "dep:ring" "dep:ed25519-dalek" "dep:x25519-dalek")))) (rust-version "1.74")))

(define-public crate-rst-common-1 (crate (name "rst-common") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "argon2") (req "~0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "~0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "~2") (features (quote ("pkcs8" "pem" "rand_core"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "~0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "erased-serde") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper-util") (req "~0.1") (features (quote ("tokio" "server-auto" "http1"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "~0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "~0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "~0.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "table-test") (req "~0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "~1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "~0.5") (features (quote ("trace" "timeout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "~0.3") (features (quote ("env-filter"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~1") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "~2") (features (quote ("static_secrets"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mjkbb5ag5g3chxacj6xlqg9x9rzgahghdlmjg0r7whkm3r00y5s") (features (quote (("standard") ("full" "standard" "with-tokio" "with-tracing" "with-logging" "with-errors" "with-tests" "with-http-tokio" "with-cryptography") ("default" "standard")))) (v 2) (features2 (quote (("with-tracing" "dep:tracing" "dep:tracing-subscriber") ("with-tokio" "dep:tokio") ("with-tests" "dep:mockall" "dep:table-test") ("with-logging" "dep:log" "dep:env_logger") ("with-http-tokio" "dep:tokio" "dep:axum" "dep:tower" "dep:hyper" "dep:hyper-util" "dep:tower-http") ("with-errors" "dep:thiserror" "dep:anyhow") ("with-cryptography" "dep:rand" "dep:rand_chacha" "dep:hex" "dep:chacha20poly1305" "dep:blake3" "dep:argon2" "dep:ring" "dep:ed25519-dalek" "dep:x25519-dalek")))) (rust-version "1.74")))

(define-public crate-rst-common-1 (crate (name "rst-common") (vers "1.1.1") (deps (list (crate-dep (name "anyhow") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "argon2") (req "~0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "~0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "~2") (features (quote ("pkcs8" "pem" "rand_core"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "~0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "erased-serde") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper-util") (req "~0.1") (features (quote ("tokio" "server-auto" "http1"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "~0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "~0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "~0.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "table-test") (req "~0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "~1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "~0.5") (features (quote ("trace" "timeout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "~0.3") (features (quote ("env-filter"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~1") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "~2") (features (quote ("static_secrets"))) (optional #t) (default-features #t) (kind 0)))) (hash "037nh7pa2dj8xl7kjkbxjzqk535mg4a6ahvg53rxjlrjk2fkzjdx") (features (quote (("standard") ("full" "standard" "with-tokio" "with-tracing" "with-logging" "with-errors" "with-tests" "with-http-tokio" "with-cryptography") ("default" "standard")))) (v 2) (features2 (quote (("with-tracing" "dep:tracing" "dep:tracing-subscriber") ("with-tokio" "dep:tokio") ("with-tests" "dep:mockall" "dep:table-test") ("with-logging" "dep:log" "dep:env_logger") ("with-http-tokio" "dep:tokio" "dep:axum" "dep:tower" "dep:hyper" "dep:hyper-util" "dep:tower-http") ("with-errors" "dep:thiserror" "dep:anyhow") ("with-cryptography" "dep:rand" "dep:rand_chacha" "dep:hex" "dep:chacha20poly1305" "dep:blake3" "dep:argon2" "dep:ring" "dep:ed25519-dalek" "dep:x25519-dalek")))) (rust-version "1.74")))

(define-public crate-rst-common-1 (crate (name "rst-common") (vers "1.2.0") (deps (list (crate-dep (name "anyhow") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "argon2") (req "~0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "~0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "~2") (features (quote ("pkcs8" "pem" "rand_core"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "~0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "erased-serde") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper-util") (req "~0.1") (features (quote ("tokio" "server-auto" "http1"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "~0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "~0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "~0.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "table-test") (req "~0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "~1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "~0.5") (features (quote ("trace" "timeout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "~0.3") (features (quote ("env-filter"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~1") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "~2") (features (quote ("static_secrets"))) (optional #t) (default-features #t) (kind 0)))) (hash "12nlfc2r0csgq7px1qh7zv1wk0irf2mxbh20idjifh9wx53q8whq") (features (quote (("standard") ("full" "standard" "with-tokio" "with-tracing" "with-logging" "with-errors" "with-tests" "with-http-tokio" "with-cryptography") ("default" "standard")))) (v 2) (features2 (quote (("with-tracing" "dep:tracing" "dep:tracing-subscriber") ("with-tokio" "dep:tokio") ("with-tests" "dep:mockall" "dep:table-test") ("with-logging" "dep:log" "dep:env_logger") ("with-http-tokio" "dep:tokio" "dep:axum" "dep:tower" "dep:hyper" "dep:hyper-util" "dep:tower-http") ("with-errors" "dep:thiserror" "dep:anyhow") ("with-cryptography" "dep:rand" "dep:rand_chacha" "dep:hex" "dep:chacha20poly1305" "dep:blake3" "dep:argon2" "dep:ring" "dep:ed25519-dalek" "dep:x25519-dalek" "dep:sha2")))) (rust-version "1.74")))

(define-public crate-rst-common-1 (crate (name "rst-common") (vers "1.2.1") (deps (list (crate-dep (name "anyhow") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "argon2") (req "~0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "~0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "~2") (features (quote ("pkcs8" "pem" "rand_core"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "~0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "erased-serde") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper-util") (req "~0.1") (features (quote ("tokio" "server-auto" "http1"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "~0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "~0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "~0.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "table-test") (req "~0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "~1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "~0.5") (features (quote ("trace" "timeout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "~0.3") (features (quote ("env-filter"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~1") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "~2") (features (quote ("static_secrets"))) (optional #t) (default-features #t) (kind 0)))) (hash "0lfbp2pamg2b2dypmvza1gjgbbxgk3lnsi1y73r2jvj7v2llzyi6") (features (quote (("standard") ("full" "standard" "with-tokio" "with-tracing" "with-logging" "with-errors" "with-tests" "with-http-tokio" "with-cryptography") ("default" "standard")))) (v 2) (features2 (quote (("with-tracing" "dep:tracing" "dep:tracing-subscriber") ("with-tokio" "dep:tokio") ("with-tests" "dep:mockall" "dep:table-test") ("with-logging" "dep:log" "dep:env_logger") ("with-http-tokio" "dep:tokio" "dep:axum" "dep:tower" "dep:hyper" "dep:hyper-util" "dep:tower-http") ("with-errors" "dep:thiserror" "dep:anyhow") ("with-cryptography" "dep:rand" "dep:rand_chacha" "dep:hex" "dep:chacha20poly1305" "dep:blake3" "dep:argon2" "dep:ring" "dep:ed25519-dalek" "dep:x25519-dalek" "dep:sha2")))) (rust-version "1.74")))

(define-public crate-rst-common-1 (crate (name "rst-common") (vers "1.3.0") (deps (list (crate-dep (name "anyhow") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "argon2") (req "~0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "~0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chacha20poly1305") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "~2") (features (quote ("pkcs8" "pem" "rand_core"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "~0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "erased-serde") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper-util") (req "~0.1") (features (quote ("tokio" "server-auto" "http1"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "~0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "~0.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "~0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "table-test") (req "~0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "~1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "~1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "~0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "~0.5") (features (quote ("trace" "timeout"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "~0.3") (features (quote ("env-filter"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "~1") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "~2") (features (quote ("static_secrets"))) (optional #t) (default-features #t) (kind 0)))) (hash "01k7w96ic0vxj7hx1x8zzr7yn2x4llv862xdn4kwwlkhps2ykp51") (features (quote (("standard") ("full" "standard" "with-tokio" "with-tracing" "with-logging" "with-errors" "with-tests" "with-http-tokio" "with-cryptography") ("default" "standard")))) (v 2) (features2 (quote (("with-tracing" "dep:tracing" "dep:tracing-subscriber") ("with-tokio" "dep:tokio") ("with-tests" "dep:table-test") ("with-logging" "dep:log" "dep:env_logger") ("with-http-tokio" "dep:tokio" "dep:axum" "dep:tower" "dep:hyper" "dep:hyper-util" "dep:tower-http") ("with-errors" "dep:thiserror" "dep:anyhow") ("with-cryptography" "dep:rand" "dep:rand_chacha" "dep:hex" "dep:chacha20poly1305" "dep:blake3" "dep:argon2" "dep:ring" "dep:ed25519-dalek" "dep:x25519-dalek" "dep:sha2")))) (rust-version "1.74")))

(define-public crate-rst-traverse-2 (crate (name "rst-traverse") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "distance") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "sublime_fuzzy") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "zip-extract") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0xfjjdl8w438m7l1abg3z5jzzh0cpaqfi3ax9r65fjgzzb315jb8")))

(define-public crate-rst-traverse-2 (crate (name "rst-traverse") (vers "2.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "distance") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "sublime_fuzzy") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "zip-extract") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0jz9k1d428x0mi79d6h5qbmsw1jkbwzn9ky8hjwzg0hz3v2i0k9g")))

