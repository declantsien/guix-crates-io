(define-module (crates-io rs df) #:use-module (crates-io))

(define-public crate-rsdf-0.0.1 (crate (name "rsdf") (vers "0.0.1") (hash "1jmcl6cafr4yrn7p515ay90invfx42x40zdms8zz7hhlzs7zqsyf")))

(define-public crate-rsdfind-0.1 (crate (name "rsdfind") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1x5apx0pmhr6mgscgyw01f2fwrsh2zzxghzqb7gn0iqb9ln5ras6")))

(define-public crate-rsdfind-0.2 (crate (name "rsdfind") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0mdsnqqhzc0gnfyy3h2wa763hfk4jlff6lz21y10kjbn425cwvfa")))

