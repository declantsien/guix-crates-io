(define-module (crates-io rs nl) #:use-module (crates-io))

(define-public crate-rsnl-0.2 (crate (name "rsnl") (vers "0.2.0") (hash "0wcxynq3jzh710fwc7cpsy16v8s7pfdv3vj53ga89n1gl597bxwf")))

(define-public crate-rsnltk-0.1 (crate (name "rsnltk") (vers "0.1.0") (deps (list (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "15ccbi3vsyiz8waqmsgyq8chkrsx5yy9961hp6si90cwhps4ybf1")))

(define-public crate-rsnltk-0.1 (crate (name "rsnltk") (vers "0.1.1") (deps (list (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "0a9fk9syibahv7pk0q5c63y8089a83y69m0r1jpi4r5rw50xfw3k")))

(define-public crate-rsnltk-0.1 (crate (name "rsnltk") (vers "0.1.2") (deps (list (crate-dep (name "natural") (req "^0.4.0") (features (quote ("serde_support"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "whatlang") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "word2vec") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "yn") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "140nggvv86hd2x63ylja4ksrlmz3g2yrb9wpavlwch485a20sz5s")))

(define-public crate-rsnltk-0.1 (crate (name "rsnltk") (vers "0.1.3") (deps (list (crate-dep (name "natural") (req "^0.4.0") (features (quote ("serde_support"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "whatlang") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "word2vec") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "yn") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1vs8abjzky29nrn9cpx5djll5760whq680wpphx5zhl29kgqss80")))

