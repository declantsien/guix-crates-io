(define-module (crates-io rs _c) #:use-module (crates-io))

(define-public crate-rs_camellia-0.1 (crate (name "rs_camellia") (vers "0.1.0") (hash "1h6zfcfma3ndy2z96a0kr4d18vx73a2y21frkl2pnfphfiqa48x1")))

(define-public crate-rs_camellia-0.1 (crate (name "rs_camellia") (vers "0.1.1") (hash "0f56kwzdgss9k81nbjbpdhc4iyk1rdc9wsggvxw0lvaiv3im5ml9")))

(define-public crate-rs_camellia-0.1 (crate (name "rs_camellia") (vers "0.1.2") (hash "0vh7768jqknn1v72cy4was58vv4m4j64l4j2v5zwbp0cr60jk4p0")))

(define-public crate-rs_cast_128-0.1 (crate (name "rs_cast_128") (vers "0.1.0") (hash "0lx14rr7zsky8xy209r808p2vhsslfvdba6hzz05ai4fbywwfc0b")))

(define-public crate-rs_cast_128-0.1 (crate (name "rs_cast_128") (vers "0.1.1") (hash "1xg7fdpxrzzfyc4f5bwxlx5qw3qkbp29n7a11jkyqfpyryqfmyjp")))

(define-public crate-rs_cast_128-0.1 (crate (name "rs_cast_128") (vers "0.1.2") (hash "1jfl8b6gasrikbz3xbk7rrmvphsli7cpd9ma170w858pg5rjz5am")))

(define-public crate-rs_chacha20-0.1 (crate (name "rs_chacha20") (vers "0.1.0") (hash "0fq0grx0j091mmlvi6lqjngg83msfs0gsndcw5jrmz3056a1iwmz")))

(define-public crate-rs_chacha20-0.1 (crate (name "rs_chacha20") (vers "0.1.1") (hash "0qbcvimqvli1s8pbpkb001q1sdv1qgryp6m0jf7gpxcv0dq36q3h")))

(define-public crate-rs_chacha20-0.1 (crate (name "rs_chacha20") (vers "0.1.2") (hash "1dc2hg6qgd91rz45jqjni6idb0rss464bh04fs4jdqbx9fz9035q")))

(define-public crate-rs_concaveman-1 (crate (name "rs_concaveman") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "1.0.*") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "robust-predicates") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0z26p1ym95kj413pqi0sjskp9wm0k6ahz9vkspqicnyqqs2pv2n0")))

(define-public crate-rs_concaveman-1 (crate (name "rs_concaveman") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "1.0.*") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "robust-predicates") (req "0.1.*") (default-features #t) (kind 0)))) (hash "13cmhqnx8b2xx5mda5hric2ybl6fqyxd5pk74fhyr2qiyw9x7fs3")))

(define-public crate-rs_concaveman-1 (crate (name "rs_concaveman") (vers "1.0.2") (deps (list (crate-dep (name "cc") (req "1.0.*") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "robust-predicates") (req "0.1.*") (default-features #t) (kind 0)))) (hash "01vi5ba4zv741csb4ppx881k2g17a2bn8lbw2qc2g4xjl7x157zm")))

(define-public crate-rs_concaveman-1 (crate (name "rs_concaveman") (vers "1.0.3") (deps (list (crate-dep (name "cc") (req "1.0.*") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "robust-predicates") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0bj1z6dy0qdn4clmq8ifyw80csgwliz8a1aa6qdx1cihq6gp7q5f")))

(define-public crate-rs_concaveman-1 (crate (name "rs_concaveman") (vers "1.0.4") (deps (list (crate-dep (name "cc") (req "1.0.*") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "robust-predicates") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1sblymblrdn4fiz26lbk31snkl8f7m3wvfywwkzl864yq9nrqmkh")))

