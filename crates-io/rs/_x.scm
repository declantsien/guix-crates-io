(define-module (crates-io rs _x) #:use-module (crates-io))

(define-public crate-rs_x25519-0.1 (crate (name "rs_x25519") (vers "0.1.0") (hash "083ydxnlg3llpj0fmqig16v60dnpc6dvc9b2mlq8g9al3gnqm7g4")))

(define-public crate-rs_x25519-0.1 (crate (name "rs_x25519") (vers "0.1.1") (hash "13iyihflfqz6jah0d0qd87g71znrlnch7cfqsipbz06x6r3y2h2m")))

(define-public crate-rs_x25519-0.1 (crate (name "rs_x25519") (vers "0.1.2") (hash "0b7330fj5byl8kkmpia5rz45r0pd3yw1c38falk2bzvg6hlx9kxl")))

(define-public crate-rs_x448-0.1 (crate (name "rs_x448") (vers "0.1.0") (hash "1r2wipana50k1zp8ws4nfjqj6az034r63d7qsw82bnyyqzb1sdpk")))

(define-public crate-rs_x448-0.1 (crate (name "rs_x448") (vers "0.1.1") (hash "16b8mimagymvhhqh8zx1cbpqxcj6ckif1xnq1n2w13141lrpy3l9")))

(define-public crate-rs_x448-0.1 (crate (name "rs_x448") (vers "0.1.2") (hash "1f4yj7zfx1a7qjvscm40sgpxn5spcrxj1c2494fc7n0x96vbmwgv")))

