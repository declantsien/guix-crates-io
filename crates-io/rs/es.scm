(define-module (crates-io rs es) #:use-module (crates-io))

(define-public crate-rsess-0.1 (crate (name "rsess") (vers "0.1.0") (deps (list (crate-dep (name "rug") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "0w3bcnk247qd204128m257ms5ay8wa2az7gvh4a4nz3jps8gzkpb")))

