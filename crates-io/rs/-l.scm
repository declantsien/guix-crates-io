(define-module (crates-io rs -l) #:use-module (crates-io))

(define-public crate-rs-lambda-0.1 (crate (name "rs-lambda") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1kjqbprcvn933128r8h968882p2qslfxj4r6yk1x9gisrv6vydf2")))

(define-public crate-rs-lambda-1 (crate (name "rs-lambda") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "reglex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0f2v342cl5xkl34nxgz28h3jqqqrndxb853njsqlmk3b8wknl5yx")))

(define-public crate-rs-lambda-1 (crate (name "rs-lambda") (vers "1.0.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "reglex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0kg7mpbn7g0is2mfhdwkr0wz2z1ivb6ncmw3cdlp1p7ab59lza3n")))

(define-public crate-rs-lean-0.1 (crate (name "rs-lean") (vers "0.1.0") (deps (list (crate-dep (name "ferris-says") (req "^0.2") (default-features #t) (kind 0)))) (hash "19nrmmlg77957q670r5shhzlixx7rn8359166ggki8k3lfwmd4zl")))

(define-public crate-rs-leveldb-0.1 (crate (name "rs-leveldb") (vers "0.1.0") (deps (list (crate-dep (name "leveldb-sys") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "0c2rkm8ch2440nyyzylxsyxpmn5cxx96v3pp5v4rhs7mcf5afjbg") (features (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1 (crate (name "rs-leveldb") (vers "0.1.1") (deps (list (crate-dep (name "leveldb-sys") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "1m5vfbsngkm0pn5r7lrvsvxpjg3w8gsk0zircdnz3sybfp5s1gif") (features (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1 (crate (name "rs-leveldb") (vers "0.1.2") (deps (list (crate-dep (name "leveldb-sys") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "1clg1gvqbxj3c8s92j6axs4dq2iyqx2qm4jqlqf21qlh2vqqbpwh") (features (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1 (crate (name "rs-leveldb") (vers "0.1.3") (deps (list (crate-dep (name "leveldb-sys") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "1gaapnd6y3mg61pdrxw8cfa4a623bidmrcb07n0z1rwq577m2y55") (features (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1 (crate (name "rs-leveldb") (vers "0.1.4") (deps (list (crate-dep (name "leveldb-sys") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "104yjp1h6hsddnlflwsxm9ypgzw6f4bvhfxb9qsi4zrrbkmdz7qj") (features (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-leveldb-0.1 (crate (name "rs-leveldb") (vers "0.1.5") (deps (list (crate-dep (name "leveldb-sys") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "0bc16fpdmdsznrpxvrbm633w97nvq8wk8b133vfw9ybq5rwdanyl") (features (quote (("default" "leveldb-sys/snappy"))))))

(define-public crate-rs-libc-0.1 (crate (name "rs-libc") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1abcn92ak88v99la74s8lqbimg2qpjciq9qaisvljsk9qkb739l0")))

(define-public crate-rs-libc-0.2 (crate (name "rs-libc") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1yflhcc3a6in3pbhv43s58labgh61w46zpy8r9rp9yjm65hb5v3i")))

(define-public crate-rs-libc-0.2 (crate (name "rs-libc") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.46") (default-features #t) (kind 1)))) (hash "0zi4gnfbi3zfadfizqq6gfvrqs9mscgxfp0ci04j0riywkd6c2xy")))

(define-public crate-rs-libc-0.2 (crate (name "rs-libc") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.46") (default-features #t) (kind 1)))) (hash "1w3pzgi4f173vmhk0xrinj7zzif7j4vkmkph7b1j9fblzwx7cd5l")))

(define-public crate-rs-libc-0.2 (crate (name "rs-libc") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0.46") (default-features #t) (kind 1)))) (hash "0q1j8gfianzxwsg346h0qb9zwm7d44ra6z6ia3cp3x8wj9drhk4i")))

(define-public crate-rs-libc-0.2 (crate (name "rs-libc") (vers "0.2.4") (deps (list (crate-dep (name "cc") (req "^1.0.46") (default-features #t) (kind 1)) (crate-dep (name "zeroize") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1jkln4xk1170w6522kxcrim39mbmjp3c7fl7495zlvxcia78qgk8")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "1b7gmnn8d9axkzddaba0rnmabi29icglgjd30n4czc5vimzkzfbz") (yanked #t)))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "1b2s1qpwv8l6w750hfisv8q1mrsy8mpy6cqsvspis632w3d36fv2") (yanked #t)))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "1j3ykza7vmxy7rfhgjnvd3y5pq29m0d8avg3q9vqsi164hwz7d5y")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.9") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "18fn5cgagvm8pnxmc02cjxacf103z29n8w1dnnbizylhd4qqy0gj")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.10") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "0mi9fwn0jj9sxmyw2wk6mmh8f6iynhj634zxz9dlv774j1s1vs36")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.11") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "00xzvqka6z01wb2fbml42z9rydgj3ha8wqhcmav14svd91y3n2fq")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.12") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)))) (hash "18s3jrglsfj00ky2dfmfa6rhjlzmvaaghrk0qw5idnkq5hrgz27j")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.13") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1a356fbhrgg7c2rkwq897scsw351w9i733ml35578vwwvcx0vr8i")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.14") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "168iab138y14yzbg5q0qxkcqm89i1hkxwxak5pihq5wwp3kkwxxr")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.15") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1kvfvqmsd6r0rzk6769b55j59bsj419hc5y55v1sfpayg6jb33fw")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.16") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "111pfs4xrj492l3sgwbby9g22frvvs3p64azgr800bg3mm8k9blv")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.17") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0vgfkpycblzmxdlmrgdsq8nzjg1psdvnsmj3r9vhvmqd0y250mhy")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.18") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "018swdq2f4g6w72anqw0shj1ryzhl5lbxqk571xy02mh0cq6ldn7")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.19") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1mzyba665hf0v2xhxzcxi40ba9hl65862zs6n7b8f3r1wacnamv4")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.20") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1br28p6fajx2yk9k44brgrljqzcvyc6kmqw0a4chsgl8wkn6036m")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.21") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0dg7qnzfbd1f3fabm2h3l0zvmjdxdqfrd4m99pjls72vg5grvc2g")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.22") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1zfvqvxk2sfsf2c4bsr1a02w01b7kww7narc3prz5wkr9vs4wkkf")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.23") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1hc9y868dl2vd4zdbn86rqz8d7rbg3k8njin0c5l3qkflp6blgvs")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.24") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1qx53g33x3511mrs478fm975s32q5ar2d1dmc0x242x8ng578qdl")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.25") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0jjz3bvbhh349xdiksmkmvg2z0ikly0b94fxirq4rzlnwr1s90yf")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.26") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1h85p9bqpa3vijgglfn5qx28cifx7gdf6ay81fphqpqx6cd49ipj")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.27") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0d6blm1lrmar0b2dr41r3cwp7p74b3934kycrms6c28a6f72md4g")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.28") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0hgz7r61lgmzkcqb6xi6r2701i87jngnm52v0j7ic65zi2y1py6f")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.29") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1ciyka1kc8r3hjn2hg4ypw31ywwq18p15av22hx16852n91kzpp9")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.30") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0gq5mxcc7zzvhm9d5yby7r3cllqf41nxnsngf3xgpbncghxlwldj")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.31") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "08q6kq515hsq57vn3kx40djdwj8mai7lnvhb3rg5afxp71rbpg0q")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.32") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1x0nqzv74x3z5knw9jdyplkbhl6dvbj40n8ckdcn69xg37pq8qq1")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.33") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1c6dxxjf0k1phj3k07yca3kj2bm6l0h87lhhgqjyjwgqj4mh34ia")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.34") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "05g731rh26sy8b02jcah1iw5i02w1par758mm0k9544zam04nzk9")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.35") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1qiz4h5xg8mavawz6l9jc6v9crf16qlb13f9zbh1mjskyin1yacp")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.36") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "07wa6whyprrid768cmagk0bbdfihd8plf7prkvwqvdxk78i9nqyc")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.37") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1fzk5bznliy67z04h35zbbhrjy8prfg8g70d0wmpsnja435r4aa0")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.38") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0n5bz59zcs3hl43j6cwakpqrkirmlbs2v6r15h3nm70x7qwmgdwn")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.39") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1mvvna8vdqiamgv5r2xayc380489kwl4ag271jcy0l0vyyrkh573")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.40") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0zmmmrvynza4cad6kjx2hsmjlbizgp9vgrpbna6j7iw6144ikllx")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.41") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1qy1b3k53yi7kz23bw658rvgfxa5j82cyrbwb5a292qzq4nwzsdl")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.42") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1z3gayqid725qbk9d1mvhqsxfi93cvckkl6q41mqhnpmf290adl9")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.43") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1mflan9a6vab99v5f0i16l7k9ciang8adjbs5n0cxzmjqkbrsxi6")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.44") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1g0fv3abphrq8wyv0z0jjcnviwzx4srhjyvs36j76g2qiidyfa8a")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.45") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "16c65fvd5ffxxx3j715g6w2wshmmqi3nbcbml3am20fdj7gfqgc2")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.46") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "11wp4z4k7x8rjlgj4vgfcz31y1yn2ccknq3piknhn5fhacizqjxq")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.47") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "074rbpv86qkv6ydvn5lwmd3izvv7yqanybxnimzl73594fsr7b0l")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.48") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0pv078ivda0ddir3xcw95cilr9qpkliqqap8miww5pl9akgg6kg3")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.49") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0q6wd9arkn2gcrxg4vwv344pxjfff3cnb4b7a4mfhm5y6qp5k4k4")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.50") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0fpcsc5vskv53386xxijla814c8kc256aj4mfsnav79wvcviy10r")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.51") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0g07lh4k3r08spsmllzbnx2fiy2f0z70r8c5zp189smqhcyly1sk")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.52") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0mcihzqlrf5pvnciz0v1qhgx9k976fd6wvlvigmmvk77103z6xq1")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.53") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0xlvbg8fl8mfzi9jaxqrp24liz84k6fdadf883mmqj8rdklky2f7")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.54") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1wr6jfkgvh1izhi97rjrn6lzh4jgm76b9216pzdjdaxdddd37w4i")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.55") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "19qfjn2332qc7qdqfpyjc9lcnr1vjqn2gmz7zq2s2jkkxfk2kbnh")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.56") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "007gy502fwn5pvaxxx79vch71wsjggrv9cm3672wx86s66zjf9cl")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.57") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1rdws6a0jiw4f4jl8blf4dzmvbb9brphsvbvvvrjfpdh9h8i7f2l")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.58") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0rfdknizkgn440zjdnv0h2fhnvkfkzz21zvwwyg993ddi6c1f2mj")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.59") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "04hbxcv228ajdjzd0i0rfq62nqkyl0pw895drij2ix0zzl7g7zwj")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.60") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1gvzdim0x9v840kf3ihk26zlb2kni2pgyk96kggjwi87rl3c0kbp")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.61") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0szg8z6zfwxsh5ilgg325qy6zzgh9jyixwg3cxy0s90ai7pqklw8")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.62") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1xd4ajqmlca7lpkwmygyjnkir5lmqb0i44a86bgj6624pxwv9mfs")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.63") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0ka7pcwnflbssfjdg1gld0jfba0wz3366sj5dbaqv1g9xlarzcz2")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.64") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1fl20z7bjkp3qrq76j764jjkjycd0irji9y68382a25jib3p4ppw")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.65") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "097rj9v2qb6z7bgxrpxp5kndj16ww7gg6q6bn0r95vkjxagdsad3")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.66") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1zybwf342a640m0zbij8fs5p2yyrgrza2zm4x6vqiffvd19h813y")))

(define-public crate-rs-llama-cpp-0.1 (crate (name "rs-llama-cpp") (vers "0.1.67") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0wvsxx4fv3i8bmh5fmfnjm08z3fprgzqhw3rji5cwvwbg61zsfpv")))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0hwqqr3vvdvxdpvykj7kvfj5kwyxybcn4p327hnw46xihxi677y0")))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "1g2fp9xxp4fym24n13f7mplyqi1z7xqlx2jfqwfwkssrsx4f39wr") (yanked #t)))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "1flk01xwr35j42yhvrq0hyi17aghi47ii4szq21hnjllkz8v4x7k") (yanked #t)))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "1rqsm7gni7vqqd9nn04hbiflg5ksc3bfnz6i8xbszfsml8mj5p1s") (yanked #t)))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0k431m98lr8vc7wrgnvpbx5lri51albh0fcnz0yayqi46x33cz3m")))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "18s0gsz227ccq01nqlz3ac7y10h2m6myxqx0ggh43xbsg90sm9pn")))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0ks2ivrv31rm9hdvv5ri8kcqkiiyazmc8y2j9canfhxkw9qmf83z")))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0w2zgsyf0mrnjjm77hraj974ax3zs977x5glwrc5dd4ak744c55y")))

(define-public crate-rs-logger-0.1 (crate (name "rs-logger") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0mkb2938nj5kj83lwr1wvbkwakkl81s1ikby0hk9wr4rdd271hi3") (yanked #t)))

(define-public crate-rs-logger-0.2 (crate (name "rs-logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "1vwhw8207n1a92kzc2wqpr5f9dpz3m4pk0m4vbyrqb5q4ji2yrqs")))

(define-public crate-rs-logger-0.3 (crate (name "rs-logger") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "0gx84443gvx7qwls9rd8x24903w9xns0vr3hqkb13ds2fmw2a9rj")))

(define-public crate-rs-logger-0.3 (crate (name "rs-logger") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "1agvhhris6chviv856xjq4sl7zn585pdvm2iw19bv4fmhfl0d80f")))

(define-public crate-rs-logger-0.3 (crate (name "rs-logger") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "13knc104yc0bn7fm8x78vs3s974r56icy6a09zv55ycsni2118s4")))

(define-public crate-rs-ls-0.1 (crate (name "rs-ls") (vers "0.1.0") (hash "0lb821lj5z8gdm30j5rkxanvcnd5nb2sw7rgxzw1h1v58s75y2jb")))

