(define-module (crates-io rs fk) #:use-module (crates-io))

(define-public crate-rsfk-0.1 (crate (name "rsfk") (vers "0.1.0") (deps (list (crate-dep (name "input-macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rsfk-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04h6lwlzbgm7c47ihhhp1ld4cd0la20wazvq8bgf267b569b9qqf")))

(define-public crate-rsfk-core-0.1 (crate (name "rsfk-core") (vers "0.1.0") (deps (list (crate-dep (name "input-macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "104m5ciamfan2fxbcn1w0fi2cz6f6qnq3plv0i9b6dh737i2p7i4")))

