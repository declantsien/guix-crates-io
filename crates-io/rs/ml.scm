(define-module (crates-io rs ml) #:use-module (crates-io))

(define-public crate-rsml-0.1 (crate (name "rsml") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1c6cmc4lxlhhmf3m5zbm0qjd1xv0v8k2151n9k6833fc1qcx2gjq")))

