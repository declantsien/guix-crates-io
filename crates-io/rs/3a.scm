(define-module (crates-io rs #{3a}#) #:use-module (crates-io))

(define-public crate-rs3a-0.1 (crate (name "rs3a") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0pa1p2jwjw8cd705xgldksz79crc88pn62mxilf8w0k7xds435h3")))

(define-public crate-rs3a-0.2 (crate (name "rs3a") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0s2nxpj40f44aav1qddgd9qz4mjyj56r0sphkyzh5mq7baknm3ch")))

(define-public crate-rs3a-0.2 (crate (name "rs3a") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1l83z4kdx8gvj75fsn75v5741yg6m68836h0117jd068kj7rg39g")))

(define-public crate-rs3a-0.2 (crate (name "rs3a") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "08ia46g46gg70bz60bhz7azh25iq506qz1zd0nmh4lkkhbribvg3")))

(define-public crate-rs3a-0.2 (crate (name "rs3a") (vers "0.2.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "19lnk3ra4qkrgshdq7h0dja24fd7h191k02p8ij1f6dhj80msvzc")))

(define-public crate-rs3a-1 (crate (name "rs3a") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0n2l278lfsfzindrx7wg604s5yab4wjb4pxn2k4vaqz20jxi05s5")))

(define-public crate-rs3a-1 (crate (name "rs3a") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0n1zsyal4nb74agdn0sakg1q9w5ydv13z7mn2ck079ngqbcbfiac")))

(define-public crate-rs3a-1 (crate (name "rs3a") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0mzlrzd7kn2s1744k1swfjzmrpw672rk4jvz4cnshaawk87i6xf0")))

(define-public crate-rs3a-1 (crate (name "rs3a") (vers "1.0.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1k9fmhb9y0zagr35sypwsw60rbj5g3qf5k2k4bp16ymy3ng7mcir")))

(define-public crate-rs3a-1 (crate (name "rs3a") (vers "1.0.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vh6csjwiy4rqr4iy97n548bmj1rbzpzkw03r2mws2rrj4j5shy3")))

(define-public crate-rs3a-1 (crate (name "rs3a") (vers "1.0.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0j5br1xdhzz1aj6hybvrcngygjv1sdz8z2zp2wibcbn0xa47xxf4")))

