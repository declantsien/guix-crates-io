(define-module (crates-io rs st) #:use-module (crates-io))

(define-public crate-rsst-0.1 (crate (name "rsst") (vers "0.1.0") (deps (list (crate-dep (name "html5ever") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^1.7") (features (quote ("from_url"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)))) (hash "0b00wmcphdj49pmca1iy5wljz3qa95qw90rx3fgwspi77cn73bqw")))

(define-public crate-rsstarter-0.1 (crate (name "rsstarter") (vers "0.1.0") (hash "0liywwqdnq2s9sx1xckn7vygip5243fm89xq5l74aw29fh346r8h")))

