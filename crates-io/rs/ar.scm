(define-module (crates-io rs ar) #:use-module (crates-io))

(define-public crate-rsar-0.1 (crate (name "rsar") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x7jlhrhf1bbhpgn2lc0d4ksh6faflddsvvkmqvn72d9qj2bz7mm")))

(define-public crate-rsar-0.1 (crate (name "rsar") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11zbvm99d8b477yaw2jnkq4ny0199ld4319xj99dycxz17w6yfjc")))

(define-public crate-rsar-0.2 (crate (name "rsar") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "19lihyymn2wjbsjc45lyp9zgsr98c092g94rc9yka61j29c9sf35")))

