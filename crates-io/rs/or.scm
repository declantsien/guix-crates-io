(define-module (crates-io rs or) #:use-module (crates-io))

(define-public crate-rsor-0.1 (crate (name "rsor") (vers "0.1.0") (hash "1s2l8j9qc7cxvicdb1lzq4mw8fzs615ax48jkxkcrb2q3gka6b1p")))

(define-public crate-rsor-0.1 (crate (name "rsor") (vers "0.1.1") (hash "1zrvi3j539j16p3q65sziq1dxhhf31l6vv2xndmmjwb7p4rkq0if")))

(define-public crate-rsor-0.1 (crate (name "rsor") (vers "0.1.2") (hash "17hmhjsm4rvf4rmhl34w423ihnbzi4r63a8yyw26lxs3rj7d12by")))

(define-public crate-rsor-0.1 (crate (name "rsor") (vers "0.1.3") (hash "1nndfyn6cb8iwks5qbc320m40mapczvyilk6cd0kh6n39nbf6msb")))

(define-public crate-rsor-0.1 (crate (name "rsor") (vers "0.1.4") (hash "0abnirppfrqaxccdw4kpg2mnv4bvdiyhxzxzmk8f4rk3g403slc5") (rust-version "1.31")))

(define-public crate-rsor-0.1 (crate (name "rsor") (vers "0.1.5") (hash "065zcgcn0d1dz4p6dmqxxlamly34rsdkjiby6bc4cddp0gyjc42h") (rust-version "1.31")))

(define-public crate-rsort-2 (crate (name "rsort") (vers "2.1.0") (hash "18w9jfg375pis84885lkmwgqa2zh0ch6f2xsgs79qhip31gicr0p")))

