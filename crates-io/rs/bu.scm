(define-module (crates-io rs bu) #:use-module (crates-io))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.0") (hash "1hh42h16nck2as0pmdc7frjac7h9hbc6n421fvdm59giir8pw9s6") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "compose_yml") (req "^0.0.59") (default-features #t) (kind 0)) (crate-dep (name "debug_print") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "eval") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "foreach") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde_any") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1jdf4k5ga9bhjz31958cqg7z9b4vgxdw4p1ad1igyb1674jm329b") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0dfk90x0lbyily6nn4lmyq249yz4xxhk5vivhdxxk8rlpiih4jy6") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1ydr64bp8npfchbcrz88kpmmdiy1vpgzjgawkpajv2417icb8fn9") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "196gll33k80kxzklqfbgvwclv1k2vcw490p4vwr2hczil377h9rf") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "160729gbidgab5vx44mgsp0kwksawydwc2harh0h9jn5hqhcck3v") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0aqz6hgf4296r87d5sa799jxzirad70ahsa289p90xghd264yhms") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.7") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1rgnra2miw6f3169q6swh3p3jp1c1nxcz0i92zda04p7bid69y0m") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.8") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0xhpj9nja8q2z5633x0a4kr3nlpgb7vrqyhb57wamsjf25mvry49") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.9") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0g6rpwvhh8as5bv6p1ldqfzffwq035dpgf5ppnh247h960z4zzmp") (yanked #t)))

(define-public crate-rsbuild-0.1 (crate (name "rsbuild") (vers "0.1.10") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "command-macros") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "02inyxn4qklpz5b7j7i06c75zgx8d5bpn82ysmhkxaww9apqww4p")))

