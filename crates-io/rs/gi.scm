(define-module (crates-io rs gi) #:use-module (crates-io))

(define-public crate-rsgit-0.1 (crate (name "rsgit") (vers "0.1.0") (hash "15z7djbivh6andd0p9ypry3izbqrn6mqfnd946pr0wh405w1qxqp")))

(define-public crate-rsgit-0.1 (crate (name "rsgit") (vers "0.1.1") (hash "1i5ixh754ry9m2rsh4pw5kx6qbpkh27rad4ss920qfj698sygk47")))

(define-public crate-rsgit-0.1 (crate (name "rsgit") (vers "0.1.2") (hash "03ffhj1z8v9lzid8wipm2vms4n24y6zwpzbxdmzb5ss2a8nsqldh")))

(define-public crate-rsgit-0.1 (crate (name "rsgit") (vers "0.1.3") (hash "0c1rj9zlyf5bjby68028wk13xblqjfim9iib0hrdw7lqw4ynsa7c") (yanked #t)))

(define-public crate-rsgit-0.1 (crate (name "rsgit") (vers "0.1.4") (hash "19f908p8icwkv82938glh451wx7zdjzhbkyfr3lyc1vsbpw8z6zl")))

