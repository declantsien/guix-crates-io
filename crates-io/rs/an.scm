(define-module (crates-io rs an) #:use-module (crates-io))

(define-public crate-rsanim-0.1 (crate (name "rsanim") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0nah9pw4spq0yg0qk8xijc1c9i154ad1a2585ylk01g942csgcxk")))

(define-public crate-rsanim-0.2 (crate (name "rsanim") (vers "0.2.0") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0xbhlv9ninldms5nwnz7lg4m8b6xz5mxi48z1qzxygf4lp96ckp3")))

(define-public crate-rsanim-0.2 (crate (name "rsanim") (vers "0.2.1") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1hlgsqkylhcybw0n5i1yr999wbn195jyqkxqk2g988s8y2c1c8nr")))

(define-public crate-rsanim-0.3 (crate (name "rsanim") (vers "0.3.0") (deps (list (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1wjcwa1c3y1fr2zcp9qnjzab7lifpinxk1ph9mpqrk5ghdcc9l90")))

