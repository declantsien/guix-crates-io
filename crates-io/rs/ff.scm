(define-module (crates-io rs ff) #:use-module (crates-io))

(define-public crate-rsff-1 (crate (name "rsff") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "1cc84h0hg5bbah3f60rgm1g36i3f0g0cmvzmyp2k824l9b2w4as8")))

(define-public crate-rsffish-0.1 (crate (name "rsffish") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "11mvhcvkjiidp60hlc2ah27ksdmkm0pi7mkf5wihlmmvj948nl7b") (yanked #t)))

(define-public crate-rsffish-0.1 (crate (name "rsffish") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "104yk73zpjbv9fg1wn1i4l18qi97d608ppzpdmf4ag2lpiblzawa") (yanked #t)))

(define-public crate-rsffish-0.1 (crate (name "rsffish") (vers "0.1.2") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0d0x6vycgy49h7pmcgblcrzzp330ckgppcr4b19331ky7nkn3nzh") (yanked #t)))

(define-public crate-rsffish-0.0.6 (crate (name "rsffish") (vers "0.0.6") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "02fkx1y3wqp9zhr0lb881d3236lr1sjmn2y7chv4bkafn7ph5k74")))

(define-public crate-rsffish-0.0.7 (crate (name "rsffish") (vers "0.0.7") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0n44f9lznr81lk7jsv1rj9346vq33v5czisypi6ix9qch1156nrq")))

(define-public crate-rsffish-0.0.8 (crate (name "rsffish") (vers "0.0.8") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0xh76iaql2imfxz1a6z0g0dzyhrnkbzqhbac1iknxdswqmjfj8jl")))

(define-public crate-rsffish-0.0.9 (crate (name "rsffish") (vers "0.0.9") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1dazmx381hirairg9nfa0dcqs9wkbnfvq7rm5k1s0dd83h7bj96m")))

(define-public crate-rsffish-0.0.10 (crate (name "rsffish") (vers "0.0.10") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "03jvl0a6n9k2dsjx5ylwy9cqfs097gp96cchw6hj1avxdyl1m882")))

(define-public crate-rsffish-0.0.11 (crate (name "rsffish") (vers "0.0.11") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0njjqjm5hxj4shjkzhxi9pqkhkvchhjgnsrmr8m8r59bcn121k9s")))

(define-public crate-rsffish-0.0.12 (crate (name "rsffish") (vers "0.0.12") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0zvphy6118rx34r32wh95jpl9kj95vp7i0c24hrfk4mchjzj88b0")))

(define-public crate-rsffish-0.0.13 (crate (name "rsffish") (vers "0.0.13") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "08dvzgjiygivkn51zxjyhh5w6qs41lvzkj8fzs4ddq4axdkx2hmc")))

(define-public crate-rsffish-0.0.14 (crate (name "rsffish") (vers "0.0.14") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0v90n595yf6f4spjbpws079qmb9rh0clzrbr60ps9j5f11w61s5g")))

(define-public crate-rsffish-0.0.15 (crate (name "rsffish") (vers "0.0.15") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "06978jg0pmy98ppdzydgl32w9y4la8m10kmkasgczxm0i7265mcr")))

(define-public crate-rsffish-0.0.18 (crate (name "rsffish") (vers "0.0.18") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "0z1gfrf9lbx9y9z3yqs9aplykjv288h75p257yj2z4i4d8l02cac")))

(define-public crate-rsffish-0.0.18 (crate (name "rsffish") (vers "0.0.18-amazonsfix1") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)))) (hash "1d2gmlxh4l368799wly08zh16awb6hf093jwfca0080lyffp3m1w")))

