(define-module (crates-io rs ak) #:use-module (crates-io))

(define-public crate-rsakeygen-0.1 (crate (name "rsakeygen") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rsa_keygen") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1qf645794xxwl963svqm19fyfb14zjd903a2x1sk78blnnqdc6ic")))

(define-public crate-rsakeygen-0.1 (crate (name "rsakeygen") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rsa") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "rsa_keygen") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "01jfjdqsd6gbsprian6a97n25x26hr1666r7g77qxg873d90r38i")))

(define-public crate-rsakeygen-0.1 (crate (name "rsakeygen") (vers "0.1.2") (deps (list (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rsa") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "rsa_keygen") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1qswwqyq0m388bw6vjcyb1l9achxbyjfpq0n09r6k4j0g5irhzg8")))

