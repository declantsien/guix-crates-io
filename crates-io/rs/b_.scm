(define-module (crates-io rs b_) #:use-module (crates-io))

(define-public crate-rsb_derive-0.1 (crate (name "rsb_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03w3cq9k0djklslzqpx84xq2yv12wq5h8318d9ghnx0wmqy10shc")))

(define-public crate-rsb_derive-0.1 (crate (name "rsb_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ckpxk0dw4c7jkimldpa0nkhwilqvgvf21p7bf8889z1hbfvlpa9")))

(define-public crate-rsb_derive-0.1 (crate (name "rsb_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "159ljrcs3pkzycd7ndnm12di41b3c748knp9v2hxpb4k0x5cl5zp")))

(define-public crate-rsb_derive-0.1 (crate (name "rsb_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "119m3kw5vr1hsc93a3v62gz763xmmnir4bml2z0sxgyc2pgl7k70")))

(define-public crate-rsb_derive-0.1 (crate (name "rsb_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zzpcm99yh9qvmqhc62dj29a8nfam5nlw6cay23bic273h073k3j")))

(define-public crate-rsb_derive-0.2 (crate (name "rsb_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yk8j3x89gmjvzdxpj9jb3hv1j5ljizn5jfwb0hi28y3fpx3301d")))

(define-public crate-rsb_derive-0.2 (crate (name "rsb_derive") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1d9jybwsw2cjyqissfna12rxvr66srjrd9yp741dn8d21rmk9lxl")))

(define-public crate-rsb_derive-0.2 (crate (name "rsb_derive") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0f1cj96xzd0sq0s1z5hb58nvifh4xjqj0c5myipja2kjcgk6mflz")))

(define-public crate-rsb_derive-0.2 (crate (name "rsb_derive") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11bxi3drksk6vhdj7al6lw86j0ivkkwq2psscj8zv4738v83y5si")))

(define-public crate-rsb_derive-0.2 (crate (name "rsb_derive") (vers "0.2.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wa4i4dcvhsfd8a7mycyfq5wanjl9vl9ppf4wsl7lhwxzjkd1n17")))

(define-public crate-rsb_derive-0.3 (crate (name "rsb_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ph56ddzm4p9pj0yvyj791cm43x1pcqx2zj8xbbwyxf359wlc2vx")))

(define-public crate-rsb_derive-0.4 (crate (name "rsb_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dmgk7n476xdqzpmprmpmz4bd5f5xf260hqxjdzj3s9h85k770c5")))

(define-public crate-rsb_derive-0.4 (crate (name "rsb_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1iazgcwdiyhwlzmxqhdrp2im3vlgmgn0l0bbhf1x31qxhk9mmi7d")))

(define-public crate-rsb_derive-0.4 (crate (name "rsb_derive") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10ixzg32dvk2c6rs036k818cmpas874nxbiymmfjkfk1ncp4cw54")))

(define-public crate-rsb_derive-0.5 (crate (name "rsb_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08gfyda5mbyy3l2vdg6m5qixih4kwk1sgf7kyi8zn63lkwch7mgw")))

(define-public crate-rsb_derive-0.5 (crate (name "rsb_derive") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19794z2gmyqyb7pw5l2pwv0w16zqg3z8b5q95qbz3ifdzi13xidj")))

