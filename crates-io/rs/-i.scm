(define-module (crates-io rs #{-i}#) #:use-module (crates-io))

(define-public crate-rs-i18n-0.0.1 (crate (name "rs-i18n") (vers "0.0.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "0mzzww74yq7a9pgvcsglpgj9rnh69fvgprp1kk6a9jkdra3mjddm")))

(define-public crate-rs-io-0.1 (crate (name "rs-io") (vers "0.1.0") (hash "1f9v8rhc9rq3m1lcyh8jivp477n53xxix618n7v5lcbr95fhv1sw")))

