(define-module (crates-io rs ne) #:use-module (crates-io))

(define-public crate-rsneat-0.1 (crate (name "rsneat") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1nc32hlayzmkf6sgw7rnl2f8y5mxxazzwaf6ynvjw8hf9lg812i4")))

