(define-module (crates-io rs wo) #:use-module (crates-io))

(define-public crate-rsworld-0.1 (crate (name "rsworld") (vers "0.1.0") (deps (list (crate-dep (name "rsworld-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1v50l5avqja68rpamm6b54vkgx793sg5ic982my370wfjvcig6jz")))

(define-public crate-rsworld-sys-0.1 (crate (name "rsworld-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "185d1527wbdbdipwvpm9x2pnnkgl9z8mcsh66yznzm1zysbx1wz6")))

