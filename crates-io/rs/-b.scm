(define-module (crates-io rs -b) #:use-module (crates-io))

(define-public crate-rs-blocks-0.1 (crate (name "rs-blocks") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1vpqw6gkfjy3w1yszy8yxsrya18kv98m9klbss1pmvpjd7yr7hal")))

(define-public crate-rs-blocks-0.1 (crate (name "rs-blocks") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rs-blocks-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0miks9qhxnn6m5kif1kqf2dw8syj3czg95qfdk18qsf5y8jclc8v")))

(define-public crate-rs-blocks-0.2 (crate (name "rs-blocks") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rs-blocks-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "171snpm91m7cm43q4ffmr8mgn53nyzfsygyrk2r6plmmcq47v2n3")))

(define-public crate-rs-blocks-1 (crate (name "rs-blocks") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rs-blocks-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1wm8v0svqspfz2xi840dyn62ga09dbq78vzvbx2l862ank9c0y1v")))

(define-public crate-rs-blocks-derive-0.1 (crate (name "rs-blocks-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l32j6cs728gkv4cpai5gi01aaszlyxkc5qbl88nbr3sqwj1mc65")))

(define-public crate-rs-borsh-0.1 (crate (name "rs-borsh") (vers "0.1.0") (deps (list (crate-dep (name "cursive") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "gettext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ds59bmwx6c4bnz1aqbjsfx4r8w4x5pv8cckvk52qyh6qhhnwi2r") (yanked #t)))

(define-public crate-rs-bucket-filter-0.1 (crate (name "rs-bucket-filter") (vers "0.1.0") (hash "0imd471b5xwz3x4b3hn16bkxqb9a17lilj9s644pnm7l4r8piqwr")))

(define-public crate-rs-bucket-filter-1 (crate (name "rs-bucket-filter") (vers "1.0.0") (hash "0qas1clzfcr8s1glbr08hpxfacwwa7z41agx5irzypqvd7jg8qn8")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0knz56ijh77bz3i21bd2wvic1z8nlqldvq94wd3msp6x0y5ssas6")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ah4hyzzk7bafkswfdf6b29j6mzby9yb4gvv1czww31m6yq5vcil")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1vm0q8jciagkw01b6zgkhbnxvv7c5wcz6qg259p6p3gndpz58g0i")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1hmx42mcb6vfi2p659z88cpkknzqbv40lngz4pxqk9v03dmrhxrj")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "04wq28ymrwx09amgqy9bdw60xlsl5gwlxgig7d56h2gys3rqb1fp")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1mzj88nfn6irflk81cp23k52vq25w0wjlmrllzkpy730vwvgzfph")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1drj73iq1qccp7zln29024fgvjpqvqvmpphbiv0dqpwn4wngwi6g")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0bzxylps3mbgic7nh6wz457xlnjyg16jzj3j1dy74cyyj1ry2c98")))

(define-public crate-rs-bush-0.1 (crate (name "rs-bush") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0hb654b5q2p19l97frwdly54ggkc9ksg86f03h05i3i2ldjxzqgv")))

(define-public crate-rs-bytebuffer-0.1 (crate (name "rs-bytebuffer") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1c8l35hh8b8wsyhv6gnwgxaxw88m74jws8k677ncvdb90wd03sv4") (yanked #t)))

(define-public crate-rs-bytebuffer-0.1 (crate (name "rs-bytebuffer") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1m3ih1wjn40sm50vvi07651cfjiynx3f8shc53k4j6qh80z5gz4a") (yanked #t)))

(define-public crate-rs-bytebuffer-0.1 (crate (name "rs-bytebuffer") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "14fj1g5cfyrj3raqjx9hhrna6i8andhhlgwrxi35phrscmgzlxws") (yanked #t)))

(define-public crate-rs-bytebuffer-0.1 (crate (name "rs-bytebuffer") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "09bs842ax5idq59q9dcjpc7yyikc5jics5bmqxh3rqd747xd2qj2")))

