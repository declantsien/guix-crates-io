(define-module (crates-io rs mn) #:use-module (crates-io))

(define-public crate-rsmnl-0.1 (crate (name "rsmnl") (vers "0.1.0") (deps (list (crate-dep (name "errno") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.101") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.7.13") (features (quote ("os-poll" "os-util" "udp"))) (default-features #t) (kind 2)))) (hash "13glqgc0hzplm9hyh5jabqp21rdrwwqxllk9hw3f7xd2g7hhznhx")))

(define-public crate-rsmnl-derive-0.1 (crate (name "rsmnl-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.75") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1cdaqm0lwhkgmpp3aibv3nx0532j0cvi5v2ry2mlr66rsklqh0s7")))

(define-public crate-rsmnl-linux-0.1 (crate (name "rsmnl-linux") (vers "0.1.0") (deps (list (crate-dep (name "errno") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.101") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.7.13") (features (quote ("os-poll" "os-util" "udp"))) (default-features #t) (kind 2)) (crate-dep (name "rsmnl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rsmnl-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q4hiw1bldahd2gn7zcsygk014cgh0sdb06jbicavfvq7rng9l66")))

