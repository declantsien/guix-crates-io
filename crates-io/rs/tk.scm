(define-module (crates-io rs tk) #:use-module (crates-io))

(define-public crate-rstk-0.1 (crate (name "rstk") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0hklw1qyw8s5z0c6sd7zxi2bbkhgvxkdgksfka4wni40fpv387gh")))

(define-public crate-rstk-0.2 (crate (name "rstk") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1ja0cljx9mmd8as39gf3rp2fpq7i43bxyymfp0hkfhslr8z3688x")))

(define-public crate-rstk-0.3 (crate (name "rstk") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "01r85pf3cvr6wdha55v5ly84y0391yhf50s67a8gm72wwgi4pp8a")))

