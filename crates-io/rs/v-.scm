(define-module (crates-io rs v-) #:use-module (crates-io))

(define-public crate-rsv-core-0.0.2 (crate (name "rsv-core") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "1zmw3qd8chqk7y0cq2dcir41f3pxx5878xpwgizf61x28l3apx9v")))

(define-public crate-rsv-core-0.0.3 (crate (name "rsv-core") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "1kmx3wh7bvzzflqc6mpxl04lrycj04gi8h4081i4b5fgis0z9hzr")))

(define-public crate-rsv-data-0.1 (crate (name "rsv-data") (vers "0.1.0") (hash "0l422cv6v4rx34qsinlh5hbpdqc8acdyws2zsg7qwszl2nkw73jz")))

(define-public crate-rsv-rust-0.0.2 (crate (name "rsv-rust") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "10m8l8l4p3261jix1hn9c6dcv4akpxjxkz0as2q55mdw2s7y7nda") (yanked #t)))

