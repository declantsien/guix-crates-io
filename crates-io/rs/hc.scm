(define-module (crates-io rs hc) #:use-module (crates-io))

(define-public crate-rshc-0.1 (crate (name "rshc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "149lc0rnczpz74aqkpn6v0s97qqs4a8y9np1s0bfn4sk2hxx1fwv") (yanked #t)))

(define-public crate-rshc-0.1 (crate (name "rshc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1idxwvl6lw0h8rj3jl8vgbsd0jgny0gi5g0mprndnzfc8r3rlyhg")))

(define-public crate-rshc-0.1 (crate (name "rshc") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0748zd93ag15g6zicry0jn4zab7kcwk4z4wcm1d4pk0ry2f3kgjg")))

(define-public crate-rshc-0.1 (crate (name "rshc") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s2zqdya0pq7l2zzqha250dhij36qq6l9s6j1dpib3lq4ihdhdlj")))

