(define-module (crates-io rs _m) #:use-module (crates-io))

(define-public crate-rs_md2-0.1 (crate (name "rs_md2") (vers "0.1.0") (hash "1w9509sig93xi30x1zdi91nd4nnwdi2h1ii91sxv5avhmab8y4rg")))

(define-public crate-rs_md2-0.1 (crate (name "rs_md2") (vers "0.1.1") (hash "05kix4sixld2s79xsa161bapx14ibixjx4c8mj36crq1bbc0cjaj")))

(define-public crate-rs_md2-0.1 (crate (name "rs_md2") (vers "0.1.2") (hash "077jqj0765gnwds648mn1nzn1b1mb3dpadpibh274zc7j4bjpbba")))

(define-public crate-rs_md4-0.1 (crate (name "rs_md4") (vers "0.1.0") (hash "07sdwk843sq7w6fk26bp5mbmsa60r8f3nvb09jm00y8r8hhk2wkp")))

(define-public crate-rs_md4-0.1 (crate (name "rs_md4") (vers "0.1.1") (hash "1xsh48zx9nrl8m2cjwlxlphs0b7zkdxkwipz6v4wmm2sdxw20v1q")))

(define-public crate-rs_md4-0.1 (crate (name "rs_md4") (vers "0.1.2") (hash "0kh4msxq94z381j9dm8198a4ngv35a0liz40i4xjmwalp40razrb")))

(define-public crate-rs_md5-0.1 (crate (name "rs_md5") (vers "0.1.0") (hash "0s2dllv5z970g5wwhvknpd7kn0vm4vnarv8phhl59dmywciar8g4")))

(define-public crate-rs_md5-0.1 (crate (name "rs_md5") (vers "0.1.1") (hash "1mnwi8isl0m14771p2r9yiriadp3jsw87rn53sx3dnsmb1fbmwq9")))

(define-public crate-rs_md5-0.1 (crate (name "rs_md5") (vers "0.1.2") (hash "0k4f1xmjb9rzjwzdz0y12nd0kk6jpdv57mgci883jpz6bpf0hira")))

(define-public crate-rs_mdc_2-0.1 (crate (name "rs_mdc_2") (vers "0.1.0") (hash "02h5v86ii3paddnhsf7s9q6wmsw3l3vkki0l29axi68cmj430mjc")))

(define-public crate-rs_mdc_2-0.1 (crate (name "rs_mdc_2") (vers "0.1.1") (hash "1rsy2g4rcmg1vlgkg9c6hv09k4w0lbqqsmj9x6hxa92i7bgjh6ml")))

(define-public crate-rs_mdc_2-0.1 (crate (name "rs_mdc_2") (vers "0.1.2") (hash "0vlnwg62brrjhimkbdmdm3lp3ldlxh2wa3pjypybk4a2yawsxz41")))

(define-public crate-rs_merkle-0.1 (crate (name "rs_merkle") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1mvbiqvb1frdrs4cjv21m093dnq3lk7syg23figirplqb1c5dcxv")))

(define-public crate-rs_merkle-0.2 (crate (name "rs_merkle") (vers "0.2.0") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0qp9hbkjhls2wbvz0gl5yh5kinijxcgqd8fw3215pkj0z8pga11a")))

(define-public crate-rs_merkle-0.2 (crate (name "rs_merkle") (vers "0.2.1") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0g1j9m034bv0hsfgan3bi18ik92p5yrxacm2lw8246xjpixfw4k6")))

(define-public crate-rs_merkle-0.2 (crate (name "rs_merkle") (vers "0.2.2") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "10irszcydcznp7g05wrrpzl4ydr6p0lfyqr29brzgc1rlbcky0cl")))

(define-public crate-rs_merkle-1 (crate (name "rs_merkle") (vers "1.0.0") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "07lbws16wl9mzinll8c6dhb8dp2g9jf0h7dwdgvplc0mv0r7h0zd")))

(define-public crate-rs_merkle-1 (crate (name "rs_merkle") (vers "1.1.0") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "06www551q1pznqzxgh72pjdnvwxq5awf4h08mps8s8binxrdnf6a")))

(define-public crate-rs_merkle-1 (crate (name "rs_merkle") (vers "1.2.0") (deps (list (crate-dep (name "micromath") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.2") (kind 0)))) (hash "0wkkhcvdbb0ya94xn12fnbx20s4vz1wq0n43w7l2ncy1hwsa8cm6") (features (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1 (crate (name "rs_merkle") (vers "1.3.0") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10") (kind 0)))) (hash "0g3js9w69ia6723rkc0mnb3c847gqw8brbpn9w9xmwqsdbxipwy6") (features (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1 (crate (name "rs_merkle") (vers "1.4.0") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10") (kind 0)))) (hash "19l91vfyyjwf8g63v74k4jb1w9sspyqi2g9il4cmczgc08c1l98y") (features (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1 (crate (name "rs_merkle") (vers "1.4.1") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10") (kind 0)))) (hash "04588zyynf7larpj8a4gx51d1zsaj3ipr8rsnyqlrpkfr995f8h5") (features (quote (("std" "sha2/std") ("default" "std"))))))

(define-public crate-rs_merkle-1 (crate (name "rs_merkle") (vers "1.4.2") (deps (list (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10") (kind 0)))) (hash "13qj85ha2wnkvwa21y5s58wwh11xc93qriwliplzjkmpb4p1s91v") (features (quote (("std" "sha2/std") ("default" "std"))))))

