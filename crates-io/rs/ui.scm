(define-module (crates-io rs ui) #:use-module (crates-io))

(define-public crate-rsui-0.1 (crate (name "rsui") (vers "0.1.0") (deps (list (crate-dep (name "piston_window") (req "^0.87.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-glfw_window") (req "^0.47.0") (default-features #t) (kind 0)))) (hash "1dqagqcqqgn36bfkq1hh6lzw2nzcv28dd69paha37adc7frywv5i")))

(define-public crate-rsui-0.4 (crate (name "rsui") (vers "0.4.0") (deps (list (crate-dep (name "piston_window") (req "^0.87.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-glfw_window") (req "^0.47.0") (default-features #t) (kind 0)))) (hash "0sp0a52cxnpklirnkjjp6vcbim7xwk0sjn724rh0dv4c47p4z5y0")))

(define-public crate-rsui-0.10 (crate (name "rsui") (vers "0.10.0") (deps (list (crate-dep (name "piston_window") (req "^0.87.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-glfw_window") (req "^0.47.0") (default-features #t) (kind 0)))) (hash "0bhf41pfgnc16ivzivx04figb9jca79b22vni3k7rmjxd1hxbiqf")))

(define-public crate-rsui-0.11 (crate (name "rsui") (vers "0.11.0") (deps (list (crate-dep (name "piston_window") (req "^0.87.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-glfw_window") (req "^0.47.0") (default-features #t) (kind 0)))) (hash "0blhfvmrp2r4qshmcvwva3mwz24x3hs27hslykvq2yzzzzz6cihf")))

(define-public crate-rsui-0.12 (crate (name "rsui") (vers "0.12.0") (deps (list (crate-dep (name "find_folder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.87.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-glfw_window") (req "^0.47.0") (default-features #t) (kind 0)))) (hash "13kfsppafmvd16radmca71ccr5j401a4i56h4ns5yzifj171brcy")))

(define-public crate-rsui-0.13 (crate (name "rsui") (vers "0.13.0") (deps (list (crate-dep (name "piston_window") (req "^0.87.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-glfw_window") (req "^0.47.0") (default-features #t) (kind 0)))) (hash "17najwd4k8svlzs1b3bb7x2k7pyyyin9p5gw44ghd548cgwa6xbm")))

