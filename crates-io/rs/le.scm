(define-module (crates-io rs le) #:use-module (crates-io))

(define-public crate-rslexer-1 (crate (name "rslexer") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0p47hy8bz3lcan0l44g7r4rvxry3jlwffzfxpc6sqrxjgzbmcqbv") (yanked #t)))

(define-public crate-rslexer-1 (crate (name "rslexer") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0pw1jg890q5c39d82jwbdz4n8a1jqshz2zkddcyby6blqfgz0n6j") (yanked #t)))

(define-public crate-rslexer-1 (crate (name "rslexer") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "07z6dqm7vdnihwjzz8gcg3lc4j3bkmbyvmkqmbfmplm2hvriw8gb") (yanked #t)))

