(define-module (crates-io rs me) #:use-module (crates-io))

(define-public crate-rsmemoryapi-0.0.1 (crate (name "rsmemoryapi") (vers "0.0.1") (hash "1wfqhwryiy1q4yz8jqfsh9l7qchaabiml2hcmhg8rqbydma8m1yq")))

(define-public crate-rsmemoryapi-0.0.2 (crate (name "rsmemoryapi") (vers "0.0.2") (hash "1y0d260jj70gjqwq34jzlszcx1hrjzhl6yl64k62blh5m8c4dq5l")))

(define-public crate-rsmemoryapi-0.0.3 (crate (name "rsmemoryapi") (vers "0.0.3") (deps (list (crate-dep (name "rsmemoryapi") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "154balzgx6ispsvc3l15sm0maakf697jc0a5mcplw796kclhbdc7")))

(define-public crate-rsmemoryapi-1 (crate (name "rsmemoryapi") (vers "1.0.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("wincon"))) (default-features #t) (kind 0)))) (hash "1xc4qcjx845sqg9lqqca7b15ff5928zmxy0z609hi6k9w7x3sphp")))

(define-public crate-rsmemoryapi-1 (crate (name "rsmemoryapi") (vers "1.0.1") (hash "0njrlzhb18wy75k7dmw81kzd5xnq8n669iv5d5rypy3p8r5k67z3")))

(define-public crate-rsmeshopt-0.0.7 (crate (name "rsmeshopt") (vers "0.0.7") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "09x3lipfgkrp6agm2rccwhcq92fwaz9nfdy9r1sp9ddh04969y9l")))

(define-public crate-rsmeshopt-0.0.8 (crate (name "rsmeshopt") (vers "0.0.8") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0a13q05759w89hy2d9ddzp4zjs83xvf3v4ggf67nk30ghy0yxkk1")))

(define-public crate-rsmeshopt-0.0.9 (crate (name "rsmeshopt") (vers "0.0.9") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1axab2z7p1dkxawnp3zvqax55zhizlrrd0hkjm5b0mvwj97pmxm8")))

(define-public crate-rsmeshopt-0.0.11 (crate (name "rsmeshopt") (vers "0.0.11") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zrrad1gmccnlax44p8q0gjnkp8aph303dp3hy5zcpacrqg40lh5")))

