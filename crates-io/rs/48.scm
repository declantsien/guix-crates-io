(define-module (crates-io rs #{48}#) #:use-module (crates-io))

(define-public crate-rs48-1 (crate (name "rs48") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "04cds80s1721bz57k1zfi2jjzj43h4jd7mr6w8rr0hcnkpjbagb0")))

(define-public crate-rs48-1 (crate (name "rs48") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rs48_lib") (req "^1.1") (default-features #t) (kind 0)))) (hash "1087rrkkjpkcb97xzf1nb0zz7430imnn0zy96z0ycsk8hwihkjwi")))

(define-public crate-rs48-1 (crate (name "rs48") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rs48_lib") (req "^1.2") (default-features #t) (kind 0)))) (hash "1kgsag667j6faxk74xdwpy017jbfd1lq0v7r0wazkfbayh9x2ak4")))

(define-public crate-rs48-1 (crate (name "rs48") (vers "1.3.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rs48_lib") (req "^1.3") (default-features #t) (kind 0)))) (hash "0c8b7pqwb9cg1qgn5lh6b6h26q5sig5j4jqwslkd91sydqj46kf3")))

(define-public crate-rs48-1 (crate (name "rs48") (vers "1.3.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rs48_lib") (req "^1.3") (default-features #t) (kind 0)))) (hash "1zhhja0wiv4f9ixjb31hmkqfnmnp2nfdr12nppawnkdnx3kcmjp9")))

(define-public crate-rs48-1 (crate (name "rs48") (vers "1.3.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rs48_lib") (req "^1.3") (default-features #t) (kind 0)))) (hash "1lq21zyzmqjfi1rizyp41dgi8s4fa47qrkqzr2smlkg8pgf86npd")))

(define-public crate-rs48-1 (crate (name "rs48") (vers "1.3.3") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rs48_lib") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1hj0s5nz6435gl0h055hawhmd6z5yd091ls558yv94sglmyd3nwg")))

(define-public crate-rs485-0.1 (crate (name "rs485") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.24") (default-features #t) (kind 0)))) (hash "0nl3ms4pmjz5d32c80vq0jbh4y565bgw2vhgrrqzmyg9s97454bl")))

(define-public crate-rs48_lib-1 (crate (name "rs48_lib") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "076ndfniwiv8cvcm1fmcy9rzgk979xndyq6v8rf355sm8z94v8rd")))

(define-public crate-rs48_lib-1 (crate (name "rs48_lib") (vers "1.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "199p3djkb5170gdcpcnc4iysrww5gkv7vq0mxh43ylcn5vsmfjcf")))

(define-public crate-rs48_lib-1 (crate (name "rs48_lib") (vers "1.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1qmwimdkjjlkvyvqgx6m9022zxcckmgvb5a1mz81ygic7b1pd1dm")))

(define-public crate-rs48_lib-1 (crate (name "rs48_lib") (vers "1.3.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0q13n21vkhqc6s20ilpkpq5pjvcab4j25yynkha0axg4cf12bcbv")))

