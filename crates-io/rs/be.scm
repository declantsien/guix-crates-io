(define-module (crates-io rs be) #:use-module (crates-io))

(define-public crate-rsbeancount-0.1 (crate (name "rsbeancount") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)))) (hash "1q2dl36vjyppzp20f5jhzw1mvn5g6ikkk9n2vhzickczj3wdjjfb")))

(define-public crate-rsbeancount-0.1 (crate (name "rsbeancount") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)))) (hash "0wygfc1m08qzrgiz4a5qi6nhdq4whyrc059748z3ih5y6jgzy3bl")))

