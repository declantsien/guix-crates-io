(define-module (crates-io rs -r) #:use-module (crates-io))

(define-public crate-rs-ray-0.0.1 (crate (name "rs-ray") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0x8xnj05r961pcd78fcsic70alf973abr1nvinikqb4can1lx02s")))

(define-public crate-rs-ray-0.0.2 (crate (name "rs-ray") (vers "0.0.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0zsj0ympf4n98877l5qsf7fag5nf8fhlvhfhfz8ljqkv7fy10afl") (features (quote (("with_tokio" "reqwest/default-tls" "tokio") ("default"))))))

(define-public crate-rs-ray-0.0.3 (crate (name "rs-ray") (vers "0.0.3") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1b7ijlai68vpz7wh8g4k5d1nmbf99srn5wxhpxp9916aysn2bl3v") (features (quote (("with_tokio" "reqwest/default-tls" "tokio") ("default"))))))

(define-public crate-rs-ray-0.0.4 (crate (name "rs-ray") (vers "0.0.4") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1idad7cff3az11c67j9glnwr9q7py0aic7bscdnzz1rbsw9kjrn6") (features (quote (("with_tokio" "reqwest/default-tls" "tokio") ("default"))))))

(define-public crate-rs-rdb2kv-0.2 (crate (name "rs-rdb2kv") (vers "0.2.0") (hash "0xcrx7kvxwamxnrp1bz6vkyd37z0b9077h5kfkxaxngxixzhx0rp")))

(define-public crate-rs-rdb2kv-0.3 (crate (name "rs-rdb2kv") (vers "0.3.0") (hash "14r2nkz6cwkr7halyad26jnss63hgf54rczqcr309zcwz0l3ig0f")))

(define-public crate-rs-rdb2kv-0.4 (crate (name "rs-rdb2kv") (vers "0.4.0") (hash "11c31y3qsly1mi5l53h924nvlhadpjs021l3mfqjh048xgkx9yp2")))

(define-public crate-rs-rdb2kv-0.5 (crate (name "rs-rdb2kv") (vers "0.5.0") (hash "1ai4j0rj7aa0kjajw67188xi7gdrara0441x4hvp02jl4j5bcfyp")))

(define-public crate-rs-rdb2kv-0.6 (crate (name "rs-rdb2kv") (vers "0.6.0") (hash "0j9nwmfbp4bc1463azqgcjvnmg84azqqazsnx6x6k4g8ims3jivk")))

(define-public crate-rs-rdb2kv-0.7 (crate (name "rs-rdb2kv") (vers "0.7.0") (hash "15cq4s87yrkjzfh2pn4zh1sg3vh8b7g95j00kda7z5gh4b9cb10r")))

(define-public crate-rs-rdb2kv-0.8 (crate (name "rs-rdb2kv") (vers "0.8.0") (hash "05mwzas759vng5z6hr6a5xa5yall1dg6jisjhknm9sn7hw2ahndm")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.0") (hash "091mbxf5pcjcfx4swl53w0j8micx5f2850pk60ln5bkiqrkg9ccm")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.1") (hash "1w6x9dvxf40wiz9q4p7gpsmmkm7gzjpcn8m9czs5naq58xdssmzn")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.2") (hash "1j8q9h9anapxknl6vg9zv0dbkkxsxx9r7yd71fxjwryn4prnx2hc")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.3") (hash "0javmypdn8zp9c5zbhr3cys44ww6jvvfih5rsf0f7i1vk01yndcc")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.4") (hash "0i5d29amkx468dz7dkx5nlybzz30fgr04nd86mpihj1k93yhrsz1")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.5") (hash "01za2ibx1nh460968qx5j71vh2yflp0mc6khrip208qvag8k00kb")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.6") (hash "0ipyssqszjr0cah8hyw2xl3ihm8l9z6g8inxg07cfan50zff1630")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.7") (hash "0wlrshgp192wi3j9cqg5m2qq92rnc8fxhrdaxn1w56qb9b7p9a6l")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.8") (hash "060s1bg4knzc5qk8ai3k1b107zvvdkzs5sdq7vipk98phlkwvv1m")))

(define-public crate-rs-release-0.1 (crate (name "rs-release") (vers "0.1.9") (hash "0yhi7i8xxmc8kpkd5b4dimqa788iww4b1k2w9b0jzya52wwvmvr1")))

(define-public crate-rs-rocksdb-0.5 (crate (name "rs-rocksdb") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "local-encoding") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rs-rocksdb-sys") (req "^0.5") (default-features #t) (kind 0)))) (hash "0279hsnwapmanxwvn65wf1dbvrszqvbv2y658xi001wyd0n1xhfr") (features (quote (("valgrind") ("default"))))))

(define-public crate-rs-rocksdb-sys-0.5 (crate (name "rs-rocksdb-sys") (vers "0.5.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "local-encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0b8j52v5i6says6dqby1pbwss0zj1pdbgz29ns8jfxzln46c8097") (features (quote (("default")))) (links "rocksdb")))

(define-public crate-rs-router-0.1 (crate (name "rs-router") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "quick-error") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wcgkwc009w99hb8x9ng7y8yanwh21a536laz5lpaqndfkrqqs1s")))

