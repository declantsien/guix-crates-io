(define-module (crates-io rs ud) #:use-module (crates-io))

(define-public crate-rsudo-rs_support-0.1 (crate (name "rsudo-rs_support") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "17zidb4j6b0mggz27gh2bjr3y8ilm1wzk1zz0pam96nv7jm92jvg")))

(define-public crate-rsudo_support-0.1 (crate (name "rsudo_support") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "04d84hjw36h1si15djp1fd3871z43p0m6a7k863nj3d1adg83q0f")))

(define-public crate-rsudo_support-0.1 (crate (name "rsudo_support") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "11xkgh2q72ap93jwxv553ifk61fgqvcw83cscik6gp2n0fzb5ani")))

(define-public crate-rsudo_support-0.1 (crate (name "rsudo_support") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0vi6w8j1mh3f2yy2z44d9sbwgb12bxj9xx2xwngggvrp1zm1jlw4")))

(define-public crate-rsudo_support-0.1 (crate (name "rsudo_support") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1d7m4l3x7bmgrm9avxcwywvyyhx4mv3d9zxxdm4a3qhpcixiwxvi")))

(define-public crate-rsudo_support-0.1 (crate (name "rsudo_support") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0nvzbhsskwnz6b0xagsf16mplr9gr3bqm0rhnkrdfd7hbzz9qvax")))

(define-public crate-rsudo_support-0.1 (crate (name "rsudo_support") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "10dy8lvx8zplay996yp53bp2nfy32n3r23mvmca6l7grpiy2h8mg")))

