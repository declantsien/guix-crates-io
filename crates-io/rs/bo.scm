(define-module (crates-io rs bo) #:use-module (crates-io))

(define-public crate-rsbot-0.1 (crate (name "rsbot") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s5yxm1y19pgbr7hlj97mxqjbf4k7agz3dr8f7krzp5lhcav2m9c")))

(define-public crate-rsbot-0.1 (crate (name "rsbot") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "079k6yxq0rb8ymdq9m72c4wan5fb5lfnpm7l1j4vb5dgazlr88fv")))

(define-public crate-rsbot-0.2 (crate (name "rsbot") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rsrnb74ds5md96ln8krm5hqzwb4jj6lxig93ximjps43s6yf68s")))

(define-public crate-rsbot-0.2 (crate (name "rsbot") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0d9982yrqmjrcdgsh2x5z3751q951lwb3z788bqi6w65ac7skm6h")))

(define-public crate-rsbot-0.2 (crate (name "rsbot") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ij6p6z2a7hag88shbm8jckd70pyx6l9dhs6gz785fys5rf4f8zf")))

(define-public crate-rsbot-0.2 (crate (name "rsbot") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1c0ncv3fqdfcjkjy1la1mj18y76dpma1fx4j7njpxzbaps6khdfm")))

(define-public crate-rsbotters-0.0.1 (crate (name "rsbotters") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15580714girxi4k7djmrp5srxavwsdpcprgxh01dl6hrvh49njjk") (yanked #t)))

(define-public crate-rsbotters-0.0.2 (crate (name "rsbotters") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17w3qwrcgbj4rml01y4zal8clki0b4x6clj4p33by6fbs1q7rlka") (rust-version "1.69.0")))

