(define-module (crates-io rs #{32}#) #:use-module (crates-io))

(define-public crate-rs3270-0.1 (crate (name "rs3270") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "10mvagm4f13mhxiqsiy4ra398jvl0c1vyj44x1md0f8zbabdl8qz") (yanked #t)))

(define-public crate-rs3270-0.1 (crate (name "rs3270") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "1kw7yqmx0cwyh2asqaj5dp5ilw0x0w9xa4hpkkbcdwdm3gjkadpv") (yanked #t)))

(define-public crate-rs3270-0.1 (crate (name "rs3270") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0g0lig8whhq7xmij236bwb6c8iaaq8a28vbnc8g72fb1sx5w0i4j")))

