(define-module (crates-io rs _l) #:use-module (crates-io))

(define-public crate-rs_lib-0.1 (crate (name "rs_lib") (vers "0.1.0") (hash "0zvsqbrs9n038l1sas7ympf87d9x9c8m5fwpwhw86wizkd5hgngd")))

(define-public crate-rs_lib-0.1 (crate (name "rs_lib") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k3l71fg15dwfhfz0wfqnyrmg6vwivcarj8pr1nkdkxrv16615p3")))

(define-public crate-rs_lockfree-0.1 (crate (name "rs_lockfree") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "core_affinity") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cnyzpdp34xbaxcrg6xakazprcj15xbc4gdjdqd2ss89xcin36kx") (features (quote (("max_thread_count_4096") ("max_thread_count_256") ("max_thread_count_16") ("default" "max_thread_count_16"))))))

(define-public crate-rs_lockfree-0.1 (crate (name "rs_lockfree") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "core_affinity") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1chyrnh6r2ar830jvxnbl7rm60gcjs4pjknigxw6blnm19rxq637") (features (quote (("max_thread_count_4096") ("max_thread_count_256") ("max_thread_count_16") ("default" "max_thread_count_16"))))))

