(define-module (crates-io rs ub) #:use-module (crates-io))

(define-public crate-rsubox-0.1 (crate (name "rsubox") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0g95vw5xnwzgsk4134padc74bzmgpviz8si0yps3z5x339ziph16")))

(define-public crate-rsubox-0.1 (crate (name "rsubox") (vers "0.1.1") (deps (list (crate-dep (name "crc") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1wfa0j8b53l562pas3lyp8302dppf8kzd8a7sqgmjix8qs7v6049")))

(define-public crate-rsubox-0.1 (crate (name "rsubox") (vers "0.1.2") (deps (list (crate-dep (name "crc") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "19s0f6bq40cjxjjvjnd1kkr0y5idcjiqimmhcpp4b1z5093mr0rz")))

(define-public crate-rsubox-0.1 (crate (name "rsubox") (vers "0.1.3") (deps (list (crate-dep (name "crc") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0lf9ia60njc8wqgisdj2qmjx6lh2g2449sckxshib509myxv6hp3")))

(define-public crate-rsubox-0.1 (crate (name "rsubox") (vers "0.1.4") (deps (list (crate-dep (name "crc") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0mhknfblzxgbpw596jzhaxcbvw2jy4pmb3lzs3xv2l65bqn5ypiq")))

(define-public crate-rsubox-0.2 (crate (name "rsubox") (vers "0.2.0") (deps (list (crate-dep (name "crc") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fnmatch-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1xd4jqfgwyzk5433ihkwwx5izvm3p1q3p3wcw50zqm3w45kk5jnm")))

(define-public crate-rsubox-0.2 (crate (name "rsubox") (vers "0.2.1") (deps (list (crate-dep (name "crc") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fnmatch-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0nd66732fi3d9c1w0agip0d05gs2dynl7psn5c7iw4iww3i5njbq")))

(define-public crate-rsubs-0.1 (crate (name "rsubs") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "rsubs-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0666hinkcl57df8ni6jah3m0hbivvq6hr53izmdzdnjbk274sar3") (yanked #t)))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "08bkflgrqkq8rkmv3nnwbmj8cqzmplmk3cm8b1vs65hiq610lk2k") (yanked #t)))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "1dd8yl9z4ahx37jslafw09izphzsly2xacfy25msgjrsg914jknh") (yanked #t)))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "1mg2y2pq4dg70swasiarhcrzbx7lka3gxgjx385f1a6p2mashzgy") (yanked #t)))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "1yr6zaw7l3p7fk80ml82dx6afiw03xlxzb0vy0wfh0ds79ln400w") (yanked #t)))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "0ganc9al230xm9ii6mvs2w5g2yls1zqa7aqzlv3h5qg0lwrqm3zl") (yanked #t)))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qglf13d8npjbnlc4lf25q98c3zzi27pp4212bylmc7pv3k4dz3x") (yanked #t)))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hc121z55wr2w6b2zz6hiz02vn9njy7hv2x728lmsv73wwai6lhj")))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18yx2i6y1f54qa623xyfk553ccxjjl3fdh927020y5m4z8hvj9ii")))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.8") (deps (list (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rcj1apb2a900jcg5yp60jxybp99vibhdq79150930f4w7f58y4m")))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.9") (deps (list (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04vy1qc88d7nv9j5lnsg75x0mii8gji9sjmyrxl79w01ak4b92mz")))

(define-public crate-rsubs-lib-0.1 (crate (name "rsubs-lib") (vers "0.1.10") (deps (list (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cs68lx3f2a82pay6q2v73jjhrp9gz1j3l0m5pnrz80xwhbsx65d")))

(define-public crate-rsubs-lib-0.2 (crate (name "rsubs-lib") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1i8wnkk37lqpil1dk66bd4wkzy3a53p3dirpnwr42nk30nd5bxqd")))

(define-public crate-rsubs-lib-0.2 (crate (name "rsubs-lib") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qnzpfazs3p1j88a3izjq6sd8wv6wikkgg4mizg0bjhgasls5k4x")))

(define-public crate-rsubs-lib-0.3 (crate (name "rsubs-lib") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3") (features (quote ("formatting" "macros" "parsing" "serde"))) (default-features #t) (kind 0)))) (hash "1vpdxc1nscv3143rir71swq598d8xqmbsrxavc3n9isb31zilgpl")))

(define-public crate-rsubs-lib-0.3 (crate (name "rsubs-lib") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.3") (features (quote ("formatting" "macros" "parsing" "serde"))) (default-features #t) (kind 0)))) (hash "00mlk80csx3rp0a7wyga7b21rmy7j8bqsbim9gi4zg5iy04pc7yh")))

