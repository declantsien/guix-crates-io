(define-module (crates-io rs hm) #:use-module (crates-io))

(define-public crate-rshm-0.1 (crate (name "rshm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.131") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1knvn6rx2m0z6c0ccvj4yiyiphbc89w3jwj0z30v52impfkj3h6n")))

(define-public crate-rshmem-0.1 (crate (name "rshmem") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi" "handleapi" "winbase" "errhandlingapi"))) (default-features #t) (kind 0)))) (hash "0lka5kzhkdjlj37wz62d0kvkl0v6piyjlb0nani2pzc8px2zsdx5")))

(define-public crate-rshmem-0.1 (crate (name "rshmem") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi" "handleapi" "winbase" "errhandlingapi"))) (default-features #t) (kind 0)))) (hash "0py0ny1yal82gf1lnj6i13b2pbdjrqxxqhmph1pnbf362zcq5dvs")))

(define-public crate-rshmem-0.1 (crate (name "rshmem") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("memoryapi" "handleapi" "winbase" "errhandlingapi"))) (default-features #t) (kind 0)))) (hash "0zppbkq2nz3mxs66lxqphbilj6s99aj9d7sqgiss318q78cj17mn")))

