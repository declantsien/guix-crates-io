(define-module (crates-io rs at) #:use-module (crates-io))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "084k93yzdi030qrkafz3pzm8lm1hs21a7qxmqiljbqcqrbdmkna1")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0zwkrdxjklzs8c13g97bv278yh8yg97zms7gzpxvjcpgvig88bm2")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "061a5fvh6y79lkdxrz8dvympcjbfljmq0dvjyy43dh5jq1ci9gzz")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "04ymbbjimdvw2csi6w0nqd2b4dznzasilxa88lp0gfv0kv9qbsiw")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "04ic8wxrp032m5kajnhni2ib829kbbhsv6bpfyr0bwfs6v2yjm0x")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1n70wn7p5a95wk1lxqy5kxfqga8jwlzpl7wv3kbdz5y3qs94wwwy")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.6") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xbll2hikqw10gwfjd5jd6xs0rdhdd08xs0h5s6498jcrm5irazj")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.7") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "154wrsf2nca1jhmrm8kpcyj6psb5kvq3hn0zipk7ygpqizh4qs8y")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.8") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zdf8s7nd1pr962clywikl93xda2n2jxh29ifrygca2v231b49zl")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.9") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "15cm0q47yiy7lq0xka8krxwka9sry1wjv8v5x4jyj139vcp7m99p")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.10") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0j5k8yikh6vwd1pghcg8ck07sqmm0b1pl1a7320wb240fzvffhq4")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.11") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1a65w3y1bmj1xbs5jz99gic6gwpg84488hs37w3vgx8aq08l8nmq")))

(define-public crate-rsat-0.1 (crate (name "rsat") (vers "0.1.12") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "solhop-types") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0f33k2xi5yix9m2niwbar125xkjr1281q94gl5hvw1pxd225dcfp")))

(define-public crate-rsat-cli-0.1 (crate (name "rsat-cli") (vers "0.1.0") (deps (list (crate-dep (name "rsat") (req "= 0.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1sqmmrc7gzng5yk5kyi1xr8wcn28yk89v8qknwj0pndkk4drnp21") (yanked #t)))

