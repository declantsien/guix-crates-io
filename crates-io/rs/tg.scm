(define-module (crates-io rs tg) #:use-module (crates-io))

(define-public crate-rstgen-0.1 (crate (name "rstgen") (vers "0.1.0") (hash "18vlj9syp88nl0r3pahqksb51mjr6g52vhiziria7s5vywgyg15s")))

(define-public crate-rstgen-0.1 (crate (name "rstgen") (vers "0.1.1") (hash "17kifcp6khjfygxwdn4xq1vr7hhq4cwvmxx3ypp9h7yk13lc1fcz")))

(define-public crate-rstgen-0.1 (crate (name "rstgen") (vers "0.1.2") (hash "10qy8kgnr5nih6j33mywaz3xsnb4zf3v80bbxdgyqf6fvv94hllw")))

(define-public crate-rstgen-0.1 (crate (name "rstgen") (vers "0.1.3") (hash "1vn9dr7zfkb1f4w64q0dvb4r8kk33rkljxndv9bl0vlqx80aj29x")))

(define-public crate-rstgen-0.1 (crate (name "rstgen") (vers "0.1.4") (hash "0fapd156qvxdkangarbmcjrwlqijjjznmq5lq4i3mdhklyn0h0xh")))

