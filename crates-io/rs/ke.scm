(define-module (crates-io rs ke) #:use-module (crates-io))

(define-public crate-rskey-0.1 (crate (name "rskey") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.201") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 2)))) (hash "09s7gqphrd9x2gsvf29jq6d67kyv03nlyzb77vn6plz1gkfi0zvw")))

(define-public crate-rskey-0.1 (crate (name "rskey") (vers "0.1.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.201") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 2)))) (hash "1pghdij1fiv68acaf1v69viby857h7910zsk7lsi1irgfz1h41v7")))

