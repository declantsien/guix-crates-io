(define-module (crates-io rs ou) #:use-module (crates-io))

(define-public crate-rsoundio-0.1 (crate (name "rsoundio") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)) (crate-dep (name "rci") (req "0.1.*") (default-features #t) (kind 2)))) (hash "126b32cxz77pwcf5w91v9fdd9v6spqycgjzvky9wwrb04cmyr0zj")))

(define-public crate-rsoundio-0.1 (crate (name "rsoundio") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)) (crate-dep (name "rci") (req "0.1.*") (default-features #t) (kind 2)))) (hash "0xmv87hz689ynzk8lapqgrkcmm4s12zy9f01ijmkpbjrb8jw7vba")))

(define-public crate-rsoundio-0.1 (crate (name "rsoundio") (vers "0.1.3") (deps (list (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)) (crate-dep (name "rci") (req "0.1.*") (default-features #t) (kind 2)))) (hash "0h0nyp1qq6fvzxp2h3qczq62v4f1ajdaaxas5bcgxjq0633458dh")))

(define-public crate-rsoundio-0.1 (crate (name "rsoundio") (vers "0.1.4") (deps (list (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)) (crate-dep (name "rb") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rci") (req "0.1.*") (default-features #t) (kind 2)))) (hash "00chj18vg6dzan6dw8ylgynfn9jks0lv8xlllppiblhv44zysjxn")))

(define-public crate-rsoundio-0.1 (crate (name "rsoundio") (vers "0.1.5") (deps (list (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)) (crate-dep (name "rb") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rci") (req "0.1.*") (default-features #t) (kind 2)))) (hash "1jrgn6byz8nqzjnyipzgg4rswrq65yndhn3krhwjqh34kljh7809")))

(define-public crate-rsoundio-0.1 (crate (name "rsoundio") (vers "0.1.6") (deps (list (crate-dep (name "pkg-config") (req "^0.3.7") (default-features #t) (kind 1)) (crate-dep (name "rb") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rci") (req "0.1.*") (default-features #t) (kind 2)))) (hash "0qm7ff60yppbxl5vl3y004id38xjw4w1v43vjd2kd4r1sdm1qwag")))

(define-public crate-rsoup-0.1 (crate (name "rsoup") (vers "0.1.0") (hash "04nddc55wj9k6cagn3kf0320yy97i5l7hzzgz9bky2j1nir73xw4")))

