(define-module (crates-io rs nt) #:use-module (crates-io))

(define-public crate-rsntp-0.1 (crate (name "rsntp") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "090m2xbv1z9i6b027bka4w5yd34prbb1dbiia5fcsxy06y4ayhp2")))

(define-public crate-rsntp-0.2 (crate (name "rsntp") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bhdfnk1s0divhl9aysi40ch2373znczzv83j16q9fdm0ng4kwbq") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-0.3 (crate (name "rsntp") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d2j4m06kf1bs7qq196hvrc483bpzfhf822amf12pvc239awn2aw") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-0.3 (crate (name "rsntp") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dpif9sh5ckhaqp8r7378y67w5a57c5srrydbv4a0wkbwpby908n") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-0.3 (crate (name "rsntp") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "16cgwz3aaj0b6ai2a5p1vy224ilsp6k7acz7qm6x9gwbxdpg5qkd") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1 (crate (name "rsntp") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "0x0r0i7cdg4i39292lma70pf5ylww7ibpzg0nryxl7a1aarsf0jw") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1 (crate (name "rsntp") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "026cfnss40wf9qbhm5w4b6qc6gjmhfn6xhdp83k4ffg1l3fwqnpy") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1 (crate (name "rsntp") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ka1qq872nd2qzf0azwlx754w31ql6fryjlyl6bj6c1s07x1m25r") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-1 (crate (name "rsntp") (vers "1.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("udp" "dns" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hy479rr4zy3r3qyxfv7gl8d3malvg671wmr458bvni9n690zxil") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-2 (crate (name "rsntp") (vers "2.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("net" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bc5149pmp9yv7dpav89k4ynd7z5cjl4fgwi2x25v8vdx0vwlvzx") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-2 (crate (name "rsntp") (vers "2.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("net" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "1crxd5bvifndyvrb0h8lgqllklbhdv6lg7mpq9bv6m253b40j0zg") (features (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-rsntp-3 (crate (name "rsntp") (vers "3.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("net" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "0737ixqqlfj3mxq55qm1j07sxlv6vqgcanh0lvj29bp2cggbzjx1") (features (quote (("default" "async" "chrono") ("async" "tokio"))))))

(define-public crate-rsntp-3 (crate (name "rsntp") (vers "3.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("net" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "0w3nlgkwd8hdn3k4d1i9wkmichh39d3wf443vglfj2laqagdnc2z") (features (quote (("default" "async" "chrono") ("async" "tokio"))))))

(define-public crate-rsntp-3 (crate (name "rsntp") (vers "3.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("net" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "127107dkz7v0542255jxq8xy69wh61h8l163lh6k113j57sqwlwm") (features (quote (("default" "async" "chrono") ("async" "tokio"))))))

(define-public crate-rsntp-4 (crate (name "rsntp") (vers "4.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("net" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qgkday7add8g0ki39rb5dzi6gqjxigidp5shbdg1i2gfr2mzkjw") (features (quote (("default" "async" "chrono") ("async" "tokio"))))))

