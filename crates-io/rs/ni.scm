(define-module (crates-io rs ni) #:use-module (crates-io))

(define-public crate-rsniff-0.1 (crate (name "rsniff") (vers "0.1.0") (deps (list (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)))) (hash "1dkizijz7gjd6pab7hvgkwb4lql329acbz91xwyrsvr0zk45l7b1")))

(define-public crate-rsniff-0.1 (crate (name "rsniff") (vers "0.1.1") (deps (list (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)))) (hash "07i32d534d2hkv4kly2g9wrd4h058ilxiqpwhq8z8vi0zjj6hqy7")))

