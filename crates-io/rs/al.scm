(define-module (crates-io rs al) #:use-module (crates-io))

(define-public crate-rsalgo-0.0.1 (crate (name "rsalgo") (vers "0.0.1-init") (hash "0gp6vm61rrpv9yxzfbr93vwklhlvn49asz53gfn26scscck6jnnw") (yanked #t)))

(define-public crate-rsalgo-0.0.1 (crate (name "rsalgo") (vers "0.0.1-init.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0nn2qhsirkc30r40fb761r4vss4kpvd87kgv2bn783dn7vcn7sp9")))

