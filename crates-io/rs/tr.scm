(define-module (crates-io rs tr) #:use-module (crates-io))

(define-public crate-rstr-0.1 (crate (name "rstr") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tree_magic_mini") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1m5rfz4z8hdz8gwx9ff8finjzy2f20wxd040ggrl5gx09325msyz") (features (quote (("progress_bar" "indicatif"))))))

(define-public crate-rstreamable-0.1 (crate (name "rstreamable") (vers "0.1.0") (hash "14zqllia7qjfbqkaalwjxfasi28ra7rkd2xw0yvqar20s0rlrs87")))

(define-public crate-rstreams-0.1 (crate (name "rstreams") (vers "0.1.0") (deps (list (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "fork_stream") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "redb") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "samsa") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "1vgdl410i9ayzizdfh2qqgl85agggpmb9c9047q22dryxazwm5ch") (features (quote (("kafka" "samsa"))))))

(define-public crate-rstree-0.1 (crate (name "rstree") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pc1jp1zyn23wvic93mqxj7lybkw949krqr7ld70c8iimmxvm90y") (yanked #t)))

(define-public crate-rstree-0.1 (crate (name "rstree") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "026sqxzj0wil5b3y843zji6mc57r8nm3wwhhfddwmj87xazvbqrr") (yanked #t)))

(define-public crate-rstring-builder-0.1 (crate (name "rstring-builder") (vers "0.1.0") (hash "15k1ba4q57bwin7m1yn64j35jzvl5icrlv7fhd42q1ajxbsd430f")))

(define-public crate-rstring-builder-0.1 (crate (name "rstring-builder") (vers "0.1.1") (hash "1iiga1yca54g4c2w0n143dk97hwry63dim47h0j19nwzkiipi1gk")))

(define-public crate-rstring-builder-0.1 (crate (name "rstring-builder") (vers "0.1.2") (hash "0ds87lmk4mbsfiwcmrl7nql9dfydln22y0mn3yvqanfzkmqx03aj")))

(define-public crate-rstring-builder-0.1 (crate (name "rstring-builder") (vers "0.1.3") (hash "0qi7sbaf8sg0na5yh7kxbi5nap486gaahcm7pcm040j04myvglir")))

(define-public crate-rstring-builder-0.1 (crate (name "rstring-builder") (vers "0.1.4") (hash "0c58as9y1k9fj6vfni8x3kc8176iw59b103wf3gdk9lpmicm4y57")))

(define-public crate-rstrpc-0.0.1 (crate (name "rstrpc") (vers "0.0.1") (deps (list (crate-dep (name "axum") (req "^0.6.20") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.10") (features (quote ("server"))) (default-features #t) (kind 0)) (crate-dep (name "jsonrpsee") (req "^0.21.0") (features (quote ("server"))) (default-features #t) (kind 0)) (crate-dep (name "rstrpc-macros") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.195") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "tower") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "ts-rs") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "typeshare") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "108gasz202ffp96m33piwrcg570gjd7zshcfa1v9dz2rvk98jk8g")))

(define-public crate-rstrpc-macros-0.0.1 (crate (name "rstrpc-macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)) (crate-dep (name "ts-rs") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "05cjqvgv9vcb5x89lp6j2zsma7b75dy4kvamgz1jkavhba0sdxw1")))

(define-public crate-rstructure-0.1 (crate (name "rstructure") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1bl8irbarm52cg39h2wmc3cylyiqhx6bgpjy8fnzs8qxkbj0h1lp")))

(define-public crate-rstructure-0.2 (crate (name "rstructure") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0n3l6vphsb129q40gxg8bfkcmb2sy2mrdv5lhpfh0ambfda9fpgn")))

