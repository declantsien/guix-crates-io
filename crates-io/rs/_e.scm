(define-module (crates-io rs _e) #:use-module (crates-io))

(define-public crate-rs_ed25519-0.1 (crate (name "rs_ed25519") (vers "0.1.0") (hash "0i46rsz5bnj7pk7yzrip80vk97i7zvlbmamn8a4g1pl2q39dqd8r")))

(define-public crate-rs_ed25519-0.1 (crate (name "rs_ed25519") (vers "0.1.1") (hash "0k0cjyjb8dfqr3q5y5q15dg5p0rpmgpdqwxpvzm26d72z97g83s4")))

(define-public crate-rs_ed25519-0.1 (crate (name "rs_ed25519") (vers "0.1.2") (hash "09wrin4x4njiq31jp4a8cahq80gfzq47b8286vghd07i419w5r3x")))

(define-public crate-rs_ed448-0.1 (crate (name "rs_ed448") (vers "0.1.0") (hash "1znx1ydlay1jfhlrnfgn0f0rsx4sblfgwy8sffdyq5d5djhgmjwl")))

(define-public crate-rs_ed448-0.1 (crate (name "rs_ed448") (vers "0.1.1") (hash "1iixjj5cnrz6ji65kic8azyqv5hj15pd3mgskdvp6pzkv4p92c6n")))

(define-public crate-rs_ed448-0.1 (crate (name "rs_ed448") (vers "0.1.2") (hash "1axmcgx6jnmbmps5yz7fj5qq2lvs9jzp4im3dzmix5lyrsaknqhr")))

(define-public crate-rs_elliptic_curve-0.1 (crate (name "rs_elliptic_curve") (vers "0.1.0") (hash "0kpfglskk3qv02d2nhnn28ixxqpny6xfaiq2lqvmkvxpfry1rdmg")))

(define-public crate-rs_elliptic_curve-0.1 (crate (name "rs_elliptic_curve") (vers "0.1.1") (hash "1flq7nwkhsg2c5zi7n42sy62kwn7bjak8w6ghqzwfy1znrgvkx0j")))

(define-public crate-rs_elliptic_curve-0.1 (crate (name "rs_elliptic_curve") (vers "0.1.2") (hash "16xi50mq38x9n5a382rrq40d03nxysgikppx6fpphhxp3vbz6nxg")))

(define-public crate-rs_envflag-0.1 (crate (name "rs_envflag") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fqypmivmiyy3cc01znayg3ll2pkxd4b1xyps58rrnzrh336g871")))

(define-public crate-rs_envflag-0.1 (crate (name "rs_envflag") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gk3nmg2jd66flhr3hm8n971lnkxmi79cwv3c7gy8vyx49is2d8s")))

(define-public crate-rs_envflag-0.2 (crate (name "rs_envflag") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kndgwdn31phw341vv251j8rb14v7nvwfln1a8wg3k3s9g25n17g")))

(define-public crate-rs_envflag-0.3 (crate (name "rs_envflag") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "078srkcdj8ic31c1dz5dkkb3j6i1kzhq2d92pyi7l7nh14q5hz0a")))

(define-public crate-rs_envflag-0.4 (crate (name "rs_envflag") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n2qirizmpg5mn6y4ff46npyi8z6cv6hbnwnr3pg86mzdf2l64p4")))

(define-public crate-rs_envflag_macros-0.2 (crate (name "rs_envflag_macros") (vers "0.2.0") (deps (list (crate-dep (name "rs_envflag") (req "^0.2") (default-features #t) (kind 0)))) (hash "10n7a287k3nyl0xxvviaiylhjfcs4wma6n2cqjwdyqrbpmm8f0cm")))

(define-public crate-rs_envflag_macros-0.3 (crate (name "rs_envflag_macros") (vers "0.3.0") (deps (list (crate-dep (name "rs_envflag") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wdq2gfy9df2vixnamnxbpd30yby0fkawjnknqvndrafp22r0872")))

