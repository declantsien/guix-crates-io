(define-module (crates-io rs -a) #:use-module (crates-io))

(define-public crate-rs-abbreviation-number-0.1 (crate (name "rs-abbreviation-number") (vers "0.1.0") (hash "1zyk1www1sr1hmbz3kjry25w71zmh0cp7xancm7jzb2g562pg8cj")))

(define-public crate-rs-abbreviation-number-0.1 (crate (name "rs-abbreviation-number") (vers "0.1.1") (hash "1vx0njcwzsnmgqr0p6wybx5jcqxngg4159rw8ivrn816ympz58zp")))

(define-public crate-rs-abbreviation-number-0.2 (crate (name "rs-abbreviation-number") (vers "0.2.1") (hash "096c139x76rwpr362b9vmgrdy67ann2dy77iy27d8p0ypq3jxbvm")))

(define-public crate-rs-abbreviation-number-0.3 (crate (name "rs-abbreviation-number") (vers "0.3.1") (hash "04lm488j280bzvl2nm2a9i7d2vq2snb1mmsjncgccj5m170zfi5x")))

(define-public crate-rs-aggregate-0.2 (crate (name "rs-aggregate") (vers "0.2.2") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.10") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clio") (req "^0.2.7") (features (quote ("clap-parse"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "ipnet") (req "^2.7.1") (default-features #t) (kind 0)) (crate-dep (name "iprange") (req "^0.6.7") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.1") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "00p9vxjarq19fw739lpq491ixrm672i0dk7s10d81brynkwzm7qy")))

(define-public crate-rs-aggregate-0.3 (crate (name "rs-aggregate") (vers "0.3.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.10") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clio") (req "^0.3.4") (features (quote ("clap-parse"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "ipnet") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.1") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1f2xmwnf7wlh422bxk6bsqn6r58dd459jswkcrn7lmd489ipzj0j")))

(define-public crate-rs-aggregate-0.3 (crate (name "rs-aggregate") (vers "0.3.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.10") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clio") (req "^0.3.4") (features (quote ("clap-parse"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "ipnet") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 2)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^3.0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)))) (hash "1fdjjbj0kfim1gr9snfwp0lnqcl03znyh7ijhvh27kz03gflx6f2")))

(define-public crate-rs-aggregate-0.3 (crate (name "rs-aggregate") (vers "0.3.2") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.10") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clio") (req "^0.3.4") (features (quote ("clap-parse"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "ipnet") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 2)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^3.0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)))) (hash "0dmwk4vhcbr8hy7sshxl5wkw6w54p0rpxp402vl44rama2j2c427") (features (quote (("default" "rayon"))))))

(define-public crate-rs-alloc-0.0.1 (crate (name "rs-alloc") (vers "0.0.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)) (crate-dep (name "rs-ctypes") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "05viwafhp5zr3hfni5pxh5fzlz31x166kfaw8j36clslil149x07")))

(define-public crate-rs-arboretum-0.1 (crate (name "rs-arboretum") (vers "0.1.0") (hash "0ldxa6jk51cbszd6kpigiks41qr9ji8q7k1yjq1b78f9m17vmfzx")))

(define-public crate-rs-args-0.0.1 (crate (name "rs-args") (vers "0.0.1") (hash "1225cdfmjvhwgl2awqwbminh6b0lcvn237x11qjgqamj6hz2crmi") (yanked #t)))

(define-public crate-rs-args-0.1 (crate (name "rs-args") (vers "0.1.1") (hash "0x8sp7dm5indiczc1nsc0b97j22ms31bgnk0shfy9ndcgk2hjwid") (yanked #t)))

(define-public crate-rs-args-0.1 (crate (name "rs-args") (vers "0.1.2") (hash "1inh8n07v6c2l1h5c876s3xnr1s3hn8nj0rbwksm5hajm5br2nmx")))

(define-public crate-rs-args-0.1 (crate (name "rs-args") (vers "0.1.3") (hash "025alca11m4bhxrxl4iggl1whlfrwv0g2bwlbcqdf1zkn4ijhq3b")))

(define-public crate-rs-args-0.2 (crate (name "rs-args") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "07l0fdwvb46d47wfav78yv05rs8pqciwy8cca5svcz9654l1jrx1")))

(define-public crate-rs-args-0.2 (crate (name "rs-args") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "1bhjd7xsmvk2f715jv88kjnqcbjjn6c6rapijq71qcz1kay3qnzs")))

(define-public crate-rs-args-0.2 (crate (name "rs-args") (vers "0.2.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "1fn7fx6smpg9b7ysqghfimhmpvd11swa58y8c5nkb6bk71l58bcs")))

(define-public crate-rs-args-0.3 (crate (name "rs-args") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "176w5cndp2xw68i4zi7km9k3jb7q7gi8khmvdwqlr2sfnva2z7vq")))

(define-public crate-rs-args-0.3 (crate (name "rs-args") (vers "0.3.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "1kzlsyfsjnlwd3kdrgv3avk2w4fk10jiwldmfrq091pl05cqhxlh")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.0") (deps (list (crate-dep (name "fsevent") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0y3jkk0z90q0hbk0pynacf58ccxapykcd0y128b2hhg2smbaw6q4")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.1") (deps (list (crate-dep (name "fsevent") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1n45l3s5cm2d3zcx9bvmg89abxk1v5virnm17wvxz99502hplzxh")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.2") (deps (list (crate-dep (name "fsevent") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "104n0k6q99if4j6j5x6jh6zmh84w54xyfkrxkhmhxjv48nn60kq4")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.3") (deps (list (crate-dep (name "fsevent") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0yxrwy9hbhdfhnqnib0r25ik626kd87887rcn1gis8rhd0zszbpj")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.4") (deps (list (crate-dep (name "fsevent") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1fq1pyzk9pm3y4vaxw8v5d60van6qw55qv3niawvv3ic8wxgn5i1")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.5") (deps (list (crate-dep (name "fsevent") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "050y2zsczydy7k7mzga79dgyf54pp4nwxaz9858vvs16qd0kr4cw")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.6") (deps (list (crate-dep (name "fsevent") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1ss38vl9wczdn0vs3vvgmyz7i4zvd8qva3xk7g6nck3kjy40nhza")))

(define-public crate-rs-auto-sync-0.1 (crate (name "rs-auto-sync") (vers "0.1.7") (deps (list (crate-dep (name "fsevent") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1cx534afdh3x5vqlws7nw98vx26y1mws1psqqlihxj1lb5jkjrvi")))

