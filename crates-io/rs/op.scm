(define-module (crates-io rs op) #:use-module (crates-io))

(define-public crate-rsop-0.1 (crate (name "rsop") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rpgpie-sop") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sop") (req "^0.7") (default-features #t) (kind 0)))) (hash "1g7ifh1ms9q3f9p09hankig64irc6ihnb8wllf4nshm8sr2s1rpg") (features (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.2 (crate (name "rsop") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rpgpie-sop") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sop") (req "^0.7") (default-features #t) (kind 0)))) (hash "0w30jmv98wkvf3dqnban9341ymril90mwl33a671328ifp6b1q8b") (features (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.2 (crate (name "rsop") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rpgpie-sop") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sop") (req "^0.7") (default-features #t) (kind 0)))) (hash "0riqsr8xwgmnvvdrs0nqcb0f9i7rrdizv4w9papwsmg4h6nicn0d") (features (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.3 (crate (name "rsop") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rpgpie-sop") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sop") (req "^0.7") (default-features #t) (kind 0)))) (hash "0dyravna9sy9gfp2d66g7k3wz143zvi205ifzf6xpwccdpahkv44") (features (quote (("default" "cli") ("cli" "sop/cli"))))))

(define-public crate-rsop-0.3 (crate (name "rsop") (vers "0.3.1") (deps (list (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rpgpie-sop") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sop") (req "^0.7") (default-features #t) (kind 0)))) (hash "19vjb3rzdqvn6j9p002z7s0n79w5qz11xxgrij3frrjcs8w7xqgl") (features (quote (("default" "cli") ("cli" "sop/cli"))))))

