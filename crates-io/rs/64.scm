(define-module (crates-io rs #{64}#) #:use-module (crates-io))

(define-public crate-rs64-periph-0.1 (crate (name "rs64-periph") (vers "0.1.0") (deps (list (crate-dep (name "volatile") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1qghw5mslqnw8hf1j23w0hi7kd9ks5x74nf57haddd56s5jyqb91")))

(define-public crate-rs64-rom-0.1 (crate (name "rs64-rom") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1w4v1f9b5k0sip3lwb5r2s98c8cm0vzk4xc45y85axxh6y92m3hb")))

(define-public crate-rs64-rt-0.1 (crate (name "rs64-rt") (vers "0.1.0") (deps (list (crate-dep (name "rs64-periph") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fcld7xchj29z5f40nxa48yipxmb6d5s0i7a8mybrsqfcl596qkh")))

(define-public crate-rs64romtool-0.1 (crate (name "rs64romtool") (vers "0.1.0") (deps (list (crate-dep (name "rs64-rom") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jwza5xcwqinnqmcag8jzksqh7s5y2sf70a0lakpjr6n7hc1sjy7")))

