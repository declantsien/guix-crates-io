(define-module (crates-io rs cr) #:use-module (crates-io))

(define-public crate-rscribe-0.1 (crate (name "rscribe") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde-pickle") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.10") (default-features #t) (kind 0)))) (hash "08dcj53gxgclm9yz34q1n28ajnla7ymzxzbz5b9g2r1358qgrl0l")))

(define-public crate-rscribe-0.1 (crate (name "rscribe") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde-pickle") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.10") (default-features #t) (kind 0)))) (hash "04ns52d5glzl174v4y8bh1kw447xgg7216l6iwd9kzz9jyv4a4zb")))

(define-public crate-rscript-0.1 (crate (name "rscript") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14j8lijyjvyksph4zk9v2xf4gxhhn7c52zj4h8d16m7cwmrbhmr0")))

(define-public crate-rscript-0.2 (crate (name "rscript") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vzhfv2n5rfiq7cc5rd49jhm4hvsa9i61dj1y6263rf80g9nj52m")))

(define-public crate-rscript-0.3 (crate (name "rscript") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05ir0d0rvangm2c2hsaigi8vwl3573najgfclq669hgayzbc9j9d")))

(define-public crate-rscript-0.4 (crate (name "rscript") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lwk2nk0r61f2z15w5sy1chsjkj09w8gz446kdxvvim016z1hbcm")))

(define-public crate-rscript-0.5 (crate (name "rscript") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1kv94qksqhgkzx00ns0fsbpgika7v83ml1dggw98nzdpyglmi1f7")))

(define-public crate-rscript-0.6 (crate (name "rscript") (vers "0.6.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pj20c28z5mwxcyymzaywp8ky4dzh6wmsbxxkqsrdya4k43h8bdv")))

(define-public crate-rscript-0.7 (crate (name "rscript") (vers "0.7.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0a70p6537kalrk3qvi8a7yblf7l3ccc3w53xqsg14vx4a4qsq5bp")))

(define-public crate-rscript-0.8 (crate (name "rscript") (vers "0.8.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1f192d39ki29vbry2hi9yf1wfggh78dnhdxm192kl1f9bykbhzq6")))

(define-public crate-rscript-0.9 (crate (name "rscript") (vers "0.9.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sklfkzzg1s2sylvi1phch45a2bymyr4k7fgl6yy70ikyf4bxfm4")))

(define-public crate-rscript-0.10 (crate (name "rscript") (vers "0.10.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "133sqjlms6wxz7j1fyifmrz8mp5nd1qwng9wbg6amp6wa39cs86r")))

(define-public crate-rscript-0.11 (crate (name "rscript") (vers "0.11.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qgg9z1nslj927bbx8k04b8fd28gf7jr23awj6jyh683ig5f9ypl")))

(define-public crate-rscript-0.12 (crate (name "rscript") (vers "0.12.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12q4m8vbvyh7r9jh959bd0265szmwlvc3i8iba6l8vil9p622jha")))

(define-public crate-rscript-0.13 (crate (name "rscript") (vers "0.13.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1yjn2i25vwm809gw2j2qngppcpj5k617gxmwch42iy3i2scjhp7w")))

(define-public crate-rscript-0.14 (crate (name "rscript") (vers "0.14.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12c267cqwzil3jrr95if1ahms3kx4dlngra2ba9csr907gn4hfvk")))

(define-public crate-rscript-0.15 (crate (name "rscript") (vers "0.15.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "037s9v7waj6j5akyv6k9115cdw4adfjklyb281bd2n1jmvn285nx")))

(define-public crate-rscript-0.16 (crate (name "rscript") (vers "0.16.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ysg7bknx0h8lycswwrs5agaj7hmpczd47dc1rvfx68aagmqpjp4")))

(define-public crate-rscript-0.17 (crate (name "rscript") (vers "0.17.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "152vp7g08zhsnfi6xbanc57pxiwai65s37d0q32xfl9w8qimx9vq")))

(define-public crate-rscripter-0.1 (crate (name "rscripter") (vers "0.1.0") (hash "02pq5g22x3y99zx2vj5giqjawwq3jbcx060w9fsbrgy829c1b1ds")))

(define-public crate-rscripter-0.1 (crate (name "rscripter") (vers "0.1.1") (hash "0ifpd7wghj8y9zdv3v3pk5g0vf6wk8pfjx2j4lzafnx5nrsmgzh1")))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1a8zx41zn6zfs445js6dw6acv79f95s7kr7xg47r3f4h1mk2584i") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0r5y5zs3b78cdha6psx8mwdz77fkwb8ww7kr2yp8caizaab2zrxv") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1bqj44wak7hygzvwwgsrgvcwi1ixhs173wgmxjdfmmx73s15fxkg") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "00r7v29ibnhgbjil181070w34411y892rykdn3xmx5nwnzqdkp20") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0dh82n04zsmc1p1hsgjnn97r6d3n3xrhx1p46c04br7m6f0dxlhb") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1h8qlw00n98z9c3ilyibfcmkml4lzw9dvr1ngz23d02j3y3kl0b3") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0q2sm05yk106mp3c5r9w8z0zk4gvv5a9428lkrv16naw1digxgc6") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1i4gjpw018kg2ym9hpix2p7vwfc5y8rxjrr4k2p98s7a4d642via") (yanked #t)))

(define-public crate-rscrypt-0.1 (crate (name "rscrypt") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0nssadld8afp93wfmvl6gyvcvlnpmpxga168356l9k0994v166c1") (yanked #t)))

(define-public crate-rscrypt-0.2 (crate (name "rscrypt") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1mlzj2vk2khq4kjk01kfc9spj3ybv6j1475g6ra9g9415gs6qdiv") (yanked #t)))

(define-public crate-rscrypt-0.2 (crate (name "rscrypt") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "072wwjcz5rpv03ddadylq3v1mmdywjjsny9m917qa5113jfjcmmf")))

