(define-module (crates-io rs au) #:use-module (crates-io))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.0") (deps (list (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "scrap") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "142mg7k69y8zkczr7pdcq5lnl4y3w6820xrz9z9283gz77rfdxvx")))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.1") (deps (list (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "scrap") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0237952b8ahl5jvbb0sw3xqchj4zdgcx1vjaswzd5b0xf32r05xg")))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.2") (deps (list (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "scrap") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1n4zviiba4jhipd6i3c2v6r7vb3w7s6yjdbg98hf46g25p8rb19m")))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.3") (deps (list (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "scrap") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1iixmfz9sw37hran6milc899dq5h4pyj20b02v08iq04za5v0vh5")))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.4") (deps (list (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "scrap") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1cqwv3dvc4kdijy1734vhnmydcs50zdrq853kzrwwha7sy0iz543")))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.5") (deps (list (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "scrap") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0w1kliwcslrh20y0p8j3brg231ssr7gc9kycj3dj1xwxasi7yvw3")))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.6") (deps (list (crate-dep (name "enigo") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "screenshots") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1nbkg8j6gpj8bz0li5h0v6n8038wqii3j94rcgbj5rsadmmj0ih7")))

(define-public crate-rsautogui-0.1 (crate (name "rsautogui") (vers "0.1.7") (deps (list (crate-dep (name "enigo") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "screenshots") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "034qwk7jvaygqdrrslmwmsxjxqvbc0hk8hc0a0a9qr8qz018lc81")))

(define-public crate-rsautogui-0.2 (crate (name "rsautogui") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "screenshots") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "winput") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1rgvcpwgny78cv920ygxhsr2r68gzgp3w1c3fi68q1zl66mf6yk8")))

(define-public crate-rsautogui-0.2 (crate (name "rsautogui") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "screenshots") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "winput") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1lk9rs4jqvdlc1l4ppay7zlqlgrbqkvcw07k2pqvv6ijhjlpzvpy")))

(define-public crate-rsautogui-0.2 (crate (name "rsautogui") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "screenshots") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "winput") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0zcqk2a6260yyc4fmjjlji1i9y99p1r9sf6paczcpijklpwh98ic")))

