(define-module (crates-io rs cm) #:use-module (crates-io))

(define-public crate-rscmm-0.3 (crate (name "rscmm") (vers "0.3.0") (deps (list (crate-dep (name "strum") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.23") (default-features #t) (kind 0)))) (hash "0wrk4lmwkrnvln68nd97iz2jvpcdgf4653a595dmdk77w0yj99iw") (yanked #t) (rust-version "1.57")))

(define-public crate-rscmm-0.3 (crate (name "rscmm") (vers "0.3.1") (deps (list (crate-dep (name "strum") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.23") (default-features #t) (kind 0)))) (hash "1gk6sf1vj1181f55j4dsvkb3pbl76i5vxbhlx7896ilv88fsdknd") (rust-version "1.57")))

