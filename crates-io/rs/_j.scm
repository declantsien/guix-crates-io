(define-module (crates-io rs _j) #:use-module (crates-io))

(define-public crate-rs_json2csv-0.1 (crate (name "rs_json2csv") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hard3bg7wfgzb67lbpqsipmdv7ki4xvkjs3kavib49khk2238ry")))

(define-public crate-rs_json2csv-0.1 (crate (name "rs_json2csv") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09l3a5g6gf7z941ji9ib2i4amrpyf4f24fm40nfzj4rznhn9njpd")))

