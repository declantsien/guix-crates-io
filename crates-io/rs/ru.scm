(define-module (crates-io rs ru) #:use-module (crates-io))

(define-public crate-rsruckig-0.1 (crate (name "rsruckig") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "float_eq") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.17") (default-features #t) (kind 0)))) (hash "0ms2vsc9ld64l0331aan13k4rjq24br4njqppvmkxxjha4p7ph71")))

(define-public crate-rsruckig-0.1 (crate (name "rsruckig") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "float_eq") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.17") (default-features #t) (kind 0)))) (hash "1fz9j6xk0306za5mmg2k0csws0h8gbhdjiynvlb9xgswbmpqkshh")))

(define-public crate-rsruckig-0.1 (crate (name "rsruckig") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1yfxqsf3n2i2zh4mwlszj0rw0yf0p5lsqc9va8qviq7jrwqx3fzk")))

(define-public crate-rsruckig-0.1 (crate (name "rsruckig") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1f0iyq4b2vmqqi6y3br54c02vyw1cgclgh10x52h7w33435l1qsn")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "17s9mqgp63y2258ipwqzlzjxma1k341ydzp78d95xhcib514w4dv")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1y2x51ick6y4qb1xz4vm725lrvzyqbwkihpn9clnxfr9rxi2amr6")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.0.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "13kghn987wr9ajz4yl6xjaggf7595w903q804ib7hc8rg2qq15yw")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1gnw2zxxg5hr96x6jff93zndlsc01rhylv1baf2q2694839hcl0w")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "05xi41s3gs8wl33vsh3lqzzv8pa4p5id4jxfpy29fr5llqd1mfd7")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "06yjry6vr1cs5b6xyywq5928w9k5i6nac6y29k7gw29vr032q4s2")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0gsmrcw2cyiayrk1pczwil91qsijipgzk1bb9rjf8lf0yfkdkh71")))

(define-public crate-rsruckig-1 (crate (name "rsruckig") (vers "1.1.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "13skwlbivd4xxp38j809g018rkzvcp9jj9j64b0bczx1y8x0cz02")))

