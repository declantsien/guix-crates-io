(define-module (crates-io rs _g) #:use-module (crates-io))

(define-public crate-rs_geometer-0.1 (crate (name "rs_geometer") (vers "0.1.0") (hash "0dbz36qwgh6xgiadv0w5m97kl82j7wk9411ksl5ls89avig91rmw")))

(define-public crate-rs_gost_28147_89-0.1 (crate (name "rs_gost_28147_89") (vers "0.1.0") (hash "0dlc6i37svdhj9dj682496izc56md9j267rckkq8m57acbjpvkkn")))

(define-public crate-rs_gost_28147_89-0.1 (crate (name "rs_gost_28147_89") (vers "0.1.1") (hash "0lzz3aqdlgvxnhk222inw9f3289zdfn4n1w73kgb4xz4vyp4b1sf")))

(define-public crate-rs_gost_28147_89-0.1 (crate (name "rs_gost_28147_89") (vers "0.1.2") (hash "00160v1rvjjl3df3f8fqyvbdnnp7xk85yi2qbqvn00v6kf6bac7n")))

(define-public crate-rs_gost_r34_10_2001-0.1 (crate (name "rs_gost_r34_10_2001") (vers "0.1.0") (hash "1hqca53ffx56k9j2gqncqq5x3rrkbiz7zjcrkqry7rhh5k0z9hls")))

(define-public crate-rs_gost_r34_10_2001-0.1 (crate (name "rs_gost_r34_10_2001") (vers "0.1.1") (hash "0dwsbs3hfsyg0ccb8q2gcib3l41mx4q0s1fnmfzrps633jn1cmsk")))

(define-public crate-rs_gost_r34_10_2001-0.1 (crate (name "rs_gost_r34_10_2001") (vers "0.1.2") (hash "1rbcvxandsh2x17g7wfyg6pqarww8nsrl8qipvhgxz3dmmm58qfx")))

(define-public crate-rs_gost_r34_11_94-0.1 (crate (name "rs_gost_r34_11_94") (vers "0.1.0") (hash "1xkr7s81kypkfl4yyal0ias9drz57zl0c0x8rk737mqclrcagszi")))

(define-public crate-rs_gost_r34_11_94-0.1 (crate (name "rs_gost_r34_11_94") (vers "0.1.1") (hash "1c24jhnzn6pz965qmswh22d021sxg694c5y2svxf4agn0x7rlh7x")))

(define-public crate-rs_gost_r34_11_94-0.1 (crate (name "rs_gost_r34_11_94") (vers "0.1.2") (hash "01hfsvybj5pcbih1m8v2bmri7v3ijffx7ihvysq0pv2hr2pwxvih")))

