(define-module (crates-io rs _d) #:use-module (crates-io))

(define-public crate-rs_des-0.1 (crate (name "rs_des") (vers "0.1.0") (hash "1xsrj6kjk1njq51cmlln0wxg4c87r69ms5d7yin6y3jabf7iynb4")))

(define-public crate-rs_des-0.1 (crate (name "rs_des") (vers "0.1.1") (hash "1h7g54giqw1xi8ppyr0mc7alb8mkmn02di38v24w2k4xhxipsmlm")))

(define-public crate-rs_des-0.1 (crate (name "rs_des") (vers "0.1.2") (hash "1iimwzmlii2dmr47anlzd5i1s6z3d2ckfwzlpz0vvlq34sza023s")))

(define-public crate-rs_diffie_hellman-0.1 (crate (name "rs_diffie_hellman") (vers "0.1.0") (hash "1ncb60rf3l18fnzr8k10czam8r7fvlpygcnvkxs99ajggv33bkg0")))

(define-public crate-rs_diffie_hellman-0.1 (crate (name "rs_diffie_hellman") (vers "0.1.1") (hash "19bjxckql093hc060jnkd5xgd52n9jpzzqcbb8nl8f84bh4f2n1b")))

(define-public crate-rs_diffie_hellman-0.1 (crate (name "rs_diffie_hellman") (vers "0.1.2") (hash "1dqh63qkir32dx23qglgmyn21s7ry2rs56p2gpay739xrmhjiyap")))

(define-public crate-rs_dsa-0.1 (crate (name "rs_dsa") (vers "0.1.0") (hash "0j83mz6x5sqq4jsjpxs7ivq2x8jhqzzcz2cmfg22s306i9jva71n")))

(define-public crate-rs_dsa-0.1 (crate (name "rs_dsa") (vers "0.1.1") (hash "1dbmi1dypk316vq2na6hp2fbd2qlpr1dlhjl8i7s4bd5mhli4j2k")))

(define-public crate-rs_dsa-0.1 (crate (name "rs_dsa") (vers "0.1.2") (hash "1kf8q9a55znfvgv0c37yyxfwiqpwsrma0r0wk5jcg6slj37vp5m0")))

