(define-module (crates-io rs er) #:use-module (crates-io))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.0") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y8zisjzvq0sd6fhjiw838881mb9l2w5i7rnr174wn52mmj4d0p5")))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.1") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "025k3hazhglbmmi89avyphvfidg1zplfv4302i6g2kybvcg4znn2")))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.2") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "15ha2519yq27ifxhydb8yq71rys43j8yiyj72ffnix55cv4if81g")))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.3") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1yffbpxrh6p8kjirsgy9zqqh2j4kfin1j525p543sjc8pi33zg3n")))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.4") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0561aascbcr6d07svy8s7jd184fkwr0jqzfiiaf1nqs5c1vlj4z5")))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.5") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "find_port") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0jlg8ilcnmwjxwrjmyabwfjcm7wyg0is63pbcq7g4b5ysj6rbbas")))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.6") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "find_port") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1d3wf3bkvw7bns9cpw9ifzhlv7d54ghswrxf7pgzskv7fh4fdq51")))

(define-public crate-rserve-0.1 (crate (name "rserve") (vers "0.1.7") (deps (list (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "find_port") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "13awlkwr00khfxndcf52nsni27gsibk6gdq0iavgs3hqwpsk7bm0")))

(define-public crate-rserver-0.1 (crate (name "rserver") (vers "0.1.0") (hash "1hb9gq94ds1pnwdw7c4399fwajw3j502q7f9fb70pgsjc0f9b8fs")))

(define-public crate-rserver-0.1 (crate (name "rserver") (vers "0.1.1") (hash "0ibwfw699dddb47js3sk96p950qqldcrifc13p8zlzwcrqwvhgzz")))

(define-public crate-rserver-0.1 (crate (name "rserver") (vers "0.1.2") (hash "0blh4nlxizh0j1p9fi5p924h44x8hi2ph47jf9dr06alx5fh3mq9")))

(define-public crate-rserver-0.1 (crate (name "rserver") (vers "0.1.3") (hash "0m9d07cbmbqjdipmz16mal0b53cg9ak5dawb1phy79yqfw463hm7")))

(define-public crate-rserver-0.1 (crate (name "rserver") (vers "0.1.4") (hash "1wf3bb7f01sjc0jx601rxc05ahkhnkyi8qp802zp1drm91g222h2")))

(define-public crate-rserver-0.1 (crate (name "rserver") (vers "0.1.5") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urlparse") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "06qj3wa89acp0njk0c74nhbpvbfawqylirzxydvjanrvw25p8yzr")))

(define-public crate-rserver-0.1 (crate (name "rserver") (vers "0.1.6") (deps (list (crate-dep (name "futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "tokio") (req "^1.21.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urlparse") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0svamvsqm4kd3a80lr78n3v3d12lcgf6v6bzhnymjd6pm7jgzc66")))

