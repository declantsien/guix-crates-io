(define-module (crates-io rs pl) #:use-module (crates-io))

(define-public crate-rspl-0.1 (crate (name "rspl") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1zxffc9syac95pvsxfp588wfy7gcbmrvk23g4l31p54qmjlk69sv") (features (quote (("std" "crossbeam") ("default" "std"))))))

(define-public crate-rspl-0.1 (crate (name "rspl") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1q3ldjr7rgw9gynr220xk006ivkwn2fyvg0myxs8drxrlbm7m3f9") (features (quote (("std" "crossbeam") ("default" "std"))))))

(define-public crate-rspl-0.1 (crate (name "rspl") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1c8dxph3k9s0yvnway4y5rwcmx8ps52jn79fg33sn8g6dd56k5x3") (features (quote (("std" "crossbeam") ("default" "std"))))))

(define-public crate-rsplit-0.1 (crate (name "rsplit") (vers "0.1.0") (hash "00m5ysjsyfh10k21z1hyn7dr4cdvq94xxnrsjxqs1qmslb44cnj5")))

(define-public crate-rsplit-0.1 (crate (name "rsplit") (vers "0.1.1") (hash "1x90f5fjpvy2aizfa4qfxnmhxfph4l6yhm6zddfn94qc2ci0r08w")))

(define-public crate-rsplit-0.1 (crate (name "rsplit") (vers "0.1.2") (hash "19n948l3qfk4d6z1brxndwgwijiq7gaj4ig24q4sp7p6y5382ksd")))

(define-public crate-rsplitter-0.1 (crate (name "rsplitter") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1wz81ww0x9z21hl5lql29f4yr241zlj68pgqi1w0xa0cp0yx024y")))

(define-public crate-rsplitter-0.1 (crate (name "rsplitter") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "04izp13nlxg0zcbvkclysc3k1d58x72a9zdvyhqr2apnxwxhaq2s")))

(define-public crate-rsplitter-0.1 (crate (name "rsplitter") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "01fih05bp14labk357xhvgw1pi6hkx1y5akpsahh0xzqw82n6nz6")))

(define-public crate-rsplitter-0.1 (crate (name "rsplitter") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1alzl5777nfi64jifawq991z67c950yfywac2z89zxz7pr1f3mlk")))

(define-public crate-rsplitter-0.1 (crate (name "rsplitter") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "14x0s8fjh0rp4f0q06bncabzhqn6sj2bgb0iv67m6c3g8y058rly")))

(define-public crate-rsplitter-0.1 (crate (name "rsplitter") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0974xvzjfjkigyrdb36blcjdcf2v6sf563a7inb2xgswlk049y85")))

(define-public crate-rsplitter-0.2 (crate (name "rsplitter") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1zgswp8rd5mjavgg257apfghhycshvzwbcikry0smhggd4zwfpws")))

