(define-module (crates-io rs ra) #:use-module (crates-io))

(define-public crate-rsrandrd-0.1 (crate (name "rsrandrd") (vers "0.1.0") (hash "0vvgsc1qz750ywpv27b9m55ki3x6knigdy9mpiqi139xdra0p5qk")))

(define-public crate-rsrandrdrs-0.1 (crate (name "rsrandrdrs") (vers "0.1.0") (hash "1hpm426404psm54l3zw80ccavrxlwgdbwh11qcdgi5gxvlwyzrlh")))

