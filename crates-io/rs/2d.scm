(define-module (crates-io rs #{2d}#) #:use-module (crates-io))

(define-public crate-rs2d-0.1 (crate (name "rs2d") (vers "0.1.0") (deps (list (crate-dep (name "glassbench") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reed-solomon-erasure") (req "^3.1") (features (quote ("pure-rust"))) (default-features #t) (kind 0)) (crate-dep (name "solana-merkle-tree") (req "^1.15.2") (default-features #t) (kind 0)))) (hash "1lrlbvc4grbfl5cq9drgdjav6v0bi0ngf2a2kjhp6m701adb678k")))

