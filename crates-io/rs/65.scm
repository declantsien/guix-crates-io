(define-module (crates-io rs #{65}#) #:use-module (crates-io))

(define-public crate-rs6502-0.1 (crate (name "rs6502") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gciqg42gcd2m2jizpylp6caavmazhwdak8qg03pbmbh3032va4c")))

(define-public crate-rs6502-0.2 (crate (name "rs6502") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "1sf92fyg0qkixrgymm4iwlbkli4f2xs788l61lvdabc7z88jhm8w")))

(define-public crate-rs6502-0.3 (crate (name "rs6502") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0plsvh6ngygknjvwizyzdm5lm81rrw1h7kabh32w2m3x0rlp5znc")))

(define-public crate-rs6502-0.3 (crate (name "rs6502") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "07xfz409h9llndx06m07k5g8dn4zx7p6yvfmfa9vns9yz02c96xp")))

(define-public crate-rs6502-0.3 (crate (name "rs6502") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gqcrjy01mbnfhhrxrnq14qfj7hn7g39p7gxs045syxk87slrfyf")))

(define-public crate-rs6502-0.3 (crate (name "rs6502") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "11sqnvpjn38h7hhv279vsa6s5hjjdqr693byvana6qlqa3nk5kbb")))

(define-public crate-rs6502-0.3 (crate (name "rs6502") (vers "0.3.4") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gx1mr7b1432j383fnxysjcpmh4m4lk38kddwxrqb4r62zx4ky8g")))

