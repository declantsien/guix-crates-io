(define-module (crates-io rs nd) #:use-module (crates-io))

(define-public crate-rsndfile-0.0.1 (crate (name "rsndfile") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1a1v272d5rpkmfwsxrpg34q6yi81j1lijdh4v3x1sa55pm1b4n9z")))

(define-public crate-rsndfile-0.0.2 (crate (name "rsndfile") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0w399nblwhzfgplbph6qyyz3rqrjhbq979x1ia2vs9ravj6scksr")))

(define-public crate-rsndom-demo-0.1 (crate (name "rsndom-demo") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0lq73cm1692ki7vqmj7lc5smrgdr2w3kz7ihrb7iglsfqgcc45qx")))

(define-public crate-rsndom-demo-0.1 (crate (name "rsndom-demo") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1xchhamr3kkppwbnq6yp5xbbksgg018b3w0ki7xvnyq6jrsvg8bf")))

