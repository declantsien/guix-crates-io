(define-module (crates-io rs l1) #:use-module (crates-io))

(define-public crate-rsl10-pac-0.0.1 (crate (name "rsl10-pac") (vers "0.0.1") (deps (list (crate-dep (name "bare-metal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1d92qcppbqmx1nb7gldvxq3lcvk0yq8347acdxfi4giwgzlyq3f1") (features (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

(define-public crate-rsl10-pac-0.0.2 (crate (name "rsl10-pac") (vers "0.0.2") (deps (list (crate-dep (name "bare-metal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "cortex-m") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fsyfq74g7iwdxnl9p9pcfd7axlzbd8wdc4hj5bibf658jvfbhmq") (features (quote (("rt" "cortex-m-rt" "cortex-m-rt/device"))))))

