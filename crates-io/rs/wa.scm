(define-module (crates-io rs wa) #:use-module (crates-io))

(define-public crate-rswap-0.0.1 (crate (name "rswap") (vers "0.0.1") (hash "0fxwkrp2g24k4gv1y8npmhybwh26xx43ylk6j18ddkl5ssq5qhna")))

(define-public crate-rswatch-0.1 (crate (name "rswatch") (vers "0.1.0") (deps (list (crate-dep (name "libcli") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "09rb1zfs0v3iscvd150i49bcnkxbwmnfy98ayzh3qf0hy1yhqscx")))

(define-public crate-rswatch-0.1 (crate (name "rswatch") (vers "0.1.1") (deps (list (crate-dep (name "libcli") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "13rbd1wpsq37gwwc0j1jwk49xd5m6dxwyjldvfn0fgky1n2977cr")))

(define-public crate-rswatch-0.1 (crate (name "rswatch") (vers "0.1.2") (deps (list (crate-dep (name "libcli") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "02jzn7f4df65wlb10gqmcvxxb3f6vw88n3d87vgfnc9ijhsiqbp2")))

(define-public crate-rswatch-0.1 (crate (name "rswatch") (vers "0.1.3") (deps (list (crate-dep (name "ctrlc") (req "^3.1.4") (default-features #t) (kind 0)) (crate-dep (name "libcli") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0nbl59j18grydlxcdj6m77f8xc40q4zfczya9mp85jhj041lm59h")))

(define-public crate-rswatch-0.1 (crate (name "rswatch") (vers "0.1.4") (deps (list (crate-dep (name "ctrlc") (req "^3.1.4") (default-features #t) (kind 0)) (crate-dep (name "libcli") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "00nj1kfra4iyhvw9l0lbw1pzrm58ffpfb0b8fm69d2sg429jhav7")))

(define-public crate-rswatch-0.1 (crate (name "rswatch") (vers "0.1.5") (deps (list (crate-dep (name "ctrlc") (req "^3.1.4") (default-features #t) (kind 0)) (crate-dep (name "libcli") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1zz7zgln3b76jz57iy3i48zl2ljzpvjk4ygk32zbfyb93j7g7kga")))

