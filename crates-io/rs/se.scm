(define-module (crates-io rs se) #:use-module (crates-io))

(define-public crate-rssettings-0.1 (crate (name "rssettings") (vers "0.1.0") (hash "0lla26l3n13g6yfzy20fr3ggvii9ixnq28bj4dpn7a0fzjqvyxq5")))

(define-public crate-rssettings-0.1 (crate (name "rssettings") (vers "0.1.1") (hash "10v7qjgyfbkk9fzxwqn74nc2l1s1pmvaiqwxql7dpi99q25bsxz8")))

(define-public crate-rssettings-0.1 (crate (name "rssettings") (vers "0.1.2") (hash "158nhb9ib9518yz367vq8zq5h2nhbvxdd4cigx3qkl5bsy9f98d5")))

(define-public crate-rssettings-0.2 (crate (name "rssettings") (vers "0.2.0") (hash "02whzgr8hqyc3nyjp0ab2yvrskp0xfhd36lacjymxq5miwh83rcr")))

