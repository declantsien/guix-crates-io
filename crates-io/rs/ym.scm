(define-module (crates-io rs ym) #:use-module (crates-io))

(define-public crate-rsymtab-0.1 (crate (name "rsymtab") (vers "0.1.0") (deps (list (crate-dep (name "rsymtab_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "033wgzwwlh9qjirhcpa9a28whcflh8kicgjz3r6w39kxvqq09p3i")))

(define-public crate-rsymtab_macros-0.1 (crate (name "rsymtab_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p1v4dc80xlqz11lfc8xrhmiv30yg3fpvbqwmg4pvai74cwsa8hj")))

