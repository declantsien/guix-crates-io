(define-module (crates-io rs na) #:use-module (crates-io))

(define-public crate-rsnappy-0.1 (crate (name "rsnappy") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6.67") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.11") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "0c138l7qf5g50m8pldnq5c1fv2wgiliad2l53cw0l5dp8604yqi5")))

