(define-module (crates-io rs js) #:use-module (crates-io))

(define-public crate-rsjs-0.1 (crate (name "rsjs") (vers "0.1.0") (hash "0pdk8qm9rmb9nknkqky07dsk4mdbdwmzifv6wi4yjsffklrrmdzw")))

(define-public crate-rsjs-0.1 (crate (name "rsjs") (vers "0.1.1") (hash "0v362c9zmk029d5n4akgq9n8g3jz5jnn6pmh8n9sn3rrik3mrx49")))

(define-public crate-rsjs-0.1 (crate (name "rsjs") (vers "0.1.2") (hash "0yjlqx7ybm30k1h94swbykcgab7ykdjajri4nfbmbkmzaa7sln00")))

(define-public crate-rsjson-0.1 (crate (name "rsjson") (vers "0.1.0") (hash "0wxyxy70fvbk73pv4q6rf8751i5v12s15r0ry7hwc0jhlqc8bal4")))

(define-public crate-rsjson-0.1 (crate (name "rsjson") (vers "0.1.1") (hash "1szr4swjb5h40rwgxzlr6p4giaksxpirx9p5hsbbysh6v5mxqbc4")))

(define-public crate-rsjson-0.1 (crate (name "rsjson") (vers "0.1.2") (hash "0b124sg4v2h493fhvc53akdds77728f5h9l8kivnms8lqv40ckil")))

(define-public crate-rsjson-0.1 (crate (name "rsjson") (vers "0.1.3") (hash "1mslcmr8firnaavjbm1qvsbdyia695x65v6dsls2ak2nl9w8b7ys")))

(define-public crate-rsjson-0.1 (crate (name "rsjson") (vers "0.1.4") (hash "0f641482ra0y21qv77sqag1kkj2nklz347mjnlyd5sab1kaxwzb4")))

(define-public crate-rsjson-0.1 (crate (name "rsjson") (vers "0.1.5") (hash "17wzijvhr6xrkipspsbww652dq0vpz4k8gapsn76gbw42shv6j75")))

(define-public crate-rsjson-0.2 (crate (name "rsjson") (vers "0.2.0") (hash "0mj4xswc0y1bbv6lg5955irj26997cqf4023p4nl6i7h5k0vs8zr")))

(define-public crate-rsjson-0.2 (crate (name "rsjson") (vers "0.2.1") (hash "1phdbxrksi2ljwkb6z3fnbhal18m1di4cplh2kmvbfggxhk6d1mc")))

(define-public crate-rsjson-0.2 (crate (name "rsjson") (vers "0.2.2") (hash "03h0jlv6586yhskkhlv6vq51yv37313bb7ys1f1ryp51hszvqdlv")))

(define-public crate-rsjson-0.3 (crate (name "rsjson") (vers "0.3.0") (hash "0vq0f8vb1cfvyszvfh28z2k5d9x5rpljwl2wh087i9ycb9v20b1g")))

(define-public crate-rsjson-0.3 (crate (name "rsjson") (vers "0.3.1") (hash "0q726pzdl3sm62hdfgimcyx0pl2f68rcmpmvn4mzd9ad2q28hq9i")))

(define-public crate-rsjson-0.3 (crate (name "rsjson") (vers "0.3.2") (hash "0yyqrbsmn6byrcvjld57qjbacs1n1091ahh0m4ksfp88gl47808a")))

(define-public crate-rsjson-0.3 (crate (name "rsjson") (vers "0.3.3") (hash "17n4ak6hzziw5a8mxz5n4waq8v95rgkz7gjajbb7zvslz1bfxwgi")))

(define-public crate-rsjson-0.3 (crate (name "rsjson") (vers "0.3.4") (hash "1q6gi4hivjkhi8p67s8v6yanavb3fm0mld728fkib13sdvr922px")))

(define-public crate-rsjson-0.3 (crate (name "rsjson") (vers "0.3.5") (hash "0kkk2k2h7gnhacf1q93lbahrl0pwm8fzwzycpy0lnxxpads5w86r")))

(define-public crate-rsjson-0.3 (crate (name "rsjson") (vers "0.3.6") (hash "0sxpyq8wvj86qk3zij760wsn2lb9klcs229imfksh4dyd6f0cniw")))

(define-public crate-rsjson-0.4 (crate (name "rsjson") (vers "0.4.0") (hash "1gjjdvdwy3d6dqgyzf8jj9vgjwanvr7hnfydll6zql786k6iph1c")))

(define-public crate-rsjsonnet-0.1 (crate (name "rsjsonnet") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libtest-mimic") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rsjsonnet-front") (req "^0.1.0") (features (quote ("crossterm"))) (default-features #t) (kind 0)) (crate-dep (name "rsjsonnet-lang") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)) (crate-dep (name "unified-diff") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "0551r6faf6ghbg6ai9a8rdyi63p2v44y4x20gjbpm4igbgxw6fzq") (rust-version "1.74")))

(define-public crate-rsjsonnet-0.1 (crate (name "rsjsonnet") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libtest-mimic") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rsjsonnet-front") (req "^0.1.1") (features (quote ("crossterm"))) (default-features #t) (kind 0)) (crate-dep (name "rsjsonnet-lang") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)) (crate-dep (name "unified-diff") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "03l57qnlbm34920c4xc5rpf51fwjmc87pwjycvdsdcw5zg6vw929") (rust-version "1.74")))

(define-public crate-rsjsonnet-front-0.1 (crate (name "rsjsonnet-front") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rsjsonnet-lang") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sourceannot") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "02zjgmdqf1jk64c21k9xgi94y32jhm1gxrxv27xipf8fhh8vlmzq") (features (quote (("default")))) (rust-version "1.74")))

(define-public crate-rsjsonnet-front-0.1 (crate (name "rsjsonnet-front") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rsjsonnet-lang") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sourceannot") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0p0wmpm91gw9axzwd0777826mqi0axs5azlblrciqy5n0ypshcmm") (features (quote (("default")))) (rust-version "1.74")))

(define-public crate-rsjsonnet-lang-0.1 (crate (name "rsjsonnet-lang") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "libyaml-safer") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.6") (kind 0)))) (hash "1wgbs1fz7rzzjyd5yi4h5znqmxdqxx1kyx2jzqrsqg1gnvph6y20") (rust-version "1.74")))

(define-public crate-rsjsonnet-lang-0.1 (crate (name "rsjsonnet-lang") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "libyaml-safer") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10.6") (kind 0)))) (hash "0rnsxxnzg1454farfdnjjzmri41krix98ga9746f4gg4jh5w2xhp") (rust-version "1.74")))

