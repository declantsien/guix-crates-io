(define-module (crates-io rs fl) #:use-module (crates-io))

(define-public crate-rsflex-0.2 (crate (name "rsflex") (vers "0.2.3") (deps (list (crate-dep (name "termion") (req "^1") (default-features #t) (kind 0)))) (hash "19l0av4ncwcbmyydr214j0gkv4986gznczdfb1bx6ldr3q2mjs53")))

(define-public crate-rsflex-0.3 (crate (name "rsflex") (vers "0.3.0") (deps (list (crate-dep (name "cmd_lib") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "mimalloc") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1ib03szi5hy0wqhw2mfiwz1zp5znqwgls3cd2dpqrxllrvp5l90k")))

