(define-module (crates-io rs #{-3}#) #:use-module (crates-io))

(define-public crate-rs-303-0.0.0 (crate (name "rs-303") (vers "0.0.0") (hash "1n3lw00kqjv1yx4gdzdcvvryglxynq2nvyh3kmw8xhv2rf6cxz98")))

(define-public crate-rs-303-0.0.1 (crate (name "rs-303") (vers "0.0.1") (hash "1wkhqzvm3kc4v2mxz6z4qfbf0b1g3plf1c5b7mkw716zz47mb9jg")))

