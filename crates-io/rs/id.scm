(define-module (crates-io rs id) #:use-module (crates-io))

(define-public crate-rsid3-1 (crate (name "rsid3") (vers "1.0.0-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 1)) (crate-dep (name "id3") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 0)) (crate-dep (name "vergen") (req "^8.3.1") (features (quote ("build" "git" "gitcl"))) (default-features #t) (kind 1)))) (hash "0vxmfndvvm7d6iwbvagz4yr0isihznylagl4hi1m7rcqg5jihplw")))

