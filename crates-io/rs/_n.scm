(define-module (crates-io rs _n) #:use-module (crates-io))

(define-public crate-rs_n_bit_words-0.1 (crate (name "rs_n_bit_words") (vers "0.1.1") (hash "0a3a93jkxaskwaqpgdrdbg7viyjfv163rcm789n1srh9csl82zl1")))

(define-public crate-rs_n_bit_words-0.1 (crate (name "rs_n_bit_words") (vers "0.1.2") (hash "12cvcs46k8ghc9rpyxr7cpdrz2ip5gjn9zcpx46vwni8wap1r6xd")))

(define-public crate-rs_n_bit_words-0.1 (crate (name "rs_n_bit_words") (vers "0.1.3") (hash "1dlwv1anfww8sk16dj9gwyk0y8ay0ssp1fp666rpc3x6qasbphcb")))

