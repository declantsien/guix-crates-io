(define-module (crates-io rs -k) #:use-module (crates-io))

(define-public crate-rs-kdbush-0.2 (crate (name "rs-kdbush") (vers "0.2.0") (hash "1wydhlhq0rk6hz5xf8v301mmn7198b325l3n4k85g39k4v49pqpf")))

(define-public crate-rs-keycloak-0.1 (crate (name "rs-keycloak") (vers "0.1.0") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1nnrm2bjqgxrhzh1zgwx93zzl2zc3hyingjmjzrkmmlzfx8bc4lb")))

(define-public crate-rs-keycloak-0.1 (crate (name "rs-keycloak") (vers "0.1.1") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1c6bpbpf8jfbqz69g125sw8apaf45f665yzq6g62jsd8ww27i88j")))

(define-public crate-rs-keycloak-0.2 (crate (name "rs-keycloak") (vers "0.2.0") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "17xkfi44krd476rk8fa27n3g1lq303ckgqi7szsh8g20xmc1ma8m")))

(define-public crate-rs-keycloak-0.2 (crate (name "rs-keycloak") (vers "0.2.1") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1v1d3mad0i2v8r4pwpd6rflybf8sq9zqqym38ya8r3i72j4jyc08")))

(define-public crate-rs-keycloak-0.2 (crate (name "rs-keycloak") (vers "0.2.2") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0c73r7j79i4gx3jrbnkafsb2d0pmpgi4v94c31ya04k2w18jgmgd")))

(define-public crate-rs-keycloak-0.3 (crate (name "rs-keycloak") (vers "0.3.0") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1fgc1hjv2h9dh38f1nz8c0kjkkxmcb7awjc0sccm35hrgcvf8k97")))

(define-public crate-rs-keycloak-0.3 (crate (name "rs-keycloak") (vers "0.3.1") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0ma5917iqxwj9zrisr9dlrvpsbi18isrisjy37fzir8zm496hp5x")))

(define-public crate-rs-keycloak-0.4 (crate (name "rs-keycloak") (vers "0.4.0") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1dknj00y6q0xdkfwvg5g6i93xd9x94dd37rjjgadvr9k2hkvz255")))

(define-public crate-rs-keycloak-0.4 (crate (name "rs-keycloak") (vers "0.4.1") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0sc23ycd6c2bw6v4j9midxr6bwinfhrsr3z0wg568ji60ifhvdjv")))

(define-public crate-rs-keycloak-0.4 (crate (name "rs-keycloak") (vers "0.4.2") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0c2gqa3al8hmg2dabsiii88md5sr1qkw6f557x7vv4c9ri553f89")))

(define-public crate-rs-keycloak-0.4 (crate (name "rs-keycloak") (vers "0.4.3") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1kl8myplyc3v68z1wzwxxg6v2h2gl60sx9crbxk86m2hy16p8m56")))

(define-public crate-rs-keycloak-0.4 (crate (name "rs-keycloak") (vers "0.4.4") (deps (list (crate-dep (name "minreq") (req "^2.8.1") (features (quote ("punycode" "json-using-serde" "https-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "152ipnm6r02nc3bjjj55l8r4c4ljic5vrbga56si8qlplxwdxvjp")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.0.1") (hash "1v71dkhi2hfalxz9a759xslj4qch9n9wsbiwr5s6zl9c0w4ashpr")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.1.0") (hash "05yvljb4ixwi8q3mx5hzbpal2341qh7h8d13r655yyv73s5v4c8d")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.2.0") (hash "0yw97wfdd37yarspknvix60v8mvrv7232cqwzfv58wvv7nj60g7w")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.3.0") (hash "0s56023i8d86d0lpyxr3h8k9q62w3z5l9df9al2xhls3ls3z7iv5")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.4.0") (hash "1alnn0c6dhsbvg6r97p286h13njhs51gmig44aqd055yik0awp4d")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.5.0") (hash "0r2klznvdsj5kxf858hv0zfzfxk8sqjlyf93mp0mzjy092mzw238")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.6.0") (hash "0qcv54q4bc7wmjy4v1mvnf2afix5rkzsmdn71nciq6dwn3x77ydr")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.7.0") (hash "1snzgn8gqh54mwjq6jzadwsc5382cxi0jmv17y8rkz20k0xcv5ly")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.8.0") (hash "18ibch53n9xza6w7j8gcp98d9fdihfxc6axzzjqd2n66yssv3nbn")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.9.0") (hash "0wwxhspc2ljxzhsic3ia6w1klyak0nd1arkw5na5iza6kgaba4dy")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.10.0") (hash "0vkql9ji8qmg3r6czgrv0awpm0bcc7bbxm4hb8md0r2p5r9n2jzf")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.11.0") (hash "1cpffnlxxpdix4v7f4jsrvb8f05n78p69qp18gsi3ym82f7yj340")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.12.0") (hash "0hj7x2wh6486vmcsp6hgz6wis5f7i7w2mxgdbigg4x4jafl0hjhs")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.13.0") (hash "1jcp5ggg3gbfwxalkv5x2h7919pszrz8wl9r88jfwhgjja13sijg")))

(define-public crate-rs-kv2spacetimedb-1 (crate (name "rs-kv2spacetimedb") (vers "1.14.0") (hash "1j3zz8f09fqkw349gnynqz9jjrnbsyxigdx6s3aaig1nm60p4hzb")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.0.0") (hash "1a59m218d99n5p2crzxi5ip11ing3c5k14kp4g0zidxdds56iaz6")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.1.0") (hash "15811z8h57b9qjgv0lnsnf99pn6grpirz2c0snkzlb1i6ckpvz77")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.2.0") (hash "0r5jnpz87xndfsqp7i6aa07bl7r7s9rwxpx47q5hmy9i97sn47ap")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.3.0") (hash "1877zi70fh2mkxisfwygh34rfal8l1zbf1528zxqdfc9jvg2snxl")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.4.0") (hash "1km9rky60p8fjxrhv51wrpxkvaw7nw822jq1p6ispysgynajid65")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.5.0") (hash "09a7a3llx8x8lgvhhqc29r3ccc4gvyp1j4j9673i6317xj0qa52s")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.6.0") (hash "0py3gwkc9z1mjsgf8k6qs9nc73vpvpsbbwm4i9kbzfvlh6rki49p")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.7.0") (hash "1yk5aq7xyys8ndj0npmx0arq4ha9xf31ryswkwg8glci6wr8mfvh")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.8.0") (hash "0b4ramwlzsycdgk0qz6fxqisgl0if6hnij65i66s76x5hp0i1k4s")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.8.1") (hash "00ask6aim19xr16cj74y8qhf1xz72vg1q1crx5a842b77vmbx5ld")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.8.2") (hash "0y6cwnvn3ymmcinkwskwn7p7zigh9fnmypqvd67gzb2sqdylnwjk")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.9.0") (hash "121q595xw6r5pfwl0r7b784l12skyihfgbc4yp6fllc60qs49dyq")))

(define-public crate-rs-kv2spacetimedb-2 (crate (name "rs-kv2spacetimedb") (vers "2.9.1") (hash "1vwipsgmqk3q9ss9qnifhsllrwgn28423l3ga058wg8pjpd48zlw")))

