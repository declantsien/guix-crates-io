(define-module (crates-io rs lu) #:use-module (crates-io))

(define-public crate-rslua-0.1 (crate (name "rslua") (vers "0.1.0") (hash "1wxrb1pygnfcj21p55wly0yn0flzx17g36qz3f5s3rxqgx5n1611")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.0") (hash "1jlj73v9pfifcw4723dx2lmplw9p7rlpzjq8i31jqqb5lrgw91sy")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.1") (hash "0xl7hvqiyx1xy7w1cx78299i69hf7j7nan1il6hhjwhkj8z8c6vf")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.2") (hash "1bk04phpiz6ii3ywv1v7vrhxansh0cqljpamrgvh6cz9ac376f9b")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.3") (hash "18fa1m72ibphwlvbd54169bgcmy75j2f9v0ba4jppvlfdagvfr56")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.4") (hash "0zhk1102ishf1fi2m6dz4n0mqlc0srhhilr9vh5lr7ww1qccm0fa")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.5") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1h215r55ip56bzzlvymxqky10099hf79pr6q7dg6jniv2r5b8d87")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.6") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0pdws0h4gfb21p2ir9z9r0pl89m6hspcxzakkzlfvhy311rcbgy7")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.7") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "04zjdm9mdrpghnkmp6y04avpdyrhd0b4sa5ggs5phizlld5yphhx")))

(define-public crate-rslua-0.2 (crate (name "rslua") (vers "0.2.8") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1cwqh8xlnd3lkgyglf4jlsbgwpanx16zwqkjddak75vpmvns85n0")))

(define-public crate-rslua-0.3 (crate (name "rslua") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rslua_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rslua_traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03jrax68vqa30ycb9km5ybq2bmdj0z5ianf2zqb8hnsg7iyp1608") (rust-version "1.72.0")))

(define-public crate-rslua-march1917-0.2 (crate (name "rslua-march1917") (vers "0.2.7") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "06xbnyma1ycwrmj5cyr5f74b9njshd5sw55qb4gcgpwjxayzf9cv")))

(define-public crate-rslua-march1917-0.2 (crate (name "rslua-march1917") (vers "0.2.9") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "112cf85gdn17yh9gba9n8hjj80gr5iqc3b1pksr8vfdwzixl8jx4")))

(define-public crate-rslua-march1917-0.2 (crate (name "rslua-march1917") (vers "0.2.10") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "06xmvlsvkd199js4jfafnjy945hxxajqarsiy2qyvjvglvhdjb35")))

(define-public crate-rslua-march1917-0.2 (crate (name "rslua-march1917") (vers "0.2.11") (deps (list (crate-dep (name "num-traits") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "0f7jzsfa5msxsrklbkzbqmgdh4pfnszhssxnabqnmpz3dblfa736")))

(define-public crate-rslua-march1917-0.2 (crate (name "rslua-march1917") (vers "0.2.12") (deps (list (crate-dep (name "num-traits") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "06za5pkl1lgjmi8104nmqhvw3xp1pb83bj9v3m30n8kxlcg8argh")))

(define-public crate-rslua_derive-0.1 (crate (name "rslua_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "04bcl02fjljkalf4vqc5xfkvljkdr8svsn2n5n9hck6pzg5hi3sx") (rust-version "1.72.0")))

(define-public crate-rslua_traits-0.1 (crate (name "rslua_traits") (vers "0.1.0") (hash "0dprq4a70ly9ihbc3hg35yg79ziqawafz6j7ld7akn7rq9m0gcj6") (rust-version "1.72.0")))

