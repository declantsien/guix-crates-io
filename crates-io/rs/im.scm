(define-module (crates-io rs im) #:use-module (crates-io))

(define-public crate-rsim-0.1 (crate (name "rsim") (vers "0.1.0") (hash "04vhyzzy5wq1zrawqky4xmlclwiny6kyici3b7cm45nli10yqkvb")))

(define-public crate-rsimple_http-0.1 (crate (name "rsimple_http") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1nynbnfr5wbm7qfnkl25wbdgfqghdrsbxv05np4b0a54qsspzh4s")))

(define-public crate-rsimple_http-0.1 (crate (name "rsimple_http") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "16q3p5lldl38pcq44h0c361qnvjh8mbi3spqyn81l460vqyhz7zb")))

(define-public crate-rsimple_http-0.2 (crate (name "rsimple_http") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 2)))) (hash "1y26b836jdqj2k1lylyhzkbwc5frllmxzgwsa5glfl81fdlg2qxw")))

