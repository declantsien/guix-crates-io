(define-module (crates-io rs _r) #:use-module (crates-io))

(define-public crate-rs_rc2-0.1 (crate (name "rs_rc2") (vers "0.1.0") (hash "0qphbmmwnh1ybm3f9icm074rdkvr0r9bq2v2jsqa5xc11ani1m0z")))

(define-public crate-rs_rc2-0.1 (crate (name "rs_rc2") (vers "0.1.1") (hash "0rb0499p5jyvd3s89yhd7nv4mxrb86bxa0pzcbw37l37cwmf9gsr")))

(define-public crate-rs_rc2-0.1 (crate (name "rs_rc2") (vers "0.1.2") (hash "1nhmfzjx1pj3j0k354cic9xphhnvz1i521nn5x2c2ay7kl1rfvc2")))

(define-public crate-rs_rc4-0.1 (crate (name "rs_rc4") (vers "0.1.0") (hash "1n56pd9y2alxgidz147aacrr08cwnlaw0xphk3nqxqirlrq33mlg")))

(define-public crate-rs_rc4-0.1 (crate (name "rs_rc4") (vers "0.1.1") (hash "1j2xlafhcs1pvbszp6x60whx534gas0y5ba35cdfikdx3l4693iy")))

(define-public crate-rs_rc4-0.1 (crate (name "rs_rc4") (vers "0.1.2") (hash "1891hqh9s79s5gyhbgpmyczq1z0ca1akhf2a4nw5rj5dxvfa54as")))

(define-public crate-rs_rc5-0.1 (crate (name "rs_rc5") (vers "0.1.0") (hash "1b4b7pmwzan3n7hy4vy46i82l2sl460j1am54j7lfdnisysviidy")))

(define-public crate-rs_rc5-0.1 (crate (name "rs_rc5") (vers "0.1.1") (hash "11nnndlcz8jrqbafsja63hab4hsqrh8pa546q3v7n04pwmrd0xvh")))

(define-public crate-rs_rc5-0.1 (crate (name "rs_rc5") (vers "0.1.2") (hash "1lna2xsjvb06df3sda2ynlrfljc1176gww6y9ginpxmivclbjqcn")))

(define-public crate-rs_ripemd_160-0.1 (crate (name "rs_ripemd_160") (vers "0.1.0") (hash "1xzz5qa91lvlxwnvl7fbbw27p6r7d3nif5z6j0ds0rhs0jy6i0j4")))

(define-public crate-rs_ripemd_160-0.1 (crate (name "rs_ripemd_160") (vers "0.1.1") (hash "0m371pbbg28g14a5i5zsnmc8rbw9zyzwyc2i7hpym5hannjnq2pd")))

(define-public crate-rs_ripemd_160-0.1 (crate (name "rs_ripemd_160") (vers "0.1.2") (hash "0dq5h2kr9jvlvsa9i7qziwiq0bz7ajxasgmnl4hwihlylijh95kk")))

(define-public crate-rs_rsa-0.1 (crate (name "rs_rsa") (vers "0.1.0") (hash "08k421h0rsh379yajrs8qqpl0gw7wvc9wpjx4bpjiirhzl2pblq3")))

(define-public crate-rs_rsa-0.1 (crate (name "rs_rsa") (vers "0.1.1") (hash "12bgxd6dl9a1b6kqwlm50pdg4k9m4bpmmwi1xclq4p3wmiq94n8c")))

(define-public crate-rs_rsa-0.1 (crate (name "rs_rsa") (vers "0.1.2") (hash "18q0gi6pgvfm6nrqh8m6hxnp50mh2wg7l3gw38n3x72cliwdb2g2")))

