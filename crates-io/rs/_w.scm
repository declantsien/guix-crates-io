(define-module (crates-io rs _w) #:use-module (crates-io))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement"))) (default-features #t) (kind 0)))) (hash "1n239s4knx53qwgliyw2hnsfgwdbamg63c1a500ynfrlqmlnjy0b")))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement"))) (default-features #t) (kind 0)))) (hash "1hmhv25fl29mcixhmmxsw676qrzc3hd2ypkcqa5spnvrwlywsr50")))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.2") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement"))) (default-features #t) (kind 0)))) (hash "1z1yy9y6k8hr4i0qwk83anfm0cw1s36vlcld65l1jm21db2n85gc")))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.3") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement" "Document" "Window"))) (default-features #t) (kind 0)))) (hash "0m6k0nq8mrw7igi08h340izjlfybp9lizgcd9vx3x2dka1a062pc")))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.4") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement" "Document" "Window"))) (default-features #t) (kind 0)))) (hash "17s6zihmb5kmdci78r13ir36k9rqx2vqy3fg7c29n4vnixm0rxis")))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.5") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement" "Document" "Window"))) (default-features #t) (kind 0)))) (hash "0cgrmk2r6bqqqldkr7wrl6ilkxsni09a3cxzhc18s62xd7wb705s")))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.6") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement" "Document" "Window"))) (default-features #t) (kind 0)))) (hash "1rj4mzhq407pbyw6xdpalrg7yg1yqm40a14kg3y760a0cm3v3ql1")))

(define-public crate-rs_web_component-0.1 (crate (name "rs_web_component") (vers "0.1.7") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.89") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.66") (features (quote ("HtmlElement" "Document" "Window"))) (default-features #t) (kind 0)))) (hash "0q8zlskciwsfd7i5hzw4awqvvqs4hjp6xd89ars9v5ay4jdb0032")))

(define-public crate-rs_whirlpool-0.1 (crate (name "rs_whirlpool") (vers "0.1.0") (hash "0ba3m09hxg30phfmhmg98v2rfwnv3d65hal0s8cazammfjmyfaz5")))

(define-public crate-rs_whirlpool-0.1 (crate (name "rs_whirlpool") (vers "0.1.1") (hash "1f1652sk1fn9rcpj4qi38n2gdmjhpfa6b3f809mnvfcd0wg06yrh")))

(define-public crate-rs_whirlpool-0.1 (crate (name "rs_whirlpool") (vers "0.1.2") (hash "118400v75q39ry35355r772lmh4bb0gch7zzzzn8lbhvqyv5bq6h")))

(define-public crate-rs_ws281x-0.1 (crate (name "rs_ws281x") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26") (default-features #t) (kind 1)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0hrgshx1z98a2jimfyhjxgdcmdiknz12b21nirb8fnrjh09yw4v2")))

(define-public crate-rs_ws281x-0.1 (crate (name "rs_ws281x") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.26") (default-features #t) (kind 1)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "1paz1g91m54xjs5n4vf2kjp0qnb0hknszn1rfrq2gzf3vh0476yv")))

(define-public crate-rs_ws281x-0.2 (crate (name "rs_ws281x") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.26") (default-features #t) (kind 1)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0czj3ws5dm4588c33mz0pk1i4nx9hbbj5j5sb2dg5kyg3zbj28my")))

(define-public crate-rs_ws281x-0.2 (crate (name "rs_ws281x") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.37") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0wbxv5y95l56pvamijwfz9lb37pi5lrsi4ms9yw98grwjgrskh9d")))

(define-public crate-rs_ws281x-0.2 (crate (name "rs_ws281x") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.37") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jkv9jzz0dbcm7r6nyr4a5d8s6nyajpkiwl2v3ijd5r1i01dgqwk")))

(define-public crate-rs_ws281x-0.2 (crate (name "rs_ws281x") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.37") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1chrqsrywm8p9lqd5m2bp27g46ybqzpmxhnrnp5j517ff1mic4bh")))

(define-public crate-rs_ws281x-0.3 (crate (name "rs_ws281x") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.54") (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i859yhn053w55j14g42zi4q1cx0zi3by46jlqdmaycrkks43363")))

(define-public crate-rs_ws281x-0.4 (crate (name "rs_ws281x") (vers "0.4.1") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ia8na7cbkvzy1020dr1lyqkp0c0ksl4apq2qsz6121am40lzjfl")))

(define-public crate-rs_ws281x-0.4 (crate (name "rs_ws281x") (vers "0.4.2") (deps (list (crate-dep (name "bindgen") (req "^0.54") (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jhngwc13wrmfffzxqgzgizdvw3rl92l14cgl6kxicwrphwvg840")))

(define-public crate-rs_ws281x-0.4 (crate (name "rs_ws281x") (vers "0.4.4") (deps (list (crate-dep (name "bindgen") (req "^0.60") (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y04iq56qj6nx4aq06q9rfl5zlj7xpip61jiil4p7rkis40j6vnq")))

(define-public crate-rs_ws281x-0.5 (crate (name "rs_ws281x") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "04nhgq4349wdsl5pcp4y1m44nsj6pfc1y8wx4ycmvzpyq55vjaxb")))

(define-public crate-rs_ws281x-0.5 (crate (name "rs_ws281x") (vers "0.5.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "053dcpz9qw05r0inap1rkkm73xwjnvqqygyhwmz7dg7faj649lc9")))

