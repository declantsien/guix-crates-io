(define-module (crates-io rs ed) #:use-module (crates-io))

(define-public crate-rsed-0.1 (crate (name "rsed") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1rslcf1fcssyjc0b1gzz5yz5sf9w2952f6c3dk9j73fdqzn00vai")))

