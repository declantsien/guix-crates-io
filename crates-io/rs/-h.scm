(define-module (crates-io rs -h) #:use-module (crates-io))

(define-public crate-rs-humanize-0.1 (crate (name "rs-humanize") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rqmidi8b74i97m60bfwig91x016zyv4951jrqwiy824h87nrklz")))

(define-public crate-rs-humanize-0.1 (crate (name "rs-humanize") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jszsyz40bgbkynay9yf1rpshgkfzcxm9b8q499nsqaf5xg78avz")))

(define-public crate-rs-humanize-1 (crate (name "rs-humanize") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0j1gxbaka16z0rmhclzly3jmybvqdv70xl7vaw82wn4wi9mhrwl4")))

(define-public crate-rs-humanize-1 (crate (name "rs-humanize") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mcgm0bpf2k6svh99bbr1vk6wmgyf41nzjz1qia00f91ziqjw5in")))

(define-public crate-rs-humanize-1 (crate (name "rs-humanize") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rcw6iyrkkrqawsnr2sm1jq1843i2wzqkkp8gxiqfk1lm161wjr7")))

(define-public crate-rs-humanize-1 (crate (name "rs-humanize") (vers "1.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1dcwnpq4xddpsg55rkyyic7mqr1gvc8iirnrjabkhv2l1rpg9npb")))

