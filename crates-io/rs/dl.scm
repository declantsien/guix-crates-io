(define-module (crates-io rs dl) #:use-module (crates-io))

(define-public crate-rsdl2-0.1 (crate (name "rsdl2") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "null-terminated") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "ptr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2-sys") (req "^0.31") (default-features #t) (kind 0)))) (hash "0l8j8skmpz496r4lncrqgaq565fxziixlz1lpbppq4g49brbmncm")))

(define-public crate-rsdl2-0.2 (crate (name "rsdl2") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "null-terminated") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "rsdl2-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dz2xyx20r96cqlr040ab66qk2qz68k603dn150xl73q647wfm7b")))

(define-public crate-rsdl2-0.3 (crate (name "rsdl2") (vers "0.3.0") (deps (list (crate-dep (name "drop-ptr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "null-terminated") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rsdl2-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "151k7r31zqk4f581lgz68jn9yrsyb2vsjsrpdrp08wdcccpk6daw")))

(define-public crate-rsdl2-0.3 (crate (name "rsdl2") (vers "0.3.1") (deps (list (crate-dep (name "drop-ptr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "null-terminated") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rsdl2-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "05b75jflf6p72mhv8bv23vjjwyx6fmikgiv91z9iasgbjzv4p3wb")))

(define-public crate-rsdl2-sys-0.1 (crate (name "rsdl2-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "19ahvy869pcr88dh7sd5s6rwk9qxy4gn73i3krhn7znr3ys2s28z")))

