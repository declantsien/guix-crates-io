(define-module (crates-io rs fs) #:use-module (crates-io))

(define-public crate-rsfs-0.1 (crate (name "rsfs") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qvsl86apd8nky756kszai6jn0iv0a9qbgjymciz69951v3qv7q2")))

(define-public crate-rsfs-0.2 (crate (name "rsfs") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "0s1qh91ffqwhp2bs9qpc3k28qdhs8npw7pk24w6i6gr2m1rhj9gp")))

(define-public crate-rsfs-0.3 (crate (name "rsfs") (vers "0.3.0") (deps (list (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "05m711zajl6ng65b7h87hizkldy992dfgg3qfsk2ycxh4m0k250d")))

(define-public crate-rsfs-0.4 (crate (name "rsfs") (vers "0.4.0") (deps (list (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "0j2k53cmm9z9lcq1dc78yk339g9zfba1d9yyx8dwl52mssw8b0m2")))

(define-public crate-rsfs-0.4 (crate (name "rsfs") (vers "0.4.1") (deps (list (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rkbah00cf0661zzlzacxl5y6m0xlrgpd485lkm7582l5aq6j68h")))

(define-public crate-rsfs-tokio-0.5 (crate (name "rsfs-tokio") (vers "0.5.0") (deps (list (crate-dep (name "async-stream") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.66") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.12") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "1q1gq5i8yjf71h9cyc9hnv733an6kivi0dq1j179yzcz3hqfd79f")))

(define-public crate-rsfsm-0.1 (crate (name "rsfsm") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0hnv636i89f3r4z84ig7rm5i8z9h8n86f12xq7yvda62sky3dp5w")))

(define-public crate-rsfsm-0.1 (crate (name "rsfsm") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0zy34xc0miqvpdl160y4sjxbprpnj8y22bidzl2v2wdb55x77d89")))

