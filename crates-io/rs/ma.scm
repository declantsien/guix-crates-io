(define-module (crates-io rs ma) #:use-module (crates-io))

(define-public crate-rsmanuf-1 (crate (name "rsmanuf") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "13ziaqq313fd40fgkmsy1imvf6wq6mil2m4dq537crsaa8xwi3q8") (yanked #t)))

(define-public crate-rsmanuf-1 (crate (name "rsmanuf") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1aszkvrxbxpk497hwmgzs6zfcspqmdqk8xsb3ajx3py637zw6kaz") (yanked #t)))

(define-public crate-rsmanuf-1 (crate (name "rsmanuf") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0v2zafpmfd0dh45fhjy240lw4g3qhzskiav0pparxk4yzb4321a6") (yanked #t)))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.0.2") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1y23rpa4924sldhm0kbxkdmy59vinz4v2m37lk94na0k8y0f44nq")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.0.3") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "15gmdyvlrkm2l6l4q2vpwvkyfdl9bvkzxwhvd7z3f0rkqcks3y8h")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.0.4") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1b7134avbpx5snxmavjzizfhlrx8m6rpl93jj9icl0wq7wihb28q")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.0.5") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1bhwryg3shkw6xkrr929wsw4w8ml9va7mg7sf53821d0cwwcf29k")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.0.6") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1z2kkq5dsmp1cw8zw60b57lhryr2w0yncyw5d78bi3ql9rlxvnln")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "082s7jrx0py9grp3ggk6s26pv55jjxbrajqn97r7lsi92c0svbf7")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "17lkd7rmv6lh65cyr9jh615h7wxz3zkybhn446m43nxa2v8w1386")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.1.2") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "13jlbkp5ypynyyyh2y5j3rhbar7j7sv8ylzvyavrv5j0zjby9dgk")))

(define-public crate-rsmanuf-2 (crate (name "rsmanuf") (vers "2.1.3") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0y7m9pmlr7qsiywjaqrlppn5i70z3g7rgcjjr4z95myc8dyx2pac")))

(define-public crate-rsmath-0.1 (crate (name "rsmath") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1macw4d80b7z0frawcsifb1c6iay4b1y2qaplj1xhrgiawwis7d7")))

(define-public crate-rsmath-0.1 (crate (name "rsmath") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1b27qvy6vjly01d2wak4dlyjlalmi1q36p9957ddh6h8b41g88f8")))

(define-public crate-rsmath-0.1 (crate (name "rsmath") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0h16l8v43hwam2fwgl438x3ni405779bq5aqg3cqnc035ykylvj6")))

(define-public crate-rsmath-0.1 (crate (name "rsmath") (vers "0.1.6") (deps (list (crate-dep (name "num") (req "^0.1.35") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zaj1r34qxlqlh2javhfh4zf6pgq017ppmv0j8j9dwq5z8l0vsc7")))

(define-public crate-rsmatrix-0.1 (crate (name "rsmatrix") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1ib2wf1lm3vss01prb08rcdqr2r639wbrfmha8gf5r4f3pzc519a")))

(define-public crate-rsmatrix-0.2 (crate (name "rsmatrix") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.0.0-pre.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0kfd13v4wwhfdik91fm89n4mrglh379y4zacrk9gbi5js3w26gfg")))

