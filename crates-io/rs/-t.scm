(define-module (crates-io rs -t) #:use-module (crates-io))

(define-public crate-rs-tiled_json-0.1 (crate (name "rs-tiled_json") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0m0jrm6yib6rgrs1dxq51v5qbaysc448yq80hnjqk08wg80gcnwk")))

(define-public crate-rs-to-zod-0.1 (crate (name "rs-to-zod") (vers "0.1.0") (hash "1fhpzf68ar2bkiynk76f6wbh9cxi8vha9w2m71cr5lsdqazpn38a")))

(define-public crate-rs-tree-0.1 (crate (name "rs-tree") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0g5rr1bv8xcvmg32b2kqx1vzwb2spn140g67463l05gr09kw1q80")))

(define-public crate-rs-tree-0.1 (crate (name "rs-tree") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "1b0vyz180pm95y8ssvyrhcb6m8simc775wcwn2m9c0ijn90hvcpx")))

(define-public crate-rs-tree-0.1 (crate (name "rs-tree") (vers "0.1.2") (deps (list (crate-dep (name "structopt") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0bpgnd8ax5fhwqwgi8y9l6z6g4qhzr3p5hyh9cg3r8qp3ff35as0")))

(define-public crate-rs-tree-sitter-languages-0.0.1 (crate (name "rs-tree-sitter-languages") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req "^0.22.5") (default-features #t) (kind 0)))) (hash "0gh8qrkyrgfjc1iqakxy1ab8963hswy4ag4wv8bwzl0269gm2g8a") (features (quote (("yaml") ("vim") ("typescript-typescript") ("typescript-tsx") ("toml") ("rust") ("ruby") ("python") ("perl") ("markdown") ("lua") ("json") ("javascript") ("java") ("html") ("haskell") ("go") ("erlang") ("elixir") ("default" "bash" "c" "cpp" "css" "d" "go" "haskell" "html" "java" "javascript" "json" "lua" "markdown" "python" "rust" "toml" "typescript-tsx" "typescript-typescript" "vim" "yaml" "elixir" "erlang" "perl" "ruby") ("d") ("css") ("cpp") ("c") ("bash"))))))

(define-public crate-rs-tree-sitter-languages-0.0.2 (crate (name "rs-tree-sitter-languages") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req "^0.22.5") (default-features #t) (kind 0)))) (hash "1ffwf2wkv49cmsslha1748h8jh7jsibsayw57cqhaqjk9jzikfn7") (features (quote (("yaml") ("vim") ("typescript-typescript") ("typescript-tsx") ("toml") ("rust") ("ruby") ("python") ("perl") ("markdown") ("lua") ("json") ("javascript") ("java") ("html") ("haskell") ("go") ("erlang") ("elixir") ("default" "bash" "c" "cpp" "css" "d" "go" "haskell" "html" "java" "javascript" "json" "lua" "markdown" "python" "rust" "toml" "typescript-tsx" "typescript-typescript" "vim" "yaml" "elixir" "erlang" "perl" "ruby") ("d") ("css") ("cpp") ("c") ("bash"))))))

(define-public crate-rs-txtar-0.1 (crate (name "rs-txtar") (vers "0.1.0") (hash "1z7hmxyh1kx2iq9ia5bmfgb6zkqsqg7zh0bmiqdwhyqqsdiv787z")))

(define-public crate-rs-txtar-0.1 (crate (name "rs-txtar") (vers "0.1.1") (hash "067s0z11pn88knh94g2kdq1v9hbqv4danwspfbvfw8cy6zmbfr7q")))

