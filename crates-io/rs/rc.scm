(define-module (crates-io rs rc) #:use-module (crates-io))

(define-public crate-rsrc-rs-0.1 (crate (name "rsrc-rs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "01f39kzgsp62awvxy3dgcryk3ba0vabxs7zxpybgvv4gfxvczk4v")))

(define-public crate-rsrc-rs-0.1 (crate (name "rsrc-rs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "winnow") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1h7rz2zmp3pymar40kh7aqn4ps90g75w5a7avj077qla76rfq7ih")))

