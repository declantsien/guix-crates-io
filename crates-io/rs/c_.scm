(define-module (crates-io rs c_) #:use-module (crates-io))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "16z9qdjqwlib35bbji67iyxg9p1dfh3cg0vb3rb665ah3kf5dbvk")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0w2k00mcrbr434w4i3ixcbqfskr84w7ws7ldv6wd83rxh8xkfn4w")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1r5r30scvnms444724qp8zr139qifv7xyfwpg9yd54mfx4bpxv8w")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1jbrg6zd0zy3yl5c2anq12js2zhgzg6zl2pvzz21lhhwrr8rwnwv")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0864ajm8kr7bj2g1ixaa9zlg6gi08310qlml67cbsdfcc97b8fgr")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0c2b0jzkbk40d4jpzr23b288ms79wrnlrssw94mc4mypyxwjivj2")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.6") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "17wmz4aap272f03ccnrcc49phcxizp0bi0d36ap6rr62sca5d8yk")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.7") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1655s8vdiimysv127cqchigkygzfzgp7wbb2lb6syfbk46jxifg0")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.8") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1192rzdrg7lc3lz8fcq3s65rsriqkkbki12cbkb28nq3y8s0vm2x")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.9") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "12dp2sdsk3yg348njbnhk7ldfdnsqc1rm831jrw0l8zsp7qi786j")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.10") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0jgd95d7a7q3n7ib0rh4xswqniixcvbh4hshq2y3fsr1a0dkm4si")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.11") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0g89hmsbc28lcd3wcmw2sl7vs7d5gryn6w8bal0zgqi48ar5887l")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.12") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0dvh4hdws8ds7kkc6sjgzp6grb9l30h3fqbncnxf141cjg2lkkp7")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.13") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1q1ydala4lkqxbil0zg6jcz1r9yfd1jpl3cdbfwjg7k2w379x9ka")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.14") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "03b1hfgbhzhw8y9rxrjal1pcg3w5xcrblycyrjwslz9hdc7fik04")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.15") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0x7s114l10mqmf9gmaacfpp0xh8si7yr1pc24xhdklscdlzyy5na")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.16") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1yadcylk42ahgj93fgfw591b3nws3zyblqs561kfr1w6xlxyq20m")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.18") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "13h5svr0cq33mb5ipilc5x6k0gs7qrp3naiyc238djgzr10fzzyp")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.19") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1x8378b87qsf991amd07wfsymnfcp4zy33lqydk9ywmalxbygjnd")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.20") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "194f8x1kk18njbzwjgw5q4c5d8183ic1746b24kvlak55vi16mhv")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.21") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0f72wpa6yjkjyrbvks8zsbqd6pp1ah5s89dpryl3qnmsx7s19sw6")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.22") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0pxxnmh1zh1vxw28rhsvfgsgz84vp0kc20x28yb38xv2x71d93l9")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.23") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "05sbpwdh5yvxjw04c5y0rmjqpr09v8qy31sn3fbbwrni1sf00a6k")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.24") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0a5dfvv3zqiwa8h8xvnhnayh2znv7ihwyb2a760f25jybh5hk0i1")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.25") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1bnh5309yfm4a64yh1bddqcaxkfb7w07iqfy6qfn9xpv1j6clll3")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.26") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1m9s8dwkxkxsdrz4qz4ran4s5bshw4lm2xg519s8wfvlsgsrcrrr")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.27") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1gv2z8bx1wj313as96r6z2bxi3dd987c8dzvnvbsd2mfh1c5863c")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.28") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1jywwr6mzv8fjb12q1plv10mm4kk7zi87w988fk8q493n64m82ak")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.29") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0gnzwprn900b2qk89kdndzzqf8xbv4zzrsch2y47l02is309zr0d")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.30") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0z4vl9cbbnlkmlff1q4f1kbglqlzcj9lbl0bcz1iwps7w4pnj1si")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.31") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0a021595rly4h4v36s1jynqqk77cm4slqw63a6x3dlkcbwyxyn3l")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.32") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1268kql7y857w1vyhz231i99k17i76mpxsgpihg4wkrw09il70wk")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.33") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1ldqdf64gpw0ry79nrvkdxy3ybri9i149lgwmmdrv89dlz08mb0f")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.34") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1mwfh2lr9z05kxav90lvna945svbqajdmnd4g8d6grbp7lmxlpm3")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.35") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0w22lkw1v1q2cwqdp86zz5vg0b3lsanvypdh477llc5m38gz294q")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.36") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0rv39r756j0hca9i1y937z9a2d2g5w4sdrard61y72l6hndd0163")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.37") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0wc2jq3xs0wffcc7sg4zfpkm323k6lcdc0cgqaf08gic3jmdh0qc")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.38") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1mmrd24xaf20mpzb46bi2i4i51b38qq71x5vg6f41nd33wwsnyi6")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.39") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1jkf81ybdi75g6mr6w3ckbjwsnjiykx2jynlgv73y8sd9acla89h")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.40") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0j4j0h1xb38fg8pm57ng69gyakpajrqq9vgnnynbkawb3p3006sl")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.41") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "175qq8zaw68d4znkpb14pa55g7dz03s8ws0qf41hlvbjgizjb5bv")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.42") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0yn2krhdm6h8xa9awrlnxsscql7755l68shwh2cmbcv0h8rzx11s")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.43") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1cjn5wk20raqy3fvab805kyl58wrqxnpkpr9angw9h004f5m5zf7")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.44") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1cp3wpj0b9n5qijx7qdq0f23vibdglb8lmrvbilfhmrl7w4yaq7z")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.45") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0kr7crvl7vzxhzx1bp4c6jh5xbh712bc3b3i5qxni9gx49c5mdmn")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.46") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0wv08bm2fr81849iv1xqb2wjk7hqsqin5s6als5q4hpd072cpk6b")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.47") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0s2h61qfmh1h590ylxvp8l7yb8cii4dc4d96wz81ynls9mmr2zpm")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.48") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0xlplm6gqj9hhd2737jfsm3armbimnvhsyjr8ns4r04vzj7676f0")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.49") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0c756hcwmflcgjlwhyn1r3srq5g3rgm7jv60mn2c5iqxn26d0xq6")))

(define-public crate-rsc_osrm-0.1 (crate (name "rsc_osrm") (vers "0.1.50") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1apmngy3j860xhh7i71c2y2anysc3fgy9kaffa8la6n89zn90nnk")))

