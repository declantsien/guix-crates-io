(define-module (crates-io rs ep) #:use-module (crates-io))

(define-public crate-rsepl-0.1 (crate (name "rsepl") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "shellfn") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1kvdpsx2ylvnnzrj90kr08vs2wai6fqk82jlqvbslk9k65xvgwxv")))

(define-public crate-rsepl-0.2 (crate (name "rsepl") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "shellfn") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dradzix167r876vn7sjnv7hi3xzbfiqab80aismmbx8i91kz7pr")))

(define-public crate-rsepl-0.2 (crate (name "rsepl") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "08sybnbdad58x7fyccw284wbmvvpxk3yzn14x9xxxjycb06d4jsi")))

(define-public crate-rsepl-0.3 (crate (name "rsepl") (vers "0.3.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5.0") (features (quote ("eq-separator"))) (default-features #t) (kind 0)) (crate-dep (name "reedline") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "10x4qgy6bp3xfras7086ia6cynwc3v7w2j1kk4ywj1qckabhdsx9")))

(define-public crate-rsepl-0.3 (crate (name "rsepl") (vers "0.3.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5.0") (features (quote ("eq-separator"))) (default-features #t) (kind 0)) (crate-dep (name "reedline") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "0lqizmb8b3zbkd5a7f7fnq1gsvbcg7mw6xd6fbpgwbq2j535c6m2")))

