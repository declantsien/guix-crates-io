(define-module (crates-io rs py) #:use-module (crates-io))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.0") (hash "1kf9v623vk2ix4nkkfji86gag0psxhr04r6dxh4afbw99ili4wy5")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.1") (hash "0c398q312wjnn4iyp352s3lqzvni5n77jknbp89pjpr3icgj6lfc")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1pdh6fczi9vmchp51scihi5h6xbhhz0bp7fa7cvfbvy9kqph4qkd")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.3") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0mkanbh20cbgvs60a73hw1ccx5m9yd1lzw321q5asly3sy5rpajr")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.4") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0ki44b6v6nrj91jgy864vs0bhmqrg1r1j4pja4f6ks1kj1ljvxip")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.5") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1hrark7414fl3hqihqhc65kwshrb0q5wb4x9vza8nxlxcg9ypgk0")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.6") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "13hqbzv34k3m4gk4wmfdxz37vc88qvlriak4vr2rk4zklh31nsma")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.7") (deps (list (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "05n0narzmxlqzw64m1i4zpcnnbgv2mp42f1aiwpn3z3vqzbnsxca")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "06x67r4val9pcj5k14b9vva1w97h2ggfr1wmx3wjaj9x9scws4c1")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "1y8b0vnrb64fpmgv53svsk0x5bkk52p1k5bldc2sspllvncclakr")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0qk5q0237476iv1wwa628y6zssl0ibv030ri6608p598j86n0krr")))

(define-public crate-rspy-0.1 (crate (name "rspy") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "1f02h5jnkd9488my71abw37a6d8syxk6mdd68vwmcqzimk1h36w5")))

