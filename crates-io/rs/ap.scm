(define-module (crates-io rs ap) #:use-module (crates-io))

(define-public crate-rsap-0.1 (crate (name "rsap") (vers "0.1.0") (hash "0bq7730rbqkxryvg9n5kqy2l3fy539110l29iyxrir90s2msaq1z") (yanked #t)))

(define-public crate-rsap-0.2 (crate (name "rsap") (vers "0.2.0") (hash "09a1bwx0mkhya6w8ggf2zwinvqqrh8528rjssj13q07gfgmvdq63") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.0") (hash "0qbs9692vh2jbqqybzivvy1a7x1a5iq1xfdn0qvkmcspy18gx7fs") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.1") (deps (list (crate-dep (name "mtk") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "16dn41xi27638j6yw0jnncc9xa8yvvm08xfljy00957bkx6yzzyj") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.2") (deps (list (crate-dep (name "mtk") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "165q50s3r49gpxa71nihpf6qqpycxddfgh6h5xchrhlccnph18pg") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.3") (deps (list (crate-dep (name "mtk") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1vhygj3j11q0wagvfsnq456mq87663pixar8cxaj7k5fcjl9pa0k") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.4") (deps (list (crate-dep (name "mtk") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "14gd9l39n0fix6lynw938zpw2mhj5jqjc65xxm6nc6rcks3hpcpw") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.5") (deps (list (crate-dep (name "mtk") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0352lcqr3fp7gwrbwg78f72pqkbf3gk104c3idkvf0za2gcypizv") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.6") (deps (list (crate-dep (name "mtk") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0jdws2i7dqk2ij76d2r0sv7wh0cynscj9z7x6bdpm12xvhal76j9") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.7") (deps (list (crate-dep (name "mtk") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0svigf4rrapm8550sfms903b5vd685f60gyala01l9yyibmc4chs") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.8") (deps (list (crate-dep (name "mtk") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0afmwq00hrgagb8xjmn9v0454fj9p4asprg94qi1xw513024r3n1") (yanked #t)))

(define-public crate-rsap-0.3 (crate (name "rsap") (vers "0.3.9") (deps (list (crate-dep (name "mtk") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "036c0g99scpv3p5psmvfj8m17mnsy3azvwyjfcj4s3sx54jf4iyz") (yanked #t)))

(define-public crate-rsap-0.4 (crate (name "rsap") (vers "0.4.0") (hash "1apjfxv9igzyhnnmamz80pa1acan5j7jm8iz9z0g2w7ck65cm8p2") (yanked #t)))

(define-public crate-rsapar-0.1 (crate (name "rsapar") (vers "0.1.0") (hash "1k2brrmwq4xvr5yf1521vginwd4lmz3vmy1cgnv4b343nyyl5bl4") (yanked #t) (rust-version "1.76")))

(define-public crate-rsapar-0.1 (crate (name "rsapar") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.20") (default-features #t) (kind 0)))) (hash "0pnbdgyccwm2gix7mi53z593nh53w4s4rhhfjs3k54z8v8rihgz6") (rust-version "1.65.0")))

(define-public crate-rsapi_http-0.5 (crate (name "rsapi_http") (vers "0.5.0-rc.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00phlhxp0fwrazcsllkx1ldp8291bxc5xzbah4pjmgpj4vaqlgz7")))

