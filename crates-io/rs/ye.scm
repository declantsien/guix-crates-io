(define-module (crates-io rs ye) #:use-module (crates-io))

(define-public crate-rsyesql-0.3 (crate (name "rsyesql") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1mx6ywwmfiypgzvp5qlhc1s4lbl8h23cgqkldjh3mqa6szam28bg")))

(define-public crate-rsyesql-0.3 (crate (name "rsyesql") (vers "0.3.1") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0jf8vl4klvs12a99vi7mkp31r0g23n54wmnyvyb96ww3xg9b1d1s")))

