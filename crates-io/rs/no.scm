(define-module (crates-io rs no) #:use-module (crates-io))

(define-public crate-rsnotifyos-0.1 (crate (name "rsnotifyos") (vers "0.1.0") (deps (list (crate-dep (name "dbus") (req "^0.9.3") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "gtk") (req "^0.9") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser" "shellapi" "errhandlingapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "175jnxxxdhbn0byk41dvqx6l00gry9svnfki7d66m8wjdqcs9b5q") (features (quote (("enable_debug"))))))

(define-public crate-rsnotifyos-0.1 (crate (name "rsnotifyos") (vers "0.1.1") (deps (list (crate-dep (name "dbus") (req "^0.9.3") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "gtk") (req "^0.9") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser" "shellapi" "errhandlingapi"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0kk8add9snxfx22c2fwlv12fc24fyz81i5a100kx6klx3nyvswcp") (features (quote (("enable_debug"))))))

(define-public crate-rsnow-0.1 (crate (name "rsnow") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19k5h2pzx0k1yd27r53kcc8ghpbda4bdzk32fvz7wb2naaaykmnx")))

(define-public crate-rsnow-0.1 (crate (name "rsnow") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0h242dmx6gznr0h664bmyrycrxpmabmj4plicbydrancklfy61jm")))

(define-public crate-rsnowboy-0.1 (crate (name "rsnowboy") (vers "0.1.0") (hash "04233p63q58m1jzc7crnnhxv3lya1x28hmiks3n4li5cqsxwghws")))

(define-public crate-rsnowflake-0.0.3 (crate (name "rsnowflake") (vers "0.0.3") (hash "0rgcn9l61xicjgkabyw4pb36fins2j7zlcxpdh1pkvldxnbfkzsa")))

(define-public crate-rsnowflake-0.1 (crate (name "rsnowflake") (vers "0.1.0") (hash "0sjm46lzi5ch1dkbak7a1cybyrbn12x4mbxm8xz3zm1rmv3s4970")))

(define-public crate-rsnowflake-0.1 (crate (name "rsnowflake") (vers "0.1.1") (hash "0zwhd730j3lzzxij6vhxwk187cs0pnfb45cr6vkcax8hp5jjnqwr")))

