(define-module (crates-io rs of) #:use-module (crates-io))

(define-public crate-rsofa-0.1 (crate (name "rsofa") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 1)))) (hash "11fja42ljz34vnhz7gnlhbjzmy71najwj6j5p2675ndyy3vv17nm")))

(define-public crate-rsofa-0.2 (crate (name "rsofa") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 1)))) (hash "0pwpywji21kfzrd9dlz0r8ypjz6m6anmfwbgp1gd4p7l2w7r5r02")))

(define-public crate-rsofa-0.3 (crate (name "rsofa") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 1)))) (hash "0s5yhikgnbqwcydrx21r2p24z53k6falbjj0ckv654766ndwwvmy")))

(define-public crate-rsofa-0.4 (crate (name "rsofa") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 1)))) (hash "1zjzjj21flgnmb4v7rgwx4qacqwdmxqci5q0b59v8lkf22yhdjzn")))

(define-public crate-rsofa-0.4 (crate (name "rsofa") (vers "0.4.1") (deps (list (crate-dep (name "approx") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 1)))) (hash "193xysj8p4s1rrmc70km5h86x554nhqfxkc7fjvcclwa46lvxrhh")))

(define-public crate-rsofa-0.4 (crate (name "rsofa") (vers "0.4.2") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 1)))) (hash "13dkgqq1id5cpknwsdgaa5ls3wdwjxh015lhpl242gh9mx86j0vx")))

(define-public crate-rsofa-0.4 (crate (name "rsofa") (vers "0.4.3") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "118yawp22533bvqg5pr11ia3vkgg2bfmdbwpwmh9krbk36gv8ff5")))

(define-public crate-rsofa-0.4 (crate (name "rsofa") (vers "0.4.4") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1b692pd7hzmch1hz6z5b9x65wp1mdnsqg52a2jc6sb490i28bar5")))

(define-public crate-rsofa-0.4 (crate (name "rsofa") (vers "0.4.5") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0dr76rr43bxiasir51r3ava5nlm82nihsgvj3zdxaanl8zkd5by7")))

(define-public crate-rsofa-0.5 (crate (name "rsofa") (vers "0.5.0") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.60.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 1)))) (hash "0zhnvrgg5gmjydv6gmgiw58p2s6k2fg7clxw2lmymjzx6j4p2wi1")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f3l8krfdzgz7q0qzzx6yzf5xk7z0iah1dxfk98ynskmxrglls4k")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1imbcjsdqsgjg3z7przdpz62rrazc5n23b0ic5vx2jzw3a5l91ri")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15i6545v01v0xpryim02x11mlgjjz08jlfs3x8gm9vcbmwc1hq8c")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "183wby00ilaz8jmwhv21x0mzlwdyc6lv3swwwj8s9a7gfb82r15n")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1886qcgvpzjxz832mffrr258rlsi7d5lqc97sb5rps6r7yaqbj8x")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.6") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kw3x6cwj7qnz09c62awrskqv5yn8s4rj170044m6mss92b1m8ww")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.7") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03g53f83vz0x2sywp7cs7r3afpndm89hvqrdcz7jzyn60liv5mbx")))

(define-public crate-rsoffkv-0.1 (crate (name "rsoffkv") (vers "0.1.8") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qlhif0f9qijcryl699bvg6mg96wzy60f6r3hik2wvypv1wnqq17")))

