(define-module (crates-io rs -x) #:use-module (crates-io))

(define-public crate-rs-x11-hash-0.1 (crate (name "rs-x11-hash") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "07bmc1cl359wxncjh8ndrwawq6fxswd19lmnjp9cqdrhpmamdjrb") (yanked #t)))

(define-public crate-rs-x11-hash-0.1 (crate (name "rs-x11-hash") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "028xa1dynx7bxq6ixdqs31cgzmna1d75a3zm1rwmyhzrxg0412w9") (yanked #t)))

(define-public crate-rs-x11-hash-0.1 (crate (name "rs-x11-hash") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "15f5n3h0wyvb4y2m5zsxgnkkl5hh803jf9bv4v6l5yyyri2gi116")))

(define-public crate-rs-x11-hash-0.1 (crate (name "rs-x11-hash") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req ">=0.57.0, <=0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "0z2vi0kbrpq0rq6rc0d4ga3ii4cawd7wpda0m38xwzg6ib62lh6a")))

(define-public crate-rs-x11-hash-0.1 (crate (name "rs-x11-hash") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req ">=0.57.0, <=0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "0fm27mmbyccgcwvhxv034cxsvkyzibm91nl38m6mgxhyv6sla60a")))

(define-public crate-rs-x11-hash-0.1 (crate (name "rs-x11-hash") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req ">=0.57.0, <=0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "0vl9vzp00xpkkn7aq8nx44ivj9n308ag4rkbck52m7k7wy3f2h9i")))

(define-public crate-rs-x11-hash-0.1 (crate (name "rs-x11-hash") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req ">=0.57.0, <=0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "0c1si5bn50qsdisk7l4fl60q9g3m0cq2613msxgnygai0ql8bsll")))

(define-public crate-rs-xfoil-0.0.1 (crate (name "rs-xfoil") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0ch31fgw9510ym2k55m5avm1l6hw9977amvx4j4373i3hqf21s9z")))

