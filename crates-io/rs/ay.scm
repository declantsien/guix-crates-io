(define-module (crates-io rs ay) #:use-module (crates-io))

(define-public crate-rsay-0.1 (crate (name "rsay") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0i909y5qcng35jbgijrmlv5gnfl2jgzwlfphs4ihyngs5k1z80xl")))

