(define-module (crates-io rs tl) #:use-module (crates-io))

(define-public crate-rstler-1 (crate (name "rstler") (vers "1.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "04k7invc48shnsclgx9gla428jnfp2paa4bnx6sngn4if8d3pnmd")))

