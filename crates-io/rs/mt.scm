(define-module (crates-io rs mt) #:use-module (crates-io))

(define-public crate-rsmt-0.1 (crate (name "rsmt") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.14") (default-features #t) (kind 0)))) (hash "0agjyhrf1m29kczysa3cwav9jn67vvgd72789r0gn5y6lggcg0nj") (yanked #t)))

(define-public crate-rsmt-0.1 (crate (name "rsmt") (vers "0.1.1") (hash "0xpn5y2x1p5gz2kg5d7hzizxsk7hll5dw21wg7qcb49gjv4a6pmy") (yanked #t)))

(define-public crate-rsmt2-0.2 (crate (name "rsmt2") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.73") (default-features #t) (kind 0)))) (hash "1zs65p9qp7y550mjwhhxcb4klnxc4xf7kbh0gcggd1y2qsg4mli5")))

(define-public crate-rsmt2-0.4 (crate (name "rsmt2") (vers "0.4.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "0dwxra9qr9zs9jyp0idf7nvrc5cpc4wnphhsry60ljnpl1ixyw0m")))

(define-public crate-rsmt2-0.4 (crate (name "rsmt2") (vers "0.4.1") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "0n30lxlmyrb4zhs121vaqx96m1asl2ig43njqxwv4sn4zih23vyk")))

(define-public crate-rsmt2-0.4 (crate (name "rsmt2") (vers "0.4.2") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "12grzghw9j871f43mm9r25168pq9grmfv5fnpm2fr4ah1ax51ddz")))

(define-public crate-rsmt2-0.4 (crate (name "rsmt2") (vers "0.4.3") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "1wy2aqjfgb2i18wd3m93adp9g5wqs5iz9ql1bw0h75xlhkzfbnj7")))

(define-public crate-rsmt2-0.4 (crate (name "rsmt2") (vers "0.4.4") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "08yyijcwd14m12js34xwvxzymhll0ikbygn6wv3l0vy9cjvfv5ml")))

(define-public crate-rsmt2-0.4 (crate (name "rsmt2") (vers "0.4.5") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "085yra2x1l5hw3g88c5dh256cbm68lq6ybab637kzmvcpd0v44kg")))

(define-public crate-rsmt2-0.5 (crate (name "rsmt2") (vers "0.5.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3") (default-features #t) (kind 0)))) (hash "1yhfdzm5py8ac1rgp9xs2pkh6p34341790ara5pvip5wlc4pka3j")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "03i80l2ljpfvjcj17w5ahhbwz7fcm3d8qz95nyz1bx2bdkfzr9ik")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.1") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1agrhbsjngybilf6cphwn0hn8s6041jdzw9nyn3ji2sqqr7sqabm")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.2") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "09kxrw20b4g2f94jrf5v0296cqdh6qb1pdazidnmi6cvmm7yy7jj")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.3") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "04kpxgh3w6cabx01hz1dkwyp7jgd8q1ivmh83p4kv1xx8p4c1bhs")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.5") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1cn9z44r7vyb431fgbq0qcjfzqj6bm19sa57wslh9aywrnb9df0z")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.6") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1j8jraqi8alh572fafd1dwgy36ssgw3nxby2122lyyb788vzhfwh")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.10") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1v6b6b0k7b2sny3fbyqpryy7cbqrfjizz1ynim631297x9l4q2d1")))

(define-public crate-rsmt2-0.9 (crate (name "rsmt2") (vers "0.9.11") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "14xr4xa77kbmz9h00mzip3chi35mlh5119n084cyj9frfx478jar")))

(define-public crate-rsmt2-0.10 (crate (name "rsmt2") (vers "0.10.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "16h9a8rc1afmy6cq0prkkcfdxz4q3m2cy3cwcxvkhpfbg9ydmdwq")))

(define-public crate-rsmt2-0.11 (crate (name "rsmt2") (vers "0.11.0-beta.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1n1wnvpr199l9i73x80rkrysi54yb8r5m1irswkkpfimadzvnl5f")))

(define-public crate-rsmt2-0.11 (crate (name "rsmt2") (vers "0.11.0-beta.2") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "0wx14zca632c4wq2n523rr066r10rgz8mz2kxa0yfdydyrlxsfv0")))

(define-public crate-rsmt2-0.11 (crate (name "rsmt2") (vers "0.11.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "164nww5yisdy0gwcskwqyvhilbc0lmd8zjb9r7sdchh6f763cm5a") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.12 (crate (name "rsmt2") (vers "0.12.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "12fwikb9g0ybaa45rmhxqq4x5k0vbfrh2qqd392g94q3gynb25mn") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.13 (crate (name "rsmt2") (vers "0.13.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "04l6za0a9acypz5rda8qv2xxf1z08lyvr76ww5g9c5x99adffa31") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.14 (crate (name "rsmt2") (vers "0.14.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "167nsx32k6dnlrj1jhb64nf5d80a2qsxv10pniam7ss0q6qglf6m") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.14 (crate (name "rsmt2") (vers "0.14.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "0z8ccpaiyv5cqgshqxhls6hkmgq2s15rfa2744p2sws1jalwizsa") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.15 (crate (name "rsmt2") (vers "0.15.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "17w7rank7gpmxf4znan1j60345q9klsddgr8z0dk8l6pc4yldby9") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.15 (crate (name "rsmt2") (vers "0.15.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1v8yr50wishif64m7b90ipn2v4gpyb04j363jlib5931333b2254") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.16 (crate (name "rsmt2") (vers "0.16.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "05lv64hzbssakylfbssgmcg3p8408mdwwqr3yznxw7v8692isny4") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.16 (crate (name "rsmt2") (vers "0.16.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "01851kdcrpwfs8w85yzqpc750p5l2hk4xfixqgx5a6prrsq374cy") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-0.16 (crate (name "rsmt2") (vers "0.16.2") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)))) (hash "17kg468dbwm5b9pzxbwiyy7kz81z1qsm6s02lqw6mp6vbwz7vyrf") (features (quote (("examples") ("default"))))))

(define-public crate-rsmt2-zz-0.11 (crate (name "rsmt2-zz") (vers "0.11.0") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1ykvv56n21iangzqdqmys23s3sbj1dmg3vydhp0jk5lrbkywkd5x")))

(define-public crate-rsmt2-zz-0.11 (crate (name "rsmt2-zz") (vers "0.11.1") (deps (list (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)))) (hash "1qyqh47dzr9d23vl9h49gd8wr266x4nf0ay4xqnzjjswmajrb30n")))

(define-public crate-rsmt2d-rs-0.0.1 (crate (name "rsmt2d-rs") (vers "0.0.1") (hash "07hpyxfifhn2qb0zglyqb2ma0dakxm8jwz6mlvx98c1w7wr5b80i")))

(define-public crate-rsmt2d-rs-0.0.2 (crate (name "rsmt2d-rs") (vers "0.0.2") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "reed-solomon-erasure") (req "^6.0.0") (default-features #t) (kind 0)))) (hash "0dw939qs9g0gkx4nq0805m7mbx88wrd0i4jkckjpqfp72hrgw0i7")))

(define-public crate-rsmtp-0.0.3 (crate (name "rsmtp") (vers "0.0.3") (hash "1vdkfvywz06xk2k2c3xg5rd6gcgrf1qqhwv00rfv3v0ws49fh4xi")))

(define-public crate-rsmtp-0.1 (crate (name "rsmtp") (vers "0.1.0") (hash "0aai64i012fpffxf0hbp54m28jzga4c7rd112d6m0w2rlqmahw69")))

(define-public crate-rsmtp-0.1 (crate (name "rsmtp") (vers "0.1.1") (hash "0gbwgi6n5vj3wwmfhkx16n6j7bg6fbgqdl32xck6fq2gzybzj53x")))

(define-public crate-rsmtp-0.1 (crate (name "rsmtp") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1184angml3hdgasrqxzqgd5jdg3x3583pmzmsch4ih2wcwidnl3k")))

(define-public crate-rsmtp-0.1 (crate (name "rsmtp") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wrkwv8hc4cdna6zpvhxfv4dwjdc1mvh7n992mhy247amiznrm1v")))

