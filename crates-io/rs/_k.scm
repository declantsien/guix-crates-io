(define-module (crates-io rs _k) #:use-module (crates-io))

(define-public crate-rs_keccak_nbits-0.1 (crate (name "rs_keccak_nbits") (vers "0.1.0") (deps (list (crate-dep (name "internal_hasher") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "internal_state") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "n_bit_words") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rs_hasher_ctx") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "074h4djmal3lyp0fllfqb8ixh1cybbvy5sbhkjhp47262x7c3jvi")))

(define-public crate-rs_keccak_nbits-0.1 (crate (name "rs_keccak_nbits") (vers "0.1.1") (deps (list (crate-dep (name "rs_hasher_ctx") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rs_internal_hasher") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rs_internal_state") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rs_n_bit_words") (req "0.1.*") (default-features #t) (kind 0)))) (hash "02c1v64praagdccql8zw5nd72b6ks2rp47imsbf386bj13nhw94k")))

(define-public crate-rs_keccak_nbits-0.1 (crate (name "rs_keccak_nbits") (vers "0.1.2") (deps (list (crate-dep (name "rs_hasher_ctx") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rs_internal_hasher") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rs_internal_state") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rs_n_bit_words") (req "0.1.*") (default-features #t) (kind 0)))) (hash "17jbfd0wzvjw8rzg52lw73flqz0j8vzf2n400a1zrhy73r885s38")))

