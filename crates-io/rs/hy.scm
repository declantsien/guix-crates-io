(define-module (crates-io rs hy) #:use-module (crates-io))

(define-public crate-rshyeong-0.1 (crate (name "rshyeong") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~2.20.1") (features (quote ("color" "wrap_help" "yaml"))) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (features (quote ("bigint" "rational"))) (kind 0)))) (hash "1xw9hpckvpi0izz5hpz2qg1njdir59a9g3xry3sra3bmbq1dfvx1") (features (quote (("big-rational"))))))

(define-public crate-rshyeong-0.2 (crate (name "rshyeong") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~2.20.1") (features (quote ("color" "wrap_help" "yaml"))) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (features (quote ("bigint" "rational"))) (kind 0)))) (hash "0k0x03z92piij4qlin3z1lkafqjr6zhpkgl5j84ab01xn74vms3n") (features (quote (("big-rational"))))))

(define-public crate-rshyeong-0.2 (crate (name "rshyeong") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~2.20.1") (features (quote ("color" "wrap_help" "yaml"))) (kind 0)) (crate-dep (name "num") (req "^0.1.36") (features (quote ("bigint" "rational"))) (kind 0)))) (hash "11kxv56d03kslsjd4h9389dsr7k83xkyn0qmv26p2jrvpi4fzp0d") (features (quote (("big-rational"))))))

(define-public crate-rshyeong-0.3 (crate (name "rshyeong") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("std" "color" "wrap_help" "derive"))) (kind 0)) (crate-dep (name "num-rational") (req "^0.4.0") (features (quote ("num-bigint"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "195hydnj9giv31n6jg5nd1z9g5xjc2aia39p37ilagjp110hx9r4") (features (quote (("big-rational"))))))

