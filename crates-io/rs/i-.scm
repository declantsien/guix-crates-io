(define-module (crates-io rs i-) #:use-module (crates-io))

(define-public crate-rsi-rs-0.1 (crate (name "rsi-rs") (vers "0.1.0") (deps (list (crate-dep (name "sma-rs") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wilders-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "113ss44qijivbwdx22vlqar1lfilm99aqkzdw9sgg3gw72garl2y")))

