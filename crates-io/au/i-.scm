(define-module (crates-io au i-) #:use-module (crates-io))

(define-public crate-aui-core-0.1 (crate (name "aui-core") (vers "0.1.0") (hash "0xwdi8nbffj3sg8q9raqpp6h4lr42ca4lpnnlmd3n7aywk1rww3k")))

(define-public crate-aui-jsx-0.1 (crate (name "aui-jsx") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ghw176cjhhhhv7l8ginckls0pisi6wdl12g384jjgh796lh5nqq")))

