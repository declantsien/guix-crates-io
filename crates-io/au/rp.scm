(define-module (crates-io au rp) #:use-module (crates-io))

(define-public crate-aurpc-0.1 (crate (name "aurpc") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "14q3dfj57i3zkxcp3b3q34cy6ja0hv1nwgz09rnr161jpqnlcncw")))

(define-public crate-aurpc-0.1 (crate (name "aurpc") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1azan475s0r5kaafif787j8p869vpb9ni7ns1d07y5790xb13lm9")))

(define-public crate-aurpc-0.2 (crate (name "aurpc") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0vj0j19n4v06y95w80js6jspadwg0jz2k3ys1banhjc7bawdgsda")))

(define-public crate-aurpc-0.3 (crate (name "aurpc") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1asbg4xkas4xzpkasw8658qqjp3rmpz2451qw6xnka4wzgkj1yaw")))

(define-public crate-aurpc-0.3 (crate (name "aurpc") (vers "0.3.1") (deps (list (crate-dep (name "async-std") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.6.1") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1q2pwc4cs24zsq58pfbj9rm82dk3msdhpq2kkfcg4s8469jzhrf0")))

