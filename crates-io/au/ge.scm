(define-module (crates-io au ge) #:use-module (crates-io))

(define-public crate-augeas-0.0.1 (crate (name "augeas") (vers "0.0.1") (deps (list (crate-dep (name "augeas_sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "06cb504pn9v24qkskxjly559vb7njki7whl2hfdylzqcv9yfdjz4")))

(define-public crate-augeas_sys-0.0.1 (crate (name "augeas_sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1whsvnlrljgg04wqm9n0hvwfxwn64j5avnkiphxpcr1pxsxsm14k")))

(define-public crate-augeas_sys-0.0.2 (crate (name "augeas_sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0czhyk19s7jbl1jw8fpls62ppz5dqlqfblipfqhlxksvb9bqkx6p")))

