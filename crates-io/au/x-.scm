(define-module (crates-io au x-) #:use-module (crates-io))

(define-public crate-aux-config-0.1 (crate (name "aux-config") (vers "0.1.0") (deps (list (crate-dep (name "config") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0j0yhs5x7l82gkqaaia3y6nc23vypybhpwysnsjifvz2l47y5a4y")))

(define-public crate-aux-context-0.1 (crate (name "aux-context") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "0rm0jpadw4c3bdl4r37k4hdsi17yv7x8m65323p75zb7nq0hpyml")))

(define-public crate-aux-enums-0.1 (crate (name "aux-enums") (vers "0.1.0") (hash "1801bc55hixb8095ngswxbrh0nlr1qp2hlv26sx42hfz1p6p5xg6")))

(define-public crate-aux-error-0.1 (crate (name "aux-error") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ky2pfz94wx7pa7bcrg2wyi6d06pdzi0k3jvqi2h0b2w4vlawgxd")))

(define-public crate-aux-i18n-0.1 (crate (name "aux-i18n") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "fluent") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "fluent-bundle") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "intl-memoizer") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0drzm66d7wq9sxsr4x22g1cacpg248y2v66dnbplpdyqxjf6zy8z")))

(define-public crate-aux-logid-0.1 (crate (name "aux-logid") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0w4pav0slhsrqjf1vhkqd84n4gy6b46xwq6axwcz2w4dnysy0ph2")))

