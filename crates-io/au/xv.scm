(define-module (crates-io au xv) #:use-module (crates-io))

(define-public crate-auxv-0.1 (crate (name "auxv") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.41") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 1)))) (hash "0xmlz8nq7a5zy8lhnzi2r3ws6p7r16gsb759ss2p4n1a3na7pm78") (features (quote (("auxv-64bit-ulong") ("auxv-32bit-ulong"))))))

(define-public crate-auxv-0.2 (crate (name "auxv") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.41") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 1)))) (hash "0wf7i5p9nz511kplihd2zfjlibz3i6g7j2ql2l2v8ynsa76g8axi") (features (quote (("auxv-64bit-ulong") ("auxv-32bit-ulong"))))))

(define-public crate-auxv-0.3 (crate (name "auxv") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.41") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 2)))) (hash "0giphs6vn3fg40g942x5jdsph04jh1hm3dadg9p1mgjhnn2gyfk0")))

(define-public crate-auxv-0.3 (crate (name "auxv") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.41") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 2)))) (hash "19lp5qh8lfkxm27qz81qj55fi9s2i72h7ggkrw9kdbbbsirkwpvq")))

(define-public crate-auxv-0.3 (crate (name "auxv") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "^0.3.41") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.19") (default-features #t) (kind 2)))) (hash "0gasmz0pkl6qra2r5c039xgb09maxrv1ra4z741gpvxqpvwk0175")))

