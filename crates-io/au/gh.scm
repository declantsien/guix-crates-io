(define-module (crates-io au gh) #:use-module (crates-io))

(define-public crate-augh-0.1 (crate (name "augh") (vers "0.1.0") (hash "1vgvhcz91gzl5h4vcg8v8dypxrmma9fzfryp0l13hg80v44axf99")))

(define-public crate-augh-0.1 (crate (name "augh") (vers "0.1.1") (hash "1sm648n12d4jny3dqxcnmbvc6zfrzlqai01z4qjwqjs7qg3brn31")))

(define-public crate-aught_common-0.1 (crate (name "aught_common") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "0v2p3kap5qzym0cahxybyx6wx1fbhzidfxj2y8s9hpjjaf1s43yk")))

