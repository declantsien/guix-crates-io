(define-module (crates-io au da) #:use-module (crates-io))

(define-public crate-audact-0.1 (crate (name "audact") (vers "0.1.0") (deps (list (crate-dep (name "cpal") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ga3vf5jakispbvpm46namjj8xpccf7r4r4linl62blwq2inkdw9")))

(define-public crate-audact-0.2 (crate (name "audact") (vers "0.2.0") (deps (list (crate-dep (name "cpal") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "110zz22y0m2r09yl9daahlldy6wjxgylb51hv42iwp265iyrx3xq")))

(define-public crate-audact-0.3 (crate (name "audact") (vers "0.3.0") (deps (list (crate-dep (name "cpal") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0jffahlbg4sys9lsa57qcpjky1g8b2d9wa1kw3fdpv2pq9nbj4zq")))

(define-public crate-audact-0.4 (crate (name "audact") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.5") (default-features #t) (kind 0)))) (hash "0kgszd70m0aw53f8yw3qdgh1fzmai7rhbk7j7zvqj8hk1bjyhkb4")))

(define-public crate-audact-0.5 (crate (name "audact") (vers "0.5.0") (deps (list (crate-dep (name "derive_builder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.6") (default-features #t) (kind 0)))) (hash "1djg9gfmjd2r11nyww8k0hmym93604bs9yz3h704s615iba3sji4")))

(define-public crate-audact-0.5 (crate (name "audact") (vers "0.5.1") (deps (list (crate-dep (name "derive_builder") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.12") (default-features #t) (kind 0)))) (hash "0dxdwprvgcxr1n54gpzikchfdix7g1dkfa1rw5z8k85g51fz1awq")))

