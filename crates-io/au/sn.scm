(define-module (crates-io au sn) #:use-module (crates-io))

(define-public crate-ausnd-0.5 (crate (name "ausnd") (vers "0.5.0") (deps (list (crate-dep (name "audio-codec-algorithms") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "1jj5iyf7xnnn8rz7z3gpq47mqk5lbz0bwyqvvjdxxhs7a5rdqfmm")))

(define-public crate-ausnd-0.5 (crate (name "ausnd") (vers "0.5.1") (deps (list (crate-dep (name "audio-codec-algorithms") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "08mcqlzkl4k1xxzgwfm73v7pk0s3378ml4gkzxcz9x5n9rzgmb9i")))

