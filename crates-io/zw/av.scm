(define-module (crates-io zw av) #:use-module (crates-io))

(define-public crate-zwave-0.0.1 (crate (name "zwave") (vers "0.0.1") (deps (list (crate-dep (name "serial") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vq69222hrliskkylml1nsmiibl0ng71yx7g3a9lkmc8bd6gzvdc")))

