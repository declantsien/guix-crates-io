(define-module (crates-io zw nb) #:use-module (crates-io))

(define-public crate-zwnbsp-0.0.0 (crate (name "zwnbsp") (vers "0.0.0") (hash "0s27qm6hn3wdsvk8x4yndi8qk34zavh1dj8f4f8xfiskg06z22fl")))

(define-public crate-zwnbsp-0.0.1 (crate (name "zwnbsp") (vers "0.0.1") (hash "1qmhma5f0p2kjxzrjcgypri3i4vnylzi9p0qk4b8bprj9fsqm4w5")))

