(define-module (crates-io r2 d2) #:use-module (crates-io))

(define-public crate-r2d2-0.0.1 (crate (name "r2d2") (vers "0.0.1") (hash "1wd0gh13ghjp65ci8n72yia4f815lh76ckczv1c49c79fdm28rnl")))

(define-public crate-r2d2-0.1 (crate (name "r2d2") (vers "0.1.0") (hash "0nci1i0f9qw911456lwvc07f5sbi76j62bl5c4ird1vsnylbx2wz")))

(define-public crate-r2d2-0.1 (crate (name "r2d2") (vers "0.1.1") (hash "1c93vdfiz19csxcb3dk4gicmhzfi9339czk1chvbxid66wap52zx")))

(define-public crate-r2d2-0.1 (crate (name "r2d2") (vers "0.1.2") (hash "1rxm6x6q57ik9k0134vrpff4nkrpwr9bcr3bl2lz31zap9rfhbpb")))

(define-public crate-r2d2-0.2 (crate (name "r2d2") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1isyli270hm7vwy7hp08s0w2hdmcykzm7ya3b4ffpfincj30b3iy")))

(define-public crate-r2d2-0.2 (crate (name "r2d2") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.1") (default-features #t) (kind 0)))) (hash "05gj7av9v5f26jjyb2z2kp0mpf0lgkvpjqzhf829rc1qk82vsm2y")))

(define-public crate-r2d2-0.2 (crate (name "r2d2") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1679la6z7pqgvdwzr1cp8zxnhsjik1mgv6jakj6pl0shb50fzjp0")))

(define-public crate-r2d2-0.2 (crate (name "r2d2") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qb3y380hig4b9h3q6cw29zfhkmmnbvmm3ipsflvbpdh0rhg615v")))

(define-public crate-r2d2-0.2 (crate (name "r2d2") (vers "0.2.4") (deps (list (crate-dep (name "log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qdrz2ankz58qzj0g7066vri3ywqcvsb2yhkfggh4f0pnv3bilpb")))

(define-public crate-r2d2-0.2 (crate (name "r2d2") (vers "0.2.5") (deps (list (crate-dep (name "log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h59khw24kg481adffih9r7xihpfyss1x2ckq1gv5hh7bj40h2p0")))

(define-public crate-r2d2-0.3 (crate (name "r2d2") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0rjz0kh8p3f4iq46dri0mf9vg5v2wbqzjcvwifkkhja3lli4mwdb")))

(define-public crate-r2d2-0.4 (crate (name "r2d2") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "14ppvj9mn3wrpfmwcdmps269d60hqmpzgn2mya0yg81cssiyyc2j")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "02zydfrsyfnivm6k88jjpkky890bpfjp02r6y1l5adj7zbjj5svf")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1ak9jjm9y3lg9zv60pcl920fqs2dy63s86jc6z6s6nz9v5mbkahs")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.2") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0ad1bb0fci0w8zq62250g52jfnpdhzli5is8vjiwsvvr75isbz8h")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.3") (deps (list (crate-dep (name "log") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "01d36p9a1iz7hdnhy22n41wxk7kjgwlambl8611mpp5airls68pg")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.4") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0b109iy9zd6h7wy6sh32dwfyx4zgmxl4hah3knk1drwicss1hr9f")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.5") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (features (quote ("std-duration"))) (default-features #t) (kind 0)))) (hash "0jidhp8fvs8cwzh7jpd94q3wn6sw15v1d56m3k1cfgxnvi0sb7wn")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.6") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (features (quote ("std-duration"))) (default-features #t) (kind 0)))) (hash "0d6mzwpbdx5c7fj7c8jv5zy8940k3a9sy06jgb86jrxcba9n9r7q")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.7") (deps (list (crate-dep (name "debug-builders") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "116lab542xq4m16n0s92zi6ww54ab4mnn2yrg02154bg3xllw1qb")))

(define-public crate-r2d2-0.5 (crate (name "r2d2") (vers "0.5.8") (deps (list (crate-dep (name "debug-builders") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1myfbfgwl8gdf5b7vrwlwlmagphkkxjfpa5iz4fqk22fwz3gy8k2")))

(define-public crate-r2d2-0.6 (crate (name "r2d2") (vers "0.6.0") (deps (list (crate-dep (name "debug-builders") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "12d3ghbl691kijvawykxs9mac01j7jihmmk4zv8aamxigm1vv6ws")))

(define-public crate-r2d2-0.6 (crate (name "r2d2") (vers "0.6.1") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1bcyqcdk5in78py7pv0rn1bncizm5qv5lkfadcbc6x11i9wg6r5r")))

(define-public crate-r2d2-0.6 (crate (name "r2d2") (vers "0.6.2") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1ndnsbc1jn7raf997i81mpdzk746q54v1834409zpfli6ayr911p")))

(define-public crate-r2d2-0.6 (crate (name "r2d2") (vers "0.6.3") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "05i5gf2ldg1xyn8rg2qds76hpadap8kx34phxhh81xbygr3prnnm")))

(define-public crate-r2d2-0.6 (crate (name "r2d2") (vers "0.6.4") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "029g7677ax53grlqaw3n6y2cj2mmsxqqmi3ddzagfx1vyqybcm6i")))

(define-public crate-r2d2-0.7 (crate (name "r2d2") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1g4pvpp7kznjxam0fb38cfi7c8ds922hw52wbha6acavcpb7sg56")))

(define-public crate-r2d2-0.7 (crate (name "r2d2") (vers "0.7.1") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cmdlgfy88dk334x6ns04lzjam5d7lbyyz689ri6cbmy0cdyvksf")))

(define-public crate-r2d2-0.7 (crate (name "r2d2") (vers "0.7.2") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ywp7ynj6qisix3kyzwrsyypmi07z848dyvz33z3rn0fkp14im0x")))

(define-public crate-r2d2-0.7 (crate (name "r2d2") (vers "0.7.3") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jnj8rjpqyvm32m306zwgf1j2csx6wxn9riv14h1d3i1id9mlrkc")))

(define-public crate-r2d2-0.7 (crate (name "r2d2") (vers "0.7.4") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kmwwhwfx3zmv9qb49j2ywi0ny644dg3j9rmiw7l9prqid8890ic")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.0") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b8g5qsrlr8ygq6xsc64qm8q10pnawb8nrhx0rf8hkx3xdd9jkv4")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.1") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s8nmqizly78c1zhvh8g3fc7bv5l978kwknqxj3cb5p4pq114qar")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.2") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "0invv0bipra9k36dsrzq2sfvd5cjvkvv4fq88b8qwmm5m2k8q1zr")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.3") (deps (list (crate-dep (name "antidote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "08mxwyrd0p6xaglxr32b7gdhvsalb19sswzzlz79rcfsl346yx2x")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "1490s6rmj0v80sxq2ncnnkfndx69pg6mz4j8538a908j4n9s5n4x")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.5") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "06zq354jfkhnkg0hx1bj8l84kncds7aixgjbl2r7yi7lv5swwhmw")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.6") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "07c65xpzbixik2in4kp4ynn8549z91hqykabxsv8c0pgk14ad3z4")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.7") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lsmnxn18lbjnwykj8rhsri1a3if85xk71rqy1rl61msbzycbd92")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.8") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bw50kp6im2asi52bj9sb921xkjb2xaqwzbn8254m3ilal4f95ql")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.9") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vxjgh83bss63mkx308p16iwl33s80c781p422f3r5w0p315np2l")))

(define-public crate-r2d2-0.8 (crate (name "r2d2") (vers "0.8.10") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "scheduled-thread-pool") (req "^0.2") (default-features #t) (kind 0)))) (hash "14qw32y4m564xb1f5ya8ii7dwqyknvk8bsx2r0lljlmn7zxqbpji")))

(define-public crate-r2d2-alpm-0.1 (crate (name "r2d2-alpm") (vers "0.1.0") (deps (list (crate-dep (name "alpm") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "alpm-utils") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pacmanconf") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "0nhp63igjix1cvkfpvx620sfvgp2krk1f752k62nywsli7dk5a22")))

(define-public crate-r2d2-alpm-0.2 (crate (name "r2d2-alpm") (vers "0.2.0") (deps (list (crate-dep (name "alpm") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "alpm-utils") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pacmanconf") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8") (default-features #t) (kind 2)))) (hash "1i9j3dr5i3d7fqm9bpqd7jx8jgr0131kqsyyjnggd3yd7v4ilw69")))

(define-public crate-r2d2-aykroyd-0.1 (crate (name "r2d2-aykroyd") (vers "0.1.0") (deps (list (crate-dep (name "aykroyd") (req "^0.2") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "r2d2_postgres") (req "^0.18") (default-features #t) (kind 0)))) (hash "1n4kn8awzlx89sbi3g3ahbpwpg1cy3jbrdca67jldhsz1wlkg4sl")))

(define-public crate-r2d2-aykroyd-0.1 (crate (name "r2d2-aykroyd") (vers "0.1.1") (deps (list (crate-dep (name "aykroyd") (req "^0.2") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "r2d2_postgres") (req "^0.18") (default-features #t) (kind 0)))) (hash "1l1v30sx6q6fsswvha6a2kg7s9d5d7vdarqvx6p8ykhrcp20hygk")))

(define-public crate-r2d2-aykroyd-0.2 (crate (name "r2d2-aykroyd") (vers "0.2.0") (deps (list (crate-dep (name "aykroyd") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "aykroyd") (req "^0.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "mysql") (req "^24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql") (req "^24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2_postgres") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2_sqlite") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30") (optional #t) (default-features #t) (kind 0)))) (hash "1npdvayc59b158qbl4v38yjss4xay65ygyqjc23q3c82jjlq4134") (features (quote (("default")))) (v 2) (features2 (quote (("rusqlite" "aykroyd/rusqlite" "dep:rusqlite" "dep:r2d2_sqlite") ("postgres" "aykroyd/postgres" "dep:postgres" "dep:r2d2_postgres") ("mysql" "aykroyd/mysql" "dep:mysql" "dep:r2d2_mysql"))))))

(define-public crate-r2d2-aykroyd-0.2 (crate (name "r2d2-aykroyd") (vers "0.2.1") (deps (list (crate-dep (name "aykroyd") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "aykroyd") (req "^0.3") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "mysql") (req "^24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql") (req "^24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2_postgres") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2_sqlite") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30") (optional #t) (default-features #t) (kind 0)))) (hash "1kzcyy5n0d45kinivpbcl4lm2lc0kqdfchpa9176g1rvi11ayvqr") (features (quote (("default")))) (v 2) (features2 (quote (("rusqlite" "aykroyd/rusqlite" "dep:rusqlite" "dep:r2d2_sqlite") ("postgres" "aykroyd/postgres" "dep:postgres" "dep:r2d2_postgres") ("mysql" "aykroyd/mysql" "dep:mysql" "dep:r2d2_mysql"))))))

(define-public crate-r2d2-beanstalkd-0.1 (crate (name "r2d2-beanstalkd") (vers "0.1.0") (deps (list (crate-dep (name "beanstalkc") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "1gzg7lq6f27ad25z17ib1k2qcg1a6jkvmcmd961f748gmq622p1h")))

(define-public crate-r2d2-bolt-0.0.0 (crate (name "r2d2-bolt") (vers "0.0.0") (deps (list (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "14s8armnmh8annfpw16161b51kp95qb2gm1zc6bsj8hx28gvvhqn") (yanked #t)))

(define-public crate-r2d2-cryptoki-0.1 (crate (name "r2d2-cryptoki") (vers "0.1.0") (deps (list (crate-dep (name "backoff") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "cached") (req "^0.42.0") (default-features #t) (kind 2)) (crate-dep (name "cryptoki") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "loom") (req "^0.5.6") (default-features #t) (kind 2)) (crate-dep (name "r2d2") (req "^0.8.10") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19qbggw3dp09pn5blxd9im8lypry2f6jlmfb4rc98b7lbx5mcspc") (features (quote (("serde" "zeroize/serde"))))))

(define-public crate-r2d2-cryptoki-0.2 (crate (name "r2d2-cryptoki") (vers "0.2.0") (deps (list (crate-dep (name "backoff") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "cached") (req "^0.44.0") (default-features #t) (kind 2)) (crate-dep (name "cryptoki") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "loom") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "r2d2") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "11hag0ap4lyqhmnigc3a1xrgyzypp631wafy721yxga4ml86mp47") (features (quote (("serde" "cryptoki/serde"))))))

(define-public crate-r2d2-cryptoki-0.2 (crate (name "r2d2-cryptoki") (vers "0.2.1") (deps (list (crate-dep (name "backoff") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "cached") (req "^0.46.0") (default-features #t) (kind 2)) (crate-dep (name "cryptoki") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "loom") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "r2d2") (req "^0.8.10") (default-features #t) (kind 0)))) (hash "0h4jmdlpax1ra8zzvvgn6wd3q5nsnbcn5djkhzprwvd4whj7m9hv") (features (quote (("serde" "cryptoki/serde"))))))

(define-public crate-r2d2-diesel-0.4 (crate (name "r2d2-diesel") (vers "0.4.0") (deps (list (crate-dep (name "diesel") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1niwcy8cg703cf2b0kzmz112ira3h6zihk2bzj73407sgp5cajw4")))

(define-public crate-r2d2-diesel-0.5 (crate (name "r2d2-diesel") (vers "0.5.0") (deps (list (crate-dep (name "diesel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0dq41znyb0x6vck12fizzkda138mz068a88dfj6zcnvicspfncdn")))

(define-public crate-r2d2-diesel-0.6 (crate (name "r2d2-diesel") (vers "0.6.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.7.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "04p78hjh6yxfdpbl414kkf8b8jghkmpbbz7r6xdj0dscimmv0dy0")))

(define-public crate-r2d2-diesel-0.7 (crate (name "r2d2-diesel") (vers "0.7.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.8.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "11p8kgcw0w7b7isqph54z2wgnni9r56lv94zbfy7xgkdgdlqjc3v")))

(define-public crate-r2d2-diesel-0.7 (crate (name "r2d2-diesel") (vers "0.7.1") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.8.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "16z3aksi7x3j48q2ld4lkyxblkw6ilm0nwabkycdq1plwy10ca0s") (features (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.8 (crate (name "r2d2-diesel") (vers "0.8.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.9.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1k6pd9b46im749v5hrcjjavji2rx6vh3s9vfry20xcc1ii7xmnmc") (features (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.9 (crate (name "r2d2-diesel") (vers "0.9.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.10.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1c0ps3jzk9gm0i2da7dkqqd71rm1fj2iv923bghqc8h75admpkla") (features (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.10 (crate (name "r2d2-diesel") (vers "0.10.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.11.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "15bl9xk1a33w1wykk68ivvvg1mn85nsa7k2d82xgnf3vg8r2aa96") (features (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.11 (crate (name "r2d2-diesel") (vers "0.11.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.12.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1kna13dxl6b9gd1awmh54s8x1sp5kydxapx68jwwa6m924nk6j8m") (features (quote (("sqlite" "diesel/sqlite") ("default"))))))

(define-public crate-r2d2-diesel-0.12 (crate (name "r2d2-diesel") (vers "0.12.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.13.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "00j4g9zjhgw4pd4l9d260naq5619js8ji88jhm01rk7n2b13bsjb")))

(define-public crate-r2d2-diesel-0.13 (crate (name "r2d2-diesel") (vers "0.13.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.14.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1ifnib0w911v0smxwmjv3d1i4i1aavyvjgi3qj1xgg48p7k49gqv")))

(define-public crate-r2d2-diesel-0.14 (crate (name "r2d2-diesel") (vers "0.14.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.15.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0rf0z7n40vywfi8i4k3ypq3agakf8qwcscl5828bksq3hamx1lb0")))

(define-public crate-r2d2-diesel-0.15 (crate (name "r2d2-diesel") (vers "0.15.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.16.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0d7hsrzxsjbalgms4vfz1bzi0wy338js06lg8jfszpg2c82d98hc")))

(define-public crate-r2d2-diesel-0.16 (crate (name "r2d2-diesel") (vers "0.16.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 0.17.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0y04dhnqqicr3zd4ylj7dipvb7rvjznm46yjjq99jibcd9lj3fgn")))

(define-public crate-r2d2-diesel-0.99 (crate (name "r2d2-diesel") (vers "0.99.0") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, < 1.0.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req ">= 0.7, < 0.9") (default-features #t) (kind 0)))) (hash "0rnqvnhh9cb7s6qkzg61hy3lv8xjw5cz69s4cr5hywl2k8afvakp")))

(define-public crate-r2d2-diesel-1 (crate (name "r2d2-diesel") (vers "1.0.0-beta1") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, <= 1.0.0-beta1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req ">= 0.7, < 0.9") (default-features #t) (kind 0)))) (hash "0ckqvzz9w6cvysila38mqs6vw5qsxh1v094mb03pl9n2ligk8g4j")))

(define-public crate-r2d2-diesel-1 (crate (name "r2d2-diesel") (vers "1.0.0-rc1") (deps (list (crate-dep (name "diesel") (req ">= 0.5.0, <= 1.0.0-rc1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req ">= 0.7, < 0.9") (default-features #t) (kind 0)))) (hash "1f2ykfbd71w9n925s56as8ql2kmn50vqzihply2lj23zmrcn3mf0")))

(define-public crate-r2d2-diesel-1 (crate (name "r2d2-diesel") (vers "1.0.0") (deps (list (crate-dep (name "diesel") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req ">= 0.7, < 0.9") (default-features #t) (kind 0)))) (hash "18xi9xp12hqwf4gbkyjclxpzx8y3xd9080icph16v9rdv6x2k77b")))

(define-public crate-r2d2-duckdb-0.1 (crate (name "r2d2-duckdb") (vers "0.1.0") (deps (list (crate-dep (name "duckdb") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "12cj2hmm8q8ji7pxg5s6khhzhm884v844b37hr4yxhr6ca4mc6ds")))

(define-public crate-r2d2-influx_db_client-0.1 (crate (name "r2d2-influx_db_client") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "influx_db_client") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "00fxdvwzgryv3rm5amylvah90ycx6zkamjx1d8ki5nnff75xpkby")))

(define-public crate-r2d2-influx_db_client-0.1 (crate (name "r2d2-influx_db_client") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "influx_db_client") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1zxpz26inf409ji8vjvvdy21xymsci94h2d34v4bp53ycg0mvva7")))

(define-public crate-r2d2-influxdb-0.1 (crate (name "r2d2-influxdb") (vers "0.1.0") (deps (list (crate-dep (name "influxdb") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "128szi3m6i1y6c284paq63fhwgkr6j4x9jp7i7h8g6saz43mbrag")))

(define-public crate-r2d2-jfs-0.0.1 (crate (name "r2d2-jfs") (vers "0.0.1") (deps (list (crate-dep (name "jfs") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "14bxz5zh8pc4ssndi0zwzdrfbap89yswhm4v9fh81gg5x0mhdkbm")))

(define-public crate-r2d2-jfs-0.1 (crate (name "r2d2-jfs") (vers "0.1.0") (deps (list (crate-dep (name "jfs") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0x4x419i5cls0scil5xi05v408l2l49wj1g1qazzz0qv0pydms7r")))

(define-public crate-r2d2-jfs-0.2 (crate (name "r2d2-jfs") (vers "0.2.0") (deps (list (crate-dep (name "jfs") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "15hvqlgb3pb6fqjja7qhw20qwz1qn44f6d3y3php3p0japravmnz")))

(define-public crate-r2d2-lapin-0.1 (crate (name "r2d2-lapin") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "futures-executor") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-threaded" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-amqp") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "015g95yhhphkdg95bi44mw5wrnfj15ih5szza6bl1apfmr1ngzrn")))

(define-public crate-r2d2-lapin-0.2 (crate (name "r2d2-lapin") (vers "0.2.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "futures-executor") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-threaded" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-amqp") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "1zir54qv7fhgaxwkd6n71h3j7zbcpx54rxbyr81avvrlnkvxlbgb")))

(define-public crate-r2d2-lapin-0.2 (crate (name "r2d2-lapin") (vers "0.2.1") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "futures-executor") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-threaded" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-amqp") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "06ryisk9r6pph54ww6cxay5bp95rkza5al0w2zmdw6zvqfy9899y")))

(define-public crate-r2d2-lapin-0.2 (crate (name "r2d2-lapin") (vers "0.2.2") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "futures-executor") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "lapin") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("rt-threaded" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-amqp") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "1c6xm3srwlfj038z9nw0mrl772bapp00d1ph3a4bnwc86mgfd3ri")))

(define-public crate-r2d2-ldap-0.1 (crate (name "r2d2-ldap") (vers "0.1.0") (deps (list (crate-dep (name "ldap3") (req "^0.8") (features (quote ("sync"))) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "154qh8fn4vlzsq1f788w5pz7c2lpvf51p0jy12im8mdjxqqz0i1r")))

(define-public crate-r2d2-ldap-0.1 (crate (name "r2d2-ldap") (vers "0.1.1") (deps (list (crate-dep (name "ldap3") (req "^0.8") (features (quote ("sync"))) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0vxjn1k2l19dqdghk6r2lyswckm6v59ip05l9xf91ggrf3n7g304")))

(define-public crate-r2d2-memcache-0.1 (crate (name "r2d2-memcache") (vers "0.1.0") (deps (list (crate-dep (name "memcache") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)))) (hash "018w4f5rbfpmis0lx3iv4c6laa6w4xv788vggwjcjaqc3vrkq5db")))

(define-public crate-r2d2-memcache-0.1 (crate (name "r2d2-memcache") (vers "0.1.1") (deps (list (crate-dep (name "memcache") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)))) (hash "0if09wzqqbhxq4vgvh3274dgc0hf9nvfwcacg7slljsyx6qxgyf0")))

(define-public crate-r2d2-memcache-0.1 (crate (name "r2d2-memcache") (vers "0.1.2") (deps (list (crate-dep (name "memcache") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)))) (hash "0slp57qsmwajvzh0qyjfcaz3pf1c157k5zar70ir0wa2z2zmfrq7")))

(define-public crate-r2d2-memcache-0.2 (crate (name "r2d2-memcache") (vers "0.2.0") (deps (list (crate-dep (name "memcache") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ya8yh2z88ypq8bb1mjxy8al0gjadjif9pa15j28scfkl3j5lxbl")))

(define-public crate-r2d2-memcache-0.2 (crate (name "r2d2-memcache") (vers "0.2.1") (deps (list (crate-dep (name "memcache") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1n72zi06b9ff5ky37n76hjf33pgrll1rmz9d11vhsj3qjmky2pgr")))

(define-public crate-r2d2-memcache-0.3 (crate (name "r2d2-memcache") (vers "0.3.0") (deps (list (crate-dep (name "memcache") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0g84qlymwq9k56xrfvg0jf8blw9dd5pjinxk1qgnf6lh315vb6kf")))

(define-public crate-r2d2-memcache-0.3 (crate (name "r2d2-memcache") (vers "0.3.1") (deps (list (crate-dep (name "memcache") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "13wwgg4md2f2pr85pm06ll17sqbgdd2jv6g16nx4p5hmw6r6hgi3")))

(define-public crate-r2d2-memcache-0.3 (crate (name "r2d2-memcache") (vers "0.3.2") (deps (list (crate-dep (name "memcache") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "07kawqxf8m9akgpa4fjn8xpn7qzw8pfxgp28v2mwlxk7imrvshx9")))

(define-public crate-r2d2-memcache-0.4 (crate (name "r2d2-memcache") (vers "0.4.0") (deps (list (crate-dep (name "memcache") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0f7gqlmrxhpm6l4i0liyhfywm2yk1sf62ijvz7qh2bfnhd2ilhk5")))

(define-public crate-r2d2-memcache-0.5 (crate (name "r2d2-memcache") (vers "0.5.0") (deps (list (crate-dep (name "memcache") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "17zr81vhvlr818jn11dfmgw5ls7f5vzrfpw2qcwpynnqxd63852i")))

(define-public crate-r2d2-memcache-0.6 (crate (name "r2d2-memcache") (vers "0.6.0") (deps (list (crate-dep (name "memcache") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0h7n37c5fsvdfvj9arhqsvn1acp2vayy9wbbzszhj6d18jydgfbl")))

(define-public crate-r2d2-mongodb-0.1 (crate (name "r2d2-mongodb") (vers "0.1.0") (deps (list (crate-dep (name "mongodb") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0cl73nixafijxbhmhzfrhmc57jacl3hk5r773kqd4rgm9r3wg8rf")))

(define-public crate-r2d2-mongodb-0.1 (crate (name "r2d2-mongodb") (vers "0.1.1") (deps (list (crate-dep (name "mongodb") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "16835v9snh5i4gvbcih7ddhmfbnwvsvllp3jhq7rq1ndzb2hpb0m")))

(define-public crate-r2d2-mongodb-0.1 (crate (name "r2d2-mongodb") (vers "0.1.2") (deps (list (crate-dep (name "mongodb") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "05j7lb3zz44kqxwf3a2kx3d3ai4gnigpj2v1m0kjml061b18bvsk")))

(define-public crate-r2d2-mongodb-0.1 (crate (name "r2d2-mongodb") (vers "0.1.3") (deps (list (crate-dep (name "mongodb") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0pkrkxfkxz1skn07r6nxh0aiq04zz8pxp9rxdgk5g0ydzs7ng5g5")))

(define-public crate-r2d2-mongodb-0.2 (crate (name "r2d2-mongodb") (vers "0.2.0") (deps (list (crate-dep (name "mongodb") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0xpa41shw4wyd6gly0ymhvnsgs3b25q2rcjrxwi57m1rrggg3kbz")))

(define-public crate-r2d2-mongodb-0.2 (crate (name "r2d2-mongodb") (vers "0.2.1") (deps (list (crate-dep (name "mongodb") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "13x9mf7xp1rr3x7nsbjklm1g4srijd3mlw125qnzh582m15kbnvw")))

(define-public crate-r2d2-mongodb-0.2 (crate (name "r2d2-mongodb") (vers "0.2.2") (deps (list (crate-dep (name "mongodb") (req "^0.3.12") (features (quote ("ssl"))) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1xadsqkmmy0svvnily1s57xisxnv0hw5mhik795h49j93rnb2f58")))

(define-public crate-r2d2-nats-0.1 (crate (name "r2d2-nats") (vers "0.1.0") (deps (list (crate-dep (name "nats") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0f7kqn583m6lfk9hm6j1p18hfn68b2yi7wxnx59rmrfbrvryyd6q")))

(define-public crate-r2d2-oracle-0.1 (crate (name "r2d2-oracle") (vers "0.1.0") (deps (list (crate-dep (name "oracle") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1vhiqrixwcydjnv772g71iqb65i52pl8q5n7k57wlgx9f4ysp9z5")))

(define-public crate-r2d2-oracle-0.2 (crate (name "r2d2-oracle") (vers "0.2.0") (deps (list (crate-dep (name "oracle") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1ihbhqncs6pr2g10prjidi72s9ji6wgrhdjxcq218kypaa31ph4p")))

(define-public crate-r2d2-oracle-0.3 (crate (name "r2d2-oracle") (vers "0.3.0") (deps (list (crate-dep (name "oracle") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1vc7mms50nm3gyrf14dnbvnp8024c558w2x5lhqivh73yw90ycrv") (features (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.4 (crate (name "r2d2-oracle") (vers "0.4.0") (deps (list (crate-dep (name "oracle") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1rk174gys02hanwzdnl0ggpzzn5nz59qrjma6gw0y9dsx26d2svq") (features (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.4 (crate (name "r2d2-oracle") (vers "0.4.1") (deps (list (crate-dep (name "oracle") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1s5f9vkz1p8gckh9lcl47x5g119rmkmjx3a7qdx4pic1hsjq5akm") (features (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.5 (crate (name "r2d2-oracle") (vers "0.5.0") (deps (list (crate-dep (name "oracle") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0axhv3m0q0v5d68wqmjamd1a9lshv6kbbrrhgdakwhjlra6kb9gc") (features (quote (("chrono" "oracle/chrono"))))))

(define-public crate-r2d2-oracle-0.6 (crate (name "r2d2-oracle") (vers "0.6.0") (deps (list (crate-dep (name "oracle") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0zynd512z823rdsa7a7ghss460r0hx87gj5dlndfpch4kndc54p5") (features (quote (("chrono" "oracle/chrono")))) (rust-version "1.56")))

(define-public crate-r2d2-sqlite3-0.1 (crate (name "r2d2-sqlite3") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "sqlite") (req "^0.23.9") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "19497zk3dbyvwgw5crankmjvg1xhzwvl65x08rs6kdiwa67gqfj5")))

(define-public crate-r2d2-sqlite3-0.1 (crate (name "r2d2-sqlite3") (vers "0.1.1") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sqlite3") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1y7xzb62zjszkaf5arm1cx10403b9fyqm07g4rjs8a65p0h3bgc0")))

(define-public crate-r2d2-testconnection-0.1 (crate (name "r2d2-testconnection") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)))) (hash "07zhn9z52hcdgqffdjbiqzyjq845kjqj0wiq2s5bgwpqj57i1mf2") (yanked #t)))

(define-public crate-r2d2_arangors-0.1 (crate (name "r2d2_arangors") (vers "0.1.0") (deps (list (crate-dep (name "arangors") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0l527ciqpi22371kq6f1hwdqkvzk5jhpbpw997hxmzr8h1mqj5ll")))

(define-public crate-r2d2_arangors-0.2 (crate (name "r2d2_arangors") (vers "0.2.0") (deps (list (crate-dep (name "arangors") (req "^0.3") (features (quote ("reqwest_blocking"))) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1mcgr0yr1l2gx7dnwm8h9swh9kgsdrm7fjwz1x36q0zhp4qs75l2")))

(define-public crate-r2d2_arangors-0.2 (crate (name "r2d2_arangors") (vers "0.2.1") (deps (list (crate-dep (name "arangors") (req ">=0.3") (features (quote ("reqwest_blocking"))) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0d2vvjfm4s8zz9db8xsyv3bsikidjx9bvz5biyf1lb8iwfjffsih")))

(define-public crate-r2d2_arangors-0.2 (crate (name "r2d2_arangors") (vers "0.2.2") (deps (list (crate-dep (name "arangors") (req ">=0.4.3") (features (quote ("reqwest_blocking"))) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1qw7cnhwc8ms23lc3rhck02gyqf02jk1fm1n5062xnnln2lm93iq")))

(define-public crate-r2d2_arangors-0.2 (crate (name "r2d2_arangors") (vers "0.2.3") (deps (list (crate-dep (name "arangors") (req "^0.5") (features (quote ("reqwest_blocking"))) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "uclient") (req "^0.1") (features (quote ("blocking_reqwest"))) (kind 0)))) (hash "0b2srshxgl02mhyz77siyx3hzq07dsi1dbwpv78i1p0zxbrnbjw0")))

(define-public crate-r2d2_couchdb-0.1 (crate (name "r2d2_couchdb") (vers "0.1.0") (deps (list (crate-dep (name "chill") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "04fk7b73ixqcmlpvphlhwn9lqqzrv3152w3rmhin8r7v7akyap2c")))

(define-public crate-r2d2_cypher-0.0.1 (crate (name "r2d2_cypher") (vers "0.0.1") (deps (list (crate-dep (name "r2d2") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rusted_cypher") (req "*") (default-features #t) (kind 0)))) (hash "1c30db6smmgyp1643cc4793j6p6b4mwaimqc8bmhgb5qwwpb2g15")))

(define-public crate-r2d2_cypher-0.0.2 (crate (name "r2d2_cypher") (vers "0.0.2") (deps (list (crate-dep (name "r2d2") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rusted_cypher") (req "*") (default-features #t) (kind 0)))) (hash "0f9h2lxampl3p1a2m16ywx644ar3ws3j513g6qc1yn1i78l8pd6x")))

(define-public crate-r2d2_cypher-0.1 (crate (name "r2d2_cypher") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusted_cypher") (req "^0.9") (default-features #t) (kind 0)))) (hash "19a34km231bx6rwrm1brckkyy8pvyqxh1x0rl6kwfii5iz4p7qgj")))

(define-public crate-r2d2_cypher-0.2 (crate (name "r2d2_cypher") (vers "0.2.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusted_cypher") (req "^1.0") (default-features #t) (kind 0)))) (hash "10fcps6x0apg47xaay05fb9s54mqclngwpknq7dxk9jdbvsxpc25")))

(define-public crate-r2d2_cypher-0.3 (crate (name "r2d2_cypher") (vers "0.3.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusted_cypher") (req "^1.1") (default-features #t) (kind 0)))) (hash "079jzrna25zhl3qr4gvbwrdr19cbxmmh98s9yasx5kgap9sm78k9")))

(define-public crate-r2d2_cypher-0.4 (crate (name "r2d2_cypher") (vers "0.4.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusted_cypher") (req "^1.1") (default-features #t) (kind 0)))) (hash "144maik2k1jhk8ijm31ak1s5jrkgylij4ms8lj922hr9xxvd54g4")))

(define-public crate-r2d2_firebird-0.15 (crate (name "r2d2_firebird") (vers "0.15.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.15") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.15") (features (quote ("pure_rust" "date_time"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.15") (default-features #t) (kind 0)))) (hash "0hjkmy7ww8kh11qxdqianm1haykib940wv3m2r0q5ysi2gcpz04b")))

(define-public crate-r2d2_firebird-0.16 (crate (name "r2d2_firebird") (vers "0.16.0") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.16") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.16") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.16") (default-features #t) (kind 0)))) (hash "1wjvcf0gq106jj7x2gxwlmpf7y6xx2g082wbrvarviq00kmx5w6n")))

(define-public crate-r2d2_firebird-0.17 (crate (name "r2d2_firebird") (vers "0.17.0") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.17") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.17") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.17") (default-features #t) (kind 0)))) (hash "16harjlg52rfm34nh9sl87v0q6ni679fxa9r6q5pps9grps3nfyj")))

(define-public crate-r2d2_firebird-0.19 (crate (name "r2d2_firebird") (vers "0.19.0") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.19") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.19") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.19") (default-features #t) (kind 0)))) (hash "1wcd6gsi2nsan96yi77ixs20mlqdnfvy6dl5i8z0xqdgcy7nvldi")))

(define-public crate-r2d2_firebird-0.20 (crate (name "r2d2_firebird") (vers "0.20.0") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.20") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.20") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.20") (default-features #t) (kind 0)))) (hash "0rajmwbszzgylxb2zlzxdvpbmrkc6g0acl3ix8bw8pd0dv3xaqbj")))

(define-public crate-r2d2_firebird-0.23 (crate (name "r2d2_firebird") (vers "0.23.0") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.23") (default-features #t) (kind 0)))) (hash "1sk5sbngb5gmsjnvbqpnlyfgnkj7pdqam1vciamwjwayamk6sww7")))

(define-public crate-r2d2_firebird-0.23 (crate (name "r2d2_firebird") (vers "0.23.1") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.23") (default-features #t) (kind 0)))) (hash "0y3vgjwgq2j0mywxrkwl6p8z6831vfzj7y5vik28h0iyv3x8792x")))

(define-public crate-r2d2_firebird-0.23 (crate (name "r2d2_firebird") (vers "0.23.2") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.23") (default-features #t) (kind 0)))) (hash "078y9f2kxijimva755w1ds9fid450h3xgf4gsf5zix1f2cl5d0cy")))

(define-public crate-r2d2_firebird-0.24 (crate (name "r2d2_firebird") (vers "0.24.0") (deps (list (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (kind 0)) (crate-dep (name "rsfbclient") (req "^0.23") (features (quote ("pure_rust"))) (kind 2)) (crate-dep (name "rsfbclient-core") (req "^0.23") (default-features #t) (kind 0)))) (hash "155l2l7cnva6zbyaxypdywhdxm3imkm08rl7w6h8977gfqrrg806")))

(define-public crate-r2d2_gluster-0.1 (crate (name "r2d2_gluster") (vers "0.1.0") (deps (list (crate-dep (name "gfapi-sys") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "~0.8") (default-features #t) (kind 0)))) (hash "0jcpa486gzir37dd62ji1qlk7wx1sv44x2h32m6fhibx2h9d9052")))

(define-public crate-r2d2_gluster-0.1 (crate (name "r2d2_gluster") (vers "0.1.1") (deps (list (crate-dep (name "gfapi-sys") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "~0.8") (default-features #t) (kind 0)))) (hash "1rpy1mmp8ygixzdkc8i3p6lgjhszjyw5vgcb9ziizj3ia9dvjjk3")))

(define-public crate-r2d2_mysql-0.1 (crate (name "r2d2_mysql") (vers "0.1.1") (deps (list (crate-dep (name "mysql") (req "^0.15.0") (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0n8vy31ajmap6kq6349fpb0783akv3m2mmbjlbgkadmkfgv5wj1i")))

(define-public crate-r2d2_mysql-0.2 (crate (name "r2d2_mysql") (vers "0.2.0") (deps (list (crate-dep (name "mysql") (req "^0.16.0") (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "14l36pcm6622lhbkhlqzk3z6hl3225cg39qzdf23ami67vkhaaan")))

(define-public crate-r2d2_mysql-0.2 (crate (name "r2d2_mysql") (vers "0.2.1") (deps (list (crate-dep (name "mysql") (req "^0.17.2") (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1azh4xandw5y96mv473b1d5rargzdy31ndkqvin4yaz6k6ww88cl")))

(define-public crate-r2d2_mysql-0.2 (crate (name "r2d2_mysql") (vers "0.2.2") (deps (list (crate-dep (name "mysql") (req "~0.18.0") (kind 0)) (crate-dep (name "r2d2") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3.0") (default-features #t) (kind 0)))) (hash "1k4dk76a4p5nap15xd21fpy94192cjz4skb7jjl1wnff3i8d318n")))

(define-public crate-r2d2_mysql-2 (crate (name "r2d2_mysql") (vers "2.2.0") (deps (list (crate-dep (name "mysql") (req "~2.2.0") (kind 0)) (crate-dep (name "r2d2") (req "~0.6.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3.16") (default-features #t) (kind 0)))) (hash "1a1raxbq4v0m3d5xzm8p4p82ifmhgfx3bwqhzz9bm2g6q6w7pbf8")))

(define-public crate-r2d2_mysql-2 (crate (name "r2d2_mysql") (vers "2.2.1") (deps (list (crate-dep (name "mysql") (req "^2.2") (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3.16") (default-features #t) (kind 0)))) (hash "1rc0xnb4h30ljgc89y8zc0bwl2ri3libvkdrd3lp9gxab0x3vm52")))

(define-public crate-r2d2_mysql-3 (crate (name "r2d2_mysql") (vers "3.0.0") (deps (list (crate-dep (name "mysql") (req "^3.0") (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3.16") (default-features #t) (kind 0)))) (hash "1dcyml9mlggffyffih056gl487l97znsqrnbj9w1vhmnmxipp4sj")))

(define-public crate-r2d2_mysql-7 (crate (name "r2d2_mysql") (vers "7.1.0") (deps (list (crate-dep (name "mysql") (req "^7.1.2") (kind 0)) (crate-dep (name "r2d2") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3.19") (default-features #t) (kind 0)))) (hash "0dx61v5n8drqagky6bzy3fx2kcfvc7ij311mzwcw77rqlxv2h5gn")))

(define-public crate-r2d2_mysql-8 (crate (name "r2d2_mysql") (vers "8.0.0") (deps (list (crate-dep (name "mysql") (req "^8.0.0") (kind 0)) (crate-dep (name "r2d2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3.22") (default-features #t) (kind 0)))) (hash "01p56n6vrsgklzspjn1ljgwx7vwngw53hb9aw9rv0qjwbnyz0k43")))

(define-public crate-r2d2_mysql-9 (crate (name "r2d2_mysql") (vers "9.0.0") (deps (list (crate-dep (name "mysql") (req "^14") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "1gfaabgk51nyg4pi1759bjjq0hdd5mvvcbwjr5kr1l0xx1afw76k")))

(define-public crate-r2d2_mysql-15 (crate (name "r2d2_mysql") (vers "15.0.0") (deps (list (crate-dep (name "mysql") (req "^15") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "0iz8971dj9q4ac7s5d7cwr9znhxycyprbf9qnkkizv1v111p3ghq")))

(define-public crate-r2d2_mysql-16 (crate (name "r2d2_mysql") (vers "16.0.0") (deps (list (crate-dep (name "mysql") (req "^16") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "055lqp1hgbs2i2q3bfrlkp79dk67kxi18smpm0q0v64754g9skgp")))

(define-public crate-r2d2_mysql-17 (crate (name "r2d2_mysql") (vers "17.0.0") (deps (list (crate-dep (name "mysql") (req "^17") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0zwnx3bqvfvkfr40135vh4cv6y4jjj3063f0zq0pss194q31cxmv")))

(define-public crate-r2d2_mysql-18 (crate (name "r2d2_mysql") (vers "18.0.0") (deps (list (crate-dep (name "mysql") (req "^18") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0bi5kb5srvcqs91f69hfinmvvkn5a1b7ak7pzlzqcrpyq93c6f10")))

(define-public crate-r2d2_mysql-20 (crate (name "r2d2_mysql") (vers "20.0.0") (deps (list (crate-dep (name "mysql") (req "^20") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0i93il547r509s5764rc0ncq0klqkp6c8wdxd7imnvsdy31c7j04")))

(define-public crate-r2d2_mysql-21 (crate (name "r2d2_mysql") (vers "21.0.0") (deps (list (crate-dep (name "mysql") (req "^21") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "1qm9qg1mal8p55qyy8zl032jk52biwmj1zzy2mamyf8bd52m3l51")))

(define-public crate-r2d2_mysql-22 (crate (name "r2d2_mysql") (vers "22.0.0") (deps (list (crate-dep (name "mysql") (req "^22") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0fz7wsmsbigyr6abkfzhd015q87jbycrsjjhl3f9kp70ga7dms7b")))

(define-public crate-r2d2_mysql-23 (crate (name "r2d2_mysql") (vers "23.0.0") (deps (list (crate-dep (name "mysql") (req "^23") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0xf5n33ppi3in63593nl31z8c2k9m9lswyrq9xs9m5b5rqwdfcwp")))

(define-public crate-r2d2_mysql-24 (crate (name "r2d2_mysql") (vers "24.0.0") (deps (list (crate-dep (name "mysql") (req "^24") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0x15yljhqfbdrb1kc446xck2573dpr6gbwl0jpdir5r1diz15r9z") (rust-version "1.65")))

(define-public crate-r2d2_mysql_batis-0.1 (crate (name "r2d2_mysql_batis") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql") (req "^24.0.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql_batis_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "096h1l4b6cqwyswjjayz94y33vxd6497947irgpkxdjnk2gqaan2") (yanked #t)))

(define-public crate-r2d2_mysql_batis-0.1 (crate (name "r2d2_mysql_batis") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql") (req "^24.0.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql_batis_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yix2h793lna9damxxsr5p2i0c1fqr6fqzw1h1irfma92dyzs7sz") (yanked #t)))

(define-public crate-r2d2_mysql_batis-0.1 (crate (name "r2d2_mysql_batis") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql") (req "^24.0.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql_batis_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ar9w154la2pm9g3nm8y7zm35gr7hzc99c69qlfkl432h6z57j2k")))

(define-public crate-r2d2_mysql_batis-0.1 (crate (name "r2d2_mysql_batis") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql") (req "^24.0.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql_batis_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0did65p48bzx2kzhvsgww5k279bsrcahwq73jwma9fq0mmd2xvxm")))

(define-public crate-r2d2_mysql_batis-0.1 (crate (name "r2d2_mysql_batis") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql") (req "^24.0.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2_mysql_batis_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "11d75ia3gxyyi8q3bhkvqafbg0byyba71hsc44n84kc27042wrki")))

(define-public crate-r2d2_mysql_batis_macros-0.1 (crate (name "r2d2_mysql_batis_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0bs5cvv93phhimpsvag88jgh07zbvssjxf5j82fcf7jkiisdmkyx")))

(define-public crate-r2d2_odbc-0.1 (crate (name "r2d2_odbc") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "odbc") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0yhj19krql5nflhm9ya3abdad8cxag9rfjcrcszv7a07jx2hvac9")))

(define-public crate-r2d2_odbc-0.2 (crate (name "r2d2_odbc") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "odbc") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0g08lsjs9llliraj6bld6hgv5qd7my5827lxqi5fcnf9i8mkyh51")))

(define-public crate-r2d2_odbc-0.3 (crate (name "r2d2_odbc") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "odbc") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1hvz5gd4j8aga0why9jan64j7bbf867v9ibdjrmw3qcraca5rcxm")))

(define-public crate-r2d2_odbc-0.4 (crate (name "r2d2_odbc") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "odbc") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "odbc-safe") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1h8hgbzwx6sscr1iqk9kf0dy7wcglqb5ifnpnkcs3ab49spl80lv")))

(define-public crate-r2d2_odbc-0.5 (crate (name "r2d2_odbc") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "odbc") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "odbc-safe") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1py8a3jrgn8vrw60dambf2x4k5cqgg80pszlg0w6m4vnvs2vfcfw")))

(define-public crate-r2d2_odbc_api-0.1 (crate (name "r2d2_odbc_api") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "odbc-api") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "15gg014ayv9y9hx6y8mw2zc9g9fv06dw6qily1kqn61vc3fz91g5")))

(define-public crate-r2d2_odbc_api-0.1 (crate (name "r2d2_odbc_api") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "odbc-api") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1vrywkcvhn6a4n63x4g4q9q2h38j2vckgdd4vqynypzy35ixm9gr")))

(define-public crate-r2d2_odbc_api-0.1 (crate (name "r2d2_odbc_api") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "odbc-api") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket_sync_db_pools") (req "^0.1.0-rc.1") (optional #t) (default-features #t) (kind 0)))) (hash "1xii2ixf9gz9zm9j63clyqbidjnm2c6x2m7k8n2mrkwv4fc5dqkr") (features (quote (("rocket_pooling" "rocket" "rocket_sync_db_pools") ("default" "rocket_pooling"))))))

(define-public crate-r2d2_odbc_api-0.1 (crate (name "r2d2_odbc_api") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "odbc-api") (req "^0.28.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket_sync_db_pools") (req "^0.1.0-rc.1") (optional #t) (default-features #t) (kind 0)))) (hash "0fajxd6k7vrg6m5h3smzgs6m1jwh36b8v3vm5sjd3k7lsyvpydv2") (features (quote (("rocket_pooling" "rocket" "rocket_sync_db_pools") ("default" "rocket_pooling"))))))

(define-public crate-r2d2_odbc_api-0.1 (crate (name "r2d2_odbc_api") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "odbc-api") (req "^0.33") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket_sync_db_pools") (req "^0.1.0-rc.1") (optional #t) (default-features #t) (kind 0)))) (hash "132729klkk5rs8yzw3qzwq94mh6rx94i282fxv1i6fx0dnfwjk9k") (features (quote (("rocket_pooling" "rocket" "rocket_sync_db_pools") ("default" "rocket_pooling"))))))

(define-public crate-r2d2_odbc_api-0.2 (crate (name "r2d2_odbc_api") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-queue") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "odbc-api") (req "^0.50.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.10") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rocket_sync_db_pools") (req "^0.1.0-rc.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1kvjlsjq6bg7qpgvmmpakjfzx746ws0ya368920bzm8qpdpyzpnw") (features (quote (("rocket_pooling" "rocket" "rocket_sync_db_pools") ("iodbc" "odbc-api/iodbc") ("hfsql" "odbc-api/iodbc") ("default" "rocket_pooling"))))))

(define-public crate-r2d2_postgres-0.1 (crate (name "r2d2_postgres") (vers "0.1.0") (deps (list (crate-dep (name "postgres") (req "*") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0n1ia1slkxaf9d8cm2a6f63g88dfj9spqdgha3i8jfqv236s8ayz")))

(define-public crate-r2d2_postgres-0.1 (crate (name "r2d2_postgres") (vers "0.1.1") (deps (list (crate-dep (name "postgres") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rydl4gbfzl0lfqwx4m4x4r05jn0sjxlrdlpjaqfmykqxsf8ih9c")))

(define-public crate-r2d2_postgres-0.2 (crate (name "r2d2_postgres") (vers "0.2.0") (deps (list (crate-dep (name "postgres") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.2") (default-features #t) (kind 0)))) (hash "00q89gp58cy9b7x0mnlc8f47llz1injbx2zjhhqlj6j3fcclvrbd")))

(define-public crate-r2d2_postgres-0.2 (crate (name "r2d2_postgres") (vers "0.2.1") (deps (list (crate-dep (name "collect") (req "~0.0") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rs5k7ygxm9mr1gbq861h72rrld724hcb6jwqbix78f560r38izs")))

(define-public crate-r2d2_postgres-0.3 (crate (name "r2d2_postgres") (vers "0.3.0") (deps (list (crate-dep (name "collect") (req "~0.0") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ng96x96k124rmlqw2gggqjri34rmsy50m6dfwpvvf92zwbhdm1z")))

(define-public crate-r2d2_postgres-0.3 (crate (name "r2d2_postgres") (vers "0.3.1") (deps (list (crate-dep (name "collect") (req "~0.0") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bhjlmqxaz3y4kz18263hy8jsrb02j1di4z91n04bdpg8hb4i5p7")))

(define-public crate-r2d2_postgres-0.4 (crate (name "r2d2_postgres") (vers "0.4.0") (deps (list (crate-dep (name "postgres") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kbzqcrvylnjcr499rb31i506i7cagxvi9si3k6bw8am3b6afg6l")))

(define-public crate-r2d2_postgres-0.5 (crate (name "r2d2_postgres") (vers "0.5.0") (deps (list (crate-dep (name "postgres") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sb10d6nj657a9pn4pkm9k4lgf9pqvzyxb69drg7g6rx5rizr3bq")))

(define-public crate-r2d2_postgres-0.6 (crate (name "r2d2_postgres") (vers "0.6.0") (deps (list (crate-dep (name "postgres") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.4") (default-features #t) (kind 0)))) (hash "061qw88b6mm6fnhkps9vfsxsgp1ih78cfj5az8j1268wz0nslwn6")))

(define-public crate-r2d2_postgres-0.7 (crate (name "r2d2_postgres") (vers "0.7.0") (deps (list (crate-dep (name "postgres") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.5") (default-features #t) (kind 0)))) (hash "1n11ias0jv98zbdzycwkx7rm1rb2xj9ljr83akw7xh2wbd9rcrcg")))

(define-public crate-r2d2_postgres-0.8 (crate (name "r2d2_postgres") (vers "0.8.0") (deps (list (crate-dep (name "postgres") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jsbahssfjrshbllmjvizimr3j5xad03fgx52v9ffaix12w0zmp3")))

(define-public crate-r2d2_postgres-0.9 (crate (name "r2d2_postgres") (vers "0.9.0") (deps (list (crate-dep (name "postgres") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.5") (default-features #t) (kind 0)))) (hash "1r63crici9sqrq2qciym8k2a3zsz0z34jxmb70jcgy9vzy6q812x")))

(define-public crate-r2d2_postgres-0.9 (crate (name "r2d2_postgres") (vers "0.9.1") (deps (list (crate-dep (name "postgres") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.5") (default-features #t) (kind 0)))) (hash "05dz37c93ps1llqnwm0sdjzzv2r36krpwgnca1cvqx1iv3mx9518")))

(define-public crate-r2d2_postgres-0.9 (crate (name "r2d2_postgres") (vers "0.9.2") (deps (list (crate-dep (name "postgres") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)))) (hash "00774nk402sfrjd1y7kjj954l45hz4nw4vcaxfc75za3n14w1fhx")))

(define-public crate-r2d2_postgres-0.9 (crate (name "r2d2_postgres") (vers "0.9.3") (deps (list (crate-dep (name "postgres") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)))) (hash "0sn1c91mnf5r3admkzjxm3ilvs6fivcwz7fyh6xa85j2d8m9y556")))

(define-public crate-r2d2_postgres-0.10 (crate (name "r2d2_postgres") (vers "0.10.0") (deps (list (crate-dep (name "postgres") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)))) (hash "1g2ilz8ljk1ndjypyyxzc6ps9bdv94pb2ls6d6a1lq82nca53hjm")))

(define-public crate-r2d2_postgres-0.10 (crate (name "r2d2_postgres") (vers "0.10.1") (deps (list (crate-dep (name "postgres") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req ">= 0.6, < 0.8") (default-features #t) (kind 0)))) (hash "1jk7j2fwn5pmn0ispnzy4fmzbjxgfqk3rnip0ahqjqk09808jsjx")))

(define-public crate-r2d2_postgres-0.11 (crate (name "r2d2_postgres") (vers "0.11.0") (deps (list (crate-dep (name "postgres") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req ">= 0.6, < 0.8") (default-features #t) (kind 0)))) (hash "0g6fvklrwk331yllp2cb2m49jsn542q8z4jznb7vrg3sr4f9nwsr")))

(define-public crate-r2d2_postgres-0.11 (crate (name "r2d2_postgres") (vers "0.11.1") (deps (list (crate-dep (name "postgres") (req ">= 0.12, < 0.14") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req ">= 0.6, < 0.8") (default-features #t) (kind 0)))) (hash "0brcpwlzyzxns1yma9yx9n79wq18fffasxa630jz0nvjfd3kvsnr")))

(define-public crate-r2d2_postgres-0.12 (crate (name "r2d2_postgres") (vers "0.12.0") (deps (list (crate-dep (name "postgres") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)))) (hash "0p1bvlkjkjybxdd49yapx5lh67gfszy1cn0wl3g77717ls7f3ah0")))

(define-public crate-r2d2_postgres-0.13 (crate (name "r2d2_postgres") (vers "0.13.0") (deps (list (crate-dep (name "postgres") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "postgres-shared") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)))) (hash "03jnw122535y55ppzsiwhg56d26rzqbm420ivmk6arv3xak1k2qz")))

(define-public crate-r2d2_postgres-0.14 (crate (name "r2d2_postgres") (vers "0.14.0") (deps (list (crate-dep (name "postgres") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "postgres-shared") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "14zv00d4mcv5kxszxn8gcamhxpwwp3j3rg32ya62jb1x1jfgxivq")))

(define-public crate-r2d2_postgres-0.15 (crate (name "r2d2_postgres") (vers "0.15.0-rc.1") (deps (list (crate-dep (name "postgres") (req "^0.16.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.2") (default-features #t) (kind 0)))) (hash "0c01rzi1l3z0804g7hgn302ar1w86mb87mal2z2wn7g70pv4i8g1")))

(define-public crate-r2d2_postgres-0.16 (crate (name "r2d2_postgres") (vers "0.16.0-alpha.1") (deps (list (crate-dep (name "postgres") (req "= 0.17.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1w4wvplk0k5l7fdy86mn2c62wgdxi9i31j45ngz1x3c2blmsljdq")))

(define-public crate-r2d2_postgres-0.16 (crate (name "r2d2_postgres") (vers "0.16.0-alpha.2") (deps (list (crate-dep (name "postgres") (req "= 0.17.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1a42brjg8j1fxhr9zig4cl5k6jx4x714r1ixfwn62v9qn9kjw85q")))

(define-public crate-r2d2_postgres-0.16 (crate (name "r2d2_postgres") (vers "0.16.0") (deps (list (crate-dep (name "postgres") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "13zyiq4rsqakmk3rgbj153dfgk7z3xhxkxj1244c3fj3dzv2fzbh")))

(define-public crate-r2d2_postgres-0.17 (crate (name "r2d2_postgres") (vers "0.17.0") (deps (list (crate-dep (name "postgres") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "199axyc7j4pndjfj6b2s49wkj1a15mds7kd6xq4hg08dfpxpdwg5")))

(define-public crate-r2d2_postgres-0.18 (crate (name "r2d2_postgres") (vers "0.18.0") (deps (list (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0viggj6105q8901x3k2137z5b5g930jkx6y4k97mq6w3dlcmsrpp")))

(define-public crate-r2d2_postgres-0.18 (crate (name "r2d2_postgres") (vers "0.18.1") (deps (list (crate-dep (name "postgres") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)))) (hash "02r329mz0sq5sxcjbyimnyb6gq8fh6bybgp047rm9jsqwrmwaabh")))

(define-public crate-r2d2_pulsar-0.1 (crate (name "r2d2_pulsar") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "pulsar") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "1gmw89xji48al356jm3b5l8k0cl1ciq8lyy5jngk0i4lzmghxcw1")))

(define-public crate-r2d2_pulsar-0.1 (crate (name "r2d2_pulsar") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "pulsar") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "1vi4n23h0hrqacdamka1sayr9jp98nfginjcc3p25l95k7c9hrmv")))

(define-public crate-r2d2_pulsar-0.1 (crate (name "r2d2_pulsar") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "pulsar") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "0c58khy1m482vcfi4rki728dn4i0cnb7mrpif0zqs0d50bm6swmi")))

(define-public crate-r2d2_pulsar-0.1 (crate (name "r2d2_pulsar") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "pulsar") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "022qy810b4x8pib77s7v9q7zncxma7r8ka16ycpzc5hxn8rxycyc")))

(define-public crate-r2d2_redis-0.0.2 (crate (name "r2d2_redis") (vers "0.0.2") (deps (list (crate-dep (name "r2d2") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1zlridw9sj77prm49c19399f4c8p549ykr1b62w133gvblqyjyal")))

(define-public crate-r2d2_redis-0.1 (crate (name "r2d2_redis") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "194arh4zix4nhsbslffjc0ayj6jn3rd4mkvln4jix6k876b0953s")))

(define-public crate-r2d2_redis-0.2 (crate (name "r2d2_redis") (vers "0.2.0") (deps (list (crate-dep (name "r2d2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5") (default-features #t) (kind 0)))) (hash "1axvgh0dc01qas5098r0pcpxjhpzpx4pjwkxfrc4yhvy0q6ka0fn")))

(define-public crate-r2d2_redis-0.3 (crate (name "r2d2_redis") (vers "0.3.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5") (default-features #t) (kind 0)))) (hash "0wyjzfxm8721i93lhh3gwjxyki7abbl4kd1633f6jamg8zv9qpj4")))

(define-public crate-r2d2_redis-0.3 (crate (name "r2d2_redis") (vers "0.3.1") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5") (default-features #t) (kind 0)))) (hash "12wlfcrv4rc0mbbskj44r9lxh98mqmsgpdx45mvyamnqqyma4hq8")))

(define-public crate-r2d2_redis-0.4 (crate (name "r2d2_redis") (vers "0.4.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.6") (default-features #t) (kind 0)))) (hash "0ai9h1knzzdqv222b0sn01wr2p569fnp8g4dibksphqwpxrgbacq")))

(define-public crate-r2d2_redis-0.5 (crate (name "r2d2_redis") (vers "0.5.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.7") (default-features #t) (kind 0)))) (hash "01jrfxnjaxj6xjjif0pz2dacvwk0ppisa94wqs57v0ijghxw3ji4")))

(define-public crate-r2d2_redis-0.5 (crate (name "r2d2_redis") (vers "0.5.1") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1hr4f5bwij4ff81jb3f89hgaxb2w56n4d0hb3zbdakrg413n9nr7")))

(define-public crate-r2d2_redis-0.6 (crate (name "r2d2_redis") (vers "0.6.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1vvn5a6n0bik7lb3nl1l2ml3z450xfd7wf4nl29ch7ispv9si68l")))

(define-public crate-r2d2_redis-0.7 (crate (name "r2d2_redis") (vers "0.7.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1i7mlw0qn7wd9y3w35gwkn835gsm9xc76qgbvxqxbgzsylnq801v")))

(define-public crate-r2d2_redis-0.8 (crate (name "r2d2_redis") (vers "0.8.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1nxramcqfkn7ghl6i1pgvirlyyfvmrsyhm6dhkh90jazpf88lccq")))

(define-public crate-r2d2_redis-0.9 (crate (name "r2d2_redis") (vers "0.9.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1pfmg95rlnqzdxqw37f35wmcwsnfjk34vy8daw7ijxdfhap0ll2l")))

(define-public crate-r2d2_redis-0.10 (crate (name "r2d2_redis") (vers "0.10.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1v938c03d5xbha60h7mxih7jhvpf4zk8daxa4l9i9xk5xa1pjada") (yanked #t)))

(define-public crate-r2d2_redis-0.10 (crate (name "r2d2_redis") (vers "0.10.1") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.11") (default-features #t) (kind 0)))) (hash "08iknxs8l13nyvlbi6j5zwnpjcs4kxv5pw7mrzbfmy4ai5vnnlwx")))

(define-public crate-r2d2_redis-0.11 (crate (name "r2d2_redis") (vers "0.11.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.12") (default-features #t) (kind 0)))) (hash "17zbpc9vszk1xkpqa6dl3v00vci9w9in9ybig52b29l0jn4dvlja")))

(define-public crate-r2d2_redis-0.12 (crate (name "r2d2_redis") (vers "0.12.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.13") (default-features #t) (kind 0)))) (hash "0rqs7sa8lpklj52ya0wlyjv4gamhz3wrbs61wfs3743nb2igd3qx")))

(define-public crate-r2d2_redis-0.13 (crate (name "r2d2_redis") (vers "0.13.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.15") (default-features #t) (kind 0)))) (hash "034l3v62zgjahhf6wvzdvlqjwxrqg1b6hrcrhw8r91fp07cbcxsc")))

(define-public crate-r2d2_redis-0.14 (crate (name "r2d2_redis") (vers "0.14.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1fv38fvqcaz5bw2f1fvq4akb876x0zi5iv426qskxfdhfsw7690q")))

(define-public crate-r2d2_redis2-0.23 (crate (name "r2d2_redis2") (vers "0.23.3") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.23.3") (default-features #t) (kind 0)))) (hash "169gd98n7micwbx6d2vjbixdgfnvfd0k43zbkwnrijv01c5pf55h")))

(define-public crate-r2d2_redis_cluster-0.1 (crate (name "r2d2_redis_cluster") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "04x6mm3y9b983pv87ndg3zrmhf8ia6xbn44w7z4a37v62bl4w4jh")))

(define-public crate-r2d2_redis_cluster-0.1 (crate (name "r2d2_redis_cluster") (vers "0.1.1") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "0njm99g62sl7r9wva641srlxz6gavwnc3fa5jmsbda0qy3h9yan3")))

(define-public crate-r2d2_redis_cluster-0.1 (crate (name "r2d2_redis_cluster") (vers "0.1.2") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "00aw9p0zhnyxmx52par3py3jak2f7msa0dqbcnwphiik1pp9jq0a")))

(define-public crate-r2d2_redis_cluster-0.1 (crate (name "r2d2_redis_cluster") (vers "0.1.3") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0vldd58wv2kmb2gm2xb54z9nrcah6p90rqbplkj7n5pi25qbs1lm")))

(define-public crate-r2d2_redis_cluster-0.1 (crate (name "r2d2_redis_cluster") (vers "0.1.4") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sl5g3jlpwb4i5l4igxmiyqb2r5s15flk86vnci035zs05cfyn1r")))

(define-public crate-r2d2_redis_cluster-0.1 (crate (name "r2d2_redis_cluster") (vers "0.1.5") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "17w3d95ads9gv5lk70nn8ik28vp12jj8c27l47v9av2kqdrg3cjm")))

(define-public crate-r2d2_redis_cluster-0.1 (crate (name "r2d2_redis_cluster") (vers "0.1.6") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "01i9926r76q0mqqdfjw27y8j6ammx899wqx9bz2lrbk8ydn22n04")))

(define-public crate-r2d2_redis_cluster2-0.23 (crate (name "r2d2_redis_cluster2") (vers "0.23.3") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis_cluster_rs2") (req "^0.23.3") (default-features #t) (kind 0)))) (hash "1fz1d6qn302pvwlp06psm926ngwpbgvjg2yq9lwn1fn8iwr7n33f")))

(define-public crate-r2d2_redis_patch-0.6 (crate (name "r2d2_redis_patch") (vers "0.6.1") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.8") (default-features #t) (kind 0)))) (hash "1x3ip72qchsgp1kvj5q3f46c9pa0f1gavx71cad2xsfmdfvzcqi1")))

(define-public crate-r2d2_sqlite-0.0.1 (crate (name "r2d2_sqlite") (vers "0.0.1") (deps (list (crate-dep (name "r2d2") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "*") (default-features #t) (kind 0)))) (hash "0k7j68bkw1sk3gy9p06hb4bnzwfhcdwxk4aw01cyg1d13z7m70ay")))

(define-public crate-r2d2_sqlite-0.0.2 (crate (name "r2d2_sqlite") (vers "0.0.2") (deps (list (crate-dep (name "r2d2") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0zba6k69i5xgkcg2s0619ixssfzjgnygmyjqiqz97ngf6w5vzfn9")))

(define-public crate-r2d2_sqlite-0.0.3 (crate (name "r2d2_sqlite") (vers "0.0.3") (deps (list (crate-dep (name "r2d2") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1611h61k6dhlrx9a1y0lbw84zd9gvi6kafh351mxbagnhggpmjs7")))

(define-public crate-r2d2_sqlite-0.0.4 (crate (name "r2d2_sqlite") (vers "0.0.4") (deps (list (crate-dep (name "r2d2") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1njpgw0l1didbaahsfrgw1zyl4d7c73x83qyhwyzkshdpw92khps")))

(define-public crate-r2d2_sqlite-0.0.5 (crate (name "r2d2_sqlite") (vers "0.0.5") (deps (list (crate-dep (name "r2d2") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gnbnnp5np40ghwz17xx5rzdy7sgps60d3z7m02qy4lx4ljnhgln")))

(define-public crate-r2d2_sqlite-0.0.6 (crate (name "r2d2_sqlite") (vers "0.0.6") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0dhidy7zac4l158jkwiprxvczgisj30dsidvxm3sh6a1ql0czc1w")))

(define-public crate-r2d2_sqlite-0.1 (crate (name "r2d2_sqlite") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ysdwyz9vkl23z8c2p4x11dn7spys208i8lgwkkvgajviy9gd86f")))

(define-public crate-r2d2_sqlite-0.1 (crate (name "r2d2_sqlite") (vers "0.1.1") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1y7nzd59rzxyqaka9x912chaf4qyn1i8pyv5bkgpj761a271p3b5")))

(define-public crate-r2d2_sqlite-0.1 (crate (name "r2d2_sqlite") (vers "0.1.2") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0dx2fjnmqsyyzbnv9chn5nnkdkil6lv5x3kq13s37y8fh17lm06w")))

(define-public crate-r2d2_sqlite-0.1 (crate (name "r2d2_sqlite") (vers "0.1.3") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gw05m7dmy9i3z23q05ni7ilxs2ma6n1f9mrsmal3p9q9b62kicn")))

(define-public crate-r2d2_sqlite-0.1 (crate (name "r2d2_sqlite") (vers "0.1.4") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "19blwqvqh8fhbvb3gzc3ldds98vlydayal889cnrp41isw05f7p4")))

(define-public crate-r2d2_sqlite-0.2 (crate (name "r2d2_sqlite") (vers "0.2.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0x03wbfsc6dn0q18p97ssjyhjyavy686dr3vbsjdb7xy26zxkhbv")))

(define-public crate-r2d2_sqlite-0.2 (crate (name "r2d2_sqlite") (vers "0.2.1") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0lg25sqz7m7jsp162zgxz0q709klx4bawrs5gz9bhd8kr19qkc9q")))

(define-public crate-r2d2_sqlite-0.3 (crate (name "r2d2_sqlite") (vers "0.3.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0if8dc9lpla7gmfv3ksr9573g1gc6x26c81cbycap4l0cx4cjxlf")))

(define-public crate-r2d2_sqlite-0.3 (crate (name "r2d2_sqlite") (vers "0.3.1") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "02jl3c8wi07ny05c7gs266zkbqlzly39g11ynax1yhpx1ih7hkac") (yanked #t)))

(define-public crate-r2d2_sqlite-0.4 (crate (name "r2d2_sqlite") (vers "0.4.0") (deps (list (crate-dep (name "r2d2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1cdmz0vxwy84k80v0lgx4nhhrrsy94hrgm5sid516qynfwbphv2w")))

(define-public crate-r2d2_sqlite-0.5 (crate (name "r2d2_sqlite") (vers "0.5.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gbbcdkbihkkxc3cvsgqq37db7ph6aahzzsqw9xljx57xivsb8j0")))

(define-public crate-r2d2_sqlite-0.5 (crate (name "r2d2_sqlite") (vers "0.5.1") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wqdphn2b4x1i0g59qdfa3z1sys69rbyrh39ybkvp22p4azs2xnp") (yanked #t)))

(define-public crate-r2d2_sqlite-0.6 (crate (name "r2d2_sqlite") (vers "0.6.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "09l417ysllcn3zhdjpb2axqx8660cxrs3jbq3qb05x5fw77f8lvk")))

(define-public crate-r2d2_sqlite-0.7 (crate (name "r2d2_sqlite") (vers "0.7.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "02g9x8xam4389ymy36d8j36gfmpl02rzvlqq10l546xvmd36sinf")))

(define-public crate-r2d2_sqlite-0.8 (crate (name "r2d2_sqlite") (vers "0.8.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0mrqcl9zc4iw4kh845dr6rvsg2aap3v4bvv5lyqnbrcsna8akjx7")))

(define-public crate-r2d2_sqlite-0.9 (crate (name "r2d2_sqlite") (vers "0.9.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1b0dfv0cddlfb2g3fj2g1bb21010g2wxfcawr5chrxvvkrypyqvh")))

(define-public crate-r2d2_sqlite-0.10 (crate (name "r2d2_sqlite") (vers "0.10.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1hplljsw1cxdrg6913y8dr4cq98dfgm3svjm8r19slj7pdfml00p")))

(define-public crate-r2d2_sqlite-0.11 (crate (name "r2d2_sqlite") (vers "0.11.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "02rwshwizxw7klfwyhkvzqj6m7dhslmwazaydxfz1xh5dbm9rq24")))

(define-public crate-r2d2_sqlite-0.12 (crate (name "r2d2_sqlite") (vers "0.12.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0czgr8qgbfmhilryhj1fxxcqpwiy0y5aq5zn9dh5m7nf6n02cvl0")))

(define-public crate-r2d2_sqlite-0.13 (crate (name "r2d2_sqlite") (vers "0.13.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "17hx5nlhm8jabizydjy4anjaw4vxzkk943r6xcgfifwjcsqzaqdh")))

(define-public crate-r2d2_sqlite-0.14 (crate (name "r2d2_sqlite") (vers "0.14.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0fryajmnpfx7ssfbq7qawzvj8qdrrdba15m6i0rild24pvf8jikw")))

(define-public crate-r2d2_sqlite-0.15 (crate (name "r2d2_sqlite") (vers "0.15.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.22") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0cvg350xm02g17a03n4df2ps794kdan5pb6c42pbi2vw9rwzy5ay")))

(define-public crate-r2d2_sqlite-0.16 (crate (name "r2d2_sqlite") (vers "0.16.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.23") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0afr0z3ydx695vf5s7m0wd9zy0nkxkmgph1vav02ib17iglfnq7d")))

(define-public crate-r2d2_sqlite-0.17 (crate (name "r2d2_sqlite") (vers "0.17.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.24") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1n59vg0z50d9bzmib14yilybjxz61xcn5w58dnkizc6byigv6yi2")))

(define-public crate-r2d2_sqlite-0.18 (crate (name "r2d2_sqlite") (vers "0.18.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ncrf5s23ixjmhi33zpflp34rhr3i8fsqlyzsd15wk1195q6094x")))

(define-public crate-r2d2_sqlite-0.19 (crate (name "r2d2_sqlite") (vers "0.19.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0028yccvqn1904f8vcf7xbpn5hrgd1cnlj2cfanw4vx7d2a3rjjl") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.20 (crate (name "r2d2_sqlite") (vers "0.20.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.27") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "15g8zw9psinz3rhhgadbn73a89hkyanvfcmyjdw151h5lx6qxp3g") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.21 (crate (name "r2d2_sqlite") (vers "0.21.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0d0hlnj9pgzrqihgb7a5xy6hgn69din35z0zv6n5rkcrgqrx1xdl") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.22 (crate (name "r2d2_sqlite") (vers "0.22.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1p4d6h4bxh0zi4sjv1blzaa8c3hfabgkhrq4hprqa4qnsqii7wwr") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.23 (crate (name "r2d2_sqlite") (vers "0.23.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1xkpfyxyidhhzwc8hp4w84hd0qinw6xifdc13rsj03nkd6v91hjd") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite-0.24 (crate (name "r2d2_sqlite") (vers "0.24.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1wn5rh3vrcr5k1x84ph31ns85x1f55dpgy3jp9npjaf1cpgjx63a") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite_neonphog-0.18 (crate (name "r2d2_sqlite_neonphog") (vers "0.18.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.26") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0vyfwmpw0k87w95zl2s4aaj52sqmdh14hr93qx4j19gw0caiqs3m") (features (quote (("sqlcipher" "rusqlite/sqlcipher") ("bundled-sqlcipher-vendored-openssl" "rusqlite/bundled-sqlcipher-vendored-openssl") ("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite_neonphog-0.1 (crate (name "r2d2_sqlite_neonphog") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "177l0z1byami8ficjx0ga59cvdppibhzp529bqnfb754hyrra7jd") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

(define-public crate-r2d2_sqlite_pool-0.1 (crate (name "r2d2_sqlite_pool") (vers "0.1.0") (deps (list (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0a7qkci1d37r4sgi0zx8rrngqk4yy30n6jrkjd1526pz1plqrayz") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled")))) (yanked #t)))

(define-public crate-r2d2_sqlite_pool-0.1 (crate (name "r2d2_sqlite_pool") (vers "0.1.1") (deps (list (crate-dep (name "nanoid") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29") (features (quote ("trace"))) (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "08lpmy5vyxjp7j0rcgg3mlc84fc2pwdbl6sj2syl0r5749qbj4f2") (features (quote (("bundled-sqlcipher" "rusqlite/bundled-sqlcipher") ("bundled" "rusqlite/bundled"))))))

