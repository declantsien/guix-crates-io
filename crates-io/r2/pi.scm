(define-module (crates-io r2 pi) #:use-module (crates-io))

(define-public crate-r2pipe-0.1 (crate (name "r2pipe") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "08d2241jrpb1jdf44rzk5bahgjf2ksh80b1dhnjjwfdhwjad3g73")))

(define-public crate-r2pipe-0.1 (crate (name "r2pipe") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1n809vgf29w8zm1zsqcbiiyc7zw9dvx75md24ygsrgbij0bif1lq")))

(define-public crate-r2pipe-0.4 (crate (name "r2pipe") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "^0.0.61") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "0wp9kgpfxd488ifkyca9cvaz36wsvxi8m7j843y4538l06hiyyi5") (features (quote (("default"))))))

(define-public crate-r2pipe-0.5 (crate (name "r2pipe") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sixd95gijl94pkpcphsgd2844g7bw8vmfkk04ggvrafyzp7zp3k") (features (quote (("default"))))))

(define-public crate-r2pipe-0.6 (crate (name "r2pipe") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2.81") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0ianlskgs7sfic84l489nki5g8xw5f46g1nc1kv6h4mz98qm3pzm")))

(define-public crate-r2pipe-0.7 (crate (name "r2pipe") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2.81") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0axgnijvphds7rrqafrcda68xwwdwdiih5pd42cgb0yvvxw30i33")))

