(define-module (crates-io r2 ch) #:use-module (crates-io))

(define-public crate-r2ch-0.1 (crate (name "r2ch") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "0nzrm4lx2xpj0325zgjwbxfgavcyn5nb2hnjfj6hlyd9clcb9gff")))

(define-public crate-r2ch-0.1 (crate (name "r2ch") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0mb5w4xdpcb1zi9cqirwbniiv37qdpal5l62m8j9pj10vfx8zjsq")))

(define-public crate-r2ch-0.1 (crate (name "r2ch") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "0mxvfib6d3x64c0va33l3adgg6h9a8fnyw801980hhxw93bp6dzi")))

