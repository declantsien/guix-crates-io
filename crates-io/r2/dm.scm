(define-module (crates-io r2 dm) #:use-module (crates-io))

(define-public crate-r2dma-0.1 (crate (name "r2dma") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "r2dma-sys") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0") (default-features #t) (kind 0)))) (hash "0vvlysvfqd863z7a35l6i4x7az4nwlfzmalsaw93fyaf4vg6kn2h")))

(define-public crate-r2dma-sys-0.1 (crate (name "r2dma-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0") (features (quote ("experimental"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0") (default-features #t) (kind 1)))) (hash "1grhrd8lvrwhn3zlgvzhkqslfn508bqbm8hq2zrh4y259caihh5p")))

