(define-module (crates-io r2 ap) #:use-module (crates-io))

(define-public crate-r2api-5 (crate (name "r2api") (vers "5.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "0mrccg2igg9b9cvmjrvg31n7n8wzyrbb502lcmnwrxm8x0dajgbm") (links "r_core")))

(define-public crate-r2api-5 (crate (name "r2api") (vers "5.6.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "1rmij8cl46gnfwb1wlpm5x2ma0arq3y2bn4s4glz5rbn5smrvyhf") (links "r_core")))

(define-public crate-r2api-5 (crate (name "r2api") (vers "5.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "1ynzfphaxyfk3bgbii0vs1f1zgghk4srvhgyaj7a0gkzz46n21an") (links "r_core")))

(define-public crate-r2api-5 (crate (name "r2api") (vers "5.9.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "0bn5n89hw3i3av7s0b5f667i8d3986i1f7aswkxvk1m8vgc7xhlk") (features (quote (("static")))) (links "r_core")))

