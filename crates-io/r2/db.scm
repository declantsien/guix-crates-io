(define-module (crates-io r2 db) #:use-module (crates-io))

(define-public crate-r2dbc-0.0.1 (crate (name "r2dbc") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (features (quote ("clock"))) (optional #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jgld1m3qqnzh78l96cvvi9qn7smc8006c2ahm5xdqc2g0kpzap0")))

(define-public crate-r2dbc-cli-0.0.1 (crate (name "r2dbc-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "r2dbc") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "r2dbc-mysql") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "r2dbc-postgres") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "r2dbc-sqlite") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "198c4xyi8cp930q2bvq99nmh3mqbkwpnmfwf1fxln7jnfaj0y4d9")))

(define-public crate-r2dbc-mysql-0.0.1 (crate (name "r2dbc-mysql") (vers "0.0.1") (deps (list (crate-dep (name "mysql") (req "^20.1.0") (default-features #t) (kind 0)) (crate-dep (name "mysql_common") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "r2dbc") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "15jg1s9dfjmfmd2xsghrr3jqwpmhsmzn1mhc2bgydfr4ckc757ys")))

(define-public crate-r2dbc-postgres-0.0.1 (crate (name "r2dbc-postgres") (vers "0.0.1") (deps (list (crate-dep (name "postgres") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "r2dbc") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "sqlparser") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "12i1gbl5d22ikib2v4sxrr40hn25686i1yyfdm6x1ha2hvra2di4")))

(define-public crate-r2dbc-sqlite-0.0.1 (crate (name "r2dbc-sqlite") (vers "0.0.1") (deps (list (crate-dep (name "fallible-streaming-iterator") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "r2dbc") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.24.2") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0q8r9lk66686ji4196pijkr47jywzm4qxm05bwp70vri5wf6jsz3")))

