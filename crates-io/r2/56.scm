(define-module (crates-io r2 #{56}#) #:use-module (crates-io))

(define-public crate-r256-0.1 (crate (name "r256") (vers "0.1.0") (deps (list (crate-dep (name "winapi-util") (req "^0.1.5") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0aanl79lzygmsich3ysg2hy303jiaafkwcz1pcwj83f8ar7bypwi")))

(define-public crate-r256-0.1 (crate (name "r256") (vers "0.1.1") (deps (list (crate-dep (name "winapi-util") (req "^0.1.5") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "10gd6zfl65ldrxna49mq03fd84nv1j886bnkfyd4vk8ixppjbp5x")))

