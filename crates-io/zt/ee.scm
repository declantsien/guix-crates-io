(define-module (crates-io zt ee) #:use-module (crates-io))

(define-public crate-ztee-0.1 (crate (name "ztee") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "08qh64akjjkws0dzwydbband5zfjrrqfslcl2rcvq0b4hzqmdwki")))

