(define-module (crates-io sj lj) #:use-module (crates-io))

(define-public crate-sjlj-0.0.1 (crate (name "sjlj") (vers "0.0.1") (deps (list (crate-dep (name "naked-function") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zmy7xwrp1nhg3a5w663ckba96vh4vqsy618grfi6zv5jfngqa27")))

(define-public crate-sjlj-0.0.2 (crate (name "sjlj") (vers "0.0.2") (deps (list (crate-dep (name "linux-raw-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "naked-function") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0p5ydky6sb4g84mh8gkx2013zmymms1mva5whgdncs5irvpq2akk")))

(define-public crate-sjlj-0.1 (crate (name "sjlj") (vers "0.1.0") (deps (list (crate-dep (name "naked-function") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "linux-raw-sys") (req "^0.2.1") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "06cny7z9sqilgpidrclmryim3gph98s0xa087rain50d6hm314wd")))

(define-public crate-sjlj-0.1 (crate (name "sjlj") (vers "0.1.1") (deps (list (crate-dep (name "naked-function") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "linux-raw-sys") (req "^0.2.1") (features (quote ("no_std" "general"))) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1v10gn9dqw1wi4rqrh3rhxqcmr19nsqn74ajrkhlqjszr0r5xrsb")))

(define-public crate-sjlj-0.1 (crate (name "sjlj") (vers "0.1.2") (deps (list (crate-dep (name "naked-function") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "linux-raw-sys") (req "^0.2.1") (features (quote ("no_std" "general"))) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0mygyrgvhx0940w2dnfr260zv3whqmrdnm3c7v2y23xlnhpn4gcc")))

(define-public crate-sjlj-0.1 (crate (name "sjlj") (vers "0.1.3") (deps (list (crate-dep (name "naked-function") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "linux-raw-sys") (req "^0.2.1") (features (quote ("no_std" "general"))) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "sc") (req "^0.2.7") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0fcfy3vyyy15il8g0bdn7hm07irp8clll4i7vjmrl1l7qgpl76rf")))

