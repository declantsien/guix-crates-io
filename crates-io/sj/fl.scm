(define-module (crates-io sj fl) #:use-module (crates-io))

(define-public crate-sjfl-0.1 (crate (name "sjfl") (vers "0.1.0") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0k22hkcgfpf6x78j987pp844adj8hx2qxz41d1wkairv10i04yjc")))

(define-public crate-sjfl-0.1 (crate (name "sjfl") (vers "0.1.1") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1ylx9svhk3qnhal8fm757lysal3j6pd1ha3796vrli2jjhy98ral")))

(define-public crate-sjfl-0.1 (crate (name "sjfl") (vers "0.1.2") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0vp1647wh9jsk0nq4lsm19mvmb8026zzp1l13ldly3j4ar45fx5b")))

(define-public crate-sjfl-0.1 (crate (name "sjfl") (vers "0.1.3") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1zmf4fjwrd7mk1iwhd4vrdxbmfzq4ivr88j8f2bap5xr9w3dr66r")))

(define-public crate-sjfl-0.2 (crate (name "sjfl") (vers "0.2.0") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "14z70shcbdkmdj9ylbixs3d4zfv1fgpz2b7w42w71hnz097ay1bi")))

(define-public crate-sjfl-0.2 (crate (name "sjfl") (vers "0.2.1") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1cliazi3k9bjpyap7frgsabgmkmj7l30ysq0y78iwkcamw6m5lmm")))

(define-public crate-sjfl-0.2 (crate (name "sjfl") (vers "0.2.2") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "144v5dhlkz9yd0bj1crfh7s6zfh64lkvd37fapj2mxxn56axxvxx")))

(define-public crate-sjfl-0.2 (crate (name "sjfl") (vers "0.2.3") (deps (list (crate-dep (name "hmath") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1nq61yp4p7ggb2534mz1cyikf96rdbkcll1hf8sqfw1f00y8ab78")))

(define-public crate-sjfl-0.3 (crate (name "sjfl") (vers "0.3.0") (deps (list (crate-dep (name "hmath") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "0j0znhm9mqxicgz61f2ilxbpc3bsz6brrxmgi2p71c3ya03vv90v")))

(define-public crate-sjfl-0.4 (crate (name "sjfl") (vers "0.4.0") (deps (list (crate-dep (name "hmath") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "1fairli8zw69scr5b8bl8i1s5nabm53g3cq4m65jjd1hi6p4z46v")))

