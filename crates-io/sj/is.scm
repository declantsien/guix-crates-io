(define-module (crates-io sj is) #:use-module (crates-io))

(define-public crate-sjis-literals-0.1 (crate (name "sjis-literals") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0mzmra670rsqgdq3dr0m8j00ns1sdf7v76b10ybbi1yj1av1kzqg")))

