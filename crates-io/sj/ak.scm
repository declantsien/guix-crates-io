(define-module (crates-io sj ak) #:use-module (crates-io))

(define-public crate-sjakk-0.1 (crate (name "sjakk") (vers "0.1.0") (hash "1lp70hyqf7w7g4ya0g5fciqm6qk97fhxwjyk1dwk15l3vg134ah1")))

(define-public crate-sjakk-0.1 (crate (name "sjakk") (vers "0.1.1") (deps (list (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0iw4f15rgvgzsvn5qk48a90kn5w3mnywyncdarrz9yifj846n2db")))

(define-public crate-sjakk-0.1 (crate (name "sjakk") (vers "0.1.2") (deps (list (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "07ds8a95in5asv4zpb3nnf8w99jvk1hakqj7wc3qc4kh9kcfahd3")))

(define-public crate-sjakk-0.2 (crate (name "sjakk") (vers "0.2.0") (deps (list (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0is92y49dysg624mv2mhf2aa9fp5p9ykd3xw7alyq3x5198sr7ci")))

(define-public crate-sjakk-0.2 (crate (name "sjakk") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "16rs8ps7z7q0a2wlbhm1kjf521gf61ms7hzjwff5184gq5m0x798")))

(define-public crate-sjakk-0.2 (crate (name "sjakk") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1v2b04yg688qcp6sc80xh3ncywc825v3y6ipsw8c6kjwhna0x0xz")))

(define-public crate-sjakk-0.2 (crate (name "sjakk") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.28") (features (quote ("thread-pool"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0vfg4an0530sd9wiga7djz9692g8369laj3s7ymvj3dkajzvdf8b")))

