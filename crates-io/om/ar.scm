(define-module (crates-io om ar) #:use-module (crates-io))

(define-public crate-omar_minigrep-0.1 (crate (name "omar_minigrep") (vers "0.1.0") (hash "0mrqkpif2c3sc44xbyyrhja0mcsd7igjxm4vimn7ddjqkb03c0pm") (yanked #t)))

(define-public crate-omar_minigrep-0.1 (crate (name "omar_minigrep") (vers "0.1.1") (hash "1glvysp8sph7n9fz36ml60jxvf4hxk4j5la87gb5cbyphmsmsyig")))

