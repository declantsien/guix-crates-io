(define-module (crates-io om n-) #:use-module (crates-io))

(define-public crate-omn-sprites-0.1 (crate (name "omn-sprites") (vers "0.1.0") (deps (list (crate-dep (name "ggez") (req "^0.3") (features (quote ("cargo-resource-root"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 2)))) (hash "1l7jlr6g69cxiqi9qjlqhr7qny6y24r6smx98a42znhg43qws116") (features (quote (("default"))))))

