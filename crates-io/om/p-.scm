(define-module (crates-io om p-) #:use-module (crates-io))

(define-public crate-omp-codegen-0.1 (crate (name "omp-codegen") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15zmyn4d2f2fbb7fhdzj1xdjv4fr9pazzh2jmdfk0c14pcba8k9v")))

(define-public crate-omp-codegen-0.2 (crate (name "omp-codegen") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yfjzd1w99sc05iv5ciwjnva42k3f4cbhk9jzfh9smmx3q8hkir6")))

(define-public crate-omp-gdk-0.1 (crate (name "omp-gdk") (vers "0.1.0") (deps (list (crate-dep (name "omp-codegen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.52.0") (features (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0b893bknkh4bjzkxrmmbwbckd315grw52wyl0ajpdf0c7xvslrlp")))

(define-public crate-omp-gdk-0.2 (crate (name "omp-gdk") (vers "0.2.0") (deps (list (crate-dep (name "omp-codegen") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.52.0") (features (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gdafk1lkllw6jysyc2zynaagvsqwgpkdw91fca411fx5srb2n3y")))

