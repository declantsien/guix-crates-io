(define-module (crates-io om gw) #:use-module (crates-io))

(define-public crate-omgwtf8-0.1 (crate (name "omgwtf8") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "12idn59j5aaci0awavm8wgvqb7avkw5v7pjbfg8ln1zk5wy0snz1")))

