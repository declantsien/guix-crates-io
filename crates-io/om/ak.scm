(define-module (crates-io om ak) #:use-module (crates-io))

(define-public crate-omake-0.1 (crate (name "omake") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req ">=3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req ">=0.2.26") (default-features #t) (kind 0)))) (hash "1ddajsv8yj4c8z195dkxcdisljcdbgdd90vaxgsdsh8qf03np032") (yanked #t)))

(define-public crate-omake-0.0.0 (crate (name "omake") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req ">=3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req ">=0.2.26") (default-features #t) (kind 0)))) (hash "15ij3zk39wr1aijac9jf1r8k86srrwkgv4h1zrvhfiwnqapq52qw") (features (quote (("omake") ("make") ("default" "make")))) (yanked #t)))

(define-public crate-omake-0.1 (crate (name "omake") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req ">=3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req ">=0.2.26") (default-features #t) (kind 0)))) (hash "1cnbyp818ypdqpk2v4k7vk99rmln9n6sf1jfxj0khglkwsr0s7mn")))

(define-public crate-omake-0.1 (crate (name "omake") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req ">=3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req ">=0.2.26") (default-features #t) (kind 0)))) (hash "0b4sg72b6ii6yia2isk828abrc4y25xn214jzk923bzd2c8s7si2")))

(define-public crate-omake-0.1 (crate (name "omake") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pz66abp9wllz6g9bvr10fngixn28ly4mzakj8by2ickz6ac2rrq")))

(define-public crate-omake-0.1 (crate (name "omake") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1ckxplajw48ly9fbhr0gh9f2cj4nchq5ghgiigjpqwc475absk6p") (features (quote (("default" "bin") ("bin" "clap" "const_format"))))))

(define-public crate-omake-0.1 (crate (name "omake") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2") (default-features #t) (kind 0)))) (hash "0krxadavm9ylx0pkm28wm8ap66ffaqw89wxr9bms491a0f51x899")))

(define-public crate-omake-0.1 (crate (name "omake") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "1sr4xmpvncsdfawmlmbp1p1dhcqg8z2mxd3ka0264y7i910mwfqy")))

