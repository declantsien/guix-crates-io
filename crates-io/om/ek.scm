(define-module (crates-io om ek) #:use-module (crates-io))

(define-public crate-omekasy-0.1 (crate (name "omekasy") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "11ihvz9ya9nccsmw5hlr0pm3pk3z8wbd22v9d292f05q2r6hz2s2")))

(define-public crate-omekasy-0.2 (crate (name "omekasy") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "0rjfvqk8c3svv9rawv5kk7vwvagj4n989si5c5znri51kdg0ilsk")))

(define-public crate-omekasy-1 (crate (name "omekasy") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "1phw4m77s68z6xgza5m9y4rmxmhv5vyzldb91199kvqaczbmn4fn")))

(define-public crate-omekasy-1 (crate (name "omekasy") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "02964mmwhzxjbddc292gycbpchszxf5b82mkg7g329zjlzqvs6a9")))

(define-public crate-omekasy-1 (crate (name "omekasy") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (optional #t) (default-features #t) (kind 0)))) (hash "07246xs91z10bp5nkxkzr4hbid68gl52r1igpz654bmyacmpccb3") (features (quote (("default" "crossterm"))))))

(define-public crate-omekasy-1 (crate (name "omekasy") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (optional #t) (default-features #t) (kind 0)))) (hash "0px5f0cyy3lv2ndv5z3vpgq9nm9idsnz24vg2zbnxl8adhzny8jw") (features (quote (("default" "crossterm"))))))

(define-public crate-omekasy-1 (crate (name "omekasy") (vers "1.2.2") (deps (list (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (optional #t) (default-features #t) (kind 0)))) (hash "19ixn9s3fs4vgj65cpbf4sci8f940wp47zx6ir44cdcd33wcv9cg") (features (quote (("default" "crossterm"))))))

(define-public crate-omekasy-1 (crate (name "omekasy") (vers "1.2.3") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (optional #t) (default-features #t) (kind 0)))) (hash "0a1gim8qfcjgyz73z0c7ccqxd2w4fgfbz8vqlyji7wdw4xaapqyz") (features (quote (("default" "crossterm"))))))

