(define-module (crates-io om ag) #:use-module (crates-io))

(define-public crate-omage-0.1 (crate (name "omage") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "1jkdxda18ml0d7imaivq322iphn27a32fn1dlrhgznb9acfqnjvm") (yanked #t)))

(define-public crate-omage-0.2 (crate (name "omage") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "0bsxdj8bmc9nl8s342r9rgdbf9ydfnvhdv0mg96g6c9kajf8hkag") (yanked #t)))

(define-public crate-omage-0.2 (crate (name "omage") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "0bbs3ii06byx5wk3cvdxyr53mhkciaq5iiwnnx4mrhyx16yj1w1s") (yanked #t)))

(define-public crate-omage-0.2 (crate (name "omage") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "1m6x7mdf1if7w4z4lnyr089lz55cfbybp2xbb69av185s1kx4ghb") (yanked #t)))

(define-public crate-omage-0.2 (crate (name "omage") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "1lazxvz1jjv6gdmq4yksa6jbdvrrky748dgx50vkk3b6kkrczxn5") (yanked #t)))

(define-public crate-omage-0.2 (crate (name "omage") (vers "0.2.4") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "1car2yzpws8r371bfks9pghiwqznqxpjb95yvi842gz0snhv15wy")))

(define-public crate-omage-0.2 (crate (name "omage") (vers "0.2.5") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "14iy014gsikbv0qr9zzdqmhkl10qwm4b5982c80haydn9hfk2w40")))

(define-public crate-omage-0.2 (crate (name "omage") (vers "0.2.6") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)))) (hash "0v7f7k403qy9qv5hpl3zk5hg6y17d0p07kifrdg9w4n2fcnygda4")))

(define-public crate-omage-0.3 (crate (name "omage") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "19zr65h0bz2wphl14a5y0jxim12ibkwf7malarr81pw83h2xbpsn")))

(define-public crate-omage-0.3 (crate (name "omage") (vers "0.3.1") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1chw6yn0yk2qwy157c8dp7xr9liim0v8c9s3hbk0lwz2nqjlzflk")))

(define-public crate-omage-0.3 (crate (name "omage") (vers "0.3.11") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0bx6bp3avbh2scpia89cqxla23vvlafchc260s7if6fwiscvv2r5")))

