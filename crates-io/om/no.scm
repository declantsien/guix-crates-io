(define-module (crates-io om no) #:use-module (crates-io))

(define-public crate-omnom-1 (crate (name "omnom") (vers "1.0.0") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "05l3shgl9dal4x0r3yjwyj9a1cs104l004hmscdmxcjjj2m5zw0r")))

(define-public crate-omnom-1 (crate (name "omnom") (vers "1.1.0") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0gg1nl3mi3xzs3imj0cyrm1ivfg4yfirg8wqsji1sy5p8f1p9wkk")))

(define-public crate-omnom-1 (crate (name "omnom") (vers "1.2.0") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0wp12a57hjkkmiyq4idr36vlk4mwhk8yi35l1c0da9whhxcmx86p")))

(define-public crate-omnom-2 (crate (name "omnom") (vers "2.0.0") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "17h6l31lv33hl01f6lh1i1wd576zkb53dk7k00j5b8k0byb2cniv")))

(define-public crate-omnom-2 (crate (name "omnom") (vers "2.1.0") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0vxxzpjbnd9h7k14qilzg87qj1dsqh9xv5xh31bn458n46q434h8")))

(define-public crate-omnom-2 (crate (name "omnom") (vers "2.1.1") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "164d27gv46nsv02pc9rsr6x3ddgqim96b5x2zhqpp4nh59sf5psn")))

(define-public crate-omnom-2 (crate (name "omnom") (vers "2.1.2") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1lwz9rj9lvsmj36nj5n1f7jkj2y8d1ad2n1iyy0fdmp0wb71dcnn")))

(define-public crate-omnom-3 (crate (name "omnom") (vers "3.0.0") (deps (list (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0662rz18f1bkv9bw53fhglvd5qxg7hnpqna9splkfnbdqs92lmk6")))

