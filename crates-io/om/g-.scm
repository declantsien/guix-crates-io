(define-module (crates-io om g-) #:use-module (crates-io))

(define-public crate-omg-api-0.0.0 (crate (name "omg-api") (vers "0.0.0") (hash "1izfmapj8x7gypaxdnr8hi41b4fhsaa1k74ai471x0lzixvdfbb0")))

(define-public crate-omg-api-0.1 (crate (name "omg-api") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0n0apfm8y579bfr1h5clh34p92yfd8lcy766m23g0drdmvg2jyjj")))

(define-public crate-omg-api-0.2 (crate (name "omg-api") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "const_format") (req "^0.2.31") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "structstruck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0vnw9b805qbg631p9b8vij7waha7rm3ls3y9r9ws8l9bb40nir9n")))

(define-public crate-omg-cool-0.1 (crate (name "omg-cool") (vers "0.1.0") (deps (list (crate-dep (name "configparser") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "enum-iterator") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.4") (features (quote ("os-poll" "net"))) (default-features #t) (kind 0)))) (hash "0qwvpmp3ph0icsla73kmwja6ijlb1xhaf505w7xj2x142ps57wql")))

(define-public crate-omg-cool-0.1 (crate (name "omg-cool") (vers "0.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "enum-iterator") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.4") (features (quote ("os-poll" "net"))) (default-features #t) (kind 0)))) (hash "11s5az5i1f0zbix40l3cl6zzjz8afdn262sw1c923d8ijyabhz1p")))

(define-public crate-omg-cool-0.1 (crate (name "omg-cool") (vers "0.1.3") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "enum-iterator") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.4") (features (quote ("os-poll" "net"))) (default-features #t) (kind 0)))) (hash "0xlhkhb650dvd292x02yxbzxc0jb9mc9dqyihw62kk26v9978hp9")))

(define-public crate-omg-cool-0.1 (crate (name "omg-cool") (vers "0.1.4") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "enum-iterator") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.8.4") (features (quote ("os-poll" "net"))) (default-features #t) (kind 0)))) (hash "1prj407wmbjrasd9vra449gxm97lkvvxzw3jlg2w9bqxj96xbww8")))

(define-public crate-omg-gui-0.0.0 (crate (name "omg-gui") (vers "0.0.0") (hash "1pakf9cfiqb0wvpkx2rgq1dqdm4vkhx7plvf1yv885z0f591iinp")))

(define-public crate-omg-html-0.1 (crate (name "omg-html") (vers "0.1.0") (deps (list (crate-dep (name "indoc") (req "^1.0") (default-features #t) (kind 2)))) (hash "1sy18ga7fjpvv3crfigj9ccv0l5b9hwj9c6wr4z0g59k3g93ljbh")))

(define-public crate-omg-html-0.1 (crate (name "omg-html") (vers "0.1.1") (deps (list (crate-dep (name "indoc") (req "^1.0") (default-features #t) (kind 2)))) (hash "0g6hmaijv6xm2wxljkw9jmxjcshxfyfndg1an6pjc9wy6wpcp0wa")))

(define-public crate-omg-tui-0.0.0 (crate (name "omg-tui") (vers "0.0.0") (hash "1kjlx25k70k3qrqgaqb015szcin3cbd6fqgn8yk8fg4xvxy2r73k")))

