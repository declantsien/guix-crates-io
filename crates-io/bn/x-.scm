(define-module (crates-io bn x-) #:use-module (crates-io))

(define-public crate-bnx-cal-0.1 (crate (name "bnx-cal") (vers "0.1.0") (hash "1hwg7bliaiamqkc263c8ww63v73rva8k461gmxmsq4xzah7j2327")))

(define-public crate-bnx-sal-0.1 (crate (name "bnx-sal") (vers "0.1.0") (hash "0lkih0yi9r3s8w8jjc3mxhr256pydmb5r4qj9cwamf41mn02bsg0")))

