(define-module (crates-io bn -p) #:use-module (crates-io))

(define-public crate-bn-plus-0.4 (crate (name "bn-plus") (vers "0.4.4") (deps (list (crate-dep (name "bincode") (req "^0.6") (features (quote ("rustc-serialize"))) (kind 2)) (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jinrwrk2a35sw4j2mf6myzfv8rmj2f0nzz7gqk8f3sspka3f07r")))

