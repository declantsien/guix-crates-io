(define-module (crates-io bn dp) #:use-module (crates-io))

(define-public crate-bndpresbufq-0.1 (crate (name "bndpresbufq") (vers "0.1.0") (deps (list (crate-dep (name "limq") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1z5gwvvwwp3bm52ki373ga6ln3gh9nrfv7s1zl51bjj6wnjvk2qb") (rust-version "1.56")))

