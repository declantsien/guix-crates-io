(define-module (crates-io tx t-) #:use-module (crates-io))

(define-public crate-txt-cluster-0.1 (crate (name "txt-cluster") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.9") (default-features #t) (kind 0)))) (hash "0cs2ikgfcfzifv4pn7by9nnh65q012040g2sz3nsywngl576y8wm")))

(define-public crate-txt-viewer-0.1 (crate (name "txt-viewer") (vers "0.1.0") (deps (list (crate-dep (name "gtk4") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1hm0gsrpakmwy9pbqikdw150i75cqrh3s78pqpjj4925vlznkmff")))

