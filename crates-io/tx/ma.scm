(define-module (crates-io tx ma) #:use-module (crates-io))

(define-public crate-txmailer-0.0.1 (crate (name "txmailer") (vers "0.0.1") (hash "1x95z2w8g3jppbl9rh6lca02kp8x42z9nq4k1yr3yjalg28sganr")))

(define-public crate-txmailer-0.0.2 (crate (name "txmailer") (vers "0.0.2") (hash "1gjbaqgfwv1gr6r8q01zs47vsl2zxy1g6wwm98d6abxq2510rg6i")))

