(define-module (crates-io tx tt) #:use-module (crates-io))

(define-public crate-txttyp-0.1 (crate (name "txttyp") (vers "0.1.0") (deps (list (crate-dep (name "ansistr") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "057axmxv0zlxr7zl98d5di2l09vp9kvvk4k0nrl4xqa4x3pjvw3m")))

(define-public crate-txttyp-0.1 (crate (name "txttyp") (vers "0.1.1") (deps (list (crate-dep (name "ansistr") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "03gs15i70kwrjm9hl9mc2v56krqwnsqywrgrqj7hgmxsklr95lq6")))

(define-public crate-txttyp-0.1 (crate (name "txttyp") (vers "0.1.2") (deps (list (crate-dep (name "ansistr") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0c1f1r3h5yc88x215n811qdw61xpza7qmbiyz7w2m6x1w2rdc582")))

