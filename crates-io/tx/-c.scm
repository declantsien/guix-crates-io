(define-module (crates-io tx -c) #:use-module (crates-io))

(define-public crate-tx-custom-boot-0.1 (crate (name "tx-custom-boot") (vers "0.1.0") (deps (list (crate-dep (name "binwrite") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "0carjp74rqlcmrcwmf9lfnwmynidvys5hypgjndqb1wghsgd0clh")))

(define-public crate-tx-custom-boot-0.2 (crate (name "tx-custom-boot") (vers "0.2.0") (deps (list (crate-dep (name "binwrite") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1a7hcqvfj8i1srihzsbdfdx13br8rm85ygpd1k5522553plmly5f")))

(define-public crate-tx-custom-boot-0.3 (crate (name "tx-custom-boot") (vers "0.3.0") (deps (list (crate-dep (name "binwrite") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "conv") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0idffxfgsmdh89blf6mi4x21p2p7hx6agfrbwnxdjl2s368whlzx")))

