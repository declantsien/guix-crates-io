(define-module (crates-io tx -p) #:use-module (crates-io))

(define-public crate-tx-padding-0.1 (crate (name "tx-padding") (vers "0.1.0") (deps (list (crate-dep (name "block-padding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("getrandom"))) (kind 0)) (crate-dep (name "typenum") (req "^1.12") (kind 0)))) (hash "1kq5igigc5y84rlci4dlkfh0p5mzsap11hjhcm76c5kmbpnzhq65") (features (quote (("thread_rng"))))))

(define-public crate-tx-padding-0.1 (crate (name "tx-padding") (vers "0.1.1") (deps (list (crate-dep (name "block-padding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("getrandom"))) (kind 0)) (crate-dep (name "typenum") (req "^1.12") (kind 0)))) (hash "19yjydmbaipzy3ng9xvbk6skahyk6dqns036c1h30w7zl9b8lwp0") (features (quote (("thread_rng"))))))

(define-public crate-tx-padding-0.1 (crate (name "tx-padding") (vers "0.1.2") (deps (list (crate-dep (name "block-padding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("getrandom"))) (kind 0)) (crate-dep (name "typenum") (req "^1.12") (kind 0)))) (hash "0pkbdngs9p7fbpai58cigp2k2r1f40wjcbbd3syc46050bphrm8g") (features (quote (("thread_rng"))))))

