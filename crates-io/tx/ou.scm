(define-module (crates-io tx ou) #:use-module (crates-io))

(define-public crate-txoutset-0.1 (crate (name "txoutset") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin") (req "^0.31.1") (default-features #t) (kind 0)))) (hash "15409s4p1x857vjgpqliv39jncqq525chx53jy48ih2aq1c5i97y")))

(define-public crate-txoutset-0.2 (crate (name "txoutset") (vers "0.2.0") (deps (list (crate-dep (name "bitcoin") (req "^0.31.1") (default-features #t) (kind 0)))) (hash "1xnv61f3yzqfhv3qh912h2n2xzxwqnfnk6yi97f37994nrw7pw4g")))

(define-public crate-txoutset-0.3 (crate (name "txoutset") (vers "0.3.0") (deps (list (crate-dep (name "bitcoin") (req "^0.31.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)))) (hash "1447jylrsjqjkk5gfwcc3bvwkv4nx9qv7n608nia3wnm170d6vh9")))

