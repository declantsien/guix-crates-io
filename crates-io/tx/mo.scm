(define-module (crates-io tx mo) #:use-module (crates-io))

(define-public crate-txmodems-0.1 (crate (name "txmodems") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (kind 0)) (crate-dep (name "core2") (req "^0.4.0") (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "11r6r1l0pg7gr75zmxlldhm1dijgqqhxl7vi51sbg5lbj0abn0ad") (features (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

(define-public crate-txmodems-0.1 (crate (name "txmodems") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (kind 0)) (crate-dep (name "core2") (req "^0.4.0") (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0yhinb88kdwpf23dpb68z1dgmrfr1l3fdfm88c4m64skilw0vq5y") (features (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

(define-public crate-txmodems-0.1 (crate (name "txmodems") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (kind 0)) (crate-dep (name "core2") (req "^0.4.0") (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "038j70h4lchnnndi0sn07nwhiy7gazki77wmvxps50v8vlmvp8yv") (features (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

(define-public crate-txmodems-0.1 (crate (name "txmodems") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (kind 0)) (crate-dep (name "core2") (req "^0.4.0") (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "17n0690ml6k12hamhs8jz61l0zlgp9ndw5gjszblpingqqqk78z1") (features (quote (("zmodem") ("ymodem") ("xmodem") ("default"))))))

