(define-module (crates-io tx tu) #:use-module (crates-io))

(define-public crate-txture-0.1 (crate (name "txture") (vers "0.1.0") (deps (list (crate-dep (name "bmp") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "18wm1k2hgid9wmr3l6mkfa5qynjig5iwk7i0ga6fiaz7242ay9gr")))

(define-public crate-txture-0.1 (crate (name "txture") (vers "0.1.1") (deps (list (crate-dep (name "bmp") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0rig838q9b7x3xg1jnzva3i5v42kbsncqxamvqgzy8k2s7vp8v4r")))

