(define-module (crates-io tx rx) #:use-module (crates-io))

(define-public crate-txrx-0.1 (crate (name "txrx") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.6") (kind 0)))) (hash "1sa84d6x1fpqyv71z3f0k7s9d7h1qk16qgkrwarbkbf7n5f4vm3x") (features (quote (("test"))))))

