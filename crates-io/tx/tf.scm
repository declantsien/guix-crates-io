(define-module (crates-io tx tf) #:use-module (crates-io))

(define-public crate-txtframe-0.1 (crate (name "txtframe") (vers "0.1.0") (deps (list (crate-dep (name "smallstr") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1iznhn4q4fb8r3xy64zkvrlsid53h09liv944kkihf7cgcih33f9")))

(define-public crate-txtframe-0.1 (crate (name "txtframe") (vers "0.1.1") (deps (list (crate-dep (name "smallstr") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0ldr01lm1m2aa1zcxkg2czjxjf0hlsjsm2r0k9ksy5qa7cndal30")))

(define-public crate-txtframe-0.1 (crate (name "txtframe") (vers "0.1.2") (deps (list (crate-dep (name "smallstr") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1hz6mqlpk66bk0ig83gy8mzaj9crnzi2ipz9hw8jylzf9hxnqkr3")))

(define-public crate-txtframe-0.2 (crate (name "txtframe") (vers "0.2.0") (deps (list (crate-dep (name "smallstr") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "01b4gfr60d22k4z5p4daj7ivlb35b440rn2bisshy8jfpa92z6ma") (features (quote (("default" "color") ("color"))))))

(define-public crate-txtframe-0.3 (crate (name "txtframe") (vers "0.3.0") (deps (list (crate-dep (name "smallstr") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "09zbr225dhpiinx43k594h18cmxdpvj5rlhqi541n7f1g3i1fbw3") (features (quote (("newline") ("default" "color") ("color"))))))

(define-public crate-txtframe-0.4 (crate (name "txtframe") (vers "0.4.0") (deps (list (crate-dep (name "smallstr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0wg7yj0f1hlmdlahqj203x2dyig1dbpf9ysyxwzyiijl28vsbv3v") (features (quote (("newline") ("esc") ("default" "color") ("color"))))))

