(define-module (crates-io tx t2) #:use-module (crates-io))

(define-public crate-txt2048-0.1 (crate (name "txt2048") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1k0p1k1wp5i78xva3axq4bw5y1rbc4j54qiqyw7f92csn15xhkz9")))

(define-public crate-txt2sql-0.1 (crate (name "txt2sql") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0rb89680zrl2a212f27130lxjyw3lrh87jn542rd8azk405y54za")))

