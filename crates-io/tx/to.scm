(define-module (crates-io tx to) #:use-module (crates-io))

(define-public crate-txtools-0.1 (crate (name "txtools") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "rfd") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0g98xk0k187b4gbb0i2x4xac5kasc3nlgvl5213nd8mrgnp7mzc6")))

(define-public crate-txtools-0.1 (crate (name "txtools") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "rfd") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1zxb1izxqjlzfp7vx4sldk0pj4iq3ixs1rdizkxhjdipp3sisp3q")))

