(define-module (crates-io tx t_) #:use-module (crates-io))

(define-public crate-txt_lower-0.1 (crate (name "txt_lower") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)))) (hash "1l1vzwk02mg0dg4s9glxvsvzwm38vpirlk57k0iysasalllznhvl")))

(define-public crate-txt_otp-1 (crate (name "txt_otp") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1szapp8n3lpmkh1qzkwmh4mp78yj2smprs7vl6slsn1kxavgkyqp")))

(define-public crate-txt_otp-2 (crate (name "txt_otp") (vers "2.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "11fp9sk8pi9imkv68icnff82hr7ncbqn9k97cn3dcq64kix7bmqv")))

(define-public crate-txt_processor-0.1 (crate (name "txt_processor") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "1q74xkacjii7jz2a5sciwxbl1fzs82cm320j0xmcd0l50xn7k8y8")))

(define-public crate-txt_processor-0.1 (crate (name "txt_processor") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "1dnrf42kcmnxiadkbc98wnbazvwacizmld01y4rmy0h1l0bbzz86")))

(define-public crate-txt_processor-0.1 (crate (name "txt_processor") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "13cnm6ivzc7jbwjkpjdddfhxfpgn0qnqaddc0ahidmmdqmw7c57s")))

(define-public crate-txt_processor-0.1 (crate (name "txt_processor") (vers "0.1.4") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "1hl12vkn0dapg2j2qsdwx0nhlyssb02q17kncbx562pw9wcf8k8s")))

(define-public crate-txt_to_md-0.1 (crate (name "txt_to_md") (vers "0.1.0") (deps (list (crate-dep (name "carrot_utils") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0axg9j247aq4ss13kprr5v73f802k25agdxfks1q8p59pji8kqlz")))

(define-public crate-txt_to_md-0.1 (crate (name "txt_to_md") (vers "0.1.1") (deps (list (crate-dep (name "carrot_utils") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "03hqd9d48dpk99xw182lvw68iaav0nfbka5x0jfb74xphbp6c0vd")))

(define-public crate-txt_writer-0.1 (crate (name "txt_writer") (vers "0.1.0") (hash "144xij6hwqkwkqkg6qxp38li77h7wnbpgmmcvaw2zjdmb7ph7scs")))

(define-public crate-txt_writer-0.1 (crate (name "txt_writer") (vers "0.1.1") (hash "0dfb2zp52b3n55yv0g225r3y4p2mqacgxys475y3jsigb42xm8sj")))

(define-public crate-txt_writer-0.1 (crate (name "txt_writer") (vers "0.1.2") (hash "1h114m4mpnx28y1y2li057y1sw3s2025xx9m82sv0y83viay6jny")))

(define-public crate-txt_writer-0.1 (crate (name "txt_writer") (vers "0.1.3") (hash "0y2197dkhyrqyhf3127wzrsifg0g75v5fp2hmz0d5xcgxsqx5ams")))

(define-public crate-txt_writer-0.1 (crate (name "txt_writer") (vers "0.1.4") (hash "0l5y0gjfbz2dljd4cr71x37b6ik0s7a00vwgfz4lp06bnqcclyhs")))

