(define-module (crates-io cr el) #:use-module (crates-io))

(define-public crate-crelang-0.100 (crate (name "crelang") (vers "0.100.0") (deps (list (crate-dep (name "crelang-core") (req "^0.100.0") (default-features #t) (kind 0)) (crate-dep (name "crelang-syn") (req "^0.100.0") (default-features #t) (kind 0)) (crate-dep (name "libcre") (req "^0.100.0") (default-features #t) (kind 0)))) (hash "0vhbji0s1yk8szkg94yixrn776hlix6j8qb0av1aafh6sbx6k8lr")))

(define-public crate-crelang-core-0.100 (crate (name "crelang-core") (vers "0.100.0") (deps (list (crate-dep (name "libcre") (req "^0.100.0") (default-features #t) (kind 0)))) (hash "0h5xwyxzqqj37z8mq2xp1cq9hx617vljvsn6hbn1ja5p7hndkpvc")))

(define-public crate-crelang-syn-0.100 (crate (name "crelang-syn") (vers "0.100.0") (deps (list (crate-dep (name "crelang-core") (req "^0.100.0") (default-features #t) (kind 0)) (crate-dep (name "libcre") (req "^0.100.0") (default-features #t) (kind 0)))) (hash "11ibr6ib2ajy7d4kc3c1f2ipafdm3yqnbm8smxplsrf1rx2i0i93")))

