(define-module (crates-io cr ib) #:use-module (crates-io))

(define-public crate-cribbage-0.1 (crate (name "cribbage") (vers "0.1.0") (hash "1zjaa1r8l4h5arsw9j9damqyh1b7sr8wslg54vmxymc18xf7r8bl") (yanked #t)))

(define-public crate-cribbage-0.1 (crate (name "cribbage") (vers "0.1.1") (deps (list (crate-dep (name "cribbage-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n1866g9alxd4ain1m1wq1x0lhfwcfsxisfkb1vmj9bnl8xbl4bi") (yanked #t)))

(define-public crate-cribbage-0.1 (crate (name "cribbage") (vers "0.1.2") (deps (list (crate-dep (name "cribbage-core") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0d1rgr44278yzih4ajsmf3xjwaxla0183bvzzc198pbianmahacr") (yanked #t)))

(define-public crate-cribbage-0.1 (crate (name "cribbage") (vers "0.1.3") (deps (list (crate-dep (name "cribbage-core") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ahiznqlhzvwywka4h6qlyi2blhnjxmm7in9sfprnh3w689xv3n7") (yanked #t)))

(define-public crate-cribbage-0.1 (crate (name "cribbage") (vers "0.1.4") (deps (list (crate-dep (name "cribbage-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1m7ymfhrzbj30sjm2l6pvbdxizcxjf50y10fg61ixswapnqaw4vg") (yanked #t)))

(define-public crate-cribbage-0.1 (crate (name "cribbage") (vers "0.1.5") (deps (list (crate-dep (name "cribbage-core") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.1") (default-features #t) (kind 0)))) (hash "00c99dignyqfbh79gpfyhpgb4sv8zcl1ca93y8mzqr9jg7dymg84")))

(define-public crate-cribbage-core-0.1 (crate (name "cribbage-core") (vers "0.1.0") (hash "0gb8g2dpqcv71s4qmri9bc7d955zphpj1w60jhz38hc4iyjrmg84") (yanked #t)))

(define-public crate-cribbage-core-0.1 (crate (name "cribbage-core") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0pagzx75cg5jdy7a4zrihy60d038qip2fhv88pg56g4f4731358j") (features (quote (("extensive-tests")))) (yanked #t)))

(define-public crate-cribbage-core-0.1 (crate (name "cribbage-core") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0dnhmrqadxiw4yhxvcawksi32j551yfsimq288vwn0076r7wsivc") (features (quote (("extensive-tests")))) (yanked #t)))

(define-public crate-cribbage-core-0.1 (crate (name "cribbage-core") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1vi302yf3hzsgrmswkas1gc4fhlc1i3wkpc67p1s31c9ybs2sg56") (features (quote (("extensive-tests"))))))

