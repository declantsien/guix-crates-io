(define-module (crates-io cr y-) #:use-module (crates-io))

(define-public crate-cry-sys-0.1 (crate (name "cry-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.67") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "1w34qp9sjlwb9smcpgll0n5pjqck93ikydhcrk0mjswxyh4al9wi")))

