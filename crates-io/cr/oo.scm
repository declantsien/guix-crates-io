(define-module (crates-io cr oo) #:use-module (crates-io))

(define-public crate-crook_calculator-0.1 (crate (name "crook_calculator") (vers "0.1.0") (hash "12pcqwhvpnmmrjqc36v3ghx3b6mvvf7zhwxxcak4v0yiy965qb4g")))

(define-public crate-crook_calculator-0.1 (crate (name "crook_calculator") (vers "0.1.1") (hash "1gzcizw34g4hjbyvlk5bd9np9g66zwmwp72z96nqyc8jskrnr595")))

(define-public crate-croot-0.1 (crate (name "croot") (vers "0.1.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0010aivw8b232psmv0zq1yk16yiwm40kldia4ygvfyhhspyi4cvn") (yanked #t)))

(define-public crate-croot-0.1 (crate (name "croot") (vers "0.1.1") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "14hsx1fwhxgbwdwd2xd59q2vnlz7z5y63hw404wwqhlnsjybzw4f") (yanked #t)))

(define-public crate-croot-0.2 (crate (name "croot") (vers "0.2.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0zdkhsvikrnawds62assk9fw5qz62sp2dkn13hszk68aj4vv59b2") (yanked #t)))

(define-public crate-croot-0.2 (crate (name "croot") (vers "0.2.1") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0hjx5i34w92j91mds0k67pq8hr6x1y6qmj1yqkdasji707mv3z54") (yanked #t)))

(define-public crate-croot-0.3 (crate (name "croot") (vers "0.3.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0fbcis40gvrfsl216m4hfjgfg3sdb3hp4bkwiksig146qz7qymc7")))

(define-public crate-croot-0.3 (crate (name "croot") (vers "0.3.1") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0z4kbdvqqajsnlxl7iqjakhdxcv1cjwg2rpn13rm9fbv3bdj0qvq")))

(define-public crate-croot-0.3 (crate (name "croot") (vers "0.3.2") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0a6wrhswp80yc1dv5112vzxqmijhyl4zngjhywk1zrq3750gg8ln")))

(define-public crate-croot-0.3 (crate (name "croot") (vers "0.3.3") (deps (list (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "04zgv3hnw8r7qll35dgkr5zabwbsll08qrdrf03pz1b4601qg073")))

(define-public crate-croot-gui-0.1 (crate (name "croot-gui") (vers "0.1.0") (deps (list (crate-dep (name "croot") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.21.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_extras") (req "^0.21.0") (features (quote ("image"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("jpeg" "png"))) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1rq7n1as67yqwhw771x381px4rfsn6hyd3nmwnggsw23s3i4427z") (v 2) (features2 (quote (("gui" "dep:eframe" "dep:egui_extras"))))))

