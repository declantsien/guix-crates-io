(define-module (crates-io cr of) #:use-module (crates-io))

(define-public crate-croftsoft-0.1 (crate (name "croftsoft") (vers "0.1.0") (hash "13aklzqfpddhm1salh6368a7iznhphv5nz7bs8pzhf5j85dz9c35") (yanked #t)))

(define-public crate-croftsoft-0.1 (crate (name "croftsoft") (vers "0.1.1") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1h5zb7mrgc4dfcvhd4p9lrl6lljb3ys45nfc8fh6sj9y99a4gm1f")))

(define-public crate-croftsoft-0.2 (crate (name "croftsoft") (vers "0.2.0") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0scnwzf24jpwskyihy3a0nhwgpc0f42p62wajj0jqgghj3cx8sr5")))

(define-public crate-croftsoft-0.9 (crate (name "croftsoft") (vers "0.9.0") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0kv8763yvzfv1nk2vxx533zlv66d2kpk6chq6c1r57qws8zswk2s")))

(define-public crate-croftsoft-0.10 (crate (name "croftsoft") (vers "0.10.0") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0c4p3dmar1xv0bf4n73nvfk6v8hp6nf1x7nq19achmjrlmrx45mx")))

(define-public crate-croftsoft-0.10 (crate (name "croftsoft") (vers "0.10.1") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "19l05zdiyqy6f5c2m9ijwp3qf3jcq9fjrglqcrl8mh7jpi8mavfv")))

