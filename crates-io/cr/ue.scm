(define-module (crates-io cr ue) #:use-module (crates-io))

(define-public crate-cruet-0.11 (crate (name "cruet") (vers "0.11.4") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1aqnh9grq7qcggkndacdx6d0xby1kgdyxi1kk5frfxknw4166c34") (features (quote (("unstable") ("heavyweight" "regex" "lazy_static") ("default" "heavyweight"))))))

(define-public crate-cruet-0.12 (crate (name "cruet") (vers "0.12.0") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "07s3fff5bjm6hlvjymsr29bgq5fi6gj8vbpbsxzly1im899vjcc1")))

(define-public crate-cruet-0.13 (crate (name "cruet") (vers "0.13.0") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0bzq841nx7zy3y32f607m3yzy41ncy4bf8hr2g4j7m3j27pxdakx")))

(define-public crate-cruet-0.13 (crate (name "cruet") (vers "0.13.1") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0w3jsf5cnxffjznh5w2sh50gyk45rkgps1qibsyha34dmpwml042")))

(define-public crate-cruet-0.13 (crate (name "cruet") (vers "0.13.2") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1qzwf3xln46mqqry5lgbw5dcmhmahcsj393hdyqixlf7cm3if3af")))

(define-public crate-cruet-0.13 (crate (name "cruet") (vers "0.13.3") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1774i91y3azksfii0dspxa2nxsnhz5dz5wcdvrvbw57nv21rwfhi")))

(define-public crate-cruet-0.14 (crate (name "cruet") (vers "0.14.0") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1bdfbidy1x456gmsnk7b1a3nhrsqwqf1zq5ijyy9c94p8fan0ck1")))

