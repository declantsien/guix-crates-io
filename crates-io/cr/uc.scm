(define-module (crates-io cr uc) #:use-module (crates-io))

(define-public crate-crucial-0.0.0 (crate (name "crucial") (vers "0.0.0") (hash "1sslr594s4lisbnfc13imv22r154r5ra28qifhz87rn8d8pka293") (rust-version "1.77")))

(define-public crate-crucial-0.0.1 (crate (name "crucial") (vers "0.0.1") (hash "1ibfbixs729bvrfl7xsy5lb8zxy1wz3qp0pxxa57qlnj2hjbvnkg") (rust-version "1.77")))

(define-public crate-crucible-0.0.0 (crate (name "crucible") (vers "0.0.0") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "tic") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0fbb8vjr56fbccm3kb8xxjlgfc5lm2gc0bp957ck9s00d2vcl4jn")))

(define-public crate-crucible-workspace-hack-0.1 (crate (name "crucible-workspace-hack") (vers "0.1.0") (hash "1izx0l1cydwj69gdv2zwplxybj9zwcyjcx9n28sc7fbc1hvr7lpv")))

(define-public crate-crucio-0.1 (crate (name "crucio") (vers "0.1.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("async-await" "nightly" "compat"))) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a785lmqf5w3560s89fb0mvs558g47sy8mdnnfslwn2ryn0azbbd")))

