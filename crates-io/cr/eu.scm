(define-module (crates-io cr eu) #:use-module (crates-io))

(define-public crate-creusot-contracts-0.1 (crate (name "creusot-contracts") (vers "0.1.0") (deps (list (crate-dep (name "creusot-contracts-dummy") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "creusot-contracts-proc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0szhhn7drx8fvpc489j3lrdp4b6lq8xgqgdn445dy5nhs61abn4r") (features (quote (("typechecker") ("default") ("contracts"))))))

(define-public crate-creusot-contracts-dummy-0.1 (crate (name "creusot-contracts-dummy") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "1xm5gagpc5c595ll773w4dwv3plsjjss5l5lygrdpiivkazbsdrd")))

(define-public crate-creusot-contracts-proc-0.1 (crate (name "creusot-contracts-proc") (vers "0.1.0") (deps (list (crate-dep (name "pearlite-syn") (req "^0.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1xrm4xr9hlj6c4jg276lkbg4ldyvbfj4zl73qsnp9sjqv86b3irb")))

