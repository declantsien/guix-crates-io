(define-module (crates-io cr c1) #:use-module (crates-io))

(define-public crate-crc16-0.1 (crate (name "crc16") (vers "0.1.0") (hash "1xrf67v8sja2xl17692zffzlribaqchb7js7c6ik6mjpq4vxh7x5")))

(define-public crate-crc16-0.2 (crate (name "crc16") (vers "0.2.0") (hash "1yhjfs56kh1fn1hn74lbv34mxfvswxrli4x2jryjls6rbc42jmd4")))

(define-public crate-crc16-0.3 (crate (name "crc16") (vers "0.3.0") (hash "19qbizm7jypwqxafj5wj6gjryv41mkwlnc38y7k3f8pdvyhipy4n")))

(define-public crate-crc16-0.3 (crate (name "crc16") (vers "0.3.1") (hash "03ancp64vnc4mkp5k9746inw1p3zxaaafikxg7vxyxb0hlp63wcm")))

(define-public crate-crc16-0.3 (crate (name "crc16") (vers "0.3.2") (hash "0synjr0vvqpcnbxgdvgpbbgb30sg6knpdiarv9qp73mhs89mbrqx")))

(define-public crate-crc16-0.3 (crate (name "crc16") (vers "0.3.3") (hash "1b79jdz5ai518mmjh33gc7y9zpfxhlfj0zxyr4is9n036hsr4an7")))

(define-public crate-crc16-0.3 (crate (name "crc16") (vers "0.3.4") (hash "1pg28xdz27mck17jl247vhdrpixgfmw3gq25b4x3wbrkjx3mr9hi")))

(define-public crate-crc16-0.4 (crate (name "crc16") (vers "0.4.0") (hash "1zzwb5iv51wnh96532cxkk4aa8ys47rhzrjy98wqcys25ks8k01k")))

