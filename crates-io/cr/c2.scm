(define-module (crates-io cr c2) #:use-module (crates-io))

(define-public crate-crc24-0.0.1 (crate (name "crc24") (vers "0.0.1") (hash "14qk14cppj6rpmnilx4kdas5wgdm33ll08y2mmqr0wz9niafp24x")))

(define-public crate-crc24-0.0.2 (crate (name "crc24") (vers "0.0.2") (hash "1klswd5lhddbq6l83n89cwrzi9xf2qvav3dnnxz2xja2jb7ryk9d")))

(define-public crate-crc24-0.0.3 (crate (name "crc24") (vers "0.0.3") (hash "1awdn441ji2d8117i4l7ad8n5n6sfcmr95bj8yfx6rar3wvb5zyp")))

(define-public crate-crc24-0.1 (crate (name "crc24") (vers "0.1.0") (hash "05g1vs13kyvqx7vgh5fz8afwydggzlxg99znppg061msvsp4nf1y")))

(define-public crate-crc24-0.1 (crate (name "crc24") (vers "0.1.1") (hash "0321icx95mlaicyd7pq0n194gr18nvynw031qdibmirbsyv12nhw")))

(define-public crate-crc24-0.1 (crate (name "crc24") (vers "0.1.2") (hash "0xxjsawxqdcpwsr69j8v4ah9pby9pv3z43a0w2r4ppkizjns16w3")))

(define-public crate-crc24-0.1 (crate (name "crc24") (vers "0.1.3") (hash "15fqycvz565ywl5f0xdjb8g1wjda22m7x70yg7zdml0xk1vighgx")))

(define-public crate-crc24-0.1 (crate (name "crc24") (vers "0.1.4") (hash "1cc5xgg10k0cqmh128pnv9znqm4jpx659vr6bzlcw1ls35xgwpd2")))

(define-public crate-crc24-0.1 (crate (name "crc24") (vers "0.1.5") (hash "0sdg5lplps1nijfp7fkhwzs6gcqfv5i7isf53smyqbwdz61qnnhw")))

(define-public crate-crc24-0.1 (crate (name "crc24") (vers "0.1.6") (hash "1876c92swdpq1iv8j3y21vvfar96pxayn8rhvl42rf1yrx0if4px")))

(define-public crate-crc24-openpgp-fast-0.1 (crate (name "crc24-openpgp-fast") (vers "0.1.1") (deps (list (crate-dep (name "crc24") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0mgv0836wg1pai21xmfj55ny51qbczvqasrwqsnhlagg99z980hr")))

(define-public crate-crc24-openpgp-fast-0.1 (crate (name "crc24-openpgp-fast") (vers "0.1.2") (deps (list (crate-dep (name "crc24") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0sjq8w2dsbsgsvq6x3jfzsk29jq241kncajibgzzclc628k6zxn6")))

(define-public crate-crc24-openpgp-fast-0.1 (crate (name "crc24-openpgp-fast") (vers "0.1.3") (deps (list (crate-dep (name "crc24") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1hqbpwzgpfhcaq0rwaci82g9wwrbpjrh0bqilqr4r2cdj0xp7hrj")))

