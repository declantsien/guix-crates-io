(define-module (crates-io cr c3) #:use-module (crates-io))

(define-public crate-crc32-0.0.1 (crate (name "crc32") (vers "0.0.1") (hash "15y37pahd06a02clvzx3xbkpvxchpvl8gpv97i5w3kinld84b4m4")))

(define-public crate-crc32-0.0.2 (crate (name "crc32") (vers "0.0.2") (hash "1zy351clxb3gbxv0y3zjl181zxgybaxsbhvgjnkgxwiqbz2vl32b")))

(define-public crate-crc32-v2-0.0.0 (crate (name "crc32-v2") (vers "0.0.0") (hash "1fx1zjkfmad03r3qrzpk44rjfva8yllva54jipynh72775kkgr4h")))

(define-public crate-crc32-v2-0.0.1 (crate (name "crc32-v2") (vers "0.0.1") (hash "0rgcln257w67sr3a3s1f1s7sfnzbbx6xfqbhdpz1lk2b4mcz2ivd")))

(define-public crate-crc32-v2-0.0.2 (crate (name "crc32-v2") (vers "0.0.2") (hash "1va0yfxkhqi09sxwdhd1h7yh61ag9qh2xmz0brf2nba5z3gng5pa")))

(define-public crate-crc32-v2-0.0.3 (crate (name "crc32-v2") (vers "0.0.3") (hash "07c0lagi12drbxdrp0scb3n0wvmqa5q9xlmq0qgif5nfz3mh0viv")))

(define-public crate-crc32-v2-0.0.4 (crate (name "crc32-v2") (vers "0.0.4") (hash "0lfs9ic0z8bkhmrsg990qaxpk0i0964hs1za7dn6k41lrk76ym3z")))

(define-public crate-crc32_digest-0.8 (crate (name "crc32_digest") (vers "0.8.0") (deps (list (crate-dep (name "crc32fast") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)))) (hash "1fjygz5bwf9bjqqxv408xcrkzak24jbcsn5c7sbz34cma0nx4vpr")))

(define-public crate-crc32_digest-0.8 (crate (name "crc32_digest") (vers "0.8.1") (deps (list (crate-dep (name "crc32fast") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)))) (hash "10qrnj4r64wrdllqbpvvggxh5klwivqj5vhsd70sc15s2syfzma5")))

(define-public crate-crc32_light-0.1 (crate (name "crc32_light") (vers "0.1.0") (hash "1jh60bgamp6nkjp0vqy01a822r2zr454b57byn93hlgy3qn1pwdp")))

(define-public crate-crc32_light-0.1 (crate (name "crc32_light") (vers "0.1.1") (hash "08q311qq1zd4mcr3p9viyyljdhdd3qrmahw98lwnyk80m4hk54yn")))

(define-public crate-crc32c-0.1 (crate (name "crc32c") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.53") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ba19jdvdljzpqrvjqwadv4x77dl6i5pzy2xqr5gy8yh87kkq0d2")))

(define-public crate-crc32c-0.1 (crate (name "crc32c") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3.53") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0i1z3rk4cb9275c5zdx9kbkqfz4irnk9brz46ja7s9jv3nr8xbw3")))

(define-public crate-crc32c-0.2 (crate (name "crc32c") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "stdsimd") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "07c6ca7hlq959zjr7cpn9k7mv0s1xgkzp2mip4rirq49ykrj8zq1")))

(define-public crate-crc32c-0.3 (crate (name "crc32c") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0x3yh3abp2mwvyn5gbm23by390f61airjrkrsaq4dik7p62h1aw9")))

(define-public crate-crc32c-0.3 (crate (name "crc32c") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1g1m1qfazn4qh3gsqsrksb1laz1rijx8yfiq3gpamqriab5sypc7")))

(define-public crate-crc32c-0.4 (crate (name "crc32c") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1ic4xmkjgscfp1jld1n3v0m5s7aysqidb0p8rv0qhaf14vpkgfkp")))

(define-public crate-crc32c-0.5 (crate (name "crc32c") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)))) (hash "1ajig62ibbkr8ypb7500iwvkb3g0329sn6677rsms1ap3ps9lhgn")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0j4l9ghdhfk78yap0ssz5s9bawglca3wv45zz8mj30ba7s9xy311")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "1d948nrdp4aj7nldd4hqv5xgidw9cw43c25xi2lwnjsqi69rqszf")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "0awgj8p771wkr4nnnlkd196f91kbgv9sd5i5zpal8bmvr2055xwq")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "0gingi2i3r057y3pfpq62vaddbiglw88l9pv8lw7m4p98bds5zix")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "0x2af5jknzh4inj0i6shwpjhm9s9r1c8l79bbhywblmlwmh8vx6q")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.5") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "1qhcylgz7y7ifw0wa92yfmnz1w0hr1aaxcs4vq4ad7wvmac4a9c9")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.6") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "0zawyjr3pmj28p2cpxpxhd7jfw2v34k5wb5bc080gsa9dsxkhsvi")))

(define-public crate-crc32c-0.6 (crate (name "crc32c") (vers "0.6.7") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("alloc" "getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "0iv9kd71hjw7s3mlr1lhxn2ww91l4ikc0579gjy4jpak7vwvj9q2")))

(define-public crate-crc32c-hw-0.0.0 (crate (name "crc32c-hw") (vers "0.0.0") (hash "067cqjlj3vz9k2virprs83k65dpgkc7fdhrww0p1s92ag9wpnwds")))

(define-public crate-crc32c-hw-0.1 (crate (name "crc32c-hw") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "1rjc1wm0jaijz4p8gay075h9s9mv8jbxsfjndlzvyd9kgh3c35mh")))

(define-public crate-crc32c-hw-0.1 (crate (name "crc32c-hw") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "1mf9vh0fbkzjrjriwb87rgzf81rjd3jafhvav4615iic94ajc3lm") (features (quote (("no-stdlib"))))))

(define-public crate-crc32c-hw-0.1 (crate (name "crc32c-hw") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)) (crate-dep (name "stdsimd") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "029bhv64hj5q4iv3axmxs3v8clf0y7jzaikk1bpv85b3v5ih6p4v") (features (quote (("no-stdlib"))))))

(define-public crate-crc32c-hw-0.1 (crate (name "crc32c-hw") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "1y4p9l7lv52m1d39i99dsj12x1lk5mah1fv1kxd3k90v1b5p4hwr") (features (quote (("no-stdlib"))))))

(define-public crate-crc32c-sse42-0.0.0 (crate (name "crc32c-sse42") (vers "0.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i8zcac9zl0mh7iy8prlxgazzc8vb5nmlh9y74ddg847kypm217y") (features (quote (("sse42") ("default" "sse42"))))))

(define-public crate-crc32csum-0.1 (crate (name "crc32csum") (vers "0.1.0") (deps (list (crate-dep (name "crc32c") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)))) (hash "055dsixqhjggjnrh34nvjpqbza1mws791limjxz1rsn9rn5wwfdx")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0ygi0x9sjyr8wzzz7zn6bpjbfzsqqw5vd1xqy4px3xscc9y4y5n2") (yanked #t)))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "09dj9q0plfxaafsjmqciqn0j2baw8x8gg1mgl4ycpd0fk918krnr")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.0.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "19bch6ih96jxh520qfaww6mj2dm1yb5nksz0dh9558iihsq3mg5r")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.0.3") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "1x34qcsfgrm90rpw901aglvz68f9jsp0g1p29xh34kn1hd45wyd8")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.0.4") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "1afz1mxgmg7mhvqs24rlc84dxl8qpjp5vq16cfn0297hm6qqrld6")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "196q5w7hb0ak0cs900bgcv9slxaf2a8nvqjxf1a60k310mqqaq5x")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1svk1y49xk0878mhq7fp6v2qfpgf8xh8f3zxdajczg58kxaqbrp0")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "14piiddv0y0r8az5myd5x2lfs9aw4wsqy52zxcxg6plpqr0547g9")))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1c9dhkvf3brrzzplcijaywxi2w8wv5578i0ryhcm7x8dmzi5s4ms") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.2.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "06ivjlkzcxxxk7nyshc44aql4zjpmvirq46vmzrakdjax3n6y5c1") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.2.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "17riq124dh6rfanysm68vmgp9sdll4sbd326qiyr3508b3lb299q") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0di8ip18srxbva7dvz8y9rqmmc43s1lc3nasl4fgr17az86jk33k") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1qv0krk4ggxzz68x199xm9wg3bw4dgiff8971dznz1r91qqrq852") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.3.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "03c8f29yx293yf43xar946xbls1g60c207m9drf8ilqhr25vsh5m") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1ahy259ypc955l5ak24hdlgllb6vm6y2pvwr6qrlyisbg255m1dk") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.4.1") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1bgb0bj6mhqxs8v7ahmi6xk97f4yv5i3jg1cbmicg19ijvbgissq") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fast-1 (crate (name "crc32fast") (vers "1.4.2") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1czp7vif73b8xslr3c9yxysmh9ws2r8824qda7j47ffs9pcnjxx9") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-crc32fix-1 (crate (name "crc32fix") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "03m9klb9pmrsz50bz060kxnyywxgn0hd3dxxfmf7y7cmvnxxay9d")))

(define-public crate-crc32mnemo-0.1 (crate (name "crc32mnemo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "mnemonic") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0yk94jnl0hfkv06wn4ffk4d91093ma4d9yv0vis38xcdiw426qcq") (rust-version "1.59.0")))

(define-public crate-crc32mnemo-0.2 (crate (name "crc32mnemo") (vers "0.2.0") (deps (list (crate-dep (name "bech32") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "mnemonic") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0v2dng6jvdcgm0c9z21rpvf7ihf1m3kcx95090jzd02hb2jrdwva") (rust-version "1.59.0")))

(define-public crate-crc32mnemo-0.3 (crate (name "crc32mnemo") (vers "0.3.0") (deps (list (crate-dep (name "amplify") (req "^4.0.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "bech32") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "mnemonic") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0y1g4iv88r43zz07ay8sqjah598lvdy9ll8wczi6wcy58ldjgnd0") (rust-version "1.59.0")))

(define-public crate-crc32sum-0.1 (crate (name "crc32sum") (vers "0.1.0") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0xp5ybxmc2msjdyasz8rlq11y4r4y6hn5fsvpgpnya152v90azaz")))

