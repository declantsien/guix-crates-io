(define-module (crates-io cr um) #:use-module (crates-io))

(define-public crate-crumb-0.1 (crate (name "crumb") (vers "0.1.0") (hash "1861vgp5xbh93ah2z8n8qmp9yq12vbpsnqmqn8wh6sipfydhg3zc")))

(define-public crate-crumb-0.2 (crate (name "crumb") (vers "0.2.0") (hash "162wc3bdnflkhp4v4ysy4mnis3a698x1zxgimw3kyy85c2dab6k6")))

(define-public crate-crumb-0.2 (crate (name "crumb") (vers "0.2.1") (hash "040vvkmf3rx052mh5yq9wbwvjpzygyhjsx177j5pgysp3dbyg61h")))

(define-public crate-crumble-0.9 (crate (name "crumble") (vers "0.9.0") (deps (list (crate-dep (name "base64") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "14j09ynjal43ny08x936wa774i5qzyxjaibbklsa7pq5gky6jkzy")))

(define-public crate-crumble-0.10 (crate (name "crumble") (vers "0.10.0") (deps (list (crate-dep (name "base64") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0314py4xag0cikvv005n96xjmi8yk9rkzll7qjlsr08ppbqka2a8")))

(define-public crate-crumble-0.10 (crate (name "crumble") (vers "0.10.1") (deps (list (crate-dep (name "base64") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "05mf4ndxnll4qfysrrqqarjb6qrlyi6kwax73c9n9p7z58wf1dqy")))

(define-public crate-crumble-0.10 (crate (name "crumble") (vers "0.10.2") (deps (list (crate-dep (name "base64") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0z6rn1g7rsqr47k7rfmqpgzw077nm74c2dk8q5bby6mxdiv6sij2")))

(define-public crate-crumble_ci-0.1 (crate (name "crumble_ci") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req ">=2.0.0, <3.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)))) (hash "04yy9ya0vvqan0rw7xba0f37253prr3niabc1w85x3ppd8ldg6p3")))

(define-public crate-crumbles-0.1 (crate (name "crumbles") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rangemap") (req "^1") (default-features #t) (kind 0)))) (hash "0xbl5nsqvcicymnz2d72crb4n6w45lvkz21k6l4kmcj1p2zzm7a7")))

(define-public crate-crumbles-0.1 (crate (name "crumbles") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rangemap") (req "^1") (default-features #t) (kind 0)))) (hash "1aya741dwnnzwf5prlqjg84mygnfgqkxmngjy8yv83p1c7bs7gcz")))

(define-public crate-crumbles-0.1 (crate (name "crumbles") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rangemap") (req "^1") (default-features #t) (kind 0)))) (hash "01p7ba02ayknnnp6jw15zhljv5djvp485cxifb1arfgmbv2xrxb3")))

(define-public crate-crumbles-0.1 (crate (name "crumbles") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rangemap") (req "^1") (default-features #t) (kind 0)))) (hash "1lmmnb7lrbk8rp926wp97jz8rk2v9zpx6bi8ha2m1ajx5c6ym7xy")))

(define-public crate-crumbles-0.2 (crate (name "crumbles") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rangemap") (req "^1") (default-features #t) (kind 0)))) (hash "13bjr1g4pvg91da48zf47klg1vszbqy0axkqj00x0qmv2v6l1svr")))

(define-public crate-crumbles-0.3 (crate (name "crumbles") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rangemap") (req "^1") (default-features #t) (kind 0)))) (hash "0dln0f4smlnmzdnm528gkmf3rdqx19ayx3s6nb5qpy5psrh5zrx8")))

(define-public crate-crumsort-0.1 (crate (name "crumsort") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "12jb0q1am8p1sl1x71bgzq4i5yp5jary1412cxh24nrb427kayy6")))

