(define-module (crates-io cr c8) #:use-module (crates-io))

(define-public crate-crc8-0.1 (crate (name "crc8") (vers "0.1.0") (hash "1w96143w80dv7fy9p4fvsx61srivfkkdbx2rln6py01h05d44bzf") (yanked #t)))

(define-public crate-crc8-0.1 (crate (name "crc8") (vers "0.1.1") (hash "17jmjwg36saj95gq0bwqariy6b9536hzihhkwkbmyf8ghds227dk")))

(define-public crate-crc8-rs-1 (crate (name "crc8-rs") (vers "1.0.0") (hash "1ajm167764xynws3l5xnz2xd1r6flsijsi6qfwkbhd1jm55bw42x")))

(define-public crate-crc8-rs-1 (crate (name "crc8-rs") (vers "1.0.1") (hash "184jrwxscgk5w87sdh6k8wh32yg1r7cbvkvfx9clc1dd28wj3n7m")))

(define-public crate-crc8-rs-1 (crate (name "crc8-rs") (vers "1.1.0") (hash "04p844sa3pza8xpa3c9vqxrq5ga97ry8caa9zj5i15wsr0aa5sk3")))

(define-public crate-crc8-rs-1 (crate (name "crc8-rs") (vers "1.1.1") (hash "0saz44aiyan3xqm1hbq4vmkzvm1mhf3jidlpslc52a0gw6j0n1l9")))

