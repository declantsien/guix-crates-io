(define-module (crates-io cr -m) #:use-module (crates-io))

(define-public crate-cr-midi-0.0.0 (crate (name "cr-midi") (vers "0.0.0") (hash "1638h52d5skbb8nhwm5hncg1n6h4khz2wx0vqlcsq8gvyi6r6kda")))

(define-public crate-cr-midi-cli-0.0.0 (crate (name "cr-midi-cli") (vers "0.0.0") (deps (list (crate-dep (name "cr-midi") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0fs0a7r2z9g7asgbs1mklvgkx6hr8zw23iw035rwxr7r74yhsvh4")))

