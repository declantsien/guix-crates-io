(define-module (crates-io cr fs) #:use-module (crates-io))

(define-public crate-crfs-0.1 (crate (name "crfs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cqdb") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.84") (default-features #t) (kind 0)))) (hash "1gnvg3gfaai6kmlcm0ia8iqw6v76n81fa6fqdhdisr7jjkwf8sg0")))

(define-public crate-crfs-0.1 (crate (name "crfs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cqdb") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crfsuite") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.84") (default-features #t) (kind 0)))) (hash "178vyqqzqsvff09n82l71hy70k4gmaccfzawlk81zj0csszsjzz8")))

(define-public crate-crfs-0.1 (crate (name "crfs") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "cqdb") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crfsuite") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "0hqj8gjlg48374vbjfpfic12f5vvgf4kd4apn41vsc9adc5x76p7")))

(define-public crate-crfs-0.1 (crate (name "crfs") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "cqdb") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "crfsuite") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "0qr1pbpw6c6pkpv68pxq8n5vssjq3n2s6wdw7sf063mqxp2n183i")))

(define-public crate-crfs-0.2 (crate (name "crfs") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "cqdb") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "crfsuite") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "199z1k3k8br9c5axnzw6bpxz1j71njn3j509vhgkqrc3jbanq8w3")))

(define-public crate-crfsuite-0.1 (crate (name "crfsuite") (vers "0.1.0") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.35") (default-features #t) (kind 0)))) (hash "03kcqlz3x4lcs3y5ljv41zy75xxifqq9q171asbx9az4hg9in5cj")))

(define-public crate-crfsuite-0.1 (crate (name "crfsuite") (vers "0.1.1") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p5nr0mxdnnq9xx4p9wky90vwff8s57h2dvgd3vjppvgsfrq1r9q")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.0") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sah136w3zh3ir9l2n093mfw5jnr6wll2g1g1a656sy0w0ccvx45")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.1") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0c1awn2mgag95qimybwk438f5fwbkl6bw2mv12ky8izbxwi4ifag")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.2") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11ihd9g1pcx1kycbqdmqxcccghxb1zf4srszq9qp8hyb6yx14y05")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.3") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wawv2y07a42mk37mvpy43q698l8s32g4v5fkkgzpqjqvby10cca")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.4") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rcqvr4c6w4sp22vyxi40s57svp3f1ggz3qv059krbc5zfw1r6cl")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.6") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q38z2rn49y1gvvs8r8m5nvmhig1flbn4hnk4h08drb8sdfv5h6l")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.7") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "123j0pvii1yahx642km2byan5s7ng4p89ikpj6dli0piglslzq7q")))

(define-public crate-crfsuite-0.2 (crate (name "crfsuite") (vers "0.2.8") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "07iadmk2d7lsgdj5lrhz62hzfy6sfmp3n9wl7hl6nwms6cr7jdgf")))

(define-public crate-crfsuite-0.3 (crate (name "crfsuite") (vers "0.3.0") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00i5kwcc3kfcsawzpbw1ybv1yyk7dakbb89gszrl5ldzrghis5ch")))

(define-public crate-crfsuite-0.3 (crate (name "crfsuite") (vers "0.3.1") (deps (list (crate-dep (name "crfsuite-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qhj9h9yfmm8g9c5hsknas7z9h456m86jqpkc7l05k2izcrgv9gi")))

(define-public crate-crfsuite-sys-0.1 (crate (name "crfsuite-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.29") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.35") (default-features #t) (kind 0)))) (hash "0h9g9w9nidln259r415iqc110zbs9q9lzni2b6hjqksjy3cr2ljr")))

(define-public crate-crfsuite-sys-0.1 (crate (name "crfsuite-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.29") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.35") (default-features #t) (kind 0)))) (hash "1wl7z1gl8w3kra20bfa689q7fjqdkzp4iiyf9qgn5p4f7vgpiwyq")))

(define-public crate-crfsuite-sys-0.1 (crate (name "crfsuite-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.29") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.35") (default-features #t) (kind 0)))) (hash "0i866l89iml73w54wv5a8dg9qfky78ci4l1gmr1ygxzqq8w5990f")))

(define-public crate-crfsuite-sys-0.1 (crate (name "crfsuite-sys") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.29") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.35") (default-features #t) (kind 0)))) (hash "054639343x90ani1cpzydizpkzajgi110liaixkzggizms8hc9jn")))

(define-public crate-crfsuite-sys-0.1 (crate (name "crfsuite-sys") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0raws1bdkq6zq48q9hc2yh1rwa88rkgh0vbfln72y3zkjv6qnjki")))

(define-public crate-crfsuite-sys-0.1 (crate (name "crfsuite-sys") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03gxxgar9b2hsxss11wk4qk2jixhsavc3z5vmwzira38f3fwjpag") (links "crfsuite")))

(define-public crate-crfsuite-sys-0.2 (crate (name "crfsuite-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a3zb7qmqzrmjh658kmzckbc2w1fpc05mw19wk86j5w4jsdhz4ic") (links "crfsuite")))

(define-public crate-crfsuite-sys-0.3 (crate (name "crfsuite-sys") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "liblbfgs-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bn55aq8ll5h0qdbfnr3q42730yzwl403bjv47590pc9lwili3wd") (links "crfsuite")))

(define-public crate-crfsuite-sys-0.3 (crate (name "crfsuite-sys") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "cqdb-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "liblbfgs-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15h9kihv08pcs5d0490xv01hwbihnyaswy6fh2606mcyc81sk9fy") (links "crfsuite")))

