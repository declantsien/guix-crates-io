(define-module (crates-io cr cx) #:use-module (crates-io))

(define-public crate-crcxx-0.2 (crate (name "crcxx") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0ngad2dfh893ynq65pbkb9yyzfgimfrcrj3ly1bp541ij63vymwb") (features (quote (("slice-by-8") ("slice-by-4") ("slice-by-32") ("slice-by-16") ("default" "slice-by-16")))) (yanked #t)))

(define-public crate-crcxx-0.2 (crate (name "crcxx") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "07amxzif77s9d96hi14m2vg04315407z7m71jz6x6ra2jhh6zni8") (features (quote (("slice-by-8") ("slice-by-4") ("slice-by-32") ("slice-by-16") ("default" "slice-by-16")))) (yanked #t)))

(define-public crate-crcxx-0.3 (crate (name "crcxx") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1x74s5dw4bd66srbq7s64kh33m4gl045fack711gig1pyvmv2431") (rust-version "1.59")))

(define-public crate-crcxx-0.3 (crate (name "crcxx") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0pkdh02sy7sjaama5677kzhhq7hvzan7dshhim2fnm3f9w3zw4jv") (rust-version "1.59")))

