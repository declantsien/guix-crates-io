(define-module (crates-io cr il) #:use-module (crates-io))

(define-public crate-crilog-0.1 (crate (name "crilog") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0ghj5jrk17v3cr0h2r42f1ij1w6kzpwaakzk00bwnbkhfbwzdylk")))

