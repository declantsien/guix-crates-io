(define-module (crates-io cr sf) #:use-module (crates-io))

(define-public crate-crsf-0.1 (crate (name "crsf") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 2)))) (hash "1nqvcad21k50sg9sj6akp9yqgdwxl03fvmh4a55arvz8h941mlrd")))

(define-public crate-crsf-0.2 (crate (name "crsf") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 2)))) (hash "18hsgs1qwz3na9gdhf20vlmvrll9fr4gg3ah282rpg3jbmbjj7yh")))

(define-public crate-crsf-1 (crate (name "crsf") (vers "1.0.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 2)))) (hash "0w9nvc39l4q7dkcm8hvmgdkbkbca9p1pr4plsgdb6ax4mah1qmi5") (yanked #t)))

(define-public crate-crsf-1 (crate (name "crsf") (vers "1.0.1") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 2)))) (hash "0k3ja140s4pagax2zl4xpcf67fi4233n8b5v24pih3jnhk6z4qrp")))

(define-public crate-crsf-2 (crate (name "crsf") (vers "2.0.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.8.0") (kind 0)))) (hash "03k9mvcdgv69brwvyq9wl26zx84awgiiw42bzyrwgiwalhmlrhlk") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

(define-public crate-crsf-2 (crate (name "crsf") (vers "2.0.1") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.8.0") (kind 0)))) (hash "006fnz1n489wzzbbfb2sgj0l5pgd4gqwrq145269cwq58dp0q5if") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

