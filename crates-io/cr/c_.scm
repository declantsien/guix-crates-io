(define-module (crates-io cr c_) #:use-module (crates-io))

(define-public crate-crc_all-0.1 (crate (name "crc_all") (vers "0.1.0") (hash "11lz5qn9hcaswdx7qn6nhfz0ic7zfsas7vv7dka3w9gzmg4zn0s5")))

(define-public crate-crc_all-0.2 (crate (name "crc_all") (vers "0.2.0") (hash "1m4724kjkw6q5cncfldclvn2rs7rdgkqazrvcsf95nya4nphx1x8")))

(define-public crate-crc_all-0.2 (crate (name "crc_all") (vers "0.2.1") (deps (list (crate-dep (name "crc_all_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r3bxlgpi4n88462lwhvy3ak96rbcqgs3p3bajizdn73y4s5ffz4") (yanked #t)))

(define-public crate-crc_all-0.2 (crate (name "crc_all") (vers "0.2.2") (hash "06vgiai5kkwbdh6053kxjzlzg111plbrmbmk9mqigygfxcbilv64")))

(define-public crate-crc_all_macros-0.1 (crate (name "crc_all_macros") (vers "0.1.0") (hash "0j6rl9parf1bsxvpnyn571mlbr9gwnr93my474z4yyarmc5k1hg8") (yanked #t)))

