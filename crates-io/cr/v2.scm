(define-module (crates-io cr v2) #:use-module (crates-io))

(define-public crate-crv20-0.1 (crate (name "crv20") (vers "0.1.0") (deps (list (crate-dep (name "casper-contract") (req "^1.4.4") (default-features #t) (kind 0)) (crate-dep (name "casper-types") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "casperlabs-contract-utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "curve-casper-erc20") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kzal8hjpfxkp4w4jx0p5bm225f2vac1xj4p3ql7i6xjmm6nqjg8")))

