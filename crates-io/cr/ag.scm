(define-module (crates-io cr ag) #:use-module (crates-io))

(define-public crate-crag-0.1 (crate (name "crag") (vers "0.1.0") (hash "1kji5bvmaibv1j3knd21xw0d79n8vrhq2bsnk0l9h2sskz02p3l6") (yanked #t)))

(define-public crate-crag-0.2 (crate (name "crag") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "unicode" "wrap_help" "env"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (default-features #t) (kind 0)))) (hash "1p2r0ivhajhdcjql8m3j649xr9xqv0dm41wf2c20hcf4dbx0icl3")))

(define-public crate-crag-0.3 (crate (name "crag") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "unicode" "wrap_help" "env"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "0qsxn6rddxcslz6yc0dg83z01s6j000acpnj7f7m2dgixayj8a47")))

(define-public crate-crag-0.4 (crate (name "crag") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "unicode" "wrap_help" "env"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "00v1kfhhs20i3a39h1z65l51w2y0hv6chf7w949xiz4cf6ajby9f") (features (quote (("cli" "clap" "colored"))))))

(define-public crate-crag-0.4 (crate (name "crag") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "unicode" "wrap_help" "env"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "1il1fkqp698hjqbj9iwfcqrjd93kdqkfgbcjn9jxg8gzkl4p76q9") (features (quote (("cli" "clap" "colored"))))))

