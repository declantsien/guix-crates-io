(define-module (crates-io cr c-) #:use-module (crates-io))

(define-public crate-crc-0x8810-0.1 (crate (name "crc-0x8810") (vers "0.1.0") (deps (list (crate-dep (name "assert_hex") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "crc") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "crc-any") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 2)))) (hash "0nfqg0ari44kkicf5fgiarphcnwfy9kkqkizf5zv7plbdw33y3n4")))

(define-public crate-crc-32c-0.1 (crate (name "crc-32c") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^3.0.1") (default-features #t) (kind 2)) (crate-dep (name "crc32c") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0b1d9az1j7gsmixk51kaabs4ls4a61fbl15fil6cycwpy5fzbcck")))

(define-public crate-crc-any-1 (crate (name "crc-any") (vers "1.0.0") (hash "0nl320j2ayja25689a9w8m29x57masa3mailh6cw3kfj5kcghbg0") (features (quote (("develop"))))))

(define-public crate-crc-any-1 (crate (name "crc-any") (vers "1.1.0") (hash "1hwr5r0f9j6mcif82577gyh54gsd7r09ipn5x6sff9ngbhvy0cvr") (features (quote (("develop"))))))

(define-public crate-crc-any-1 (crate (name "crc-any") (vers "1.1.1") (hash "01rhgd36ff3n8k7iwfjg9g3b0jgkz23c9lg3rcllqkq8n8gdc9mk") (features (quote (("develop"))))))

(define-public crate-crc-any-1 (crate (name "crc-any") (vers "1.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "05g7jyn4lqs90l61f3nsrhaqm4xbb4b9pabqrby19y2agh3xcw09") (features (quote (("develop"))))))

(define-public crate-crc-any-1 (crate (name "crc-any") (vers "1.1.3") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)))) (hash "1906nlcsmr4nzg93af8pk3dnnw86k47631pdfib12807524hvzqz") (features (quote (("develop"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0dnc1qapr64g6ig5hwqbbdq9gd5szzdnf9kw62sa3gzx8xfgzlqn") (features (quote (("no_std") ("development"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "19jcgccj8k8zn4dvi15nvfl234849gx1dd0pni0z115q0pb5s3y8") (features (quote (("no_std") ("development"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y2ajjv6k3j5yz1n9n48pg3wc2vx4idhw1drkq4b3b9hx157vx6v") (features (quote (("development"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cpgdrp39clizvq2zb7242dg2pfjj89dnagbm0m91nb2hv6g87d9") (features (quote (("development"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.1.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (default-features #t) (kind 0)))) (hash "03dp4ini5aaq87bk4gfq542jsd0hwqh7rfh917dfqqrcplprm61k") (features (quote (("development"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0fi6vwfbiy1x8wgiv32r7v3py61i0z4xslqrl427lsy4v8f0lbm3") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.2.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "008pkmlld3vlh3vcisxir3bkjw7dsqr3c1dvksinbqpqhydvq0g5") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.2.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0dqdy6v7n120w7azdc2v42ns6x8a7bagzhbdn9z05wc2xncgrr6z") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.2.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0kc3ww4cd5cjs2xr836djmd517xvdhygw7pb3gypdas2q29r1jjy") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "034kx5ss5g5ablrv85yxprmswndy83kxlg42448zvxg0nii79kwa") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1w8j5vp546v7djlrypm1ijrm191vvn1c20wpikfy8zhzxh80z2ml") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1r9869f9qvp3vqwvhlqbcsmcba2ai98mh9w8qbba0myc4k99rxcq") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0rczsw72pn10cjn9f9wrirv643mr2lzawasnzvhyd91vfklmbd0g") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.4") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1izd56xcxmdmvpzx779b1j46lq9s1vwcqqp3pj7mmp4n3j0m2lqr") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.5") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "01wh7pb0svpi8vvqalk5rimxx6ba9xvhpvv93kaz8sclvzplny63") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.6") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "055qmzpwm4a47d122sqgxpczv1pknh7plx4xqwpwjgd54ii7k620") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.7") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0qhd7i09xa30q68mnjiv1gmz2k3qpbb86kca4yq8ddlb428l5xrn") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.8") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1wzc346jf1s87lij3fpkizbxj64s3bak5qhh2l95vg223n3drsc3") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.9") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1zr8fph3vls47j5455sjsqkb4jfadnlmaax8l92bwcw6100vx60d") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.11") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1q9lzw1pi8ngphvqx0nf40cy08y5fvn0l6szkxrb0i64qn8hx5dr") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.10") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "04vzdp9crm2al86i79dsnmdnkbyq4h0h72f4d94xdn7sbrw57fak") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.3.12") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0fq85y5akcadahnj5nqbs47qhgp5cpfn2z19zc3gp4wpxr3989kr") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1mjn9al1gf01hn7yr1vrbd0fmdmg73hvbzwlcy6f8vxg9ppnihj4") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.4.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "08d9g8mh1ihnhf9i8f5d320nkwr7l1wrip7vk8bcxpjq99l7acq7") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.4.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1yvq3jfglgjy22hzr72irbz7wn43rk989z97lyh1pradqcr9zz53") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.4.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1pw89ncs7gbqvxnkp5spks61qn6bcg13vwab1zml6dpnhyv4cikp") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper"))))))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.4.4") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1l5n1szzwx05i5j659gg3x7zs6b7jjg99pvvk84vcvqzi0gmw6n0") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper")))) (rust-version "1.60")))

(define-public crate-crc-any-2 (crate (name "crc-any") (vers "2.5.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "debug-helper") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0wzs26q5cf29fhfnrkrjsr8dpai0rlm4im8b53by8rbrbzzwjbm6") (features (quote (("std") ("development" "std") ("default" "alloc") ("alloc" "debug-helper")))) (rust-version "1.60")))

(define-public crate-crc-catalog-0.1 (crate (name "crc-catalog") (vers "0.1.0") (hash "0l81n7l6ln66fqw8kwwvcdx1xy0fshyyp0yznzfskci9vpx6px22")))

(define-public crate-crc-catalog-1 (crate (name "crc-catalog") (vers "1.0.0") (hash "0a6kgpym6bqdccn7jhzvgbvzplapg18fzsa23ly61pjm9b0772y6")))

(define-public crate-crc-catalog-1 (crate (name "crc-catalog") (vers "1.1.0") (hash "007kxrpjwr9xjb4310smbddanvl3cqbzd2wqdyfdyq2cd5ablv4b")))

(define-public crate-crc-catalog-1 (crate (name "crc-catalog") (vers "1.1.1") (hash "00qlxgzg15fnyx6nwviibz94rjw803l2avi2k3shjfx0dnsyvbnc")))

(define-public crate-crc-catalog-2 (crate (name "crc-catalog") (vers "2.0.0") (hash "0gks5h0pxc6ghviyhmq9cqh3f9j32xk2p4w1rrn0jbkx1mm4g4fh")))

(define-public crate-crc-catalog-2 (crate (name "crc-catalog") (vers "2.0.1") (hash "14gc4wdrf669m10rn0cvfvy8gd4wd116l67js48di5f06fqqaa79")))

(define-public crate-crc-catalog-2 (crate (name "crc-catalog") (vers "2.1.0") (hash "1zzkk9fjm262z5hrg4xsx77grvmmld6vq2z86s77grhaj396a09d")))

(define-public crate-crc-catalog-2 (crate (name "crc-catalog") (vers "2.2.0") (hash "115l7pzskv5xzp9i7146rp1qrbfdi7gikig1p80p6zpham7fib4w")))

(define-public crate-crc-catalog-2 (crate (name "crc-catalog") (vers "2.3.0") (hash "1y9jax8hjq8nmafqrvw4zqs7hvps2800kwrpdn4qrga42knzjfa9") (yanked #t)))

(define-public crate-crc-catalog-2 (crate (name "crc-catalog") (vers "2.4.0") (hash "1xg7sz82w3nxp1jfn425fvn1clvbzb3zgblmxsyqpys0dckp9lqr")))

(define-public crate-crc-core-0.1 (crate (name "crc-core") (vers "0.1.0") (hash "177nk83frd779l72dqcvjs8rmds19f3gaw1l49w56h4xfxq12g80")))

(define-public crate-crc-core-0.1 (crate (name "crc-core") (vers "0.1.1") (hash "1p5b4kgbmv21qdhn1002vaxsscv383hyafdlmml2x1fkq0npyk0f") (features (quote (("std") ("default" "std"))))))

(define-public crate-crc-core-0.1 (crate (name "crc-core") (vers "0.1.2") (hash "1y7nlhffqxzgzapffnqvx77ra2cri13h1c9psl9ny6czd32j17nk") (features (quote (("std") ("default" "std"))))))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.0") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "04whhq6pz1dzv5y35xkwqmipzmgkpljrpfjzyjy46776arrk2cij")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.1") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0kncvwh0r4vgsp1g1y35i678h8zry8bxsn2k0cc13ck0w4x9kd9l")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.2") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1bbjhxkwca22n6jag4jh8d250cj0plj0iyiprgyjzy7pbilpxy3l")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.3") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1wmvps1r4p5csw6vn3qqc8a22rhpw1ir142kkj20ixf8pnwn73nk")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.4") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0s9czipnvzmz0vnm5db71szza3ifji6ib1x10h1qfkjsllym7p7k")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.5") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1ir13ggdwr2a7pp9b83lrnzrqymdsxvhrcymz9imrd1zx231ris7")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.6") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1r79cf3wdw6k985j9ggykfqyvcrm6z176bnpvdgmq78yial7ycxn")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.7") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0m5gblmz89sc82130adzsci2wid89lxcpxa51gyzv7yfnx24k1rf")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.8") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1chxardplc32adq4vyx72k8gnzbcymyyg4p5zfhjf56kjpg4q6xg")))

(define-public crate-crc-frame-1 (crate (name "crc-frame") (vers "1.0.9") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fault-injection") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "1x0yg4xwzln9zsij0x5817wqp91257hwhayvy4q5j6h87dpgc9bz")))

