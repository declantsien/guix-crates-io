(define-module (crates-io cr _p) #:use-module (crates-io))

(define-public crate-cr_program_settings-0.1 (crate (name "cr_program_settings") (vers "0.1.2") (deps (list (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "0qlvxbzndkjd5knmvab28gr6z2g04a28paf09nj6a87w786lbag8")))

