(define-module (crates-io cr ur) #:use-module (crates-io))

(define-public crate-crurl-0.1 (crate (name "crurl") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.21") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1kpsq201qjmha63vwq0fq7hj4p5d2avshx45ysyfww0lmylcvzpw") (yanked #t)))

(define-public crate-crurl-0.2 (crate (name "crurl") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08awqxy7mrf10diaf1z3jyrd05xnavpg4if0q0pv25y906d4ybjz") (yanked #t)))

