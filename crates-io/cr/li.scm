(define-module (crates-io cr li) #:use-module (crates-io))

(define-public crate-crlibm-0.1 (crate (name "crlibm") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "ctor") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0nwjyz186jyp4w3rcaq1crz6x0vp5cbzqby9dcixwar1156dngbx")))

(define-public crate-crlibm-0.1 (crate (name "crlibm") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "ctor") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1lx38y40i37ri0g9mdnq3bslii1iw04pxbymxwbdy3ybg7v7zy6c")))

(define-public crate-crlify-1 (crate (name "crlify") (vers "1.0.0") (hash "1ra6h95mw1cd745w6n2xjjmvw8jnhqnxa7900ijgpv3mzp1lbfyk")))

(define-public crate-crlify-1 (crate (name "crlify") (vers "1.0.1") (hash "0a8bnzsh7k6zwzxyk47phpsyn38ppxlvqijsmbv6fc6lr1kk9l4b")))

(define-public crate-crlify-1 (crate (name "crlify") (vers "1.0.2") (hash "15i6c3m16qrmjgb3x31yc8nzcv0pan4519r8307q3nr5csy4knb1")))

(define-public crate-crlify-1 (crate (name "crlify") (vers "1.0.3") (hash "1h05anipl41b7a2j71cp394hjkbdw9dx5qx56mqc35i6gc7wizyp") (rust-version "1.66")))

(define-public crate-crlify-1 (crate (name "crlify") (vers "1.0.4") (hash "1gyg1frk270r1ld8wa6kxx5avcv19w1nalh4wix3cl34m6fxj714") (rust-version "1.67")))

