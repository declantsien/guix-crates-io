(define-module (crates-io cr x2) #:use-module (crates-io))

(define-public crate-crx2rnx-1 (crate (name "crx2rnx") (vers "1.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "~2.27.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "rinex") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0pwgjwq6c7z970kwy540xckzy5f91l5x623q1wrx0nqfjcmdj6ig")))

(define-public crate-crx2rnx-1 (crate (name "crx2rnx") (vers "1.0.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "rinex") (req "^0.6.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "03csix23qw277w4b5y0gc6yk0n23mxdn8jhnycjdlb45763clkz1")))

(define-public crate-crx2rnx-2 (crate (name "crx2rnx") (vers "2.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "^0.10.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1hnr250q6kjvl5mylbbhbcha58mv5b2fpkg7h39rh2cfkxpzvvyw")))

(define-public crate-crx2rnx-2 (crate (name "crx2rnx") (vers "2.1.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "=0.15.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0lwp8vhq7vmq6hph3wf58yy9rks83hnlr1nzpkikm1j4l76j4klg")))

(define-public crate-crx2rnx-2 (crate (name "crx2rnx") (vers "2.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "=0.15.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0w5v4ml98d88bx1iiv9bfkw8i9cym3nbwaxn9hzgsvjpyy5wqkrl")))

(define-public crate-crx2rnx-2 (crate (name "crx2rnx") (vers "2.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "=0.15.2") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0y1smw1x47zy4njr4v8k61mmr13p069xr0b8i5vraz5i6bn4lxk5")))

(define-public crate-crx2rnx-2 (crate (name "crx2rnx") (vers "2.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "=0.15.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1xs2w8jylrmdzjpjcnb8grms2q4sbz8wfid11r5yyy7ppris25ii")))

(define-public crate-crx2rnx-2 (crate (name "crx2rnx") (vers "2.3.3") (deps (list (crate-dep (name "clap") (req "^4.4.13") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "=0.16.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0ficljcxdgzxn3iw7sjngml564h1xn5mi60spjcgypv9qq1hk79i")))

(define-public crate-crx2rnx-2 (crate (name "crx2rnx") (vers "2.3.4") (deps (list (crate-dep (name "clap") (req "^4.4.13") (features (quote ("derive" "color"))) (default-features #t) (kind 0)) (crate-dep (name "rinex") (req "=0.16.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0q7nyav00cr5jkj2nx9gnp6dgilrk42raahsr2w2vxkqajrh8mg6")))

