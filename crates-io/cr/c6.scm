(define-module (crates-io cr c6) #:use-module (crates-io))

(define-public crate-crc64-0.2 (crate (name "crc64") (vers "0.2.0") (hash "0jgn9awzx7df3pidpf2nkc30vn55yan8prvj559hnwlz4cqb3mmj")))

(define-public crate-crc64-0.2 (crate (name "crc64") (vers "0.2.1") (hash "0x2nb7v4jchhrbgq1r72p05pdqfxcx7agw1vs8nbaa2cq1mh7f1i")))

(define-public crate-crc64-0.2 (crate (name "crc64") (vers "0.2.2") (hash "008yx18v4fnv22x8m7p8sayyhpjqnc0a6sdmq3sdcswv9xrnsf0j")))

(define-public crate-crc64-1 (crate (name "crc64") (vers "1.0.0") (hash "0469vp0q9431pqx1236g60if5q3xyxpv4h14smkd45dfzsa6aqjm")))

(define-public crate-crc64-2 (crate (name "crc64") (vers "2.0.0") (hash "1wwqdss36dmhz4fd0wynlaig463l4dwvr21db1fvf6aypapy61r7")))

(define-public crate-crc64-rs-0.1 (crate (name "crc64-rs") (vers "0.1.0") (hash "129dfq0az0mgdzd8f7xkxw4dbvixpiax6x0k664il8ydx1nycvq7") (yanked #t)))

(define-public crate-crc64-rs-0.2 (crate (name "crc64-rs") (vers "0.2.0") (hash "1pv0a3w65zckv7ywm464wz87d3kdxklrbinwgc9axp722f50b4za") (yanked #t)))

(define-public crate-crc64fast-0.1 (crate (name "crc64fast") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0633av1fqz0mkc7j60praw0wi85ff3d1gzv8vi8xvjm7w06m30ha")))

(define-public crate-crc64fast-1 (crate (name "crc64fast") (vers "1.0.0") (deps (list (crate-dep (name "crc") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1gc2ibmx2rryk1c6hpa140di7n5065g1d0z5c6d72k4jaz2i0gdx") (features (quote (("pmull") ("fake-simd"))))))

(define-public crate-crc64fast-1 (crate (name "crc64fast") (vers "1.1.0") (deps (list (crate-dep (name "crc") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1qi9ah3j83r58l2pikmqxmyk4zjbsq9jg7h0y3y1wa90xbn95fr6") (features (quote (("pmull") ("fake-simd")))) (rust-version "1.70.0")))

