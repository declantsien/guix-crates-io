(define-module (crates-io cr -s) #:use-module (crates-io))

(define-public crate-cr-sys-0.1 (crate (name "cr-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "18sg7v56p54lz1gc9ciw545l17dfgkkxhkyzzazx3fnidbp4hkwp") (features (quote (("guest")))) (links "cr")))

(define-public crate-cr-sys-0.1 (crate (name "cr-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0ns3gymlabbj0hqjvbag6qbr1icp9r9fclld5fflg4z0988hadmd") (features (quote (("guest")))) (links "cr")))

