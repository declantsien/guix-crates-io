(define-module (crates-io cr im) #:use-module (crates-io))

(define-public crate-crimp-0.1 (crate (name "crimp") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ndi08qh6s87xbmvpsr614pr1k263plagh04pynmwyfidkpbwd7y") (features (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-crimp-0.2 (crate (name "crimp") (vers "0.2.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "075sh2na65l7xfaycwnafjr6bjffhwqjf890f80wcza03119dk7z") (features (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-crimp-0.2 (crate (name "crimp") (vers "0.2.2") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1527s14p0xwzsfgif40an5zjkgc72llamvdc7cp1m75d42izks5v") (features (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-crimp-4087 (crate (name "crimp") (vers "4087.0.0") (deps (list (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0f9bw42mvdidxqgbnanp5k6rqxfis22mk8pplvjviyfiyy1jrb8f") (features (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-crimson-0.1 (crate (name "crimson") (vers "0.1.0") (deps (list (crate-dep (name "crimson_utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1icwy5qp3ka5nmn4rmbjh92qja66fq1jzxw45bv6q5sgnjvw7z5l") (yanked #t)))

(define-public crate-crimson-0.2 (crate (name "crimson") (vers "0.2.0") (deps (list (crate-dep (name "crimson_utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02fy74yknxwz81y73aa8x5mrhnrf0lramjni63nws5hbg8m8n1dx") (yanked #t)))

(define-public crate-crimson-0.2 (crate (name "crimson") (vers "0.2.1") (deps (list (crate-dep (name "crimson_utils") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0lvlsnny5fxwq9zp7k4xqa8awilixkwmq8zlbrqpw15grpvzhf8z") (yanked #t)))

(define-public crate-crimson_utils-0.1 (crate (name "crimson_utils") (vers "0.1.0") (hash "1p6w98kkljj6dyigi0kl4xvh9yhq652205dzfy09yix05pzp5hgz") (yanked #t)))

(define-public crate-crimson_utils-0.2 (crate (name "crimson_utils") (vers "0.2.0") (hash "03jp13lrh3cr3id7fykp8q0qgbnh8p44jjv1mr6kkmi3r7fjzixa") (yanked #t)))

(define-public crate-crimthy-dragnit-schemes-0.1 (crate (name "crimthy-dragnit-schemes") (vers "0.1.0") (deps (list (crate-dep (name "dragnit") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0pnvldsc6z9li69nb4lk1vxkasz0667b8mdymildyb6f7innmcsc")))

