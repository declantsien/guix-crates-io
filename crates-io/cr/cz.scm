(define-module (crates-io cr cz) #:use-module (crates-io))

(define-public crate-crczoo-0.1 (crate (name "crczoo") (vers "0.1.0") (hash "14jx7j0ymhdch56z2cfwlnscrnx8s5p1ghmsrf0rmdqbs7xa2f7f")))

(define-public crate-crczoo-0.1 (crate (name "crczoo") (vers "0.1.1") (hash "0g3dvizxh0x86p9w5amgsk795zjlhszv7ljs8nhrsbdyjafm5hzx")))

(define-public crate-crczoo-0.1 (crate (name "crczoo") (vers "0.1.2") (hash "1kwga0skaihb1fjw8k7mbgwc5vnr56zsc1krkbfvbk2aym9417jw") (yanked #t)))

(define-public crate-crczoo-0.1 (crate (name "crczoo") (vers "0.1.3") (hash "0gwcp0zlslliqf51mqhcr2408g7wkdnfdwjyrpsq8fmjh910g233")))

