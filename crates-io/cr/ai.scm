(define-module (crates-io cr ai) #:use-module (crates-io))

(define-public crate-craigslist-scraper-0.1 (crate (name "craigslist-scraper") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "html5ever") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "slack") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "tendril") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0y61hjsf24fgvv0z8ag8mdsx9cf2bywm6l1vk7zqq5cll3vhgp5j")))

