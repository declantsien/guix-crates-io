(define-module (crates-io cr ae) #:use-module (crates-io))

(define-public crate-crae-0.1 (crate (name "crae") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "0qa8snl2vn595qp4phnriihqs79sfsix0cll8c0w8kjfprh79yrf")))

