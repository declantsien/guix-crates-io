(define-module (crates-io cr mp) #:use-module (crates-io))

(define-public crate-crmprs-0.1 (crate (name "crmprs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crc64") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0iqcyjgg770lhmx7281qbby2zgj25r1z2lpv28vndrkydpcvcbf1")))

