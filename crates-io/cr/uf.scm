(define-module (crates-io cr uf) #:use-module (crates-io))

(define-public crate-crufst-0.1 (crate (name "crufst") (vers "0.1.0") (hash "15200iqq2ymvg19fmwbr1i4al18k6s25i97fj82n8cpq6vl4r0wp")))

(define-public crate-crufst-0.0.0 (crate (name "crufst") (vers "0.0.0") (hash "02g12j2m7xhpdpr6rbcfv4vp5qxdzly58rljdw9sr77nicwr7s0c")))

