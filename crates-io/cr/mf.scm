(define-module (crates-io cr mf) #:use-module (crates-io))

(define-public crate-crmf-0.0.0 (crate (name "crmf") (vers "0.0.0") (hash "18rjgsp7l1ly9vv2j0ymddpg2r275cmridbv9zx9ddbd0bxsp54p")))

(define-public crate-crmf-0.2 (crate (name "crmf") (vers "0.2.0-pre.0") (deps (list (crate-dep (name "cms") (req "=0.2.0-pre") (default-features #t) (kind 0)) (crate-dep (name "const-oid") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "der") (req "^0.7") (features (quote ("alloc" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "spki") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "x509-cert") (req "=0.2.0-pre.0") (kind 0)))) (hash "05k19x5wzbkwgv1bq5rxcxh33ddh88sh85irdsb501jl44ccdgk0") (features (quote (("std" "der/std" "spki/std") ("pem" "alloc" "der/pem") ("alloc" "der/alloc")))) (rust-version "1.65")))

(define-public crate-crmf-0.2 (crate (name "crmf") (vers "0.2.0") (deps (list (crate-dep (name "cms") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "const-oid") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "der") (req "^0.7") (features (quote ("alloc" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "spki") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "x509-cert") (req "^0.2") (kind 0)))) (hash "14pdc9czxcjzwy7p5rkwh6n100f087n04wjv9gggb1svdnwj3zin") (features (quote (("std" "der/std" "spki/std") ("pem" "alloc" "der/pem") ("alloc" "der/alloc")))) (rust-version "1.65")))

