(define-module (crates-io cr #{4t}#) #:use-module (crates-io))

(define-public crate-cr4t3-3 (crate (name "cr4t3") (vers "3.0.0") (hash "1plvjpqfhb0n866lnn1ikgg0481k36c3ns9qnxdyibqn7rq0qg3k")))

(define-public crate-cr4t3-3 (crate (name "cr4t3") (vers "3.1.0") (hash "12wry3sz7bncmbr4br7jp7560xsdsqd81zw7vfynwf3izl6l1syz") (yanked #t)))

(define-public crate-cr4t3-3 (crate (name "cr4t3") (vers "3.1.1") (hash "0d13rgxiivlrjkk9rajk916qbxqjb95l5pg8r20ya2vygbk3w9r7")))

(define-public crate-cr4t3-3 (crate (name "cr4t3") (vers "3.1.2") (hash "11gmcbq07qyzg78hq0fj99v9z6js7s7n7jdnrpkrfva4dgyr3rck")))

(define-public crate-cr4t3-3 (crate (name "cr4t3") (vers "3.1.5") (hash "08y8irm4941rr59148mxidplz8nzifangdvql1k2fqxd9ssfxh0q")))

(define-public crate-cr4t3-4 (crate (name "cr4t3") (vers "4.0.0") (hash "06czcyzb0iz3qzplx5p8csa9i1n5w385ajrkqfic4s5padyzv5a4")))

(define-public crate-cr4t3-4 (crate (name "cr4t3") (vers "4.1.0") (hash "1hv39q9p519c7bkg560iq988mssfnlj9z73cpg08lr0949h8j37l")))

(define-public crate-cr4t3-4 (crate (name "cr4t3") (vers "4.1.5") (hash "1kd3hh8dk5pkxls55zmqifwz05pfy5rsgf6w71jh6lnlnbxn869g")))

(define-public crate-cr4t3_2-4 (crate (name "cr4t3_2") (vers "4.0.0") (deps (list (crate-dep (name "cr4t3") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0wr0pjkng80zk7q9nj4d4rq11gma8g6j2wlaxr2gliy8wna40gaq") (yanked #t)))

