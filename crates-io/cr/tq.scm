(define-module (crates-io cr tq) #:use-module (crates-io))

(define-public crate-crtq-0.0.1 (crate (name "crtq") (vers "0.0.1") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hazard") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "queuecheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1pkld226nkvcsl9r97ycn95hpjz6s9sb1jc77nfw2xvx5iljal24") (features (quote (("valgrind")))) (yanked #t)))

(define-public crate-crtq-0.1 (crate (name "crtq") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hazard") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "queuecheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1957cf379qpx7spna9hz8wckvvkd43n5if0q1jkzsc300ni1pjbs") (features (quote (("valgrind"))))))

(define-public crate-crtq-0.1 (crate (name "crtq") (vers "0.1.1") (deps (list (crate-dep (name "hazard") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "queuecheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "01zpydas8rjhjhc1rcvzv6p1n6pibkxg4v3m07jsskzcvirq78ix") (features (quote (("valgrind"))))))

