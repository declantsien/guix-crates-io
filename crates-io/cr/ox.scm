(define-module (crates-io cr ox) #:use-module (crates-io))

(define-public crate-crox-0.0.0 (crate (name "crox") (vers "0.0.0") (hash "1bhpj2hamxw4dr5baqk0bjwm2q9zym5bnjrl3ji2d12lygky4whk")))

(define-public crate-crox-rs-0.1 (crate (name "crox-rs") (vers "0.1.0") (hash "00a62xl5m915cmqw2iljimysv7li04nh471li2md8086n3ss33g5")))

