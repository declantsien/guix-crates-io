(define-module (crates-io nd co) #:use-module (crates-io))

(define-public crate-ndcopy-0.1 (crate (name "ndcopy") (vers "0.1.0") (deps (list (crate-dep (name "ndshape") (req "^0.1") (default-features #t) (kind 0)))) (hash "0w81rwj8zn7qp6gnc18rggviknnfiyafyznwzgs8dingx86fhf30")))

(define-public crate-ndcopy-0.2 (crate (name "ndcopy") (vers "0.2.0") (deps (list (crate-dep (name "ndshape") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s5qwkbk50sc8015m7k1hwxsivjrj1qjb3hgd89p7cv44dzrgz3a")))

(define-public crate-ndcopy-0.3 (crate (name "ndcopy") (vers "0.3.0") (deps (list (crate-dep (name "ndshape") (req "^0.3") (default-features #t) (kind 0)))) (hash "0naw17z0wsy36phmymdg8gyalvwa6sxy2nvm8avs9xz3h4dy9x14")))

