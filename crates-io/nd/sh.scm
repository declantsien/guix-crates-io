(define-module (crates-io nd sh) #:use-module (crates-io))

(define-public crate-ndshape-0.1 (crate (name "ndshape") (vers "0.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)))) (hash "0ygrmjvsrcq2sl6k2c3wrjyvnr536izia6iady2m318fyblppfyh")))

(define-public crate-ndshape-0.2 (crate (name "ndshape") (vers "0.2.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)))) (hash "16dh2i0xxqfcxla9m76gsskk4d4bmihrhlk7wh277rkysj3xjgld")))

(define-public crate-ndshape-0.3 (crate (name "ndshape") (vers "0.3.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)))) (hash "033hibaajwvq9gbbz5c6b1m6hl3hq03yjvn2dmzy4dynd9ccwnwp")))

