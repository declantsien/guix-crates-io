(define-module (crates-io nd im) #:use-module (crates-io))

(define-public crate-ndimage-0.1 (crate (name "ndimage") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "16zd7j1k0qj1w9ac54l1k2gjkvrg26f75ak03rq29fnqwz610370")))

(define-public crate-ndimage-0.1 (crate (name "ndimage") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-npy") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0lhgb8yz2kbcixvm29xfjbfy98nivfqcmpsbb9id8ycifdzq2zd4")))

