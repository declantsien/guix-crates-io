(define-module (crates-io nd ma) #:use-module (crates-io))

(define-public crate-ndmath-0.1 (crate (name "ndmath") (vers "0.1.0") (hash "13vrdc2j8yracxxdq3fdbnds9xlmq2inn1anig01cy0m805wqi4z")))

(define-public crate-ndmath-0.1 (crate (name "ndmath") (vers "0.1.1") (hash "0d6sxvifjhg5gpfj8r213y5ir856k8l81lgzhw3xjqj2kjpi74fv")))

(define-public crate-ndmath-0.1 (crate (name "ndmath") (vers "0.1.2") (hash "0fvd1ldqmwr0gg3jrpnz7l4dl37hx33zhkchz0iw0bs2493xc0si")))

