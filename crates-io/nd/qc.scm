(define-module (crates-io nd qc) #:use-module (crates-io))

(define-public crate-ndqc_minigrep-0.1 (crate (name "ndqc_minigrep") (vers "0.1.0") (hash "1nsrjsi04paa6517l81x2qw8zcq1rssqx5dm8fyxniyl5n8sw67m") (yanked #t)))

(define-public crate-ndqc_minigrep-0.1 (crate (name "ndqc_minigrep") (vers "0.1.1") (hash "0yh4d93ywy5n7p30vfpimh4gnbihm1sf9vy822sp94wv7cs0vr0h")))

