(define-module (crates-io nd _v) #:use-module (crates-io))

(define-public crate-nd_vec-0.1 (crate (name "nd_vec") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1c1s4dn1zby01zg0imzvnwk4ary6bb659l03m7ff9vw8j6lv289j")))

(define-public crate-nd_vec-0.2 (crate (name "nd_vec") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1l9b6q4linw93maqwgfv2rrfnzcdvch0cn4s2i4r4sqhr4g3kxwm")))

(define-public crate-nd_vec-0.3 (crate (name "nd_vec") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1y95hshldjg7kij9qrk4aqna0wdkjgghpkj6fg285azjicbmgbcz")))

(define-public crate-nd_vec-0.4 (crate (name "nd_vec") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0qldplxddr4c8z429ip4z21pgl45wbjzhrfn4rsdq9x57mxxkdpy")))

