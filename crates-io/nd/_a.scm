(define-module (crates-io nd _a) #:use-module (crates-io))

(define-public crate-nd_array-0.1 (crate (name "nd_array") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0yjx5yhvlgw1piv5q359njv9hn3l3mb27rp2jr0jnwrkpj0w7n3x")))

