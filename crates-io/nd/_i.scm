(define-module (crates-io nd _i) #:use-module (crates-io))

(define-public crate-nd_interpolate-2 (crate (name "nd_interpolate") (vers "2.0.2") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1ll793n8d6n6z89vw457mrrz020qrwnbf05w8av6inbj0ry0s18x")))

(define-public crate-nd_iter-0.0.1 (crate (name "nd_iter") (vers "0.0.1") (hash "1bkxpkbs5h07974xwdjc0ip2i8rq380j7qzb1hyhkx3py3dl8ywx")))

(define-public crate-nd_iter-0.0.2 (crate (name "nd_iter") (vers "0.0.2") (hash "0ggzzh8p0n0ci2qg6z4f3nwdjvkkknql6gai7bpssvsdx3pxlm66")))

(define-public crate-nd_iter-0.0.3 (crate (name "nd_iter") (vers "0.0.3") (hash "0j5xvbkrhwnsxqsjrkm4gr964rsvjsm2ia7ahpww9c34l7a3ry01")))

(define-public crate-nd_iter-0.0.4 (crate (name "nd_iter") (vers "0.0.4") (hash "12g8dm410dsc78dihk98jsgca07wsjv8apw63zqmxgmrq18nqqlp")))

