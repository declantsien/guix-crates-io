(define-module (crates-io nd i-) #:use-module (crates-io))

(define-public crate-ndi-sdk-rsllm-0.1 (crate (name "ndi-sdk-rsllm") (vers "0.1.1") (deps (list (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "ptrplus") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s9ykj6wgw0nknznrcljgm348mvh8kjaid29vyx239mhv3jbvazh") (features (quote (("dynamic-link"))))))

(define-public crate-ndi-sdk-rsllm-0.1 (crate (name "ndi-sdk-rsllm") (vers "0.1.2") (deps (list (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "ptrplus") (req "^1.0") (default-features #t) (kind 0)))) (hash "16w9kbykgdv0ngcnppx1vq2kznl5m18y5r5v0wg9zm1ha1rf6a8d") (features (quote (("dynamic-link"))))))

(define-public crate-ndi-sys-0.1 (crate (name "ndi-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (optional #t) (default-features #t) (kind 1)))) (hash "04md3r203kag3ihv11rb1fv6xk8rcql7xp96ij305mbdk4ph2l1n") (features (quote (("dynamic_link") ("default" "dynamic_link") ("bindings" "bindgen")))) (links "ndi")))

(define-public crate-ndi-sys-0.1 (crate (name "ndi-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (optional #t) (default-features #t) (kind 1)))) (hash "045sdra6zsi8ikqipln7lari8s9vxp7pns0nngfs3xc8q7xwjh95") (features (quote (("dynamic_link") ("default" "dynamic_link") ("bindings" "bindgen")))) (links "ndi")))

