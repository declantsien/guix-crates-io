(define-module (crates-io nd _l) #:use-module (crates-io))

(define-public crate-nd_lib-0.1 (crate (name "nd_lib") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.5.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0xixd1l18z1m1vhf5hv4b1vz1wjndh0z0fyb0r8djqngl09mkp9v")))

