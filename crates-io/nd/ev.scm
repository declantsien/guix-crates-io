(define-module (crates-io nd ev) #:use-module (crates-io))

(define-public crate-ndev-0.1 (crate (name "ndev") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0ymj0ia0bmbqla1155rh9fphc9ijy94vgsiv7ya9xsxlg15gzi0a")))

