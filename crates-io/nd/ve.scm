(define-module (crates-io nd ve) #:use-module (crates-io))

(define-public crate-ndvec-0.1 (crate (name "ndvec") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_arrays") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1m2w2ai9vxhn4w0k36xpxfay9z2sgdb3kmw1m4g38gpq44d35354") (features (quote (("default")))) (v 2) (features2 (quote (("serde_arrays" "serde" "dep:serde_arrays") ("serde" "dep:serde"))))))

