(define-module (crates-io nd ic) #:use-module (crates-io))

(define-public crate-ndice-0.1 (crate (name "ndice") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "14p3kg4wgrfmv72qc49pvlaxch1qsf3qayrlvwzziy2km64lnmwx")))

(define-public crate-ndice-1 (crate (name "ndice") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qlg8iv64sdxsybp4p2vs0pxyypa3wz0kw0hn9jfdyl9lr5ffdi4") (v 2) (features2 (quote (("json" "dep:serde"))))))

