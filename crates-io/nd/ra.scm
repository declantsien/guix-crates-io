(define-module (crates-io nd ra) #:use-module (crates-io))

(define-public crate-ndraey_downloader-0.1 (crate (name "ndraey_downloader") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)))) (hash "0nqmbsjy984y147a4z082qq5kgdqsfjqj6hzrpfak9n0frfcnhac")))

(define-public crate-ndraey_downloader-0.1 (crate (name "ndraey_downloader") (vers "0.1.1") (deps (list (crate-dep (name "futures-util") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)))) (hash "1n1zgqah18ykaf0f7y8sfaaxznwrla76gdpy3y08nmby2nh45bb2")))

(define-public crate-ndraey_downloader-0.1 (crate (name "ndraey_downloader") (vers "0.1.2") (deps (list (crate-dep (name "futures-util") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)))) (hash "0ppi8h5br0fjsgssa8zxn13l23bj4n2cmizcb2pxcfnnwkacnhjg")))

(define-public crate-ndraey_downloader-0.1 (crate (name "ndraey_downloader") (vers "0.1.3") (deps (list (crate-dep (name "futures-util") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)))) (hash "102wwrccc6ijrcqkg9iyq9s8p76gls6zddcsqkgzrmhm2i13qy6f")))

(define-public crate-ndraey_downloader-0.1 (crate (name "ndraey_downloader") (vers "0.1.4") (deps (list (crate-dep (name "futures-util") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bg52jw14zxfq9f9k7ypirpw8cyalzzpbpq40vmw2q6pminmgi78")))

(define-public crate-ndraey_downloader-0.1 (crate (name "ndraey_downloader") (vers "0.1.5") (deps (list (crate-dep (name "futures-util") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qvih2f732i32n0wp2sq40ybx6w0ydvk8j6pfgkfryc8x5q89y7a")))

(define-public crate-ndraey_downloader-0.1 (crate (name "ndraey_downloader") (vers "0.1.6") (deps (list (crate-dep (name "futures-util") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("stream"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y6d6r6y6va243q65zbxgxdbrp1ylb55icsc82zd194cyj5afzpm")))

