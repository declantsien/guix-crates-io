(define-module (crates-io nd -t) #:use-module (crates-io))

(define-public crate-nd-triangulation-0.1 (crate (name "nd-triangulation") (vers "0.1.0") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1carlks8kgx62l5lclg5js2lfyw507y4h8i1rbvr41b63xjpj8zk")))

(define-public crate-nd-triangulation-0.1 (crate (name "nd-triangulation") (vers "0.1.1") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "14qlp34rb5qmsgy6cp7p133gnkpzq3hi75a46m86f80paal1x22y")))

(define-public crate-nd-triangulation-0.2 (crate (name "nd-triangulation") (vers "0.2.0") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0nfg1x95d1g1cg1ynnhdr1d4ggxyi73fa0d25ff2n4hyb04mb3vp")))

(define-public crate-nd-triangulation-0.3 (crate (name "nd-triangulation") (vers "0.3.0") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1cinkq8p08dn37v0aw9f2ycyk3rnjzlia13h7ij3hw2mlq5zhp91") (features (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3 (crate (name "nd-triangulation") (vers "0.3.1") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0w57b8xvlg1m3ivk1swx8jp8zdfsjvnfs7s3i7s0r32dgxqncilw") (features (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3 (crate (name "nd-triangulation") (vers "0.3.2") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "04p77maf5n7zjrarh7q6gxsqs9qdvhrcqi0am3x7c5b08fsfkbyv") (features (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3 (crate (name "nd-triangulation") (vers "0.3.3") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1rd87k3vx1hcyzs10rzqxjxwqzfciflca6wfxyfa9argy6qbh7ch") (features (quote (("docs-rs"))))))

(define-public crate-nd-triangulation-0.3 (crate (name "nd-triangulation") (vers "0.3.4") (deps (list (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1svg1iplmw0kk2y9rpdbmg8n74z22m7cqi18504nv773356869is") (features (quote (("docs-rs"))))))

