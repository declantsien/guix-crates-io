(define-module (crates-io nd so) #:use-module (crates-io))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03i01sazshqinmh2qxfp4l1lfq2liyhgxchkg1d58mq8xdlncwfm")))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ilz4kh1l1mhsz136mj1jw98iqh4xjp3sjnsanilrd7nqy5r13aa")))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.11") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1bpx7mnj7fzqfkjxlwvscjavhhl9x0a11d8idfkql9dfx9sfyqky")))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.12") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1j1ybm4v026n5ij638y8xhx431a0faw4vidmwcix1gd4pcp5pibq")))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.13") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0sjyp3b1fxqdnykx8vpgay9w84vk3qdw460szds4sg9asgg356nl")))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.14") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "01dy9n7af7q2jsp8q8hg8x6mq3l0iz0ddf0sb70z8nmrsg24fvxh")))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.15") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1610yxh5czfrvy36m9b6laxfvq7ln9ay4g1lf60rh83pxmqhy9w8")))

(define-public crate-ndsort-rs-0.1 (crate (name "ndsort-rs") (vers "0.1.16") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wqh16w15xj818x1nbr20jfcjyc0fkd80mwldjwi9gvwnq5zb95b")))

