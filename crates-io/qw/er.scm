(define-module (crates-io qw er) #:use-module (crates-io))

(define-public crate-qwerasdfzxcv-0.1 (crate (name "qwerasdfzxcv") (vers "0.1.0") (hash "0zssn3zw4mmhv3sx73inkldp3dqy58k3xgpvdy7rjjhvynwvhr4v")))

(define-public crate-qwerty-0.1 (crate (name "qwerty") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.169") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0l0rvjfl2a76yfr0bh5np4yfy7p1fvf0g3rh52ymqs0krflgialz") (features (quote (("strict" "clippy") ("default"))))))

