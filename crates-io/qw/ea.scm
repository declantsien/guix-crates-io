(define-module (crates-io qw ea) #:use-module (crates-io))

(define-public crate-qweather-http-client-0.1 (crate (name "qweather-http-client") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json" "blocking" "gzip"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pvzs91bmgvz66spaxqxzj3c65bqvk8ccbhyc2cwgjlaa6rbzd1q") (features (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

(define-public crate-qweather-http-client-0.1 (crate (name "qweather-http-client") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json" "blocking" "gzip"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qzzgm6p14643l08l198nz1f59fv9yfcdfxa8z8fp5izrvy24plc") (features (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

(define-public crate-qweather-http-client-0.2 (crate (name "qweather-http-client") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.77") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json" "gzip"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gw4ckiczg26j8nff3f3y9qqskzaws62nacmis5lx21yi2fdb88y") (features (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

(define-public crate-qweather-http-client-0.2 (crate (name "qweather-http-client") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json" "gzip"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)))) (hash "0if9lrv380988gmf5r23cwksl2fp4yirhx9bd0sbxllmniy1qr6x") (features (quote (("reqwest-http-client" "reqwest") ("default" "reqwest-http-client"))))))

(define-public crate-qweather-service-0.1 (crate (name "qweather-service") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "qweather-http-client") (req "^0.1.0") (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1mlygll83c4bd3db76jbicd7qf690isblchysg6c7i1mamh0wwdy")))

(define-public crate-qweather-service-0.1 (crate (name "qweather-service") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "qweather-http-client") (req "^0.1.1") (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (features (quote ("parsing"))) (default-features #t) (kind 0)) (crate-dep (name "time-macros") (req "^0.2.16") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0x598a786fsd3qdbs13lcysm0c0v2x7qz4k2bq4x0mp96jghjwhw")))

(define-public crate-qweather-service-0.2 (crate (name "qweather-service") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "qweather-http-client") (req "^0.2.0") (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (features (quote ("parsing"))) (default-features #t) (kind 0)) (crate-dep (name "time-macros") (req "^0.2.16") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "168amr28lkzby8dp4b8sar689438hc7gz6z677pp8vhbhpjzxvvk")))

(define-public crate-qweather-service-0.2 (crate (name "qweather-service") (vers "0.2.1") (deps (list (crate-dep (name "qweather-http-client") (req "^0.2.1") (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (features (quote ("parsing"))) (default-features #t) (kind 0)) (crate-dep (name "time-macros") (req "^0.2.16") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "19444mvac470g6bwjhs7fic02c5mshdyyjzr4cwkpqylzwhx61m5")))

(define-public crate-qweazsxdc-0.1 (crate (name "qweazsxdc") (vers "0.1.0") (hash "042qksqfzjcgim60a9w59wqh2lfq37fjb4l8kw5m0l16mdjshvgn")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ffqvmi5ipqrc4kvrim7cpxhfgq74nw04yprhv8h7hb2cdzpdva7")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03a7p406l0xy9kw3v4nv66hf9s8lvj20xaqrrld3g8qn255981av")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sl6xpanqrchc23yp4k44033qj341rx5lk58m2rvijx5yhb23fbq")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dlpali0r1icyid8znkrz2fqnj9rdhcfm9iri2qgakj8ina5bax3")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.5") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0imj4hq55pbzxv8ywhnqxn9hh6c48h29frc9gy2qg8dk89i175nl")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bg0qgxqi48yy5ngpxwkxybswgplzqvqcrknx4mbv86j2d5ryqhn")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.6") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jlk5ckxaiiiq84hhjrvkkp3835va5hyk83zcmmrds475g6v4x8a")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.7") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0z06q92jgzwpgl2hvdn70cyn6rfsmyi3s9khqjylgracv9acjayl")))

(define-public crate-qweazsxdc-0.2 (crate (name "qweazsxdc") (vers "0.2.8") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hq4yr6zigmlmchwjyqhfwqa6sbnqwhri3bkr2pix5dy13sl47w0")))

