(define-module (crates-io qw ut) #:use-module (crates-io))

(define-public crate-qwutils-0.1 (crate (name "qwutils") (vers "0.1.0") (deps (list (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "1gib8s1v21l2qz4m19s95v6d9cmhg0xxl9s8x1fa3fnh8jaami1v")))

(define-public crate-qwutils-0.1 (crate (name "qwutils") (vers "0.1.1") (deps (list (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "10skfsfji2yppqzz9ymgvipr4hp95z1lml78cgvmh2zczl61bp1i")))

(define-public crate-qwutils-0.2 (crate (name "qwutils") (vers "0.2.0") (deps (list (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "0wm248hnyi06ban2shr3bg8kpxwg15s46mpm0aa33q9mfq67l43c")))

(define-public crate-qwutils-0.3 (crate (name "qwutils") (vers "0.3.0") (deps (list (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "0ds100dn63mxwnpvg0w77sg4g1qnm8vznk1rdbvhshc4icnd4sl1")))

(define-public crate-qwutils-0.3 (crate (name "qwutils") (vers "0.3.1") (deps (list (crate-dep (name "boolinator") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "0vdzvrmzclvcs4005g5sc6f5r8pdqiwgs4bd9jn81jip8vwhqg1j")))

