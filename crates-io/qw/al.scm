(define-module (crates-io qw al) #:use-module (crates-io))

(define-public crate-qwal-0.1 (crate (name "qwal") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("fs" "io-util" "macros" "rt-multi-thread"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ax4310msim68yilvmmn9mjnp3rgndca757hcqq15vyhjrimmpyw") (features (quote (("default" "tokio")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("async-std" "dep:async-std"))))))

