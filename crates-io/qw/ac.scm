(define-module (crates-io qw ac) #:use-module (crates-io))

(define-public crate-qwac-0.1 (crate (name "qwac") (vers "0.1.0") (hash "11cfp4kxvv2ikk2539rghkqj29mvj2znnclnm6llzbnc9a8s9vch")))

(define-public crate-qwac-0.1 (crate (name "qwac") (vers "0.1.1") (hash "0cdfwvkcm7jvga5nf5mjkymymfcv92ljlwilnlpdflh43xhrsc46")))

(define-public crate-qwac-0.1 (crate (name "qwac") (vers "0.1.2") (hash "0hjpyicnsnj67v7vmrdfsbm2378675875p5i5wygx22l3hbf2pca") (features (quote (("alloc"))))))

(define-public crate-qwac-0.1 (crate (name "qwac") (vers "0.1.4") (hash "13kgd4d3vbhkvgwjdrnndqzmmdphqlrxgm5bzpy20cg4fdxi1p8b") (features (quote (("alloc"))))))

(define-public crate-qwac-0.2 (crate (name "qwac") (vers "0.2.0") (hash "0n9rc9pfrsm32bxxzqglaa42sk4hl8kb5zhj5px2i0ki75j35ni2") (features (quote (("alloc"))))))

(define-public crate-qwac-0.3 (crate (name "qwac") (vers "0.3.0") (hash "0vqhn1c3f968wlv86pxmdj22hsc9gapkwr4i62hgr4qxy32gvfxk") (features (quote (("alloc"))))))

(define-public crate-qwac-0.4 (crate (name "qwac") (vers "0.4.0") (hash "0fllz2r4c15aj7dwbrwnh9q1qhhzcssgj0kgnv26fz6ya2m1f683") (features (quote (("alloc"))))))

(define-public crate-qwac-0.4 (crate (name "qwac") (vers "0.4.1") (hash "0n6aamannviqqqnzs2xd9854v04njydj6v1h5x1x3pmdnx21x4c0") (features (quote (("alloc"))))))

(define-public crate-qwac-0.4 (crate (name "qwac") (vers "0.4.2") (hash "1y4pfnq8jb7rsnh2bl7m89hmxq1lbbkn2si6k48jnbam9j68q270") (features (quote (("alloc"))))))

(define-public crate-qwac-0.4 (crate (name "qwac") (vers "0.4.3") (hash "16mwwspwssmw4qzadkjcccc23gbxf39vs5g1imga190945w27g5g") (features (quote (("alloc"))))))

(define-public crate-qwac-0.5 (crate (name "qwac") (vers "0.5.0") (hash "0bganf0yy06hf5fqrhpkb0g357hpx7j1xvh4qqxwmxaqfkiz3rdz") (features (quote (("alloc"))))))

(define-public crate-qwac-0.8 (crate (name "qwac") (vers "0.8.0") (hash "029wnjv99796pgnqpjl50ba4p6cnbicr3js6gsrbq4symbqmp77m") (features (quote (("f64-canvas") ("f32-canvas") ("alloc"))))))

(define-public crate-qwac-0.8 (crate (name "qwac") (vers "0.8.1") (hash "0sxmsmhc2i0cc249w52kmzs091arp928simvjxnm3mpjcmcnhq6j") (features (quote (("f64-canvas") ("f32-canvas") ("alloc"))))))

(define-public crate-qwac-0.8 (crate (name "qwac") (vers "0.8.2") (hash "0vwcb7z4xz3qhyjgk5l7ax6gh2dyylfzbkdb8p6haahvlkjrmdvg") (features (quote (("f64-canvas") ("f32-canvas") ("alloc"))))))

(define-public crate-qwac-0.9 (crate (name "qwac") (vers "0.9.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "1pqgzisf2brv1njl4xb43g5j303365hpzalmbjjszaajmrilbyv2") (features (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9 (crate (name "qwac") (vers "0.9.1") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0ilb18as2gc4sk6fshkph4ksy85z2nl5y3qz0phbi00qs72sjpc1") (features (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9 (crate (name "qwac") (vers "0.9.2") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0qdfs126shpd85kzcq7x92j05rx2vs4ligp0a1171rvppz3d0s0y") (features (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9 (crate (name "qwac") (vers "0.9.3") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "1lxmrj68qhwcr278xb37vz6bm4hv69l1c2zmpkicnyq2k8jkg08w") (features (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.9 (crate (name "qwac") (vers "0.9.4") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0hawc2d233lnpymz4z3krxc88gvqmxkqmp0clmnsssbz5ppr01dg") (features (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.10 (crate (name "qwac") (vers "0.10.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "041xq6mqgw26bgnvkf0nfyhni5s67d4p66fgazs1cn488b7f67w5") (features (quote (("std" "euclid/std") ("f64-canvas") ("f32-canvas") ("default" "std"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.1") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0116wl4yiy0gxkhzbr96hhnjr57jgmi7yi674n17gy324rgyg33m") (features (quote (("std" "euclid/std" "alloc") ("f64-timings") ("f64-canvas") ("f32-timings") ("f32-canvas") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.2") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "1cl2y4l503idgb09r1kxkfhdb9gma83l22xzv6nrz6zcznk0rdn2") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.3") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0y06jrs243m6advc4ybv8dqzxvprbwjn53svnjbmk411kr1pq333") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.4") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "1h6qwabngdgx22q3jv8m4v4hbcb16kqjpk78l1v0p7p4dsi96y34") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.5") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "12wzjf48f50p9m1gf4wgb3vfz9bjjdpq3ns052fvcs3kki679fpm") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.6") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0qqa9af0f1f65lkfgdp6h8vlc9v5axdjqs12pc1lfbb7vv0sy8bi") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.7") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0zdx4agklf708kg810l7d1f6kivsfn8533rdrxx4q416ldw5v11n") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.8") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0lbrmr25qm95qpjggxcs745mcspcddlgbsrs6lrv9wj0paccfl45") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.9") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "1i8wn23049fx1lg7h3k8l38nf19qzv5galxnmmqbm45c404sqa6p") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0-a.10") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "0f767lhzc7ai37f4qkip4a6988lb5gkarzp4l3zgx97i4x3vhsyi") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.11 (crate (name "qwac") (vers "0.11.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)))) (hash "1r7hfs630r0ny53v1hxhb4gr0yhp3knnsjcdjhljyf53vdx8vr66") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.12 (crate (name "qwac") (vers "0.12.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (features (quote ("libm"))) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0g0fx6q6h91gdyrs2cp737xw5fvlyg9plmhi7g76awx9l1ia4nmp") (features (quote (("std" "euclid/std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-qwac-0.15 (crate (name "qwac") (vers "0.15.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0yl6ml6nnmxkhndp5zk5gs4vabpgm0hvan96v0j9qpvdcrcayhkh")))

(define-public crate-qwac-0.16 (crate (name "qwac") (vers "0.16.1") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "0917jgbjjb6gl78k73vpin4i4d5w02vmjngnbfmhv64a7n11p1vx")))

(define-public crate-qwac-0.17 (crate (name "qwac") (vers "0.17.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "1hwva3z9zmawva8xgyqrcfnpfmd87f2y1h1ps7fkb4r92jw2h1g8")))

(define-public crate-qwac-0.17 (crate (name "qwac") (vers "0.17.1") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "0n0w14c2km7k8aqgry5kpc4i9fvfrw023jpdj99y40cin9bpwpqw")))

(define-public crate-qwac-0.18 (crate (name "qwac") (vers "0.18.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0nvb6kcba79qd9xjvk0sgz6z9w6svvv5a04p4nzr44kwgwch7zhw")))

(define-public crate-qwac-0.19 (crate (name "qwac") (vers "0.19.0") (deps (list (crate-dep (name "euclid") (req "^0.22.6") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "15n8vbwc2cfd89lzv7apljbvdvl4wji8jvfj8jm3iq6arg19d61a")))

(define-public crate-qwac-0.21 (crate (name "qwac") (vers "0.21.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "10048saz8nafhg619v689cm56sjsgi6ajadcm90m633c3zjlwcb4")))

(define-public crate-qwac-0.22 (crate (name "qwac") (vers "0.22.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1wzwm8nqqzv25lx0ahqc320p7zvq27zvjh6fv689wb131myxrszb")))

(define-public crate-qwac-0.23 (crate (name "qwac") (vers "0.23.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "10229jvk6passaqjbsibxi9nkrv4xfpzjc58avzxszlm1q3jxzjq")))

(define-public crate-qwac-0.24 (crate (name "qwac") (vers "0.24.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1795j4gx289gpqf2h1g167zs74xa2fg231qlymdi6grcvkv2mrp3")))

(define-public crate-qwac-0.26 (crate (name "qwac") (vers "0.26.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "0frpmf4fwj9iinrxjzmai4m3z3h2an09zpp16gd9d3jav3yrbpbc")))

(define-public crate-qwac-0.27 (crate (name "qwac") (vers "0.27.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "11f59r3gh4gdmbil8jcdlwv2bbkyixw2qw90qwm44b0hzlzxz9hr")))

(define-public crate-qwac-0.28 (crate (name "qwac") (vers "0.28.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "0wjxsc7lr6v98h915y5lrlzi1fsc37va4xx948giv5c0xfckzqip")))

(define-public crate-qwac-0.28 (crate (name "qwac") (vers "0.28.1") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "0176fd1fy45g742l3pnlx66yh1ihbg423x7ivq6hpnzvbp2cpxbp")))

(define-public crate-qwac-0.28 (crate (name "qwac") (vers "0.28.2") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.28.1") (default-features #t) (kind 0)))) (hash "1ipki3nvglx27ylg0yz07i2bfi1cp3nk7i7rb5ha3v0qs5f4jx5d")))

(define-public crate-qwac-0.29 (crate (name "qwac") (vers "0.29.0") (deps (list (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "qwac-sys") (req "^0.28.1") (default-features #t) (kind 0)))) (hash "0gqfdd0hgyv9wq7l5pa7h6bcj3hmbj0c9lcbkwm8564jjxvpviz9")))

(define-public crate-qwac-sys-0.11 (crate (name "qwac-sys") (vers "0.11.0") (hash "0ycl35ql76w9xrmimzypmrwa2hwwid492664aw6rc13cyiyp6a6l")))

(define-public crate-qwac-sys-0.12 (crate (name "qwac-sys") (vers "0.12.0") (hash "0p147h9265i88ncswh3p46s2kglg1f76bgxdl6kgc366rhfjjk1q")))

(define-public crate-qwac-sys-0.13 (crate (name "qwac-sys") (vers "0.13.0") (hash "0xsxylgmagiz5lafz48iqkmzis5h3bd2qrzdn02ib96r8rm8sw7s")))

(define-public crate-qwac-sys-0.14 (crate (name "qwac-sys") (vers "0.14.0") (hash "0q4ssdclffjmd79gr5i3lbvwqh52by3g8d71wg5720nmyhk1km5d")))

(define-public crate-qwac-sys-0.15 (crate (name "qwac-sys") (vers "0.15.0") (hash "178m3kvj3a83hl2bam40kdjirmgfpfqpgk1i3pmynnw3w4mvnpvr")))

(define-public crate-qwac-sys-0.16 (crate (name "qwac-sys") (vers "0.16.0") (hash "0y1152qx667c6dszsnxp68v7n18zn1k3bx5xfg7ir8032bfj5km1")))

(define-public crate-qwac-sys-0.16 (crate (name "qwac-sys") (vers "0.16.1") (hash "0qhm7263cicrimqxvgpsi7yv1rc19bhc9kc58n45dqh5m3ps880j")))

(define-public crate-qwac-sys-0.16 (crate (name "qwac-sys") (vers "0.16.2") (hash "19a3djhgp1ap0l6vcl3ra6q7603sbwp8ms6irh8vfgzgilqrsk4v")))

(define-public crate-qwac-sys-0.18 (crate (name "qwac-sys") (vers "0.18.0") (hash "0vdf0p1if9mxabjgbwzh1x0sgdlzrd72lcdxkg8gki7mq008fm2b")))

(define-public crate-qwac-sys-0.19 (crate (name "qwac-sys") (vers "0.19.0") (hash "1dxdd6ilz10dkp4yy7shb4bhfsyf2jfbknj7brwv538pi271l1my")))

(define-public crate-qwac-sys-0.20 (crate (name "qwac-sys") (vers "0.20.0") (hash "11ifhkl41in8v00l1ivlra3196v5ajjs6x60r5f7sw1qv9ayl6cp")))

(define-public crate-qwac-sys-0.21 (crate (name "qwac-sys") (vers "0.21.0") (hash "13dwcr4a092hbmd7b21dz42ably7k9swig1iggq0kl661byiid9q")))

(define-public crate-qwac-sys-0.22 (crate (name "qwac-sys") (vers "0.22.0") (hash "0sr3gfcyyhldmb4q8kvqfg06x9zrz4vk91dyk1qxlxxfm13jhyyn")))

(define-public crate-qwac-sys-0.23 (crate (name "qwac-sys") (vers "0.23.0") (hash "1csvvf2pw3ql8ll7ym2v52hnjxcd7lixgaa2hxvrwm8acy5c2b91")))

(define-public crate-qwac-sys-0.24 (crate (name "qwac-sys") (vers "0.24.0") (hash "01wym29affp9nzq3c4k86f4xckhl787nclp9yb9kxfrkjarldn77")))

(define-public crate-qwac-sys-0.25 (crate (name "qwac-sys") (vers "0.25.0") (hash "1pyd8m2wzlwhqy4kbsadci4ilc7c52icg2kf2sgc1pbqrrw1081k")))

(define-public crate-qwac-sys-0.26 (crate (name "qwac-sys") (vers "0.26.0") (hash "05szwq56j7lwjiq1kaswxz2pbflbpcj6axghmc3n17g79l2qffvr")))

(define-public crate-qwac-sys-0.27 (crate (name "qwac-sys") (vers "0.27.0-a1") (hash "0yszbjp4aq0c3syyvlq6pc076a1cx69j7vs8w2c6hcmfib8dy2vc")))

(define-public crate-qwac-sys-0.27 (crate (name "qwac-sys") (vers "0.27.0-a2") (hash "13ms8m74hzkp6ranpf7mw4k99d8bm39x2spfpwcy1wh20wlb8pyf")))

(define-public crate-qwac-sys-0.27 (crate (name "qwac-sys") (vers "0.27.0") (hash "017wflwppn6x65qkyyzl0jsffjqkbll2c3ajj63hzfxwzs2qz6gv")))

(define-public crate-qwac-sys-0.28 (crate (name "qwac-sys") (vers "0.28.0") (hash "1sjl6zav1rxvxmw0q4jmjavn62b2il3qida760hh7x75p29jn7pa")))

(define-public crate-qwac-sys-0.28 (crate (name "qwac-sys") (vers "0.28.1") (hash "1zva15ak7spa8cqzwks9hdq0hzv53iyb0f8b4601qwrbs4464gvc")))

