(define-module (crates-io qw ik) #:use-module (crates-io))

(define-public crate-qwik-core-0.1 (crate (name "qwik-core") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "glob") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.8.0") (default-features #t) (kind 2)) (crate-dep (name "pathdiff") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "relative-path") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 2)) (crate-dep (name "simple-error") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "swc_atoms") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "swc_common") (req "^0.14.5") (features (quote ("sourcemap"))) (default-features #t) (kind 0)) (crate-dep (name "swc_ecmascript") (req "^0.85.0") (features (quote ("parser" "transforms" "module" "typescript" "optimization" "minifier" "react" "utils" "visit" "codegen" "utils"))) (default-features #t) (kind 0)))) (hash "1kdrzsp7kkpbwqs8ml4l5nvl32hin8ig9da47kcrnabwggx461i0") (features (quote (("fs" "glob"))))))

(define-public crate-qwiki-0.1 (crate (name "qwiki") (vers "0.1.1") (deps (list (crate-dep (name "wikipedia") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "10cxpgi9x7rvfjbzbbr4hhxmxhy62ni438c91vi0l26j7r9ac1dl")))

(define-public crate-qwiki-0.1 (crate (name "qwiki") (vers "0.1.2") (deps (list (crate-dep (name "wikipedia") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "04h5lyf7qv1rfg1dk8955w57g8hxvy7a4f4216fy3ghr0fvnh9xj")))

(define-public crate-qwiki-0.2 (crate (name "qwiki") (vers "0.2.0") (deps (list (crate-dep (name "wikipedia") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "105j2pxpm8jcm3nyjn182q87pxdwpv0wfvzs8crfb3fj4mzbmbhx")))

(define-public crate-qwiki-0.3 (crate (name "qwiki") (vers "0.3.0") (deps (list (crate-dep (name "wikipedia") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1yl1inwfwrwp08f49zcqhfkf7vvgzfrfrp492622ksmq0dlf2xkj")))

(define-public crate-qwiki-0.3 (crate (name "qwiki") (vers "0.3.1") (deps (list (crate-dep (name "wikipedia") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1r6r821ihvvmxl0gg909f1bdjjjdf9sdj9bny3k901b8zg47q8vj")))

