(define-module (crates-io qw ii) #:use-module (crates-io))

(define-public crate-qwiic-adc-rs-0.1 (crate (name "qwiic-adc-rs") (vers "0.1.0") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "06nan5mws2xry0pgqvvihprl78920szlfx6qrh0zxdvnj2k7q2wk")))

(define-public crate-qwiic-adc-rs-0.1 (crate (name "qwiic-adc-rs") (vers "0.1.11") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1wpz4zcfxjch3awvr9gbc0k4bf6k473232knqqns62c5b8ssny3g")))

(define-public crate-qwiic-button-led-0.1 (crate (name "qwiic-button-led") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1az9n5180d25li91ad3nx2qhirwjis6j1j4g9vhai1laj4gnrhkj")))

(define-public crate-qwiic-button-led-0.1 (crate (name "qwiic-button-led") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sk2b7ckvkcpq9wzqfnjpv6810xmyxdf9jxrv712n6h2irjbrygv")))

(define-public crate-qwiic-lcd-rs-0.1 (crate (name "qwiic-lcd-rs") (vers "0.1.0") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1lq8dkx3xdz7zdw2bnqv5nk2m90lbq5pgfg534q2iawz6d0pr1wn")))

(define-public crate-qwiic-lcd-rs-0.1 (crate (name "qwiic-lcd-rs") (vers "0.1.1") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0y2vm3wkzqhacd2mdzjaflqpk41h52x83jsmld8vjhb20xkn5f17")))

(define-public crate-qwiic-lcd-rs-0.1 (crate (name "qwiic-lcd-rs") (vers "0.1.11") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1lvf4vnp6n1x70h3zb43bcg9yyc9q3y6av4d0bmp43vwb0nyipws")))

(define-public crate-qwiic-mp3-trigger-0.1 (crate (name "qwiic-mp3-trigger") (vers "0.1.0") (deps (list (crate-dep (name "i2cdev") (req "~0.3.1") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "1hcylz2m5d7dgp745dvlzjj2agpsnm7psf0h7yw9k450xjc35ml2") (yanked #t)))

(define-public crate-qwiic-mp3-trigger-0.1 (crate (name "qwiic-mp3-trigger") (vers "0.1.1") (deps (list (crate-dep (name "i2cdev") (req "~0.3.1") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "0cc969abnhphfv7yvbim41hnws3x6k79qchclp4bk4fvdzpzi1qh")))

(define-public crate-qwiic-mp3-trigger-0.1 (crate (name "qwiic-mp3-trigger") (vers "0.1.2") (deps (list (crate-dep (name "i2cdev") (req "~0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "1mbczv99byyl9cw68cslfnhyvj9mk606h4khmmwfhpp697vl9hip")))

(define-public crate-qwiic-relay-rs-0.1 (crate (name "qwiic-relay-rs") (vers "0.1.0") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0zfxhx17d3pcn7vd6cdz5yjwax16d4xjf3cnfqbm7k9zm5nw1qk8")))

(define-public crate-qwiic-relay-rs-0.1 (crate (name "qwiic-relay-rs") (vers "0.1.1") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0irh378zrxkncp022knja66skj9fqmc15nzlwpfvdcb0p70d58yd")))

(define-public crate-qwiic-relay-rs-0.1 (crate (name "qwiic-relay-rs") (vers "0.1.11") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1drsq10vl92c9336zjpw14sjr9zjqkhdgbz3f16dvkqm7fhxh3va")))

