(define-module (crates-io qw es) #:use-module (crates-io))

(define-public crate-qwest_r-0.1 (crate (name "qwest_r") (vers "0.1.0") (hash "10a5sjz9mb439ljd3m2cld0grv2p9afx40bnf1944bqmc9skcqqw")))

(define-public crate-qwest_r-0.2 (crate (name "qwest_r") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tcod") (req "^0.15") (features (quote ("serialization"))) (default-features #t) (kind 0)))) (hash "0wk235495mqcfdrj2h2ma8r9dp3nk0x6r62jwlk4x86262i6r6bd")))

