(define-module (crates-io wd _b) #:use-module (crates-io))

(define-public crate-wd_balancing-0.0.1 (crate (name "wd_balancing") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "wd_log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mxvk67r4k5g9m9wh36804bhx45mdydx5z1y90pyvdzpjcw1rrl2")))

(define-public crate-wd_balancing-0.0.2 (crate (name "wd_balancing") (vers "0.0.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "wd_log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fmqamib6r88i85w9my1yp36fgzvcz9xg76nhgcpyblcvqnbs99m")))

(define-public crate-wd_balancing-0.0.3 (crate (name "wd_balancing") (vers "0.0.3") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "wd_log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1awjyx3frl3wmk3bazs7b3ka9isgdl766nb6wp7z1xwzjglr2ky3")))

(define-public crate-wd_balancing-0.0.4 (crate (name "wd_balancing") (vers "0.0.4") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "wd_log") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ps7lx378x5sa8fd9jq6a2v8cad4881hm7yz907h1cj9vx7cw94l")))

