(define-module (crates-io wd _l) #:use-module (crates-io))

(define-public crate-wd_log-0.0.1 (crate (name "wd_log") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1za5db8ajdn1z65awk34jkfj0bgjhf0im2zyqydh8z0qqhi2536g")))

(define-public crate-wd_log-0.0.2 (crate (name "wd_log") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1g8khfa0p2413dpl2zwr7r22w3k3mdir3gw4f57q4xvnanmvlial")))

(define-public crate-wd_log-0.1 (crate (name "wd_log") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "128844yhdp0866qsadilzvymj26ks0am5r1cvym7xnbbapji6yrw")))

(define-public crate-wd_log-0.1 (crate (name "wd_log") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "162j5n04icqwa7wm77iadgxxpmbc5ql23q84zny6l7g72jhy3bfj")))

(define-public crate-wd_log-0.1 (crate (name "wd_log") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "05mihmff2h5sn7rsj592i7gmcx3ysgvgxss95ma2cd45i6rqjcgb")))

(define-public crate-wd_log-0.1 (crate (name "wd_log") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0wxbq3xvhz3hb8vikx8zzq7ml5v95ycr81xf2cxb40gqmzpwph48")))

(define-public crate-wd_log-0.1 (crate (name "wd_log") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0i59qy5ayhaix9yc8xws5pgy76qvpkwym86w5vn8b93477cfkjck")))

(define-public crate-wd_log-0.1 (crate (name "wd_log") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1n908qi5cmfjb5z2iiz8w1z6prcpl521mbkv0ryfhl5q4b75ch6v")))

(define-public crate-wd_log-0.1 (crate (name "wd_log") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "15jr5ds67l4m7anicmkalhr05kgznn80hag8mrh22gjn8m3w12f1")))

(define-public crate-wd_log-0.2 (crate (name "wd_log") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1h9wl4bx70l9ld1ggs2bkdhm983yiwy4zg1zvfpj640ksdmplcbi")))

(define-public crate-wd_log-0.3 (crate (name "wd_log") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "136r0cf22qg8s1rkpqy7xlff85cdb8rh236sfpbk1fr8lbvfjbf3")))

