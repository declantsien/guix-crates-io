(define-module (crates-io wd #{40}#) #:use-module (crates-io))

(define-public crate-wd40-0.1 (crate (name "wd40") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ksnhinv55pn45hwg7crxsl6vw4y2965zyzgwva8hibmwyallrmh")))

(define-public crate-wd40-0.2 (crate (name "wd40") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.8") (default-features #t) (kind 0)))) (hash "0gwz2bi82sgbj5lkyxmy454a15s2mm4pa73p0fd4r2v4hnspf007")))

(define-public crate-wd40-0.2 (crate (name "wd40") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.8") (default-features #t) (kind 0)))) (hash "181wkfhjf6a7h6ysq94bh7b4wxw8qmwin1hijcxxagmibvv9nr68")))

