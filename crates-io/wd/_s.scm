(define-module (crates-io wd _s) #:use-module (crates-io))

(define-public crate-wd_sonyflake-0.0.1 (crate (name "wd_sonyflake") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ci43ihgx7dlf3d5wsasmb38p1i9hw88gapz41kf1gyxgm6p01m3")))

