(define-module (crates-io wd _m) #:use-module (crates-io))

(define-public crate-wd_macro-0.0.1 (crate (name "wd_macro") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "15v9lqldbfx243wrz10wyjklx0nmz4j42s9gj81agkcx3dyj0ln6")))

(define-public crate-wd_macro-0.0.2 (crate (name "wd_macro") (vers "0.0.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19y198wfqkwv7kgj3amlkhqs6krnzih4x1y6j8m5m88mzzpg55hv")))

(define-public crate-wd_macro-0.0.3 (crate (name "wd_macro") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qlmc1i2xl2pkl61jyzswn4m5my81npfrnc50p69q5a67xqik84r")))

