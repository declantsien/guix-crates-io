(define-module (crates-io wd av) #:use-module (crates-io))

(define-public crate-wdav-0.1 (crate (name "wdav") (vers "0.1.0") (deps (list (crate-dep (name "dav-server") (req "^0.5.3") (features (quote ("warp-compat"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1rl8i9y63jbicyjiavhvvzppqlmgnvrc780qv5rzps1gs348pdwg")))

(define-public crate-wdav-0.1 (crate (name "wdav") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dav-server") (req "^0.5.3") (features (quote ("warp-compat"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0kfw8h7qd76mpxqmzvdla7397v1h3wfq5zj9j933lr231iy9c63h")))

