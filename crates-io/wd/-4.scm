(define-module (crates-io wd #{-4}#) #:use-module (crates-io))

(define-public crate-wd-40-0.1 (crate (name "wd-40") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.21") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "12787r35shab49gnkk3xkbi7i89fcvs88lrhva1sjqqj5j3l77pl")))

