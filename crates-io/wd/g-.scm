(define-module (crates-io wd g-) #:use-module (crates-io))

(define-public crate-wdg-base16-0.1 (crate (name "wdg-base16") (vers "0.1.0") (hash "0yyb5dzm97p55cv5mn2chif7dkxqrxqxhmrhk77dwvivz3dlbd6g")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.0") (hash "1sjsmr47f70p6am7pyckgylv632lags9z4gls12m9r5q5hbfjyf0")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.1") (hash "1d9a5hxi33jgjbyyz7hx15ama34kbhganrv9shmk0fd39f5b1m5r")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.2") (hash "1kp5h2gj49cs25cjrkibb3w03m2hpaz29069bdm2iwzlyns8n6v0")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.3") (hash "1miw1d1w2qff6wriqyw28vjywhvgp7r5mq3wiccgrpga7i0y3hm1")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.4") (hash "1rnx6ng1qrgs15a8jx5jazh0x5sxs1v4sfngssnmawvqmw6yar3v")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.5") (hash "0kqg8zd40nyi8i30z6njina7vgns7xrrp5m770crhfi1ckhd47ag")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.6") (hash "0dzwj9hg52ar89iv3xmaz6prh5ry8a0sw6dma9vpiv4imyx4i816")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.7") (hash "0psgcrjx8ymf5mnn1wr9d8kn2iq5kyirqcfr0y5qf6040xx880pa")))

(define-public crate-wdg-base16-0.2 (crate (name "wdg-base16") (vers "0.2.8") (hash "1ylvv09rjvazkmq0fdws8na3b83chmhdlccl0saxvblpwshwhbvn")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.0") (hash "01lfhjmsfkls4bn1f0fcmly66kdz34l5w39dwydd5sck7w9cylw5")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.1") (hash "11j4gjp7k0b8fx4cagy5xxn6hsw0k1gqyk2qzj476i3ny9cqffsb")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.2") (hash "0g2cyk7gflvlmp6hwzsnyygdx9zi7nzfn0db4vm2gl37gxmnhdgx")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.3") (hash "11sxaxw18sbfd5vnnzlmpqngvs3lxz7nspvgacpvjh0m6p4jyj7c")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.4") (hash "0k56f91w3fkb7jk1gwk9jkywvr7xp561n9bjr0fnpb5bynzybnaq")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.5") (hash "0lr7iqnnl1wmbiw7dcycdckj7ihiga2jk3fgq6wvdcrbnfvr5cz6")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.6") (hash "11jxbcz9a2fdp08rrfbxwfvyzhqbavp6narrwlbcpzysqpvv6wad")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.7") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "182cl413l5la3cj0x6n22sg92b68xxx463gyrxrgh8x5yxwpjf4h")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.8") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0sknxk1gh7byr862h408rblpyg0xl5jcck7g07ykavrn9c8xjizi")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.9") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0xc8vjhvvbhnfwqpncx42s645xpd1rjnlznlwpgrjbg1h560jk12")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.10") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1814d150s13bapxd0j2y3gn95pm4qy1ggmzyd47qmcihnjpaiklz")))

(define-public crate-wdg-base16-0.3 (crate (name "wdg-base16") (vers "0.3.11") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1k44041jv295zlfhk2735k8cavbhncy0dmbpl6hbaxw09r15abrf")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.0") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "14rkmsxfxgd57bssyz0sbwpw4q68njyqkf9p2z64h2z4ah1kyb8v")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.1") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "01p34xn1nbmmgzfbzl406s00rywa3vmrbz0vs7fj9vvc0aqq7z3m")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.2") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0bb4ghpp4wbyy751l7xwddyp0vbanbqfn2lc8dpq2m45l6mwmwi5")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.3") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1qm3c4q36ln7a41byxigann1b86331cibx3yyi3c0ra2z0qra7pc")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.4") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0194d3apigfdccmd41yl898a106pnp1j6nqi3q9bwb3ma7i6qgw3")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.5") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0j01j451lx9fvn44lw873gad5klls6qz5nhz47glljpidid1qfj5")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.6") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1c89xdycx6c2i802dw5h1xibshsa19x61nb7g8qzz4w3jx938qdx")))

(define-public crate-wdg-base16-0.4 (crate (name "wdg-base16") (vers "0.4.7") (deps (list (crate-dep (name "futures") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "futures-cpupool") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0i4jnklps8j74pnm6zpq438qig2318jm4vdjd3vggplaajc5341p")))

(define-public crate-wdg-base32-0.1 (crate (name "wdg-base32") (vers "0.1.0") (hash "18f4rxr0hg5z99181dp1plp2bqsinnvxqbcngv2ja2n1h7czrygx")))

(define-public crate-wdg-base32-0.1 (crate (name "wdg-base32") (vers "0.1.1") (hash "0brj71x1faqd7rw20rafq35pdp1629ryrdksr534zh95c65lrbnx")))

(define-public crate-wdg-base32-0.1 (crate (name "wdg-base32") (vers "0.1.2") (hash "0m9hqx9gva5jz0gyzd8j4964k5qvz8pfrn6vyhyxy0f2zrxmr8ms")))

(define-public crate-wdg-base32-0.2 (crate (name "wdg-base32") (vers "0.2.0") (hash "1r4wkv69hapwjk44kh1f6ga3j448bsvdy68sysqpjh9y7sk1khfi")))

(define-public crate-wdg-base32-0.2 (crate (name "wdg-base32") (vers "0.2.1") (hash "0kjkng8c0imm8f95i822kx3czc59zmk7bwnvdy3l4daf8shfn40g")))

(define-public crate-wdg-base32-0.2 (crate (name "wdg-base32") (vers "0.2.2") (hash "08i0vj9mwahk0r7m0alnz0j00yzs43prgdkwinb4jnic9dfqcmza")))

(define-public crate-wdg-base32-0.2 (crate (name "wdg-base32") (vers "0.2.3") (hash "19nqnwsdz3rnsmhpimyslb246l8g26a430s6i4l92xnkag3x8z8h")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.0") (hash "0mxma1kqyak2i1w9fpl4iwiks45bbpxvsj10yj51dn98x4fr4s6x")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.1") (hash "0j54x1f6mfmji84vzdxdjzw3xzvdvqr1a2mvmll19hm16bv42rl3")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.2") (hash "1n31g7bpa1hskbb82ns5ds8l69lwq2f9z1k1yivm5ybdc8bkzyzn")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.3") (hash "00znnixsvi11ipyqpib146v3ykpz4yjml9blns6kyvfiqwsjmbxc")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.4") (hash "10s9sscy7d53jsxk9bkcilw81674igf39gv9i10cpsbfzn6x77sd")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.5") (hash "1fqspfxgz9fzpgn28qlhjmv2xjmkpzqksz8azqzb7k0xg47caysb")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.6") (hash "0bvq52hnjxwqib7q6x68rls8wqaza9cyfwha1b9024l2wfxzmaf1")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.7") (hash "0gybdc8iv6ryzc4am8rav7pcdh4np323fzxpg68wl0ga8xxb624r")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.8") (hash "13zjba2w728y8w5knjhswqrg8hg49k2bxjix3ya36lw97j18rfkc")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.9") (hash "1calap6nlg28k84ckvid6glcjw2wnxv64g8095iji4gxs5zr4x00")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.10") (hash "1gh6n1db4gl37q8d3m3q8qb6isq5lm9dpcdszlawmhc7lmccm53q")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.11") (hash "13q2881hm5426nbmk1xl8nhhbk7rjhncl0fl4v6zrzwy0awxmnqw")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.12") (hash "03dz7j2b77s9akf8hib04m4i3184amgis34v8x33y1jidm7wfw60")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.13") (hash "0a4mmban7f4nfg3wxcg3n128wxxgwlq7as4wdwfc86hv11r2jdn5")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.14") (hash "0bynzbwmsjhgxnf9kqpm05j188az83sz0lv1xdr4dxf4a8mfwj3p")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.15") (hash "0ln16lr465nrmsldnv11gf9p4918d5c399qzc4g63br8pnc5y1jh")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.16") (hash "0lkia56i4jwkc0cw7dw9i4q1rcrd1s9nvvy548lr7z0s4f2ix5db")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.17") (hash "1fr4gjp3hv9vcqn34d0jg9ljg2xdcpyxl0swj9mjb5yvnpnd221x")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.18") (hash "1p7wakq8grndhp35b9ppcbnq5lz956km7raslh4c858i0lc399xp")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.19") (hash "1ihkf2apcii9sr43cbw20zyfa26dw8bg9ynmk4v504rqz31hn2xh")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.20") (hash "01vglmr705qfivx53n2kildwlyq3nmir7d1vd7ldpzykp297pyvr")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.21") (hash "08m14z4g44jsh3xb7icl3k4ky39s3qhl30bl32rng8aby0mmp7g0")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.22") (hash "038jg3yj1gail0p58is3ax4hw2hw5n912wij9nm9vw8frfidnvjk")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.23") (hash "1ski0c82i924w183w644cwvlx0x8d2r9y6wgci55iykp61i9prm0")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.24") (hash "1vmfmxmiw4cap7b55zf963skcmc7hf9fgdqxfcag82iw5qrvfpz6")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.25") (hash "1wgj3pk68a21r5d3hsxljig8l1c8viasf6f7l4pdd5i98qh0a9i6")))

(define-public crate-wdg-base32-0.3 (crate (name "wdg-base32") (vers "0.3.26") (hash "060ch8xncrbb21yyrw2502q0zx5zicf6i6azg5ka4wj2xcij3sa9")))

(define-public crate-wdg-base32-0.4 (crate (name "wdg-base32") (vers "0.4.0") (hash "1a4li7jgk341rrfzpxmynvj8i27nlv0dij7qwbx0rf44l9jc2xcv")))

(define-public crate-wdg-base32-0.4 (crate (name "wdg-base32") (vers "0.4.1") (hash "1nv8kv53ihq91frp0fj304442q20gp5h1j4rwyr7mxkf88mjykhv")))

(define-public crate-wdg-base32-0.4 (crate (name "wdg-base32") (vers "0.4.2") (hash "00qscwfsvwrz1br1wjbrwn0ccn8rmcvp4gznpw4iqvqh344gs4b0")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.0") (hash "0i4bhqamwnahisisfp464anba90ybnzp7rpf2bjbn3jqxf2ghxl1")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.1") (hash "1wkz8qs0dzflbpzq8dnaka0v7rvm1bw487hwb8qhy1pgh1xm12f4")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.2") (hash "0w9nhxz9f1c9jss4z2m0clxg33j2nyqa3qjhz79gv0rabn3jd3lr")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.3") (hash "0swqhimfklajkrx0kismnx3d3y8lrlfcxs3s7sx2q4xnnwpj6sja")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.4") (hash "1j0p4vkznb5igykq1aylcnnp7xrlg749f177d5av1c7fb6cfqd55")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.5") (hash "0vmm6411kzl3drbg0px87bz9a461vpjynzq8vc5hb0skmdcnxik3")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.6") (hash "0pdk9cn0w6dzwrdlva5mxg5z97dlxy019r827x7km7hfgqgba84n")))

(define-public crate-wdg-base32-0.5 (crate (name "wdg-base32") (vers "0.5.7") (deps (list (crate-dep (name "wdg-converter") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "00kl1w6582ksmz32di8d6ygbjippk4r1q48hrqq4i5r39xwadlps")))

(define-public crate-wdg-base32-0.6 (crate (name "wdg-base32") (vers "0.6.0") (deps (list (crate-dep (name "wdg-converter") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0b7dphm4mlcmmxparnfj6mvlxgkyyfajjiyv7g6fc8n1335w6cp0")))

(define-public crate-wdg-base32-0.6 (crate (name "wdg-base32") (vers "0.6.1") (deps (list (crate-dep (name "wdg-converter") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0sr32m6v1m966srgg19j1qi70brkz14npmzrrjibwgj4fnp23ld3")))

(define-public crate-wdg-base64-0.1 (crate (name "wdg-base64") (vers "0.1.0") (hash "1qwb6zrrwik2lxvzc6iw7ir4qhyg64kmczs5sz3pcq5gi5sdksja")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.0") (hash "02scyn93m5qww32d4qsrqr9zz4mbs1091ga21bc6n3avyv7gsmx8")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.1") (hash "0lhq4mjdz9mhsjsvwjcm0ffhsq9z30d39rm4nzlpyhd836ifvfmm")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.2") (hash "00lk25dw2lcys2m4lxj610qzhin47mk3r108d22mw2g2dsk81qiw")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.3") (hash "04fam2rli63wjqr57m5pi4jciwhhjl01fzpmxk9vq75rrkia2nka")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.4") (hash "08czf6rqgqccfk7a2mdigv577bsiphhkz0nwar2shn9838jzaxi6")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.5") (hash "1ra44n7vmp03mwzk256fv3x5qppy4q9b4vdpk35s0nz1hn91x4v2")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.6") (hash "0knpif6x9szc885ii0giafdz49r70y26rhdws48f4ngr88lwnhj3")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.7") (hash "1j3n1i4r5rvcbbcqpf4gdrzz6i5ls30hrr52a7vzj3i1bfp4sf8p")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.8") (hash "0dcc1d3jmcag9nwwpzly6bigxmqgvq48g461ki05k0f2rpanjsif")))

(define-public crate-wdg-base64-0.2 (crate (name "wdg-base64") (vers "0.2.9") (hash "0rgaajwl5i2wkbsp1flbc744zb2pax7f7pq4dwfk95l4qkx9mnws")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.0") (hash "0v3ijawxqyz1kz445zm9nim51wf06kml8xqdfii6qhbhz9v6kpvg")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.1") (hash "1bpdf01mziyj5lk8s60wir175xx2w304qp72gq095zkn78ag60bf")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.2") (hash "0gn8ncp12ryj2xnzsrh276kyxl9w1i9s5gk7a5pjccn8xcmb657z")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.3") (hash "0lqwjvn684whls4ymcx1d9g73rfx8z5ym3fbfq0x0i0cb05xx2mc")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.4") (hash "0qq0z2ky8aw7srcc5hn0yaj488iw9gdb4025fxdrc5xk9fca7vj0")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.5") (hash "1zkdkj9488iqfvjcd2l7wyfwvw5i6cmnwy9dbvmxb6j2szm9d34j")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.6") (hash "0qgk3i6c08ijaz381ad0gq47fri3pxd0ib6crh6qp5r1acm8pzbz")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.7") (hash "0f4lyvm5wk95bav4a7c19a7l9gd9xh9ypk5n1hlaj2kygkbm8lk5")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.8") (hash "0h9i311mji8vc2fjnwc9320sr1pnlchyifaak667ffhjbdyg46ii")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.9") (hash "0lmnf9hxmqq86b8mb2cqldxwjc8p0nysw38d9mxvdddvvr37vkhn")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.10") (hash "0x47x2vd182fp7wi4d2vj0zrxhyxgrnf3iagbx0bbf1di2r6g87f")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.11") (hash "0abi365yygrv3x2nakcilpq26nwvmynmjbf2543g4g3z5449gvwq")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.12") (hash "1y648mnp8yvnppxkshksbxs4s55r7mvqqchmjx6y3p0cl1zcv9vh")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.13") (hash "0zm8jcdc5jzfsmy4ilc6iwf3b6chs69jzh4qm900hajk5qjcnhaq")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.14") (hash "142nrhh670a9qq3frvzw67hkq5d9nljn982lz2fd4blr1r0a7znp")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.15") (hash "1lcydxcd77966yn5fsjfkkmj3245f2dih7sdhjbjbfjf40hpv8z5")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.16") (hash "19hzdq4smgf2w2pls2hsp8y5gwqx42k2896sh2xq4a9z2bw63wi1")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.18") (hash "1z20dgwrk7wf9rp9j46k3b8v6jybxwsp7799ixpsp5asd833pxks")))

(define-public crate-wdg-base64-0.3 (crate (name "wdg-base64") (vers "0.3.19") (hash "0cdbcvhcyxm1qvflga1gk6pq2p7vyj47bzxhks7yd99sg1i5lgxy")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.0") (hash "11q4psl7v46bh4gk6r383csn1j6xygmb33apb3ijkpk5m4pw7xsp")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.1") (hash "12mfkh5bvgyr57xnpdzsv7zbrxdd1ljkg4pap5ipj0clwk0l3m6i")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.2") (hash "0ddg16la5lddd7d3xxxwa0sbsb3m075495ji5xb6wpwkq6gh8fil")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.3") (hash "020i3sq4rys79wzmldsxwxpkvgs75l5fkdab2wqyifc09z8hjgfi")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.4") (hash "16qbm5wavkrhsdhk2g8r224ih8crqvyjzd37x99fx4pdbv61k6pv")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.5") (hash "03w9dk2fi207v1sqpkpdrc00i093c6nly3cb5a7ficlrhgkqls1p")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.6") (hash "0hj3smm3ipff009h9hal97y8gis2d44hzciq4jr7jnml20bslryf")))

(define-public crate-wdg-base64-0.4 (crate (name "wdg-base64") (vers "0.4.7") (hash "1qnw52ylknijhgasq74769n2ra8cz1p53950gn9h1hwmkxvn8nsf")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.0") (hash "00bp850pg4i7sms8rh9237rbpblr9d8qxlnrkzm4igp7arnnzpmi")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.1") (hash "0ixmfdl79kc8k3llwcl018fsc7k8np2fvn0nhi1hnj7gafkpxrnf")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.2") (hash "1x5mppnlm4vxjsncpikwhkcwh99ck4n37xfj3zzn1xx6n7sjhazg")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.3") (hash "0nznixwagvpsp62f6sf3wfx7vdap0knvpwl0jh48bahvs587qryb")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.4") (hash "0ykyhdspmxwvqrm14g7lzj63d6nz1nz671553ypqxd84xzrp5nv9")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.5") (hash "1fbqfmjmdyw53bshjkm5v4swn99x43b3nnjdivqc1r6lkxa0f4aw")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.6") (hash "0ixgfsv2yh97aqngs2ddrymqa84h9dnwi4ydq8p6inr7v45izp23")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.7") (hash "1h0k615vn160bk4wm76jdjgbq96zim58xh398annbx7gcija70y0")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.8") (hash "0zb3jjxkmxslilxf56kyicd7yssd9p9n8clwg7330j1zacn17fi4")))

(define-public crate-wdg-converter-0.1 (crate (name "wdg-converter") (vers "0.1.9") (hash "0rlnw8vhlip21az09avxa1rlmsnzabxhnb6ly3hssjnb0xracrph")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.0") (hash "1nz1vxyf0p7872w9dhlvkpyqy68pyz318391w404p4y2cic8c9nh")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.1") (hash "0manbzcvfx5hw2rfvskb47xf42vkw8whnnjqqdvvgzlw4nd1gawj")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.2") (hash "0yihp25jw5li82mknrqfspksgy12ndj79l0za7v6g23dvybq6a3m")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.3") (hash "0j62vsmhsh0f9cv8hq5jw4zgckprazhpn8yq2mbflcpbl01p7mbx")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.4") (hash "0z9kfadg9bm8m7cyn1fgj929rvssix2arm0zdlgyvqmsnrsly9iw")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.5") (hash "0mnm8gid8l234dgy9az7dba6k08w19440904m5xvgr88a1vhq327")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.6") (hash "1v26m2s2m7rk2cda5waclynq0nb8438f9zsia2955n990szsak0d")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.7") (hash "0z0wic6fnpwm8jgpzs4g0amwpqahjzmdajjd1bzv796nva0v51li")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.8") (hash "09w5a03ar1hj1c803bwij34aw2wfigjy23pjpiph8ip6ls5zdc0d")))

(define-public crate-wdg-converter-0.2 (crate (name "wdg-converter") (vers "0.2.9") (hash "0cl39yl8q3xqk4wplba8fg72f83n0fhyic3kmlas1997jlrixjy6")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.0") (hash "0ipinkq1zjzizhilzv8imp49cwv5x9f8b32bc7njz6jf9vdna3fz")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.1") (hash "07cx6w09lbrz7cjy322l9f9zx5p321hjk5jfv1p0kxc424c6pdpb")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.2") (hash "0vi1dr3y12k7kn955fvlxfncr4ap51bgbsij6p7wjckzljgqja03")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.3") (hash "1mzfz2bsa4h19n2ws9c32n5j0b5l7av78mgx0f4hskzlwfni8n0y")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.4") (hash "05apv6ycj9lwi2rg186hhpsl11vrfnhlyhpiv5rv71db5b0xywjj")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.5") (hash "1hy9clnlg3z5jkvvdh4a3bcfslf048d9x8qnwqpccnx0lyv7qfyw")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.6") (hash "1vyzs5dr4mv3b3iw0sikfg26j6gkvqvrd4471gv7kr0pxpvmcypb")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.7") (hash "1q5sfdf8mj20cm8qv4jydy2vgf9lqmklbsq3p0mcd0vcc5j4zv8b")))

(define-public crate-wdg-converter-0.3 (crate (name "wdg-converter") (vers "0.3.8") (hash "1v9b2hgs6zwf3n7qi9wk8s47wz84h1kfdair1164s766blr9j5ax")))

(define-public crate-wdg-converter-0.4 (crate (name "wdg-converter") (vers "0.4.0") (hash "13qfvj3my5sfwd96fskxhw6a7rdan6my9hf2nlgy8gbf7m67q5yy")))

(define-public crate-wdg-ds-0.1 (crate (name "wdg-ds") (vers "0.1.0") (hash "02zaj2aqadn1zknbvwmwvhh9qzr0ip175qh4qdd3vjbx90b0mvgx")))

(define-public crate-wdg-request-0.1 (crate (name "wdg-request") (vers "0.1.0") (hash "0f7h9sl2wi9wiwmldr27944zg43fsjbbk8ngr282n51galidkrdj")))

(define-public crate-wdg-request-0.1 (crate (name "wdg-request") (vers "0.1.1") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "wdg-uri") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0463bm824nslk3z8iy64myfrd5j3jszjg4sbim4asm19j4lrgsqm")))

(define-public crate-wdg-request-0.1 (crate (name "wdg-request") (vers "0.1.2") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "wdg-uri") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "05nhbqni3x7qiqmsd5q1b6nv1ryjc52868azd29ls8d2iwwnpz2l")))

(define-public crate-wdg-request-0.1 (crate (name "wdg-request") (vers "0.1.3") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "wdg-uri") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1yv52814l4m5sbhsbprmm5k1f5s3b9dbykn3bcvbcd43310cji22")))

(define-public crate-wdg-telegram-bot-0.1 (crate (name "wdg-telegram-bot") (vers "0.1.0") (hash "11b8j3h537zhl2nlxxf0ylbms8k095y924npbq618hl59asr7i28")))

(define-public crate-wdg-telegram-bot-0.1 (crate (name "wdg-telegram-bot") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tokio-tls") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0k1zsjajspqvnv987mg0q2z9nz32id8f6w5n2fjkkl4h7i1l9gm5")))

(define-public crate-wdg-telegram-json-0.1 (crate (name "wdg-telegram-json") (vers "0.1.0") (hash "13p2f2bbprw7m5n61xv23kjkxaxg6gh1zy33332ipchibjzjg8rg")))

(define-public crate-wdg-telegram-types-0.1 (crate (name "wdg-telegram-types") (vers "0.1.0") (hash "0nf50qy31pf483wzic9f20mbrv2pl2r0bb98n46rf9r6pzbavv52")))

(define-public crate-wdg-telegram-types-0.1 (crate (name "wdg-telegram-types") (vers "0.1.1") (hash "04jgcnn2l65vbjnm30rwicaf98y405jk5c9q6310m34daflb4idk")))

(define-public crate-wdg-telegram-types-0.1 (crate (name "wdg-telegram-types") (vers "0.1.2") (hash "0277v09qplq907n84d5r44sqa02d2sbq6qclz2yrxdd1hnbwb399")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.0") (hash "1sy0h03whw4kp7j8vv26gr95ajr463k0j5dwafca43jh1pxkq0c2")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.1") (hash "07shsjzqd3qymgv03f58py50r75kyzs1gbks17n1p17f1k6l5v94")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.2") (hash "140dqc8awf4azznpf280glb8g3w4iz18f1qf5ada2qaqj17kvrfn")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.3") (hash "0j3ncskj5d6ckf3cvxchzpanyrsp1a0pchq8wvvl484i2843kwnc")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.4") (hash "1rcqcd2wnx7w4na60w99d154r69r8f6ms41rib1mr0ycqd0jh9q9")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "16r32vgh39sn4ji2jgqqnz5akcdmmp9ypqx5h1pzk0r9bhaxvwyk")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "006w2yr4w6s0b6a856i01ky4wkf7knivby7zb0ahgryahk3nqqhp")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "15bjp37cnsk8qygxw4mlmf7kyq70d4jhmx9j7y1qkqrw6vkl6vip")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.8") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1hkxbzkrsjmm5am2nn5vmp98bqxzn6lj6w6r51k3qd76y43jvwm1")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.9") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1yb987krlk6wbpd6ym0803zqaaziy8ag9xvixpzra24qrlkgz9sb")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.10") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1ihvavdjz8r1s1niqzficv4j56dksha723g12gzqa0xh819gd1gv")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.11") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1w4avi5k9c2ag8mqmfcsdyvfnvb2bff144302yr4bcxim60pf265")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.12") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1bkmghdj77rc1lyk3sdsh35367719s82x4flr3brlsmmfrpcqr1l")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.13") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0c4aphcpqcvhz04n2l539x2pch23vjgf2x1igazpjl7hngv3sbw8")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.14") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0dr6jy6lvvcxjwl44szbypqp7gmirrkp88mm7jff5n8aab13dry7")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.15") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1bcj3f8glbg5dq8mgz21bhhj2zc0mg832y0m704vm4f3m5klv1fz")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.16") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "10z391x4z0yjwk4zhisrd5x5vvjrva0bjcgz9mh2y7dshx0fz89j")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.17") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0qcj81iibr3vk5ip2yzv0spg3md4mj5kq3xaxvqxgc99l90qz456")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.18") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1zci2q1ibwxgbv5rv4ax3qkkambi5dzamz7pn1x1p5b2nfk35cqy")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.19") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1hcrqvhw6xc8knj47zdll6657j2ki2a0bavhw0bk6970dimdhfwp")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.20") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1dd9wiwcp3496d1mmqab2ypxxfxgcl4z2j04qxcpvki13vv6ph4y")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.21") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "134fci920wrpv3rbf6m5klphg1cjpfpca0sdqghd8s1rg842y66h")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.22") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0m462cld07jwllprxdbbadqvilwppf79m2kn6r9z2r39ipmlykn2")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.23") (deps (list (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0qdp35xrqni3n0hripi5x6g80ji66bg0mj1w1fdnsayk7h6jlrsz")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.24") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1zzv4h31xw7fg3fhcd41jgk5bj2wv7na40gn6p2g67292lgkhq2v")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.25") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1kf7ydazprzkf3vffqqhsrzj3vhg2ln7dq6k1zz136z8ikb5i7ix")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.26") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "12jj3xirzrpa0p53p3a8db1ikvx7xbkzvyyixhafzvf8vd74vd1i")))

(define-public crate-wdg-uri-0.1 (crate (name "wdg-uri") (vers "0.1.27") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1zwrrjkn3vy0iifj81vpz08ydkmr7wyrw93n33yq339hmv6ihlvb")))

(define-public crate-wdg-uri-0.2 (crate (name "wdg-uri") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1b91im8xbxsd9kin24z375p109riig20nqi6bmnr5jh1zsc99dbg")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0dl69mklycy9nbgax30vcizvqslyw3qqlb0sw1f0ys88g568ad46")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0pz30gywqjy9h9ckm90i6kmkjscbg1yw4ji5hw08l74fl8kxn7pc")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0lfgmbjrsrhgzn71qbqccqqhn6dfg9rw17rg0jcbajlj3b2vc5z5")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.3") (deps (list (crate-dep (name "default-port") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "01wqv7fr56bh1kd2rbcvbsvi34c84whph655gxb6xmdyx0d8m5jm")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.4") (deps (list (crate-dep (name "default-port") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "17v3d3mm07v1k077b75ds3c1br4sgp46m068id3dw53wx16h24kr")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.5") (deps (list (crate-dep (name "default-port") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1g6dz37imk7ibmqy89fbz9gkndnzad2gc51d917lj4pg4w824p68")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.6") (deps (list (crate-dep (name "default-port") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0n3x99rmn7n25ifnzkh66p7xiah603mjdj9w2jh9gwgyk3xpnr0y")))

(define-public crate-wdg-uri-0.3 (crate (name "wdg-uri") (vers "0.3.7") (deps (list (crate-dep (name "default-port") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "regexp") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "string-repr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0gyv8xrg6m14d926xhmair2wwv60lp2ipp0rgjn2asj6hbmrl6gp")))

