(define-module (crates-io ol s_) #:use-module (crates-io))

(define-public crate-ols_regression-0.1 (crate (name "ols_regression") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (default-features #t) (kind 0)))) (hash "04zfxicrqn0x6cnfng4hqz56ngrqzmk2gfdrz7f4nlxqkn1r420w")))

