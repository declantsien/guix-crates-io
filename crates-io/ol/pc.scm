(define-module (crates-io ol pc) #:use-module (crates-io))

(define-public crate-olpc-cjson-0.1 (crate (name "olpc-cjson") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hfrvygsaf6hj09z2j5znm0pb4wwmf4m3369hy9z3j366d4y42cl")))

(define-public crate-olpc-cjson-0.1 (crate (name "olpc-cjson") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "1753jspq3h3k2xqw9jcxxfk6bfyp9slpsm7f4kqvn52hd3z4kjkj")))

(define-public crate-olpc-cjson-0.1 (crate (name "olpc-cjson") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "0g61m0ck54zg81rydpbqwfql67jxbp6bq6hwcx9xi310fb7pbp47")))

(define-public crate-olpc-cjson-0.1 (crate (name "olpc-cjson") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yh2n9ppm4mlqgpqm5hy5ynm21ihd2llz3ysjzswz733bg0wjdyn")))

