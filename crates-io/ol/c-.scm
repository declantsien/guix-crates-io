(define-module (crates-io ol c-) #:use-module (crates-io))

(define-public crate-olc-pge-0.1 (crate (name "olc-pge") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1rd5zvszj0v7s2722l09ilhn8p879yizqkqy8jvsv3lrxnqy69y0") (yanked #t)))

(define-public crate-olc-pge-0.1 (crate (name "olc-pge") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1i23xn23gxgp9lk33y0rhcad543gjjh5yv8hgaajsgyd20rpp7ji") (yanked #t)))

(define-public crate-olc-pge-0.1 (crate (name "olc-pge") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.23.13") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0k6qy63hr7sm47p9x6apxngjl1mda7rbv5zyig6n903ihg718kzj")))

