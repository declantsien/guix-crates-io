(define-module (crates-io ol in) #:use-module (crates-io))

(define-public crate-olin-0.1 (crate (name "olin") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qmn2gl21v12wl9fw3gybljw6kii6p9wm6qiaah4m2693pv2hawd")))

(define-public crate-olin-0.3 (crate (name "olin") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1yvidc6aqavq5z05p8d1d5899bip64hy96apr1n8bakjwjwrsgrl")))

