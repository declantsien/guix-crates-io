(define-module (crates-io ol d-) #:use-module (crates-io))

(define-public crate-old-http-0.1 (crate (name "old-http") (vers "0.1.0-pre") (deps (list (crate-dep (name "openssl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "0048k2fm36r2k8a0rgigjvwz5pdppgi0m05p3gpq2qlngwqm7klz")))

(define-public crate-old-http-0.1 (crate (name "old-http") (vers "0.1.1-pre") (deps (list (crate-dep (name "openssl") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gcq8a0cmv2qaf69q04d4f7jmkj9qp92p18k76s90wmaym8pvy9f") (features (quote (("ssl" "openssl") ("default" "ssl"))))))

