(define-module (crates-io ol ds) #:use-module (crates-io))

(define-public crate-oldschool-0.0.1 (crate (name "oldschool") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1rp1n7w3b7ij51mpvb5bi6sb4ibrlq4swaxhc6j5vngi2qclyxxh")))

