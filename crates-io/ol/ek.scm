(define-module (crates-io ol ek) #:use-module (crates-io))

(define-public crate-olekit-1 (crate (name "olekit") (vers "1.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.1") (default-features #t) (kind 0)) (crate-dep (name "ole") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0a0n4g4xzc2d012dpksb53mv4f3zs15brpgn5855cxkcpg5c1jhq")))

(define-public crate-olekit-1 (crate (name "olekit") (vers "1.0.1") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.1") (default-features #t) (kind 0)) (crate-dep (name "ole") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "1llags763cpa09zcnak8187q24y3gdag0n6m9n8iqc3d6fq9kvkv")))

(define-public crate-olekit-1 (crate (name "olekit") (vers "1.0.2") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.1") (default-features #t) (kind 0)) (crate-dep (name "ole") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0xla2vawb4396qh9figb2cxskbmhfpknrzincl2pzkb8wcdz23h9")))

