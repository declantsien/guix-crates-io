(define-module (crates-io ol ag) #:use-module (crates-io))

(define-public crate-olagem-0.1 (crate (name "olagem") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "1nz58j83aviwdymbryy37hymnmqg735xngc5n2bqngda17xajxwm")))

(define-public crate-olagem-0.1 (crate (name "olagem") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0znjl1hy4pgbh1kk8b2k1v9lcb8fik1pbhry955z2l1dvrh19c9p")))

