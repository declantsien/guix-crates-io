(define-module (crates-io ol c_) #:use-module (crates-io))

(define-public crate-olc_pixel_game_engine-0.1 (crate (name "olc_pixel_game_engine") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0dwdfvp7l4xsnyc7agxdr8gqw6287i1wfjzxq92bxdk8vir6j244")))

(define-public crate-olc_pixel_game_engine-0.2 (crate (name "olc_pixel_game_engine") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1sfpwsmrh1sdfa07mql5mxqsj6nacia5a55ncv8mm1q407pqlgwr")))

(define-public crate-olc_pixel_game_engine-0.3 (crate (name "olc_pixel_game_engine") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "10d74zvvm5cmm048j5sxcx665alr6i1vif9rk42y18rfd1jcwzbf")))

(define-public crate-olc_pixel_game_engine-0.4 (crate (name "olc_pixel_game_engine") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "13zniliaxpbcwqga3g8wc2i55d6kks3f245qsmrh7gacl9g8y2pc")))

(define-public crate-olc_pixel_game_engine-0.5 (crate (name "olc_pixel_game_engine") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1znvckaijm4md5ybs7g348hqz22ns019wpg7x1sgfa1dicq339b1")))

(define-public crate-olc_pixel_game_engine-0.6 (crate (name "olc_pixel_game_engine") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0h0fqci1821riy9rm42j50w0pyn9x383djbfq39bs9jb20prripq")))

(define-public crate-olc_rust_game_engine-0.1 (crate (name "olc_rust_game_engine") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1zbvg1hr9lrv57l2a216b8dh9k2szx5r6hza0clyfs50wz5x0dbw")))

(define-public crate-olc_rust_game_engine-0.2 (crate (name "olc_rust_game_engine") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "keyboard_query") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "06xw0797nzxdf5j02ixfnlnbk0942gdjh5y3sygqyrzfbp5hjfcm")))

(define-public crate-olc_rust_game_engine-0.2 (crate (name "olc_rust_game_engine") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "keyboard_query") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0k0ifvk9wilmppcymhbgpx0a023s844xlz55kprgpzyi3rkfxqxd")))

(define-public crate-olc_rust_game_engine-0.2 (crate (name "olc_rust_game_engine") (vers "0.2.2") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "keyboard_query") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1zv0pkbin9bq9xabajh3wh6vxz4v9a2sf145j9l6sa97m3ga3k4s")))

(define-public crate-olc_rust_game_engine-0.3 (crate (name "olc_rust_game_engine") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "keyboard_query") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "09p5dyf4r3j5l3ghaaffq6k728kawp1awzg1wi20035h106imlc5")))

(define-public crate-olc_rust_game_engine-0.3 (crate (name "olc_rust_game_engine") (vers "0.3.1") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "keyboard_query") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1bbaxwn4qiiqbanyjry2217ddc6rmlq9wys6kgbjv60gx6ri1hs6")))

(define-public crate-olc_rust_game_engine-0.3 (crate (name "olc_rust_game_engine") (vers "0.3.11") (deps (list (crate-dep (name "crossterm") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "keyboard_query") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1j0jj23b0sgq3vr8i01xg6a94f71glmlb6d7a11x61z8f9442n1r")))

