(define-module (crates-io ol e3) #:use-module (crates-io))

(define-public crate-ole32-sys-0.0.1 (crate (name "ole32-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1z9ydzn8ggymvify7svmzzxky3k6p42csvzkcjvra6akdj48g3cy")))

(define-public crate-ole32-sys-0.0.2 (crate (name "ole32-sys") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "18wn7jxj3z4pd1qapfk9kfz9v90h3p0hnsdkxsgwh5ak223p8h8h")))

(define-public crate-ole32-sys-0.1 (crate (name "ole32-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1w7cn6vjpj7izjf31nh87dch1bh7f6a1zb74fyx17mvzp258nh59")))

(define-public crate-ole32-sys-0.2 (crate (name "ole32-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "134xg38xicrqynx4pfjfxnpp8x83m3gqw5j3s8y27rc22w14jb2x")))

