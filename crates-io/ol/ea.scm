(define-module (crates-io ol ea) #:use-module (crates-io))

(define-public crate-oleaut32-sys-0.0.1 (crate (name "oleaut32-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0yyhv61py3xpgc9m2va1sl4kr5fld154x04w0vps1hinbz8vs3wq")))

(define-public crate-oleaut32-sys-0.2 (crate (name "oleaut32-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0bv1vzlzjp8wcvjibgglccibhmn3ki1ca0jvkbyllf171frigphz")))

