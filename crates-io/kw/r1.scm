(define-module (crates-io kw r1) #:use-module (crates-io))

(define-public crate-kwr103-0.1 (crate (name "kwr103") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y98lh9yv01qsch6acwl3cdvicfixl7bqzg5845dbhc9sqh9p4br")))

