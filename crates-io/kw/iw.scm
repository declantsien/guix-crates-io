(define-module (crates-io kw iw) #:use-module (crates-io))

(define-public crate-kwiwk-0.1 (crate (name "kwiwk") (vers "0.1.0") (hash "1qpfam817jdbc02qfvkrz3nq15rfhzakjp5rics46ffrdlj7ii7c")))

(define-public crate-kwiwk-async-0.1 (crate (name "kwiwk-async") (vers "0.1.0") (hash "049h7774lb90iapbsp9nclkd2laxq9csy5rrdjf4ddhnvwcz9hkr")))

(define-public crate-kwiwk-error-0.1 (crate (name "kwiwk-error") (vers "0.1.0") (hash "0wajx8gm2fw0f74vrq21lsmcfnbjilymm4kmhwr48f73qgx05iap")))

(define-public crate-kwiwk-experimental-0.1 (crate (name "kwiwk-experimental") (vers "0.1.0") (hash "0vf9gm4248hrizyvhf9qzl8friz6p21l7hxwi1j7q7j4855xg43i")))

(define-public crate-kwiwk-experimental-0.2 (crate (name "kwiwk-experimental") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1zq57cqdyydrd5vq97skfav0ck27zrw067a3kz2nz9jrrdvllkpa")))

(define-public crate-kwiwk-experimental-0.2 (crate (name "kwiwk-experimental") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "04fz2dc7vidmr5mab6q99lyp81kp8fcmp51ar65xnh0630d5g4jr")))

(define-public crate-kwiwk-experimental-0.2 (crate (name "kwiwk-experimental") (vers "0.2.3") (deps (list (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0g2dg4vylvip973dmhi4dwxaas08rakc5nmc1hsphflk49h6k6dx")))

(define-public crate-kwiwk-experimental-0.2 (crate (name "kwiwk-experimental") (vers "0.2.4") (deps (list (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "03zhj6ya65b7xrqjf60ryykf368cg36mbmdvxccx6pa50r1q0p3d")))

(define-public crate-kwiwk-experimental-0.3 (crate (name "kwiwk-experimental") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "127sf9hjpfh8ha2ixf5bkk8nh4q7pff5brk5dlkbz6knk1hqkwp4")))

(define-public crate-kwiwk-log-0.1 (crate (name "kwiwk-log") (vers "0.1.0") (hash "0jmfzs0lavf8pdkih06fvlp65iq5lzgfd6gjfghbglclizzx09hj")))

(define-public crate-kwiwk-option-0.1 (crate (name "kwiwk-option") (vers "0.1.0") (hash "1hz6zj8z8v73phgr3lsmhn9igsqrcvs9rlmq514mckk2icz4wipc")))

(define-public crate-kwiwk-result-0.1 (crate (name "kwiwk-result") (vers "0.1.0") (hash "07i6f1zlmcxz8jsrnkkj4xanpd8x8z9j2isvn3gg3mgi44plamd9")))

