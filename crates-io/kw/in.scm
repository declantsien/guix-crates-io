(define-module (crates-io kw in) #:use-module (crates-io))

(define-public crate-kwindex-0.1 (crate (name "kwindex") (vers "0.1.0") (hash "1srlhsvhvsf31ainqarry4314haw1py6mdapwzf9h926bylsla1l")))

(define-public crate-kwindex-0.1 (crate (name "kwindex") (vers "0.1.1") (hash "1arpr5ql1mzsw1bdcfarjm2s2gar6i8yp8cm1z4sbcc1wwwjyr25")))

(define-public crate-kwindex-0.1 (crate (name "kwindex") (vers "0.1.2") (hash "1zh2xd1bsq4jpnlmz1gd6jdfdinzmdkw95qwcnjfj8766a9d70qq")))

