(define-module (crates-io kw ar) #:use-module (crates-io))

(define-public crate-kwarg_macros-0.0.0 (crate (name "kwarg_macros") (vers "0.0.0") (hash "1dg0xmhgc5jnlqacvp47x2ipdx10mc1lgrc3jwd876gvbpi6v7yk")))

(define-public crate-kwarg_macros-0.0.1 (crate (name "kwarg_macros") (vers "0.0.1") (hash "1x0b5vyxjzk53xm6ip2gh528p9a86n0yriady0a4jpbf0mxg951m")))

(define-public crate-kwarg_macros-0.0.2 (crate (name "kwarg_macros") (vers "0.0.2") (hash "0sk0fy5zc82rfgsjkm5h8qax957f2c4zy64by0j14bvp38clmgv4")))

(define-public crate-kwarg_macros-0.0.3 (crate (name "kwarg_macros") (vers "0.0.3") (hash "0r7w7s5lhp2851g85mabqnij2ls3bm9wqxl7y1bhbsr91i6pr5r8")))

(define-public crate-kwarg_macros-0.0.4 (crate (name "kwarg_macros") (vers "0.0.4") (hash "0i9f4bqd0aha89cyqjrl280is2zj32vq5n7d8g6m6yqcviv4jkdh")))

(define-public crate-kwarg_macros-0.0.5 (crate (name "kwarg_macros") (vers "0.0.5") (hash "1ha3fy7y9402zg35vycq0x1xypzpp1x1hhvlhaj3a3zj3zcxkxyw")))

(define-public crate-kwarg_macros-0.0.6 (crate (name "kwarg_macros") (vers "0.0.6") (hash "1giwpqvnrsjwpamg8p3jnh1lvl8wjh4yw2aadqvwpldsz3vg1hll")))

