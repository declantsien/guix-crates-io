(define-module (crates-io qb ar) #:use-module (crates-io))

(define-public crate-qbar-0.0.0 (crate (name "qbar") (vers "0.0.0") (deps (list (crate-dep (name "terminal") (req "^0.2.1") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)))) (hash "1vy7xj92xa7fb6hxj6ljqizihr4dmwnhgxh4bhxpw5p45rmcnx4z")))

