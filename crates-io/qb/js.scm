(define-module (crates-io qb js) #:use-module (crates-io))

(define-public crate-qbjs_deserializer-0.0.1 (crate (name "qbjs_deserializer") (vers "0.0.1") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ijakwc0lq4rfmin9n6pflb60wz8mc0dss4vlwxf196q1k9cs1na")))

(define-public crate-qbjs_deserializer-0.0.2 (crate (name "qbjs_deserializer") (vers "0.0.2") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pr7mwllszs3nqjh0gzzpb497i4yg187fp6wcgjky07k1x4jnxvn")))

(define-public crate-qbjs_deserializer-0.0.3 (crate (name "qbjs_deserializer") (vers "0.0.3") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "191gm0blkd051prgqmc8ch938zhpdn0k7phph0z3f0nwlw0yxic0")))

(define-public crate-qbjs_deserializer-0.0.4 (crate (name "qbjs_deserializer") (vers "0.0.4") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jh3mmfzr7cba10kq14nkvpdq8rg2klw6yzb4mdz0wbq9fhsh5y4")))

(define-public crate-qbjs_deserializer-0.0.5 (crate (name "qbjs_deserializer") (vers "0.0.5") (deps (list (crate-dep (name "encoding") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "07z98cny7fr5xd5l8a9mq641cv8s4fy7zawa5wibq2qbfzaa1yvp")))

