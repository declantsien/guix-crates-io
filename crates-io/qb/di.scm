(define-module (crates-io qb di) #:use-module (crates-io))

(define-public crate-qbdi-sys-0.1 (crate (name "qbdi-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0zq45jrcrcnq91b40j134zrbgaq9gawz3k1w844l8f93ian6jpw7") (yanked #t)))

(define-public crate-qbdi-sys-0.1 (crate (name "qbdi-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "05jy0hp9lhfpzh4avq3n7zc44vw1ys14zc61j1g069n2a11cr17g") (yanked #t)))

(define-public crate-qbdi-sys-0.1 (crate (name "qbdi-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "0qfjq875pf1dicjkav888bgspdx50z8mkpkxz9x01z8c7f50a86h")))

