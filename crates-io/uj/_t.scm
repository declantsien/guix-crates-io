(define-module (crates-io uj _t) #:use-module (crates-io))

(define-public crate-uj_tcs_rust_23_1188-0.1 (crate (name "uj_tcs_rust_23_1188") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uj_tcs_rust_23_18") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03fxyx31xpf758bnd4m0qm2n9la81nw896k746ma5z91sxcbazml")))

(define-public crate-uj_tcs_rust_23_1188-0.1 (crate (name "uj_tcs_rust_23_1188") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uj_tcs_rust_23_18") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mmbhbdxs67198sh7c2vhh28j341qvm0fqg70p3lkx6wqa2axf7z")))

(define-public crate-uj_tcs_rust_23_17-0.1 (crate (name "uj_tcs_rust_23_17") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uj_tcs_rust_23_18") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kzrxff3afbwcbfkpbk9qw4z6ai0fwqk1v1bzz38xl4mii11ja2g")))

(define-public crate-uj_tcs_rust_23_17-0.1 (crate (name "uj_tcs_rust_23_17") (vers "0.1.2") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju_tcs_rust_23_19") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zr5dcqqj9p5y94f4mgh03ks5al457x1z49j9s1ng7gah2baj7kd")))

(define-public crate-uj_tcs_rust_23_18-0.1 (crate (name "uj_tcs_rust_23_18") (vers "0.1.0") (hash "0z9dha2nzvpjwdv09n3jipgl9d9l4ablxwb8a6pr1jcdb54y6ryi")))

(define-public crate-uj_tcs_rust_23_18-0.1 (crate (name "uj_tcs_rust_23_18") (vers "0.1.1") (hash "03n9j361dvf7ix50ixcyr8lg7l2v6kmi14sjf491svlgiq8h7053")))

