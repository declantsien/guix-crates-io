(define-module (crates-io uj -t) #:use-module (crates-io))

(define-public crate-uj-tcs-rust-2023-13-0.1 (crate (name "uj-tcs-rust-2023-13") (vers "0.1.0") (hash "15iiw87j787k3l1j867c1z1fv8dggippjrqqligc819hpl75ihhm")))

(define-public crate-uj-tcs-rust-23-1188997-0.1 (crate (name "uj-tcs-rust-23-1188997") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uj_tcs_rust_23_18") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10brj2dx3ijbkl829w7cza96f02g523nkq57aj8xknqyij1hw199")))

(define-public crate-uj-tcs-rust-23-26-0.1 (crate (name "uj-tcs-rust-23-26") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1j2nc1h0l9bkwm6xqqvyln27ylsw6yxwxn8aw8jfp58haxhibk55")))

(define-public crate-uj-tcs-rust-23-26-0.1 (crate (name "uj-tcs-rust-23-26") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ju-tcs-rust-23-25") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0yid0lpydgqvgd2lh61qdhfvxydy66z8y46hn071kb4xfp7vbn8d")))

