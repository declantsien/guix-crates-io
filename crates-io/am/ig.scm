(define-module (crates-io am ig) #:use-module (crates-io))

(define-public crate-amigo-0.1 (crate (name "amigo") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "11k51sy8lzgb3i530q627zzbymxzi9j9fhcppsm3rxfpy9cspjsk")))

(define-public crate-amigo-0.2 (crate (name "amigo") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1z2i5cssicpkbjrgy04bhlh04yqg866ykyxbv4csg3rqg7mxkp8x")))

(define-public crate-amigo-0.2 (crate (name "amigo") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ys88q8a0drspvwwimk5asb6bsc2xdb425rbkyczk7ba5cyyn7l6")))

(define-public crate-amigo-0.2 (crate (name "amigo") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "16m56l2a92zb10sn4fpd6067vls3kih9a2disprw3l9cxdlkqd2m")))

(define-public crate-amigo-0.3 (crate (name "amigo") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yv4fl7mrsj8a929a438692k225vgqz0vjrnqaj48acw0v4xgm9a")))

(define-public crate-amigo-0.3 (crate (name "amigo") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b025pxb08san2xj2jc65rki7vslilm6jxx71bagp5n05rx5nyn3")))

