(define-module (crates-io am xm) #:use-module (crates-io))

(define-public crate-amxml-0.4 (crate (name "amxml") (vers "0.4.7") (hash "0h6p5rx17mkjbvdpy5jcnp55iwnwvl7d9dz60fcxad5zar61xg64")))

(define-public crate-amxml-0.4 (crate (name "amxml") (vers "0.4.8") (hash "13mrgml0fqxgq436gkg1l6w4w4rvhj1ypk7rzc6i4rg68wd9r8qx")))

(define-public crate-amxml-0.4 (crate (name "amxml") (vers "0.4.9") (hash "01655jq5z83nbbc3h44fbqs7hr4xpv6pczq6h26njllc93jry6j9")))

(define-public crate-amxml-0.5 (crate (name "amxml") (vers "0.5.0") (hash "1xd6frcfdnw8whjywcdh70paak45852s21pxk4ddq2cl75j34qhy")))

(define-public crate-amxml-0.5 (crate (name "amxml") (vers "0.5.1") (hash "108xlhqvyfn5vwm5v4q4smzi6lhavgmz7dq30mb920vni22hf627")))

(define-public crate-amxml-0.5 (crate (name "amxml") (vers "0.5.2") (hash "07y6cw3jy6dkj0is1gh6rpxwnpffagk31fya5lvq2qzd5548jqdj")))

(define-public crate-amxml-0.5 (crate (name "amxml") (vers "0.5.3") (hash "0s5h59dzxm5600bwnbg76v4n4x0n5mhq1wq6jr2g107ljlksbxpz")))

