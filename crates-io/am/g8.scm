(define-module (crates-io am g8) #:use-module (crates-io))

(define-public crate-amg88-0.3 (crate (name "amg88") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "=0.4.0-alpha.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15") (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "13qq96nb1hd7cinmwy4cphq000nb9iaq99qpz794b8wn054rn4qy") (features (quote (("std" "ndarray/std" "num_enum/std") ("default" "std"))))))

(define-public crate-amg88-0.4 (crate (name "amg88") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15") (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "1j66al2c5dm88bf42b89pvnl2iil0hvrzljl4swf59jql4fwhky1") (features (quote (("std" "ndarray/std" "num_enum/std") ("default" "std"))))))

(define-public crate-amg88-0.4 (crate (name "amg88") (vers "0.4.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15") (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "1pdgshfbilyl3ccf04cqyvmag87wpcygxiphghna11lyhh2abzhn") (features (quote (("std" "ndarray/std" "num_enum/std") ("default" "std"))))))

