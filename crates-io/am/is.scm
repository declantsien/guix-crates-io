(define-module (crates-io am is) #:use-module (crates-io))

(define-public crate-amisgitpm-0.0.1 (crate (name "amisgitpm") (vers "0.0.1") (deps (list (crate-dep (name "git2") (req "^0.15") (default-features #t) (kind 0)))) (hash "0yn8qlqscww9cby50l5nanppx38qjv6rczi9sm1np458gr7l0gkr")))

