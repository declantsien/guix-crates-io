(define-module (crates-io am -p) #:use-module (crates-io))

(define-public crate-am-parse-0.99 (crate (name "am-parse") (vers "0.99.0") (hash "06g7l2dwk05v0d2y33bqhn5add0q4sbwhbr740qzky7w6a9nc9ra")))

(define-public crate-am-parse-0.99 (crate (name "am-parse") (vers "0.99.1") (hash "0j9kcl0wbzb2cjcplmhk2w0ndq88z5whc6bsghagc8q1ylv0cjs3")))

(define-public crate-am-parse-0.99 (crate (name "am-parse") (vers "0.99.2") (hash "1srjr08pvb7dkdwndpjqjm46wgjlsa9xjby1rkpgjs7b34q472w2")))

(define-public crate-am-parse-0.99 (crate (name "am-parse") (vers "0.99.3") (hash "0j7gdqansvhbqd6m8ywgnp0xc3xz0h1cy2ykw01hwc74j1m61pax")))

(define-public crate-am-parse-0.99 (crate (name "am-parse") (vers "0.99.4") (hash "0xw42jyxpkkljqxcwlfvnq6xaigmm12c22gl00pkz50xjcca8jk4")))

(define-public crate-am-parse-0.99 (crate (name "am-parse") (vers "0.99.6") (hash "0shajjqygwfi98y992jhjbyh4q4zlr22x4gd7zbzmv5zdlh4kl7y")))

