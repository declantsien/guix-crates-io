(define-module (crates-io am id) #:use-module (crates-io))

(define-public crate-amid-0.1 (crate (name "amid") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "14rkr253ir2iv3fy3r0shkkwzsrw15vl9n85kh3fzz5y7vvp5xhk")))

(define-public crate-amid-0.1 (crate (name "amid") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0gl17r521qb60ghrcpx5w0k2xcpqh98giw9dgdsiw1wghdywc0zw")))

(define-public crate-amid-0.1 (crate (name "amid") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "085amvav8i45jlfvb13p9vqpgwhv9rrsp2lhz27qhl91pdyh1rm7")))

