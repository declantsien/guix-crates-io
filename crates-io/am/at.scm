(define-module (crates-io am at) #:use-module (crates-io))

(define-public crate-amath-0.1 (crate (name "amath") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.15.2") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1wv8xgcqqvjisyz6yga1d9hj9wdd4dd04i28v5sfvxbykm99l35x") (yanked #t)))

