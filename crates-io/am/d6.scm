(define-module (crates-io am d6) #:use-module (crates-io))

(define-public crate-amd64_timer-1 (crate (name "amd64_timer") (vers "1.0.0") (hash "14s9fp56vv3ywndk3gs11fc444n81s6nbqfxiz01kbcdjbx6b67p")))

(define-public crate-amd64_timer-1 (crate (name "amd64_timer") (vers "1.2.0") (hash "12gy335w7xy8lq5gdr6km6cxy4p64g55cggv7f5dbsmyqi8zjmbd")))

(define-public crate-amd64_timer-1 (crate (name "amd64_timer") (vers "1.2.1") (hash "1b3hjxjjdhzjfqgkhd26g9nshwvsjbx63s9cq03yxlh8cws3dc4f")))

(define-public crate-amd64_timer-1 (crate (name "amd64_timer") (vers "1.3.0") (deps (list (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "0lcbk9a0kjl5fmpbcs8f2l89n1x6fj7q3k581pv2vv2q8pclwpnf") (features (quote (("std") ("default") ("OLD_ASM"))))))

