(define-module (crates-io am pr) #:use-module (crates-io))

(define-public crate-ampr-api-0.1 (crate (name "ampr-api") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mcw527kdba6pzmfkzcmfvhcmh8ryhs1q82rmxx3lh1yn37f904g")))

