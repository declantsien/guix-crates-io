(define-module (crates-io am as) #:use-module (crates-io))

(define-public crate-amasdawdwadawdawdawd-0.1 (crate (name "amasdawdwadawdawdawd") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (default-features #t) (kind 0)))) (hash "1fcd09fxz0bqlhkqylzji58j8zd3q0mv4dvjvz69p7k07gpp02nv")))

(define-public crate-amasdawdwadawdawdawdb-0.1 (crate (name "amasdawdwadawdawdawdb") (vers "0.1.0") (deps (list (crate-dep (name "amasdawdwadawdawdawd") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (default-features #t) (kind 0)))) (hash "0ai5nxmgqbxjcxp12c3xgg7pnix9mkisax1mi0ibf3idmywljl12")))

(define-public crate-amass-0.1 (crate (name "amass") (vers "0.1.0") (deps (list (crate-dep (name "amass-macro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "telety") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fclk656lhv43sf8vxm4xg9mfdigsvs1bd0gpikyqfrp8qpwqq0b")))

(define-public crate-amass-macro-0.1 (crate (name "amass-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("visit" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "telety") (req "^0.2") (default-features #t) (kind 0)))) (hash "103i65zggr74fyxr4ggda28dn3wwn4hqd5g1sly05zpx3x7d0z12")))

