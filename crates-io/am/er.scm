(define-module (crates-io am er) #:use-module (crates-io))

(define-public crate-americas_capitals_game-1 (crate (name "americas_capitals_game") (vers "1.0.0") (deps (list (crate-dep (name "noiserand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1dxijssz5zihirbgq76yjnz28sxxzm43yv9cjwidw4hgw35cx55q")))

(define-public crate-ameritrade-0.0.0 (crate (name "ameritrade") (vers "0.0.0") (hash "18wfsjr23g9ar2ra1nis4z0lj4ha6w2yqgvy44pzm4k6wzmpks8m")))

