(define-module (crates-io am ou) #:use-module (crates-io))

(define-public crate-amount-2 (crate (name "amount") (vers "2.3.1") (hash "12wsc5pc2g56vzwfspwxyannllxziq9ql70hvz6x1ssdim9mrw2b")))

(define-public crate-amount_conversion-0.1 (crate (name "amount_conversion") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 2)))) (hash "0f4rj6cc2zd413n9w2pcps5azkf32wnm32v25v74ylqdj8pz499y") (rust-version "1.65")))

(define-public crate-amount_conversion-0.1 (crate (name "amount_conversion") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 2)))) (hash "04fkqjqln0hz0mxp7mspiqbq3irajvq1401z1wvl4dilbjwh25dx") (rust-version "1.65")))

