(define-module (crates-io am t2) #:use-module (crates-io))

(define-public crate-amt22-0.1 (crate (name "amt22") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0jqp4vi3bn7bjkrygs704pkqgsk62664ac9l5v07nj8cdmpg7whw") (features (quote (("std") ("default"))))))

