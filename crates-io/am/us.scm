(define-module (crates-io am us) #:use-module (crates-io))

(define-public crate-amuse-0.0.1 (crate (name "amuse") (vers "0.0.1-dev") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "midir") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "pitch_calc") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.11") (default-features #t) (kind 0)))) (hash "098j0vx59jh4wlw3mc7191b0ni68xig4hqxrvpzizw3ndx34h9bh")))

