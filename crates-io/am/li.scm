(define-module (crates-io am li) #:use-module (crates-io))

(define-public crate-amlich-0.0.1 (crate (name "amlich") (vers "0.0.1") (deps (list (crate-dep (name "rustbox") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.6.2") (features (quote ("rustbox"))) (kind 0)))) (hash "1cpiahlp6qxc13gwa944ykq8ldrcyl3vhka8qffyk5984qrwp00q")))

