(define-module (crates-io am l_) #:use-module (crates-io))

(define-public crate-aml_parser-0.2 (crate (name "aml_parser") (vers "0.2.0") (hash "0xrzwg0nz9zcww9fyvjlc7cs3r04jz35x9ksxi0f8kicb6xgsb5h") (yanked #t)))

(define-public crate-aml_parser-0.3 (crate (name "aml_parser") (vers "0.3.0") (deps (list (crate-dep (name "bit_field") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x2r6mlil87zfklbjmh06wy445fsg8m617hsa7al3hx0zj2g8m3q") (features (quote (("debug_parser_verbose") ("debug_parser")))) (yanked #t)))

