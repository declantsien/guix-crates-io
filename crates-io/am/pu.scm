(define-module (crates-io am pu) #:use-module (crates-io))

(define-public crate-Amputate-0.1 (crate (name "Amputate") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "mem_storage") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "windows-sys-win32-system") (req "^0.22.6") (default-features #t) (kind 0)))) (hash "16bmgn1v3apimaj2069sx4kxhlvz8addbvv5bqwhzpyyd71j25hx")))

