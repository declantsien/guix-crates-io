(define-module (crates-io am ts) #:use-module (crates-io))

(define-public crate-amts-prelude-0.1 (crate (name "amts-prelude") (vers "0.1.0") (deps (list (crate-dep (name "bson") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02za8awyqvb4qd0vqg3d49l7bvc5xalkbasr5vqdancnlr9kzyb2")))

(define-public crate-amts-prelude-0.1 (crate (name "amts-prelude") (vers "0.1.1") (deps (list (crate-dep (name "bson") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "083cw8zqr175437285igmhi78kbgyh31bi551nsizx7iwkq66v51")))

(define-public crate-amts-prelude-0.1 (crate (name "amts-prelude") (vers "0.1.2") (deps (list (crate-dep (name "bson") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "nats") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12s58rpi8xsh490gp52gcbfqlw0yhw6l2av01a6a1rj48m41hik2")))

(define-public crate-amts-prelude-0.2 (crate (name "amts-prelude") (vers "0.2.2") (deps (list (crate-dep (name "nats") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vxikniirv29vn7v6kyiwk5cww2zh0yvcnys4fzz5fydwl3hd6hj")))

(define-public crate-amts-prelude-0.2 (crate (name "amts-prelude") (vers "0.2.3") (deps (list (crate-dep (name "nats") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "195pq289kpbm9h8fhz0427pmh3mgk52ci9pyp32lhb0zq6zlb4wm")))

(define-public crate-amts-prelude-0.2 (crate (name "amts-prelude") (vers "0.2.4") (deps (list (crate-dep (name "nats") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0w17ng0vn4gnd2kgqh8650jipyidwy153bhbpia9c0iwl8xk6blb")))

