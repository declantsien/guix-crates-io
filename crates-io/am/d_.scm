(define-module (crates-io am d_) #:use-module (crates-io))

(define-public crate-amd_sys-0.1 (crate (name "amd_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0zdv8amh9nbk3s3aijazvl659ax3sh7bii495fdfag7kfbwamw83") (features (quote (("dynamic")))) (links "amd")))

