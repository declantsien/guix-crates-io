(define-module (crates-io am #{23}#) #:use-module (crates-io))

(define-public crate-am2320-0.1 (crate (name "am2320") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "0yrdf5y35h4byggvjg4fcmh0nr0hhss1ss16jckjkj8lifc9q6s9")))

(define-public crate-am2320-0.1 (crate (name "am2320") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m06p4yyi8ra0im290idqhy1grhrzvcrv3v6pwrjs185jkxzq5qx")))

(define-public crate-am2320-0.2 (crate (name "am2320") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1izr9agnkdy6r3c92sdvcp1x7il733cxd7lcqzzymmsp7ww763ww")))

