(define-module (crates-io am en) #:use-module (crates-io))

(define-public crate-amen-0.0.1 (crate (name "amen") (vers "0.0.1") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "trie-rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "0z7zdngzhhi1q9m1xhk3wynd6lxzc7gidxq3ssy5vc4gjqqwn63n")))

(define-public crate-amen-0.0.2 (crate (name "amen") (vers "0.0.2") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "trie-rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "05da61j7n1c6sj1jxn8mdjfabvdn69x4frmdbsfyg65ybdhwk6z0")))

(define-public crate-amen-0.0.3 (crate (name "amen") (vers "0.0.3") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "trie-rs") (req "^0.1") (default-features #t) (kind 0)))) (hash "05xhfg6cnczzz1h19kxy2n57l9qdjkx43szcwfdvr51h4q41c9vp")))

(define-public crate-amend-0.0.0 (crate (name "amend") (vers "0.0.0-alpha") (hash "1n45vs246m54paxkwfq6b78vwl5dn3bjljchqiqkgxf2hlh6mg74")))

(define-public crate-ament_rs-0.2 (crate (name "ament_rs") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "17iip1rz4553pq7hyyc4fi99js4iay8z7vvmwaaa8yb0m8r0p690")))

(define-public crate-ament_rs-0.2 (crate (name "ament_rs") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0jbfrwfq1sxn16ymkyzmlxazh6k86iv0rqb38pa205kvdfadl0dr")))

