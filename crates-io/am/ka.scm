(define-module (crates-io am ka) #:use-module (crates-io))

(define-public crate-amka-0.1 (crate (name "amka") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "luhn") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1mm1vhdn47w1layhrqbxl7hzp48daw96hvfkr1b4g7a47s1dp0vn")))

(define-public crate-amka-0.1 (crate (name "amka") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "luhn") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0mairbkpgr1q9y8pnfjr5x312ziqwyl8dl2632aipgswax28g0xv")))

(define-public crate-amka-1 (crate (name "amka") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "luhn") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "10zxw4gy9hq26m5g36vrwxp25abjjz23rxdj2z6rfjawc92ncd4g")))

(define-public crate-amka-1 (crate (name "amka") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "luhn") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "04v4lxw41q6d1s6wiyx2kd7bg0apxda083vyfjgwh7nmvmxh1lp6")))

(define-public crate-amka-1 (crate (name "amka") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "luhn") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1m3dm0fg23f266i9989278pds4wrybrq9j2nnisahl4k7vk10k0r")))

