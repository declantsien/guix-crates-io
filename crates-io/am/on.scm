(define-module (crates-io am on) #:use-module (crates-io))

(define-public crate-amon-0.1 (crate (name "amon") (vers "0.1.0") (hash "1s8ak97k5nhhxsnh9f9vbks04xnks58l088l9jgzdfgxwv4lqxk4")))

(define-public crate-among_us-0.1 (crate (name "among_us") (vers "0.1.0") (hash "0wrlh78aahvdvds9sly68bxlhxslgd25dshr1ig425f2b9pn3jxh")))

(define-public crate-among_us-0.2 (crate (name "among_us") (vers "0.2.0") (hash "0zrvwyy7i0va15py2vf7bh09fb732nixlz2hcyafwfgnvpldyxn6")))

(define-public crate-amongify-0.1 (crate (name "amongify") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0ljrvrs83vpzapl5b4r4b2hpvvsk65vlj702ai7f0hwx13b92vkk")))

(define-public crate-amongos-0.11 (crate (name "amongos") (vers "0.11.0") (deps (list (crate-dep (name "bootloader") (req "^0.9.23") (features (quote ("map_physical_memory"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "linked_list_allocator") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pc-keyboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pic8259") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "uart_16550") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "volatile") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "x86_64") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "01d69aay8hwsk9s7cdmz5pyvywwwcjh131ljxic470j4imdvmjp2")))

(define-public crate-amongrust-0.1 (crate (name "amongrust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1sjkav9hj9v7ilvw2lxyk7s97m6f94zz2qzvvzr42d5hda8a6lwg")))

(define-public crate-amongrust-0.1 (crate (name "amongrust") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1jdhph4x9d0sks33avm98nzyv4d426sij9pnmngjk8g4d8j8s1ps")))

(define-public crate-amongrust-0.1 (crate (name "amongrust") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1xv8b909xgcmhx4xr5zrf6nw9i1y894f7chzcxqdk4vm0ckzi97m")))

(define-public crate-amongrust-0.1 (crate (name "amongrust") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.0.27") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0z1r0r0w17vhkfsn2g5my06jabryma4jqzpnijdj2gd50f5f6czr")))

