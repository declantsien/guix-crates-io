(define-module (crates-io am tk) #:use-module (crates-io))

(define-public crate-amtk-0.1 (crate (name "amtk") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "block-modes") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "10141xa8xi00s5qbqp9nrxw2dkk9y9wmhj9rxlrds51plna0mdwf")))

