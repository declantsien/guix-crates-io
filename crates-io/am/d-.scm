(define-module (crates-io am d-) #:use-module (crates-io))

(define-public crate-amd-comgr-1 (crate (name "amd-comgr") (vers "1.0.0") (deps (list (crate-dep (name "amd-comgr-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1qg19fyixdijpnpawc61n7hcmz6r37m5a2ahh7za1r71fdfjpja3")))

(define-public crate-amd-comgr-sys-1 (crate (name "amd-comgr-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "02x2apqshv4yxx8j35b1igkr8hafckvy7wrjxihhs6nd0z0nlzl6")))

(define-public crate-amd-ext-d3d-0.1 (crate (name "amd-ext-d3d") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (default-features #t) (kind 0)))) (hash "1i5wz3zmifvrb7qk3rc0l95m2nrcrchwhxsfx1zma10v11wrdvwb")))

(define-public crate-amd-ext-d3d-0.1 (crate (name "amd-ext-d3d") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (default-features #t) (kind 0)))) (hash "0afn8slmbd3g2sjvlsjnb50mj54lx2768f41zcycmiyq3ifa0wm0")))

(define-public crate-amd-ext-d3d-0.2 (crate (name "amd-ext-d3d") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51") (features (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (default-features #t) (kind 0)) (crate-dep (name "windows-core") (req "^0.51") (default-features #t) (kind 0)))) (hash "11g57dgfh5vwfz2m9qj7m6hm15lglk5jirh6syavnqppk7v5fs4k")))

(define-public crate-amd-ext-d3d-0.2 (crate (name "amd-ext-d3d") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "windows") (req ">=0.51, <=0.52") (features (quote ("implement" "Win32_Foundation" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (default-features #t) (kind 0)) (crate-dep (name "windows-core") (req ">=0.51, <=0.52") (default-features #t) (kind 0)))) (hash "0fcpaqrlvb2zf71hwdavvqrj6whk5scsvxj95v1gi49h782ab2ad")))

