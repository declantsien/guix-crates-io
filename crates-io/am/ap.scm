(define-module (crates-io am ap) #:use-module (crates-io))

(define-public crate-amap-0.1 (crate (name "amap") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)))) (hash "1v92hz07cyb34fdwc8h6pm641wlfl6xhbyksyawk9wf579pahl77")))

(define-public crate-amap-0.1 (crate (name "amap") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09h0g3pwg7a2y9a21024xry4q58863hp7qf6f7xacaih7a071g4i")))

(define-public crate-amap-0.1 (crate (name "amap") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0v43n5p9fy63n5xlcdwhn3dpf7yqspa1n1f9x6bzs1zkrzn7mzih")))

