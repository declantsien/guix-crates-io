(define-module (crates-io he ka) #:use-module (crates-io))

(define-public crate-hekate-0.0.0 (crate (name "hekate") (vers "0.0.0") (deps (list (crate-dep (name "hekate-core") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0nn8fj4mi4lmc6nl2gzl3xwcnhaabb0g6fif6hr83094a25avj5l")))

(define-public crate-hekate-core-0.0.0 (crate (name "hekate-core") (vers "0.0.0") (hash "1y710zbx65xmbbmzvwa55wjrpa7w09pipz5r2lj36p50ybdg233i")))

