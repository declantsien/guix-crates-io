(define-module (crates-io he ks) #:use-module (crates-io))

(define-public crate-heks-0.0.1 (crate (name "heks") (vers "0.0.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)))) (hash "0ylmwjswahgh9q3fl1699cmddjsambhmzg8k5ammkc5c7bvdzp2m")))

(define-public crate-heks-0.0.2 (crate (name "heks") (vers "0.0.2") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "nom") (req "^6.1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0.0") (default-features #t) (kind 0)))) (hash "0fwdhg1pc950sji4cnxi0aq7k8rrbvkxgmkbg9bmdm8mkdps2b4r")))

