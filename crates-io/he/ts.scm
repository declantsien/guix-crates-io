(define-module (crates-io he ts) #:use-module (crates-io))

(define-public crate-hetseq-0.1 (crate (name "hetseq") (vers "0.1.0") (hash "065cqyn96cgkj6m1jk03ly2ycg2n9va6cai1p8w8r12d1yf48r12") (features (quote (("nightly"))))))

(define-public crate-hetseq-0.1 (crate (name "hetseq") (vers "0.1.1") (hash "0w1l7iv6qwc260j60c0z2x661z9wdicbh9p8axy0sz5fj91lyjbz") (features (quote (("nightly"))))))

(define-public crate-hetseq-0.1 (crate (name "hetseq") (vers "0.1.2") (hash "1kgk4rsv3viisy121lxvzv62vp8m2gxfr67vzfmil1yvfli9nyw6") (features (quote (("nightly"))))))

(define-public crate-hetseq-0.1 (crate (name "hetseq") (vers "0.1.3") (deps (list (crate-dep (name "rayon") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0ixqcgirv3xj7awblkzbnlxbigb9jfpbbipsa8m9nxnkf8svjhpc") (features (quote (("par_iter" "rayon") ("nightly"))))))

(define-public crate-hetseq-0.1 (crate (name "hetseq") (vers "0.1.4") (deps (list (crate-dep (name "rayon") (req ">= 0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shred") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1hzqhfnkrmflmrw1c1fixmd4ffw1b8z148g1g4z03sz2si3mmgaq") (features (quote (("system_data" "shred") ("par_iter" "rayon") ("nightly"))))))

(define-public crate-hetseq-0.1 (crate (name "hetseq") (vers "0.1.5") (deps (list (crate-dep (name "rayon") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shred") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0qshch1ryr5nas36vcyhk9g9yqcr0dv4q8fdnkpyk1ga0fg3pqfv") (features (quote (("system_data" "shred") ("par_iter" "rayon") ("nightly"))))))

(define-public crate-hetseq-0.1 (crate (name "hetseq") (vers "0.1.6") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shred") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1dddl31yb72qvxfyz0mff4s8rvmz87knsyn83844ri8v5i7sxwsc") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-hetseq-0.2 (crate (name "hetseq") (vers "0.2.0") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shred") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1bm6vnwsnyb6fi99ykqi7nasfm3dkgwbgzpbqjcwjpf4k2kyi337") (features (quote (("nightly"))))))

