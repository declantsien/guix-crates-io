(define-module (crates-io he rc) #:use-module (crates-io))

(define-public crate-herco-0.1 (crate (name "herco") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aph8nn8c087cwibpbj5ckc6v4dc6jh3jki7arg5ms14xxw59xqf") (yanked #t)))

(define-public crate-herculanocm_library-0.1 (crate (name "herculanocm_library") (vers "0.1.0") (hash "0pwhkfmannyraig6qcmfnkz8xrhnyx6sydlz5w79l7m7k4h1g7fp")))

(define-public crate-hercules-0.1 (crate (name "hercules") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "smolprng") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "sprs") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1jh4jy25k72wxvp0mawkyf1zdqqvh56kqmic3121hyysy6wc4fw2")))

(define-public crate-hercules-0.2 (crate (name "hercules") (vers "0.2.0") (deps (list (crate-dep (name "clarabel") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl-static"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.2") (features (quote ("extension-module" "abi3-py37"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "smolprng") (req "^0.1.6") (features (quote ("no_std"))) (default-features #t) (kind 0)) (crate-dep (name "sprs") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "12f3kzd4v0xii54r1mc649z10rsz8wf9m1cxww31jpb59yknc1s7")))

