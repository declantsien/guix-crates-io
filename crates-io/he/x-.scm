(define-module (crates-io he x-) #:use-module (crates-io))

(define-public crate-hex-buffer-serde-0.1 (crate (name "hex-buffer-serde") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "ed25519-dalek") (req "= 1.0.0-pre.0") (features (quote ("serde" "sha2"))) (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0hw5d32290ixpabwcwxy45ah01yhp7418id8pc2hnbf5y3rqgfaz")))

(define-public crate-hex-buffer-serde-0.1 (crate (name "hex-buffer-serde") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "ed25519-dalek") (req "= 1.0.0-pre.0") (features (quote ("serde" "sha2"))) (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ag1m5nl9f8mc7bp72s9ky9c8q0y0c68ks91jw6n7qvqnhmrb7hy")))

(define-public crate-hex-buffer-serde-0.2 (crate (name "hex-buffer-serde") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "ed25519") (req "= 1.0.0-pre.1") (features (quote ("serde"))) (default-features #t) (kind 2) (package "ed25519-dalek")) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0x4cnc11mr46bgnnvrca7in0qf858a44gqrkw5v1ss24a86n8kdv")))

(define-public crate-hex-buffer-serde-0.2 (crate (name "hex-buffer-serde") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "ed25519") (req "= 1.0.0-pre.3") (features (quote ("serde"))) (default-features #t) (kind 2) (package "ed25519-dalek")) (crate-dep (name "hex") (req "^0.4") (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0azq05ck3mk5yd8h0r88pxcvfz3hv905gil2805ha9p8f51vprgf") (features (quote (("std" "serde/std" "hex/std") ("default" "std"))))))

(define-public crate-hex-buffer-serde-0.2 (crate (name "hex-buffer-serde") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "ed25519") (req "^1.0.1") (features (quote ("serde"))) (default-features #t) (kind 2) (package "ed25519-dalek")) (crate-dep (name "hex") (req "^0.4") (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1f1smkw0qhs8pgp3ivq30nif8gl3yq4lcqp030x5mrk4zxw9a3ii") (features (quote (("std" "serde/std" "hex/std") ("default" "std"))))))

(define-public crate-hex-buffer-serde-0.3 (crate (name "hex-buffer-serde") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "ed25519") (req "^1.0.1") (features (quote ("serde"))) (default-features #t) (kind 2) (package "ed25519-dalek")) (crate-dep (name "hex") (req "^0.4.3") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0fc04acgls0dwwfybjlzhhlh982s0j43azvjji28nrk0q4921x88") (features (quote (("default" "alloc") ("const_len") ("alloc" "hex/alloc"))))))

(define-public crate-hex-buffer-serde-0.4 (crate (name "hex-buffer-serde") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "ed25519-compact") (req "^2.0.2") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1l560sb7gjzbfv05j5bigf25y6qx24fdawq6yic4mkq1lr2lds67") (features (quote (("default" "alloc") ("const_len") ("alloc" "hex/alloc" "serde/alloc")))) (rust-version "1.57")))

(define-public crate-hex-conservative-0.0.1 (crate (name "hex-conservative") (vers "0.0.1") (hash "1kfsjm602nmp89yj5g70z5hnsdszg5974l8w03slvyi45b5am9wl")))

(define-public crate-hex-conservative-0.1 (crate (name "hex-conservative") (vers "0.1.0") (deps (list (crate-dep (name "core2") (req "^0.3.2") (optional #t) (kind 0)))) (hash "0gsqd97hiifvpdwgpnbp3avwghb48yqg8n4mp0njwnsr5ffcv79b") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.1 (crate (name "hex-conservative") (vers "0.1.1") (deps (list (crate-dep (name "core2") (req "^0.3.2") (optional #t) (kind 0)))) (hash "1qji1wg2m41vahpxzc9gfmv32kgr8lanczhy3kcbdk2qyhx49v9h") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.2 (crate (name "hex-conservative") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "core2") (req "^0.3.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "11n9q9qwsjsiy9c4247v9832jkiabqdcfhgdbk9pxqsiyhxjgap1") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.1 (crate (name "hex-conservative") (vers "0.1.2") (deps (list (crate-dep (name "core2") (req "^0.3.2") (optional #t) (kind 0)))) (hash "085xvjs4fzq7kdf67s298f9l10byi5n0098074clhjrm08hbjai1") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-conservative-0.2 (crate (name "hex-conservative") (vers "0.2.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (kind 0)) (crate-dep (name "core2") (req "^0.3.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1kg6xb2n59zpid87kdgqvc5mks68qh9gdnq8m1jp0n9wrrrb04sk") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-hex-coordinates-0.1 (crate (name "hex-coordinates") (vers "0.1.0") (hash "1w5bdggn2yb17mf9dbkhr2a35xjqyx0agynfjqca21125v1r227v")))

(define-public crate-hex-coordinates-0.1 (crate (name "hex-coordinates") (vers "0.1.1") (hash "1hj4snmbmgq1sxn1a1ryq5sdrjsxhx2zpp7qsdjkhw8nnqja1ayj")))

(define-public crate-hex-coordinates-0.1 (crate (name "hex-coordinates") (vers "0.1.2") (hash "0qf0bbhjqmy6brp0w3pvbmf03wpznapycj443pyq8bcqkfddakfx")))

(define-public crate-hex-coordinates-0.1 (crate (name "hex-coordinates") (vers "0.1.3") (hash "1hg80cfpkabp3p04a4sf7s7y1116b3v9234vyqsmv7n6ds231ic3")))

(define-public crate-hex-ct-0.0.0 (crate (name "hex-ct") (vers "0.0.0") (hash "0ik79b41ggbjxh5q85h44gwv689i8nz3in49690rlnw3y5nwf9zh")))

(define-public crate-hex-display-0.1 (crate (name "hex-display") (vers "0.1.0") (hash "0mqqmmj1nhc7ipx0schd5529cmkfsq37z1679lyyd6m49bnr2q4c")))

(define-public crate-hex-display-0.1 (crate (name "hex-display") (vers "0.1.1") (hash "0335ng09ym75b10x7m657ii31rccv203m2iz59d5bcj8i799f3q1") (features (quote (("std"))))))

(define-public crate-hex-display-0.2 (crate (name "hex-display") (vers "0.2.0") (hash "0d0xy5v5s9556w2y1876cghzyv3a6n7idd81afsip6krn73xbfwq") (features (quote (("std")))) (yanked #t)))

(define-public crate-hex-display-0.3 (crate (name "hex-display") (vers "0.3.0") (hash "0mhp58lw60sv0a4l6c7mhxwgzld8ajp62wp7vsgmsz2h6jkdclwb") (features (quote (("std"))))))

(define-public crate-hex-game-0.1 (crate (name "hex-game") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4.13") (default-features #t) (kind 0)))) (hash "0hry6v2c0nfxzv87lvyg9zcyniqrmqwhd8ypafgyj61g52ddjspg")))

(define-public crate-hex-literal-0.1 (crate (name "hex-literal") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ys7b0rvzprxxfwkic8s9plrhr4yvv3cvwp5mbqladxb43snwm5x")))

(define-public crate-hex-literal-0.1 (crate (name "hex-literal") (vers "0.1.1") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "159day3l4lfk0jphza5pvf4a7gfarymfrvaf98i1m9yq3ghg19ad")))

(define-public crate-hex-literal-0.1 (crate (name "hex-literal") (vers "0.1.2") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "00zkpfsx0kkag8syjrgqmwwlvgnz01qy6q4qwfh63rk5zcq5q3mf")))

(define-public crate-hex-literal-0.1 (crate (name "hex-literal") (vers "0.1.3") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yinds8bw7g60mwij46wv9mlg59rm34mjjqf4a3nqrm6nkl5qi97")))

(define-public crate-hex-literal-0.1 (crate (name "hex-literal") (vers "0.1.4") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ffnn5g9q5xhdmzj2ic5hk9y18kyqflbmqcssqcya9gixs5r5hnx")))

(define-public crate-hex-literal-0.2 (crate (name "hex-literal") (vers "0.2.0") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ni2nv3di0jpih2xnmlnr6s96zypkdr8xrw2cvk4f8fx5wb6inn3")))

(define-public crate-hex-literal-0.2 (crate (name "hex-literal") (vers "0.2.1") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1q36f0qq31ggh4ipcwb7a5g6jmci2010vn2v3qpaz4csxhhf47cn")))

(define-public crate-hex-literal-0.3 (crate (name "hex-literal") (vers "0.3.0") (hash "1hd0gwnnymsi88vgc32chinzii756ahkjphqy6gxif513i0clf6w")))

(define-public crate-hex-literal-0.3 (crate (name "hex-literal") (vers "0.3.1") (hash "1j0ri05s2r5870v4mj13j019x069w6zkdccjhgblbi8vxwszdwas")))

(define-public crate-hex-literal-0.3 (crate (name "hex-literal") (vers "0.3.2") (hash "0f71b21g4d6a5rkl7p55by69k05vmdr89dk0nfxvsfyanqk5wl3n")))

(define-public crate-hex-literal-0.3 (crate (name "hex-literal") (vers "0.3.3") (hash "0nzljsyz9rwhh4vi0xs9ya4l5g0ka754wgpy97r1j3v42c75kr11")))

(define-public crate-hex-literal-0.3 (crate (name "hex-literal") (vers "0.3.4") (hash "1q54yvyy0zls9bdrx15hk6yj304npndy9v4crn1h1vd95sfv5gby")))

(define-public crate-hex-literal-0.2 (crate (name "hex-literal") (vers "0.2.2") (deps (list (crate-dep (name "hex-literal-impl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "04ba6fmk6q2mmzpl1wmfsaz3wyljcd0va8577wpmbx1wkccr61np")))

(define-public crate-hex-literal-0.4 (crate (name "hex-literal") (vers "0.4.0") (hash "13ijkrzbxy333kdqqdl67xffb3fy6gkvpygj3lcsg4lw8cz5pjsb") (rust-version "1.57")))

(define-public crate-hex-literal-0.4 (crate (name "hex-literal") (vers "0.4.1") (hash "0iny5inkixsdr41pm2vkqh3fl66752z5j5c0cdxw16yl9ryjdqkg") (rust-version "1.57")))

(define-public crate-hex-literal-impl-0.1 (crate (name "hex-literal-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ccn5ly8h7424jh0v5vvplmhp5nx7xnmd1a9sl0lv9giqyj6v9rf")))

(define-public crate-hex-literal-impl-0.1 (crate (name "hex-literal-impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "02hk47xfbrrjp5y8dkprzq28l9x5099vcvfi3gdzccpj2ijhnd0x")))

(define-public crate-hex-literal-impl-0.1 (crate (name "hex-literal-impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.4") (default-features #t) (kind 0)))) (hash "1nnxqhyn9l998ma04ip79bmpqv1as6003s03g26ynhrr471p022j")))

(define-public crate-hex-literal-impl-0.2 (crate (name "hex-literal-impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "04m6d1k57a9h3hhdgn0vq1hkfwjv9hfkw6q73bqn0my0qw45s286")))

(define-public crate-hex-literal-impl-0.2 (crate (name "hex-literal-impl") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0bgldhp5gdwwnikfdxigmz9b64qpgwbjqk6mfgv0pvig9s25qk4x")))

(define-public crate-hex-literal-impl-0.2 (crate (name "hex-literal-impl") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ilvbgqfxhw8pjk08xksik1rjclplixpn683ccbxwcgbk6apcgw5")))

(define-public crate-hex-literal-impl-0.2 (crate (name "hex-literal-impl") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "1a25nz3v1323qhy328qr0mdzz0iyhjfxdhvqgy8bcpraz318yi2r")))

(define-public crate-hex-magic-0.0.1 (crate (name "hex-magic") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits" "printing"))) (default-features #t) (kind 0)))) (hash "04lsrk2gvx2nb811469jqg3g6z0zz4b4wnzhw0pd2b9akwicw0wm")))

(define-public crate-hex-magic-0.0.2 (crate (name "hex-magic") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits" "printing"))) (default-features #t) (kind 0)))) (hash "0ip6sks33ilkkl173l9yn9sk2vzcmsqhvhc9mz52k2mrh25i5jr5")))

(define-public crate-hex-noalloc-0.3 (crate (name "hex-noalloc") (vers "0.3.2") (hash "1pq4ylwr5annp832m7wlmbcdk9d6b83sbz7agb332p0hin7mjcp3") (features (quote (("std") ("default" "std") ("benchmarks")))) (yanked #t)))

(define-public crate-hex-noalloc-0.3 (crate (name "hex-noalloc") (vers "0.3.2-post1") (hash "028xqdkvinx6c5xncnhzq9g21fg61kla93cbkf8y331px57cvh2q") (features (quote (("std") ("default" "std") ("benchmarks"))))))

(define-public crate-hex-rgb-0.1 (crate (name "hex-rgb") (vers "0.1.0") (deps (list (crate-dep (name "copypasta") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "17vkyhg9sl3hywf6cs6mykmhz1sycjcj080d8fg2jab0zzmkph4j")))

(define-public crate-hex-rgb-0.1 (crate (name "hex-rgb") (vers "0.1.1") (deps (list (crate-dep (name "copypasta") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0csc7m98qyg3ap9c5m7zhcrr6jdhw75jw9xzm4ln82blzmh76rbh")))

(define-public crate-hex-serde-0.1 (crate (name "hex-serde") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.27") (default-features #t) (kind 2)))) (hash "04kh3fzyhi72j92c9fr0zjcj87dl99g8mbjn33ycbi2jksydg1il")))

(define-public crate-hex-serde-util-0.1 (crate (name "hex-serde-util") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vw12s94wg7zx41cv7clqcbqa4wg2v8mkxnqbrg7h7kci09whpr4")))

(define-public crate-hex-simd-0.1 (crate (name "hex-simd") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.1.2") (features (quote ("hex"))) (default-features #t) (kind 0)))) (hash "15yi1f0x2425kws6zmp8na4hfc5cs284fc8ggg6nazwm82a01xab") (features (quote (("std" "alloc") ("detect" "simd-abstraction/std") ("default" "std" "detect") ("alloc")))) (yanked #t)))

(define-public crate-hex-simd-0.3 (crate (name "hex-simd") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.3.0") (features (quote ("hex"))) (default-features #t) (kind 0)))) (hash "1d2acm48yzsaqj6ny1v735lg74rw9kxaid8dr2bih03nn1s24psp") (features (quote (("std" "alloc") ("detect" "simd-abstraction/std") ("default" "std" "detect") ("alloc"))))))

(define-public crate-hex-simd-0.5 (crate (name "hex-simd") (vers "0.5.0") (deps (list (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "1rh4d7nfs6hswz45xx4dwnxvl2azxnawx55yn11d8kwq4by7a37y") (features (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc") ("detect" "simd-abstraction/std") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (rust-version "1.57")))

(define-public crate-hex-simd-0.5 (crate (name "hex-simd") (vers "0.5.1") (deps (list (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.7") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.31") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "11pmlcsl5cy8phl6vqna4xchhh96la9l6lc770alsr4ywf3zwxzn") (features (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc") ("detect" "simd-abstraction/std") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (rust-version "1.57")))

(define-public crate-hex-simd-0.6 (crate (name "hex-simd") (vers "0.6.0") (deps (list (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.7") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.31") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0zaf3079ryw5hv6s98ghrf7299b0bbsq6za5xnw3j74lhkw3bhw5") (features (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc") ("detect" "simd-abstraction/std") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (rust-version "1.57")))

(define-public crate-hex-simd-0.6 (crate (name "hex-simd") (vers "0.6.1") (deps (list (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.7") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.31") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "1z7gp4vi97wn7m4vksykf5c0z6iyv7izxky9d3yqzlbpvnxrqx50") (features (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc") ("detect" "simd-abstraction/std") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (yanked #t) (rust-version "1.57")))

(define-public crate-hex-simd-0.6 (crate (name "hex-simd") (vers "0.6.2") (deps (list (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.7") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.31") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "13bhhv4mk40il9wz93dd0bjkf2z2izaxhcw003iv0i8r7vymgxnc") (features (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc") ("detect" "simd-abstraction/std") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (rust-version "1.57")))

(define-public crate-hex-simd-0.7 (crate (name "hex-simd") (vers "0.7.0") (deps (list (crate-dep (name "faster-hex") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2.7") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "simd-abstraction") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.31") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "1dxzapzcvdr62nzjswv7qyav43h1hdjhrh563ry6fvgdml6s9ksx") (features (quote (("unstable" "simd-abstraction/unstable") ("std" "alloc" "simd-abstraction/std") ("detect" "simd-abstraction/detect") ("default" "std" "detect") ("alloc" "simd-abstraction/alloc")))) (rust-version "1.61")))

(define-public crate-hex-simd-0.8 (crate (name "hex-simd") (vers "0.8.0") (deps (list (crate-dep (name "getrandom") (req "^0.2.8") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "outref") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "vsimd") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.33") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "1viv1llx4s81fv8n2y93xyw1ifp9a5fjzwq54vy0xhiznnz8axhz") (features (quote (("unstable" "vsimd/unstable") ("std" "alloc" "vsimd/std") ("detect" "vsimd/detect") ("default" "std" "detect") ("alloc" "vsimd/alloc")))) (rust-version "1.63")))

(define-public crate-hex-slice-0.1 (crate (name "hex-slice") (vers "0.1.0") (hash "1q0daiba6brd19bjba51m65bnpda3213wj1vd7nkcl2jb1ak3br5")))

(define-public crate-hex-slice-0.1 (crate (name "hex-slice") (vers "0.1.1") (hash "0miylg2raz33i7c9xwh4n4yqcxz11sg8w53icpijr9z3rg5f65g1")))

(define-public crate-hex-slice-0.1 (crate (name "hex-slice") (vers "0.1.2") (deps (list (crate-dep (name "suppositions") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1qm7l0zi86fhm91pxdca9c95cb02zr5qlsxc391drsd7wbs6i443")))

(define-public crate-hex-slice-0.1 (crate (name "hex-slice") (vers "0.1.3") (deps (list (crate-dep (name "suppositions") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1w5g2lpwbv9hhva533qvs170b2zw3x2933zm5zfmly7m6mxid22k")))

(define-public crate-hex-slice-0.1 (crate (name "hex-slice") (vers "0.1.4") (deps (list (crate-dep (name "suppositions") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0k3ck81m9lz5wz3c7qqj3j3m5xa5pr599n41gbq58i91w04a74al")))

(define-public crate-hex-spiral-0.1 (crate (name "hex-spiral") (vers "0.1.0") (deps (list (crate-dep (name "crepe") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "0hsdj2z0xl96hl1x21hp6wik4qypg7jc8dvw3sis5ngi7zjybg0c")))

(define-public crate-hex-string-0.1 (crate (name "hex-string") (vers "0.1.0") (hash "02sgrgrbp693jv0v5iga7z47y6aj93cq0ia39finby9x17fw53l4")))

(define-public crate-hex-utilities-0.1 (crate (name "hex-utilities") (vers "0.1.0") (hash "150gvgz10mn7nmq9ymwxlyvvalw70kkxxcxkbqabva1759wzp141")))

(define-public crate-hex-utilities-0.1 (crate (name "hex-utilities") (vers "0.1.1") (hash "0qwsasv8369i5032f2dpcxvnd393lp703pfwblnw4sasjilciaqx")))

(define-public crate-hex-utilities-0.1 (crate (name "hex-utilities") (vers "0.1.2") (hash "1gk9pbnddl3p2dsxg96njaqqkax1c18l22qfbg8c3fz2yaczx5sr")))

(define-public crate-hex-utilities-0.1 (crate (name "hex-utilities") (vers "0.1.3") (hash "1iq7a9zzgxli4ap0pn09msx7agchhrqgd125lv99crgn515aysvg")))

(define-public crate-hex-utilities-0.1 (crate (name "hex-utilities") (vers "0.1.4") (hash "01v05fi3ikhk9cg78c0dwfmmsfcf8yh88gsv7wixgxazm8rdjk3k")))

(define-public crate-hex-utilities-0.1 (crate (name "hex-utilities") (vers "0.1.5") (deps (list (crate-dep (name "binary-utils") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "14p8108xykq874x8l7xwf6b4kwmvhc274wirky250b5i59apl5fj")))

(define-public crate-hex-utils-0.1 (crate (name "hex-utils") (vers "0.1.8") (hash "0y4ppxy5qcfg7hx9z86kwzxrg76vzw81l6svl4z684vhz410bxqn")))

(define-public crate-hex-utils-0.1 (crate (name "hex-utils") (vers "0.1.11") (hash "0ifcjzysrd3yknj6lz9xqwzpyp0m9g5m3hbr5sx437z42v0g7z0h")))

(define-public crate-hex-utils-0.1 (crate (name "hex-utils") (vers "0.1.12") (hash "0mf937wh71y8qvpx3za6ks7nfgrvs81265hc4lsdcxpgfzsxjcng")))

(define-public crate-hex-view-0.1 (crate (name "hex-view") (vers "0.1.0") (hash "0afqhicp7nl2dbmbf4dfswl907yjr3iz1wlnalbgs53aavgv5igv")))

(define-public crate-hex-view-0.1 (crate (name "hex-view") (vers "0.1.1") (hash "0zigbibnrvbd45lkf2yb11x49ilv0apq2vf5lz6cpydib6jdysmn")))

(define-public crate-hex-view-0.1 (crate (name "hex-view") (vers "0.1.2") (hash "0vpfqkn73gvj0zqlh0zcd1fyzhgf6l13s66gwss6g3qxmjcjbmbi")))

(define-public crate-hex-view-0.1 (crate (name "hex-view") (vers "0.1.3") (hash "0zx9bkzj1wkh6rh13sqcxa8mc11qgvrc17pyyf42ml2dzv4ickj9")))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "07kllj24h9qqvnsqx19rnd43p6bj46y617bc76rnhybd0wl4gi6f") (features (quote (("default"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "17qb7qhiykqkl446qf7c5xh7gbcdxwa6is14dcza8swml8i1z95v") (features (quote (("default"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1sw6w41w33c8fmkknyq5kk94yza6q331fszx6kv5n5m41a5sn7cn") (features (quote (("default"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jz6n8wnw77c4jh99h6n6r09y7nfwljj0jag49f5xbms4mwmakpg") (features (quote (("default"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0smiinlhxv4vjpm1yy0kskkq0d4zialx74s7d9jxx4imgd41xkwn") (features (quote (("default"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ida4j52zzf5281g50d92zvrw5fpwa0lmv5izszpnmga0abx9d92") (features (quote (("default"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.2.1") (deps (list (crate-dep (name "diesel") (req "^1.4") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0qhzc0y2nqbdzpb881wc4wvvk3nilrhyr5lsjblv8zhbpshfqbg4") (features (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.3.0") (deps (list (crate-dep (name "diesel") (req "^1.4") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1kdzwk7lrqyn122bcwfi2wn03q9n0i7w078ssb7iay01ld4canhk") (features (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.3.1") (deps (list (crate-dep (name "diesel") (req "^1.4") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vyr9b5df8kw3vj3k0jxj6q5b02n232s91ap49w90a5n17kf1pba") (features (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.3.2") (deps (list (crate-dep (name "diesel") (req "^1.4") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "06pdq4v15njmic0xrl45v03972l0bi8p4qi0qjpwcsrrlb183vi1") (features (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.4.0") (deps (list (crate-dep (name "diesel") (req "^2.0") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0b1wprmh0v20rfc6s43kz71cqp2mggdlqpvj270wdvx0n137lvrq") (features (quote (("default") ("db" "diesel"))))))

(define-public crate-hex-wrapper-1 (crate (name "hex-wrapper") (vers "1.4.1") (deps (list (crate-dep (name "diesel") (req "^2.0") (features (quote ("with-deprecated"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1whc22jm1gcwxxxgi56wwqcic83znqkg0vwqigjqzwppdlg82qwm") (features (quote (("default") ("db" "diesel"))))))

