(define-module (crates-io he xg) #:use-module (crates-io))

(define-public crate-hexgame-0.1 (crate (name "hexgame") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)))) (hash "1y4r8qnfv2zmaplvnrk3i11xczjcvq18y52021pgwd1cnbldfiqp")))

(define-public crate-hexgame_rs-0.1 (crate (name "hexgame_rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "158z83z71fi7lqhlyjsfr8bg2pvl4zkdq4r7vlh8131bsz17v9yx")))

(define-public crate-hexgame_rs-0.1 (crate (name "hexgame_rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "139ndhvi01dszfdqdi4idp90npv0bqjajq0xnhnjr4dim9cdw713")))

(define-public crate-hexgame_rs-0.1 (crate (name "hexgame_rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1b6rlnd6mlxw4ah087ll97vcnilk4f0g9rdqr6mxm17zk2srszd4")))

(define-public crate-hexgame_rs-0.1 (crate (name "hexgame_rs") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "16fizll72f2chd06kws5wm9jayrk7rdw4ndvr7rbwnhm557fha5g")))

(define-public crate-hexgrep-0.0.0 (crate (name "hexgrep") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "iocore") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "10wkhah0swkc4isfvpqdv20xhrfnbwwi3dyg7c1b0lh3g7c4xyzg")))

(define-public crate-hexgrid-0.1 (crate (name "hexgrid") (vers "0.1.0") (hash "0w9d5pyjmx8fyb3mbnffz0qnihg9hbb3sav424pp5sqrw05law18")))

(define-public crate-hexgrid-0.2 (crate (name "hexgrid") (vers "0.2.0") (hash "0z87w437hr2cry1ai0c2z0vxbrissr0nhhzl39mriyh1baw3pq2y")))

