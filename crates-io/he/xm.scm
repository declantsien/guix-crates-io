(define-module (crates-io he xm) #:use-module (crates-io))

(define-public crate-hexmap-0.1 (crate (name "hexmap") (vers "0.1.0") (hash "1x69ykkrqq77nr55g65dzfcnk33ls5fnwf7arvla840qppy9r4s5")))

(define-public crate-hexmap-0.2 (crate (name "hexmap") (vers "0.2.0") (deps (list (crate-dep (name "bevy") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ixdpj9vkqqwq9fh21zv5r275k93944vypvr3vbk8a53i3qly56g")))

(define-public crate-hexmap-0.2 (crate (name "hexmap") (vers "0.2.1") (hash "14a0q5qxw91si0a5bdgcynzrmj2j7sx6gk6n0bwblnsp3wr5pl9g")))

