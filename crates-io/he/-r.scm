(define-module (crates-io he -r) #:use-module (crates-io))

(define-public crate-he-ring-0.1 (crate (name "he-ring") (vers "0.1.1") (deps (list (crate-dep (name "feanor-math") (req "^1.5.2") (features (quote ("generic_tests"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "11il7f3cq6gr60p438fivzk6ws8r92rdcxnz3akbb2n5c1qwgkg2") (features (quote (("record_timings"))))))

(define-public crate-he-ring-0.2 (crate (name "he-ring") (vers "0.2.0") (deps (list (crate-dep (name "feanor-math") (req "^1.6.0") (features (quote ("generic_tests"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0fvrp46mf03wyxs3vbby8bamf38zy9li2f800gnrbdw3vacf60ds") (features (quote (("record_timings"))))))

(define-public crate-he-ring-0.2 (crate (name "he-ring") (vers "0.2.1") (deps (list (crate-dep (name "feanor-math") (req "^1.6.0") (features (quote ("generic_tests"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "09jgw0lyviqwwmvm2skm7smvjz2f9490mj82h58rxfn64qdkknw3") (features (quote (("record_timings"))))))

(define-public crate-he-rpc-0.0.0 (crate (name "he-rpc") (vers "0.0.0") (hash "1q51szqqhp5b8gr3qvkhf9ai660y3chpdvfr8vm7an7wbdj3238p")))

(define-public crate-he-rs-0.1 (crate (name "he-rs") (vers "0.1.0") (hash "0czk2vn5kkkqhxvp0ghhn38xw443wffdn2s40yjm77xg3rw4171g")))

