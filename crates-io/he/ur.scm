(define-module (crates-io he ur) #:use-module (crates-io))

(define-public crate-heuristic-graph-coloring-0.1 (crate (name "heuristic-graph-coloring") (vers "0.1.0") (deps (list (crate-dep (name "poloto") (req "^15") (default-features #t) (kind 2)))) (hash "1xrh00gjgwzgadwgm9psw3g8x3q26cz9mw6f4ysiwn559wknidj9")))

