(define-module (crates-io he ra) #:use-module (crates-io))

(define-public crate-hera-0.1 (crate (name "hera") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs_io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "tokei") (req "^8") (default-features #t) (kind 0)))) (hash "1sd2lvlzhgmxnzs6xcjmv577xl6f5j421ndsszpk3lnkvc7665sh")))

(define-public crate-hera-cmd-0.1 (crate (name "hera-cmd") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1z7d9l1cqpg56nrshlbq32m12r07jqrf1plra01bj05q9alrqlki")))

(define-public crate-heraclitus-0.0.1 (crate (name "heraclitus") (vers "0.0.1-placeholder") (hash "0an1pqg05r72nslpdws2hdhd2fgd3bpq3wrcl4zcsskkbrglvn0m")))

(define-public crate-heraclitus-compiler-0.1 (crate (name "heraclitus-compiler") (vers "0.1.0") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "06k6ifg3n1nmq43q65c9hp916yid3xr48khjcnymx28a6dpjffnb")))

(define-public crate-heraclitus-compiler-0.2 (crate (name "heraclitus-compiler") (vers "0.2.0") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "11r1bq51n63b42aqlvqcmwagynfzfk6dz43pav3viq50bdg60yza")))

(define-public crate-heraclitus-compiler-0.2 (crate (name "heraclitus-compiler") (vers "0.2.1") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0sj9cqcq2d3yvmzwzrwxcypyz23yk11z0rv3k500p5b1b90mqxqx")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.2") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1ivlhws9v539p63sn8rlkx89rg9q9hvdjxb7hdm99rzzivmiwhb3")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.3") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1x41m1cd2vlwzphy3jzyvyvnzi3hqvfs1mkyfpb7p6nq041ff1r7")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.4") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0zf4nwia6yp8b3ljjyckz2g8x06nqx2mfwgg996hz7ycnp4d1s95")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.5") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1wj712an3dzxzs76xxwb6iq4prv51na6j8z2j38bh8f70dbdszzk")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.6") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0ibzwd5n1rxznxhxrzyn7vykiiw5da0c0048c5ilgl5bn2220nqa")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.7") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0k9bmv04r3khsirxd6hfs313gki46ppycq37paln9sad7ishswhy")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.8") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0x54j0bc46cvkljynhd5cjahazx831a5vkb26j1pc2lyczl3nccx")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.2.9") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1k9z84apmljygapj3kjj2fhl31ndpf4828ggp02kgwfqsidxhvfn")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.3.0") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "03519hqncwnaw2ldfh3rmlwxdb8fa5zfd32625i1bhywvd5386zx")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.3.1") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1rj50gxsjjd1bsc95d5h31ag27vvgq17sg00km2jwmrzjpaxw2ml")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.3.2") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1gyjz8wy2ylr3z7g6cahlcbh5nkkxdl571nzk2sylkv6d8nd4jif")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.4.0") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "186frrql8mknxahg71cpwl5drcxv1iig2shhknmkc8fvaqyni05n")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.0") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1sy9vlf74lda2mh1jf5psa22468r0wsds4zw502387fmn1s2l6r2")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.1") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0k4pzqdbsk2vqpscac10kylvz53pjp4mm66c7lpmr7df6nsr2sw6")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.2") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1v0bndsnja4b7xsh8zydjs9k8l5b4v4j1b512bqm1pgnd6455f9g")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.3") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "039bcyb6p8kvs41hpznlq66ia51f8nkh3vjb92vc3wbhfgyqaqwb")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.4") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "08vj0p8ygh2b3fa9mw9rllfr6z32i60ywr4vibgzrk7qhd343sqj")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.5") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0d9kq4znxalj57pjzfrm7vabifpnv4idwgb7nw7cd6rw83my2rg0")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.6") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0sx20q4hxvzavrnf583kp6mjwbsw5r5rhygalv6ac7nkq7589zh7")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.7") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "150xpb2j9ri1mcs4rxfpfpyr1mqpf2qw8d5mrl1hcrl01yzvz3f6")))

(define-public crate-heraclitus-compiler-1 (crate (name "heraclitus-compiler") (vers "1.5.8") (deps (list (crate-dep (name "capitalize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0hzb9l3xgb4bkbfn3gjx49rkyz5hx939a6mdj5x4gxfva1qx6c0v")))

(define-public crate-herald-0.1 (crate (name "herald") (vers "0.1.0") (deps (list (crate-dep (name "herald_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rxrust") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0g33qh7f3nky3aiw5raw1rb8bnwnmx8s30bb1810j9kkf0psx5gh")))

(define-public crate-herald_derive-0.1 (crate (name "herald_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.12") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00cb82p1srpmc5a1yv775kwfw8dks5gvjx0h9aqpzgad2wbzgxpg")))

