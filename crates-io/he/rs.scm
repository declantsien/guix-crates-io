(define-module (crates-io he rs) #:use-module (crates-io))

(define-public crate-hersenneuk-0.1 (crate (name "hersenneuk") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1w5hipp9aqjrdqxidx1mk0l4k1l71np40nkaikpbm365nj0yyyzh")))

(define-public crate-hersenneuk-0.2 (crate (name "hersenneuk") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1f8f4dikhcksanw81kfmn9y8slf5q0awrr3qkq7p8npkrwph68rl")))

(define-public crate-hersenneuk-0.2 (crate (name "hersenneuk") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0jwg9n9war6gngk147hgydhwj1jprwcsbpxnjx9pvrr0j257wl3h")))

(define-public crate-hersenneuk-0.2 (crate (name "hersenneuk") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "08dck1c9lyk1w7khqm5lxyj6r2f1ijv8ix47a9bz7jlc8qwwj4w2")))

(define-public crate-hersenneuk-0.2 (crate (name "hersenneuk") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.0.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1llwf0b1vh34f3my0a3jsxvkmrgy0jwfghq90n89jfgaqjhivgrz")))

(define-public crate-hershey-0.1 (crate (name "hershey") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "127akaxp4cfx0nzih38nkjv7vnhwpcn1mr3xahrp3v70mj3jn7cb")))

(define-public crate-hershey-0.1 (crate (name "hershey") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r62vh2wf18wi71j96518gayb8b4c01sakxl080cikyjjdlq4s6f")))

