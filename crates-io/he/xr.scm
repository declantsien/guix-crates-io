(define-module (crates-io he xr) #:use-module (crates-io))

(define-public crate-hexrw-0.1 (crate (name "hexrw") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1mly1hy96gnxxgr3k9j0jgiaa8gy1xxcpifyhc6r7sk3v1agyasc")))

