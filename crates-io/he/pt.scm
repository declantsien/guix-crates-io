(define-module (crates-io he pt) #:use-module (crates-io))

(define-public crate-heptgen-0.1 (crate (name "heptgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1a1xc9npll6z0sm3s506lz4rbyrsw35qrr8q1d4mmg0hk22vvgim")))

