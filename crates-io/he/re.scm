(define-module (crates-io he re) #:use-module (crates-io))

(define-public crate-here-1 (crate (name "here") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)))) (hash "1ld5yjripxw36bvr92dxdk22nw1mdrsa68wrdsps8a6xs252rgj3")))

(define-public crate-here-1 (crate (name "here") (vers "1.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (optional #t) (default-features #t) (kind 0)))) (hash "06hlqgbm64qfw2mcsbr10lqam57n791p1l0dyh1m7ds51w9xp7jp") (features (quote (("std") ("off_on_release") ("default" "std" "off_on_release"))))))

(define-public crate-here-1 (crate (name "here") (vers "1.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1bkf5c5nzqib0saj8drdp4b7p5dkn99r051xhqnxpa5bi0y7j1mf") (features (quote (("std") ("off_on_release") ("default" "std" "off_on_release"))))))

(define-public crate-here_be_dragons-0.1 (crate (name "here_be_dragons") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "188ky8kgim5sli4r8wh95zr1w916nyj1dcn1qgwc5qds04rvg2p6")))

(define-public crate-here_be_dragons-0.1 (crate (name "here_be_dragons") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0fdygm6yawl6hm2f13cslvfwlc43p8pjix0jyigcvdhr4gv5ly7l")))

(define-public crate-here_be_dragons-0.2 (crate (name "here_be_dragons") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0na0s9awa1nsf46p11dbqmzm14vvrjwqpazp2vx4y1s9dl4yry2c")))

(define-public crate-here_be_dragons-0.2 (crate (name "here_be_dragons") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1h0jjsbxkgbvkcd5vp5z40zz5xwqx508lz2xrq1wdh1phfppabxf")))

(define-public crate-here_be_dragons-0.3 (crate (name "here_be_dragons") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0l2i293fxg6qm20y6vbym98zpr6qrcmy13zhb4qrq4qw61d2gv8g")))

(define-public crate-hereditary-0.1 (crate (name "hereditary") (vers "0.1.0") (deps (list (crate-dep (name "forwarding") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trait_info") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trait_info_gen") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "15plhvm429krpbagzkql6968lwn67b9vv3sa5hbi3x5dnv8b0zf1")))

(define-public crate-heredity-0.2 (crate (name "heredity") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "01j016drv3zxdl5ml84kpdv1l7j3g9l25v7hwbxqibwann4x38hn")))

(define-public crate-heredoc-0.0.1 (crate (name "heredoc") (vers "0.0.1") (hash "021k40zgjg3rgd7x03ga5938yh4xzfxh7sqp2krxpvf3a3x31n03")))

(define-public crate-heredom-0.1 (crate (name "heredom") (vers "0.1.0") (deps (list (crate-dep (name "tuplex") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kfzzk9k8rp4j13pdsjkx3z18jnlpzc8m43fnp7m22v7z9v4syla")))

(define-public crate-heresy-0.0.0 (crate (name "heresy") (vers "0.0.0") (hash "0p6in2hjj7vz48p0n9ca5d4xsz5z8391xv2n45kfb9mxj2z1agbh")))

