(define-module (crates-io he nr) #:use-module (crates-io))

(define-public crate-henrique_art_test-0.1 (crate (name "henrique_art_test") (vers "0.1.0") (hash "0avd8df1wa7zf2hr2yb33iikin6apjh0cgrd8gpr0k8v4l3d9dxg")))

(define-public crate-henry-0.1 (crate (name "henry") (vers "0.1.0") (deps (list (crate-dep (name "dashmap") (req "^3.11.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("sync"))) (kind 0)))) (hash "1jkdqkmvsv2p1zr6fggvxq1gq9j4a9xkf4rg4byjc7ir6zm6qhn6")))

(define-public crate-henry-0.2 (crate (name "henry") (vers "0.2.0") (deps (list (crate-dep (name "dashmap") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "07jd7n1ab7lzs2vygp4imgii7nj49y95hbcxd77dyx310i7ry0x8")))

(define-public crate-henry_the_cow-0.1 (crate (name "henry_the_cow") (vers "0.1.0") (hash "03k575gpn5fdsajs1hc6nn9p0x0lqrzxli8y5afdwp0dh3nh3379")))

