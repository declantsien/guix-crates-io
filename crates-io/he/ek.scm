(define-module (crates-io he ek) #:use-module (crates-io))

(define-public crate-heekkr-0.0.0 (crate (name "heekkr") (vers "0.0.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "prost") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.10.2") (default-features #t) (kind 1)))) (hash "1wcvr6qqzpc84vv5fn7xgv1rrr9dn62acj61x2zwjz8rvp31cffc")))

(define-public crate-heekkr-1 (crate (name "heekkr") (vers "1.3.1") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "prost") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.10.2") (default-features #t) (kind 1)))) (hash "18akxbkwp30amba019v9fkjihg2g01v88skq7li8w83fr7dw4shy")))

