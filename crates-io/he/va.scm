(define-module (crates-io he va) #:use-module (crates-io))

(define-public crate-hevayo_first-0.1 (crate (name "hevayo_first") (vers "0.1.0") (hash "0vzgmdaadn6907wkvhw94iys0rfh17hb2rzw3g9fgsypc7gmrgpy")))

(define-public crate-hevayo_first-0.1 (crate (name "hevayo_first") (vers "0.1.1") (deps (list (crate-dep (name "hevayo_greetings") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jlq2lzvkk7mcg4gvhql6wprhzpdmycmw8sh508czn1a1xxag55q")))

(define-public crate-hevayo_greetings-0.1 (crate (name "hevayo_greetings") (vers "0.1.0") (hash "1mhpzcg9d4cai9xma051n7h5ywhydvyim7qxvkh4xxcqmg4dq405")))

