(define-module (crates-io he is) #:use-module (crates-io))

(define-public crate-heisspot-0.1 (crate (name "heisspot") (vers "0.1.0") (deps (list (crate-dep (name "get-last-error") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_WiFi" "Networking_Connectivity" "Foundation_Collections" "Devices_WiFi" "Foundation"))) (default-features #t) (kind 0)))) (hash "0zwzqa393wd8kwr41i29nlf80qvfscrg7cngi54p9vp2f1j8jrcz")))

