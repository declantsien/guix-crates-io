(define-module (crates-io he ss) #:use-module (crates-io))

(define-public crate-hessian-0.0.1 (crate (name "hessian") (vers "0.0.1") (hash "0z5nz7q1pllybiyljd2wxb7h7kii2zzhpzl3pgwxmbdkm1gyxn94")))

(define-public crate-hessian_rs-0.0.1 (crate (name "hessian_rs") (vers "0.0.1") (deps (list (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fxbw2s2cnw119zi8lp78bi42lq2cqrvbpilx2kg3iwwa5qkg75l")))

(define-public crate-hessian_rs-0.0.2 (crate (name "hessian_rs") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cln968np68a2hkgczmww87z11ni6dfikdn40dq8wws3j4shd9jj")))

(define-public crate-hessian_rs-0.0.3 (crate (name "hessian_rs") (vers "0.0.3") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "ordered-float") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bsj7blii99cfysysfd4zs62q82rnbiznh2481qpl3ymvibaf73d")))

(define-public crate-hessian_rs-0.0.4 (crate (name "hessian_rs") (vers "0.0.4-rc1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0266iljzh3sfimjn8a0rxa6il6mkpqalx7mc1lzhc9d6wasqr0j0")))

(define-public crate-hessian_rs-0.0.4 (crate (name "hessian_rs") (vers "0.0.4-rc2") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0pg8czb38wh4gb8ahqh75mx0ivdsn9n6xiin62p2kcycldsskqr2")))

(define-public crate-hessian_rs-0.0.4 (crate (name "hessian_rs") (vers "0.0.4-rc3") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0qc6wiyjg35m33rcg4slk1kgg6shqivdnwr48m5fgnxyqdf280lw")))

