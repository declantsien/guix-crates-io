(define-module (crates-io he tu) #:use-module (crates-io))

(define-public crate-hetu-0.1 (crate (name "hetu") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "119yis55h69gkgqsvzbd2136qmjr6ssms64rnc45msdxzy1548m5")))

(define-public crate-hetu-0.1 (crate (name "hetu") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "13n2d6lv5dbar0vqas48hcxnzrkrvjwwq35v4b9xxn52aw8q9p5h")))

(define-public crate-hetu-0.2 (crate (name "hetu") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1fc5zxf26krrnldqpzy22p97si95ay8ipkgac1km0nag4clcs554")))

(define-public crate-hetu-0.3 (crate (name "hetu") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1q65xn9hzrfvilm3xi6gb3m3aqqm4z9xd5s4113m3aj67i8kpwpn")))

(define-public crate-hetu-0.4 (crate (name "hetu") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0jg9hmb6sz868bva48kxhr0y007fl36myy8zz7dbkvjvc12z6hs2")))

(define-public crate-hetu-0.5 (crate (name "hetu") (vers "0.5.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0032h11pmjs07xfi6yjq2hd6s8z6gwh6lliv7mdnpx8zzrxiix77")))

(define-public crate-hetu-0.5 (crate (name "hetu") (vers "0.5.1") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1lr24yfh42vc6626zwpdmy1ghv03kv4128gkm1yahyxqikh746xi")))

(define-public crate-hetu-0.6 (crate (name "hetu") (vers "0.6.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0xwh4627kszh7q062imq4izflawsyv2ipjlmcdcj3gkipvc79j1m")))

(define-public crate-hetu-0.6 (crate (name "hetu") (vers "0.6.1") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "01vp7rhjwgqh01am2dfqim065cmrrf5v519lj21icv3i79srdpiv")))

(define-public crate-hetu-0.7 (crate (name "hetu") (vers "0.7.0") (deps (list (crate-dep (name "ansi_term") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 2)))) (hash "0az260y848fa3fr4dxqgl45slqnfc1c2lhc8y7yc3lknb1hdmcv0")))

(define-public crate-hetu-0.8 (crate (name "hetu") (vers "0.8.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 2)))) (hash "0j9c15rp7xcn30nrpqpd8ypq6jfyj3ynbm7x0yq551mg08927jd5")))

(define-public crate-hetu-0.8 (crate (name "hetu") (vers "0.8.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 2)))) (hash "0kd95ygnl2r6ja3alwv6p3d32al4v21117czb5lglz9sn0ska96l")))

