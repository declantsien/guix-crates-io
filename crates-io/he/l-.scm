(define-module (crates-io he l-) #:use-module (crates-io))

(define-public crate-hel-colored-0.1 (crate (name "hel-colored") (vers "0.1.0") (hash "086x82fniapdfkda50ciz12cbws2264ypzvah91mj0r12d5nrgwf")))

(define-public crate-hel-colored-0.2 (crate (name "hel-colored") (vers "0.2.0") (hash "1658q5nv9j7d4891xyvbapd3yq3vps9i0mywmcgqk4njq7qfj0fl")))

(define-public crate-hel-colored-0.3 (crate (name "hel-colored") (vers "0.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1q7mb82qncr574h3f8dpz24x87r7g9mmaqi6c0achkv69zz36jby")))

(define-public crate-hel-colored-0.3 (crate (name "hel-colored") (vers "0.3.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1bdw7vg9h0g19hy010qml0wf0xdafxkldrm4jpqh9d0vr38xs3l8")))

(define-public crate-hel-colored-0.4 (crate (name "hel-colored") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0p4rifz39pjhp19byfl0gfqvjm2cmd5rfgbcba86npfwf69x8gif")))

(define-public crate-hel-colored-0.4 (crate (name "hel-colored") (vers "0.4.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0l02xi0jgs6dg8dc93j4lwir39f7a4b3dvpn0sd5ddwchh2rw2d1")))

(define-public crate-hel-colored-0.4 (crate (name "hel-colored") (vers "0.4.4") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "07hs90pb8rvx0n4d9awv1ifzmn1g57dzzp2pmkifc3rhyrb6vrfq")))

(define-public crate-hel-colored-0.5 (crate (name "hel-colored") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0yqz4cmvdxhj8bxz2xdj727qs6jpa71k17pyvzv9xlb6wxhzkv0c") (features (quote (("nested"))))))

(define-public crate-hel-colored-0.5 (crate (name "hel-colored") (vers "0.5.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1dqz0mnmq7czc9d1b1zdbszvmd20dvgdgl7y8m2yk4qslvf3f2yc") (features (quote (("nested"))))))

(define-public crate-hel-fs-0.1 (crate (name "hel-fs") (vers "0.1.0") (hash "11408g4w1v9kmxhjvfpi7677gfasaya205021jgiik8idvxvfhz4")))

(define-public crate-hel-fs-0.1 (crate (name "hel-fs") (vers "0.1.1") (hash "0n304b93i0dp4nl5q205swqb6n2ab5syadhh0ni9j64bx6gh7kgs")))

(define-public crate-hel-prompt-0.1 (crate (name "hel-prompt") (vers "0.1.0") (hash "05qld58hk60hisk15fjvi0gn0ia716wqm01blxnm5dn7cjrvd9za")))

(define-public crate-hel-prompt-0.1 (crate (name "hel-prompt") (vers "0.1.1") (hash "0n1vc92xyal1r3v8fgwmi4k89q8iy1yal6lxmd60sw77v8pxpz08")))

(define-public crate-hel-prompt-1 (crate (name "hel-prompt") (vers "1.0.0") (hash "0fchpkydqbqzs1f13wqsc32ws5bd5xva69qwfj8i3whbz5h5qzrn")))

(define-public crate-hel-random-0.1 (crate (name "hel-random") (vers "0.1.0") (hash "1h1qvf9i2fbqwf6if466g2f7319bdm0bnkda16xsc8nj1pqlncr1")))

(define-public crate-hel-random-0.1 (crate (name "hel-random") (vers "0.1.1") (hash "10myrdgvbr2cm3yzxif9nsawp403947gsmxapgfa5k2gg1zm5n69")))

(define-public crate-hel-random-0.2 (crate (name "hel-random") (vers "0.2.0") (hash "01kqwzmpqa74wsz16xcg4bvb9ys13hwdn8wf4bq7ynl428hh0qlg")))

(define-public crate-hel-random-0.3 (crate (name "hel-random") (vers "0.3.0") (hash "1ss7mq1aba17m7hvjba0y0z6rlmrygrk4a11aks11pr9h4dvc89y")))

(define-public crate-hel-random-0.4 (crate (name "hel-random") (vers "0.4.0") (hash "1ml7g1vrz49qj1ia7jw7hrf7q1gws9yapp2r51k17lqcl43q9fx9")))

(define-public crate-hel-thread-pool-0.1 (crate (name "hel-thread-pool") (vers "0.1.0") (hash "0yhihwvzbpa5lwp4wl97fraan4nsay4mqcs839w87spwvmmgk5sy") (yanked #t)))

(define-public crate-hel-thread-pool-0.1 (crate (name "hel-thread-pool") (vers "0.1.1") (hash "1whfzc2gnrx6cvjillll1f63srp8bslmy27jhy35qln5wb9cnf6f")))

(define-public crate-hel-thread-pool-0.1 (crate (name "hel-thread-pool") (vers "0.1.2") (hash "0km4qlg5cgkryhcp76j7r45aq0p83cz9prz4jf801lglsl27dv8f")))

(define-public crate-hel-thread-pool-0.2 (crate (name "hel-thread-pool") (vers "0.2.0") (hash "0vd8cqljlbg0l5r99smz59hrgqg388x2hg8sv5scprd32yiish1b")))

(define-public crate-hel-thread-pool-0.3 (crate (name "hel-thread-pool") (vers "0.3.0") (hash "0yx7j7rapjxvmj8h9i1v9rxq4c7cw05a9izk54vlyy5rrwj66sv5")))

(define-public crate-hel-thread-pool-0.3 (crate (name "hel-thread-pool") (vers "0.3.1") (hash "1w9iwja3zvi06nkz3mcp1f3l46d16nyr0x5v671nwzdx9dv75dlc")))

(define-public crate-hel-time-0.1 (crate (name "hel-time") (vers "0.1.0") (hash "0g7n865ahf6yi9sqyrcnl23w734y9lgllannhn7incq5vj3dbry0")))

(define-public crate-hel-time-0.2 (crate (name "hel-time") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)))) (hash "19w1akj8wc7i8r5cq10w8jlr793s2r24hnwbba0499h9653i7qx0")))

(define-public crate-hel-time-0.3 (crate (name "hel-time") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)))) (hash "0ywcs84rgsp44kgwcvlyvl6xshrhx5dfqanr4gd3s4b6ngm5asc0")))

(define-public crate-hel-time-0.4 (crate (name "hel-time") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)))) (hash "0grfrqcz1krs66y0gm5qlz74hy2nvfz00zaqw45w2kcwgsbbvhn6")))

(define-public crate-hel-time-0.4 (crate (name "hel-time") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)))) (hash "0617svdwwghs69p2mhrfy0la6gr6ppjakvcixliqkpdppw5b7z10")))

