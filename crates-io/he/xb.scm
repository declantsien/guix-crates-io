(define-module (crates-io he xb) #:use-module (crates-io))

(define-public crate-hexbomb-0.1 (crate (name "hexbomb") (vers "0.1.0") (hash "1a4lvpf64898bbfjma82lal3p5ldagyk688slianannw94x2sbwv")))

(define-public crate-hexbomb-0.2 (crate (name "hexbomb") (vers "0.2.0") (deps (list (crate-dep (name "arguably") (req ">=2.0.0-alpha, <3.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req ">=2.0.0, <3.0.0") (default-features #t) (kind 0)))) (hash "1snkyqblq407sxwf4qc5wv8hl9y0drb4d1xj46s34asifm4afs3p")))

(define-public crate-hexbomb-0.2 (crate (name "hexbomb") (vers "0.2.1") (deps (list (crate-dep (name "arguably") (req ">=2.0.0-alpha, <3.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req ">=2.0.0, <3.0.0") (default-features #t) (kind 0)))) (hash "0k0g52mz6w2c17hjbmnkas78yrmsvyfm4s92d3ackwzvix7nv8c0")))

(define-public crate-hexbomb-0.3 (crate (name "hexbomb") (vers "0.3.0") (deps (list (crate-dep (name "arguably") (req "^2.0.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "11is2104jrc20ycmcsdddqr6gk5bkw67c5gl302rra516z97ak9g")))

(define-public crate-hexbomb-0.3 (crate (name "hexbomb") (vers "0.3.1") (deps (list (crate-dep (name "arguably") (req "^2.0.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1wbwcf6xcpdgc86hfxyr1cvvgjdzw205f8wvc8cgc6a2cm3lcysi")))

(define-public crate-hexbomb-0.3 (crate (name "hexbomb") (vers "0.3.2") (deps (list (crate-dep (name "arguably") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0q9g1pxccpabjxw6b1sfyn9zf3jsch1yg8kcxjwdiy1xva1wpia8")))

(define-public crate-hexbytes-0.1 (crate (name "hexbytes") (vers "0.1.0") (hash "0rf72wn2xwlfk0gyhla8h9sazzsp34ikaxm6whfpia8023hv4gsv")))

