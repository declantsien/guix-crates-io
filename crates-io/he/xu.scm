(define-module (crates-io he xu) #:use-module (crates-io))

(define-public crate-hexutil-0.1 (crate (name "hexutil") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "err-derive") (req "^0.2.4") (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "1hcmbmd1208lcp0b3g667p0vkpqc82hi7zwcf7rzc0cbm1jjzphs") (features (quote (("std" "alloc" "err-derive/std" "serde/std") ("default" "std") ("alloc" "serde/alloc"))))))

