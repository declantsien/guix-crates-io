(define-module (crates-io he xi) #:use-module (crates-io))

(define-public crate-hexi-0.0.1 (crate (name "hexi") (vers "0.0.1") (hash "1ykrqrdzgmbh6yzb67cqcijj7njxr6v1wwcyn6d787dfdd1lcrqb")))

(define-public crate-hexise-0.0.1 (crate (name "hexise") (vers "0.0.1") (deps (list (crate-dep (name "version-sync") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "003x49y494dh587n04f4ggw9vm9bhx6a58pjwfsig74iybbfq12n")))

