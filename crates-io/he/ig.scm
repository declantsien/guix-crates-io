(define-module (crates-io he ig) #:use-module (crates-io))

(define-public crate-height-mesh-0.1 (crate (name "height-mesh") (vers "0.1.0") (deps (list (crate-dep (name "ndshape") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pxfnn6aj989q6i3zxfanjnjvx704a4fr8q9z0a7f6nbkz6flhdf")))

(define-public crate-heightmap_to_stl-0.1 (crate (name "heightmap_to_stl") (vers "0.1.0") (deps (list (crate-dep (name "stl_io") (req "~0.5.2") (default-features #t) (kind 0)))) (hash "125v9i66hjirb3dzapkbllpyl27sw4nsd27d2l073n23pvfr6pk9")))

