(define-module (crates-io he y-) #:use-module (crates-io))

(define-public crate-hey-jarvis-0.1 (crate (name "hey-jarvis") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chatgpt_rs") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0djb9rbcilarrrpdrjcnrrjbqgra495cblcsjr8xh68ssvvwdxrl")))

