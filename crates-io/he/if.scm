(define-module (crates-io he if) #:use-module (crates-io))

(define-public crate-heif-0.0.1 (crate (name "heif") (vers "0.0.1") (hash "12xccih5klj50gwvkg4mhwjllg2ny9p0a99jj0jmkxlp1q9v2n5g")))

(define-public crate-heif-sys-0.1 (crate (name "heif-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.45") (default-features #t) (kind 1)) (crate-dep (name "bmp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mozjpeg") (req "^0.8") (default-features #t) (kind 0)))) (hash "1pdxfnm3vmwvlm2h42ym7sn7yyb743adjfyw7gzmf9hbywswakh5") (links "heif")))

(define-public crate-heif-sys-0.1 (crate (name "heif-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.45") (default-features #t) (kind 1)))) (hash "13bnklr1a5kn4rxnc6ih4bqj72l4a8bbaxw3q89mf6m9f2ksbr8a") (links "heif")))

