(define-module (crates-io he ng) #:use-module (crates-io))

(define-public crate-heng_rs-0.1 (crate (name "heng_rs") (vers "0.1.0") (deps (list (crate-dep (name "futures-channel-preview") (req "^0.3.0-alpha.19") (features (quote ("sink"))) (kind 0)) (crate-dep (name "futures-util-preview") (req "^0.3.0-alpha.19") (kind 0)) (crate-dep (name "tokio") (req "^0.2.0-alpha.6") (default-features #t) (kind 2)) (crate-dep (name "tokio-executor") (req "^0.2.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "tokio-timer") (req "^0.3.0-alpha.6") (default-features #t) (kind 0)))) (hash "1w8mmy156qdw81lwjpb1mz87jz1q38blprgig2xsgqj220zxm72m")))

