(define-module (crates-io he x_) #:use-module (crates-io))

(define-public crate-hex_-0.1 (crate (name "hex_") (vers "0.1.0") (hash "12w482r5cjzayms0v7zjig87s836xgxp83ncccqiwm9044r8hkcz")))

(define-public crate-hex_color-1 (crate (name "hex_color") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0nx096wl1xycfr4xcsa385k206s0ap7d9hjp6f7qzln7g9mzbq6f")))

(define-public crate-hex_color-2 (crate (name "hex_color") (vers "2.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "024q45qjfhmcq10s64clh16hlalzkjskkkyxjgg00y68rd8p14gz") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "rand?/std" "rand?/std_rng") ("serde" "dep:serde" "dep:arrayvec") ("rand" "dep:rand"))))))

(define-public crate-hex_color-2 (crate (name "hex_color") (vers "2.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1yf1v9w1awlahjcm1nxgfa9pjrs0s3xncnvna0qln5w7ffkz8j3g") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "rand?/std" "rand?/std_rng") ("serde" "dep:serde" "dep:arrayvec") ("rand" "dep:rand"))))))

(define-public crate-hex_color-3 (crate (name "hex_color") (vers "3.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1w2f91y34b30x8jy6y4f37fh6i9ia1h16pjb5v5gfcy6yhdi0zyk") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "rand?/std" "rand?/std_rng") ("serde" "dep:serde" "dep:arrayvec") ("rand" "dep:rand"))))))

(define-public crate-hex_d_hex-0.1 (crate (name "hex_d_hex") (vers "0.1.0") (hash "01sliy89sga98x4axij9vrlqshx67l65m73j65gv82mr757bfl2b")))

(define-public crate-hex_d_hex-1 (crate (name "hex_d_hex") (vers "1.0.0") (hash "09899m4pgddw1zjf1898j1w9gngc1ph5achl2dn0kkf8gcasdrvq")))

(define-public crate-hex_d_hex-1 (crate (name "hex_d_hex") (vers "1.0.1") (hash "09s8mw6p76xz3zqddlaan17sf343zgxz5z0g41492hwjv0ag9lpq")))

(define-public crate-hex_dump-0.1 (crate (name "hex_dump") (vers "0.1.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "1kk03kqlqjh03yyzaamgg2d3yv7q636pwh5ljhdydzx4aqwl49pl")))

(define-public crate-hex_dump-0.1 (crate (name "hex_dump") (vers "0.1.1") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "11cdiw4hk92plgg1jmixjq5ihiwbbpkjv0vb4hfa75vr78mg2bcc")))

(define-public crate-hex_dump-0.1 (crate (name "hex_dump") (vers "0.1.2") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0ijsq5i01cz8ibw8ypb91pja0nvcypbgd61vpwxczg3cc2kmijgz")))

(define-public crate-hex_dump-0.1 (crate (name "hex_dump") (vers "0.1.3") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "0xh5widkdp9wf5ffjqwkm88qma6l33bmrnf3lpmllw1jab59jjks")))

(define-public crate-hex_dump-0.1 (crate (name "hex_dump") (vers "0.1.5") (deps (list (crate-dep (name "doe") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "1dzxnnnfqyj3jix9qyqbk7jv0xpzsy7sraiz4k8mdwm7g98pc0yw")))

(define-public crate-hex_fmt-0.1 (crate (name "hex_fmt") (vers "0.1.0") (hash "1nh4zdj474qsyh4i168c0a0svkian5fv4pgf4s4rxv9k3p5vd7l7")))

(define-public crate-hex_fmt-0.2 (crate (name "hex_fmt") (vers "0.2.0") (hash "12zqyxv5756dinissv0ph5c6rw9baa97d9n2b5i0r3ybjn4k7y16")))

(define-public crate-hex_fmt-0.3 (crate (name "hex_fmt") (vers "0.3.0") (hash "0vrkzxd1wb4piij68fhmhycj01ky6nsn73piy37dk97h7xwn0zxh")))

(define-public crate-hex_gfx-0.0.1 (crate (name "hex_gfx") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "hex_math") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "hex_win") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1pfahh8nxbq5ff256vk7w0kkj08dwnvkc4ah8f0m27mqip13awkl")))

(define-public crate-hex_grid-0.1 (crate (name "hex_grid") (vers "0.1.0") (hash "0003hv13c8jpjkgv7iaxgjbqr6v8s5325pdf5s9rpc3iqrj23sa3")))

(define-public crate-hex_grid-0.1 (crate (name "hex_grid") (vers "0.1.1") (hash "1mm3i05lfvfkcafgj4qdf0jcwki13nbp0ydl2rb922nmrz4gd5dq")))

(define-public crate-hex_grid-0.1 (crate (name "hex_grid") (vers "0.1.2") (hash "1p7j5dd5i1501dvla4cfl2vqgnwh06ahwksl41ai4xf5b0mrmc8j")))

(define-public crate-hex_grid-0.2 (crate (name "hex_grid") (vers "0.2.0") (hash "1xsaf9cwajxw7sf0hzz2gljp58l189k88ldnpn8fnryz41jb6gl4")))

(define-public crate-hex_grid-0.2 (crate (name "hex_grid") (vers "0.2.1") (hash "0ifsmwrl0jx6pnjchw07s42mnhxw8j5jclxfywbfp93ip62h3ciw")))

(define-public crate-hex_it-0.1 (crate (name "hex_it") (vers "0.1.0") (deps (list (crate-dep (name "term_size") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tui-tools") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0f8wccavmm7ks587i4h2lwyh4ry9ymfpfiisbnk8iba4fgaqfq1w")))

(define-public crate-hex_it-0.1 (crate (name "hex_it") (vers "0.1.1") (deps (list (crate-dep (name "term_size") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tui-tools") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0pzic5vv8v5h16bl7gjivhx9ysn08hlrvr096f2dycp6hildxba7")))

(define-public crate-hex_lit-0.1 (crate (name "hex_lit") (vers "0.1.0") (hash "1wmx5v4axqsxasxa8q4r1adfhnazp62nab9vbsvvcav1zfxq0wwx") (features (quote (("rust_v_1_46"))))))

(define-public crate-hex_lit-0.1 (crate (name "hex_lit") (vers "0.1.1") (hash "1g8bln2js2c49ray6fy6c9bk9wfm5hnwjspx7jqng60m7whx249h") (features (quote (("rust_v_1_46"))))))

(define-public crate-hex_literals-0.1 (crate (name "hex_literals") (vers "0.1.0") (hash "1pmm9gvc6cv8ypbd4kk02nk84imv5fcmav2x1397n3awpx9qdxv2")))

(define-public crate-hex_literals-0.1 (crate (name "hex_literals") (vers "0.1.1") (hash "0iqz2mln56zvr5zdzhhy95z5pmfqz5hszpz0kkrfnan5ywwjgw9n")))

(define-public crate-hex_literals-0.1 (crate (name "hex_literals") (vers "0.1.2") (hash "1dri7dyrn51jb4bc6w41f1a3nh6xd0d6vqdsd8xak5gkdl9drp97")))

(define-public crate-hex_literals-0.1 (crate (name "hex_literals") (vers "0.1.3") (hash "0w4g3h43z0vj9kmrsm74kp8cwwvcyvc1km7pkfxdkb38179137yy")))

(define-public crate-hex_literals-0.1 (crate (name "hex_literals") (vers "0.1.4") (hash "1p8b6h2a1hk2jli7nlg9zgjcrn4l6i7y7r7pzas1bzp8cka2gmmx")))

(define-public crate-hex_literals-0.1 (crate (name "hex_literals") (vers "0.1.5") (hash "0d6h6rj1gaf9f0bb79s5ipm9ims6zx8cf5kv9q2rl7rzvidik9lr")))

(define-public crate-hex_literals-0.1 (crate (name "hex_literals") (vers "0.1.6") (hash "16m550478sj7k3c26510jgnglcmsdfivk6lsrk9nvgbf98yxwa79")))

(define-public crate-hex_ln-0.1 (crate (name "hex_ln") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ttf-parser") (req "^0.18.0") (kind 0)) (crate-dep (name "bindgen") (req "^0.64") (kind 1)))) (hash "0bsq8s7qigy7nz8xxfla3acz6i580ky6h0k0kkmn8abvxdzphbi4") (features (quote (("no_std") ("log"))))))

(define-public crate-hex_math-0.0.1 (crate (name "hex_math") (vers "0.0.1") (hash "1n6m1pyshixars8ic23kjwjh5mbg1lbvxbl7yck53amy7x2kxyxc")))

(define-public crate-hex_pp-0.1 (crate (name "hex_pp") (vers "0.1.0") (hash "1ii2p1qdnnz75rkj1db46kh750vppvmjgfg717q34w5wqra2rj1i") (yanked #t)))

(define-public crate-hex_pp-0.1 (crate (name "hex_pp") (vers "0.1.1") (hash "0p5zzkhxv9hgvz94lb4r608f24n1v2dk8nk8wql8fypsrp3b0dmw") (yanked #t)))

(define-public crate-hex_pp-0.1 (crate (name "hex_pp") (vers "0.1.2") (hash "123ks2vp58m66wkqzklv4n3db923c7459jgf2s240009jxnajd8v")))

(define-public crate-hex_renderer-0.1 (crate (name "hex_renderer") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0z914sd0g892jlbfpr6s8yg32krphx6iv3iqlgk67afm91c30wn3")))

(define-public crate-hex_renderer-0.2 (crate (name "hex_renderer") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1365gxzmmym9lcqx5wdp9alkv0nv85fzknd75zdl346ir1xyi0v5")))

(define-public crate-hex_renderer-0.2 (crate (name "hex_renderer") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0hjjb55wmaz8cax0925mxzx8324hpj5pwi3s7724hdswl4awz9c6")))

(define-public crate-hex_renderer-0.2 (crate (name "hex_renderer") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1l8wwqghgpizh4k4896f7lc8nalnxggsl9mgfs35xc9jzvv4nd6s")))

(define-public crate-hex_renderer-0.2 (crate (name "hex_renderer") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0yrb45q86swhx01x1awa7ybh980a7v6018mljnh0hn50iayf45c8")))

(define-public crate-hex_renderer-0.2 (crate (name "hex_renderer") (vers "0.2.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0iaalm67bkyz2w5d3h5h6n7llagmjh2mfrmkmpbhpi95cfdw3wwd")))

(define-public crate-hex_spell-0.1 (crate (name "hex_spell") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)))) (hash "05c1g687akiwn1jvxyawqbwfn05wnmjafnd9rnv079dza94ys6ys") (yanked #t)))

(define-public crate-hex_spell-0.1 (crate (name "hex_spell") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)))) (hash "0aiz1ldj8b31r6mqm16ykqakbc4fp8cv3x3pdxmc4rgms1vhpw95") (yanked #t)))

(define-public crate-hex_spell-0.1 (crate (name "hex_spell") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)))) (hash "1w87n6dql6645bjpknhrjb7j1jqs0vwbgb5l93qnvnbqjc8jf4y7") (yanked #t)))

(define-public crate-hex_spell-0.1 (crate (name "hex_spell") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)))) (hash "096scf5igzbkf262bdb6vjp0a88if1fcjwamzzw4p5zd2icbfl22") (yanked #t)))

(define-public crate-hex_spell-0.1 (crate (name "hex_spell") (vers "0.1.4") (deps (list (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)))) (hash "1q159mndrnqifmz9lqd8vlf2ns6cj7nis21fynwi2cq832qf4iss") (yanked #t)))

(define-public crate-hex_spell-0.1 (crate (name "hex_spell") (vers "0.1.5") (deps (list (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 2)))) (hash "1nix9aw9dykpwc2y5dmavzmakgj6352vdbsxkbgjxpmz29m70kax") (yanked #t)))

(define-public crate-hex_str-0.1 (crate (name "hex_str") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (features (quote ("min_const_gen"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "10fb8n03h5f8gyaxlz6ks0gk4iinwlk4j5nl2sf47ppnv6k8nsj1") (v 2) (features2 (quote (("serde" "dep:serde") ("rand" "dep:rand"))))))

(define-public crate-hex_str-0.1 (crate (name "hex_str") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (features (quote ("min_const_gen"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0f80rmx9fpg8hq79b2nshhgqj2nxynv49506xwxmy1i8cbaylyb6") (v 2) (features2 (quote (("serde" "dep:serde") ("rand" "dep:rand"))))))

(define-public crate-hex_table-0.1 (crate (name "hex_table") (vers "0.1.0") (hash "0i5wcd6w665kwrj5dwm2fii3y4mx6jy91wvcnh6af60i0mzbdm72")))

(define-public crate-hex_table-0.1 (crate (name "hex_table") (vers "0.1.1") (hash "10b1ahwzhwg78dxlgrdn4gh8q26mjrg5mn6n4m85l1hmx620xj62")))

(define-public crate-hex_table-0.1 (crate (name "hex_table") (vers "0.1.2") (hash "055vr5sygxyr7bam7llhf1bnvwbbbwdya5ygr4is8dzh7vzsxq5n")))

(define-public crate-hex_table-0.1 (crate (name "hex_table") (vers "0.1.3") (hash "0cwc2fd7wa0xckc960fm7kcq08pkk14i97zbd5n2an247k3bkskd")))

(define-public crate-hex_table-0.1 (crate (name "hex_table") (vers "0.1.4") (hash "1sc87p552vkrim7ba1kw0gxifbzjiichv94d81d050n95839qxjn")))

(define-public crate-hex_win-0.0.1 (crate (name "hex_win") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "1zn575rp48wid6dk87pdrdgpjc0g8hrybif9hb8kjhdc6ix7mln2")))

