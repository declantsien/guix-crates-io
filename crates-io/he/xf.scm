(define-module (crates-io he xf) #:use-module (crates-io))

(define-public crate-hexf-0.1 (crate (name "hexf") (vers "0.1.0") (deps (list (crate-dep (name "hexf-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hexf-parse") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0f3mgrfzswbg83nfwpklamqvzi2rmnplf9jk6qd7ff6q6b656ip5")))

(define-public crate-hexf-0.2 (crate (name "hexf") (vers "0.2.0") (deps (list (crate-dep (name "hexf-parse") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.41") (features (quote ("parsing" "proc-macro"))) (kind 0)))) (hash "1pd0md9xdrj6kanj6sqvnjps3zdij9cpi1n6pkhi4sjn3qlr9rcj")))

(define-public crate-hexf-0.2 (crate (name "hexf") (vers "0.2.1") (deps (list (crate-dep (name "hexf-parse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.41") (features (quote ("parsing" "proc-macro"))) (kind 0)))) (hash "0zl2pk9n995xj0iq7jsr46aslcgsipa1iar6a7drszfd1maz8636")))

(define-public crate-hexf-impl-0.1 (crate (name "hexf-impl") (vers "0.1.0") (deps (list (crate-dep (name "hexf-parse") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "04jyp11fxwdd8rdrqjqpyl75xws5s1vsbsk44w5p6sm7vgxdrsi2")))

(define-public crate-hexf-parse-0.1 (crate (name "hexf-parse") (vers "0.1.0") (hash "1b2h0lvksn8748764x46729ygpz8grack24spin0k29ssmr6yabr")))

(define-public crate-hexf-parse-0.2 (crate (name "hexf-parse") (vers "0.2.0") (hash "18lpcm4qq8kjpxihc1cipjiy1wbaa0qazdjj4624c4jgczlp4bkw")))

(define-public crate-hexf-parse-0.2 (crate (name "hexf-parse") (vers "0.2.1") (hash "1pr3a3sk66ddxdyxdxac7q6qaqjcn28v0njy22ghdpfn78l8d9nz")))

(define-public crate-hexf-parse-libm-0.2 (crate (name "hexf-parse-libm") (vers "0.2.1") (deps (list (crate-dep (name "libm") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0nk0j7fhgbm1aqb6vxy8lgqg61shxssc99r9azf34m0pnv11dnim")))

(define-public crate-hexfloat-0.0.1 (crate (name "hexfloat") (vers "0.0.1") (hash "0i8rq4lk28ypny3nza84wbv7ahy0104qrpy21p1vdrdrilzbbdyg")))

(define-public crate-hexfloat-0.0.2 (crate (name "hexfloat") (vers "0.0.2") (hash "1npgr5y1yjwlw0r10wrm16dmqiawv0ildmmm1ibapnjyav2h6z6i")))

(define-public crate-hexfloat-0.0.3 (crate (name "hexfloat") (vers "0.0.3") (hash "0nw2rsgn25mfqbyg5fknsclxbcy0ld5v8vqlpy91mhdcaqa8p8j2")))

(define-public crate-hexfloat-0.1 (crate (name "hexfloat") (vers "0.1.0") (hash "08v2frkx2imiyckand569xa5zr3ym5bfajmihnjq3ilhimr78agm")))

(define-public crate-hexfloat-0.1 (crate (name "hexfloat") (vers "0.1.1") (hash "1vpr2rxgz4n5kf75da63pa09v85jz61ishk1yk3m7rlwjil6bbvb")))

(define-public crate-hexfloat-0.0.4 (crate (name "hexfloat") (vers "0.0.4") (hash "0gnlzk1ks1fabfsnh3xn9lbzhx9b7civ1hzblz68vpbys6008j6n") (yanked #t)))

(define-public crate-hexfloat-0.1 (crate (name "hexfloat") (vers "0.1.2") (hash "1g269b85ydjznaa9dyhfgnrxq3kwqv2a94d1pvia1n371p91a75k")))

(define-public crate-hexfloat-0.1 (crate (name "hexfloat") (vers "0.1.3") (hash "1aq3f99qsns7bfdh1hhkcyfkvm4s4rg6ddpigx8jjn3yd8mzc29a")))

(define-public crate-hexfloat2-0.1 (crate (name "hexfloat2") (vers "0.1.0") (hash "1smbvvjichqghjyby819hlxn6cb05lkc5dym05icx62s41yv3dm9") (rust-version "1.70")))

(define-public crate-hexfloat2-0.1 (crate (name "hexfloat2") (vers "0.1.1") (hash "08p6rn0wsxqgk8mca03v9j2wq3q7ihm810id7k13xm79l2mr7n48") (rust-version "1.67")))

(define-public crate-hexfloat2-0.1 (crate (name "hexfloat2") (vers "0.1.2") (hash "1gc2qx9d6pz0hl7sdgz77lg65h94f915k35ivjxlsdjimy9sknsz") (rust-version "1.67")))

(define-public crate-hexfloat2-0.1 (crate (name "hexfloat2") (vers "0.1.3") (hash "134znh5pjgh798r6xdz2pxpqb38rqc71mlp0yv6l200998b6bzmy") (rust-version "1.67")))

