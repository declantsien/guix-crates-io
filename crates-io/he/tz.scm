(define-module (crates-io he tz) #:use-module (crates-io))

(define-public crate-hetzner-robot-rs-0.0.1 (crate (name "hetzner-robot-rs") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1d06s6mhavqsm5sb7m7ns81z7ibks5d5n4rv167zy29nb2d68by7") (yanked #t)))

(define-public crate-hetzner-robot-rs-0.0.1 (crate (name "hetzner-robot-rs") (vers "0.0.1-alpha.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1qlbg3yzh5bg861ckcdwcgq874r99sz82x2a4ma80xhq544sayz5")))

