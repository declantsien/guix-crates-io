(define-module (crates-io he -c) #:use-module (crates-io))

(define-public crate-he-core-0.0.0 (crate (name "he-core") (vers "0.0.0") (deps (list (crate-dep (name "he-core-lib") (req "^0.0") (default-features #t) (kind 0)))) (hash "0njynb9s7fjrbw84wbd713yb0lgw89h98w04xaa2si8iqbz8cs5x")))

(define-public crate-he-core-lib-0.0.0 (crate (name "he-core-lib") (vers "0.0.0") (hash "0q9xwiqdgwikxw5hg9z9yyy1h19jz8szlvxsx58ncp2jmcrrm2wb")))

