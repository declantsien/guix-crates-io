(define-module (crates-io he ar) #:use-module (crates-io))

(define-public crate-heart-0.0.0 (crate (name "heart") (vers "0.0.0") (hash "0dq6jmpq98la8i0vc0ml3nv1cj90zpg5k85lc4ynp90327rs925l")))

(define-public crate-heartbeat-0.0.1 (crate (name "heartbeat") (vers "0.0.1") (deps (list (crate-dep (name "toml") (req "*") (default-features #t) (kind 0)))) (hash "1z69pg1697mggdvl5dpwifkgxhndmcmc0yv52547xwbf6ss87np1")))

(define-public crate-heartbeat-server-0.1 (crate (name "heartbeat-server") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "pling") (req "^0.2.0") (features (quote ("http-sync"))) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.1") (default-features #t) (kind 0)))) (hash "1nc2dkw51pjcd2m9g96iqii9nc81r0xf3lp0n4560bpjynrhg6kz")))

(define-public crate-heartbeats-simple-0.2 (crate (name "heartbeats-simple") (vers "0.2.0") (deps (list (crate-dep (name "hbs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-acc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-acc-pow") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-pow") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cy9xlwpym19kdjnw6a6j04rnjx781dhdrx68bpgg7gq9z5czd9k")))

(define-public crate-heartbeats-simple-0.3 (crate (name "heartbeats-simple") (vers "0.3.0") (deps (list (crate-dep (name "heartbeats-simple-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cb04p6xw3f1hfwgciqrxn7vy04rplj1as8z7zfhpkga483q3h3q")))

(define-public crate-heartbeats-simple-0.4 (crate (name "heartbeats-simple") (vers "0.4.0") (deps (list (crate-dep (name "heartbeats-simple-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p5a733r7zwv3hsksrsapaljnc389vlcss9cbzcyjm9r4g707l4s")))

(define-public crate-heartbeats-simple-0.4 (crate (name "heartbeats-simple") (vers "0.4.1") (deps (list (crate-dep (name "heartbeats-simple-sys") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12z2hr7xsy6wlfjdad9wb4jqc1xrbfgxamqa4rlakgqxpzgzggkq")))

(define-public crate-heartbeats-simple-sys-0.2 (crate (name "heartbeats-simple-sys") (vers "0.2.0") (deps (list (crate-dep (name "hbs-acc-pow-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-acc-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-common-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-pow-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0jk51n8qhqyp1wjn37bkhyhg6b6b7rs5c1q0710i2zmsmx7jnln5")))

(define-public crate-heartbeats-simple-sys-0.2 (crate (name "heartbeats-simple-sys") (vers "0.2.1") (deps (list (crate-dep (name "hbs-acc-pow-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-acc-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-common-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-pow-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hbs-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1mis2rhkg8d6mc9ir6hsrdbpm5hfrs6vq8zjhpnr0lbqd32b55hz")))

(define-public crate-heartbeats-simple-sys-0.3 (crate (name "heartbeats-simple-sys") (vers "0.3.0") (deps (list (crate-dep (name "cmake") (req "^0.1.13") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0022h1056piyzvjflfvj53pkdz5jmmhz66l5fwg0gnqqg1sf8dil")))

(define-public crate-heartbeats-simple-sys-0.3 (crate (name "heartbeats-simple-sys") (vers "0.3.1") (deps (list (crate-dep (name "cmake") (req "^0.1.13") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0pd405gzvgc88yn2k8p81yha5wx0jmcvm512myikrr0gp1k5d5wx")))

(define-public crate-heartbeats-simple-sys-0.3 (crate (name "heartbeats-simple-sys") (vers "0.3.2") (deps (list (crate-dep (name "cmake") (req "^0.1.13") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "008kvi0bxcg555jj5af9ri7nmv0v7j2gk0gkf90pypb62xvbdi2k")))

(define-public crate-heartbeats-simple-sys-0.4 (crate (name "heartbeats-simple-sys") (vers "0.4.0") (deps (list (crate-dep (name "cmake") (req "^0.1.13") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "1al0sldk1cvaciq5igsg67wphc9zmia2j03vwlhmawjg62f87rwb")))

(define-public crate-heartbeats-simple-sys-0.4 (crate (name "heartbeats-simple-sys") (vers "0.4.1") (deps (list (crate-dep (name "cmake") (req "^0.1.13") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "195iizchzixi074yh1n0vyp1jy1pf0f8dxs9007cq9ql0700i971")))

(define-public crate-heartbeats-simple-sys-0.4 (crate (name "heartbeats-simple-sys") (vers "0.4.2") (deps (list (crate-dep (name "cmake") (req "^0.1.13") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0bn244nw61xhfd3h1d004358m84h3j1byhpjayiplb2vnhcj8idc")))

(define-public crate-heartbeats-simple-sys-0.4 (crate (name "heartbeats-simple-sys") (vers "0.4.3") (deps (list (crate-dep (name "cmake") (req "^0.1.13") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.2") (default-features #t) (kind 1)))) (hash "0df94icyaacbl186b1ih135gymg8qlxxd2i8bl0jjgyw5x46xw19")))

(define-public crate-heartfelt-0.1 (crate (name "heartfelt") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex-lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xcvai94p56klifl81zscsr25wrgm7zcpyv6g8d3pbh12zhdnklv")))

(define-public crate-heartfelt-0.1 (crate (name "heartfelt") (vers "0.1.1") (deps (list (crate-dep (name "ahash") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex-lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gsdqhr7c17ldnpd8lls7qqvpcap0vdidbrjghylriqcf9fzkgg3")))

(define-public crate-heartfelt-0.1 (crate (name "heartfelt") (vers "0.1.2") (deps (list (crate-dep (name "ahash") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex-lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10lraawi90h5b2knhvp2skvpx1gqcaqjk2bg6sqay7l0rfvqh2ni")))

(define-public crate-heartfelt-0.1 (crate (name "heartfelt") (vers "0.1.3") (deps (list (crate-dep (name "ahash") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex-lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1w0v4ghmw5n058wq60rzmralgpbqny49dhhxh5ygqrdirf0bnz3i")))

(define-public crate-heartfelt-0.1 (crate (name "heartfelt") (vers "0.1.5") (deps (list (crate-dep (name "ahash") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "regex-lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hqffck874gk36akicnn1rrb8iml0v5x1836xycr381gj09jrk53")))

(define-public crate-hearth-0.0.0 (crate (name "hearth") (vers "0.0.0") (hash "0kj6ifyvw7k29mgrq2vh0d6yki6wykg44ywsxjgqsy06nslmxxz3")))

(define-public crate-hearth-interconnect-0.1 (crate (name "hearth-interconnect") (vers "0.1.0") (deps (list (crate-dep (name "schemars") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.160") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1ikb2dzqw17xq2jmvyw494h3lphx28jxkjppq36j50hb3bg858vi")))

(define-public crate-hearthstone-0.1 (crate (name "hearthstone") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1l2d5k1dnk23jh0dcm9xgyy6sdc13xxnxdlnykk861j7wf8sv3bg")))

(define-public crate-heartless-0.1 (crate (name "heartless") (vers "0.1.0") (deps (list (crate-dep (name "altio") (req "^0.2") (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "wait-timeout") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1d7whv5n9j0gdjhs8z0ljf1h0q1f8x5khxh3yf2980ij2bl2g26r") (features (quote (("test-replica") ("altio" "altio/altio"))))))

(define-public crate-heartless_assets-0.1 (crate (name "heartless_assets") (vers "0.1.0") (hash "19kha7n8j132q0mxh7lizpf92cixfxr059803cvydcgraipx4i97")))

(define-public crate-heartless_tk-0.1 (crate (name "heartless_tk") (vers "0.1.0") (deps (list (crate-dep (name "bind") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "heartless") (req "^0.1") (features (quote ("altio"))) (default-features #t) (kind 0)) (crate-dep (name "heartless_assets") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tcl") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "tk") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "145ij70il7nwma5gw1cak4y88m6458w8yxcsw4x6zmcvl9njf93d")))

(define-public crate-hearts-0.1 (crate (name "hearts") (vers "0.1.0") (hash "0zdr3z6drnb0wcs8l8i9fml7r932as5kwyfm2n0amxlzqrn66gi0")))

(define-public crate-heartwood-0.1 (crate (name "heartwood") (vers "0.1.0") (hash "1w14k7j423kiy8a9yidk44bgx1pya4qmv3mjhsjmsbsf5bihjs2w")))

(define-public crate-heartwood-0.1 (crate (name "heartwood") (vers "0.1.1") (hash "0jgypygc54zh5cl8gfaglywn0k3rns40zlsbrpjyffmj1q1h3yy1")))

