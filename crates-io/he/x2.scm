(define-module (crates-io he x2) #:use-module (crates-io))

(define-public crate-hex2ascii-0.1 (crate (name "hex2ascii") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.21.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1qmn596pm5y4p8kdwg00ij11zg6i3l7jg7mi9gdk0w1cnzxswx3l")))

(define-public crate-hex2ascii-0.2 (crate (name "hex2ascii") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.21.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "1n9wm303al7gcyzdqvf4rav287a82fzz9vyvl20b5lazi319z3cy")))

(define-public crate-hex2ascii-0.2 (crate (name "hex2ascii") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.21.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "1z3qqh0i60sb7j99vfggfn9ikw9z233czqr1w138msm9rxgy89w2")))

(define-public crate-hex2ascii-1 (crate (name "hex2ascii") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.21.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "0ypdq2fx468kb9zl4mym145s0f8hwf7qqna9gvqi2m7fdw5i6rrn")))

(define-public crate-hex2ascii-1 (crate (name "hex2ascii") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.21.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dhb6ymdgqa09m27y7ca93q3701swzxbyvnjw2x5k41h37b85zd2")))

(define-public crate-hex2ascii-1 (crate (name "hex2ascii") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.21.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "03499dg9gzzacpk385dmja8rwshpvk94bc8yimnqzlpx2mw15fcf")))

(define-public crate-hex2bytearray-0.1 (crate (name "hex2bytearray") (vers "0.1.0") (hash "13dcjb44gjwyjmm4m5mg69cnvqkjkdjbgda9gjxz81k8cnsp1lzm")))

(define-public crate-hex2d-0.0.3 (crate (name "hex2d") (vers "0.0.3") (deps (list (crate-dep (name "num") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1pg7lg0bk80q9pbbjxjpdbyp9n6ji0p22410068rs9qcrrxylmp4")))

(define-public crate-hex2d-0.0.4 (crate (name "hex2d") (vers "0.0.4") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0i0fb95i96km2zag7gj7ih1473rhz27k549n1s2ygml5clrn6380")))

(define-public crate-hex2d-0.0.6 (crate (name "hex2d") (vers "0.0.6") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1w8bvwq2v9dbgwfnwf7aqyvli6i6q2466x3yc5vvsddyp9q1dcx9")))

(define-public crate-hex2d-0.0.7 (crate (name "hex2d") (vers "0.0.7") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1m3y94m65bkx81dq6g3hvln10qgmw69qxbgn7pq695fcbmqknl1l")))

(define-public crate-hex2d-0.0.8 (crate (name "hex2d") (vers "0.0.8") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "00r08zgz4hyhzipbhda6asfx2w0z4xv5f1qzad306nqv3h27ik38")))

(define-public crate-hex2d-0.0.9 (crate (name "hex2d") (vers "0.0.9") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1ijrvxr5dgchccsy24r3z5f6apmp2rhvfgkcaz016q7map6jjg52")))

(define-public crate-hex2d-0.0.10 (crate (name "hex2d") (vers "0.0.10") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1vqxlnyndgcz1vqsza5h7rn32yrgvnlzw0am7mji1ljma6720ard")))

(define-public crate-hex2d-0.0.11 (crate (name "hex2d") (vers "0.0.11") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0gz9byzb5cf9zxa166kfkyzsfwdz3yv69ngw2xvdzn4nmf0svlxk")))

(define-public crate-hex2d-0.0.12 (crate (name "hex2d") (vers "0.0.12") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "18daw7saj886rnc2sb4lwamcivac0g3h5ak0nb7b3f4bg8nsfpgf")))

(define-public crate-hex2d-0.0.13 (crate (name "hex2d") (vers "0.0.13") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1qxswzgqyl0mr5kvpwkb5ci8si4mnrw99cn18j8jn397fd20pcqg")))

(define-public crate-hex2d-0.0.14 (crate (name "hex2d") (vers "0.0.14") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1pw60gnxhmrhg8ivcf5vihsaysrr5v6cnz002w4sw30i5ihsszc9")))

(define-public crate-hex2d-0.1 (crate (name "hex2d") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "19mlf1dbclbhcyh106qkhnd403lyas2bypyf8mz8m95n59qpkwnz")))

(define-public crate-hex2d-0.2 (crate (name "hex2d") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vq67k6bk1ra5anxkj2j7h0rdw7wkssyjcs0lkjh1pb1m8l9yi0v")))

(define-public crate-hex2d-0.3 (crate (name "hex2d") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jsqn5hak60jz9x62aqy6jfyvb36azivsbpm5gn3i2xas0hpz9l9") (features (quote (("serde-serde" "serde" "serde_derive") ("default" "serde-serde"))))))

(define-public crate-hex2d-1 (crate (name "hex2d") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1x3jzsy1kjzqljy3p6x8wwnjg4qwlhssm5bd0zw7n3kk360g5343") (features (quote (("serde-serde" "serde" "serde_derive") ("default" "serde-serde"))))))

(define-public crate-hex2d-1 (crate (name "hex2d") (vers "1.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "12z07byxpz7n7j5hgj6gj07h131d297l1zvi472nd7cadrmqjfq4") (features (quote (("serde-serde" "serde" "serde_derive") ("default" "serde-serde"))))))

(define-public crate-hex2d-dpcext-0.0.1 (crate (name "hex2d-dpcext") (vers "0.0.1") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "075pj8k384h8nn6yl6p20qxrw7g5lw39f0b9zxzyr06v7z01m1g7")))

(define-public crate-hex2d-dpcext-0.0.2 (crate (name "hex2d-dpcext") (vers "0.0.2") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1z69wy5rq3pa1y5wmyjny34slcdb71v8bnl5r6wj9g9vvxbbwvwi")))

(define-public crate-hex2d-dpcext-0.0.3 (crate (name "hex2d-dpcext") (vers "0.0.3") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0di5dkw2g461rxs1kc9f30vkx8wkz0yhmrkds5aw9ilbh201slix")))

(define-public crate-hex2d-dpcext-0.0.4 (crate (name "hex2d-dpcext") (vers "0.0.4") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1rxrljmhwcn2kypnk577i1r9kzjz3w5gww2w7nk8jm98y7hq2wi4")))

(define-public crate-hex2d-dpcext-0.0.5 (crate (name "hex2d-dpcext") (vers "0.0.5") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1j4xm87vbd7wfh20iq4kg41fckh1ah048pyjxgcwg74lyzzhd30f")))

(define-public crate-hex2d-dpcext-0.0.7 (crate (name "hex2d-dpcext") (vers "0.0.7") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0r3c79wr18nmhf6fr52sbbwnaz1xl3asm97fw7mwnbpyb7p4y0pg")))

(define-public crate-hex2d-dpcext-0.0.8 (crate (name "hex2d-dpcext") (vers "0.0.8") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0wgfkxdqbhz8ph73glc7hz7kz65pxhh2l08s48py11pfa2cnpfqr")))

(define-public crate-hex2d-dpcext-0.0.9 (crate (name "hex2d-dpcext") (vers "0.0.9") (deps (list (crate-dep (name "hex2d") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "0s2qqf1p97a8wss4fgg3cz57jcxk4w1y5sdy9bjqblgp4r9na0zx")))

(define-public crate-hex2d-dpcext-0.1 (crate (name "hex2d-dpcext") (vers "0.1.0") (deps (list (crate-dep (name "hex2d") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1mhklcynmm5zaziiyb721kz6094v12n3zn2d8x6iwnr87331jm92")))

(define-public crate-hex2dec-0.0.1 (crate (name "hex2dec") (vers "0.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "1fdwzd99icm3mciafb2h3xvl82cfi36ra7fpafk0amnlhrlc9b0j")))

