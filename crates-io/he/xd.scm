(define-module (crates-io he xd) #:use-module (crates-io))

(define-public crate-hexdecode-0.1 (crate (name "hexdecode") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "11h0d7p80y753c74pvli2yahi9l1j36b9mizwrjc35lcr31d7glw")))

(define-public crate-hexdecode-0.2 (crate (name "hexdecode") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "1f9k5qnvsadg4i1gcsj6kb4pdh1zaqdacc8j1kf6w1bnvsjfy1mc")))

(define-public crate-hexdecode-0.2 (crate (name "hexdecode") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "0k3yqsa10bf8613rw09wlzmi9kjafjphbbsdz1xhxmz918fcx8jg")))

(define-public crate-hexdecode-0.2 (crate (name "hexdecode") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "055angrs0nrk73283rqpm8157z606bilqxiw3882z89f13isrmgf")))

(define-public crate-hexdi-0.1 (crate (name "hexdi") (vers "0.1.0") (deps (list (crate-dep (name "iced") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0qzmag8636ckc795y831r36b8zd8lfcjagp8p59snjlk4fgwnkm0")))

(define-public crate-hexdi-0.1 (crate (name "hexdi") (vers "0.1.1") (deps (list (crate-dep (name "iced") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1prbxwzyxpjgzz8kmrkc9lf5xrvj8dl0liw24vi32g55farln8nr")))

(define-public crate-hexdino-0.1 (crate (name "hexdino") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.91") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "10lsn51rg6srbc1hrxkamr80334sdsvf5lywc921h5d9ygkjq8h4")))

(define-public crate-hexdino-0.1 (crate (name "hexdino") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.98") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "1al29xha1gc3qri0dglbqzah10z4z8dk3dknm2znfrrl32yfrps1")))

(define-public crate-hexdino-0.1 (crate (name "hexdino") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "1l8bcwva03m1xycdnmfhr4anv41gbaldpkp0k2zkyrajka38z9il")))

(define-public crate-hexdino-0.1 (crate (name "hexdino") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memmem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "0l1h0jdw4nqc07pkb5sjb25f3jpnqs3g6ccyw3krz0nm3gxz9b9v")))

(define-public crate-hexdmp-0.1 (crate (name "hexdmp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "1kl7wnql12clk4hj4dr8qqga3f6fvpxybdxlwhi2dh9dmqg9f6gm")))

(define-public crate-hexdmp-0.1 (crate (name "hexdmp") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "0qp6w4z1lf381gf2kyykc47a25h4kdf3x747npqljpny9igwm3rh")))

(define-public crate-hexdmp-0.1 (crate (name "hexdmp") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "0l41g7x7xk12lrg3awjfmzdc34l9fvi91dfln1qzskgym7iphh5w")))

(define-public crate-hexdump-0.1 (crate (name "hexdump") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req ">= 0.3.0, < 0.5.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.26") (optional #t) (default-features #t) (kind 0)))) (hash "0ar677vsqhjar3sfp209f8fh9zw0smyhhpj8qjb0y36j6cn3y3w5") (features (quote (("nightly-test" "quickcheck" "quickcheck_macros"))))))

(define-public crate-hexdump-0.1 (crate (name "hexdump") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req ">=0.3.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.27") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.26") (optional #t) (default-features #t) (kind 0)))) (hash "1wh555ab0c570fmkbng1jamy9d0pgdqivgkqi1vszwq2vgd860p4") (features (quote (("nightly-test" "quickcheck" "quickcheck_macros"))))))

(define-public crate-hexdump-0.1 (crate (name "hexdump") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)))) (hash "0cnqvrrjw332ksdzci7lrisbscb15asfkn3v8b3s2ic1xmkancfg")))

(define-public crate-hexdump-lt-0.1 (crate (name "hexdump-lt") (vers "0.1.0") (hash "0c11aj2mxkpfj55jlxpqv2s64advn735b3miq5nkaw00n8c5fy7a")))

(define-public crate-hexdump-lt-1 (crate (name "hexdump-lt") (vers "1.0.0") (hash "0r5p379wyi75czk68707s41ymkm4avcjcr2r3dakf215csfwlcjf")))

(define-public crate-hexdump-lt-1 (crate (name "hexdump-lt") (vers "1.0.1") (hash "19qk20gw24gizlq42lncyzsp50glrpv4i8z7297hnviv3hbx0wih")))

(define-public crate-hexdump-lt-1 (crate (name "hexdump-lt") (vers "1.0.2") (hash "09dhzzy0gcalcbqwfj2iv8cygb478v3phnf96a3z5n9msr00riqa")))

