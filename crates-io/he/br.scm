(define-module (crates-io he br) #:use-module (crates-io))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.0") (hash "0c09zb0jz0x0m9mifrp4k59fac3ld56j36zcjld15pplgcqcd27p") (yanked #t)))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.1") (hash "0x1rj4rdb0r47693m1xflksldil1pjidzbxgs5m8gm6c6bq52nxm")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.2") (hash "1v54164zzn9ayx1i8bldqr53vh4kklpzhkgwhfrxpq97wjxf8brj")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.3") (hash "0xkx25xra9jmp70q49h7xza1wcp353vwm1xyj59l2w6rhv9pk98j")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.4") (hash "1p66445ja7ybnqx6zjdfif0557lkpirwyhjqzb0p3im2s5sykmzm")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.41") (hash "0s6ikhcbmclr17y79wd97644136fd1q0fvcf8lpvlvyp7gz7vq39")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.42") (hash "19kpjjcg27npk5zrsgpxc8cfk9wqdjc3apjv2ivb9gk2xzcmm4xr")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.43") (hash "0922gapm4bnm7b96vr5jm6ir17jdkxp94dcnjffplgxrqvrnfbq5")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.44") (hash "00yvd67i248ri9v6v679z0v3667ci8rxsw64wzcqxw1whji81mad")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.45") (hash "0vw0rgagsb1ciqfcgsvdb1alji9bqv899j7ggcacdf1y5xyd0c7l")))

(define-public crate-hebrides-0.1 (crate (name "hebrides") (vers "0.1.46") (hash "0k2rcfdphp4nlla256bn8xbzsph1z90gmwmivnbr7z64z1zppznr")))

