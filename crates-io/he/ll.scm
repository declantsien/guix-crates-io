(define-module (crates-io he ll) #:use-module (crates-io))

(define-public crate-hell-0.1 (crate (name "hell") (vers "0.1.0") (deps (list (crate-dep (name "dope") (req "^0") (default-features #t) (kind 0)))) (hash "1s32yrkmrxrvfl7vhilcpqgjgq0755j96g37rh5apn6n1wqzscsj")))

(define-public crate-hellcheck-0.1 (crate (name "hellcheck") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.12.18") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "openssl-probe") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "tokio-timer") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "17v3q6ybsn10vakaxpk044jgydgjnb3givlnpspf514m3yciayam")))

(define-public crate-hellcheck-0.1 (crate (name "hellcheck") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.12.18") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "openssl-probe") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "tokio-timer") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1wjpqzm13szhn7a1v6jsvf0d92x1hnlr9h147qggbb3nf9vlx530")))

(define-public crate-hellcheck-0.1 (crate (name "hellcheck") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.12.18") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "openssl-probe") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "tokio-timer") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "058jf1cm23z3xsxxmqswmd231qgsdd3wf92xqhfzcfvw3y8dyxyz")))

(define-public crate-helldive_rs-0.3 (crate (name "helldive_rs") (vers "0.3.1") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1lpw340qripfa82k8ryvdps3lj3pgvmn3mlb77jc10p5gbasrivq")))

(define-public crate-helldive_rs-0.4 (crate (name "helldive_rs") (vers "0.4.0") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1d802j73pn1qfjnbxdyd8bfdnr2372q80vvhn5z1y1c0swg749q6")))

(define-public crate-helldive_rs-0.5 (crate (name "helldive_rs") (vers "0.5.0") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "01iilff2ybrl0i6c7plffijwqvc8xmrpf54qh13jrs9p6raw0s47")))

(define-public crate-helldive_rs-0.6 (crate (name "helldive_rs") (vers "0.6.0") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1kiz1iirxf6b6chnwmgj8njk9wb2lyyj9kxzlnvwaa4wwz22khy6")))

(define-public crate-helldivers_rs-0.1 (crate (name "helldivers_rs") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1l1w1m4ajpgkdxdvl62xj4jasg7xkgxfglq0sls74xxh81fxcbss") (yanked #t)))

(define-public crate-helldivers_rs-0.2 (crate (name "helldivers_rs") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1vfh4472m0xwx1r0qhaq8qjb7lwb38yl4laf2bajjh7jzrp1rjfq") (yanked #t)))

(define-public crate-helldivers_rs-0.3 (crate (name "helldivers_rs") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1rhfdn1iblcph1vpggb3w2by57cfnkin6dr277gw8my8z7h7hlcv") (yanked #t)))

(define-public crate-helldivers_rs-0.3 (crate (name "helldivers_rs") (vers "0.3.1") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0iazn7xs1vianalx5g3z436ch3idkf7z72my1i66rmpby7igpmnc") (yanked #t)))

(define-public crate-helldivers_rs-0.3 (crate (name "helldivers_rs") (vers "0.3.2") (deps (list (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0bgd7x9n5skwf85l8saigqba6xpc7xfqynivhn8aapv31gs5h5l0") (yanked #t)))

(define-public crate-hellfish-0.1 (crate (name "hellfish") (vers "0.1.0") (hash "114q200ivr9fmbxiqv7zf8af2w4npabqf6mc6klp5vji4dkxqhin")))

(define-public crate-hello-1 (crate (name "hello") (vers "1.0.0") (hash "1cbck1m9wc6f64iwh5cnz178m7n2scxxkvfpjh5ykpcqbr8csvr5")))

(define-public crate-hello-1 (crate (name "hello") (vers "1.0.1") (hash "1skacwlycwn30r23w67dxalx7s8y5w087aqgnmlpsm1vjjgz7n32")))

(define-public crate-hello-1 (crate (name "hello") (vers "1.0.2") (hash "1z7lvzx1l8fsyc3z0akghxad82yp65qs39d5rkpxkfqf65ng351b")))

(define-public crate-hello-1 (crate (name "hello") (vers "1.0.3") (hash "1f0ynk0ahhxfydix0vyl10a3icyk5v40dnwhs2p03n7q67m81cag")))

(define-public crate-hello-1 (crate (name "hello") (vers "1.0.4") (hash "10vay1ia77pclip60ajmnrljx7ab3namyihf927l0gsq7wswsxz6")))

(define-public crate-hello-adamben-0.1 (crate (name "hello-adamben") (vers "0.1.0") (hash "1bbnmir9083xrdk76hjji27fyj1s405a2mlfhacbf3j1wddh1g5w") (yanked #t)))

(define-public crate-hello-adamben-0.1 (crate (name "hello-adamben") (vers "0.1.1") (hash "0nzxmagbbvf4cqh9qpvkv6bnf76a0r076njpy24kwx2qkawnqhbs") (yanked #t)))

(define-public crate-hello-bh90210-0.1 (crate (name "hello-bh90210") (vers "0.1.0") (deps (list (crate-dep (name "ferris-says") (req "^0.2") (default-features #t) (kind 0)))) (hash "18wkjad9xr8bpwskavav3gl6i7nklki5agv37ssd8xw5fmxp6s7c")))

(define-public crate-hello-bin-0.0.0 (crate (name "hello-bin") (vers "0.0.0") (hash "08758mszpv1qbbw10h1rrcdgi83bvw4vb2n7gr9qwapn0bydhani")))

(define-public crate-hello-cargo-from-a-rookie-0.1 (crate (name "hello-cargo-from-a-rookie") (vers "0.1.0") (hash "1jngv391bpfz4g60rqmdfiypf89x6x7cx94qm1xmvhdv0g3i8gq0")))

(define-public crate-hello-china-0.1 (crate (name "hello-china") (vers "0.1.0") (hash "14sp2pls24bl7aln2hyry240fr0hl7i7aq62rq1jj83hgzj4p4d6") (yanked #t)))

(define-public crate-hello-china-0.1 (crate (name "hello-china") (vers "0.1.1") (hash "1k6fdxm6mb5z9n2jv0bmws181mpin6m898cbwd2l4qx54m1viy4m")))

(define-public crate-hello-cli-0.1 (crate (name "hello-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)))) (hash "155l2xdac4l96afi7sb5m4wb7kfyzgs97fyg20pjkyfmchbr8vpn")))

(define-public crate-hello-cli-0.1 (crate (name "hello-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)))) (hash "1x3ghnb2j562fpqb6a565mhlx6i07m5d5cadwqbc5g9d45bm41nc")))

(define-public crate-hello-cli-0.1 (crate (name "hello-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)))) (hash "17zazijkdplp6rgf5dgxbbrx6bg6x6a6xjyi9yrg0vf028qwajr0")))

(define-public crate-hello-cli-0.1 (crate (name "hello-cli") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)))) (hash "1i5i20rycc75hv7kwmpjw5rjp8vxrl8zxq69fqvnjfbqdkwwxi7a")))

(define-public crate-hello-cli-0.2 (crate (name "hello-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)))) (hash "1d9r093vx6pdhb2bjxwdq74jz99dcl6lq7khnmfa0rcmx9vrqcyy")))

(define-public crate-hello-cli-0.2 (crate (name "hello-cli") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)))) (hash "0npxbbcl4gks5ixhcgcnl519dzz12r7z4vl3dcmvypjmdarw4gw2")))

(define-public crate-hello-cli-0.2 (crate (name "hello-cli") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.26.2") (default-features #t) (kind 0)))) (hash "0qilj9b94ig7z160kazk41k2iy1hprdk5qwaym4fnf5x9fiksild")))

(define-public crate-hello-crates-0.1 (crate (name "hello-crates") (vers "0.1.0") (hash "17pynniify21nh6vfjr4p17ivv7wjrnsarjj9d1x3wn0sw7a94gk")))

(define-public crate-hello-crates-grrs-0.1 (crate (name "hello-crates-grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1.5") (default-features #t) (kind 2)))) (hash "0cficbqg10z3g750j39b67va2fbw3dy02zyz0i1n7h9f4g2ww675")))

(define-public crate-hello-dipto-0.1 (crate (name "hello-dipto") (vers "0.1.0") (hash "08nfzpfsdwnfiam5zwckv1aca6aaj5y7bd6wvpbsam1sw81jndf1") (yanked #t)))

(define-public crate-hello-fn-0.1 (crate (name "hello-fn") (vers "0.1.0") (hash "1k34zqrng51rjysckan0fi7rz5yd6f8aw1gm4g37n9v9a4a2d13z")))

(define-public crate-hello-foo-rs-0.1 (crate (name "hello-foo-rs") (vers "0.1.0") (hash "0cafw949c117lyawxcrb1gdxr51mfjxf2yd8m4np47mm9cnfrac4")))

(define-public crate-hello-from-generated-code-0.1 (crate (name "hello-from-generated-code") (vers "0.1.0") (hash "1wvxl70z6q8713l9f7jbf116fidxjxl9xr1lz83fzr7nrwhwrd1c")))

(define-public crate-hello-gosim-0.1 (crate (name "hello-gosim") (vers "0.1.0") (hash "176gjcfbn7pw7y5adhavxy540xisi0qdm7l6pjvlwfijkqgq5pas")))

(define-public crate-hello-phext-0.0.6 (crate (name "hello-phext") (vers "0.0.6") (deps (list (crate-dep (name "impl_new") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ri5n7hf7zcdmx2k41h1j86lj0v6hmq0anmd03z9ksizizbhl860")))

(define-public crate-hello-rs-0.1 (crate (name "hello-rs") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv_codegen") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "egg-mode") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "fantoccini") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "rawr") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "slack_api") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "0vq4cmglk0a9xrhkabjznzhhfxd3wcha1j1gxzv8vzpxnrwdaciz")))

(define-public crate-hello-rust-0.1 (crate (name "hello-rust") (vers "0.1.0") (hash "1kmi39z84xbjns2kf21giysfhwhn7jpz8ah561fw22y4nsids4cx")))

(define-public crate-hello-rust-heru-0.1 (crate (name "hello-rust-heru") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "07jy08chi4x269yihldf7hv978crpwaqkfxlj1kzhqaqy55a967g")))

(define-public crate-hello-rust-olso-0.1 (crate (name "hello-rust-olso") (vers "0.1.0") (hash "08v2b7qyjy90jw1f7qnvihdv4kw0cn4wcp8vfdss89pnvm97dzqw")))

(define-public crate-hello-soroban-kit-0.1 (crate (name "hello-soroban-kit") (vers "0.1.7") (deps (list (crate-dep (name "soroban-kit") (req "^0.1.7") (features (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker"))) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "008m491kyrarq987axbh5xh0047iqgam4mfrazd4wa1mb0306wn6")))

(define-public crate-hello-soroban-kit-0.1 (crate (name "hello-soroban-kit") (vers "0.1.8") (deps (list (crate-dep (name "soroban-kit") (req "^0.1.8") (features (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle"))) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "1an4yid1cd7cwglhvqfhd2v302sll7w1x347wml6w83mfjpmcl0l")))

(define-public crate-hello-soroban-kit-0.1 (crate (name "hello-soroban-kit") (vers "0.1.9") (deps (list (crate-dep (name "soroban-kit") (req "^0.1.9") (features (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle" "utils"))) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "1r649iv4826cagcrywxxqrsv1vxxaqpgx6pahlwgc091c24pdrnw")))

(define-public crate-hello-soroban-kit-0.1 (crate (name "hello-soroban-kit") (vers "0.1.10") (deps (list (crate-dep (name "soroban-kit") (req "^0.1.10") (features (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle" "utils"))) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.3.1") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.3.1") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "112hdg6zgb4z42xiqphv7xi4pk7n3n8is7k8k154lr30mgf26hkb")))

(define-public crate-hello-soroban-kit-0.1 (crate (name "hello-soroban-kit") (vers "0.1.11") (deps (list (crate-dep (name "soroban-kit") (req "^0.1.11") (features (quote ("commitment-scheme" "storage" "state-machine" "circuit-breaker" "oracle" "utils"))) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.3.2") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.3.2") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "1ddzy0cj6scs3y9wd8rgqd3ppjiqzm6m1ab00z2b27zhvib549dn")))

(define-public crate-hello-squirrel-0.1 (crate (name "hello-squirrel") (vers "0.1.0") (hash "0n7xvwbjyqbvpfdmyg3c10iylg64rlmc5m9557c4chycqdhp6zkj") (yanked #t)))

(define-public crate-hello-virus-0.1 (crate (name "hello-virus") (vers "0.1.0") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "1z32nsvhn1n7j8c7h53yphs71iacbv2imcrhah79likik8i9psg6")))

(define-public crate-hello-virus-0.1 (crate (name "hello-virus") (vers "0.1.1") (deps (list (crate-dep (name "pyu_rust_util") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "128qamdszqk0p8p6z59qwrc4bwf34mgs6zxp7na4yw9dr4zmkqvv")))

(define-public crate-hello-wasm-0.1 (crate (name "hello-wasm") (vers "0.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "143dbnzz26gk582xg0fnvqgc4spf0vpgpgp5f251fbdym6k7lsq1")))

(define-public crate-hello-world-0.1 (crate (name "hello-world") (vers "0.1.0") (hash "0naac6ygr3wwbr0j045brn88hs4b14167qqm1n3vcf5hlh686g6s")))

(define-public crate-hello-world-1-0.1 (crate (name "hello-world-1") (vers "0.1.0") (hash "0bwaln73wvlak67jvmyiki850bcic47zl9h8zdlsbvdx9sljqxjs")))

(define-public crate-hello-world-1-0.1 (crate (name "hello-world-1") (vers "0.1.1") (hash "14z54mjjw7b68arv9zj45mmidfizdw4kncjnf122flfny3m0s5g5")))

(define-public crate-hello-world-1-0.1 (crate (name "hello-world-1") (vers "0.1.2") (hash "0il1bdc0p47dzayiz8m7ikx6i9p1xh6wmvpq3fnx5xp28p89i8nm")))

(define-public crate-hello-world-2022-10-01-0.1 (crate (name "hello-world-2022-10-01") (vers "0.1.0") (hash "1qzw53qdn0f8cqhy8m6v1ag6fys5vxlyspv7j09x5i5xi3kpmn71") (yanked #t)))

(define-public crate-hello-world-42-0.1 (crate (name "hello-world-42") (vers "0.1.0") (hash "1izg6qp3yya3484q0k327alxbsn33mqh5ms6asw66p7mw035cskh")))

(define-public crate-hello-world-42-0.1 (crate (name "hello-world-42") (vers "0.1.1") (hash "14ql092mpbyzvvw0n52j5ygnkxz8d6ify9318gvfhr3byzgkdmmn")))

(define-public crate-hello-world-a-0.1 (crate (name "hello-world-a") (vers "0.1.0") (hash "086vyiaxpmw18mnkjnjrx0dp3hibrcggg35c9h1k3gsa4xxy686y") (yanked #t)))

(define-public crate-hello-world-aaaaa-0.1 (crate (name "hello-world-aaaaa") (vers "0.1.0") (hash "0nc6pxgg8nf2aqc51py6lypbqr50ng47bv93lcfz3m76ql4b6vnh")))

(define-public crate-hello-world-cargo-rust-practise-techlflow123-0.1 (crate (name "hello-world-cargo-rust-practise-techlflow123") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1970bfrpz1r3qzp510g6rrnk4j2bf37k46z5xzm5k2pknb88mzml")))

(define-public crate-hello-world-in-rust-0.1 (crate (name "hello-world-in-rust") (vers "0.1.0") (hash "0sh8aiyaxvlj92l71l6p7psyyix8gc5cizh5wv5870a1aiwsnz11")))

(define-public crate-hello-world-jjr-0.1 (crate (name "hello-world-jjr") (vers "0.1.0") (hash "1vxawmb1nr5r1cpy1ms4fpvs0qixi8pc15pzrkjbs454gxzq838a")))

(define-public crate-hello-world-poharik-0.1 (crate (name "hello-world-poharik") (vers "0.1.0") (hash "1ndv4xwqa1r1h0l5pyslvxrydzjki492959bv2d1cc7wy88kdhbl")))

(define-public crate-hello-world-polish-0.1 (crate (name "hello-world-polish") (vers "0.1.0") (hash "1mir3lmk715lb6mlrq8aqjn93gqkz98bvm9a2ybqwqi2sj8qsl0m")))

(define-public crate-hello-world-redbaron-0.1 (crate (name "hello-world-redbaron") (vers "0.1.0") (hash "0qxxrkfm8yysl9i7x9x53r24zlgibslx0jr551b4rlp9danjjpzq")))

(define-public crate-hello-world-roger-0.1 (crate (name "hello-world-roger") (vers "0.1.0") (deps (list (crate-dep (name "cosmwasm-schema") (req "^1.0.0-beta6") (default-features #t) (kind 2)) (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta6") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-storage") (req "^1.0.0-beta6") (default-features #t) (kind 0)) (crate-dep (name "cw-multi-test") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "cw-storage-plus") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "cw2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zcxx80r8a0q89nrzg5d73jrlv0b97c3pcs5skqrx8krha3z7nv1") (features (quote (("library") ("backtraces" "cosmwasm-std/backtraces"))))))

(define-public crate-hello-world-rs-0.1 (crate (name "hello-world-rs") (vers "0.1.0") (hash "1dh5hznlvf4gys48m9j2g2zb9fhw5gkk4cw1m4khc93l7wqkyl2r")))

(define-public crate-hello-world-rs-0.1 (crate (name "hello-world-rs") (vers "0.1.1") (hash "1i92cx8nkq9xkmz0kwgx945yzrn9b14k9czsl6fl5c1fxxlmy0g4")))

(define-public crate-hello-world-rs-0.1 (crate (name "hello-world-rs") (vers "0.1.2") (hash "0ryrvbcg12rlgm6zxfs4fh333aimlhnzh20x14z2k8l1jyn5wmr4")))

(define-public crate-hello-world-rs-0.1 (crate (name "hello-world-rs") (vers "0.1.3") (hash "0b48l99w4z3ypshpv4yanvadllalf6cvp913jdjl3lq6c09bjxp3")))

(define-public crate-hello-world-rs-0.1 (crate (name "hello-world-rs") (vers "0.1.4") (hash "119b3l4njaif6bnm0f36h0pbhwg9abdfpbwd7hivnygzagwl5828")))

(define-public crate-hello-world-rs-1 (crate (name "hello-world-rs") (vers "1.0.0") (hash "1fiffkhpik79qdkpjlrxaz8xx2r7nlh067prbgc1liznssf9jmlm")))

(define-public crate-hello-world-sfngmt-0.1 (crate (name "hello-world-sfngmt") (vers "0.1.0") (hash "04x6ybk32zyd5wwilp59jpszpvcavmhivk73lhxyk0qaj7wc96lp") (yanked #t)))

(define-public crate-hello-world-x-rust-x-library-0.1 (crate (name "hello-world-x-rust-x-library") (vers "0.1.0") (hash "1zn1bvdyrzmigfqic9gppxn9h3avzf527f5r6d5q22dmsrxjnqw9") (yanked #t)))

(define-public crate-hello-world-x-rust-x-library-0.1 (crate (name "hello-world-x-rust-x-library") (vers "0.1.1") (hash "18zs9rfcm28s0z1px12pc3h6grla5zjhny4169kyfg8y6d1xg29j") (yanked #t)))

(define-public crate-hello-world-x-rust-x-library-0.1 (crate (name "hello-world-x-rust-x-library") (vers "0.1.2") (hash "1960690swnf3yb3qww0yj5p480jkgnd32wn2jd39l967mc2bgvqx") (yanked #t)))

(define-public crate-hello_bash_plugin-0.1 (crate (name "hello_bash_plugin") (vers "0.1.0") (deps (list (crate-dep (name "bash_plugin_rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1iy4xdfl95zblw7vc328zh2893r69lrl21361ahjf5rcx06j7v2c")))

(define-public crate-hello_borer-0.1 (crate (name "hello_borer") (vers "0.1.0") (hash "0cc6dbkd4rjngzysp1iww654aili33vdy7y1asm5l2ghijgkrrrw")))

(define-public crate-hello_cargo-0.1 (crate (name "hello_cargo") (vers "0.1.0") (hash "12v3m0k2y6yrjaxrrjahlb0ikb3qa3g9bfrmcv6g88haznjri6ql")))

(define-public crate-hello_cargo_arst-0.1 (crate (name "hello_cargo_arst") (vers "0.1.0") (hash "1yih2f3ggjrz85j5mr6f836zkk7957n7bzk3hzdmjvdm58i5px5v")))

(define-public crate-hello_cargo_h-0.1 (crate (name "hello_cargo_h") (vers "0.1.0") (hash "0qbbgmmd6dhlspj8g5wm1whlbzlv4qjv1qmghc0dix50aa2s9m0q") (yanked #t)))

(define-public crate-hello_chiago-0.1 (crate (name "hello_chiago") (vers "0.1.1") (hash "078f4w0jrbxjlhfm8xggj01sdxx8a4briiqr8ms979q9hs17li2s")))

(define-public crate-hello_closures-0.1 (crate (name "hello_closures") (vers "0.1.0") (hash "0jddlsbvzyqlmdknk5ll8wfddwmq9ajim0r2p4f9zq69yv54mq9a")))

(define-public crate-hello_demo_cargo-0.1 (crate (name "hello_demo_cargo") (vers "0.1.0") (hash "06z83yzlzmh3jrsz4lbksqj5zpvaibbizq9dp746i4kdxsrwmpdg")))

(define-public crate-hello_devops-0.1 (crate (name "hello_devops") (vers "0.1.1") (hash "02y79855p32kf63v9sjq7jqg9grbx2yrrig1v91dsgx66v07r1xw")))

(define-public crate-hello_egui-0.1 (crate (name "hello_egui") (vers "0.1.0") (deps (list (crate-dep (name "egui_animation") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_dnd") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_inbox") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_pull_to_refresh") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_suspense") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0l3mk08bw86cqjjk6djnkai7q48hnzwcc0vjcb9v9cs4nlfnnfhk") (features (quote (("default" "dnd" "inbox" "pull_to_refresh" "animation" "suspense")))) (v 2) (features2 (quote (("suspense" "dep:egui_suspense") ("pull_to_refresh" "dep:egui_pull_to_refresh") ("inbox" "dep:egui_inbox") ("dnd" "dep:egui_dnd") ("animation" "dep:egui_animation"))))))

(define-public crate-hello_egui-0.2 (crate (name "hello_egui") (vers "0.2.0") (deps (list (crate-dep (name "egui_animation") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_dnd") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_inbox") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_infinite_scroll") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_pull_to_refresh") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_suspense") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_thumbhash") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_virtual_list") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0356wjz13sgimrl78i8kwlyr5zh4mv91b668gdxis39vdyfysr24") (features (quote (("tokio" "egui_suspense/tokio" "egui_infinite_scroll/tokio") ("async" "egui_suspense/async" "egui_infinite_scroll/async") ("all" "dnd" "inbox" "pull_to_refresh" "animation" "suspense" "virtual_list" "infinite_scroll" "thumbhash")))) (v 2) (features2 (quote (("virtual_list" "dep:egui_virtual_list") ("thumbhash" "dep:egui_thumbhash") ("suspense" "dep:egui_suspense") ("pull_to_refresh" "dep:egui_pull_to_refresh") ("infinite_scroll" "dep:egui_infinite_scroll") ("inbox" "dep:egui_inbox") ("dnd" "dep:egui_dnd") ("animation" "dep:egui_animation"))))))

(define-public crate-hello_egui-0.3 (crate (name "hello_egui") (vers "0.3.0") (deps (list (crate-dep (name "egui_animation") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_dnd") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_inbox") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_infinite_scroll") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_pull_to_refresh") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_suspense") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_thumbhash") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_virtual_list") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0bp98jr70l0jzb1nfnjqbmmhfwjclqblig06flscmvnw605kn89z") (features (quote (("tokio" "egui_suspense/tokio" "egui_infinite_scroll/tokio") ("async" "egui_suspense/async" "egui_infinite_scroll/async") ("all" "dnd" "inbox" "pull_to_refresh" "animation" "suspense" "virtual_list" "infinite_scroll" "thumbhash")))) (v 2) (features2 (quote (("virtual_list" "dep:egui_virtual_list") ("thumbhash" "dep:egui_thumbhash") ("suspense" "dep:egui_suspense") ("pull_to_refresh" "dep:egui_pull_to_refresh") ("infinite_scroll" "dep:egui_infinite_scroll") ("inbox" "dep:egui_inbox") ("dnd" "dep:egui_dnd") ("animation" "dep:egui_animation"))))))

(define-public crate-hello_egui-0.4 (crate (name "hello_egui") (vers "0.4.0") (deps (list (crate-dep (name "egui_animation") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_dnd") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_inbox") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_infinite_scroll") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_pull_to_refresh") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_suspense") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_thumbhash") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_virtual_list") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1k4cmflln3lrpgzrwsrcacqdzmfscn4wr8vjaq0qqkp4j1mbz5f3") (features (quote (("tokio" "egui_suspense/tokio" "egui_infinite_scroll/tokio") ("async" "egui_suspense/async" "egui_infinite_scroll/async") ("all" "dnd" "inbox" "pull_to_refresh" "animation" "suspense" "virtual_list" "infinite_scroll" "thumbhash")))) (v 2) (features2 (quote (("virtual_list" "dep:egui_virtual_list") ("thumbhash" "dep:egui_thumbhash") ("suspense" "dep:egui_suspense") ("pull_to_refresh" "dep:egui_pull_to_refresh") ("infinite_scroll" "dep:egui_infinite_scroll") ("inbox" "dep:egui_inbox") ("dnd" "dep:egui_dnd") ("animation" "dep:egui_animation"))))))

(define-public crate-hello_egui-0.4 (crate (name "hello_egui") (vers "0.4.1") (deps (list (crate-dep (name "egui_animation") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_dnd") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_form") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_inbox") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_infinite_scroll") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_pull_to_refresh") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_suspense") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_thumbhash") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_virtual_list") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jjv04632in5m1b1cj6yhciq5bmlkr83nskfqn76b4g4pxg6dy0s") (features (quote (("tokio" "egui_suspense/tokio" "egui_infinite_scroll/tokio") ("full" "all" "async" "tokio") ("async" "egui_suspense/async" "egui_infinite_scroll/async") ("all" "animation" "dnd" "form" "inbox" "infinite_scroll" "pull_to_refresh" "suspense" "thumbhash" "virtual_list")))) (v 2) (features2 (quote (("virtual_list" "dep:egui_virtual_list") ("thumbhash" "dep:egui_thumbhash") ("suspense" "dep:egui_suspense") ("pull_to_refresh" "dep:egui_pull_to_refresh") ("infinite_scroll" "dep:egui_infinite_scroll") ("inbox" "dep:egui_inbox") ("form" "dep:egui_form") ("dnd" "dep:egui_dnd") ("animation" "dep:egui_animation"))))))

(define-public crate-hello_egui-0.4 (crate (name "hello_egui") (vers "0.4.2") (deps (list (crate-dep (name "egui_animation") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_dnd") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_form") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_inbox") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_infinite_scroll") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_pull_to_refresh") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_suspense") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_thumbhash") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui_virtual_list") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pg0ivi89l8wyaikh97x0i2ngq037y80g2vij4f2x8l2xbxnm94r") (features (quote (("tokio" "egui_suspense/tokio" "egui_infinite_scroll/tokio") ("full" "all" "async" "tokio") ("async" "egui_suspense/async" "egui_infinite_scroll/async") ("all" "animation" "dnd" "form" "inbox" "infinite_scroll" "pull_to_refresh" "suspense" "thumbhash" "virtual_list")))) (v 2) (features2 (quote (("virtual_list" "dep:egui_virtual_list") ("thumbhash" "dep:egui_thumbhash") ("suspense" "dep:egui_suspense") ("pull_to_refresh" "dep:egui_pull_to_refresh") ("infinite_scroll" "dep:egui_infinite_scroll") ("inbox" "dep:egui_inbox") ("form" "dep:egui_form") ("dnd" "dep:egui_dnd") ("animation" "dep:egui_animation"))))))

(define-public crate-hello_egui_utils-0.1 (crate (name "hello_egui_utils") (vers "0.1.0") (deps (list (crate-dep (name "egui") (req ">=0.22") (kind 0)))) (hash "1c3wmr6l4z8dkji2ii7b76xhkpbgwspzqdnq9vp7jsz3hrqprirz")))

(define-public crate-hello_egui_utils-0.1 (crate (name "hello_egui_utils") (vers "0.1.1") (deps (list (crate-dep (name "concat-idents") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.25") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0134ci55b7zswsl6kp2jbawig2a19y55yw3j4amxanrk8caai2ka") (yanked #t) (v 2) (features2 (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

(define-public crate-hello_egui_utils-0.2 (crate (name "hello_egui_utils") (vers "0.2.0") (deps (list (crate-dep (name "concat-idents") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.25") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0r53q7cf98llsyq4dxbyhi57krag9r5klixh8rik90pr718nfcbq") (v 2) (features2 (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

(define-public crate-hello_egui_utils-0.3 (crate (name "hello_egui_utils") (vers "0.3.0") (deps (list (crate-dep (name "concat-idents") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.26.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1qjbk12hk301wv0bwf0i010391kkvj64kibly1wfs2z4zbzy5bw2") (v 2) (features2 (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

(define-public crate-hello_egui_utils-0.4 (crate (name "hello_egui_utils") (vers "0.4.0") (deps (list (crate-dep (name "concat-idents") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.27.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1i5k7dly5z6l27n2wqzyvflm2w7x8qd0qwqafwqjl1jznbg616w5") (v 2) (features2 (quote (("tokio" "async" "dep:tokio") ("async" "dep:wasm-bindgen-futures"))))))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.0") (hash "17dp7glqcyf9vppjhy7l9qm0j91cfiy893giajs6rlwx0kqw1fqv")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.2") (hash "0bi06dwyin9qgic0vdiy6nx1ahafb6lpxxnbkb834873q2fwf058")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.3") (hash "0x9zf4nlqkc6nn93jxsv8fk56rcviyyqfkx9wvhcd1dm0fk9w10x")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.4") (hash "11d2izpzjd4cd0dapjwr2h4sq714znwclw4m6zv6zq2znwd1a4nl")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.5") (hash "04c2r9qxms1a29cigxabaa5wvmsxybkzh0rsp0c8d7s0jax8472w")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.6") (hash "0sjswsx3xr1xwakpcnfnlbs5wsgmsi2f35vfb91fadk2ls6fvb5g")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.7") (hash "15rrnjsp1d783k680kwba0ysw25fp1lgcig2gj8d28l95h8mj766")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.8") (hash "0ixn6462c8fm0qgr3kbpda2pm7fl4mx2kn1bhxwzd7jzcb9mla2w")))

(define-public crate-hello_exercism-0.1 (crate (name "hello_exercism") (vers "0.1.9") (hash "03k2igmfdjxvvxlhvqcqfnm1kyf0wc0d8k84asclsk26jc639idz")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.0") (hash "0qxgscp94h2526zy51zvwwfbr0lcr8dlazggf58aba85jaya1ykb")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.1") (hash "058c8slvznk7kx577b9ga5ar5f2zha493lh3qkc891195r8b8hw9")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.2") (hash "1261j7kvc430q18lbkjvsmh1s4myk2y8midqv226g7r05425mz8h")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.3") (hash "0m7iqj4c6kjg9dsz54b4q7ics25jxggwj1kn55hynlyk1qjmld40")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.4") (hash "0pb1fi9fn8qc83ndyjmqxnn98fkcvvc71i8ldz16z7mnssv3w4r0")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.5") (hash "0zrdi4bj5skkyd0lhb1z1k9fqfq7hpllf68ij8x284mxixwwarpd")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.6") (hash "10pm0szlmci4z54d0ff55c28whrx1hh734932ig2s5abi73gxmw1")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.7") (hash "0rqqk5h0jj17z9xd8kvdm0gmqqgi8xymkwa3jgq98ldjm9p8xgpr")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.8") (hash "0s7gcznk11f8j9rcxcy3hdryix88wgcfbvk86s3djmy7pwxvb9rh")))

(define-public crate-hello_exercism-0.2 (crate (name "hello_exercism") (vers "0.2.9") (hash "0i6r6k520rqrd4hpgjcj3x7dqq4hf6sgml959nzxgpsqyfihnhmd")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.0") (hash "1mxpjyikqa1l3xnl4rcw2cwgqsqph44mjpld6cljnyysc9ljzwmq")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.1") (hash "1x8j0l4z9mz48plimhs9n9697y29cg9gimam9p27s33b68rm09j7")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.2") (hash "1c5vwhha1fzw173y21nvzgmlpa3rmdv856f9ap4hwwp6p1c5pq5i")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.3") (hash "0nv5nxhlyhzlgzzww99p979b9b2bfk49ivwp97lyii8wpv3dwmmp")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.4") (deps (list (crate-dep (name "i_crate") (req "^0.3.2") (default-features #t) (kind 0) (package "hello_exercism")))) (hash "061gkrsw13irznghcdyxj0xg5nm48h00ifsjazkl2i6yibipx8b4")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.5") (deps (list (crate-dep (name "i_crate") (req "^0.3.3") (default-features #t) (kind 0) (package "hello_exercism")))) (hash "1l15cwz5nyj2lpx3ar4sj6y4gc0jjcfbmn55g037mvryf955k3rf")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.6") (deps (list (crate-dep (name "i_crate") (req "^0.3.3") (default-features #t) (kind 0) (package "hello_exercism")))) (hash "1lfsjcdfk1nnjjl0kicnccxqqw6ymnv3zvirhd7zyv6padbsdkhy")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.7") (hash "1la7fwxnd9q07i0g503biargf1y9x4r6sw56ddmli1li54yli671")))

(define-public crate-hello_exercism-0.3 (crate (name "hello_exercism") (vers "0.3.8") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "0fajnm2iw500dw7hj33vhq5wb965xw05digx7706nbmy7ml5nhdf")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.0") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "0k583m7nyhpfp4psvk8gc1xhkpvmr4b0sy444d7g1m1rbinf6caz")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.1") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "0znlzkqjk6zjdbkgr8h2nszpffry1xaw1y2alf32knsdba8r98z8")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.2") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "0ap267bq9xw8aw20aqhnj9wv6acl76dy4dj2y9wxr2giq31fwh8a")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.3") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "14z0wcg6mj1lss6cmq3lrq3575iva6mcxasdbjnp8wvzkamlym1i")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.5") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "0sjmmi4h2qv50r8r762cywgwyx2niimrb0a6pmh73wix176ygfkv")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.6") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "01ay5s4wixnps6q21brcy5r1vdp28mj6v713a1yrp66d08sq83k5")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.7") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "00rcklxsqprj2zmvj7bi2g2skv8ri76ksmj25bvkww993fddcmla")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.8") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "09cj483lqa9461m8z5sx7bbxxfr48rwfjzr7a3i41pam99bl22pv")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.9") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "1vc52xc6ijl138d4sz2xw0c61jzw872v7f9dzzbs4s2zqcii5wap")))

(define-public crate-hello_exercism-0.4 (crate (name "hello_exercism") (vers "0.4.10") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "09rwzgs6p6ldjkbgbfhf7cdjc99g7ncff1blbrrfqwlldnc1yjax")))

(define-public crate-hello_exercism-0.5 (crate (name "hello_exercism") (vers "0.5.0") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "0c611b96mbq2j1661k5m4fdldszz6fd4377wb86b0r4pnqc9kznk")))

(define-public crate-hello_exercism-0.5 (crate (name "hello_exercism") (vers "0.5.1") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "11sfg27p1y8yq3xpqgqf1b0kh3pf481kwz7fc3xahqg5wbk7ziib")))

(define-public crate-hello_exercism-0.5 (crate (name "hello_exercism") (vers "0.5.2") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "19m8rplj6npxdfkqrnlfydikfr5r8bkizsqfjr5g717whqlz34js")))

(define-public crate-hello_exercism-0.5 (crate (name "hello_exercism") (vers "0.5.3") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "1dgchspm4swrrzhc15qywpc5ggqvabajwhn0bf16l70szi9nyky0")))

(define-public crate-hello_exercism-0.5 (crate (name "hello_exercism") (vers "0.5.4") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "1jj511xy4063gzlzjdrwazkh4pzxl41xd8gf8vffz8bf0afw4xg1")))

(define-public crate-hello_exercism-0.5 (crate (name "hello_exercism") (vers "0.5.5") (deps (list (crate-dep (name "i_crate") (req "^0.1.1") (default-features #t) (kind 0) (package "hello_extern")))) (hash "0s8mv918ys74x9x2adfzr7sadi08wcdg4y7piyi3ndalnlhs0p1p")))

(define-public crate-hello_extern-0.1 (crate (name "hello_extern") (vers "0.1.0") (hash "0234g2vajsrj8fxlxw1w3ckhjjkb00b31631dhfxz4340spbwk1x")))

(define-public crate-hello_extern-0.1 (crate (name "hello_extern") (vers "0.1.1") (hash "1nxbiq7c1a108x4l9rv6ib0y9z1abn6aa3yd04nfzbr23gjp4g1f")))

(define-public crate-hello_from_christian-0.1 (crate (name "hello_from_christian") (vers "0.1.0") (hash "08gg54w96vx8hqzb9pkzsyjzi11bnvkbydvfv4kr8qynny653cmk")))

(define-public crate-Hello_how_are_you-0.1 (crate (name "Hello_how_are_you") (vers "0.1.6") (hash "1fwg5vkjgg86xxfm06n32rr147m0h63il8w65jcv9pywq90q1z0c") (yanked #t)))

(define-public crate-hello_idc-0.1 (crate (name "hello_idc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.18") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.25.0") (features (quote ("axum"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "openidconnect") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "opentelemetry") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "opentelemetry-otlp") (req "^0.14.0") (features (quote ("grpc-tonic" "http-proto"))) (default-features #t) (kind 0)) (crate-dep (name "opentelemetry-semantic-conventions") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "opentelemetry_sdk") (req "^0.21.1") (features (quote ("rt-tokio"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)) (crate-dep (name "tower-cookies") (req "^0.9.0") (features (quote ("signed" "private"))) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "^0.4.4") (features (quote ("tracing" "trace"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-opentelemetry") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("fmt" "json" "env-filter" "std" "registry"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "14qidc180mndnyki60i8vl1p4fnx3vsyck1aarszx0jnilcay1a3")))

(define-public crate-hello_idc-0.1 (crate (name "hello_idc") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.18") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "maud") (req "^0.25.0") (features (quote ("axum"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "openidconnect") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "service_conventions") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("tls" "tls-roots"))) (default-features #t) (kind 0)) (crate-dep (name "tower-cookies") (req "^0.9.0") (features (quote ("signed" "private"))) (default-features #t) (kind 0)) (crate-dep (name "tower-http") (req "^0.4.4") (features (quote ("tracing" "trace"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0139vl68xsv86is2prxyrma48cbwwmrmz44knrgs5m7yciigrh7q")))

(define-public crate-hello_julianshu-0.1 (crate (name "hello_julianshu") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "08x2k0pbr2pc7phpw0qkf8pvgdf1dqiyz050hah5xf6bk15sa4jx")))

(define-public crate-hello_julianshu-0.1 (crate (name "hello_julianshu") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1cqxjjda7fnpq23p3dr8x5ak4412hs6dchdqmm0nl0hxp90xf4b1")))

(define-public crate-hello_julianshu-0.1 (crate (name "hello_julianshu") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1f1pml8317p5sjidr7xl02h7hjh5mmh5mjkmdfllp732jlb22r0p")))

(define-public crate-hello_lib-0.1 (crate (name "hello_lib") (vers "0.1.0") (hash "1b8r82bxbf410rmaxm008bcjjkj8sndf9401m3q7xrcjkhvdnk7x")))

(define-public crate-hello_lib-0.1 (crate (name "hello_lib") (vers "0.1.1") (hash "1yx2j9zc06h29lng3w1sja5zfr53isr2i4icnfjjkg06wk5am90j")))

(define-public crate-hello_lib-0.1 (crate (name "hello_lib") (vers "0.1.2") (hash "1qb6nvf7in5zzzc4iinyfykdmisl0v90x4v5acvxvwzfla5jpd5p")))

(define-public crate-hello_lib-0.1 (crate (name "hello_lib") (vers "0.1.3") (hash "0nqwxnw9nh1cs4d9ampdry71zfdm1736gjd0n1b61v21id71kqyn")))

(define-public crate-hello_lib-0.1 (crate (name "hello_lib") (vers "0.1.4") (hash "0191zly0wwk05f091jkn8j5ra2zvvdla14rk5y9szzm7jldbfsl1")))

(define-public crate-hello_lib-0.1 (crate (name "hello_lib") (vers "0.1.5") (hash "02lw33m1bibpxcsxdwi78n7kwhy2k95a9zx88p73d8lnnpd7grm6")))

(define-public crate-hello_lib-0.1 (crate (name "hello_lib") (vers "0.1.6") (hash "1xmcxb6f4vg24f01860kzbndgm8zifgcx03d2l9df05cm9mwr2hs")))

(define-public crate-hello_macro-0.1 (crate (name "hello_macro") (vers "0.1.0") (hash "1wjwmqih59mph62mwqxa5m0rjgj87a5izl8k8y1lgh58x3pjlbnx")))

(define-public crate-hello_macro_derive-0.1 (crate (name "hello_macro_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d6xnzf771269ybfnkm0yi8f9k1lklyyy5jma4mqsv2j7nh33sr8")))

(define-public crate-hello_maiwe-0.1 (crate (name "hello_maiwe") (vers "0.1.0") (hash "0n617jnz5a08lz0ymdvzw1q19jlm8ivfizcn0rvjyl7y4smlsb6n")))

(define-public crate-hello_rust_lang_book_chpater_20-1 (crate (name "hello_rust_lang_book_chpater_20") (vers "1.0.0") (hash "0bqvmkc1n7wcsnwvylysbbxmbdg47l5j7q3pxywkkgrdlzjm1yzg")))

(define-public crate-hello_rust_world-0.1 (crate (name "hello_rust_world") (vers "0.1.0") (hash "1nb9f865pnyd7741lcgxdj5y284rvffv94ndkwhjbirs8yi05lgr")))

(define-public crate-hello_rusty_worlds-0.1 (crate (name "hello_rusty_worlds") (vers "0.1.0") (hash "0bp76qd9b327kljb0hr1b54rsw2y0r8v4qgca0fn4na1q28psnib")))

(define-public crate-hello_s2tarky-0.1 (crate (name "hello_s2tarky") (vers "0.1.0") (hash "0hwd3d6p2prrrqflz1acsms95pj5872lajyz2vn54jwhbf8kn6s1")))

(define-public crate-hello_sj-0.1 (crate (name "hello_sj") (vers "0.1.0") (hash "00y2nq30qlssrbiqvyd60l8ay6z5w1cxffjkha1iam56falvbfrv")))

(define-public crate-hello_v256_I-0.1 (crate (name "hello_v256_I") (vers "0.1.0") (hash "04h4af8q9kq61x9fvlg7l1asq74m985nywmz17ia409bz9rl659l")))

(define-public crate-hello_word-0.1 (crate (name "hello_word") (vers "0.1.0") (hash "1c7fh5f26vc8bhb3pi6gcd7ya2mz71pz6zsgk2xlijpaj1sd4pg1")))

(define-public crate-hello_world111_ky-0.1 (crate (name "hello_world111_ky") (vers "0.1.0") (hash "1k9hbyjb3x2rbgz835c47mr69wy128jnks5f3x8g95w2ac6jk1jj")))

(define-public crate-hello_world2-0.1 (crate (name "hello_world2") (vers "0.1.0") (hash "132fw5fx6n47dmzy2aqwjr4ficza6d6b9lf2mfr7lxhx2g46a9fn")))

(define-public crate-hello_world2-0.1 (crate (name "hello_world2") (vers "0.1.1") (hash "0zjxmk343szb6j89q7yb549p13a11i3k2v2yi644y7ir9qjn9ia7") (yanked #t)))

(define-public crate-hello_world_0228-0.1 (crate (name "hello_world_0228") (vers "0.1.0") (hash "1bvm0x7yvka5w46vxpnbs7ywcnxlabr0qpf0pdq0qrbp32xkb6vj")))

(define-public crate-hello_world_0228-0.1 (crate (name "hello_world_0228") (vers "0.1.1") (hash "1967fgws375qci6062l08sl1paxqij10c5x3zlp61lsrbkjcizqg")))

(define-public crate-hello_world_0228-0.1 (crate (name "hello_world_0228") (vers "0.1.2") (hash "167z6dizgin3ca61rf6xw0g1sf7l2im0jws0c3v98k70p6s3a22a")))

(define-public crate-hello_world_2-0.1 (crate (name "hello_world_2") (vers "0.1.0") (hash "1z70rg9ly6xwimiagli9iakdvhqqj2i513bsfh0syv8ic8qxchnj")))

(define-public crate-hello_world_by_wuzhiguocarter-0.1 (crate (name "hello_world_by_wuzhiguocarter") (vers "0.1.0") (hash "0brp923hhnvs5k2lrmg4s30nhskvqvbvsgayl8phcmlp4zyly0wn")))

(define-public crate-hello_world_by_wuzhiguocarter-0.1 (crate (name "hello_world_by_wuzhiguocarter") (vers "0.1.1") (hash "013srzgsr6f626glw6aalxaq4ykbx1qi7mzrmxkkxx2s07ws5abq")))

(define-public crate-hello_world_by_zihao-0.1 (crate (name "hello_world_by_zihao") (vers "0.1.0") (hash "013d9gh0y6d3q0swppppvr8jm6apk8imq3m17nqvd0h9apwnzcka")))

(define-public crate-hello_world_chww-0.1 (crate (name "hello_world_chww") (vers "0.1.0") (hash "1pyaa3d6g6da7dk42p4fghhckxlmm5ydd9ggdvg2d6hm858wvrmj")))

(define-public crate-hello_world_ev-0.1 (crate (name "hello_world_ev") (vers "0.1.0") (hash "1z99x50h3zxid5a449b6jf0hgqrrl233zndjiainaxhpzihahhqa") (yanked #t)))

(define-public crate-hello_world_ev-0.1 (crate (name "hello_world_ev") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1dy4ix5i796bb1n40ghd8klp7ah9ga57bc6qhla6ch9ymznz5iqc")))

(define-public crate-hello_world_ky-0.1 (crate (name "hello_world_ky") (vers "0.1.0") (hash "1qalrmxppzqnmxv6w0swxrzr2nvg43875l92lxm92fh4pg9f6q82")))

(define-public crate-hello_world_lib-0.1 (crate (name "hello_world_lib") (vers "0.1.0") (hash "1vdyvmrxkipvm9fb9x274xwb5q8zandak0vcq52r76slxzigqi2y")))

(define-public crate-hello_world_macro-0.0.1 (crate (name "hello_world_macro") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (default-features #t) (kind 0)))) (hash "08x8a7hhf1dxb6q49s7fa4ni2vr4hv0qjdii0g9ya6wi6k1xqfi8")))

(define-public crate-hello_world_matterlabs-0.1 (crate (name "hello_world_matterlabs") (vers "0.1.0") (hash "0vv5pbdzx80h0vplcbvzhvyjja0fsvkcaskagdm7c1a00da10mxp")))

(define-public crate-hello_world_matterlabs-0.1 (crate (name "hello_world_matterlabs") (vers "0.1.1") (hash "1rnj5c3gcnj1yvqfkbcrj0va2cwxy83vb601lkrgxyrq7izadn8d")))

(define-public crate-hello_world_publish-0.0.1 (crate (name "hello_world_publish") (vers "0.0.1") (hash "1il6zi6a1x71zypqd8bl4s4la4ii2rqji0c7yiy1ysxi4i6c1y5l")))

(define-public crate-hello_world_rust-0.1 (crate (name "hello_world_rust") (vers "0.1.0") (hash "0bv9wzk114aig8nwhwnxnysnfa4kfx17vf5cqhpwrf7x7kkx2m32")))

(define-public crate-hello_world_rust_wenhaozhao-0.1 (crate (name "hello_world_rust_wenhaozhao") (vers "0.1.0") (deps (list (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "176bq53lry8vrgiv21nh81j75179xxqq0b1jvh0kajddqgmd10qy")))

(define-public crate-hello_world_rustaceans-0.1 (crate (name "hello_world_rustaceans") (vers "0.1.0") (hash "02mv65xid8p9blby59fbi54m3cjmg6yl9c5qx2gkb1llcqcwavv5") (yanked #t)))

(define-public crate-hello_world_rustaceans-0.1 (crate (name "hello_world_rustaceans") (vers "0.1.1") (hash "0bm5whib9fn6yhqr446g9plpfibyqx5mkzv28dsc9za80xmsqy46")))

(define-public crate-hello_world_rustaceans-0.1 (crate (name "hello_world_rustaceans") (vers "0.1.2") (hash "1y1aspd5blvjx1q2i93s0h1y9ii1xbjxhrqqjgv4ln0388b08zvp")))

(define-public crate-hello_world_test-0.1 (crate (name "hello_world_test") (vers "0.1.1") (hash "06lvb6cdr2383k3svxgjyd7i6yd7hs0yfgrb84f2zqz06f9ybp3v")))

(define-public crate-hello_world_test_publish-0.1 (crate (name "hello_world_test_publish") (vers "0.1.2") (hash "1s49jri60raavsrwpjds0zy3sqhfhvadp2j5ldzgm5l3bdzk28q2")))

(define-public crate-hello_world_testpublish-0.1 (crate (name "hello_world_testpublish") (vers "0.1.0") (hash "0665rn4nycrf5d7g2hzasnjkkwvlczbnpn4mnqs6lp2r2h3khvr0") (yanked #t)))

(define-public crate-hello_world_via_frank_wang-0.1 (crate (name "hello_world_via_frank_wang") (vers "0.1.0") (hash "1bc9b2jfqxy49xxgrq7yp1yrzg22dxicsnvnrq943zzk5vdrjkim")))

(define-public crate-hello_world_vsmelov-0.1 (crate (name "hello_world_vsmelov") (vers "0.1.0") (hash "0w6snmpa9xmd1vf2m2wkb1k1c5n1nn7s7rkhx2af0vycaf9ir2rk")))

(define-public crate-hello_world_vsmelov-0.1 (crate (name "hello_world_vsmelov") (vers "0.1.1") (hash "19mqx81cqmb2ghmj1ghsq1rns9jnxcdikzq88jmh5xbrzn3f4g2x")))

(define-public crate-hello_world_vsmelov-0.2 (crate (name "hello_world_vsmelov") (vers "0.2.1") (hash "05qmzjn0njg88r12w9cyy0vq22yc6bncma7vhk0cx9nkhac22z91")))

(define-public crate-hello_world_vsmelov-0.2 (crate (name "hello_world_vsmelov") (vers "0.2.2") (hash "1dvcbdff33j1xa5yx0prbhxn8lmcm3ynycjymq8ncf86l0j3lvkn")))

(define-public crate-hello_world_vsmelov-0.3 (crate (name "hello_world_vsmelov") (vers "0.3.0") (hash "0y2a9nmf8iar6w9anm4wnmk8hynzjv3ms1bhz33704fhkqbbxbrf")))

(define-public crate-hello_world_winter_1021-0.1 (crate (name "hello_world_winter_1021") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "03j5spjda5na2s74b2v79kz02vfw5qvggqg0747k5dfipq6fjaq3")))

(define-public crate-hello_world_winter_1021-0.1 (crate (name "hello_world_winter_1021") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1yp1rdq6fqr90nfpil27mx0whdkh1dygbrn36zs750l2bwq7r6gl")))

(define-public crate-hello_world_winter_1021-0.1 (crate (name "hello_world_winter_1021") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0gv82ashrg6rb2dngnwkvgvpa9g8fzr3idhldcf25nv27sn9c5sz")))

(define-public crate-hello_world_zwj-0.1 (crate (name "hello_world_zwj") (vers "0.1.0") (hash "1xxyqrg4x2ydjrk1p4nifspfad9whvfccgslp7nf3v6ffcr6m5a3")))

(define-public crate-hello_world_zwj-0.1 (crate (name "hello_world_zwj") (vers "0.1.1") (hash "14bad48182k01bacmgyw3sr3fh3057232dna58z6kn722c5zqi57")))

(define-public crate-helloA-0.1 (crate (name "helloA") (vers "0.1.0") (hash "0ywyfagdmsznjay4m36cvsmpw5ww8lc8cvnib4h2g3hw64m0mgn6") (yanked #t)))

(define-public crate-helloasso-0.1 (crate (name "helloasso") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ywmb05lfrpixa2pd3wv20akfr8jzbl1y9dfhsyzd9hriz92vwzg")))

(define-public crate-helloasso-0.1 (crate (name "helloasso") (vers "0.1.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fws2gzarm7q5jp0gfsjfyl9b2zj1c71cdd88qli3hr1bqqd21v4")))

(define-public crate-helloasso-0.2 (crate (name "helloasso") (vers "0.2.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0v4j281wkbld2qpx3n80qmxhdq93jsigdb88amwahr1yqkmqaxvx") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-helloasso-0.2 (crate (name "helloasso") (vers "0.2.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0m5l6qj41pj7aaf9250kcnklhrdlh7lz8gr9a67zxilg2ihpcqkb") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-helloasso-0.3 (crate (name "helloasso") (vers "0.3.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.4.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "14fr1crws0fb080b0vmiz6c6pdkyq2vsmfm2ki3fg6d7lfxq53cr") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-helloasso-0.4 (crate (name "helloasso") (vers "0.4.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.4.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0hvz15z8dwyxkllbsx60yszf6zs08kscamksl6pdi0dz7xv2pdah") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-hellochi-0.1 (crate (name "hellochi") (vers "0.1.0") (hash "0s5mb0air3dk1qpfz009k0694876fbc02jzsmsa3852f9hj13ar8")))

(define-public crate-hellodc-demo-0.1 (crate (name "hellodc-demo") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "19pj12bpzybglsl029x10kjb3pwny2882cnjf93ydnkvxk6f1ixc")))

(define-public crate-hellofluent-0.1 (crate (name "hellofluent") (vers "0.1.1") (deps (list (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0g7y6i439fbybdj9mnzkq7wjwcgm48c13723zid040cw312l7f0z")))

(define-public crate-hellofluent-0.1 (crate (name "hellofluent") (vers "0.1.2") (deps (list (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0i79lxvv7s67b35wkclxmikqddgrf8qmaks272kqs8l02zklf0p7")))

(define-public crate-hellofluent-0.1 (crate (name "hellofluent") (vers "0.1.3") (deps (list (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0ir39i8giwkdlzrw9hf12dch1hfxkqaqzkq12y87bd3w6nyf8k6k")))

(define-public crate-hellofluent-0.1 (crate (name "hellofluent") (vers "0.1.4") (deps (list (crate-dep (name "dialoguer") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1v5z2avnlh75ln6jqml3szc8dhi8nw97q03jwcjkfhv5afj3in35")))

(define-public crate-hellogauges-0.1 (crate (name "hellogauges") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.63") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.13") (default-features #t) (kind 2)) (crate-dep (name "yew") (req "^0.19.3") (default-features #t) (kind 0)))) (hash "02bk6a7km5wbmrvh4f5f0h8q0569kbmw2bn34v4caa72a0rc6mvw")))

(define-public crate-hellohellocrate1-0.1 (crate (name "hellohellocrate1") (vers "0.1.0") (hash "1l9ms62q5sjp97l6l296sxpm2z46nlm12q7whi1c4q6f42lpsnrw")))

(define-public crate-helloLiao-0.1 (crate (name "helloLiao") (vers "0.1.0") (hash "1b6s8iy5bfzlz8bmvsh8prrd1bg2f2r4nfa8x0r80bi9pln3bqv6")))

(define-public crate-hellorust20xx-0.1 (crate (name "hellorust20xx") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0956sw4z5wlksyr08mqxfxlmw3hdnzbcnp15hawp098957vgjqph")))

(define-public crate-hellos-0.1 (crate (name "hellos") (vers "0.1.0") (hash "1r9rwa0302krxpnqr90s6q5dpx5w7kssv03jsfmrir0xdf3h8pic")))

(define-public crate-hellosamp-0.1 (crate (name "hellosamp") (vers "0.1.0") (hash "17mlq5sy20kllkfmh9lyf67dydly5rpdc5my5247ykabblmsjqnk") (rust-version "1.67")))

(define-public crate-hellow-0.1 (crate (name "hellow") (vers "0.1.0") (hash "13x5skcvdyb39yrpi19k44gvzdy8jkdps498allwmcmh2bncdbm8")))

(define-public crate-hellow-0.1 (crate (name "hellow") (vers "0.1.1") (hash "0q97v0r5pkklj9vg1bkvkpnd3229h19zjm02vd87srz8vh4h8iml")))

(define-public crate-helloworld-0.1 (crate (name "helloworld") (vers "0.1.0") (hash "10vinb6zibf3f71zm5n5fr8yz022vxplfnfnh7gxmxhv0qramfpd")))

(define-public crate-helloworld-rs-0.1 (crate (name "helloworld-rs") (vers "0.1.0") (hash "0df86h2kpjp7lfc01aqjaviwypff1p9214h1z27mvr0dkhxapwfh")))

(define-public crate-helloworld-tonic-bsr-0.1 (crate (name "helloworld-tonic-bsr") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "pbjson") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "pbjson-types") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("gzip" "tls"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.10.2") (default-features #t) (kind 1)) (crate-dep (name "tonic-reflection") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 2)))) (hash "1q276q1sfmpxws53k4hfxk5isf4vzbih5bssyvmp165x81jg8ps9")))

(define-public crate-helloworld-tonic-bsr-0.1 (crate (name "helloworld-tonic-bsr") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "pbjson") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "pbjson-types") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("gzip" "tls"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.10.2") (default-features #t) (kind 1)) (crate-dep (name "tonic-reflection") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 2)))) (hash "0bphcqkbp07lhvyw06915lckl6jjhm1nb4y6d7ys03cy4nbgdyd1")))

(define-public crate-helloworld-tonic-bsr-0.2 (crate (name "helloworld-tonic-bsr") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "pbjson") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "pbjson-types") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("gzip" "tls"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.10.2") (default-features #t) (kind 1)) (crate-dep (name "tonic-reflection") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 2)))) (hash "1l3c5qvl9xzq7fxp6cj3zrj5y3gxapqsl6a2g5zkf95ddi0cvpm5")))

(define-public crate-helloworld-tonic-bsr-0.3 (crate (name "helloworld-tonic-bsr") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "pbjson") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "pbjson-types") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.10.2") (features (quote ("gzip" "tls"))) (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.10.2") (default-features #t) (kind 1)) (crate-dep (name "tonic-reflection") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.2.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 2)))) (hash "10bmcb59cvqif2nzi0gw6razcrhc1ndpf0sj7q0hhn484nsx177m")))

(define-public crate-helloworld-yliu-0.1 (crate (name "helloworld-yliu") (vers "0.1.0") (hash "0mzb0qjvsymnz7wmpgb7dnijlr1fsbp4sh7fgpf65lib8hj3w60m")))

(define-public crate-helloworld-yliu-0.1 (crate (name "helloworld-yliu") (vers "0.1.1") (hash "01vyfxlgrr2sffk0w9x7rl0vaw650ngarglx29j68hd7cafl7jvz")))

(define-public crate-helloworldqshuai-0.1 (crate (name "helloworldqshuai") (vers "0.1.0") (hash "16kbgmyz2r3c48agnr5gr4f6c8vbanmnnz7hf1zr1d262rhq2jx5")))

