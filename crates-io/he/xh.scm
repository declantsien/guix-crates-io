(define-module (crates-io he xh) #:use-module (crates-io))

(define-public crate-hexhex-1 (crate (name "hexhex") (vers "1.0.0") (deps (list (crate-dep (name "hexhex_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hexhex_macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "14fws0lx0hi5blnp42p0r57zrr4r72zs3hba3ns8v4gjlpvfpcag") (features (quote (("std" "hexhex_impl/std") ("default" "std"))))))

(define-public crate-hexhex-1 (crate (name "hexhex") (vers "1.1.0") (deps (list (crate-dep (name "hexhex_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hexhex_macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1chsni8lgm0yvhz63gfjm16b6cwxsqh9bsfdfl1r3iw22b24vydj") (features (quote (("std" "hexhex_impl/std") ("default" "std"))))))

(define-public crate-hexhex-1 (crate (name "hexhex") (vers "1.1.1") (deps (list (crate-dep (name "hexhex_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hexhex_macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1z6j7qknq6w6xcqhryshzppa7sfihcfiz92y2pwlv3briac94rf5") (features (quote (("std" "hexhex_impl/std") ("default" "std"))))))

(define-public crate-hexhex_impl-0.1 (crate (name "hexhex_impl") (vers "0.1.0") (deps (list (crate-dep (name "fallible-iterator") (req "^0.2.0") (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0bjzrxi8q5yqnlypmzp55d0g8bq2dzdhiwhq92d3v9vy5g61s2hk") (features (quote (("std" "fallible-iterator/std") ("proptest" "std") ("default"))))))

(define-public crate-hexhex_impl-0.1 (crate (name "hexhex_impl") (vers "0.1.1") (deps (list (crate-dep (name "fallible-iterator") (req "^0.2.0") (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0jhr5vxz0glw347j1z3rd6mrswrzivd24zgn50832q8r11k0qlqz") (features (quote (("std" "fallible-iterator/std") ("proptest" "std") ("default"))))))

(define-public crate-hexhex_macros-1 (crate (name "hexhex_macros") (vers "1.0.0") (deps (list (crate-dep (name "hexhex_impl") (req "^0.1.0") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "02x3sxnvdfn1jyi20lpp2fiz9wz94gbqz6i7bvzvrc27jcl19gpl")))

(define-public crate-hexhex_macros-1 (crate (name "hexhex_macros") (vers "1.0.1") (deps (list (crate-dep (name "hexhex_impl") (req "^0.1.0") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "07qy77fr9mc96f51a6x7hgxq6rx56k28b88gxxj4d8ai6g4y42nx")))

