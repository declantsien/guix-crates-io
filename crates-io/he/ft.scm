(define-module (crates-io he ft) #:use-module (crates-io))

(define-public crate-hefty-0.1 (crate (name "hefty") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "typle") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1m2n17vazih0vh8frjxn2z6mdqzzfvmy4m467q9vsrydi2hvip8x")))

(define-public crate-hefty-0.2 (crate (name "hefty") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "typle") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1gsaamlmfr0j94fi712hf2flbmfilq8khgs0mj3xf2xqvvrygp1q")))

(define-public crate-hefty-0.2 (crate (name "hefty") (vers "0.2.1") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "typle") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "06gzxg9jzjms3dfi5bmw6v77lw0q1q08q6asg7gwcgizpxnd3yrm") (rust-version "1.65")))

(define-public crate-hefty-0.3 (crate (name "hefty") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "typle") (req "^0.4") (default-features #t) (kind 0)))) (hash "1cznqni0zfl3lv3v30m6xqqhppnzv6kxbhwq77pi44yvsxcxd5hp") (rust-version "1.65")))

(define-public crate-hefty-0.3 (crate (name "hefty") (vers "0.3.1") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "typle") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "070axz4h6plvk2vsi3n9ccxxyd3az284pg8vvpwr4g3nmh38pdxv") (rust-version "1.65")))

