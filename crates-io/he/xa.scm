(define-module (crates-io he xa) #:use-module (crates-io))

(define-public crate-hexa-0.0.7 (crate (name "hexa") (vers "0.0.7") (hash "0f640hgb3jzam5sb3gkkgiy5wz5vmy01w0gfn0y1v18s23v6gikj") (yanked #t)))

(define-public crate-hexa-0.0.0 (crate (name "hexa") (vers "0.0.0") (hash "1fxd54zlbvl9jfkjk6qvkrkdrc4s25hmg7zcn64rkw6li90n388j") (yanked #t)))

(define-public crate-hexa_vm-0.0.7 (crate (name "hexa_vm") (vers "0.0.7") (hash "08kf0zdx86rbmnz9kwxs6f9whkh0zjzih4j9z662h14p80vglfij") (yanked #t)))

(define-public crate-hexafreeze-0.1 (crate (name "hexafreeze") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0s6s28nlh51sfb5sy9zhi3ycbpkr5r7b9nx0rl7k37i76slnbx1a")))

(define-public crate-hexafreeze-0.2 (crate (name "hexafreeze") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0351p1wskyy4y1gk4m3ls96arnb1w3xl1203snixmdldxagz8ziz")))

(define-public crate-hexafreeze-0.3 (crate (name "hexafreeze") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0nw6vc00vvv22gcywxl1y8pdnjna5ai25rsby0h1zalc80h5wndc")))

(define-public crate-hexafreeze-0.3 (crate (name "hexafreeze") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1vackd65gfzlkh4mpfqc8kyxlrqz63g1y4sk4lk5pkb3p4hq16q9")))

(define-public crate-hexafreeze-0.3 (crate (name "hexafreeze") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1spkr11ya28kyb427zm0bbpzsj1lqrjvd5nvqpqxwvsz8s3jr6m2")))

(define-public crate-hexafreeze-0.4 (crate (name "hexafreeze") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0kgzb3chkrlblildn77xnsf2yvdvyppsa3ldf9pkqkv9qzlrmym2")))

(define-public crate-hexafreeze-0.4 (crate (name "hexafreeze") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("async_tokio"))) (default-features #t) (kind 2)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "11czsihwpmdssww25babpsivw4xwh3510cgl00p37jdgbw4chpb5")))

(define-public crate-hexafreeze-0.4 (crate (name "hexafreeze") (vers "0.4.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("async_tokio"))) (default-features #t) (kind 2)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "0z534gxavd7yx87234yr8xwxf202igxq7zp12hc02x6ha0v3dzcl")))

(define-public crate-hexafreeze-0.5 (crate (name "hexafreeze") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("async_tokio"))) (default-features #t) (kind 2)) (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 2)) (crate-dep (name "test-log") (req "^0.2") (features (quote ("trace"))) (kind 2)) (crate-dep (name "tokio") (req "^1.24") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "0hn3p23lgfdqh4pd8djcgzz2pczymdin1xkxay86yc52400gl8cj")))

(define-public crate-hexagex-0.1 (crate (name "hexagex") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.25") (default-features #t) (kind 0)))) (hash "11kh9l7hq5y1f63c60ab8swxm6vk8pz7nrrxbng1bfgp37xbcg59")))

(define-public crate-hexagex-0.1 (crate (name "hexagex") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.25") (default-features #t) (kind 0)))) (hash "1xqbbcy80h8c01gcqy5hsz15w8476j17zj9j2chpgkb8lx4wf5bc")))

(define-public crate-hexagex-0.2 (crate (name "hexagex") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.25") (default-features #t) (kind 0)))) (hash "1pnms896k8gkkb6rmzc0krqswh58rd2ds0c58ivylr75b235v9xm")))

(define-public crate-hexagex-0.2 (crate (name "hexagex") (vers "0.2.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.27") (default-features #t) (kind 0)))) (hash "0ni030jlyjzgbjplf354z67wdvm2ddwj6vdwc000267xlx35n8dy")))

(define-public crate-hexagex-0.2 (crate (name "hexagex") (vers "0.2.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.28") (default-features #t) (kind 0)))) (hash "0yrwb9iv1p521rqlcchmhsqg4acc6v3sz9ah4vwxl1801mjkra19")))

(define-public crate-hexagex-0.2 (crate (name "hexagex") (vers "0.2.3") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0ladz08ylp53z45im1ibsnhbbccb7ynvlcmy5zllnx0lkgaxgpwh")))

(define-public crate-hexagon-0.1 (crate (name "hexagon") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "0zivy712bfcf6g33gfpyf45v9ff45vhyw9jkp98258d1q0cw1mps")))

(define-public crate-hexagon-0.1 (crate (name "hexagon") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "0ckgkl6x9dsci8hja6qjgy4kly32n3b06j5v199rj73vnx3p0zy4")))

(define-public crate-hexagon-0.1 (crate (name "hexagon") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "1hw909dkv35ljnk1a1nbgq0l0ppk4jw857qhqm131wxi634y74b1")))

(define-public crate-hexagon-e-0.1 (crate (name "hexagon-e") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "148nbii97xcw4ifwjwj3i0dvpsi2vf8pmf3cn5ppqm78mx6m2mvf")))

(define-public crate-hexagon-e-0.1 (crate (name "hexagon-e") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "0dm4z4j00ks21a98n9pas2yz56hjl0w3sndwxp12q67qcm22ll5k")))

(define-public crate-hexagon-e-0.1 (crate (name "hexagon-e") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "08v1l118ygfwrlmqn2cwlls4mz9kvvrrsbdhfb61jc1sic02wa7y")))

(define-public crate-hexagon-e-0.1 (crate (name "hexagon-e") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "0wk0iikdz85yg8dk7iys0pf8dx8cbs4pzp3jcz0vg95p2iggwvs2")))

(define-public crate-hexagon-e-0.1 (crate (name "hexagon-e") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "12dq8npiji0yyppqwvlbqs5l9nz6823y309mhxiiav1cczqlk1dz")))

(define-public crate-hexagon-e-0.1 (crate (name "hexagon-e") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "1cna6zhrzdwc34b82ih4d1w74rw9vj2rlnckm54hl60lp45lgb83")))

(define-public crate-hexagon-map-0.0.0 (crate (name "hexagon-map") (vers "0.0.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08mmprbgcnb4052jdbfmjxm3pxa25zd7xy1yxy8sg08y0g8cqnfp") (features (quote (("default"))))))

(define-public crate-hexagon-map-0.0.1 (crate (name "hexagon-map") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0alz9ypbs2p79qjnfbj419rj1sdbldc6gjbsz5gsy7k55q2qf671") (features (quote (("default"))))))

(define-public crate-hexagon-map-0.0.2 (crate (name "hexagon-map") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.6.0") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^4.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shape-core") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1cc6sslykg3rim99x181k1wdpfx54fkn3446cjbxha21ajbiqlic") (features (quote (("default"))))))

(define-public crate-hexagon_tiles-0.1 (crate (name "hexagon_tiles") (vers "0.1.0") (deps (list (crate-dep (name "float_eq") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1xz9h1ikqdklyqpv0zsxqm1iqmm3jlqn4p5snjl53lr5dlkxxy4a")))

(define-public crate-hexagon_tiles-0.2 (crate (name "hexagon_tiles") (vers "0.2.0") (deps (list (crate-dep (name "float_eq") (req "~1.0.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pa1p0ng7nz2dfx91sicawvb67lgf4pxkwmr8v6hrp8vzlfv4qqg")))

(define-public crate-hexagon_tiles-0.2 (crate (name "hexagon_tiles") (vers "0.2.1") (deps (list (crate-dep (name "float_eq") (req "~1.0.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yrxrqp4y1z2768iapmy9w32d17hlafkpd8fiaxyvgw9rf418lzm")))

(define-public crate-hexagonal-0.0.0 (crate (name "hexagonal") (vers "0.0.0") (hash "09k6d9ys6vyk7f1x0ykhc9iw0v8c4f0fha11qsn30apibqgh67kf")))

(define-public crate-hexagonal_pathfinding_astar-0.4 (crate (name "hexagonal_pathfinding_astar") (vers "0.4.1") (hash "1s0ajkgfm4sq8ha7i5gk0vnxmd92z1qkdh1z8ii31v49r6rwq3z5")))

(define-public crate-hexagonal_pathfinding_astar-0.4 (crate (name "hexagonal_pathfinding_astar") (vers "0.4.2") (hash "0y2gdhrhq1xlh9j4r515i7mraa7mlq12njzrfz1jbhj6z3b1iv65")))

(define-public crate-hexagonal_pathfinding_astar-0.5 (crate (name "hexagonal_pathfinding_astar") (vers "0.5.0") (hash "1nry3jjgjwdhzimkqxrb9igrf9rljpbnb8pk6vd6sm8w69h8pwmb")))

(define-public crate-hexagonal_pathfinding_astar-0.5 (crate (name "hexagonal_pathfinding_astar") (vers "0.5.1") (hash "1ggivf24s1c2m41w7man30881cza3331qzsbpvv8asd48va2q3qy")))

(define-public crate-hexagonal_pathfinding_astar-0.6 (crate (name "hexagonal_pathfinding_astar") (vers "0.6.0") (hash "12k5yxganq17lkd6p4vaz5k74dblj8gbkrm5k04dkmqa795fdzj3") (rust-version "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.7 (crate (name "hexagonal_pathfinding_astar") (vers "0.7.0") (hash "06iy427b5q7k8s42aw9hr3pwvpgfr54y0nrizqg2qzx46hp50331") (rust-version "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.8 (crate (name "hexagonal_pathfinding_astar") (vers "0.8.0") (hash "1kv6bihlpxpd9f44vc6lm1n71icwij9m352vrcz3zk3gfhk1gj6q") (rust-version "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.8 (crate (name "hexagonal_pathfinding_astar") (vers "0.8.1") (hash "1bjz33vhqc6cw0yq64lkp1ml7plwsfwlw51l65r9qilf2d8yq353") (rust-version "1.56.0")))

(define-public crate-hexagonal_pathfinding_astar-0.8 (crate (name "hexagonal_pathfinding_astar") (vers "0.8.2") (hash "0qynhcz8r4f1zx1j7dq0yj64xgv40gavz6bwkyfpizhcbxbhk034") (rust-version "1.58")))

(define-public crate-hexagonal_pathfinding_astar-0.8 (crate (name "hexagonal_pathfinding_astar") (vers "0.8.3") (hash "016kgh46k8flv3jkgynw4jmy549lzdd2pwxqpvndm3pdc3vkkqn1") (rust-version "1.58")))

(define-public crate-hexagonal_pathfinding_astar-0.8 (crate (name "hexagonal_pathfinding_astar") (vers "0.8.4") (hash "02bvfmir34hnp5s7g4xpx6rr4k95f2rmpxvx8rv3z8a5lq80jr2x") (rust-version "1.58")))

(define-public crate-hexagonal_pathfinding_astar-0.9 (crate (name "hexagonal_pathfinding_astar") (vers "0.9.0") (hash "1c0hirli695wvaxv8ilxhlmmnyf642yls06ncs78kw8djz77jrzs") (rust-version "1.58")))

(define-public crate-hexagonal_pathfinding_astar-1 (crate (name "hexagonal_pathfinding_astar") (vers "1.0.0") (hash "0nmmqvfh7q6z8lkn42p6i2pfb26d2a3i6phwkj827qgvd29vnk5m") (rust-version "1.59")))

(define-public crate-hexal_lib-0.1 (crate (name "hexal_lib") (vers "0.1.0") (hash "0cjqlgmhz4s9n4sbk35ba58pqzppihqshsl61jx31ypgcfxh7rg6") (yanked #t)))

(define-public crate-hexane-0.1 (crate (name "hexane") (vers "0.1.0") (deps (list (crate-dep (name "winit") (req "^0.28.6") (features (quote ("android-native-activity"))) (default-features #t) (kind 0)))) (hash "01pgv3lm6i8mhn0r70jx79chs77d735h01s7fhqp3vddwr97air5")))

(define-public crate-hexarr-0.0.0 (crate (name "hexarr") (vers "0.0.0-alpha") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1b4vflyq253n0j9rgq99522amgjhmr5w4g5lm3qn7bkk3s1rc8y0") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-hexarr-0.1 (crate (name "hexarr") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "025018qdjgascl5hs2yyzvylkjl9844gpvdk218r8wdc6ibnkprb") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde") ("glam" "dep:glam"))))))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1ilpmps88h9gvq2s4fmxy5ricrd2d3a3j6pcf2m9ic8jpyfj7l4s") (yanked #t)))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "106bxafrk9cvk7f5vqr7p6w7vd6hgf1hi08jwlkv53ad70bb566z") (yanked #t)))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.2") (deps (list (crate-dep (name "glam") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0rgg9wsid4ycbrd9d48if77bcl3k9zhc3zrlqyc1a51bbzfaqkd3") (yanked #t)))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.3") (deps (list (crate-dep (name "glam") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0pdj1llcfwrlrrfw8s7pp9wmr3c75wf3705sqhd1241wzdri9cmv")))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.4") (deps (list (crate-dep (name "glam") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0dj9mz2inxph0x4ha5q4vqk7k6a9xs7s34kj8z68zhkzl6b8kgci")))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.5") (deps (list (crate-dep (name "glam") (req "^0.9") (default-features #t) (kind 0)))) (hash "085rb5mx3hwz8mw48n1y8agfr989qrr5a1zqgj0gnnks80iq66bz")))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.6") (deps (list (crate-dep (name "glam") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1j0hgjp7ffnwffnpa05h4kzw4fzjlxfrncif271qh8kdny0c19rd") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-0.1 (crate (name "hexasphere") (vers "0.1.7") (deps (list (crate-dep (name "glam") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "08y5apxp00rql9ply78riyzfjllvswx0h9i1gvs0qpzaxbkpqlhg") (features (quote (("adjacency" "smallvec")))) (yanked #t)))

(define-public crate-hexasphere-1 (crate (name "hexasphere") (vers "1.0.0") (deps (list (crate-dep (name "glam") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0xc1za5mbrmhc2vis86mcnpkkpihl5n7d24n89lgn3p7flbr5b80") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-2 (crate (name "hexasphere") (vers "2.0.0") (deps (list (crate-dep (name "glam") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1y67ia0j5whgxf3ms58cgj4nmmw9vbfklab38dqw1pxpzr1njibf") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-2 (crate (name "hexasphere") (vers "2.1.0") (deps (list (crate-dep (name "glam") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0yybp0wvnn6g2ip4805pgzpqwj2i317f4n3qsk4did1lfij7wjak") (features (quote (("adjacency" "smallvec")))) (yanked #t)))

(define-public crate-hexasphere-3 (crate (name "hexasphere") (vers "3.0.0") (deps (list (crate-dep (name "glam") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0g9ap44m85fc7x2c1m7iwyf57bgxwnpxd0irmff9avsa0hvwi54f") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3 (crate (name "hexasphere") (vers "3.1.0") (deps (list (crate-dep (name "glam") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0mck0prrdgi4yb73hlyqj2daq279wv2dfp0smn7x92q999bz87mq") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3 (crate (name "hexasphere") (vers "3.1.1") (deps (list (crate-dep (name "glam") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "12sabk2n617qni2nxbh210f53hcn3blwa3xv2xp0wcyd5x5mn16d") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3 (crate (name "hexasphere") (vers "3.2.0") (deps (list (crate-dep (name "glam") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0wa135ab4rxp7gyzdvzmdvzgkipm7szdsjsaw0w4256gc4ls94n5") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3 (crate (name "hexasphere") (vers "3.3.0") (deps (list (crate-dep (name "glam") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0i63161bv8l1hr39azqr4jmf0yd89y47f3pmjbi7szfp2d0flplp") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-3 (crate (name "hexasphere") (vers "3.4.0") (deps (list (crate-dep (name "glam") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1z70841j3j6rwg4kgm7b4yp1bg2za55ijfdf97k7ywycja8ajzs9") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-4 (crate (name "hexasphere") (vers "4.0.0") (deps (list (crate-dep (name "glam") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0dcamzzj2b44hw40w0p25j877jhy0pl5x18kmqxa4zbv8v16bb81") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-4 (crate (name "hexasphere") (vers "4.1.0") (deps (list (crate-dep (name "glam") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1qncdypr8x6xqxfvxxc1m15nyfbx4y7n7kcljggccb5pmgnkc23l") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-4 (crate (name "hexasphere") (vers "4.1.1") (deps (list (crate-dep (name "glam") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "09qizj5qmqvvyi7h5k4130wa1im3y57d48f6hxhzwgj4cgpj965n") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-5 (crate (name "hexasphere") (vers "5.0.0") (deps (list (crate-dep (name "glam") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1hpcv5gg8rgj8agfsi01psgagv20fai5waj2fdkd5zs9j7g654jn") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-5 (crate (name "hexasphere") (vers "5.0.1") (deps (list (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0zc7mm1skr77n3qwls2qffmbx2ldckpqxciwg23s0ap2z6b732v3") (features (quote (("adjacency" "smallvec")))) (yanked #t)))

(define-public crate-hexasphere-6 (crate (name "hexasphere") (vers "6.0.0") (deps (list (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "09v66vdsk25ypghb0wx3n4950rc85rpma13qf3211j4fsv7jvilx") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-6 (crate (name "hexasphere") (vers "6.1.0") (deps (list (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1xa6109ij858d788ci9dchk3c17h9py6nqr9akx0ajdiavdrn89b") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7 (crate (name "hexasphere") (vers "7.0.0") (deps (list (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "16wp2mrpiji80hdnpigppqlcyqlzhf8mx23x59cg2gsip8h9vaq4") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7 (crate (name "hexasphere") (vers "7.1.0") (deps (list (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0zb168hk9bzhmicpxjwk0000hqfh264jns4ricbr44ak417kp9mj") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7 (crate (name "hexasphere") (vers "7.2.0") (deps (list (crate-dep (name "glam") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1w0b9frg76k6clrzh47sqzd7lpg2qq7zcf8h0r53gip9gvnz4lln") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-7 (crate (name "hexasphere") (vers "7.2.1") (deps (list (crate-dep (name "glam") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "1m5d8fk9dy5z76qgs0almpwlhn6fgq0qf6ajyp7k9bdnpv8szbda") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-8 (crate (name "hexasphere") (vers "8.0.0") (deps (list (crate-dep (name "glam") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0slhibrgfr7znxxslszf2gzk4w4w4hh1p7l0lz8dg0c6amafd731") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-8 (crate (name "hexasphere") (vers "8.1.0") (deps (list (crate-dep (name "glam") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "121myzm13f8ms3li67v3c2wvqhwcl1i8pmfsls0a7gvqz51x8hdx") (features (quote (("adjacency" "smallvec"))))))

(define-public crate-hexasphere-9 (crate (name "hexasphere") (vers "9.0.0") (deps (list (crate-dep (name "constgebra") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1djv1ip7wdqihm5n9f68gblm3im2xn6ssg80qcr80jrjnzyd5rja") (features (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-9 (crate (name "hexasphere") (vers "9.1.0") (deps (list (crate-dep (name "constgebra") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "044si1igr1j54rp04ia4l6p71jkp9zhmbg9a16ybbcdwlwbdzcvw") (features (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-10 (crate (name "hexasphere") (vers "10.0.0") (deps (list (crate-dep (name "constgebra") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0an08gpd46rm54a6fyhkfkqik5wbrnc8ps3jq01ygna3f5zxnggk") (features (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-11 (crate (name "hexasphere") (vers "11.0.0") (deps (list (crate-dep (name "constgebra") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0hpf6dhxhmi82vvy3p2hlzsn39kkbswgg46698z4psl2b3cxklif") (features (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexasphere-12 (crate (name "hexasphere") (vers "12.0.0") (deps (list (crate-dep (name "constgebra") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1v286wvxi8x54yvf5jwkhwr2zqls2qsaxvcnfh56n20g2qwb1mpd") (features (quote (("shape-extras") ("adjacency" "tinyvec"))))))

(define-public crate-hexavalent-0.1 (crate (name "hexavalent") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (kind 0)) (crate-dep (name "time") (req "^0.2.9") (kind 0)))) (hash "1r8mh0kckwyhfcssyiqj6irv70n4cjykw5qm576h6x91hwwb0zfd") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1 (crate (name "hexavalent") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (kind 0)) (crate-dep (name "time") (req "^0.2.9") (kind 0)))) (hash "1rvysf8d5avcizqcj9m3kgmihfijx5shb5r7sxp9kxkvnf23g2gy") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1 (crate (name "hexavalent") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req ">=1.2.1, <2.0.0") (kind 0)) (crate-dep (name "libc") (req ">=0.2.67, <0.3.0") (kind 0)) (crate-dep (name "time") (req ">=0.2.23, <0.3.0") (kind 0)))) (hash "0wrr3hi7958y378yjz4l8x52yws44baldganwqpx5fin1qn3sim3") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1 (crate (name "hexavalent") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req ">=1.2.1, <2.0.0") (kind 0)) (crate-dep (name "libc") (req ">=0.2.67, <0.3.0") (kind 0)) (crate-dep (name "time") (req ">=0.2.23, <0.3.0") (kind 0)))) (hash "05dx7arssxy30gmavzl6dhgz7k4y1ax1rdzsdidc72sxm3fblqkg") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1 (crate (name "hexavalent") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req ">=1.2.1, <2.0.0") (kind 0)) (crate-dep (name "libc") (req ">=0.2.67, <0.3.0") (kind 0)) (crate-dep (name "time") (req ">=0.2.23, <0.3.0") (kind 0)))) (hash "085r89f91bd2bhchvf9fv2nq61yl8ap6m9i8jjzqwqir6s53yald") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1 (crate (name "hexavalent") (vers "0.1.6") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (kind 0)) (crate-dep (name "time") (req "^0.2.23") (kind 0)))) (hash "0gpr4bbj5xi777pdcg30485kv2k6as3fz03fl310nll2k0l5n9ms") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.1 (crate (name "hexavalent") (vers "0.1.7") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (kind 0)) (crate-dep (name "time") (req "^0.2.23") (kind 0)))) (hash "027xwvzsapabbvw6v5d7j9n0h5r05gmsiz4kd9r5yd9mx2bf45xz") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.2 (crate (name "hexavalent") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (kind 0)) (crate-dep (name "time") (req "^0.3.7") (kind 0)))) (hash "177f9qz0kazcslvxz77gnbh74a5n1pljrz24pxxqmypv0i1ca5ya") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.2 (crate (name "hexavalent") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (kind 0)) (crate-dep (name "time") (req "^0.3.7") (kind 0)))) (hash "0m7xhhsm2vfcdabzx98l1swmymd21frvfljnlg4kgpqjvjyz3f2p") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

(define-public crate-hexavalent-0.3 (crate (name "hexavalent") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (kind 0)) (crate-dep (name "libc") (req "^0.2.67") (kind 0)) (crate-dep (name "time") (req "^0.3.7") (kind 0)))) (hash "0larwkgdafbvcby8w1m2jpy6m2lj0nh089vrdmr4z4yb41n2l6p2") (features (quote (("default") ("__unstable_ircv3_line_in_event_attrs"))))))

