(define-module (crates-io he ck) #:use-module (crates-io))

(define-public crate-heck-0.1 (crate (name "heck") (vers "0.1.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "01nf4l2dq5zfm5zdrsqvmacfc8h9ry3gnq7582bk6xbpj7d6ml2i")))

(define-public crate-heck-0.2 (crate (name "heck") (vers "0.2.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1y90w2knz832z4ac04k6l73yw3dyb4zg43r5rxmll16cchppv03g")))

(define-public crate-heck-0.2 (crate (name "heck") (vers "0.2.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0l94sb6x6dfy7py1xl59a2365p9fkb7sirw5hri7spaajai45nz0")))

(define-public crate-heck-0.3 (crate (name "heck") (vers "0.3.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "10hvfw5j6wifsppx3im68vif9ggxf5rc0vw0ghdfa1afmlzgl17a")))

(define-public crate-heck-0.3 (crate (name "heck") (vers "0.3.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "01a2v7yvkiqxakdqz4hw3w3g4sm52ivz9cs3qcsv2arxsmw4wmi0")))

(define-public crate-heck-0.3 (crate (name "heck") (vers "0.3.2") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1b56s2c1ymdd0qmy31bw0ndhm31hcdamnhg3npp7ssrmc1ag9jw7")))

(define-public crate-heck-0.3 (crate (name "heck") (vers "0.3.3") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0b0kkr790p66lvzn9nsmfjvydrbmh9z5gb664jchwgw64vxiwqkd")))

(define-public crate-heck-0.4 (crate (name "heck") (vers "0.4.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ygphsnfwl2xpa211vbqkz1db6ri1kvkg8p8sqybi37wclg7fh15") (features (quote (("unicode" "unicode-segmentation") ("default"))))))

(define-public crate-heck-0.4 (crate (name "heck") (vers "0.4.1") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1a7mqsnycv5z4z5vnv1k34548jzmc0ajic7c1j8jsaspnhw5ql4m") (features (quote (("unicode" "unicode-segmentation") ("default"))))))

(define-public crate-heck-0.5 (crate (name "heck") (vers "0.5.0-rc.1") (hash "02ak8z07aynwcn0nn0ciiqc4sakl0lyvjrnhh3sr5vi1klpzgsi8") (rust-version "1.56")))

(define-public crate-heck-0.5 (crate (name "heck") (vers "0.5.0") (hash "1sjmpsdl8czyh9ywl3qcsfsq9a307dg4ni2vnlwgnzzqhc4y0113") (rust-version "1.56")))

(define-public crate-heck-but-macros-0.0.1 (crate (name "heck-but-macros") (vers "0.0.1") (deps (list (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1cb5d9jflwq4g6zy8i1yc357li2cdvzlqbrfn4z1v4lhy4yxzzbp")))

(define-public crate-heckcheck-1 (crate (name "heckcheck") (vers "1.0.0") (deps (list (crate-dep (name "arbitrary") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "08k7a6qr8wi69bbi37s7yxhpq10xka0jv2qcl8bv0c4n0khm0gyr")))

(define-public crate-heckcheck-1 (crate (name "heckcheck") (vers "1.0.1") (deps (list (crate-dep (name "arbitrary") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0hi5d42cdnzj5a6i4m71dx53kmxz7bbl7jn19j7yk7f3y61f7xcs")))

(define-public crate-heckcheck-1 (crate (name "heckcheck") (vers "1.0.2") (deps (list (crate-dep (name "arbitrary") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0xp927zxwdv18glxq4zdp3fb69xc3n15dryn7zl9704p1fw4fs1b")))

(define-public crate-heckcheck-2 (crate (name "heckcheck") (vers "2.0.0") (deps (list (crate-dep (name "arbitrary") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1kr59bqynin2n7nwpkcigzrignwfpfxb32ds2hr6vwnilbajz52q")))

(define-public crate-heckcheck-2 (crate (name "heckcheck") (vers "2.0.1") (deps (list (crate-dep (name "arbitrary") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "arbitrary") (req "^1.0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0fpg34jxjm6snpa2w3dqprkgimyx498grl05sfgilhwz238fzgs7")))

(define-public crate-heckdiff-0.1 (crate (name "heckdiff") (vers "0.1.0") (deps (list (crate-dep (name "difflib") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 2)))) (hash "0y01cn3vz7w01ljkz6b90rp9spici5c5pc38ki7z47fh9i2rfh68")))

(define-public crate-heckdiff-0.1 (crate (name "heckdiff") (vers "0.1.1") (deps (list (crate-dep (name "difflib") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "17zfcr9zd3m3f0vwrdrr1v3wvnlgbw11ayszj4fci2pxzdgk93b6")))

(define-public crate-heckdiff-0.1 (crate (name "heckdiff") (vers "0.1.2") (deps (list (crate-dep (name "difflib") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iqwa92jjzxb4a5md9r69wq1bazh1wa94jxspmi2qap3c8w40m8g")))

(define-public crate-hecker-0.1 (crate (name "hecker") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0b7xx2dq2ffyi0s9das2w7i9s4mdmdvvhy9g2dcidmvwpbhfj9ib")))

(define-public crate-hecker-0.1 (crate (name "hecker") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "1g80mahn9wyh50n6y0c96j35mjxwkac2ckjzn36rrz00q7qdgn1y")))

(define-public crate-heckmv-1 (crate (name "heckmv") (vers "1.0.0") (deps (list (crate-dep (name "errata") (req "^2.1.0") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "sarge") (req "^7.0.2") (default-features #t) (kind 0)))) (hash "0w3lcq8m4isvgqzhc6pg4p8jyma0478ny21y0m51s2cvgshrfqsf")))

(define-public crate-heckmv-1 (crate (name "heckmv") (vers "1.0.1") (deps (list (crate-dep (name "errata") (req "^2.1.0") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "sarge") (req "^7.0.2") (default-features #t) (kind 0)))) (hash "1w7pw6823b7gblxmm7wfp0mdrs63vv47g83nhq58mhd86fr1ranw")))

