(define-module (crates-io he me) #:use-module (crates-io))

(define-public crate-heme-0.3 (crate (name "heme") (vers "0.3.1") (hash "0mm8rj1i5wnc9iizrdc8w0zm18f8m2wgvr0fy0mxqdq8hfx84yp5")))

(define-public crate-heme-0.3 (crate (name "heme") (vers "0.3.2") (hash "0da25fg9l6wlhrsvmbkwcib8x1ycq16nrzphn13f97ckaadr30bb")))

(define-public crate-hemera-0.0.0 (crate (name "hemera") (vers "0.0.0") (hash "1f7a93y9793b9n9r7jzfk205frpd3v256m7ziayp47f1kdjv7hwh")))

