(define-module (crates-io he te) #:use-module (crates-io))

(define-public crate-hetero-container-1 (crate (name "hetero-container") (vers "1.0.0") (hash "06iryf8bnd0wr4y9m57cp1rm47m5dl4yskn5akl13qz1ssn2b34b")))

(define-public crate-heterob-0.1 (crate (name "heterob") (vers "0.1.0") (deps (list (crate-dep (name "funty") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1ycjr1li8a6x8vgzqs7mz4rx3g9kn8hy4acb8yz1b0qi8cyyvymz")))

(define-public crate-heterob-0.2 (crate (name "heterob") (vers "0.2.0") (deps (list (crate-dep (name "funty") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0v900szkqdnq68ipffwxpidzz2m5z3kgvxm3q06bpl58nd7s0wk9")))

(define-public crate-heterob-0.2 (crate (name "heterob") (vers "0.2.1") (deps (list (crate-dep (name "funty") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "18ifnsc2rjx0cr7bjdc07shkbjq7qlp898h5gbsp8xwz6mf9nr48")))

(define-public crate-heterob-0.2 (crate (name "heterob") (vers "0.2.2") (deps (list (crate-dep (name "funty") (req "^2.0.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1ghn8h7g8rylx7hdr2fzii5xbzrassyjh5sa9kqa34kg620afvsj")))

(define-public crate-heterob-0.3 (crate (name "heterob") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "funty") (req "^2.0.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "11m7v6gri1lmq0yljbziqkkf1z32rhbbbsffw4pyd4sqlzzmfh6z")))

(define-public crate-heterogeneous_graphlets-0.1 (crate (name "heterogeneous_graphlets") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "0ls62zkbzqj5klfdkmliyw5747gabd6g0y73khssj5djh22aph9q")))

(define-public crate-heterogeneous_graphlets-0.1 (crate (name "heterogeneous_graphlets") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)))) (hash "0n8ria5d7sd74ywlq6q4ydi0d8wdi7iay818zp4lkh3s60x9m6gv")))

(define-public crate-heterovec-0.1 (crate (name "heterovec") (vers "0.1.0") (hash "1qmscrik9k2hgzfrssym0xfmv55d62zm79s4kwq4jsa5idbk78ca")))

(define-public crate-heterovec-0.1 (crate (name "heterovec") (vers "0.1.1") (hash "12ri3bgd4v7fwgfrxvymfs4v6477r7lipi57nm11371dkz7lpa1l")))

(define-public crate-heterovec-0.1 (crate (name "heterovec") (vers "0.1.2") (hash "14azikizbm2ajcycvdrl5vrdcsyyi09p79g54xjqrbcq8d7abd7n")))

