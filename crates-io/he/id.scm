(define-module (crates-io he id) #:use-module (crates-io))

(define-public crate-heidi-0.1 (crate (name "heidi") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0nq5l825vi4jyjmviic3v9yin4xl1xs4cmvqlchxd81zq3vmp9w0")))

(define-public crate-heidi-0.2 (crate (name "heidi") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1irx7qpr54gnx3cfmidb8fgxg05xnw1x96j4nc8qbw438ps4v4km")))

