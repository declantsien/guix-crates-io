(define-module (crates-io he xv) #:use-module (crates-io))

(define-public crate-hexviewer-0.1 (crate (name "hexviewer") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "evalexpr") (req "^5.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0.0") (features (quote ("with-fuzzy"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline-derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0rn99x14ym2g3dwb9nhrzswpxc9sh29s4n9v9wisg2m61fa0ppp1")))

