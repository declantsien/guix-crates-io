(define-module (crates-io he ml) #:use-module (crates-io))

(define-public crate-hemlock-0.0.1 (crate (name "hemlock") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0w3114imq2vkh45gbh8jlkvxivxzh430vnfn7cvpzq643drpcc26")))

(define-public crate-hemlock-0.0.2 (crate (name "hemlock") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1fp9pzf661nasmrd9wba9ar0lair1g886kk7b5nbawcr0f510929")))

