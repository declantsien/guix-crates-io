(define-module (crates-io he xl) #:use-module (crates-io))

(define-public crate-hexlit-0.1 (crate (name "hexlit") (vers "0.1.0") (hash "1lyl1sga6a6vm6zfp56n9q6n0jgfvma957g77230nrhhgk4qjmvi")))

(define-public crate-hexlit-0.2 (crate (name "hexlit") (vers "0.2.0") (hash "1c4mgj5yycddkvl68iqy1xnyippzis7lm7xxsvz0h77zl0gl9db2")))

(define-public crate-hexlit-0.2 (crate (name "hexlit") (vers "0.2.1") (hash "0f8z5l9v5sagb975nsik12ni4cgf8pb21n9b7lby5dlf789cd586")))

(define-public crate-hexlit-0.3 (crate (name "hexlit") (vers "0.3.0") (hash "0fm1wj9di3xpij5xj2m3f8lyv7nsqldfb16qhyhhqjnpq4l1mak9")))

(define-public crate-hexlit-0.3 (crate (name "hexlit") (vers "0.3.1") (hash "1j493sfzwy6955s9cmva1kamdzw8wn16q9pp9c38xlsrzn8kqgdy")))

(define-public crate-hexlit-0.3 (crate (name "hexlit") (vers "0.3.2") (hash "0jlz21hq09k4l48xzbdkfz7vhixkk25rkm1b55hiqydg909v467y")))

(define-public crate-hexlit-0.4 (crate (name "hexlit") (vers "0.4.0") (hash "1jak231awp4r9xg9j271qmrskh3llvd7blb7k6ww101hgkn5qh0c")))

(define-public crate-hexlit-0.5 (crate (name "hexlit") (vers "0.5.0") (hash "0hf27ihbfa9ajydwr7drm1f7h46hvbyajzjf9rbnlghlzbp9nrj9")))

(define-public crate-hexlit-0.5 (crate (name "hexlit") (vers "0.5.1") (hash "07sjxgb16w0cspky66y4fh8p7dbs603hv5mbbysz3j8mzd3yh29a")))

(define-public crate-hexlit-0.5 (crate (name "hexlit") (vers "0.5.2") (hash "0yb3acsmxyyd41c5f8185i1c5d4lra70rq9mc9xl2h2ndh1wqk3l")))

(define-public crate-hexlit-0.5 (crate (name "hexlit") (vers "0.5.3") (hash "02vb2d2j1azgkkwl9j8jksjbhza9aahwv58mk1q9hvn0m0j4606j")))

(define-public crate-hexlit-0.5 (crate (name "hexlit") (vers "0.5.4") (hash "1g85z986b4bpxyf1z8zzvzw5mb6w31if6gjif51f813y4s9nlib9")))

(define-public crate-hexlit-0.5 (crate (name "hexlit") (vers "0.5.5") (hash "1zgzfhdcbbgj67nv59xkfnhdpblyr4jqpj4s7z2nl8flc347avjv")))

