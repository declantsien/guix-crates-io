(define-module (crates-io x_ ut) #:use-module (crates-io))

(define-public crate-x_util-0.0.1 (crate (name "x_util") (vers "0.0.1") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (optional #t) (kind 0)))) (hash "1nqqqj80slpnj82aak1ixg3jskfr4610mn830566rm5jmyqfjcip") (features (quote (("timeout" "tokio/time"))))))

