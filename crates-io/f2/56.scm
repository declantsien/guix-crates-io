(define-module (crates-io f2 #{56}#) #:use-module (crates-io))

(define-public crate-f256-0.1 (crate (name "f256") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "f256_pow10_div_pow2_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow2_div_pow10_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow2_div_pow5_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow5_div_pow2_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0f5qnw8lrv3qh3dh7wx5cjksjgyk5byq1j0vslgfdjzybbmi4rza") (features (quote (("std") ("default" "std"))))))

(define-public crate-f256-0.1 (crate (name "f256") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "f256_pow10_div_pow2_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow2_div_pow10_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow2_div_pow5_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow5_div_pow2_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "1njzjlqwxqc3mx13dacqrf553zfzrpf85zkpv6gv21lb5fz1iims") (features (quote (("std") ("default" "std"))))))

(define-public crate-f256-0.2 (crate (name "f256") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "f256_pow10_div_pow2_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow2_div_pow10_lut") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow2_div_pow5_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "f256_pow5_div_pow2_lut") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0lzqrlr219vf5h9ya2jvh2g1d8nj0ym1nw8vm8lkb7kjj7p8hbpv") (features (quote (("std") ("default" "std"))))))

(define-public crate-f256_pow10_div_pow2_lut-0.1 (crate (name "f256_pow10_div_pow2_lut") (vers "0.1.1") (hash "00ih03mhk46q31nd7la0jz5wn0hhar5ha7na744yscx4w4f8vjpn")))

(define-public crate-f256_pow2_div_pow10_lut-0.1 (crate (name "f256_pow2_div_pow10_lut") (vers "0.1.1") (hash "1sf818iphsrdm4kq4z3h8h3x01lprjgcvvcdj3638xlgqvs7hp46")))

(define-public crate-f256_pow2_div_pow10_lut-0.2 (crate (name "f256_pow2_div_pow10_lut") (vers "0.2.0") (hash "1d02l87pdbd1xp6cf7lrmmmp2v8b4cxrspd4kqzp5lqp9mhf3035")))

(define-public crate-f256_pow2_div_pow5_lut-0.1 (crate (name "f256_pow2_div_pow5_lut") (vers "0.1.1") (hash "0vngdw0bwc7zqndh9vkmd3nn15lz01zps0k4zc6vx9g2na643dvn")))

(define-public crate-f256_pow5_div_pow2_lut-0.1 (crate (name "f256_pow5_div_pow2_lut") (vers "0.1.1") (hash "1f4whqfpx4dmmi4w52bnqnirgzfxfx3shcq10jwp23pbl3ws03i5")))

