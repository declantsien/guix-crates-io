(define-module (crates-io pm sa) #:use-module (crates-io))

(define-public crate-pmsa-0.1 (crate (name "pmsa") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "1bklsh8dcz05rs410jrrja35c0f2jxfz0g9z5z6614xbv6p0gpzh")))

