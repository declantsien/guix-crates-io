(define-module (crates-io pm ap) #:use-module (crates-io))

(define-public crate-pmap-0.0.4 (crate (name "pmap") (vers "0.0.4") (hash "0w0659pczb72k46b7yy0dqmyf65x2z8v8rhhlxjichy62rakjvcm")))

(define-public crate-pmap-0.0.5 (crate (name "pmap") (vers "0.0.5") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1988bjpnmrz4j2jy41rcbh3kv8lr8nl1awil2546wfhj3j0q6v5x")))

