(define-module (crates-io pm ac) #:use-module (crates-io))

(define-public crate-pmac-0.0.0 (crate (name "pmac") (vers "0.0.0") (hash "0kk0j094wci38r05qasrhyn44a08ffrvikdx5w1kdmn9qz9vc8cj")))

(define-public crate-pmac-0.1 (crate (name "pmac") (vers "0.1.0") (deps (list (crate-dep (name "aesni") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "block-cipher-trait") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.6") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "dbl") (req "^0.1") (default-features #t) (kind 0)))) (hash "053pp5bg4bb819nhc67n2vpl3h5f95qj6b1lm5a97jad8l9cr0ha")))

(define-public crate-pmac-0.2 (crate (name "pmac") (vers "0.2.0") (deps (list (crate-dep (name "aes") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "block-cipher-trait") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.7") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "dbl") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rb1x9s6h7k5bws9fmpadq1hnwxq7gcz0y9h0sys8rxahqzdcxmx")))

(define-public crate-pmac-0.3 (crate (name "pmac") (vers "0.3.0") (deps (list (crate-dep (name "aes") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "block-cipher") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.8") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "dbl") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fr6a6zyspi7fkjxf4am7g2bhhs75dfk05mg9f4xx5wm7jkxm6wp")))

(define-public crate-pmac-0.4 (crate (name "pmac") (vers "0.4.0") (deps (list (crate-dep (name "aes") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "crypto-mac") (req "^0.9.1") (features (quote ("block-cipher"))) (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.9.1") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "dbl") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zkj78lprlbdw1r2k64k9fqf8cr210znhwlfaaa75bzs8va7hy31") (features (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.5 (crate (name "pmac") (vers "0.5.0") (deps (list (crate-dep (name "aes") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "crypto-mac") (req "^0.10") (features (quote ("cipher"))) (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.10") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "dbl") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vs7cpr4nxrjv80d1mqs9i4qvqcqbv14inad0mkg3rbfc0ycfylh") (features (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.5 (crate (name "pmac") (vers "0.5.1") (deps (list (crate-dep (name "aes") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "crypto-mac") (req "^0.10") (features (quote ("cipher"))) (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.10") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "dbl") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jq5wlfqrivpskv3h6qax7wyq5hqdp5483gl2mhpx6hhi7mmghp1") (features (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.6 (crate (name "pmac") (vers "0.6.0") (deps (list (crate-dep (name "aes") (req "^0.7") (features (quote ("force-soft"))) (default-features #t) (kind 2)) (crate-dep (name "crypto-mac") (req "^0.11") (features (quote ("cipher"))) (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.11") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "dbl") (req "^0.3") (default-features #t) (kind 0)))) (hash "04z0w16ni1ln86brihv3f430rhj5wmcy4yp2icd17dxkzamcdn22") (features (quote (("std" "crypto-mac/std"))))))

(define-public crate-pmac-0.7 (crate (name "pmac") (vers "0.7.0") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dbl") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.2") (features (quote ("mac"))) (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10") (features (quote ("dev"))) (default-features #t) (kind 2)))) (hash "0avg0856miq872p5mhj08kp2np9blnl4wz8cx5nig2ral26aaj7v") (features (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (rust-version "1.56")))

(define-public crate-pmac-0.7 (crate (name "pmac") (vers "0.7.1") (deps (list (crate-dep (name "aes") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "cipher") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "dbl") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.3") (features (quote ("mac"))) (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.3") (features (quote ("dev"))) (default-features #t) (kind 2)))) (hash "01z4dmfnbch8x3l5i4q6gwqaisddyrsk90sp5dr6lc7hxxvzf8ps") (features (quote (("zeroize" "cipher/zeroize") ("std" "digest/std")))) (rust-version "1.56")))

(define-public crate-pmacro_ruly-0.1 (crate (name "pmacro_ruly") (vers "0.1.0") (hash "1rwfrjwm3mmgn9nkjn11wl7z1wl3qpgcyychfwmpcaqzpbjxq9r1")))

(define-public crate-pmacro_ruly-0.1 (crate (name "pmacro_ruly") (vers "0.1.1") (hash "1zrwd6f3p4k35nyg8a9k30k4yd7kad5qbyhibjygdgr6vrx08vnw")))

(define-public crate-pmacro_ruly-0.2 (crate (name "pmacro_ruly") (vers "0.2.0") (hash "0nmsfnpkpbr4hwlbd8w747wyks9s67yxn80d8vqvv595pnyms8fw")))

(define-public crate-pmacro_ruly-0.2 (crate (name "pmacro_ruly") (vers "0.2.1") (hash "1nn59rif7vnkrv1gq9ghpchvfsiz4jm4vhksmzbj7d54n24dznss")))

(define-public crate-pmacro_ruly-1 (crate (name "pmacro_ruly") (vers "1.0.0") (hash "1hk9wnmm37bifz6q1vzyl1fa5xas4bqmk4nfm300ym84j9i7hz0c")))

(define-public crate-pmacro_ruly-2 (crate (name "pmacro_ruly") (vers "2.0.0") (hash "0nzv48r3n1dvabli3dps5vnw5c89r9p4bjj4nw2ylhszhaa3sajm")))

(define-public crate-pmacro_ruly-2 (crate (name "pmacro_ruly") (vers "2.1.0") (hash "0kfx9pl28qqya3z0nrkh8lxd4dw3pl2dzvrxbcydljsin89qbvvw")))

(define-public crate-pmacro_ruly-2 (crate (name "pmacro_ruly") (vers "2.2.0") (hash "167k79qbi17i8wgja980jwbjq7nz2ahbjm0g0ag3mkvz3vhl1xvd")))

