(define-module (crates-io pm w1) #:use-module (crates-io))

(define-public crate-pmw1-0.1 (crate (name "pmw1") (vers "0.1.0") (hash "15hbsx9x38bcwqyrlbmp8pyaazpjpch2wcpbi81zn5m4x7bg84h1")))

(define-public crate-pmw1-0.2 (crate (name "pmw1") (vers "0.2.0") (hash "0mqxard43brk512i4a5xsc2qlf1qlxbvasj13q0xx77g59ynz1sd")))

(define-public crate-pmw1-0.2 (crate (name "pmw1") (vers "0.2.1") (hash "08nvwf1kpvszav1jcn2mfgs4y4lk74m2v6y3fn44a8qnx0drf687")))

(define-public crate-pmw1-0.2 (crate (name "pmw1") (vers "0.2.2") (hash "0ca4bnrhamq7wxjb2yi2vr0v8rgqc77i22vyk9dw5iw8ii2mfrs5")))

