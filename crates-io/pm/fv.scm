(define-module (crates-io pm fv) #:use-module (crates-io))

(define-public crate-pmfview-0.1 (crate (name "pmfview") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "libpolymesh") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "raylib") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0d3v7acv6pc7mrl3qgzssaa73zxmbsq7g7y6vs44mqlrl1scwm0p")))

