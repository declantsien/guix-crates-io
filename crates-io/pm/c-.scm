(define-module (crates-io pm c-) #:use-module (crates-io))

(define-public crate-pmc-rs-0.1 (crate (name "pmc-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmc-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dvarjvp2g9l1f71hqc9c993qwhhvbn22nzy7iwnnmj7xvgm8cnx")))

(define-public crate-pmc-rs-0.1 (crate (name "pmc-rs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmc-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0wdhy3zvw36j05p49ym15yq860clpga4yqm839nni8wd926ap8b8")))

(define-public crate-pmc-rs-0.2 (crate (name "pmc-rs") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmc-sys") (req "^0.1.2") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)))) (hash "1j65gkxw8sh0v0b3w4wa185fqs3kfh7mpkc5arqi12v0vkkqzwxj")))

(define-public crate-pmc-rs-0.2 (crate (name "pmc-rs") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmc-sys") (req "^0.1.2") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)))) (hash "0v1vmdn839vkhliam5fsqf8xw0n11y41vh4lvv2ws1vfzr1156pa")))

(define-public crate-pmc-rs-0.2 (crate (name "pmc-rs") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmc-sys") (req "^0.1.3") (default-features #t) (target "cfg(target_os = \"freebsd\")") (kind 0)))) (hash "1kf2zxsy9ydc9hw4w8h97v6rgm43zjdnrscxfbs2zgijqspv4cwx")))

(define-public crate-pmc-sys-0.1 (crate (name "pmc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.33") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wyng87qb66y16qb3jlkmrdfxgqqvdvimvlnrfcnwdfn8xy5dgl3")))

(define-public crate-pmc-sys-0.1 (crate (name "pmc-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sv11zdw897npxfhdwfn1dbj6i15x6pb68rfh2r6srqa7lzs8vdg")))

(define-public crate-pmc-sys-0.1 (crate (name "pmc-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "10jinx70fxw9cx5g27ng4df90nycmghmnjzdlglqdrgy1ijq8qqq") (links "pmc")))

(define-public crate-pmc-sys-0.1 (crate (name "pmc-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s4h84ji0iy16cvs7q263izf9nwfbhnb0ygxplb4m837d62wmb22") (links "pmc")))

