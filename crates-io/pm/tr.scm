(define-module (crates-io pm tr) #:use-module (crates-io))

(define-public crate-pmtree-1 (crate (name "pmtree") (vers "1.0.0") (deps (list (crate-dep (name "ark-serialize") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34.7") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak"))) (default-features #t) (kind 2)))) (hash "1wcf2fkl9v0b4rm2ql77bsk6ri7jaa5cnwzb7ws702gxynsxba6q")))

(define-public crate-pmtree-2 (crate (name "pmtree") (vers "2.0.0") (deps (list (crate-dep (name "ark-serialize") (req "=0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "=0.3.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "=1.7.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "=0.34.7") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "=2.0.2") (features (quote ("keccak"))) (default-features #t) (kind 2)))) (hash "047m4s6ncr58x941qmv7bmgy90k62sbpgf27rn3csb3dx4p34m70")))

