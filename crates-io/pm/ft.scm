(define-module (crates-io pm ft) #:use-module (crates-io))

(define-public crate-pmftree-0.1 (crate (name "pmftree") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libpolymesh") (req "^1") (default-features #t) (kind 0)))) (hash "0zq0pcw60wmnskl42sd967jmgbdixrdgya8mycv3yjnn0wq3mbdx")))

