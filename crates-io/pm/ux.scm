(define-module (crates-io pm ux) #:use-module (crates-io))

(define-public crate-pmux-0.0.0 (crate (name "pmux") (vers "0.0.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "0l2rfyikz5k0lkhy3i9c96n36xhain4bzldd6wigwx15lrc4rj4m") (yanked #t)))

(define-public crate-pmux-0.0.1 (crate (name "pmux") (vers "0.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "1fwfbkzp18bv548rpin2maxg53lr766ilmfq6zqvajn51na75wi8") (yanked #t)))

(define-public crate-pmux-0.0.2 (crate (name "pmux") (vers "0.0.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)))) (hash "166jypnlwaw0pc2kzgl9xsh3nv15p4hmxc11pd6bwyaf08xhljh2") (yanked #t)))

