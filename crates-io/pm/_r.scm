(define-module (crates-io pm _r) #:use-module (crates-io))

(define-public crate-pm_rlua-0.1 (crate (name "pm_rlua") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "td_clua") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0c9ay562nwrmbwp5abp358d802kp9s82ffwpbaw1p6fbmkmlkab9")))

