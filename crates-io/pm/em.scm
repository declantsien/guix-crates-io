(define-module (crates-io pm em) #:use-module (crates-io))

(define-public crate-pmem-0.0.0 (crate (name "pmem") (vers "0.0.0") (hash "0jih98adbx31az4jvadzzvy5li3bj063dr3ldba9ibp9q157xpqm")))

(define-public crate-pmem-0.0.1 (crate (name "pmem") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pxk57dxx008wfyhbx4032km23yzxh9pvzjgvzf4xr8pjnkrv6hi")))

(define-public crate-pmem-0.0.2 (crate (name "pmem") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmem-sys") (req "^0.0") (default-features #t) (kind 0)))) (hash "1jkdcac2aax6xyhv1bfakzi2nqk4apg9v4pipx2ffyd04fyc03hn")))

(define-public crate-pmem-0.1 (crate (name "pmem") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmem-sys") (req "^0.0") (default-features #t) (kind 0)))) (hash "1fvffvvk68hnyyq3v1z6ab2c1zf5cc3r6rcq6f2mkh61qy5lgfy9")))

(define-public crate-pmem-blk-0.0.1 (crate (name "pmem-blk") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmemblk-sys") (req "^0.0") (default-features #t) (kind 0)))) (hash "1jz1fjngd0jz3j7il43w6ajqfmsb9350gmh033vvq73sam4cq296")))

(define-public crate-pmem-blk-0.0.2 (crate (name "pmem-blk") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmemblk-sys") (req "^0.0") (default-features #t) (kind 0)))) (hash "1hhk13ma5f6lfc6i82jcgfhm0isv4c84811vcb5rvccq3r6vz312")))

(define-public crate-pmem-log-0.0.1 (crate (name "pmem-log") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmemlog-sys") (req "^0.0") (default-features #t) (kind 0)))) (hash "1aynlac3qrad0zllzn979abgrc1hqazg6sx04503i81lfffk16s9")))

(define-public crate-pmem-obj-0.0.1 (crate (name "pmem-obj") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pmemobj-sys") (req "^0.0") (default-features #t) (kind 0)))) (hash "0spl4f9j8573wrq3lpn5ig0smy622fjyfjqfxdgpj7y2j3hhi2b0")))

(define-public crate-pmem-sys-0.0.1 (crate (name "pmem-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xmwzbnxialj1lcjxrsdlvq2k4qmr6qnsblliqh5wd2695iik5d8")))

(define-public crate-pmem-sys-0.0.3 (crate (name "pmem-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hnqppdhvd396mp8lsvh1vyylii3gdjdq26b4ls4ysn8pdf5q8cs")))

(define-public crate-pmemblk-sys-0.0.1 (crate (name "pmemblk-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cdb50arg9k56igpsphp4wbih74f5lsppn2421bxhqfljlzzw891")))

(define-public crate-pmemblk-sys-0.0.2 (crate (name "pmemblk-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ngyy47pqjm6zkcnkc43xl7nfqvzhcd3rgy1ikfcbh04wcryhm4v")))

(define-public crate-pmemkv-0.1 (crate (name "pmemkv") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pmemkv-sys") (req "^0.1.0-alpha.0") (default-features #t) (kind 0)))) (hash "1ga0r3h0kyxxfshmbsj7lkv359f7klqgsz75ljp9zs51675vm9wf")))

(define-public crate-pmemkv-0.1 (crate (name "pmemkv") (vers "0.1.1-alpha.0") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pmemkv-sys") (req "^0.1.0-alpha.0") (default-features #t) (kind 0)))) (hash "1dk6q91hayq4d3y8pbwin0m8jql31ldz3sv9gi8r5ng0v143ib9g")))

(define-public crate-pmemkv-0.1 (crate (name "pmemkv") (vers "0.1.2-alpha.0") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pmemkv-sys") (req "^0.1.0-alpha.0") (default-features #t) (kind 0)))) (hash "06jmxh5pnlr5n4m29rzd0g7j7b41qrnzb8g49qzb6az44sd9nm1a")))

(define-public crate-pmemkv-0.1 (crate (name "pmemkv") (vers "0.1.3-alpha.0") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pmemkv-sys") (req "^0.1.0-alpha.0") (default-features #t) (kind 0)))) (hash "1fp2kvzpd2j757nqc7wpjcjjp1b52dmia8pwg5zvpm31dqbg5l59")))

(define-public crate-pmemkv-sys-0.1 (crate (name "pmemkv-sys") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "13fwzfxyaz974m916rlaa88ki13x8aji7bwy6nh0ackzi1r2c5ac")))

(define-public crate-pmemlog-sys-0.0.1 (crate (name "pmemlog-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j529m3psx89cxgh2nyd670579yrwsy90c84zkh4h6y49sgyv3a4")))

(define-public crate-pmemobj-sys-0.0.1 (crate (name "pmemobj-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0djsdd6airbmrvwqqv20fm2chl3wda2ykl1z51q28glmd99nsm2l")))

