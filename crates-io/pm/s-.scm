(define-module (crates-io pm s-) #:use-module (crates-io))

(define-public crate-pms-7003-0.1 (crate (name "pms-7003") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0kx43l72n6ffvr417w0vknr1hxd8k5jav2baqx5335i3xgawjwmy")))

(define-public crate-pms-7003-0.2 (crate (name "pms-7003") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1fxq904ml1f48zlsvk4ar6i4qzrr04ib5h5rh2fbkqhw6z4syg4f")))

(define-public crate-pms-7003-0.3 (crate (name "pms-7003") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1pga3j8z0fpgbldfw212ghv6vf7y0vhk8m1l60qzb7fixay56wkj")))

