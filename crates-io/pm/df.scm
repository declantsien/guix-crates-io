(define-module (crates-io pm df) #:use-module (crates-io))

(define-public crate-pmdfonttool-1 (crate (name "pmdfonttool") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "pmd_cte") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "pmd_dic") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1mvy95qcylwkgb3dn83bhwcpij18zzm8clkjw55hcsm9j3rkn8hk")))

(define-public crate-pmdfonttool-1 (crate (name "pmdfonttool") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "fontdue") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "pmd_cte") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "pmd_dic") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1c4nxf2g5qajvbpcwj136insrf1dab5qyja4d0fan2prwillmksb")))

