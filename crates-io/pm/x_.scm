(define-module (crates-io pm x_) #:use-module (crates-io))

(define-public crate-pmx_parser-0.1 (crate (name "pmx_parser") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vnrf0r7vzx7zf8w46skr2fm1fhd7vywzvihns1yi465dvjg9xbb")))

(define-public crate-pmx_parser-0.1 (crate (name "pmx_parser") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "018hhz55bas9biyp6smfjn4dg7c7a7rb9igcfwfwrq0chzvrg3vd")))

(define-public crate-pmx_parser-0.2 (crate (name "pmx_parser") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0a7zs5bifq1axc3qadsyf3032z3022c78abc9gfpiamig2prjzz6")))

