(define-module (crates-io pm wi) #:use-module (crates-io))

(define-public crate-pmwiki-rss-discord-bot-1 (crate (name "pmwiki-rss-discord-bot") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2") (features (quote ("tls"))) (kind 0)))) (hash "1xyf7p0dijzlv8czgzi2skihryh47i4llq5wwarz4hsv5sm632qg")))

