(define-module (crates-io pm s7) #:use-module (crates-io))

(define-public crate-pms700x-0.1 (crate (name "pms700x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "01hwgargbypapvgkv05f9k85m1vwx0ibznqljdb84dd16iilakzn")))

(define-public crate-pms700x-0.1 (crate (name "pms700x") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hah1rimxfknpxi3835vp5xrjql4l1wy2k5whlam3nw3lgd7qsxp")))

