(define-module (crates-io pm he) #:use-module (crates-io))

(define-public crate-pmhelp-0.0.1 (crate (name "pmhelp") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "10k7d3n18in6n3kkvb4s0mpcs08my4jxw4mswvbjmssgny8lh51w")))

(define-public crate-pmhelp-0.0.2 (crate (name "pmhelp") (vers "0.0.2") (deps (list (crate-dep (name "pmhelp-internal") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1akpivgdrp5hwrxg56zmppf091cf5ahh97gwd3v8c1gac70y86wz")))

(define-public crate-pmhelp-internal-0.0.1 (crate (name "pmhelp-internal") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0c4nxyz6jjsanr44f89h2i6g66v4c2i920rfxgmsmj8y6jcwyx1j")))

(define-public crate-pmhelp-internal-0.0.2 (crate (name "pmhelp-internal") (vers "0.0.2") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1rk0vpz2qdxxq2v3kmxcl1vhdpg712c5gp1bj7d6xd6khab1hwwf") (features (quote (("from-base"))))))

(define-public crate-pmhelp-internal-macros-0.0.1 (crate (name "pmhelp-internal-macros") (vers "0.0.1") (deps (list (crate-dep (name "pmhelp-internal") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0s10h28jlx1z8z953vwmfgzn0a2sxr351yb6svxr1150s0xmdmdw")))

(define-public crate-pmhelp-internal-macros-0.0.2 (crate (name "pmhelp-internal-macros") (vers "0.0.2") (deps (list (crate-dep (name "pmhelp-internal") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1rpmhwv2b9ab36vyyjbhay8aiwc3gk5z721iiji7kdsj8z6pddfn")))

