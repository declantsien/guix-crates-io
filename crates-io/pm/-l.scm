(define-module (crates-io pm -l) #:use-module (crates-io))

(define-public crate-pm-lexer-0.0.1 (crate (name "pm-lexer") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "0l90r6505hlw5lfwb52di6bh4i68p04xdzj08ikr8qza6xmlb3rr")))

(define-public crate-pm-lexer-0.1 (crate (name "pm-lexer") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "0w7xz55rdwcsqvm5r9k0pq75ig0d044hdvyx2c8lbwqh89nk10xf")))

(define-public crate-pm-lexer-0.1 (crate (name "pm-lexer") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "0y9cz0wdl9r3f9bkaz76wrg48ch30dynxyv0l5zkk0aymb5m1wg8")))

(define-public crate-pm-lexer-0.1 (crate (name "pm-lexer") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "1nx5w0vlpvk0h3gxxlbzrmwbsyiaawxxsxrxbj561kf266ir2bmi")))

(define-public crate-pm-lexer-0.1 (crate (name "pm-lexer") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "1gilidjyjajhxhrc77r100w2fqvykpsvwx9iz0fr91h0wy28v23f")))

(define-public crate-pm-lexer-0.1 (crate (name "pm-lexer") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "00wnwybjwxgylp0yb8xmi5rgy5z675j5dc4j4vdbyrnb0ijnwg38")))

