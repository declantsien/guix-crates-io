(define-module (crates-io pm w3) #:use-module (crates-io))

(define-public crate-pmw3901-1 (crate (name "pmw3901") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "spidev") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1pwazbr9myvk70n36lcb6zqjn3crc5ryszyqldgz8l5nsld6w69f")))

(define-public crate-pmw3901-ehal-0.1 (crate (name "pmw3901-ehal") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-rtt-core") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "040g6gbj8yzp13nhwbwvdjyppjql54j4s8gkhn8kwslgvbhgv6y7") (features (quote (("rttdebug" "panic-rtt-core") ("default"))))))

