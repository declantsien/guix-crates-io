(define-module (crates-io pm dr) #:use-module (crates-io))

(define-public crate-pmdr-0.1 (crate (name "pmdr") (vers "0.1.0") (hash "1sn9lnw509nkina7m2jjsmwzjmk3j8cp239fqgpm7flfv40szsa7")))

(define-public crate-pmdr-0.2 (crate (name "pmdr") (vers "0.2.0") (hash "0cd07knnf5k03ycjzvir6rg9g653har19bj90gridyycm4hgfm60")))

(define-public crate-pmdr-0.3 (crate (name "pmdr") (vers "0.3.0") (hash "0rxly1gnhzry0qmi2riy3lwzf5b1x9idkbyic76j3sipn70ifhi7")))

