(define-module (crates-io pm pm) #:use-module (crates-io))

(define-public crate-pmpmc-0.1 (crate (name "pmpmc") (vers "0.1.0") (hash "0krfncisr2bfg82avyjkv0bpn3d3pvjpa8f8fl852lg335lipalj")))

(define-public crate-pmpmc-0.1 (crate (name "pmpmc") (vers "0.1.1") (hash "1jvxzwd3y8xbvnpy45csl4lq2gvcv7bvp0lhh322z0yml7rmx3z3")))

(define-public crate-pmpmc-0.1 (crate (name "pmpmc") (vers "0.1.2") (hash "0b75jkwfczm8knbv0m1lxa4zisib2cgki0kc2lhhznp5hlsj71m3")))

(define-public crate-pmpmc-1 (crate (name "pmpmc") (vers "1.0.0") (hash "155ysz2mw5sswr8d4cnfkg18zp540xjd187j6cfiv45xawh8021y")))

