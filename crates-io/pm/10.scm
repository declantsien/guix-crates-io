(define-module (crates-io pm #{10}#) #:use-module (crates-io))

(define-public crate-pm1006-0.0.1 (crate (name "pm1006") (vers "0.0.1") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1sk3gvjcrd7lmwdzd4i9h2an8dz5y9lwp3pl381kk1fw5sp0z9k5") (features (quote (("default" "log"))))))

(define-public crate-pm1006-0.0.2 (crate (name "pm1006") (vers "0.0.2") (deps (list (crate-dep (name "embedded-io") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1r1918895bmac53j4hxfw7c8kvpg007bnf3rl2sk9jvs3xkxjjyr") (features (quote (("default" "log"))))))

