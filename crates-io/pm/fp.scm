(define-module (crates-io pm fp) #:use-module (crates-io))

(define-public crate-pmfpack-0.1 (crate (name "pmfpack") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "libpolymesh") (req "^1") (default-features #t) (kind 0)))) (hash "08zdr6hc74l5h7lq785ij3sx7fi6d45bcl940f3l8df33yhcvfx8")))

