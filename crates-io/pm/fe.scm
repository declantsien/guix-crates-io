(define-module (crates-io pm fe) #:use-module (crates-io))

(define-public crate-pmfextract-0.1 (crate (name "pmfextract") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "libpolymesh") (req "^1") (default-features #t) (kind 0)))) (hash "0p035ck2f0jvr3s7lmhzka95j8702bmqgw3zhllzb5lk3p4w0lcf")))

