(define-module (crates-io pm fc) #:use-module (crates-io))

(define-public crate-pmfcube-0.1 (crate (name "pmfcube") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "libpolymesh") (req "^1") (default-features #t) (kind 0)))) (hash "1qjihj4s1ccvwkz38yhy8f572897fy5r9h0fviihv38a98cnhs7f")))

