(define-module (crates-io pm ut) #:use-module (crates-io))

(define-public crate-pmutil-0.1 (crate (name "pmutil") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.2.1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.3") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "1mjlkmzyvcxs6627d97qjlba2a8af3y7lziy04aawdm3s4mq30sl")))

(define-public crate-pmutil-0.2 (crate (name "pmutil") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.1") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "1d384hhaq1ga564b5wgvdvbrxhj61c2q0kc2n6w69qrq5kqbfn1z")))

(define-public crate-pmutil-0.3 (crate (name "pmutil") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.26") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "1z1686k2dj281b7slra8ghh8zd1ciryp7mflv3hacjqxpncri36w")))

(define-public crate-pmutil-0.4 (crate (name "pmutil") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "00z0960r92zhmz2dy83sm7zxnavhb0mfq162i4x9k7qybsggb201")))

(define-public crate-pmutil-0.5 (crate (name "pmutil") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "1wffpfq2n8vqmqsq4bdvmal0lsk7x43b9fk0gwxmg23lqgk8z7l8")))

(define-public crate-pmutil-0.5 (crate (name "pmutil") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "00r85njpj1784k5kghihw07izhpcs73fzk1nxf23b433rm2xskaz")))

(define-public crate-pmutil-0.5 (crate (name "pmutil") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "1bsqni870j30ycrmhlgr2skcd1x8mwmhnm497znl5jiv8047xfrl")))

(define-public crate-pmutil-0.5 (crate (name "pmutil") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "0170zgziivri4qsch682pga3qq3z4wpr4wngzr5f9jyc97ayb51q")))

(define-public crate-pmutil-0.6 (crate (name "pmutil") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "0zb0x3bdh09paqy7gpxhjr7na0nm34fgksnb9lq5sg1137lw1lkc")))

(define-public crate-pmutil-0.6 (crate (name "pmutil") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("derive" "parsing"))) (kind 0)))) (hash "1ih9gbgcygfyrxqlgm3smffzqngzlnlpn5lb5l6h8n1c1k3hp92j")))

