(define-module (crates-io aw oo) #:use-module (crates-io))

(define-public crate-awoo-0.1 (crate (name "awoo") (vers "0.1.0") (deps (list (crate-dep (name "try-guard") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rx5rvfzg71zhv4g6bcafj5792j69x5bk34cifbj4fgnx91c2f5j")))

(define-public crate-awoo-0.2 (crate (name "awoo") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "try-guard") (req "^0.2") (default-features #t) (kind 0)))) (hash "0irfrfv0w747g3dnji9jmbcl1g5kh6pn21gj6f9dng8ain3ajha7") (features (quote (("json" "serde") ("default" "json"))))))

