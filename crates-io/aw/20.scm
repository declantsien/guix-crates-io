(define-module (crates-io aw #{20}#) #:use-module (crates-io))

(define-public crate-aw2013-1 (crate (name "aw2013") (vers "1.0.0") (deps (list (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1n5s75kvll28ysc8zk5l1n8gi5gday94cf1sw8v6f87ai00mn8a5")))

(define-public crate-aw2013-1 (crate (name "aw2013") (vers "1.0.1") (deps (list (crate-dep (name "rppal") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "09f4262fnw642mal12v7aigmz0g0xi30gc45fs0a1s0z7wapwk4n")))

(define-public crate-aw2013-2 (crate (name "aw2013") (vers "2.0.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.4.0-alpha.4") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 2)))) (hash "1rwvzv307xbm9bk6bjybyfwq7n4sgp7pnfp9jqlhdwc828d79g7j")))

(define-public crate-aw2013-2 (crate (name "aw2013") (vers "2.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.4.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 2)))) (hash "0irfpbw67nrqh8wciks41852l9i11wlc2zcpayyypl3cjxz6c6y3")))

