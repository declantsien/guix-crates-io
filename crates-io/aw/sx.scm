(define-module (crates-io aw sx) #:use-module (crates-io))

(define-public crate-awsx-0.1 (crate (name "awsx") (vers "0.1.0") (hash "0hycyhzsrxl34sacdw1llll0yiqcyy1d4iqfb6z1i6mmx46y3kmq")))

(define-public crate-awsx_cli-0.1 (crate (name "awsx_cli") (vers "0.1.0") (hash "0fvsgicn9lhzpcrw4341jzdpf8yiw9cg907zpq3mpghq4hi9i5ny")))

