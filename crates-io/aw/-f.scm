(define-module (crates-io aw -f) #:use-module (crates-io))

(define-public crate-aw-fel-0.1 (crate (name "aw-fel") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3") (default-features #t) (kind 0)))) (hash "02xbj72iz4s0aa8l4p78wa7vahqmxjzhgw1v4jzw4m16azraz6gi")))

(define-public crate-aw-fel-0.2 (crate (name "aw-fel") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x42arlvm6njl47dypygcb2y5d524xv8ynv75djgd3y7rp03n3dj") (features (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.3 (crate (name "aw-fel") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xk55nd8piix4bxkkjx33i45z0q05497j2z7wm9v7503wqz164gf") (features (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.4 (crate (name "aw-fel") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1ma1kc0cf082kzzq29lw11n5g1yjdwx0c3sxb9zxhlrqk5spki16") (features (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.4 (crate (name "aw-fel") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1h33xwv4y7qv20xllbfw017qgzk8w61h6rf58kc2nsrixyx3bmb7") (features (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.5 (crate (name "aw-fel") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1zr5ghqj4vp95cxw3ghsf7i74i7n52dhbznfljcmdzbhdnv8zzb9") (features (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.5 (crate (name "aw-fel") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0fb6lfrray1jg0fraviw4dyj2d4vc2f2qkiwcbk7wm2g99cz814m") (features (quote (("uboot") ("default" "uboot"))))))

(define-public crate-aw-fel-0.5 (crate (name "aw-fel") (vers "0.5.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "07s76rygm6rsjh3y3i95ijwrl7wva4kp1d828wcw4yi16sahv4fl") (features (quote (("uboot") ("default" "uboot"))))))

