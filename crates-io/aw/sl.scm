(define-module (crates-io aw sl) #:use-module (crates-io))

(define-public crate-awsl-0.1 (crate (name "awsl") (vers "0.1.0") (hash "1bcbjcfn5nvpq45fnjw7s43y8rxg440km32yfyn908czv71aba9f") (features (quote (("default"))))))

(define-public crate-awsl-error-0.1 (crate (name "awsl-error") (vers "0.1.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.0") (features (quote ("std" "serde" "rayon"))) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "type-uuid") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "yggdrasil-shared") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1943sxxp795j9zxihjbwi9044kgxnv9l67nzcm07vy1mbz729kn3")))

(define-public crate-awsl-pest-0.1 (crate (name "awsl-pest") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_generator") (req "^2.1.3") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0.16") (default-features #t) (kind 2)))) (hash "0h4rsbb3f3zcaw7kdanrnqsb83p74wi4grvha4msvqqygm1wxmgk")))

(define-public crate-awsl-syn-0.1 (crate (name "awsl-syn") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.89") (features (quote ("full" "parsing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0nmnylfzwnklbd71n2fc1h2ggkp6i47n63fpvlwl964mk6kcx62m")))

(define-public crate-awslc-0.1 (crate (name "awslc") (vers "0.1.0") (hash "0h6p64vfmlv2n6ypfchcqbwm846znx117ycyvjp8nzl77yjlrsai") (yanked #t)))

