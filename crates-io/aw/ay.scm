(define-module (crates-io aw ay) #:use-module (crates-io))

(define-public crate-away-0.0.0 (crate (name "away") (vers "0.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0jwfsv3qv0wg5h0snkwfc5rijar2fcw8r84b925myyx2fp9439rh") (features (quote (("default"))))))

