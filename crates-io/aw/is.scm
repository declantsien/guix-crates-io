(define-module (crates-io aw is) #:use-module (crates-io))

(define-public crate-awis-rs-0.1 (crate (name "awis-rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.15") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1z921jn5bgwfj90zrqz70iqbadr7hg1r90j5s48r624922hcavf0")))

(define-public crate-awis-rs-0.1 (crate (name "awis-rs") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.15") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0cxy32i84jlmpl379ddqmgw5gi3dsy45s0n7rs5c0g3mnp4xkzxa")))

