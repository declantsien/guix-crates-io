(define-module (crates-io aw f-) #:use-module (crates-io))

(define-public crate-awf-codegen-0.1 (crate (name "awf-codegen") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19009kxfpvj0fpgcwnf2848ghqnd8qnjrfsl5qkqckdhj0r76mb8")))

(define-public crate-awf-codegen-0.1 (crate (name "awf-codegen") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13sv3rvsmzvky9mri3a4mf8qznmvpgp5n350012h213381a6fxv8")))

(define-public crate-awf-codegen-0.1 (crate (name "awf-codegen") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1m5j5lb3qg0pq1gv448pxlksfywb15vz64l2cnv2g4kjlcmnwfkw")))

(define-public crate-awf-help-0.1 (crate (name "awf-help") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "awf-codegen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "inventory") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w4vpp5cggrsxjqh2892ddldvxfb34gikycf66skplmrijk336mn")))

(define-public crate-awf-help-0.1 (crate (name "awf-help") (vers "0.1.1") (deps (list (crate-dep (name "actix-web") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "awf-codegen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "inventory") (req "^0.1") (default-features #t) (kind 0)))) (hash "1di35qlw1b7j2q2sjq675lcla9a9461zkdf9pvnda14nsp8cmq34")))

(define-public crate-awf-help-0.1 (crate (name "awf-help") (vers "0.1.2") (deps (list (crate-dep (name "actix-web") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "awf-codegen") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "inventory") (req "^0.1") (default-features #t) (kind 0)))) (hash "1m0p2c76vn334hwkibkh313vi0s3v6bjhb4jhxnlycabxmfnl1r3")))

