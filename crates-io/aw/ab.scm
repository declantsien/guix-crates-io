(define-module (crates-io aw ab) #:use-module (crates-io))

(define-public crate-awabi-0.1 (crate (name "awabi") (vers "0.1.0") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0q6k0afrvaq8pnvm7f8r1q1nx2jiq6k7ygyxz2hk30l879smp5ik")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "01vcl073lcs1bwjjykq0gfy1jz353fzdv2p1wnkkvspnl6hzfqzm")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1zgw9vy4prf5x43ypqijfn0amy771q9pcq3rpbgsrh8cb9q5hfcc")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0hw09pxl51ga3pzrqlm84h97kn3dr3374zxnj0pwlxhd84mj7csh")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1jfc68951kpkvqyizpxi3yifhpd53jk9g903g10rxw7xan2xmk3b")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "19kvxfraw1brvxhgryybq6msz5pz7i0fcj3hsh357qgk1xmiqwhx")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.5") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0a1cinc9bw32nn610yhzq7s60xfqdls17cgq9vn42yc0wcmsp45r")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.6") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1vaw8ld1dfxm825q4gkj78mgsr1cqz7v21hn7lapkq4cfn58nr9v")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.7") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "03cpn5fivvkgkp00a42wdwcyx78z9n1a2i8nskb2yi54a3scn3ld")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.8") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "15ciqkj9y1d464piqps2wikg2l3v1fcffmpwsbb9jkhrq5vw5rwj")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.9") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1zzgxsw7k0ywcf2idgrajvncbkl3m5qr12glfq4brgvjxxv5jnxq")))

(define-public crate-awabi-0.2 (crate (name "awabi") (vers "0.2.10") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0vh6gsxhb1nhyzjdh1y263fsz975iil8bl23xnd95za6p7saj6cb")))

(define-public crate-awabi-0.3 (crate (name "awabi") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0izcj51awdfnp5zf2ibk8bwhp1h4vmgkh6izh97cshmwgq9518nd")))

