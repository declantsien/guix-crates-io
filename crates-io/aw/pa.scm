(define-module (crates-io aw pa) #:use-module (crates-io))

(define-public crate-awpack-0.1 (crate (name "awpack") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.14") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hqvxa03yd7lks0219prx3i2ay29331gf6b3rz98sjfzc590qjg4")))

