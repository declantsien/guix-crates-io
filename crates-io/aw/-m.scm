(define-module (crates-io aw -m) #:use-module (crates-io))

(define-public crate-aw-models-0.1 (crate (name "aw-models") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.7") (features (quote ("chrono"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1n60db86pd2945nrq4ixgisvfaq1ik3bfg0ijjk1a0qyk13wi8lv")))

