(define-module (crates-io oc ro) #:use-module (crates-io))

(define-public crate-ocron-0.1 (crate (name "ocron") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "07m5v6ynkgs68z0dz51l8cvvh10yyjrjxd2ssvhab24jbiyhjvb9")))

(define-public crate-ocron-0.2 (crate (name "ocron") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1pnmi2yck05f5d5ajlxrmpl6c6ai2k7x4l28znmyf2syll0d206b")))

