(define-module (crates-io oc ap) #:use-module (crates-io))

(define-public crate-ocap-0.0.0 (crate (name "ocap") (vers "0.0.0") (hash "099srpr70xk5z8a87vc8xi7qm00app0vzic3igqssxad3psx6pp6") (yanked #t)))

(define-public crate-ocapi-0.0.0 (crate (name "ocapi") (vers "0.0.0") (hash "05dlpwnrsjcwhmzzmvvqmvwrb06vi5mjb11qhs7wp4h3gi694pys")))

