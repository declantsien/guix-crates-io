(define-module (crates-io oc kt) #:use-module (crates-io))

(define-public crate-ockta-0.1 (crate (name "ockta") (vers "0.1.0") (deps (list (crate-dep (name "gel") (req "*") (default-features #t) (kind 0)))) (hash "00sqmd5cfc2r1hrzmmi51zx3278mn8azgy294fl9xjkjpnjnbm9q")))

