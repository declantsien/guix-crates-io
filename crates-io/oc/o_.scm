(define-module (crates-io oc o_) #:use-module (crates-io))

(define-public crate-oco_ref-0.1 (crate (name "oco_ref") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0sxcf4qfmfl4ha0kgixsg6ijpp7c8lrjqdzlrysbpmwsjj02ghf8") (rust-version "1.75")))

(define-public crate-oco_ref-0.1 (crate (name "oco_ref") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0217bj4r4hdsyk87dwgg04dms8g4i8n565d1pvhabfghnbpvq7n5") (rust-version "1.75")))

(define-public crate-oco_ref-0.2 (crate (name "oco_ref") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "165rmfvk4h33jykn1ndsvgdcxwj9g1xg2zzn3ib63a1rzs14kfb4") (rust-version "1.75")))

