(define-module (crates-io oc hr) #:use-module (crates-io))

(define-public crate-ochre-0.1 (crate (name "ochre") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.21.0") (default-features #t) (kind 2)) (crate-dep (name "usvg") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "0dqy9azzz17lvpknf0c02inrah8k6fk2n5f8qlc2ivad14n88ixs")))

(define-public crate-ochre-0.2 (crate (name "ochre") (vers "0.2.0") (deps (list (crate-dep (name "gl") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "glutin") (req "^0.21.0") (default-features #t) (kind 2)) (crate-dep (name "usvg") (req "^0.13.0") (default-features #t) (kind 2)))) (hash "1xhikql3njk3483ip4zlil2a2ywyl3cxkqfd16zahn2q07n57qj8")))

