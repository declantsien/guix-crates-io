(define-module (crates-io oc he) #:use-module (crates-io))

(define-public crate-ochenslab-0.0.1 (crate (name "ochenslab") (vers "0.0.1") (hash "1mvrdss8z1h5f1b4d3micknp4vv21ybyk43yjwiyz7wp8fz2kj0n")))

(define-public crate-ochenslab-0.0.2 (crate (name "ochenslab") (vers "0.0.2") (hash "1pq4whwsk2x7rp2i8d7l2iwbzdsp4r71ci40lhhhwyal68myxm9n")))

