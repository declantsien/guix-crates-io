(define-module (crates-io oc ct) #:use-module (crates-io))

(define-public crate-occt-sys-0.1 (crate (name "occt-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0281g67kiaam9r9pp04352gdkq6qjzhbbcmqx59bh02ar8zf55ww")))

(define-public crate-occt-sys-0.2 (crate (name "occt-sys") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1688744f4szfxr9l6bazz996w3wr1d3iwff9qpbw30zlazcagjks")))

(define-public crate-occt-sys-0.3 (crate (name "occt-sys") (vers "0.3.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "02h35jv8n5m1az15xhcpv4fv58l3pmy2abmwvx3vyhhradkf5xxb")))

