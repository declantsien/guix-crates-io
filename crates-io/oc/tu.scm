(define-module (crates-io oc tu) #:use-module (crates-io))

(define-public crate-octussy-0.2 (crate (name "octussy") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pk1ajwb85qci5m1lki9v8nc3alm8vla3lsv5n9q1wp64y3rncjv")))

(define-public crate-octussy-0.3 (crate (name "octussy") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1xfwdgh1cfp93xp31wfg4hib584dhlic12y8vw45grrasddij8cl")))

(define-public crate-octussy-0.3 (crate (name "octussy") (vers "0.3.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "06acj9qb9hpa2jfc2zg5k883x9wq0vyrv0b5fh482k2bwzjwl59w")))

(define-public crate-octussy-0.3 (crate (name "octussy") (vers "0.3.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0brfxcv9mjj67i9f94nkj5j43hm0xzqi4qxrjr58cs3n7n92kagp")))

(define-public crate-octussy-1 (crate (name "octussy") (vers "1.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "05vj28sf1vv85i3ks0jb7cxayjr13nirj83p5b13pbks1vk7dzq2")))

(define-public crate-octussy-1 (crate (name "octussy") (vers "1.0.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1pd0897kg8li6gvijrdyqvr3zapq99rbl3sra6rvfhyyrnpgvph2")))

(define-public crate-octussy-1 (crate (name "octussy") (vers "1.0.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1q86d7fbz42q6sy9fwqbz0k6kslzk5a0rxqflc0hm1yny54rpj1a")))

