(define-module (crates-io oc b3) #:use-module (crates-io))

(define-public crate-ocb3-0.0.0 (crate (name "ocb3") (vers "0.0.0") (hash "01430y10xvhc4k4hknqvz406ydcb0dl26l2267xymra3xgs8msm3")))

(define-public crate-ocb3-0.1 (crate (name "ocb3") (vers "0.1.0") (deps (list (crate-dep (name "aead") (req "^0.5") (kind 0)) (crate-dep (name "aead") (req "^0.5") (features (quote ("dev"))) (kind 2)) (crate-dep (name "aes") (req "^0.8") (kind 2)) (crate-dep (name "cipher") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ctr") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "subtle") (req "^2") (kind 0)) (crate-dep (name "zeroize") (req "^1") (optional #t) (kind 0)))) (hash "1nyyj0rx870iv24ad8j2x55qlabald1pazkpslyq8727dhky15n1") (features (quote (("stream" "aead/stream") ("std" "aead/std" "alloc") ("rand_core" "aead/rand_core") ("heapless" "aead/heapless") ("getrandom" "aead/getrandom" "rand_core") ("default" "alloc" "getrandom") ("arrayvec" "aead/arrayvec") ("alloc" "aead/alloc")))) (rust-version "1.60")))

