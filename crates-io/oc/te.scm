(define-module (crates-io oc te) #:use-module (crates-io))

(define-public crate-octets-0.1 (crate (name "octets") (vers "0.1.0") (hash "03948fdngdinazf8gbqrr89sbjiiq6l2ydhs8b644r6phgn0da9z")))

(define-public crate-octets-0.2 (crate (name "octets") (vers "0.2.0") (hash "002hs18023rsdpwwjrzg71hxpryl6scgh2hlcc53mm14lz6z4x1s")))

(define-public crate-octets-0.3 (crate (name "octets") (vers "0.3.0") (hash "168xxcix9gxrx9n7rg3im3z25vn9znjinwvnc28yh7i7j6h8768h")))

(define-public crate-octez-0.1 (crate (name "octez") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0k4svrh92xjrrq9y9yn7cn21yjdv3w044j52dh5jw8lmpm0mxqak") (yanked #t)))

(define-public crate-octez-0.1 (crate (name "octez") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "12fwiipgwc75izizswcgpdpl347908bg0rh1bmwqpiqyzwl3hgpp")))

