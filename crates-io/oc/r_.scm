(define-module (crates-io oc r_) #:use-module (crates-io))

(define-public crate-ocr_b_checksum-0.1 (crate (name "ocr_b_checksum") (vers "0.1.0") (hash "0w3xskr02jdprsli8zdhnbs45bdf71svyrkiq9k54i4pmxghbz1l")))

(define-public crate-ocr_b_checksum-0.1 (crate (name "ocr_b_checksum") (vers "0.1.1") (hash "1ckdcs58qa696xnsl1pg9m8q37y1zgrpyf6cy8p8ch09v8d1f2qk")))

(define-public crate-ocr_core_rs-0.1 (crate (name "ocr_core_rs") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0cqasp5h3yn46mlqhx5kbq26sw1sfnxx42m9pgil3zp6gs1kl9dq")))

(define-public crate-ocr_latin_vocabulary-0.1 (crate (name "ocr_latin_vocabulary") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "164f6n09gfa7rr5vhhna13sch8j4v40s991b2lpx4aab38vra4yz")))

