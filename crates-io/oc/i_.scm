(define-module (crates-io oc i_) #:use-module (crates-io))

(define-public crate-oci_cfg-0.2 (crate (name "oci_cfg") (vers "0.2.0") (deps (list (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "1yd46m2gzavxb7jd85xyd5bhv3lq4v5ypk0xl8mn2w2i4ppmmmm4")))

(define-public crate-oci_rs-0.3 (crate (name "oci_rs") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "0z0zym64rkilf9dm4d3gq807skfbfaiskmajadviqga4d6g479iq")))

(define-public crate-oci_rs-0.3 (crate (name "oci_rs") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "1x979j0b3qvfzka9hp6jcyqynqykp84064ifyc1vkx6mwqck068y")))

(define-public crate-oci_rs-0.3 (crate (name "oci_rs") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "1hnw97rjwbp2dmvss2g1hzb3ibd9sbksp69lc59l5l8kiclq74yn") (yanked #t)))

(define-public crate-oci_rs-0.4 (crate (name "oci_rs") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "0pb4x7gxwib4v4lss821zn81gvqqdx59n3ylx13cy73xq8slib0g")))

(define-public crate-oci_rs-0.5 (crate (name "oci_rs") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "1znli41l72hqvr11a2cllp85s5ffwwv7dnzf4585rdj49yr98b0w")))

(define-public crate-oci_rs-0.6 (crate (name "oci_rs") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "0ccb9cfrqnb01a30a91djawvx8v6lp2j13is0m9y7a7xrch03qm6")))

(define-public crate-oci_rs-0.7 (crate (name "oci_rs") (vers "0.7.0") (deps (list (crate-dep (name "build-helper") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "1mqkg3cgpd5hsqb4bkdsj6sib0ydx22mq1yipwmcwzhdwj6z9w0d")))

(define-public crate-oci_rs-0.8 (crate (name "oci_rs") (vers "0.8.0") (deps (list (crate-dep (name "build-helper") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "12ip3l9wc3y6szgyvajhgj24yxxxbwd7czj4sdlaf07xqrspq3b2")))

