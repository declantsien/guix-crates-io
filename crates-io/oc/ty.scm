(define-module (crates-io oc ty) #:use-module (crates-io))

(define-public crate-octyl-0.1 (crate (name "octyl") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ropey") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ph8gs910mcd2ck0mvamh2xln2c8mrngxspshc5acx47qn3r3qz1")))

