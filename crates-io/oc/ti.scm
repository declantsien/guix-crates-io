(define-module (crates-io oc ti) #:use-module (crates-io))

(define-public crate-octicons-0.1 (crate (name "octicons") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "svgparser") (req "^0.5") (default-features #t) (kind 1)))) (hash "0bz82cdra9970wn2sw79y2vd1nzm4yj8jwhqrmq8z7qdr97y0lzj")))

(define-public crate-octicons-0.1 (crate (name "octicons") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "svgparser") (req "^0.5") (default-features #t) (kind 1)))) (hash "10gmkffsl7wmdiccg3zs25ijw69b26rp1nk1f82p9g4r2bhn8f9j")))

(define-public crate-octicons-0.2 (crate (name "octicons") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "xmlparser") (req "^0.13") (default-features #t) (kind 1)))) (hash "12sn1p9j5z3bq77cw04ig9fji3kfcbb8hgz1qairfd9lpnyq1793")))

