(define-module (crates-io oc y-) #:use-module (crates-io))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "0iqxv6xpqk7hvihj2q7acj67m9g9bcf96m81ij3ip68w7k0mh2xz")))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "1f1nf6qzixwpyw0c1q26bbvks2bng3xl62cgg3zm5658grmc0vkh")))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.2") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dk3pc9zzc69pwlhmyyi1s90rsgirjzjmjmgq6vjn31kcfbiggsk")))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.3") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lk0h0slfby6xj0gcy9dngb60qyzf16n0ci4n248rafbj99b0nld")))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.4") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "12hplz91gbddnlii22hf6v8md2cf2z9mb2wnnwghzjzsfazna89i")))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.5") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "101zk0wpa1d975f335pnpnnb45wn8db0mjjcyxyl6jvai00z8q3w")))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.6") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "030kxjs900xs8v74kibzzlxz24c7j5v05j1vfx3kbxjcadw6qjh7")))

(define-public crate-ocy-core-0.1 (crate (name "ocy-core") (vers "0.1.7") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "0im52n70s3psbfvb2wvbyh2qmgf7q91vvxkxhypvbfhnldiqkrdd")))

