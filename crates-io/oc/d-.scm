(define-module (crates-io oc d-) #:use-module (crates-io))

(define-public crate-ocd-cli-0.0.1 (crate (name "ocd-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.1.4") (default-features #t) (kind 0)))) (hash "04wxvq4j9afsj4mqica28dzgs2k62nxcplsz91hkl6vl136gz06x")))

