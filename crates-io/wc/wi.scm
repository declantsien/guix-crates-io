(define-module (crates-io wc wi) #:use-module (crates-io))

(define-public crate-wcwidth-1 (crate (name "wcwidth") (vers "1.0.0") (hash "0gwyfri8xqka7m22j1q4vyjpphf52xwfgpki431hvw3dnxyvas6m") (yanked #t)))

(define-public crate-wcwidth-1 (crate (name "wcwidth") (vers "1.0.1") (hash "13i7vpiys24vz1lz83ycmxa37mf2bwxcr8f00ssd2a1n40yjmwyv") (yanked #t)))

(define-public crate-wcwidth-1 (crate (name "wcwidth") (vers "1.12.1") (hash "159h4nc4s7qxqj7h3rjgwk29szia6kac3wii4a920ws1iqg9n1ji") (yanked #t)))

