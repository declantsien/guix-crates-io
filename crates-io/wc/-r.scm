(define-module (crates-io wc -r) #:use-module (crates-io))

(define-public crate-wc-rs-0.1 (crate (name "wc-rs") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)))) (hash "1md08qayr22949v9ld8prb52i58hb0pd176mfh5fhii40jz3b8am")))

(define-public crate-wc-rs-0.1 (crate (name "wc-rs") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)))) (hash "1r2zqlhgs570jk4mcqh5an7s33d18klnzag9nbkghijp4h0j3b10")))

(define-public crate-wc-rs-0.1 (crate (name "wc-rs") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)))) (hash "1i1i67z581106059i649xkpk1x4vn338xqwcbrqfpdaqg7y7r6rz")))

