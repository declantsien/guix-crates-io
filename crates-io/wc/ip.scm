(define-module (crates-io wc ip) #:use-module (crates-io))

(define-public crate-wcip-0.1 (crate (name "wcip") (vers "0.1.0") (deps (list (crate-dep (name "attohttpc") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "pledge") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "pobsd-db") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "pobsd-parser") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0lyyp29bdyqn1414jl993vq3w1qhqby0mqlm2v0ypk3g33bz9ika")))

(define-public crate-wcip-0.1 (crate (name "wcip") (vers "0.1.1") (deps (list (crate-dep (name "attohttpc") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "pledge") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "pobsd-db") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "pobsd-parser") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1gpz5yy6xgfdz22syjgnqrlwkisb8p80rgldn4wc7hlrny2i3gyq")))

(define-public crate-wcip-0.1 (crate (name "wcip") (vers "0.1.2") (deps (list (crate-dep (name "attohttpc") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "pledge") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "pobsd-db") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "pobsd-parser") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0fqhkffaskr98fqhd3fksfxbm4r5fnlgxy0nxykzl9rc7p9jmvhg")))

