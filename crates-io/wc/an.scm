(define-module (crates-io wc an) #:use-module (crates-io))

(define-public crate-wcanvas-0.0.1 (crate (name "wcanvas") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.72") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.28.6") (default-features #t) (kind 0)))) (hash "1s7la3b0ajinc0c2fqgz7wvgf8i59m2r22h54ih94dbz1h641gjr")))

