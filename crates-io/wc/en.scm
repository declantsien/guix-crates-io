(define-module (crates-io wc en) #:use-module (crates-io))

(define-public crate-wcensor-0.1 (crate (name "wcensor") (vers "0.1.0") (deps (list (crate-dep (name "wtest_basic") (req "~0.1") (default-features #t) (kind 2)) (crate-dep (name "wtools") (req "~0.1") (default-features #t) (kind 0)))) (hash "05m9kgzibfhdivyfl4k3qj01ck001k9rwnfy49wjvmqn0kyzj5qv")))

(define-public crate-wcensor-0.1 (crate (name "wcensor") (vers "0.1.1") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)) (crate-dep (name "wtools") (req "~0.2") (default-features #t) (kind 0)))) (hash "1qmjb6saak1bd3r8ymj55q41wvhpycjjdxbackvpw69fywm591z0") (features (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

