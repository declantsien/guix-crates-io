(define-module (crates-io wc om) #:use-module (crates-io))

(define-public crate-wcomm-0.1 (crate (name "wcomm") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "fundsp") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0v9n9qcws8g9g08m4wk77mnng8m3lmfzi6a3sr2sgqw0p92pylqg")))

(define-public crate-wcomm-0.1 (crate (name "wcomm") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "fundsp") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1hvalx5wgb4qrb099k8abxgkdnajr51vpx54g231sqfzmsy4f2a6")))

(define-public crate-wcomm-0.1 (crate (name "wcomm") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "fundsp") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1wlam1h9j15v7h8hk7zshm58ljml1ckcmhyp4xmnwd728j1hddgw")))

(define-public crate-wcomm-0.1 (crate (name "wcomm") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "fundsp") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0g3z4f2ya47avdl04knl77z2sbij4lgz6i3max3zachs4wx6dwlx")))

