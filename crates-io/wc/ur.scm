(define-module (crates-io wc ur) #:use-module (crates-io))

(define-public crate-wcurses-0.0.1 (crate (name "wcurses") (vers "0.0.1") (deps (list (crate-dep (name "libw") (req "^0") (default-features #t) (kind 0)))) (hash "13y02rpmni8rmsc6z467i3gcdm173hyq1qcflbpjq7dddmdzv0rp")))

(define-public crate-wcursorgen-0.1 (crate (name "wcursorgen") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "riff-ani") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y6qk3lnsh39j8hdzdz9pkpn2s86i8863zq7w2a7ziraysfvhpfh")))

(define-public crate-wcursorgen-0.1 (crate (name "wcursorgen") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "riff-ani") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10hb46sk85z7sg4kliwl473zbxx1i6ynn42vppgb8jf1ngd0ssq6")))

