(define-module (crates-io wc al) #:use-module (crates-io))

(define-public crate-wcal-0.1 (crate (name "wcal") (vers "0.1.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1yxgc0dl1r6h6cb6w59rf27qcky034spnvv2z0rz1yshhjdy4fv5")))

(define-public crate-wcal-0.2 (crate (name "wcal") (vers "0.2.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0a1l62vsi21jd6jzc9azmcy6r2ygqn2vig7mspdwdw9b4crz165y")))

