(define-module (crates-io wc xh) #:use-module (crates-io))

(define-public crate-wcxhead-0.1 (crate (name "wcxhead") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "ntdef"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1llbiw2s5y35ss87c5spa7v4qxww4h86jwsmf0q0ysjphlc43hr6")))

