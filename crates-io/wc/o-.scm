(define-module (crates-io wc o-) #:use-module (crates-io))

(define-public crate-wco-rs-0.1 (crate (name "wco-rs") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pn6qf0z60jxlzv2m5c62n6a4g7ymcxppal5m6bkfwg60j2iybwq")))

(define-public crate-wco-rs-0.1 (crate (name "wco-rs") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "091i87py4i13ksh6sklldha5i63jxfkpjrsrnk1k5f0j42jgnvr0")))

(define-public crate-wco-rs-0.1 (crate (name "wco-rs") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.31.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h8451fi4z61kab4cf362lcszlbbj3b6bcdizc5vq4h080zw63rx")))

