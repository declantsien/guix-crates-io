(define-module (crates-io wc rs) #:use-module (crates-io))

(define-public crate-wcrs-0.1 (crate (name "wcrs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13s6930hqp042fi70jz4n2f064j0ws5cc1kvpgz3dl3rfx9h1cja")))

(define-public crate-wcrs-0.2 (crate (name "wcrs") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xn5vg3mkldj07hv9gzci51qi4y355f680094kr8bbyc1lq4zcxa")))

