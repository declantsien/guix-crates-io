(define-module (crates-io wc -g) #:use-module (crates-io))

(define-public crate-wc-grab-0.1 (crate (name "wc-grab") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.22.0") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "00pjibwh5c7cnydlb7pmv279vlmrh6ny2bw38g92zqnml0p0w6dg")))

(define-public crate-wc-grab-0.3 (crate (name "wc-grab") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.22.0") (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.12.3") (default-features #t) (kind 0)))) (hash "0sij2qkmb8nvxgdzbisv0n7pmi9dl7kfwkc266rk9f0qwacwj327")))

