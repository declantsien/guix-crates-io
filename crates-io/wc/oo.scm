(define-module (crates-io wc oo) #:use-module (crates-io))

(define-public crate-wcookie-0.1 (crate (name "wcookie") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1ns383rsari0hw1d903fibq854n50slkvjmi8k5nds424amqi5w5")))

(define-public crate-wcookie-0.1 (crate (name "wcookie") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1a49vi041fh21s9df0zxqf088bqcp4kcijl2a31v03d4xcsal36b")))

(define-public crate-wcookie-0.1 (crate (name "wcookie") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "003gimycvw6xhkgzqnmaz0zyzmq0k4137wffq2mfkqyz5zkz1aai")))

(define-public crate-wcookie-0.1 (crate (name "wcookie") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1dgdcwrrw9lsmgj4gkv2rk7rpzhdkzvrvc38rspwvs1h4y75jc3h")))

