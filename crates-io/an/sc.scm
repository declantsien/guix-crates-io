(define-module (crates-io an sc) #:use-module (crates-io))

(define-public crate-anscape-0.1 (crate (name "anscape") (vers "0.1.0") (hash "0x97br8vgvp7kn18bmnk7crplv71zj1wa58vfdda3r7i9i7hy5rs")))

(define-public crate-anscape-0.1 (crate (name "anscape") (vers "0.1.1") (hash "1n4jf880p5axhv61f69d93ig4jf0frklkm16jk81hjv5f6gnc3vm")))

(define-public crate-anscape-0.2 (crate (name "anscape") (vers "0.2.1") (deps (list (crate-dep (name "paste") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0bkfaxb369v3bflxdikaa7p279fpiq524bixccfqm5rma8c719q7")))

(define-public crate-anscape-0.2 (crate (name "anscape") (vers "0.2.2") (deps (list (crate-dep (name "paste") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0iknw1g3rx0ccrb1j1lwfv5f786zg42lgyd892risivan61pkm61")))

