(define-module (crates-io an -d) #:use-module (crates-io))

(define-public crate-an-data-0.1 (crate (name "an-data") (vers "0.1.0") (hash "06kbp7waqpl09difqgw2hkcpck7lv0msv9wra3djqpbk5jz5hkc7")))

(define-public crate-an-db-0.1 (crate (name "an-db") (vers "0.1.0") (hash "1b1762zasjxrl86hra6xi97rwl0kxj55bq4sl3r270a8wf8cx29m")))

