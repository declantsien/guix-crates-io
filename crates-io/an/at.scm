(define-module (crates-io an at) #:use-module (crates-io))

(define-public crate-anat-rs-0.1 (crate (name "anat-rs") (vers "0.1.0") (hash "1lyvqbw6ariyjryshhs9941bh8xm3cksjlk7hdqv0pl8mfk7hjhl")))

(define-public crate-anat-rs-0.1 (crate (name "anat-rs") (vers "0.1.1") (hash "0qygicac5667bgvb6gigp4crj4f4x17ljsx2wi3fh7mddx4cpzw2")))

(define-public crate-anathema-0.1 (crate (name "anathema") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.83") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0jpj4vq087nbw6kng1zirhcima8yr7sx8jgc7n9xgazdlgj4cmp6") (features (quote (("widgets") ("templates" "widgets") ("serde-json" "serde_json") ("runtime" "templates") ("logging" "log") ("default" "runtime")))) (v 2) (features2 (quote (("flume" "dep:flume")))) (rust-version "1.62")))

(define-public crate-anathema-0.1 (crate (name "anathema") (vers "0.1.1-alpha") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.83") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0v9q1axfvg5z9zsaji8s1bsz7b8283jrmiq46cnxa818788xkgyq") (features (quote (("widgets") ("templates" "widgets") ("serde-json" "serde_json") ("runtime" "templates") ("logging" "log") ("default" "runtime")))) (v 2) (features2 (quote (("flume" "dep:flume")))) (rust-version "1.62")))

(define-public crate-anatolia-0.1 (crate (name "anatolia") (vers "0.1.0") (hash "0azkp4dd9h15yd56i3a5jnliax2m0kn5bd6b82a7v45zvpa5384v")))

