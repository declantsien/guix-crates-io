(define-module (crates-io an od) #:use-module (crates-io))

(define-public crate-anode-0.1 (crate (name "anode") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "188iwasaks2svj53wrhrjalvls6qr6lzzfah60667ilvjvnj6l6p")))

(define-public crate-anodize-0.0.0 (crate (name "anodize") (vers "0.0.0") (hash "0znqbswh9wpnmkhqjsa8xfzqdzpawah1sc6c5nynr9wr9khfavx2")))

