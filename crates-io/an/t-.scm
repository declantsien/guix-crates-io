(define-module (crates-io an t-) #:use-module (crates-io))

(define-public crate-ant-derive-0.1 (crate (name "ant-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pky29h776zn6rj05lal2j8wwgscr1j02yd8bl3x2j13fjs2jbig")))

(define-public crate-ant-design-rs-0.1 (crate (name "ant-design-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "implicit-clone") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0") (features (quote ("rand" "serializing"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "stylist") (req "^0") (features (quote ("yew_integration" "yew_use_media_query" "yew_use_style"))) (default-features #t) (kind 0)) (crate-dep (name "yew") (req "^0") (features (quote ("csr"))) (default-features #t) (kind 0)))) (hash "0dy5d7cigkfil9jnq999nb7ya8gsr8hyv2kkz14gcxs62sicqz0l")))

(define-public crate-ant-plus-0.0.1 (crate (name "ant-plus") (vers "0.0.1") (hash "0zln2fmcszs2w8ic9dv3152723r3h9dkfgpzg291ss6yzpy8vcgc") (yanked #t)))

(define-public crate-ant-sys-0.1 (crate (name "ant-sys") (vers "0.1.0") (hash "1ipp8lcmbggj57qg62j193ilpa34a65ddpvw8wk717xs1jjgqcxz")))

(define-public crate-ant-usb-0.0.1 (crate (name "ant-usb") (vers "0.0.1") (hash "0panl95pjl83flgwzyjszklsn3hqqbxakckspdzv15ldisnz15xm") (yanked #t)))

