(define-module (crates-io an tr) #:use-module (crates-io))

(define-public crate-antr-0.4 (crate (name "antr") (vers "0.4.0") (deps (list (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.13") (default-features #t) (kind 0)))) (hash "0xqa97xk7bbr4nf1mxkqvhxy2m8p94iawdv9bdlpskzvj1wrxjw1")))

