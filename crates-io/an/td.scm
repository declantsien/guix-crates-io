(define-module (crates-io an td) #:use-module (crates-io))

(define-public crate-antd-sys-0.1 (crate (name "antd-sys") (vers "0.1.0") (hash "003cnzsra5sgahcp4463q13zha6vr5xp8m5pccaz32ynfi4nc1h8")))

(define-public crate-antd-yew-0.1 (crate (name "antd-yew") (vers "0.1.0") (hash "1kmlh2bn56l01vk659wkh42q9a0pddzs1dnl6s205yn55prrfb3a")))

