(define-module (crates-io an as) #:use-module (crates-io))

(define-public crate-anas-0.1 (crate (name "anas") (vers "0.1.0") (hash "0ghz248a9h69nr2yam2b5m3ck4mmgdslmjxxig2ig1c29g0lh195")))

(define-public crate-anas-0.1 (crate (name "anas") (vers "0.1.1") (hash "0ryw2i5jphhs11hay552lwv12pxd2r43fbbvy9v6s62dyf6a5h0y")))

(define-public crate-anaso_api_models-0.1 (crate (name "anaso_api_models") (vers "0.1.0") (hash "0f6cjdc5xml9yrk3gidnm1s66ibqp2vdxcd5bshbpdvfvy2i0wgi")))

(define-public crate-anaso_sdk-0.1 (crate (name "anaso_sdk") (vers "0.1.0") (hash "016p6ncb7qx9vfh83vzpy9by6mrwql5x13if9idq4kb6q91fbrcm")))

(define-public crate-anaso_sdk-0.2 (crate (name "anaso_sdk") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bs58") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde-email") (req "^3.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "06b29inj1qgc1390618vdscqag6nmjalrz7llhrr0rf57fzvz1q3") (features (quote (("models" "bitflags" "serde-email" "chrono") ("default" "client" "models") ("client"))))))

