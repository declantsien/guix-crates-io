(define-module (crates-io an ub) #:use-module (crates-io))

(define-public crate-anubis-0.0.1 (crate (name "anubis") (vers "0.0.1") (hash "1ji12918s0ijdv1jpp2az871hrksllnr8mv4c0ah6y2495cfm7yi")))

(define-public crate-anubis-0.0.2 (crate (name "anubis") (vers "0.0.2") (deps (list (crate-dep (name "gilrs") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "slint") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "slint-build") (req "^1.3.2") (default-features #t) (kind 1)))) (hash "07h2xhw8rvpq16s2pqr8z1nnc4sdkd7ihri59j07l0i9kbi35y2n")))

