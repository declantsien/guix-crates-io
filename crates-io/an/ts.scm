(define-module (crates-io an ts) #:use-module (crates-io))

(define-public crate-antsi-0.0.0 (crate (name "antsi") (vers "0.0.0-reserved") (hash "0a8pv4jczqa739g3pfkq9jd3hfx574c6ccf4d0dpq40rwh3pm2hw") (rust-version "1.63.0")))

(define-public crate-antsy-1 (crate (name "antsy") (vers "1.0.0") (hash "0qwlpmffn0kpyjpicv8k5kwqxhkyyxv4sbwp59ylxnr7zz3z2kxm")))

(define-public crate-antsy-1 (crate (name "antsy") (vers "1.1.0") (hash "1il0g5x1vggha7ad1v6cqzlk9g6an8fdvsxscvqv4a62k257sirw")))

