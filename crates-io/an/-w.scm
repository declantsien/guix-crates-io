(define-module (crates-io an -w) #:use-module (crates-io))

(define-public crate-an-web-0.1 (crate (name "an-web") (vers "0.1.0") (hash "0xavcg6n8iwcf60alv5pbadn45ddvqyl8js91yzsh5bgdr2vjaap")))

(define-public crate-an-websocket-0.1 (crate (name "an-websocket") (vers "0.1.0") (hash "13c6vw1nw71wjbvj3133kinda6ahjivdyql9hvsacz0324vb7fz1")))

(define-public crate-an-wechat-0.1 (crate (name "an-wechat") (vers "0.1.0") (hash "1jqclypg62v9smbyivcrl4bcfxzbs6w4s8c52bl9zyv8zc3hqini")))

(define-public crate-an-weixin-0.1 (crate (name "an-weixin") (vers "0.1.0") (hash "1facbr3w68a867lkv1r78kia6xrs4vhm71qryz7js43npnhg84f3")))

(define-public crate-an-windows-0.1 (crate (name "an-windows") (vers "0.1.0") (hash "15rc4l28634yzf71kc1m80d5kphla1bmfcs8f04a56chnvdzj7qa")))

(define-public crate-an-ws-0.1 (crate (name "an-ws") (vers "0.1.0") (hash "034mcngjlf3ffq81jf3rq483sqw93gzbz5n8p1ja6ys2hkcl14ib")))

