(define-module (crates-io an xi) #:use-module (crates-io))

(define-public crate-Anxiety-0.0.0 (crate (name "Anxiety") (vers "0.0.0") (hash "1bnpwhv6gmdhvgqb68ram08wapyanp73c9zmhzdpxg8b5qbqbdrr")))

(define-public crate-anxious-0.1 (crate (name "anxious") (vers "0.1.0") (hash "03w0v3r0qgkcfpskb1b5d9zc6ifm6bkp5nq3zi6fymwn2mvcs70r") (features (quote (("nightly"))))))

