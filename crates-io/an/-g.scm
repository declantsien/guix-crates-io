(define-module (crates-io an -g) #:use-module (crates-io))

(define-public crate-an-gms-0.1 (crate (name "an-gms") (vers "0.1.0") (hash "13vxh0f8j0k2ms4nlb6rng0h8l18bsy93bwaxjyw3f4i26w177xz")))

(define-public crate-an-gui-0.1 (crate (name "an-gui") (vers "0.1.0") (hash "0y1sk56k2ihiwl9yn6g0c2clgn1qjvmxzrbrm4554vrh6jsgrpqn")))

