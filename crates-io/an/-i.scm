(define-module (crates-io an #{-i}#) #:use-module (crates-io))

(define-public crate-an-image-0.1 (crate (name "an-image") (vers "0.1.0") (hash "12qjx4cj0aim3nvwjzzaazyk3a1ld9jn6cmnnghfm1x8j8cxmw8l")))

(define-public crate-an-ios-0.1 (crate (name "an-ios") (vers "0.1.0") (hash "01yk6kz7b5c3rzc8qnd5v3sbs6k2qzrm4kr8ql41fx2qn4gwrnvi")))

