(define-module (crates-io an -a) #:use-module (crates-io))

(define-public crate-an-android-0.1 (crate (name "an-android") (vers "0.1.0") (hash "0ip3qjkqysrbwf39xnqqgw7n43ygkqrsg3x7kj2737mchafs6kqw")))

(define-public crate-an-api-0.1 (crate (name "an-api") (vers "0.1.0") (hash "0c2zb78a86bghcglw1zbd5gyq751wd3f2bhh1nxa6yzj3rzjv4g5")))

(define-public crate-an-audio-0.1 (crate (name "an-audio") (vers "0.1.0") (hash "1gq13xsvxadwbm94n7scm6fh8rnaxgg82l5lhyww293rycjf68w1")))

