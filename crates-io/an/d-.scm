(define-module (crates-io an d-) #:use-module (crates-io))

(define-public crate-and-then-concurrent-0.1 (crate (name "and-then-concurrent") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.3") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "01l1c5g7fscj2pammzz82d5c6cyddp404lr8gmlfr2layrwczk4g")))

(define-public crate-and-then2-0.1 (crate (name "and-then2") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vx9yg259qs90s515kpixs0vxgd1d2iasdynx76chy4v7mf9bg67")))

(define-public crate-and-then2-0.1 (crate (name "and-then2") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kh664sy6wxbkk6mgdjdc4baa817401h974qs80k41jhsbav05iw")))

