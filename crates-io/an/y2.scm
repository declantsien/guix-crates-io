(define-module (crates-io an y2) #:use-module (crates-io))

(define-public crate-any2utf8-0.1 (crate (name "any2utf8") (vers "0.1.0") (deps (list (crate-dep (name "chardetng") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.33") (default-features #t) (kind 0)))) (hash "1smjm4g5s8xrvflsb952ad2h17dnvz2rbwjnni9mmb93bxlxdha0")))

(define-public crate-any2utf8-0.1 (crate (name "any2utf8") (vers "0.1.1") (deps (list (crate-dep (name "chardetng") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.33") (default-features #t) (kind 0)))) (hash "0cp6vmnpvdrvw369wlvnmw3z8y0p0vfcxrdc733iqan8hd5riqfn")))

