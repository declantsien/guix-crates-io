(define-module (crates-io an cs) #:use-module (crates-io))

(define-public crate-ancs-0.1 (crate (name "ancs") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "077r80mp9c6hi8sn147kzg0mlqamzi38zw1qmfwp8wlnh8rpwdx8")))

(define-public crate-ancs-0.2 (crate (name "ancs") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "17mp66kk71l1r66hi50h176d26qyk8si5lwc3gy5ysa14lc5rh2d")))

