(define-module (crates-io an gs) #:use-module (crates-io))

(define-public crate-angsd-io-core-0.1 (crate (name "angsd-io-core") (vers "0.1.0") (hash "1nai9vvmr735q1779xvihviicbbv1901wkc0yas9ff77fwf4vpjq")))

(define-public crate-angsd-saf-0.1 (crate (name "angsd-saf") (vers "0.1.0") (deps (list (crate-dep (name "angsd-io-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bgzf") (req "^0.17") (default-features #t) (kind 0) (package "noodles-bgzf")) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8") (default-features #t) (kind 0)))) (hash "12ybsgi8fvprr0khdfr2mqb8jxv5brslm1583dd456pznlih3492") (features (quote (("libdeflate" "bgzf/libdeflater") ("default"))))))

