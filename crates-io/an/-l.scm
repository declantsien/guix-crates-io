(define-module (crates-io an -l) #:use-module (crates-io))

(define-public crate-an-linux-0.1 (crate (name "an-linux") (vers "0.1.0") (hash "0af5gj0nqch6dipcjjanfa0ndgx6p13xcrrk1h3yn1v19380am1h")))

(define-public crate-an-log-0.1 (crate (name "an-log") (vers "0.1.0") (hash "0ji1vfx5igvfy0y4hmpyq81ms5nsr4vsa1dla7nw92b970jlxaci")))

