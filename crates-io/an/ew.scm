(define-module (crates-io an ew) #:use-module (crates-io))

(define-public crate-anew-0.1 (crate (name "anew") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "17p755xpg6fwh7475cp2pf3fdkb88paa5jm4nm2i2qj76kqxbknw")))

(define-public crate-anew-0.1 (crate (name "anew") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "03i5dbxkr93hs0sa2nc5v229rxp2w3wjj2d6f2mbnk0p758qglj9")))

(define-public crate-anewer-0.1 (crate (name "anewer") (vers "0.1.2") (deps (list (crate-dep (name "ahash") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0snl8k3xp872hdzq7s7pdr4pqgcrpqys0lr3bvfyvh6f736m1h68")))

(define-public crate-anewer-0.1 (crate (name "anewer") (vers "0.1.4") (deps (list (crate-dep (name "ahash") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "10nzqz15kjy367im8lsmz7y81arm47q9s2r0cp2c611cdm8dlrqi") (yanked #t)))

(define-public crate-anewer-0.1 (crate (name "anewer") (vers "0.1.5") (deps (list (crate-dep (name "ahash") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1h9xmqamrhrmf48yq8a6k8wq44y9zmqlasc1ndx17jfyd5hrwj5i")))

(define-public crate-anewer-0.1 (crate (name "anewer") (vers "0.1.6") (deps (list (crate-dep (name "ahash") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "15ci4mhzdcbng9iq2v6q4qaf31fkcw5vapis24jkfafilifj5ml0")))

