(define-module (crates-io an in) #:use-module (crates-io))

(define-public crate-aninamer-0.1 (crate (name "aninamer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0lrr5f16gvak2wwy6j6pyy8rk8snqj67jsis32bjjn5myzsbj3x5")))

(define-public crate-aninamer-0.1 (crate (name "aninamer") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "1l5wlyv0xk7y8js24ac2mqw52pby61pbsrm12pjbhapzcd02qp21")))

(define-public crate-aninamer-1 (crate (name "aninamer") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "185b2blnv0myaafzq54n5cjwlrh6x3j7hpsasalbxqq9g0qrk2qi")))

(define-public crate-aninmals-0.1 (crate (name "aninmals") (vers "0.1.0") (hash "0gw89v070vwv1i5lvixqs2m944ykqppdjir05gb82crf0vz71j7x")))

(define-public crate-aninmals-0.1 (crate (name "aninmals") (vers "0.1.1") (hash "1h6m4imw742agim5pqb8i8ffy2qw7akcinhnq40d18j9xbx7rj98")))

(define-public crate-aninmals-0.1 (crate (name "aninmals") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "11zw61i6vg64gdk4dbh6jzsfxkmwpi7521az7p02v8jsg9kwzzf2")))

(define-public crate-aninmals-0.1 (crate (name "aninmals") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0jkxhd2sqfhylc6d49qax6zh24njp7lwbgn53w5y6f6jw4b9irzw")))

