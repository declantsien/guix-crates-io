(define-module (crates-io an yr) #:use-module (crates-io))

(define-public crate-anyrange-0.1 (crate (name "anyrange") (vers "0.1.0") (hash "0rnyh7ckqwxhx9qwkx7fnnd39g6rh6p76vbg76wzj1hflw1rvmcp")))

(define-public crate-anyrun-interface-0.1 (crate (name "anyrun-interface") (vers "0.1.0") (deps (list (crate-dep (name "abi_stable") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "11ji8k6s2bsnm6d9rzyiwqvfnisl478lfb07k1fxh3j4dzgpb2vy")))

(define-public crate-anyrun-macros-0.1 (crate (name "anyrun-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05kc3vp4a41gwlkymznid24f61i12dhzq6jx3w20fr09zichm4bc")))

(define-public crate-anyrun-plugin-0.1 (crate (name "anyrun-plugin") (vers "0.1.0") (deps (list (crate-dep (name "abi_stable") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "anyrun-interface") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "anyrun-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14jpddqlxm5nc511gr95969l5sxjiy7pcgbk9msnklkh6f4psb41")))

(define-public crate-anyrust-0.0.0 (crate (name "anyrust") (vers "0.0.0") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0mb3g5612qxi1xarabl9l888cy7r4bg95ilhs9ncf8wbhas4c574")))

(define-public crate-anyrust-0.1 (crate (name "anyrust") (vers "0.1.0") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0ss3xfijk8amz8m5z650i0y7r8xhjyhkzxg9vwcfmy92zc36fbv2")))

(define-public crate-anyrust-0.2 (crate (name "anyrust") (vers "0.2.0") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1pnskfq4c2aph8i7zkakydv2gr2vwkn4z0shsga3gzy19jpmag02")))

(define-public crate-anyrust-0.2 (crate (name "anyrust") (vers "0.2.1") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0pplk98zjl8hvny2q4b0vsrmrfsqvkf522rw8d3sjpy8dlhp98sc")))

(define-public crate-anyrust-0.2 (crate (name "anyrust") (vers "0.2.2") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "19dny5d36y40mwqdpiz1z4fd79gslkkj761m75shcq05ia8nnl32")))

(define-public crate-anyrust-0.2 (crate (name "anyrust") (vers "0.2.3") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0fjvl4j7bq1r2d7hms79zwwl9cjhsrz15cg41ldjal9xqmc5ha66")))

