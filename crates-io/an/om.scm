(define-module (crates-io an om) #:use-module (crates-io))

(define-public crate-anoma-0.1 (crate (name "anoma") (vers "0.1.0") (hash "1ismkszqmi72ck04l3xg6m0l3b6ghpl0bghm2djyyrdrqg12qbms")))

(define-public crate-anomalocaris-0.1 (crate (name "anomalocaris") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0qdckvfsm11xj2zfklmybkps1krck5vyfja9ij19srff9p97zzdg")))

(define-public crate-anomalocaris-0.1 (crate (name "anomalocaris") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1rxm797aa49vy32qfvb9y7zbw6kanga1w9g9zy0419f6lb2rq6nl")))

(define-public crate-anomaly-0.0.0 (crate (name "anomaly") (vers "0.0.0") (hash "0b76yqgy1ac8n7qp1qsjd73l3nddqdz3nfy6csgr4sbxdl05lr2d") (yanked #t)))

(define-public crate-anomaly-0.1 (crate (name "anomaly") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "085ks103f4v9lh46j9ff9qz611viyqppnxb4prkidgzdcv13cjiw") (features (quote (("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace")))) (yanked #t)))

(define-public crate-anomaly-0.1 (crate (name "anomaly") (vers "0.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rpr01sfig1irnwarcznl0sk199in4s6vqr51r50053br6si9xgp") (features (quote (("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace")))) (yanked #t)))

(define-public crate-anomaly-0.1 (crate (name "anomaly") (vers "0.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1rknbd9i94179lvsc9agncxixrl2jw5sik58dcb6qb69hxhrwzrw") (features (quote (("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace")))) (yanked #t)))

(define-public crate-anomaly-0.2 (crate (name "anomaly") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 2)))) (hash "1jswpi7zcz1kr89ikjcykg20y0v3hnj3m34r8xgimbk82pik41jm") (features (quote (("serializer" "backtrace/serde" "serde") ("gimli-symbolize" "backtrace/gimli-symbolize") ("default" "backtrace"))))))

(define-public crate-anomaly-0.99 (crate (name "anomaly") (vers "0.99.0") (hash "03sk4fddsz33my3b6wlspk8pmhcxmhgnmbh39is9z214xydw7cwr")))

(define-public crate-anomaly_detection-0.1 (crate (name "anomaly_detection") (vers "0.1.0") (deps (list (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.1") (default-features #t) (kind 0)))) (hash "11rh859c23qy0wv0jfnlavp50l58hmf3bg7f0xjjsk34pa249xrn")))

(define-public crate-anomaly_detection-0.1 (crate (name "anomaly_detection") (vers "0.1.1") (deps (list (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.1") (default-features #t) (kind 0)))) (hash "05rcvbjgp38k9bdbv7w0dffzpd68ilfg7jz7sv0l3j35wgdnds3c")))

(define-public crate-anomaly_detection-0.1 (crate (name "anomaly_detection") (vers "0.1.2") (deps (list (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.1") (default-features #t) (kind 0)))) (hash "17zlani6ab9xwf5128dqsczcknl73bxrl5lb15hccdh3m883qpy2")))

(define-public crate-anomaly_detection-0.2 (crate (name "anomaly_detection") (vers "0.2.0") (deps (list (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.2") (default-features #t) (kind 0)))) (hash "14l3j2rnvppz20x3945pmshyk5ls0bvvnj2h6pdai7fwyfcwp1wc")))

(define-public crate-anomaly_detection-0.2 (crate (name "anomaly_detection") (vers "0.2.1") (deps (list (crate-dep (name "statrs") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.2") (default-features #t) (kind 0)))) (hash "077ps91il2c1awsywskzr4gxpqcdr3hv3xb9dr1808hv4jpm6p1k")))

(define-public crate-anomaly_detection-0.2 (crate (name "anomaly_detection") (vers "0.2.2") (deps (list (crate-dep (name "distrs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.2") (default-features #t) (kind 0)))) (hash "16h58fr1z3255n2yghi2n3br47f6z44b5nbm878j54wkx5z0n421")))

(define-public crate-anomaly_detection-0.2 (crate (name "anomaly_detection") (vers "0.2.3") (deps (list (crate-dep (name "distrs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ap2aim17qji8wccgd65x60nzflp5qlcprq4dh1y876nkn0954qp")))

(define-public crate-anomaly_detection-0.3 (crate (name "anomaly_detection") (vers "0.3.0") (deps (list (crate-dep (name "distrs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stlrs") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bxbw1ddvril7bz2klwq1c3spyz79d13yfiw1wzjwlpvw07b7zjj") (rust-version "1.56.0")))

