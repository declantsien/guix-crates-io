(define-module (crates-io an ac) #:use-module (crates-io))

(define-public crate-anachro-client-0.1 (crate (name "anachro-client") (vers "0.1.0") (deps (list (crate-dep (name "anachro-icd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (kind 0)))) (hash "0kbgqkv4jizvpqx4is6mdjqdkay8dpp7jv7avsaxn48ib0vn2ygq")))

(define-public crate-anachro-forth-core-0.0.1 (crate (name "anachro-forth-core") (vers "0.0.1") (deps (list (crate-dep (name "heapless") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.7.2") (features (quote ("use-std"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive" "std"))) (kind 2)))) (hash "0piicdhjwy5zjllp1jn2i280416n97vmmm0nsdaski7zrw51yqjg") (features (quote (("std" "serde/std") ("default"))))))

(define-public crate-anachro-forth-core-0.0.2 (crate (name "anachro-forth-core") (vers "0.0.2") (deps (list (crate-dep (name "heapless") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.7.2") (features (quote ("use-std"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive" "std"))) (kind 2)))) (hash "0x5byy8g4f6932sm50iazhm2zwijr2cj6p1zxmzy10m08g7plajc") (features (quote (("std" "serde/std") ("default"))))))

(define-public crate-anachro-icd-0.1 (crate (name "anachro-icd") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.5.1") (kind 2)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (kind 0)))) (hash "1xj86prnkiaai2d62pplzwaj555llj0yh1j55pbdykgzm492p0h6") (features (quote (("std" "postcard/use-std"))))))

(define-public crate-anachro-icd-0.1 (crate (name "anachro-icd") (vers "0.1.1") (deps (list (crate-dep (name "heapless") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.5.1") (kind 2)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (kind 0)))) (hash "162pdy37vgx5kddrc7x5kpl7bi1qsj08f1ibmsrvv5nd2c83cc5a") (features (quote (("std" "postcard/use-std"))))))

(define-public crate-anachro-icd-0.1 (crate (name "anachro-icd") (vers "0.1.2") (deps (list (crate-dep (name "heapless") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.5.1") (kind 2)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (kind 0)))) (hash "1dgwfqhp408mards9g003y2fyx57k1vp83jgsr35m0r1dic0a7ch") (features (quote (("std" "postcard/use-std"))))))

(define-public crate-anachro-icd-0.1 (crate (name "anachro-icd") (vers "0.1.3") (deps (list (crate-dep (name "heapless") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.5.1") (kind 2)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (kind 0)))) (hash "0jdd4jav1bb9s10pfrmnyzhjqryz3zkcn3vxx02wl0zafhnq6asy") (features (quote (("std" "postcard/use-std"))))))

(define-public crate-anachro-server-0.1 (crate (name "anachro-server") (vers "0.1.0") (deps (list (crate-dep (name "anachro-icd") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "08hzns1pdg37nv65lizllksv1nk58sspp3b6qkk7c0d2337kzfvc")))

(define-public crate-anaconda-0.0.0 (crate (name "anaconda") (vers "0.0.0") (hash "14mjqm0vdpl90abpl92yj949mczl9l2l4a3p21zcvhiq6zlmvyyx") (yanked #t)))

(define-public crate-anacrolix-libc-0.2 (crate (name "anacrolix-libc") (vers "0.2.151") (deps (list (crate-dep (name "rustc-std-workspace-core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0svpn39kgw7ar450ba3yd9mvkc8ll0vxwd351s7z7v171g1a7yid") (features (quote (("std") ("rustc-dep-of-std" "align" "rustc-std-workspace-core") ("extra_traits") ("default" "std") ("const-extern-fn") ("align")))) (rust-version "1.71.0")))

