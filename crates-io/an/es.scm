(define-module (crates-io an es) #:use-module (crates-io))

(define-public crate-anes-0.0.1 (crate (name "anes") (vers "0.0.1") (hash "19i020cavnf5m4m2xx1mrkpms2s3mz63gig6q5b89k28ayzg83vy")))

(define-public crate-anes-0.0.2 (crate (name "anes") (vers "0.0.2") (hash "1ixpnzfxigb4pa878sfvvl1qkiysl8z9pcdncwii6a05i0wwj1xb")))

(define-public crate-anes-0.1 (crate (name "anes") (vers "0.1.0") (hash "0say24qq3ghnj8akb0i8hix8bl85nzqm4f9m4f68cm2h9nf48sax")))

(define-public crate-anes-0.1 (crate (name "anes") (vers "0.1.1") (hash "16lwwj7jvjnxf4i2ll5609hg9957q3ay5q5s6zckjcjr9clj70sp")))

(define-public crate-anes-0.1 (crate (name "anes") (vers "0.1.2") (hash "0fx0cwa71gi8p0hcziq085sp6jcd5kka5zxv9mx0mpr27jr4ysmz")))

(define-public crate-anes-0.1 (crate (name "anes") (vers "0.1.3") (hash "1s663xpifzs4001brsw957q4hp6c2n19fwn409klv561krddmvid")))

(define-public crate-anes-0.1 (crate (name "anes") (vers "0.1.4") (hash "1r0l8dzjkflfaxikflb15kzvyyrw44kynfsn0sgqakdj7hk4pq75")))

(define-public crate-anes-0.1 (crate (name "anes") (vers "0.1.5") (hash "1ad5v1fna725ppavc0c2b0vrpkvjq37vcl81bg0vw8sdyzdbf3dn")))

(define-public crate-anes-0.1 (crate (name "anes") (vers "0.1.6") (deps (list (crate-dep (name "bitflags") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 2)))) (hash "16bj1ww1xkwzbckk32j2pnbn5vk6wgsl3q4p3j9551xbcarwnijb") (features (quote (("parser" "bitflags") ("default"))))))

(define-public crate-anes-0.2 (crate (name "anes") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 2)))) (hash "111yf4wav38if2p48bmjbfzg76iv3kw2np12h0lglz55ihwlypbk") (features (quote (("parser" "bitflags") ("default"))))))

