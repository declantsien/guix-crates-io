(define-module (crates-io an ar) #:use-module (crates-io))

(define-public crate-anar-0.1 (crate (name "anar") (vers "0.1.0") (hash "1bib2jvzrc8sc8j1hp4s2hg1h7yq1gydl50cmslvr66621xas0mn") (yanked #t)))

(define-public crate-anarchist-readable-name-generator-lib-0.1 (crate (name "anarchist-readable-name-generator-lib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1skcj75mj353lifmkflpfbs4g5zqfffxfsf9igl39kal302053wp")))

(define-public crate-anarchist-readable-name-generator-lib-0.1 (crate (name "anarchist-readable-name-generator-lib") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "16wf6v94qsf678ki9jh5l1npxbgf3gdm7m0cn8z4y8367i23sv24")))

