(define-module (crates-io an to) #:use-module (crates-io))

(define-public crate-antonio_art-0.1 (crate (name "antonio_art") (vers "0.1.0") (hash "1zmkc68i3b895a20wf6in4wzg3b46m50iw92z5ldff4s3npljqb9")))

(define-public crate-AntonsRus-0.1 (crate (name "AntonsRus") (vers "0.1.0") (hash "1pzjp7g87qrj3cy948d2hlhh2zv1amgqgjihldf2kmjmjc7aba4p") (yanked #t)))

(define-public crate-AntonsRust-0.1 (crate (name "AntonsRust") (vers "0.1.1") (hash "1nhk9rhv357fhjjk110jb7cwqrkpq2ib6c6d9cwqxkv2g77cw0fp") (yanked #t)))

