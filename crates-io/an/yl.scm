(define-module (crates-io an yl) #:use-module (crates-io))

(define-public crate-anyleaf-0.1 (crate (name "anyleaf") (vers "0.1.0") (deps (list (crate-dep (name "ads1x1x") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1pqshpvkrmki9f07irwffq12gh5g5apm3fvygpph0i4b4108sv3b")))

(define-public crate-anyleaf-0.1 (crate (name "anyleaf") (vers "0.1.1") (deps (list (crate-dep (name "ads1x1x") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1gqhfkyn611qddp3vcv80rah728dhj6whxdgbww3snm2cr6n96i0")))

(define-public crate-anyleaf-0.1 (crate (name "anyleaf") (vers "0.1.3") (deps (list (crate-dep (name "ads1x1x") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1cfq9cx5xf4jyiwwfhxkca2rgp5d2dpnpjz4d5wb4z8mp97pz664")))

(define-public crate-anyleaf-0.1 (crate (name "anyleaf") (vers "0.1.4") (deps (list (crate-dep (name "ads1x1x") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "02bilrfbr8b33y58yv7kdkv2mm8f0lhl11haddm983l1s1p5b12v")))

(define-public crate-anyleaf-0.1 (crate (name "anyleaf") (vers "0.1.5") (deps (list (crate-dep (name "ads1x1x") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "filter") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18.0") (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0w59is0c5lg6mnqklggpcj44xp91ixbmcc5r8yikcxhzraqgdng9")))

(define-public crate-anyleaf-0.1 (crate (name "anyleaf") (vers "0.1.6") (deps (list (crate-dep (name "ads1x1x") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "filter") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21.0") (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (features (quote ("libm"))) (kind 0)))) (hash "1drzz34qis9nj2xwj71gii61avh8v9qjk6ziji7v9wl14vw7nn71")))

(define-public crate-anyleaf-1 (crate (name "anyleaf") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1qpy14a17nkczccz5c5kcv4f1wfw808fypkk2dgbkac7y6987lz9")))

(define-public crate-anyleaf-1 (crate (name "anyleaf") (vers "1.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0nhy7jpx8s77l57p0b2zp2b6kbvkzjy98qinlds6l0z6a8dd1yfb")))

(define-public crate-anylist-0.1 (crate (name "anylist") (vers "0.1.0") (hash "0qiq19v9aivcbbkyck6d0zq4cnxvcj9k28xjrrxwks377b88whyk") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.2 (crate (name "anylist") (vers "0.2.0") (hash "0sq9lsjp85w5vka33djjdnr7ssr50qa1kb9cbcd5jhwhg3b7pshm") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.2 (crate (name "anylist") (vers "0.2.1") (hash "02w8r5i047c3xpkmzsm97i0zmqla7ysw6w1yvsnrkacxns1vr3ln") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.2 (crate (name "anylist") (vers "0.2.2") (hash "0alab4vc8qkwlk3338byx4n9m7p3yrk2ayji08cp3k0hb623g82m") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.2 (crate (name "anylist") (vers "0.2.3") (hash "0qnzqqfzf89km9a8fmawsd6hakkl5m0lyqxsr5frvyxh9hrzwajh") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.3 (crate (name "anylist") (vers "0.3.3") (hash "1xqsbbk4jdymml354hjyyk5w8difxf5sf13l3hydp983k0fm1abv") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.4 (crate (name "anylist") (vers "0.4.3") (hash "08r35nfgh21wr8m70lpp45843mls6sqsvnjq5ji9is5kczjq3icl") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.5 (crate (name "anylist") (vers "0.5.3") (hash "0mg8cpicki169xlvjs6j2kd82hlkhj4yaybz7aqi9jxhjv0mr897") (yanked #t) (rust-version "1.56")))

(define-public crate-anylist-0.5 (crate (name "anylist") (vers "0.5.4") (hash "10xyxz14w3qp7mb4ac7qg60g8ap8d5dlqs8zd3n0vzvzi05mzc8d") (rust-version "1.56")))

(define-public crate-anylist-0.6 (crate (name "anylist") (vers "0.6.4") (hash "0a8l18r13qawf7zwbzbm06ml86z8lx1kasivfbhpcajm6b1p22jr") (rust-version "1.56")))

(define-public crate-anylog-0.1 (crate (name "anylog") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "101zh7hhms36r9ag7aif59vi2ihx5iivdm517b47n4j0smy63796")))

(define-public crate-anylog-0.2 (crate (name "anylog") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "12xjy3dfwq8qy7s3sh4szzqckxgjqcyxhbsww5x2bf9v3i675b3f")))

(define-public crate-anylog-0.2 (crate (name "anylog") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1mfkjfdg8nc96y2rrwdkkh3nd9hky2rh16c864y8a2cz23wdrr1r")))

(define-public crate-anylog-0.3 (crate (name "anylog") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0giwk9ljxsp4icknq6c1wjvdsz15611gibxz0ji5r1q1jdmff37v")))

(define-public crate-anylog-0.3 (crate (name "anylog") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1waarkd98fk2xpp6q7d3hb1rzvgrjd37xjsmjyg5gg1r8b36hmbf")))

(define-public crate-anylog-0.4 (crate (name "anylog") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "196h08lpl7bxp1y0p0m1v6s5c1v3099k0a4nfdmm5j3vywfpbkrg")))

(define-public crate-anylog-0.5 (crate (name "anylog") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (features (quote ("std"))) (kind 0)))) (hash "0vmqzww48mh5xzfppqf4drmjj0xdfrws02w5crqfhx3z1ml7fdkv")))

(define-public crate-anylog-0.6 (crate (name "anylog") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (features (quote ("std"))) (kind 0)))) (hash "0pxybj2ynhsslsgh3j20s6w99sz9h6gip7rwz3xfpblpsisgs225")))

(define-public crate-anylog-0.6 (crate (name "anylog") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.12.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (features (quote ("std"))) (kind 0)))) (hash "0c279var6hvins8waifap9mvcs74y5dr4nv14nfvp47nzngdmhbb")))

(define-public crate-anylog-0.6 (crate (name "anylog") (vers "0.6.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.21.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (features (quote ("std"))) (kind 0)))) (hash "02sh89cnj3zbjgbdfd8yi939ik6rymhjfi4vihry294g0nwgmlbv")))

(define-public crate-anylog-0.6 (crate (name "anylog") (vers "0.6.3") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.21.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (features (quote ("std"))) (kind 0)))) (hash "1w8j5vp7r09c4cdmyrmgw9p7mi4jkpb3b4fkjxc2d5v6gcqcc5ls")))

(define-public crate-anylog-0.6 (crate (name "anylog") (vers "0.6.4") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (features (quote ("clock" "std"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "insta") (req "^1.21.0") (default-features #t) (kind 2)))) (hash "0pr2zkl4cq4gwf94iy0dk0vwyif81bi0lqbbi1p2pjk75mxrlbhy")))

