(define-module (crates-io an -c) #:use-module (crates-io))

(define-public crate-an-cache-0.1 (crate (name "an-cache") (vers "0.1.0") (hash "10iwgrh8n059cma59kbkb4fzyih1k9hlc9n06c6473d518nh67l7")))

(define-public crate-an-cli-0.1 (crate (name "an-cli") (vers "0.1.0") (hash "1n52da4grq6bz0f7sxs9z8mx6vkfdaqgval0snmsq70y8ad9g709")))

(define-public crate-an-cms-0.1 (crate (name "an-cms") (vers "0.1.0") (hash "103lddyak49ianmrrm2l7cnr16jkp0329g7mdhrdzincd5q3ymgc")))

(define-public crate-an-config-0.1 (crate (name "an-config") (vers "0.1.0") (hash "1xwmlismd9k5pgr77742hzn3rjnfw3x5rwcp7p4mp85dgyf1jmzj")))

(define-public crate-an-core-0.1 (crate (name "an-core") (vers "0.1.0") (hash "1kfhlamcrxrr0v9var8fld4cqp48l04dxr1v2h9gap2xz7c8jm51")))

(define-public crate-an-crypt-0.1 (crate (name "an-crypt") (vers "0.1.0") (hash "1sir2569njysxc1afm9pryf0wkg9l63av1hmfql6qm15b0ghdanm")))

(define-public crate-an-cv-0.1 (crate (name "an-cv") (vers "0.1.0") (hash "1hj8661lz14dhyfhxc8nkcffbak7xpqngm4d5jr3qgly2x0135dz")))

