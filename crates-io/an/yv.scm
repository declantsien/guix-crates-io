(define-module (crates-io an yv) #:use-module (crates-io))

(define-public crate-anyvec-0.1 (crate (name "anyvec") (vers "0.1.0") (hash "0hg4d3dk1j0z9n7mlsx9cvm4wkz2sgc4870jcl5lbl7fvvip4ch0") (yanked #t)))

(define-public crate-anyvec-0.2 (crate (name "anyvec") (vers "0.2.0") (hash "1cdahzpr4mzh70j7bsis6zrwj0wjwbbdw9z3qh3inybwyrdpb5bh") (yanked #t)))

(define-public crate-anyvec-0.2 (crate (name "anyvec") (vers "0.2.1") (hash "174zrp913zg6391zj3fxafdqh3hgkv95g26s9mqxr7lhlv3aqzlr") (yanked #t)))

(define-public crate-anyvec-0.2 (crate (name "anyvec") (vers "0.2.2") (hash "1dg3kp1k4gn0x1xvymzyxl95kcikws70zfxsbsfs3yfvrzlnlfwm") (yanked #t)))

(define-public crate-anyvec-0.2 (crate (name "anyvec") (vers "0.2.3") (hash "0p3qm3xs8qvc2lqdi47icmhm6yyyq9r6pzabag0kjigm5dzpgpaj") (yanked #t)))

(define-public crate-anyversion-0.1 (crate (name "anyversion") (vers "0.1.0") (hash "1fvcd17cp60g3xskfwi0mcddjkvy75ybxfs9zva2yhw9azb0ns9p")))

(define-public crate-anyversion-0.2 (crate (name "anyversion") (vers "0.2.0") (hash "1n3ij001chvjpknv2kswng06sf5qva50lkm8164g8qbs4fbrwzbz")))

