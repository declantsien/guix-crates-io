(define-module (crates-io an co) #:use-module (crates-io))

(define-public crate-ancora-0.1 (crate (name "ancora") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1pfvkwrz4g22bk8g9g3wvz2fg8iwk77rrrmb24qvwagzaibs0fxy") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

