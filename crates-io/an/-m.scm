(define-module (crates-io an -m) #:use-module (crates-io))

(define-public crate-an-mac-0.1 (crate (name "an-mac") (vers "0.1.0") (hash "06awkxwlh98rfd3jczsrck3325yl67s7hpzb1nrrk0khi020ij35")))

(define-public crate-an-mq-0.1 (crate (name "an-mq") (vers "0.1.0") (hash "0dn949gh87aab9hgd9ss1msi30xjrk3szjghfblbx4nqkbh091yn")))

