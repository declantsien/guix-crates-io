(define-module (crates-io an ze) #:use-module (crates-io))

(define-public crate-anzen-1 (crate (name "anzen") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wkq7wb4pcgmb2vs75ch3awy0yx5g46xwznq88h7j8jdbvd77yca")))

