(define-module (crates-io an u-) #:use-module (crates-io))

(define-public crate-anu-macros-0.1 (crate (name "anu-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m2z4vhwzpddwi2255q85ld1p87q3b79b4s014lgcfpymfsbsdxh")))

