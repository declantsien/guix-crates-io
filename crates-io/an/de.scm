(define-module (crates-io an de) #:use-module (crates-io))

(define-public crate-andersonpgrep-0.1 (crate (name "andersonpgrep") (vers "0.1.0") (hash "0k61943a7klspvgnbrm4jl6nxxw836hl7ncq2lgg30vyadx7galj")))

(define-public crate-andes-0.1 (crate (name "andes") (vers "0.1.0") (deps (list (crate-dep (name "iced") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "02jiaq5fj3v7vp4f4qx677h57z5dmp8r28r35hl81jm2rgcbaf41") (yanked #t)))

(define-public crate-andex-0.0.3 (crate (name "andex") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0q6xhrbc98rz4982jcvc3g5i0r29kzv01dkdks0664whdrj7grsh")))

(define-public crate-andex-0.0.4 (crate (name "andex") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "16a6vc6r8lzmd7k16njwggcipq4c1ms1njndqv2y4ddiawbbvfdd")))

(define-public crate-andex-0.0.5 (crate (name "andex") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "094ai65i3mvbcncircff160yppijcysjcsj2idvvavw9jhcdc3xs")))

(define-public crate-andex-0.0.6 (crate (name "andex") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "0bl4ahay0868bqynw6xgxwv6wsv1hrqxgc0bxnm24lm81wsm3hmv")))

(define-public crate-andex-0.0.7 (crate (name "andex") (vers "0.0.7") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1fzvfvhg95s98r41dpijkpzqi4xw0qmarz4vikrzxmz49sbx3nnn")))

(define-public crate-andex-0.0.8 (crate (name "andex") (vers "0.0.8") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1dy7jzm2nbhm3xfzi4hd070lk9zakp6vmkd6c1hs841la04b51j4")))

(define-public crate-andex-0.0.9 (crate (name "andex") (vers "0.0.9") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1fq685akqrnd3x5lnvyyr9gnv9ssqslpmsp13mw185nyx1kpd554")))

(define-public crate-andex-0.0.10 (crate (name "andex") (vers "0.0.10") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "155rixq09ywzb4zy5xxrx7lyp0cp456319hmpm326xq4yd5f6fqq")))

(define-public crate-andex-0.0.11 (crate (name "andex") (vers "0.0.11") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "053y2vqdkqzr6xhddzcwkwl7vdpgh8xv387969hmgfhzxfvcyj8g")))

(define-public crate-andex-0.0.12 (crate (name "andex") (vers "0.0.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "1fr4v9wgrnpx1276sgkgaiwjm2524bnv4px3qvc1skd2bk6mpxy5")))

(define-public crate-andex-0.0.13 (crate (name "andex") (vers "0.0.13") (deps (list (crate-dep (name "anyhow") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "1hm9skdaq0jdz0jd30gw286xsy3klr707k1ijdlxsclvdmzgmwpg")))

(define-public crate-andex-0.0.14 (crate (name "andex") (vers "0.0.14") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 2)))) (hash "1yhkg8jcrrqza8wpzw7qyfiqif6ynvmykplgn6l7xkaznl92yjpy")))

