(define-module (crates-io an yw) #:use-module (crates-io))

(define-public crate-anyway-0.1 (crate (name "anyway") (vers "0.1.0") (hash "07pyj053429s9dlnwkwy09im2h75snq5q5pbdll33h1z2g00740s")))

(define-public crate-anyways-0.1 (crate (name "anyways") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "10jhfyp6jk2jmcl8jacnsdr2zkw9gs71q4bazidmd4bf5150illl")))

(define-public crate-anyways-0.1 (crate (name "anyways") (vers "0.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "0jj3lf1n3bfi2lki6jnlplhlsna7ac5lnn19xwfpzivcf4bk1v3h")))

(define-public crate-anyways-0.1 (crate (name "anyways") (vers "0.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "14qik7fzkkl1lngga18n257rnmj836w5dm6sv8k9pnlaqdpqr4lj")))

(define-public crate-anyways-0.1 (crate (name "anyways") (vers "0.1.3") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1hgwvgp8fc1xhx7q0iqmm4q0x35fhh00r06pkz0arpp03bh6f263")))

(define-public crate-anyways-0.1 (crate (name "anyways") (vers "0.1.4") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1sh9dishz6ymsb23rja1hp5w0ajg6rj346nyskfgp126vijn8i73")))

(define-public crate-anyways-0.2 (crate (name "anyways") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1pwddmabbdk7pfpirl3y6f2lvidj6gi3kcy8f6qbcn87pg1abj9y")))

(define-public crate-anyways-0.3 (crate (name "anyways") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.65") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1ik4q4hg0zfskzrg054118whrwhaara9fcs5z0415rqaqf3mb9sl") (features (quote (("sync") ("send"))))))

(define-public crate-anywhere-0.0.0 (crate (name "anywhere") (vers "0.0.0") (hash "08yyw3wzygywhirdsjj570a18mp3yrrvf4bribmmmlizyrb9wqq5")))

(define-public crate-anywhere-0.0.1 (crate (name "anywhere") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.4.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "lunchbox") (req "^0.1") (features (quote ("serde"))) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "relative-path") (req "^1.7") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "08pa3p4khpc00nbayj8wicvr35jfm4kq24wdaf95qyx4ywawnyvs") (features (quote (("tcp" "tokio/net"))))))

(define-public crate-anywhere-rs-0.1 (crate (name "anywhere-rs") (vers "0.1.0") (hash "1qnicyjl0llay31nfsnai62qswmv58ncxgr2wqjc0lp5f5115mfy")))

