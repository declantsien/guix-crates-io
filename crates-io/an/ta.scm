(define-module (crates-io an ta) #:use-module (crates-io))

(define-public crate-antarc-0.1 (crate (name "antarc") (vers "0.1.0") (deps (list (crate-dep (name "antarc-protocol") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "089hdzmjckifg4r8w7lj3rfxkkncy9r9hax4bh0s9cb0mq3kga5r")))

(define-public crate-antarc-protocol-0.1 (crate (name "antarc-protocol") (vers "0.1.0") (deps (list (crate-dep (name "crc32fast") (req "^1.2") (features (quote ("std" "nightly"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hckvs4y2vwjw93k92mmbvy84ppnq5ki6y6wb94sv5j5kwsnscsk")))

(define-public crate-antares-0.1 (crate (name "antares") (vers "0.1.0") (hash "1wz9awh4p8zz48jyi25qljq201arsjzdilqpds5qbl352n088xnf")))

