(define-module (crates-io an gl) #:use-module (crates-io))

(define-public crate-angle-0.1 (crate (name "angle") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1lz0dyfz0x96656ar03ki9xm8gvb10k8za3v43cf93jrwibli8ic")))

(define-public crate-angle-0.1 (crate (name "angle") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0ggarsc1p8spcm9lh2wxz2i8439ymisr24ga0yhqf09hvlymw0lb")))

(define-public crate-angle-0.1 (crate (name "angle") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "0axazm91pi1i44ahsfy67i9a8dk35mv69cjspyq4qdfdm8vb08rs")))

(define-public crate-angle-0.1 (crate (name "angle") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "1kaizmcnswzwngnsfd8czdc6f76ckrw49pdi8aw7p4k1p8wbkhf6")))

(define-public crate-angle-0.1 (crate (name "angle") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "1fglq6b945ii9mghcf9rabvp5bnb0nnsk09rj118ddpb30kpmdcx")))

(define-public crate-angle-0.1 (crate (name "angle") (vers "0.1.55555") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "0cszi3zxfm2zb4f972amxb951yjkj55a7k2fk8h9wy16apq4nl73")))

(define-public crate-angle-0.2 (crate (name "angle") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "00jlk5f2fk49cwy005izkbdzsyf0qqgvj009880avbn4ashj19c8")))

(define-public crate-angle-0.3 (crate (name "angle") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h09hhlwr8h3kdwyix7x9gvjqgilf7x62b5r0iq5lhclgs0vcwkb")))

(define-public crate-angle-0.3 (crate (name "angle") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hz7k56nvjyhvp5vcsgwrmrimlbkr1f7rn4zmkg1fgb3bp4f7fjm")))

(define-public crate-angle-0.3 (crate (name "angle") (vers "0.3.2") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "026v7r4ibsvdkqnrh4nn6fzfgc3klz01mvgnh0wc0d67s9r6vnpn")))

(define-public crate-angle-0.3 (crate (name "angle") (vers "0.3.3") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qjzpvr6q5rwrdmsrbk6ppbp9dd6vsy5x8m2sghx84bkvjf2spnq")))

(define-public crate-angle-0.3 (crate (name "angle") (vers "0.3.4") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "08idvhy1mbj4pibp3dw5qkj57w2dbsd6d59rmnym0wmilcf605z8")))

(define-public crate-angle-0.3 (crate (name "angle") (vers "0.3.5") (deps (list (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m045jaq9d0bidj8880pakxqd76hnbqwqhspwnmipsh38syzw262")))

(define-public crate-angle-0.4 (crate (name "angle") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "10bqmh5r0zi1afzjvx3jscnz7wvgmw0n4zqi08ccwlm9hxrahaxs")))

(define-public crate-angle-0.5 (crate (name "angle") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "simba") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1ym5qjvnqqnqgwlcbci2wrsgxw0pgm9fy5g8a20wdj22n6v6by8b")))

(define-public crate-angle-sc-0.1 (crate (name "angle-sc") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1q7rkk95nn1g60csbnhighwis0x6yqwg7xj1b03z6r82h5g1l694")))

(define-public crate-angle-sc-0.1 (crate (name "angle-sc") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0b1wl61smq8w40x81bg4bsrkynywqlqw5bkrfgh6hga8vp8f74n9")))

(define-public crate-angle-sc-0.1 (crate (name "angle-sc") (vers "0.1.2") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "05xxd0q0gxbbfsb34xlcp05qy6wqx1rj9iajlrl5mkv6f0mr5nkm")))

(define-public crate-anglosaxon-0.1 (crate (name "anglosaxon") (vers "0.1.0-rc1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "08d9n5nz834ddzrddyd8s2s2adpzgb98001nnig0lfzbibwb5h4n")))

(define-public crate-anglosaxon-0.1 (crate (name "anglosaxon") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ps2lzflvixlwjfgih9496i791yvakwb3drzkdqxpqc5vy06qy5v")))

(define-public crate-anglosaxon-0.1 (crate (name "anglosaxon") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "14w7srcr5g4hmac942b0rq0g6xyjzyl6r198iavdqfdjx4bf95d0")))

