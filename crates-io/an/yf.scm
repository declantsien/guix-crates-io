(define-module (crates-io an yf) #:use-module (crates-io))

(define-public crate-anyflux-0.1 (crate (name "anyflux") (vers "0.1.0") (deps (list (crate-dep (name "anymap") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.11") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "15yrpxrn5cbi6dcx8y7vhclx64jvbkysbsmny9wjzlqh6p06qiqk") (features (quote (("future") ("default" "future"))))))

(define-public crate-anyfuck-0.1 (crate (name "anyfuck") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libbfi") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "1vvklcr98wjy0lahqm7fxh4xlaxq1m4xi3x2fggc450hz4kriicf")))

(define-public crate-anyfuck-0.1 (crate (name "anyfuck") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libbfi") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "0b7jgwkc6wh3zgdknyzdy8avck0k51ffzw8k6iak2bfy8xm9ir9v")))

