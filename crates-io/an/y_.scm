(define-module (crates-io an y_) #:use-module (crates-io))

(define-public crate-any_all_workaround-0.1 (crate (name "any_all_workaround") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "11ifmzawvvi6d4r1lk0dkdnbswf574npgkika4535k7j6l3s9zl8")))

(define-public crate-any_ascii-0.1 (crate (name "any_ascii") (vers "0.1.2") (hash "0p50nqkxm2c9sc6s7n1j87chn45dw1jcar0bwgp8sdsp2c40v9sq")))

(define-public crate-any_ascii-0.1 (crate (name "any_ascii") (vers "0.1.3") (hash "0ggy1clp3lmcqz4i4lry7sdaxcg3l9y2lify4al05c22i67mn45n")))

(define-public crate-any_ascii-0.1 (crate (name "any_ascii") (vers "0.1.4") (hash "0njpsjz7qfn9gzp4pxqmhlxjpsjb822hbxjdli9wmpflrrizngdz")))

(define-public crate-any_ascii-0.1 (crate (name "any_ascii") (vers "0.1.5") (hash "0f8zwwwvrqyg4djj9z9vzzfd071wkbb81i02j1p81sfzm84c83wh")))

(define-public crate-any_ascii-0.1 (crate (name "any_ascii") (vers "0.1.6") (hash "1ff2r5v6wibd6m6fs40knmb46lh9y3a0s2wrlz110j3hxjmwhpin")))

(define-public crate-any_ascii-0.1 (crate (name "any_ascii") (vers "0.1.7") (hash "07kyb9is518jr1sbc6804kgg5pnx8djl328q3al28lcbxdvkf0vh")))

(define-public crate-any_ascii-0.2 (crate (name "any_ascii") (vers "0.2.0") (hash "0w447dbd76vp37zqha3qbnpgr7310lj94v0zcdp4ibambwkv3ka9")))

(define-public crate-any_ascii-0.3 (crate (name "any_ascii") (vers "0.3.0") (hash "05fnnmiar25z42i0w6x8w6kasih64a4xw3yhxy4zac3lpv5ymx1g")))

(define-public crate-any_ascii-0.3 (crate (name "any_ascii") (vers "0.3.1") (hash "127njhkxkdjc9cwg6zcbp91nmj8y8ybk9h96pp8wrisr94ggdhmi")))

(define-public crate-any_ascii-0.3 (crate (name "any_ascii") (vers "0.3.2") (hash "0b6c2vb3wlzcsd7s5l6s9fyq4r07a8yab9r7qvw474sbg95v2l7a")))

(define-public crate-any_cmp-0.1 (crate (name "any_cmp") (vers "0.1.0") (hash "12ri8v4vpi8c75bsxpiwrnp9ki6b6d9201spm5ng7gblpyvwknvi")))

(define-public crate-any_cmp-0.1 (crate (name "any_cmp") (vers "0.1.1") (hash "1gcqah0lkiasyp2a2a4xs0lyj64y9ylh1f6jyxkmi5nhjbp1ivyk")))

(define-public crate-any_cmp-0.2 (crate (name "any_cmp") (vers "0.2.0") (hash "1v0kxhadm85gbp4gxx8x3fnm4sj7v4y9k0acbbblg0sb0bgrmcrs")))

(define-public crate-any_cmp-0.3 (crate (name "any_cmp") (vers "0.3.0") (hash "0clpkm6f1rjwwm1df3vf25h3f878lm09as8q2b3qn7j10b36ylz5")))

(define-public crate-any_cmp-0.4 (crate (name "any_cmp") (vers "0.4.0") (hash "0y1ygqy9jfvdnzab9wm4yxkdd4dnmaf1ihx9pbf8b37g07msq1rr")))

(define-public crate-any_cmp-0.4 (crate (name "any_cmp") (vers "0.4.1") (hash "13xxp9s9yzyjfhs9bgrrk0kn4bgiv35mrcnlxnn9mx941zqw0fb6")))

(define-public crate-any_handle-0.1 (crate (name "any_handle") (vers "0.1.0") (hash "0hhx92vid9v32kc00pqbhir91sm0vsc3yhk34blqhkks4pcnj248")))

(define-public crate-any_handle-0.1 (crate (name "any_handle") (vers "0.1.1") (hash "0li2b9f5dds0d0amv04dibsdjpyn6asf62x1m08xxk1d04mpnnki")))

(define-public crate-any_handle-0.1 (crate (name "any_handle") (vers "0.1.2") (hash "1kgq592w3rzid3q1x8wjfhm3jyl1x8p4n8bw6xc2b8yjsnij85mx")))

(define-public crate-any_handle-0.1 (crate (name "any_handle") (vers "0.1.3") (hash "1dsm9x4l4qxfvbs6m58ii8za1xv80nxd041q12p3mw653knixqjz")))

(define-public crate-any_handle-0.1 (crate (name "any_handle") (vers "0.1.4") (hash "0al4518sir3ilff1ydfvx2nmj81rafjnp9gfj5p3xbk3x2y5957d")))

(define-public crate-any_key-0.1 (crate (name "any_key") (vers "0.1.0") (deps (list (crate-dep (name "debugit") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "mopa") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "04xqpi505a25qpqvgv4fqdm6ndjb6cw5z1zii9w8q73mj34lv6f1")))

(define-public crate-any_key-0.1 (crate (name "any_key") (vers "0.1.1") (deps (list (crate-dep (name "debugit") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "mopa") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0lml7jd3kw09sss5bhj7z0fxxgldc2fdj7a1d6fyv1w0mg6v46yj")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.0") (hash "1ywbv81ccni2m0yc6h5vz4yka7s1yi0fzdw75n1l2plqd3i2g0hr") (rust-version "1.60")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.1") (hash "0lx3bs0sfjpgyl9s9ik9sxi7vwwdjs9fbfwqb9yr8372qpvr61jp") (rust-version "1.60")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.2") (hash "00dphdx32av5aga8qdwli9xjwv8v02l0bbfj79igb1qh9fa1kd5s") (rust-version "1.60")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.3") (hash "1ims818zlfr01skmh7hi6svv4ivr77am72qrkay6nx5cniadicvl") (rust-version "1.60")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.4") (hash "082gbbgdyby0jqznds49y3b7yphky9xpkrnj9hjw39z956prn4cr") (rust-version "1.60")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.5") (hash "1zznphv86nxfq8wv42h0mqg96ay0smdld777aj2isppr1s2vg4yj") (rust-version "1.60")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.6") (hash "0kkr8cglv51cw0w8bqnmmk4fs23sj3vwq2irckizf1yl8pym4hmc") (rust-version "1.60")))

(define-public crate-any_library-0.1 (crate (name "any_library") (vers "0.1.7") (hash "06j62hj36m0pax9wbfk50z8990sdxrz6q3a0s09cazmxw98ix6ga") (rust-version "1.60")))

(define-public crate-any_mail-0.1 (crate (name "any_mail") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "flume") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^5.0.0-beta.5") (features (quote ("rust-embed"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lettre") (req "^0.11") (features (quote ("smtp-transport" "pool" "builder"))) (optional #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pwvins0vp2sjjcy519872pwjh8i2w53bcnpyi1246i7xnbknc5c") (features (quote (("tokio_rustls" "tokio" "lettre/tokio1-rustls-tls") ("smtp" "lettre" "lettre/smtp-transport" "lettre/builder") ("reqwest_rustls" "reqwest/rustls-tls")))) (v 2) (features2 (quote (("mail-whale" "dep:reqwest" "dep:serde_json") ("mail-gun" "dep:reqwest" "dep:serde_json") ("default" "smtp" "mail-whale" "mail-gun" "dep:handlebars" "tokio_rustls" "reqwest_rustls"))))))

(define-public crate-any_ref-0.1 (crate (name "any_ref") (vers "0.1.0") (deps (list (crate-dep (name "any_ref_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0z0j4nkc9dgahf0bn8hr28k1dgm5qsvhlh8p7jhcwr5gc39dwm1m") (yanked #t)))

(define-public crate-any_ref-0.1 (crate (name "any_ref") (vers "0.1.1") (deps (list (crate-dep (name "any_ref_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0j7scz0wjxkwnbgsbfnrc6qrskagzdgd31h0da2rv9p89m8smplg") (yanked #t)))

(define-public crate-any_ref-0.1 (crate (name "any_ref") (vers "0.1.2") (deps (list (crate-dep (name "any_ref_macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0r4bwlgvck324apa15wjj7mlwzwxc5kf7wddl73agyaznxncyzwk") (yanked #t)))

(define-public crate-any_ref-0.1 (crate (name "any_ref") (vers "0.1.3") (deps (list (crate-dep (name "any_ref_macro") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "1sn3cyp3bjrdsixy5z6jgvjdw3n7l4jpszgmlzxzshb13jf0nh7b")))

(define-public crate-any_ref-0.1 (crate (name "any_ref") (vers "0.1.5") (deps (list (crate-dep (name "any_ref_macro") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0klcy533a0i5v5m2jm94z3yx94x0zyx83cqd6hx2igw05x56prqg")))

(define-public crate-any_ref-0.2 (crate (name "any_ref") (vers "0.2.0") (deps (list (crate-dep (name "any_ref_macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0s8yj25wbfpqgy87695yvxxqir0vgcpp44vjlmrdgsysx0p2w591")))

(define-public crate-any_ref-0.2 (crate (name "any_ref") (vers "0.2.1") (deps (list (crate-dep (name "any_ref_macro") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0jail3vjq2pyn76lz12v17h52lfsv2ibdyj5vvi9kzfaygzyw3qf")))

(define-public crate-any_ref_macro-0.1 (crate (name "any_ref_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1xadw7pc7pnr0428hpg2f9d8iv9gig3p9fm4x8s08ljsxcmxxpdl") (yanked #t)))

(define-public crate-any_ref_macro-0.1 (crate (name "any_ref_macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0fmircx20dnibz8k8als72dbsjc7gzkcbfpgs47xmxfd3kq2aqma") (yanked #t)))

(define-public crate-any_ref_macro-0.1 (crate (name "any_ref_macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "13i98wdivl676g2dfw48mxzcw0800yipks7b68ghxlygws5h6n82") (yanked #t)))

(define-public crate-any_ref_macro-0.1 (crate (name "any_ref_macro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1z4p2v7m419sl95dchqdznb5mml86az6mizv4wv5vq96nc2mrqf5")))

(define-public crate-any_ref_macro-1 (crate (name "any_ref_macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "09fan0wll2gxvyc9b0gjk4iv1303kg4sjyzwk8nqnj1pvzxb0vs0")))

(define-public crate-any_ref_macro-1 (crate (name "any_ref_macro") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0idxwv0yq4vpj3ahdp0gkk1s35l85835phgjgibjxwmg7n1ndqll")))

(define-public crate-any_spawner-0.1 (crate (name "any_spawner") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "^0.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (optional #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "178w1n58vq9f8fhshxkxpsq8j2zxqjjs4wkp3hlm1a19f6mwq3sp") (features (quote (("futures-executor" "futures/thread-pool" "futures/executor")))) (v 2) (features2 (quote (("wasm-bindgen" "dep:wasm-bindgen-futures") ("tracing" "dep:tracing") ("tokio" "dep:tokio") ("glib" "dep:glib"))))))

(define-public crate-any_terminal_size-0.1 (crate (name "any_terminal_size") (vers "0.1.17") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "widestring") (req "^0.5.1") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "handleapi" "memoryapi" "psapi" "synchapi" "tlhelp32" "winbase" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "10am66xm58n7ybnn2zlpfn50nqhf655kn82mkp4c9li4xv53yxqx") (yanked #t)))

(define-public crate-any_terminal_size-0.1 (crate (name "any_terminal_size") (vers "0.1.18") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "widestring") (req "^0.5.1") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "handleapi" "memoryapi" "psapi" "synchapi" "tlhelp32" "winbase" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1rbgll2qp362hw0h66j5ji56b781xqcpqypi9q04qvp87xm07470")))

(define-public crate-any_terminal_size-0.1 (crate (name "any_terminal_size") (vers "0.1.19") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "widestring") (req "^0.5.1") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "handleapi" "memoryapi" "psapi" "synchapi" "tlhelp32" "winbase" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "091nprbmyka5f3qkhyfpapxnwnkdl033l07xf6kxly4p2qlrija9") (yanked #t)))

(define-public crate-any_terminal_size-0.1 (crate (name "any_terminal_size") (vers "0.1.20") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "widestring") (req "^0.5.1") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "handleapi" "memoryapi" "psapi" "synchapi" "tlhelp32" "winbase" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0w9j825j951gv1rpnmr7hv0l9i3znpgfdw2p7sns4k98b0k3k5s5")))

(define-public crate-any_terminal_size-0.1 (crate (name "any_terminal_size") (vers "0.1.21") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "widestring") (req "^0.5.1") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "handleapi" "memoryapi" "psapi" "synchapi" "tlhelp32" "winbase" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0xbg2cd62spw4lqfa36bp0bq3mwxylclxx30021986mr866150a3")))

(define-public crate-any_vec-0.1 (crate (name "any_vec") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "19m6flgrddzbvdknj65fy22rhl2l2wrxiimj1rgw2khyvh7zn728")))

(define-public crate-any_vec-0.2 (crate (name "any_vec") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "1y156mjq3yzdxn6j2wl2aaqzs1dz4gfhxci37jwqr1wg38yqwfx9")))

(define-public crate-any_vec-0.2 (crate (name "any_vec") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "1cxwlvrz0s4jd0ys3z644q5pw1r6caqbm2zndxvcyqj0pjlf1mjg")))

(define-public crate-any_vec-0.2 (crate (name "any_vec") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "19ayahwir9ch3zi6ynfa92mjscjk13d6riza95fb7gam8mka5rba")))

(define-public crate-any_vec-0.3 (crate (name "any_vec") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "0nfqm3kv1j8d0d4ms9c5ap16h3w7b0asrlspchhphsdmy6qizsh0")))

(define-public crate-any_vec-0.4 (crate (name "any_vec") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "0kb15zxnbjllxyv406pdq2lbq8fkcynbn0ydidphplw9vfnxn7v7")))

(define-public crate-any_vec-0.5 (crate (name "any_vec") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "1ih36ly5r4lshi4i5gv3qrs8bq8zpsmdkcr2jcwmqv4m8nl7j27l")))

(define-public crate-any_vec-0.6 (crate (name "any_vec") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "07kz33aw21x3r4nfqlz2vdp80qqj2d1qg3raw6yphinwvnz0paws") (rust-version "1.61.0")))

(define-public crate-any_vec-0.7 (crate (name "any_vec") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)))) (hash "1a2fsbnlb3k81zz83s3ad96zbrf3d6s4dr1czklx0asgxwqp37k0") (rust-version "1.61.0")))

(define-public crate-any_vec-0.8 (crate (name "any_vec") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0pm25xp0d5y03575x3hx8p2cn3smj32q4458cw59clnc9yzm6nhj") (rust-version "1.61.0")))

(define-public crate-any_vec-0.9 (crate (name "any_vec") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1k328yhqx0zy4mc1m99kz6ald2gjn46ql505ibk67hy63a2y1f30") (rust-version "1.61.0")))

(define-public crate-any_vec-0.9 (crate (name "any_vec") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0k1xcvkcryzlfvkavwyk11a86rr2i00r5ycqkwwma6m4yrpbnihw")))

(define-public crate-any_vec-0.10 (crate (name "any_vec") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "06j0cljzsgird5nqfs9vk694dl7348rckwy7wc89fx57ji3w0sjz")))

(define-public crate-any_vec-0.11 (crate (name "any_vec") (vers "0.11.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1xr2xg6bzqsxpxl7s2d6m2h73glbk9v45510k1ijjrnq5n7l0mjv")))

(define-public crate-any_vec-0.12 (crate (name "any_vec") (vers "0.12.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "065pcb55d5wnk6x5pyyzxbj8iv3afklq5y4vqh70z13w8h8m7snp")))

(define-public crate-any_vec-0.13 (crate (name "any_vec") (vers "0.13.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1kcp1wj75agny3v5d4wq9sg2psvj0z0qa63vxwgvkiqvrjx1g3z7")))

(define-public crate-any_vec-0.14 (crate (name "any_vec") (vers "0.14.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "impls") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1x2zaw4km3422m9xhl95kpc1ady93ipjj6rgb45fyb0mwg2n1k9l") (features (quote (("default" "alloc") ("alloc"))))))

