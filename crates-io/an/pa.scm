(define-module (crates-io an pa) #:use-module (crates-io))

(define-public crate-anpa-0.2 (crate (name "anpa") (vers "0.2.0") (hash "0k6jdv6ml0rgcvj1j9z6k11gsbgclk860fyxl9rjpihq4x4ssvrn") (rust-version "1.75.0")))

(define-public crate-anpa-0.3 (crate (name "anpa") (vers "0.3.0") (hash "1sas65xg2ba1rs5kb7fkrw1qjiwiqhd2v6cl3clgq2nphy5aj2y9") (rust-version "1.75.0")))

(define-public crate-anpa-0.4 (crate (name "anpa") (vers "0.4.0") (hash "061vnr3w2gl69swqbiyf4y4ikikkhryc001v6z4pr9aysk361x3x") (rust-version "1.75.0")))

(define-public crate-anpa-0.4 (crate (name "anpa") (vers "0.4.1") (hash "0r3424vk0b85lbn17gmdfpnv90n7zjwlypvqhay4jlsffn4gs9g4") (rust-version "1.75.0")))

(define-public crate-anpa-0.5 (crate (name "anpa") (vers "0.5.0") (hash "030457rkj0vf5m8vbgcmywhscra135zm951m9wg9wk25glgzsz8z") (rust-version "1.75.0")))

