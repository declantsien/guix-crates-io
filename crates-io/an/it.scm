(define-module (crates-io an it) #:use-module (crates-io))

(define-public crate-anitable-0.1 (crate (name "anitable") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mockito") (req "^0.22") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.0-alpha.2") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "= 0.2.0-alpha.6") (default-features #t) (kind 2)))) (hash "09si3f6pfwmykgqqy0b1mnycviiklhas70nhh8r63c7p5hhb9hwi")))

(define-public crate-anitable-tui-0.1 (crate (name "anitable-tui") (vers "0.1.0") (deps (list (crate-dep (name "anitable") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "= 0.2.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.7") (default-features #t) (kind 0)))) (hash "1agllqz9z73b3vq3c581cpqhsyyin35ckkin8cnlsf5bcvr2pjnp")))

(define-public crate-aniterm-0.1 (crate (name "aniterm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.186") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0swpv31jx9ljdlv006c76j8i2hfq8rac1ghdvi53i087ljdib8yj")))

(define-public crate-aniterm-0.1 (crate (name "aniterm") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.186") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1i2cgcy0zfk317b7crhn2gcw3f0g54fgnh1s0nwigyw3lrcp8zv3")))

(define-public crate-aniterm-0.1 (crate (name "aniterm") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mockito") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.186") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nlcrsa91is3408vm693glaj0l246lll7fb1as6ymcdssa5psym9")))

(define-public crate-anitomy-0.1 (crate (name "anitomy") (vers "0.1.0") (deps (list (crate-dep (name "anitomy-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0l65sy7gxj583lwmr0kaydm2i2wb4blza983rabr6bpqrwnyzgwk")))

(define-public crate-anitomy-0.1 (crate (name "anitomy") (vers "0.1.1") (deps (list (crate-dep (name "anitomy-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gcmsv1y403c741b46kr20jrq5wh1apxqkj1n5551k3jjjca0xwg")))

(define-public crate-anitomy-0.1 (crate (name "anitomy") (vers "0.1.2") (deps (list (crate-dep (name "anitomy-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "17aaqjj6d1qyab3xyga78d8ycnd80j41wb0qahll8fjdn77x5nx5")))

(define-public crate-anitomy-0.2 (crate (name "anitomy") (vers "0.2.0") (deps (list (crate-dep (name "anitomy-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "19qvadbm9zm7r19v268pn0qg3xd7ijxgv3b63g7fqjr3gpyg0l7c")))

(define-public crate-anitomy-sys-0.1 (crate (name "anitomy-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1rw1ydxzcx86bjfk3rzdj3bpx0gyc0h4znagbd62k9ca6w21j21w")))

(define-public crate-anitomy-sys-0.1 (crate (name "anitomy-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1bkqvnfmh0ycbl9k87dxsdlglwfim07g01bf0cc6c2yyhxm49wgg")))

(define-public crate-anitomy-sys-0.1 (crate (name "anitomy-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1sih5q50f57rwg57jpcw7lagh55aid9jcvwqj0gykmmhy7gygbdi")))

(define-public crate-anitomy-sys-0.2 (crate (name "anitomy-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1nqp7514mf333fvnnh1gc4xpdj85zpsgjkhz0g6jzsgm0qyy3aij")))

