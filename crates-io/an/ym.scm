(define-module (crates-io an ym) #:use-module (crates-io))

(define-public crate-anym-0.0.0 (crate (name "anym") (vers "0.0.0") (deps (list (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0b949j0mhd6q2929yyfbb4cx0wrxq0h3a3i013yhzy6zywx4lapi")))

(define-public crate-anymail-0.1 (crate (name "anymail") (vers "0.1.0") (hash "0q4322k7i8z7cjlpl1hwa4y28lcq2qc1pg9wvlhr4z9q5hhklawv")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.0") (hash "15jfxx61vgmr7j7z1hni5lbay09iq2d328mnssg8fabpnpsicavw")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.1") (hash "1lmvy0awljm9mci71i0nxpask7kjk6gbb2fnadw1spgc2i6ilhwq")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.2") (hash "1fmf4p8x7nl72l8y1zx4iihcjjhw2jfca6z22769qw2lhzixn25c")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.3") (hash "00df537qd7wgni8gxdadpa1l0glhwh418ph31va5iv02w844n0hm")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.4") (hash "166mjr3249hawwsjrkcis02frmj0p8344ic54rypmhjal20zh5mh")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.5") (hash "1vjj1z745ifnswqir3khiwjikmpw50i4ph8nxyq11g6n8357gp88") (yanked #t)))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.6") (hash "0jxlb4yl8y2ymq6cwszpa2p0yba38x70hhxxch2d9z41fiwqrbh3")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.7") (hash "07man7lrgdc4k15brk4pa887b7c54rjkinhi7p7j4i75lp8ih4br")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.8") (hash "1ixi20wicf0dlgfinlzc8hyid9ii62bf51bx1hwd8jw1xnv5dap6")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.9") (hash "1b5a8lp7qhsnqsn9cq6bxf4c7zzsayki957dsqiv9pakllqksc5w")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.10") (hash "134prbf7asqpicgyy3gw5qip23xv38a0iwi8vgg25cm1v52zd9l3")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.11") (hash "05fd1d837fyiamkhy5sdpx8i8gkdfdx7qmv9wxm5lniwijd4xns4")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.12") (hash "1g67pr37rblxs6vk4bn62g5s1g6bbi9bifndb7vmz7iy9mxqsm3c")))

(define-public crate-anymap-0.9 (crate (name "anymap") (vers "0.9.13") (hash "0z4p2jylcw3lbrf3gx6sb7kqr870m0fgpgzap1b0vfc2ghm458wq")))

(define-public crate-anymap-0.10 (crate (name "anymap") (vers "0.10.0") (hash "17i2f1yliil33f5j7520jk8j3kfnjfx83ysx378jx3hqgdb9s266") (features (quote (("clone"))))))

(define-public crate-anymap-0.10 (crate (name "anymap") (vers "0.10.2") (hash "1snq3m5vmka50gzysldqm45h40vla1z5yndzbvgdh6bgbc2gx4pv") (features (quote (("clone"))))))

(define-public crate-anymap-0.10 (crate (name "anymap") (vers "0.10.3") (hash "0mm8fcnwi0pdq8g3zafv20r3h5hm5iipy6qx71nrm4s5bkfyxlbg") (features (quote (("nightly") ("clone"))))))

(define-public crate-anymap-0.11 (crate (name "anymap") (vers "0.11.0") (hash "1q86p7hzxnp7is9zw3sh0dqwsgghgzijcizh1ch433mgk97af7fs") (features (quote (("unstable"))))))

(define-public crate-anymap-0.11 (crate (name "anymap") (vers "0.11.1") (hash "10agz8rzm9b3f9x1b3yrl7k5jz4pplsvfj2z3i0iga64bilj8zml") (features (quote (("unstable"))))))

(define-public crate-anymap-0.11 (crate (name "anymap") (vers "0.11.2") (hash "0yxkm6pzg8m2s29k0gw00h42maqq0h68cifwd5bz3bmflm522j31") (features (quote (("unstable"))))))

(define-public crate-anymap-0.12 (crate (name "anymap") (vers "0.12.0") (hash "048llfh86kilxa2vk8xw1gjj6pkd21xmvc070j9lghs8s5xl40n7") (features (quote (("bench"))))))

(define-public crate-anymap-0.12 (crate (name "anymap") (vers "0.12.1") (hash "0i23vv38i22aazp5z2hz0a44b943b6w5121kwwnpq1brpm1l559k") (features (quote (("bench"))))))

(define-public crate-anymap-1 (crate (name "anymap") (vers "1.0.0-beta.1") (deps (list (crate-dep (name "hashbrown") (req ">=0.1.1, <0.13") (optional #t) (default-features #t) (kind 0)))) (hash "0amxwizzny9lhmm53dcz8snxrpckc817myjcii3hrvz53x6wvl0x") (features (quote (("std") ("default" "std")))) (rust-version "1.36")))

(define-public crate-anymap-1 (crate (name "anymap") (vers "1.0.0-beta.2") (deps (list (crate-dep (name "hashbrown") (req ">=0.1.1, <0.13") (optional #t) (default-features #t) (kind 0)))) (hash "0whxr4r34rzvw5xwr80clamb4szra0x5kmwp6ygdhl1xdxd8y7wg") (features (quote (("std") ("default" "std")))) (rust-version "1.36")))

(define-public crate-anymap2-0.13 (crate (name "anymap2") (vers "0.13.0") (hash "031kw3bp0zh2pn9fcayaw0w0gydgpgfhm08pg4yz5cml9jwv60fk")))

(define-public crate-anymap3-1 (crate (name "anymap3") (vers "1.0.0") (deps (list (crate-dep (name "hashbrown") (req ">=0.1.1, <0.13") (optional #t) (default-features #t) (kind 0)))) (hash "09vwpspwx9bx1513n31zic8szinkqnqkyk209vahn9s7z05ia5lk") (features (quote (("std") ("default" "std")))) (rust-version "1.36")))

(define-public crate-anyml-0.1 (crate (name "anyml") (vers "0.1.0") (hash "0pxm7hwm698644rz53sbrlivpsm7a9r6ind6fx9isf6grds2hz2d")))

(define-public crate-anymsg-0.1 (crate (name "anymsg") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1q8yf5bkk2hxw176f87a5cl5lnq9m9gf5ni0jvdzbhhnkmx8amlp")))

