(define-module (crates-io an sl) #:use-module (crates-io))

(define-public crate-anslatortray-0.1 (crate (name "anslatortray") (vers "0.1.1") (hash "1019517cnmzkzybxh4qxsnxcc130m9qy98bwjf9kybiknp1gdp5a")))

(define-public crate-anslatortray-0.1 (crate (name "anslatortray") (vers "0.1.3") (hash "1324vx7rypqpjjrn2as653235039qx056id6lzsypis3fwvqc0lz")))

(define-public crate-anslatortray-0.2 (crate (name "anslatortray") (vers "0.2.0") (hash "0dlx7cbqd1izbb7l8svg9m6fy48lg67gq44chb5539hryfgi9p3w")))

(define-public crate-anslatortray-0.3 (crate (name "anslatortray") (vers "0.3.0") (hash "1q346kh6amc3wgifb48g07mypz8a8xkhxvrgfl4vxyz0q156chq0") (features (quote (("nightly-features") ("default"))))))

(define-public crate-anslatortray-0.4 (crate (name "anslatortray") (vers "0.4.0") (hash "1wyf5nnrf9nvbijk9283zp8269g8mk5024695p6h5lnznn603zpz") (features (quote (("nightly-features") ("default"))))))

(define-public crate-anslatortray-0.5 (crate (name "anslatortray") (vers "0.5.0") (hash "1qz8q7n14j7in9ji849zdfn2bm3hrgv16hqw1hh9xxwp0844ndwy") (features (quote (("nightly-features-benches") ("nightly-features" "nightly-features-benches") ("default"))))))

