(define-module (crates-io an pp) #:use-module (crates-io))

(define-public crate-anpp-1 (crate (name "anpp") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)))) (hash "0q486hp0smdd1mwfpcf18ns5kpr8ksanzgn31cdwd20510mrl32j") (features (quote (("std" "arrayvec/std"))))))

(define-public crate-anpp-1 (crate (name "anpp") (vers "1.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)))) (hash "0p3zc9cjbwjik6xa0rrr9d3c5ffzasazawrfp8lf81wqqiic5qx3") (features (quote (("std" "arrayvec/std"))))))

(define-public crate-anpp-2 (crate (name "anpp") (vers "2.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)))) (hash "0aw5hfgk1d5fbnmgji80vfdgblg6mzz0978gyaqkv3bqydkkrryl") (features (quote (("std" "arrayvec/std"))))))

