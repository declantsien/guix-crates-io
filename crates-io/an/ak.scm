(define-module (crates-io an ak) #:use-module (crates-io))

(define-public crate-anakin-0.1 (crate (name "anakin") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.28.0") (features (quote ("process" "signal"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("process" "tokio-macros" "macros" "rt" "fs" "io-util" "time"))) (default-features #t) (kind 0)))) (hash "10a7zcfy56i9xn0000dx716kq8r1rc21anw8v5braq7d0iwrpp5x")))

