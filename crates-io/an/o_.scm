(define-module (crates-io an o_) #:use-module (crates-io))

(define-public crate-ano_jit_bfi_rs-0.2 (crate (name "ano_jit_bfi_rs") (vers "0.2.0") (hash "03xx8sjazinngk4w8v42imabfkp1fmsgipn72v5s95qzpiy3327b")))

(define-public crate-ano_jit_bfi_rs-0.2 (crate (name "ano_jit_bfi_rs") (vers "0.2.1") (hash "1cwb3s1abm9w69p9w81k0l2j5ra58hf7ikafgbdnsv52c92zpy7v")))

(define-public crate-ano_jit_bfi_rs-0.2 (crate (name "ano_jit_bfi_rs") (vers "0.2.2") (hash "010qdxhgkzkhfigkc84zx213bxdab0c4z23rlkphqfmpkm5vlql2")))

(define-public crate-ano_jit_bfi_rs-0.2 (crate (name "ano_jit_bfi_rs") (vers "0.2.3") (hash "1i09yylngkggsh1qsgam9gid65b4xrbm0bs6x2ml5qmn4s546zi1")))

(define-public crate-ano_jit_bfi_rs-0.2 (crate (name "ano_jit_bfi_rs") (vers "0.2.4") (hash "07949gf0b0yv8zz7x0cl7s5lhk47lww7kz366bfh9fs021ry1dk5")))

(define-public crate-ano_jit_bfi_rs-0.2 (crate (name "ano_jit_bfi_rs") (vers "0.2.5") (hash "1kphbsp2y6a4hnb78h3abdj2rza6fvm6dvwficpggk7kk0375c37")))

