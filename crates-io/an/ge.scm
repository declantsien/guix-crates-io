(define-module (crates-io an ge) #:use-module (crates-io))

(define-public crate-ange-0.1 (crate (name "ange") (vers "0.1.0") (hash "04akzvcw65wszj17q5l0xg0z004w7x1gal9ql0g2hny67gy0qni0")))

(define-public crate-angel-0.1 (crate (name "angel") (vers "0.1.0-alpha.1") (hash "17j5f3ym0qjdljxbg53dx6rg64cmfs3q59fxllf1krv5l9jzzfc5")))

(define-public crate-angel-0.1 (crate (name "angel") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "tracing") (req "^0.1.34") (optional #t) (kind 0)))) (hash "1qrmxr8bmgmxichzyssiv7bzswvfn499cjv9dnrc92xamjwrg9rg") (features (quote (("default" "std")))) (v 2) (features2 (quote (("trace-errors" "dep:tracing") ("trace-calls" "dep:tracing") ("std" "tracing?/std"))))))

(define-public crate-angelcake-1 (crate (name "angelcake") (vers "1.0.0") (deps (list (crate-dep (name "angelmarkup") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "cleasy") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0svahh6y8cdbp1il43nqyskqpq9wjx37pq3vvirrisf5qglmmsnm")))

(define-public crate-angelio-0.1 (crate (name "angelio") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1ri6j1x984ig181k7ja0xkyadkgmlk7xbdsh3l58jh6iplx42y6q")))

(define-public crate-angelmarkup-1 (crate (name "angelmarkup") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1jnbpdqlr5iwpfnk8bsxra2hna3gn9iqcnk9chhs290braq9459k")))

(define-public crate-angelmarkup-1 (crate (name "angelmarkup") (vers "1.1.0") (deps (list (crate-dep (name "cleasy") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1ypfgm6060bybmqslh7gkqcszxhrbc0k93hz3534nvp5z9pndh0l")))

(define-public crate-angelscript-0.1 (crate (name "angelscript") (vers "0.1.1") (deps (list (crate-dep (name "angelscript-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0r2wds4qlznhci0jkbhgmg1apx0k5lbw8kchpsr3l0asfwwq2nrx")))

(define-public crate-angelscript-sys-0.1 (crate (name "angelscript-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)))) (hash "1n7baycyd536lw344zm4mp8yf8y3zn5ajsg4kapvjx80p4d40xrd")))

(define-public crate-angelscript-sys-0.1 (crate (name "angelscript-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)))) (hash "05abs11ipf0z722psdp5m0ahjjrf9mnkg5mxnr9s0sykaqkisxjq")))

(define-public crate-angelscript-sys-0.2 (crate (name "angelscript-sys") (vers "0.2.312") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)))) (hash "1wymrq41zg3ilnx4ldnlk6ingi3kf9nyh9acm7sjvfr8acj2r67g")))

