(define-module (crates-io an dy) #:use-module (crates-io))

(define-public crate-andyleung9527_demo_crates_20231016-0.1 (crate (name "andyleung9527_demo_crates_20231016") (vers "0.1.0") (hash "13d761p4sq1q9ydm8jjk0n8bzz0yqbp0icm8pwjnm0307k6ycnrw")))

(define-public crate-andys_guessing_game-0.1 (crate (name "andys_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0rz747mmxh3c2vixdkyck5zx6whj1317j212bgab7ib9srwi95pk")))

