(define-module (crates-io an vi) #:use-module (crates-io))

(define-public crate-anvil-0.0.0 (crate (name "anvil") (vers "0.0.0") (hash "00zi6sa6pxxyasn9l86zr6ibcippyrg727nr8y9lqh6h3l8b839c")))

(define-public crate-anvil-0.0.1 (crate (name "anvil") (vers "0.0.1") (hash "03vf9x16kzxahd2sayr8c7hndd6n9jhvnnn9lb82k5wpyqlyw1fk")))

(define-public crate-anvil-region-0.1 (crate (name "anvil-region") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "062akby84vifkhdr0f4b5v8jvxcbj6y5ni0hwb4hiwzd1zg70qvg")))

(define-public crate-anvil-region-0.2 (crate (name "anvil-region") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "0sj8a53wm7y5n9dsxh1kqf86qidry6wv011xyj3dpqsm7kgpg19s")))

(define-public crate-anvil-region-0.3 (crate (name "anvil-region") (vers "0.3.0") (deps (list (crate-dep (name "bitvec") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "0mk948wib4c9pid4mnk9dn793cxvxhghwyq39mfvza1k7cysjv5n")))

(define-public crate-anvil-region-0.3 (crate (name "anvil-region") (vers "0.3.1") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "1nmcaq2mch2mjyb6m7dvvk076z2wqsjc8h9cfcr81sk94393c7gc")))

(define-public crate-anvil-region-0.3 (crate (name "anvil-region") (vers "0.3.2") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "0y3ki6j755dy72rf55fp3rly1zn1kqpbfm935hqbvkmxk4rm99a0")))

(define-public crate-anvil-region-0.4 (crate (name "anvil-region") (vers "0.4.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "17y5nn7x0bsb22rixn2pjw75kicvl1wxib0bzg4qw4lkywz63qfq")))

(define-public crate-anvil-region-0.5 (crate (name "anvil-region") (vers "0.5.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ml72ivdbizcq7wllyip8zllmqqrs3yf28ivh23vphzaklr9h2ly")))

(define-public crate-anvil-region-0.6 (crate (name "anvil-region") (vers "0.6.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.3") (default-features #t) (kind 0)))) (hash "129mnqg2iaxys5n6kanpcpn79mi6kpmr2bxiszdkb3ls8sx30q6p")))

(define-public crate-anvil-region-0.7 (crate (name "anvil-region") (vers "0.7.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ylh2m5zwkx2xh76gls8zfw5qksf6jjz5s95qrsn1l4s6w7pg40y")))

(define-public crate-anvil-region-0.8 (crate (name "anvil-region") (vers "0.8.0") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vq70k8h1vcisy650rcc5pxrn5m00wvcn72cglff6vbskgnxql7j")))

(define-public crate-anvil-region-0.8 (crate (name "anvil-region") (vers "0.8.1") (deps (list (crate-dep (name "bitvec") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "named-binary-tag") (req "^0.6") (default-features #t) (kind 0)))) (hash "1yf9vy2mc1ij3k0j74p5nxkykldsr9n7dvcak48qxlj94kamynr2")))

(define-public crate-anvil_db-0.2 (crate (name "anvil_db") (vers "0.2.0") (hash "0w07nq7hbw2hxmn484y1l0bf078l7623f05gbi9mldj7x9drpn0r")))

(define-public crate-anvil_db-0.2 (crate (name "anvil_db") (vers "0.2.1") (hash "0vb95qxvnc6v6s0an0fg46wzczx6lx4nfl7r5yn7i7lizf2mps0s")))

