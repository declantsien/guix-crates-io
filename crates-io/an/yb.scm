(define-module (crates-io an yb) #:use-module (crates-io))

(define-public crate-anybar-0.1 (crate (name "anybar") (vers "0.1.0") (hash "18csjzvs9p0c6jjgv0yz076v1v1sqkl4y2d20lzglpnr8hwzy6dq")))

(define-public crate-anybar-0.1 (crate (name "anybar") (vers "0.1.1") (hash "1ynl92wy6857sscgkjhrpidzyqga1cd6bvjyq5j5h5vhfjm7nibz")))

(define-public crate-anybar-0.1 (crate (name "anybar") (vers "0.1.2") (hash "08akl7q72mrag93fgpy116szmd2rhkkpsf95rbbpmlqgjyzjwp6p")))

(define-public crate-anybar-0.1 (crate (name "anybar") (vers "0.1.3") (hash "1ha9z8kd0wmmgswghxlc41hbazdp9v0xyklyzkcnv2y6hrsb7lia")))

(define-public crate-anybar_rs-0.1 (crate (name "anybar_rs") (vers "0.1.0") (hash "1jbayv3s91agcjlcxw004lkbw6zrpv31fzxnbf3877yk8vipf6jy")))

(define-public crate-anybar_rs-0.2 (crate (name "anybar_rs") (vers "0.2.0") (hash "0giikadaij30g9l4qwxr36m8fv8bidxas4iv238d13kigcyy7w1x")))

(define-public crate-anybar_rs-0.3 (crate (name "anybar_rs") (vers "0.3.0") (hash "0yqi9xsq3mvi698wjnlsg9z2nqjrmxp37nyid0f91hk167xip5mp")))

(define-public crate-anybar_rs-0.4 (crate (name "anybar_rs") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cq3ylzqm2f28szvvby922yaani7c4vp9lz3w8shcpfvfcln1mnw")))

(define-public crate-anybar_rs-0.5 (crate (name "anybar_rs") (vers "0.5.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zip9lmy50p1qgym677m41l9h00rgf6is45gwn4yh42x6z6nirq5")))

(define-public crate-anybar_rs-0.6 (crate (name "anybar_rs") (vers "0.6.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "10w5r9k1vg32b5xnrfn6gqkz95614i6zmfv0lw04p2mz4l7wh7sj")))

(define-public crate-anybar_rs-0.7 (crate (name "anybar_rs") (vers "0.7.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0d66bi8ih9d6adhlchv0xczkxpc4a65vcr6v9yx37izjbajl7qfm")))

(define-public crate-anybar_rs-0.8 (crate (name "anybar_rs") (vers "0.8.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fh28lvxpfy7nn59yacbnbl0z00iwriqm32xdsrrz34dsw82m6n7")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dc103jzxhky27mi853cjffxs5scnsqi2fkssyzl588qlrkfw92w")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wgrrphrabg48psw3vzlprlgdhr0rh05yzkpm1g20aqssqi2kcdy")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1smw0pmvfcg1mg63g6iz42y44x5jdla1c0j8c5gn5w9h8lmwaa35")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g4v09y231c0cisd5h5cswf8bldk1p8j78ci904ci14prfdf3q65")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s8q8wwnp1bmyx92zsc44gjd7aixyhl3d72m1pis63n16w0l5v28")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.5") (deps (list (crate-dep (name "clap") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1q9a4dkfrqljrr5ra197gj6nbkhxlknvzpiaxr9dvflpm5rbnsdx")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.6") (deps (list (crate-dep (name "clap") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1qprq1m6wb9ypjhalqv9zfx3f6bgc80l8xzvjhba0jvhab7316ic")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.7") (deps (list (crate-dep (name "clap") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0jssal4n8lan3jl475r09sprkcg5nys91l0vvpwsbqd80sjw9n25")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.8") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "1b19rbvj4syikvz9y5caxvngl09xsg74mjlmhsr7la4zx956jaff")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.9") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0w0f2awqzm483fc5awr7v8610ipbmcf8nz024smi3221fmqn2h1d")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.10") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "1i9f3dgv83dyfvgjsv5gzk599a5db1g3iami53hf5j2bnmd7yin4")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.11") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "10s1j5j2xaipvihph7cray8wn50l6ssmms3rxhwvdy1mq88kv472")))

(define-public crate-anybar_rs-0.9 (crate (name "anybar_rs") (vers "0.9.12") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "04zfjwh6lfa9nvb3yqn6wwpm34pllck64fw9i7sb0m7k9bwdxpcc")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0ijmwn01n162kipk6s1hdxhpzzrlsz319fby192ihqlf26zwc8f4")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "10ji0dm8bqhz89kyl12sf7mcf7gp0qjwr3k4gyf6jb0qm5811lal")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "088l6vqslnm70csxrzmm53nh1ii98ygrgvhnlkm9plzy1v3yrjjn")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0pghxniwsksj6vb2pml4fa72s8y4j5bsas0mxmmy0mbgyhnd79la")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "1rs718zvih2afnwwv25h3rds90rlalqlnimd18x7x10zisq4rpw1")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^2.23.3") (default-features #t) (kind 0)))) (hash "0jv7xl3nvj1v2aw02x4diggznw66dsa2qx47ip7rb4ppbnggix44")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.6") (deps (list (crate-dep (name "clap") (req "^2.23.3") (default-features #t) (kind 0)))) (hash "0azjy2552sz7c04fqk26b0vk6zwipvq8q1j117jhk3585a18jy26")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.7") (deps (list (crate-dep (name "clap") (req "^2.23.3") (default-features #t) (kind 0)))) (hash "0hslnxj4p80k477mbz5428fzkqnx1syqf1ka5c6miaaxiqnvh332")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.11") (deps (list (crate-dep (name "clap") (req "^2.30.0") (default-features #t) (kind 0)))) (hash "16048f414p9knpccriwj46adxc5ngjalji2fhcv8n266xpfl0qp8")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.12") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1ailzh83scpgadaj9lap78p1z785v649gf47rsidcs182lash63h")))

(define-public crate-anybar_rs-1 (crate (name "anybar_rs") (vers "1.0.14") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "08wp0np3wziivwlxl3bxk83jdxcia9w0xcg485y7f9416lbrr2s6")))

(define-public crate-anybox-0.1 (crate (name "anybox") (vers "0.1.0") (hash "0n0k90di0gdwvv21lkbsz6yzz44r8fwm4cdz0s31phrmps062829")))

(define-public crate-anybox-0.1 (crate (name "anybox") (vers "0.1.1") (hash "0ar89p1bdqdqhdmydsagchdwapywql9735gpjnfb9k21h3ypd1bg")))

(define-public crate-anybuf-0.1 (crate (name "anybuf") (vers "0.1.0") (hash "1sz4q1vckixway0d5nsg9m9p1dk4xvfivbhsn49drvvbbgfkhigi")))

(define-public crate-anybuf-0.2 (crate (name "anybuf") (vers "0.2.0") (deps (list (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "1p5hxnsf3lkzm4mqzg4p9lvfp225789p2wd88aann1n7m3xa81py")))

(define-public crate-anybuf-0.3 (crate (name "anybuf") (vers "0.3.0-rc.1") (deps (list (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "0yi9p4wi25dfm8f47vm0chiikyyhh38in93jds71pd6hwxw3sz7c")))

(define-public crate-anybuf-0.3 (crate (name "anybuf") (vers "0.3.0") (deps (list (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "0ndqxjvrhklk3zvcljz07qi829gq0yz30ybjmin4c8gsam6yn0ws")))

(define-public crate-anybuf-0.4 (crate (name "anybuf") (vers "0.4.0") (deps (list (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "1nr0xnmxbmk4k23d0lb7nji41jrk1yhsw3mcs72rdwqlvjvl66jw")))

