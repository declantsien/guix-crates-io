(define-module (crates-io an -v) #:use-module (crates-io))

(define-public crate-an-video-0.1 (crate (name "an-video") (vers "0.1.0") (hash "0a145rjd1rg04r59d6hb0g5dm64a2hm4fdcgsay4h8ssk2srf90p")))

(define-public crate-an-view-0.1 (crate (name "an-view") (vers "0.1.0") (hash "08a0lhr5213fp8dr7pk9gvkgs45y62h8as6yxcd31cz3m3884f5y")))

