(define-module (crates-io an yo) #:use-module (crates-io))

(define-public crate-anyof-0.1 (crate (name "anyof") (vers "0.1.0") (hash "1a485pbrada9zxkv8in67bf1chcw47aql66jcvfrfjn47wk67hfs")))

(define-public crate-anyotp-0.0.1 (crate (name "anyotp") (vers "0.0.1") (deps (list (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.3") (default-features #t) (kind 0)))) (hash "0dd7pvg4svjfnqq8diz06w994lhwwn8av0l6x70rjd1qisp4d96c")))

