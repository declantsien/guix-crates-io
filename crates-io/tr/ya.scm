(define-module (crates-io tr ya) #:use-module (crates-io))

(define-public crate-tryagain-0.1 (crate (name "tryagain") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.2") (features (quote ("rt" "macros" "time"))) (optional #t) (default-features #t) (kind 0)))) (hash "1b9a1sxa87ha8bz8pggymb462smi934yd1fzd74vr8x4a5bs6b88") (features (quote (("runtime-tokio" "tokio" "pin-project") ("runtime-async-std" "async-std" "pin-project") ("default" "runtime-tokio"))))))

