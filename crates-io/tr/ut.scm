(define-module (crates-io tr ut) #:use-module (crates-io))

(define-public crate-truth-0.0.1 (crate (name "truth") (vers "0.0.1") (hash "0cjqpy9sxbbjnxkbyyy8437dmdsp7pwrwkafi9z0yjl1waarcash")))

(define-public crate-truth-table-0.1 (crate (name "truth-table") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "0655m71k59jkjxm8hwm5mqxvlcxqkpnf2v5rf1qizkvg8sbsngg1")))

(define-public crate-truth-table-0.2 (crate (name "truth-table") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0gzn74kvlc5b110s1p2jpmdj9lr5c5jap9cbkc0biw3qnqbbr0m1")))

(define-public crate-truth-values-0.1 (crate (name "truth-values") (vers "0.1.0") (hash "1agfhf44si9zkdp98hw93kxbmcxar50nrrf4qm1wlkvqxr47qrjk") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-truthful-0.1 (crate (name "truthful") (vers "0.1.0") (deps (list (crate-dep (name "cli-table") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "permutator") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^10.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml-base-config") (req "^0.1") (default-features #t) (kind 0)))) (hash "110xvzbxn9066fiad5wrbyl2qzj9aw2gjl17rhhvwj7akd9ph4pa")))

(define-public crate-truthful-0.1 (crate (name "truthful") (vers "0.1.1") (deps (list (crate-dep (name "cli-table") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "permutator") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^10.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml-base-config") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vmqp6yn70w1pvg8az4k8qd94gy25wp6z1c5wz6sj5375qkr7h60")))

(define-public crate-truthtable-0.0.1 (crate (name "truthtable") (vers "0.0.1") (hash "0sr38q07mzaiwz9xbyz1aak8v84n7nzbivd3qdhb32xb191r9d38") (yanked #t)))

(define-public crate-truthy-0.0.1 (crate (name "truthy") (vers "0.0.1") (hash "19ylyalnxf4lnwvl9vga7c1wwiyq1lb8gp0291cj35naq90vpdx7") (yanked #t)))

(define-public crate-truthy-1 (crate (name "truthy") (vers "1.0.0") (hash "0zysxkyz39ki4awfwfx8cj6mycxhanvz54b2c0wv6lfww56icd74")))

(define-public crate-truthy-1 (crate (name "truthy") (vers "1.1.0") (deps (list (crate-dep (name "either") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "08llbz30qpqdfq5qmpj34g28g980zsvwq1wdhw3zv3r9vzqx1sy6") (features (quote (("default") ("and-or" "either"))))))

