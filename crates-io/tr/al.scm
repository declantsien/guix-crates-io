(define-module (crates-io tr al) #:use-module (crates-io))

(define-public crate-tralloc-0.1 (crate (name "tralloc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "06q75i390ygxy1liv24lj93fash0s36vz5ia9cbricj1hig22bfh")))

(define-public crate-tralloc-0.1 (crate (name "tralloc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xjzlmi3ijghsgs6gk5j46gz6p8cwqsc093mns7halrmn6xbhxmq")))

(define-public crate-trallocator-0.1 (crate (name "trallocator") (vers "0.1.0") (hash "15zg3807s1fn77lghi2z68rsm1bjdb1m2jx1kjwd093lk84pa1yd") (features (quote (("max-usage") ("allocator-api"))))))

(define-public crate-trallocator-0.2 (crate (name "trallocator") (vers "0.2.0") (hash "1pmxfbhwx51zfmhh2i6z4ljn827c58afaaf5773rxkb3r14mi5jq") (features (quote (("max-usage") ("allocator-api"))))))

(define-public crate-trallocator-0.2 (crate (name "trallocator") (vers "0.2.1") (hash "0rlqs3acpp1w4lwccrc1r9pmvhi0xlpai8npiykm009gn47wbvyi") (features (quote (("max-usage") ("allocator-api"))))))

