(define-module (crates-io tr -r) #:use-module (crates-io))

(define-public crate-tr-rs-0.1 (crate (name "tr-rs") (vers "0.1.0") (deps (list (crate-dep (name "float-ord") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "wilders-rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yfz7zrnm3f20s0r723riw24c4yy6xf596r6alrhckbjg3kln51w")))

