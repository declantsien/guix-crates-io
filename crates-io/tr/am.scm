(define-module (crates-io tr am) #:use-module (crates-io))

(define-public crate-tram-0.1 (crate (name "tram") (vers "0.1.0") (hash "00zikzyx34gi3bb1z8ipf5ijzpykd1phxchr6q1nx8vhx7ih59s2")))

(define-public crate-tram-0.2 (crate (name "tram") (vers "0.2.0") (hash "11y5n4srx15njpmawssx3k25c8glbl53kcfpp654hx0cybzsvcam")))

(define-public crate-tram-0.3 (crate (name "tram") (vers "0.3.0") (hash "0yk65cpvjmdi8yr657ps2qmj24f0nsgdpin6007qbn2hrhjk4h5r") (yanked #t)))

(define-public crate-tram-0.3 (crate (name "tram") (vers "0.3.1") (hash "1p437yibnk53npzs5j0mhs7gxskaimmxw2h9g7q11318v9y3f7rc")))

(define-public crate-tram-0.4 (crate (name "tram") (vers "0.4.0") (hash "08139pjvby9wi5laf1s9prjacc89725m87z4ykahs4gyvrs0vrcl") (yanked #t)))

(define-public crate-tram-0.4 (crate (name "tram") (vers "0.4.1") (hash "150ys3ql7n545v7nk8zv23a5w2fn7f2scgsblcc6dg0w0ynb08c6")))

(define-public crate-tram-0.4 (crate (name "tram") (vers "0.4.2") (hash "0cg4hwrajxiccwr404lj6jjyz37j87zyrd401fnadk7nk2saslvy")))

(define-public crate-tram-0.5 (crate (name "tram") (vers "0.5.0") (hash "19a1ayyhl214vwfshkh6v8prqy9rkh5p8m83dfn024xrpgwpaib4")))

(define-public crate-tramex-0.1 (crate (name "tramex") (vers "0.1.0") (hash "0sin6q80pdhzrrk0d2jrszsr9bv69bf0y9ggalmgg6z6chg5xng5")))

(define-public crate-tramorred-0.0.1 (crate (name "tramorred") (vers "0.0.1") (hash "0nz0q28ahkr31m7j12s8872linnm393snhlphxjk1jh78n5hmjqy")))

(define-public crate-tramp-0.1 (crate (name "tramp") (vers "0.1.0") (hash "1g0a3adw6nc3y77wa62h4mb4nm613gsyxqv6aav8bgh4rnydvy7h")))

(define-public crate-tramp-0.2 (crate (name "tramp") (vers "0.2.0") (hash "12zf4l5zs0m7x2v1y48rx92a88z13zvf8s0r5xifcj8x26klx5xq")))

(define-public crate-tramp-0.2 (crate (name "tramp") (vers "0.2.1") (hash "09b5zh683z2r1r94d0va4bkahvgx8dj0pah4k7906pri6dimrghd")))

(define-public crate-tramp-0.3 (crate (name "tramp") (vers "0.3.0") (hash "1x1c7c0xrijfckivjh81rmw9niafxarbplpyjfdq2blzv8p07vmi")))

(define-public crate-trampoline-0.1 (crate (name "trampoline") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (kind 1)))) (hash "07zasbl0clskr7cf04jgp394hzxhpm7kdpmkp93xi6ml0qirc1gc")))

