(define-module (crates-io tr uf) #:use-module (crates-io))

(define-public crate-truffle-0.1 (crate (name "truffle") (vers "0.1.0") (hash "0qjz87d8vzrnr3sd33ckgykgikwkfrkiwd7fmx71i7l5nc23zivi")))

(define-public crate-trufflehunter-0.1 (crate (name "trufflehunter") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "larry") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "two_timer") (req "^1") (default-features #t) (kind 0)))) (hash "0yaylf7cdhiwj5dgfallh3plr6yc4ldh4gn0fs69qa0kplhyr8wh")))

(define-public crate-trufflehunter-0.1 (crate (name "trufflehunter") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "larry") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "two_timer") (req "^1") (default-features #t) (kind 0)))) (hash "0gk4qr26bdzw8cp6q6gf33vf0iwsgx6akagb45m7fa8y9f0w7llx")))

