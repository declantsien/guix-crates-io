(define-module (crates-io tr oi) #:use-module (crates-io))

(define-public crate-troika-0.1 (crate (name "troika") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1zhkzx2mdzy016np8jhpp9ccl0pw5yhq1pl4nj3c26vkqvdx8kgx")))

(define-public crate-troika-rust-0.1 (crate (name "troika-rust") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ict") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0p7w3y4bc3zvkvarcswn3yn5ms74z0jmz2h6fvnaq6ah2m65xh9f")))

(define-public crate-troika-rust-0.1 (crate (name "troika-rust") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ict") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0g9lgp0856wcwqm1jdl4lfx3v80qhn18ph095zn7qicy8a1iaznp")))

