(define-module (crates-io tr mv) #:use-module (crates-io))

(define-public crate-trmv-1 (crate (name "trmv") (vers "1.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1bcca1nagqa1x1nynzbx3hsxdmxzq22kyx54iygq7cm1mar7a9h0")))

(define-public crate-trmv-1 (crate (name "trmv") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "09rqndirynmdl3m68klwjiz7ilkc4yglhzf1p7vjy6dlzg9h1v5w")))

(define-public crate-trmv-1 (crate (name "trmv") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0xl7rz87an2ndih5qld6wj1vxrm5ps68q53mgar7yhlmhw947k0q")))

