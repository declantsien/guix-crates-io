(define-module (crates-io tr ue) #:use-module (crates-io))

(define-public crate-true-0.1 (crate (name "true") (vers "0.1.0") (hash "16js9hahpc4hgsyqrh2hbq52mnnjrks8sicvxr5w04c4x5cw33jl")))

(define-public crate-truebner-smt100-0.1 (crate (name "truebner-smt100") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "~0.6") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "stream-cancel") (req "~0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "~0.3.1") (features (quote ("rtu"))) (optional #t) (kind 0)) (crate-dep (name "tokio-serial") (req "^3.2") (optional #t) (default-features #t) (kind 0)))) (hash "0vxv1g4aj0s399ykqj7hsz29rlgljmf8zz81a017bx92v2z60lxj") (features (quote (("modbus-rtu" "tokio-modbus" "tokio-serial") ("mock") ("default" "modbus-rtu" "mock"))))))

(define-public crate-truebner-smt100-0.2 (crate (name "truebner-smt100") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "~0.6") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "stream-cancel") (req "~0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "~0.3.2") (features (quote ("rtu"))) (optional #t) (kind 0)) (crate-dep (name "tokio-serial") (req "^3.2") (optional #t) (default-features #t) (kind 0)))) (hash "090zpdrwys11jp1chq3as88vv9bw0n5mlbqcby9mvn58f0hwd217") (features (quote (("modbus-rtu" "tokio-modbus" "tokio-serial") ("mock") ("default" "modbus-rtu" "mock"))))))

(define-public crate-truebner-smt100-0.2 (crate (name "truebner-smt100") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "~0.6") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "stream-cancel") (req "~0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-modbus") (req "~0.3.2") (features (quote ("rtu"))) (optional #t) (kind 0)) (crate-dep (name "tokio-serial") (req "^3.2") (optional #t) (kind 0)))) (hash "13cg1xnqp2fwmx1vgpbk1ml6nk4zkwy6cc0626p474ywqa1laf4x") (features (quote (("modbus-rtu" "tokio-modbus" "tokio-serial") ("mock") ("default" "modbus-rtu" "mock"))))))

(define-public crate-truecase-0.1 (crate (name "truecase") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.29.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0cmmaaykbs70d4mzqv01nm22wf9aj7rqykpgbgd9b5iq5ckpq4zj")))

(define-public crate-truecase-0.2 (crate (name "truecase") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "0j979yskg7wwqxx9qgz1m9irs977n77gh9n40d0ndf52f9cvl2wy")))

(define-public crate-truecase-0.3 (crate (name "truecase") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "indexmap") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g2cmzb8sz7gkj4yx3cww4hi32fgp1x1wkccr0941j6dbbbs5wlf") (features (quote (("default") ("cli" "clap" "anyhow"))))))

(define-public crate-truecase-0.3 (crate (name "truecase") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "indexmap") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "101n6k9jhk81wpmqw6xb1xbz89gg30fgg2km66x8j3hm319lp97b") (features (quote (("default") ("cli" "clap" "anyhow"))))))

(define-public crate-truegrf-1 (crate (name "truegrf") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 0)) (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "1xcsfvgmsgp6wdjik70w00y4dbhqy5k69dgvgyjygybr468l09cb") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-truegrf-1 (crate (name "truegrf") (vers "1.0.1") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 0)) (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "1yw52nf24r6llxgxbqf68kx06qpmb7lyr6l310168k25hav3hw5w") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-truegrf-2 (crate (name "truegrf") (vers "2.0.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 0)) (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "0n891ryd0j3fs9zqhwxjvk1c0nwz4xz77m92czrlrb6nzf8nx6xv") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-truegrf-2 (crate (name "truegrf") (vers "2.0.1") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 0)) (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "1c2zy69wi3v4v2brwwwhv4brmz7y2jf3ssafj5an4920laf4fqqf") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-truegrf-2 (crate (name "truegrf") (vers "2.1.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (features (quote ("png"))) (kind 0)) (crate-dep (name "js-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "0p0nb458ambkmpc3vsk6jyh2c9m0xzpx3jilzf8f7yihy1g7hs3b") (features (quote (("default" "console_error_panic_hook"))))))

(define-public crate-truelayer-extensions-0.1 (crate (name "truelayer-extensions") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "1v39qk5bk3nzzg7gbf1i2213dda8fb39wnmg2f3lz1n2wbzxrdsr")))

(define-public crate-truelayer-extensions-0.1 (crate (name "truelayer-extensions") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "0wdki49n9swy21299j2rykjry65r032x5px6nfp7lcikn8n142al")))

(define-public crate-truelayer-extensions-0.1 (crate (name "truelayer-extensions") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "1qs1x8nhfr914kzn1af394zpgfri7xk4vb0809fzmr6ibz73dxv9")))

(define-public crate-truelayer-rust-0.1 (crate (name "truelayer-rust") (vers "0.1.0") (hash "0fdlb5yrj1fld5zil3nrvvvh19gfb86kdh5q51pqrfzfh05v1a19")))

(define-public crate-truelayer-signing-0.1 (crate (name "truelayer-signing") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0vp89q3d4a0jdrvwjwcnnw6fskr4pj9x65b80cbdww9hnlm0ikva")))

(define-public crate-truelayer-signing-0.1 (crate (name "truelayer-signing") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0745zzg2i28qabbix9p47wz26sk057nc5hd5x1qwcq752hxy01ng")))

(define-public crate-truelayer-signing-0.1 (crate (name "truelayer-signing") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1yaza14pxk4874jj0bax9g0q85va5g44iii2vjl8h6a70sfv4vzq")))

(define-public crate-truelayer-signing-0.1 (crate (name "truelayer-signing") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1fbf3bi20360fxssky4vcp99ln1zpkaaprv9wysm7f3wcq3xcflg")))

(define-public crate-truelayer-signing-0.1 (crate (name "truelayer-signing") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1fp3yrynzrrbv30xiaw7wa0v4chkccjg1iplppkcb4f1hyyqn052")))

(define-public crate-truelayer-signing-0.1 (crate (name "truelayer-signing") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1njry8q0fb8n7xc5mrnzq327p43sgvl39s9864cw0iq7myn6a2g5")))

(define-public crate-truelayer-signing-0.1 (crate (name "truelayer-signing") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1f1a8xk7s4xzc49n3b8zwzjdfym1a0m64p29m8xhp6h02g893c0w")))

(define-public crate-truelayer-signing-0.2 (crate (name "truelayer-signing") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y0cw8xvgyi2ahpsk8lbjpfxgfq9w7mmhq30wzg2ad2pd12nqnw5") (yanked #t)))

(define-public crate-truelayer-signing-0.2 (crate (name "truelayer-signing") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pp2lgwnx2adn4zlv1wvfxab5bh9zvfm6lz8fvj883bxhl8ns0l9")))

(define-public crate-truelayer-signing-0.3 (crate (name "truelayer-signing") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0571vj35kzin32c5cmbksb9fzsw4zw0zd0jribhrn5fzwxjfvvi3")))

(define-public crate-truemoji-0.1 (crate (name "truemoji") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "truemoji-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1k317rwny5z0n00vqrvmiak970vkvn4qv7r798fdnrb7fj0s4c8h") (yanked #t)))

(define-public crate-truemoji-core-0.1 (crate (name "truemoji-core") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1awxawkla5carbav7vr2c04dwz8z7mmvxib8nbgckwjyxas3cmlr") (yanked #t)))

(define-public crate-trueshare-0.0.0 (crate (name "trueshare") (vers "0.0.0") (hash "130mil5j25lcgvl3jg2gqy00x01idkzfhbr0kgmily4z5dnwdr9x")))

(define-public crate-truesocks-1 (crate (name "truesocks") (vers "1.0.0") (deps (list (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "socks" "gzip" "deflate" "brotli"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest-middleware") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest-retry") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zbczk4ypy4y18af0iagvc6wncmabzd8yaz77r6p1l2wy58hn9gm")))

(define-public crate-truetree-0.1 (crate (name "truetree") (vers "0.1.0") (hash "1mhvgkcl0qx9wd68h41sjgw5sg9m0fbz3bl5wpnw4bxjp0m2zpyr")))

(define-public crate-truetree-0.1 (crate (name "truetree") (vers "0.1.1") (hash "0fq4zdnxdwbfd3ix0m0vpdkcm3m60s687mb8klcl6nhcl671la6v")))

(define-public crate-truetree-0.1 (crate (name "truetree") (vers "0.1.2") (hash "10fag0zwz8nf7y8z7cm0cx995fcrwkz96d6z1swa66wgqrsspdf5")))

(define-public crate-truetree-0.1 (crate (name "truetree") (vers "0.1.3") (hash "0kw2cqd8y34mqcsss3jpm6mzhdazqzifrb2s4fdnyhxqqngyc52y")))

(define-public crate-truetype-0.0.1 (crate (name "truetype") (vers "0.0.1") (hash "057ydxf454sq86mhz2pr5xb4jdc63p2rfp3a7lavg6gdgprz5y3z")))

(define-public crate-truetype-0.1 (crate (name "truetype") (vers "0.1.0") (hash "1wi8k6n8jrzaznwr98619fdfvibvwrvis6lknhwy0g32j72mlrsk")))

(define-public crate-truetype-0.2 (crate (name "truetype") (vers "0.2.0") (hash "01swbyzxqrqvym3kd90x7n1ghwkgv78s65x6icyqrj8cgj4vyspm")))

(define-public crate-truetype-0.2 (crate (name "truetype") (vers "0.2.1") (hash "0cf3wz11k8p8z4nhvmy5v1z74669rzbhps2sw2nzr1s6hm1xgm2s")))

(define-public crate-truetype-0.2 (crate (name "truetype") (vers "0.2.2") (hash "052zlcs68wrfmmvibngk7mh5vcbdh8s7k3mhzsjbhhlgi2bkbqgy")))

(define-public crate-truetype-0.3 (crate (name "truetype") (vers "0.3.0") (hash "0x7s52r1nkivwxw9nr31505yhdfjm2yrh5c8w5m7v04cj57scxvn")))

(define-public crate-truetype-0.4 (crate (name "truetype") (vers "0.4.0") (hash "03c69sy1nfjrqyfgvzbppp63x5msk96xz6kaymfrg5242vlrdpyn")))

(define-public crate-truetype-0.5 (crate (name "truetype") (vers "0.5.0") (hash "18aqzwc0lgm0spcay743ckvx8w39v8syifhnyp9zwag0qyrqgg99")))

(define-public crate-truetype-0.6 (crate (name "truetype") (vers "0.6.0") (hash "1rl0jqazfgna35sjfw8308v9f40p2dqy0q6shh1lnj4yg94sfl8v")))

(define-public crate-truetype-0.7 (crate (name "truetype") (vers "0.7.0") (hash "1nkvl75cbcj9x3b05iz3rgmdchxcjzagwqshdbfzvp0qmz7lxh0a")))

(define-public crate-truetype-0.8 (crate (name "truetype") (vers "0.8.0") (hash "0w9xhp9a340dl3l5xfilbiwncrpssjk1c57q6fq2ciirfmgz2aan")))

(define-public crate-truetype-0.8 (crate (name "truetype") (vers "0.8.1") (hash "0a6calyqqm2hgczl3gnv8mlivvbi4rfbymdwmkf9dylkdrhjx4rb")))

(define-public crate-truetype-0.9 (crate (name "truetype") (vers "0.9.0") (hash "0i2mwzj69vqm4256g9j048hl7849ccsl5zvwrxypp6q6jdw3d8i3")))

(define-public crate-truetype-0.9 (crate (name "truetype") (vers "0.9.1") (hash "0l31d86y67mzz1d2sgz812pvcw2nn68hcx5viihcldj7wvqi9vsy")))

(define-public crate-truetype-0.10 (crate (name "truetype") (vers "0.10.0") (hash "1y28xjm95wisx8dp0x3ymgg649nd4hwsxi53lpa5wcnr8hn8rp88")))

(define-public crate-truetype-0.10 (crate (name "truetype") (vers "0.10.1") (hash "16h8fq9k4qyvdhwr6518fgpn05r46pwxidzikdhw24lxqr79fp51")))

(define-public crate-truetype-0.10 (crate (name "truetype") (vers "0.10.2") (hash "143jh8b7r9vrwkvgpr2zc9fs3a5j4c2mxhs2rnkbhz92vcc8w8k4")))

(define-public crate-truetype-0.10 (crate (name "truetype") (vers "0.10.3") (hash "0m82vqxgn9lsxdkbznwafp2sbs0kx20y139s7ilkcy7gf57a0hd7")))

(define-public crate-truetype-0.10 (crate (name "truetype") (vers "0.10.4") (hash "0vla46c5m4rnk170cdnz6i1av58hcdm95y0pr5ammym721kmmniw")))

(define-public crate-truetype-0.11 (crate (name "truetype") (vers "0.11.0") (hash "00db5c98facbvl66dskr0h2sjaq3ci6cg77098pc08grv2qll1v1")))

(define-public crate-truetype-0.11 (crate (name "truetype") (vers "0.11.1") (hash "1gzic1356bp0cmjghd98g4rzs2j04v0gl2cd8f46dy0lk7f1dzq9")))

(define-public crate-truetype-0.12 (crate (name "truetype") (vers "0.12.0") (hash "1n5pck0nrnvyz5hgr6f1bdx657x9zzmg96yjdr614i54ramqzbvp")))

(define-public crate-truetype-0.13 (crate (name "truetype") (vers "0.13.0") (hash "1gfjpdwm21yj6lkvs0jzq79vz2hrwm5jybpxzrlz1bm12l838vi7")))

(define-public crate-truetype-0.13 (crate (name "truetype") (vers "0.13.1") (hash "0s8yrv59gal5ax5yzxxsn1xr19dnr82gzbhdxkb8pz6x06ymyccj")))

(define-public crate-truetype-0.14 (crate (name "truetype") (vers "0.14.0") (hash "1gycs2q951xl06cfk94nz0bzb3pkm0masls1a3mjfqhdjff6isqj")))

(define-public crate-truetype-0.14 (crate (name "truetype") (vers "0.14.1") (hash "0z6san40z36z2gsa0hgqfqbybqschyh8k26f79yjq4d440jffarj")))

(define-public crate-truetype-0.14 (crate (name "truetype") (vers "0.14.2") (hash "1m1djgv1lghm9w2vc9z779rmyvmpkvi1n5ns2vk8524z8jk77ymd")))

(define-public crate-truetype-0.14 (crate (name "truetype") (vers "0.14.3") (hash "0i87pj48alb0bfgsni66q1d02zbibyyhk6qffmw67pbpvyypm9hm")))

(define-public crate-truetype-0.15 (crate (name "truetype") (vers "0.15.0") (hash "00llwrdgmb9q73f38lj5xsim3v818qiy3p3ja5wdsiiaqyzqkjz5")))

(define-public crate-truetype-0.15 (crate (name "truetype") (vers "0.15.1") (hash "0849f8b883ifjw2vy59dxr5fnkxsvpx89x983ys23bzm90jdlqfn")))

(define-public crate-truetype-0.16 (crate (name "truetype") (vers "0.16.0") (hash "1k1hqk9sj936m4c01yjb6qjrd3j1j7dpws5wwhq42p73slm8asvw")))

(define-public crate-truetype-0.17 (crate (name "truetype") (vers "0.17.0") (hash "0gw7kh8x8snfjpsy5pf8jj5cz6ndlvi3wbb5qmnsiy8d2k9nzhms")))

(define-public crate-truetype-0.18 (crate (name "truetype") (vers "0.18.0") (hash "133pknwdfzgj1kvp0nvwdnf0n920s0536fs4galcwqh24yqvizzn")))

(define-public crate-truetype-0.19 (crate (name "truetype") (vers "0.19.0") (hash "1ag6a1qhxxg8pz586l60srk1610hhpwzh19dbpjvl2jz8sy84jsl")))

(define-public crate-truetype-0.19 (crate (name "truetype") (vers "0.19.1") (hash "0l59bbv0dhw4gzvmwbxlpdq460hqc3rl693pk6gd2k4rcdz7jia3")))

(define-public crate-truetype-0.20 (crate (name "truetype") (vers "0.20.0") (hash "1v49z3vbgzjkrjq31h448h5azv8kf7mqhmvi62i197hiwa9fkfa3")))

(define-public crate-truetype-0.21 (crate (name "truetype") (vers "0.21.0") (hash "1rslzasq5k2qd8y5d3km04lmv3pczj199wskzml7lprg85psmb08")))

(define-public crate-truetype-0.22 (crate (name "truetype") (vers "0.22.0") (hash "15997d2kcnxnxlclvg2xdna13lx4h1li8bp1qfydsfz1fhwvcxjr")))

(define-public crate-truetype-0.23 (crate (name "truetype") (vers "0.23.0") (hash "0i0zm5lfazmg412y2ymnhx83czymymv2iz00ix19hfmh6zgjpkn9")))

(define-public crate-truetype-0.23 (crate (name "truetype") (vers "0.23.1") (hash "1vggxcmpchy3ydxi4m4998a1par4ncirz514is0vic5khmgx3192")))

(define-public crate-truetype-0.24 (crate (name "truetype") (vers "0.24.0") (hash "1pdw08ddplwpjys3d6cqfkiixh3ipgi1sxhn9mhmywhldmypq5l7")))

(define-public crate-truetype-0.25 (crate (name "truetype") (vers "0.25.0") (hash "0rk77cig801pw9lkv6rjahvnbqk6w7aczm67cr5d3d0j02sgn7jr")))

(define-public crate-truetype-0.26 (crate (name "truetype") (vers "0.26.0") (hash "0xxqxacp3b9m5jn1m55ych31bsfbns35fqhsvk4xmmik0qsk1v5c")))

(define-public crate-truetype-0.26 (crate (name "truetype") (vers "0.26.1") (hash "0vhv2nxrr5gpjisf17032g60s4k0fxry29psprs7qdpynw2j5lx6")))

(define-public crate-truetype-0.26 (crate (name "truetype") (vers "0.26.2") (hash "1v1gisnn1vb3122a56phadhygpc527827vzbzz89kjkr5515fiba")))

(define-public crate-truetype-0.27 (crate (name "truetype") (vers "0.27.0") (hash "0dk17zr4iq8f51bq15x6kxxqq5fjhmzisy5hgbz1gkggprkiqzki")))

(define-public crate-truetype-0.28 (crate (name "truetype") (vers "0.28.0") (hash "0qxm51mjy9qgw0b76s3msxk1q4lh7phz3bblgly7vfvlsy49b3a7") (yanked #t)))

(define-public crate-truetype-0.29 (crate (name "truetype") (vers "0.29.0") (hash "11ig46q9n4vz9rrsk3jsh772yi7qybndvn7wahwdvsfbhl749r0a")))

(define-public crate-truetype-0.30 (crate (name "truetype") (vers "0.30.0") (hash "0pinknsv47av2lk4mssj71y9b0w5h1xhlandq0y82i9jlgyykgca")))

(define-public crate-truetype-0.30 (crate (name "truetype") (vers "0.30.1") (hash "0c2pj8lqchb04kpcz156i3bqy2ykhh51i3wwcdz6fmky3b7sffh6")))

(define-public crate-truetype-0.30 (crate (name "truetype") (vers "0.30.2") (hash "0nws0962axfqggr5zs4piacn2768kqd6nmawq0c68kjpjqkqw20w")))

(define-public crate-truetype-0.30 (crate (name "truetype") (vers "0.30.3") (hash "1pj4xwq0a6myhqzk8rq3rjkg6c6bj1zk9fvf9m37h5hy85vqgwq4")))

(define-public crate-truetype-0.31 (crate (name "truetype") (vers "0.31.0") (hash "1vb6dl9pzl26rjhm8n7adf2wywv8r6ri2n7zg52dz4kmfnprwv41")))

(define-public crate-truetype-0.31 (crate (name "truetype") (vers "0.31.1") (hash "08yn5m2ys914m7siqi5h7nfr7yp9jmw4h35h15n9hiacah3hhcmz") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.32 (crate (name "truetype") (vers "0.32.0") (deps (list (crate-dep (name "typeface") (req "^0.1") (default-features #t) (kind 0)))) (hash "0anx5nwmgrffbyqwqxnjplp4hgy8vlqjhkzva3s97j64gv09g9aq") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.33 (crate (name "truetype") (vers "0.33.0") (deps (list (crate-dep (name "typeface") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pp3i5yfzcsffi4qa52bxdxid6aga6qv1crg73204x4sc5c021jg") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.34 (crate (name "truetype") (vers "0.34.0") (deps (list (crate-dep (name "typeface") (req "^0.1") (default-features #t) (kind 0)))) (hash "0r97ias0p9s0ffw0sg7w5a36cbym73h1a9spia7ifgknwgr4skjv") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.35 (crate (name "truetype") (vers "0.35.0") (deps (list (crate-dep (name "typeface") (req "^0.1") (default-features #t) (kind 0)))) (hash "075y7dfm0fxd1k9vh0fdwf98bcx93kp6wx9km9grpmh5jybpk3wa") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.36 (crate (name "truetype") (vers "0.36.0") (deps (list (crate-dep (name "typeface") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fzzqfip995fg7lq5c4p1qwqnrqhz9ln00hah8qnvksy5286n8l0") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.37 (crate (name "truetype") (vers "0.37.0") (deps (list (crate-dep (name "typeface") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m9i8fzxh12c7wk3xgzwxc21cyhhz150wmw678l03yh5nhrlyhzm") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.38 (crate (name "truetype") (vers "0.38.0") (deps (list (crate-dep (name "typeface") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p4qy1949l9cs97jdrwv4yy97r3zmjidq084yyskm2459q7q5rns") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.38 (crate (name "truetype") (vers "0.38.1") (deps (list (crate-dep (name "typeface") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ivmha62dp1bbk580xp8lfrhdpfnqxjq6msm3dghsjwwfl65v81j") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.39 (crate (name "truetype") (vers "0.39.0") (deps (list (crate-dep (name "typeface") (req "^0.2") (default-features #t) (kind 0)))) (hash "1056aqd26ca5gvkj20dydnnl1w75604c46vgjnwpfadwzm0kfb00") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.39 (crate (name "truetype") (vers "0.39.1") (deps (list (crate-dep (name "typeface") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i0zc9q5j9g94z6c0fh5mgj4gggnbyv2bb56rzi0brc6rw87gz8p") (features (quote (("ignore-invalid-flags"))))))

(define-public crate-truetype-0.40 (crate (name "truetype") (vers "0.40.0") (deps (list (crate-dep (name "typeface") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1k83khnqljxcpvydb6hqaa3ny52xfgw4j9ki584g7fr14ypqlqkw") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.40 (crate (name "truetype") (vers "0.40.1") (deps (list (crate-dep (name "typeface") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "05sxnh62k0hqd7yzvlqxh7xaa5cvlrnly6fvnszzvvalgr94klyy") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.41 (crate (name "truetype") (vers "0.41.0") (deps (list (crate-dep (name "typeface") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1r8fx11js9dr3p6sahvhbph510k5kqrll1nm8czhsl7m661yl8cm") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.41 (crate (name "truetype") (vers "0.41.1") (deps (list (crate-dep (name "typeface") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1ad7cg73rja3wflg7clsvxlbki3ysdin812k2w43n2k9l2j9mr0q") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42 (crate (name "truetype") (vers "0.42.0") (deps (list (crate-dep (name "typeface") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0z6p7ckmlix44j8j88lximiavqfiq6gzx87lsm5gg4lcbr95v95n") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42 (crate (name "truetype") (vers "0.42.1") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1lihqs1z6xap65bzpaisc0iq6i39c18hs9avf8qbvnxx2kc0z8lf") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42 (crate (name "truetype") (vers "0.42.2") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "05ph8hmmjfnm14f95gfvv7z4mrh5wggcskkdz5zx7nmmdj3f9vng") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42 (crate (name "truetype") (vers "0.42.3") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1n776xsldgmdysj50aynpxsdcym8rl8qg0033di9llr9dms769sp") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42 (crate (name "truetype") (vers "0.42.4") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1f0dz8igb404mgmz3nj6qybxn9khk51z2yswr9684chkp0pixhyb") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42 (crate (name "truetype") (vers "0.42.5") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "16zy8nhf4ibck4488pr28rwjy942a8i62lf9b2vc20p99k082b7z") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.42 (crate (name "truetype") (vers "0.42.6") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "02qhqb6gfkx1afby4k93db3rg761jh7p84xcik7k462syf52glcq") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43 (crate (name "truetype") (vers "0.43.0") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1ma539sm58hd7hgwl155q0qzz0vp9dhijdp0flglgacb5y8vjdz7") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43 (crate (name "truetype") (vers "0.43.1") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yy1w7c41blkqmnk345hdaxb5pcr1a5slw5m1d06y4ilh8ml6mz3") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43 (crate (name "truetype") (vers "0.43.2") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1mh44ig7jgkl0zxrh7rrjpdmwwqy153xba781k80c8pji83ncsnx") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-composite-glyph-flags"))))))

(define-public crate-truetype-0.43 (crate (name "truetype") (vers "0.43.3") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1fxvrpglq1ihyq1scqjqcv5hccyh1c25gwyi3nc30v3h2kr8ipyi") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.43 (crate (name "truetype") (vers "0.43.4") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19anyy8av8j3kx5nvvs2jn65k2k3swhkd1nsmcdq1if8n7jqn4sk") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.43 (crate (name "truetype") (vers "0.43.5") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19417jyw7jvfdpk0ph9vsh49zwm3aqrfyqkjlhx8snjh3x07f7rc") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.44 (crate (name "truetype") (vers "0.44.0") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1icbasz5jyfypqs0nzxza2m2xybhklxvi1zp6ffw0y82g5vplqiy") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.44 (crate (name "truetype") (vers "0.44.1") (deps (list (crate-dep (name "typeface") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0y1r1fq6pjrqz52y71dzvmanbm8y3p8svjvsslcwcqh3p85kxbw2") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.45 (crate (name "truetype") (vers "0.45.0") (deps (list (crate-dep (name "typeface") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1l6x932850jl51dynnp4w19vkn1rdyagym7vms2hja7ha80vp4l1") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.45 (crate (name "truetype") (vers "0.45.1") (deps (list (crate-dep (name "typeface") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "06h0yj0xdl1cnvnhirk0gfxw4icw2iwfc879q6micpdz765x1gcm") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.46 (crate (name "truetype") (vers "0.46.0") (deps (list (crate-dep (name "typeface") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "10w1kqgpfsx6jr13prdxbva7wbc1sdwkqwxqy3fynj5sxjfrdlxz") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47 (crate (name "truetype") (vers "0.47.0") (deps (list (crate-dep (name "typeface") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1xwwz3bylz8hlcmvdzhfjdzpkapx64q5dgrw6m202jqpvbjarkla") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47 (crate (name "truetype") (vers "0.47.1") (deps (list (crate-dep (name "typeface") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "13c5crp63c73jh3rc79l5d5qpvfilm3bdgmbiamjckkc6wnmh0cs") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47 (crate (name "truetype") (vers "0.47.2") (deps (list (crate-dep (name "typeface") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "123aiidjz3b394kbcj9nw37zxq6n57m1gf6fzdi73y3x25ckdyva") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47 (crate (name "truetype") (vers "0.47.3") (deps (list (crate-dep (name "typeface") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "05r49npxgbj3qr61g3gcl89zmxgp00l8dj2jj59fq35m430ivwnf") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47 (crate (name "truetype") (vers "0.47.4") (deps (list (crate-dep (name "typeface") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "01c2wm8r4pqasrnwvvphpj43v47nb201d58h9xygpzaii4bvs2zq") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags")))) (yanked #t)))

(define-public crate-truetype-0.47 (crate (name "truetype") (vers "0.47.5") (deps (list (crate-dep (name "typeface") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "12r6ysby7pr62rch7151wzgdd9vrkf9p9haghjmrk84yzkvdq9ps") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

(define-public crate-truetype-0.47 (crate (name "truetype") (vers "0.47.6") (deps (list (crate-dep (name "typeface") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "192c78vggdvzxsx9ilb9wnmg12adz17as501h7jk527fm2w8gf0g") (features (quote (("ignore-invalid-language-ids") ("ignore-invalid-embedding-flags") ("ignore-invalid-component-flags"))))))

