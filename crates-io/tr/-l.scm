(define-module (crates-io tr -l) #:use-module (crates-io))

(define-public crate-tr-lang-0.1 (crate (name "tr-lang") (vers "0.1.3") (hash "074wxsdw3bkri3l6mn74w46p6pch2v716vsggcfwr5g24xj1aa0w")))

(define-public crate-tr-lang-0.1 (crate (name "tr-lang") (vers "0.1.4") (hash "10n9dd5y02znxriq1ijhjhyw9vbld8d73r0s0in92c43b3fipxds")))

(define-public crate-tr-lang-0.1 (crate (name "tr-lang") (vers "0.1.5") (hash "06bphh32cxncykz37xsi35dyrgwkhxk465a2shaa0b45ly599hsq")))

(define-public crate-tr-lang-0.2 (crate (name "tr-lang") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0z246ir0jyii06sipcw48zin1rn09bfc6fsb8f528n4y2jnbbmz2")))

(define-public crate-tr-lang-0.2 (crate (name "tr-lang") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0s1npg8jr6mbdhw4ms0hf2ckb8jmba9px9yfn342xvm7yd5j6c3h")))

(define-public crate-tr-lang-0.2 (crate (name "tr-lang") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0s4w2wn75lqsjsshajlfdmlrld99qgxkc2w3sm07midi0v7mypl8")))

(define-public crate-tr-lang-0.2 (crate (name "tr-lang") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0718qp4q00jim1cf3qzckja61p0q5qnlkrkjqqw5yaw999vs3zsf")))

(define-public crate-tr-lang-0.3 (crate (name "tr-lang") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18yy0414c80xdms0clrm6smpzhkxp8wgk1llvbgwqmivxlvzq47m")))

(define-public crate-tr-lang-0.3 (crate (name "tr-lang") (vers "0.3.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11bryhsqp7icppmdkfpmsl5g1bc8b1b2z2bdsa77466mg6zl74b7")))

(define-public crate-tr-lang-0.4 (crate (name "tr-lang") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "locale_config") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^9.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1imx1xxhci3f02a0x0pmdw8n767q1pnq3v359zc8p3jiz22lr5jh") (features (quote (("interactive" "rustyline" "regex" "lazy_static") ("default" "interactive"))))))

