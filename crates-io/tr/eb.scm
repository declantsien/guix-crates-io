(define-module (crates-io tr eb) #:use-module (crates-io))

(define-public crate-treblecross-0.1 (crate (name "treblecross") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "lib_treblecross") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "059s5v385c3xw0vfj5qj9mdl0bq5vw1cw7i3kwn98i8zpm9j6b32")))

(define-public crate-treblecross-0.2 (crate (name "treblecross") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "lib_treblecross") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dhg2ir7bvayfslk5cpz8jhrijsllq3mrl81h1f6zwvycnp85jwp")))

(define-public crate-trebuchet-0.1 (crate (name "trebuchet") (vers "0.1.0") (hash "0sl8f5sci1w656b7cmicpj0c3n7cfrw3bbp89kg9vnyh7yzmy502")))

