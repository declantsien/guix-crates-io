(define-module (crates-io tr ec) #:use-module (crates-io))

(define-public crate-trecs-0.1 (crate (name "trecs") (vers "0.1.0") (deps (list (crate-dep (name "trecs_proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16c30kvm9nf74n574p4x7gdlwicrlkq4c4fqlclqqr2ija7f8gy2") (features (quote (("system" "trecs_proc/system") ("default" "system"))))))

(define-public crate-trecs-0.1 (crate (name "trecs") (vers "0.1.1") (deps (list (crate-dep (name "trecs_proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0si54h3a9ccbsh764zxq0162frch5if77dbyi212n9lasb5iqvv0") (features (quote (("system") ("default" "system") ("async"))))))

(define-public crate-trecs-0.1 (crate (name "trecs") (vers "0.1.2") (deps (list (crate-dep (name "trecs_proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jjr42h7jh2mz2068730bv9wzx31k62yszqclr1dli74bnfd1gp0") (features (quote (("system") ("default" "system") ("async"))))))

(define-public crate-trecs-0.1 (crate (name "trecs") (vers "0.1.3") (deps (list (crate-dep (name "trecs_proc") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0r9rih584i139g796m2a713g8ji83dwm8vrrs4n82c6534hpx9c1") (features (quote (("system") ("default" "system") ("async"))))))

(define-public crate-trecs_proc-0.1 (crate (name "trecs_proc") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1liign9lj13fr2l0a27dibi38yj1swlpsdpg4ccqgsn3srswsdy0") (features (quote (("system") ("default"))))))

(define-public crate-trecs_proc-0.1 (crate (name "trecs_proc") (vers "0.1.2") (deps (list (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jg2klj4dy9gqipsl5jzm44lmzb914h23cfl79xagw6iz6wdk2zx") (features (quote (("system") ("default"))))))

(define-public crate-trecs_proc-0.1 (crate (name "trecs_proc") (vers "0.1.3") (deps (list (crate-dep (name "syn") (req "^2.0.20") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z4lxwgd1j8jqfr7m4n23gfw85wpv875fk52q0q5n2s9siyjphid")))

