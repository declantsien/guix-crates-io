(define-module (crates-io tr ol) #:use-module (crates-io))

(define-public crate-troll-0.0.1 (crate (name "troll") (vers "0.0.1") (hash "095c8vmwb6jp31bw3iv6axcajqxm5rmw3pms4z0lkmd97bs19pxa") (yanked #t)))

(define-public crate-trolling-0.1 (crate (name "trolling") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0") (default-features #t) (kind 0)))) (hash "0q9cwz40wm4slv5g6zl5f5yp964vsfm6h04jh8vvphmb47vdv7bq")))

(define-public crate-trolling-0.1 (crate (name "trolling") (vers "0.1.111111111111") (deps (list (crate-dep (name "reqwest") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0") (default-features #t) (kind 0)))) (hash "145sny26h34pr6fwy4kzlgzy4yfrwvya9pvifyjkdqgkzck580jy")))

(define-public crate-trolling-0.1 (crate (name "trolling") (vers "0.1.11111112223621") (deps (list (crate-dep (name "reqwest") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0") (default-features #t) (kind 0)))) (hash "193h7wjydw7apf4xc4025cwp6afhbfyrn6yd7ixncr29q7hm7a86")))

(define-public crate-trollus_guessing_game-0.1 (crate (name "trollus_guessing_game") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1z6ghgmj641d1h8v49ncvy04gzm553301dpffp2a3vy1yn69cpkg") (yanked #t)))

(define-public crate-trollus_guessing_game-0.1 (crate (name "trollus_guessing_game") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0j09yhg0c8dwhh66w8gdyzlply4lpkyla9i2w2kihgfpvm395b2r")))

