(define-module (crates-io tr ev) #:use-module (crates-io))

(define-public crate-trev-0.1 (crate (name "trev") (vers "0.1.5") (hash "0x67rj10xhwd5f6waj9b1wnnivh40inad37dp93mpcyr5y8kazkn")))

(define-public crate-trev-0.1 (crate (name "trev") (vers "0.1.6") (hash "15zlq0m86mjqy7rgh76csvjhqshdl7iczkqg1qcimlbifcz4a645")))

(define-public crate-trevor-0.1 (crate (name "trevor") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "166jx6xcih6ia9jccnpw2zb9hhsx56j4kh5a7c7syzzvxbkqygv6")))

(define-public crate-trevor-0.1 (crate (name "trevor") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0y1h52b39jrkm8fz9szv7yaq9xlqgm5701jir53b63sp4n7vcqhn")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.0") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0cwyb9054jpaqqm4jjb13k296nmxli79iypzq5iiymdyz3p6rqs3")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.1") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "115x06w3yi2yvi5y82vnlph7li2jzsczxfgk91sgvnlrrq77zyd7")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.2") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "029asn4kxhv7y8j27h1ka93p4l9vxsdq233ia5a03ls8ag9psnil")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.3") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1v5axkz03sfy1izkw471jwf8ih0dk4m4zd1ydxdmrv82ynbg5hrs")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.4") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "17xg38qjnjzfm5xgkksc2jn0z58pw6g4k71yf9ck8bl933s29l04")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.5") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0kyqx2yh1zarwhiwb9db8dxy246fxklfqjbxvak34mm9wnj4i6xg")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.6") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "16cg4ap0v8581kx3k7i0c65bi8d00ski72ayq8vwhzcg2ib0z8v5")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.7") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "10c7cav9m8j8lk4j3n94s89yv44bqdb7ipx1q377ljzmdfprdqrx")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.0.8") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0m016dk3ylq0i79p9mnz5dlwldkadxqbi05lcd25ipzd4yr7ww4w")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.1.0") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "19pcq0nyxzpn5md0hg3y0kqvrqh4filf277a66p3chmg50aivs2p")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.1.1") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1riq4fsbk96sx16q74fbwmyab25q5j8idbx4wqncav336giyi1py")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.1.2") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "19fcn75rfgirmspqaiw34dwrh6n1rag54lr0cnmq69si7ikjz1qr")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.1.3") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "04xx4h4i2cw0bikxy3zw7cnzwcvk10fd41ihipdbgbfbn204dsrp")))

(define-public crate-trevordmiller-1 (crate (name "trevordmiller") (vers "1.1.4") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0i3kbrcs6l2ksswv4v3wwr10hi2b3msf80ljbfi7xyyqnsriynna")))

