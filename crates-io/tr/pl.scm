(define-module (crates-io tr pl) #:use-module (crates-io))

(define-public crate-TRPL_14_02_art-0.1 (crate (name "TRPL_14_02_art") (vers "0.1.0") (hash "0mmb1svvdjajnskgvr3wcbjlgz9lgvy0kbd4lrjk0hi0xfgvl7nm") (yanked #t)))

(define-public crate-trpl_cain-0.1 (crate (name "trpl_cain") (vers "0.1.0") (hash "1yv18350k073wjf8gz93pyw8wjsnsh0vykgggimp40ppvc4amvbc")))

(define-public crate-trpl_cain-0.2 (crate (name "trpl_cain") (vers "0.2.0") (hash "0r82g1xygixp390x1y52smnjwq1w5vxk9y86ac6w55y8zfzqpbg9")))

(define-public crate-trpl_commons-0.1 (crate (name "trpl_commons") (vers "0.1.0") (hash "00hx1lh2naxqwsdav7jrvqbvs80yk9wdciygr2wd4pw00sgj5qdf")))

(define-public crate-trpl_dv-0.1 (crate (name "trpl_dv") (vers "0.1.0") (hash "0wnmmp5vg72cgx6afbnzp6z5a1pbixd902c3h0gjizwa6hg4dj1l") (yanked #t)))

(define-public crate-trpl_dv-1 (crate (name "trpl_dv") (vers "1.0.0") (hash "1zbbbr5x2zv8xy5ddkcgiqflh1z6ra97744ah413i32lh5azja3g")))

(define-public crate-trpl_tut_sorry_for_the_bloat_need_to_learn-0.1 (crate (name "trpl_tut_sorry_for_the_bloat_need_to_learn") (vers "0.1.0") (deps (list (crate-dep (name "art") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1v9vnjzqb3szz9dscp913zzfvf7qhh0xd8lv01gd1rbdfg94xll6")))

