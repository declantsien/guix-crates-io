(define-module (crates-io tr n-) #:use-module (crates-io))

(define-public crate-trn-pact-0.1 (crate (name "trn-pact") (vers "0.1.0") (deps (list (crate-dep (name "bit_reverse") (req "^0.1.8") (kind 0)))) (hash "0l8f8r4x5pl17cdm4lyzdww61bk0hqirkc4yfqdplmzfrcwqp7iw") (features (quote (("std") ("default" "std"))))))

(define-public crate-trn-pact-0.2 (crate (name "trn-pact") (vers "0.2.0") (deps (list (crate-dep (name "bit_reverse") (req "^0.1.8") (kind 0)))) (hash "0wgarf3n9mah2b9fxwrsvs3880p5jbkkknv3jb33awn6qg749566") (features (quote (("std") ("default" "std"))))))

(define-public crate-trn-pact-0.2 (crate (name "trn-pact") (vers "0.2.1") (deps (list (crate-dep (name "bit_reverse") (req "^0.1.8") (kind 0)))) (hash "15p0fzcyb5x1r7v1ivdpjwi24fxif2lhlah0i1n6y6xa5sw0hydz") (features (quote (("std") ("default" "std"))))))

