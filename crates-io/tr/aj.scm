(define-module (crates-io tr aj) #:use-module (crates-io))

(define-public crate-trajan-0.1 (crate (name "trajan") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.16") (default-features #t) (kind 0)))) (hash "0gjy3j83dinm1f1f0nqbcyznbgs74iymljs6nzlqykjl978727rl")))

(define-public crate-trajectory-0.0.1 (crate (name "trajectory") (vers "0.0.1") (deps (list (crate-dep (name "gnuplot") (req "^0.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "00idi984nhh2d38cg35mw87ngyv4bglij2jcn4bl4awzvpgfrrlx")))

(define-public crate-trajectory-0.0.2 (crate (name "trajectory") (vers "0.0.2") (deps (list (crate-dep (name "gnuplot") (req "^0.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x8zp5gwwq8jxqpz3xfr15xvgy5gs4y8m53ackwsggmfvvnsz9jm")))

(define-public crate-trajectory-0.1 (crate (name "trajectory") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jisqxlj0444jipz8s7zpqs5y55is6z17fl6fqwzhwa039x1ka7k")))

(define-public crate-trajgen-0.1 (crate (name "trajgen") (vers "0.1.0") (deps (list (crate-dep (name "assert") (req "^0.7.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29") (default-features #t) (kind 0)))) (hash "0qazz2qq54bv7jy3a1xykaib456azil95wa3y868ab9j50i2wwa6")))

(define-public crate-trajgen-0.1 (crate (name "trajgen") (vers "0.1.1") (deps (list (crate-dep (name "assert") (req "^0.7.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29") (default-features #t) (kind 0)))) (hash "0kwab84qln5av6nk47ifmwkpvj21jhfys59adx2i784rs3p7c79x")))

