(define-module (crates-io tr ex) #:use-module (crates-io))

(define-public crate-trex-0.1 (crate (name "trex") (vers "0.1.5") (deps (list (crate-dep (name "ansi_term") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0lxhb3khswa9b1cag9jgyr36ig82afnni1ha9xhpkjb32m6l28w9")))

(define-public crate-trex-0.1 (crate (name "trex") (vers "0.1.6") (deps (list (crate-dep (name "ansi_term") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1.15") (default-features #t) (kind 2)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1scw3hxzn68c912m4zk6b0wgad6a6s5k0gccch4qkdp55c574m57")))

(define-public crate-trex-0.2 (crate (name "trex") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "08kc0qynkp5zmdg4bviib64s0w34w3lxf8z2qly959wyaqv2sjia")))

(define-public crate-trexio-2 (crate (name "trexio") (vers "2.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "16sglihniv2lspvz3hv7sz82xf5m55lc3zd0ql35kcanzxjgpcc1") (yanked #t)))

(define-public crate-trexio-2 (crate (name "trexio") (vers "2.4.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.27") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "0b77ln4nl04af0x14xwi7khg47111c1b727rqvjaska10hahrgk0")))

(define-public crate-trexio-2 (crate (name "trexio") (vers "2.4.2") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "hdf5") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "rustls-tls"))) (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "0vsrqvlhqfdsqd432kq9zcscyldp0mzli32w2k2sw6vmbwdqbb5x")))

(define-public crate-trexter-0.1 (crate (name "trexter") (vers "0.1.0") (hash "1bcj5ajpncvmx55rljg5dxsh1ciwmld4g121sslk6k2f5mkiv2rw")))

(define-public crate-trexter-0.1 (crate (name "trexter") (vers "0.1.1") (hash "1j8yc7advsarbzf1zb1naz0489sbx49zsr86n21bz8jh14c66lwr")))

