(define-module (crates-io tr ey) #:use-module (crates-io))

(define-public crate-trey-0.1 (crate (name "trey") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)))) (hash "0wascpy2iz1cxasn0kwn1ypr3d0l9b9xd1alb6lqgmqpfhfyhavc")))

(define-public crate-trey-0.2 (crate (name "trey") (vers "0.2.0") (deps (list (crate-dep (name "lyn") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)))) (hash "1p2y6w2ablzi6hirnxvbsja3lwai7jny1h8zn947rfxdlh5yywqz")))

