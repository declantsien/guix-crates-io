(define-module (crates-io tr fr) #:use-module (crates-io))

(define-public crate-trfr-0.1 (crate (name "trfr") (vers "0.1.0") (hash "1rlqyz2plx3ci7hlfmm47rk0qdnjbbvzpaj9p9mhx6fn1mcmw99n")))

(define-public crate-trfr-0.1 (crate (name "trfr") (vers "0.1.1") (hash "00qrpr2xjzcximzlnl4s4f0caimcm6208gj8f1lylms09v0ik7r6")))

(define-public crate-trfr-0.1 (crate (name "trfr") (vers "0.1.2") (hash "1z308p1gj0jxwa2zlz7im6lkj2wyi1kic3156q3g4js7gd357kmb")))

