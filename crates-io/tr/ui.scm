(define-module (crates-io tr ui) #:use-module (crates-io))

(define-public crate-truid-0.1 (crate (name "truid") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "httpclient") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "06kj2szxzb7crqxq2s41lzqi23ryssqc75qq808rknbb3agbnmz4")))

