(define-module (crates-io tr ef) #:use-module (crates-io))

(define-public crate-tref-0.1 (crate (name "tref") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1qkhsisqll3x0ipfz9k2q53k7ms6valv74vbj4148a48gig9wfqg")))

(define-public crate-tref-0.1 (crate (name "tref") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "10svzfgvgxgh0nc5206zfm42sfhjzwbphyhg45dww0sx9gbj5242")))

(define-public crate-tref-0.2 (crate (name "tref") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "100w4h3kxbnwz5qd13q742lnmda296rs7ydvihqsz7ka9xrlnmw0")))

(define-public crate-tref-0.3 (crate (name "tref") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "socarel") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1pc85gzq50b5226i2nq5vqi8nbc2l3b7x7ljyinnm1qdg70bca0f")))

(define-public crate-tref-0.4 (crate (name "tref") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "socarel") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0dcqjqam8s84chkgdx7qna78asmy2f1c05y6q507ndpq5j86117w")))

