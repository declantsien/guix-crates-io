(define-module (crates-io tr eg) #:use-module (crates-io))

(define-public crate-treg-0.1 (crate (name "treg") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.56") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02j1np9b8ihbwwmqpwg8a3q9ppycndiyimy05jnbwy9sbny7d6rq")))

(define-public crate-treg-0.1 (crate (name "treg") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.56") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1766cnx7aa4axpjjrdmd805pvmkp7xy3mqmfl4xzz5jiq10nr7c3")))

