(define-module (crates-io tr yr) #:use-module (crates-io))

(define-public crate-tryrun-0.1 (crate (name "tryrun") (vers "0.1.0-rc1") (hash "1wsmcc410kf14y5yywmaa1gcjh0sgldkra3r5vnw594g2llnh2z5") (yanked #t)))

(define-public crate-tryrun-0.1 (crate (name "tryrun") (vers "0.1.0-rc2") (hash "0a265a53ff2j786ravwzrghswprvmqsw5as4qmwk53wfxdycrjv4") (yanked #t)))

(define-public crate-tryrun-0.1 (crate (name "tryrun") (vers "0.1.0") (hash "1czyyc2xzsjwkyvkrjrw892lliv4hyyjy6aq644n2yhds7avl15f") (yanked #t)))

(define-public crate-tryrun-0.1 (crate (name "tryrun") (vers "0.1.1") (hash "0xi9kiqsbldgp1im3hyagnr3mqb82hpnxrk9rv4i75fmbypajr9q") (yanked #t)))

(define-public crate-tryrun-0.1 (crate (name "tryrun") (vers "0.1.2-rc1") (hash "17rpixrg32pf4cmfg8pcqqyz71xsdiqc6h7ja4s0bzv4wh2v0gay") (yanked #t)))

(define-public crate-tryrun-0.1 (crate (name "tryrun") (vers "0.1.2") (hash "1697kxv83n5p23lpbwxjl88v97ai0a3gabg1izp3bzrk2ppna6d2") (yanked #t)))

(define-public crate-tryrun-0.2 (crate (name "tryrun") (vers "0.2.0-rc1") (hash "030pzkxdh1f17wjhykz4b77vql5s5r0pmvx5pd1rfbna2xz8570r") (yanked #t)))

(define-public crate-tryrun-0.2 (crate (name "tryrun") (vers "0.2.0") (hash "1cpvmcban9c2qr1jwr4xmxs907wkcb369xc9pvz0ri5bwf568ych") (yanked #t)))

(define-public crate-tryrun-0.2 (crate (name "tryrun") (vers "0.2.1") (hash "1h9hky6mzpj8pwy3z6havrfl07qdcrys6xk6i95rmgvsinvfspw5") (yanked #t)))

(define-public crate-tryrun-0.2 (crate (name "tryrun") (vers "0.2.2") (hash "0s2gsi2c5bjjaizdiqpw1plnf5sj9326dyhhzw1076nkvjmzr9gd") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.0") (hash "0y11brqxc8c04mc0la7jif3nk2wc2yk2dvbjyzr1vh84d8wwksbg") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.1") (hash "1rl5x8rcaq2qcvaf0h6ms5z0dn54cqkma2h02dnp6d65aqfpi3cy") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.2") (hash "0m78csqkqk7zxcrdh0a848jczd3xyrayz97fqhwliiq4ygvk2ldr") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.3") (hash "023jpf4qzsqvy1kcrniqh4izp8rwnj5jp20jsnm9arcv6xh9081k") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.4") (hash "1l14i3a6vhyvhy6dkl9zi0a8dziqm01cq79j39sm9i0gqvkf4gnp") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.5") (hash "19bfigy1py02xgz4sa4qp6zpzvcyppy1kgp5w0macwlxljs8ci1y") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.6") (hash "0blz6hl6wa05wqbmwssfplkxslh4xwicrqc98v82768mc4vpqldg") (yanked #t)))

(define-public crate-tryrun-0.3 (crate (name "tryrun") (vers "0.3.7") (hash "110kffsqwkw57lnsmlbl381bpvb9fgwwzfqjp11cdkisdd496iwg") (yanked #t)))

(define-public crate-tryrun-0.4 (crate (name "tryrun") (vers "0.4.0") (hash "0avc3j14i7hgkglhs1nrbbwkqg40phxafibfl64arqv1hx0ja6dn") (yanked #t)))

(define-public crate-tryrun-0.4 (crate (name "tryrun") (vers "0.4.1") (hash "0swvg97wdlj2cg2yx8jzwrr2jl4shkr2yybgd7v2krr4ra29gazb") (yanked #t)))

(define-public crate-tryrun-0.5 (crate (name "tryrun") (vers "0.5.0") (hash "0csqxc3pq7zrcv3n2yqnk9mbhjh99jdz9f0cg6h19syzjknl0jky") (yanked #t)))

(define-public crate-tryrun-0.5 (crate (name "tryrun") (vers "0.5.1") (hash "0hjk5mdqd098nmhg65vfkq1fahr2p94pxm6pg7crg35iv3rcdr5i") (yanked #t)))

(define-public crate-tryrun-0.5 (crate (name "tryrun") (vers "0.5.2") (hash "10wdxhq86rlc9lal1gbq8sawm6dqpyy43z936z59bmmjpvbzkr9n") (yanked #t)))

(define-public crate-tryrun-0.5 (crate (name "tryrun") (vers "0.5.3") (hash "0y6jqib32cp0yq62fzrxl9g4agva850kilssihsvzf0l9aypzdfn") (yanked #t)))

