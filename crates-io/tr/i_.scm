(define-module (crates-io tr i_) #:use-module (crates-io))

(define-public crate-tri_art-0.1 (crate (name "tri_art") (vers "0.1.0") (hash "0m9nmazl0xh7s6hhz4s8l191acnw80ad1aq33cb8b70qkzvsy98a")))

(define-public crate-tri_avltree-0.1 (crate (name "tri_avltree") (vers "0.1.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0n97jr1ahl3jw36j0bh5rhz5svvpj6rn8brciwq0gqvlhxbmg3l1") (yanked #t)))

(define-public crate-tri_avltree-0.2 (crate (name "tri_avltree") (vers "0.2.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "04jw9yyhqv4gknc0m5iivzmiclnsiwbv9m9kf0h9ppfmglpw6c37") (yanked #t)))

(define-public crate-tri_avltree-0.3 (crate (name "tri_avltree") (vers "0.3.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1f3n357bsxqa14hy0xs06g6jwswq0k9iwi2pgr5qcgs6d3803ikx") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.0") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "001im8a16zdjlbg6ndyipl0vi5ag5abkz88ifsjshx26djajc5x5") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.1") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "0xsq5qff2yqqfcriiz4yv34n9s11pgy3k4bqr85gds7chpd8w84b") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.2") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1r5vpxjm1mmf79g39wiz89m1a336pxz7dr015ls38gjyp0ywlx9w") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.3") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "1mmivmmi0z17y2ykzr05p3hh2n6rdxj5njswklskc1b2m7lc0k1b") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.4") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "10cranvfyansbixa75k4fc7z3vk3623rqnskvgnqniajfs6bdx3w") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.5") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "01cdydmy3rf3pzvdizp4v9py0nxcm0sbbg8zbx18sjwyif1b7wg7") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.6") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "138sqjqgq6c39vgda6i8zjbmp7vr1lv2swailg41cpfwkrs6lfkc") (yanked #t)))

(define-public crate-tri_avltree-0.4 (crate (name "tri_avltree") (vers "0.4.7") (deps (list (crate-dep (name "rustc-hash") (req "^1.0") (kind 0)))) (hash "16ll53i4igh62pjajbxlmziblz2pmdv84riprw2ifxpc7x0a52ay") (yanked #t)))

(define-public crate-tri_poly_moment-0.1 (crate (name "tri_poly_moment") (vers "0.1.0") (hash "1h9218in6x40c1hmjsz0ixi3bfzgmybdvm6b0vb25ail6vvk4ncm")))

(define-public crate-tri_poly_moment-0.1 (crate (name "tri_poly_moment") (vers "0.1.1") (hash "1jq8h98km550b7161fbiadq9f7wqrl1cd8nx0149wzg1fb91xsf0")))

