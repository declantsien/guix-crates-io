(define-module (crates-io tr oj) #:use-module (crates-io))

(define-public crate-trojan-0.1 (crate (name "trojan") (vers "0.1.0") (hash "13mq0vk05bxaiym3i425rklgvp5n9snpvrk1ap8iz57pm9kwqqx3")))

(define-public crate-trojan_rust-0.1 (crate (name "trojan_rust") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rustls-native-certs") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25") (default-features #t) (kind 0)))) (hash "1gz12paa5pm1iymw4sdhi2p9zxykbw77ydy3j7bgr4cm7pr40mph")))

