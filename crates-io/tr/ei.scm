(define-module (crates-io tr ei) #:use-module (crates-io))

(define-public crate-treiber_stack-1 (crate (name "treiber_stack") (vers "1.0.0") (deps (list (crate-dep (name "arc-swap") (req "^1.6") (default-features #t) (kind 0)))) (hash "1qrf4j68z3h1rdij88y1sxa240rzkjqfr7jgib0x4mlmx5vhnkb7")))

(define-public crate-treiber_stack-1 (crate (name "treiber_stack") (vers "1.0.1") (deps (list (crate-dep (name "arc-swap") (req "^1.6") (default-features #t) (kind 0)))) (hash "1lnl5zdn204alv6gzy4nwnxf5cj4rbv5zxvqv07bncd2a16vwqcr")))

(define-public crate-treiber_stack-1 (crate (name "treiber_stack") (vers "1.0.2") (deps (list (crate-dep (name "arc-swap") (req "^1.6") (default-features #t) (kind 0)))) (hash "0cp4s38lp2kd7zkaxcwvprxxv7rai30qxynl4cqrafmqmbs2xcnp")))

