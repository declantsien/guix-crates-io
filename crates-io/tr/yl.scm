(define-module (crates-io tr yl) #:use-module (crates-io))

(define-public crate-trylog-0.1 (crate (name "trylog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0p81m90i9s5mbbm7ihsbzlabpfwspwjgcxfd3q45sn056kf00ywv")))

(define-public crate-trylog-0.2 (crate (name "trylog") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0any7zbbqxrj1w1yfkn42c8nl266w3zhskl8rbx1l8c4fwdvm4zn")))

(define-public crate-trylog-0.3 (crate (name "trylog") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.0") (default-features #t) (kind 2)))) (hash "12mq3dqhp5qx5g3nrji56kmi4gn6gj2scdh1n8kdn0k1qlm3p336")))

(define-public crate-trylog-0.3 (crate (name "trylog") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.0") (default-features #t) (kind 2)))) (hash "1cf7ba032vb0c96va7dqsc3msgmgpfhzxnik48y2zgin3d7mag2w")))

