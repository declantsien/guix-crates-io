(define-module (crates-io tr at) #:use-module (crates-io))

(define-public crate-trata-0.1 (crate (name "trata") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 2)))) (hash "13xs3qksr1m0gx1hm0vnljxnlz2148xs48hwp2m1glypiqdpdxv1")))

