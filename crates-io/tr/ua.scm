(define-module (crates-io tr ua) #:use-module (crates-io))

(define-public crate-truant-1 (crate (name "truant") (vers "1.0.13") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0znmlvpz66dz4dvg738v9qch0jfmhj5cbabmhbqxdfqp8mq8q3vc")))

(define-public crate-truant-1 (crate (name "truant") (vers "1.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "01k8ini2nia8b7d4mb6g6jszagmhhmjzripfp7nkzzpr72mq3wy0")))

