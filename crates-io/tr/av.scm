(define-module (crates-io tr av) #:use-module (crates-io))

(define-public crate-travailleur-0.0.1 (crate (name "travailleur") (vers "0.0.1") (deps (list (crate-dep (name "garde") (req "^0.18.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 2)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.34") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.8.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0liqal75jdn4nv5s3ijcg1km8rq0cmks43b15a9sfb6lh4n0536r") (features (quote (("default" "validate" "yaml")))) (v 2) (features2 (quote (("yaml" "dep:serde_yaml") ("validate" "dep:garde" "dep:itertools" "garde/derive"))))))

(define-public crate-travelling_salesman-0.0.1 (crate (name "travelling_salesman") (vers "0.0.1") (hash "0kbss13h438zb9i032z03l602n9bi9vasm3y9s3p0dvdf0q4acr8")))

(define-public crate-travelling_salesman-0.0.2 (crate (name "travelling_salesman") (vers "0.0.2") (hash "1j8qwiy1ffjg6ajx7w3c91008x4r2m9grvnnlmplxjrbw66g83rz")))

(define-public crate-travelling_salesman-0.0.3 (crate (name "travelling_salesman") (vers "0.0.3") (hash "11cf111sk9j5bxmlrl4np1z4fc6z4sxdanmn6wjwbik2xj8y9463")))

(define-public crate-travelling_salesman-0.0.4 (crate (name "travelling_salesman") (vers "0.0.4") (hash "1s92ilnlqjmbgl1wa6hfapx6g5cal7mh85csnbyripfx0dpz6p82")))

(define-public crate-travelling_salesman-0.0.5 (crate (name "travelling_salesman") (vers "0.0.5") (hash "0cn4yylah3vwvaqh6yghpx15cwagnwzhdrzx9rxf8hiir1iw14gj")))

(define-public crate-travelling_salesman-0.0.6 (crate (name "travelling_salesman") (vers "0.0.6") (hash "1kar2hzr55h8pqsm8ymj7hh4ymm5yp7i7lqj2mcs3q78v3wkjg0y")))

(define-public crate-travelling_salesman-0.0.7 (crate (name "travelling_salesman") (vers "0.0.7") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "1ilwv3p2k6kz41v5bi5hhcaj849rjybwmdh22bx7wdsnp70b9mrk")))

(define-public crate-travelling_salesman-0.0.8 (crate (name "travelling_salesman") (vers "0.0.8") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "0lcmdfvcrdsw56wdp5vl3ny65236q3gc377q11yyfmsh97jmn4d8")))

(define-public crate-travelling_salesman-0.0.9 (crate (name "travelling_salesman") (vers "0.0.9") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "0al4q3vxgy42pwffg010z5lkhivwbd6qvcxd0hgsf61bwx8a0rs2")))

(define-public crate-travelling_salesman-0.0.10 (crate (name "travelling_salesman") (vers "0.0.10") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "05iqklkr3scbbim1pm4ri227hi8fvmf0nnaycgk1mp0cs5jcj8jm")))

(define-public crate-travelling_salesman-0.0.11 (crate (name "travelling_salesman") (vers "0.0.11") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "1iixq6flm4hrkrhy2q8kp2797nky0ck9y3arkiqlhi2wl6x53rr2")))

(define-public crate-travelling_salesman-0.0.12 (crate (name "travelling_salesman") (vers "0.0.12") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "120wvdb7k98dgdq2ggl5nznsj25bh67yznv33i179z2djjjy3z39")))

(define-public crate-travelling_salesman-0.0.13 (crate (name "travelling_salesman") (vers "0.0.13") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "029an781mf3l2mmczcgkammqn4hyzb6zrf7aixrz549w3qn4l2vv")))

(define-public crate-travelling_salesman-0.0.14 (crate (name "travelling_salesman") (vers "0.0.14") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "05fkhy1qhv216qwwg3kd5xv0z2wh9yb6zx91casr8q7sah238ips")))

(define-public crate-travelling_salesman-0.0.15 (crate (name "travelling_salesman") (vers "0.0.15") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "1g8cikijsic0q2vw9yj4xldppywxvclmds9x9f3iw75cx4hylbir")))

(define-public crate-travelling_salesman-1 (crate (name "travelling_salesman") (vers "1.0.16") (deps (list (crate-dep (name "metaheuristics") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "1sy4xnlwayp9v9s8ypk3zxlr3jj19nslq8mkfhz863rq1a4k6g14")))

(define-public crate-travelling_salesman-1 (crate (name "travelling_salesman") (vers "1.0.17") (deps (list (crate-dep (name "metaheuristics") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0ccjwvpad4syz1bq7nkrg52lx0p1y9rzfyqbnnky5p6i6cvfj2c9")))

(define-public crate-travelling_salesman-1 (crate (name "travelling_salesman") (vers "1.1.18") (deps (list (crate-dep (name "metaheuristics") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0gm2ymavhgph26y575jrgv3a4l0hn3yic2i7pa1625jc922kpfll")))

(define-public crate-travelling_salesman-1 (crate (name "travelling_salesman") (vers "1.1.19") (deps (list (crate-dep (name "metaheuristics") (req "=1.1.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "=0.8.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "=0.3.5") (default-features #t) (kind 0)))) (hash "1njy88k0v6hagn49nhyywiyssys3sbi3ad3bbif7jlvl4746hxzr")))

(define-public crate-travelling_salesman-1 (crate (name "travelling_salesman") (vers "1.1.20") (deps (list (crate-dep (name "metaheuristics") (req "^1.1.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1cfcfbfzhjk4sk1wwzawzpxdwxq79bjnyy7alw39bgp9r6wn9a80")))

(define-public crate-travelling_salesman-1 (crate (name "travelling_salesman") (vers "1.1.21") (deps (list (crate-dep (name "metaheuristics") (req "^1.1.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1wyq6ailyb774i03j7dw9la5lmwv84xhf9j6vx48gl3bl73cbyi4")))

(define-public crate-travelling_salesman-1 (crate (name "travelling_salesman") (vers "1.1.22") (deps (list (crate-dep (name "metaheuristics") (req "^1.1.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1200ymx704ylxrzqglrjw7aq5l34r5kk441swd579rz1l6zs1ad2")))

(define-public crate-traversal-0.1 (crate (name "traversal") (vers "0.1.0") (hash "1f7xidncsai6nzqi4rjm5axgcqnacyac365g0p76hrs3x4dcv74x")))

(define-public crate-traversal-0.1 (crate (name "traversal") (vers "0.1.1") (hash "0xw0psvn2yyvfjx4wp2q1f8rqx8hm99dkxgh208yi16ys3pvcik3") (yanked #t)))

(define-public crate-traversal-0.1 (crate (name "traversal") (vers "0.1.2") (hash "168cs331pnp4qn9id828sdgzjhjfv2icnr8aij78nz2isx2rgv7h")))

(define-public crate-traverse-0.0.1 (crate (name "traverse") (vers "0.0.1") (deps (list (crate-dep (name "stainless") (req "*") (default-features #t) (kind 2)))) (hash "09ma8gf1hpc8z09a5wvmx38rvb9xigdvms25h344l7sl3xwbspm2")))

(define-public crate-traverse-0.0.2 (crate (name "traverse") (vers "0.0.2") (hash "09li64ng37zjvhdwifpmgw8z63c35pm0glrbwy3la7hbkyxih83d")))

(define-public crate-traverse-0.0.3 (crate (name "traverse") (vers "0.0.3") (hash "1pncabwk36g3n3ws1vv4fqzlc414g43wh3dn7264jfp24iqig9kz")))

(define-public crate-traverse-0.0.4 (crate (name "traverse") (vers "0.0.4") (hash "0xy8a2999aqr8h2zkvd4smlhysp0zclxv3pnmsw3mfc5wgvhq3xj")))

(define-public crate-traverse-0.0.5 (crate (name "traverse") (vers "0.0.5") (hash "0qfm9sqwlr6wc6pqhsc0a4npw5m9hjlfd5daygaphyss4sqiix67")))

(define-public crate-traverse-0.0.6 (crate (name "traverse") (vers "0.0.6") (hash "11xml38pkg5hmcbrmq2wkx0i488iqsrjbimyksng088m1hfh7c9h")))

(define-public crate-traverse-0.0.7 (crate (name "traverse") (vers "0.0.7") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "01qx37fp9r7bvvmjqnssn7dz6ff4pls7k7m0n1c77djqvy3hqwkn")))

(define-public crate-traverse-0.0.8 (crate (name "traverse") (vers "0.0.8") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "016jqx9ydsm8pxffcm59gi3ndnn4wq1dnfx43hgxsv8gv1siznal")))

(define-public crate-traverse-0.0.9 (crate (name "traverse") (vers "0.0.9") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "128a9gfbdmjpzh1f6q7r148jbidcp1hrd125vw2p5zyw2lrphgjs")))

(define-public crate-traverse-0.0.10 (crate (name "traverse") (vers "0.0.10") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1l090k5rkjkiszpi4m67m9ak2s835g38lymxcacfafn5zk9bcx1k")))

(define-public crate-traverse-0.0.11 (crate (name "traverse") (vers "0.0.11") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0a9kjwy10k1c6nz4vwfrqyb197gi6d3d4xrrd4g8yjizmsg62kn4")))

(define-public crate-traverse-0.0.12 (crate (name "traverse") (vers "0.0.12") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "121y6sazhkhgfmkvsl5qnflnyp1q7w5f0wpqz5yjyy15w7jw824l")))

(define-public crate-travis-0.0.0 (crate (name "travis") (vers "0.0.0") (deps (list (crate-dep (name "derive_builder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "tokio-timer") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1maxs5wp2l652cxdzvj67z749b12sr1ypw36plzh8n65d9sa3pk8") (features (quote (("tls" "hyper-tls") ("default" "tls"))))))

(define-public crate-travis-0.1 (crate (name "travis") (vers "0.1.0") (deps (list (crate-dep (name "derive_builder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0pad9rcwxmmfx1q105kpyywsiqvbpwm74sp80sz9cp6ahpsq3d7f") (features (quote (("tls" "hyper-tls") ("default" "tls"))))))

(define-public crate-travis-0.1 (crate (name "travis") (vers "0.1.1") (deps (list (crate-dep (name "derive_builder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0z5ngsxi5pzh6c9r7k59m7yqkk1idxfk5s6qlf08zfvcypp4xznn") (features (quote (("tls" "hyper-tls") ("default" "tls"))))))

(define-public crate-travis-after-all-1 (crate (name "travis-after-all") (vers "1.0.0") (deps (list (crate-dep (name "curl") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "1wgfqgy6j3v8mri446nmwnbc9b2m4grcmhyg027rnki89mzc3ac7")))

(define-public crate-travis-after-all-2 (crate (name "travis-after-all") (vers "2.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.5") (default-features #t) (kind 0)))) (hash "0ycnfzyzyvp87jb1857wy4swn2labsf2599px31m5yaq7v3ypnpp")))

(define-public crate-travis-cargo-0.1 (crate (name "travis-cargo") (vers "0.1.0") (hash "1qifw5fsaxmamh03r95q6pzq3r18vy0zf641nbd0k5hg6g9frpij")))

(define-public crate-travis-pipeline-0.6 (crate (name "travis-pipeline") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "shellwords") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1rqk120143b19ylmxk60bd3bfsqdp1387h4q6n3338iqhzgw019s")))

(define-public crate-travis-rust-demo-0.1 (crate (name "travis-rust-demo") (vers "0.1.0") (hash "0zb2pzh62k5s8agv2g0s8a8fv5dbh45z23j3zrs62qzrwkhfnxjj")))

