(define-module (crates-io tr y_) #:use-module (crates-io))

(define-public crate-try_all-0.0.1 (crate (name "try_all") (vers "0.0.1") (hash "1klmr4svb37hgxmna1czaq97qnya4yrcj1jlky35y1ia9b71fdgn")))

(define-public crate-try_all-0.0.2 (crate (name "try_all") (vers "0.0.2") (hash "1b3bhx2zlqab6x7074lq27nz3phl0l84fd3c6vjl9w7jpnpcyvig")))

(define-public crate-try_as-0.1 (crate (name "try_as") (vers "0.1.0") (deps (list (crate-dep (name "try_as_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "try_as_traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1nx0hpqs735vl23yyisjs52x8yhhmiph6lblxbd63ndhpkgnqij4")))

(define-public crate-try_as_macros-0.1 (crate (name "try_as_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("derive" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "try_as_traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gcqskxp04zdl2hvx961ya0bzrjax7fn2wrspgspmmfv3p1l629v")))

(define-public crate-try_as_traits-0.1 (crate (name "try_as_traits") (vers "0.1.0") (hash "0han8sbh6m612vqhfcrhh6l7zmindaynbzc6359hb8dr0m4mparx")))

(define-public crate-try_buf-0.1 (crate (name "try_buf") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0crbg0k72x440pcgf4qj30r2yp21l3ph3fp7gsw0wq1k46pjmqnc")))

(define-public crate-try_buf-0.1 (crate (name "try_buf") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ipaw7qv8fggm6biwshqnl0zg16rh79h4yyx9g7s7njqlvyyqm1z") (yanked #t)))

(define-public crate-try_buf-0.1 (crate (name "try_buf") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "154bfdmgwislkc3wfbg4b9qgvs22kmk5i8f4vpvg8zi4fgzvzw53")))

(define-public crate-try_buf-0.1 (crate (name "try_buf") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v03j6f9dh2my7xp4h7lx44x509pim3c9a02r4gjgw6yfbhvrwgf")))

(define-public crate-try_clone_derive-0.1 (crate (name "try_clone_derive") (vers "0.1.0") (deps (list (crate-dep (name "fallible_collections") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1y6bsh6rx4m78qwsm3kbl6rhxdwi0xvm45nrjrlyd5nqhcy70kkm")))

(define-public crate-try_clone_derive-0.1 (crate (name "try_clone_derive") (vers "0.1.1") (deps (list (crate-dep (name "fallible_collections") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0312vdd49kww6q4r5snxxayq5y4ds32mgs5r0wj5blhd1ks065d7")))

(define-public crate-try_collections-0.1 (crate (name "try_collections") (vers "0.1.0") (hash "0xf7zjxr79bsi8xmlrvcx4q9y0ars5ndnihgd3clp8ph4bli6lhw")))

(define-public crate-try_collections-0.1 (crate (name "try_collections") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.8.2") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0m78jdy288ng1jbbjxlrzk14zscrqh79y0kxj9fi3vskiy4flq4y")))

(define-public crate-try_convert-0.1 (crate (name "try_convert") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "19k6as9kgn0sfsaikljarkf4fkfpbjb3z1c0gwn2z286ai3wf4s2") (features (quote (("thiserror") ("default" "thiserror"))))))

(define-public crate-try_convert-0.1 (crate (name "try_convert") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "0jd9yw9sjlcz7lrbcg2sba0rk1mi1a3j212gq67d1fhf03rd9zwj") (features (quote (("thiserror") ("default" "thiserror"))))))

(define-public crate-try_default-1 (crate (name "try_default") (vers "1.0.0") (hash "0gh4n9r8p324a3syj0xr8cqhpf0p7f2xxzmzprrhbvb1cl20cmc5")))

(define-public crate-try_default-1 (crate (name "try_default") (vers "1.0.1") (hash "022rywc5dh1zmnkbi7lwa7plihqvb30sj1xzvbb5dgjpl46kv4w2")))

(define-public crate-try_encoding_from-0.1 (crate (name "try_encoding_from") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1blzj89b21gir7536br1n63spqnfcrnm1bysfkfddlhliwyz7dvk") (features (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("cbor" "serde" "serde_cbor"))))))

(define-public crate-try_encoding_from-0.1 (crate (name "try_encoding_from") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0ak942q1lxfh4s7i0sjpsakpai0195ixl0d5l3y3flgd89lprzk3") (features (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("cbor" "serde" "serde_cbor"))))))

(define-public crate-try_encoding_from-0.1 (crate (name "try_encoding_from") (vers "0.1.2") (deps (list (crate-dep (name "btree_graph") (req "^0.2") (features (quote ("serde"))) (kind 2)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0cy7j3sm5bl8yw9nf12s831mbrj0dqwkvc21rd16a4nvy51njyr9") (features (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("cbor" "serde" "serde_cbor"))))))

(define-public crate-try_encoding_from-0.1 (crate (name "try_encoding_from") (vers "0.1.3") (deps (list (crate-dep (name "btree_graph") (req "^0.2") (features (quote ("serde"))) (kind 2)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0gz3j54z2r8qfirfc69szv0r034bprfy05f2zrg7a5bxr4shmfjy") (features (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("cbor" "serde" "serde_cbor"))))))

(define-public crate-try_encoding_from-0.1 (crate (name "try_encoding_from") (vers "0.1.4") (deps (list (crate-dep (name "btree_graph") (req "^0.2.2") (features (quote ("serde"))) (kind 2)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1kzsjimdw0d98yaqr4bckxyxxb42qp0q7pjdhlsnqw8w9mi10q5j") (features (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("error") ("cbor" "serde" "serde_cbor"))))))

(define-public crate-try_enum-0.1 (crate (name "try_enum") (vers "0.1.0") (hash "0qhrv96av9qjd1v9bb65fbavx38mrqz9d842q8l774qxidkpw6dd")))

(define-public crate-try_fold_impl-0.1 (crate (name "try_fold_impl") (vers "0.1.0") (hash "166wmnfx0bj20aizw0zf5yy6p1j0lfxiqkv3qfr80pxa7rs292bz") (yanked #t)))

(define-public crate-try_fold_impl-0.1 (crate (name "try_fold_impl") (vers "0.1.1") (hash "0ijkbmy9z54wnglkgsh4vi2bzrgy6kxvw9zawgy1sydw8c3pyvh2") (yanked #t)))

(define-public crate-try_fold_impl-0.1 (crate (name "try_fold_impl") (vers "0.1.2") (hash "10272dzd5n4xqb5wxbp3pgkmd4laf4jdhkkv16wg11bw7y80v3b5")))

(define-public crate-try_fold_impl-0.2 (crate (name "try_fold_impl") (vers "0.2.0") (hash "0b2pgzhxb5zl5cql7w4711ls36si8rrpq4cm4pv7wrfwiwzsn7h5")))

(define-public crate-try_fold_impl-0.2 (crate (name "try_fold_impl") (vers "0.2.1") (hash "10b960p03fjg3pfvaszs6gg1nfizdqarm0vgrpmk7p1fn964hgy4")))

(define-public crate-try_from-0.1 (crate (name "try_from") (vers "0.1.0") (hash "03biqhpm57wazim8asxhbak7imwhhrylcmyjvhsazdldyrkadyn5")))

(define-public crate-try_from-0.2 (crate (name "try_from") (vers "0.2.0") (hash "1ycvpcd4kacpj4n53v2fwk1fj6r00hdhknrrb3mmlrwnvn56bw0j")))

(define-public crate-try_from-0.2 (crate (name "try_from") (vers "0.2.1") (hash "0lszjrjkcrkq4hp7dbpg0ksv7c3ssv0dgc18vr3maxar3fr1zss8")))

(define-public crate-try_from-0.2 (crate (name "try_from") (vers "0.2.2") (hash "0481awlrf5j37mlh4pa0a890lqn93i8v9gk1a9lfigvxx7ipwflj")))

(define-public crate-try_from-0.3 (crate (name "try_from") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)))) (hash "0skqyin60vv4vi5nbqqimq8nhk0xiqsg5ylymh9d6iv922hrbmf9") (features (quote (("no_std"))))))

(define-public crate-try_from-0.3 (crate (name "try_from") (vers "0.3.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b0kl2mnir48pcxp4lmj5nxf4ci8022zfls12ws4gwlwkdzm52p4") (features (quote (("no_std"))))))

(define-public crate-try_from-0.3 (crate (name "try_from") (vers "0.3.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)))) (hash "12wdd4ja7047sd3rx70hv2056hyc8gcdllcx3a41g1rnw64kng98") (features (quote (("no_std"))))))

(define-public crate-try_from_tup_macro-0.1 (crate (name "try_from_tup_macro") (vers "0.1.2") (hash "0ynnw4lmdfjr98nqclhbcjf8by8w4if4n86nmcj9i0pyw8jxzmqm") (features (quote (("try_from_tup_8") ("try_from_tup_16"))))))

(define-public crate-try_future-0.1 (crate (name "try_future") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0hgxcq1ikybgkzh2n9c1n73zh1bzgh0010sq12g8rsn9z2ppadyd")))

(define-public crate-try_future-0.1 (crate (name "try_future") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0xw612fs47h28j2ggqg3814f92018xp4kclqf8phgqblkcissqjl")))

(define-public crate-try_future-0.1 (crate (name "try_future") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0i85i3x87nc69zx1b7x7f6ymscbhn1z6bb7qy7w9kasd429im6dm")))

(define-public crate-try_future-0.1 (crate (name "try_future") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1mxabd69nhcp8l7gkdhd8f2kxhxpfgvykil6yc39xwiqxs342i9h")))

(define-public crate-try_into_opt-0.1 (crate (name "try_into_opt") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "05wpmqlss332sgkw0qvipm215g041hb6w1wfzf6brl1m145rmp4w") (features (quote (("nightly-testing" "clippy"))))))

(define-public crate-try_map-0.1 (crate (name "try_map") (vers "0.1.1") (hash "14mi01fk3kil8bnv5rqx8ijngyamfcb6h9lplywmp29kk7hmyn5x")))

(define-public crate-try_map-0.2 (crate (name "try_map") (vers "0.2.0") (hash "0fsa9fxb2880acm3faxg4khn9zn885vqdga5xj5rsmy3r70gk56p")))

(define-public crate-try_map-0.3 (crate (name "try_map") (vers "0.3.0") (hash "1ychd04d210csd0sc3bqjc50ga4sq24mlmydsd2nwbdwfz9dcjyn")))

(define-public crate-try_map-0.3 (crate (name "try_map") (vers "0.3.1") (hash "106mz6cxmgf5m34ww6h45g0ags2j92zc153xy4nbphdmgk82c5pv")))

(define-public crate-try_match-0.1 (crate (name "try_match") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "try_match_inner") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "07asa158mn6c0nsx5cqvkvxhp8k0wqpqiy3vmfnysnfjskaia6mx") (features (quote (("std") ("implicit_map" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2 (crate (name "try_match") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "try_match_inner") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0pjwflbqzn8a1q60dl1bg3vgg0bnjcmh1kgbqc4d6825g1p2x55h") (features (quote (("std") ("implicit_map" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2 (crate (name "try_match") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "try_match_inner") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0j1r2rrfglz7rifpw8jk9y6bw3d16mpg45b77v9r88z8bf6iyqhn") (features (quote (("std") ("implicit_map" "proc-macro-hack" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2 (crate (name "try_match") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "try_match_inner") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "070fdzpmkzlffljrrpg3j4a1cvqj53wsm215hars2wjywgapk0hk") (features (quote (("std") ("implicit_map" "proc-macro-hack" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.2 (crate (name "try_match") (vers "0.2.3") (deps (list (crate-dep (name "if_rust_version") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "try_match_inner") (req "=0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1a6x88wyva8nrrh5x0gmhlfnavszfsbk9a5v9b8li2zz0kyxp3mv") (features (quote (("std") ("implicit_map" "proc-macro-hack" "try_match_inner") ("default" "implicit_map" "std"))))))

(define-public crate-try_match-0.3 (crate (name "try_match") (vers "0.3.0") (deps (list (crate-dep (name "try_match_inner") (req "=0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jsq3jj49aq24ls6wknfk90g4n718sw6k1n9vj8wax6cfka9r7vq") (features (quote (("std") ("implicit_map" "try_match_inner") ("default" "implicit_map"))))))

(define-public crate-try_match-0.4 (crate (name "try_match") (vers "0.4.0") (deps (list (crate-dep (name "try_match_inner") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "087ra5lrqr8w1j7dmbi83jbb9jiibrcg6qf3082842gcml3p2qmm") (features (quote (("unstable") ("std") ("implicit_map" "try_match_inner") ("default" "implicit_map") ("_doc_cfg")))) (rust-version "1.56")))

(define-public crate-try_match-0.4 (crate (name "try_match") (vers "0.4.1") (deps (list (crate-dep (name "try_match_inner") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.80") (default-features #t) (kind 2)))) (hash "1r1bkqw8ix7dk89lb2vl619sx2gszf1jcmwfs8q9x1g884ckrbk1") (features (quote (("unstable") ("std") ("implicit_map" "try_match_inner") ("default" "implicit_map") ("_doc_cfg")))) (rust-version "1.56")))

(define-public crate-try_match_inner-0.1 (crate (name "try_match_inner") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n6fqpwk3qj4ipwjfdjby3960n012qqkyxln4x1qxjnv7097i0xr")))

(define-public crate-try_match_inner-0.1 (crate (name "try_match_inner") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req ">= 0.3.1, < 2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pz0w9h39ikj2xmkh3pcy4wqjdpqzr60gl716xxg7pr5x8yvysxm")))

(define-public crate-try_match_inner-0.2 (crate (name "try_match_inner") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-error") (req ">=0.3.1, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bpqbs5kahgpcjaqpsh7k4kisa8j43mddc9gdlqyfbl47pfw0xdw")))

(define-public crate-try_match_inner-0.3 (crate (name "try_match_inner") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro-error") (req ">=0.3.1, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.44") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xg8649bkzzciirpppwvnd29ij3awvp4b3x3xczwf9kjk01n8bx7")))

(define-public crate-try_match_inner-0.4 (crate (name "try_match_inner") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro-error") (req ">=0.3.1, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.44") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1aa9vcar6mi8zlf9498fyc8lr0v5m4650710sih1mpqvpisnnzk0")))

(define-public crate-try_match_inner-0.5 (crate (name "try_match_inner") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0008jazl3cabiggx3x2zyzi6gy43yh57jkys5mmf1bv4bx7alzpg")))

(define-public crate-try_match_inner-0.5 (crate (name "try_match_inner") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kpkpwf8fy8r27xh5fb1gdg8frjmjj4795rwnbncm6172c9igadh")))

(define-public crate-try_more-0.1 (crate (name "try_more") (vers "0.1.0") (hash "01bybwzc2h1mm9zm2gnr86dggbgks9hm1by5swqjp4wrpwr0nz4q")))

(define-public crate-try_more-0.1 (crate (name "try_more") (vers "0.1.1") (hash "1c219bfysy080m95l9sha5aa714qjjxlks9p97h89p4ddr2ns8m0")))

(define-public crate-try_ops-0.1 (crate (name "try_ops") (vers "0.1.0") (hash "1iljcnb6gd0js6wcykmmgg4i72qadnq41nqhcc0dgs5j10gcnxcl")))

(define-public crate-try_ops-0.1 (crate (name "try_ops") (vers "0.1.1") (hash "0rdmiliar22bflnvb0kr9rrjaww5afxfn51i6n5b351ayjins4vq")))

(define-public crate-try_opt-0.1 (crate (name "try_opt") (vers "0.1.0") (hash "189x3lylfn0zk5yr5nbakn09dm8njf4s7zcqwqg258a0w2yhqnlk")))

(define-public crate-try_opt-0.1 (crate (name "try_opt") (vers "0.1.1") (hash "1c8d6y430h5ggjjsnqh9vxd025kgwd79zfpkffc1yma0pbkp22yk")))

(define-public crate-try_opt-0.2 (crate (name "try_opt") (vers "0.2.0") (hash "1h2v7q57y7a8jrxinb67hwkiv5pm93jvxkjl6jgl6zc6jxj2qa4f")))

(define-public crate-try_or-0.1 (crate (name "try_or") (vers "0.1.0") (hash "059r31x7ippmhg8jik7wyrr240kh2z6rvlkfffn8clkmq9hsvq98")))

(define-public crate-try_or-0.2 (crate (name "try_or") (vers "0.2.0") (hash "0ha5azqda4bl6qi86ndlqs56152zdscsj063rg1rns7fcsnylw0x")))

(define-public crate-try_or_unwrap-0.0.0 (crate (name "try_or_unwrap") (vers "0.0.0-0.0.0-0.0.0") (hash "062154bvga7h968p8awgg11ddm0n6h3ywf441rv1wkni6pis0bl1") (yanked #t)))

(define-public crate-try_or_unwrap-0.0.0 (crate (name "try_or_unwrap") (vers "0.0.0--") (hash "0qvjap07jb879yzz9mbbdgrqxzyzbyis45v0kygaa5g0h7m4mslp") (yanked #t)))

(define-public crate-try_or_unwrap_internal-0.0.0 (crate (name "try_or_unwrap_internal") (vers "0.0.0-0.0.0-0.0.0") (hash "0kwy85y8vf9v8hzy974dhm3mwi7ps6jzch3mv5gixgm4b7hc6lbz") (yanked #t)))

(define-public crate-try_or_unwrap_internal-0.0.0 (crate (name "try_or_unwrap_internal") (vers "0.0.0--") (hash "1zmf74686r7vkvgagpx1rsvgfasr97yw4sp21lg6pxw7hhkr4ih5") (yanked #t)))

(define-public crate-try_or_wrap-0.0.2 (crate (name "try_or_wrap") (vers "0.0.2") (hash "0dyyw7m2pwvha2w89xnhd6flxbkwgivr91bb1ga351d02xn26425")))

(define-public crate-try_or_wrap-0.0.3 (crate (name "try_or_wrap") (vers "0.0.3") (hash "17c357cy4g0bkinqgxjly7lmva73kc32q2y804irr8zfijzwrdxa")))

(define-public crate-try_or_wrap-0.0.5 (crate (name "try_or_wrap") (vers "0.0.5") (hash "0bgjalm3f9wxk8cj235dm022l4bddc6dqh1wcaav6vzxvpavai46")))

(define-public crate-try_or_wrap_s-0.1 (crate (name "try_or_wrap_s") (vers "0.1.0") (hash "1z37qrqjz1fc7p8apch9gq7vgkiccy52w40frbgdp5majhy6kjnf")))

(define-public crate-try_or_wrap_s-0.2 (crate (name "try_or_wrap_s") (vers "0.2.0") (hash "19cmmj0kkha5d3fwz0x4xgsrg642dn5dnayand39sx32fycc1nai")))

(define-public crate-try_print-0.0.1 (crate (name "try_print") (vers "0.0.1") (hash "0vdr6xdwmgqd2kb7lv0r13ixa26mzv85s6gh41avim0x0cf1k8fh")))

(define-public crate-try_publish_for_demo-0.1 (crate (name "try_publish_for_demo") (vers "0.1.0") (deps (list (crate-dep (name "pick-one") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "14n0wgb5fyalv47h8lbgdwi4005x0s0fmmspdi205wv8hsgcacsm")))

(define-public crate-try_stream-0.0.0 (crate (name "try_stream") (vers "0.0.0") (hash "0cw7m08d9b7d0i82ymiy2zfsd4r0l30722nw2fxjjdlfw6zx4pd1") (yanked #t)))

(define-public crate-try_take-0.1 (crate (name "try_take") (vers "0.1.0") (hash "1vj634640lslr20fnsv4d4hhrw4y12yypfq88pdx61sf9j68k8df")))

(define-public crate-try_transform_mut-0.1 (crate (name "try_transform_mut") (vers "0.1.0") (hash "1phx4j3d19sd0rq0wvpmh5p1j9v0yp5ym8z1ppjzklkrwwj43ik5")))

(define-public crate-try_transform_mut-0.1 (crate (name "try_transform_mut") (vers "0.1.1") (hash "0bcja5qgwgj37cf1z8b8vkxp09vl4c7a3jsxg6ir3f5kfrws9i05")))

(define-public crate-try_tup_to_arr_macro-0.1 (crate (name "try_tup_to_arr_macro") (vers "0.1.0") (hash "0v7lk3sfgwws0yz1iwcclqw2zf2xii9gackd2kvlfn4fw081xnad") (features (quote (("try_tup_to_arr_8") ("try_tup_to_arr_16"))))))

(define-public crate-try_utils-0.1 (crate (name "try_utils") (vers "0.1.0") (hash "049pv2hs4d4cpm5yp5gwrzg50m0j099hqgb1203kphii1vdxk315")))

(define-public crate-try_utils-0.1 (crate (name "try_utils") (vers "0.1.2") (hash "0wihbpzr7qrmjacif3hyc1x43id4xz43zfmf591fmnvgsi73ipn4")))

(define-public crate-try_wrap_ok-0.1 (crate (name "try_wrap_ok") (vers "0.1.0") (hash "0yjcsxxcc32bk5c7bkhv1ihxcmmn58v9avg2n3000s7s2h0l0nv0") (yanked #t)))

(define-public crate-try_wrap_ok-0.1 (crate (name "try_wrap_ok") (vers "0.1.1") (hash "0klyrip7rfhc54a8caqlp25kb52vsqhg7008lzdnbyqs2n9xwi3q")))

(define-public crate-try_zip-0.1 (crate (name "try_zip") (vers "0.1.0") (hash "0gcvqbp1dy6yw75cjwfmcw5wlf823h14izx0c7dxirlf2kmjzwl3")))

(define-public crate-try_zip-0.2 (crate (name "try_zip") (vers "0.2.0") (hash "0hcbgs1k9jm20ai5y21195ci8jj6hzf6x4j0ij1409rjp7d7ig4c")))

(define-public crate-try_zip-0.2 (crate (name "try_zip") (vers "0.2.1") (hash "1gc48pb3p6513325c4652limncx8jn91x6xsyd66c93g8ibcdkpf")))

