(define-module (crates-io tr ia) #:use-module (crates-io))

(define-public crate-triable-0.1 (crate (name "triable") (vers "0.1.0") (hash "0cb4cxqx30is9ndzpvv67qy8dmyaaxyh5xbyj9s1c2wxqks9ibq8")))

(define-public crate-triable-0.1 (crate (name "triable") (vers "0.1.1") (hash "1l2qsrx98m7apq23998vrbkgvackfzhyb29sjdb972wvsy8pvma2")))

(define-public crate-triable-0.1 (crate (name "triable") (vers "0.1.2") (hash "121cldpbvsxpk7lcdjlyp812d9rqwc5l7ysz6kbp5x16vx3xv7cc")))

(define-public crate-triadic-census-0.0.1 (crate (name "triadic-census") (vers "0.0.1") (deps (list (crate-dep (name "petgraph") (req "*") (default-features #t) (kind 0)))) (hash "12b8b58mkv2k1xf95z742vd6sqkh7jlmx1x97j9gfz8y3jj0isqz")))

(define-public crate-triadic-census-0.1 (crate (name "triadic-census") (vers "0.1.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ys84pygy9wymyxyrrmn4sgxyvvadzhbr7q80brspq6ysqivndav")))

(define-public crate-triadic-census-0.1 (crate (name "triadic-census") (vers "0.1.1") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0pb5vjnfdwgzym7dxygi0wnwx5dk0614mxx4fxag8lsrxsn7fb3x")))

(define-public crate-triadic-census-0.2 (crate (name "triadic-census") (vers "0.2.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "08nr3v11m0kqs86z62qra0b2iz175ac641za830g3waic1sv48bj")))

(define-public crate-triadic-memory-0.1 (crate (name "triadic-memory") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive" "env" "regex" "unicode"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "00c4nyy6s5rgj8xv9fky8nb4akbxl02n87yhb7bakykpal55fyw4")))

(define-public crate-triadic-memory-0.1 (crate (name "triadic-memory") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive" "env" "regex" "unicode"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0nf3w7qngz774bjyz02yji8h1g5y7irkk5z7a8rarzxcbdgj8qp9")))

(define-public crate-triageo-0.1 (crate (name "triageo") (vers "0.1.0") (hash "1xgavqnmnh8mk65frbr1jmjw5qw2v7wha24c06ahqylv4ga5dbj1")))

(define-public crate-triaka-rpc-0.1 (crate (name "triaka-rpc") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("net" "io-util"))) (default-features #t) (kind 0)))) (hash "05k9ix98v4a22pg6pndcbjxcqqbs45lxa6px50z8fwkr1mzxqiym")))

(define-public crate-triaka-rpc-proto-0.1 (crate (name "triaka-rpc-proto") (vers "0.1.0") (deps (list (crate-dep (name "bincode-json") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net" "io-util"))) (default-features #t) (kind 0)))) (hash "05k3ldyvbjbyn1i0631hdjiplh2k0wzizzfll4fw9nnyprpxyg7h") (yanked #t)))

(define-public crate-triaka-rpc-proto-0.2 (crate (name "triaka-rpc-proto") (vers "0.2.0") (deps (list (crate-dep (name "bincode-json") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "1v7xrrb833hd1kr1gzqs3r2wfv8i2swbi4qc8x2iajyk4wwn2yyj") (features (quote (("default" "nonblocking")))) (yanked #t) (v 2) (features2 (quote (("nonblocking" "dep:tokio"))))))

(define-public crate-triaka-rpc-proto-0.2 (crate (name "triaka-rpc-proto") (vers "0.2.1") (deps (list (crate-dep (name "bincode-json") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mmxwssa3fnixjarl9qkc38w123j5fmazjhkp8cykv4s0z4iic7l") (features (quote (("default" "nonblocking")))) (yanked #t) (v 2) (features2 (quote (("nonblocking" "dep:tokio"))))))

(define-public crate-trial-lib-0.1 (crate (name "trial-lib") (vers "0.1.0") (hash "144fprn1256hjivrgc771qs7djqvdy66qdb88vv4va4rxih8q153")))

(define-public crate-triamond-0.0.1 (crate (name "triamond") (vers "0.0.1") (deps (list (crate-dep (name "amethyst") (req "^0.15.1") (features (quote ("vulkan"))) (default-features #t) (kind 0)))) (hash "14c7i4qcysajdj8bgpmfmv5s9psb40kj6z808x2ms6x0fdj7kb0h") (yanked #t)))

(define-public crate-triamond-0.0.2 (crate (name "triamond") (vers "0.0.2-discontinued") (hash "03ixq8mazg4vqr80rhal2kibd4is4v65y7an5iaxrbc7mi4nvy3i")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.0") (hash "08f0abykv53z1yw5zw04n0b7f42f6jj97rkah1smlhkg8sv1g2q8") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.1") (hash "0b8j73d45k18wbm9y30sz24d5r6z471i7lxd7wvim25wr9lhxb2w") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.2") (hash "1j94mfhz9ji2chmggkw0fwcz1acdkk38dfy551hcpszh2mvvsfhj") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.3") (hash "0xhy9il1slnbwkdy96ppcgbb2bxwdyli959wvw8lz7q4b8hgb5jg") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.31") (hash "0acy27d4nnic4nflciaqfznzijvgk28zigvi70zdjvhwxipj7ywf") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.311") (hash "1xi7m8s1aqvll2km3rdyhwrh26zh0r86v0bfqm9xx9zlf7i12chc") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.32") (hash "1h6ax04ypjvqqg0d4zwxnzihqj5hh4bcjdiya9jf9dcl7lxxcsql") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.34") (hash "1nvy50cr8w4461ab3h7ail2cxf12wpz4wc5fl5nrpv8cvs9i717k") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.340") (hash "06lv5y41b9jlyw19l3fgrypzzh1lpsxicz791z4c76qmxqmyyghp") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.341") (hash "19qg5n1b8dfcyl9qwbfb9372f44ysa9ca8dqvv6phxy9jmbzszpk") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.342") (hash "14mdpgbqxz3wn35frgpzf4ckd0klfxlyr1vr6f4j42zqa280wgzf")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.344") (hash "16rrv7rqr3x1r8dn0vjs89ms9w50kaqh183wfi5b40vk77b10sa7")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.345") (hash "1f05hwi2a0knv8qm3bnqm5gf0shixvnpzfxymgl0zvgh2vgalvq8")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.346") (hash "1q0dv0wv88pj6qwl920mypg5j40hh2i1dn46v81czppc2gyv3vk8")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.347") (hash "1cqjri5gn5cbkncrgz6g58n794bviarxnnippkihiz3nprgzc0w2")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.348") (hash "1a3ar6bmy6n277vz911s3jfvv40jx6qi8czsgk97a567z95kq4z5")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.349") (hash "0x72a0faz43jdm8ij98grazzc4nqqp4rgzgc2y5i83kck6y0akav")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.350") (hash "1gf4nc6rf800w50hwi44ncp9z829qb4w34fdcrlr48458b1rzy79")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.351") (hash "0jpk01nj1876a2jnn3aimlj0n2id7iaz6abyji5sj61k4ldqz3vl")))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.352") (hash "17ys2nwdviz9akaw5mz2y63fka08yjkshcp4fy3fwgnwmnimzp48") (yanked #t)))

(define-public crate-triangle-0.1 (crate (name "triangle") (vers "0.1.353") (hash "11gxr4z4cgnji0wcqq1jc53s86x4d6bwn3wj9bb46d410nnr2f31")))

(define-public crate-triangle-0.2 (crate (name "triangle") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0b84b13fzhh9wymnhx9mf4gi5k9r8ms9c32q5hhpcmpllkrrk38b")))

(define-public crate-triangle-from-scratch-0.1 (crate (name "triangle-from-scratch") (vers "0.1.0") (hash "13sbz8y40bfhx5q5r3rrkrj8yfzb1k0l6vlxpwq1wh3xarz1zh8l")))

(define-public crate-triangle-rs-0.1 (crate (name "triangle-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-pickle") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "spade") (req "^1.8.2") (default-features #t) (kind 2)))) (hash "0s4cnsqi7qmlbbz9cjm48hkj1dahhmmjfvvqm57p7nsd7jvmsf3b")))

(define-public crate-triangle-rs-0.1 (crate (name "triangle-rs") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-pickle") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "spade") (req "^1.8.2") (default-features #t) (kind 2)))) (hash "0s8hg577pqgq1dvkigp1mf93cpvzq8lz024bpjghld598fgssz5n")))

(define-public crate-triangle-rs-0.1 (crate (name "triangle-rs") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-pickle") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "spade") (req "^1.8.2") (default-features #t) (kind 2)))) (hash "0k6fdakpns3ikmp0as0nyv87prvkgchpf140m8l9vq0f5yy49b3r")))

(define-public crate-triangle_matrix-0.1 (crate (name "triangle_matrix") (vers "0.1.0") (hash "08nl90lk7r6h77da2k4bd2ds7vjy715vll4bhvym513gj04zz74n")))

(define-public crate-triangle_matrix-0.1 (crate (name "triangle_matrix") (vers "0.1.1") (hash "0x76xjkxhjaxpzyyhc7in1rlmk2l6rvb2drrm80jgam2mgfddycz")))

(define-public crate-triangle_matrix-0.2 (crate (name "triangle_matrix") (vers "0.2.0") (hash "0z15h8rvax66110693lzvz5fp884nih6wx45a7yxmc4rqwwsnzwh")))

(define-public crate-triangle_matrix-0.2 (crate (name "triangle_matrix") (vers "0.2.1") (hash "1jy00q46w9fj5ipwhca1zj3v269rrwqnxvia6yznzifp534hmi43")))

(define-public crate-triangle_matrix-0.2 (crate (name "triangle_matrix") (vers "0.2.2") (hash "11za5v46mk32nw04kwnwzd3pn3f96g15n0k1qhjch7ik874ia2ki")))

(define-public crate-triangle_matrix-0.2 (crate (name "triangle_matrix") (vers "0.2.3") (hash "1nqk5ny09ygw5z6n4ffiys6mhfbwsdhj214lsbrgqp1ln5irpqg7")))

(define-public crate-triangle_matrix-0.2 (crate (name "triangle_matrix") (vers "0.2.4") (hash "0vkzsdnfxzri2zhgv4dqrfkyx86i14gg6183i9am8ba8g2gjshfh")))

(define-public crate-triangle_matrix-0.3 (crate (name "triangle_matrix") (vers "0.3.0") (hash "0y3sgznxkhixmj2bdrmzp1smkl0i1kycd8rgnlm7xdkzrc8y2bl3")))

(define-public crate-trianglo-0.1 (crate (name "trianglo") (vers "0.1.0") (hash "04788flrwyzjixlq6xsczm6jqs3haqynmqnk1r3y8bbx7krzb39p")))

(define-public crate-trianglo-0.1 (crate (name "trianglo") (vers "0.1.1") (hash "0iylgnpfcw22w1ml8r5s9cxdlcvwxk2bc0xa29fznnxdia04awvg")))

(define-public crate-trianglo-0.1 (crate (name "trianglo") (vers "0.1.2") (hash "1divyqqf5rgmdbwd83blrrhj7wmmzllx8b15smw2gbjyl4x2gl8c")))

(define-public crate-triangular-0.1 (crate (name "triangular") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (kind 0)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (kind 2)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "19bz1y7akvbm2m84knpxhz2jn2j2fkq26jsk63yl6pzfp2mq4pz6")))

(define-public crate-triangular-0.1 (crate (name "triangular") (vers "0.1.1") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (kind 0)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (kind 2)) (crate-dep (name "smallvec") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "068bdnhzp3qgpx13pg8j1yvnhcsyw6pqqh1r3x9xkm7arjl9lfa6")))

(define-public crate-triangular-earth-calendar-0.1 (crate (name "triangular-earth-calendar") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dateparser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "simplelog") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "0bsjllkjri20a0pa9vwm1cvvwy52x1dk2x4ya916ndmiah5nc5xs")))

(define-public crate-triangular-earth-calendar-0.1 (crate (name "triangular-earth-calendar") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dateparser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "simplelog") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "1v7fjg4n7c1mf9ilpiafnjxz2ydiqfw3f5k0a2i2zqhk5b0l6j9b")))

(define-public crate-triangular-earth-calendar-0.2 (crate (name "triangular-earth-calendar") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dateparser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("env-filter"))) (default-features #t) (kind 0)) (crate-dep (name "triangular-earth-calender-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1y26jslr15x7v68j6k4z8zf7g6k7gbs7fl54fg8yg8b9fvg74w30")))

(define-public crate-triangular-earth-calender-lib-0.1 (crate (name "triangular-earth-calender-lib") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dateparser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "152yaxizy4gb339ij6xgh5c24h0kxx68vl774g9hh466m4sqri8x")))

(define-public crate-triangulate-0.1 (crate (name "triangulate") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.58") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "text_trees") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zot") (req "^0.1") (default-features #t) (kind 0)))) (hash "11fvc1x1nr1jm4mg0ipyqf258srk8myak1x9cyi86a91rpyjx0ln") (features (quote (("default") ("debugging" "text_trees" "svg_fmt") ("benchmarking"))))))

(define-public crate-triangulate-0.2 (crate (name "triangulate") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.58") (default-features #t) (kind 0)) (crate-dep (name "earcutr") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "svg_fmt") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "text_trees") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zot") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1xppdbfgsd2rzl0a1g0ldlhvj36fx8a3rxi43vynfss9rjdysdzp") (features (quote (("default")))) (v 2) (features2 (quote (("_debugging" "dep:text_trees" "dep:svg_fmt") ("_benchmarking" "dep:earcutr"))))))

(define-public crate-triangulation-0.1 (crate (name "triangulation") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.20.1") (default-features #t) (kind 2)) (crate-dep (name "imageproc") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (kind 2)))) (hash "176pq711cn766f5c9fvvvs6hqbj0za8zws7vh54ri4xc2zsbyk3l") (features (quote (("parallel" "rayon") ("default"))))))

(define-public crate-triangulation-0.1 (crate (name "triangulation") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.20.1") (default-features #t) (kind 2)) (crate-dep (name "imageproc") (req "^0.17.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (kind 2)))) (hash "0ymkk6nr7r63hml27g6crlls78qp93994gafphsnby3rrlicshhd") (features (quote (("parallel" "rayon") ("default"))))))

