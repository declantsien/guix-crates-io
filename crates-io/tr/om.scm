(define-module (crates-io tr om) #:use-module (crates-io))

(define-public crate-trombone-0.1 (crate (name "trombone") (vers "0.1.0") (hash "101qr4brhmwwahl69lllxwgfyfkpgsngmiz0adragfpmy3jgwn8n")))

(define-public crate-trompt-0.0.1 (crate (name "trompt") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gka2kspaxnn1lifgrwh0pqngy4cfmq40jzd29wy393r9knn6jhc")))

(define-public crate-trompt-0.0.2 (crate (name "trompt") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b6flj1m7yck3wx50arj9v4w2wafb85wx12iip0qqnpk1wmdcbca")))

(define-public crate-trompt-0.0.3 (crate (name "trompt") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hy5hviz4slg2787jfww7lka2rywjga1mdzsmcrw108kw80zw92d")))

(define-public crate-trompt-0.0.4 (crate (name "trompt") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m96win54x223pimz9njq0f3qmvy1b457rl11nca8ghm6bvxcan4")))

