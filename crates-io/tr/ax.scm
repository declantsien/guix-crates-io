(define-module (crates-io tr ax) #:use-module (crates-io))

(define-public crate-trax-0.0.1 (crate (name "trax") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1930q192casrrskvcx6sc5066bj41xb7c5xndakh6ydy70xb7vdr")))

(define-public crate-traxex-0.1 (crate (name "traxex") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "1c9sy5cl8hg4nf1my6q1y5wllqj9arcgdbvdsfv83qwdc18gl432")))

(define-public crate-traxex-0.1 (crate (name "traxex") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "0r2dbw73xf27nkcf843cz49idgs06rqy4q01fs3bnhfb8jac5q1z")))

