(define-module (crates-io tr yv) #:use-module (crates-io))

(define-public crate-tryvial-0.1 (crate (name "tryvial") (vers "0.1.0") (deps (list (crate-dep (name "tryvial-proc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0izxgwf51aimqrlrcapgcx7q2sz63mjl7xmhajhw64sr3has22hh") (features (quote (("default" "proc-macro")))) (yanked #t) (v 2) (features2 (quote (("proc-macro" "dep:tryvial-proc"))))))

(define-public crate-tryvial-0.1 (crate (name "tryvial") (vers "0.1.1") (deps (list (crate-dep (name "tryvial-proc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "130wchhg68g62w7lg65rl3g9rn8c2av6cgx7jfrhq6fv0ih2fywv") (features (quote (("default" "proc-macro")))) (yanked #t) (v 2) (features2 (quote (("proc-macro" "dep:tryvial-proc"))))))

(define-public crate-tryvial-0.1 (crate (name "tryvial") (vers "0.1.2") (deps (list (crate-dep (name "tryvial-proc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1x1ml56jndvq8k7b7ybn59kggg9p0mzl4cmvhr6b3m6sfwl5zylk") (features (quote (("default" "proc-macro")))) (v 2) (features2 (quote (("proc-macro" "dep:tryvial-proc"))))))

(define-public crate-tryvial-0.2 (crate (name "tryvial") (vers "0.2.0") (deps (list (crate-dep (name "tryvial-proc") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vk4jhnq65zc00gcqqxs433wmi1vjrwpwykxzi22z9nm69pvxszd") (features (quote (("default" "proc-macro")))) (v 2) (features2 (quote (("proc-macro" "dep:tryvial-proc"))))))

(define-public crate-tryvial-proc-0.1 (crate (name "tryvial-proc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g9ibyxa5hn58sk2knfzjyk9nclr5plxa0ai53799phnys2qnd15") (yanked #t)))

(define-public crate-tryvial-proc-0.1 (crate (name "tryvial-proc") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18bh0gc9j257zk3cj0hqshr7pv91k9wzfxzy2d6ahys7zgqz09vg") (yanked #t)))

(define-public crate-tryvial-proc-0.1 (crate (name "tryvial-proc") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "venial") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1chis1774hzfx6pjny3n15z4falc4j02kggbflm4lwi5q2kkp9aa")))

(define-public crate-tryvial-proc-0.2 (crate (name "tryvial-proc") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "venial") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0jfis116bffy4kbbdzncqlkpd3agp7hygny4plkffnqxsxgj9mvq")))

