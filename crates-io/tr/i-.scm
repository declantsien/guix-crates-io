(define-module (crates-io tr i-) #:use-module (crates-io))

(define-public crate-tri-mesh-0.1 (crate (name "tri-mesh") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "0lcxqra7gnvw8yl3dm4x90n56jdyhgwshhncz110lsgny69r1w9d")))

(define-public crate-tri-mesh-0.1 (crate (name "tri-mesh") (vers "0.1.1") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "1wv78lyc1hrh2q1y10x38md3gm4vm5qmipi1wxi3yldiihr4phrc")))

(define-public crate-tri-mesh-0.1 (crate (name "tri-mesh") (vers "0.1.2") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "02kr2qy8b41ar7z5kcdknblaghc2mh0ybzknvagbpqsg329vbq1d")))

(define-public crate-tri-mesh-0.2 (crate (name "tri-mesh") (vers "0.2.0") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "dust") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "wavefront_obj") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "02dzy61l5j17d2ygzazf7vknam21qvs9y5546g09wgwks4glhsng")))

(define-public crate-tri-mesh-0.3 (crate (name "tri-mesh") (vers "0.3.0") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "dust") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "wavefront_obj") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "0vq6jzs5nhp4pivbz24q2brf00blcfxysh3rfw8lb2niy4hscxhs")))

(define-public crate-tri-mesh-0.4 (crate (name "tri-mesh") (vers "0.4.0") (deps (list (crate-dep (name "cgmath") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "dust") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "wavefront_obj") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "1950b3zijq5fdbnir6zxha746sjra2np3i1pskjlh6gvl8cv0fws")))

(define-public crate-tri-mesh-0.5 (crate (name "tri-mesh") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "dust") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wavefront_obj") (req "^5.1") (optional #t) (default-features #t) (kind 0)))) (hash "19g80s8f6kcbgv17p0y6qk46lxif2ddms35jan93y4w9fify1ihy") (features (quote (("obj-io" "wavefront_obj") ("default" "obj-io" "3d-io") ("3d-io" "serde" "bincode"))))))

(define-public crate-tri-mesh-0.6 (crate (name "tri-mesh") (vers "0.6.0") (deps (list (crate-dep (name "cgmath") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "three-d-asset") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "three-d-asset") (req "^0.4") (features (quote ("obj"))) (default-features #t) (kind 2)))) (hash "0khvs2al84i19fi8pga4wlbhnf8li3p8nwzkmsq0hf1fk7az6zl0") (features (quote (("default"))))))

