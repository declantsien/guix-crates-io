(define-module (crates-io tr iv) #:use-module (crates-io))

(define-public crate-trivet-1 (crate (name "trivet") (vers "1.0.0") (hash "1nwqdwwkdgabsfkj28kk5g3d0k0difdhm7ssw3a0fl8pqiarff31") (features (quote (("strict"))))))

(define-public crate-trivet-1 (crate (name "trivet") (vers "1.0.1") (hash "09lzikimrjbz5x601aw6m8zqxyh950ar02i598ar63bs71g5dvys") (features (quote (("strict"))))))

(define-public crate-trivet-1 (crate (name "trivet") (vers "1.0.2") (hash "0q1n3srjsflj8z9vkvlvn8im8lls3598d77c219qra8rzy0p12lw") (features (quote (("strict"))))))

(define-public crate-trivet-1 (crate (name "trivet") (vers "1.0.3") (hash "0r2vk5hmblsdbp3g6fvakgjwxr7r49kqz8a5mqc6drlai6mpd4qj") (features (quote (("strict"))))))

(define-public crate-trivet-1 (crate (name "trivet") (vers "1.1.0") (hash "0i4fmwsv3phmy34mafbxqqcj54pglf36anr0p0psbzg3h0gf3mv6") (features (quote (("strict"))))))

(define-public crate-trivet-1 (crate (name "trivet") (vers "1.1.1") (hash "1ch2lh8s09sgnwdfy38dl30ny8n7lrmbalhjqv0lxag9biaipb5i") (features (quote (("strict"))))))

(define-public crate-trivet-2 (crate (name "trivet") (vers "2.0.0") (hash "02dkjb5cj1081b4mady1fmvif3n0722jvnsccw21crpd48cxx2ss") (features (quote (("uppercase_hex") ("strict") ("no_ucd") ("no_tracking") ("no_stall_detection") ("legacy") ("default" "legacy"))))))

(define-public crate-trivet-2 (crate (name "trivet") (vers "2.0.1") (hash "0brvda51148lvmgv99cxinh670j9nlj91vwdafn823j7334fm454") (features (quote (("uppercase_hex") ("strict") ("no_ucd") ("no_tracking") ("no_stall_detection") ("legacy") ("default" "legacy"))))))

(define-public crate-trivet-2 (crate (name "trivet") (vers "2.0.2") (hash "19nza3nr04b93vaw7qyl1b7swpybgkysf0mvf0njfhb2ssvlax3f") (features (quote (("uppercase_hex") ("strict") ("no_ucd") ("no_tracking") ("no_stall_detection") ("legacy") ("default" "legacy"))))))

(define-public crate-trivial-argument-parser-0.1 (crate (name "trivial-argument-parser") (vers "0.1.0") (hash "0k2mmn11b3prgwf7pc555n2s2rvm95v1yga7d8hvkhsz0qhwijii")))

(define-public crate-trivial-argument-parser-0.2 (crate (name "trivial-argument-parser") (vers "0.2.0") (hash "1xzx35pp55g07gg7h3n69qvqq2877zcf2dlzqf59z1rql8cq384p")))

(define-public crate-trivial-argument-parser-0.3 (crate (name "trivial-argument-parser") (vers "0.3.0") (hash "1v70n94fqls6nzwb2h01bhzywyn0bprmmx365gzmcxczp3py853l")))

(define-public crate-trivial-argument-parser-0.3 (crate (name "trivial-argument-parser") (vers "0.3.1") (hash "1xm89cyj4vhssrfqw1db43qzk80ps9xppria8q8ljrcimyhjf24q")))

(define-public crate-trivial-compiler-0.1 (crate (name "trivial-compiler") (vers "0.1.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1qdzadzjdksr9vy76hskwamjv8wp40gckicsgwfp7vg3mhmf3d5r")))

(define-public crate-trivial-compiler-0.2 (crate (name "trivial-compiler") (vers "0.2.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lwhck9ws97nshdjq8xlvfc14g1vy0vyqxapa21zmx3r5r8495gx")))

(define-public crate-trivial-compiler-0.3 (crate (name "trivial-compiler") (vers "0.3.0") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0lvvmik5j517nqvvfyqgq6axzm0lrlcc544svm8pxmx3z26i4s8v")))

(define-public crate-trivial-kernel-0.1 (crate (name "trivial-kernel") (vers "0.1.0") (hash "1npa184r4qmslnbkfmxi0749ls5qp5smf8g63qr9n27hz7qp22yw")))

(define-public crate-trivial-kernel-0.2 (crate (name "trivial-kernel") (vers "0.2.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1fidi1z5sxap8vym5604cq65714dns8fdimhfck2ibalp65zz655")))

(define-public crate-trivial-kernel-0.3 (crate (name "trivial-kernel") (vers "0.3.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0f46cdy1kpzd9pa4wc2vq21y5dirsfmg4nqap834syff5813iqbd")))

(define-public crate-trivial-kernel-0.4 (crate (name "trivial-kernel") (vers "0.4.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1nz5jylkkj655f015a1fnlcd4smsmjvyxa82fbv18ry7x0fyxd0v")))

(define-public crate-trivial-kernel-0.5 (crate (name "trivial-kernel") (vers "0.5.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ydk72wm9icfp0zi8pp4cnxba9krrglnmxxcpc4wa6qdv0xplq30")))

(define-public crate-trivial-kernel-0.6 (crate (name "trivial-kernel") (vers "0.6.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1pf3rkvj8gghj6h4qwzjmlya82v4d9lqb70bvkhpja30d76flaka")))

(define-public crate-trivial-kernel-0.7 (crate (name "trivial-kernel") (vers "0.7.0") (deps (list (crate-dep (name "mmb-types") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0fx0j6mvxrf978xaz5r6mcnaj6bm13bzs1xrrldrnxrh8f0q23fv")))

(define-public crate-trivial-kernel-0.8 (crate (name "trivial-kernel") (vers "0.8.0") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "14ipqz3y1lx6xk5m3r3vqis44zx91rna0y493bnbxjlgvlbhdr4v")))

(define-public crate-trivial-kernel-0.8 (crate (name "trivial-kernel") (vers "0.8.1") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1jgfj2qr7wmhdvp4nbr29ir418blk5l56y1xv35jk5ix69l576hi")))

(define-public crate-trivial-kernel-0.9 (crate (name "trivial-kernel") (vers "0.9.0") (deps (list (crate-dep (name "mmb-types") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n63j9dx6yacsc4i0pca9lzzykddq89l24567ik13ndvnld2rw73")))

(define-public crate-trivial-verifier-0.1 (crate (name "trivial-verifier") (vers "0.1.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vww656kdnba4pqf50qlhazxlb3i18c5hnb2zkzlsbsd82n1iy9h")))

(define-public crate-trivial-verifier-0.2 (crate (name "trivial-verifier") (vers "0.2.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-compiler") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0s4f1l9hl7j2h4prhglcacpgjbi5hw6sxb5wz7jb0f5zjp7vvyxg")))

(define-public crate-trivial-verifier-0.3 (crate (name "trivial-verifier") (vers "0.3.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-compiler") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0mfhwih4prxbfb9n6348szrisyn46dba13cl4fxqx6s1ddqk9fpd")))

(define-public crate-trivial-verifier-0.4 (crate (name "trivial-verifier") (vers "0.4.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-compiler") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "04c2ammaqsnbqv2f11zdjqaa5p5dq7vj2dhg5xls4sgy3w0j7imz")))

(define-public crate-trivial-verifier-0.5 (crate (name "trivial-verifier") (vers "0.5.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-compiler") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0dvrmwpl0hdkz979syk2cw0h6ydp96wjysninlvxxhs7rg2zpbzb")))

(define-public crate-trivial-verifier-0.6 (crate (name "trivial-verifier") (vers "0.6.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-compiler") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "06hv9n0r6lpkalylnqyipg1smaf9jib8v7clq87ghdk9j45a5in6")))

(define-public crate-trivial-verifier-0.7 (crate (name "trivial-verifier") (vers "0.7.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-compiler") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1r1wawd1xa7s293gs5x4ng63gzb5rzc9n8q44i604gjamgm0kmph")))

(define-public crate-trivial-verifier-0.8 (crate (name "trivial-verifier") (vers "0.8.0") (deps (list (crate-dep (name "mmb-parser") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-compiler") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "trivial-kernel") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "10j5c96n1ppdwmjfqdzls33jzx774zy0yj8m53z2rzg33ksj490y")))

(define-public crate-trivial_colours-0.1 (crate (name "trivial_colours") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0csyc1hf0mg4alr82q87gznrh74hpcvg71c9rkz0yyr48zw1kgcn") (yanked #t)))

(define-public crate-trivial_colours-0.1 (crate (name "trivial_colours") (vers "0.1.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1dc1myjg73zawdmisadn0yns79cpmkg6hnwad6ixa76i496nwc18")))

(define-public crate-trivial_colours-0.2 (crate (name "trivial_colours") (vers "0.2.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1a4519f7crp7i3y2bkysnk72pvq3h0z232y80zam5ss6i2m2dpq3") (features (quote (("use_winapi" "kernel32-sys" "lazy_static" "winapi") ("default" "use_winapi"))))))

(define-public crate-trivial_colours-0.2 (crate (name "trivial_colours") (vers "0.2.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0sg8b1j6pygfjv8ijx6xfd5hxkqvlz0pjsbfz5al1ki9wjqrfd45") (features (quote (("use_winapi" "kernel32-sys" "lazy_static" "winapi") ("default" "use_winapi"))))))

(define-public crate-trivial_colours-0.3 (crate (name "trivial_colours") (vers "0.3.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (optional #t) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0n5lddjkj1hkh876g7y4j0fv59ir6frs2kdwxvi0qnkcl5g3clvi") (features (quote (("use_winapi" "kernel32-sys" "lazy_static" "winapi") ("default" "use_winapi"))))))

(define-public crate-trivialdb-0.1 (crate (name "trivialdb") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0v3vjijg1nlg0h38pb7cclnshpf71nnmhdzp7r7mk8nc4v2d431y")))

(define-public crate-trivialdb-0.1 (crate (name "trivialdb") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req ">=0.65") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0nndfag9rgswrjp77cnsqmhxwdfapv7j0qpz13vq0wgw9q092hss")))

(define-public crate-trivialdb-0.1 (crate (name "trivialdb") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req ">=0.65") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0v5ax94lkk84b2idjrbs8npb9yy62w33dp46pbsp500l90npcydr")))

(define-public crate-trivialdb-0.1 (crate (name "trivialdb") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req ">=0.65") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.1.1") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "12nxwg91ph6ii54ns1j5p5vm673vkbpknjyyx2bd8q86zhxwmw3y")))

(define-public crate-trivialdb-0.1 (crate (name "trivialdb") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req ">=0.65") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.1.1") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "18cc1dglphxq37s6ds8brxwjhzd3mb8ly9ppakjawn11p0vmyiy8")))

