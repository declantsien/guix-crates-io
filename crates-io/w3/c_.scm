(define-module (crates-io w3 c_) #:use-module (crates-io))

(define-public crate-w3c_validators-0.1 (crate (name "w3c_validators") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "019g0w4l9ra9ziq3m8c0hjkd6pgn5rsmy110kl56rcazj1s6s25d")))

