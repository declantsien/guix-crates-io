(define-module (crates-io w3 #{2-}#) #:use-module (crates-io))

(define-public crate-w32-error-1 (crate (name "w32-error") (vers "1.0.0") (deps (list (crate-dep (name "winapi") (req "^0.3.0") (features (quote ("errhandlingapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0hzvz97zfd3crz8p3ydnir2nn3rl4mbrhw615z0niqcipnk62z7s") (features (quote (("std" "winapi/std"))))))

