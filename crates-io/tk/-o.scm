(define-module (crates-io tk -o) #:use-module (crates-io))

(define-public crate-tk-opc-0.1 (crate (name "tk-opc") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "125k1h0d2n29hbi8zvbzpb5p93r0r330mly9yy5rv5hqv9884sm8")))

(define-public crate-tk-opc-0.1 (crate (name "tk-opc") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jaawd8sm416fn2rji9jfpx3ww1agvnxrh22wzcy4gxyxl5h2xpp")))

(define-public crate-tk-opc-0.1 (crate (name "tk-opc") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qk5cv8m1mhhpav5gsadvaiscrsplgkv9i25clhm7qcy6bz2375q")))

(define-public crate-tk-opc-0.1 (crate (name "tk-opc") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "02l60a6cihs0b6n7ff76l5hx3frhpkgsqr9lsrz0a1vl9k5xyj0g")))

