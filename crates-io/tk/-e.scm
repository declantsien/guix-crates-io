(define-module (crates-io tk -e) #:use-module (crates-io))

(define-public crate-tk-easyloop-0.1 (crate (name "tk-easyloop") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "scoped-tls") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "040z8adkmsndn5prrhfq4yfyv8p2cqdr3dksxcqcj9lyhr14jgha")))

(define-public crate-tk-easyloop-0.1 (crate (name "tk-easyloop") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "scoped-tls") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ccaqkxjav5digd53hhm5bgiyj7pjvqan8k1yhpyrh3g907106i9")))

