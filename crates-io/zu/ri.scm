(define-module (crates-io zu ri) #:use-module (crates-io))

(define-public crate-zuri_nbt-0.3 (crate (name "zuri_nbt") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1gk8r382jpgckiknlnya5xmflv8fq4zwh5al0p5mi6r34lwvm0hr")))

