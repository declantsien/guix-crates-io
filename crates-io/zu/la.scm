(define-module (crates-io zu la) #:use-module (crates-io))

(define-public crate-zula-0.0.7 (crate (name "zula") (vers "0.0.7") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0hszhy1iz0db7l9a3iz6gqphdrgd8i681xj5fwzyjfmd56nwgz4v")))

(define-public crate-zula-0.0.8 (crate (name "zula") (vers "0.0.8") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1xsdnvp6qdm4jw98j5brxamwmlz499b8mpk38d4awj56kpfi611l")))

(define-public crate-zula-0.0.10 (crate (name "zula") (vers "0.0.10") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "02zhzbyq2gk8k33kzidyrnqbgqdzgsc7p3dz059imxkzyxcwa3j5")))

(define-public crate-zula-0.1 (crate (name "zula") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "00854ay78yv6hck2s1yxsk5f47lkixx9cygvapn99agf3ni744hv")))

(define-public crate-zula-0.1 (crate (name "zula") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "0vw480jjcrp79k5i99fmqr7n4wwpv5jiik6vajgqxfj548jzbs0v")))

(define-public crate-zula-1 (crate (name "zula") (vers "1.0.2") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1cvjir4brcf9jqsacnpzbmqzpz9hlg6gigw62z005hh97wmwpzri")))

(define-public crate-zula-1 (crate (name "zula") (vers "1.0.3") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "10q1vdl83jh5mmngpnrwcff5gf7ciqf7vss6b3z06j37l1yh6f94")))

(define-public crate-zula-1 (crate (name "zula") (vers "1.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "08sfhn71km4vijkiyf5sp6b12j2423wz0azj8f2l9vi49965dg27")))

(define-public crate-zula-2 (crate (name "zula") (vers "2.0.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0s01x7ngm1ywqgzvpgf9p6a7lvd1f09fgx326gjhrpwm9p6rk0fy")))

(define-public crate-zula-2 (crate (name "zula") (vers "2.0.1") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0zqa90y5yhd7gqdbarfqwyqgy3gk8hj89944h64qbz80kga2ymyc")))

(define-public crate-zula-2 (crate (name "zula") (vers "2.0.2") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0y0d5x4r8byrgi2lnlk96ndbqd0rkfi6x2cbhqzd3bxa23zphjgk")))

(define-public crate-zula-2 (crate (name "zula") (vers "2.0.3") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "zula-core") (req "^3.0.2") (default-features #t) (kind 0)))) (hash "1jzh4279a3c6bmssn59x7aq3dp65qgv5l130as4nf4andln0c42q")))

(define-public crate-zula-core-0.0.8 (crate (name "zula-core") (vers "0.0.8") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "19sjbp5lzs6bj75nnj1m8j4cg2lbl2czj6wa7hm6ml6cwncv9mrv") (yanked #t)))

(define-public crate-zula-core-0.0.9 (crate (name "zula-core") (vers "0.0.9") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0zfzw6lkn71g9pgys19wgjb2yz2w10281p3a5lbximwvljh6ryxd") (yanked #t)))

(define-public crate-zula-core-1 (crate (name "zula-core") (vers "1.0.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1f0gqbfib3wsk9xmcpc95vn8lnyc5w6r060ai3ix3kd0qw7l57b1") (yanked #t)))

(define-public crate-zula-core-2 (crate (name "zula-core") (vers "2.0.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1ncwxbgy4g0z6qrmf6wiqxvzcacmslyqgza90a1bgr4knd9n3lcs") (yanked #t)))

(define-public crate-zula-core-2 (crate (name "zula-core") (vers "2.0.1") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0cs7qfcjs0bmsq372dj0z5zbm5dlzq9f0g4mv492a0z2h3d5h8fv") (yanked #t)))

(define-public crate-zula-core-3 (crate (name "zula-core") (vers "3.0.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1d5ng8ixzm8xds6g2sdkdiqwbwq3rm7ahjkpyidigrjwsdrlpghb") (yanked #t)))

(define-public crate-zula-core-3 (crate (name "zula-core") (vers "3.0.1") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "03xqly9ic7n2rjmq5y72ch1bfdmh9l9w9i4yrbbjs3q1nxsa3ga5") (yanked #t)))

(define-public crate-zula-core-3 (crate (name "zula-core") (vers "3.0.2") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1y2i2lca2m4wf4ps8iglaz72c6nq10xzn9ix2sbg3r1v5bynrkp9") (yanked #t)))

(define-public crate-zula-core-3 (crate (name "zula-core") (vers "3.0.3") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "041jzdkqfsxxcb5vhhgiyjm498wdbnvlgwjjilpdzq22wfdb0gxn") (yanked #t)))

(define-public crate-zula-core-3 (crate (name "zula-core") (vers "3.0.4") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "18g3jp9a3qagj51bcvzky8cva6c6f3j7qjrhbbg3c2gdvc7v6yhm") (yanked #t)))

(define-public crate-zula-core-3 (crate (name "zula-core") (vers "3.0.5") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "12knwh5pnarhb9fh7ya6y7qbqsvw8f4946b6nhxzd9h923p7cnrg") (yanked #t)))

(define-public crate-zula-core-4 (crate (name "zula-core") (vers "4.0.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0scj49wd67nbhbppbigwhflxdp4iqw0y1d5sdxdbqa12095pdvrv")))

