(define-module (crates-io zu i-) #:use-module (crates-io))

(define-public crate-zui-core-0.0.1 (crate (name "zui-core") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hi2651xq6ql2a21w3j3j1phsqpxziyywkr9szp3g1xbdkqqp0w9")))

(define-public crate-zui-core-0.0.2 (crate (name "zui-core") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0naaz35qlikwmc23hbsq66ng56l44igysil089gki3jydfm2gxfs")))

(define-public crate-zui-core-0.0.3 (crate (name "zui-core") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kfvhd6x5bh2bbkjam4nw7l5qqjjh2517wfi90hk4xz4jr5gdzkk")))

(define-public crate-zui-runtime-0.0.0 (crate (name "zui-runtime") (vers "0.0.0") (hash "0gz5rfyvf23rxfqv60d61jw11byz7pksflfg7rvrd2yc6gp12ycc")))

(define-public crate-zui-shared-0.0.0 (crate (name "zui-shared") (vers "0.0.0") (hash "0akhbc6vs9zz6lhmdlqn19bph9ghl7wlghdwyfyb7ada6d9j7lia")))

(define-public crate-zui-web-view-0.0.0 (crate (name "zui-web-view") (vers "0.0.0") (hash "04bdycp61wf36f85ra34ziv5dcq5cfibikm68wp51788fb631jpm")))

(define-public crate-zui-widgets-0.0.1 (crate (name "zui-widgets") (vers "0.0.1") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "cassowary") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libzui") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "07danngp3nbmvsvj3zwq298rarpzh080dhmf6s0jnwf4ay2kzwbf")))

(define-public crate-zui-widgets-0.0.2 (crate (name "zui-widgets") (vers "0.0.2") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "cassowary") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zui-core") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0cklkpz08vm374pgamdh13q8nhlrijlhmv5wisd77zgl4bnhb0r0")))

(define-public crate-zui-widgets-0.0.3 (crate (name "zui-widgets") (vers "0.0.3") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "cassowary") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zui-core") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1fnyqh1bwk3376749axsbkhbg00v3v4c0zm3ymk0q0635hykm09a")))

(define-public crate-zui-window-0.0.0 (crate (name "zui-window") (vers "0.0.0") (hash "1myqabwc85d2zyb8yhzngwsw3zwg4k1mmf460h4ws3sj943fyzxv")))

