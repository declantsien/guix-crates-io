(define-module (crates-io zu mm) #:use-module (crates-io))

(define-public crate-zummi-0.1 (crate (name "zummi") (vers "0.1.0") (hash "0ax7fyq9gbgnf364zqsfb1l5y3dxnsc4c5wmmnyxg8plr9ih016x")))

(define-public crate-zummi-0.1 (crate (name "zummi") (vers "0.1.1") (hash "1z1y5d51dd62rjgc95a9gfsrllj2laybkxpmrmybgsvv98nzh1rn")))

(define-public crate-zummi-0.1 (crate (name "zummi") (vers "0.1.2") (hash "1n7nzkc74gc70vaby43yd621sy97pqy50kpx4dyxbazisr5gdv5c") (features (quote (("nightly"))))))

