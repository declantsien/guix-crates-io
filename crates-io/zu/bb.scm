(define-module (crates-io zu bb) #:use-module (crates-io))

(define-public crate-zubbers-0.0.1 (crate (name "zubbers") (vers "0.0.1") (deps (list (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "flame") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "flamer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "im-rc") (req "^14.3.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 2)))) (hash "0s8v89r52l879xvkp5q4w0xy3g2pqqww5n4lf3fchj1b55p5ysry")))

