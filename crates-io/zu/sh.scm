(define-module (crates-io zu sh) #:use-module (crates-io))

(define-public crate-zush-0.1 (crate (name "zush") (vers "0.1.0") (hash "1bi3sdbgpf5nv4q8wm5r8h6qlgiw4dimfscp074vv8cpcc575i8w") (yanked #t)))

(define-public crate-zush-0.0.0 (crate (name "zush") (vers "0.0.0") (hash "14i1zzz4zdkpx1cma9fw04iak64ziaj47z3w9nxl3jyx198c127v") (yanked #t)))

