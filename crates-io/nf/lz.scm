(define-module (crates-io nf lz) #:use-module (crates-io))

(define-public crate-nflz-0.1 (crate (name "nflz") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0lk5q0izrgvgc06m1yg2mb1kc8kyalj6bgqqlmvj4w4xwbcnb7mx")))

(define-public crate-nflz-0.1 (crate (name "nflz") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1q3x9gkg090iqxr5nggk63z9knzambbb5sxa0ig86rafpdrl93cw")))

(define-public crate-nflz-0.1 (crate (name "nflz") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1vabshjm654xwb3sj3h3b0x6c0sn6hzank0gx44j9fsrw9grbbka")))

(define-public crate-nflz-1 (crate (name "nflz") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "189xqagj7m9zky54s1gc2v6rhhiknl5z1k7fbs728b0979n3kwk3")))

(define-public crate-nflz-1 (crate (name "nflz") (vers "1.0.1") (deps (list (crate-dep (name "fs_extra") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "15gm3lj6s0rl021aw2zxs6raz5prfg91qyrxwlpd62n1clijr525")))

(define-public crate-nflz-1 (crate (name "nflz") (vers "1.0.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2") (default-features #t) (kind 2)))) (hash "164i9bg86mywmxwkwbz9izxwb8dkrn0fqq5081cck1vrd5kx37sb")))

