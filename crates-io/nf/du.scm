(define-module (crates-io nf du) #:use-module (crates-io))

(define-public crate-nfdump-0.1 (crate (name "nfdump") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "bzip2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "minilzo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1b3hnl354ki214w08kp4zpr1xcgz7hbdj4l25maj8421c4fizdwd")))

