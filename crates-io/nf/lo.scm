(define-module (crates-io nf lo) #:use-module (crates-io))

(define-public crate-nflog-0.9 (crate (name "nflog") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0fjvrjgg44w91461iwac8yl27fr4ydgvqcvgsr6vzw144pj04lc3")))

(define-public crate-nflog-sys-0.1 (crate (name "nflog-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.99") (default-features #t) (kind 0)) (crate-dep (name "nfnetlink-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1vbzav3mrvcnh0x1zh0klfyl4kqd1xhq9bpav38nasi7y715zixr") (links "netfilter_log")))

