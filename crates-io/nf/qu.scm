(define-module (crates-io nf qu) #:use-module (crates-io))

(define-public crate-nfqueue-0.9 (crate (name "nfqueue") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.12.0") (default-features #t) (kind 2)))) (hash "12kcqqfskznb397crnbg91rg32dgwyhspz9n0qgplc2djdff88fm")))

(define-public crate-nfqueue-0.9 (crate (name "nfqueue") (vers "0.9.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.17.1") (default-features #t) (kind 2)))) (hash "1jhv24qdknj0gkgvvajk1q7cvi3v66zni27ynbk78kxfcrp6k146")))

