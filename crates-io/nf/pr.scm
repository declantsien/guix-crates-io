(define-module (crates-io nf pr) #:use-module (crates-io))

(define-public crate-nfprobe-0.0.1 (crate (name "nfprobe") (vers "0.0.1") (deps (list (crate-dep (name "elf_rs") (req "<=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13nk5z1ipc4kvk50ahhl3dbhr2jz5pl77v1hbx5l7487w47mvlyj")))

