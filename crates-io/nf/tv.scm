(define-module (crates-io nf tv) #:use-module (crates-io))

(define-public crate-nftver-0.1 (crate (name "nftver") (vers "0.1.0") (deps (list (crate-dep (name "paq") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1386pyiagz2wj5zlxywv9x0js9d65xdd6rriwsdkj9h3g5rnj6qp")))

