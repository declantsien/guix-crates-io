(define-module (crates-io nf d2) #:use-module (crates-io))

(define-public crate-nfd2-0.1 (crate (name "nfd2") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0vydkm0nw9cldy9yxxxsig9lpsdgwgbm4fy472yd28pfkxsvdnic")))

(define-public crate-nfd2-0.1 (crate (name "nfd2") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "09i67w5ximwn9l7jc1849xqnr539m8wcmpwwnwmvwd1kxyfsivhw")))

(define-public crate-nfd2-0.2 (crate (name "nfd2") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1pc6m25jccfraxa3y0rx21yrqprl4djfzx5k2qr6hd4ifqwgd02c")))

(define-public crate-nfd2-0.2 (crate (name "nfd2") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "15lzca5qqd544wcq7cpy5zic1gxn8d5hcici9608cranqw5xphqa")))

(define-public crate-nfd2-0.2 (crate (name "nfd2") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1k37x05nl4ind13d1wyglgjn578jq2znirsqgrcl9d48yc3kl3h4")))

(define-public crate-nfd2-0.2 (crate (name "nfd2") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1mahfa79jms7kjw84kmlliqd0bglihk8vwsvn28ghrydr8gdrnyf")))

(define-public crate-nfd2-0.3 (crate (name "nfd2") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0xbzx5xkwndm9vl1x87gi7anxisd5wbgrw4c7b6widwczzsjfq9h")))

(define-public crate-nfd2-0.3 (crate (name "nfd2") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "19gsxw7sl2axpj4wfz387iayxns6df8akp43x7ammf5bv6kg20a0")))

