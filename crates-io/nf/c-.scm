(define-module (crates-io nf c-) #:use-module (crates-io))

(define-public crate-nfc-device-0.0.0 (crate (name "nfc-device") (vers "0.0.0-unreleased") (hash "0s4zhawnprv3l8xvgr1akkwp0dzls938h6yhd82y8ramd47v1a2m")))

(define-public crate-nfc-oath-0.1 (crate (name "nfc-oath") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nfc") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "12x0bd4rpcv4xln01b8fz90sginx7ljnyj292f7cj6hn53w3h6rn")))

(define-public crate-nfc-oath-0.2 (crate (name "nfc-oath") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "nfc") (req "^0.1.11") (default-features #t) (kind 0)))) (hash "1vzl3w4rlvd3ard258vwn12xwjnd6dpkgk1yak15b6m1m1ibcqxw") (links "nfc")))

(define-public crate-nfc-rs-0.1 (crate (name "nfc-rs") (vers "0.1.0") (hash "1fqji3adv2hx9ck75j2m2b9hdyiw0kgq4j20y1bm9vh8qcz1ni98")))

(define-public crate-nfc-sys-0.1 (crate (name "nfc-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04f2cx30sy6aivcli9ppl3k322a36h3n3apjhvmsb6p6qj0wraby")))

(define-public crate-nfc-sys-0.1 (crate (name "nfc-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "183srb563dsmmgbqcj1rnnabw3bhkndyb8hdkkg0fwwm79adf9kv")))

(define-public crate-nfc-sys-0.1 (crate (name "nfc-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1085pavr4lnfp5hnxh9z0m0nribfmp1grkjfr95ma56irv956fqd")))

(define-public crate-nfc-sys-0.1 (crate (name "nfc-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ygpw1886027rsmhxs1i7qdpfab6gm06fc1vjz4y5hv46s2zm3d7")))

(define-public crate-nfc-sys-0.1 (crate (name "nfc-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02k6x8l06qwhxhz8i237q1ac2h4p8lhjfk2v9bjkf2100gq35a02")))

(define-public crate-nfc-sys-0.1 (crate (name "nfc-sys") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "13nxzhcsa8qr44vcnzgxl306nsw59dx7q6yznwwk41j10yj1ms4q")))

