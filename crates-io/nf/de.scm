(define-module (crates-io nf de) #:use-module (crates-io))

(define-public crate-nfde-0.0.1 (crate (name "nfde") (vers "0.0.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0b5qx9syvxkwl625ws6g48di4bzjfpz1nypxdi37142a40vav1hx")))

(define-public crate-nfde-0.0.2 (crate (name "nfde") (vers "0.0.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0vk2y7z9ny5iimapnz8k834hbv0rgc0qs9n15zkj2pm5n04lb5gw")))

(define-public crate-nfde-0.0.3 (crate (name "nfde") (vers "0.0.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0zg0x4pvgb71kz2fk3cl3y3hdj15jm0l6hqc0ziyn5zjqb1bm1gh")))

(define-public crate-nfde-0.0.4 (crate (name "nfde") (vers "0.0.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "027lvc4s98zddfiryq9119x058k2frxyhv4hw3g6xymhzx0vdq65")))

(define-public crate-nfde-0.0.5 (crate (name "nfde") (vers "0.0.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^0.5") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0vchrn0qgdmwak7cpild5ydx883lxxwpgq0qx9ik14dwmz9igfl3")))

(define-public crate-nfde-0.0.6 (crate (name "nfde") (vers "0.0.6") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^0.5") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "04rjap43amdfl4r7lcwf2zi2qxxwg1av38hh2s72jv56zg7k7mlv") (yanked #t)))

(define-public crate-nfde-0.0.7 (crate (name "nfde") (vers "0.0.7") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0nxq1x4pvvn8q425zyxkzr22ikk0zs235rfwdikv1mghm0w7pbgw") (yanked #t)))

(define-public crate-nfde-0.0.8 (crate (name "nfde") (vers "0.0.8") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "widestring") (req "^1.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "03qkcjfzl6w86r2hi2klydlaw4zasi7n8601xfc8q6l6hxarv1ln")))

