(define-module (crates-io nf ne) #:use-module (crates-io))

(define-public crate-nfnetlink-sys-0.1 (crate (name "nfnetlink-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.99") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1jh6287fkp2ns6w3k2as4hrjz5q9y34v0iakvnvzzhn06kf2kn0n") (links "nfnetlink")))

