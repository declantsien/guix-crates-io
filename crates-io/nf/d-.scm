(define-module (crates-io nf d-) #:use-module (crates-io))

(define-public crate-nfd-sys-0.1 (crate (name "nfd-sys") (vers "0.1.3") (deps (list (crate-dep (name "cocoa") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "gtk") (req "^0.0.7") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1spp11g9i47bwjv0vwq00v6892wcb1rf7infizakn5s4y5k1jhny")))

(define-public crate-nfd-sys-0.1 (crate (name "nfd-sys") (vers "0.1.4") (deps (list (crate-dep (name "cocoa") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "gtk") (req "^0.0.7") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1lzz28g33a3nh5ypjxcsl1k1qxlpx04hrnqm4x14s5ncfqna2h8s")))

(define-public crate-nfd-sys-0.1 (crate (name "nfd-sys") (vers "0.1.5") (deps (list (crate-dep (name "cocoa") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "gtk") (req "^0.0.7") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "18xjvcsy6nh445xb7yd7ilizzffm6vphy604qbpcgbpd6r397iaz")))

