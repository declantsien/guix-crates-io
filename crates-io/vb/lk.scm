(define-module (crates-io vb lk) #:use-module (crates-io))

(define-public crate-vblk-0.1 (crate (name "vblk") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mgb1mmphlln82mx8w59nxnx8521z743sdqcgpdy0iklr67hkr35")))

(define-public crate-vblk-0.1 (crate (name "vblk") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rjy6sjk9i7cazcf54qp33firydfkgy4zh26i3s2cvsjwb6h34yj")))

(define-public crate-vblk-0.1 (crate (name "vblk") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1") (default-features #t) (kind 2)) (crate-dep (name "nix") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.2") (default-features #t) (kind 0)))) (hash "1n8vq21f02206sbpnzcfw109n1gc6xic6xahp4nzgpvlk71qfqla")))

