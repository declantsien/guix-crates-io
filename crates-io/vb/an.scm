(define-module (crates-io vb an) #:use-module (crates-io))

(define-public crate-vban-0.1 (crate (name "vban") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0ja8ziwkpfyjmrckvamd8m6z48pn2mv5n2vw92jcjrlr7nv1h28s")))

