(define-module (crates-io vb ox) #:use-module (crates-io))

(define-public crate-vbox-0.1 (crate (name "vbox") (vers "0.1.0") (hash "16rhmswa5ac7n6krqikgp33aa3g4dabpmzl34sbx0javql8kf24h")))

(define-public crate-vboxhelper-0.1 (crate (name "vboxhelper") (vers "0.1.0") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "0zcr7wnkz2432bw4rzjwfdwqa8z4i5mzc3qg9v6qwmlhfclaj286")))

(define-public crate-vboxhelper-0.1 (crate (name "vboxhelper") (vers "0.1.1") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "1bbl8jw5q2l4ynwj76smmp0f3il7jbz46vkaz0hkamwspqsg2c8y")))

(define-public crate-vboxhelper-0.1 (crate (name "vboxhelper") (vers "0.1.3") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "0img87197fr9bg8p50pg4ypmcvnl2xqpfiw5x8lh2356nk86kynq")))

(define-public crate-vboxhelper-0.1 (crate (name "vboxhelper") (vers "0.1.6") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "0gddd3gzn0m02a3nmdhc08gnbjd55sh4smm8lp61sprnv29xqzky")))

(define-public crate-vboxhelper-0.1 (crate (name "vboxhelper") (vers "0.1.8") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "1ygvd7mxr3cbb4knyg7c9vd6ypddy98laz1b0l8320apiwvxdabl")))

(define-public crate-vboxhelper-0.2 (crate (name "vboxhelper") (vers "0.2.0") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "1y2a91piydqg4i3ilh5wc5vdlvp2zc12kj665p0a4ana6w127dvm")))

(define-public crate-vboxhelper-0.3 (crate (name "vboxhelper") (vers "0.3.0") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "1rfw5q0656zv0y4my71hdb56ig4z4498d16zn949wygvc8zmamjh") (yanked #t)))

(define-public crate-vboxhelper-0.3 (crate (name "vboxhelper") (vers "0.3.1") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "05mqc812b6bkbwfsf5027bv4k6hjd640n1blk5dhi903zdsxxgz9") (yanked #t)))

(define-public crate-vboxhelper-0.3 (crate (name "vboxhelper") (vers "0.3.2") (deps (list (crate-dep (name "eui48") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "01nl1kbz8fh6lqb3c2990rq44pd4mfl8x3rh285l7j7hzghxl73c")))

(define-public crate-vboxhelper-0.3 (crate (name "vboxhelper") (vers "0.3.3") (deps (list (crate-dep (name "eui48") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1kbknpcbnjp4dhcmlzahvpmf0nrlbhs4p1yfqgcwmw2g5s1hxqff")))

