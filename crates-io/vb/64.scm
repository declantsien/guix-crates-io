(define-module (crates-io vb #{64}#) #:use-module (crates-io))

(define-public crate-vb64-0.1 (crate (name "vb64") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "15a325s1yjvridn69wxq2jihank02sfgcy1aszirdmppnm5byf75")))

(define-public crate-vb64-0.1 (crate (name "vb64") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "09bjpq30csj221j20rdh37k48f80x5pd156n6y10r17ps1qyhz1y")))

(define-public crate-vb64-0.1 (crate (name "vb64") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "09y761141qisrxh16q250gqq8jl84lr3wjqxh06f9x8ps97s89na")))

