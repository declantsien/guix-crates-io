(define-module (crates-io k5 #{10}#) #:use-module (crates-io))

(define-public crate-k510-pac-0.0.1 (crate (name "k510-pac") (vers "0.0.1") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "riscv-rt") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1x4x6j8zyl2943q0nxm31zbnwx29zhy2j4hf8gr94qchz0c4vwzi") (features (quote (("rt" "riscv-rt"))))))

(define-public crate-k510-pac-0.0.2 (crate (name "k510-pac") (vers "0.0.2") (deps (list (crate-dep (name "bare-metal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "riscv-rt") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0fkr75ag5vifbrgqayga9fh06xmc78a2zdfd2zrwlb88jifgb9hc") (features (quote (("rt" "riscv-rt"))))))

