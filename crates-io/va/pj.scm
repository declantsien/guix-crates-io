(define-module (crates-io va pj) #:use-module (crates-io))

(define-public crate-vapjson-0.1 (crate (name "vapjson") (vers "0.1.0") (deps (list (crate-dep (name "rustc-hex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vapory-types") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "07vy5k41nj1rrmivb3nsa2dwdwp5flf74psdcjb1inmy7r0dn5bs") (features (quote (("test-helpers"))))))

