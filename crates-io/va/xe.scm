(define-module (crates-io va xe) #:use-module (crates-io))

(define-public crate-vaxeral_dictionary-0.1 (crate (name "vaxeral_dictionary") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0vv0ans1c60hgyiw39xn84vaz4g2gi2kah0dy4nc8pn3fqdhskq9")))

(define-public crate-vaxeral_dictionary-0.1 (crate (name "vaxeral_dictionary") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "13vqvngjvssm95ah77200skl254rmbkwmg57hjylk4xbv9wjrmy1")))

