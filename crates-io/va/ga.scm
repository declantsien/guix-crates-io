(define-module (crates-io va ga) #:use-module (crates-io))

(define-public crate-vagabond-0.1 (crate (name "vagabond") (vers "0.1.0") (deps (list (crate-dep (name "cdrs") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "kankyo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "16y9sn42r5z9fpws3jy8jgm231qq95g4k2mgx8571n72va0pail8")))

(define-public crate-vagabond-0.1 (crate (name "vagabond") (vers "0.1.1") (deps (list (crate-dep (name "cdrs") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "kankyo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "020grlw1l3q6myxrlqh8kf06nwj75hjx57gy3qczzbj246px0akk")))

