(define-module (crates-io va ne) #:use-module (crates-io))

(define-public crate-vanel-0.1 (crate (name "vanel") (vers "0.1.0") (deps (list (crate-dep (name "libic") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xmltojson") (req "^0") (default-features #t) (kind 0)))) (hash "0gqfw7kg658bxcicna7fbb4z34r05y8mbkxxg5jip2wf2rsmdp6g")))

(define-public crate-vanessa-0.1 (crate (name "vanessa") (vers "0.1.0") (hash "1pf66acig7yxmpwjkvfidya70jr14j2q9mkn01rmhmaawwr56mj8") (features (quote (("workers") ("multilog" "file-log") ("file-log") ("default" "workers" "file-log")))) (yanked #t)))

(define-public crate-vanessa-0.1 (crate (name "vanessa") (vers "0.1.1-alpha") (hash "13ppc4w8nrrc39yy1x1zf2g2ih51s6qi81svkp3b4dklsl2xd5qv") (features (quote (("workers") ("multilog" "file-log") ("file-log") ("default" "workers" "file-log"))))))

