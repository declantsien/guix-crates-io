(define-module (crates-io va ra) #:use-module (crates-io))

(define-public crate-vara-runtime-0.0.0 (crate (name "vara-runtime") (vers "0.0.0") (hash "1bk2wip4hzgizlrvh6g1ica2nk5f70j41291r3px7gla38s5n322")))

(define-public crate-vararg-0.1 (crate (name "vararg") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 2)))) (hash "0g72wq372panadgny0m7gnmfik5xm4m7ml7smia8hvn2rz5zzsnc")))

(define-public crate-vararg-0.1 (crate (name "vararg") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 2)))) (hash "1n3b0b2ar1m9bsg2n74z97fb2jk2hvm39xblciix8vl8827hsdpn")))

