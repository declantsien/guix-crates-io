(define-module (crates-io va rw) #:use-module (crates-io))

(define-public crate-varweeks_millis-1 (crate (name "varweeks_millis") (vers "1.0.15") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "041gkdysv3h0hl9qygqs6fx09yfdqf400wyhwkqc5f20n67id3jv")))

(define-public crate-varweeks_millis-1 (crate (name "varweeks_millis") (vers "1.0.19") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0kxmdzlazqni4qwgjr9mi5nback9f48zagpklasrxv855psz7cbm")))

