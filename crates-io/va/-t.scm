(define-module (crates-io va -t) #:use-module (crates-io))

(define-public crate-va-ts-0.0.1 (crate (name "va-ts") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "~0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~1.7") (default-features #t) (kind 0)))) (hash "0pz1sblr12a9w1mypnr7h7m2g35jssds744rwgpp8xyk9j4r9rhm")))

(define-public crate-va-ts-0.0.2 (crate (name "va-ts") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "~0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~1.7") (default-features #t) (kind 0)))) (hash "09ynlw3bcjnsdfz271sq84xl3dlacvsx9fmg97c4g2prhzsiqvy8")))

(define-public crate-va-ts-0.0.3 (crate (name "va-ts") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 2)) (crate-dep (name "encoding_rs") (req "~0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~1.7") (default-features #t) (kind 2)))) (hash "0bmq715bh7d7yn4wmcdd9y9hqgb6drma5i38grs47bq7xvm4rqxi")))

(define-public crate-va-ts-0.0.4 (crate (name "va-ts") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 2)) (crate-dep (name "encoding_rs") (req "~0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~2.2") (default-features #t) (kind 2)))) (hash "02a8z3vma2vibhwgfah7nqawy4gw47lcckanlnja1yi177ynqdpd")))

