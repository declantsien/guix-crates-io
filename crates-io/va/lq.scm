(define-module (crates-io va lq) #:use-module (crates-io))

(define-public crate-valq-0.1 (crate (name "valq") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 2)))) (hash "059bmg0wgqfd86swd01bmclnv8j86kl37d2q4834xbbr0g5561dh")))

