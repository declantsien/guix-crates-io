(define-module (crates-io va #{41}#) #:use-module (crates-io))

(define-public crate-va416xx-0.1 (crate (name "va416xx") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req ">=0.6.15, <0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "10h733r2p17x8b42sni5zd8yix5xli9vfcwjcvqclzmhpmgldzln") (features (quote (("rt" "cortex-m-rt/device"))))))

(define-public crate-va416xx-0.1 (crate (name "va416xx") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req ">=0.6.15, <0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "07zqvkxp25rpp6gyishjf1zgww9mv7ydrcnr6c2vn43i24hcgw87") (features (quote (("rt" "cortex-m-rt/device"))))))

