(define-module (crates-io va l_) #:use-module (crates-io))

(define-public crate-val_unc-0.7 (crate (name "val_unc") (vers "0.7.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "10ss0v0yrw37x2kbrxbq06h41rlfnaycx7yfvga0r5iczigqqslc")))

(define-public crate-val_unc-0.7 (crate (name "val_unc") (vers "0.7.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1p03qqp36bvqj8f3jiqvcn9zx01gnm109cbfcnp9nmz6zgian6wf")))

(define-public crate-val_unc-0.8 (crate (name "val_unc") (vers "0.8.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08pzg1lb9w27pdy0h870ypfvh7hcsd5yjcik5xr92b6ycdrrw1vg")))

