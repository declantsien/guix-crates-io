(define-module (crates-io va rt) #:use-module (crates-io))

(define-public crate-vart-0.1 (crate (name "vart") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0z4pmrikxd4djs5dd05iyf77qvlxvsw1942w43v89yca5pr1ppi9")))

(define-public crate-vart-0.1 (crate (name "vart") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0slzmrf4q4zgp09jl1jbaqss42sxp7kbmk5fbawn9bgaqbifnwz2")))

(define-public crate-vart-0.1 (crate (name "vart") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1nm4sk88iawpzlfavc6q2xavfnqhmjpxl6qjyr75lgwz84xcfkdx") (yanked #t)))

(define-public crate-vart-0.2 (crate (name "vart") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1p2srg8xslpclwgkhsw4fypgykyjyh30qm0pa8b3sx629f7q4mjx")))

(define-public crate-vart-0.2 (crate (name "vart") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0w75kbf16azyv5bz3f6cfqh0ix09mnmanpm28sh8y2hd2jvxn1vc")))

(define-public crate-vartyint-0.1 (crate (name "vartyint") (vers "0.1.0") (hash "01s2k9l0z3s49jb4h5jh9bli7hvnyvicbyfw7i6znfdx2byvq783")))

(define-public crate-vartyint-0.2 (crate (name "vartyint") (vers "0.2.0-rc1") (hash "1yy16bahighi4fdv9lj02xcpyjcm805ggn71lwm5x9qz773grjfx")))

(define-public crate-vartyint-0.2 (crate (name "vartyint") (vers "0.2.0-rc2") (hash "1xs5i176iblnq0hsbsmi53xy122fdmifsvbgillx2drl8smpcf5h")))

(define-public crate-vartyint-0.2 (crate (name "vartyint") (vers "0.2.0") (hash "0jnllq8r87sman0ss7hxaafasic72caxxx1zxg55mnvxrpj1z17c")))

(define-public crate-vartyint-0.3 (crate (name "vartyint") (vers "0.3.0-rc1") (hash "0klg5v1fa81s1m9ghxrfld9dgqd2ir8y5br3wy9fc35dq6pxz7cr")))

(define-public crate-vartyint-0.3 (crate (name "vartyint") (vers "0.3.0") (hash "0yky0a7vq89w29knzx6lx841rkvj9b7dkyn5k3qqi91gdjjazskx")))

