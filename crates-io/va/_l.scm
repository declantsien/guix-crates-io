(define-module (crates-io va _l) #:use-module (crates-io))

(define-public crate-va_list-0.0.1 (crate (name "va_list") (vers "0.0.1") (deps (list (crate-dep (name "va_list-test") (req "*") (default-features #t) (kind 2)))) (hash "1vxmsr4r0cjwlsr6ah666037sr78h7ch3d75b088i5zxlg163p9m")))

(define-public crate-va_list-0.0.2 (crate (name "va_list") (vers "0.0.2") (deps (list (crate-dep (name "va_list-helper") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "1lh96sxa7hf4p9b3s42fnnjkhc7a7w7xwx73h89r5inqidy9bc2h") (features (quote (("no_std"))))))

(define-public crate-va_list-0.0.3 (crate (name "va_list") (vers "0.0.3") (deps (list (crate-dep (name "va_list-helper") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "0amncdfyp98bw670bss8sliab1837q4p2xyxly0gkdvzx0dcyj9g") (features (quote (("no_std"))))))

(define-public crate-va_list-0.1 (crate (name "va_list") (vers "0.1.0") (deps (list (crate-dep (name "va_list-helper") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "0a1ipnjsmdc3653cdz4az4ds0725y2jilj8jq7dx0pp5mqpqcrbk") (features (quote (("no_std"))))))

(define-public crate-va_list-0.1 (crate (name "va_list") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "va_list-helper") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "0k7jmb9h27nfb8wd7rc6k88y77kg1vmddhlp31v3f6nkr4si1pdn") (features (quote (("no_std"))))))

(define-public crate-va_list-0.1 (crate (name "va_list") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "va_list-helper") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "195m0fwgzn6y850kf7m47f9kcj15rmjd23spy4n2kv50jkq9xcsb") (features (quote (("no_std"))))))

(define-public crate-va_list-0.1 (crate (name "va_list") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "va_list-helper") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "19g78bc8lpfaf27818irfrkxbjb15wf9ygvbvd7i74nss58ldjwk") (features (quote (("no_std"))))))

(define-public crate-va_list-0.1 (crate (name "va_list") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "va_list-helper") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "1nin5pskdbv8kpambwwncis6cdsfpqafb36hghw8m5sgfkpxa2rm") (features (quote (("no_std"))))))

(define-public crate-va_list-helper-0.0.2 (crate (name "va_list-helper") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "1p35xfcvjkxps4shblw511jkmmzl2jbx17qbgn0py39fp6c29l8i")))

(define-public crate-va_list-rs-0.0.1 (crate (name "va_list-rs") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m35gn4b538agifgblrg273gw8jm71lw3aqbr24dyn9ywlzxpi7c")))

(define-public crate-va_list-rs-0.0.4 (crate (name "va_list-rs") (vers "0.0.4") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ghbwqdfjavwfipnf2q3ynqyz7fwfv7ygzgbh9h9vwiprm91r63x")))

(define-public crate-va_list-test-0.0.1 (crate (name "va_list-test") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "*") (default-features #t) (kind 1)))) (hash "09f048p0p6iy7n0lvxkacqihyzks9b2san204963w38ds0l7k1v1")))

