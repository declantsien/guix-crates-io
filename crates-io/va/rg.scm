(define-module (crates-io va rg) #:use-module (crates-io))

(define-public crate-varg-0.0.1 (crate (name "varg") (vers "0.0.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mr1zc2g266wv7ins3ih9dr0nrjapmp1fc5psji4yv4gbkhbqzmi")))

(define-public crate-varg-0.0.2 (crate (name "varg") (vers "0.0.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0scb928703ipiy5xq7lxmc6iv7l3nci0rx7c9mhj8m7ybmf2qdnh")))

(define-public crate-varg-0.0.3 (crate (name "varg") (vers "0.0.3") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qhk0jm0x5a3a4jf7j0r3p447sp9k30wk8apl1mhay9yy4c6cxl6")))

(define-public crate-varg-0.0.4 (crate (name "varg") (vers "0.0.4") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "07s7sqhdybk4zw6qcz5n3bxp3f12kg136yisvj2mslwc24z59brd")))

(define-public crate-varg-0.0.5 (crate (name "varg") (vers "0.0.5") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bwclg7xz5xdps0fcybagkqzsnc2zdm1fsam3swzklqn5jclac3m")))

