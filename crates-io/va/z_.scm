(define-module (crates-io va z_) #:use-module (crates-io))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "15bcwwyqjxyn130y2cdynrr1c0855bxhsnyrr26ldcfy6537x376")))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1n62akymasafcx0k1baxp1g7ww1nn81p7xgwi6w9gmndlw7xrpp1")))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.2") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1mgvskpbgq5nd2q53hbacjch7li52aqc4g0j5nghc2fbxikkn8ky")))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.3") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1rf4z0q5d7ympi1fii7cplfyz4l411hbihhgajdrnvns6slnfrn2")))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.4") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "17yp583rjnx1qbf01zfmwhvh6zz1lb1hmx4gq3klzxi8vdcvprad")))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.5") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0w2xzw1874niw7rd8f04xc2cih30kkymxk1jsr4bldm4s6zf51vc")))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.6") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0r0i75k8sqifxzximmhvdmb3fnv18jsbgadrqywibp3bq8g1qk6n")))

(define-public crate-vaz_lib-0.1 (crate (name "vaz_lib") (vers "0.1.7") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1zk4y9xcgz5pfyzf1qq12a0j9jm62k2g20p6vbi926mpjx36v2d9")))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1psfszaca6piadgzz65z7gc4mlc0680l2p8907zc9xqkphghqwn6") (yanked #t)))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.1") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "09zkvf8qrs5yq5p9wmcwn5rnwaq5gmn5nijlr1fi4fn9wj22g5ng") (yanked #t)))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.2") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0cighbjbqlfb3cp18dy4l6fd24z88nxln0r9li9awz1kf2bxh0m4")))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.3") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1kjriwl5n1qqkrz18r9vszgnygr6gwbdyakq5pc59l5p513v72r5")))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.4") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "1lwww7n25wg04ncs7bgdk3hdbni79wj9qjg97lyi4bxkyr75psjg")))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.5") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0hb16am1mc25pjnf3wnvbljis5cmvi14k94a2nvmcisps6bfac09")))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.6") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "13j5i34v3jigbnv8hj76q1v6zich3rplz1cbqq345zj9jakm14in")))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.7") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "09595h1g8k3i0lp84l8lp1x2kccw1i7gi53wilh7k175c3gkdayr")))

(define-public crate-vaz_lib-0.2 (crate (name "vaz_lib") (vers "0.2.8") (deps (list (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)))) (hash "0as3adw30rglwfcw3ziv51ymwgbvhya0x1p61h49z33497m1zwpx")))

