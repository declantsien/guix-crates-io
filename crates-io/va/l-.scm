(define-module (crates-io va l-) #:use-module (crates-io))

(define-public crate-val-test-0.1 (crate (name "val-test") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0wf9a2bj9xbfapab2m8nazsfj087dv33x367mslgw4dxfx5bpwxz") (yanked #t)))

(define-public crate-val-test-0.1 (crate (name "val-test") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "07zgiz584x4yrf446qppyw4j5p742xhx34f9basnc3hk6g80s8xz") (yanked #t)))

(define-public crate-val-test-0.1 (crate (name "val-test") (vers "0.1.2") (deps (list (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0nbhvg81838z06gi6hl9m9975ni7ll5nhp3c0ijinysg4h7m2rgp") (yanked #t)))

(define-public crate-val-test-0.2 (crate (name "val-test") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "10zamx3c11ylnz3a0iji5fm6agg2ra462m0rsr0g404ha4makxl3") (yanked #t)))

(define-public crate-val-test-0.2 (crate (name "val-test") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0a68lxfs3hnyrql9mz39dfldch8pq5si1ppkmzka4xam8xwz20gb") (yanked #t)))

(define-public crate-val-test-0.2 (crate (name "val-test") (vers "0.2.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "03mwhjckwqy10jvgqk210vkl76xax030vp5jhfv7i0wb473x727w")))

