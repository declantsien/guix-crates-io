(define-module (crates-io va rb) #:use-module (crates-io))

(define-public crate-varbincode-0.1 (crate (name "varbincode") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "17vgwalm7a1lryjj565p4rnx5h3hr8vwm5504r92s7j41f8gl4mp")))

