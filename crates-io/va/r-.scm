(define-module (crates-io va r-) #:use-module (crates-io))

(define-public crate-var-bitmap-0.1 (crate (name "var-bitmap") (vers "0.1.0") (hash "0df3jgb5l0npxy5kjbnd6jqwi696sfpws24jsa0lvpgrd2shp6kb")))

(define-public crate-var-watcher-0.1 (crate (name "var-watcher") (vers "0.1.0") (hash "000zddmpd2cizrb5m0zkkkf03s17ry44a1v0x0ylsbrwkx5g7n3p")))

