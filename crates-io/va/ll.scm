(define-module (crates-io va ll) #:use-module (crates-io))

(define-public crate-valley-0.1 (crate (name "valley") (vers "0.1.0") (hash "0kljdza0024nm70jlnipwp8j8k69hb9syl767i0n369iixr9da1k")))

(define-public crate-valley-free-0.1 (crate (name "valley-free") (vers "0.1.0") (deps (list (crate-dep (name "bzip2") (req "^0.4") (default-features #t) (kind 2)))) (hash "1w86l7r4d1gic0kgwpdpdx6xpifj8i5d939vhciaiv4a5k1yrn9s")))

(define-public crate-valley-free-0.1 (crate (name "valley-free") (vers "0.1.1") (deps (list (crate-dep (name "bzip2") (req "^0.4") (default-features #t) (kind 2)))) (hash "0knh4h6jx01a9b32hqbnik8xc5cbkhigq86kwn79s00r67m8y63n")))

(define-public crate-valley-free-0.2 (crate (name "valley-free") (vers "0.2.0") (deps (list (crate-dep (name "bzip2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "12nd297ii3kw2nlb6b9xik5481jscxq53s3hd76vslsp2pgz9g9l") (features (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

(define-public crate-vallmo-0.0.0 (crate (name "vallmo") (vers "0.0.0") (hash "0l9la0vlbvd9kms0ma51k1fdaryfk92arvrr6yv8c8ilz65x6j3p")))

(define-public crate-vallmo-flatzinc-0.0.0 (crate (name "vallmo-flatzinc") (vers "0.0.0") (hash "1n3a2shyqzzgrh4xxpixjx170wyap9512ihp2r77q177gag254sv")))

(define-public crate-vallmo-flatzinc-parser-0.0.0 (crate (name "vallmo-flatzinc-parser") (vers "0.0.0") (hash "0i916hg622dgjp5ya753zf02yavbyf3q6mrfzsb3m7n7xsa9zhms")))

(define-public crate-vallmo-minizinc-0.0.0 (crate (name "vallmo-minizinc") (vers "0.0.0") (hash "159q9sk4w5yqjc8a9wdrfmb6f6shxwfg0blp9ri639bfr2frd3kg")))

(define-public crate-vallmo-minizinc-parser-0.0.0 (crate (name "vallmo-minizinc-parser") (vers "0.0.0") (hash "1w1p3l51wcj9va6hxa7wf7w964dbd80ymyvbl83dzfjzszmbdaan")))

