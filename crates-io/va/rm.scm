(define-module (crates-io va rm) #:use-module (crates-io))

(define-public crate-varmint-0.1 (crate (name "varmint") (vers "0.1.0") (hash "0cxj0kwz906kcssbh2n8a1d9awwhi5ivs2p7hdcrzv0lq6wk9h3z") (yanked #t)))

(define-public crate-varmint-0.1 (crate (name "varmint") (vers "0.1.1") (hash "0bksx052m9br7xrwwzhvqqzq9c83nlqqwbbdr0cihziw8gpj7ka3") (yanked #t)))

(define-public crate-varmint-0.1 (crate (name "varmint") (vers "0.1.2") (hash "115fap6jc77vjzmjmss85s0wfx49pxvifqnp1n9ckbc6ixp9f4aj") (yanked #t)))

(define-public crate-varmint-0.1 (crate (name "varmint") (vers "0.1.3") (hash "1ms5vlnx2wm39kyxv8c35mw48nhi159d9bbl4g3z60jlrq24p5ip") (yanked #t)))

