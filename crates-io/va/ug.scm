(define-module (crates-io va ug) #:use-module (crates-io))

(define-public crate-vaughan-0.1 (crate (name "vaughan") (vers "0.1.0") (deps (list (crate-dep (name "polars") (req "^0.39.2") (features (quote ("lazy" "test" "parquet" "ndarray" "diff" "is_in" "serde" "avx512"))) (default-features #t) (kind 0)))) (hash "134ppzbxx076q23n6nvmchhldc6mqd10bv1lbvmaaajzvz0hjhbf") (rust-version "1.76")))

