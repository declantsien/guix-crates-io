(define-module (crates-io va so) #:use-module (crates-io))

(define-public crate-vason-0.0.1 (crate (name "vason") (vers "0.0.1") (deps (list (crate-dep (name "minifb") (req "^0.23.0") (default-features #t) (kind 2)))) (hash "00pkvp3fiab2lapwpa5h1h1mzlxq29k7ppagrlhjj054r4rvnhsa") (yanked #t) (rust-version "1.58.1")))

(define-public crate-vason-0.0.2 (crate (name "vason") (vers "0.0.2") (deps (list (crate-dep (name "minifb") (req "^0.23.0") (default-features #t) (kind 2)))) (hash "06a0kcjc50yxg5szc0j5c8ibk97zma0qzr3n68wygc5hbmqpkmyl") (rust-version "1.58.1")))

(define-public crate-vason-0.0.3 (crate (name "vason") (vers "0.0.3") (deps (list (crate-dep (name "minifb") (req "^0.23.0") (default-features #t) (kind 2)))) (hash "0yr2840s4vwpf6cmn1k54n13mc4xljjabjbpmjaq6dx83dvy8cds") (rust-version "1.56.1")))

