(define-module (crates-io va ti) #:use-module (crates-io))

(define-public crate-vatican-0.0.1 (crate (name "vatican") (vers "0.0.1") (hash "1f2h7mkrq3515fn1vn733cmfk3z6p7xy91bcqjwbay2xgpy1n3w4")))

(define-public crate-vatican-0.0.2 (crate (name "vatican") (vers "0.0.2") (hash "1xpgja2lpqh3clkskcxlslymsim2ghs0gsbs183cjr40325anmyk")))

(define-public crate-vatican-0.0.3 (crate (name "vatican") (vers "0.0.3") (hash "04iggyp6wsnna0mfycl7xlx96nixwvbnziq3zqqjz1ainywiw3pn")))

