(define-module (crates-io va lg) #:use-module (crates-io))

(define-public crate-valgrind-0.1 (crate (name "valgrind") (vers "0.1.0") (hash "0qwrm1xaqlma6sn4272pf1z9qf57g4czqj94435f0s9fqavj38b8")))

(define-public crate-valgrind-0.1 (crate (name "valgrind") (vers "0.1.1") (hash "0mvcfadlbf8i8skbrkp8717pzpab5icrpsfhhdbk7cwv504v30mz")))

(define-public crate-valgrind-0.1 (crate (name "valgrind") (vers "0.1.2") (hash "109dxz5iv5x78a8nadciv85rmzrq8nniwvzc1p3jrhb2qnv5804q")))

(define-public crate-valgrind_request-1 (crate (name "valgrind_request") (vers "1.0.0") (hash "1sh56qh7ch1qml72mayi3p0qv7rh84nrimn2llf1nxc4j9b98yy9")))

(define-public crate-valgrind_request-1 (crate (name "valgrind_request") (vers "1.1.0") (hash "16m941mac859gvw1fg99nhvmz02cwj4chfa4wd816gj72jdi7yyh")))

