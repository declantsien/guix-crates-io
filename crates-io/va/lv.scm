(define-module (crates-io va lv) #:use-module (crates-io))

(define-public crate-valve-0.0.1 (crate (name "valve") (vers "0.0.1") (hash "197iixnhw3hxgk5bd1wsmvaffa77q0iwcx7n3srka1fx79f8iglw")))

(define-public crate-valve-rcon-0.1 (crate (name "valve-rcon") (vers "0.1.0") (hash "1svbbfmvrllcqfdzb4sym3qiphchz122m3kp2s82113c6l631sqs")))

(define-public crate-valve-rcon-0.1 (crate (name "valve-rcon") (vers "0.1.1") (hash "175z99zwfgfdlm6p7b9cfn6f4mp47q7gnf76jk2d2rd13l8qkmda")))

(define-public crate-valve-rcon-0.1 (crate (name "valve-rcon") (vers "0.1.2") (hash "02i1q1q73gq13vrw6606a3sxlgl2479x2bipf7w5zmkyvas3srl2")))

(define-public crate-valve-rs-0.1 (crate (name "valve-rs") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.18") (features (quote ("tokio"))) (default-features #t) (kind 0)) (crate-dep (name "deadpool") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "extendr-api") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.26") (features (quote ("client"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("io-util" "macros" "rt-multi-thread" "signal"))) (default-features #t) (kind 0)))) (hash "08kdqh9vxfih3ma4b8qwy11qsjdyyxxcjp39gzydv2va7jncz584") (features (quote (("default" "rlib")))) (v 2) (features2 (quote (("rlib" "dep:extendr-api"))))))

(define-public crate-valve-rs-0.1 (crate (name "valve-rs") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.18") (features (quote ("tokio"))) (default-features #t) (kind 0)) (crate-dep (name "deadpool") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "extendr-api") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.26") (features (quote ("client"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("io-util" "macros" "rt-multi-thread" "signal"))) (default-features #t) (kind 0)))) (hash "0fnqvgh1r5jbs0qpd72c5zkfivy7d0k0ffa18kkxzfbd1ri309g3") (features (quote (("default" "rlib")))) (v 2) (features2 (quote (("rlib" "dep:extendr-api"))))))

(define-public crate-valve-rs-0.1 (crate (name "valve-rs") (vers "0.1.2") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.18") (features (quote ("tokio"))) (default-features #t) (kind 0)) (crate-dep (name "deadpool") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "extendr-api") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.26") (features (quote ("client"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("io-util" "macros" "rt-multi-thread" "signal"))) (default-features #t) (kind 0)))) (hash "0581gsls1w62hrqia98qgi44n63hfi0qgjs52kxhndh1pa16d2sh") (features (quote (("default" "rlib")))) (v 2) (features2 (quote (("rlib" "dep:extendr-api"))))))

(define-public crate-valve-rs-0.1 (crate (name "valve-rs") (vers "0.1.3") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.6.18") (features (quote ("tokio"))) (default-features #t) (kind 0)) (crate-dep (name "deadpool") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "extendr-api") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.26") (features (quote ("client"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("io-util" "macros" "rt-multi-thread" "signal"))) (default-features #t) (kind 0)))) (hash "1x5kgrk0rgksmryw6nsni8lnx8ny4q4hmajdlwymgqdhxq0kia1f") (features (quote (("default" "rlib")))) (v 2) (features2 (quote (("rlib" "dep:extendr-api"))))))

(define-public crate-valve-sdk13-rng-0.1 (crate (name "valve-sdk13-rng") (vers "0.1.0") (hash "059dd8n3q6hsgddi8dch8vrf46pffbgg8kri2pj9dkcpv3qp0xfp")))

(define-public crate-valve-server-query-0.3 (crate (name "valve-server-query") (vers "0.3.1") (hash "09i4hck7nnfxp5an3r7q8dccqkzszy7z85x1vfzdpqmii3nwlw35")))

(define-public crate-valve-server-query-0.3 (crate (name "valve-server-query") (vers "0.3.2") (hash "0i1lgws024cl01jsy64whmlz0vcllyrk9a0im5q88ha4blsi4x0h")))

(define-public crate-valve-server-query-0.3 (crate (name "valve-server-query") (vers "0.3.7") (hash "0v2cwyi74wblahz6icb350rmmfx2j0ccgjkc1frvblxaq48d3ab0")))

(define-public crate-valve-server-query-0.3 (crate (name "valve-server-query") (vers "0.3.8") (hash "1dqmdsa7ajrp3pckzjw2ipa6wlx4vqibh4nbbyr3x63if7nxh6l4")))

(define-public crate-valve-server-query-0.3 (crate (name "valve-server-query") (vers "0.3.9") (hash "0jml0lb921cpxfz1ljwn4aaw0p65a7mz24rsc6ajh4yxrz9arhz0")))

(define-public crate-valve-server-query-0.3 (crate (name "valve-server-query") (vers "0.3.10") (hash "0jrhb9iidhsds20hk8q54bn9mb9n82i208mi743cs3lsi4kg3jaw")))

(define-public crate-valve-server-query-0.3 (crate (name "valve-server-query") (vers "0.3.11") (hash "0zfxjk23v9r1v7x4s2bblj5xfnn5qlpal17s55223w1zy6hjv42k")))

(define-public crate-valve-server-query-0.4 (crate (name "valve-server-query") (vers "0.4.0") (hash "16ld6ja04cw1kzcb6zkn1p8xx3q36b8sc4dyjdnn859b1998yhr5")))

(define-public crate-valve-server-query-0.4 (crate (name "valve-server-query") (vers "0.4.5") (deps (list (crate-dep (name "console_log") (req "^0.2.0") (features (quote ("color"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^2.3.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "0il5wa6zw34r61v3fw44v3s0v21s3l31rs15j632ya6a8r1spibh")))

(define-public crate-valve-server-query-0.4 (crate (name "valve-server-query") (vers "0.4.6") (deps (list (crate-dep (name "console_log") (req "^0.2.0") (features (quote ("color"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^2.3.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "0l23arbki4jiim12i54q2aj99az9l97d29dr88x6vi761a183p34")))

(define-public crate-valve-server-query-0.4 (crate (name "valve-server-query") (vers "0.4.7") (hash "1yp9p41gnvfx1kcng4ap920psdhcrqz47a4d24szi6a5hgwfkcwb")))

(define-public crate-valve-server-query-0.4 (crate (name "valve-server-query") (vers "0.4.8") (hash "0pikzbfaaqiimmijqx06sl6pvimaxp4q964wsj89jm5z70kqgq54")))

(define-public crate-valve-server-query-0.4 (crate (name "valve-server-query") (vers "0.4.9") (hash "0k38z7wpfg9x5p09vsmbs1a1aqrld8rshsvahbrkjvh6qrwx47g1")))

(define-public crate-valve_kv_tools-0.1 (crate (name "valve_kv_tools") (vers "0.1.0") (deps (list (crate-dep (name "lsp-types") (req "^0.94.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-wasm-bindgen") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "=0.2.87") (features (quote ("strict-macro" "serde-serialize"))) (default-features #t) (kind 0)))) (hash "19xam9i8dpajbkhjl4rcpcsax4ly1ab013dnsj46d6mgbj49mmkn")))

(define-public crate-valve_kv_tools-0.2 (crate (name "valve_kv_tools") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (kind 0)) (crate-dep (name "minreq") (req "^2.7.0") (features (quote ("https"))) (default-features #t) (kind 2)) (crate-dep (name "pest") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-wasm-bindgen") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "=0.2.87") (features (quote ("strict-macro" "serde-serialize"))) (default-features #t) (kind 0)))) (hash "0l1kyg9yhzbaqmrsirkv0d590cq66r1swfsdxfq264knv6rps95d")))

(define-public crate-valve_kv_tools-0.3 (crate (name "valve_kv_tools") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (kind 0)) (crate-dep (name "minreq") (req "^2.7.0") (features (quote ("https"))) (default-features #t) (kind 2)) (crate-dep (name "pest") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-wasm-bindgen") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "=0.2.87") (features (quote ("strict-macro" "serde-serialize"))) (default-features #t) (kind 0)))) (hash "1qk0kriy56a91xf57ywir8d8svz895q2lmiwbcv641cldgmkypf5")))

