(define-module (crates-io va ry) #:use-module (crates-io))

(define-public crate-vary-0.1 (crate (name "vary") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "15422p1vs8f16mdf46hjmh6z8im49dmnkdihhxzbi43g5hb15jpz")))

(define-public crate-varys-0.1 (crate (name "varys") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1lm0nyb0ma9q8sjm18620b041slsanlbmp2g8n1hyjbrwf924wll")))

