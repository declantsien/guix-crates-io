(define-module (crates-io va al) #:use-module (crates-io))

(define-public crate-vaal-0.1 (crate (name "vaal") (vers "0.1.0") (deps (list (crate-dep (name "deepviewrt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kkxl7ycbysf0zr8d8p1kn1qvxr29rll59rm7waxgwlsr1qjawfs")))

(define-public crate-vaal-0.2 (crate (name "vaal") (vers "0.2.1") (deps (list (crate-dep (name "deepviewrt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "03f502yxkhwx8q26acm2wf0gpn23mfcshd90wmicndg02x6yb8vd")))

(define-public crate-vaal-0.2 (crate (name "vaal") (vers "0.2.2") (deps (list (crate-dep (name "deepviewrt") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "01mgg3ghvci627viw6wcr4sa6hlcfz505nbqsvalagx5xlya7iv1")))

(define-public crate-vaal-0.3 (crate (name "vaal") (vers "0.3.0") (deps (list (crate-dep (name "deepviewrt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0gd2f9vm89rh407s2wa5xh21hnvhk2q9s2dykljhibgi9w2pbb8l")))

(define-public crate-vaal-0.3 (crate (name "vaal") (vers "0.3.1") (deps (list (crate-dep (name "deepviewrt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0vcr0b79rfnf75h2bldaqh8m35ncjlhahxyab0pmccs781k6dsq9")))

(define-public crate-vaal-0.3 (crate (name "vaal") (vers "0.3.2") (deps (list (crate-dep (name "deepviewrt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "040h5ad039wzlpyqxjn08m4bfglr1qsgpqrc5kn9v7i6zchmqsvn")))

(define-public crate-vaal-0.3 (crate (name "vaal") (vers "0.3.3") (deps (list (crate-dep (name "deepviewrt") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1df1g4j9x0wc0cslr6021g8wjd3sfa02jlsini83jkx2naq9qfc0")))

(define-public crate-vaal-0.3 (crate (name "vaal") (vers "0.3.4") (deps (list (crate-dep (name "deepviewrt") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "vaal-sys") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0ksayss48k14kv6szqx5xjzkm3fbkxfjd68m1znlcayc6h7331pd")))

(define-public crate-vaal-sys-0.1 (crate (name "vaal-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rck6app82hjcwylcx483hmzbszkgrg3jrnj02w52aqvj4jjkdwp")))

(define-public crate-vaal-sys-0.2 (crate (name "vaal-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s1pxfh6l8bc0gv6klibsyx3krzj414x41aihqgr1lhya7p12vxq")))

(define-public crate-vaal-sys-0.2 (crate (name "vaal-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1776vf0r5628vq9c90h3c3qalj1h73nyqqx9x04vidr90zda24nx")))

(define-public crate-vaal-sys-0.3 (crate (name "vaal-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03zh1daicbdxgdwxag9wm5830m0fkqsws13bgplafm567klvv0ny")))

(define-public crate-vaal-sys-0.3 (crate (name "vaal-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0aqb99px8fxslmck4npwsc8r6plmfhncb6j7sk0ifah3h4nlfajv")))

(define-public crate-vaal-sys-0.3 (crate (name "vaal-sys") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "02rgf3lvlskl0zs7l0iwnzqa7v97a6hygnfskmlx6m8a21hwy2ls")))

(define-public crate-vaal-sys-0.3 (crate (name "vaal-sys") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "02rc9000zlb7gg3m5czy2dh6ip12jzhsrynla1kjzncccnhzj9s3")))

(define-public crate-vaal-sys-0.3 (crate (name "vaal-sys") (vers "0.3.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lj2ah9rs4r9r9hybw07i0mv33x2d3pqvfxqbjq61nl39nwg8k3w")))

