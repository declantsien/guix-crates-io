(define-module (crates-io va rj) #:use-module (crates-io))

(define-public crate-varj-0.1 (crate (name "varj") (vers "0.1.0") (hash "1wk4gl1byxhv9vldrnfglpdz35q80ak7p5sba015vr64zcbhdaiz")))

(define-public crate-varj-1 (crate (name "varj") (vers "1.0.0") (hash "1jai6lq73s6l7yggg6rwlga71jgqiz8nckd8xni58qpc4i9xwaxs")))

(define-public crate-varj-1 (crate (name "varj") (vers "1.0.1") (hash "18nxdi05qfxncnl8cbzfcvs5wgkbqih31rrdczgr22f0xby79wdr")))

(define-public crate-varj-1 (crate (name "varj") (vers "1.0.2") (hash "0wwlrkp0kgbdq9xwjshwgs07gq9xxnl01lp47szsp4289hxddzgb")))

(define-public crate-varj-1 (crate (name "varj") (vers "1.0.3") (hash "03m2vd1295wnlmcbs5ybj4wp3s5ggvvlyxd1hald7kc51gk894ji")))

(define-public crate-varj-1 (crate (name "varj") (vers "1.1.0") (hash "0klnvvjgyay17hw5zlz387s91v724v9f0kpx0g8zjsg9rgmpsqvp")))

