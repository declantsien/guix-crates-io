(define-module (crates-io va d-) #:use-module (crates-io))

(define-public crate-vad-sys-0.1 (crate (name "vad-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "0ygp3n3k6knw0i5ijhs6kg1dnk9a2frh904q2p3lb3vsanldqnc4") (yanked #t) (links "vad")))

(define-public crate-vad-sys-0.1 (crate (name "vad-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "170y84jflkir3yrxkfz7bb12s3532bcxwn9vl8z49ydk5rdn3zq0") (yanked #t) (links "vad")))

