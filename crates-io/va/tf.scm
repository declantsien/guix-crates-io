(define-module (crates-io va tf) #:use-module (crates-io))

(define-public crate-vatfluid-0.1 (crate (name "vatfluid") (vers "0.1.0") (hash "00bwkr6zpa7f51avbajfl4ywxgqj2xkb6376l1x6ps8pv30r8dss")))

(define-public crate-vatfluid-0.2 (crate (name "vatfluid") (vers "0.2.0") (hash "1bj8im6a59pywajdc8ifv88x1xp2zc91sl75zwj464wakwcyjwdc")))

(define-public crate-vatfluid-0.3 (crate (name "vatfluid") (vers "0.3.0") (deps (list (crate-dep (name "assert_matches") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "03gwmjfqqhg283rv51ykamb5rjgdsignxz9nmk6r4j2r9msnvazi")))

