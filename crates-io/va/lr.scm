(define-module (crates-io va lr) #:use-module (crates-io))

(define-public crate-valrow-0.0.0 (crate (name "valrow") (vers "0.0.0-2024-02-12") (deps (list (crate-dep (name "abistr") (req "^0.1") (default-features #t) (kind 2)))) (hash "1asghhisv983s6dbyg5kfmm87vwadplspwza4pyw7cpr137yb1ia") (features (quote (("std" "alloc") ("intrinsic") ("default" "core") ("core" "intrinsic") ("alloc" "core")))) (yanked #t) (rust-version "1.71.0")))

(define-public crate-valrow-0.0.0 (crate (name "valrow") (vers "0.0.0-2024-02-13") (deps (list (crate-dep (name "abistr") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "ialloc") (req "^0.0.0-2023-05-28") (default-features #t) (kind 2)))) (hash "0yym41bc42z0n0zd6afkzviyh2g32hxi1hh4p4jr90sscp5d4w37") (features (quote (("std" "alloc") ("intrinsic") ("default" "core") ("core" "intrinsic") ("alloc" "core")))) (rust-version "1.71.0")))

