(define-module (crates-io va rs) #:use-module (crates-io))

(define-public crate-vars-0.1 (crate (name "vars") (vers "0.1.0") (hash "09wl8p6plb7lng6nxd8fnrd6cxhh3c1ldpgz67b01zlxx0m9xlpp")))

(define-public crate-vars-0.2 (crate (name "vars") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "vars_macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0if3g5bj5lpa3gs20r9aqgp9cn4kjh2jfwk8l0b2b965vp7hsaa3")))

(define-public crate-vars_macro-0.1 (crate (name "vars_macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.16") (default-features #t) (kind 0)))) (hash "1mpn9x9ai8frgipk1k1sjdiic9j39pplhsckjyh6jprnj2i3c8kg")))

(define-public crate-varstack-0.1 (crate (name "varstack") (vers "0.1.0") (hash "1hc3ibqp0r0s43r5cs456rlc3isfvizml37q3i86yq6gm2jqmh7w")))

(define-public crate-varstack-0.2 (crate (name "varstack") (vers "0.2.0") (hash "0pblby1sqy9z8xdy4y7gvkzbcb99a2ip5sw88wl8b67vys2k5w5h")))

(define-public crate-varstack-0.2 (crate (name "varstack") (vers "0.2.1") (hash "04pqk7g9kyhx0sjjdl9xkx4px2dwhqpcq4vq3a1jmw3rc7yzr35f")))

(define-public crate-varstack-0.2 (crate (name "varstack") (vers "0.2.2") (hash "1qgr82aznjyfvznjcpkk9nmz4hhqgzz2mg94faq7w8n1s4sjc2q5")))

(define-public crate-varstack-0.2 (crate (name "varstack") (vers "0.2.3") (hash "17bf3mv4nxrak5s3dxck76q99nhdwbrhlyqp64hx3529a681j5gk")))

(define-public crate-varstack-0.2 (crate (name "varstack") (vers "0.2.4") (hash "0il6fqjwk6r03d80yr000l95rc6zbdiw09zqbxdhi80v7wayxwh3")))

(define-public crate-varsun-0.1 (crate (name "varsun") (vers "0.1.0") (hash "1l31xkj5glpl000x1jgvmm3mvgnwmrg4d8r720gbhnhc9ri5min1") (yanked #t)))

(define-public crate-varsun-0.1 (crate (name "varsun") (vers "0.1.1") (hash "12296a5qv78a3azhkld8fd589zi2g4mf7iwxb5zm97xp0caaaz9f")))

