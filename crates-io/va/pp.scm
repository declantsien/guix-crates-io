(define-module (crates-io va pp) #:use-module (crates-io))

(define-public crate-vapper-0.1 (crate (name "vapper") (vers "0.1.0") (deps (list (crate-dep (name "fast_vk") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0l5sxgnsvmq5w1fsmjli3ghklra65vkdnw06g8d61x7d566fvk59")))

