(define-module (crates-io va t_) #:use-module (crates-io))

(define-public crate-vat_jp-0.1 (crate (name "vat_jp") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "cargo-watch") (req "^8.4.0") (default-features #t) (kind 2)))) (hash "1c6n12mc6a5l6nmxm9qjk61i3y5k7qbayar5pldc3zjkv2r7yqph")))

(define-public crate-vat_jp-0.1 (crate (name "vat_jp") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "cargo-watch") (req "^8.4.0") (default-features #t) (kind 2)))) (hash "153kanj2hfwy76scdsfik08ybm6ri4m51mbx5pip4jbifavvm9yf")))

(define-public crate-vat_jp-0.1 (crate (name "vat_jp") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "cargo-watch") (req "^8.4.0") (default-features #t) (kind 2)))) (hash "15kswnps563wz51vfh555bja3c115j1nsy1rbas2p8w56xn7lcvb")))

