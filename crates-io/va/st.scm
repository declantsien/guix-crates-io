(define-module (crates-io va st) #:use-module (crates-io))

(define-public crate-vast-0.1 (crate (name "vast") (vers "0.1.0") (deps (list (crate-dep (name "pretty") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1aq44byqycgkgdmw94d2mvz0q72h6m36sx8d4xfbijzdy41iq04b")))

(define-public crate-vast-0.2 (crate (name "vast") (vers "0.2.0") (deps (list (crate-dep (name "pretty") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "197i4d16rzgj315kgp3xk2vqcil9vf63bw549kz2vybpx0qd1bac")))

(define-public crate-vast-0.2 (crate (name "vast") (vers "0.2.1") (deps (list (crate-dep (name "pretty") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0gzvr9dkqfg70ic2g0l47awpp8xad35q3v9kp4p01r3wlq34m4ai")))

(define-public crate-vast-0.2 (crate (name "vast") (vers "0.2.2") (deps (list (crate-dep (name "pretty") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "110r5w4415qga71pxikhms5qmcp4c0llxhph28c8l89mh8b46xhh")))

(define-public crate-vast-0.3 (crate (name "vast") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0fr0avaqlxsxlrs7ryxgg1xm050l0i0ynxfff3kf6238bw0zzwy1")))

(define-public crate-vast-0.3 (crate (name "vast") (vers "0.3.1") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1z4n9d1nya0745bgq9a7572mmw8j51n4brvklcivfhz37vgnl0pk")))

(define-public crate-vast-0.3 (crate (name "vast") (vers "0.3.2") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.10") (default-features #t) (kind 0)))) (hash "0z7prdkjd7zxgpzy01rfckw564aw4ya1qwclqjs8lgwpdpxwldmd")))

(define-public crate-vast-0.3 (crate (name "vast") (vers "0.3.3") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "^0.11") (default-features #t) (kind 0)))) (hash "0clfw0cck4n0pl8cgixr8giw0iyvzvdn69gl9ihypibi1vns9nhs")))

(define-public crate-vast-protocol-0.1 (crate (name "vast-protocol") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.25") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1crry9456avg55y2xfbwagv6zirzx4khnhd1daykpql4y7ri873q") (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-vast-protocol-0.2 (crate (name "vast-protocol") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.25") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12dad6fg5c8wv6hkaxcygn5j0vf5khq1gm3s18rl13v26nahp69w") (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-vast-protocol-0.3 (crate (name "vast-protocol") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.25") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0phv8hlz4b969wjp8xrx2igz7qb0k0365afhs5m64jvhmxw5xv92") (v 2) (features2 (quote (("chrono" "dep:chrono"))))))

(define-public crate-vast4-0.1 (crate (name "vast4") (vers "0.1.0") (deps (list (crate-dep (name "hard-xml") (req "^1.21") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)))) (hash "0zwaach9mjqs289k1zzzwfkm09ippqy8g1yq29jn5hlczg4rnmih")))

(define-public crate-vast4-rs-1 (crate (name "vast4-rs") (vers "1.0.0") (deps (list (crate-dep (name "hard-xml") (req "^1.21") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)))) (hash "1aq5m8slwai2rfqfkga3llm5a1c3f8c63r4y8y3d1jhcfj331r8j")))

(define-public crate-vast4-rs-1 (crate (name "vast4-rs") (vers "1.0.1") (deps (list (crate-dep (name "hard-xml") (req "^1.23.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)))) (hash "0km8rmdar4szbysk4gza3yndh43ari6g2hx3y9ajdrns9v5b7chf")))

(define-public crate-vast4-rs-1 (crate (name "vast4-rs") (vers "1.0.2") (deps (list (crate-dep (name "hard-xml") (req "^1.23.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)))) (hash "1f22j5ahh29rrjdi94hacqdai8vwn6dlj6vgvyy1lkwcpmppd8fj")))

(define-public crate-vast4-rs-1 (crate (name "vast4-rs") (vers "1.0.3") (deps (list (crate-dep (name "hard-xml") (req "^1.23.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)))) (hash "16xy7bfgd8bwypmlkh5cch4qb7vniaf2hzij306agnfn0s3hkzdc")))

(define-public crate-vast4-rs-1 (crate (name "vast4-rs") (vers "1.0.4") (deps (list (crate-dep (name "hard-xml") (req "^1.23.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)))) (hash "0kqq66m4ib88krrsxfidp775gkyb1smsbxf2g5q5bwlqv0zqqii7")))

(define-public crate-vast4-rs-1 (crate (name "vast4-rs") (vers "1.0.6") (deps (list (crate-dep (name "hard-xml") (req "^1.23.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3") (default-features #t) (kind 2)))) (hash "1s1n9wdvlc7x91plldcaqf016v08gbdmrs9k40w5hl7yavcn0krm")))

