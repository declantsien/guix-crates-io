(define-module (crates-io va pe) #:use-module (crates-io))

(define-public crate-vape-0.0.1 (crate (name "vape") (vers "0.0.1") (hash "11n2c12l2pm9b8cgicx7gmwxm2sxhbp28i1qmdcjhg96mqvh23xf") (yanked #t)))

(define-public crate-vape-0.0.2 (crate (name "vape") (vers "0.0.2") (hash "00wcjyajwm9h7a6v052inv2591m9yic1r47fm1yakqsshd1hh099") (yanked #t)))

(define-public crate-vape-0.1 (crate (name "vape") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xklqcj6b689lpkji6wxn2lp6i2wps920nhrr871rs782d63k0k2")))

(define-public crate-vape-0.1 (crate (name "vape") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rwn2y36n4xsamc9d2rzkhhzqxswwysvk0vkj50q45sclbjxd32g")))

(define-public crate-vape-0.1 (crate (name "vape") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gzfik9hs12jakq5ziqa6h0bg7gfj30547s58naghnss2zday8qp")))

(define-public crate-vape-0.1 (crate (name "vape") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k4bpxcfmszynanizr5sjqyddq43ixm83i31mxxfs5iz1kakwyhw")))

(define-public crate-vape-0.2 (crate (name "vape") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.5.0") (default-features #t) (kind 0)))) (hash "0fl99sy7b8kv31wnnf7ssp61fn43x2fnsvff4fzh1b2r8c49y07n")))

(define-public crate-vape-0.2 (crate (name "vape") (vers "0.2.1") (deps (list (crate-dep (name "getopts") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.5") (default-features #t) (kind 0)))) (hash "1n4qpn4kxgsrfqwlcf7jfpsr2pyj3a0qc31cyvd660dql4blr61c")))

(define-public crate-vape-0.2 (crate (name "vape") (vers "0.2.2") (deps (list (crate-dep (name "getopts") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.5") (default-features #t) (kind 0)))) (hash "0hqd53a4id3z7xv33x22b4z1bcxrgg23m2fjrhai7mk6jk9d37c7")))

(define-public crate-vape-0.2 (crate (name "vape") (vers "0.2.3") (deps (list (crate-dep (name "getopts") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.7") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "0ycy4kavvdz960pnv69dr8jlklwcd29hs7prvr8kamz0cydjnw1l")))

(define-public crate-vape-0.3 (crate (name "vape") (vers "0.3.1") (deps (list (crate-dep (name "getopts") (req "~0.2") (default-features #t) (kind 0)))) (hash "0h15lw8p2c5hn5b5klcvclm50cmk6yfahjsbj9aqgd1p16vziayv")))

(define-public crate-vape-0.3 (crate (name "vape") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "~0.2") (default-features #t) (kind 0)))) (hash "0pss3aya9hhxnjwiyl3brjb0prnhibxn3928pvryhml1pwwbw64i")))

(define-public crate-vape-0.4 (crate (name "vape") (vers "0.4.0") (deps (list (crate-dep (name "fastrand") (req "=1.2.4") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "~0.2") (default-features #t) (kind 0)))) (hash "0nalr4zw4y2frf5yabcid34a4rc7675iz54a8kjrksb1yr0xk6xc")))

