(define-module (crates-io yg op) #:use-module (crates-io))

(define-public crate-ygoprodb-0.1 (crate (name "ygoprodb") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("color" "suggestions" "yaml"))) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f4303ld3j0gvb270i2ljyrzjcr3j0l6hli7dymwc2s8y5pz7f08")))

