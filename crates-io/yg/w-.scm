(define-module (crates-io yg w-) #:use-module (crates-io))

(define-public crate-ygw-macros-0.1 (crate (name "ygw-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kc79hkg0212bz0rnpxgpvyh45sv21m7l6yyxqfy72b1sj1hsz33")))

