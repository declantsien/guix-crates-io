(define-module (crates-io yg ln) #:use-module (crates-io))

(define-public crate-yglnk-core-0.0.0 (crate (name "yglnk-core") (vers "0.0.0") (deps (list (crate-dep (name "int-enum") (req "^0.5") (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh64"))) (default-features #t) (kind 0)))) (hash "02vpcwmakv1c4vk7vbpc5ngn79ifm1x4si2y7sk9xr3ks9n9375z")))

(define-public crate-yglnk-core-0.0.1 (crate (name "yglnk-core") (vers "0.0.1") (deps (list (crate-dep (name "int-enum") (req "^0.5") (kind 0)) (crate-dep (name "memchr") (req "^2.5") (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh64"))) (default-features #t) (kind 0)))) (hash "119x8lvfzr8nknfw6mjrzgmjk67vc2l70gzz21v7nvn1imsflv64")))

(define-public crate-yglnk-core-0.0.2 (crate (name "yglnk-core") (vers "0.0.2") (deps (list (crate-dep (name "int-enum") (req "^0.5") (kind 0)) (crate-dep (name "memchr") (req "^2.5") (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8") (features (quote ("xxh64"))) (default-features #t) (kind 0)))) (hash "0gx8r3in83k2bpxjz1658cyhhq3q0lmwf3vmaxqkrw88ficpfsv8") (features (quote (("alloc"))))))

