(define-module (crates-io ax os) #:use-module (crates-io))

(define-public crate-axos-0.1 (crate (name "axos") (vers "0.1.0") (deps (list (crate-dep (name "axos-primitives") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "07smsdwvixrbimpn5zli55ys1qb1jyq7d9srvj0cc20dylqig1wc")))

(define-public crate-axos-primitives-0.1 (crate (name "axos-primitives") (vers "0.1.0") (deps (list (crate-dep (name "alloy-primitives") (req "^0.4.2") (features (quote ("hex-compat" "serde"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "alloc"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "1mqanlbvf2jiw5k2gvm40b6ljjvay9ma293cw69sg66zgbd9v1pg")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.0") (deps (list (crate-dep (name "quicksilver") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0im3m265cw8agsw4rpfcfzcfc82h1a38805xki88clma6gc0b3cg")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.1") (deps (list (crate-dep (name "quicksilver") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1w1d7l9rh7650fywp1igmc6hpsc1gxsfpa8szsx7dpfi2y51c153")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.2") (deps (list (crate-dep (name "quicksilver") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0nm0kxzpx769qil1w6gp0s8wi207ri31gyhfq3vbn1ggj97d71li")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.3") (deps (list (crate-dep (name "quicksilver") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "11s23bzaaqcvskzkv7z4my3srvgj7fl1qv3x83qhjkh3f1km9d73")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.4") (deps (list (crate-dep (name "quicksilver") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1jbvcb3gwvy73p6visfmh770angj26714knis19jb46ya6bw55w8")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.5") (deps (list (crate-dep (name "quicksilver") (req "^0.3.14") (features (quote ("fonts"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "17any65aywapvx9q99rbr043lyxiyfjkiqqjkk0000g9pygnwxy6")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.6") (deps (list (crate-dep (name "quicksilver") (req "^0.3.14") (features (quote ("fonts"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1c74xmvbxs5lr25y4j6lp3q44m2v8yhliz658x2mk7zi3k6id6rg")))

(define-public crate-axosnake-0.1 (crate (name "axosnake") (vers "0.1.7") (deps (list (crate-dep (name "instant") (req "^0.1.1") (features (quote ("stdweb"))) (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.3.16") (features (quote ("fonts"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1qg8530x4kccd56h095zmxkdiq2y04aqic4dmha1yi4sjq1jb1l6")))

