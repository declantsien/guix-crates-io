(define-module (crates-io ax p2) #:use-module (crates-io))

(define-public crate-axp20x-0.0.1 (crate (name "axp20x") (vers "0.0.1") (deps (list (crate-dep (name "bitmask-enum") (req "^1.1.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "0z91r4y7l6mq37d9ksa0rfx318qdw8q44zg76232cvyzh3jszy2b") (features (quote (("std") ("default"))))))

(define-public crate-axp20x-0.0.2 (crate (name "axp20x") (vers "0.0.2") (deps (list (crate-dep (name "bitmask-enum") (req "^1.1.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "1w9dhv1m0ls720fbjbv9hdj2ba7dcwbpyjp1sgg5ryyn7fpi7hlx") (features (quote (("std") ("default"))))))

