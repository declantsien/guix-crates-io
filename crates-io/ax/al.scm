(define-module (crates-io ax al) #:use-module (crates-io))

(define-public crate-axal-0.1 (crate (name "axal") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "080mqrv6k62aryqa3x615fx697lzzwjl8xwnw1dx07x689mr70m6")))

