(define-module (crates-io ax ot) #:use-module (crates-io))

(define-public crate-axotag-0.1 (crate (name "axotag") (vers "0.1.0") (deps (list (crate-dep (name "miette") (req "^5.6.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "1wy7q0mm3p5cribvvri19dd2f63rq93mg2bb8r0rgw3wx92vcaf8")))

(define-public crate-axotag-0.2 (crate (name "axotag") (vers "0.2.0") (deps (list (crate-dep (name "miette") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "0m3c38gbz2dk42s5qlspqdayjnmg4mdgqhx76vgwnr1ynz0gm26q")))

