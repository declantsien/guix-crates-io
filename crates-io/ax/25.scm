(define-module (crates-io ax #{25}#) #:use-module (crates-io))

(define-public crate-ax25-0.1 (crate (name "ax25") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.29") (optional #t) (default-features #t) (kind 0)))) (hash "0c617igxp6vspfazi6qv2l2ghpvjgiv8s9svk1lm4bplj2dbhplq") (features (quote (("linux" "libc") ("default" "libc"))))))

(define-public crate-ax25-0.2 (crate (name "ax25") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "01n5kkwp5av0r5xwcl3sm56j6d5g93yi5qhdxv5sry93zc62xw98")))

(define-public crate-ax25-0.3 (crate (name "ax25") (vers "0.3.0") (hash "0fqzn9zkb4rxyh055f27vx818idi238nhcxa2y56daapk8ccdzlp") (features (quote (("std") ("default" "std"))))))

(define-public crate-ax25_tnc-0.3 (crate (name "ax25_tnc") (vers "0.3.0") (deps (list (crate-dep (name "ax25") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.9") (features (quote ("local-offset"))) (default-features #t) (kind 2)))) (hash "1g8rk137s4fj76141q9cq5igxfnjgdicapaf4x3kh0hiqcld1cm6")))

