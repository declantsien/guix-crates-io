(define-module (crates-io ax ml) #:use-module (crates-io))

(define-public crate-axmldecoder-0.1 (crate (name "axmldecoder") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "16wvvq8wc8vs3nvdkrh1ngk598qchinb2b39djmvn81v9bqr3p22")))

(define-public crate-axmldecoder-0.2 (crate (name "axmldecoder") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1l570742zg339q3bpa472rkwkbjcwb5qix33hw8f5v4ly0mb5ajn")))

(define-public crate-axmldecoder-0.3 (crate (name "axmldecoder") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1jhqis78ll68v5jw7y9ha1jzaa4hgn9r9h9qj5b04hbsqmc3rvwy")))

(define-public crate-axmldecoder-0.4 (crate (name "axmldecoder") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "deku") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0sdyf9ydk16r6q6pizbv5zhy5brm930265mk1ra3ybkbs0qynck9")))

(define-public crate-axmldecoder-0.5 (crate (name "axmldecoder") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "deku") (req "~0.16") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "19qlzn0xi3pk7flgckh5kcx42jg33mli958vrkxpmaayal604dy8")))

