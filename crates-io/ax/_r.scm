(define-module (crates-io ax _r) #:use-module (crates-io))

(define-public crate-ax_rope-0.1 (crate (name "ax_rope") (vers "0.1.0") (hash "16vmlnwvd2j712d0q4ywrwpl5y9gv5c3whsnkw30zvzxh4kbryvn")))

(define-public crate-ax_rpc-0.1 (crate (name "ax_rpc") (vers "0.1.0") (hash "03syivqnsyazhc8pffcpkkjpqii0lyiy3hncwnjly8nsrbdf3wv7")))

