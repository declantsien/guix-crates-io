(define-module (crates-io ax is) #:use-module (crates-io))

(define-public crate-axis-0.0.0 (crate (name "axis") (vers "0.0.0") (hash "0lc13mgs8hkydnjl7ngv7fg1wn877mb0zqbln5pxqqrxyz9wmjq3") (yanked #t)))

(define-public crate-axis-ticks-0.1 (crate (name "axis-ticks") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1c885zbj0qrm1zp6wbrimbvh65lkhx941iicyb6yb3idrpq7338v")))

(define-public crate-axiston-0.1 (crate (name "axiston") (vers "0.1.0") (hash "03mli6gjmnd5s8irhziacsb31d58lsi84gdqras0g6g072b58w0j")))

