(define-module (crates-io ax im) #:use-module (crates-io))

(define-public crate-aximate-0.1 (crate (name "aximate") (vers "0.1.0") (hash "130clvv9qxbs8dhpr9lh0x4hl8wy7bd3zn4vpaigrb3m3zfk4shm")))

(define-public crate-aximate-0.1 (crate (name "aximate") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0n0qk7jxsm02n6kgvmmkj030bw3xx2dxcy5y9n27kbpsn2kgffsf")))

(define-public crate-aximate-0.1 (crate (name "aximate") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "18ikxvyj0i5amyks6g3qy1g09lrrvvw3bx0rfk9jh1q01nd7wywc")))

(define-public crate-aximate-0.1 (crate (name "aximate") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1bwy698wlgh7dgv03hazvcbhni4ns618k67qzwi9nyg7drdz5466")))

(define-public crate-aximate-0.1 (crate (name "aximate") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1plg8ns34kymkh4azvdyll32bhi06bwby07b4acdxbqc3ijdlyja")))

