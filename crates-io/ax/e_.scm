(define-module (crates-io ax e_) #:use-module (crates-io))

(define-public crate-axe_fx_midi-0.1 (crate (name "axe_fx_midi") (vers "0.1.0") (deps (list (crate-dep (name "ascii") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "coremidi") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0y6nhzzyrf0by55izak5dn7jiwzlkyfjs1aid2xnlbmi18kq87sq")))

