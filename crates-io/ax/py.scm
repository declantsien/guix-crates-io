(define-module (crates-io ax py) #:use-module (crates-io))

(define-public crate-axpy-0.1 (crate (name "axpy") (vers "0.1.0") (hash "1qjd3ixqsiag8z2sbp5kdmn4fimj65x8s4a556dmrki7hn4c5avf")))

(define-public crate-axpy-0.2 (crate (name "axpy") (vers "0.2.0") (hash "1zdnk29x8c320ws85vjsdjm71vc3sxa0ldv4pvia0sx3y0wwxfrj")))

(define-public crate-axpy-0.3 (crate (name "axpy") (vers "0.3.0") (hash "0mnrln292h8k6mknspvg7aabhwnhahva02a6z5nq9wfyvn15m1xj")))

