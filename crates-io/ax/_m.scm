(define-module (crates-io ax _m) #:use-module (crates-io))

(define-public crate-ax_macros-0.1 (crate (name "ax_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1qixradlamqn432x546ibwcp5j37xcj2326k01kxwk1wh4hbzp01")))

