(define-module (crates-io d8 #{8f}#) #:use-module (crates-io))

(define-public crate-D88FileIO-0.0.1 (crate (name "D88FileIO") (vers "0.0.1") (hash "1k1lk385bldhdlns3n80av7v3kyfh768wmswm3ajx0i1ypxk0vqs")))

(define-public crate-D88FileIO-0.0.2 (crate (name "D88FileIO") (vers "0.0.2") (hash "0fhpv57vs74ynkj011p8ynybhrp4b367xy603xac1r4bp5dbvzy8")))

(define-public crate-D88FileIO-0.0.3 (crate (name "D88FileIO") (vers "0.0.3") (hash "12gpamfppsm87smn7lq9frqlxbk700cbd696fsgnsafgixxznms3")))

(define-public crate-D88FileIO-0.0.4 (crate (name "D88FileIO") (vers "0.0.4") (hash "0mwf3jnx49y01z8rfwx7555y9v8n9jac17xdd6hxbxnp6g1givjh")))

(define-public crate-D88FileIO-0.0.5 (crate (name "D88FileIO") (vers "0.0.5") (hash "0jm18whw9q65ijxryq1fg6h6575zk9nf7awhpl4flxsr31hp77f1")))

(define-public crate-D88FileIO-0.0.6 (crate (name "D88FileIO") (vers "0.0.6") (hash "19ay3skj85xbsq6hpvafq5aibynha8023yz9k9a3ka85x2ckxncb")))

(define-public crate-D88FileIO-0.0.7 (crate (name "D88FileIO") (vers "0.0.7") (hash "0l4y221lbyv6acd1bjak94z7gnj48k7nb6bcx77lnp0sw2ippnv3")))

(define-public crate-D88FileIO-0.0.8 (crate (name "D88FileIO") (vers "0.0.8") (hash "0m8jj64vzsak2hy93scvkhhxqllmsxggkb25xxwv3g722wba12w2")))

