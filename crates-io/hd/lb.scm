(define-module (crates-io hd lb) #:use-module (crates-io))

(define-public crate-hdlbr-0.1 (crate (name "hdlbr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "1g7n72ip1vrcx45kbwfv91gpvc7jys6ribi1nsswvf2vjyk9glmn")))

(define-public crate-hdlbr-0.1 (crate (name "hdlbr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1lmbcq6r69666868r7c0ni0q216acdxagm3dw5cgagb0xfr8czb8")))

