(define-module (crates-io hd rl) #:use-module (crates-io))

(define-public crate-hdrldr-0.1 (crate (name "hdrldr") (vers "0.1.0") (deps (list (crate-dep (name "minifb") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 2)))) (hash "19q5cih6a48xa43myalswkazw0z0mpw8qzvsmqpcqjmg6rx0w96l")))

(define-public crate-hdrldr-0.1 (crate (name "hdrldr") (vers "0.1.1") (deps (list (crate-dep (name "minifb") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 2)))) (hash "1jvbd9s4k711q8zsr81sv4i5f3shz5smgqspa0d484aggvmawpx7")))

(define-public crate-hdrldr-0.1 (crate (name "hdrldr") (vers "0.1.2") (deps (list (crate-dep (name "minifb") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 2)))) (hash "02an9lwjx47x0nzl6b8nbn1wwlji86h0bi4x50i4d0mybry73jxn")))

