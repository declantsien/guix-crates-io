(define-module (crates-io hd c2) #:use-module (crates-io))

(define-public crate-hdc20xx-0.1 (crate (name "hdc20xx") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w7gjn8ficcjb1aywf2ijrx68wbnvcf68ijbj6qr7wyp8c55xcqk")))

