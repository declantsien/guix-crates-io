(define-module (crates-io hd iu) #:use-module (crates-io))

(define-public crate-hdiutil-0.1 (crate (name "hdiutil") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "binread") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "camino") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "0z4rhdndj06p3nw8w2zk71k851j3rynvwzyslg67z78vf3r6fqn1")))

(define-public crate-hdiutil-0.1 (crate (name "hdiutil") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "binread") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "camino") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "09hpnp3jinawa485jz272ac9lqw316qvkh6szq6r7gjnvspsxwgd")))

