(define-module (crates-io hd rc) #:use-module (crates-io))

(define-public crate-hdrcopier-0.1 (crate (name "hdrcopier") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0n8qsfj6l5gbhz3sszl70rj5fq9lw234g41hqcrlplva4ny70z5b") (rust-version "1.56")))

(define-public crate-hdrcopier-0.1 (crate (name "hdrcopier") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "1p7sgy3ysx7ryjcf0hqg8ypxww79r33k49jdajajzsw1zp6rmnps") (rust-version "1.56")))

(define-public crate-hdrcopier-0.2 (crate (name "hdrcopier") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0psrg5hhlpwfaxn0wpsx5yi2v154aqwhxyd7lrpcic92mrzl777h") (rust-version "1.56")))

(define-public crate-hdrcopier-0.2 (crate (name "hdrcopier") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "10fawdzn1fi3625gb4b7dh3khnn0xx6awab59q5zafsv5g5m9xfz") (rust-version "1.56")))

(define-public crate-hdrcopier-0.2 (crate (name "hdrcopier") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0vfbxd2bik7ingy10581xdgv5hbxmbyr8z2hafn3780f2k6bil8s") (rust-version "1.56")))

(define-public crate-hdrcopier-0.2 (crate (name "hdrcopier") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "03zsdba1xh89ld71amn5fchaaq787q303imkwsx9w1bcl5mx6kpk") (rust-version "1.56")))

(define-public crate-hdrcopier-0.3 (crate (name "hdrcopier") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.13") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "1ll1zg49w0spcsf29rh93kbxa94r8ah9g9dn7nf5y6lhff3hbkc0") (rust-version "1.56")))

(define-public crate-hdrcopier-0.3 (crate (name "hdrcopier") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.13") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "00vs0d6l7f7a0cyn9m8fgq131gkhf7iy4vvn805wa2ay3afr73kk") (rust-version "1.56")))

(define-public crate-hdrcopier-0.3 (crate (name "hdrcopier") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.13") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "12l6xf0wpspn95k7pi35d9a9kb49m1q04mb04gpr7nk56qkk2wib") (rust-version "1.56")))

