(define-module (crates-io hd ex) #:use-module (crates-io))

(define-public crate-hdexecutor-0.1 (crate (name "hdexecutor") (vers "0.1.1") (deps (list (crate-dep (name "hdrepresentation") (req "^0.1.82") (default-features #t) (kind 0)) (crate-dep (name "penguincrab") (req "^0.1.62") (default-features #t) (kind 0)))) (hash "1zw4gbi0l79gsj7ibak30gzg1halwj63k92syhfbxavvrg3fvaf8")))

(define-public crate-hdexecutor-0.1 (crate (name "hdexecutor") (vers "0.1.10") (deps (list (crate-dep (name "hdrepresentation") (req "^0.1.82") (default-features #t) (kind 0)) (crate-dep (name "penguincrab") (req "^0.1.62") (default-features #t) (kind 0)))) (hash "02hmd8jcm0q3mccwrzi2914j71n5nv3z30mnxgdjacidl22vnb4r")))

(define-public crate-hdexecutor-0.1 (crate (name "hdexecutor") (vers "0.1.11") (deps (list (crate-dep (name "hdrepresentation") (req "^0.1.852") (default-features #t) (kind 0)) (crate-dep (name "penguincrab") (req "^0.1.62") (default-features #t) (kind 0)))) (hash "0y42d6zhgnpa7k026ff7kfl1562qyhfa8vl9lvsa6435111n7313")))

