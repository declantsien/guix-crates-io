(define-module (crates-io hd lc) #:use-module (crates-io))

(define-public crate-hdlc-0.1 (crate (name "hdlc") (vers "0.1.0") (hash "13g18y9sr6l8m61vzvzjmznnd8djj724mslkp7ifbkdp2xsf3548")))

(define-public crate-hdlc-0.1 (crate (name "hdlc") (vers "0.1.1") (hash "003wjx0krdlnm50qj571aw9ghbf9qhrkm65k74jaijkdpgzlgn4z")))

(define-public crate-hdlc-0.1 (crate (name "hdlc") (vers "0.1.2") (hash "0r91nznxsz17z8f19vj0ahlyv4jlpcfq83qyj16k65ah1lvlf18w")))

(define-public crate-hdlc-0.1 (crate (name "hdlc") (vers "0.1.3") (hash "02d5x1s0zs7rl1mqnyajw1a3vxg7wl5n51czdc83f6qf69qal54s")))

(define-public crate-hdlc-0.2 (crate (name "hdlc") (vers "0.2.0") (hash "12p41akwd644dsimmks5ynm06d1cpw841m3jp6j0i6fga919kvx6")))

(define-public crate-hdlc-0.2 (crate (name "hdlc") (vers "0.2.1") (hash "1zhklczbhz11cx074h7fyscr240yrh0qmkkny2wkrfwhn57azx13")))

(define-public crate-hdlc-0.2 (crate (name "hdlc") (vers "0.2.2") (hash "1mz0kiwpzfpxm04sdyjbaj96fghzrd3ar5sizsxy7kfdqwg8f58m")))

(define-public crate-hdlc-0.2 (crate (name "hdlc") (vers "0.2.3") (hash "0ys2wqsd1wv9m99na3x82yjybwk29lhh3wd9rc82yyd1fj51qsq2")))

(define-public crate-hdlc-0.2 (crate (name "hdlc") (vers "0.2.4") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "18qx38wpv863q2kr320f09fi7wymf5wyiwy49s8b2ixamcsp23kg")))

(define-public crate-hdlc-0.2 (crate (name "hdlc") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1d791yzmd4micrnfxmj6vvvz804dvlyk5p48vgsb9j555abqagxg")))

(define-public crate-hdlcparse-1 (crate (name "hdlcparse") (vers "1.0.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "0vwph4aljj6xky5w90bq7lnk0qv944ys1p11spzx9lclshl0ckh6")))

(define-public crate-hdlcparse-2 (crate (name "hdlcparse") (vers "2.0.0") (deps (list (crate-dep (name "nom") (req "^7.1") (kind 0)))) (hash "0rl10gwpql0scy9hzrhs04vwcvmvj602prq31zfmf740cvi39rwk") (features (quote (("std" "nom/std") ("default" "std"))))))

