(define-module (crates-io hd c1) #:use-module (crates-io))

(define-public crate-hdc1080-0.1 (crate (name "hdc1080") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "byteorder") (req "^1") (kind 2)) (crate-dep (name "i2cdev") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "00x5fv52x02nfw2lbnjdm9mli74m22hknlgdsahlb8awgmv31wzw")))

