(define-module (crates-io hd bs) #:use-module (crates-io))

(define-public crate-hdbscan-0.1 (crate (name "hdbscan") (vers "0.1.0") (deps (list (crate-dep (name "kdtree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1wq7ks6shsnj80676qk4ifizi0wvqmgpdz6krscqgkrkac0rfak6") (yanked #t)))

(define-public crate-hdbscan-0.2 (crate (name "hdbscan") (vers "0.2.0") (deps (list (crate-dep (name "kdtree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1cypzyqlw3ng8qibqyzvrsyvdd2309f493j0kh8m76i4gs2c6lrm") (yanked #t)))

(define-public crate-hdbscan-0.3 (crate (name "hdbscan") (vers "0.3.0") (deps (list (crate-dep (name "kdtree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1wvf30m4k440iaxlciavvsywk62gd08ysir2rgkbmik09gz3y8lk") (yanked #t)))

(define-public crate-hdbscan-0.4 (crate (name "hdbscan") (vers "0.4.0") (deps (list (crate-dep (name "kdtree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "07hc9p31y37nic8mxc291hy4mm1fvisg4cbw22wihcigm3rp3qi6") (yanked #t)))

(define-public crate-hdbscan-0.4 (crate (name "hdbscan") (vers "0.4.1") (deps (list (crate-dep (name "kdtree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "19ga6bif1n6cr652g05w30p7wvar8438ahky2can0y6zafsz04m3")))

(define-public crate-hdbscan-0.5 (crate (name "hdbscan") (vers "0.5.0") (deps (list (crate-dep (name "kdtree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "19r2psqiskg5ypgz1sr0n83xknqh8pnk42fjavbq48a95gjzknv4")))

(define-public crate-hdbscan-0.6 (crate (name "hdbscan") (vers "0.6.0") (deps (list (crate-dep (name "kdtree") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1mr6f8mzfkvx0245n9yh1xf997n7whn714nw783ngsk652pqljgp")))

