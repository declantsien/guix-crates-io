(define-module (crates-io hd at) #:use-module (crates-io))

(define-public crate-hdate-0.1 (crate (name "hdate") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "hdate_core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00xadm41w2wnq3b8kbc09ymhmsw20p6p6q6nd2y7gwc5qnzjx6pl")))

(define-public crate-hdate-0.1 (crate (name "hdate") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "hdate_core") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0n78sxw39myvd54gp1pgsj5ddq2g9g7y0z7a6pd764k0c80b3ihj")))

(define-public crate-hdate_core-0.1 (crate (name "hdate_core") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1pw4ihwm20yy9hx838m80l9gg97s576rrkil381wj9clz85db1bc")))

(define-public crate-hdate_core-0.1 (crate (name "hdate_core") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1la34y3jn9k75f3jq92wlz95fwwk1w0a4hv5b7cwxiyd30hy00i6")))

