(define-module (crates-io hd dr) #:use-module (crates-io))

(define-public crate-hddrand-0.1 (crate (name "hddrand") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "size") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "16nw18a4i8qx15abgd7b99bavwcsxkqrh6rrzd4s1nhq5hb1d02i")))

(define-public crate-hddrand-0.1 (crate (name "hddrand") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "size") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0bsnyqs55a2dh1xxwv1nr1apwvcxm7nqa6kwwraka7hjzwk4hkmv")))

