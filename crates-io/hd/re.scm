(define-module (crates-io hd re) #:use-module (crates-io))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1rghdks7l4a6mqhlgqnqp67d99jakr73arng1nkhcw26xinghby8")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "04mi4sfsaf5mpx9q9pq5v6sjyn8dj445nmajwlvwby58glbjl6zl")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "06dsl5w1j6f65kz7vw90nxvlvs6x1smfk0q3si9qyz381ffrq8zx")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1jdw2f3wik2kz2iii726qn7s9l5d5my9qn4ry6r32szwj0j4s8f0")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1aip3f25q4a4wg6jd0jsl7l7fvimwlw36jmgrxzk6csjddha75yh")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0dx52xilmbjrsn5v2vzb6pazsckv45z8fp8vzgsc1gq0fdkdnq6g")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "16z6zbhsiwdbj2wrw19v6y6hgd6l2m6hchabd6gbgbcmafqkpbnf")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "000b1hs5mijhzb8qx4jcm49khnkqxx03p1bmbggrmg75404kvh7g")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "07zxwbi2fxckf0d72ad3vaikrbn7bacy2mg4d8xlysmc9fxmx5ww")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.81") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0m6zi7qn9xmybxi1bz4qh0y5953y27416xq8xm6vsvgahw689vb7")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.82") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-tuple-vec-map") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "111cwyx5hlqqhdqdzd1cj7vdy7fq9z1wvv7zsr3dwj090lfz1b80")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.83") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "0l0jd6wfbxhc3500x6a71z3b9ml7xdycb2nihkmy4cr8690d1d9c")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.84") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0ag8775sdjzmahs75z1y0rqlidk5wlyqy1271qm768dcqcj5vq8c")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.85") (deps (list (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1bcpg67j9577jlnc1jjrpmw6ab82bjnpcir1j9x379rgh6w6xj9p")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.851") (deps (list (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "13dqy0nb1cm77w4j2cf3hfp967p4n8dahlnq0hxj6y31g1jph0fh")))

(define-public crate-hdrepresentation-0.1 (crate (name "hdrepresentation") (vers "0.1.852") (deps (list (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0x55il7knvkyq06h2i0bb49fh3hgvmzg9qiwp576baj36hp2cqxx")))

