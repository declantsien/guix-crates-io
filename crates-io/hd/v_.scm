(define-module (crates-io hd v_) #:use-module (crates-io))

(define-public crate-hdv_derive-0.1 (crate (name "hdv_derive") (vers "0.1.0") (deps (list (crate-dep (name "hdv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "01jq9ac5ac2f7497m61kg01x3z7ni6s8k0abq11cb0gdnclfgchk")))

(define-public crate-hdv_derive-0.1 (crate (name "hdv_derive") (vers "0.1.1") (deps (list (crate-dep (name "hdv") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0dqs7x7m07xjl79240rirw4qv0d1ngj2bk4cwsli78zz5y5ipwxg")))

