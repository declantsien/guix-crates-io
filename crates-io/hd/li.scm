(define-module (crates-io hd li) #:use-module (crates-io))

(define-public crate-hdlibaflexecutor-0.0.1 (crate (name "hdlibaflexecutor") (vers "0.0.1") (deps (list (crate-dep (name "hdrepresentation") (req "^0.1.82") (default-features #t) (kind 0)) (crate-dep (name "more-asserts") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1xjbi05d3cs9fphgzn6vg7qhskw7vy89cjnjjlfq3ffx9npxpx3m") (links "shared")))

(define-public crate-hdlibaflexecutor-0.0.11 (crate (name "hdlibaflexecutor") (vers "0.0.11") (deps (list (crate-dep (name "hdrepresentation") (req "^0.1.82") (default-features #t) (kind 0)) (crate-dep (name "more-asserts") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1d2lsp534g5rx6n7xsn0ryb8lc2i16s9i49sryk10vg33np1c5ac") (links "shared")))

