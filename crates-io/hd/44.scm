(define-module (crates-io hd #{44}#) #:use-module (crates-io))

(define-public crate-hd44780-0.1 (crate (name "hd44780") (vers "0.1.0") (hash "19xkfx5xvimg1896ay2bkvb5pr07y7xrbg0zng137hiya8mmack6")))

(define-public crate-hd44780-0.2 (crate (name "hd44780") (vers "0.2.0") (hash "1jsbq7bavyfh2m5xz97rp1g15a8b99nxf2a004nlnzvykpgw9xsb")))

(define-public crate-hd44780-0.2 (crate (name "hd44780") (vers "0.2.1") (hash "1q7489q74wj7nlmcp63nh59fil13vyz3m1k24qrlq7m4xlvhdan0")))

(define-public crate-hd44780-0.2 (crate (name "hd44780") (vers "0.2.2") (hash "16ffh15fwcph2xphrag4pn37v0sv88irwcryjcgwriqzxacfj0ny")))

(define-public crate-hd44780-driver-0.2 (crate (name "hd44780-driver") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "11qn1dwbwm6nnqkgjv5q4wf6r0m7drvs0qwcdv7xw8rlkycyzc4m")))

(define-public crate-hd44780-driver-0.2 (crate (name "hd44780-driver") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "11q2d0gz527f48l3akdwfpqrcb87bfjsiwgayympvhx7hg5rznx6")))

(define-public crate-hd44780-driver-0.3 (crate (name "hd44780-driver") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sf8k81zavrrf5c5p191y6mfhgcika89w1x50pgw2qrgs7d2m5s9")))

(define-public crate-hd44780-driver-0.4 (crate (name "hd44780-driver") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0fkxhlad3ai82iivb5iiikyair5x5j5w4mrs2glxvmxfvqzv3cma")))

(define-public crate-hd44780-hal-0.1 (crate (name "hd44780-hal") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "18dcnab7v3bkvb3aw0007csy23nqqfxjfyaczkwinwhklqpcx0zs")))

(define-public crate-hd44780-hal-0.1 (crate (name "hd44780-hal") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gznsisiaq7hlbxzrwmbrci32h1ydmid5f5hygikpinhx3xmizvd")))

(define-public crate-hd44780-hal-0.1 (crate (name "hd44780-hal") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q7jk4sm3xca1gw75yrv644yf5fr0653nwbf112dyrdawh92729j")))

(define-public crate-hd44780-hal-0.1 (crate (name "hd44780-hal") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1527m2qq326y912zqspwbp99bc5wp94zj5slys506r88bx7q9bh1")))

(define-public crate-hd44780-hal-0.1 (crate (name "hd44780-hal") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0a7mci4hkxygm8wmxgfd3mbg65qcdb7hpp7f62qim3rzqx5y4d42")))

(define-public crate-hd44780-hal-0.2 (crate (name "hd44780-hal") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g9z98aikvr1133wcx7463brj0iwza311f51xqv4akf7p88s8k3g")))

(define-public crate-hd44780-hal-0.2 (crate (name "hd44780-hal") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s400yii6xi304z4yc5wajcq1h67b62wrbyh0dcw8yfjfs7lncj0")))

(define-public crate-hd44780-ntb-0.0.4 (crate (name "hd44780-ntb") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "sysfs_gpio") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "01ll58s6jgf61ws4p2ksrka95z8wg336vyca2v146p7bhzp9mzbw")))

(define-public crate-hd44780-ntb-0.0.5 (crate (name "hd44780-ntb") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "sysfs_gpio") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "03ryf73r6457803f6fkmq71hbg48mm0wh8lscyf4m1nckq922mkn")))

(define-public crate-hd44780-ntb-0.0.6 (crate (name "hd44780-ntb") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "sysfs_gpio") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1zraja1489pnrmdd115ynry568limrmk2c47gvwbz1mib75lnan3")))

(define-public crate-hd44780_menu-0.1 (crate (name "hd44780_menu") (vers "0.1.0") (hash "094cynyj8ff9yihy803la92j5p5gjsjxpn18wm8nvnjymkr5sn3v")))

(define-public crate-hd44780_menu-0.1 (crate (name "hd44780_menu") (vers "0.1.1") (hash "0c9jnjba9iwgnzpysvg636yb1y0gnnfyxzwyadr325j4glqfwn2m")))

(define-public crate-hd44780_test-0.1 (crate (name "hd44780_test") (vers "0.1.0") (hash "1prp7ikwckhwy281ybmjwkxzx658wssji52aplwwj3ppidlv9kwh")))

