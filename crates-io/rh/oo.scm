(define-module (crates-io rh oo) #:use-module (crates-io))

(define-public crate-rhook-0.1 (crate (name "rhook") (vers "0.1.0") (hash "1j1lr7h710zijqjar9ddq3p9q3v4b0xd92z08mlfxhzqgqhf8ml5")))

(define-public crate-rhook-0.2 (crate (name "rhook") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0pclwiilx4fx2gzn6gw5rrg9arfcji5bjv4v2gqvwsiygzj6jfb4")))

(define-public crate-rhook-0.2 (crate (name "rhook") (vers "0.2.1") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "157cghzh3pqfk0i6167zlxzfx334ghhzxi4i0vvrv6csmfvfdcpc")))

(define-public crate-rhook-0.2 (crate (name "rhook") (vers "0.2.2") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "15p6bwh1jgimxbf6px2rli77bnz2xpzaiv820j8vv7kakynq1vz3")))

(define-public crate-rhook-0.3 (crate (name "rhook") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "13aw88aib3wvqj8n26hr785l26l2s5flig8awvv10zgipzp6gd1s")))

(define-public crate-rhook-0.4 (crate (name "rhook") (vers "0.4.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "15r07m30nyrggwc34nqpg31s2k8y9lyka86nm84yg0fcd43r7byk")))

(define-public crate-rhook-0.5 (crate (name "rhook") (vers "0.5.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "01s76qd92252sfhig2w2nbhrrwcp21s41wwhymhy2dw21nmzzgxx")))

(define-public crate-rhook-0.8 (crate (name "rhook") (vers "0.8.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "171k4n0lld5ny3l2ihb74pfdmv1r5bmv58md10323a83zriwd207")))

