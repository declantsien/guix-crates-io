(define-module (crates-io rh ep) #:use-module (crates-io))

(define-public crate-rhep-0.1 (crate (name "rhep") (vers "0.1.0") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^2.1") (default-features #t) (kind 0)))) (hash "1q8mcx0ww3sp4x44m2ncsak1dmrkpzgn9m9il8c21kljrqaw7xbi")))

