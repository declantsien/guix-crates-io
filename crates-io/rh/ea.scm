(define-module (crates-io rh ea) #:use-module (crates-io))

(define-public crate-rhea-0.1 (crate (name "rhea") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11flprr0z8jfv93rfhvxm5q9hy90xbdagxxv2x015vqqjs43lnnn")))

(define-public crate-rhea-0.1 (crate (name "rhea") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pgj96vdbzwhkdd6zhbai694cswbjfwbwca50llxacwnx7d3nxdq")))

(define-public crate-rheap-0.1 (crate (name "rheap") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ibz4bhk1bsqfhr31mmj8x19fpf1b3iz37ccz9v48p9br902r5yd")))

