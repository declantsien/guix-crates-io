(define-module (crates-io rh ym) #:use-module (crates-io))

(define-public crate-rhyme-0.1 (crate (name "rhyme") (vers "0.1.0") (deps (list (crate-dep (name "cmudict") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0pq245a8xv7q316ggvdbh8mafc9530d3bwp41rdzdrrq1h9mqnff")))

(define-public crate-rhyme-0.1 (crate (name "rhyme") (vers "0.1.1") (deps (list (crate-dep (name "cmudict") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0b6x1azwnd477c2pn8xvhdhzcn9w83qlcasn8ayd9b961665f586")))

(define-public crate-rhyme_dictionary-0.0.1 (crate (name "rhyme_dictionary") (vers "0.0.1") (hash "0z7k32y558l5ap3nw1l8jqxanp00by701kr09hlwjzw43xdf7843") (yanked #t)))

(define-public crate-rhyme_dictionary-0.0.2 (crate (name "rhyme_dictionary") (vers "0.0.2") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0a3kjxr4vjafk9qsncrinwhjbc0r3ad13swk8wymzq1cp1rhnpn5") (yanked #t)))

(define-public crate-rhyme_dictionary-0.0.3 (crate (name "rhyme_dictionary") (vers "0.0.3") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1zbfcsgxcr1faxmlvgv5c024lld6s858fbsfys17l95jpmmf9vi5") (yanked #t)))

(define-public crate-rhymessage-1 (crate (name "rhymessage") (vers "1.0.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "187wzwg84fn7xj8jbxg4h17y4yma7jq79pwxvimwj25cdr8mqnfl")))

(define-public crate-rhymessage-1 (crate (name "rhymessage") (vers "1.0.1") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "11n1szrj0p409zxbwjwp19ld21sbpr3rjk5pgabl16wfvphbxrrn")))

(define-public crate-rhymessage-1 (crate (name "rhymessage") (vers "1.0.2") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bw4i7lg3pvy7byv8wqrp02c2sdlsajd8mb1idfjvpn4d3x5w4pk")))

(define-public crate-rhymessage-1 (crate (name "rhymessage") (vers "1.1.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lqj4jnsrisaidwli788ww450rlf0im0nh5ycq4qn1z97vl6zm98")))

(define-public crate-rhymessage-1 (crate (name "rhymessage") (vers "1.2.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g10b0gpfmpy69d4jb3mx5lb9pkk3l3ybr4igsxcqrd3kyr0dn2v")))

(define-public crate-rhymessage-1 (crate (name "rhymessage") (vers "1.3.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ysi1lrcyb9h62hry2pxqswcpl1a4h3lkclkl7rnx1l7gmlq3cfp")))

(define-public crate-rhymessage-1 (crate (name "rhymessage") (vers "1.3.1") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y9dlx9pcwin910spmfzmkblipb2dknddabasqjlaahiyjwjk7cy")))

(define-public crate-rhymuproc-1 (crate (name "rhymuproc") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1gfs6glj16bsshzzb8sz13ap7vahaxxighfvlwb8x5gd6lhnff40")))

(define-public crate-rhymuproc-1 (crate (name "rhymuproc") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0fhjzmbayjgmaaxxd00ahvc7vvimh8qii3kacvfdyipa2fjvlmlx")))

(define-public crate-rhymuproc-1 (crate (name "rhymuproc") (vers "1.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0p7z53v6zr3dab7wcmqg9g2wy6qibacf2h2dyglwfpvds742mjdh")))

(define-public crate-rhymuproc-1 (crate (name "rhymuproc") (vers "1.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "0f5l7l8mjyfk7drjsfdxcwvxwkcnyk07zff4hxwdzchj6a18y5m4")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.0.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wwk6l9s0wgvh5rv6dfxkxgl7m8dvkjch3w0mci5j7ggqbaiw31a")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.0.1") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1igmdf40sqj4lg5igzw8lx42374v3n7g8k59frmkzz6qs4k7lk0d")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.0.2") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "19vj45ikkm5l04fk66wap8w0wdss5zxzmx73m47gw7lq36lxj1br")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.1.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "082fq2xr6zbhf4s3q29yxs0qn45xsrmj3kpzjfrjmmp8whfzynxd")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.1.1") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "120s5vyy2ra6l5k3lw9dbg1jy66lpgwpi7ziw4889g85ff5iq54s")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.2.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i9xf6gd0ddxh60p78ac1gnxin62zknwfkcfd639y0m3dl3m2i7s")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.3.0") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l6rjxhshr8qkr4cdf199rjzj9r9pa9z2jsraq9knd1vxw9hfy40")))

(define-public crate-rhymuri-1 (crate (name "rhymuri") (vers "1.3.1") (deps (list (crate-dep (name "named_tuple") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r2vr6yd0y3f1x593a60c8kz39fdnbnwxakc5z223h6k01zbnd1i")))

(define-public crate-rhymuweb-1 (crate (name "rhymuweb") (vers "1.0.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rhymessage") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rhymuri") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ypjgyq4iam6qh0phhb1b9da6q65wbjl82s4i7wncdfkg1zvvpmr")))

(define-public crate-rhymuweb-1 (crate (name "rhymuweb") (vers "1.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rhymessage") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rhymuri") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1frr48vwf2qc1ar4iz4g0lp9hlik52xlkb2cpgs82030pihcy66f")))

(define-public crate-rhymuweb-1 (crate (name "rhymuweb") (vers "1.2.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rhymessage") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rhymuri") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ja4jz19g70phqz7a4j71575byphz11r75mvn7lx64n0x634jj22")))

(define-public crate-rhymuweb-1 (crate (name "rhymuweb") (vers "1.2.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rhymessage") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "rhymuri") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ald9mvbfl2k1rgk40zc8kqff65f5fv491pbd6l208q70akry7ik")))

