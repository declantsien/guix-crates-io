(define-module (crates-io rh dl) #:use-module (crates-io))

(define-public crate-rhdl-0.1 (crate (name "rhdl") (vers "0.1.0") (hash "1945dfm6bh7cf79s5syhrkxbmd1z1r21m49x54424lspc1jrcm30")))

(define-public crate-rhdl-bits-0.1 (crate (name "rhdl-bits") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1bnb773xiab7c7vypvd1514awphg2599dkiy029c3bkar289vkxh")))

