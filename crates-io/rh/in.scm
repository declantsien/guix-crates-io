(define-module (crates-io rh in) #:use-module (crates-io))

(define-public crate-rhind-0.0.1 (crate (name "rhind") (vers "0.0.1") (hash "1apnwg25qddmi9vrql0mk10ak8fpdmg0d1kqqlx9p9hgn2s1bk7c")))

(define-public crate-rhinoceros-0.1 (crate (name "rhinoceros") (vers "0.1.0") (hash "0b4rg8pdvszrwh7xcv9fpwaj4y11c80rwmr34vriv34p4f1km8gn")))

(define-public crate-rhinopuffin-0.2 (crate (name "rhinopuffin") (vers "0.2.1") (deps (list (crate-dep (name "aes-gcm") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0ip68dify0g7lwsvb5lr95dq78p06f68ydvbxnwdp0pa99bwklf5")))

