(define-module (crates-io rh tm) #:use-module (crates-io))

(define-public crate-rhtml-0.1 (crate (name "rhtml") (vers "0.1.0") (hash "0hdj2if53xj8vfgsik2x9bgck7cndrbzbmsb2j292q1myb2d15jj")))

(define-public crate-rhtml-0.1 (crate (name "rhtml") (vers "0.1.1") (hash "0p9claddf6a111s1whnghdxmd9picqksd49kblb94hcwkgr37qgj")))

(define-public crate-rhtml-0.1 (crate (name "rhtml") (vers "0.1.2") (hash "12d3ci2n989h03714vk4z75aqzz4chvmq3brpbg7y1zhnk8d0cqk")))

(define-public crate-rhtml-0.1 (crate (name "rhtml") (vers "0.1.3") (hash "1d6cf90k6kyd7rr4rlv3cp1m1q1hw6mfx556rvzf61c1q7pzhd1m")))

(define-public crate-rhtml2md-0.2 (crate (name "rhtml2md") (vers "0.2.13") (deps (list (crate-dep (name "html5ever") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "jni") (req "^0.19.0") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "1fn35mz82y6awc08appbbwya7jgas9vdc8pafipy4b146fi2h1pp") (yanked #t)))

(define-public crate-rhtml2md-0.0.1 (crate (name "rhtml2md") (vers "0.0.1") (deps (list (crate-dep (name "html5ever") (req "^0.25.1") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "jni") (req "^0.19.0") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "1qh6xxzz6h4ay9vsjjfpg1m4f1nclb4lv33m0g0x3sfij3zhrz88")))

