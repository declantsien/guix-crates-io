(define-module (crates-io rh tt) #:use-module (crates-io))

(define-public crate-rhttp-0.0.1 (crate (name "rhttp") (vers "0.0.1") (deps (list (crate-dep (name "openssl") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.2.37") (default-features #t) (kind 0)))) (hash "0n03mfybpiff4l5bxjn0l22q83hxaynr0p92icwxjqdzw7vw96j9")))

(define-public crate-rhttp_status_code-1 (crate (name "rhttp_status_code") (vers "1.0.0") (hash "1y73r2487gxlz09dlqr5jazarqdxryh87k069cmvdz4yd0qplqyk")))

