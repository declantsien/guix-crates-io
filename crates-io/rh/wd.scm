(define-module (crates-io rh wd) #:use-module (crates-io))

(define-public crate-rhwd-0.0.1 (crate (name "rhwd") (vers "0.0.1") (deps (list (crate-dep (name "diesel") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.12.35") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1vls0lv59i6wx2yr9s3ckisg91a2g5jvr7nngaza4878zrdf9r1g")))

