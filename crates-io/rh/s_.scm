(define-module (crates-io rh s_) #:use-module (crates-io))

(define-public crate-rhs_first_assign-0.1 (crate (name "rhs_first_assign") (vers "0.1.0") (deps (list (crate-dep (name "if_chain") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.8") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.14") (features (quote ("full" "parsing" "printing" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1rr0021mfbmv8mvzyqn32awpgxq1fna5alkn14k32xd1zppmnp96")))

