(define-module (crates-io rh um) #:use-module (crates-io))

(define-public crate-rhumblinelib-0.1 (crate (name "rhumblinelib") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0wyb6649b5320pns0xjfwcqblg7qmh009c3077wibr0h7m9a6ydx")))

(define-public crate-rhumblinelib-0.1 (crate (name "rhumblinelib") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0mzlx0jcb95xilh76w23ax4wynh119wq4fqf4cc46ac2smz24h5s")))

(define-public crate-rhumblinelib-0.1 (crate (name "rhumblinelib") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0yz198mnq6jxd5618fwvd6yyg5380r53067d5583axv5r8rxp44y")))

(define-public crate-rhumblinelib-0.1 (crate (name "rhumblinelib") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1lzb9nqs9n1wcasbnj2bxd2k7rmwqyn7vlwy4wfv02vrvwrw0a7y")))

(define-public crate-rhumblinelib-0.1 (crate (name "rhumblinelib") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1cd51pfwdbaz1x1s9vznf1cgnw6xpyswb4l98vgfvsxdr4h5qc87")))

(define-public crate-rhumblinelib-0.1 (crate (name "rhumblinelib") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0jgpzkqxlhapary6cphnlqy8sbcm7ffg5pqas8rx8r4r5df7ahkd")))

