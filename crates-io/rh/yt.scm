(define-module (crates-io rh yt) #:use-module (crates-io))

(define-public crate-rhythm-0.0.1 (crate (name "rhythm") (vers "0.0.1") (hash "0ng8ars3q3940an09nny6nmbvx6xx64s2269w2pwsmvqrbbxp374")))

(define-public crate-rhythm-0.0.2 (crate (name "rhythm") (vers "0.0.2") (hash "0y1bzvl5vdrmjmjbqafybfbym7vvjam7754m9d4w0xvxn6rp59pf")))

(define-public crate-rhythm-0.1 (crate (name "rhythm") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1gsi4hypl1i181hfp93nyrsccdms8qh3fs8ydrwg0wns3b38jbn8")))

(define-public crate-rhythm-core-0.1 (crate (name "rhythm-core") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "132aa0ak1c2ayyywiccnaizhizhyxb7cp8jpfjb3yj0y4d46wkva") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-rhythm-core-0.2 (crate (name "rhythm-core") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0h0y125ydpcx6kqnc12r1ph7fh8hhmp2l62kh5d578w0zgpqaj4i") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-rhythmc-0.0.2 (crate (name "rhythmc") (vers "0.0.2") (deps (list (crate-dep (name "rhythmc_compiler") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "rhythmc_macros") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing"))) (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.19") (default-features #t) (kind 0)))) (hash "07wh7nmmbgci8c3q8w1a35jjwskiv9lg0hpsicyjdwlgffd81yhn")))

(define-public crate-rhythmc_compiler-0.0.2 (crate (name "rhythmc_compiler") (vers "0.0.2") (deps (list (crate-dep (name "glsl") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.54") (features (quote ("parsing" "printing" "full"))) (default-features #t) (kind 0)))) (hash "1mddfglvyhady1z5zy3fir0vapngyrvq47kcn1xgrh4sjqdp85dp")))

(define-public crate-rhythmc_macros-0.0.2 (crate (name "rhythmc_macros") (vers "0.0.2") (deps (list (crate-dep (name "glsl") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.23") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "rhythmc_compiler") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.54") (features (quote ("parsing" "proc-macro" "full" "extra-traits" "printing"))) (default-features #t) (kind 0)))) (hash "0lsm196zgf2src6npci7sy75x44g20kj7a17jz666qd5pcwywrp6")))

(define-public crate-rhythms-0.1 (crate (name "rhythms") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1.5") (default-features #t) (kind 0)))) (hash "0n7y9mr7nkiyvgcd6kg78lhhf9jxlsvl5n71qb1nk70gmkcb2fr0")))

