(define-module (crates-io rh ex) #:use-module (crates-io))

(define-public crate-rhex-0.1 (crate (name "rhex") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.86.0") (default-features #t) (kind 0)))) (hash "0djdja11pyaz2cmkjgd4pgnw3vchni37qrlmbjciw202hxpa056q")))

(define-public crate-rhexdump-0.1 (crate (name "rhexdump") (vers "0.1.0") (hash "0467kqh1hya60x97880bxwqrirm9ahyl021vxqpfvr59q8vakkjm") (yanked #t)))

(define-public crate-rhexdump-0.1 (crate (name "rhexdump") (vers "0.1.1") (hash "0iwqawqszy3apj4ci1z0l068i2vwk4x33h6i4jgy6da9axjazsf5")))

(define-public crate-rhexdump-0.2 (crate (name "rhexdump") (vers "0.2.0") (hash "1isihflwds9s1wxf0s46adxf319vbl8kkwzyfwviw8nxlghwp4ls")))

