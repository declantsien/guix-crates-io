(define-module (crates-io rh yo) #:use-module (crates-io))

(define-public crate-rhyoea-0.1 (crate (name "rhyoea") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)) (crate-dep (name "colored") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)))) (hash "18i7z9gxd9x0rrv2b46n1kxa1i5a48fwl6b5ygmnld6bi46xi78y") (features (quote (("no-color" "colored/no-color"))))))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0k1rn6l7b0gpyh4973d89pvi7v3rz7lzn6h7x8p56bmxndyrr0sh") (yanked #t)))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.2") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0a7pividcwl7zqhk1ys1ds89c8zrzwfq348aljx4a3lhib4pdzja")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.3") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0g6xlvhgx5qq514ggkaa1jgxyhgsp7wsih271wk279y5j06if6fx")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.4") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0k3yjnzfcsk6w7anynypkrgw9zs8yqi7yw8b9w7vpcx1wg8ib2mz")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.5") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0nydy1cxrgrahhlwnx9lh6md9kcb1vc29vs1z4aj313saynfavyx")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.6") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0ff6l5z78nrn456kcc3sx4jdz2g4dr09szkbfxpcra8nvw6ycajb")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.7") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "1rgxf0gvvl5h429vzidbrpkz6dwi5cw7bfn8nx5a2rf17fchpzlb")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.8") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "1jyp53gzpg6dgq5x4ggwj4l3i5ringg7zim15h9rhizq6lap9n4d")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.9") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "1ksl9wzyljjhsg47ja9s74gf3268i2gnwvqvid1g3bpbs1zsbfga")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.10") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0mlb0164vsmyw4ld6br8r531l7p471v4pijvl241jb1jf9fk3vcw")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.11") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0zwg0jxjmyhx2l1dhkwdq2aspgg050i0g5pwx2q7mjw1f1d5gva7")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.12") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0frvbl95gzfa4zp6fad4whzwjj282djbhakx7hcj88ch2cnpb7bv")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.13") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "17827qfkwp3mrip3y2x2k302ljdqbzjz86nfy2fp8gpgqzzgnf8g")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.14") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0mlxbh4ij93fcjdk9zds29is14xbiwbngm221gn07zndrkla1yd3")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.15") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "1fg49qkppdlbqd9bkl3mqlg87bm0w2dbgxlwpjgsa7knn9l5kj9p")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.16") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "1livrmxwdvfm2vgw0bw6z13cbmycpyrdfkcdx65jps6hqpdmhlcb")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.17") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0vyx9d58mjshybkgj6nfdgi7r75maq1pjd7a5zyh7lsq64z6hpsv")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.18") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "15gy8r27jr4qvhs2rxr61hkmvmpaf3nl81hc2dwbd42c70dajaic")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.19") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "0grglz4hwgwl3cvfkdsnfjijnz2jjyyk8185sc80di7l597013k1")))

(define-public crate-rhyoea-common-0.1 (crate (name "rhyoea-common") (vers "0.1.20") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (default-features #t) (kind 2)))) (hash "05xyysackyyg6n04pn7wl801c9h9496n6j2n3ic7a4w5v3nb6isl")))

