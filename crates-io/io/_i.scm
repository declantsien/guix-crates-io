(define-module (crates-io io _i) #:use-module (crates-io))

(define-public crate-io_interner-0.1 (crate (name "io_interner") (vers "0.1.0") (hash "0d6gpn6yg1b9ai7dlnpj2dilaqfzdwx125f793jzxzfjh9q18pmc") (yanked #t)))

(define-public crate-io_interner-0.1 (crate (name "io_interner") (vers "0.1.1") (hash "0wzd1x2n0hax2vf5b3m9fhmy6z7i3gjs211ybfrdczhlncd8k61y")))

(define-public crate-io_interner-0.1 (crate (name "io_interner") (vers "0.1.2") (hash "19zvp76aafcj97p75lqc715px1ipqw8xy1lqxx3lsbxj2jbjjkgv")))

(define-public crate-io_interner-0.2 (crate (name "io_interner") (vers "0.2.0") (hash "16fv429wm8b9yay7frcy9ml58r02fzf7l5y357rrf8ba0fdkmv1c") (yanked #t)))

(define-public crate-io_interner-0.2 (crate (name "io_interner") (vers "0.2.1") (hash "1i1l2z59kgqzaf602irywv4z4kz6h2rwf12ivghh7gbzig2aba00") (yanked #t)))

(define-public crate-io_interner-0.2 (crate (name "io_interner") (vers "0.2.2") (hash "0c18yh36wp51cgd83lfz6p88wn5g74r1c40srxbalhihdjxx6am3") (yanked #t)))

(define-public crate-io_interner-0.2 (crate (name "io_interner") (vers "0.2.3") (hash "1km42gh3llnp5clyfadsj6x9j0c38hrabblxwq0g64y7vpr5mwn4")))

(define-public crate-io_interner-0.3 (crate (name "io_interner") (vers "0.3.0") (hash "0xix923kwvyp5qggmf4ms3qzxx83iw77nyfc5ihfz762d4w5wwjp")))

(define-public crate-io_interner-0.4 (crate (name "io_interner") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sync_2") (req "^1") (default-features #t) (kind 0)))) (hash "01abvldg4ir8g0sk500v442k1gnrpsmlpwqcrwzqxqzg96zchdcy") (features (quote (("serde_support" "serde" "serde_derive" "lazy_static"))))))

(define-public crate-io_interner-0.4 (crate (name "io_interner") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1h0r6y24l3bcai41pmh6sfid1lgq820w1lm7jj06y24dzhpp5g16") (features (quote (("serde_support" "serde" "serde_derive" "lazy_static" "memchr") ("nightly"))))))

