(define-module (crates-io io pr) #:use-module (crates-io))

(define-public crate-ioprio-0.1 (crate (name "ioprio") (vers "0.1.0") (deps (list (crate-dep (name "iou_") (req "^0.3") (optional #t) (default-features #t) (kind 0) (package "iou")) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.19") (default-features #t) (kind 0)))) (hash "1wwm3gvn0iiwqqnxc9894k8j4kha83mg0033w234bkws24jsvz4c") (features (quote (("iou" "iou_") ("docs" "iou") ("default"))))))

(define-public crate-ioprio-0.1 (crate (name "ioprio") (vers "0.1.1") (deps (list (crate-dep (name "iou_") (req "^0.3") (optional #t) (default-features #t) (kind 0) (package "iou")) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.19") (default-features #t) (kind 0)))) (hash "0mwx94vjbd2grzprpzsr9b2wsr1w5kp1qqxmlc92fynzpfcc4aar") (features (quote (("iou" "iou_") ("docs" "iou") ("default"))))))

(define-public crate-ioprio-0.2 (crate (name "ioprio") (vers "0.2.0") (deps (list (crate-dep (name "iou_") (req "^0.3") (optional #t) (default-features #t) (kind 0) (package "iou")) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.21") (default-features #t) (kind 0)))) (hash "1z1fg2pwcv8nd2zk9k3yhphsw2dny569fkvcbj79aiqgjnd3rl5f") (features (quote (("iou" "iou_") ("docs" "iou") ("default"))))))

