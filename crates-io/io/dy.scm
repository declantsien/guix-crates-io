(define-module (crates-io io dy) #:use-module (crates-io))

(define-public crate-iodyn-0.2 (crate (name "iodyn") (vers "0.2.0") (deps (list (crate-dep (name "adapton") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jyk6f4mdmfzmy1k46kxbr8k23v925hix6mvmh3dpb347abkfh0h")))

(define-public crate-iodyn-0.2 (crate (name "iodyn") (vers "0.2.1") (deps (list (crate-dep (name "adapton") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pf8pnns57d97mp91j4s7xcvr3w0w95a4s6a23w5mb397alav1ih")))

