(define-module (crates-io io nt) #:use-module (crates-io))

(define-public crate-iont-0.1 (crate (name "iont") (vers "0.1.0") (deps (list (crate-dep (name "ions") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1y7s8gjbjnvg0cka7hymd5qrxd2bc4vajgw20v980jnn3j1mqb7i")))

(define-public crate-iont-0.1 (crate (name "iont") (vers "0.1.1") (deps (list (crate-dep (name "ions") (req "^0.1") (default-features #t) (kind 0)))) (hash "11m19js34cw4lc30m6x3imf7v74qvg60jki8rcjmyhksbv0qp14h")))

(define-public crate-iont-0.2 (crate (name "iont") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ions") (req "^0.1") (default-features #t) (kind 0)))) (hash "01pcbh7c4jmqw9661rfk6xp5y0fyrhbcw9d522haq6krj8pqdasw")))

(define-public crate-iont-0.2 (crate (name "iont") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ions") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p4yq1kaksafpm10n3xz1qfik5b9izpqlpnyqlm2ddvmrfkn11dd")))

(define-public crate-iont-0.2 (crate (name "iont") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ions") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gars2c8iwbbzqb301b7pdrc57rsa433394rw1am5vicni241p1h")))

(define-public crate-iont-0.2 (crate (name "iont") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ions") (req "^0.1") (default-features #t) (kind 0)))) (hash "1diswysw10n0qjvcdrqj4lid6w69492jkri5frw12byynwaks3di")))

