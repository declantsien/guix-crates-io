(define-module (crates-io io _e) #:use-module (crates-io))

(define-public crate-io_err-0.1 (crate (name "io_err") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "09gwjs1xqpwz4qsc22jpx02kn3f42nrcbykc1sl0lj97w92s66bg")))

(define-public crate-io_ext-0.1 (crate (name "io_ext") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "03dbfxfn2cfapwn03qx7am3a53m27f9qy2amgvfqlky334w9wswb")))

(define-public crate-io_ext-0.2 (crate (name "io_ext") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1833s26r1n83qrlbs6kpwmaqfgv4mgw14sxscnrhcp5kn42hp3n5")))

