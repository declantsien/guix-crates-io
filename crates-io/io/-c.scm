(define-module (crates-io io -c) #:use-module (crates-io))

(define-public crate-io-cast-0.1 (crate (name "io-cast") (vers "0.1.0") (hash "1a22dj3svgwj0y7wywba1y9cbm9zkias6fc6agb0dhgh9q3gkwvv")))

(define-public crate-io-cast-0.1 (crate (name "io-cast") (vers "0.1.1") (hash "13s5p2nydr6fi619grqpgk64qyl8ijh0n3vvyl6c8qvy13smvf07")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "1a6p6vy8sxkpn3hdh2gqk43848wjsklj29q71bgk5xn4d03dhj0y")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "0naqj96dsdm6h4y9x3vax5fd35051pqnrfnr5zxbfxrlyns4v984")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)))) (hash "0yajg8wmnvkf0pc86bll125gc9rv5qp6r1jkgjjn263sb2h08v4x")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.3") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1hs7v26jcmymvjgs5aazb0bx6ddc091haw42a2y9nk12y1bbkcjg")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.4") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1xy3sk5q7dr0wmhlzw6simjmi86xx9vdgjj2c2y50nrglnbyjp7f")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.5") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "0h00940vnmbcy404w8zblfyr3wj4pam4rmifalc30kcsc3ddgv31")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.6") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "0mxvvp78vdm9rlqy761xyvyifflkj78x6h335ksabypiyyy4yn4s")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.7") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "09qr4j6s6z5rjp7j8rjc1amq2k1rvcx7gr35bp0b1g3bz9f9dni5")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.8") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "06q099bni2msdm26r3li5y7r6slv0fqq29gldxbd3laikkxm6fip")))

(define-public crate-io-close-0.1 (crate (name "io-close") (vers "0.1.9") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "1wbn3r7jc0q31w5lv8vvh59vcy6lsy4lisk4h35khh4pzic0k63v")))

(define-public crate-io-close-0.2 (crate (name "io-close") (vers "0.2.0") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "12c4lwgz50jp47aqxv5hisa1ijirkym2sgjfmin8y3gxznij3v8j")))

(define-public crate-io-close-0.2 (crate (name "io-close") (vers "0.2.1") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "0qk08s19xfh0w4p9rdyb85iljdqhply5gvx4spaix41l0nxjx004")))

(define-public crate-io-close-0.2 (crate (name "io-close") (vers "0.2.2") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "1kl7qkhmpc96mnxzhjsjx99nl71f53fxpmw6q631wylc2gncl05f")))

(define-public crate-io-close-0.2 (crate (name "io-close") (vers "0.2.3") (deps (list (crate-dep (name "kernel32-sys") (req ">=0.2.2, <0.3.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req ">=0.2.80, <0.3.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req ">=3.1.0, <4.0.0") (default-features #t) (kind 2)))) (hash "1rbh8fq3jxfnc19gv7jirxn5ca9lhxzsw9df6aw0gdqw19xfafmx")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "05f709v605vhv42ac6zn4b907jisyms0ncdf1nypnjpklk3rd8nw")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("handleapi" "std" "winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0f50v4bnsxxmvc57l6lbks9ai5085dclnhlyl9vvdscdyxi5l26m")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("handleapi" "std" "winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0mv6qay1qbbhx0lrc9mml5h1rrjq4gsngp3am51p855rvrs3m180")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("handleapi" "std" "winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1sbnb1ywzxiz2pkbpdksl0qrdk9gpjz940fl5kkiwbq9n2x7w8cb")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.4") (deps (list (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "os_pipe") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("handleapi" "std" "winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1qpwcgp36bmmnfhsy920zzpgdqyrfkpq9rgfl8f8qxlv854i3ckw")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.5") (deps (list (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "os_pipe") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("handleapi" "std" "winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "10wjqn73jggx43rzvl8lkbih86s81cd3rlsj9k4famds8jrj0dik")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.6") (deps (list (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "os_pipe") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("handleapi" "std" "winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1dgvkgq46cdhlk34w2j84zzjmqlpx6bglm3vwj7q6z28198j640a")))

(define-public crate-io-close-0.3 (crate (name "io-close") (vers "0.3.7") (deps (list (crate-dep (name "libc") (req "^0.2.80") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "os_pipe") (req "^0.9.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("handleapi" "std" "winsock2"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1g4hldfn436rkrx3jlm4az1y5gdmkcixdlhkwy64yx06gx2czbcw")))

(define-public crate-io-context-0.1 (crate (name "io-context") (vers "0.1.0") (hash "0g1qyhpv2cfl4nmir6ggnskigjqa1rp5v77phvq2d3qpi1raphi4")))

(define-public crate-io-context-0.2 (crate (name "io-context") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1378v98xxakiq4y90zwrzsbi83al0pa75zlwhacbbd33d04givvd") (features (quote (("default") ("context-future" "futures"))))))

