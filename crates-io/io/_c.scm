(define-module (crates-io io _c) #:use-module (crates-io))

(define-public crate-io_check-0.1 (crate (name "io_check") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.66") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0npdbvqjl1y0ic1b3ydg3y71a4bs4ml47vb2qlj89cd5vk9hs2rd") (yanked #t)))

(define-public crate-io_check-0.1 (crate (name "io_check") (vers "0.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.66") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1m2cvp02qsnwvbmv8hki9zipslrfmkbrn82dp7skchsspf18lb2v") (features (quote (("default" "backtrace"))))))

(define-public crate-io_check-0.1 (crate (name "io_check") (vers "0.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3.66") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1a3ich73limsr1iqc01zcdv8zi4z4wqzcdv01v7l1i55rdb0r1mj") (features (quote (("default" "backtrace"))))))

