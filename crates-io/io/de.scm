(define-module (crates-io io de) #:use-module (crates-io))

(define-public crate-iodef-0.1 (crate (name "iodef") (vers "0.1.0") (hash "0hacq405nw9nalcz24icrskrg4qk8wl9pbs4jh2gvppkg46mcrpb")))

(define-public crate-iodeser-0.1 (crate (name "iodeser") (vers "0.1.3") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1inlmdr4x5n9c352n15xwf8mqzwclqhp7pna0ivxnnmwx0h01ygf")))

(define-public crate-iodeser-0.2 (crate (name "iodeser") (vers "0.2.0") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0076kxv1p86p64q4mcaw46xazhs46q2c6wwks8g78r7nb5wnzrry")))

(define-public crate-iodeser-0.2 (crate (name "iodeser") (vers "0.2.1") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "07q7i7ky4bag1g4dfbfa3ckljimg5wm2ywgh31495vh2snar78ib")))

(define-public crate-iodeser-0.2 (crate (name "iodeser") (vers "0.2.2") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0y2l2qn3dm7qq85g9whqnxpdw5qhl6i7f33hv2x8rh4r7cysvrii")))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.0") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "10wydb2m3nfihsjhr4hrwakqq3khbpi8d3lir6kcgajimbylspp1") (yanked #t)))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.1") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0i8mbxhawq672zrvmki7hk1schy1wyi05kd1kvzanmxk3zrm5qgx")))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.2") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1jv5wxq2ffyb9r8z4jws5wc19jf9dax9v81qh6pajkgdvy7sxyf4")))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.3") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1brssygh18q00xc2qb8cbmr4kyshv5kn9p7mgkj09sf80c680zq0")))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.4") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0hnv8ianj9v2bixg2r9jgkriaifjvcfnpl8xig10ky8cyvd5n30y")))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.5") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "02s28dxxk28866cp1w2vxq2mav0sdyxj06acfxdgc63nr7ccrbh1")))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.6") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "10y8v8gh8r0bwq0bcnlxdcfpj2jj57gz9prqni16kas5vm2z3lh9")))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.7") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "15sl49lm9k8jxvpm2v61sg601ki2arc20ap82f62yxnaz07a8ck4") (yanked #t)))

(define-public crate-iodeser-0.3 (crate (name "iodeser") (vers "0.3.8") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1vavaskw5i0nj6knfjnf8bagrdl9dzyxf5k10qcf2nj3g18v34xj")))

(define-public crate-iodeser-0.4 (crate (name "iodeser") (vers "0.4.0") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1aahgsd5xzzrm0zj3a6664cpv5fgvfw7bfrmlrak8y5nicpkfdry")))

(define-public crate-iodeser-0.4 (crate (name "iodeser") (vers "0.4.1") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1zh5fxa1i0y5j5cakdj23zvnrszkwb7hcd0p2ha2hyahxjaysmq0")))

(define-public crate-iodeser-0.5 (crate (name "iodeser") (vers "0.5.0") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0p3pl54avkfbn4i5mfvrfd2g2ngckrhkjmq3849mldcvmshkdxg4")))

