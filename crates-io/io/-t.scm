(define-module (crates-io io -t) #:use-module (crates-io))

(define-public crate-io-test-0.0.0 (crate (name "io-test") (vers "0.0.0") (hash "0rwnm1in7h0biwx6p5qcry3qignf8bwvmjwk32cb9x21spiqm3y0")))

(define-public crate-io-test-0.1 (crate (name "io-test") (vers "0.1.0") (deps (list (crate-dep (name "io-trait") (req "^0") (default-features #t) (kind 0)))) (hash "0w4ghq5fpmwjj6g3r6x3iqjl1vr1fvp4y26rwwv5vvacfc6bx5xm")))

(define-public crate-io-test-0.1 (crate (name "io-test") (vers "0.1.1") (deps (list (crate-dep (name "io-trait") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w9mfa1vzcmfr2m69n5i3ahbqaw2kwps1d2mw888yh4z3ymi54m3")))

(define-public crate-io-test-0.1 (crate (name "io-test") (vers "0.1.2") (deps (list (crate-dep (name "io-trait") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1n96dlgfvcllxcw5f8bivjsa1w0ldhhb3hf5qxz5zgdy7jv36fwk")))

(define-public crate-io-test-0.1 (crate (name "io-test") (vers "0.1.3") (deps (list (crate-dep (name "io-trait") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "00ppavhkibpl1p3vwy7gw865pwzdvr78vxklj8x054qavhai4kf0")))

(define-public crate-io-test-0.1 (crate (name "io-test") (vers "0.1.4") (deps (list (crate-dep (name "io-trait") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "1smhnszb77hyz5wbhjxhpsafhyac68q8a821brbl86sdi5ci3gzb")))

(define-public crate-io-test-0.1 (crate (name "io-test") (vers "0.1.5") (deps (list (crate-dep (name "io-trait") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "1kg5yh7qr881zb1dcpa1vmgc8y48f55fn9x4paq61nk84788r0fh")))

(define-public crate-io-test-0.2 (crate (name "io-test") (vers "0.2.0") (deps (list (crate-dep (name "io-trait") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "1x9m1ck4p42pybhbghm1497hsx2agwri9dv7ahnagnwz4qhrb768")))

(define-public crate-io-test-0.3 (crate (name "io-test") (vers "0.3.0") (deps (list (crate-dep (name "io-trait") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "07wyc1709zvjbnqyyg86ncm7js4074kf2imk91sf0a6k45rk14b2")))

(define-public crate-io-test-0.4 (crate (name "io-test") (vers "0.4.0") (deps (list (crate-dep (name "io-trait") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "1pmf4wfvm6rx7d251nnxywybgd489kahbfg7jvbgjlxqfhm3vbnx")))

(define-public crate-io-test-0.5 (crate (name "io-test") (vers "0.5.0") (deps (list (crate-dep (name "io-trait") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (kind 2)))) (hash "1pxrbda26zrmsxawqybcmfq3ag2gxrhl1z40iim7n3gdggxwc45h")))

(define-public crate-io-test-0.6 (crate (name "io-test") (vers "0.6.0") (deps (list (crate-dep (name "io-trait") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.38") (default-features #t) (kind 2)))) (hash "13a3gd8zsgwmg6cam8cvpzxn4imybx0sq9g8clp22d5fc2dqhcig")))

(define-public crate-io-test-0.7 (crate (name "io-test") (vers "0.7.0") (deps (list (crate-dep (name "io-trait") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.38") (default-features #t) (kind 2)))) (hash "1zmnh4zvnhrzaip3yf2dpggznm9hnsfdlspx4zsfc1cjrzggr817")))

(define-public crate-io-test-0.8 (crate (name "io-test") (vers "0.8.0") (deps (list (crate-dep (name "io-trait") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.38") (default-features #t) (kind 2)))) (hash "1mjv2yvivyiba4ylmx1v8117yz18j81gjld2kbnfjhv3lwql0rl1")))

(define-public crate-io-test-0.8 (crate (name "io-test") (vers "0.8.1") (deps (list (crate-dep (name "io-trait") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.38") (default-features #t) (kind 2)))) (hash "0sc4kyfv5nmh42g64psg983aw14xj38p55vrpycp5zn4vhc0fc5q")))

(define-public crate-io-test-0.9 (crate (name "io-test") (vers "0.9.0") (deps (list (crate-dep (name "io-trait") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)))) (hash "1l615c3bzpy7vcva49chmh12di7qbdi3qdw9zv7jn23m2bmlfrd4")))

(define-public crate-io-test-0.10 (crate (name "io-test") (vers "0.10.0") (deps (list (crate-dep (name "io-trait") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)))) (hash "14vig2q19rd2abplndw3zx76bg4hm4lazc90a6ldnap1z3brny4b")))

(define-public crate-io-test-0.10 (crate (name "io-test") (vers "0.10.1") (deps (list (crate-dep (name "io-trait") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)))) (hash "18qh34mxpvps6dj2ly0sj85mal5hfycqsqqsz54yn189vlb76vxs")))

(define-public crate-io-test-0.11 (crate (name "io-test") (vers "0.11.0") (deps (list (crate-dep (name "io-trait") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)))) (hash "1nwdrvgy4y98x7vf74jzbak1xwbl3q414a5z4dnkfs57c24jqk4y")))

(define-public crate-io-test-util-0.1 (crate (name "io-test-util") (vers "0.1.0") (hash "1nazp3lrrhbi2lyg57r6n2qc4sizwwilvz5xpndi2q2gzb90g2bm")))

(define-public crate-io-tools-0.1 (crate (name "io-tools") (vers "0.1.0") (hash "1rwzbwzpfhfaxygcam9pvrw0m1h9cmkjwdwv5y18anm0z4sc5hdi")))

(define-public crate-io-tools-0.1 (crate (name "io-tools") (vers "0.1.1") (hash "1ydl5hq37kszjk97z04xzxi99168zdc32lnb49jkfj7l6v6qr4pb")))

(define-public crate-io-trait-0.0.0 (crate (name "io-trait") (vers "0.0.0") (hash "1ayc1bmfys2w2qazsd4nq2j62pzwaigcj2j3qiph4dqyqp0xxbqq") (yanked #t)))

(define-public crate-io-trait-0.1 (crate (name "io-trait") (vers "0.1.0") (hash "18li24s0yqssyn602d1jvwvg72gi9z510yk4l68gmazmgfzk6qnz")))

(define-public crate-io-trait-0.1 (crate (name "io-trait") (vers "0.1.1") (hash "1h7diqs3cx0ksa1s563zrg83hs6ykwrq62jr1jrqzn54k5mff5gh")))

(define-public crate-io-trait-0.1 (crate (name "io-trait") (vers "0.1.2") (hash "07niv9mp506ngb0i78p0hc5z7h426jl3a49yl6m2hr3ppa78b99r")))

(define-public crate-io-trait-0.1 (crate (name "io-trait") (vers "0.1.3") (hash "1i5afljjvwgh79ivj8vw185s59i7vlbkvdcimyh69ks0sj0wdh9a")))

(define-public crate-io-trait-0.1 (crate (name "io-trait") (vers "0.1.4") (hash "0r8bdhrps15sg7v1qkx4wip51bp7pvkynlsxybggcd9fkzwxkjwp")))

(define-public crate-io-trait-0.3 (crate (name "io-trait") (vers "0.3.0") (hash "11zypqn59aawlv9srw52p016fgibn97m5wkjvkrpi458v4f5bdgd")))

(define-public crate-io-trait-0.4 (crate (name "io-trait") (vers "0.4.0") (hash "1dbacsm1jymicrb3d57i5ivja58xppmkpw0b6igpz0m68saa1lqv")))

(define-public crate-io-trait-0.5 (crate (name "io-trait") (vers "0.5.0") (hash "0i8pbias0rpj5ka3njl0awdd2mzdzab4f17wi6rbrf0pwxfy16s9")))

(define-public crate-io-trait-0.6 (crate (name "io-trait") (vers "0.6.0") (hash "126ya3fbarws4hs3wnhrqh6jpf7bz99s5h6zh810zk6agfw6ccyl")))

(define-public crate-io-trait-0.7 (crate (name "io-trait") (vers "0.7.0") (hash "11j3idadijjfxxl2875n20bs0s22acbf05w2j7m5zdpwrkavgi20")))

(define-public crate-io-trait-0.8 (crate (name "io-trait") (vers "0.8.0") (hash "0yq496drq86i1h4yijajhp6czlfbvwjsmamvc62mc9hk86r66l2q")))

(define-public crate-io-trait-0.9 (crate (name "io-trait") (vers "0.9.0") (hash "0197r8x0x0k93c7k98dfdhi7d4k6k2cy02bl632s1pxkb4zmz4mc")))

(define-public crate-io-trait-0.10 (crate (name "io-trait") (vers "0.10.0") (hash "08q717lgkdvzf265s7vy4n3sa75xd732x9p3mfkfbjna7zvrqzv3")))

(define-public crate-io-trait-0.11 (crate (name "io-trait") (vers "0.11.0") (hash "0hivwshn6bq8rwbmaamfr401l1wqrn3lp8irv6pvlblj2x99xhzy")))

(define-public crate-io-truncate-0.1 (crate (name "io-truncate") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "0ilqy11dhw2mdyhy7jzh847r4jwy6jgj8lai71ra9xcwp8b7ix80")))

(define-public crate-io-tubes-0.0.1 (crate (name "io-tubes") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.57") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v90vbwgqk67bndh9vcknqix8dnnhvi3cgp3ijc22m5lgim272kx")))

(define-public crate-io-tubes-0.0.2 (crate (name "io-tubes") (vers "0.0.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.57") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12gz8vnv2787m3l267z65k4l7lqkrdc7frgcgq4hwdfx950mc46g")))

(define-public crate-io-tubes-0.0.3 (crate (name "io-tubes") (vers "0.0.3") (deps (list (crate-dep (name "async-trait") (req "^0.1.57") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kksg0vcxzd9sl3yck3pd1c5p6h8d6hsqnzzn3bmaqjlsdkqd0fh")))

(define-public crate-io-tubes-0.1 (crate (name "io-tubes") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pgv0h3zii8iy2csbrqz9zccdagvn3d2kpp11496lp69gx4446kx")))

(define-public crate-io-tubes-0.1 (crate (name "io-tubes") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pretty-hex") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sj3vp1gn8bpvr51pywsj6wyrz1g8jqv5ybbbj1f85zqkh6fk3i9")))

