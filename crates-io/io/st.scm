(define-module (crates-io io st) #:use-module (crates-io))

(define-public crate-iostream-0.1 (crate (name "iostream") (vers "0.1.0") (hash "1h57jqks3iig34z1bwy2v3ds0a2akilh1fps0x8nqn0qm1xrr088") (yanked #t)))

(define-public crate-iostream-0.1 (crate (name "iostream") (vers "0.1.1") (hash "0s09xii32c8vs9l31vc6gzmilam923ij4hdzw8cqh3j7mknlhilx") (yanked #t)))

(define-public crate-iostream-0.1 (crate (name "iostream") (vers "0.1.2") (hash "1l4al8i8dkfjscz70gz7kcfr9xamls2cfj6qzc352vrc2hqjc1a7") (yanked #t)))

(define-public crate-iostream-0.1 (crate (name "iostream") (vers "0.1.3") (hash "1njyhfj0kajgrc11fzdcdfvm0mxgxmn15fyfv5l1kb19wih5s4hp")))

