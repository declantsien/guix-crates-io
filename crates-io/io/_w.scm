(define-module (crates-io io _w) #:use-module (crates-io))

(define-public crate-io_wrapper_statistics-0.1 (crate (name "io_wrapper_statistics") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "0warf2g3v7rpdiwc2nv83yikrynvsb04kj0a0a095jzy18d8ahjl")))

(define-public crate-io_wrapper_statistics-0.1 (crate (name "io_wrapper_statistics") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 0)))) (hash "019rxcilik26fsdyd9ycgbx5zmr3cj4nkirnqb98gi1krfq7rcxn")))

