(define-module (crates-io io _s) #:use-module (crates-io))

(define-public crate-io_self-0.1 (crate (name "io_self") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "01xldnfcsji5qqwnlf7ygs8whpgjhwcqkb6md2h0rd4d77gdpsmv") (features (quote (("default"))))))

(define-public crate-io_self-0.1 (crate (name "io_self") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0k0syh5zqxp7l6mdify0al5x9iwykh59s8crj1hd3wciqxfp8k50") (features (quote (("default"))))))

(define-public crate-io_self_derive-0.1 (crate (name "io_self_derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fcxv4x5yrf7plkw6jsc12v74r9dpcyhmk0jvd5ll4p7hvhvip9j")))

(define-public crate-io_self_derive-0.2 (crate (name "io_self_derive") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "18xsvcyb816lyc7b9rp57blhg4zrzbqc8alyw6490s6nm0pz6n0g")))

