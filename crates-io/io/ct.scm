(define-module (crates-io io ct) #:use-module (crates-io))

(define-public crate-ioctl-0.1 (crate (name "ioctl") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0ssjqnqv0m978fzbr078fsld6xqp8v5vrplw33p359q2vj0mv2vx") (yanked #t)))

(define-public crate-ioctl-0.1 (crate (name "ioctl") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1ih41y5fkh21bx27cj0z8ig0dmwc10g84km8wwww389mmjhkx3h3") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ioctl-0.1 (crate (name "ioctl") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0729gkh7fl4j57s16zk16n6nnbavjnppzw1nnffhvpk95y1s8zh7") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ioctl-0.2 (crate (name "ioctl") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1ignc3a59i6mygpqiqjvmw7myskrxyqg9r263mci67pmqn7i95fl") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ioctl-0.2 (crate (name "ioctl") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "04zzi5i8gsfaqgq5ahrw0wilb5f8mdgxrbyvxdynnqi0vm67panv") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ioctl-0.3 (crate (name "ioctl") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0im0xhz8iddl9wlvd7l8a6jzisynprnr116wi5jf0bkys9djf6i7") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ioctl-0.3 (crate (name "ioctl") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0vwirw87c9ghrxqsa4b32rhl9q908lbqls3lbwqqfwcg0n2ysv5r") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ioctl-0.3 (crate (name "ioctl") (vers "0.3.3") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0b00759bnn32vfxmwnkjx4gcm8nwmzm4ljpf6nhhbkwvba26sg4i") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ioctl-0.3 (crate (name "ioctl") (vers "0.3.4") (hash "11b3swr6wih345ag3y24np46kkgiwvrjhyfyyxmd3j11d6gy91i1") (yanked #t)))

(define-public crate-ioctl-gen-0.1 (crate (name "ioctl-gen") (vers "0.1.0") (hash "1kihj6pbhmamf2kyy6ndfl03xiydblbnp0dsf3dwikzj6dl22kjz")))

(define-public crate-ioctl-gen-0.1 (crate (name "ioctl-gen") (vers "0.1.1") (hash "1rvbj1rwn60ajrw8i76b75902nc9jmhfw7s9xnznc7y0vsfrfi8p")))

(define-public crate-ioctl-macro-0.0.1 (crate (name "ioctl-macro") (vers "0.0.1") (deps (list (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "0j4jfaqf6dy35hdfhgk2kbwf2aq9cfj4d0niaykpjiz01gg17656") (features (quote (("extern_fn") ("default"))))))

(define-public crate-ioctl-macro-0.0.2 (crate (name "ioctl-macro") (vers "0.0.2") (hash "08bxaaxgnhrvi1qi6vizlqz5q0fn9lr7h28kw00yw8d097j1rv6d") (features (quote (("extern_fn") ("default")))) (rust-version "1.64.0")))

(define-public crate-ioctl-macros-0.1 (crate (name "ioctl-macros") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.28") (default-features #t) (kind 1)))) (hash "07irsa5ms93as164g2pzwcs63vlfvypy87qmkdcvizhpyjda9s6j")))

(define-public crate-ioctl-rs-0.1 (crate (name "ioctl-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1zf6r1aa2vcqx142rn2l8y2zrqvv9qp09z2g9gi97jf9aqlnh7v9")))

(define-public crate-ioctl-rs-0.1 (crate (name "ioctl-rs") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "04skjwmsldf8cb8fkfaj13wl3jp29hd3a465zkg16plhcqcjn7va")))

(define-public crate-ioctl-rs-0.1 (crate (name "ioctl-rs") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1sycny0iflwgpc0z4rvh2w4qrcpa1sf4qmh7f9sw32lh9wvksga3")))

(define-public crate-ioctl-rs-0.1 (crate (name "ioctl-rs") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1cwxfg89cvjngikfgbzxy9is3phrkjr94ivm7ankym3kw8254ga7")))

(define-public crate-ioctl-rs-0.1 (crate (name "ioctl-rs") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hgxiwy9c3cv3grca6rckcc6klwgjj2njqcjb6px18f5cjhr5p4j")))

(define-public crate-ioctl-rs-0.1 (crate (name "ioctl-rs") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rrjmwd4fccid00kdnikv53jrvir9j6w96mqzrdkdi40wrmvscy7")))

(define-public crate-ioctl-rs-0.1 (crate (name "ioctl-rs") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zdrgqxblrwm4ym8pwrr7a4dwjzxrvr1k0qjx6rk1vjwi480b5zp")))

(define-public crate-ioctl-rs-0.2 (crate (name "ioctl-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0laishgm8pvhjkm1q6by804kqxnkql9xgk2m8rsfdzwa7ig0syv0")))

(define-public crate-ioctl-sys-0.4 (crate (name "ioctl-sys") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03zh94bc2q6gsa488jwnva1rkvag1jq5bfwikcjn88m0h2h57l01") (features (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.4 (crate (name "ioctl-sys") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fqcfa82csviz06qdp2fvjac8z5q72fsscffzal6b1dibqby6sd0") (features (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.5 (crate (name "ioctl-sys") (vers "0.5.0") (hash "1jhzxjvgpc335i4zz3wlb03302aamcc1m01b1ryfxphvrl43abyv") (features (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.5 (crate (name "ioctl-sys") (vers "0.5.1") (hash "14jrrnhl0rrvpfjwixyd5643bjmb742f1v6kkwy247wxpr5xsppy") (features (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.5 (crate (name "ioctl-sys") (vers "0.5.2") (hash "0jjh6fggwd9akklwcgcqyz9ijd79k6yskkx7ijmfm5i46lk4nb2y") (features (quote (("unstable"))))))

(define-public crate-ioctl-sys-0.6 (crate (name "ioctl-sys") (vers "0.6.0") (hash "0bhqal9zbala895b5iwvwfi8k13jbxb2dz19jmk8iwjqlvzryhhw")))

(define-public crate-ioctl-sys-0.7 (crate (name "ioctl-sys") (vers "0.7.0") (hash "0q02rpkkpry71cp0p3acdj31jsba95d81cqysnx0sk3jigiy8zpb")))

(define-public crate-ioctl-sys-0.7 (crate (name "ioctl-sys") (vers "0.7.1") (hash "021vy96y6cq6n9rpj0f7axi455gx6vf0wn8himbqfm484dmhp7bz")))

(define-public crate-ioctl-sys-0.8 (crate (name "ioctl-sys") (vers "0.8.0") (hash "0g0kjq308kdm81wq3f1p6q1v2x5sd2v31iwqzzsjch2354x1zlcb")))

(define-public crate-ioctls-0.5 (crate (name "ioctls") (vers "0.5.0") (deps (list (crate-dep (name "ioctl-sys") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gvgl1ymqfj6hssvdxpg56dxbvvkip0mswng559ydp5fczjxhsy3") (features (quote (("unstable"))))))

(define-public crate-ioctls-0.5 (crate (name "ioctls") (vers "0.5.1") (deps (list (crate-dep (name "ioctl-sys") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00vn6giq76xgszkshh5vm0ky1jx7f4k1v093qi9avjnr2wzfri2a") (features (quote (("unstable"))))))

(define-public crate-ioctls-0.6 (crate (name "ioctls") (vers "0.6.0") (deps (list (crate-dep (name "ioctl-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yc9kmgh5s0vzq1d10ybjgngkrc0dgkc7ybg1g78bl3wxlb9d75c")))

(define-public crate-ioctls-0.6 (crate (name "ioctls") (vers "0.6.1") (deps (list (crate-dep (name "ioctl-sys") (req ">=0.6, <0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1liymfbgxy4airib1nfqlxavqjijsmbvciq6jhdylczpcak0qy65")))

(define-public crate-ioctls-0.6 (crate (name "ioctls") (vers "0.6.2") (deps (list (crate-dep (name "ioctl-sys") (req ">=0.6, <=0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s3ivrhcyfqc3bmr30gpfpad8xc8pn18gh7vrz24fvqw1w71nnwk")))

