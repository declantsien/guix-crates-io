(define-module (crates-io io -b) #:use-module (crates-io))

(define-public crate-io-block-0.1 (crate (name "io-block") (vers "0.1.1") (deps (list (crate-dep (name "ioctl-sys") (req "^0.4") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "14g4lb2nm4cm3ba3qj6sjw57vwcs66xld2wzgd5cjliffpwvsibz")))

(define-public crate-io-block-0.1 (crate (name "io-block") (vers "0.1.2") (deps (list (crate-dep (name "ioctl-sys") (req "^0.4") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "13qjv9cbp1zvyp3q0l0g492scvjr3wc2q86dhfkbbj0iahm9xx19")))

(define-public crate-io-block-0.1 (crate (name "io-block") (vers "0.1.3") (deps (list (crate-dep (name "ioctls") (req "^0.5") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1mn7y73pk912z4r6lc5kzanyf9g7a5akxx35kvg6jw89114rg5bq")))

(define-public crate-io-block-0.2 (crate (name "io-block") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(any(target_os = \"linux\", target_vendor = \"apple\"))") (kind 0)))) (hash "0cp026b2z9yvad68a012b5wy0lcnvdga9gqm4g50ldh5q4fibif6")))

(define-public crate-io-block-0.2 (crate (name "io-block") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(any(target_os = \"linux\", target_vendor = \"apple\"))") (kind 0)))) (hash "006idlda6brgpxzyy1jacf083fixzvwr5hwcs0c7f3fin20xz697")))

(define-public crate-io-block-0.2 (crate (name "io-block") (vers "0.2.2") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (target "cfg(any(target_os = \"linux\", target_vendor = \"apple\"))") (kind 0)))) (hash "0pr447xsna7skyjz87qiyyvs7hk1xp8i5wvd4yzc7vzwkwphhpd5")))

