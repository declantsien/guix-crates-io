(define-module (crates-io io ni) #:use-module (crates-io))

(define-public crate-ionic-0.0.0 (crate (name "ionic") (vers "0.0.0") (hash "0haq642r43yf3j29k2igx36s6q4cjn4bp9z62sslwyyd7bw7vhbl")))

(define-public crate-ionic_deckhandler-0.1 (crate (name "ionic_deckhandler") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1mwc0mds779b923qmbxdg7ym1n0ckrniyr069qrf29xx1hvyfqch")))

(define-public crate-ionize-0.3 (crate (name "ionize") (vers "0.3.2") (hash "0j2aaxl3vwqcgwwsp5nbcm53b34bhzyb490mz6p2rlixblmb06cm")))

(define-public crate-ionize-0.3 (crate (name "ionize") (vers "0.3.3") (hash "142m1y027glmvkznhc27554wprci2n8a3vgrmybl1ysnfb5ngmnd")))

(define-public crate-ionize-0.3 (crate (name "ionize") (vers "0.3.4") (hash "02pdk1zjfmpjvxkgpmscf8i7xjph3qj4acwxk1z237rac79s22zj")))

(define-public crate-ionize-0.3 (crate (name "ionize") (vers "0.3.5") (hash "1wnj3fl53nsmvf5vdyyndp9n7fdcf0n75i3v5yqzc1pfab7fm174")))

(define-public crate-ionize-0.3 (crate (name "ionize") (vers "0.3.6") (hash "1aryzilwjmz1ss94s1117svd7hc0l9pfzmv9r8l2vbrbh2jv7l4p")))

