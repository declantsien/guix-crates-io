(define-module (crates-io io la) #:use-module (crates-io))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.0") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0z6bmfn86s86lcyhjwfczr41xhbja7rbzak3aqzmdz1ylkda3zqc")))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.1") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0m6y835plxw1d5r50kn7hz7c85jra764cbmf6w3md1kawq2l38nf")))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.2") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0b31nnwix4b5sl5xfixrkc739s2ih5084krg0wd8478rbffy3f2x")))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.3") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0zwvk7a59cxzfvfz5sr0xafyi5lv2kml6mwpg293bmimym4mhwn2")))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.4") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fg9i7v38hnibk1wrgvrnxcdbqknvnj85k6ia6n74wj46r67adx6")))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.5") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h59c1s8xhy42fxb3fwbhhq7rizaxqyb37kk0c95rj7r913wpz67")))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.6") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bggfqz9xmv8vnyaqvml8bkjz2yjrksbswr4phhjv4c3wp6sgzrb")))

(define-public crate-iolaus-0.1 (crate (name "iolaus") (vers "0.1.7") (deps (list (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "05nz21prgbx8syni21njgmawbzi9dkj3pf97crv1w2i1kbgihp21")))

