(define-module (crates-io io ki) #:use-module (crates-io))

(define-public crate-IOKit-sys-0.1 (crate (name "IOKit-sys") (vers "0.1.0") (deps (list (crate-dep (name "CoreFoundation-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1ldrhd52k8zx33z7qkcggpfyds84imcp03jvivhf7749w57dxhcn")))

(define-public crate-IOKit-sys-0.1 (crate (name "IOKit-sys") (vers "0.1.1") (deps (list (crate-dep (name "CoreFoundation-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0jl5w238q3cb2474il9hrfjvp23d2mfvmszr3fc5rj2b4898nglw")))

(define-public crate-IOKit-sys-0.1 (crate (name "IOKit-sys") (vers "0.1.2") (deps (list (crate-dep (name "CoreFoundation-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0rz67cff01fsf0rp5imjib2vk4gzw1z3ks1d34fm9l0gifa32xjn")))

(define-public crate-IOKit-sys-0.1 (crate (name "IOKit-sys") (vers "0.1.3") (deps (list (crate-dep (name "CoreFoundation-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "08s7ka8vfzvs8lndfgvw1m4z9mjr6wgn5s2s05j9wkxbqdh7zra1")))

(define-public crate-IOKit-sys-0.1 (crate (name "IOKit-sys") (vers "0.1.4") (deps (list (crate-dep (name "CoreFoundation-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "14q1fkq4nzrydxqqhx2frkcddpbxmjrazaxqvnhfcwbkjkl00b0r")))

(define-public crate-IOKit-sys-0.1 (crate (name "IOKit-sys") (vers "0.1.5") (deps (list (crate-dep (name "CoreFoundation-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jmzyc55r312c5y9v2963bjhryr7ssrvsxl06v96kxmsihwnqscr")))

