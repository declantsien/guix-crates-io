(define-module (crates-io io s7) #:use-module (crates-io))

(define-public crate-ios7crypt-0.0.1 (crate (name "ios7crypt") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1fs901lis1smp9pi3ncj2czfwmcch8i1dahhcnw3d9n795w27mjz")))

(define-public crate-ios7crypt-0.0.2 (crate (name "ios7crypt") (vers "0.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0krgqfxj2k6j9akibri1f4iw685f2pm5a6hvk16k70zf0vdp4s6a")))

(define-public crate-ios7crypt-0.0.3 (crate (name "ios7crypt") (vers "0.0.3") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0ccps7m7cbb6cy7z0bgid3lzxxb60hqfv9f49jlmg5xdz7zg8pf0")))

(define-public crate-ios7crypt-0.0.4 (crate (name "ios7crypt") (vers "0.0.4") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "166fmfrz82kl6jpxx164vg8gbnvf4fmpi4rrp9hl6nrqlv6wl3p1")))

(define-public crate-ios7crypt-0.0.5 (crate (name "ios7crypt") (vers "0.0.5") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1gnlbagiwy6549pr340jdiqg9in7diq752qrg2263vngw4vs01ca")))

