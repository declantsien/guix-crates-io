(define-module (crates-io io wr) #:use-module (crates-io))

(define-public crate-iowrap-0.1 (crate (name "iowrap") (vers "0.1.0") (hash "1lcbbsqzm7d4rdjb0maryj6c9mik4vw0817x3drfdl25285sdw6s")))

(define-public crate-iowrap-0.1 (crate (name "iowrap") (vers "0.1.1") (hash "1s5h0j0pkak483s839lf86zhmmy9cd2yclrvm6scf7bc1ddzf2nk")))

(define-public crate-iowrap-0.1 (crate (name "iowrap") (vers "0.1.2") (hash "0djjvja62bzcypawi8x2c0gachlsikjps0vdcd015wgsvy11cqd6")))

(define-public crate-iowrap-0.2 (crate (name "iowrap") (vers "0.2.0") (hash "0qnf5b86frmf4vyqs4s594r6ni833km8z7b7g34x7a332l46xq5a")))

(define-public crate-iowrap-0.2 (crate (name "iowrap") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "0nni5b9mgjvkwhdkn3d356wd41grbynf65r0ys8qs4zslkcqnxwd")))

