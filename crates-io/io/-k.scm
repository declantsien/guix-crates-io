(define-module (crates-io io -k) #:use-module (crates-io))

(define-public crate-io-kit-sys-0.1 (crate (name "io-kit-sys") (vers "0.1.0") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.2") (default-features #t) (kind 0)))) (hash "186h7gm6kf1d00cb3w5mpyf9arcdkxw7jzhl1c4wvm2xk5scq7gj")))

(define-public crate-io-kit-sys-0.2 (crate (name "io-kit-sys") (vers "0.2.0") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "mach") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "068r17sns736ayykdxw4w9v9wxfyaa8xc2ai9wb9cvv8r7rzg2bp")))

(define-public crate-io-kit-sys-0.3 (crate (name "io-kit-sys") (vers "0.3.0") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "mach2") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1pj8cc7wmpnm7w1bfni45hmv1gcp95dk5q7cpl7zzpn1mhll8bcv")))

(define-public crate-io-kit-sys-0.4 (crate (name "io-kit-sys") (vers "0.4.0") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "mach2") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0h2674vwf4z1hpipsmja04x74iwcyya3w3bkqq7p3wfwwlqcnsa7")))

(define-public crate-io-kit-sys-0.4 (crate (name "io-kit-sys") (vers "0.4.1") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "mach2") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0ysy5k3wf54yangy25hkj10xx332cj2hb937xasg6riziv7yczk1")))

