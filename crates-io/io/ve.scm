(define-module (crates-io io ve) #:use-module (crates-io))

(define-public crate-iovec-0.1 (crate (name "iovec") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gi6md320ys3x0k6b3zvw6bwbvznr4s0zrvjw5dz5kgwc7p65l19")))

(define-public crate-iovec-0.1 (crate (name "iovec") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1xxjhs9v477wz7bs4mbgzp4hcmmy69libwai25m6rkvz4k1bks5n")))

(define-public crate-iovec-0.1 (crate (name "iovec") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "025vi072m22299z3fg73qid188z2iip7k41ba6v5v5yhwwby9rnv")))

(define-public crate-iovec-0.2 (crate (name "iovec") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.3") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "ws2def"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "198hnlc5dzm9vs1y5dmhq1rhfrxli406569vmfp0kyrblbxscy9l") (yanked #t)))

(define-public crate-iovec-0.1 (crate (name "iovec") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0rv5dc9zlzaibg9ch9q962xp0mlmrlbry6dvrl9yvzvkm806jqy9")))

(define-public crate-iovec-0.1 (crate (name "iovec") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0ph73qygwx8i0mblrf110cj59l00gkmsgrpzz1rm85syz5pymcxj")))

