(define-module (crates-io io kc) #:use-module (crates-io))

(define-public crate-iokcloud-crypto-0.1 (crate (name "iokcloud-crypto") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.7") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1qjny9xzz3816iq95654b3lgfxk01z8q6dhr7qp7z05j84q7f5a5")))

