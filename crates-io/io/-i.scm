(define-module (crates-io io #{-i}#) #:use-module (crates-io))

(define-public crate-io-impl-0.1 (crate (name "io-impl") (vers "0.1.0") (deps (list (crate-dep (name "io-trait") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0w5s5skjwxn2rq17z0pvd6vanw57kwwb888ixdar6n89547mm0da")))

(define-public crate-io-impl-0.1 (crate (name "io-impl") (vers "0.1.1") (deps (list (crate-dep (name "io-trait") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mz9853a230cd088qxbl456ijvzspn1z7yz6drsyyzpxxxrnqvx2")))

(define-public crate-io-impl-0.1 (crate (name "io-impl") (vers "0.1.2") (deps (list (crate-dep (name "io-trait") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cv16qwcydrzdyw9pz0775pn7p75ijvbqm16z2hv4s35iy3vfs5i")))

(define-public crate-io-impl-0.1 (crate (name "io-impl") (vers "0.1.3") (deps (list (crate-dep (name "io-trait") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ami627cnl4daz3k6yjgymzjxfzv92q3bip7aqy5nr2xr3dic2i2")))

(define-public crate-io-impl-0.1 (crate (name "io-impl") (vers "0.1.4") (deps (list (crate-dep (name "io-trait") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0186jlfhyyj203fxhiqcbhl53xih0778f4avx958vlc2dmn7wmlz")))

(define-public crate-io-impl-0.1 (crate (name "io-impl") (vers "0.1.5") (deps (list (crate-dep (name "io-trait") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1vpcnkrgfp6inv5l320j28p6gvra8ibfpaincy66kvaazg46pp42")))

(define-public crate-io-impl-0.3 (crate (name "io-impl") (vers "0.3.0") (deps (list (crate-dep (name "io-trait") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1f1xgbp431n8yaiqrar56jw5cxkfhp0l95j83dna3gack4ggddi8")))

(define-public crate-io-impl-0.3 (crate (name "io-impl") (vers "0.3.1") (deps (list (crate-dep (name "io-trait") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1q2ilamj6q79i9aqhqhx0mz6khlixwgcnzcf9xzn6zpkcpma7nlx")))

(define-public crate-io-impl-0.3 (crate (name "io-impl") (vers "0.3.2") (deps (list (crate-dep (name "io-trait") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1025262jxd5hxr1d86fs99nzpqs92ahrk2gj3isck01n58zi19hm")))

(define-public crate-io-impl-0.4 (crate (name "io-impl") (vers "0.4.0") (deps (list (crate-dep (name "io-trait") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1dy6kn8sjbrg1mdyfwr2kvc27a9rlay80kgwkdyx0pdaci9mv4n6")))

(define-public crate-io-impl-0.5 (crate (name "io-impl") (vers "0.5.0") (deps (list (crate-dep (name "io-trait") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1a3ky6nbk9nswf7ah0q58m3dmf15c3czsns6jm95pa51smkivcl6")))

(define-public crate-io-impl-0.6 (crate (name "io-impl") (vers "0.6.0") (deps (list (crate-dep (name "io-trait") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "0ayjhdyp9xvcdzap9ipss44r6pmnwlbxci0z2d3n8jcy2xm37mhf")))

(define-public crate-io-impl-0.7 (crate (name "io-impl") (vers "0.7.0") (deps (list (crate-dep (name "io-trait") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "1d8whwd155f3phflys921nh884zc7qblp65981lxc8zqv5pd2si5")))

(define-public crate-io-impl-0.8 (crate (name "io-impl") (vers "0.8.0") (deps (list (crate-dep (name "io-trait") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0vrd3qsxmh0zhkrxrdv375znvx391f9kbyvw4if2nz4rw03kpbwa")))

(define-public crate-io-impl-0.8 (crate (name "io-impl") (vers "0.8.1") (deps (list (crate-dep (name "io-trait") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)))) (hash "1d6fpqr9cy9j4725syp2al9f9mz1v4f1rjffmxkh6lxfqhhm2k6x")))

(define-public crate-io-impl-0.9 (crate (name "io-impl") (vers "0.9.0") (deps (list (crate-dep (name "io-trait") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1362nqr91l5nkyjm8x8cdrg1dlywdn857056jx0zf71gv069lhyf")))

(define-public crate-io-impl-0.10 (crate (name "io-impl") (vers "0.10.0") (deps (list (crate-dep (name "io-trait") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1wcqxnbr8zamd8dzk07wpr3f3mr4l3a59l119azizys28fszn7vp")))

(define-public crate-io-impl-0.11 (crate (name "io-impl") (vers "0.11.0") (deps (list (crate-dep (name "io-trait") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "12p27amfbilfsp1ljf26pld7ddgc5pl9nrwc5qlbknvkr09b4zd4")))

