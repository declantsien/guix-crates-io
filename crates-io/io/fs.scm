(define-module (crates-io io fs) #:use-module (crates-io))

(define-public crate-iofs-0.0.0 (crate (name "iofs") (vers "0.0.0") (hash "01vx2bzw6bn6nrirfwk51gmsqpx9xnr5rj0pprqrcm18zkd8vixf") (features (quote (("hook_key"))))))

(define-public crate-iofs-0.0.1 (crate (name "iofs") (vers "0.0.1") (hash "0pfm8r0qi4yc75ln9slpzbwaxkvq1d4c7h7f09yksxywhpfsgyxk") (features (quote (("hook_key"))))))

(define-public crate-iofs-0.0.2 (crate (name "iofs") (vers "0.0.2") (hash "110db0anns10x9j5zm8bvw2xwz12ma3hcr057p21ykyspkirlczh")))

(define-public crate-iofs-0.0.3 (crate (name "iofs") (vers "0.0.3") (hash "0a5csknw0z9pi3f83wnc4pdlb7cxkw2lg6k51gzaal1nfblqjyzh")))

(define-public crate-iofs-0.0.5 (crate (name "iofs") (vers "0.0.5") (hash "18sd99y9s6h7fw5q8kscrzagl2j7w3swr0w3wjlam748lasrnkq2")))

(define-public crate-iofs-0.0.6 (crate (name "iofs") (vers "0.0.6") (deps (list (crate-dep (name "http") (req "^0.2.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)))) (hash "1s8f0w7j7v1kzyffby7wsm5y7gbhk0a2ig56pqa4wnhc7l1s0dss") (features (quote (("web_warp" "warp" "http"))))))

(define-public crate-iofs-0.0.7 (crate (name "iofs") (vers "0.0.7") (deps (list (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0mlfm9wyybj2v1ncrnzfxbv8w0p70kbwz441nppvalbzhx37407y")))

(define-public crate-iofs-0.0.8 (crate (name "iofs") (vers "0.0.8") (deps (list (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)))) (hash "063drqln6p0i1irq54mwwy395sny6822djfln279j0lvg4xxgnsg") (features (quote (("web_warp" "warp" "url"))))))

