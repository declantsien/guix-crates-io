(define-module (crates-io io ut) #:use-module (crates-io))

(define-public crate-ioutil-0.0.1 (crate (name "ioutil") (vers "0.0.1") (hash "0knksk8q80qi5sbj8amvacxdycmki392dzm02i5w1w5f44h9pks1")))

(define-public crate-ioutils-0.1 (crate (name "ioutils") (vers "0.1.0") (hash "1z89i0mc53q8shhah8nizd1nfv0nzdnj0r68r7brsrdb0d0abkcv") (features (quote (("default"))))))

