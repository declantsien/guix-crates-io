(define-module (crates-io io _d) #:use-module (crates-io))

(define-public crate-io_de_ser-0.0.3 (crate (name "io_de_ser") (vers "0.0.3") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "deser") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1mwjwrwh7jkbxr1y0kpy510r6lz723z3wq1zqpfzf478amr0k06v") (yanked #t)))

(define-public crate-io_de_ser-0.1 (crate (name "io_de_ser") (vers "0.1.1") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "02gj56j0c7gr5bwydfwnc506g8wqn4q6v8nnnh55zzrry1ah5d9i") (yanked #t)))

(define-public crate-io_de_ser-0.1 (crate (name "io_de_ser") (vers "0.1.2") (deps (list (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "io_deser") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1vbvw0l5hsf2dnx30gnrqvmykpgzhfh904hrp9854gxdlcygrd9c") (yanked #t)))

(define-public crate-io_demo-0.1 (crate (name "io_demo") (vers "0.1.0") (hash "12dmqa11qpn2cxg29w67cln7c0fxjv9sshb6plpih53h6hw3pjj7")))

(define-public crate-io_deser-0.0.2 (crate (name "io_deser") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cnh6kmm1dry15yr80nddbb5bsd91nym7pwp2ky71jngvzd68r9k")))

(define-public crate-io_deser-0.0.3 (crate (name "io_deser") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bpr5wqiyx6rg8vqc53yln37z2g5s9fpk4g46vf0s19a22pmajnc")))

(define-public crate-io_deser-0.3 (crate (name "io_deser") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d7r4i63qilbw8wj06yz2yfjpl6hqam1pkghlzgynp94mn1az53c")))

(define-public crate-io_deser-0.3 (crate (name "io_deser") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1adisjpvibpffm91vhf31ah0aiva8b6gq7d68rhgqqykxqh74hck")))

(define-public crate-io_deser-0.3 (crate (name "io_deser") (vers "0.3.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b7n2bds59qbfqc4fki9fn31crg3k6fdch3gi7wximky1ccipdk0")))

(define-public crate-io_deser-0.4 (crate (name "io_deser") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "12jrvw22wx0ypwp4s9b6n5gy7kf39d41siaagm9lr28ic95lgzs9")))

