(define-module (crates-io io t-) #:use-module (crates-io))

(define-public crate-iot-plaything-0.1 (crate (name "iot-plaything") (vers "0.1.0") (deps (list (crate-dep (name "rocket") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.7") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0mqss6pm57llsqfxigsbybx14x4zix9jp96d9dlmzvqa5yz4pwxf")))

(define-public crate-iot-shell-0.1 (crate (name "iot-shell") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xv0yldi41w4f76pg7ipzp5y04bbf8v8pg4a2d2yfmrvdhf9f9zj")))

