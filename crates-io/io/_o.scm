(define-module (crates-io io _o) #:use-module (crates-io))

(define-public crate-io_operations-0.0.0 (crate (name "io_operations") (vers "0.0.0") (hash "1r3cky3nlplnqrxvx5gjy8ylybhi5mxb81469c2pfp79vphkc88b")))

(define-public crate-io_operations-0.1 (crate (name "io_operations") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0q41ip4khz0nf3kvzxxmnfj0cc1xdcja4pllckixi8fmj7zm226b")))

(define-public crate-io_operations-0.2 (crate (name "io_operations") (vers "0.2.0") (hash "08dmvq8r1ml5bz61mz3206k4mx52b880ivqk1insmpxacw3yzfn7")))

