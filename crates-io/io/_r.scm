(define-module (crates-io io _r) #:use-module (crates-io))

(define-public crate-io_reader-0.1 (crate (name "io_reader") (vers "0.1.3") (hash "1ril6a2xgvwr19n5fcwlhkx8bpdm0wxd48hcqk0907sar20qs0ap") (yanked #t)))

(define-public crate-io_resp-1 (crate (name "io_resp") (vers "1.0.0") (hash "0kkm9famshcvzzf2q6q7llk2ha823id6iwikwdyirfz635yykiy7")))

(define-public crate-io_ring-0.0.1 (crate (name "io_ring") (vers "0.0.1") (deps (list (crate-dep (name "rustix") (req "^0.38") (features (quote ("io_uring"))) (kind 0)))) (hash "1d25dka9gv04ifqk13nm5swhi169670ghm3s2ch4pvs1v1c68rw9") (links "uring-ffi")))

(define-public crate-io_ring-0.0.2 (crate (name "io_ring") (vers "0.0.2") (deps (list (crate-dep (name "rustix") (req "^0.38") (features (quote ("io_uring"))) (kind 0)))) (hash "0j4zgbjixwqzs6p3w5apfw3jz013a2sqg49w5917d0qp593b6ijd") (links "uring-ffi")))

