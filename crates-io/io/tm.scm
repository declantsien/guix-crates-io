(define-module (crates-io io tm) #:use-module (crates-io))

(define-public crate-iotmesh-0.1 (crate (name "iotmesh") (vers "0.1.0") (hash "0pip0wq2ppafg5cxf1r3v9qp5fx9mvpbc420k3xxqwa1mzqszjnr")))

(define-public crate-iotmesh-0.1 (crate (name "iotmesh") (vers "0.1.1") (hash "081qwv5ycxfzlgdhx6ngdr609saigzy97hm2zajs0r8dm6nwvjwx") (features (quote (("default") ("all"))))))

