(define-module (crates-io io s_) #:use-module (crates-io))

(define-public crate-ios_calculator-0.1 (crate (name "ios_calculator") (vers "0.1.0") (deps (list (crate-dep (name "num-format") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0h58m9sbsxhlw5vzfvs4199479rnls55ynkcamixwvx66c9ycsm2")))

(define-public crate-ios_calculator-0.2 (crate (name "ios_calculator") (vers "0.2.0") (deps (list (crate-dep (name "num-format") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0qbj0xmvgn4f4s3d7v7cq695x3bl1rpr2sgm5cgygd0iqknj7kdw")))

(define-public crate-ios_calculator-0.2 (crate (name "ios_calculator") (vers "0.2.1") (deps (list (crate-dep (name "num-format") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "029c3ysbgqqdkkhkl9h7n1pfbw2mf10zgspkb0c2ha4cmjrg89kc")))

(define-public crate-ios_impact-0.1 (crate (name "ios_impact") (vers "0.1.0") (deps (list (crate-dep (name "icrate") (req "^0.0.4") (features (quote ("Foundation"))) (default-features #t) (kind 0)) (crate-dep (name "objc2") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0yy9bcc82zj6zfpyrzpmq047cp6ym6pqby90yzvm4li90m3n20bv")))

(define-public crate-ios_impact-0.1 (crate (name "ios_impact") (vers "0.1.1") (deps (list (crate-dep (name "icrate") (req "^0.0.4") (features (quote ("Foundation"))) (default-features #t) (kind 0)) (crate-dep (name "objc2") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1ia356nxxg4p2jywfndylphx46878brmprnz54gd9xkhhkaj3dhx")))

