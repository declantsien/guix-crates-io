(define-module (crates-io io ma) #:use-module (crates-io))

(define-public crate-iomath-0.1 (crate (name "iomath") (vers "0.1.0") (hash "1h40pxkj17vm2q9n1x587km595s0n37xyihz88wmvrj882dibqw3")))

(define-public crate-iomath-0.1 (crate (name "iomath") (vers "0.1.1") (hash "1sc4a26cpby4fviznm1jsvmx203jh4i3iasgzk8hzkhn5n6vbwji")))

(define-public crate-iomath-0.1 (crate (name "iomath") (vers "0.1.2") (hash "0ijx1qn04wcy8vbyz0yazpj45y5cjdvii6cmzppac5f0b3w7r77n")))

