(define-module (crates-io io bu) #:use-module (crates-io))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.0.0") (hash "118wv0mpzadcxafk2h2dp3kl7yisxzwshvp395ff3nja9k9y8f18")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.0.1") (hash "0q7aqkik2x5j3i7siv1khvc2fg9xbgn8xijmfl22mybnafpapx69")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.0.2") (hash "0lddxzx7dwgff4zvsflnxg4fljyd7chxx43yzd1pn7cgj10ygffy")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.0.3") (hash "061sk7is5q9wvpzmz2p0rm4d16vf9xw3gbxwwgsp3ky14m3n2yxn")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.0.4") (hash "13cia64s7khl3nfgdv8wnk5aycknprfal20y3qgz1x6chz9g320f")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.0.5") (hash "195qhhivpq04kqmdac0km6g0vraf76rljbs53y807cql42x4pwgb")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.0.6") (hash "16y2yx86nm1x8w52l8qhl1qc8w23zmndyc4hcpbhcadg936n7i4y")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.1.0") (hash "0lbnqvzgbq6v60bnvdjhh7cycj3xf9x79saap41pkfk7gw8j16gh")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.1.1") (hash "1wgp8kc8i4hpi76w7q12hvvqrznxygrk6dyagc74i1c67z9wqh5w")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.2.0") (hash "0hy6nhqbdply5prlmqw7946xpzxx11qbfmx17v6gijhjvhq9z614")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.2.1") (hash "1l1n8n9pliks9gidbd4rmmv54wc4fz5hc5sspk46msanvs4q7akw")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.2.2") (hash "0dw6pn2s79rncfjl2415q2zq4p6ix96qb1skvx8n0mfawb5b46jr")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.2.3") (hash "0fvsd7hjl24aqhaw31bvqvsyz8v01l2867ypdqd8cs0lr6a0ya9l")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.2.4") (hash "1cyfaha0fgy259lrvns01dd39hlpql9p94v2rbchxkl8khyx1qcg")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.3.0") (hash "0rppmpc5y0cdc40ddvmwnjqqx66c3dww39p89lfcg67hidyww5dx")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.3.1") (hash "0nf2qvm3906ygd27bdvpkf3sqsq4vk21jqav5lmkp3vym5ywxr4z")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.3.2") (hash "0kbrcb8dqbhr5y8ynb32z83xdazqnrlcwabb6y8q3d4z6xfll1a9")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.3.3") (hash "1gs78ma89hiw70wvmsaksgc6xr2f07581wvwggnlwqxhpjwr5bzh")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.4.0") (hash "1krqrp2f1kwxr67n0a25zzig0vk938pyalq79v32x0dd46wgw6bf")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.4.1") (hash "06f5svk306xd81lbgqm0y9p8qk5cij91zs2wqb8sds10z5hyc9n6")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.4.2") (hash "110xs18yfy6ks5spjnlqkr3qqmr41fvf5qr4m3mqs7y7g4rcpk2k")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.4.3") (hash "1mm8z7n4rw0m3v36pc6kiywyxd7w5rfpcw7jycmpz9w7dlsn959l")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.0") (hash "1lxgx0x6gl7kfwcbgbbk5wrysnbzv9z3hxy7q8bz6hqz28n1clgm")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.1") (hash "0dckv5yzc4jy8z3lp3ldd573310ilvqkqjf9rxixq4r6l6myifss")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.2") (hash "0vh6cnv9bgg1zbx3vc4rpzv3mqabhnqyn0nfjjlwld9rx4q0s82w")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.3") (hash "0gd5p81lq6gg5yxk1243w91rm8zvya0nc4qfpb1dmwylin8zzrpg")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.4") (hash "0f6rc1ifciv7sixn831h6nlndb5phipn2q53x5aq8n8hgj50vw1y")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.5") (hash "03g5z06injjpygn9cqwrc510k784n29bx9wimlqa2my25kcbl7az")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.6") (hash "11c5aw947jwbdacrkwb91mgmnvjwdddch6i4hxvrr8g3n7qacm15")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.7") (hash "1a602fh08x2xngvrmg3rn8ql4rfw9cjs27abzy0gizjfkx8jxjck")))

(define-public crate-iobuf-3 (crate (name "iobuf") (vers "3.5.8") (hash "13in952y7zr4i99bq5dwhqwdfc9a10ii01nbixx6ikqw0v1fc3r8")))

(define-public crate-iobuf-4 (crate (name "iobuf") (vers "4.0.0") (hash "1y3xp42q8lgx5awb2n34bk84q3qi70l606bqw9m0c0bby4bxwnxk")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.0.0") (hash "0dd64brildaq78p7azz61s60pwzgqzhp063aiagslfgwa22qgcxy")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.0.1") (hash "0j8jg9239859hwk4xn00mad2ynf7iihrcng7w9h1f62307zrx1xs")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.0.2") (hash "054wbx6c44dqabgyn1mnmihkpm97w3zf6a9n1c0k2k0g7dpjm6rq")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.0.3") (hash "0bgbxkxvwr9yn47b8dy9bbnpgabzks5prgd7nxkyb90q3asbd5vm")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.0.4") (hash "0h6i1g0h54l71m0n1ndhq0d0iqmcllar7p6whibf72l29b849gpy")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.0.5") (hash "1sjpjlmkgjzqkfj2ri8935i176kw4x5xn9zm25ypgrxm7qimsa0r")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.1.0") (hash "0qwf7x0qq4vr4fxmaw61rpwgk3md5hfjhp3wnbh6jsra34r440kf")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0") (default-features #t) (kind 0)))) (hash "131zx008sr7g9czg504385k5sp3nmihvhlbhf8ikywdsipgl1cyr")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.1") (hash "0qw8zr51xfkhfhgkyb79avgwkmb1bjq2azgmfgahzn1vgjisz0dj")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.2") (hash "0mgjqdg80rkqspg1mw1j81bvdy8b9bxk8rk4y0pfzvqb159f2bsv")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.3") (hash "0f18y42rny70lqg16p2m70g8x5l5ryzw6zrrm4k8qv3lybp6vp5n")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.4") (hash "0nfjxc485prvi21m20nx170jbcl9pl5sm501kqv1vxbgsdx1avzm")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.5") (hash "1igs0vqmvndp79w9vlww83rb9725qn7yq25hjcfbbq1x05q7kg67")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.6") (hash "0lj70a4pg90gvzrp80nzkxr70yl3bl5c0fkw7si2h4krz76vcbl3")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.7") (hash "0vlvl965pfkj7sdg8n9262x0d3qdx7mfpahcmi4dzn7p6wzk1gnb")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.2.8") (hash "0a8kwky8izcpnyv7i62j1k3gb7yvi5cp2hp37ir69f6zpn925c4i")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.3.0") (hash "0i0v1a9vzs99azyk2s53wb3w24k2c8xgnj3b7b4lk91gvgi9zpms")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.3.1") (hash "0rzkh2bpwmsqfqx7y9v2s7si63ynf3c6bh3z3gpm44xf8ykg2p6m")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.3.2") (hash "088qnx9w37cc0430r13vc9iyhfrkhkpqlnrybgsqcn0iyhz2471y")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.3.3") (hash "08s04gk6zq403if88wcpa2d38l3nff4m1822dmjvicqpkldyls4i")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.3.4") (hash "1z9x8gmlvc5qmp8cqi2bgv65avkqajifm8ydc37ryiv3bsdkd347")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.3.5") (hash "0sqib98w73j7k20bfdqh44qi43vkjcdjs06x7drxm03634f5abcq")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.0") (hash "1x3vl32ybnjn5f36m53r2109ga6r46s29qqczny708kf1cdgk962")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.1") (hash "0csdyxgy3vhpihy95xd64rky9m2s6y7gizg3f42fdw96v5jpdx3w")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.2") (hash "1ayi46i4r9bnj90fpqhrz74ph7yqqg90izrcl6jbi27jj428kl23")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.3") (hash "094dgc9qhlc4dh46cvar42j5n0zc33r1m79zxc2vwy964d5a7b99")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.4") (hash "11x57wq80h0i9hm77m6vkdprnlgb9pv3gnb7r3jwmq80cbi0ssyg")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.5") (hash "1fa0z788zgflnfmcqf10fnbnwxhnnnj3w0zfz8c6h10fp6ga9z8k")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.6") (hash "07bw6ri126pi0rihp799q4myvzvp4bfqnxqp6rlr4hrw3j8f719l")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.7") (hash "0nmgwh5zxiif1vm1s36645rwbb2xb6s5kqnr4rnm116c4afcb946")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.8") (hash "0lckrgr35hnf2m2pz8xr8ma6jv3j3r204cwqvdxqavx90vhnhjba")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.9") (hash "13jys5k2mpzf4ak6whgn55qhjizvgfyi5lzzwpr6ncqpvy132wnx")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.10") (hash "1l3mrh49cyq31gdw422q2y3j20i435ijp1rfif26q5a39vy1gg0y")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.4.11") (hash "1ayqqw3va6bssdph1vvi83kdp3xlwr85n2imwzcpajncy43c7n4v")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.5.0") (hash "026mvwqjjvay2fjl88mq38mfd8qgpk0s4ly97y1pr8qad91awl1m")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.5.1") (hash "157cqk4423n2n7h5nkbijhxnrn2zr8xhpcbqpy88jj2vkch9wqa5")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.5.2") (hash "1w32pd9s9sxqnif2jswrx86zm3n6xq5n7pjpk4giyqa1zlss6yf2")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.5.3") (hash "0cx34mb42klx4dpy2ia1rs5g3igz8wcsi23xx7v1wj5rr1kriwh1")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.6.0") (hash "09piwqc8na1c5bj4rzlv2344771clv1cxhhrpqfglgllkq8x5q81")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.6.1") (hash "0qrrrp6l4vyxia59rs0l1q2lvp6qajjcyrdgy08q5jqywra3qa06")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.6.2") (hash "0qfnm42pxjl4g64i1qid7ah6rzch60m73hagykqqfzsirs005j76")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.6.3") (hash "1g5k49bmj7447bba4xc85xs49w4bmgfbl3phyp5cdhg33calpbgb")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.7.0") (hash "097baqkqklhxdr3m00szw2fjf5s13lhvj1ni80hp47b2wivwl27b")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.7.1") (hash "0qkzsfwikpbcxy7nbsvvjr0qh25ibsiabrgq2qa320mf1j4bwac8")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.8.0") (hash "1i5bp17r5g07jplff0603n4nbcfs2vryr9gnsbhsnz58rp762nnr")))

(define-public crate-iobuf-5 (crate (name "iobuf") (vers "5.8.1") (hash "0qn0yqb7bz4v0i7xvs5g5ap981l5v11iml7gpc6zkyrsjkr4n0kf")))

(define-public crate-iobuffer-0.1 (crate (name "iobuffer") (vers "0.1.0") (hash "140br30kk04c33fsfpv3ddwbqrzh1gc97bjqvb24j4f663d7kl44")))

(define-public crate-iobuffer-0.2 (crate (name "iobuffer") (vers "0.2.0") (hash "171jiadifcm0ii7iar7czf41b6092c05k80h13gv813qfsfvdchr")))

