(define-module (crates-io io cp) #:use-module (crates-io))

(define-public crate-iocp-0.0.2 (crate (name "iocp") (vers "0.0.2") (hash "17554l2mv2674gd2jsd8cd6m4899jmdf9vf3rddppfbbd8rf7nh9")))

(define-public crate-iocp-0.0.3 (crate (name "iocp") (vers "0.0.3") (hash "1i7gyixx8zagz6jw1yyqbgcmw2m0yp7q5z53gx365m91vnzwzfx4")))

(define-public crate-iocp-0.0.4 (crate (name "iocp") (vers "0.0.4") (hash "18n7mms30hqlj1yb945z5hcp7b268vf15xwvvfgcg2bcbfnkfyzd")))

(define-public crate-iocp-0.0.5 (crate (name "iocp") (vers "0.0.5") (deps (list (crate-dep (name "kernel32-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0pnfbmnf77n79hpnd7g1nwvs0mraqflihg8ggclbc5h0ln7gglci")))

(define-public crate-iocp-0.0.6 (crate (name "iocp") (vers "0.0.6") (deps (list (crate-dep (name "kernel32-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1b7i4is2frwq5bf3lcs7gr3x43pncimq5hqvadg3n8rcv3g37wzq")))

(define-public crate-iocp-rs-0.1 (crate (name "iocp-rs") (vers "0.1.0") (deps (list (crate-dep (name "windows-sys") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "03kgcrrfwhra7nlqk06hrw2jfsbbx094004795bqhd5cxpj9g2cj")))

(define-public crate-iocp-rs-0.1 (crate (name "iocp-rs") (vers "0.1.1") (deps (list (crate-dep (name "windows-sys") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1ld2z79barcnvwprvbmg2qinkkyxbcw244q96b9xxb3a1kvl6hd4")))

(define-public crate-iocp-rs-0.1 (crate (name "iocp-rs") (vers "0.1.2") (deps (list (crate-dep (name "windows-sys") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1p0gj22zhkp8vvr8q5fmnnrnb0yyxfg6nm0ffhcwim949jr20x27")))

(define-public crate-iocp-rs-0.1 (crate (name "iocp-rs") (vers "0.1.3") (deps (list (crate-dep (name "windows-sys") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Networking_WinSock" "Win32_Security" "Win32_Storage_FileSystem" "Win32_System_IO" "Win32_System_Pipes" "Win32_System_Threading" "Win32_System_WindowsProgramming"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1hgp4r6x18zq9x3i5kcb4cr26jdwkb0hn794a162y4av7dh5m0nb")))

