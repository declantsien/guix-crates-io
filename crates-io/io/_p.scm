(define-module (crates-io io _p) #:use-module (crates-io))

(define-public crate-io_parser-0.1 (crate (name "io_parser") (vers "0.1.4") (hash "1kf72x9b6w6mw5kilr7l2xspqjhgjjah4ai3izhza5jyi3d6wx48") (yanked #t)))

(define-public crate-io_parser-0.1 (crate (name "io_parser") (vers "0.1.5") (hash "1vx3dqqx3ygzzjv9fbbaih3v89dgr748bi3bpqzgqnfjdyg4agyd")))

(define-public crate-io_partition-1 (crate (name "io_partition") (vers "1.0.0") (hash "0sm5cc5a8fjvf8f48q1ja2rln8ffjvn8pz9zb2xhm7a93gycj3jn")))

(define-public crate-io_partition-1 (crate (name "io_partition") (vers "1.0.1") (hash "0g2gpz0pl02ch7pvwj8vkjblknfmi6zfji8ffqwvyg53fyw8p038")))

(define-public crate-io_partition-1 (crate (name "io_partition") (vers "1.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0zfigkp5qkbfw7y018f1iz18i60ahv8fmkxq00ljrm86hks98jr4")))

(define-public crate-io_partition-1 (crate (name "io_partition") (vers "1.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0hp8jvg221pnw5m2lc9vwrq11gz0y7gjxjcx15808jaskv26gh8f")))

