(define-module (crates-io ww pa) #:use-module (crates-io))

(define-public crate-wwpasswd-1 (crate (name "wwpasswd") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "dialog") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lsx") (req "^1.1") (features (quote ("sha256"))) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0mxch2202xkhzbfcfpc1123v6jy2wqs4571q8002qil3pavzdlv1")))

(define-public crate-wwpasswd-1 (crate (name "wwpasswd") (vers "1.0.1") (deps (list (crate-dep (name "base64") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "dialog") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lsx") (req "^1.1") (features (quote ("sha256"))) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "05n2clffjmd3y1h25gn1d5m95akvkfrg9rk4pyamnb6i285r8jrw")))

