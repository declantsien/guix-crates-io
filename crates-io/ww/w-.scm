(define-module (crates-io ww w-) #:use-module (crates-io))

(define-public crate-www-authenticate-0.1 (crate (name "www-authenticate") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1jhydvxxybwxca78dgf5n9qc0m90axaisq9ikn70hjcww14f3l0l")))

(define-public crate-www-authenticate-0.2 (crate (name "www-authenticate") (vers "0.2.0") (deps (list (crate-dep (name "hyper") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "1nsiqv516mdyp0mp01izaaq4cw3wgx1jas6x3s5kjiyf8n97q43i")))

(define-public crate-www-authenticate-0.3 (crate (name "www-authenticate") (vers "0.3.0") (deps (list (crate-dep (name "hyperx") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "1p87dhx51j0kwvrxqgn4lkd7hyr305vkk1r2fd64xnlw4nwfyqlc")))

(define-public crate-www-authenticate-0.4 (crate (name "www-authenticate") (vers "0.4.0") (deps (list (crate-dep (name "hyperx") (req "^1.3") (features (quote ("headers"))) (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.5") (default-features #t) (kind 0)))) (hash "1l18fkkmg9kya700yzb7jw41qcxnqsh9n8jb2119i3axa1q1kz82")))

