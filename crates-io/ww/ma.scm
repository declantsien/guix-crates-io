(define-module (crates-io ww ma) #:use-module (crates-io))

(define-public crate-wwmap-0.1 (crate (name "wwmap") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cidr-utils") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "convert-base") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0v3i0xwx2s81wxga86k809dmqvwq1bmj7m3acvq9mgm05l7jbxxf")))

