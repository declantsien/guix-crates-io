(define-module (crates-io ww -m) #:use-module (crates-io))

(define-public crate-ww-macro-0.0.1 (crate (name "ww-macro") (vers "0.0.1") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (default-features #t) (kind 0)))) (hash "1avd16xqwwsnnqayj13z6im7xjy15ar782sy7xkv5kglmi7iwjlm")))

