(define-module (crates-io rr s-) #:use-module (crates-io))

(define-public crate-rrs-lib-0.1 (crate (name "rrs-lib") (vers "0.1.0") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "00fh4mx3g4xk8d4lq008k3495zsg27cyv9jbyvkxmsx4ycx2sf5l")))

