(define-module (crates-io rr ai) #:use-module (crates-io))

(define-public crate-rrain-0.1 (crate (name "rrain") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json" "blocking" "stream"))) (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "textplots") (req "^0.8") (default-features #t) (kind 0)))) (hash "0p0bp02szw27456lgjykqsrvbnmcdx39jd2yxdbvrkcr5kfidwax")))

