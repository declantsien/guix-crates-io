(define-module (crates-io rr tk) #:use-module (crates-io))

(define-public crate-rrtk-0.1 (crate (name "rrtk") (vers "0.1.0") (hash "17yhpbfaqipq2iyp9h1lds1n49av91dp97dpyal8484gji3hnxyk") (features (quote (("std") ("default" "std"))))))

(define-public crate-rrtk-0.1 (crate (name "rrtk") (vers "0.1.1") (hash "1mv8nmq83vjx4n7pywrsj4pd28zl3c9c0jwlp1700nb2d2h54ix6") (features (quote (("std") ("default" "std"))))))

(define-public crate-rrtk-0.2 (crate (name "rrtk") (vers "0.2.0-alpha.1") (hash "0l14zw96kq4w1wb8117fizk9q4z80865yw4kriavrg76fvi0jinr") (features (quote (("std") ("default" "std"))))))

(define-public crate-rrtk-0.2 (crate (name "rrtk") (vers "0.2.0-alpha.2") (hash "1qcf7wvqdhq5qndmsbbnj4qzv4w02wkgpcf6yhjyp029xidjsdxn") (features (quote (("std") ("default" "std"))))))

(define-public crate-rrtk-0.2 (crate (name "rrtk") (vers "0.2.0-beta.1") (hash "0dg2g6y9ahbmdwhxl03w2v6x3hbqdv06bnwpj87bvmji6n3c7dd6") (features (quote (("std") ("default" "std"))))))

(define-public crate-rrtk-0.2 (crate (name "rrtk") (vers "0.2.0-beta.2") (hash "12h9mskc0i7bi52msa5r25dal35sw5hdniw3cd0d6hk81qadlwar") (features (quote (("std") ("default" "std"))))))

(define-public crate-rrtk-0.2 (crate (name "rrtk") (vers "0.2.0") (hash "1fi1w93lvxm0jg7i2w8p6hlqadixvq5qysf7can62pk2y8wmgqhg") (features (quote (("std") ("pid") ("motionprofile") ("devices") ("default" "std"))))))

(define-public crate-rrtk-0.3 (crate (name "rrtk") (vers "0.3.0-alpha.1") (hash "09h446yzlqxkbm208p6rmv4jhzwnghicfby3xx8sqml6lm2bnib6") (features (quote (("std") ("pid") ("motionprofile") ("devices") ("default" "std"))))))

(define-public crate-rrtk-0.3 (crate (name "rrtk") (vers "0.3.0-alpha.2") (hash "0gw55kv0v9jvzp2x4hs1n661mcbj5j61zczd18l666lgspi4yb5q") (features (quote (("std") ("pid") ("motionprofile") ("devices") ("default" "std"))))))

(define-public crate-rrtk-0.3 (crate (name "rrtk") (vers "0.3.0-alpha.3") (hash "0can2m2rjkl16gjkkb9d21afnhfvzz1i31k85pv0bprjh6wsny5b") (features (quote (("std") ("pid") ("motionprofile") ("devices") ("default" "std"))))))

(define-public crate-rrtk-0.3 (crate (name "rrtk") (vers "0.3.0-alpha.4") (hash "1yq5cgvlkswnx6jkwwygscl7blp7ij12y895fcw04wkwsph8dgqp") (features (quote (("std") ("pid") ("motionprofile") ("devices") ("default" "std"))))))

(define-public crate-rrtk-0.3 (crate (name "rrtk") (vers "0.3.0-alpha.5") (hash "1h5n25fh6a8ag66p5dda42grmccx4i94l51h7gxwmimjb1blmh91") (features (quote (("std") ("motionprofile") ("default" "std"))))))

