(define-module (crates-io rr ol) #:use-module (crates-io))

(define-public crate-rrole-0.1 (crate (name "rrole") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sts") (req "^0.32.0") (default-features #t) (kind 0)))) (hash "18z5s5climq2sjk8659vh7yr086sf3xz6b2zfbhmbsbz6081pbad")))

(define-public crate-rrole-0.1 (crate (name "rrole") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.32.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_sts") (req "^0.32.0") (default-features #t) (kind 0)))) (hash "064hq02x480sj6b3qv8fjk5ii1l6nk700z5s6digy1npqshiiv21")))

