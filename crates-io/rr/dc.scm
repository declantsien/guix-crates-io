(define-module (crates-io rr dc) #:use-module (crates-io))

(define-public crate-rrdcached-client-0.1 (crate (name "rrdcached-client") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1822k8hxajg5hhiwwl7jjjvh17s466fvqkl7imgc8ffhiz1ar3br")))

(define-public crate-rrdcached-client-0.1 (crate (name "rrdcached-client") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03h2l3z0zkvgj3fdlj0zmiq9m4jdfp06wc6kjmrlkdh10gpcy7bc")))

