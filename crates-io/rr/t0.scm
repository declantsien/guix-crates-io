(define-module (crates-io rr t0) #:use-module (crates-io))

(define-public crate-rrt0-0.1 (crate (name "rrt0") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.1") (default-features #t) (kind 0)))) (hash "10pmz2m5cvaga7dnb8px3dyc1id5cswsz9n3s4jn4gg96vi51kz4")))

(define-public crate-rrt0-0.1 (crate (name "rrt0") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cpnmayqff6li539p62v4jxdbd9vjqh3a4izblsvc4n754aapaim")))

(define-public crate-rrt0-0.1 (crate (name "rrt0") (vers "0.1.2") (deps (list (crate-dep (name "libm") (req "^0.1") (default-features #t) (kind 0)))) (hash "0126hbfrmj2y9z991vfxljjs7y4ml4zn6ihws73hk6malpjpvp1l")))

(define-public crate-rrt0-0.1 (crate (name "rrt0") (vers "0.1.3") (deps (list (crate-dep (name "libm") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pwznr84kfg24j47ayd5p82vksdlpndshyylryby8swwi9z7y3bh")))

(define-public crate-rrt0-0.2 (crate (name "rrt0") (vers "0.2.0") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "0v1m4m2f6az1gbx50v9w4j0d1i2n6km0k5qvrmg5szyikv62r2hz") (yanked #t)))

(define-public crate-rrt0-0.2 (crate (name "rrt0") (vers "0.2.1") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "1agb1r1jidf403vhy93mi7yci23bpb5hrzgwl5hy0xfyz2078xai")))

(define-public crate-rrt0-0.3 (crate (name "rrt0") (vers "0.3.0") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w5gcj5ql9nxnm3bfhlfig0xr0hl383v672v8waj96rzrxvb61fp") (yanked #t)))

(define-public crate-rrt0-0.3 (crate (name "rrt0") (vers "0.3.1") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v60fb96f57zkm9afd7lw3qxqnw8x2wg2xq6d81w7whnq6b0m6cm")))

