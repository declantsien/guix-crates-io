(define-module (crates-io rr sy) #:use-module (crates-io))

(define-public crate-rrsync-0.0.0 (crate (name "rrsync") (vers "0.0.0") (hash "1ysa2qp6bn9x3f9nsw768663x8l6zww7c5sgl5dypbbvakazv3xz") (yanked #t)))

(define-public crate-rrsync-0.1 (crate (name "rrsync") (vers "0.1.0") (deps (list (crate-dep (name "cdchunking") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (features (quote ("termcolor" "atty" "humantime"))) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.16") (features (quote ("chrono"))) (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1x1ysfqcgvbvllx037r7waqc485sv3grk5s9lm4pnsm3an47wsvg") (yanked #t)))

