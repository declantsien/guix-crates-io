(define-module (crates-io wh oa) #:use-module (crates-io))

(define-public crate-whoa-german-numbers-0.0.1 (crate (name "whoa-german-numbers") (vers "0.0.1") (hash "1l26yh1lqmbdpvf8lalipv43amxfk65pp9gsqbn0w2n06pg9yrxb")))

(define-public crate-whoa-german-numbers-0.0.2 (crate (name "whoa-german-numbers") (vers "0.0.2") (hash "1sb2p4lwkpr8c90x8ws0z1zwh6wb3wa5i651mh76wj94kh2hprra")))

(define-public crate-whoami-0.1 (crate (name "whoami") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.28") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)))) (hash "0icgwng3slaglgh8j1ax8s165gc2l629v2wk1pf1g3d4dp6b7gla")))

(define-public crate-whoami-0.1 (crate (name "whoami") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)))) (hash "0cf9abc0lc93jny1zs3yk47h2gp44c68nndwd5v00g3pyfr1l456")))

(define-public crate-whoami-0.2 (crate (name "whoami") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.6") (optional #t) (default-features #t) (kind 0)))) (hash "001k73v38dwhrvy9l23n4j0bivfhzlaads3c9p2gf2p4b4p06j7l")))

(define-public crate-whoami-0.2 (crate (name "whoami") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0k6p1pmipahp7ckil8n7ff9r6pv403h6kab2ij5mh1zjl3cz3h6w")))

(define-public crate-whoami-0.2 (crate (name "whoami") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1wak5w0cxfq7py3jhkb8v4iriinqyw7yv6ywgzl4zdsgjnzjna58")))

(define-public crate-whoami-0.2 (crate (name "whoami") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "02rq3f53wf4kxg1giv3rxkbm5wisq0by0srj0yjxk6wx9dmf5hkq")))

(define-public crate-whoami-0.2 (crate (name "whoami") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "0lghdcq4cmv1p3j25b0vidvdrfvm3vcsh1iiamgjf1gc5cm5c39s")))

(define-public crate-whoami-0.3 (crate (name "whoami") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1rh8bjfgd2mv31hwfz1kpm94liipy16cjc1v1x6m7lxcpfy1dbq7")))

(define-public crate-whoami-0.4 (crate (name "whoami") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m17lwx60nhbkm2scy567ryp9hp7xmmmdpl6vj9n6c9yfdvddg7y")))

(define-public crate-whoami-0.4 (crate (name "whoami") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x1bzf7bvc74vrglzhcsaklas43d38xzxrlcal65da06jq7j370v")))

(define-public crate-whoami-0.5 (crate (name "whoami") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "106v852gxxgj6h1vh8mxdkzf5bv7lvqz22nj8y0f20q9y9ipga96")))

(define-public crate-whoami-0.5 (crate (name "whoami") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l4nz4p39qxz8fcl7xk29pis0dkx1id84ml6ymd2a32bcqnhvnvy")))

(define-public crate-whoami-0.5 (crate (name "whoami") (vers "0.5.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ygvnddglilpf9cm0ncn2hy0a0y9w7q3j4nr3lm5ng056kxgff96")))

(define-public crate-whoami-0.5 (crate (name "whoami") (vers "0.5.3") (hash "1wvj97y0hlsh84nc273k7gj7nkj4vwrkdmnzi60s6wc093sg6m3i")))

(define-public crate-whoami-0.6 (crate (name "whoami") (vers "0.6.0") (deps (list (crate-dep (name "stdweb") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1azw17wadmxxmw9n62yxbijvifrndx7xyfns9w71l3qvwgr0syy4")))

(define-public crate-whoami-0.7 (crate (name "whoami") (vers "0.7.0") (hash "1sg8lj61j9hy489760qixbdd4lqc5m9zp2klk3hplaz18ly6cak2")))

(define-public crate-whoami-0.8 (crate (name "whoami") (vers "0.8.0") (hash "16y6nvb3rsvlvi0wijfy4rx93r5v0nqy77mnmpy9zxkn3pmf2gsi")))

(define-public crate-whoami-0.8 (crate (name "whoami") (vers "0.8.1") (hash "14f3vqv6x7pli2nnm5vv9ip30zkzvvp5cidrh4g8isjqn52bi3m0")))

(define-public crate-whoami-0.8 (crate (name "whoami") (vers "0.8.2") (hash "0z18m6w2q8a6rivd61sh3f00pdhyvxiwycs2j5088gvgdxb5bfqq")))

(define-public crate-whoami-0.9 (crate (name "whoami") (vers "0.9.0") (deps (list (crate-dep (name "cala_core") (req "^0.1") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "012mw2q72gpmf354yw2qc5w105ziac75shpqp1f62x4hnqx7g13q") (features (quote (("wasm-bindgen" "cala_core/wasm-bindgen") ("stdweb" "cala_core/stdweb") ("default") ("cala" "cala_core/cala"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.0.0") (deps (list (crate-dep (name "wasm-bindgen") (req ">=0.2.0, <0.3.0") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req ">=0.3.0, <0.4.0") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1n42rcsi8bbd00inm1sw9kxrlh8aiww9pn5av2bm3x4n3661pf70") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.0.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0d90722ay0dlbgakxghmx5wpiibsqgcjb5wf382mgiplzbkram73") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.0.2") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0mvj18k7463g2armwf3n7p8wjg0nv1vany405ld9grh5zpgk4xzd") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.0.3") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0dmlxqq6758wysx46zr64b4q9g72d0ax9f4hlpb86qgk8vhv55fm") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "07pf27fnx54awhwn2l6sc46yj4gdy2arpfqbdh5ir9bqsl51r4hs") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0w5w785g94fwr657lyyrlcpjnmimnc7xm9gbqg2s5jwk15anya8y") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.1.2") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1xiaddvcd54yl9jyk3lnw3gvc0d9y8viv4s622pzx3cmbhrczfja") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.1.3") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "HtmlDocument" "Document" "Window"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1qbs5lvc076xc85pg3scrxw43yjfais5bykdr5ksh002lihi2x7p") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.1.4") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1w0zpl1imagklg2dmbmrpyr55b3j0knxjnhbgsai2rlklhmf5gya") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.1.5") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "05v1nsf82sf9mmgkm2s7pi70zway66s2xg08xf8fqgx9w7z5jfj8") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.2.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1xzmdsp0d8kvjm99l5a4ly9ivif0dhnf2rw9r7rgnkka4gpcafn3") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.2.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1s355zs8ir1li29cwvzgaqm6jyb51svmhqyx2hqgp8i0bbx5hjsj") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.2.2") (deps (list (crate-dep (name "bumpalo") (req ">= 3.0, <= 3.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen") (req ">= 0.2, <= 0.2.78") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "05cz6s3rpq2x1c5xpkanadffzw42c47rw4f4vbxa7i0mc53gmnaz") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.2.3") (deps (list (crate-dep (name "bumpalo") (req "^3.0") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0wfmrfaaqh41rrj08n1ddyjg07i4mnkz3s12nr0ii6ym5xm1nqyn") (features (quote (("default"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.3.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)))) (hash "1qr6ais0yikqyf8bh05ry854lkhpxkfkggd9c717v8nw1hgwgns5") (features (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.4.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)))) (hash "0s4zdcmikx7blgavwkilw7skgz5h7i98jkl69v09qh6a29226w1c") (features (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.4.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)))) (hash "0l6ca9pl92wmngsn1dh9ih716v216nmn2zvcn94k04x9p1b3gz12") (features (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.5.0-pre.0") (deps (list (crate-dep (name "wasite") (req "^0.1") (default-features #t) (target "cfg(all(target_arch = \"wasm32\", target_os = \"wasi\"))") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)))) (hash "0bm06f8w7zp1gkww621kz22fnyc4j05m7miywpw307z6s1x4pmd7") (features (quote (("web" "web-sys" "wasm-bindgen") ("default" "web"))))))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.5.0") (deps (list (crate-dep (name "redox_syscall") (req "^0.4") (default-features #t) (target "cfg(all(target_os = \"redox\", not(target_arch = \"wasm32\")))") (kind 0)) (crate-dep (name "wasite") (req "^0.1") (default-features #t) (target "cfg(all(target_arch = \"wasm32\", target_os = \"wasi\"))") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)))) (hash "0gl9f1rd8yyyiv459kprgn9i4k46qa7qxlbf89iq27xl90fpiv0g") (features (quote (("web" "web-sys") ("default" "web")))) (rust-version "1.40")))

(define-public crate-whoami-1 (crate (name "whoami") (vers "1.5.1") (deps (list (crate-dep (name "redox_syscall") (req "^0.4") (default-features #t) (target "cfg(all(target_os = \"redox\", not(target_arch = \"wasm32\")))") (kind 0)) (crate-dep (name "wasite") (req "^0.1") (default-features #t) (target "cfg(all(target_arch = \"wasm32\", target_os = \"wasi\"))") (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Navigator" "Document" "Window" "Location"))) (optional #t) (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(target_os = \"wasi\"), not(target_os = \"daku\")))") (kind 0)))) (hash "1aafr70h2zlqr73i58bj84hdf9rgplxbpygqbgsqhkk3mngv8jm4") (features (quote (("web" "web-sys") ("default" "web")))) (rust-version "1.40")))

(define-public crate-whoareyou-0.1 (crate (name "whoareyou") (vers "0.1.0") (deps (list (crate-dep (name "hostname") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1403xjv7c4cgybx2a39xgk6z46n0131bnxqlaxgiaakl65cgvb0x")))

