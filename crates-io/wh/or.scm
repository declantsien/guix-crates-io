(define-module (crates-io wh or) #:use-module (crates-io))

(define-public crate-whorl-0.1 (crate (name "whorl") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1d9cnzqzqahx4kpkqhmrfkgnvxhb9ax9x9rsqk2kfkc720b0jgj1")))

(define-public crate-whorl-0.1 (crate (name "whorl") (vers "0.1.1") (hash "1d51ijpzv3j87xshcz6z7ms7g6n2cm0mhkxndhg9cif6yvilr8ch")))

