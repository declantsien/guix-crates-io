(define-module (crates-io wh ym) #:use-module (crates-io))

(define-public crate-whyme_string_randomiser-0.1 (crate (name "whyme_string_randomiser") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand_seeder") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "179lfrs7p1gl9gpb2198f746wwlllqnfxmvy8zqijrxg7kiw1hrq")))

