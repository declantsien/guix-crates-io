(define-module (crates-io wh as) #:use-module (crates-io))

(define-public crate-whasm-0.0.1 (crate (name "whasm") (vers "0.0.1") (hash "0fzya6c5cq0r3b3vsx5n2ldcjv6dl0irlv8glmjcznvs3qd9b69i")))

(define-public crate-whasm-0.1 (crate (name "whasm") (vers "0.1.0") (deps (list (crate-dep (name "err-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "whasm-grammar-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cmyb6bj73x7i4m1mjkyh2vrbh6j09dymh134398rnc5mz17v2pm")))

(define-public crate-whasm-0.1 (crate (name "whasm") (vers "0.1.1") (deps (list (crate-dep (name "err-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "whasm-grammar-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w9yjmpil77gl2rb08i3hdlmq0q568r887xd1nxciaca37l2lfs1")))

(define-public crate-whasm-grammar-derive-0.1 (crate (name "whasm-grammar-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-crate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0cms0p8rdcvg458nnavjsskx6wrvyprrqar703v2v98h1z3k0bq3")))

(define-public crate-whasm-grammar-derive-0.1 (crate (name "whasm-grammar-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-crate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "08d652471lbydm7dg7h5vba44nflqls3p5ff7vyb5x26p5xrawzk")))

(define-public crate-whassap-0.0.0 (crate (name "whassap") (vers "0.0.0-wrong-crate") (hash "0sn4cb51f8q9hsy6m2b1ywy72r6q4pgqks8zfzd2311j1qv25r00")))

(define-public crate-whassup-0.0.0 (crate (name "whassup") (vers "0.0.0-reserved") (hash "0l4dndnqic32sjxlm91pxanpmcqv0zbk03hni1jrmn8l5xg6ayb0")))

