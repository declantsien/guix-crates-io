(define-module (crates-io wh y-) #:use-module (crates-io))

(define-public crate-why-not-0.1 (crate (name "why-not") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "18788j37ylna97b8awr0lwfkb0vhd84f0b1v54014s4nmzibn84x")))

