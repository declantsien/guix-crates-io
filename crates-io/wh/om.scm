(define-module (crates-io wh om) #:use-module (crates-io))

(define-public crate-whome-0.1 (crate (name "whome") (vers "0.1.0") (deps (list (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^0.4") (default-features #t) (kind 0)))) (hash "14wf4pfcrssqg6ca0vyq0665ixhlm5jf8b835vvfrnwphggn7as2")))

(define-public crate-whome-0.1 (crate (name "whome") (vers "0.1.1") (deps (list (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ls5nlfb0i1wdkbjxvz5c8rjh9qcn700wp7x1fl0w362gardahym")))

(define-public crate-whome-0.2 (crate (name "whome") (vers "0.2.0") (deps (list (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ngc96mayz8zl5wxmfnkmbm8cdgyv7wn1bjgcv0x1y6ipr1i12x7")))

(define-public crate-whome-0.2 (crate (name "whome") (vers "0.2.1") (deps (list (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^0.8") (default-features #t) (kind 0)))) (hash "0pgfs03nyw53icpnh6a7iqlrji37xkc8v1mvxs331h8r02am4naa")))

(define-public crate-whome-0.3 (crate (name "whome") (vers "0.3.0") (deps (list (crate-dep (name "term") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^0.9") (default-features #t) (kind 0)))) (hash "0zpx46ya1lxnsngi94kmda486q5m3g0kqzsn9js7l2b7crn9qldx")))

(define-public crate-whome-0.4 (crate (name "whome") (vers "0.4.0") (deps (list (crate-dep (name "term") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vrvc33d3w0byk9r2y56jdnas9g119df5zlqk8kawrn1m8rlmfnz")))

(define-public crate-whome-0.5 (crate (name "whome") (vers "0.5.0") (deps (list (crate-dep (name "term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2") (default-features #t) (kind 0)))) (hash "11m4j0y6bii1nw9jwzv3fwf4zpcx4cz6abgjwc0hdv0qv126bl0z")))

(define-public crate-whome-0.6 (crate (name "whome") (vers "0.6.0") (deps (list (crate-dep (name "term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "13lhcavs8flmgcyr46vxf1cfax8q2l3y0hbk5h6ildrxj9c9gkhi")))

