(define-module (crates-io wh ip) #:use-module (crates-io))

(define-public crate-whip-0.0.0 (crate (name "whip") (vers "0.0.0") (hash "1hqsl0lq4j7aqmqs61n3m8pnn5bmzw3il380pjpqcvjjdipirs59")))

(define-public crate-whip-up-0.1 (crate (name "whip-up") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.16") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1msk7rdc3cisjy3130j6yqifjs2y8zp5c1rl0qig9cziklk258yd")))

