(define-module (crates-io wh er) #:use-module (crates-io))

(define-public crate-where-0.0.0 (crate (name "where") (vers "0.0.0") (hash "1f5hab9zbfhv38xlfs9k7gm1xk650684cxrs4wj7liywfpfcdjjf")))

(define-public crate-where39-0.1 (crate (name "where39") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0633rdkigy345qmq7ysplz76sagry2akfdmrya74p0d05hjfn9mx")))

(define-public crate-where39-0.1 (crate (name "where39") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "02w4vaka327487jx1i5g0j4hnzg5jvc9cbnnx4kwzvg6kgfsc1r5")))

(define-public crate-where_am_i-0.1 (crate (name "where_am_i") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1q9xgp3kjddsvqxgj27ij4yilr7pvwxi41fhv3mnbfmwbljc1zc1")))

(define-public crate-where_am_i-0.1 (crate (name "where_am_i") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02a5rfz75ig1dfl7axv4dhg2gfcfilma42g3pgi9nky5zwgad8pk")))

(define-public crate-where_am_i-0.1 (crate (name "where_am_i") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1hf9wnxczxldg6cn66r638z02gsn0v208mdj3nk6vdcykrxjddqx")))

(define-public crate-where_is-0.1 (crate (name "where_is") (vers "0.1.0") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "07j12b50n2s7yryc7dmz5rkql65fyiqg1wi91zv68nmjsl5vgx56")))

(define-public crate-whereami-1 (crate (name "whereami") (vers "1.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ir2i37ivgwsq7wjy6g9v6b011c0rqdlwh0g72c00sbvvs9j78ha")))

(define-public crate-whereami-1 (crate (name "whereami") (vers "1.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hgc78d6wxn7yhj98jgmk8ljam9m0bljj0084g591vkzxcssal6s")))

(define-public crate-whereami-1 (crate (name "whereami") (vers "1.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "19kyrlkd13jdd26zd8pc4n49zr3098c4va51q8wp7mrf1xwnwri9")))

(define-public crate-whereiam-0.1 (crate (name "whereiam") (vers "0.1.0") (hash "0pajzsscd6dh6czxyncx074day24pblwa21bl3pn7hiy65dcnc0s")))

(define-public crate-whereiam-1 (crate (name "whereiam") (vers "1.0.0") (hash "08qgx391v1j16ds9vql6n90rzwphhspcgv5z78xrk32i2zhm8bih")))

(define-public crate-whereiam-1 (crate (name "whereiam") (vers "1.1.0") (hash "1hzdhllska0pz38vxk5qy42g557vgz6cl8bpv7z7f8f1ivkscgdw")))

(define-public crate-whereis-0.1 (crate (name "whereis") (vers "0.1.0") (deps (list (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "1c18c7cd73cb2sw2yy18cffr0xvksfzhvjdjn3905j0yxig8fpid")))

(define-public crate-whereis-0.1 (crate (name "whereis") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nothing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ssm4in2sn17chl05cnjg6s78cfnpd6fn9019swwa7qbcyjfgk4b")))

(define-public crate-whereismybin-0.0.1 (crate (name "whereismybin") (vers "0.0.1") (hash "193vh06kprjf59r8vnkn9xwb11ywz4cbv2pw8d42n0kd8j5ag5j8")))

(define-public crate-wheres_my_pi-0.2 (crate (name "wheres_my_pi") (vers "0.2.0") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "10wygjlica5fnzqx2i7595fi53kzjrpvv8ccs3w6pnja1pzfxqgj")))

(define-public crate-wheres_my_pi-0.3 (crate (name "wheres_my_pi") (vers "0.3.0") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1q4wip1m8x862yc48ximnzlm99x24mjifxkf61hdnfa63y2bk8qm")))

(define-public crate-wheres_my_pi-0.4 (crate (name "wheres_my_pi") (vers "0.4.0") (deps (list (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.29.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0pwdkz0mnvsp8h5baciqxxyllnv4vp26z3nnfpxxnxrddslmh1x8")))

(define-public crate-whereyoufrom-0.1 (crate (name "whereyoufrom") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.36") (features (quote ("rt" "net" "signal" "io-util"))) (default-features #t) (kind 0)))) (hash "0m7jd91pcgpvj935qgm7qxa5ywb86885wv9264fkrmc2qfimshmb") (rust-version "1.76.0")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.0") (deps (list (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fpf20517y9nk7xn1qlir2gw4y5rz7vpqrm0gs6wj2kl7422jvdv")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.1") (deps (list (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1j4aczpvny331zkkcxkkf5p08pa6aj5s7x00jbdy358m7vc48308")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.2") (deps (list (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1n9rrnmkik6kf3bpvk4jnjqgbpm06317yp6y98lv67yz6f56wx80")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.3") (deps (list (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ijni1a9dw80iydbkaka7m5zsb55kj0s2gh35iqf53a5vmfdfp4f")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.4") (deps (list (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "0i7v3wx8yn4pisz7b1z2dfl63r82sicclcplrp2gqmy0p33baxck")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.5") (deps (list (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "04vj0barzkqp25k8rqy0bzb07836403y3ii1jxmvxzg0bvcmsfn8")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.6") (deps (list (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jxi082dbqwd6wc09cqxcrppr1zijn5cwbahhfmckpr506wnrhsr")))

(define-public crate-wherr-0.1 (crate (name "wherr") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wherr-macro") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sxf2qqz7lkvrxylqnc77xki9ykcjffq9kmjqymj652vdm1znr8k") (v 2) (features2 (quote (("anyhow" "dep:anyhow"))))))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "0rrxjac3mpfb0z5ngw8hrhrqh84955i2gi76ic87src42wzx7fcf")))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "1haqwlc2yylfa0bi2nclh3nk3ww3zxklk9bbz3jrdb7clh2l7hkc")))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "00mpkx40xv3sjy2dbikkm2k0zmm0sd83z625qhh7h218bqjrsvbn")))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "0vchpfg6z4bgzfw277v4lm4c46azhakv2508aira1gm6wy25127v")))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "0izwf2rqbgrr6al5vqlc400gf4zr2wpkxndzwmrkkdm0rz7zafdi")))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "12pl8fcl7rqrc3ciqbxczxrpag91lfdf6hl1r2qmjhbi5626h792")))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "12kxamnwi742rxcbwf2zap828jwfd5x6zx5c93raws1wxdjzg577")))

(define-public crate-wherr-macro-0.1 (crate (name "wherr-macro") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "visit-mut" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "061ymkn36c0r3sdfqg1q78lx8y589fvmdhy964hgy5dqp5rw3nfw")))

