(define-module (crates-io wh oo) #:use-module (crates-io))

(define-public crate-whoo-0.1 (crate (name "whoo") (vers "0.1.0") (hash "101rjwpr0wzxqs8x5rp96jlx65i5rh8mxgq8wz0wdl1ih28v7wf1")))

(define-public crate-whoo-0.1 (crate (name "whoo") (vers "0.1.1") (hash "0n389zpsf7fpaa6rxdz6a6k9iqbbfl040kpmq9kzvyj1pmfmcahg")))

(define-public crate-whoo-0.1 (crate (name "whoo") (vers "0.1.2") (hash "09p6pph7hjn1wqx7mzpj5fz7z9rqb8ly7p4kk24cyg4gb7xf6nh7")))

(define-public crate-whoo-0.1 (crate (name "whoo") (vers "0.1.3") (hash "0aj1043d1m51s1h20wmw1mankhh3fwjrs2jg8p64bh4zprvg1pwd")))

(define-public crate-whoops-0.0.1 (crate (name "whoops") (vers "0.0.1") (hash "0fzqj11khsdk9bh4cix32y9dsjks2rn2sc0wryh31vrdhjakld3n")))

