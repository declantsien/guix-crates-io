(define-module (crates-io wh ee) #:use-module (crates-io))

(define-public crate-wheel-resample-0.1 (crate (name "wheel-resample") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i85y7n58vkcd5mgvkcs8zd3rzm5jv60rzz6lrq2kib8hm9szd7j") (yanked #t)))

(define-public crate-wheel-resample-0.1 (crate (name "wheel-resample") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p7fxmsg7mmw8id0ga7rkfvigq5qrc8ad78j0n8z9jdj92cq48k2") (yanked #t)))

(define-public crate-wheel-resample-0.1 (crate (name "wheel-resample") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2") (default-features #t) (kind 0)))) (hash "09igjrxrqpcmsi16dy1xpg9i8s6s3a402n2f57gqpv6mpcjic4q8")))

(define-public crate-wheel-resample-0.1 (crate (name "wheel-resample") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.2") (default-features #t) (kind 0)))) (hash "18a0bz6kk3z7g5qmbqspy8nwfr8hc3mibhagm8s1x1b28k4j17wb")))

(define-public crate-wheel-timer2-0.1 (crate (name "wheel-timer2") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.6") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("time" "macros" "rt" "net" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-tungstenite") (req "^0.16") (default-features #t) (kind 2)))) (hash "1x3w6ir1nnzyhifyg3zsbmq4gwanx8xn136pzsnnp4azv6zfx0yi")))

(define-public crate-wheel-timer2-0.1 (crate (name "wheel-timer2") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.6") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("time" "macros" "rt" "net" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-tungstenite") (req "^0.16") (default-features #t) (kind 2)))) (hash "0bjfw06jz5610qj5aca4av3ncr54ljcvbiqlmbyz4ylkn59bz3b5")))

(define-public crate-wheel-timer2-0.1 (crate (name "wheel-timer2") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.6") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("time" "macros" "rt" "net" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-tungstenite") (req "^0.16") (default-features #t) (kind 2)))) (hash "029pia7bh7rgwd4jx3di4nrcaxjpnmficxlwpg1qcvc7rpmxzbb5")))

(define-public crate-wheel_timer-0.1 (crate (name "wheel_timer") (vers "0.1.1") (hash "190s942ywvdnklsn9xh82badjakk3kkpg3w6fzmliga26c6n7d6v")))

(define-public crate-wheel_timer-0.2 (crate (name "wheel_timer") (vers "0.2.0") (hash "1f7aaq8mwwhqjgy6h82clk04j81lnp2ammm09wq3qcgkg9q6zgrd")))

(define-public crate-wheel_timer-0.2 (crate (name "wheel_timer") (vers "0.2.1") (hash "11ckdm2asp71qyl5hga5b8zyrvj9j1v9cs5yfhi209ijiss1j902")))

(define-public crate-wheel_timer-0.2 (crate (name "wheel_timer") (vers "0.2.2") (hash "0kikwlwi6ny95lxkpxy3m2zk0mqyj95piysqi21j195lnzzmm2zi")))

(define-public crate-wheel_timer-0.3 (crate (name "wheel_timer") (vers "0.3.0") (hash "0mvlbyk7w2kd3qyf4l6p4kiqjy70si7l34lfkqa7h8vldmbmfas2")))

(define-public crate-wheel_timer-0.3 (crate (name "wheel_timer") (vers "0.3.1") (hash "00vm9cpaggpsq8xprikqqp59mi2hnvpcglkx2drkhz90gxrg12zk")))

(define-public crate-wheelbuf-0.2 (crate (name "wheelbuf") (vers "0.2.0") (hash "0g6dzmn2fwmb4fdmmcwbnrlhxpwxfxxzlywwfnr1q4kakb4mp532")))

(define-public crate-wheelhoss-0.1 (crate (name "wheelhoss") (vers "0.1.2") (deps (list (crate-dep (name "file_diff") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fs3") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1smbqxr00bwnzkv5416r41dpj2va16da4ks529cjh76swnr8aj68")))

(define-public crate-wheelhoss-0.1 (crate (name "wheelhoss") (vers "0.1.3") (deps (list (crate-dep (name "file_diff") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fs3") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0pi61yyir6m0yql38597qm4ahngayy8frzgg31ir4wdiz0sn6ab7")))

(define-public crate-wheelie_bin-0.1 (crate (name "wheelie_bin") (vers "0.1.0") (deps (list (crate-dep (name "naughty_lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "okay_lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04jrvd1i5cx9fmdf3djayxbdrpb1ihbpgqzwsv1k0qp5l2mai118")))

(define-public crate-wheelsticks-1 (crate (name "wheelsticks") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("color" "derive" "error-context" "help" "std" "suggestions" "usage" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.2") (default-features #t) (kind 2)))) (hash "137ybrg278rbdrx6ym4aq1gn2xirmn1d3nk28p9m4q0p7604vjis")))

