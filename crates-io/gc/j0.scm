(define-module (crates-io gc j0) #:use-module (crates-io))

(define-public crate-gcj02-0.1 (crate (name "gcj02") (vers "0.1.0") (deps (list (crate-dep (name "geo") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "15kcg91jxgd38blzd07psw2sypiwij2k3y5ni1gq8afw2lj9cg0l")))

