(define-module (crates-io gc v_) #:use-module (crates-io))

(define-public crate-gcv_spline-0.1 (crate (name "gcv_spline") (vers "0.1.0") (hash "15kmj43n3g3r3bb04p6aah58b1cvfsp0abpnalgp47rxxgglvvhk")))

(define-public crate-gcv_spline-0.2 (crate (name "gcv_spline") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (features (quote ("libm"))) (kind 0)))) (hash "0bbgam5c1ambcpk1qfx96ak3j2yjg51kk4w0nmh183aw8smnjygz")))

