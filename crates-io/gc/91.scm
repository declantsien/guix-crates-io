(define-module (crates-io gc #{91}#) #:use-module (crates-io))

(define-public crate-gc9106-0.9 (crate (name "gc9106") (vers "0.9.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "13akm793ycqb6fllpw7w8rrmi63558qs4ffnjnalq5dzmp8by1yn") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

