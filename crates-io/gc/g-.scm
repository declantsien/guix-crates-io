(define-module (crates-io gc g-) #:use-module (crates-io))

(define-public crate-gcg-parser-0.1 (crate (name "gcg-parser") (vers "0.1.0") (hash "1j93h0c55rv3c4j3sc6h59yx0g8c7aqy0ya349cx7hdl1wl1l5kn")))

(define-public crate-gcg-parser-0.1 (crate (name "gcg-parser") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1s1idl4r61hindmqi3alda95axjxqmdc0p703qmzid9658iw66w3")))

(define-public crate-gcg-parser-0.1 (crate (name "gcg-parser") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1j3ypka85rgah4086p5g1gcszzziahmdqgz2xd7kr314mlr9fc77")))

(define-public crate-gcg-parser-0.1 (crate (name "gcg-parser") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1a33pnqig6lpz6g79c7n9xnzg7sn6r96d9h4wfnbqkg86izgp9vw")))

(define-public crate-gcg-parser-0.1 (crate (name "gcg-parser") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0vz50qwv7bykpfvrnmabbylnmwrllim9k35c2p9r3flkg1y1z017")))

(define-public crate-gcg-parser-0.2 (crate (name "gcg-parser") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0d4i42wi4mdjmhglwmz13n95l8q01fd5v5f83jm9zcjif6znhmq1")))

(define-public crate-gcg-parser-0.3 (crate (name "gcg-parser") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0fdxvsd76xab43zb5fwwcjv8xy96b5fyf82c9a78svj0dl033rqy")))

(define-public crate-gcg-parser-0.4 (crate (name "gcg-parser") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1m3xs0ymch60wgb6k049a4smn9py3nnsffkr20hx994rx7m1yy4w")))

(define-public crate-gcg-parser-0.5 (crate (name "gcg-parser") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "11wprdf0n7363mi23lgyvkg6bh3n043gni9c00xr81wrijm8fcsz")))

(define-public crate-gcg-parser-0.5 (crate (name "gcg-parser") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0qj6c1x3nwr58v8l4zrbva2jqqshbc2xsv8crml315pxab5jch8g")))

