(define-module (crates-io gc co) #:use-module (crates-io))

(define-public crate-gccompat-derive-0.1 (crate (name "gccompat-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0fgpn7zqsgw4fan7k73kd3hq3cis4jnk08f8gn5vh38i7x9722y6")))

(define-public crate-gccompat-derive-0.1 (crate (name "gccompat-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1bidhxdfk8b8m7w01l860s4b9xy19j8qiw4q0ldwn7iagqrkwacm")))

(define-public crate-gccompat-derive-0.1 (crate (name "gccompat-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0x1snvbndi3blfp1g0wi1yi462qccmcc5nk01y19y083yrw7hd49")))

