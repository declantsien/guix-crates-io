(define-module (crates-io gc cj) #:use-module (crates-io))

(define-public crate-gccjit-0.0.1 (crate (name "gccjit") (vers "0.0.1") (deps (list (crate-dep (name "gccjit") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0kysb9hwhqaw8gs72lb9nh0d90nc2cy9acaalzxbp1aaivh5ji9g") (yanked #t)))

(define-public crate-gccjit-0.0.2 (crate (name "gccjit") (vers "0.0.2") (deps (list (crate-dep (name "gccjit_sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "12ibmi493f8g33affm1wkxc4z53w006yzfn96l928d6l5p65zqln")))

(define-public crate-gccjit-1 (crate (name "gccjit") (vers "1.0.0") (deps (list (crate-dep (name "gccjit_sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1p2pkcypjs9nggyywab8qmayn3fijnmn0dq9ym67b5ibx3c49xj1")))

(define-public crate-gccjit-2 (crate (name "gccjit") (vers "2.0.0") (deps (list (crate-dep (name "gccjit_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1khpchi4vc5k2hk8x7d9v4i0a7db95fzgbsg3fcilk6pl8ylrapc") (features (quote (("master" "gccjit_sys/master"))))))

(define-public crate-gccjit_sys-0.0.1 (crate (name "gccjit_sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "14d2fxl9jkdshi8lgcx3sxq3lqb34nn9sxm4ykpd8nkwqblz4y8g")))

(define-public crate-gccjit_sys-0.1 (crate (name "gccjit_sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n271fswii6clh5ffn3wgdk3mpckaqz49ygjc4ka1w85l3xncsj0") (features (quote (("master"))))))

