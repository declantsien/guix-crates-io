(define-module (crates-io gc st) #:use-module (crates-io))

(define-public crate-gcstat-0.0.1 (crate (name "gcstat") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("std" "derive" "color"))) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.38.2") (default-features #t) (kind 0)))) (hash "0q3szlx6bd3gm2qqkq0rd5yhg00wab8qcv4krcy3vp04fm8dw49q") (yanked #t)))

