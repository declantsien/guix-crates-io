(define-module (crates-io gc ma) #:use-module (crates-io))

(define-public crate-gcmap-0.1 (crate (name "gcmap") (vers "0.1.0") (hash "0bnbx0krkb2ndn72izdfqqsi63km1jmxn2lld0ycrhzp0wrfpkbr")))

(define-public crate-gcmap-0.1 (crate (name "gcmap") (vers "0.1.1") (hash "0ivkxql01nllqg1acwnr0ywbf1f1lk9i4by6biac0nvh0vm7y829")))

(define-public crate-gcmap-0.1 (crate (name "gcmap") (vers "0.1.2") (hash "19c3qkc9a9gmlznrk97i1z14r7k5734vwyg5yrlcp8kw1y5cb42z")))

(define-public crate-gcmap-0.1 (crate (name "gcmap") (vers "0.1.3") (hash "0s5g0fijympabh2zxjzb6l5wkqdsayq02vjldyfmplipn6yvi8qg")))

(define-public crate-gcmap-0.1 (crate (name "gcmap") (vers "0.1.4") (hash "0di1gga5a21q8b4bnd4glzp8iqpl3rcvap0csy1pyg33baz5m9h1")))

