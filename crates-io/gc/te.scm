(define-module (crates-io gc te) #:use-module (crates-io))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wjyxqpyzc90aqbz1vsvbzvk4alrsn0m8zmq8qcqi2pk1zsrsxz1")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sqfb8a0m3x3x2avvgcdnyk219pm86s5rfh9392hmwnm5zkfnfv3")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13l0qxn92yfzdbbpmz4hv6nyw883nmmpn930xgws9wxax0yr0z42")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rajrr04vf3pqy477jsialwsd2zjpka0dvfyf07v1hhl9gfmwdl0")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0dkmfq4i43z6xzgdgbzpgh56nl85r3r3ipdmq5nyg500fqcncma2")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "091h3v5r8prwm43fkdwqwcf9zwh81hd6by88sd5w4f5qgbyrjd0m")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "01s9x2asl0fr5wxzm886rpacnws29l9naflzm9dgw4z5vbh6809a")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "07whsy0h95mjrlywayr4m2pxq3kfcpysj1zprjc9s7g8g08gf7mn")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1l8a7dass9sqhbvvip5anfn5r62zgrm4l2d865jgkis47qabz1k4")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.12") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0nyhnpk427ghis7spbzqlwgc4nmc7kca0l18vkf1f9m4r08hahgp")))

(define-public crate-gctex-0.1 (crate (name "gctex") (vers "0.1.13") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "008icq5vparb0qz44zdj6vksagn04d15xamqqv13bcgp46phfsry")))

(define-public crate-gctex-0.2 (crate (name "gctex") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1skwb7zd2y2clpf2qrn0kalsdn3hmcs11wcx8xyrv9blgfvhb828")))

(define-public crate-gctex-0.2 (crate (name "gctex") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1m3f14drsvpqc6qa7gd5nxc5xzf32058m12j3akd03zgnyrrzx7n")))

(define-public crate-gctex-0.2 (crate (name "gctex") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1xab630bl69l0h10xjfs7rpxmcwgn8ga8ixf60ara3hc38d3xhws")))

(define-public crate-gctex-0.3 (crate (name "gctex") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "02dk2pmwx8l8xqnnhnq33vnbbwj60r1rb1d899mnsj5p4cscgb72")))

