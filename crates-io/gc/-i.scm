(define-module (crates-io gc #{-i}#) #:use-module (crates-io))

(define-public crate-gc-image-0.0.1 (crate (name "gc-image") (vers "0.0.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0i0gq6vrhyv32n8qgg3yhv3mbxip0qzwvjl7mmbyffkx2hc3ilhy") (yanked #t)))

(define-public crate-gc-image-0.0.2 (crate (name "gc-image") (vers "0.0.2") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0gjrb9rir46cn3540ngljsdc0xphc1crbgamrjkkzyqwz2ynnfl5")))

