(define-module (crates-io gc j-) #:use-module (crates-io))

(define-public crate-gcj-helper-0.1 (crate (name "gcj-helper") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0xndzinn08bsjcfblj8rmrgf16xj1w2x4kwfi26ic9vm9m3dzmrx") (yanked #t)))

(define-public crate-gcj-helper-0.1 (crate (name "gcj-helper") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0p100zszwxys5jflrga9vhk0nib26bvz07vnasv4n9xdywj7lqp9")))

(define-public crate-gcj-helper-0.2 (crate (name "gcj-helper") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1xrbdinn0vzlnmnv41ylb643q3ja4si1gcqqzvxlbpl53vdmvf1v")))

(define-public crate-gcj-helper-0.3 (crate (name "gcj-helper") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "013gz00fs0lgd3ynr94rfnm050la3kyr9bjybdhw4frhgzwia8pp") (features (quote (("parallel" "rayon") ("default"))))))

(define-public crate-gcj-helper-0.4 (crate (name "gcj-helper") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1r7d5ivr4l6314xc0n3bnszwkmjn5n2zqaxd55vd747sskbr7vay") (features (quote (("parallel" "rayon") ("default"))))))

(define-public crate-gcj-helper-0.5 (crate (name "gcj-helper") (vers "0.5.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "132mmbgv1zi2c4bj2x41rxxvpnyimkzfzd1r548c502x4w9128bs") (features (quote (("parallel" "rayon") ("default"))))))

