(define-module (crates-io gc ol) #:use-module (crates-io))

(define-public crate-gcollections-0.1 (crate (name "gcollections") (vers "0.1.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1xj6vxwlall80xfb48xibk14isg402nd2pgircdfbv368ibj7y43")))

(define-public crate-gcollections-0.2 (crate (name "gcollections") (vers "0.2.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1578kjz6qa81idbrmz915446wng8146l468whys2cxsy6k3f0nvy")))

(define-public crate-gcollections-0.2 (crate (name "gcollections") (vers "0.2.1") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "0hnnbqlndwpzl84dwfy2l50xs9xbc415zw4va1df7i84m6lxkfdr")))

(define-public crate-gcollections-0.2 (crate (name "gcollections") (vers "0.2.2") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "0gbbdmr7na5dxhk0bk2crh01w1y9bkcqk74wiw6w1lp2i13dpbhk")))

(define-public crate-gcollections-0.2 (crate (name "gcollections") (vers "0.2.3") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "19bcblx6c3nqg946p5iqy39b8h701k1fiv9d8b21pgyq3ja1lwjr")))

(define-public crate-gcollections-0.2 (crate (name "gcollections") (vers "0.2.4") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "08sgq4dpfs5v51b7fb3g1qsc0hl7giqfg8q6mj6l68y49w6kbx4f")))

(define-public crate-gcollections-0.2 (crate (name "gcollections") (vers "0.2.5") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1fks0d8gxqawvcbhzl0hp52402l42xxpd9jrrnhcfp1rqinxicsa")))

(define-public crate-gcollections-0.3 (crate (name "gcollections") (vers "0.3.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "0dpak2dx9pv8j8zwdvfsblsg97pcf9fhki38mp499478gvpkxn5r")))

(define-public crate-gcollections-0.3 (crate (name "gcollections") (vers "0.3.1") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1vlyab4gv5bhq0dky5prakxp5q7b6sv909305g0l1jiy4acfynmp")))

(define-public crate-gcollections-0.3 (crate (name "gcollections") (vers "0.3.2") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "0vp9s8cx7v1w2f8dm8yg0w7811z4j39lfwp01blvd89vwb22gxs6")))

(define-public crate-gcollections-0.3 (crate (name "gcollections") (vers "0.3.3") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "0gsrxsbypk2ki1jf6jxn7xgqqs2xbqxhjy3s863bcp0ck8jmcmyy")))

(define-public crate-gcollections-0.3 (crate (name "gcollections") (vers "0.3.4") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1ddjq2ffjvlz759353y7sg4bwzphi20mvpx5zgvlcinx776wayyg")))

(define-public crate-gcollections-0.3 (crate (name "gcollections") (vers "0.3.5") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)))) (hash "1p4x71kxs4hqyb71df8pgnwbd6znsiyi577fw03q6zfarzp17bn9")))

(define-public crate-gcollections-1 (crate (name "gcollections") (vers "1.0.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "trilean") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ipxj3xbl3r69f5y9pay8zp4b82rcdik8q1va5vwcpgqfrhkjs95") (features (quote (("nightly"))))))

(define-public crate-gcollections-1 (crate (name "gcollections") (vers "1.1.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "trilean") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0l0ywrsdxlwp0n55h6s0ydsspz13zywn95j8dd9p4xzrkzfza1zn") (features (quote (("nightly"))))))

(define-public crate-gcollections-1 (crate (name "gcollections") (vers "1.1.1") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "trilean") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "05d42l8mwd8286hs0rqjjiwyczdgwhlaf8wiga8n65krrrfy1a2d") (features (quote (("nightly"))))))

(define-public crate-gcollections-1 (crate (name "gcollections") (vers "1.2.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "trilean") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0qvn5h1hnw5x2i4qwy9q0rrbg5hcfbghi7n6pw6yn8ss0b1kpbnm") (features (quote (("nightly"))))))

(define-public crate-gcollections-1 (crate (name "gcollections") (vers "1.3.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "trilean") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1qhkrsw1j84s27pjwmn738z8xvinzjx57j8j9v557b93l46plwan") (features (quote (("nightly"))))))

(define-public crate-gcollections-1 (crate (name "gcollections") (vers "1.4.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.29") (default-features #t) (kind 0)) (crate-dep (name "trilean") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1c8k4a8yqjwlxpcc5pnh5zkpqxgh09ksf8sw0jsh85f6jgzx8g1g") (features (quote (("nightly"))))))

(define-public crate-gcollections-1 (crate (name "gcollections") (vers "1.5.0") (deps (list (crate-dep (name "bit-set") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.44") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "trilean") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0xs30iidvavg7gcrqmp3ryv6f7578y8nc6a9fngk507g4ggiym9g") (features (quote (("nightly"))))))

