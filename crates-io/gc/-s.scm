(define-module (crates-io gc -s) #:use-module (crates-io))

(define-public crate-gc-sequence-0.1 (crate (name "gc-sequence") (vers "0.1.0") (deps (list (crate-dep (name "gc-arena") (req "^0.1") (default-features #t) (kind 0)))) (hash "02wz0frbzj23s0q41innys07qifaq9jsn7yssd308gk2wsaf2k4y")))

(define-public crate-gc-sequence-0.1 (crate (name "gc-sequence") (vers "0.1.1") (deps (list (crate-dep (name "gc-arena") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dgk58vy8c679ffjlvgai61w0vr6cmdm5a2vw7im3ria9j5bcmdb")))

(define-public crate-gc-sequence-0.2 (crate (name "gc-sequence") (vers "0.2.0") (deps (list (crate-dep (name "gc-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qhq3gmmcd8j84lyr566asl4nr3pm49h52sc1bxrgmy7wjdbhx8n")))

(define-public crate-gc-sequence-0.2 (crate (name "gc-sequence") (vers "0.2.1") (deps (list (crate-dep (name "gc-arena") (req "^0.2") (default-features #t) (kind 0)))) (hash "0apx9spj1skq4bwh5fvb3h1gkxrf9ng3adlmcvm3akpaaqyll4b8") (features (quote (("std") ("default" "std"))))))

(define-public crate-gc-sequence-0.2 (crate (name "gc-sequence") (vers "0.2.2") (deps (list (crate-dep (name "gc-arena") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0kwl9dbihwisv18fg5byw1rjqgrjwpdg3wjz6hrlh8gays3cvr6r") (features (quote (("std") ("default" "std"))))))

(define-public crate-gc-shadowstack-0.1 (crate (name "gc-shadowstack") (vers "0.1.0") (deps (list (crate-dep (name "mopa") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0li0daqiadj3gp24c4zlhipgnx91ynbw17nqp9ln666lf5igzasr")))

(define-public crate-gc-shadowstack-1 (crate (name "gc-shadowstack") (vers "1.0.0") (deps (list (crate-dep (name "mopa") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1jrqrl30anq6adlbn27v1qkw1f3n5kcf75yg1ws1rgg0dlhcms70")))

(define-public crate-gc-shadowstack-1 (crate (name "gc-shadowstack") (vers "1.1.0") (deps (list (crate-dep (name "mopa") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1dm3lnby2hb7hycv5bckmj5w30xxzgc871wbm79hz1y569sray7z") (yanked #t)))

(define-public crate-gc-shadowstack-1 (crate (name "gc-shadowstack") (vers "1.1.1") (deps (list (crate-dep (name "mopa") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1w7ar5l3p07r853kg51n3w8pg2ib5799k5l78h65324f3cmj16ya")))

