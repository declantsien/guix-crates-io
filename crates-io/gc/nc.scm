(define-module (crates-io gc nc) #:use-module (crates-io))

(define-public crate-gcnctrlusb-0.1 (crate (name "gcnctrlusb") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "libusb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1nphwkm8gf3bnm1kxxn8cyq0lpmla6yk469626vz663q8x9r6l5p")))

