(define-module (crates-io gc mo) #:use-module (crates-io))

(define-public crate-gcmodule-0.1 (crate (name "gcmodule") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1ba4vlq5p09prvfmzd0pvpwrh4qm7v6l5d9kcl3m2gz8ycqh9dy2") (yanked #t)))

(define-public crate-gcmodule-0.1 (crate (name "gcmodule") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1qvnpkgzdvhx3bslxm7kx4b8acy353h4xynrb11blrhyjdm3llli") (yanked #t)))

(define-public crate-gcmodule-0.1 (crate (name "gcmodule") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "07xhazjpchyj25dgk68kfgmrgjb0s5mvm1x6nkmlv26v7nvkphif") (features (quote (("nightly"))))))

(define-public crate-gcmodule-0.2 (crate (name "gcmodule") (vers "0.2.1") (deps (list (crate-dep (name "gcmodule_derive") (req "= 0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "1xn6d4fhwl9lkrzf80ms2wh50ch209j8ch6lnx2p26y4nqlj8zll") (features (quote (("testutil") ("nightly") ("derive" "gcmodule_derive") ("default" "derive"))))))

(define-public crate-gcmodule-0.2 (crate (name "gcmodule") (vers "0.2.2") (deps (list (crate-dep (name "gcmodule_derive") (req "= 0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "1m7pr4wk2zymm4z5gnfg6xjhvnxk1rp9payxw0qp68w5s3qwrky2") (features (quote (("testutil") ("nightly") ("derive" "gcmodule_derive") ("default" "derive"))))))

(define-public crate-gcmodule-0.2 (crate (name "gcmodule") (vers "0.2.3") (deps (list (crate-dep (name "gcmodule_derive") (req "= 0.2.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "1h1rk8pipwqx74zcnbiyqr4vvgxyhyhx43mylj67n42dsr0c50ij") (features (quote (("testutil") ("nightly") ("derive" "gcmodule_derive") ("default" "derive"))))))

(define-public crate-gcmodule-0.3 (crate (name "gcmodule") (vers "0.3.0") (deps (list (crate-dep (name "gcmodule_derive") (req "= 0.2.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "0zvdgh32qpz07sl60slyzcz4gnqf8z0kcrqg8r01965min7bw0v7") (features (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync"))))))

(define-public crate-gcmodule-0.3 (crate (name "gcmodule") (vers "0.3.1") (deps (list (crate-dep (name "gcmodule_derive") (req "= 0.2.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "06p5vyly0b4qmlzciqgnf5644gzymlnj73pz0dkffy37rlfw13yk") (features (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync"))))))

(define-public crate-gcmodule-0.3 (crate (name "gcmodule") (vers "0.3.2") (deps (list (crate-dep (name "gcmodule_derive") (req "= 0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "0f8fiizv92hhwp4drrhfj814djxsa6c7q1xdnnn2l10zhz6b5p32") (features (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync"))))))

(define-public crate-gcmodule-0.3 (crate (name "gcmodule") (vers "0.3.3") (deps (list (crate-dep (name "gcmodule_derive") (req "= 0.3.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "09gdn29n3k2gz3la6nzrwhzqs2m26r4wf7s09bs28wa33agkx95f") (features (quote (("testutil") ("sync" "parking_lot") ("nightly") ("derive" "gcmodule_derive") ("default" "derive" "sync") ("debug"))))))

(define-public crate-gcmodule_derive-0.2 (crate (name "gcmodule_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1p6k223snbixm1whii4zrggbyi866pwx103c8navm0in8s1nxyw1")))

(define-public crate-gcmodule_derive-0.2 (crate (name "gcmodule_derive") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17x1a08w2hqz7z92x1sl0hynlvpkkzf6lhdw8qpnw190nfscrsr0")))

(define-public crate-gcmodule_derive-0.2 (crate (name "gcmodule_derive") (vers "0.2.3") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zi5ah05jhcmshx59mvjn6whs3c4bnpba047bxiyqs7ycwjwki79")))

(define-public crate-gcmodule_derive-0.3 (crate (name "gcmodule_derive") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yqdvwx4smziyf9mam8b806rflv9818xihlmkjj5ric2wxpr0xcj")))

(define-public crate-gcmodule_derive-0.3 (crate (name "gcmodule_derive") (vers "0.3.3") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12bhq08ywp332gccb6rf6z9bmbi869fx4jqxsixd2qcbm7p3n8mq")))

