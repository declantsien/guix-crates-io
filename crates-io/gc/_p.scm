(define-module (crates-io gc _p) #:use-module (crates-io))

(define-public crate-gc_plugin-0.0.1 (crate (name "gc_plugin") (vers "0.0.1") (hash "178mk7pmyh535ij829ivxl3vpqx29qv8wc8jijvaivbvl3a3fwyp")))

(define-public crate-gc_plugin-0.0.3 (crate (name "gc_plugin") (vers "0.0.3") (hash "10l4m3l37lgwccgiv4xf878rcfysrwwxccdnq2ws55bq112jy1dc")))

(define-public crate-gc_plugin-0.0.4 (crate (name "gc_plugin") (vers "0.0.4") (hash "0ahy0disvxjjsblmqafxrhf1hpm0mhxj8sia4zxgv1cg3dhfgn41")))

(define-public crate-gc_plugin-0.0.5 (crate (name "gc_plugin") (vers "0.0.5") (hash "08hki60gqlsr4v0pjbdi9nlch6qvk360zzinsqr2v8xxq4ax9s0g")))

(define-public crate-gc_plugin-0.0.6 (crate (name "gc_plugin") (vers "0.0.6") (hash "1aspga1fp98sqhnql9m1684w1pnss10fsrjwy3jd37jzcy2s2a90")))

(define-public crate-gc_plugin-0.0.7 (crate (name "gc_plugin") (vers "0.0.7") (hash "025hmqs9jqh9v4bs2yp8h7c13bznjkm62jli1hwdzlaqcbzyskhn")))

(define-public crate-gc_plugin-0.0.8 (crate (name "gc_plugin") (vers "0.0.8") (hash "0asbkxda714nzwryh5bdlhpm00f7wpbpxm2sffyr0627c8m88yq5")))

(define-public crate-gc_plugin-0.1 (crate (name "gc_plugin") (vers "0.1.0") (hash "00hmrlg9l6gfln3cnydga2fc45jfnmzlx3cv2vv4ji3bzs4q73sh")))

(define-public crate-gc_plugin-0.0.9 (crate (name "gc_plugin") (vers "0.0.9") (hash "0g2vc793czag23lmr7zz8xrxpiw43i9p8x4wai5nnjhpsxwd8w1l")))

(define-public crate-gc_plugin-0.1 (crate (name "gc_plugin") (vers "0.1.1") (hash "0byvhyf37ds05jjw8vchjcxh1adb6dm774vv03rf9z31cmmk40rf")))

