(define-module (crates-io gc _d) #:use-module (crates-io))

(define-public crate-gc_derive-0.2 (crate (name "gc_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cvf54hvfr2yvplhn1im85hidi2xksiiysqw54q9jnm8cp5chypx")))

(define-public crate-gc_derive-0.2 (crate (name "gc_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.4") (default-features #t) (kind 0)))) (hash "011fch698q5l37lygmp7ln9m7pir22j5sqbsai1mc0hpxh2r77hg")))

(define-public crate-gc_derive-0.3 (crate (name "gc_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.4") (default-features #t) (kind 0)))) (hash "19syhpy3i9nvnlhdsdxkbxidj4x0ia09qacj71wcpx8359fwbf23")))

(define-public crate-gc_derive-0.3 (crate (name "gc_derive") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.5") (default-features #t) (kind 0)))) (hash "1xlkc3jbyb988qdc8dyk0lpxg2yi7mc1i6nvm9kkm9cphxxksljd")))

(define-public crate-gc_derive-0.3 (crate (name "gc_derive") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.6") (default-features #t) (kind 0)))) (hash "19hf9qv5bj3irpk7z4ypc4qr6alqa59shyk12hi0r2pjp9fc2095")))

(define-public crate-gc_derive-0.3 (crate (name "gc_derive") (vers "0.3.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "04wkqgj2w9g6v6mlfs38sq1gbjw9fvjjv9bi4spik8a4h269cnva")))

(define-public crate-gc_derive-0.3 (crate (name "gc_derive") (vers "0.3.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "1f39r2zhdrlwxvclciy76xpkn2984iba959nsb46xz5j8k3zqkq3")))

(define-public crate-gc_derive-0.3 (crate (name "gc_derive") (vers "0.3.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "0lb1x02jh8bxi84pi3nsysjcj271qvbkrj221v3zqlqx9nwk9isj")))

(define-public crate-gc_derive-0.4 (crate (name "gc_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "0p31d81xnq0pwn99wm1cvvq7p8kqvrfl4g3g6xq784iz085vpvv5")))

(define-public crate-gc_derive-0.4 (crate (name "gc_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "0hdr1m8f8mk2n2msm3d9c4ikr36qny7fg00xcf2pizwly1289pv0")))

(define-public crate-gc_derive-0.5 (crate (name "gc_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "07pnbg2bkqr8b3apwzy11gy395ham38vlr08yq32i2v5d46iwvzv")))

