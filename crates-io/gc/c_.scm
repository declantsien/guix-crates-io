(define-module (crates-io gc c_) #:use-module (crates-io))

(define-public crate-gcc_lgr_auth_service-0.1 (crate (name "gcc_lgr_auth_service") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0mwv4ia27vy29djz1d74m3hcls0aa2f7is1cyxzrv5h0l5bp38gx")))

