(define-module (crates-io gc nd) #:use-module (crates-io))

(define-public crate-gcnd-0.0.1 (crate (name "gcnd") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.9.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "14qzk54c79vdgdmkiaxh792g01xvx1jhj3jc9pxlskp9qr5kxbgg")))

