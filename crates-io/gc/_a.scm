(define-module (crates-io gc _a) #:use-module (crates-io))

(define-public crate-gc_api-0.1 (crate (name "gc_api") (vers "0.1.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fvpp10ympigcqjlfbs6blkq3fwwpy1badsnbbpy9s0mz2x32kip")))

(define-public crate-gc_api-0.3 (crate (name "gc_api") (vers "0.3.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (default-features #t) (kind 0)))) (hash "0hxp5lny4p8xz9mqfh7md9wp217vrhs3xzlsj70k4msy2y45fc3y")))

(define-public crate-gc_api-0.4 (crate (name "gc_api") (vers "0.4.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1fkl3dc6c0qba4ancip2whxfjsd33ncpp9zqf89gzq1pa1321qap")))

(define-public crate-gc_api-0.5 (crate (name "gc_api") (vers "0.5.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0my76fy2irz2r6i0rqclgp61p49qp5bp07svamxblr5l4ik5d36a")))

