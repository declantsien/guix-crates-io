(define-module (crates-io gc dx) #:use-module (crates-io))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.0") (hash "08s1amr9kcjl11z7im9m6y464cr9agjwhgiy86va7k0hkv3cgf7w")))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.1") (hash "07zn90jd53sd79s6jcwvgyx17jf9q1brvwalgw4kcglya3ndhw8r")))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.3") (hash "041dh7c7j4s6ck5d5nrxyr6abjx4mq1mamnszifqm18wdzckidwk")))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.4") (hash "1686id4kcw0zp19d77lhln1mdryj7dlpp7j5if3dlyzshhk2wkya")))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.5") (hash "13l3ph4lb4g4v6gi8il6a456a0cg9n60gq4q9r7g0plwhdk47njv")))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.6") (hash "0zh2lfpldlsxd1kqpvfc1wm212fq5cx5rkid723y9acd2fhzardk")))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.7") (hash "035akxzhi9pw1bxwjn87lh5r2rqb7p91p2wnzx5b163i1l0jifxk")))

(define-public crate-gcdx-0.1 (crate (name "gcdx") (vers "0.1.8") (hash "1gi28mc1svlhxw1fqhvqjw7qay032k6z1a5kdsak4856glds4kf4")))

