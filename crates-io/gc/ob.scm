(define-module (crates-io gc ob) #:use-module (crates-io))

(define-public crate-gcob-0.1 (crate (name "gcob") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0p1qxr7vm1n12rafr9pdn5086lxz3bhzd8mhlwlgvbnza9sw2m6q")))

(define-public crate-gcob-0.2 (crate (name "gcob") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "04g34mv3rlw9nl2w5l68k245qs56mwyrhxgwjwxv7hlnza0gd5dc")))

