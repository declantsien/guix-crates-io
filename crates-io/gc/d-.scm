(define-module (crates-io gc d-) #:use-module (crates-io))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.0") (hash "0nwq7kl5jl6smb0xnw640nbpcpjbqxavsnw33qi9gglwdrbv8h4w")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.1") (hash "14fhdl4lr9i8iy6hd62f2bzkd85h8rw88jhlcjir0g1klkjz2hna")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.2") (hash "13asqnp7mdrwvgdyzh5xskx8q5ni6n2vkkhq06iqbj1la8jab55v")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.3") (hash "1shq620qb1prf4a36hz86q16khs9cz04qx3vwm36jg17rlcgyd89")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.4") (hash "0y5aq2c3vldyg5gw842d2wsj41ysklbz8d8xmvp2kxa08prg20ak")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.5") (hash "1043vnam6aw6qn1qfkcjjvyvd9kn5ag26zns0plqia7jsr6j4hqy")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.6") (hash "1mgwvdjlfylam3vay9hxb8bypsvsmwl3f7lyxy12kdwa3mai16h3")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.8") (hash "1h1ykyll4bb8gnnlfv6i9991sa2zxbp8rsxn01akyfw6d128nygf")))

(define-public crate-gcd-bitwise-0.1 (crate (name "gcd-bitwise") (vers "0.1.9") (hash "0q8pih6byr7wadzy60wn5c5pxzcngbw2nk6a87rs5kdlxdfqdlf9")))

(define-public crate-gcd-bitwise-0.2 (crate (name "gcd-bitwise") (vers "0.2.0") (hash "0yx126chwxr446ynkw649rnkm459ikpvw8i4bgx14400413hy4b2")))

(define-public crate-gcd-bitwise-0.3 (crate (name "gcd-bitwise") (vers "0.3.0") (hash "1kpfmmpzqjy108lm33x0clzxg75mbshvw844bs7b527cb0h6wffk")))

(define-public crate-gcd-cli-0.0.1 (crate (name "gcd-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.21.0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0dwplfarpw91fc3rvqplg2fx2r822xkm2ibwracap409h516qnk5")))

(define-public crate-gcd-cli-0.2 (crate (name "gcd-cli") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req ">=2.33.3, <3.0.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req ">=0.13.0, <0.14.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req ">=3.1.7, <4.0.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req ">=0.3.0, <0.4.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req ">=0.15.0, <0.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req ">=1.4.2, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req ">=0.24.1, <0.25.0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "1sgprbfvyvz20sa9jfgkr8m3cfc5n0nw3qqyr911938pass2jy8s")))

(define-public crate-gcd-cli-1 (crate (name "gcd-cli") (vers "1.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.7") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.0.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25.3") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0yv11hdsln9v1b5qrbmkf49bgb4hyr49razkwxxda2b168x5hv6r")))

(define-public crate-gcd-rs-0.1 (crate (name "gcd-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "07d91ngl17h0nhrmpadlm2kmprrk15q02kaz1a4ricmkalhsdrpb")))

(define-public crate-gcd-rs-0.1 (crate (name "gcd-rs") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "1bq881ay2lcb8mamrlv4ks0fdh37fp2q85xph1pvad62hyr0b45y")))

(define-public crate-gcd-rs-0.1 (crate (name "gcd-rs") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 2)))) (hash "08p6mk42y0gg2ydzsjb0nlfkp12l0zq4mbzcirpggravy5v80lxd")))

