(define-module (crates-io ve da) #:use-module (crates-io))

(define-public crate-veda-0.1 (crate (name "veda") (vers "0.1.0") (hash "0yiv73956xaajyvb006d63xp88wm661dwpi0rvm76cgi3v8jbi7a")))

(define-public crate-vedasynth-0.0.1 (crate (name "vedasynth") (vers "0.0.1") (deps (list (crate-dep (name "cpal") (req "^0.15") (features (quote ("jack"))) (default-features #t) (kind 0)))) (hash "0202mh83zkas7jgrhbn4gyx476lwwkr9afiqxbba8kfb7qaqxcad")))

(define-public crate-vedasynth-0.0.2 (crate (name "vedasynth") (vers "0.0.2") (deps (list (crate-dep (name "cpal") (req "^0.15") (features (quote ("jack"))) (default-features #t) (kind 0)))) (hash "1zr1mb0mg237kaic18wd9n7280zzq2pv5faympk4kxmz1siday59")))

