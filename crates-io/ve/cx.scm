(define-module (crates-io ve cx) #:use-module (crates-io))

(define-public crate-vecxd-0.1 (crate (name "vecxd") (vers "0.1.0") (hash "1s6dqm8b8bkwfvrkky7ggg6sz67ynxvhjlycp8ps4kp34wxqpp3x")))

(define-public crate-vecxd-0.1 (crate (name "vecxd") (vers "0.1.1") (hash "07dnfp8idn8if3mccx21iyb8w8nlw6s7i5qdj8r49zijw91523xq")))

(define-public crate-vecxd-0.1 (crate (name "vecxd") (vers "0.1.2") (hash "1q31982gdlz5xda90l2nm75krndi6rmdzlzw5d9xx0arc98517n6")))

