(define-module (crates-io ve dv) #:use-module (crates-io))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1af0irlsfs1fyaak9d6ypi3n6vkaax6d517pfjz3wngwcfcbjjy4")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "08b8piy6s7gczdkkpsmffm5hwsf35rlzwi14pjch66d9h552lc42")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1i26bqip3scvgymjp94ysn409vcylj1gd0n7r8r3prpdcbnlgisv")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1xpp6rh20f7435s8lc874kqnrcgzl6jb3dpxswwqw0z1fbakvyjl")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.5") (deps (list (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1sakv62l8zz6a0qpaf6gxcmzkjhva7ll1c03dhvs0d2bha97xspc")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.6") (deps (list (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1r2834aabvrla8h767274gb53wy366fpqf88ga5bgnld4y194aas")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.7") (deps (list (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "12d2jcxk8myzy3jz3g7cyba7dd24j82lhzdxr8wnvgsc77q36s6k")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.8") (deps (list (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "17s41vsxm09hsmqw3xn9fykqqd9zhnvv9a45rwz174bxviy3ijsg")))

(define-public crate-vedvaring-0.1 (crate (name "vedvaring") (vers "0.1.9") (deps (list (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "06pmmibs4kifj4r6yxglhis7l4rljwkw9m8c16mihn9m5agvdnl5")))

