(define-module (crates-io ve -o) #:use-module (crates-io))

(define-public crate-ve-orn-bindings-0.0.1 (crate (name "ve-orn-bindings") (vers "0.0.1") (deps (list (crate-dep (name "ethers") (req "^1.0.2") (features (quote ("abigen"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1gi8fl2b0vd7r44ichf6z5wnl2cri60z6yk4xxn03ac69253gvxc")))

(define-public crate-ve-orn-bindings-0.0.2 (crate (name "ve-orn-bindings") (vers "0.0.2") (deps (list (crate-dep (name "ethers") (req "^1.0.2") (features (quote ("abigen"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0692vjvwqrvw1fxbdjgixx8pszfvbsw0zyj0l7z56zgm9kpyjb4w")))

