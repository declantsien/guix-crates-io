(define-module (crates-io ve sy) #:use-module (crates-io))

(define-public crate-vesync-0.1 (crate (name "vesync") (vers "0.1.0") (deps (list (crate-dep (name "attohttpc") (req "^0.14.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0wvdn25y31r4c2s43avghpcw0nissdsg7w9mrgjk2cbmj32qhkgh")))

