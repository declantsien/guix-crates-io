(define-module (crates-io ve rk) #:use-module (crates-io))

(define-public crate-verkehr-0.1 (crate (name "verkehr") (vers "0.1.0") (hash "1p0rp9z03cwl03193y75cdlgbw4xyfypzxmnhmph85ai8dgawjnn")))

(define-public crate-verkle-0.1 (crate (name "verkle") (vers "0.1.0") (deps (list (crate-dep (name "ark-ec") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ark-poly-commit") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "02iikvqzyd03ls1dck8zcp1jgnq4q7yd3y4b1wx01np4pxq4rg44")))

(define-public crate-verkletree-0.1 (crate (name "verkletree") (vers "0.1.0") (hash "06z30fjlqhqv5kixg238pyvyx83yzplfqmvm17rlxhygn3cqp51v")))

(define-public crate-verkletree-0.1 (crate (name "verkletree") (vers "0.1.1") (hash "18q1g077cpmcwfgrdvvm2ilv889akdzzdff31cx2i9hisdxvsjn3")))

