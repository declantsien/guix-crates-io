(define-module (crates-io ve mi) #:use-module (crates-io))

(define-public crate-vemigrate-0.1 (crate (name "vemigrate") (vers "0.1.0") (hash "18npcdy1rc0s50zdrcvv1ry1nggy60c5s2p8q5m6vhpxn9xxsh8v")))

(define-public crate-vemigrate-0.1 (crate (name "vemigrate") (vers "0.1.1") (hash "0sav2yymyb2p95izhnd740f6457pxwwj64dkvm1r105y7ach6pdx")))

(define-public crate-vemigrate-0.2 (crate (name "vemigrate") (vers "0.2.0") (hash "1fm2ldns3p6chrl3krjyvcxwi04ng6wwf6mr2czxrvwq99ppa305")))

(define-public crate-vemigrate-0.3 (crate (name "vemigrate") (vers "0.3.0") (hash "13fd2r124dbcwzh745vh9s9c3s995xizq8r0n7ks53yzcbz1id4k")))

(define-public crate-vemigrate-0.3 (crate (name "vemigrate") (vers "0.3.1") (hash "1r1dyqsb0wg7qrnyyvnrp9a7rh2pibmrv321gbymnmpx6pawkyy2")))

(define-public crate-vemigrate-cli-0.3 (crate (name "vemigrate-cli") (vers "0.3.0") (deps (list (crate-dep (name "cdrs") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "cdrs_helpers_derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vemigrate") (req "^0.3") (default-features #t) (kind 0)))) (hash "14sdd78qvprhh43d4xpyj5pckhg75gp2p1yf2pm4laz45bkaii2h")))

(define-public crate-vemigrate-cli-0.3 (crate (name "vemigrate-cli") (vers "0.3.1") (deps (list (crate-dep (name "cdrs") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "cdrs_helpers_derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vemigrate") (req "^0.3") (default-features #t) (kind 0)))) (hash "16hdpkvf9jcsx29a3q7j7rvdpd3mx3in1acxcihyp63hkmaf1xia")))

