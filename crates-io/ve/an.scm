(define-module (crates-io ve an) #:use-module (crates-io))

(define-public crate-veandco_logger-0.1 (crate (name "veandco_logger") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fpiabpbp6xg7y3r1vhd51q1v75hjiz26mm0qi2hsqsh2ps39d0n")))

(define-public crate-veandco_logger-0.2 (crate (name "veandco_logger") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0x77wv1k6qjaybl27v43lvq158ink0ddqhddwb4dfk3x2aqvysng")))

