(define-module (crates-io ve r-) #:use-module (crates-io))

(define-public crate-ver-cmp-0.1 (crate (name "ver-cmp") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yqv07d01w66a52mqi1s81xxak655dd4gyg1m05w7rn0av5zfmjq")))

(define-public crate-ver-cmp-0.1 (crate (name "ver-cmp") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mmjqsxk3w8lcds8gpnd3aa9g11xxmidn8z858pq74iqw4njc3hy") (features (quote (("build-binary" "clap"))))))

(define-public crate-ver-cmp-0.1 (crate (name "ver-cmp") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xnh3jyp98nnm0d75n74h49x0w26i3xscs5rb7qncraczl16m8zz") (features (quote (("build-binary" "clap"))))))

(define-public crate-ver-cmp-0.1 (crate (name "ver-cmp") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1vspviwvpss0kdxmg5mvdf5hyj855df1pg96jm1clrx3dcfd3aqh") (features (quote (("build-binary" "clap"))))))

