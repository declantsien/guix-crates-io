(define-module (crates-io ve kt) #:use-module (crates-io))

(define-public crate-vektor-0.0.0 (crate (name "vektor") (vers "0.0.0") (hash "1fyfnzgii81m9cp2p52yhmd8r7w64byvinsrhzxsvfdglvic5wq0")))

(define-public crate-vektor-0.2 (crate (name "vektor") (vers "0.2.0") (hash "1kdnc1cw0jq26y9w0aqn286ddfa9bxdqpa3a3lzm0fnzja01f0hw")))

(define-public crate-vektor-0.2 (crate (name "vektor") (vers "0.2.1") (deps (list (crate-dep (name "packed_simd") (req "^0.3") (default-features #t) (kind 0)))) (hash "1nikx62d4sqqsn7f21yp6a3dk6658wpdvrnv43bpspddx6z6zs22")))

(define-public crate-vektor-0.2 (crate (name "vektor") (vers "0.2.2") (deps (list (crate-dep (name "packed_simd") (req "^0.3.4") (default-features #t) (kind 0) (package "packed_simd_2")))) (hash "1c0x3m6riz7qw3qy57xdwpaii44hqc3jzc7snckmxwbs1qirfv6y")))

