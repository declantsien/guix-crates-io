(define-module (crates-io ve ci) #:use-module (crates-io))

(define-public crate-vecio-0.1 (crate (name "vecio") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ws2_32-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "097xn9dvjgz6bwg10d4xxf7lylvvpwas7zd34l2yi6njfqas3587")))

(define-public crate-veciter-0.1 (crate (name "veciter") (vers "0.1.0") (hash "03ggp17qkkc6gz9j162c3782v8dirsca7g2k74pyzl8cw2in4407")))

