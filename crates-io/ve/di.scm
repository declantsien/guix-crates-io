(define-module (crates-io ve di) #:use-module (crates-io))

(define-public crate-vedirect-0.1 (crate (name "vedirect") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i44zg9b6r50rd65ng55h6i9126972xzqvhdz6js7jnfqmsy7yfp")))

(define-public crate-vedirect-0.2 (crate (name "vedirect") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^4.1") (kind 2)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m0bxxwjzqdc1nj91sjy2bmwm1yzibqa63cxmqwg5al44xvlg08s")))

