(define-module (crates-io ve c4) #:use-module (crates-io))

(define-public crate-vec4-0.1 (crate (name "vec4") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0anfwmmxpj8pinmlzwq6wfh9qvy08fkvjl3mvyfcg93yxrd9sgsh")))

(define-public crate-vec4-0.1 (crate (name "vec4") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "17h0hl361jw8hdk52prnp711gsm4k61bn5vzslji5wg1dj6534c5")))

(define-public crate-vec4-0.1 (crate (name "vec4") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dph6ikkzdfgmpbi43mb52var6f3mvgvvpsnxhvlp3sl99hwkx49")))

(define-public crate-vec4-0.1 (crate (name "vec4") (vers "0.1.3") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0a6vqpbjga9qwcwp2j3plww834dmccspimdgmpy94xam0igfgmjc")))

(define-public crate-vec4-0.1 (crate (name "vec4") (vers "0.1.4") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cdyz7hn4qx8c8sarkrs4vi0yc2slf6pmavnzf2w8slx9y75pzps")))

(define-public crate-vec4-0.1 (crate (name "vec4") (vers "0.1.5") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lx0va2qjiqn44jiv856wqgyzpa74a8a7nw9wzhz3p7sny477xhb")))

(define-public crate-vec4-0.2 (crate (name "vec4") (vers "0.2.0") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.2") (default-features #t) (kind 0)))) (hash "03gji7c4sqakrq9sm94dzz22zf3kxjd9ygsvc74g12f932kdpiql")))

(define-public crate-vec4-0.2 (crate (name "vec4") (vers "0.2.1") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fh6j1zsxss41gmvpfqhvhgr5x32nmqjn5jihjaqy29nj9jc74yq")))

