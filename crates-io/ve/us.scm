(define-module (crates-io ve us) #:use-module (crates-io))

(define-public crate-veusz-0.1 (crate (name "veusz") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (features (quote ("from"))) (default-features #t) (kind 0)))) (hash "1yb7z2pscqwwf98xch5ydmwd9f1k2q1162dz1cwxhw3njvs519dr")))

(define-public crate-veusz-0.1 (crate (name "veusz") (vers "0.1.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (features (quote ("from"))) (default-features #t) (kind 0)))) (hash "1dxyhh6ijaw25ybpa90sw0swnb5bx7i4a5cn1gbbb6hly44rf53m")))

(define-public crate-veusz-0.1 (crate (name "veusz") (vers "0.1.2") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (features (quote ("from"))) (default-features #t) (kind 0)))) (hash "1bm36f0lycf25dvfgbb6i9nnxkhhxd7i3fyzrahq1b1dnrb6dhpf")))

