(define-module (crates-io ve ro) #:use-module (crates-io))

(define-public crate-verona-rt-0.0.1 (crate (name "verona-rt") (vers "0.0.1") (deps (list (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "verona-rt-sys") (req "=0.0.1") (default-features #t) (kind 0)))) (hash "0v13bywpb6wqc7ial4xi72n717b3xp84wffq2k41lhvirsa0rxm5") (features (quote (("systematic_testing" "verona-rt-sys/systematic_testing") ("flight_recorder" "verona-rt-sys/flight_recorder"))))))

(define-public crate-verona-rt-0.0.2 (crate (name "verona-rt") (vers "0.0.2") (deps (list (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "verona-rt-sys") (req "=0.0.2") (default-features #t) (kind 0)))) (hash "1vbxzyn0ypripf9zjga3y0njyadnghgz5bfjjvv11p46agrvqs52") (features (quote (("systematic_testing" "verona-rt-sys/systematic_testing") ("flight_recorder" "verona-rt-sys/flight_recorder"))))))

(define-public crate-verona-rt-sys-0.0.1 (crate (name "verona-rt-sys") (vers "0.0.1") (deps (list (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "1fvql4jn7wkj12hy3iq99sjvy8qs0skys5grav5w1rcfjp1b6bwd") (features (quote (("systematic_testing") ("flight_recorder"))))))

(define-public crate-verona-rt-sys-0.0.2 (crate (name "verona-rt-sys") (vers "0.0.2") (deps (list (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "1x8y9647w5ygfpycwyq2r5dkb8jv23yw7xmwjfclckr5kxd6y9mm") (features (quote (("systematic_testing") ("flight_recorder"))))))

(define-public crate-veronicakv-0.0.0 (crate (name "veronicakv") (vers "0.0.0") (hash "081gmpwragk6inqb6w9y6m160bfwl8dkgi8n7ijqjsdqylgb1n9q")))

