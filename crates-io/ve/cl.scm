(define-module (crates-io ve cl) #:use-module (crates-io))

(define-public crate-veclist-0.1 (crate (name "veclist") (vers "0.1.0") (hash "10da4ppakhk6xmz82x9giz1r9g2gwwfwpc168awy693lwxsjdf5l")))

(define-public crate-veclist-0.1 (crate (name "veclist") (vers "0.1.1") (hash "1b5ns9xn10nki9936qgv4bsp0swk3anpm6wp4dnc00v10yn6lrd1")))

(define-public crate-veclist-0.1 (crate (name "veclist") (vers "0.1.2") (hash "1s46hnkv02pnaxm3m8gxsx4xx6lp70j4x7nmvi3d3srj8idbqqpr")))

(define-public crate-veclite-0.1 (crate (name "veclite") (vers "0.1.0") (hash "1rifjlil2n2817jk8n78azq9bcl017rpkpda0c742icff057mq0m")))

