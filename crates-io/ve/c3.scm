(define-module (crates-io ve c3) #:use-module (crates-io))

(define-public crate-vec3-0.1 (crate (name "vec3") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "0q4vns7j2fwb3k40nip6x6smyiva89rnamkcdg9fqva6y0vsajw7")))

(define-public crate-vec3-0.1 (crate (name "vec3") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qniqm87m4mxcv63wxlxjzcdzz0rickj0ijz3a6k7bfhy8jl1n7x")))

(define-public crate-vec3-0.1 (crate (name "vec3") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pnc39qdyf8896dgbi1d86djj7qgsiwr08gfvfsxc4xzxrqja4n9")))

(define-public crate-vec3-0.1 (crate (name "vec3") (vers "0.1.3") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x3cfm67vbdcyg2drwz9hdsxyxhjp16k5jvnqsj2yl5i28vrw85s")))

(define-public crate-vec3-0.1 (crate (name "vec3") (vers "0.1.4") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b17cfi3f2n9djl2ix4895nnhcn3ss7kiy54nqvk3pdx2qfaywg5") (yanked #t)))

(define-public crate-vec3-0.1 (crate (name "vec3") (vers "0.1.5") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d1i86r4rgnx6hx9blsmv9i4akdpcjm426w73c1kncm02w6ra8fh")))

(define-public crate-vec3-0.1 (crate (name "vec3") (vers "0.1.6") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "17qsmz20hdzrwplr6g12q2s5bp5aymznz9fmj1rdpm7lm9zs00bf")))

(define-public crate-vec3-0.2 (crate (name "vec3") (vers "0.2.0") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0szmqz3v4z6dwb2apl126c1q422a2vxwjr54xgz165xg9pdnhi11")))

(define-public crate-vec3-0.2 (crate (name "vec3") (vers "0.2.1") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qa90j9x2dcs4lzs0m7sf69cxdh34b7gjg71d5kwhdmwpk7m5nba")))

(define-public crate-vec3-rs-0.1 (crate (name "vec3-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1d7l54w6dlnz5m6h28syi6ghxrzdsnm92xjvh75s90f8l529flvf")))

(define-public crate-vec3-rs-0.1 (crate (name "vec3-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0r4q0pq04pbz1bgfhv1w52rk9x4zy937p7r2zx4z31svspxiy1df")))

(define-public crate-vec3-rs-0.1 (crate (name "vec3-rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "069pr2f7724xa598ymv1wjwdzf0wlw6s1ll96lgjlz5mqy1mjajs")))

(define-public crate-vec3-rs-0.1 (crate (name "vec3-rs") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0qgpqwfxbmvjdsr5dbzlbm5bcg9ny3x15qri4h9hzqv2zm033ikz")))

(define-public crate-vec3-rs-0.1 (crate (name "vec3-rs") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0d460rvbj0h37ajsi3qsfnn1ahgw3v30ni5cpcwzanz5n2ydv91b")))

(define-public crate-vec3-rs-0.1 (crate (name "vec3-rs") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "05pnzgvdkjp25l2iyqfc8krkhn7y5fcn9m0sh7lrw7323hsc4kxb")))

(define-public crate-vec3-rs-0.1 (crate (name "vec3-rs") (vers "0.1.6") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "10hxjcnffs8a1f29p8vxx788grc35qn316b86mnnp39ar16nw9ms")))

(define-public crate-vec3D-0.1 (crate (name "vec3D") (vers "0.1.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nearly_eq") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0qriwhh3afkz2j2ixp9408k8ab7wv8bwi5si1s8klqzjar3qrak1")))

(define-public crate-vec3D-0.2 (crate (name "vec3D") (vers "0.2.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nearly_eq") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0hjw4xblz62sfa9illc0xybv047g464c95xq41yrrs746f9wmx1w")))

(define-public crate-vec3D-0.3 (crate (name "vec3D") (vers "0.3.0") (deps (list (crate-dep (name "float-cmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nearly_eq") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0v94w1vdxb488c3xh4jf4q8ypsvkfqavw2jswaz4ikqn7686jpyb")))

