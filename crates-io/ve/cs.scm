(define-module (crates-io ve cs) #:use-module (crates-io))

(define-public crate-vecs-0.1 (crate (name "vecs") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1db2v01qrbaqv61hkfmz474npl2d58qhq83ckjra6g0z61ny69wh") (yanked #t)))

(define-public crate-vecs-0.1 (crate (name "vecs") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0c279ifrm3zzwjhmkb54c752kjgsahzci2jdq8r4wy3p057mqmi6") (yanked #t)))

(define-public crate-vecs-0.1 (crate (name "vecs") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1cjawysd9xjym3vgcbz34f6rf9wbx7bpyqas488g1f50id8sraa0") (yanked #t)))

(define-public crate-vecs-0.2 (crate (name "vecs") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "02d72wgbvx8rdmxv7q408nwmzbg537vqyflr1ldj9qf92lg1gvj8")))

(define-public crate-vecs-0.2 (crate (name "vecs") (vers "0.2.1") (hash "06b7hwjwnliysldhfxlik3zpsw5hx0d1g3c0mi5q7cmxh2nx35sa")))

(define-public crate-vecs-0.2 (crate (name "vecs") (vers "0.2.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "15s94cawylmlzyx4q7bcaa5yk5yjqg44cqhvcd0rllai664f34wm") (yanked #t)))

(define-public crate-vecs-0.2 (crate (name "vecs") (vers "0.2.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0fsw5dj71jd9wdyjxh8s9zdk635f1d64asm21vm1d7gdys2kfnjh")))

(define-public crate-vecs_file-0.1 (crate (name "vecs_file") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "14f83mbab47c7zfhyibigqa6j62ibzd0r83ql51pm1606bg65nzl")))

(define-public crate-vecset-0.0.1 (crate (name "vecset") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "19d6v7w8cybch2cchlil3id3dfs5a00nf5i9kqqbb5zb7a44lpd8") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-vecset-0.0.2 (crate (name "vecset") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1pag6gy7is4s7d8i7n6fhbcjcnhvvkb5zqz0cv8z132fi8p5rb3s") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-vecshard-0.0.1 (crate (name "vecshard") (vers "0.0.1") (hash "17g0xfzm5a0shwk232v9bk02a6cnv4bsyb6xzjx0rfmz41np9ry5")))

(define-public crate-vecshard-0.0.2 (crate (name "vecshard") (vers "0.0.2") (hash "12b9r25hkmv4hidg5dyqf1l9sffxj1v9fbbbdq1a6y1y5radwwrx")))

(define-public crate-vecshard-0.1 (crate (name "vecshard") (vers "0.1.1") (hash "0xgq4jzhq50ybxsnqvndfqz7pck1c9vwj2p3d67iqj1w0500qr15")))

(define-public crate-vecshard-0.2 (crate (name "vecshard") (vers "0.2.0") (hash "19pkxpbvn2zm6qqbwgf5y12lm1n2xcmm7d4cg3r7k88nnjj0q689")))

(define-public crate-vecshard-0.2 (crate (name "vecshard") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.90") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.90") (default-features #t) (kind 2)))) (hash "0r9bm5ckxrscp35vcwyzlks8xrayq9fmrfyncrbi0k71vzbamdrn")))

(define-public crate-vecstorage-0.1 (crate (name "vecstorage") (vers "0.1.0") (hash "1d2hdxm6vl2hp18fpqvm9b40x51jx69gdrcb88mfjib89zambzc5")))

(define-public crate-vecstorage-0.1 (crate (name "vecstorage") (vers "0.1.1") (hash "0m8dh18d5d1qpjv4bgyysryqrhjig5vsgny5d2n4c1fq0f5ai09v")))

(define-public crate-vecstorage-0.1 (crate (name "vecstorage") (vers "0.1.2") (hash "0xaf57nqmkzcw6xzrr3c0m62s97r5iijn41skbm9d5ks3ixkvdz2")))

