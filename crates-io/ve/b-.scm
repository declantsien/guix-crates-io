(define-module (crates-io ve b-) #:use-module (crates-io))

(define-public crate-veb-rs-0.1 (crate (name "veb-rs") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.21") (optional #t) (default-features #t) (kind 0)))) (hash "1j7f8xn60lbd1s3f91mdb3gd4l82ihgbxv1lr57wbfih3vc3nyxq") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-veb-tree-0.1 (crate (name "veb-tree") (vers "0.1.0") (deps (list (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ghost") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "pprof") (req "^0.11") (features (quote ("flamegraph"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0gbkzy4c0fp9yrwsh29v57107bl7hbq5wkqb1vkbhzn799cz88d2")))

