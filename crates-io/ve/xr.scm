(define-module (crates-io ve xr) #:use-module (crates-io))

(define-public crate-vexriscv-0.0.1 (crate (name "vexriscv") (vers "0.0.1") (deps (list (crate-dep (name "bare-metal") (req ">= 0.2.0, < 0.2.5") (default-features #t) (kind 0)) (crate-dep (name "bit_field") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1xyg30wbq1l1xp26lfw7xp710x2yq0ym8bjwdwaxy6ghkjgg9ld2") (features (quote (("inline-asm"))))))

(define-public crate-vexriscv-0.0.2 (crate (name "vexriscv") (vers "0.0.2") (deps (list (crate-dep (name "bare-metal") (req ">= 0.2.0, < 0.2.5") (default-features #t) (kind 0)) (crate-dep (name "bit_field") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0l8aiwfjmh3ciwxj7rdmggmg6a4xa9khfnlanfnrq1609sia9k58") (features (quote (("inline-asm"))))))

(define-public crate-vexriscv-0.0.3 (crate (name "vexriscv") (vers "0.0.3") (hash "01ndfjg1wwfc3ksgh9b32bnz15inglc2qszaf9icfs04ilwkvb3f") (features (quote (("inline-asm"))))))

