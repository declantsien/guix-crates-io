(define-module (crates-io ve cd) #:use-module (crates-io))

(define-public crate-vecdb-0.0.0 (crate (name "vecdb") (vers "0.0.0") (hash "0l7cwflp0y2qg1p82c7mhbbqyk2abmrpzahhrbklbgls8mmr2x1y")))

(define-public crate-vecdep-0.1 (crate (name "vecdep") (vers "0.1.1") (hash "0f4w2lbw2gp2gn5sczqgbnyifrqd8bxl3xjkyggsl1mzsi3k57qa")))

(define-public crate-vecdeque-stableix-1 (crate (name "vecdeque-stableix") (vers "1.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "08pdjwp59yvg1pgvkr5l38ryymy1xzkpyvibazpydz5lkmaidd0a")))

(define-public crate-vecdeque-stableix-1 (crate (name "vecdeque-stableix") (vers "1.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mgc9550y4wlffkylqgngarff69x2bzfc7vfl477ymmdssplhna0")))

(define-public crate-vecdeque-stableix-1 (crate (name "vecdeque-stableix") (vers "1.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1acz8gf4n00fvi9341gydajdnia86q3y335d4jkjr6nag1sz2lyn")))

