(define-module (crates-io ve cf) #:use-module (crates-io))

(define-public crate-vecfile-0.1 (crate (name "vecfile") (vers "0.1.0") (deps (list (crate-dep (name "desse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1ri1hmp3g6r4ryjhqc8ypmaain2ybp44vdf4117irz7frb3ns240") (yanked #t)))

(define-public crate-vecfile-0.2 (crate (name "vecfile") (vers "0.2.0") (deps (list (crate-dep (name "desse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1d53g63kgz54i3dn03ki1zd3zra7kzfm8cmp1n7aipkg3kkmdcay") (yanked #t)))

(define-public crate-vecfile-0.3 (crate (name "vecfile") (vers "0.3.0") (deps (list (crate-dep (name "desse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1abzm4662pf8d2ljpikwicch0rp6z3l6yhw3mpa7j66avmjcc19v") (yanked #t)))

(define-public crate-vecfold-0.1 (crate (name "vecfold") (vers "0.1.0") (hash "0csrjpb1w3adchzvfbm3nm2p7jir1wfyj7s5w49i2d3v2zfpm8rd")))

(define-public crate-vecfold-0.1 (crate (name "vecfold") (vers "0.1.1") (hash "08i0lwg9jwwar0ij7p5k1a4lrl4z237wjq0hypwfypwdqk2w3n5z")))

(define-public crate-vecfold-0.1 (crate (name "vecfold") (vers "0.1.3") (hash "0223pa9iqkh5imj0crxl1c7cvbwf3gsih7drpncl9kl2g12rjbv0")))

(define-public crate-vecfold-0.1 (crate (name "vecfold") (vers "0.1.4") (hash "1glzxhrf4wjk722hf5cnighmw65p6lplfdgmg4ffjknp4pxamjh9")))

(define-public crate-vecfold-0.1 (crate (name "vecfold") (vers "0.1.5") (hash "1lf3pfda6wb25cm7r4s396m9qyjnbjvw45scwfizj117h54rzi68")))

(define-public crate-vecfold-0.1 (crate (name "vecfold") (vers "0.1.6") (hash "14agxq8z2qhn8z18q9hhfwh5f5crp1jrjk58iz1v0sqlipm1244n")))

(define-public crate-vecfx-0.0.8 (crate (name "vecfx") (vers "0.0.8") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.19") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)))) (hash "0y7kqd77s9amvvjdb1pndzzh2217rs6hibrk7adc7r870wilvc3h") (features (quote (("default"))))))

(define-public crate-vecfx-0.1 (crate (name "vecfx") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.19") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)))) (hash "16c5pah1dfpfmjcppg4193r56hvji7d3f7yavc9dvcjfhmgl2ins") (features (quote (("default"))))))

(define-public crate-vecfx-0.1 (crate (name "vecfx") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.19") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)))) (hash "0grmw9204plv067znjmz9zdshnq3j8mlap50y0zcnj533ifn5fpk") (features (quote (("default") ("adhoc"))))))

(define-public crate-vecfx-0.2 (crate (name "vecfx") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.20") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qwi5gf8fnxk80jkaccmrmsdi7ym50jb8iismxywgvfb2prw3amq") (features (quote (("default") ("adhoc")))) (yanked #t)))

(define-public crate-vecfx-0.1 (crate (name "vecfx") (vers "0.1.2") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.20") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)))) (hash "0h96awsbap6dq4qw0gr94gdg2n56ib1na8fipxn7lq589p8277bm") (features (quote (("default") ("adhoc"))))))

(define-public crate-vecfx-0.1 (crate (name "vecfx") (vers "0.1.5") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.30") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.10") (default-features #t) (kind 0)))) (hash "0xwam4wxf5yxphkvm6shsi1wyghr5c9xhxazamzpkbs87lwk8s97") (features (quote (("default") ("adhoc"))))))

(define-public crate-vecfx-0.1 (crate (name "vecfx") (vers "0.1.6") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3") (features (quote ("serde" "rand"))) (default-features #t) (kind 0)))) (hash "1cr0kn6bfcyadyx7v5cx5x0c2k4iqplbzi20r7zprs68kabm3pr1") (features (quote (("default") ("adhoc"))))))

