(define-module (crates-io ve cc) #:use-module (crates-io))

(define-public crate-vecc-0.1 (crate (name "vecc") (vers "0.1.0") (hash "1g13h6c0ji8pr1dkxrdikh6v4w5h36ri07vck71f7fm74d0b07y3")))

(define-public crate-veccell-0.1 (crate (name "veccell") (vers "0.1.0") (hash "0bcpnzkc4a1vbk4rz6mnk66jnnq2qrqw4vyd1a5ag5xz2hn9znaa")))

(define-public crate-veccell-0.1 (crate (name "veccell") (vers "0.1.1") (hash "1drnq25a1z52a2b8fz40hf3hsj7xgmzn2va107nar5b9ziz10rfh")))

(define-public crate-veccell-0.2 (crate (name "veccell") (vers "0.2.0") (hash "0jpzy5b1mwm3w4fwjg6k3wx0csfxbicpwfyx1ch9jcl93jzm6a07")))

(define-public crate-veccell-0.2 (crate (name "veccell") (vers "0.2.1") (hash "10dlf2ca2hag4c1bnx2wfpkc7pshq8zw4fikd1xbzvvbh85fhc8y")))

(define-public crate-veccell-0.3 (crate (name "veccell") (vers "0.3.0") (hash "0f0l4fzsvim3zdvri0zr905kkb02sxf6mr5idgn12hllvyz95sik")))

(define-public crate-veccell-0.4 (crate (name "veccell") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1kipj004n3aqhvj1cvz7yfndsva89phynx70jym9z5agg1jmhk9p") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-veccentric-0.1 (crate (name "veccentric") (vers "0.1.0") (deps (list (crate-dep (name "overload") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "1643dskwlmlki6lpfr4spln53mdahz7vn3biraka86l6429ag1wl") (features (quote (("random" "rand"))))))

(define-public crate-veccentric-0.1 (crate (name "veccentric") (vers "0.1.1") (deps (list (crate-dep (name "overload") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mnr0bdl15syk0d3cvynv4n2vnyynv8cncsyf7svnfn1xfmzyqhj") (features (quote (("random" "rand") ("default"))))))

(define-public crate-veccentric-0.1 (crate (name "veccentric") (vers "0.1.2") (deps (list (crate-dep (name "float-cmp") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "overload") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "1cpp8j2fww6h6rbg050h3wzp9a8v8j6lxik4jyza4wkvp4ly10ql") (features (quote (("random" "rand") ("default"))))))

(define-public crate-veccentric-0.2 (crate (name "veccentric") (vers "0.2.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "overload") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 2)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 2)))) (hash "19414zb9bs3b0hk6gdd97dk5lvihjw5aag361a3v3cam8s650zya") (features (quote (("random" "rand") ("default") ("all" "random"))))))

(define-public crate-veccentric-0.3 (crate (name "veccentric") (vers "0.3.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "overload") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pixels") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 2)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 2)))) (hash "0jsdfsrklgchxnrydhgpy82rndscxl6mp3cgn5l55lq310zp0rdb") (features (quote (("random" "rand") ("default") ("all" "random")))) (yanked #t)))

(define-public crate-veccentric-0.3 (crate (name "veccentric") (vers "0.3.1") (deps (list (crate-dep (name "float-cmp") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "pixels") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 2)) (crate-dep (name "winit_input_helper") (req "^0.10") (default-features #t) (kind 2)))) (hash "11l3laac88079jwfapxbahnl4kf3wsdpbx98rzfw405lfixh5wrn") (features (quote (("random" "rand") ("default") ("all" "random"))))))

