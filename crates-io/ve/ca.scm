(define-module (crates-io ve ca) #:use-module (crates-io))

(define-public crate-vecarray-0.1 (crate (name "vecarray") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "161hzr3cb06dvzw428w3xy1h0pynhzd8awsm84dxblmb88mcni02") (features (quote (("std" "serde/std") ("default" "std" "serde"))))))

(define-public crate-vecarray-0.1 (crate (name "vecarray") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "12r1ijlsz6ph9j0jcq08z0ghkaw1ij2a7cncw9gmagd14fr04f9a") (features (quote (("std" "serde/std") ("default" "std" "serde"))))))

(define-public crate-vecarray-0.1 (crate (name "vecarray") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "14d0y75188nlfc71k0g8ksc046p3gbj8v60k3hsygg86z26ij7cn") (features (quote (("std" "serde/std") ("default" "std" "serde"))))))

(define-public crate-vecarray-0.1 (crate (name "vecarray") (vers "0.1.3") (deps (list (crate-dep (name "parity-codec") (req "^4.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)))) (hash "0gzqxks19xlq4789znibarkznad2zglhjq4bap351nfpnxrqmmnl") (features (quote (("std" "serde/std" "parity-codec/std") ("default" "std"))))))

