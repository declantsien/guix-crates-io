(define-module (crates-io ve c_) #:use-module (crates-io))

(define-public crate-vec_2d-0.1 (crate (name "vec_2d") (vers "0.1.0") (hash "0fxwk1gxcrz7ynq5dfhmyzvzwnz1q738ykmrq3ss3nyhaywmfzs8")))

(define-public crate-vec_2d-0.1 (crate (name "vec_2d") (vers "0.1.1") (hash "0bbg6am9xvfn39wgjfpmsvw4v06pcwqlznidhyvvln5y6fgj70lk")))

(define-public crate-vec_2d-0.1 (crate (name "vec_2d") (vers "0.1.2") (hash "1ipx65x6c823kwzvnqcfcs03pd5c7si2gxs51mw1c3672bg580wd")))

(define-public crate-vec_arith-1 (crate (name "vec_arith") (vers "1.0.0") (hash "1csa3qinrsv6g0h5rb92ylc31vq0cq5vyjhrwkb35w9jv95i4l05") (yanked #t)))

(define-public crate-vec_arith-1 (crate (name "vec_arith") (vers "1.0.1") (hash "1lh5rgk4gsgjh1qb12sx2qvcw4ih588aypxdn21wvmzgvnc7qz5z") (yanked #t)))

(define-public crate-vec_arith-1 (crate (name "vec_arith") (vers "1.0.2") (hash "1qgmxncms9sm5dsbhsqxh48d7x4ksxc28qspn80cp6xafkkpm6wp") (yanked #t)))

(define-public crate-vec_arith-1 (crate (name "vec_arith") (vers "1.0.3") (hash "1j7pbsz1dw7fcf5yr3xnhsg8vl0jg33r22j2y091y82nb6z5h51k")))

(define-public crate-vec_arith-1 (crate (name "vec_arith") (vers "1.0.4") (hash "0mzlink3czq606rx45sqc9hpy7xx37ysgm7lr343x4q2l3yy23rf")))

(define-public crate-vec_box-1 (crate (name "vec_box") (vers "1.0.0") (hash "0azvk78y7lq7ib313l9hsibr8nw8gx6lnykhkwcvbmhw9b4kya65")))

(define-public crate-vec_cell-0.1 (crate (name "vec_cell") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bmk1y2sv5gh0c107j3077h6cnby98ydgnjmw6i1v88s47r17qdc")))

(define-public crate-vec_cell-0.1 (crate (name "vec_cell") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mx3aida4a1rc2s47vvb6cfvjq72wj9as9j0djlcipa7ldmq3rli")))

(define-public crate-vec_cell-0.1 (crate (name "vec_cell") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yb0fx85ppyqp9557v71p29yh7yz3rjiz7wc3jpxnqjymynsp6cm")))

(define-public crate-vec_cell-0.1 (crate (name "vec_cell") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zvhq8wgd72dsm96kr5qpb7jrr5c2zqrxxhhi21jc3gp0q9h63ng")))

(define-public crate-vec_cycle-0.1 (crate (name "vec_cycle") (vers "0.1.0") (hash "0a604d9gi3sbw6r2ncj3rqvhq0hhlvjq0y704jr2p0pg8160n78k")))

(define-public crate-vec_cycle-0.2 (crate (name "vec_cycle") (vers "0.2.0") (hash "171wiimx7xi2118h8fihr91byqmsndn10krjz2whsbsq7m2bxa2g")))

(define-public crate-vec_extract_if_polyfill-0.1 (crate (name "vec_extract_if_polyfill") (vers "0.1.0") (hash "173mk5vjn2vzn932ykxyfnd4qvjbvpkzrcvf1cqr49kwnrgwpja0")))

(define-public crate-vec_file-0.1 (crate (name "vec_file") (vers "0.1.0") (deps (list (crate-dep (name "desse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "17cv6ryzv4y2yrwd30jnnyv7pcwgb4sv6s00mlabaffk82k85n0y") (yanked #t)))

(define-public crate-vec_file-0.1 (crate (name "vec_file") (vers "0.1.1") (deps (list (crate-dep (name "desse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "05aqwpyrp8xxjasqx6x048yp56sp8y5i2l3wzviq6kwmmyf43yac") (yanked #t)))

(define-public crate-vec_filter-0.1 (crate (name "vec_filter") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "vec_filter_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gjhqh3kmrd3w2bcs1p56g2c9c95hk4h3f69p0s640fbbk4kyqlz")))

(define-public crate-vec_filter-0.2 (crate (name "vec_filter") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "vec_filter_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 2)))) (hash "19s1pkwk8fd9hqmy0kqjsg6q8xki9xlpqs4nx2wkbl7sj6nzcv4q")))

(define-public crate-vec_filter-0.2 (crate (name "vec_filter") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "vec_filter_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 2)))) (hash "06wxxcsys23i0772j3dbrv32a1khjlxz9kdzk9a3icynffmx12qm")))

(define-public crate-vec_filter-0.2 (crate (name "vec_filter") (vers "0.2.2") (deps (list (crate-dep (name "lru") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vec_filter_derive") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.17.0") (default-features #t) (kind 2)))) (hash "029vi3pp5plfgxi3prg9ksl03aybaaz450q7hxqx6jqcgl8rdqgg")))

(define-public crate-vec_filter_derive-0.1 (crate (name "vec_filter_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ispll233g7grwnjhnnp6wfj72r65bcxsn59ry3zyx6xidqxnmdp")))

(define-public crate-vec_filter_derive-0.1 (crate (name "vec_filter_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1md4dsz26czg7j2xs6ri3k0l4k1h8jg789iwizrvbas6r0kgwdkw")))

(define-public crate-vec_filter_derive-0.1 (crate (name "vec_filter_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "006098r5w9ni3fls4sxv1yrlkla0d1w6hspfrknsparijyfb1v5h")))

(define-public crate-vec_key_value_pair-0.1 (crate (name "vec_key_value_pair") (vers "0.1.0") (hash "0x4nyg3pd8hm0xwp7nag37w8j1872rskmikymayk8nz0n1mzw53h")))

(define-public crate-vec_map-0.0.1 (crate (name "vec_map") (vers "0.0.1") (hash "1pfsh5jydhy2dp1l9gzh44hky84s8smscdnbxykvjyfcrr515fy1")))

(define-public crate-vec_map-0.1 (crate (name "vec_map") (vers "0.1.0") (hash "10s7lkzyvn3fgaarxnbvlkg5wc67lxv7wlcfxidwa8glhbxb0y7f")))

(define-public crate-vec_map-0.2 (crate (name "vec_map") (vers "0.2.0") (hash "085fzmvx3kyhwx2h6vrwvn715wdsxr16rb9riyd2z28ya9ggjyxy") (features (quote (("nightly"))))))

(define-public crate-vec_map-0.3 (crate (name "vec_map") (vers "0.3.0") (hash "11by6bywlqfka6kyqy08dim8myagaywfw959x1mxigy61gvwiw5b") (features (quote (("nightly"))))))

(define-public crate-vec_map-0.4 (crate (name "vec_map") (vers "0.4.0") (hash "00lgjk07wdggvq0b21y78a4yijcyai5i7j6rx4kx8wkw1n51218q") (features (quote (("nightly"))))))

(define-public crate-vec_map-0.6 (crate (name "vec_map") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "0pxhvwm6hp6n44dqd066vkv67vk2ql0wg0sgz314x88grgjyzifa") (features (quote (("eders" "serde" "serde_macros"))))))

(define-public crate-vec_map-0.7 (crate (name "vec_map") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "15qq6q6kb5ic1gb1cbvnn535c4ppczkb4zrmfbc8w6fh7fwwikgq") (features (quote (("eders" "serde" "serde_derive"))))))

(define-public crate-vec_map-0.8 (crate (name "vec_map") (vers "0.8.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "134q4kiznx47ynvd1y48cqcd704zhr6dv9xspcl1dl1a3iimnyw8") (features (quote (("eders" "serde" "serde_derive"))))))

(define-public crate-vec_map-0.8 (crate (name "vec_map") (vers "0.8.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "06n8hw4hlbcz328a3gbpvmy0ma46vg1lc0r5wf55900szf3qdiq5") (features (quote (("eders" "serde"))))))

(define-public crate-vec_map-0.8 (crate (name "vec_map") (vers "0.8.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1481w9g1dw9rxp3l6snkdqihzyrd2f8vispzqmwjwsdyhw8xzggi") (features (quote (("eders" "serde"))))))

(define-public crate-vec_mut_scan-0.1 (crate (name "vec_mut_scan") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.9.3") (default-features #t) (kind 2)))) (hash "1xpr7y9m3vrms6a3wrkpngcv82kgs22ybsdkdvz8snh7664ndmdl")))

(define-public crate-vec_mut_scan-0.2 (crate (name "vec_mut_scan") (vers "0.2.0") (hash "0zm7b91kmcqcssx3cggwq2im4as34lvl3xdr3zdm9a1ikc0vmf8g")))

(define-public crate-vec_mut_scan-0.3 (crate (name "vec_mut_scan") (vers "0.3.0") (hash "1lkz66l8z13lvjll69s23vrca12inpyyh00kwg0djqsyil563vb8")))

(define-public crate-vec_mut_scan-0.4 (crate (name "vec_mut_scan") (vers "0.4.0") (hash "086b90j4gvssxj5rz6gphdchnzzyyiknp0jh56p01s708k0khkx2")))

(define-public crate-vec_mut_scan-0.5 (crate (name "vec_mut_scan") (vers "0.5.0") (hash "0vsqgcqzrcd04a1hkfgnv1aybgv2y40lfp3hb6fanz1z4cbg5c9g") (rust-version "1.36.0")))

(define-public crate-vec_once-0.1 (crate (name "vec_once") (vers "0.1.0") (hash "108a6d3zm34lzlpg1xz0hscfiivn5kj8b92cdkl6fg8nn8kck73g")))

(define-public crate-vec_once-0.1 (crate (name "vec_once") (vers "0.1.1") (hash "08fl9hyfk3gvf5g6gh4ah1nrsrjq97viaci7rqa2df4qhag7xjwn")))

(define-public crate-vec_ptr-0.1 (crate (name "vec_ptr") (vers "0.1.0") (hash "1n2dy5rbjhamh3mnabbmr2nbbym2cpq09lysq4m1il2npj0q70lr")))

(define-public crate-vec_rand-0.1 (crate (name "vec_rand") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "05pvvjsv39zdr9blgdxrwaxblci2dw7vdr15wbs3pmhx4q6wv3xz")))

(define-public crate-vec_rand-0.1 (crate (name "vec_rand") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1b0fwwkm09d2nqfpgshrc7ldgjfxndajjla5va9knk6bymm3lvam")))

(define-public crate-vec_rand-0.1 (crate (name "vec_rand") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1l8431fjrc11735zv0gp5vzx58c5a7gn5hzqwp7jfx32r7d5009z")))

(define-public crate-vec_remove_if-1 (crate (name "vec_remove_if") (vers "1.0.0") (deps (list (crate-dep (name "pretty_assertions") (req "~0.6.1") (default-features #t) (kind 2)))) (hash "0nq1bgk8nf375ah9yl88vaphzi925grzip98ap7kr4srmh85j4lh")))

(define-public crate-vec_saver-0.0.1 (crate (name "vec_saver") (vers "0.0.1") (hash "0ccyvw342jsfc3q4dx83x09l9k7mrlhql03kaypz8jdjbfmaf219")))

(define-public crate-vec_saver-0.0.2 (crate (name "vec_saver") (vers "0.0.2") (hash "19wlj2jnnnnpmbz6dqrr4k08nffh3c9d2gxxm43kxilsvpsik11s")))

(define-public crate-vec_security-0.1 (crate (name "vec_security") (vers "0.1.0") (deps (list (crate-dep (name "crypto-hash") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "0i77rqzgxdqlv4ikzfba8d1w02npz3ni9p52p587nnffqmgsb2rm")))

(define-public crate-vec_security-0.1 (crate (name "vec_security") (vers "0.1.1") (deps (list (crate-dep (name "crypto-hash") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "0fpzhfzn7f3njryh1sg7ynnf5i2azim0m781k1q2h8b345r2aba6")))

(define-public crate-vec_shift-0.1 (crate (name "vec_shift") (vers "0.1.0") (hash "13p0qclc4466s59xlk8m49sgph0m1g0ilh21c42i2xa81aqc41fw")))

(define-public crate-vec_split-0.1 (crate (name "vec_split") (vers "0.1.0") (hash "0bazdqi0rv81zmp7gg1ddq2y5z46p47l5mwrp9d3xp1mx3w8ix9q")))

(define-public crate-vec_split-0.1 (crate (name "vec_split") (vers "0.1.1") (hash "1b4ri0b1nqiacinala4305wcd91s2wsmkyapwm21xbbx9l8sa2qn")))

(define-public crate-vec_split-0.1 (crate (name "vec_split") (vers "0.1.2") (hash "0z6pp05ik11r1nyyv20i66wxalvdh6hr661zzg40vam7lmzglqzx")))

(define-public crate-vec_split-0.1 (crate (name "vec_split") (vers "0.1.3") (hash "00g36599ynbvs7921qb5fs5v370551mcdxfyxqsbmj5nsvj5fkpj")))

(define-public crate-vec_split-0.1 (crate (name "vec_split") (vers "0.1.4") (hash "0fy9ryc7fbndgkizpwl02vchqyi72039rwd96qy1iq55ih1sdqg0")))

(define-public crate-vec_storage_reuse-0.1 (crate (name "vec_storage_reuse") (vers "0.1.0") (deps (list (crate-dep (name "recycle_vec") (req "^1") (default-features #t) (kind 0)))) (hash "175ihj1ar2lfy9bkk9kw8d7mjd4vwj8w6sfrckkr43c9j01fqhl7")))

(define-public crate-vec_to_array-0.1 (crate (name "vec_to_array") (vers "0.1.0") (hash "0hbyhx20d8ai2w5m6y8h7rb6nlgvrzggajswyxwxm9zm492xm3q9")))

(define-public crate-vec_to_array-0.2 (crate (name "vec_to_array") (vers "0.2.0") (hash "0i5hkfhsf6s120vrpnp86x259d5phv41p3ppil846ppnh3r6hyqv")))

(define-public crate-vec_to_array-0.2 (crate (name "vec_to_array") (vers "0.2.1") (hash "0dsr5ld710yijr3dd9szcywa0ds359266fhiidj4m2jmkv86kiq6") (rust-version "1.73")))

(define-public crate-vec_to_array-0.2 (crate (name "vec_to_array") (vers "0.2.2") (hash "1s9q3pdl6fbs8vi23r54a4kjlkwyv9ldnmpsjfdsry8mw7m5fl7h") (rust-version "1.73")))

(define-public crate-vec_to_array-0.2 (crate (name "vec_to_array") (vers "0.2.3") (hash "1x4xfflc39hr1gwc0pmppapnay83dd3c7h9hr5pxg4120iz29sbs") (rust-version "1.73")))

(define-public crate-vec_vec-0.1 (crate (name "vec_vec") (vers "0.1.0") (deps (list (crate-dep (name "stack-trait") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jbmx7yvqp5xz8rsxj7d29jadh6mf72jixibxi2f4cy9z91c8y11")))

(define-public crate-vec_vec-0.2 (crate (name "vec_vec") (vers "0.2.0") (deps (list (crate-dep (name "lending-iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nougat") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stack-trait") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qd5ppn4gqw45s1bjbihrbcc3v0z6q2dnwp9f3nd6myzby7cgrx6")))

(define-public crate-vec_vec-0.3 (crate (name "vec_vec") (vers "0.3.0") (deps (list (crate-dep (name "lending-iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nougat") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stack-trait") (req "^0.3") (default-features #t) (kind 0)))) (hash "0iawzgcn518ci5lprnz1f5k0kvk511z97cmrf15ycc43vlkv9mw9")))

(define-public crate-vec_vec-0.4 (crate (name "vec_vec") (vers "0.4.0") (deps (list (crate-dep (name "lending-iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nougat") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stack-trait") (req "^0.3") (default-features #t) (kind 0)))) (hash "17awpd2pcgp4sx3zcsabqh17f7m47l2rssywxsm50ig6y36dgqzj")))

(define-public crate-vec_vec-0.4 (crate (name "vec_vec") (vers "0.4.1") (deps (list (crate-dep (name "lending-iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nougat") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stack-trait") (req "^0.3") (default-features #t) (kind 0)))) (hash "17wzcp2w5vxykg248dsjwi4w1b3q6bq1k4syvim7sc888ckvx316")))

(define-public crate-vec_vec-0.5 (crate (name "vec_vec") (vers "0.5.0") (deps (list (crate-dep (name "lending-iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nougat") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stack-trait") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ds1swz4b0507hkv00g5bi3yl2iq9k2z0aylyryv72rp98bk0j7f")))

(define-public crate-vec_vec-0.5 (crate (name "vec_vec") (vers "0.5.1") (deps (list (crate-dep (name "lending-iterator") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nougat") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stack-trait") (req "^0.3") (default-features #t) (kind 0)))) (hash "0b241lb2imvrscdnd322rqxicl6fggsfd5f1a53kpj52l412np6g")))

