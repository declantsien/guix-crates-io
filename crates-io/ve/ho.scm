(define-module (crates-io ve ho) #:use-module (crates-io))

(define-public crate-veho-0.0.1 (crate (name "veho") (vers "0.0.1") (hash "1bm57lsfi4dkdzwymq7qb28w49w6kylpcacswgrbwvxdzh21jlkv")))

(define-public crate-veho-0.0.2 (crate (name "veho") (vers "0.0.2") (hash "08pd18ipwpg5l5a6sil5fj6y9dk7aqkc3yrbxy84mvly6qvhd71k")))

(define-public crate-veho-0.0.3 (crate (name "veho") (vers "0.0.3") (hash "0hgn8dqqkqh7wgjicwgr847pqca5yd3p64m38c822l7i8aakm53m")))

(define-public crate-veho-0.0.4 (crate (name "veho") (vers "0.0.4") (hash "0cf45ycbxv4cm425dap3fnw274mznb6nv777n87xvs9rsj8yk67g")))

(define-public crate-veho-0.0.5 (crate (name "veho") (vers "0.0.5") (hash "0qkyi14svqvknkq08p8821wsa1ghrjp5i2yqf2yhg9x3frzp6jhw")))

(define-public crate-veho-0.0.6 (crate (name "veho") (vers "0.0.6") (hash "1z6wzxs72na9nkx08pz92yc6h6iivm20shd9ikzd50jkld5wfkkc")))

(define-public crate-veho-0.0.7 (crate (name "veho") (vers "0.0.7") (hash "1c2hgdrgyh9kgqvqb9ymagy6mx51k3qqwf2f1rzmys2yq84snfn0")))

(define-public crate-veho-0.0.8 (crate (name "veho") (vers "0.0.8") (hash "1pryb1blf91idk6qvqqfjd8j5hx88s08yzzv7jl6m4vq1rpi5qrf")))

(define-public crate-veho-0.0.9 (crate (name "veho") (vers "0.0.9") (hash "18axpf34r717l9j2cdvs6znsij7fp0qpjyqhqxvcb3x0dx7989vl")))

(define-public crate-veho-0.0.10 (crate (name "veho") (vers "0.0.10") (hash "0sk2ahb9f6ih00n4137ag86lfgimhbsxssfgfk57xbchwhw9w3q6")))

(define-public crate-veho-0.0.11 (crate (name "veho") (vers "0.0.11") (hash "1wbnfhd3c4bn7457lmvm0hmfhzq1h40mrlcrl0wgsndahdi7lvyr")))

(define-public crate-veho-0.0.12 (crate (name "veho") (vers "0.0.12") (hash "1s9bx7lalfqdc9by8y6p5m5qn7z14mmbr3gqsjjcaz9br7rgbmy4")))

(define-public crate-veho-0.0.13 (crate (name "veho") (vers "0.0.13") (hash "1cf0r90c35782a1m9syygv2rclpqbgyrw2nrycsxqa0pvkv59mqc")))

(define-public crate-veho-0.0.14 (crate (name "veho") (vers "0.0.14") (hash "1dmd8650lpqz37jnahbnkmh2mz12n371cb68f7w6bpg18y70pfn8")))

(define-public crate-veho-0.0.15 (crate (name "veho") (vers "0.0.15") (hash "1c0wamg5rxwrhvpdgg9wg9wby1yl8f5a1zlx5q6g3vwb7hf19lfp")))

(define-public crate-veho-0.0.16 (crate (name "veho") (vers "0.0.16") (hash "0fwwnlirl8pibr2xwxa2hbny5vhgvnxdmgpm80dv9fn5113ncxkx")))

(define-public crate-veho-0.0.17 (crate (name "veho") (vers "0.0.17") (hash "1jn7xd6srihcx4p5zk2l10bqfhjlmh98xhqlwldmhm5i2kz92pc6")))

(define-public crate-veho-0.0.18 (crate (name "veho") (vers "0.0.18") (hash "1ryll60xbyrmg56a4fp8qkz7ncykdxv0crvzf5r37yx8p1i46lbi")))

(define-public crate-veho-0.0.19 (crate (name "veho") (vers "0.0.19") (hash "01km3q31hbsih4p4bljfrf0pa0p9qh9q7d93hyv5aipisxhcnzcz")))

(define-public crate-veho-0.0.20 (crate (name "veho") (vers "0.0.20") (hash "11q2hv61lzdafilj6byb5la98lvlss0jy0z0r1qs5mrw108hzy33")))

