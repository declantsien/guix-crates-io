(define-module (crates-io ve le) #:use-module (crates-io))

(define-public crate-velect-0.1 (crate (name "velect") (vers "0.1.0") (deps (list (crate-dep (name "delegate") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "05qmd0n1fwmikm6vbzc2dqrjk7mpjhmh1zn57f6d8vjjglwy42h8")))

(define-public crate-velect-0.2 (crate (name "velect") (vers "0.2.0") (deps (list (crate-dep (name "delegate") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1sqjm4cc4kvba9fgkq1mj04xn816hxk1y5xhyz9wxrpk4n2aknvr")))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1gl9fnrhxibc4l37mld1zrapavd15x3q5jjhbhs3n4fqgg0jxnbn") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "03kdfk5vm96b8x707iik4pwlqqm1l4vavlfrkffwxcjgxjz17h86") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1qdy9hniyawcqy8igqbl2hzzwq847ammz4p5dlm4vyknps02wbnp") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0163mv3hpqcchgwvm250gl0fhsqpi7kk2d3qf5mwq23h9wfw726c") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "13dqlllqdsxzabbdh1a28qhyc67r60290afzaz71r2nhc14fk0m5") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "13kfrx1nz2csxfl0h77jf89m0whmkgglrlx33bb0pmr3zsa0wph8") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "101drjy78zwvnffjm7jg2wghiiw9fz6vnh3hpynfvy2s59qa1crb") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0pvyxjz2a3i8axfccrj421piy7bjv088vndxr5vwkn3lm1xmibfr") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.8") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "09zcfg8hmzfnw0hp1j1i6jrn6ib53jixhz06ixn4826xbs07vhj0") (yanked #t)))

(define-public crate-velen-0.1 (crate (name "velen") (vers "0.1.9") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1pi3vng485aad2qlblir364am8cm1jm051k0vnc7m7rcd62xa0kq")))

(define-public crate-veles-0.1 (crate (name "veles") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jppwhg4yf7x4kd69msfrpjw1z6ikvpn5z0gwqbcwk5ahff4mljf")))

