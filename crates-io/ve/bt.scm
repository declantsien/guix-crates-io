(define-module (crates-io ve bt) #:use-module (crates-io))

(define-public crate-vebtrees-0.1 (crate (name "vebtrees") (vers "0.1.0") (hash "15jjzk4ysn9nwwvnkg7f8i5nk5di4s6y1f40qw0jgicfb3l3lg42")))

(define-public crate-vebtrees-0.1 (crate (name "vebtrees") (vers "0.1.1") (hash "1bzqvgchbdq6dyd0hiw8vjjr0yv8c26fzc3m9r0xccky19970jdv")))

(define-public crate-vebtrees-0.1 (crate (name "vebtrees") (vers "0.1.2") (hash "0y7sz3p2sr84z3wbiqgyjs5nhzzrp3hcmqq5xqpcfm4pl6qn6gaz")))

(define-public crate-vebtrees-0.1 (crate (name "vebtrees") (vers "0.1.3") (hash "1r6qf01sfp87kmpgm0nw7rnvgg6wyhxgn1v45qwmkmqrqp5bnc91")))

(define-public crate-vebtrees-0.1 (crate (name "vebtrees") (vers "0.1.4") (hash "1h734093gjdlbdkqr3ll48n4sb3q26a5fzj628mjc2vf37vl268l")))

