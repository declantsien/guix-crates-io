(define-module (crates-io ve rm) #:use-module (crates-io))

(define-public crate-vermilion-0.0.0 (crate (name "vermilion") (vers "0.0.0") (hash "0fvfymi5jxyqyig351vwy8fd0wvdnagqwpk94djnwwyykxar4h5r")))

(define-public crate-vermilion-0.0.0 (crate (name "vermilion") (vers "0.0.0-unreleased") (hash "0z6zbqh1sn59fwmqda6kgygh26jwqipjpw8c7nr80crsp8y6ybwp")))

(define-public crate-vermilion-codegen-0.1 (crate (name "vermilion-codegen") (vers "0.1.0") (deps (list (crate-dep (name "vermilion-object") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vermilion-vm") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10knkpgqwr3g56wjdhjqqwfb8ja0a7av6na4lb6nhcjz5crxpnxm")))

(define-public crate-vermilion-object-0.1 (crate (name "vermilion-object") (vers "0.1.0") (deps (list (crate-dep (name "vermilion-vm") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1l5krlhdj97hg3rakgyf4df01rxw67ka6qpccc7ay8b8xjxijfy4")))

(define-public crate-vermilion-vm-0.1 (crate (name "vermilion-vm") (vers "0.1.0") (hash "1id1ws30839bq2fhhyvj2xl8z18zvmk23mgj4zyf10j5cx76sx0f")))

(define-public crate-vermilion_jit-0.1 (crate (name "vermilion_jit") (vers "0.1.0") (deps (list (crate-dep (name "vermilion-codegen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vermilion-object") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vermilion-vm") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mfzbvdmz0907s8xw9q0rwp57d0bjyg1gb47qsxrh6273y6fplzr")))

(define-public crate-vermilionrc-0.0.0 (crate (name "vermilionrc") (vers "0.0.0-unreleased") (hash "0plpnihygyrysg42v0w0y373dqqjsp88qckkp25gkv6166wwc9s6")))

