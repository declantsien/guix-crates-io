(define-module (crates-io ve ge) #:use-module (crates-io))

(define-public crate-vegemite-0.1 (crate (name "vegemite") (vers "0.1.0") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0sl6jhm9b8jg2x5ycnr3jm2pq8mbq5nd6cg2d4w6w8ndz6spl9n8") (yanked #t)))

(define-public crate-vegemite-0.1 (crate (name "vegemite") (vers "0.1.1") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0s4dv7dssa882nng7v9n7vc1vshivcy4m9gc011ydy5w0vi47q4c")))

(define-public crate-vegemite-0.1 (crate (name "vegemite") (vers "0.1.2") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1rv3py75dp393nddr2x3kx7ssrigkc86jdvr7czibm4k6whcf3m0")))

(define-public crate-vegemite-0.2 (crate (name "vegemite") (vers "0.2.0") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "15xvywk5m7akgi0c6i9hpvj0wm2rc416m52a5r05ga8rl156509m") (rust-version "1.65.0")))

(define-public crate-vegemite-0.2 (crate (name "vegemite") (vers "0.2.1") (deps (list (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1v6vjv7qx0xgwx528vp9vncgldzc124ndda59ablbamji76b2xmq") (rust-version "1.65.0")))

(define-public crate-vegetation-cli-0.1 (crate (name "vegetation-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "08x9ch218xn1mp61dad65aqcwykhxkd13l99xska8gyjzfbnxzbv")))

