(define-module (crates-io ve se) #:use-module (crates-io))

(define-public crate-vesema-0.0.1 (crate (name "vesema") (vers "0.0.1") (hash "1aqvmlsdg6fnvmfhsjp6gyzcnsaasf5x6k6plnxgbzqv1d2ns8hg")))

(define-public crate-vesema-0.0.2 (crate (name "vesema") (vers "0.0.2") (hash "1vqkzgrnka2471rp95xbwkmbkqsjzagrxv2gbwny2h8pjj5qad7g")))

