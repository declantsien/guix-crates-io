(define-module (crates-io ve c2) #:use-module (crates-io))

(define-public crate-vec2-0.1 (crate (name "vec2") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lkhi7njyxgi73m33jk0x5biyq8kx0ybsrag5cxazqm3760sd4p1")))

(define-public crate-vec2-0.1 (crate (name "vec2") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0kgb08ia85dlj1piwm72bfzclkhmfx5y94z7vgqi6nc5xfc8g6f1")))

(define-public crate-vec2-0.1 (crate (name "vec2") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "04hvkzpzl3ysh8mhjmjwsyi3mqnvy4x2415qzfsxvcdy8jynmfbs")))

(define-public crate-vec2-0.1 (crate (name "vec2") (vers "0.1.3") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nmn0mq1a8b8j76wb5wpiphwzjxicsbnvygfvwagysrsv3vw9qp8")))

(define-public crate-vec2-0.1 (crate (name "vec2") (vers "0.1.4") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "01r2hpm2gipccg1ayfm0464m8z921cjqiljji0d4mrb5brc95apm")))

(define-public crate-vec2-0.1 (crate (name "vec2") (vers "0.1.5") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gachs1wv3z68bibj0l4hnn700lj4bff4xvykf9kphjf6aa3n8dm")))

(define-public crate-vec2-0.2 (crate (name "vec2") (vers "0.2.0") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.2") (default-features #t) (kind 0)))) (hash "1nhh0sb300m026qx4qck8rrqnqj4i6likyyg5my0qnnkfvfplwyj")))

(define-public crate-vec2-0.2 (crate (name "vec2") (vers "0.2.1") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec3") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gxv9hdl07imqsjqh2vrpckflf7k51qnjajmajn0yn0vacaq8k55")))

(define-public crate-vec2checkd-0.1 (crate (name "vec2checkd") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nagios-range") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "prometheus-http-query") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("rustls-tls" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1y1zigiyds9ndzqkbaf13jj02g5va309vv9lv5hqpkwbf8dx5nz3")))

(define-public crate-vec2checkd-0.1 (crate (name "vec2checkd") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nagios-range") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "prometheus-http-query") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("rustls-tls" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "00swi5x7i7xvzjgkzkyzzmmbcpbc651bn83ndw3bi8bl18744hg6")))

(define-public crate-vec2checkd-0.2 (crate (name "vec2checkd") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "nagios-range") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "prometheus-http-query") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("rustls-tls" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "149jaxny5hngb51sn79q04gk6db857b4ly598pyrxmy6xm1x6mq7")))

(define-public crate-vec2checkd-0.2 (crate (name "vec2checkd") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "nagios-range") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "prometheus-http-query") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("rustls-tls" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "16xcxwjphwy9kylp4s426vaznpqjzk3dsgm93s3y17jgycvfn9bc")))

(define-public crate-vec2checkd-0.2 (crate (name "vec2checkd") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "nagios-range") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "prometheus-http-query") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("rustls-tls" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wqwfnx8nnxw4amvjsgilgc089y3idf18f86bk0b0vzlyflpc436")))

(define-public crate-vec2d-0.0.1 (crate (name "vec2d") (vers "0.0.1") (hash "0dwvpqz4fmr4dirn432vr5fm6r7n4yk872a706zwmb7yqbvba2vx")))

(define-public crate-vec2d-0.0.2 (crate (name "vec2d") (vers "0.0.2") (hash "17p76221p6k81xrd63m0c1f1l8wh0y4j00fbhz2rd3plwqvqmvcl")))

(define-public crate-vec2d-0.0.3 (crate (name "vec2d") (vers "0.0.3") (hash "1jbngi2x4vfhkvcv6szvj5bb32zlmcgpqqrllwfjanraz6nkx2h4")))

(define-public crate-vec2d-0.0.4 (crate (name "vec2d") (vers "0.0.4") (hash "0gp12c5jjmmwmnzy5i25dm54hm479dsg7w114cmdl3i9myg4p275")))

(define-public crate-vec2d-0.0.5 (crate (name "vec2d") (vers "0.0.5") (hash "0ah0ryar88r0wm5bxbjgj2sd4nnqwqk7z4mzfy180cbd49liicxk")))

(define-public crate-vec2d-0.1 (crate (name "vec2d") (vers "0.1.0") (hash "1gmcn8flddkmgw59aqvjygdharfisqyz6dgyg4ncfdq9vd42k2zl")))

(define-public crate-vec2d-0.1 (crate (name "vec2d") (vers "0.1.1") (hash "1bm2cqympyi1dq9j539hhif6l0vjsy488lgnvwix71wwiymla8hj")))

(define-public crate-vec2d-0.3 (crate (name "vec2d") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "09g4pvgbdiqszjada5sfgg1ysrrldl4sbxrwjayx4ma5qjix2y40") (features (quote (("serde_support" "serde"))))))

(define-public crate-vec2d-0.4 (crate (name "vec2d") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.73") (default-features #t) (kind 2)))) (hash "1pwmqa89v3jbmm9nw738drmb1lzaic1y3va2mg58k39b5z6almnm") (features (quote (("serde_support" "serde"))))))

(define-public crate-vec2d-0.4 (crate (name "vec2d") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0.132") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.73") (default-features #t) (kind 2)))) (hash "0ccyirx2qd34lvl7vrv1r7q27a6mdqzqq4z5fgrmx3s38f5w19h5") (features (quote (("serde_support" "serde"))))))

(define-public crate-vec2dim-0.1 (crate (name "vec2dim") (vers "0.1.0") (hash "1kh9xlxg3k86rppzmgf988zzh3947vfxacsmx3n561kl9gigx4ay")))

(define-public crate-vec2dim-0.1 (crate (name "vec2dim") (vers "0.1.1") (hash "0yln28dh9zcmaqs4a1gkgy3ssbq5fayqzylxsfz7k0bsh17mrr10")))

(define-public crate-vec2dim-0.2 (crate (name "vec2dim") (vers "0.2.0") (hash "03ahjgvcs62mzjwm9n6v3ffc1bsg5wslmxq72syxgbbk2s4ln741")))

(define-public crate-vec2dim-0.2 (crate (name "vec2dim") (vers "0.2.1") (hash "1j2ymz81fbnr4gdxvz2jbvxcrkcsxgzqf6jz09smw65008byjigf")))

(define-public crate-vec2dim-0.2 (crate (name "vec2dim") (vers "0.2.2") (hash "1n1q5bb4405rhcfv6hd3d0x50ynjag0hys6dqar5a2yaz4jc93c8")))

(define-public crate-vec2dim-0.3 (crate (name "vec2dim") (vers "0.3.1") (hash "1cbai2bd2kix7yfx4cjbylkkdmbm3g0m0js826z6as2895y3ghnl")))

(define-public crate-vec2dim-0.3 (crate (name "vec2dim") (vers "0.3.2") (hash "16cd9nzih4dwcf35jqrchbwyaqw3j87lxz2680ssjs1cyd4wdc7v")))

