(define-module (crates-io ve xt) #:use-module (crates-io))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.0") (hash "1yy50ba5iaysl8gg2jbh255gf7kl85lrc1zigvxgpdp0f1rmarv2") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.1") (hash "10q76jg6y8sv6g7gq8cxhx4ff3wdna05m159mpqnbnrvmj2779cb") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.2") (hash "1xrpzl2zqa3szcmzbgl3qrl59074dyvk5arqa9v191yzwcqzz3p8") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.3") (hash "06drgyphzsyx33dxhkvwikapg5jyjfz5v2g26723n0dxfw9ciaj0") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.4") (hash "1igx9ba3xkrjlskv1c1j69jhw3izkmb5j5fzf1x9hl5xq8j9s8vw") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.5") (hash "1cjm00wgqjav80zkcg9p678a14fy3yd1vlhpfkignmlmr9fpkhza") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.6") (hash "0sy1lskillaqkrdidfx46hn704fsgi8y1f1j15sliilgj920chc6") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.7") (hash "17ap50x24ys04ybgyn99m4lf7wkflig451c63gjixylz9v8m462p") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.8") (hash "0rv6i5n9wkp7zq6j8jpp5z7nd9miyhjsfhs6c7yrvbi1j0hbvn0f") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.9") (hash "0ijh80psba7v0j9r8fy9d79ixmflh1xs8f2cj8g8k3qfdp4gnvsa") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.10") (hash "17wa17jn43y2ayg9nzln2nvjrlw31jdfk52v4w21w4iis86ka156") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.11") (hash "1z9bxg3mmnzg2c80x3654kl0mdbm55gjka20rf31ab63k4nnfw2x") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.12") (hash "1lsik14rrrn4s8phjlszarsr0jf6v0wk2qbxlbfcrmxq7b7m9f4c") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.13") (hash "1d3p3a9mr5ss9qxifn2dlv6wdivlhp3pwf89swjprsc04cjn39nm") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.14") (hash "0r4l1fglxsn0pkncfk9g7vxz9iqx00fig6ffiqhanygbha9c06gg") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.15") (hash "1cx6znjpwpi38c2in53y9g5cqrwzpd0vzcgajzcnjd9185b3w4rw") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.16") (hash "0z4imb0pnkxwsrcg2gqsh8hyr1zqg1pmmxhml70az0n5v817ad1v") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.17") (hash "0pj4yzcs2cphlc34rpqv96sqwj0hrcs23z2332p60k9aja85xnvh") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.18") (hash "0g2m7kl6wpzrrbg9hzfn95sm1cyf7gk7q2k7gkhnjd5374aadszc") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.19") (hash "172i1vn8af5xynb04kliy972v55n7qbnrclzsb0pvddcbvcnbfa8") (yanked #t)))

(define-public crate-vext-0.1 (crate (name "vext") (vers "0.1.20") (hash "17n4dfbdg2kpa3w8wfj40jslc9yknks1x6qjngaz8z76k89slx84") (yanked #t)))

(define-public crate-vextractor-0.1 (crate (name "vextractor") (vers "0.1.0") (hash "0f95v1m7cwpa0n5sxdgjjadcwk2j0q94g3nj2qb2sq1qmyw583ia") (yanked #t)))

(define-public crate-vextractor-0.1 (crate (name "vextractor") (vers "0.1.1") (hash "1m06a7qpmqwl6j6bv522z2q9jpikb292a7m0qabnhc5cr33sb2ig") (yanked #t)))

(define-public crate-vextractor-0.1 (crate (name "vextractor") (vers "0.1.2") (hash "1zjnwdwbnglj3xg0gyhsnwcrrbdzkb68kasi2j7yvidwz6cd195c")))

(define-public crate-vextractor-0.2 (crate (name "vextractor") (vers "0.2.0") (hash "0645kzcda365dyilz74i33x1vmjrf8jy9saq6263rxyhgpc9qsz9")))

(define-public crate-vextractor-0.2 (crate (name "vextractor") (vers "0.2.1") (hash "1x55f3sgmabsym4h7kg0m9lx856w4jmgiavm9sqdhjgqwiwiks9h")))

(define-public crate-vextractor-0.2 (crate (name "vextractor") (vers "0.2.2") (hash "07n0scisjpjvfk70x60jnqbbp7l5zi63ilc19wsvdrk1cv1mx51d")))

(define-public crate-vextractor-0.3 (crate (name "vextractor") (vers "0.3.0") (hash "1wzdygys12dmiyc9r7zcxawaqf3qid49hc8q9xbwxdyynll4cmbw") (yanked #t)))

(define-public crate-vextractor-0.3 (crate (name "vextractor") (vers "0.3.1") (hash "08ywqn19xy4ica0dm6ld1hyqia84v25r570rfql58vyc2kgkcd2m") (yanked #t)))

(define-public crate-vextractor-cli-0.2 (crate (name "vextractor-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "vextractor") (req "^0") (default-features #t) (kind 0)))) (hash "0ywwgkb3fcdszv9v8rkwh9zjhyzhd9bawga762p3djqscwrxp4zv")))

(define-public crate-vextractor-cli-0.2 (crate (name "vextractor-cli") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "vextractor") (req "^0") (default-features #t) (kind 0)))) (hash "16vx6r3svvlvrvn6b98bkh704a0vfdbmpnicp2swncgy15v3icfg") (yanked #t)))

