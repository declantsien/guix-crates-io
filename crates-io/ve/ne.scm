(define-module (crates-io ve ne) #:use-module (crates-io))

(define-public crate-veneer-0.1 (crate (name "veneer") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^0.4") (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "sc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "veneer-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ga8pzp2xbjavbxp7lc0ivq02j1im63d3sds4bkncplw07d5jgmh")))

(define-public crate-veneer-0.1 (crate (name "veneer") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^0.4") (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "sc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "veneer-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "11aw1gmpgzmhwba55rra635dxv1fa85p48l3pzra6541qa8a155l")))

(define-public crate-veneer-0.2 (crate (name "veneer") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "sc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "veneer-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pf9qqrqjyj8xdmlcb0ds8myyvbd9w19g315nbfxirh8gdibcx37") (features (quote (("rt") ("mem") ("default" "mem"))))))

(define-public crate-veneer-0.2 (crate (name "veneer") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^1") (kind 2)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "sc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "veneer-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1l9qdn4lfwpkiw891gr7vg5d7mn3kr5dy8p5iwr334321r02m2iv") (features (quote (("rt") ("mem") ("default" "mem"))))))

(define-public crate-veneer-macros-0.1 (crate (name "veneer-macros") (vers "0.1.0") (hash "0zv9l8fa6r1ijb4lnkm0dsf8sbicjkn0b5v52xyq972ahr216q1j")))

