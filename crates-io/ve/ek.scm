(define-module (crates-io ve ek) #:use-module (crates-io))

(define-public crate-veeks_millis-0.5 (crate (name "veeks_millis") (vers "0.5.5") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0h43qzfg35wbvdh7kdxz76yyg36ckpfc228xiaxz62rxg7qiz91r") (yanked #t)))

(define-public crate-veeks_millis-1 (crate (name "veeks_millis") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0cdn17585a6441n65y59idwnm5dslcjx71vxx7ckqdx7rfcsgaa8") (yanked #t)))

(define-public crate-veeks_millis-1 (crate (name "veeks_millis") (vers "1.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1ydqy7cvfmz6d31rzkchcd6dy78y8f5xw20f4hdi3s0cs8b3yk47") (yanked #t)))

