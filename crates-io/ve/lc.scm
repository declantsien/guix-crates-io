(define-module (crates-io ve lc) #:use-module (crates-io))

(define-public crate-velcro-0.1 (crate (name "velcro") (vers "0.1.0") (hash "0cw42c9dw4380k5q4qywnmsn11j5p63bdzx6cpj7kyaimqs2kk1m")))

(define-public crate-velcro-0.1 (crate (name "velcro") (vers "0.1.1") (hash "0iczb0y8i7bg58ygcddm3d6p9df1cjq9xz92wslz5c4xi5fwk6ky")))

(define-public crate-velcro-0.1 (crate (name "velcro") (vers "0.1.2") (hash "1x3bgbcj8nzdakqsnviyxrj8bj8iazhvirdcxggspg7zr4hjds6j")))

(define-public crate-velcro-0.2 (crate (name "velcro") (vers "0.2.0") (deps (list (crate-dep (name "velcro_macros") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "1zkwshpyfy0sps8z6fxzf5qbva21hn6wdx7pl44za05djw8hplg3")))

(define-public crate-velcro-0.3 (crate (name "velcro") (vers "0.3.0") (deps (list (crate-dep (name "velcro_macros") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "13l6zqyladjsdh2502crx3k4ihaism4wv853dv12kb2m2q1maa3s")))

(define-public crate-velcro-0.4 (crate (name "velcro") (vers "0.4.0") (deps (list (crate-dep (name "velcro_macros") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "18r2m1yhs34kl07fvxgg799kwxl7nxp7grhys6b13xmcx3gg6id9")))

(define-public crate-velcro-0.4 (crate (name "velcro") (vers "0.4.2") (deps (list (crate-dep (name "velcro_macros") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "0mbpd18214bpzqpm7c4mmnjzwd97g99v02i6k16pr2lljnsi8dv1")))

(define-public crate-velcro-0.4 (crate (name "velcro") (vers "0.4.3") (deps (list (crate-dep (name "velcro_macros") (req "=0.4.1") (default-features #t) (kind 0)))) (hash "1scb9jjzy40jkv2vp7pn4cfhk8iflszlrcf7g2d86bnm1fiqjhls")))

(define-public crate-velcro-0.4 (crate (name "velcro") (vers "0.4.4") (deps (list (crate-dep (name "velcro_macros") (req "=0.4.2") (default-features #t) (kind 0)))) (hash "0l7m85xsd7kaygk3wh7kpfy2lg29c5wn2ibkq5m0lswabhmhwsn1")))

(define-public crate-velcro-0.5 (crate (name "velcro") (vers "0.5.0") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "velcro_macros") (req "=0.5.0") (default-features #t) (kind 0)))) (hash "12c7kgdnpk4kg8jb4r9fyx2gvw5is4ajghr4kmnmzskbz3p08x8q")))

(define-public crate-velcro-0.5 (crate (name "velcro") (vers "0.5.1") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "velcro_macros") (req "=0.5.1") (default-features #t) (kind 0)))) (hash "02m8c84mll17nr6nddizj14lpmy2hngj0yx4c9b408k4viadcy5q")))

(define-public crate-velcro-0.5 (crate (name "velcro") (vers "0.5.2") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "velcro_macros") (req "=0.5.1") (default-features #t) (kind 0)))) (hash "0hsj12fbnvdlm2xpq0ispyv9lcb76wx4g05czmpxn5g1yd9hqlbm")))

(define-public crate-velcro-0.5 (crate (name "velcro") (vers "0.5.3") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "velcro_macros") (req "=0.5.2") (default-features #t) (kind 0)))) (hash "1gq7gqxsabcm1i3n66zlrcqnbpg14p2wfcav6dmrc9vm9cm4wmc5")))

(define-public crate-velcro-0.5 (crate (name "velcro") (vers "0.5.4") (deps (list (crate-dep (name "indexmap") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "velcro_macros") (req "=0.5.4") (default-features #t) (kind 0)))) (hash "1z4nc54ljgfij1ncw138ryg47mbchp26vq07fdsk845shccabiii")))

(define-public crate-velcro2-0.0.1 (crate (name "velcro2") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "elasticsearch") (req "^8.5.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12nfaagv9hjx023zkmq4jcm8ijgk8rqqv6vccijmazivqikwicqg")))

(define-public crate-velcro_core-0.3 (crate (name "velcro_core") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jxbvdhyb9zbzp23fl2ql6yw055jvwwgn4n940x925dm9dg8q80l")))

(define-public crate-velcro_core-0.4 (crate (name "velcro_core") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jply3viibsfl3k6rrd2ll2prrqd6lz42m7nn1xczi3rkv7j3vhl")))

(define-public crate-velcro_core-0.4 (crate (name "velcro_core") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05gacllz9n0s2xbl3a7xfk14h8chjksfd3ndhfwps5awnkajr635")))

(define-public crate-velcro_core-0.4 (crate (name "velcro_core") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15p3zxvm0d482w3ramj6hzpvkh23698y4gxavh4ndjzanawdfhk7")))

(define-public crate-velcro_core-0.5 (crate (name "velcro_core") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c9a46yfqhk3yb6z2zgir15h9j4mdn82rlfbnqmcnqhkyazwwx8v")))

(define-public crate-velcro_core-0.5 (crate (name "velcro_core") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "13hjhbp7xa0ngj6a0qx0xhavw5dc0pyw8pkgjafhz4yd684gja6b")))

(define-public crate-velcro_core-0.5 (crate (name "velcro_core") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "166ln0ixy2l7mhr74gyhifchi1jy6chflz675cwarhqd3nxmq4lr")))

(define-public crate-velcro_core-0.5 (crate (name "velcro_core") (vers "0.5.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "0v4ixhkg7liqn8b75x41bb3r0f4pm013c23yhwa7d6wq0xfz8b3l")))

(define-public crate-velcro_macros-0.2 (crate (name "velcro_macros") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ay366pk9lb9plf2kqjhhb9sps1idh6y29kn20lwym48m9hhikmz")))

(define-public crate-velcro_macros-0.3 (crate (name "velcro_macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "0xypcrg5k6121vsw7gqyfb7g1fahglwjqn6w9d8gnp3gmdrhj0lr")))

(define-public crate-velcro_macros-0.4 (crate (name "velcro_macros") (vers "0.4.0") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "0f7ik6kvabrf8jj1zhmsj53z72bbcc918j7gli8188d8pzxpg9bw")))

(define-public crate-velcro_macros-0.4 (crate (name "velcro_macros") (vers "0.4.1") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.4.1") (default-features #t) (kind 0)))) (hash "1h68yvq6z9h7h1npmxd9x5rifxx6203xlx2baasgnjlggf1vl0qk")))

(define-public crate-velcro_macros-0.4 (crate (name "velcro_macros") (vers "0.4.2") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.4.2") (default-features #t) (kind 0)))) (hash "0539kgm7105dna892z9sn6w86sfqxw995ar7w95cvzw8wwk8y22c")))

(define-public crate-velcro_macros-0.5 (crate (name "velcro_macros") (vers "0.5.0") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.5.0") (default-features #t) (kind 0)))) (hash "02nbd3c9j6dspqgic53wp28faqbynmmqc05nwn9dg5d8hlbwsjld")))

(define-public crate-velcro_macros-0.5 (crate (name "velcro_macros") (vers "0.5.1") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.5.1") (default-features #t) (kind 0)))) (hash "0c6wci2cl77swipk1h9qr3kv7knvma4aqw2sxd4sxjb12g8i20fm")))

(define-public crate-velcro_macros-0.5 (crate (name "velcro_macros") (vers "0.5.2") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.5.2") (default-features #t) (kind 0)))) (hash "010r6kg1xfq9cy1yigrb7s5cqqii6wahbgrrbv3kzl01jqzcpmnl")))

(define-public crate-velcro_macros-0.5 (crate (name "velcro_macros") (vers "0.5.4") (deps (list (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "velcro_core") (req "=0.5.4") (default-features #t) (kind 0)))) (hash "1346p2srlw56y7ym5iji5dqapp01mhhd3rifw7k7g6dlsw3ch8vv")))

