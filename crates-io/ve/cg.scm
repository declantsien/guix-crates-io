(define-module (crates-io ve cg) #:use-module (crates-io))

(define-public crate-vecgenericindex-0.1 (crate (name "vecgenericindex") (vers "0.1.0") (hash "0b0jj3bjqr2jiw66rl8b80v7j80qyivp9jsjn4ih11zxbnlgr1gm")))

(define-public crate-vecgrid-0.1 (crate (name "vecgrid") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "15ajcq0kkbxvpnpd3bgmhn56cx9gqyafn006v5zrx3qnljbxfpqg")))

(define-public crate-vecgrid-0.1 (crate (name "vecgrid") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d570nybbjkziwgigkgsk3klnhn89q7q9c1xnixwq208gl8z0km7")))

(define-public crate-vecgrid-0.1 (crate (name "vecgrid") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "19xrm1ba7d702kxki6j50xjz8di3043i1mg5ib2k5sixpfis0za2")))

(define-public crate-vecgrid-0.1 (crate (name "vecgrid") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0p6i9nkcz7lnhiv5hxcviz96vmvxx1kdxz23llikb0vrjws7f6w9")))

(define-public crate-vecgrid-0.2 (crate (name "vecgrid") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0l25fa05n1vramp6haq32v83k0rlxz4dqr5y26i1d6mpabadmc3z")))

(define-public crate-vecgrid-0.2 (crate (name "vecgrid") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ngpaf24j57la43mb58fw57pi74c50xmfszfbjsh08vhsp73nnrh")))

(define-public crate-vecgrid-0.2 (crate (name "vecgrid") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cc7icl9qbfn6hzmxsf45jpnjpdnp5k6w6phipizc7x5xzddaiax")))

