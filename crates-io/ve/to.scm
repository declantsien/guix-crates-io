(define-module (crates-io ve to) #:use-module (crates-io))

(define-public crate-vetomint-0.1 (crate (name "vetomint") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0saxm0vnhpyll0yfhd79h175dbpqx22zjlapsqfm2rb3ygx4ljch")))

(define-public crate-vetomint-0.2 (crate (name "vetomint") (vers "0.2.0") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0hlrq41y3hqc5hrkql61m0wrwwwgx89ma00maiah8j9h8ghxs6bj")))

