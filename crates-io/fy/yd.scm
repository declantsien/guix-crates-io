(define-module (crates-io fy yd) #:use-module (crates-io))

(define-public crate-fyyd-0.0.0 (crate (name "fyyd") (vers "0.0.0") (hash "0k8yml854wjmj6yfdm9lphhcryvpl5pnmdzlcy90g8zqp5mm48md")))

(define-public crate-fyyd-api-0.1 (crate (name "fyyd-api") (vers "0.1.0") (hash "0y4n7qcfzcbbw1qhm57b17ga1di389n1qgrl4bavyscdycss6s7z")))

(define-public crate-fyyd-api-0.1 (crate (name "fyyd-api") (vers "0.1.1") (deps (list (crate-dep (name "fyyd-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wi7na53yi2p2a67vv35g8n573r40brb22qq2plrp48hry57awmp")))

(define-public crate-fyyd-api-0.1 (crate (name "fyyd-api") (vers "0.1.2") (deps (list (crate-dep (name "fyyd-types") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vfp2pkh3if1bqrhcygscjilrfbh2ib333h08a02n2s4zfrgx5hd")))

(define-public crate-fyyd-api-0.1 (crate (name "fyyd-api") (vers "0.1.3") (deps (list (crate-dep (name "fyyd-types") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0ikjbxq8b4grqww56xlzpw97gpxs6i1fvwa5pilmnxym8im02l49")))

(define-public crate-fyyd-types-0.1 (crate (name "fyyd-types") (vers "0.1.0") (hash "0iiy6whc4pgkss6wfjyk5nxbhdn18jn8vbrmxqbiy42rdx9rc3vi")))

(define-public crate-fyyd-types-0.1 (crate (name "fyyd-types") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f8md0a4k5l8zgdyhac6f0jjpb0xaxrdqs2v0gry37kpr8zvi0sw")))

(define-public crate-fyyd-types-0.1 (crate (name "fyyd-types") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x8j14hvnhylfncvh7524h04f204idywi13ynfizd3xqh1zbqp74")))

