(define-module (crates-io fy lo) #:use-module (crates-io))

(define-public crate-fylorg-0.0.1 (crate (name "fylorg") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.0.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0xg5r8nff4qq8xxigc4sqahh6v7wrg4p7458k8lk57iyrx97dasc")))

