(define-module (crates-io fy re) #:use-module (crates-io))

(define-public crate-fyre-0.1 (crate (name "fyre") (vers "0.1.0") (hash "00qsrc8gvfga1phvf5anf6nw0r03jmrn47jkbng9pi3r92k3d6il")))

(define-public crate-fyrelog-0.1 (crate (name "fyrelog") (vers "0.1.0") (hash "0qxv4m84328fw1ndais6ajva1rp94n3jnppkfzg4kiam19p7jwr7")))

