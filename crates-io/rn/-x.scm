(define-module (crates-io rn -x) #:use-module (crates-io))

(define-public crate-rn-xlsx2csv-0.1 (crate (name "rn-xlsx2csv") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "1kbc5y62w07iwmm7gjny1jmz1lfvb6fb6d8qd8iqs255lbxwnqhq")))

(define-public crate-rn-xlsx2csv-0.1 (crate (name "rn-xlsx2csv") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "017qsjrlpcqfzaz04ydll266zmxg3n3nywxqcglzh2wdxrmclj9i")))

(define-public crate-rn-xlsx2csv-0.2 (crate (name "rn-xlsx2csv") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "0mgrvbiwzn549z8dccm768z24hwdmzfiwnp2z0g9q6j9i72w71av")))

