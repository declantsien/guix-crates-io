(define-module (crates-io rn #{29}#) #:use-module (crates-io))

(define-public crate-rn2903-0.1 (crate (name "rn2903") (vers "0.1.0") (deps (list (crate-dep (name "quick-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^3.3") (default-features #t) (kind 0)))) (hash "0hkqf54gr5wngvqz8qqc4gwhzpxraisc75rajz1nlv0ji9x199kx")))

(define-public crate-rn2903-0.2 (crate (name "rn2903") (vers "0.2.0") (deps (list (crate-dep (name "quick-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^3.3") (default-features #t) (kind 0)))) (hash "08j6dwvy6x40vzdw2k5madhy74lav768dsrnz05gdaamnk2dakky")))

