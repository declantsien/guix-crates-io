(define-module (crates-io rn pm) #:use-module (crates-io))

(define-public crate-rnpmrc-0.1 (crate (name "rnpmrc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "18qc039yckja7wz0sazz7cvplylgx68xlsvjhihxazy5igxqxdf6")))

(define-public crate-rnpmrc-0.2 (crate (name "rnpmrc") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1p3dal841pbc9kssn4hbslnwfkgndvpkjwbbxy7phmh62ykj7z2p")))

(define-public crate-rnpmrc-0.3 (crate (name "rnpmrc") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "06a6pccqa1am93g4iqnn3nidfm6zkwjh4mi16x1l5qlyg46sqi1s")))

(define-public crate-rnpmrc-0.4 (crate (name "rnpmrc") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "12ixywz3lrlsj9426cmzbgrabjfk8fwppp57xpvj1vdgcck491ja")))

