(define-module (crates-io rn #{48}#) #:use-module (crates-io))

(define-public crate-rn4870-0.1 (crate (name "rn4870") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1jzvfh20bpazrkhflhyg2dhra8887lsxlqsya5r748dws2gys8yy")))

(define-public crate-rn4870-0.2 (crate (name "rn4870") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1l5vwcy5wg2p6ascjm24lhvxzjphlv236pyf33z187x9kcsfkcwn")))

(define-public crate-rn4870-0.2 (crate (name "rn4870") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1llimazwsjbqq47bfixa34cwfsgxwpj84vcysx38bm6a1ij7zfcq")))

(define-public crate-rn4870-0.2 (crate (name "rn4870") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ig65h01vqzbwy3j0b0n79iwwz7dm7dsq97by27lhairhkj2qxkl")))

(define-public crate-rn4870-0.3 (crate (name "rn4870") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0qfhhb2viqlw0yxlm1rw62hds6a6gg00b2mrrjicpl3lgmnr3ypj")))

