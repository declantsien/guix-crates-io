(define-module (crates-io rn cr) #:use-module (crates-io))

(define-public crate-rncryptor-0.1 (crate (name "rncryptor") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "rust_sodium") (req "~0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1q5j839q8rr5anmzwggjhk99hx9v3y5j54w83hsf8vnj0mm7wvhp")))

