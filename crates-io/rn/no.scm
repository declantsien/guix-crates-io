(define-module (crates-io rn no) #:use-module (crates-io))

(define-public crate-rnnoise-c-0.1 (crate (name "rnnoise-c") (vers "0.1.0") (deps (list (crate-dep (name "rnnoise-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1khhnbb0lqlr0v6r992ms8qnnly33k7dxlkxsd253mv98hw6hdb1")))

(define-public crate-rnnoise-c-0.2 (crate (name "rnnoise-c") (vers "0.2.0") (deps (list (crate-dep (name "audrey") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "rnnoise-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.8") (default-features #t) (kind 2)))) (hash "1dmdyjdvcg29bmpxsnbys39jx2ilmqgm8k7hzpdl3pmazkk0pizg")))

(define-public crate-rnnoise-c-0.2 (crate (name "rnnoise-c") (vers "0.2.1") (deps (list (crate-dep (name "audrey") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "rnnoise-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.8") (default-features #t) (kind 2)))) (hash "0mdd93v6sh253mxkxh1vj87ajwr81sgq750110v7ckfmqh6xza9p")))

(define-public crate-rnnoise-sys-0.1 (crate (name "rnnoise-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0") (kind 1)))) (hash "01k3nl21n7x7dmxidclm5i0k1a22bdc9y5xg1605xrqp7wfjdd8g")))

(define-public crate-rnnoise-sys-0.1 (crate (name "rnnoise-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.52") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0") (kind 1)))) (hash "1a8w0jxdhyyf4vwyj2zvq70qh296qaaaaasa03f4py0nn0cc5am5")))

(define-public crate-rnnoise-sys-0.1 (crate (name "rnnoise-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.53") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0") (kind 1)))) (hash "1aid5m22kdfv9yh3a12z2bpmg0n6jp9skwlvkhvh3pc97mzql5j4")))

(define-public crate-rnnoise-sys-0.1 (crate (name "rnnoise-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.53") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0") (kind 1)))) (hash "00nrbrgvs7ij6c20hcwlgcj4pb5jknkn16pxcav738qgc80wh7zq")))

