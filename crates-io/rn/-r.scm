(define-module (crates-io rn -r) #:use-module (crates-io))

(define-public crate-rn-run-0.1 (crate (name "rn-run") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rkc6gkl1815f8hn41x5m1g7623k25x2hc8cvki9bvr2w2b2pw7h")))

(define-public crate-rn-run-0.1 (crate (name "rn-run") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "007wiyh38862j4k62ghqxsz9np5ic7fabdnd2q2n3fk8n0ly0ykn")))

