(define-module (crates-io rn dm) #:use-module (crates-io))

(define-public crate-rndmator-0.1 (crate (name "rndmator") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1507vhg3niyp2wvcvxhqdk6pzlk1bggsqhyyvfdqy2v56zdsdr26")))

