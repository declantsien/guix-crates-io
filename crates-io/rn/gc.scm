(define-module (crates-io rn gc) #:use-module (crates-io))

(define-public crate-rngcache-0.1 (crate (name "rngcache") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1vccsqi08nfrssbvdma8p2kia8bk95zp26fl3qv50pdrrnai4pys")))

(define-public crate-rngcache-0.1 (crate (name "rngcache") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "150d3kwxah3gf797ynihza44zkkd9jcnahhn9dldh2s7jiwbhvbx")))

(define-public crate-rngcache-0.1 (crate (name "rngcache") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "007dqwsa45g71grv3fyaw89grgvp2vxkh562icsshcg6w16bgd9f")))

(define-public crate-rngcache-0.1 (crate (name "rngcache") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0lcarhmh6jblsrdc2b8k4mrdmlg4wpjw5sqmy5c7yv41f2cf0279")))

(define-public crate-rngcheck-0.1 (crate (name "rngcheck") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.11.0") (kind 0)) (crate-dep (name "libm") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "std_rng"))) (default-features #t) (kind 2)))) (hash "03ra1i51hmph0l9nl5cpdzngnsgwagasa94jzxzx90k4q9cxrg6z")))

(define-public crate-rngcheck-0.1 (crate (name "rngcheck") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "std_rng"))) (default-features #t) (kind 2)))) (hash "00sjdcs3mj0rlfcihfqsky4x101gbxsyl361qvvhzsv6059sw28y")))

