(define-module (crates-io rn gs) #:use-module (crates-io))

(define-public crate-rngstr-0.1 (crate (name "rngstr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cli-clipboard") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m7fr13rz2dm93cjn0p8myghy1xr64nj16l2jwl46g978f8gwgf5")))

(define-public crate-rngstr-0.1 (crate (name "rngstr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cli-clipboard") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1irrssahikp1zjhzq49k4qlc9f5vh9s1j2nvmb78s654p8g5yal3")))

(define-public crate-rngstr-0.2 (crate (name "rngstr") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cli-clipboard") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1zf292gahli5p70g9fp6kr77w52ngs0nwhyzf10nziia893aimsn")))

