(define-module (crates-io rn am) #:use-module (crates-io))

(define-public crate-rname-0.1 (crate (name "rname") (vers "0.1.0") (deps (list (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "16idjbdvs502h98acp528nz317s20hqrq7dsp1q716s7f0n5by7l")))

