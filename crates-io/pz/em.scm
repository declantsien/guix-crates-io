(define-module (crates-io pz em) #:use-module (crates-io))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "15siy043k9v771psksfwinjd296908rgc38b12yv3d8hyfpqv5w7")))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "10mxzibiw8lbxywmlrrvpnqdk4c6vy0v92ikm1wwjas7ss514bzz")))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.2") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "1y4g3z857ric936634kw3wa9l9bhiw7n3x9azwjwd8cikza6s33i")))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.3") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "0np1wckkhzmxqzgr5ixvxi180axfjs3khd5b8r8zds9hq8kbmahr")))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.4") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "1yhx8c4if6gldb58fi5x5x12hy0qipd1rsv5gm0bprvp9rmm0m4c")))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.5") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "0sl2pvk381k4kh2i64qja549y84yskp5z94m72094lvvlxmjjwls")))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.6") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "1rl1crr42w960h4md32drrd577daw7a31wf7ap7pyjalyswkvgwi")))

(define-public crate-pzem004t-0.1 (crate (name "pzem004t") (vers "0.1.7") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1.0.2") (kind 0)))) (hash "18q7jrvyw5wqnppdm54jfwhmchbl25mc978yr59hmq8gq2ylgj82")))

