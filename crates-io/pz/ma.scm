(define-module (crates-io pz ma) #:use-module (crates-io))

(define-public crate-pzmacro-0.1 (crate (name "pzmacro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "170ffv9qqrlv0dfdbzfbw3f757vindd60h21c5ns0y7bffvnciyp")))

(define-public crate-pzmacro-0.2 (crate (name "pzmacro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0vr3r8k3zf0af85ik4mkqlp1lk1vir9zgdiqc9xvvd1f06pinpcp")))

(define-public crate-pzmacro-0.3 (crate (name "pzmacro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0acbxw5wi3ax3dwb0sknisdb2nnq0higrrqc9pr9q6qi2cr7d52h")))

(define-public crate-pzmacro-0.4 (crate (name "pzmacro") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("parsing" "extra-traits" "full" "visit"))) (default-features #t) (kind 0)))) (hash "12ss899956r3csvn917ard1jy5vzyxa34zj7gklsd17ib514h4n1")))

(define-public crate-pzmacro-0.5 (crate (name "pzmacro") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("parsing" "extra-traits" "full" "visit"))) (default-features #t) (kind 0)))) (hash "00f3wm1nvn6f4a68yfq8dpxil2aqyx6qcsga0y4mqkw4l2yjnv27")))

(define-public crate-pzmacro-0.6 (crate (name "pzmacro") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("parsing" "extra-traits" "full" "visit"))) (default-features #t) (kind 0)))) (hash "118i25zglsrlqyi10isb2yyy2jzdd8w57azxs9w26ds4ixsv32l1")))

