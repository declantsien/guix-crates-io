(define-module (crates-io pz n_) #:use-module (crates-io))

(define-public crate-pzn_say-hello-0.1 (crate (name "pzn_say-hello") (vers "0.1.0") (hash "04231ngbj6xmc08hdbzxsw4h3wbh13d5afv31g6yh4dd836jdyxs")))

(define-public crate-pzn_say-hello-0.2 (crate (name "pzn_say-hello") (vers "0.2.0") (hash "0y55a26j68f9ddg5y4c08lvkd4ji6l0grvk52rvhdsim5lf72hn1")))

(define-public crate-pzn_say-hello-0.3 (crate (name "pzn_say-hello") (vers "0.3.0") (hash "1si12ypzpvkw813im9ms4fy37si73v6rm81pva103wmbmgkb70qw") (features (quote (("hello") ("bye") ("all" "hello" "bye"))))))

(define-public crate-pzn_say-hello-0.4 (crate (name "pzn_say-hello") (vers "0.4.0") (hash "1y9b0ihhkw02m06l51ciyhrfwh0pyvhl25cljcviwg5if6s0f35m") (features (quote (("hello") ("default" "hello") ("bye") ("all" "hello" "bye"))))))

