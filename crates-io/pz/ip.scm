(define-module (crates-io pz ip) #:use-module (crates-io))

(define-public crate-pzip-0.1 (crate (name "pzip") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.4.2") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "~0.16.14") (default-features #t) (kind 0)))) (hash "0ypzslahakqb1ykl94x59x476rvfkfd2av4ydnm03v1r7p091a2k")))

(define-public crate-pzip-0.2 (crate (name "pzip") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)))) (hash "0ij3j51irs6lj0wqacvjglr3r0yajapyl8jf1rk68i86wbhygy2h")))

(define-public crate-pzip-0.2 (crate (name "pzip") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.4") (default-features #t) (kind 2)) (crate-dep (name "libdeflater") (req "~0.8") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.17.0-alpha.11") (default-features #t) (kind 0)))) (hash "0ifrjx9kk8nyr844p85vg8xsw9783gr7af7gnq2prbz2p0wvprpr")))

(define-public crate-pzip-bwt-0.1 (crate (name "pzip-bwt") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1zwi3x3hz1p7wbwcia25wkq2zd7hc1rsrmyl8d8radf2accv0p8f")))

