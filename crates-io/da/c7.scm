(define-module (crates-io da c7) #:use-module (crates-io))

(define-public crate-dac7571-0.1 (crate (name "dac7571") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "188shk208ya2m5ly5g07mfidr90fcxrv9bj51h0cp8z92r0dqglj")))

