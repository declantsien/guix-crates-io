(define-module (crates-io da ya) #:use-module (crates-io))

(define-public crate-dayan-0.0.0 (crate (name "dayan") (vers "0.0.0") (deps (list (crate-dep (name "latexify") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "pex") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1w5pyl69iki1ibhk4lfxy3fas7ki88y9h1rl2nrys7c01jh1v65c") (features (quote (("default"))))))

(define-public crate-dayan-0.0.1 (crate (name "dayan") (vers "0.0.1") (deps (list (crate-dep (name "latexify") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "pex") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0dj40ngp5g7395wq7wvxqyfsl2x2j6wqxn24hz9nra6cn0fvwk4f") (features (quote (("default"))))))

