(define-module (crates-io da li) #:use-module (crates-io))

(define-public crate-dali-0.2 (crate (name "dali") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "luminance") (req "^0.33.0") (default-features #t) (kind 0)) (crate-dep (name "luminance-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "luminance-glfw") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "12gzivn4sg60imda1ijawx8hr1hxxjfy1vji8grb704y7d9wxsgh")))

(define-public crate-dali-0.2 (crate (name "dali") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "luminance") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "luminance-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "luminance-glfw") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "054204kym7z1c38inxphp4grmv8g26j8lyqkbdvr2xckdxz23kh0")))

(define-public crate-dali-0.2 (crate (name "dali") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "luminance") (req "^0.34") (default-features #t) (kind 0)) (crate-dep (name "luminance-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "luminance-glfw") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1igdyjkfvvkpn7z5x3lj77hagpvlm9jfi6kvi3irr16sl1p34a7f")))

(define-public crate-dali-0.2 (crate (name "dali") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "luminance") (req "^0.35") (default-features #t) (kind 0)) (crate-dep (name "luminance-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "luminance-glfw") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "14zrx604mmdbrpjpq69yi4268v1323xmn35df7i013jnrfgv0cj2")))

(define-public crate-dali-0.3 (crate (name "dali") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.22.1") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.19.2") (default-features #t) (kind 2)) (crate-dep (name "luminance") (req "^0.37.1") (default-features #t) (kind 0)) (crate-dep (name "luminance-derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "luminance-glfw") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1v3lks2s14jwznm85ik08ii93131fywvrr53jjnzdmzmx79gdmnl")))

(define-public crate-dalia-1 (crate (name "dalia") (vers "1.0.0") (deps (list (crate-dep (name "shellexpand") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "14sf8b5ihc99asmq7m25494k2xm3nkq1p041mwp7sypm0af66a1h")))

(define-public crate-dalia-1 (crate (name "dalia") (vers "1.0.1") (deps (list (crate-dep (name "shellexpand") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0sp9k93mf387p7zgqsih2ksxrn38whjqcf7v19n6zc4a87z8shvp")))

(define-public crate-dalia-1 (crate (name "dalia") (vers "1.0.2") (deps (list (crate-dep (name "shellexpand") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0yhr1a9lsan6lvv3rxsazipc0046v8f6h0zff8c18li8dh2capbl")))

(define-public crate-dalia-1 (crate (name "dalia") (vers "1.1.0") (deps (list (crate-dep (name "shellexpand") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2") (default-features #t) (kind 2)))) (hash "1i7bwh0bk4vhcvd6wz624h29xhrw461ql630c9lbi9qnngazaml8")))

(define-public crate-dalia-1 (crate (name "dalia") (vers "1.1.1") (deps (list (crate-dep (name "shellexpand") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2") (default-features #t) (kind 2)))) (hash "1rnk301z5n11gf0fk4d6dhq1xq5ln86fwmwsh7w7lmjwb9pjbzy8")))

(define-public crate-dalia-1 (crate (name "dalia") (vers "1.2.0") (deps (list (crate-dep (name "shellexpand") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2") (default-features #t) (kind 2)))) (hash "1i3yah5imz8mkja83gygi480xsx6fbps60lsaidccg432kkig00b")))

