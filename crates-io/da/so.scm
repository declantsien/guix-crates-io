(define-module (crates-io da so) #:use-module (crates-io))

(define-public crate-dasom-0.1 (crate (name "dasom") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "overload") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0rhvgqdx3glm7sp7hh3riz81cqqicbxx6ba7x1fllq3sjck37b50")))

(define-public crate-dasom-0.1 (crate (name "dasom") (vers "0.1.1") (deps (list (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "overload") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1j68sv2brsbd4c960c13gcn663i7w9brrnvzffrj59wznpiviz2f")))

