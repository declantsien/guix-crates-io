(define-module (crates-io da nd) #:use-module (crates-io))

(define-public crate-dandelion-0.1 (crate (name "dandelion") (vers "0.1.0") (hash "08mqswn63wawglybi2w58p7fp4vlkwpg4zw6xnzzbvsn6sxzja22")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1b1qb62d9wlpdziihbz281943p9vz1lhfgsyp2c6yysav0amw1md")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0af2kzmcc8fi24ni8fwb33r1v5lwyiykaafbm4zxpm3vh328bxh3")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0wd7nfisnngcjd3yipyng5sb5l0dx4smfm0ljgmsxrz2fzbvi5xw")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1mzyp4i8f2q89a8qbbmjwpn569iy11v2baq7j20hzyi6vgvddlfg")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.4") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0kajg0w8rf5qn6qj38vlai30z0bwz2g6c2f1qcc40kkv6990i036")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.5") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0fgsg7jcy1jgqbpmdj098ybfzc7jazfhzwqq5jh712v2rs6663lw")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.6") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0a3kmxljbp8gbq63lyd20ppa1rh5nvg15szqd3f4iiimknmmwl24")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.7") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0nqhy3lp22cbc7qnvj0jp6ng72hgjr0n2kkpwj1ssq165qmmkmx4")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.8") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "0kvbfzslzd88bzm50bni0v9c30d5zfkvbks5zcif89w5pd5yd7mp")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.9") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1cxkxzjd9xjhr8kivxkbdfvy4lq36yfxl57qq7cbawv8dbvlflpk")))

(define-public crate-dandy-0.1 (crate (name "dandy") (vers "0.1.10") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1zlq7gdxd3arnw9x5kib2hm63pqcy2rj49d8iziczpxrl1w64x3c")))

