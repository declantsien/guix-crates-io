(define-module (crates-io da nc) #:use-module (crates-io))

(define-public crate-dance-0.1 (crate (name "dance") (vers "0.1.0") (hash "1az6368f6jasmv7j7j82slrrq4m3s7pia7nvpvdsbnlify0zwqsz")))

(define-public crate-dancewithpeng_hello_cargo-0.1 (crate (name "dancewithpeng_hello_cargo") (vers "0.1.0") (hash "1nlbjjlrbh31gxz589x0an1ghlvycn3lzj5gnrjwj6nak289nr60")))

(define-public crate-dancing-cells-0.1 (crate (name "dancing-cells") (vers "0.1.0") (hash "160gaa21l2h29674b9mb6kdxfjw6990sjchfxy8y40zsdzi4xiwz")))

(define-public crate-dancing-links-0.1 (crate (name "dancing-links") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.4") (default-features #t) (kind 0)))) (hash "13dj1dx3yy9iqwv0z0l0mm07z75wcc64z7r2alpxvm3frkyhfajd")))

(define-public crate-dancing-links-0.1 (crate (name "dancing-links") (vers "0.1.1") (deps (list (crate-dep (name "bumpalo") (req "^3.4") (default-features #t) (kind 0)))) (hash "0h836y54glj3qzyfrr0k0764vak7cxssfrlklidqx9mih787ncjx")))

(define-public crate-dancing-links-0.2 (crate (name "dancing-links") (vers "0.2.0") (deps (list (crate-dep (name "bumpalo") (req "^3.4") (default-features #t) (kind 0)))) (hash "1jkcjlglgj926md563g9v9kl3jpd6m9rsnd2gx8rf2arnngaslpv")))

(define-public crate-dancing-links-0.3 (crate (name "dancing-links") (vers "0.3.0") (deps (list (crate-dep (name "bumpalo") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "13m9zh34nvmid7wh2jiicx33s34p1ip2lni583gvk9kbqi6jp6h5") (rust-version "1.60")))

(define-public crate-dancing_crate-0.2 (crate (name "dancing_crate") (vers "0.2.2") (hash "03arhlddlf8qx119nkk8asizhcxzwlgpz833i6skbyl6jd8xs8i7")))

(define-public crate-dancing_crate-0.2 (crate (name "dancing_crate") (vers "0.2.3") (hash "0a6pcqvinkb55235yn1xqxaikfd6aa9h6x7vq6sfv95kxrvvslv7")))

(define-public crate-dancing_crate-0.0.2 (crate (name "dancing_crate") (vers "0.0.2") (hash "0dfdw648ixlag30gvg465gdml8c1hvryqs76lp1zjb5mdvh4h7bl")))

(define-public crate-dancing_crate-0.2 (crate (name "dancing_crate") (vers "0.2.4") (hash "1qmvyngxja8xbybpavnf8x4ncbgihymyhs58vpkqn5by0qa9v2rx")))

