(define-module (crates-io da c5) #:use-module (crates-io))

(define-public crate-dac5578-0.1 (crate (name "dac5578") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "06lrz580mja92h69qjq42vccfii80690yfb874c0nwm55bpc4j49")))

(define-public crate-dac5578-0.1 (crate (name "dac5578") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1p66bxp6j99c9672bxzx6dba04r0krxgmsylbz6dic9nlfwclzij")))

(define-public crate-dac5578-0.2 (crate (name "dac5578") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "03iqpf7mi5qvhhmkz246qq76iliainhz9y1j2al9imdw9ilxcnlg")))

(define-public crate-dac5578-0.2 (crate (name "dac5578") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "060szqhdjwmdpq3dzq8g44cgmhxjhpg6j4qq45adawr7hy4q57rw")))

