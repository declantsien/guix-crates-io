(define-module (crates-io da ma) #:use-module (crates-io))

(define-public crate-damaru-0.0.0 (crate (name "damaru") (vers "0.0.0-dummy") (hash "1yfn64f946nyavgz0l0q5mx2lx6hq66d7a8s3wdydkzihckn6vi5")))

(define-public crate-damaru-cli-0.0.0 (crate (name "damaru-cli") (vers "0.0.0-dummy") (hash "1xr7vzsvlgbx5bwiav3fbcvq1w5djycba16pcxllqqlbggl32kkh")))

(define-public crate-damaya-0.1 (crate (name "damaya") (vers "0.1.0") (hash "0bha7zxfs91sazyml6584p8mwbjyph5mi2hnwq2hgrzapdg7mf0h")))

