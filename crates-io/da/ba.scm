(define-module (crates-io da ba) #:use-module (crates-io))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1czv78l1n2wk867h0b6gcsa5xf7iivv15vypbzhp198s7d33k2n9")))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "168n8xk66c64jmb6jgp8p8x4l9rzsda514c22p8lgzznszxb3hfd")))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1ymkjs5zadldwqihkpkmyfb1w2ryy1l64cq0n3jqrp04dafrn01v")))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1qw375mi8ynipfavlmmdqj59sg02l44nkka8v0mvlw37qg9g6fsn")))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1nvkvb3sbrnp3rvx5yibplqhvnvmc01kkyz1mnw58grm4bv5axkc")))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "17dgzj1xq21bjw7bnbkgkv1d9i196sw16ij4ps59gdazcd3kavdm")))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0krda1xgadhp5ha3rq8scmwlvfvamc41fpxpwbgs12zicv6vm2px")))

(define-public crate-dababy-0.1 (crate (name "dababy") (vers "0.1.7") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1fnfsr1ppb51n0wjxh0vlqzlr7bzsxd53ia18vspwy32a3mzisip")))

(define-public crate-dababy-0.2 (crate (name "dababy") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0px9q066rcsdhz7x3hqa4jgfx379l81b6av4jn12hjv6idc6sgj5")))

(define-public crate-dababy-0.2 (crate (name "dababy") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1xqsd5afnxhq7l1hm4vsh22gfq5y320ybx1br2fyvb4hmwchzp55")))

