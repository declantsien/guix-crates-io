(define-module (crates-io da da) #:use-module (crates-io))

(define-public crate-dada-0.0.1 (crate (name "dada") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "10fxlsg6b9cxk62fvx6910jm1c324h70sfqqa9nigxqhn5z120zi")))

(define-public crate-dada-poem-generator-0.1 (crate (name "dada-poem-generator") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "04m2namqgvhi8iylfsj44d4yl5crx5msq66r7ps0blxdz4zmykz2")))

(define-public crate-dada-poem-generator-0.1 (crate (name "dada-poem-generator") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "070hb2acf2nkc4x761mfm927mw1as7w0ww37dqw5hyf69pdr6h26")))

(define-public crate-dadada-0.9 (crate (name "dadada") (vers "0.9.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "11y9rh0cii3hs1i2f5cg9vdgxqfmjasrw52rk28rfpkndyzdd1h6")))

(define-public crate-dadada-0.9 (crate (name "dadada") (vers "0.9.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0fdlxhxk9qwpsrafw9428gdabb41fir8icdiksdv4mk1q181z6m2")))

