(define-module (crates-io da hu) #:use-module (crates-io))

(define-public crate-dahufi-0.1 (crate (name "dahufi") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "clap_conf") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "gobble") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "1m6bg1mh8y8dv9vh8x3klhwgaysc9gzgz6g76lab7bhlynf3vz8r")))

