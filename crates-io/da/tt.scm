(define-module (crates-io da tt) #:use-module (crates-io))

(define-public crate-datta-0.1 (crate (name "datta") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1drp93rz1xbjh3ix3i49l5bqqs39dy9q4dk8jw62n8j0599226rv")))

(define-public crate-datta-0.1 (crate (name "datta") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "02k9cyndhga58srhhaq7bxfqsnfag8c55ikl9w03a7gh5ppbd5r8")))

