(define-module (crates-io da nk) #:use-module (crates-io))

(define-public crate-dankboi-0.1 (crate (name "dankboi") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1cw3cpwmc446l3pl144adq780yv117mp6wafpgz6php9hwwvx7xp")))

(define-public crate-dankboi-0.1 (crate (name "dankboi") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0p2ich7zp5nz05kx2ry81yzirhn1a6fq7jkrwrqwlzk4l5drj82j")))

(define-public crate-dankboi-0.1 (crate (name "dankboi") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "01g4yhc5gjcyrwvx6hay555jiwhz2qx2mx4kpqkgmqyqjxkq4g4k")))

