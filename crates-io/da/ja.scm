(define-module (crates-io da ja) #:use-module (crates-io))

(define-public crate-dajarep-0.1 (crate (name "dajarep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.31") (default-features #t) (kind 0)) (crate-dep (name "lindera") (req "^0.18.0") (features (quote ("ipadic"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "10rhais9kwrl9jbbjkc2psmcg4lzp4avjildj88pwcvf8my8q6bx")))

