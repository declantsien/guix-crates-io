(define-module (crates-io da de) #:use-module (crates-io))

(define-public crate-dade-0.1 (crate (name "dade") (vers "0.1.0") (deps (list (crate-dep (name "dade_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 2)))) (hash "18bf133dpqcldqr5ikrss0d0iscy2c0skmyr4qq7jmw03yklhm88") (yanked #t)))

(define-public crate-dade-0.1 (crate (name "dade") (vers "0.1.1") (deps (list (crate-dep (name "dade_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 2)))) (hash "06jr8r4zv4jlwswrb78yjnps4bvj51pbgmzw0j06vcxkq7lvx9a4") (yanked #t)))

(define-public crate-dade-0.1 (crate (name "dade") (vers "0.1.2") (deps (list (crate-dep (name "dade_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "03xn085zqm2bgc0n10y9qmqmmi3mmkpvznz19pkma3vn5s86q31n") (yanked #t)))

(define-public crate-dade-0.1 (crate (name "dade") (vers "0.1.3") (deps (list (crate-dep (name "dade_derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "12m3z64c7zhqfdbbgjh7zggmp54yxxmgfxhnz3l8f81qajjvdvia") (yanked #t)))

(define-public crate-dade-0.1 (crate (name "dade") (vers "0.1.4") (deps (list (crate-dep (name "dade_derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "19dnpmyffr17hy22w7fn219qy7pwxswijyn4yys8gc6ry0cc7c61")))

(define-public crate-dade-0.2 (crate (name "dade") (vers "0.2.0") (deps (list (crate-dep (name "dade_macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "0z4ziks4jwk5a3xnww53lbvp6wwzz6mqfw0p8rgpzcsil0q6czbi")))

(define-public crate-dade_derive-0.1 (crate (name "dade_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xy0kdpwj40c07959dqsf1hmpra41hcsf91ad4cfbsf2cl8hgypg") (yanked #t)))

(define-public crate-dade_derive-0.1 (crate (name "dade_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04kmr4lc17bp27hqvw879x858wy1hkajbgbqdm8sqj0p07c7b7y4") (yanked #t)))

(define-public crate-dade_derive-0.1 (crate (name "dade_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08hb1l081iqpzzbxxn24sm6d2jyjjdcma8my8qpmgy92imyd8h46") (yanked #t)))

(define-public crate-dade_derive-0.1 (crate (name "dade_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dqka2y911nvgp95m4vb5h1f7m7sz4hy0mj32yf6930rrmr7bf8n") (yanked #t)))

(define-public crate-dade_derive-0.1 (crate (name "dade_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mr5469kbzxlxzvm20sllcpa5mr6ilp4ax9178vg3ykg1brfchv1")))

(define-public crate-dade_macro-0.2 (crate (name "dade_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18jrzkix4l687g2hd3s6hf6ci7g0f014wpymvvsyhbknxf19450l")))

