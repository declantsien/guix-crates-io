(define-module (crates-io da s-) #:use-module (crates-io))

(define-public crate-das-grid-0.1 (crate (name "das-grid") (vers "0.1.0") (hash "04fnkha8vqr7i8ac4b8424m18k5cnya6w5yqnc8z1iq0sn6jb3xv")))

(define-public crate-das-grid-0.1 (crate (name "das-grid") (vers "0.1.1") (hash "1k4s9v61nlmvv7w06gfw1mml0xxb7d7xbjilvlc7g57zxw5pjkxs")))

(define-public crate-das-grid-0.1 (crate (name "das-grid") (vers "0.1.2") (hash "1nbfgl65x1yjnn8rsy8ggv7l0qzm4agwdvvdvkmlf3g9g95xzgjh")))

(define-public crate-das-grid-0.1 (crate (name "das-grid") (vers "0.1.3") (hash "1psnp4j1zbc22acz3wy4qps3434fv7fm0n0mgdpd8cjgyrqsv2np")))

(define-public crate-das-grid-0.1 (crate (name "das-grid") (vers "0.1.4") (hash "15c6pyhmim79ng5mb33kv6wzclgn33ybabwzm801hwp43p0pyscl")))

(define-public crate-das-grid-0.1 (crate (name "das-grid") (vers "0.1.5") (hash "0vb9ns9932faz36hvhs9nrq0kk2q63bmc0zg5n84n2rnfrswph1s")))

