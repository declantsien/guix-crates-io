(define-module (crates-io da pp) #:use-module (crates-io))

(define-public crate-dapper-0.0.1 (crate (name "dapper") (vers "0.0.1") (hash "0xgvr9y6kwv51g2fp4jr1arnzrk1s41lqd8jvrx07k0bw78wqk9l")))

(define-public crate-dapper-fw-0.0.1 (crate (name "dapper-fw") (vers "0.0.1") (hash "1j6b0wh3iiyafgzvrv0g3ix1709g6sjkrrkdgcf82wf5qy9y3p2i")))

(define-public crate-dapper-host-0.0.1 (crate (name "dapper-host") (vers "0.0.1") (hash "16aqlyplm8g9cq595ychznz9kkfj586l7ah6nd9jyfch8bd13zgl")))

(define-public crate-dapper-protocol-0.0.1 (crate (name "dapper-protocol") (vers "0.0.1") (hash "1rjylivlh379km5lz1j8ja1f9sxafnmdz62lngbi88dz3k6m0nyj")))

