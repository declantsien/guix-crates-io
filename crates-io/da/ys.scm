(define-module (crates-io da ys) #:use-module (crates-io))

(define-public crate-days-0.1 (crate (name "days") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sy1k7kxcy74b5ds2z25g9l34bqngpmxspsd5dwzgrk225n28m75")))

(define-public crate-days-in-month-0.1 (crate (name "days-in-month") (vers "0.1.0") (deps (list (crate-dep (name "leap-year") (req "^0.1") (default-features #t) (kind 0)))) (hash "0c036a8j8ywmz7fsnbhqzzvfpw18jzd5gkw3mhcwmwbzhjq3am25")))

(define-public crate-days-in-month-1 (crate (name "days-in-month") (vers "1.0.0") (deps (list (crate-dep (name "leap-year") (req "^0.1") (default-features #t) (kind 0)))) (hash "0clw9zq9lg6lx04zwdg1afvd0cwrgs7208gmb25cvzdfgy1s63xd")))

(define-public crate-days-in-month-2 (crate (name "days-in-month") (vers "2.0.0") (deps (list (crate-dep (name "leap-year") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wg1w7x1brnn810y6iwz1fwmjz0my60zd5piqv6cb6vpqmnyafh0")))

(define-public crate-daysbetweendates-0.1 (crate (name "daysbetweendates") (vers "0.1.0") (hash "170vak2dpb0f07w1lbqj8f22a5g5y3w1xx47gxghh5vx59frpd23")))

(define-public crate-daysbetweendates-0.1 (crate (name "daysbetweendates") (vers "0.1.1") (hash "1cn7x0ks660s3iwky7y4jlsvqh63v9r0pc0ci0zfr0w2cvzcmdr7")))

(define-public crate-daysbetweendates-0.1 (crate (name "daysbetweendates") (vers "0.1.2") (hash "0w48pp5yx9jw16bn9qfiv58867hs8ld2y6lf0y43kcp1mqaxawxi")))

