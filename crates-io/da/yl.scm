(define-module (crates-io da yl) #:use-module (crates-io))

(define-public crate-daylio-0.1 (crate (name "daylio") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0r8cxpj9hm9dcclyn1hya58m2w4040093vrvgp5bnbaadr65kl5r")))

(define-public crate-daylio-0.2 (crate (name "daylio") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1z76kd47hk0xdvfxxh3539whr6wcwfrzszbvmqp5m24sdj6hqslm")))

