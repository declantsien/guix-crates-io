(define-module (crates-io da ye) #:use-module (crates-io))

(define-public crate-dayendar-0.1 (crate (name "dayendar") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.20") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "09rqgncz67aqp3impy8m8nynw3lvnw594h119fq6wv2acdfr5mz0") (yanked #t)))

(define-public crate-dayendar-0.1 (crate (name "dayendar") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.20") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0x4hh88x5cz0jcxr4zhjvp1i0jq55aw4h4glcd42nwp4bpwcdr20")))

(define-public crate-dayendar-0.1 (crate (name "dayendar") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "grcov") (req "^0.8.19") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.20") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0ryhr6fc8jpppfazxfs1ay24dgdyn3vgyjagg1dwsrkdm1h8mh3j") (features (quote (("testing"))))))

