(define-module (crates-io da t-) #:use-module (crates-io))

(define-public crate-dat-network-protocol-0.1 (crate (name "dat-network-protocol") (vers "0.1.1") (deps (list (crate-dep (name "protobuf") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^1.4") (default-features #t) (kind 1)))) (hash "03i8yznjwgn7c250gzjr5cx431lbjbc87wcdjp503sji6f0nc1d9")))

(define-public crate-dat-network-protocol-0.1 (crate (name "dat-network-protocol") (vers "0.1.2") (deps (list (crate-dep (name "protobuf") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^1.4") (default-features #t) (kind 1)))) (hash "06wcysrj135cxa0j2nvk3ncbm80shzvd7riv97sg3pdk2dswlr81")))

(define-public crate-dat-network-protocol-0.1 (crate (name "dat-network-protocol") (vers "0.1.3") (deps (list (crate-dep (name "protobuf") (req "~1.5") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "~1.5") (default-features #t) (kind 1)))) (hash "04xjhi4pmwabn3kpinav7544jq5nphrmbsv27rw110zj0kmzac2k")))

(define-public crate-dat-network-protocol-0.1 (crate (name "dat-network-protocol") (vers "0.1.4") (deps (list (crate-dep (name "protobuf") (req "~1.5") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "~1.5") (default-features #t) (kind 1)))) (hash "1rvnldkj5zy8mvk7kbhg1lfsx4x9vhq4iygzqfgwya0h6n1jizzm")))

(define-public crate-dat-network-protocol-0.2 (crate (name "dat-network-protocol") (vers "0.2.0") (deps (list (crate-dep (name "protobuf") (req "= 2.10.1") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen-pure") (req "= 2.10.1") (default-features #t) (kind 1)))) (hash "0m3pfgxw9rdd1k27dz9snhjmyzn2wi0hq5bzvk96s5pwkfniwk1v")))

(define-public crate-dat-network-protocol-0.3 (crate (name "dat-network-protocol") (vers "0.3.0") (deps (list (crate-dep (name "protobuf") (req "= 2.10.1") (default-features #t) (kind 0)) (crate-dep (name "protobuf-codegen-pure") (req "= 2.10.1") (default-features #t) (kind 1)))) (hash "1sk5vswpzinm1c68fbnw81jri3lic5baqqb7l3g87h05sl4qa4dq")))

