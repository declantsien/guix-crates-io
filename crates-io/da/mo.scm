(define-module (crates-io da mo) #:use-module (crates-io))

(define-public crate-damo_fetch-0.1 (crate (name "damo_fetch") (vers "0.1.0") (deps (list (crate-dep (name "nixinfo") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0fsglvv6586p94rjxx0xvca0yy45mkywlqkcy6y4gk3icfaj2p20")))

