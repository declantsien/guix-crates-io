(define-module (crates-io da ge) #:use-module (crates-io))

(define-public crate-dager-0.1 (crate (name "dager") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "03v2q3v18iq26dwb2nn2q5b6xzk99f836bcfxnsva9qba44q03ay")))

(define-public crate-dager-0.1 (crate (name "dager") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req ">=0.4.11, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req ">=1.13.0, <2.0.0") (default-features #t) (kind 0)))) (hash "1hznh9i39w59skx6wq4bmc3j2fhgvwgbgx7hn12yn80q8c9j83bg")))

