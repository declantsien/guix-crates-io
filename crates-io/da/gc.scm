(define-module (crates-io da gc) #:use-module (crates-io))

(define-public crate-dagc-0.1 (crate (name "dagc") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0qyki69y9axzcsj6k9ypc5z6qyl1mpcy7ds9ac550fkmgqkaj9gb")))

