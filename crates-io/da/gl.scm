(define-module (crates-io da gl) #:use-module (crates-io))

(define-public crate-daglib-0.1 (crate (name "daglib") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0y41v0cl3c9h4a0jyrl4cm2ld144n8jdzml85xdq273kj77nmsx8")))

