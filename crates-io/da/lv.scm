(define-module (crates-io da lv) #:use-module (crates-io))

(define-public crate-dalvik-0.1 (crate (name "dalvik") (vers "0.1.0") (hash "043c499c4zqxbrap8zhsigm2vd5yz2p6fdn7m83s6g2gbq564s3y")))

(define-public crate-dalvik-0.1 (crate (name "dalvik") (vers "0.1.1") (hash "0625s789lmw9h1hk3kri8p8sgp4c6bzhwqrnza45rr9m55pnx7il")))

(define-public crate-dalvik-0.1 (crate (name "dalvik") (vers "0.1.2") (hash "14pw8l71kzv63hqj35inlmpp9f8ql2y1iziw4nrpa436l63s0jql")))

