(define-module (crates-io da rd) #:use-module (crates-io))

(define-public crate-dardan_ui-0.1 (crate (name "dardan_ui") (vers "0.1.0") (deps (list (crate-dep (name "sdl2") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "17dirq4pvbp6dgxv3wayamni2qzjx5yqsbbir2wa03cffvwa31nl")))

(define-public crate-dardan_ui-0.1 (crate (name "dardan_ui") (vers "0.1.1") (deps (list (crate-dep (name "sdl2") (req "^0.31.0") (default-features #t) (kind 0)))) (hash "1zn31ibdxwvzfq6vbdz70k1canbx6irwshhk9w33lq42l9fvc444")))

