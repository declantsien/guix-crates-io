(define-module (crates-io da ku) #:use-module (crates-io))

(define-public crate-daku-0.1 (crate (name "daku") (vers "0.1.0") (hash "1zq7pw2q5j5p65lvn5hdf1f2a61qjhgpjfw2vksigkl24hzaflli")))

(define-public crate-daku-0.2 (crate (name "daku") (vers "0.2.0") (hash "1pz8xm9mz7gfkb3p961pp9yg54sq80715yjj6bnxw78r2gwiq608")))

(define-public crate-daku-0.3 (crate (name "daku") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "05adhjm1ac1s5jhr6w6jvmn16b1zpq0b782lfirzbdk9wf8z286l") (features (quote (("prompt")))) (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-daku-0.3 (crate (name "daku") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "0vjb1aq0jrxy24bpgnrxpj2sym845wcggyq5y0w03r9gr8psps20") (features (quote (("prompt")))) (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-daku-0.3 (crate (name "daku") (vers "0.3.2") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "1ywdpw9ks2s8q8xzi0l0aw601x15g5ycbr03737cprad4xcqh6v3") (features (quote (("prompt")))) (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-daku-0.3 (crate (name "daku") (vers "0.3.3") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "02ym9nw7d8bvdrygsk6rddgbm36cng2q8gln5bww1pdwncx8mirh") (features (quote (("prompt")))) (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-daku-0.4 (crate (name "daku") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "1965hk0cpz6lyajjbrapzyxk187i63wyxy2zhf6qb6968sggw6g5") (features (quote (("prompt")))) (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-daku-0.5 (crate (name "daku") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "0bnxwnwwqnks0dyyzv6fmhzgwfqj5aclziaq9ck0vrfwwgy3f7f3") (features (quote (("prompt")))) (v 2) (features2 (quote (("log" "dep:log"))))))

