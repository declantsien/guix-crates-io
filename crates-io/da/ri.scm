(define-module (crates-io da ri) #:use-module (crates-io))

(define-public crate-dariodip_grrs-0.1 (crate (name "dariodip_grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)))) (hash "0m2nmaalqvrlw0n5vghn9sr7lz6ij3kaw5dgmpczgx4dpfl7hsmy")))

(define-public crate-dariotd_threadpool_rs-0.1 (crate (name "dariotd_threadpool_rs") (vers "0.1.0") (hash "1zdq4jskwspaqadwlkl6n6xcczxr423iff258js2wbcqp5ffq610")))

