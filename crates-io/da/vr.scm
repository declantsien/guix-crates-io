(define-module (crates-io da vr) #:use-module (crates-io))

(define-public crate-davros-0.0.0 (crate (name "davros") (vers "0.0.0") (deps (list (crate-dep (name "curve25519-dalek") (req "^0.13") (default-features #t) (kind 0)))) (hash "0gn33700is5xvq1ar43fi692c4xg6yngz2rp76da630s69plm35l") (features (quote (("std" "curve25519-dalek/std") ("nightly" "curve25519-dalek/radix_51") ("default" "std") ("bench"))))))

