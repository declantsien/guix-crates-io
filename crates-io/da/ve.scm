(define-module (crates-io da ve) #:use-module (crates-io))

(define-public crate-dave-0.1 (crate (name "dave") (vers "0.1.0") (hash "021b2pm0hqjshygnf6a76nkr76539n8p97r2dn90an1jwfg2q2qj")))

(define-public crate-dave-0.1 (crate (name "dave") (vers "0.1.1") (deps (list (crate-dep (name "hal9000") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vzspbsyrmaq2rlw8xpszq2yz3amkz9mvj8a1ayfzgwri0pax42a")))

(define-public crate-davenport-0.1 (crate (name "davenport") (vers "0.1.0") (hash "00igvlvcliws19rjw8dcp382sz3hr7si8jcbc9vj8vjalbfpinsm")))

(define-public crate-davenport-0.1 (crate (name "davenport") (vers "0.1.1") (hash "09qgm5yl2m2n15d10d4gy8b31ycb5fcszxa6bdvigy24w0znmmnz")))

(define-public crate-daveparr_minigrep-0.1 (crate (name "daveparr_minigrep") (vers "0.1.0") (hash "04x7b1wwa4wfksvx4arb5jl6x7m1h0mqypc7ww3rkakzj4v4mq53")))

