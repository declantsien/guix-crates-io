(define-module (crates-io da xp) #:use-module (crates-io))

(define-public crate-daxpedda-vsprintf-1 (crate (name "daxpedda-vsprintf") (vers "1.0.2-pr") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "186p4r2y1j3kx5a26qwczfavwg2447kd761rq79z03wqz759qb15")))

