(define-module (crates-io da mm) #:use-module (crates-io))

(define-public crate-damm-0.1 (crate (name "damm") (vers "0.1.0") (deps (list (crate-dep (name "digits_iterator") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0iqd0i5pz74g841bqk6q61b853w2s78l936vmg23haw2g1a31y9d")))

(define-public crate-dammcheck-0.1 (crate (name "dammcheck") (vers "0.1.0") (deps (list (crate-dep (name "pyo3") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cxm77xp4784vvia827kqkcyvq7im3yxzrbbvgzckjic3747li92") (features (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

(define-public crate-dammcheck-0.1 (crate (name "dammcheck") (vers "0.1.1") (deps (list (crate-dep (name "pyo3") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00946qbvwn09z4ygmmrxbpkiry7q2vpy280a67m6fxkixdd4rpv4") (features (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

(define-public crate-dammcheck-0.1 (crate (name "dammcheck") (vers "0.1.2") (deps (list (crate-dep (name "pyo3") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0mkxzficxc7jrvpgzhz83ih4wiy7rjpkk6b57pg94fy8sz0m13cj") (features (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

