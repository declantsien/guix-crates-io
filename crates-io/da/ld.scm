(define-module (crates-io da ld) #:use-module (crates-io))

(define-public crate-daldalso-0.1 (crate (name "daldalso") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.2") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ygxck8w7jjhzx9j5rpa7zkznr8fbnbmzb9csqz14sc937v4jkgr")))

