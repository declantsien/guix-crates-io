(define-module (crates-io da hr) #:use-module (crates-io))

(define-public crate-dahriim-0.1 (crate (name "dahriim") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ng7a9ya96jw9293h5mpyrd4pnl1z8snc57iffi8h05h7vr869h0") (yanked #t)))

