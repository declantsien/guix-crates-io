(define-module (crates-io da go) #:use-module (crates-io))

(define-public crate-dagom-0.0.0 (crate (name "dagom") (vers "0.0.0") (hash "1fj34jw4nbia1kjl9vkimnipf24w2cl535lnj44z2szvyzb431s2")))

(define-public crate-dagon-0.0.1 (crate (name "dagon") (vers "0.0.1") (hash "048855cjaw302nlrp6l7gy0w1wsx86h9926w2nyr5sb9yiaph5z6")))

(define-public crate-dagon-0.0.2 (crate (name "dagon") (vers "0.0.2") (deps (list (crate-dep (name "acacia") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "herbie-lint") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req ">= 0.9, < 1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rmp-serialize") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dx8ma0fda9aaznwnsr28klr9qrvlhxakp5ld26hhvpp4ssgc7bj") (features (quote (("default"))))))

