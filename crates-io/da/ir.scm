(define-module (crates-io da ir) #:use-module (crates-io))

(define-public crate-dairy-0.1 (crate (name "dairy") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1c9xsmz06aqfaki0zzi2hvksjkzdnh0f49q6afq7fnaswk4lh4fj") (features (quote (("unix" "std") ("std") ("default" "std"))))))

(define-public crate-dairy-0.1 (crate (name "dairy") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "14y7dnmyl0nw830p3bv2xasfq5j2xr7rpzbdbf52cxhkl7sd08ba") (features (quote (("unix" "std") ("std") ("default" "std"))))))

(define-public crate-dairy-0.2 (crate (name "dairy") (vers "0.2.0") (deps (list (crate-dep (name "beef") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0kjdkq20xjl2s6dg6v2638q33hbaw1kp79fbcrhgj26hbyrfhq89") (features (quote (("std") ("default" "std"))))))

(define-public crate-dairy-0.2 (crate (name "dairy") (vers "0.2.1") (deps (list (crate-dep (name "beef") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1g7hqs5phm68ipf8pmqhwdw5kkkwq0yd0h8zpcnbd2pwirbja0ly") (features (quote (("std") ("default" "std"))))))

(define-public crate-dairy-0.2 (crate (name "dairy") (vers "0.2.2") (deps (list (crate-dep (name "beef") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0grxmb6wwzw6pyzmfgi5vyk3y9ihdvpvy3s1bk504w7mq405p29k") (features (quote (("std") ("default" "std"))))))

