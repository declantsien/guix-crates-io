(define-module (crates-io da en) #:use-module (crates-io))

(define-public crate-daence-0.0.0 (crate (name "daence") (vers "0.0.0") (deps (list (crate-dep (name "chacha20") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "poly1305") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "subtle") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "universal-hash") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0zj762w5rgkajiy4n4rgx77czc97pich41ahwcxyjrj2z881bksw") (rust-version "1.58")))

