(define-module (crates-io da v1) #:use-module (crates-io))

(define-public crate-dav1d-0.3 (crate (name "dav1d") (vers "0.3.0") (deps (list (crate-dep (name "dav1d-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "11s4v21ala10vmy5ha9gx7pcm0y0am0621sl2klw7xzzl4w6p8xz")))

(define-public crate-dav1d-0.4 (crate (name "dav1d") (vers "0.4.0") (deps (list (crate-dep (name "dav1d-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1j8wqk62kr3ki5cmc4k0bvq9pvaz4gjc76apgqrwy0rf623vk50r") (features (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.5 (crate (name "dav1d") (vers "0.5.0") (deps (list (crate-dep (name "dav1d-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0iz1z9pbnqnggqbrn6w7hx9q5z6khbgq64w1acab9wms1zidj87a") (features (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.5 (crate (name "dav1d") (vers "0.5.1") (deps (list (crate-dep (name "dav1d-sys") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1nq8fgjyk1qp0pqs5qgn38cj5zb1vypscacxfmsaj4xhlkrslh1n") (features (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.5 (crate (name "dav1d") (vers "0.5.2") (deps (list (crate-dep (name "dav1d-sys") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0bcjy00fs4vf8ni3jl2wsxqycs0vv6dw4fbksvpy5i1y1q5yfd7a") (features (quote (("build" "dav1d-sys/build"))))))

(define-public crate-dav1d-0.6 (crate (name "dav1d") (vers "0.6.0") (deps (list (crate-dep (name "dav1d-sys") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0pn6r1a9qfrpg2xwc7ci2iddvnzxb17ddca0bwymgi839cxc2chl")))

(define-public crate-dav1d-0.6 (crate (name "dav1d") (vers "0.6.1") (deps (list (crate-dep (name "dav1d-sys") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0qz2lx37pmx798lysgh6k5lk5y20ckr7pp8c1p6v2z0p721i913j")))

(define-public crate-dav1d-0.7 (crate (name "dav1d") (vers "0.7.0") (deps (list (crate-dep (name "dav1d-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1rcgi6f3bcqs6gawxpn3myqb6mn536rjsj4i27sryaacmxflaccz")))

(define-public crate-dav1d-0.8 (crate (name "dav1d") (vers "0.8.0") (deps (list (crate-dep (name "dav1d-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "12iashg7429zjzkh6fg85dd766c82q6jmx6zm8pnrm5p7zb9yrnr")))

(define-public crate-dav1d-0.9 (crate (name "dav1d") (vers "0.9.0") (deps (list (crate-dep (name "dav1d-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0axph7xflqk89yks3mza1dydbygsj8q3a0vs8y6npm8zn4ld79m5")))

(define-public crate-dav1d-0.9 (crate (name "dav1d") (vers "0.9.1") (deps (list (crate-dep (name "dav1d-sys") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1i3834wh9p9v55wzy40z0ssl342sgk99vx7sw1scdqa4nr5hczcm")))

(define-public crate-dav1d-0.10 (crate (name "dav1d") (vers "0.10.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "12pxwk3qyahrvlcl3sgsadv083wj8rscvlwcg11qxq44y8488009") (features (quote (("v1_1" "dav1d-sys/v1_1")))) (yanked #t)))

(define-public crate-dav1d-0.9 (crate (name "dav1d") (vers "0.9.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "176cikf6313rd5r5bgahbik1w11sv60dr74a9jfdvv3v8azvshy3") (features (quote (("v1_1" "dav1d-sys/v1_1")))) (yanked #t)))

(define-public crate-dav1d-0.9 (crate (name "dav1d") (vers "0.9.3") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1cvv1fxc4jbqbii78p3m4mhzr0zrhqr5q663hpxrdmdw6w5b4an0") (features (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.9 (crate (name "dav1d") (vers "0.9.4") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0xacd2hyr6fjwd7nq94jclsc2h6cm2va9nazdc433scfwp447jl7") (features (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.9 (crate (name "dav1d") (vers "0.9.5") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "057l2llgvqn44kwvjgmznzsqmw8n5jpdd9w7rsy8w4v0wgjzgs9s") (features (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.9 (crate (name "dav1d") (vers "0.9.6") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1j5dpkqs07kq4mfyxa3m9z1lf897lrmqc8frzga83p0zx0x8bgln") (features (quote (("v1_1" "dav1d-sys/v1_1"))))))

(define-public crate-dav1d-0.10 (crate (name "dav1d") (vers "0.10.1") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0c827hzq86r1w51b2l774mia3yhxa7p3r4qfdca1znkw5s8rljxh")))

(define-public crate-dav1d-0.10 (crate (name "dav1d") (vers "0.10.2") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0c3h6w2yz2wks52lhmx19afldcyihv8zcgnly1gyhyjbjnlizsyp")))

(define-public crate-dav1d-0.10 (crate (name "dav1d") (vers "0.10.3") (deps (list (crate-dep (name "av-data") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dav1d-sys") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)))) (hash "1qd13sm1bfbc5chjgrzk4syffkky994lkyzhqrqklqxg1fj58jqd")))

(define-public crate-dav1d-sys-0.1 (crate (name "dav1d-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.45.0") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.12") (default-features #t) (kind 1)))) (hash "1xqphls1wpnq28h1dlqp35k17isv1raypqlp3m7dl24xavwaci0c")))

(define-public crate-dav1d-sys-0.1 (crate (name "dav1d-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.45.0") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.12") (default-features #t) (kind 1)))) (hash "1ark8i151fmcqz79x6iwvvs22v0j2m9hydsvmhrgnzjjwwn2bxyl")))

(define-public crate-dav1d-sys-0.1 (crate (name "dav1d-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.47") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.12") (default-features #t) (kind 1)))) (hash "0lq3lqd63pcgc3sxr09r978k6pgx0v33lb9b03r7ckvnm012xnmv")))

(define-public crate-dav1d-sys-0.2 (crate (name "dav1d-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.12") (default-features #t) (kind 1)))) (hash "1bam0h2wldricgk9di9s20ggf0cdmkvkqv9ksf92vp2ya667c3jx")))

(define-public crate-dav1d-sys-0.2 (crate (name "dav1d-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.12") (default-features #t) (kind 1)))) (hash "1bcnzdiqzisqka88vdi7ik225qz8qmhr0634dd3g6y92jqvmcr04") (features (quote (("build"))))))

(define-public crate-dav1d-sys-0.3 (crate (name "dav1d-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.12") (default-features #t) (kind 1)))) (hash "1022czzp3s54r42x6rhr870w1fwzyp7b6qn0zirpz55zmqjpgnwa") (features (quote (("build"))))))

(define-public crate-dav1d-sys-0.3 (crate (name "dav1d-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.12") (default-features #t) (kind 1)))) (hash "13hd1r4hiai0hnhc6a9savgddk29ivnclny5fqz7sgb3nadkvsb1") (features (quote (("build"))))))

(define-public crate-dav1d-sys-0.3 (crate (name "dav1d-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)))) (hash "1jdxhnlxcml6jd67lx78ifzkn1xm18zfk4li7vjdh3fa61i073kx") (features (quote (("build"))))))

(define-public crate-dav1d-sys-0.3 (crate (name "dav1d-sys") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.56.0") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^2.0") (default-features #t) (kind 1)))) (hash "1pngmfsim56377xvwjaz62vawvvxj4jfnr63qp93gvl36asx82gi")))

(define-public crate-dav1d-sys-0.3 (crate (name "dav1d-sys") (vers "0.3.4") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^3") (default-features #t) (kind 1)))) (hash "020lla2l703iy69gbksq18snj2b1sp7vmjf39qqykd4242d4msr5")))

(define-public crate-dav1d-sys-0.3 (crate (name "dav1d-sys") (vers "0.3.5") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "10y8637snqc3kb9mhs8p9zi8171ba2hlbvhk06vs6hfifx60rr48")))

(define-public crate-dav1d-sys-0.4 (crate (name "dav1d-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "188pyql0s2xin0slnrkpcb7gyld8mz1ldrkqhgwjfnivmpr7zk5k")))

(define-public crate-dav1d-sys-0.5 (crate (name "dav1d-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "00g57dn0x6igr1ljrlwifybkgqvz7r4dvj3y2h3inrlsxd1jnzi3")))

(define-public crate-dav1d-sys-0.6 (crate (name "dav1d-sys") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "12da79929a865cydik1h8bbbyzsfm6krwa8w68s6f3pnq5y4mhp6")))

(define-public crate-dav1d-sys-0.7 (crate (name "dav1d-sys") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "16gzjsfnvfd5zr8mrx5n9mdd4vjvwfwpk9hfscgz7sjyzjdjzcm0")))

(define-public crate-dav1d-sys-0.8 (crate (name "dav1d-sys") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "11r720fdnlq14hi1gcc2w11h9d8ys7pldd8768m4dda9lcbkh28z") (features (quote (("v1_1")))) (yanked #t)))

(define-public crate-dav1d-sys-0.7 (crate (name "dav1d-sys") (vers "0.7.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "0bgaaaiwjs3yykzzb420f66j2s11j1c25adb8rgpk2y12jxl4mb1") (features (quote (("v1_1"))))))

(define-public crate-dav1d-sys-0.7 (crate (name "dav1d-sys") (vers "0.7.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^6.0") (default-features #t) (kind 1)))) (hash "1gi164rn93q6qpy5s2760lbdw1yz2plhrlmg06v749xkzlyphhs3") (features (quote (("v1_1"))))))

(define-public crate-dav1d-sys-0.7 (crate (name "dav1d-sys") (vers "0.7.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.2") (default-features #t) (kind 1)))) (hash "13z5qvf35lkda67l6l1bkdp1gmqg75cqfblldxh4n8rbmn4zsj9s") (features (quote (("v1_1"))))))

(define-public crate-dav1d-sys-0.8 (crate (name "dav1d-sys") (vers "0.8.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.2") (default-features #t) (kind 1)))) (hash "0ad50rjpz2h6r4grwbppdqadd4i2zli84lgxc4zgk3wv7a15s6m1")))

(define-public crate-dav1d-sys-0.8 (crate (name "dav1d-sys") (vers "0.8.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.2") (default-features #t) (kind 1)))) (hash "158fqp97ny3206sydnimc2jy1c1gcxa4llqvvkp3ii2dixg1rjvf")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "07aq1xlydfnsjjbzdi594fwrskixjf4j7xqisvqjliwm3i550b90")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1f03kjxdhw2qiswy0d88x9mnvyngagzamin0b6cj28ikvgw83xbw")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "04mf5x6j8fx3fp93n8wghb6xvria1p696wzify6fkz5rsnrlss2v")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1kn9h6fjvnyv3nyl3na62xci7d3jfjc330ms5kx9n1lwy5mfslww")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "0y8n2zb1lf89cfa7rnkd069zqz6zbx0hb25azf688sjv0k3x54gc")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.3") (default-features #t) (kind 1)))) (hash "0sdx214xh6ix55f7bjy8bbcgfv6yhmv1j8pcarahqpd3lljmmbqk")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "0l9rrn2162lzbqplml68yll982bpyy75n03f3zzq9hfxkrm7f0n8")))

(define-public crate-dav1d-sys-po6-0.1 (crate (name "dav1d-sys-po6") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "po6") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "1yxska8d1la7szv2k6ry7wi3zjpn1k4a5q0hz7gc3dc4dsnwp90a")))

