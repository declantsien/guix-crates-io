(define-module (crates-io da mn) #:use-module (crates-io))

(define-public crate-damn-0.1 (crate (name "damn") (vers "0.1.0") (hash "1k9wxz3l9aszzbyvrhr88n8z312953axy8hp6i259jr6vw758d1p")))

(define-public crate-damn-0.1 (crate (name "damn") (vers "0.1.1") (hash "1arqaliq7gavw0lv18x1m2cagpkd0qcc91s41xlj3w8dix8p7grd")))

(define-public crate-damn-0.1 (crate (name "damn") (vers "0.1.2") (deps (list (crate-dep (name "damn-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1w3kk85hgxlz5dqp1956rrfr1wc28xf531nc7q34dv6mplpnx0ni")))

(define-public crate-damn-core-0.1 (crate (name "damn-core") (vers "0.1.1") (hash "0k8s1lc09y8hwd18b5czyhka0gy2hsjfg64s4x6hj0hyqhjgzh6b")))

(define-public crate-damn-core-0.1 (crate (name "damn-core") (vers "0.1.2") (hash "10hzwwlhy35j9l2jc7zcfpcp9ylbg09pb2sk42y2x53icdrsyqlx")))

(define-public crate-damn-it-0.1 (crate (name "damn-it") (vers "0.1.0") (hash "0swrc1p9c3h45wpgd9ya70h7zdkahwpmbq5fskxydzvpc9g2j2k2")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.0") (hash "1chp1k6sv0n2g9ai7b8njg0flh6l585xap31iyv16l6lnb971jw7")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.1") (hash "0x1wy9xz59ldhfs68layicm6q31vba0xcq3066rbqafasbl5v0md")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.2") (hash "0f1h3llazfjxh15l3qp4amkv40czw5qmmm2vccsrn2mrhi4by3ys")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.3") (hash "17fpkf1j06rwzfk39ddm5b8ivjklxarghzh9zl13vhmpvgrdjpvc")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.4") (hash "1fxgdmwk5f2i2y40fciv2jfsg2v52hvq9rd8aiiz2ah6dcracak9")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.5") (hash "0wfmrnhf27gvxh1qnbiwxb0g3vcsb56k37l7p08qzrirq4f1lpm7")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.6") (hash "01xsvh56bak3lghrvxxib5yka8h47wzx4diva4zpbdz6rxr88lcj")))

(define-public crate-damndiff-0.1 (crate (name "damndiff") (vers "0.1.7") (hash "1lv52p21hcv9k0fwwj011x8lm13ph9kc791hdsdj0kg6gg06gb4d")))

