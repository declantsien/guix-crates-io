(define-module (crates-io da rc) #:use-module (crates-io))

(define-public crate-darc-0.0.1 (crate (name "darc") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1b6jgq315isfs95zrgmjgq6x3sddprmay6z431wid8khyidybhk7")))

(define-public crate-darc-0.0.2 (crate (name "darc") (vers "0.0.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "08hzzc2wgvgdn6yvijzgj00rry786f940fi40vkrm3b9ryjl308p")))

