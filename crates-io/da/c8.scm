(define-module (crates-io da c8) #:use-module (crates-io))

(define-public crate-dac8564-0.1 (crate (name "dac8564") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "05wi001yzw91szb6wqrsqxx9s5prvnkcwj3hzs4x7nbc2s3a763q") (features (quote (("default")))) (yanked #t)))

(define-public crate-dac8564-0.0.1 (crate (name "dac8564") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1mbc4hqjpwprckgq754fkgymqd3ad5zbqfjc9078fv7sgf2w1n7d") (features (quote (("default"))))))

(define-public crate-dac8564-0.0.2 (crate (name "dac8564") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "06w9h57h9x93wbf5hi2nbzsfmi2rzkasj17lca3yk511nd1sjh36") (features (quote (("default"))))))

(define-public crate-dac8564-0.0.3 (crate (name "dac8564") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1n8jk5zq6bzn61zzgxyd84g65n7mfryhc9bajdx45kb86ay1a395") (features (quote (("default"))))))

(define-public crate-dac8564-0.0.4 (crate (name "dac8564") (vers "0.0.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "146jk9f5z9arq2mdxcq8hdlwd2affbacv88596g9xv26yqisrjgq") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.1 (crate (name "dac8568") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0a8cphb7xmnwma49cn5nzqp9gwljadknf4vzwnhs7874mph1fkmn") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.2 (crate (name "dac8568") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "04rkvwj335091cmnjfy8zqly2ivdcgb7sbd2p7021nkrgglpyvyx") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.3 (crate (name "dac8568") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "19qdzzkzaqdglqf5ykqbcbnwwb95zxh5f77lsx7a4fdfmsnwqk6g") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.4 (crate (name "dac8568") (vers "0.0.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1kgspsj5pjrs3r8hqwycc9q0kw5c8k1a2dq0xa88mz296s2f4gqj") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.5 (crate (name "dac8568") (vers "0.0.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "09pf0ka7shib3jgk9n549cfzymmqrfbry635q4lnppwsl6b5s126") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.6 (crate (name "dac8568") (vers "0.0.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0zwf3hm570qj876g3w48axvjdjaz05qwkwx6v7d9nicmk74i3i7l") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.7 (crate (name "dac8568") (vers "0.0.7") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0ck50s7mxizr6cidldam6i6sz9d9m5jagq8frnbk1ifsdgf7z1g1") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.8 (crate (name "dac8568") (vers "0.0.8") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1a51ygvbxmlrxrrz9b3b9lsybw1rxb57qdcv42xyxx4fpjiv8rps") (features (quote (("default"))))))

(define-public crate-dac8568-0.0.9 (crate (name "dac8568") (vers "0.0.9") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0kx00fqxikma9vm38aqcgi8cg5pn17v4pqydf7kqw14abiidin6m") (features (quote (("default"))))))

