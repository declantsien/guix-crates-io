(define-module (crates-io da wr) #:use-module (crates-io))

(define-public crate-dawr-0.0.1 (crate (name "dawr") (vers "0.0.1") (deps (list (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d5jdb5gpl8hnsqhzgz3hc82bhqvh40f9lspmn4n6gdyjmnq2cqs")))

(define-public crate-dawr-0.0.2 (crate (name "dawr") (vers "0.0.2") (deps (list (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mj8hwl2d7a5394mgifabi2vl289jgwhqcva3shyg5hp56wz83xn")))

