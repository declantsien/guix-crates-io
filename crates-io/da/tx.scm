(define-module (crates-io da tx) #:use-module (crates-io))

(define-public crate-datx-0.1 (crate (name "datx") (vers "0.1.0") (hash "05j02rq6gax221jjmfa81plvmyr938iylifyfxang105lpv7s2p6")))

(define-public crate-datx-0.1 (crate (name "datx") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "all"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15wygvl5bpnfa6v6c1mq1w4snv3pq59xp8fla8rkb091q46qq6jj")))

