(define-module (crates-io da ab) #:use-module (crates-io))

(define-public crate-daab-0.1 (crate (name "daab") (vers "0.1.0") (hash "1j5n6kji5c8x9x61fxq7kgniy58d14xgcrs577m7fkrkwsys62yr")))

(define-public crate-daab-0.2 (crate (name "daab") (vers "0.2.0") (hash "0ypk4ya15fza12zw8yjyx756lni26xr5yv6fcxmhiv312vaihiw7")))

(define-public crate-daab-0.2 (crate (name "daab") (vers "0.2.1") (hash "0544l9gizb8wxzs0lxfs4lhm4x1k2l9sm1im86m8k7224azic8mh")))

(define-public crate-daab-0.2 (crate (name "daab") (vers "0.2.2") (hash "0b5l3jfb9ywj9yqnz0f2sq6i39alcy70vqiz9x8awq926v2ic4ap")))

(define-public crate-daab-0.2 (crate (name "daab") (vers "0.2.3") (hash "1qnnz94iiy7kp9w9rlhcvaksfyv1hr789i8ydxs5jgh22bhjkmjp")))

(define-public crate-daab-0.2 (crate (name "daab") (vers "0.2.4") (hash "0na4ipfjiqj4218bx34gnpijx3qcsd03dvp6qc90dn320frwxjlh")))

(define-public crate-daab-0.3 (crate (name "daab") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "tynm") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "14c7ld2nnvsjvg2q6m8svhca1n3vdw8lgb6idnddxbhnd65hcj8p") (features (quote (("diagnostics") ("default"))))))

(define-public crate-daab-0.3 (crate (name "daab") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "tynm") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0hw72ln714icrih1v0ppfhkj8if1z21pyqkk55w1fj30qwbhbxqm") (features (quote (("diagnostics") ("default"))))))

(define-public crate-daab-0.3 (crate (name "daab") (vers "0.3.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "tynm") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0397vy2w7vqisghb64zn5lpnr9w9i3ig49d1zwmw7jbf7dkjmc7d") (features (quote (("diagnostics") ("default"))))))

(define-public crate-daab-0.4 (crate (name "daab") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "never") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tynm") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "05f5d9bi6y1vvqvkzab3l78krmwv2i4af8zmr8ykpgmpya80gai7") (features (quote (("unsized") ("mut_box") ("doc_cfg") ("diagnostics") ("default"))))))

