(define-module (crates-io da wg) #:use-module (crates-io))

(define-public crate-dawg-0.0.1 (crate (name "dawg") (vers "0.0.1") (deps (list (crate-dep (name "config") (req "^0.13.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.6.2") (features (quote ("offline" "runtime-tokio-native-tls" "uuid" "macros" "postgres" "migrate" "json"))) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.4") (features (quote ("v4" "serde"))) (default-features #t) (kind 0)))) (hash "0rbx96i4hx4yxfswlkjspgqrcq2jzykxng1bgsbckwh7kbimlxvv") (features (quote (("threading"))))))

(define-public crate-dawg-0.0.2 (crate (name "dawg") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "113n5s81ciy7cxk36bvv2bx3l4q2g3h8v29xgjrgkhggw39fixbq") (features (quote (("threading")))) (yanked #t)))

(define-public crate-dawg-0.0.3 (crate (name "dawg") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "05gcxp1lj1jmw4yw1wpyqqq5d5qykxlyxh1q4340hrjcpwf2rvn4") (features (quote (("threading"))))))

(define-public crate-dawg-0.0.4 (crate (name "dawg") (vers "0.0.4") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)))) (hash "03s4zrdsn3sryvim85p53594glhciigd5f4yvq82sd4pgzm4gmaq") (features (quote (("threading"))))))

(define-public crate-dawg-0.0.5 (crate (name "dawg") (vers "0.0.5") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.10.1") (default-features #t) (kind 0)))) (hash "1y0y59g5kzqi0d7g14qs6z2h2ahyz23lc1mhx3w2qs2qnd5a0fii") (features (quote (("threading"))))))

