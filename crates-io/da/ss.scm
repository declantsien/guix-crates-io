(define-module (crates-io da ss) #:use-module (crates-io))

(define-public crate-dass-0.1 (crate (name "dass") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1arwfvgii8gaqxr1lciszcn8awsms2797yjhw40j1b4y97lg42ga")))

(define-public crate-dass-0.2 (crate (name "dass") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ibcb9754s54il864favrs4bd9w6fa909v1q15i59q7cc4zf1lmx")))

(define-public crate-dassign-0.1 (crate (name "dassign") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kaq2a640q3393q32jj04hyab7h14il3vd6fig5i6fs4rhinm1xh")))

(define-public crate-dassign-0.2 (crate (name "dassign") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r5swnk9302k0yflr35gjakcdai5093zxm8hpg96cyapbmjzh2vl")))

