(define-module (crates-io da la) #:use-module (crates-io))

(define-public crate-dala-0.1 (crate (name "dala") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.7.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.0") (default-features #t) (kind 0)))) (hash "1qr22vc2yqz2havxhc5ylhapmgfxdv5ny5vxizqgg59bclvq6msw")))

(define-public crate-dala-0.1 (crate (name "dala") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.7.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.0") (default-features #t) (kind 0)))) (hash "06aw3izxalzwypaqndjjzrq8xxhl4minrrwlcf3fwv388f6lmqbf")))

(define-public crate-dala-0.2 (crate (name "dala") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.7.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.0") (default-features #t) (kind 0)))) (hash "1yqk484aq6qzv4p065r1093pj9qfxjwggj9rhak1h3ipzvcr1xkf")))

(define-public crate-dala-0.3 (crate (name "dala") (vers "0.3.0") (deps (list (crate-dep (name "pest") (req "^2.7.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.0") (default-features #t) (kind 0)))) (hash "0xbi4nxlcd20qdcgx21j5xvqhxsh6srblfzhsf1nwfv8wqfy2qfx")))

