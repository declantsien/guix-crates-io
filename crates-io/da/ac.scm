(define-module (crates-io da ac) #:use-module (crates-io))

(define-public crate-daachorse-0.1 (crate (name "daachorse") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0ijsm9cyyxplinxdk0if3s08svnfx11h9i8pbfkz7xvg94d6ac78")))

(define-public crate-daachorse-0.1 (crate (name "daachorse") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0aypvag0rsr7a0ig9qskiv19xgddz8pv5c3x9s44rk0fp1i4jd2w")))

(define-public crate-daachorse-0.2 (crate (name "daachorse") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1wgpnkajag4x46m795b5pzw3rgiaxkj4j8whiffph5w80y5dvhxz")))

(define-public crate-daachorse-0.2 (crate (name "daachorse") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0fnh4s3j7z4mzvj3m2bb6pk746g33i49sicb2zvz28jbakizmmwh")))

(define-public crate-daachorse-0.3 (crate (name "daachorse") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0qyi3b558fkhixhivhxkb49r08bl1hxnn3il5ydwik8xydhg8imf")))

(define-public crate-daachorse-0.4 (crate (name "daachorse") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "119nzc98z52mjgx2llx82148csrdnpcifkqwsp9mmnmacg9fzpww")))

(define-public crate-daachorse-0.4 (crate (name "daachorse") (vers "0.4.1") (hash "0bx8h5yfai37i02ymkvl41wvxsi1dp6rndf1g8a0y1w689gqik4n") (features (quote (("std") ("default" "std"))))))

(define-public crate-daachorse-0.4 (crate (name "daachorse") (vers "0.4.2") (hash "0zlnb1h52rwh94sgmxkrw9zm26lbmjv86ln33wdbj72gxqi81gpj") (features (quote (("std") ("default" "std"))))))

(define-public crate-daachorse-0.4 (crate (name "daachorse") (vers "0.4.3") (hash "19g77ywf1dazxq4spqg3nqcaz0zmmbyhmp2djv9b2ycy25cnb3h4") (features (quote (("std") ("default" "std"))))))

(define-public crate-daachorse-1 (crate (name "daachorse") (vers "1.0.0-rc.1") (hash "074s3fbm4fi6f53pzhn2zap0m6k5f2k0clgkqrrnmrgiw7b1rvlx") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.58")))

(define-public crate-daachorse-1 (crate (name "daachorse") (vers "1.0.0") (hash "0djcm42cxp9jmpj09mlzy54dpb8dhcpa5l0491zka2g59dxfzdv3") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.58")))

