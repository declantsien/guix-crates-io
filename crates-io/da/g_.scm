(define-module (crates-io da g_) #:use-module (crates-io))

(define-public crate-dag_compute-0.1 (crate (name "dag_compute") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("getrandom" "small_rng"))) (kind 2)) (crate-dep (name "slotmap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req ">=0.9.3, <0.10.0") (features (quote ("html_root_url_updated"))) (kind 2)) (crate-dep (name "wav") (req "^1.0") (default-features #t) (kind 2)))) (hash "0x0gmhahf08y8i1llf8yc2k9qcjskjqdxhpdmn0drxc4rzzazmdp")))

(define-public crate-dag_stripper-0.1 (crate (name "dag_stripper") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "02whk3hl3vja8mkl1p2ff3v79al5gjd8k9gkch4ai7593rbmvi7c") (yanked #t)))

(define-public crate-dag_stripper-0.1 (crate (name "dag_stripper") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0ph6v3cmhjvyg6ibp4xlzpqfgam65yxjhbzg0iqsv1zjdkk5x66x") (yanked #t)))

(define-public crate-dag_stripper-0.1 (crate (name "dag_stripper") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1q5h2j9k53ijqyh4cmabf5ddz99qy18mhw3md6az1rxbz05xndyc") (yanked #t)))

(define-public crate-dag_stripper-0.1 (crate (name "dag_stripper") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0qqql9zbpw3h17108z2g74z3f1ymmn4ll8ll2rf8d2wdga1m1ndb") (yanked #t)))

(define-public crate-dag_stripper-0.1 (crate (name "dag_stripper") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1gp9bbkff9hhr3k540xbh0dkb8prr2v813xxp1nxczbms8x7hc4h") (yanked #t)))

(define-public crate-dag_stripper-0.1 (crate (name "dag_stripper") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0qbrg8jc6pf3915lhn2w5yx8xjw2gwhc9l2v1whwplvm91n1lqx0") (yanked #t)))

(define-public crate-dag_stripper-0.1 (crate (name "dag_stripper") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1jzf5q6i6h3nmjsks7c7y9jag81cxlm95xil6sd0kbaciz6dnlj0") (yanked #t)))

(define-public crate-dag_stripper-0.2 (crate (name "dag_stripper") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0xnmhnrcniwbcpxapaf1q50hrhg0cw9nas4j0d4c53wrlrmc8jvc") (yanked #t)))

(define-public crate-dag_stripper-0.2 (crate (name "dag_stripper") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "13cv3i4bz279nl23b8i3q1rv3nygc9diqvxacld9mn35hhns8cpv") (yanked #t)))

(define-public crate-dag_stripper-0.2 (crate (name "dag_stripper") (vers "0.2.5") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1yrw1k5gp7r66zrklbfpq46i93bb667djfi3pz6a2zfcd1709vgm") (yanked #t)))

