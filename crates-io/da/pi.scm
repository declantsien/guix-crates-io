(define-module (crates-io da pi) #:use-module (crates-io))

(define-public crate-dapi-0.1 (crate (name "dapi") (vers "0.1.0") (hash "02hwdxvsjff2jzm7rjbp6skkiksfhpa2ahihlwxrpx7nhnynxzx3") (yanked #t)))

(define-public crate-dapi-client-rust-0.1 (crate (name "dapi-client-rust") (vers "0.1.0") (hash "1wc7h819a6mhbcw4dmbgf3ac7ay4ja2ndcx5ikwfrw7fh3xbwv5b")))

