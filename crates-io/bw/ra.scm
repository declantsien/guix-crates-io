(define-module (crates-io bw ra) #:use-module (crates-io))

(define-public crate-bwrap-0.1 (crate (name "bwrap") (vers "0.1.0-beta1") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0zpki6gnsjpg5mwv1458kkm3v1mward9in322w2f4pyi20723pzr") (features (quote (("use_std"))))))

(define-public crate-bwrap-0.1 (crate (name "bwrap") (vers "0.1.0-beta2") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "15vl9h0inzz3wl7yjsjv95nlhnyxm3p4awg4cp984dqgdgpnvy2f") (features (quote (("use_std"))))))

(define-public crate-bwrap-0.1 (crate (name "bwrap") (vers "0.1.0-beta3") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0ihxkzys98yrzpxv1fa7wvpnl0ap4lg21g986g5lrmwx0wpifyng") (features (quote (("use_std"))))))

(define-public crate-bwrap-0.1 (crate (name "bwrap") (vers "0.1.0-beta4") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09ccg8vwzi5i8c03rg3id19kb1bf6dmp325jckzzs5y92dwk7m5d") (features (quote (("use_std"))))))

(define-public crate-bwrap-0.1 (crate (name "bwrap") (vers "0.1.0-beta5") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0731zfgxjrimnmcm4pb999vx8bjdbhvvjcmysfcr6d6lzj4hm0iy") (features (quote (("use_std"))))))

(define-public crate-bwrap-0.1 (crate (name "bwrap") (vers "0.1.0-beta6") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0688ws54psnm48g287whi87w48klcslf3h01jan3ag5vk4dna7y5") (features (quote (("use_std"))))))

(define-public crate-bwrap-1 (crate (name "bwrap") (vers "1.0.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "163sn2x45vx5iwsyv5wpp0f9h6znc9qxr1pzjjrzawckljm4scp5") (features (quote (("use_std"))))))

(define-public crate-bwrap-1 (crate (name "bwrap") (vers "1.1.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kzdiwpbcvgh5qvl1491hgl4jbyxwijr3m4kzs7m2ssshi531qvf") (features (quote (("use_std")))) (yanked #t)))

(define-public crate-bwrap-1 (crate (name "bwrap") (vers "1.1.1") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g1ac7jd1r4jvm2i1bqxn6gp7pbad4lzynqj355kx42ag9rb52mx") (features (quote (("use_std"))))))

(define-public crate-bwrap-1 (crate (name "bwrap") (vers "1.2.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r6hvv707llvnh06v41k40ihvjjan0bgj29w9lhjmy38s83yahjg") (features (quote (("use_std"))))))

(define-public crate-bwrap-1 (crate (name "bwrap") (vers "1.2.1") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kz47ikws7a6dz9hq2awi4gw17c06y301mlmvzpcbv6b9z2jlmca") (features (quote (("use_std"))))))

(define-public crate-bwrap-1 (crate (name "bwrap") (vers "1.2.3") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0848x6gghli2fi49vwnn107raxf6ip4121jlv5z1pqd9lmq4mvmm") (features (quote (("use_std"))))))

(define-public crate-bwrap-1 (crate (name "bwrap") (vers "1.3.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15dv9mawgmwgpj88k832qjdi94w70advg9zv28bpgig5k8qs6gfi") (features (quote (("use_std"))))))

