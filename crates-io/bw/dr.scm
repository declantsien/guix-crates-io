(define-module (crates-io bw dr) #:use-module (crates-io))

(define-public crate-bwdraw-0.1 (crate (name "bwdraw") (vers "0.1.0") (hash "0g6880rli407lc3diq673lrgs2hllngn7gkbacwqgndcnfrjg23c")))

(define-public crate-bwdraw-0.1 (crate (name "bwdraw") (vers "0.1.1") (hash "06gggaqahswhx5kvqwnpjlf32pkc2bf72zw1vixxjw480608n4in")))

(define-public crate-bwdraw-0.1 (crate (name "bwdraw") (vers "0.1.2") (hash "113grrqh9vpr2akky4mxcvj5fsjlcv6ih642n6a7lp5wpzf32phv")))

