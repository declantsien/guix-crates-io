(define-module (crates-io bw s-) #:use-module (crates-io))

(define-public crate-bws-std-0.1 (crate (name "bws-std") (vers "0.1.0") (hash "0il2lkjafvmfix6aan15g5dvmlx8jfcnidj8030xw21ay7nr0y5y") (yanked #t)))

(define-public crate-bws-std-0.0.0 (crate (name "bws-std") (vers "0.0.0") (hash "07nnpxnc7pqdhxmi4b03fjh9mnkfaprhnjc3vcjiw232v43k91p6") (yanked #t)))

(define-public crate-bws-std-0.0.1 (crate (name "bws-std") (vers "0.0.1") (hash "0qd3l33kk2na2vhmbq1vmkfwrlbg9crc74i78zqi6bab2c9vc2il") (yanked #t)))

