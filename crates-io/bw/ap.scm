(define-module (crates-io bw ap) #:use-module (crates-io))

(define-public crate-bwapi-0.1 (crate (name "bwapi") (vers "0.1.0") (hash "06zm1c9iiamg1wrjq4cd6wqg8mwpc7z47yg6p8p11381cicmahr8")))

(define-public crate-bwapi-0.2 (crate (name "bwapi") (vers "0.2.0") (deps (list (crate-dep (name "bwapi-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1v3b6n01i7pv29fkdrg51yj72akp8dpsa7s1zkb4n1cn0864mnkw")))

(define-public crate-bwapi-sys-0.1 (crate (name "bwapi-sys") (vers "0.1.0") (hash "0iy3hw8kdixrs6makdz7hmb3r2yvax86z58a6fniim5ambj615sy")))

(define-public crate-bwapi-sys-0.1 (crate (name "bwapi-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "1w57naxhy4kx5pl2kn5q4mc4nl8csysa6ifif0zz8q3rscd8h8mg") (links "BWAPIC")))

(define-public crate-bwapi-sys-0.1 (crate (name "bwapi-sys") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "0wi6aalc1laada11v2jv7knd9r91xamkvian8cc72lrrl38yx8k3") (links "BWAPIC")))

(define-public crate-bwapi_wrapper-0.1 (crate (name "bwapi_wrapper") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0vqrjgfhisakj93pqdvgksdhw1z3y4knvp9b1nlbwpzfv9kh47ia")))

(define-public crate-bwapi_wrapper-0.2 (crate (name "bwapi_wrapper") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1w4lhgznwh60zabznxn360z7czbzwczpxw29gklka8jah9y0bmlx")))

(define-public crate-bwapi_wrapper-0.2 (crate (name "bwapi_wrapper") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1vj2ar3mdhyhqil34w8vp9gxw13xcxk6bn9nx5ngp6shx8fhfpfb")))

(define-public crate-bwapi_wrapper-0.2 (crate (name "bwapi_wrapper") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0kbcb0mxpbfwxc959wwsji907cn02lfmn8p0aal7vc2r3shxmcpk")))

(define-public crate-bwapi_wrapper-0.3 (crate (name "bwapi_wrapper") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0psrkm1nzwflcjv04krl487dqs2l4aqb6jzdzqj425aspic3ciqq")))

(define-public crate-bwapi_wrapper-0.3 (crate (name "bwapi_wrapper") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.64") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1l10nbcix3ad6493ms32zmffvbkijldhxzbyggwrc1g45bpiq38y")))

(define-public crate-bwapi_wrapper-0.3 (crate (name "bwapi_wrapper") (vers "0.3.3") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mmd1f6v0xjbyrdmrsrkk4sc2k6ldwwn25r8yfmpz2pccwhdwk3m")))

