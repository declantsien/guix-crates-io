(define-module (crates-io bw sr) #:use-module (crates-io))

(define-public crate-bwsr-0.1 (crate (name "bwsr") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "feed-rs") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1cgdzmj4pjkvfrixjwhm9s54ma4wij07wzcyvasbyn88mmczyj7d")))

