(define-module (crates-io bw as) #:use-module (crates-io))

(define-public crate-bwasm-0.1 (crate (name "bwasm") (vers "0.1.0") (deps (list (crate-dep (name "parity-wasm") (req "^0.41") (default-features #t) (kind 0)) (crate-dep (name "wasmi-validation") (req "^0.3") (default-features #t) (kind 0)))) (hash "1acpj5cix0ss8kn0kd5jpvm7d6mdffl4dc461qr6bjgl2zi002pm")))

(define-public crate-bwasm-0.1 (crate (name "bwasm") (vers "0.1.1") (deps (list (crate-dep (name "parity-wasm") (req "^0.41") (default-features #t) (kind 0)) (crate-dep (name "wasmi-validation") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lxbixb5qp744dapyac1sgghsdcm093l08r11gw5890grc0rd3vd")))

