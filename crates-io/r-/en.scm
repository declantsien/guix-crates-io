(define-module (crates-io r- en) #:use-module (crates-io))

(define-public crate-r-env-0.1 (crate (name "r-env") (vers "0.1.4") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "13a0nmp7asbp6l5rknm565xbxwkhpilg13lvgzvqsjrqds7rjchc")))

(define-public crate-r-env-0.1 (crate (name "r-env") (vers "0.1.5") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "1i8mbpg746f2x35gv9kji3kxadv6fz78q7lb90v3gl23v73w8kdd")))

(define-public crate-r-env-0.9 (crate (name "r-env") (vers "0.9.0") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "1cq18l8jrw2fwjw0842naldf0mc3m7fp8y6fl0x1y06yixwghz4f")))

(define-public crate-r-env-0.9 (crate (name "r-env") (vers "0.9.1") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)))) (hash "1hfpaqdag21p7rg4y4h79yhd6l09na9xvn73j0cwvyjkc2j2bp7z")))

