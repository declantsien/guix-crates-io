(define-module (crates-io r- ca) #:use-module (crates-io))

(define-public crate-r-cache-0.1 (crate (name "r-cache") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)))) (hash "0v898pda6vb0125qvnj5y3jc0y3zbh9hl3gr3y2cc3kq47knbk5s")))

(define-public crate-r-cache-0.2 (crate (name "r-cache") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.6.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)))) (hash "0rjx8zqfdia8f6mcsrs4pqc3sya3vahfb7w0156c1mllx02b9kkk")))

(define-public crate-r-cache-0.2 (crate (name "r-cache") (vers "0.2.1") (deps (list (crate-dep (name "async-std") (req "^1.6.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.15") (default-features #t) (kind 0)))) (hash "1hqb7yxwksgc5qp8z3562i6hqh3rv03ir8k2jwl0dbinc0g6gq2j")))

(define-public crate-r-cache-0.3 (crate (name "r-cache") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "03v3v0iavfl6pvcggpj67bnfgjrp3ggzj1jb80w7lbgji7qv1mc4")))

(define-public crate-r-cache-0.3 (crate (name "r-cache") (vers "0.3.1") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "0311hmb51q1kr3kfmaa9qakxqgwp2kqnfhk86pi2mhnyclpw6jcx")))

(define-public crate-r-cache-0.3 (crate (name "r-cache") (vers "0.3.2") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "0zvg6yms0knfzvaqk7qzxanv5lwlrpyadfdzhv5nl6c2220bin7m")))

(define-public crate-r-cache-0.3 (crate (name "r-cache") (vers "0.3.3") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "0ys4i9v1j1qwh782xfs5p2z5n2jsadcg7wqfybj97gcihjnqdsjh")))

(define-public crate-r-cache-0.3 (crate (name "r-cache") (vers "0.3.4") (deps (list (crate-dep (name "async-std") (req "^1.6.5") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "0jigajas6qs19b0mqy12fksijfcy1ir9g9xjf5xkpp4n7sx6jm34")))

(define-public crate-r-cache-0.4 (crate (name "r-cache") (vers "0.4.0") (deps (list (crate-dep (name "async-std") (req "^1.8.0") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "1y5vyxz55xxvf4039m3padzlpz3ncaf6xcb6wx7a36917yf3c5rh")))

(define-public crate-r-cache-0.4 (crate (name "r-cache") (vers "0.4.1") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 0)))) (hash "1nrk63aycg9ngcixxjzm5m51qvqydpxgs9fclkzysddql5m0yd8a")))

(define-public crate-r-cache-0.4 (crate (name "r-cache") (vers "0.4.2") (deps (list (crate-dep (name "async-lock") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)))) (hash "1gs0mjnfa8cl4ma677lsg0kmih7vsxl9h7mps46wripkkp7b83rx")))

(define-public crate-r-cache-0.4 (crate (name "r-cache") (vers "0.4.3") (deps (list (crate-dep (name "async-lock") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 2)))) (hash "0dx94wfy1gcvf7c7k1sa8q8cg72330cjaaa1szvwa8iphpx7fsfv")))

(define-public crate-r-cache-0.4 (crate (name "r-cache") (vers "0.4.4") (deps (list (crate-dep (name "async-lock") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)))) (hash "13al31ll65cd0pqqm5fgnyvhay4774afixf2vddii7hw8yy0r5ay")))

(define-public crate-r-cache-0.4 (crate (name "r-cache") (vers "0.4.5") (deps (list (crate-dep (name "async-lock") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)))) (hash "094c09y7r6wjqbqv5rvm4j7nj5v7n7plgggcig4x7yi6nnwf51s5")))

(define-public crate-r-cache-0.5 (crate (name "r-cache") (vers "0.5.0") (deps (list (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.12") (features (quote ("attributes"))) (default-features #t) (kind 2)))) (hash "0yrz3idw8za0b8711g43lblca18w0z7s9d35sk6c3njw0bhfk883")))

