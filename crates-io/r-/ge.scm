(define-module (crates-io r- ge) #:use-module (crates-io))

(define-public crate-r-gen-0.0.1 (crate (name "r-gen") (vers "0.0.1") (deps (list (crate-dep (name "probability") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "r-gen-macro") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1kqpsnffwf1gfkqzv6jqn3qbh482xpsj61yb6rcdab32wsaz045p") (yanked #t)))

(define-public crate-r-gen-0.0.2 (crate (name "r-gen") (vers "0.0.2") (deps (list (crate-dep (name "probability") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "r-gen-macro") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "statrs") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0cipy0lhzn5piaw5ij66lnxcyb7gxg69k2jljfzxzzp69vz48yg5")))

(define-public crate-r-gen-macro-0.0.1 (crate (name "r-gen-macro") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0jwwyzhlykyias1a2r0m2vs410mp50pv1058g7x82yl1qmqzsj7r")))

