(define-module (crates-io r- gr) #:use-module (crates-io))

(define-public crate-r-graph-0.1 (crate (name "r-graph") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "17hk69pddd7ym25m1102l1ayw1vwvbs4wzcjcr3jz4vq2ym5bnww") (yanked #t)))

(define-public crate-r-graph-0.0.1 (crate (name "r-graph") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1b874l222fcbv83s21srxixgck5z1z4dcnyvmyp00lvbllrk835r") (yanked #t)))

(define-public crate-r-graph-0.0.11 (crate (name "r-graph") (vers "0.0.11") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "19nb8gjbppajawhwqvsha13fb7alcj0pvagmx8q4rfz4hx28hifm")))

(define-public crate-r-graph-0.0.12 (crate (name "r-graph") (vers "0.0.12") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "17gvka7fq3h8zjfjnv02q6ar6f89s0chjdnbvqa9678z6lqwpywl")))

(define-public crate-r-graph-0.0.13 (crate (name "r-graph") (vers "0.0.13") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "149y5q2bb397jg7rkfiih37kxqwqjxp4yvaq9cs7qmfx6f5bhii1")))

(define-public crate-r-graph-0.0.14 (crate (name "r-graph") (vers "0.0.14") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "06h5i5vgf56zcw4cbn9pxfsc8ys4xshgdrxsmjwrmv15ww0k29nx")))

(define-public crate-r-graph-0.1 (crate (name "r-graph") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1z5ap1n0wqjshs76i4pv1iq9ab5rww69gglnldr8aacr2rdkqcdb")))

(define-public crate-r-graph-0.1 (crate (name "r-graph") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0gsmaya08ax2kiz24fw4vak32xvc6qpk0p1f8cibg4rgg0c05w2l")))

(define-public crate-r-graph-0.1 (crate (name "r-graph") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "060nq9pacnnrgpx9grv5988c8bi5vbcpc9iwfcg9p2ml3cnk4c2m")))

(define-public crate-r-graph-0.1 (crate (name "r-graph") (vers "0.1.5") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "192rfys8raipfvjij1d500cnjfbfbhyn1h3im9kr7gqpxn9v6vl7")))

