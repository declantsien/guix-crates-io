(define-module (crates-io r- fr) #:use-module (crates-io))

(define-public crate-r-frame-0.1 (crate (name "r-frame") (vers "0.1.0") (deps (list (crate-dep (name "rocket") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.2") (features (quote ("serve"))) (kind 0)))) (hash "0n66wbk2sz8bvdqi0h43rc9l2jizc41krgxgr44c4k44wnfsfgz5")))

