(define-module (crates-io r- fa) #:use-module (crates-io))

(define-public crate-r-fairdist-0.1 (crate (name "r-fairdist") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16l1vwm4rbcypj6pv47pw1ly0p1xfcwvnvrf810lyymv8128yfdp")))

