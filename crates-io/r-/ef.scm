(define-module (crates-io r- ef) #:use-module (crates-io))

(define-public crate-r-efi-0.1 (crate (name "r-efi") (vers "0.1.0") (hash "1gxpy4yp8k0vghkzqfckhd0jngzj3zrv8qkzmzkavq3whsswnxfq")))

(define-public crate-r-efi-0.1 (crate (name "r-efi") (vers "0.1.1") (hash "0ahz8q86m9lqz8d9sxmq7m293r4dl21vpxgkvv5gbmfs0ql8iay0") (features (quote (("examples"))))))

(define-public crate-r-efi-1 (crate (name "r-efi") (vers "1.0.0") (hash "0qkybbxb6il7df8bfvz8q3w2880qmn917d3n681cfyqc40z08xrc") (features (quote (("examples"))))))

(define-public crate-r-efi-2 (crate (name "r-efi") (vers "2.0.0") (hash "01g4y12jlj3vc0nwrqbr6a5n38k291k48y0qdbmb7kbgz03ng3hd") (features (quote (("examples"))))))

(define-public crate-r-efi-2 (crate (name "r-efi") (vers "2.1.0") (hash "0gdk5gf4a8vv8wzfny2bfy5zm95p2pkkbwnjr6j56w1nayg7yb4g") (features (quote (("examples"))))))

(define-public crate-r-efi-2 (crate (name "r-efi") (vers "2.2.0") (hash "12276qb7f1v2xbfq76a6sjkzkf1h9ylz7nd0snr4wscdapmcwjkv") (features (quote (("examples"))))))

(define-public crate-r-efi-3 (crate (name "r-efi") (vers "3.0.0") (hash "17rrxpn9h6gpc5qif415vyajya77nfdip6h4vkhnnyal6z447n7h") (features (quote (("examples"))))))

(define-public crate-r-efi-3 (crate (name "r-efi") (vers "3.1.0") (hash "1pc3drinkpzkavpzvl4q0xnl58490qx3xpl22da1iich5cgviakf") (features (quote (("examples"))))))

(define-public crate-r-efi-3 (crate (name "r-efi") (vers "3.2.0") (hash "0yxahgn4mwnzkwnghzy7pxycddc9jlxyq2913w2wzn8gj2d9k631") (features (quote (("examples"))))))

(define-public crate-r-efi-4 (crate (name "r-efi") (vers "4.0.0") (hash "1f71ydg7gfxky150nk6kncwpcclklwrifcf073wxfr5kzyk5fhkc") (features (quote (("examples"))))))

(define-public crate-r-efi-4 (crate (name "r-efi") (vers "4.1.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")))) (hash "1wz6vikh4h86c8wxxhzkq4yq262nm65a49xhwx2nfg434b34awwy") (features (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("examples") ("efiapi"))))))

(define-public crate-r-efi-4 (crate (name "r-efi") (vers "4.2.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")))) (hash "04f939yk94c91dss2c46gjbns9gciyjfrxnxzayssm6sngcw4psp") (features (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi"))))))

(define-public crate-r-efi-4 (crate (name "r-efi") (vers "4.3.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")))) (hash "1q7j3bf4a5dh5lx8v32qp165jmr6sixidlr1wpwnfc1sw2b4y90f") (features (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi")))) (rust-version "1.68")))

(define-public crate-r-efi-4 (crate (name "r-efi") (vers "4.4.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")))) (hash "0d3saq12k8jfqr96k1jyw7156xqp7lr0a11vnws0dk646vv9cwf4") (features (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi")))) (rust-version "1.68")))

(define-public crate-r-efi-4 (crate (name "r-efi") (vers "4.5.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")))) (hash "18w6vl7ir0yv82vkmk7r7gnf1pk9vhcfzjd4q3giaiw5qppkbsg9") (features (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("native") ("examples" "native") ("efiapi")))) (rust-version "1.68")))

(define-public crate-r-efi-alloc-0.1 (crate (name "r-efi-alloc") (vers "0.1.0") (deps (list (crate-dep (name "r-efi") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "13jwcw7sd4dx6i105pk6j9yncyi240wlxmln7v1ss26w5g17f7iy") (features (quote (("examples") ("allocator_api"))))))

(define-public crate-r-efi-alloc-1 (crate (name "r-efi-alloc") (vers "1.0.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.79") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "r-efi") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1mrgw0hj9q1pb0cbq7igzld5i5lpjzj4rhyjqd5h9bdnwagz1mii") (features (quote (("rustc-dep-of-std" "compiler_builtins/rustc-dep-of-std" "core") ("examples") ("allocator_api"))))))

(define-public crate-r-efi-string-0.1 (crate (name "r-efi-string") (vers "0.1.0") (hash "12ixv9hy603izrp00hf4rnj4r6iy9jg6m7sk07la0ckjysi883dc")))

