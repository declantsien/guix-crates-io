(define-module (crates-io r- ls) #:use-module (crates-io))

(define-public crate-r-ls-0.1 (crate (name "r-ls") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "07bfn4h82hm9c8569b47c16hmj2x89z4dvsny11ay5rc0lzwn7cp")))

(define-public crate-r-ls-0.2 (crate (name "r-ls") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)))) (hash "0751fqmsrllghk7z72f1p4znijl3qpkdyggy9g30hh6dqwm6wfm2")))

