(define-module (crates-io r- lo) #:use-module (crates-io))

(define-public crate-r-lombok-macros-0.0.1 (crate (name "r-lombok-macros") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1klamxvgbs6lyidd67r68a811x5gnbg05avk7xch2ik6iv22l0kx")))

(define-public crate-r-lombok-macros-0.0.2 (crate (name "r-lombok-macros") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14av63yh0nak2szdhbkww67cn7ywdprq21s27yj9zma1hmymprs2")))

(define-public crate-r-lombok-macros-0.0.3 (crate (name "r-lombok-macros") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ihmha8cpsl6qsbbcw74sxmf4mwqwa0wpr9agcfrqkf5am8xxshh")))

