(define-module (crates-io pf he) #:use-module (crates-io))

(define-public crate-pfhe-0.0.1 (crate (name "pfhe") (vers "0.0.1") (deps (list (crate-dep (name "crypto-bigint") (req "^0.5.3") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "impl_ops") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1z9q8yk5zzm3jlmbl3ycg7jar8y06g7jzzisczfmaz7r04xk329k")))

