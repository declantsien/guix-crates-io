(define-module (crates-io pf ly) #:use-module (crates-io))

(define-public crate-pfly_rust-0.1 (crate (name "pfly_rust") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.3.12") (features (quote ("unix"))) (default-features #t) (kind 0)))) (hash "0f84vjzdyq3xkbdc9zjcvs6lf1giig9ya9l2bpqlp2qzigbk1cg7")))

(define-public crate-pfly_rust-0.1 (crate (name "pfly_rust") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.3.12") (features (quote ("unix"))) (default-features #t) (kind 0)))) (hash "0zdhx7nv8z075yqk6pbwizsdkg77df7p3nfdy3dim6jjirj8cwg9")))

