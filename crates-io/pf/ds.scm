(define-module (crates-io pf ds) #:use-module (crates-io))

(define-public crate-pfds-0.1 (crate (name "pfds") (vers "0.1.0") (hash "0aw4mxrjk6spqlqvlwvazs4cc3nhx14zp0gmchligdbz3sdfif6z")))

(define-public crate-pfds-0.1 (crate (name "pfds") (vers "0.1.1") (hash "038lr5f5b2b64g9pa5fswr2vv3vzg1vgqn0h1jq4jxv4gpqz2r25")))

(define-public crate-pfds-0.1 (crate (name "pfds") (vers "0.1.2") (hash "097zmskqva6vy1r0353micgfmi3dw573r7mcx7kc7ddq3h9xliyn")))

(define-public crate-pfds-0.2 (crate (name "pfds") (vers "0.2.0") (hash "1fikicn2dn14q1rk2azswks707qcbnia6wvhxs9nrqs5fbivs3fl")))

(define-public crate-pfds-0.3 (crate (name "pfds") (vers "0.3.0") (hash "0j8bhqqsszj4pch5w3jajwszgwjfzrrbwddfdq73159capnkwswm")))

(define-public crate-pfds-0.3 (crate (name "pfds") (vers "0.3.1") (hash "0qfzilhcfa09nsf4p1y2nl0jwq7xdyhgp5ahya1pgsj989qfrzss")))

(define-public crate-pfds-0.3 (crate (name "pfds") (vers "0.3.2") (hash "0ii6hvnmrlz3s1zf7s5y6pkw29xnr24g6fvzb378ack3ld4ykh0d")))

(define-public crate-pfds-0.3 (crate (name "pfds") (vers "0.3.3") (hash "0fnvy606qayl5216cm47fc4pppwx13yzi700w2ggd4winffc7kjz")))

(define-public crate-pfds-0.3 (crate (name "pfds") (vers "0.3.4") (hash "0ibb4bbsxcplsln5i7f2dz7kzrmpnx60x8h5gkrdzzr6c208fa9h")))

(define-public crate-pfds-0.4 (crate (name "pfds") (vers "0.4.0") (hash "132y5cb4ixkrlkc0v5w85n39js9jy4khxwisd7q0lih8v604vh3v")))

