(define-module (crates-io pf ff) #:use-module (crates-io))

(define-public crate-pffft_rust-0.1 (crate (name "pffft_rust") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.55") (default-features #t) (kind 1)))) (hash "1znv2svixwjibk9rbzhd0xdd1vkm7n0qk49wmmi9771b351nh0f2")))

(define-public crate-pffft_rust-0.1 (crate (name "pffft_rust") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.55") (default-features #t) (kind 1)))) (hash "1gl1sawkl5z57s5sm2576ygzj89j6jv9lysc6k7cadjcqkgbh461")))

(define-public crate-pffft_rust-0.1 (crate (name "pffft_rust") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.55") (default-features #t) (kind 1)))) (hash "1dcql2q70s5ykzpsn8gb2j08ym689y7m6h3ijxacbm8ajcm7yhf5")))

