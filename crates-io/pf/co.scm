(define-module (crates-io pf co) #:use-module (crates-io))

(define-public crate-pfconv-0.1 (crate (name "pfconv") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.27.2") (features (quote ("lazy" "parquet"))) (default-features #t) (kind 0)))) (hash "0jx7nl9gbrbjw85x0b5knddkvn7c84zp8y9r5wj3ms9grcgacrr2")))

(define-public crate-pfconv-0.1 (crate (name "pfconv") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.27.2") (features (quote ("lazy" "parquet"))) (default-features #t) (kind 0)))) (hash "0ydq135p88qai6bjmh0zd355v2cis65jcq0hb3j6m2dx66bz36f7")))

