(define-module (crates-io pf or) #:use-module (crates-io))

(define-public crate-pformat_macro-0.0.1 (crate (name "pformat_macro") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1w1jxh7klizmbmn0kpz72l894n2mkrfz1a63r3yyasf93galmnzi")))

