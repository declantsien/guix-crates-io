(define-module (crates-io pf ec) #:use-module (crates-io))

(define-public crate-pfecs-0.1 (crate (name "pfecs") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1p24r53n2cza10cbvmhqdj4q1q86yyjgkajwyzykvwdsgln880qy") (yanked #t)))

