(define-module (crates-io pf la) #:use-module (crates-io))

(define-public crate-pflag-0.1 (crate (name "pflag") (vers "0.1.0") (hash "1pl4a4lzriy3r4cd8xq204z64k7cy9a5qzndlrja8bz2vz69sx0x")))

(define-public crate-pflag-0.2 (crate (name "pflag") (vers "0.2.1") (hash "1n1ldlzi0gkq367rnjdm7w2cqv0w5lhs3mlrs97ww0shq7daxyfz")))

(define-public crate-pflag-0.3 (crate (name "pflag") (vers "0.3.0") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zpn85p9a4ac0qggpd48za90k0nlvky3wy4sbv6kyszyq5rksjkr")))

(define-public crate-pflag-0.4 (crate (name "pflag") (vers "0.4.0") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fj171xqxxba7xkh41ak2wn27j1w1hs0i0pbm6ngy07bxfzg3lip")))

(define-public crate-pflag-0.4 (crate (name "pflag") (vers "0.4.1") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zm2l6cimgb3d10ddx04fq69kwkmzxcci0gkh00lg9q2acaz9bwp")))

(define-public crate-pflag-0.5 (crate (name "pflag") (vers "0.5.0") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aky8fnxmh6c63j1h87pnk92crzgck0zx685jd7rs0xx9f3rl634")))

(define-public crate-pflag-0.5 (crate (name "pflag") (vers "0.5.1") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y319xfbbb3pic34wkvrqb784q9ra1zcbxrfgmf9hc1qdg41hkah")))

(define-public crate-pflag-0.5 (crate (name "pflag") (vers "0.5.2") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b0rshy6zpsa0vjvh66580nkrnhknrx6qagdfjlv74pvrfhcgq8a")))

(define-public crate-pflag-0.6 (crate (name "pflag") (vers "0.6.0") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jjj32qzn6nqqin662w7srkcd7gbz7kx68k9id5qj5rs0x7s3vb9")))

(define-public crate-pflag-0.6 (crate (name "pflag") (vers "0.6.1") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b62aw738r9kamsbjnwcnv2q5mris715mmb0n2cx570k06wwqi7v")))

(define-public crate-pflag-0.6 (crate (name "pflag") (vers "0.6.2") (deps (list (crate-dep (name "concat-idents") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lzxlqi9rsz21p61jv8dbsfx54irgikschvmx2jf5lljqapbs1m8")))

