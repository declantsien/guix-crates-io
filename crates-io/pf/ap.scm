(define-module (crates-io pf ap) #:use-module (crates-io))

(define-public crate-pfapack-0.1 (crate (name "pfapack") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (kind 0)) (crate-dep (name "pfapack-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1r9xmn137i7jqisr5jvijp1fiff8cb7i83qimmwhh389cw8h9lcy")))

(define-public crate-pfapack-0.2 (crate (name "pfapack") (vers "0.2.0") (deps (list (crate-dep (name "assert") (req "^0.7.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (kind 0)) (crate-dep (name "pfapack-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "13afaf39cxcps557hwgxf60cakasyr4cj3waj05wsmpriiczwpg8")))

(define-public crate-pfapack-sys-0.1 (crate (name "pfapack-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (kind 0)))) (hash "1h72zndzjcald5rcglcbf6b1jw9yjqpvwgghz6k5mx2dkr8gx9gm") (links "pfapack")))

(define-public crate-pfapack-sys-0.1 (crate (name "pfapack-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (kind 0)))) (hash "1r2iw8nn0paqz1f1f6ykvqlvnj31xbb83bbx74836f8bkc7zy68s") (links "pfapack")))

