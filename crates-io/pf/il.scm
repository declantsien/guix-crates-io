(define-module (crates-io pf il) #:use-module (crates-io))

(define-public crate-pfile-0.1 (crate (name "pfile") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0x73pyqkn5yf9k5y27k98gmf4whvyilhzi8cqhalp2vbzdhki9li")))

(define-public crate-pfile-0.1 (crate (name "pfile") (vers "0.1.1") (deps (list (crate-dep (name "indicatif") (req "^0.17.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y9xcxflkgzaq4mn4y4npdlsm3yxwajcn8rm1wlihw6y690v61yj")))

