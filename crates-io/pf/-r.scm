(define-module (crates-io pf -r) #:use-module (crates-io))

(define-public crate-pf-rs-13 (crate (name "pf-rs") (vers "13.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "15cf6l7pwk75jy1igq63rg0rf2sz89dpkik62llpn753xg0lqppk") (features (quote (("log_to_stdout") ("default")))) (yanked #t)))

(define-public crate-pf-rs-13 (crate (name "pf-rs") (vers "13.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1jrmp34vvbjppb2njpmyl6lshw6rwwldsdliv0mnsyfp496rq9wk") (features (quote (("log_to_stdout") ("default")))) (yanked #t)))

(define-public crate-pf-rs-13 (crate (name "pf-rs") (vers "13.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1arnz428biw364q8q585s1nz3lf120rx00da71z3fi17fi5vspkp") (features (quote (("log_to_stdout") ("default")))) (yanked #t)))

(define-public crate-pf-rs-13 (crate (name "pf-rs") (vers "13.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0f30zqww16yr27524mrdbz3f4b050hpxafbi9vlv99xhr5ajcm74") (features (quote (("log_to_stdout") ("default")))) (yanked #t)))

(define-public crate-pf-rs-13 (crate (name "pf-rs") (vers "13.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 0)))) (hash "00wspxycg7w3w219gy2xkzdc96q4qpfl4s1vx48i58gsxrkq61ib") (features (quote (("log_to_stdout") ("default")))) (yanked #t)))

(define-public crate-pf-rs-13 (crate (name "pf-rs") (vers "13.2.1") (deps (list (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 0)))) (hash "101yhbkdpjy8m4z76zk1w3z3b8n029yd3b3my1ipjixjv9mx6bq9") (features (quote (("log_to_stdout") ("default")))) (yanked #t)))

(define-public crate-pf-rs-0.2 (crate (name "pf-rs") (vers "0.2.2") (deps (list (crate-dep (name "nix") (req "^0.27") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "0j3w1a2bxbyivshqn20k68clij478vkj7krfxrinkjdg7cs3ghy1") (features (quote (("log_to_stdout") ("default") ("FREEBSD_V14_0" "FREEBSD_C1") ("FREEBSD_V13_2" "FREEBSD_C1") ("FREEBSD_V13_1" "FREEBSD_C1") ("FREEBSD_V13_0" "FREEBSD_C1") ("FREEBSD_C1"))))))

(define-public crate-pf-rs-0.2 (crate (name "pf-rs") (vers "0.2.3") (deps (list (crate-dep (name "nix") (req "^0.27") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "0pfbc3cxcxidn3b4d5mkyqg7gcl5vs6pbz03bgc8xzs7na2208q9") (features (quote (("log_to_stdout") ("default") ("FREEBSD_V14_0" "FREEBSD_C1") ("FREEBSD_V13_2" "FREEBSD_C1") ("FREEBSD_V13_1" "FREEBSD_C1") ("FREEBSD_V13_0" "FREEBSD_C1") ("FREEBSD_C1"))))))

(define-public crate-pf-rs-0.2 (crate (name "pf-rs") (vers "0.2.4") (deps (list (crate-dep (name "nix") (req "^0.27") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "1920x6j45a0z498cqish033lcqryaf3ca0kn74291653zq2c5lb6") (features (quote (("log_to_stdout") ("default") ("FREEBSD_V14_0" "FREEBSD_C1") ("FREEBSD_V13_2" "FREEBSD_C1") ("FREEBSD_V13_1" "FREEBSD_C1") ("FREEBSD_V13_0" "FREEBSD_C1") ("FREEBSD_C1"))))))

