(define-module (crates-io pf en) #:use-module (crates-io))

(define-public crate-pfen-1 (crate (name "pfen") (vers "1.2.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror-no-std") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0j1m2ckqshryq675c4zxjfwk66jig2ks3xd395bp1hjpicv27jvs")))

