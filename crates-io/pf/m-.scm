(define-module (crates-io pf m-) #:use-module (crates-io))

(define-public crate-pfm-sys-0.0.1 (crate (name "pfm-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0m8p7f2mr47spdvawx3i11fygdglsi4np3y5pq58zj9fg8p5r63a") (yanked #t)))

(define-public crate-pfm-sys-0.0.2 (crate (name "pfm-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0yqb6xi5fwrq51zs348dz60fyd1dda91pacwxbif58l0lx1qfkbh") (yanked #t)))

(define-public crate-pfm-sys-0.0.3 (crate (name "pfm-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0zrdcfnsxf86na867ra4q36a5cpc3r5dlbc087i8lcqsds484ryw") (yanked #t)))

(define-public crate-pfm-sys-0.0.4 (crate (name "pfm-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1wdzq89vkk5xkyrrj2dqbmc18f8cdx4qx04w2w17m7w9izj5wclv") (yanked #t)))

(define-public crate-pfm-sys-0.0.5 (crate (name "pfm-sys") (vers "0.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0m4n2c2z44r7mj4m9y06046lz0b4h50hdi01rivafccdm8hdk9nh") (yanked #t)))

(define-public crate-pfm-sys-0.0.6 (crate (name "pfm-sys") (vers "0.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0i1mb0f2qnnssl9b9xqi8q29b7b1qa3fv6agvj10lsfi73zv2srx") (yanked #t)))

(define-public crate-pfm-sys-0.0.7 (crate (name "pfm-sys") (vers "0.0.7") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1mb5nfcnys2hkrbb5d6bz3laqq7mzqnqx25l812i02554226p00b") (yanked #t)))

(define-public crate-pfm-sys-0.0.8 (crate (name "pfm-sys") (vers "0.0.8") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.94") (default-features #t) (kind 0)) (crate-dep (name "pfm-sys") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "17wmv5n50x974330kfl7bscdcbxi593n8x66778lgyfhks4yi2v9") (yanked #t)))

(define-public crate-pfm-sys-0.0.9 (crate (name "pfm-sys") (vers "0.0.9") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0zhhiiwkh1rra92y4dgvcqkmzpy7155cv47vyhdlpzwgaz2xvka4") (yanked #t)))

(define-public crate-pfm-sys-0.0.10 (crate (name "pfm-sys") (vers "0.0.10") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1qxr45yxx7py6933vbdl8gpcfn7qk1k9xdd6wcjvj9fjv2ph704y") (yanked #t)))

(define-public crate-pfm-sys-0.0.11 (crate (name "pfm-sys") (vers "0.0.11") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0c3f2v752z3hhwrn81vv09spm28q0zqx1b8fnl8y393j1jnlv5ym")))

(define-public crate-pfm-sys-0.0.12 (crate (name "pfm-sys") (vers "0.0.12") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1jii7q359hb2iakvlsvap4flf9pnyfddr92nr0azpd415q3x3l0h")))

(define-public crate-pfm-sys-0.0.13 (crate (name "pfm-sys") (vers "0.0.13") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1wr3gqiqqirl8m2bqvffv6ikrq0yq6amyv5np8jc6am3j261jhg6")))

(define-public crate-pfm-sys-0.0.14 (crate (name "pfm-sys") (vers "0.0.14") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)))) (hash "0kd49h47w5f8af9vki1q769m5ki9mdahmjacr8942svhrkmmfbdc")))

(define-public crate-pfm-sys-0.0.15 (crate (name "pfm-sys") (vers "0.0.15-libpfm4.533633ad") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)))) (hash "0lky16n3196sm5d3qlhlg16ip64yxm1vj1781r6lmwnk8v13rra3")))

(define-public crate-pfm-sys-0.0.16 (crate (name "pfm-sys") (vers "0.0.16-libpfm4.13.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1kqnz4j19frz4k0bcchbk3ajkrx43fx6hzj8kdbfv2z8qbgff85m")))

(define-public crate-pfm-sys-0.0.17 (crate (name "pfm-sys") (vers "0.0.17-libpfm4.13.0+535c2042") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "1nlxsc200vm743an69wgc5gcsfxsjbczjw0c3hw5j3ia6aszzy03")))

(define-public crate-pfm-sys-0.0.18 (crate (name "pfm-sys") (vers "0.0.18+libpfm4.13.0-42d6d0c3") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "14351khgkk2c8hdk6xqfkhp5q5m6dfw0qvd1jmibjv70xvwadklf")))

