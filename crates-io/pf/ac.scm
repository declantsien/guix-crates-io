(define-module (crates-io pf ac) #:use-module (crates-io))

(define-public crate-pfacts-0.1 (crate (name "pfacts") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0a8ndncy7695fia6b717smvgkyxs6h0kyla9sivg2c2c6bdypaw3")))

