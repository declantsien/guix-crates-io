(define-module (crates-io pf du) #:use-module (crates-io))

(define-public crate-pfdump-0.1 (crate (name "pfdump") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "16v422qx8nkgr703p40rbi7lpm0p4acw122pqcpccchfrrds2yqm")))

