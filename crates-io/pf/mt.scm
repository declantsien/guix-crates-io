(define-module (crates-io pf mt) #:use-module (crates-io))

(define-public crate-pfmt-0.1 (crate (name "pfmt") (vers "0.1.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "galvanic-test") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0p7q5yw6pj2ndsblrmd1vynm0lgp19fysywjssi4262y3b8rwy68")))

(define-public crate-pfmt-0.2 (crate (name "pfmt") (vers "0.2.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "galvanic-test") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "09akj5794qk69nxwq7dw0c6n9nkc9kx1iakjk90277v7lijx074m")))

(define-public crate-pfmt-0.3 (crate (name "pfmt") (vers "0.3.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "galvanic-test") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05hh10zzsyb1nbs2xmps0px56sjaw8y8h71nadsmjyxw34fgzmw8")))

(define-public crate-pfmt-0.4 (crate (name "pfmt") (vers "0.4.0") (deps (list (crate-dep (name "galvanic-assert") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "galvanic-test") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0g7cg88g1bzc3gcr1a10m6n6dakl24zkzzh1rxvnwmizljq6vagq")))

