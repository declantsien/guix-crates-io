(define-module (crates-io so rc) #:use-module (crates-io))

(define-public crate-sorceress-0.1 (crate (name "sorceress") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "rosc") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)))) (hash "0vyijq7n45r1bwzwpzxrw3hn482ak9ma09q3876rcxc8fv1jhv6q")))

(define-public crate-sorceress-0.2 (crate (name "sorceress") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "rosc") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "14d5gvb1jpk118b3ni7vyk064zlj9avd3184p2a9vwpg6k1xjg8i")))

(define-public crate-sorcery-0.1 (crate (name "sorcery") (vers "0.1.0") (hash "013dn17x4k4j4cc9pwqi293aqgayx7j2ywbxxp7gk3had8a56aa7")))

