(define-module (crates-io so lh) #:use-module (crates-io))

(define-public crate-solhop-0.1 (crate (name "solhop") (vers "0.1.0") (deps (list (crate-dep (name "msat") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rsat") (req "=0.1.12") (default-features #t) (kind 0)) (crate-dep (name "solhop-types") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fwrcgxn3a1j1yrz256516j1h64f646b2lg5rijybwprkrqbs43k")))

(define-public crate-solhop-types-0.1 (crate (name "solhop-types") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "1h9ngsqqgmr1n6s8ww75p1cg0l17xkv41hqs77pyn4wy0a367gsp")))

