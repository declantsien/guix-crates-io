(define-module (crates-io so a-) #:use-module (crates-io))

(define-public crate-soa-rs-0.3 (crate (name "soa-rs") (vers "0.3.0") (deps (list (crate-dep (name "soa-rs-derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xakljbvmnbx6npqwzsb5rwr1k29b52rm4p9hglvpja8y4spfp32")))

(define-public crate-soa-rs-0.3 (crate (name "soa-rs") (vers "0.3.1") (deps (list (crate-dep (name "soa-rs-derive") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0piwzga99yw02rzwmsdpwlbaa9za898d4fmkbg3ik8zmbzlmif2f")))

(define-public crate-soa-rs-0.4 (crate (name "soa-rs") (vers "0.4.0") (deps (list (crate-dep (name "soa-rs-derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "19pvl40hmgr6987q9424f5jy4yki6k36i9zmzvqsd5lh95z1zqg2")))

(define-public crate-soa-rs-0.5 (crate (name "soa-rs") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0.199") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "soa-rs-derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "15qpg1n9m6dznndf8g8pqbvw7mz0vyr1ffk0r6knfbcfjamjngwa") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-soa-rs-0.5 (crate (name "soa-rs") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "^1.0.199") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "soa-rs-derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1p7nf7gmvwnpl21whsk7svyrsl0ia9vlgasjk2i52hw9mhsiamy8") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-soa-rs-0.6 (crate (name "soa-rs") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0.199") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "soa-rs-derive") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0n536xc7188d2msmk6hqil233xbdlk6zikxhqal1x2ngfd47381h") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-soa-rs-0.6 (crate (name "soa-rs") (vers "0.6.1") (deps (list (crate-dep (name "serde") (req "^1.0.199") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "soa-rs-derive") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "14ydga3hbjp55k59m2w58z8lgk8l5qckx8l2dd0hv9h9zxfnhxjw") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-soa-rs-derive-0.3 (crate (name "soa-rs-derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0qi1kvkbk50dmm16dkwyf3n2587fr2gykh76jxf8gfap6m3lh69w")))

(define-public crate-soa-rs-derive-0.3 (crate (name "soa-rs-derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0xj58kg1xsk4v5hr1hs1xw6sq88xgrppsxafhbh6xz4q2zg2h8s2")))

(define-public crate-soa-rs-derive-0.4 (crate (name "soa-rs-derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0wb9clpah2kym4ggnwdiwcsvhhkmq5q5s6nn8p9nk4yjjlvzlaf7")))

(define-public crate-soa-rs-derive-0.6 (crate (name "soa-rs-derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0xqmhg53s96dc3qil3l3krqi5b5bzybg5nnahipcrrm7bk91ya54")))

(define-public crate-soa-vec-0.1 (crate (name "soa-vec") (vers "0.1.0") (deps (list (crate-dep (name "second-stack") (req "^0.2.0") (features (quote ("experimental"))) (default-features #t) (kind 0)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1pqjcmcggfgs7w671vc53p0a3fcq2dsz723g47p7n6xc38zzklv1")))

(define-public crate-soa-vec-0.2 (crate (name "soa-vec") (vers "0.2.0") (deps (list (crate-dep (name "second-stack") (req "^0.2.0") (features (quote ("experimental"))) (default-features #t) (kind 0)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0cxq0rn0cw5zrif0jzcnn6yql7n2pjq5s0yjzg2f809a11jsg6dd")))

(define-public crate-soa-vec-0.3 (crate (name "soa-vec") (vers "0.3.0") (deps (list (crate-dep (name "second-stack") (req "^0.2.0") (features (quote ("experimental"))) (default-features #t) (kind 0)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "11438pln8rrg91ndnr0qmq8scvj1f8xh70ixbv6p26v7d92c3rgb")))

(define-public crate-soa-vec-0.4 (crate (name "soa-vec") (vers "0.4.0") (deps (list (crate-dep (name "second-stack") (req "^0.2.0") (features (quote ("experimental"))) (default-features #t) (kind 0)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1xpzcaql6lchhxmn1qrqyy3vm8hd7licczd8b4385x3sl2d6rnmh")))

(define-public crate-soa-vec-0.5 (crate (name "soa-vec") (vers "0.5.0") (deps (list (crate-dep (name "second-stack") (req "^0.2.0") (features (quote ("experimental"))) (default-features #t) (kind 0)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0vvif24vr7n36w99jkfxnlk81r1zmg2lciafh1vs190yr8jqpvfb")))

