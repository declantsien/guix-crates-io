(define-module (crates-io so gi) #:use-module (crates-io))

(define-public crate-sogi_pll-0.1 (crate (name "sogi_pll") (vers "0.1.0") (deps (list (crate-dep (name "pid") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "11ih8akfrplr5395dzzw50n7rqhvlh90dapi24pq5szr4caz473j")))

(define-public crate-sogi_pll-0.2 (crate (name "sogi_pll") (vers "0.2.0") (deps (list (crate-dep (name "micromath") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0qnbjr4k9x5d72hifmwgf97v0grm54vg3fyfnglgdvwxlckqgzii")))

(define-public crate-sogi_pll-0.2 (crate (name "sogi_pll") (vers "0.2.1") (deps (list (crate-dep (name "micromath") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1p3ab58hgy7l45x6zjppkjfnsa0sa0idi33zsd3kiz3wwrvcz1li")))

