(define-module (crates-io so i-) #:use-module (crates-io))

(define-public crate-soi-io-0.1 (crate (name "soi-io") (vers "0.1.0") (deps (list (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1wak03vmx06ndpwfxw2g8w7g0niww5zhi8dbqsahlngmw211gz7g")))

(define-public crate-soi-io-0.1 (crate (name "soi-io") (vers "0.1.1") (deps (list (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0zvyxrbp7d0bld9f1vpzh1q5svsf48r8sqs7v9lwb37xvng76dbm")))

(define-public crate-soi-io-0.1 (crate (name "soi-io") (vers "0.1.2") (deps (list (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "188ljn6k76gskdk1l4xh1pfpqkll0c5l6flss9x76jvc1brzrvhi")))

(define-public crate-soi-io-0.1 (crate (name "soi-io") (vers "0.1.3") (deps (list (crate-dep (name "text_io") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0r9djy02khm9c61lx2sz9c9hzbqsczrp944gzga6mmpbq09yydr0")))

