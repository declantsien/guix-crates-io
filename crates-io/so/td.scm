(define-module (crates-io so td) #:use-module (crates-io))

(define-public crate-sotdb-0.1 (crate (name "sotdb") (vers "0.1.0") (hash "14lkg0k8vf145y300q557cqkqd2w1rk0wf9571k0kkw57achn3qj") (yanked #t)))

(define-public crate-sotdb-0.1 (crate (name "sotdb") (vers "0.1.1") (hash "0czpnx30cs8d486xj359zv53l5iaw1k75fa0c9idc045yh12c7x6") (yanked #t)))

(define-public crate-sotdb-0.1 (crate (name "sotdb") (vers "0.1.2") (hash "1b3bivvwhy5fv5ldc60dbanw5mh9mm0l01jjsyzj2708n65afjii") (yanked #t)))

(define-public crate-sotdb-0.1 (crate (name "sotdb") (vers "0.1.3") (hash "0swimb2ncq9f78iqxpq24ii9vmvgqz15rzzllh1rnads0xb2dzag") (yanked #t)))

(define-public crate-sotdb-0.1 (crate (name "sotdb") (vers "0.1.4") (hash "0z01y2hxbf9vrfp0ck5kxi4srsnsnrpjndfdpy4q6q4x1qvnglkp")))

(define-public crate-sotdb-0.1 (crate (name "sotdb") (vers "0.1.5") (hash "14d00l2j3r1psp586rqx3aayywb27p0rbb9y7xgqksdrh8jvnm4l")))

