(define-module (crates-io so ak) #:use-module (crates-io))

(define-public crate-soak-0.1 (crate (name "soak") (vers "0.1.0") (deps (list (crate-dep (name "soak-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qwg3i4j9ndwgs11lm1p1f4amgnaghzr6z5ynaarw88n6qqiw6x2")))

(define-public crate-soak-0.2 (crate (name "soak") (vers "0.2.0") (deps (list (crate-dep (name "dioptre") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "soak-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iwhbd5ardyspcxav1ijsckfyp2xb3ybrlpbv6za735pwz2893kh")))

(define-public crate-soak-derive-0.1 (crate (name "soak-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "090p6pq60lzjzfv70qqrgz1p7sqfbmmii7r1vhzzrrsj3x0cspq4")))

(define-public crate-soak-derive-0.2 (crate (name "soak-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "170iy8qdhydws8vcqs18qn205bmasfg223j8dm5d24gjg32wvi1n")))

