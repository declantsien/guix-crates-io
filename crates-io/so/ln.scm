(define-module (crates-io so ln) #:use-module (crates-io))

(define-public crate-solnedgang-0.0.1 (crate (name "solnedgang") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mci70jwqjijl7fj41m78l0091mikkv59dncz72iailwmzmv5vak")))

(define-public crate-solnedgang-0.0.2 (crate (name "solnedgang") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k5161y9zxvm8zpk0a2v02g8lvf1cfm8mp23czjxmgmlr2rmyb1x")))

(define-public crate-solnedgang-0.0.3 (crate (name "solnedgang") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1y6w7gibb4kx41bz3415vffa73pc212p026brrawkh0gskz3vxqc")))

(define-public crate-solnedgang-0.0.4 (crate (name "solnedgang") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "11r9r4xlg4xa2f7ra7dwwvlyyv8gvpmksz7s3mdzavh3dx4md3qa")))

