(define-module (crates-io so bo) #:use-module (crates-io))

(define-public crate-sobol-0.1 (crate (name "sobol") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1nkn05g7apbdc054pxh847nydrllcwldhnazkjqdclwhbipwi4qr")))

(define-public crate-sobol-0.1 (crate (name "sobol") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zj4ck0w9qqvikvs7d1j6wwz3cx24zalazyif2wp2v4i2h0a2an5")))

(define-public crate-sobol-1 (crate (name "sobol") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x76l2xqcw17c5bfp3pfj6yj2ayn684risbxbrijryya5hsmzzgl")))

(define-public crate-sobol-1 (crate (name "sobol") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "09vm1mb7gygn8bmcc948z3izdmvijrms72cfnppasx2izflshr8r")))

(define-public crate-sobol-1 (crate (name "sobol") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libflate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "037jxmka5apfzyf10qqnvgx4srzhqapgi9qrmf663qnnhqc2d2bc")))

(define-public crate-sobol_burley-0.1 (crate (name "sobol_burley") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0rf9yjp00lk5jqknh19bpz882dyzhqqvq6h3sixww7hfxws7cccj")))

(define-public crate-sobol_burley-0.2 (crate (name "sobol_burley") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0r5802mf9dkgrpikqknn6b8x45wn0kzxjd13gsc7lcvxj8wymlxr")))

(define-public crate-sobol_burley-0.3 (crate (name "sobol_burley") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0l6mad55hvia4mr0ffk2qn6mc3g8s5i48ailabhv3wdi165m5qr6")))

(define-public crate-sobol_burley-0.3 (crate (name "sobol_burley") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0pc0xmk3j38pg0ap4gzj8j9jp2x7l6wfqqqljiif4w7gdip922r7") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-sobol_burley-0.4 (crate (name "sobol_burley") (vers "0.4.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "185n1i4sm0winq72rhwrjpwsw4rpk0dbazlf3cpkfvl9pcmv6h9l") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-sobol_burley-0.5 (crate (name "sobol_burley") (vers "0.5.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1lg1mx9jxiaxyipvginmk9l3p0gp4rxdwghmfy1hgi4p3np7rwq9") (features (quote (("simd") ("default" "simd"))))))

