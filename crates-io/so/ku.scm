(define-module (crates-io so ku) #:use-module (crates-io))

(define-public crate-soku-0.1 (crate (name "soku") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1pjan1fnkllq5wlka1lyw20i8dxjznh4bq7672z50z9dgb7a62nn")))

(define-public crate-soku-0.1 (crate (name "soku") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.27") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "056piwxkw9j5i9wwh6dvy8jyii3fwyiznb7xw686i098rz58ds5m")))

