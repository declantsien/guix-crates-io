(define-module (crates-io so so) #:use-module (crates-io))

(define-public crate-sosorted-0.1 (crate (name "sosorted") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1iancpag2zfcygx2ssvk51a38lgl0yg1gwkww2fm085q34yyprmw")))

