(define-module (crates-io so nc) #:use-module (crates-io))

(define-public crate-soncai-0.0.1 (crate (name "soncai") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.8") (features (quote ("compat" "thread-pool"))) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nslrig3zyrqgraza73is1lcsvifn7icm0cb4f34a9fz29bc8prf")))

