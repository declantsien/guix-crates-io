(define-module (crates-io so lf) #:use-module (crates-io))

(define-public crate-Solf-0.1 (crate (name "Solf") (vers "0.1.0") (hash "1qa6srinywzk84i38zm3c4gqa57acz6nvz1b1d7kk0r9a8cm63f5")))

(define-public crate-Solf-0.0.0 (crate (name "Solf") (vers "0.0.0") (hash "0kjfk8kj7j4lr1f8r5ng9kfvyvfja41757kzqlc3d2h6d1d0a2pv")))

(define-public crate-solfmt-0.1 (crate (name "solfmt") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0874n1i7r26rx7fj06flw3wsg48dw7x5jh72mdbm6wbmbii7jxka")))

(define-public crate-solfmt-0.2 (crate (name "solfmt") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1mgy9g9nsxmviadd29cvac9m6misa4y1wikzvhblwj9gklbk7gxa")))

(define-public crate-solfmt-0.2 (crate (name "solfmt") (vers "0.2.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0rh84sg16agnlha04rgh5ia52a00r2i0lag8fdz3yqh44lp8zj8p")))

(define-public crate-solfmt-0.3 (crate (name "solfmt") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0swjfrd9lvv0j9bjfh13zbvcm3a3555qwgqnbv1lhzqpa8jlqwda")))

(define-public crate-solfmt-0.3 (crate (name "solfmt") (vers "0.3.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1lqyc6c8m8ak0f6lyrza2xwypldixx0lmdqb5z7ijqfic38k3dx0")))

