(define-module (crates-io so us) #:use-module (crates-io))

(define-public crate-sous-0.1 (crate (name "sous") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0nl6wxyfh429pqa6hbw8z3w71sq7372pf06fi8d9nxpc7yzhnvrw")))

(define-public crate-sous-0.2 (crate (name "sous") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.13") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0bww7x3wc4nawqjc7xl4fy57gb6h2ninbkrp4qm1363pwx3saq8m")))

(define-public crate-sous-0.3 (crate (name "sous") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "06w6i94j0wgf47kdpvjr7rmxi6qcgsjvk0fn3m2vwkss4kfr65ir")))

(define-public crate-sousukerai_hello_world-0.1 (crate (name "sousukerai_hello_world") (vers "0.1.0") (hash "0dx52xpy3q7k7afxiiyxjhi85h1p3n4d2sghm2dbz2xxc46yyfbs")))

