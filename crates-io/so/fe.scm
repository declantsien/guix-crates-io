(define-module (crates-io so fe) #:use-module (crates-io))

(define-public crate-sofe-cratesio-hyphen-test-0.1 (crate (name "sofe-cratesio-hyphen-test") (vers "0.1.0") (deps (list (crate-dep (name "snafu") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "08j1bnlihlxcwvv4glkwwvzhy8jsi1r8bcwm8dlwh3i7mjzzi6k5")))

