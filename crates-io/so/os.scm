(define-module (crates-io so os) #:use-module (crates-io))

(define-public crate-soos-sample-project-1 (crate (name "soos-sample-project") (vers "1.0.0") (hash "1c2519zgwgysrqvlrd35r4h7m4pzcn5g0xpyd872hbrn1b21k5bk")))

(define-public crate-soos-sample-project-1 (crate (name "soos-sample-project") (vers "1.0.1") (deps (list (crate-dep (name "aac") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "acc_reader") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "09814iicqmc3msk9l15bnvshc6g7214f4krkafr5i03anfm0v1x2")))

