(define-module (crates-io so xr) #:use-module (crates-io))

(define-public crate-soxr-0.6 (crate (name "soxr") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.14") (features (quote ("derive" "must_cast" "min_const_generics"))) (default-features #t) (kind 0)) (crate-dep (name "libsoxr-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yf9vanx25macrnfblwxkd4bxay4i7zmawi1qf4svnh2av36pjnk")))

